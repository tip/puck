package org.tip.puck.visualization.layouts.hierarchical;

import java.awt.Color;
import java.util.Map.Entry;
import java.util.*;
import org.gephi.graph.api.*;
import org.gephi.layout.plugin.AbstractLayout;
import org.gephi.layout.spi.LayoutBuilder;
import org.gephi.layout.spi.LayoutProperty;
import org.tip.puck.visualization.layouts.GraphSource;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.ManyValuesMap;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.ChainCluster;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.ColoredChain;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.NodeLayoutData;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.Triad;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.containers.BorderContainer;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.containers.ChainContainer;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.partition.Partition;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.partition.PartitionItem;

/**
 * @date May 23, 2012
 *
 * @author Edoardo Savoia
 */
public class HierarchicalLayout extends AbstractLayout {

    // Flags
    private boolean executing = false;
    private static final int clusterDistance = 50;
    private static final int containerDistance = 50;
    
    private static final int nodeXDistance = 50;
    private static final int nodeYDistance = -550;
    private static final int marriedXDistance = 50;
    private static final int marriedYDistance = 100;
    private static final boolean centered = true;

    
    private GraphSource graphSource;
    private List<ChainCluster> rootsClusters = new ArrayList<ChainCluster>();
    

    public HierarchicalLayout(LayoutBuilder layoutBuilder) {
	super(layoutBuilder);
    }

    @Override
    public void initAlgo() {
	executing = true;

	MixedGraph graph = graphModel.getMixedGraph();
	graph.readLock();

	Set<Node> rootNodes = new HashSet<Node>();

	Node[] nodes = graph.getNodes().toArray();
	for (Node node : nodes) {
	    node.getNodeData().setX(0.0f);
	    node.getNodeData().setY(5000.0f);

	    Set<Node> parents = graphSource.getParents(node);
	    
	    if (parents.isEmpty()) {
		if (!graphSource.getChildren(node).isEmpty()) {
		    rootNodes.add(node);
		}
		else {
		    node.getNodeData().setColor(0.0f, 0.0f, 0.0f);
		    NodeLayoutData.color(node, NodeLayoutData.Color.BLACK);
		}
	    }
	    else {
		// node hasParents
		if (parents.size() == 2) {
		    // color red
		    node.getNodeData().setColor(1.0f, 0.0f, 0.0f);
		    NodeLayoutData.color(node, NodeLayoutData.Color.RED);
		}
	    }
	}
	graph.readUnlock();

	// create clusters
	for (Node node : rootNodes) {
	    ChainCluster rootCluster = ChainCluster.createCluster(graphSource, node, 0);
	    rootsClusters.add(rootCluster);
	}
    }


    private ManyValuesMap<ChainCluster,ChainCluster> clusterGraph(Collection<ChainCluster> clusters){
	ManyValuesMap<ChainCluster,ChainCluster> graph = new ManyValuesMap<ChainCluster, ChainCluster>();
	for (ChainCluster chainCluster : clusters) {
	    Set<ChainCluster> allClusters = getClusterNeighborhood(chainCluster);
	    if (!allClusters.isEmpty()) {
		graph.addAll(chainCluster, clusters);
	    }
	}
	return graph;

    }

    private Partition<ChainCluster> partition(Collection<ChainCluster> elements, int level){
	if (elements == null || level < 0 ) {
	    return null;
	}
	Partition<ChainCluster> partitionForest = new Partition<ChainCluster>(elements);
	// first partition in sources internal and targets

	Set<ChainCluster> free = new HashSet<ChainCluster>();
	Set<ChainCluster> sources = new HashSet<ChainCluster>();
	Set<ChainCluster> targets = new HashSet<ChainCluster>();
	Set<ChainCluster> internal = new HashSet<ChainCluster>();

	Set<ChainCluster> temp = new HashSet<ChainCluster>(elements);
	while (!temp.isEmpty()) {
	    ChainCluster element = temp.iterator().next();
	    temp.remove(element);
	    // get targets
	    Set<ChainCluster> clusterTargets = getClusterTargets(element, level, level);

	    // get sources
	    Set<ChainCluster> clusterSources = getClusterSources(element, level, level);
	    boolean isTarget = clusterSources.isEmpty();
	    boolean isSource = clusterTargets.isEmpty();
	    if ( isTarget && isSource) { // case 1,1 (1&&1 = 1)
		free.add(element);
	    } else if(!isSource ^ !isTarget){ // case 0,1/ 1,0 (!0 ^ !0 = 0)
		if (isSource) {
		    sources.add(element);
		} else {
		    targets.add(element);
		}
	    } else { // case 0,0
		internal.add(element);
	    }
	}
	if (sources.size()>targets.size()) {
	    Set<ChainCluster> tmp = sources;
	    sources = targets;
	    targets = tmp;
	}
	partitionSources(partitionForest, internal,level);
	partitionSources(partitionForest, sources,level);
	return partitionForest;
    }

    private void partitionSources(Partition<ChainCluster> partitionForest,
		Collection<ChainCluster> sources, int level) {
	if (sources == null)  {
	    return;
	}

	if (sources.isEmpty() ) {
	    return;
	}

	for (ChainCluster source : sources) {

	    PartitionItem<ChainCluster> sourcePartition = partitionForest.getPartition(source);
	    // get targets
	    Set<ChainCluster> clusterTargets = getClusterTargets(source, level, level);

	    Set<PartitionItem<ChainCluster>> properClusters = new HashSet<PartitionItem<ChainCluster>>();
	    Set<PartitionItem<ChainCluster>> specialClusters = new HashSet<PartitionItem<ChainCluster>>();

	    for (ChainCluster target : clusterTargets) {
		PartitionItem<ChainCluster> targetPartition = partitionForest.getPartition(target);
		if (targetPartition.isSingleton()) {
		    properClusters.add(targetPartition);
		} else {
		    specialClusters.add(targetPartition);
		}
	    }
	    if (!sourcePartition.isSingleton()) {
		specialClusters.add(sourcePartition);
		if (!properClusters.isEmpty()) {
		    PartitionItem<ChainCluster> newSource = properClusters.iterator().next();
		    sourcePartition = newSource;
		    properClusters.remove(newSource);
		} 
	    }
	    
	    for (PartitionItem<ChainCluster> properPartition : properClusters) {
		partitionForest.merge(sourcePartition, properPartition);
	    }
	    // weights special clusters
	    Map<PartitionItem<ChainCluster>,Integer> map = new HashMap<PartitionItem<ChainCluster>, Integer>();
	    for (PartitionItem<ChainCluster> partition : specialClusters) {
		PartitionItem<ChainCluster> root = partition.find();
		Integer previousValue = map.put(root, 1);
		if (previousValue != null) {
		    map.put(root, previousValue+1);
		}
	    }

	    // Sort by weight
	    List<Entry<PartitionItem<ChainCluster>, Integer>> list = new ArrayList<Entry<PartitionItem<ChainCluster>, Integer>>(map.entrySet());
	    Collections.sort(list, new Comparator<Entry<PartitionItem<ChainCluster>, Integer>>() {

		@Override
		public int compare(Entry<PartitionItem<ChainCluster>, Integer> o1, Entry<PartitionItem<ChainCluster>, Integer> o2) {
		    return o1.getValue().compareTo(o2.getValue());
		}
	    });
	    for (Entry<PartitionItem<ChainCluster>, Integer> entry : list) {
		sourcePartition = partitionForest.merge(entry.getKey(), sourcePartition);
	    }
	}
    }
   
    @Override
    public void goAlgo() {

	System.out.println("roots: " + rootsClusters.size());
	
	Partition<ChainCluster> partition = partition(rootsClusters,0);
	rootsClusters.clear();
	Set<PartitionItem<ChainCluster>> roots = partition.getRoots();
	Set<ChainCluster> singletons = new HashSet<ChainCluster>();
	for (PartitionItem<ChainCluster> rootPartition : roots) {
	    List<ChainCluster> toValues = PartitionItem.toValues(rootPartition);
	    if (rootPartition.isSingleton()) {
		// rootsClusters.addAll(rootsClusters.size(),toValues);
		singletons.addAll(toValues);
	    } else {
		rootsClusters.addAll(0,toValues);
	    }
	}
	rootsClusters.addAll(singletons);
	List<ChainCluster> augmented = augmented(rootsClusters);

	int positionX = 0;
	int positionY = 0;
	for (int i = 0; i < augmented.size(); i++) {
	    ChainCluster chainCluster = augmented.get(i);
	    // draw chainCluster
	    positionX = drawCluster(chainCluster, positionX, positionY, randomPastelColor());
	}
	MixedGraph graph = graphModel.getMixedGraph();
	graph.readLock();

	Node[] nodes = graph.getNodes().toArray();
	for (Node node : nodes) {
	    if (NodeLayoutData.isColored(node, NodeLayoutData.Color.BLACK)) {
		float cX = 0.0f;
		float cY = 0.0f;
		int counter = 0;

		EdgeIterable edges = graph.getEdges(node);
		for (Edge edge : edges) {
		    if (!edge.isDirected()) {
			Node opposite = graph.getOpposite(node, edge);
			cX += opposite.getNodeData().x();
			cY += opposite.getNodeData().y();
			counter++;
		    }
		}
		if (counter > 1) {
		    node.getNodeData().setX(cX / (counter * 1.0f));
		    node.getNodeData().setY(cY / (counter * 1.0f));
		}
		else if (counter == 1) {
		    node.getNodeData().setX(cX + (marriedXDistance * 1.0f));
		    node.getNodeData().setY(cY + (marriedYDistance * 1.0f));
		}

	    }
	}
	graph.readUnlock();
    }

    private int drawCluster(ChainCluster cluster, int x, int y, float[] rgbColor) {
	ChainContainer rootContainer = cluster.getCluster();
	return drawContainer(rootContainer, x, y, rgbColor) + clusterDistance;
    }

    private int drawChain(ColoredChain chain, int x, int y, float[] rgbColor) {
	Node[] nodes = chain.getNodes(graphSource);
	int distance = 0;
	for (Node node : nodes) {
	    node.getNodeData().setX(x * 1.0f);
	    node.getNodeData().setY((y + distance) * 1.0f);
	    node.getNodeData().setR(rgbColor[0]);
	    node.getNodeData().setG(rgbColor[1]);
	    node.getNodeData().setB(rgbColor[2]);
	    distance += nodeYDistance;
	}
	return y + distance;
    }

    private int drawContainer(ChainContainer container, int x, int y, float[] rgbColor) {

	int result = 0;
	ColoredChain chain = container.getChain();
	int lastY = drawChain(chain, x, y, rgbColor);

	int tmpX = x;
	// int tmpY = y + nodeYDistance;
	int tmpY = lastY;

	Set<ChainContainer> children = container.getChildren();
	ChainContainer[] toArray = children.toArray(new ChainContainer[children.size()]);
	Arrays.sort(toArray, new Comparator<ChainContainer>() {

	    @Override
	    public int compare(ChainContainer o1, ChainContainer o2) {
		return o1.getWidth() - o2.getWidth();
	    }
	});

	for (ChainContainer chainContainer : toArray) {
	    int lastX = drawContainer(chainContainer, tmpX, tmpY, rgbColor);
	    tmpX = lastX;// convertWidth(chainContainer.getWidth())+containerDistance +nodeXDistance;
	}
	if (centered) {
	    drawChain(chain, (x + tmpX - containerDistance - nodeXDistance) / 2, y, rgbColor);
	}
	result = tmpX + containerDistance + nodeXDistance;
	return result;
    }

    @Override
    public void endAlgo() {
	MixedGraph graph = graphModel.getMixedGraph();
	graph.readLock();

	Node[] nodes = graph.getNodes().toArray();
	for (Node node : nodes) {
	    NodeLayoutData.clean(node);
	}
	graph.readUnlock();
	executing = false;
    }

    @Override
    public void setGraphModel(GraphModel graphModel) {
	super.setGraphModel(graphModel);
	graphSource = new GraphSource(graphModel.getMixedGraph());
    }

    private float[] randomPastelColor() {
	//to get rainbow, pastel colors
	Random random = new Random();
	final float hue = random.nextFloat();
	final float saturation = 0.9f;//1.0 for brilliant, 0.0 for dull
	final float luminance = 1.0f; //1.0 for brighter, 0.0 for black
	Color hsbColor = Color.getHSBColor(hue, saturation, luminance);

	float[] rgb = new float[3];
	rgb[0] = (1.0f * (float) hsbColor.getRed()) / 255.0f;

	rgb[1] = (1.0f * (float) hsbColor.getGreen()) / 255.0f;
	rgb[2] = (1.0f * (float) hsbColor.getBlue()) / 255.0f;
	return rgb;
    }

    @Override
    public LayoutProperty[] getProperties() {
	return null;
    }

    @Override
    public void resetPropertiesValues() {
    }

    private Set<ChainCluster> getClusterSources(ChainCluster cluster, int clusterLevel, int sourceLevel) {
	Set<BorderContainer> borders = cluster.getBordersAtLevel(clusterLevel);
	Set<ChainCluster> neighbours = new HashSet<ChainCluster>();

	for (BorderContainer border : borders) {
	    Triad[] triads = border.getTriads();
	    for (Triad triad : triads) {
		// if triad is on cluster contains triad's children ->
		// -> there is another cluster linking to triad's children (source)
		if ( /*!triad.isInternal() &&*/ triad.isOn(border)) {
		    BorderContainer sourceBorder = triad.getOtherBorder(border);
		    if (sourceBorder.getOrigin() == sourceLevel) {
			ChainCluster otherCluster = sourceBorder.getRootCluster();
			neighbours.add(otherCluster);
		    }
		}
	    }
	}
	return neighbours;
    }
    
    private Set<ChainCluster> getClusterTargets(ChainCluster cluster, int clusterLevel, int targetLevel) {
	Set<BorderContainer> borders = cluster.getBordersAtLevel(clusterLevel);
	Set<ChainCluster> neighbours = new HashSet<ChainCluster>();
	if (borders == null) {
	    return neighbours;
	}
	for (BorderContainer border : borders) {
	    Triad[] triads = border.getTriads();
	    for (Triad triad : triads) {
		// if triad is off cluster doesn't contain triad's children ->
		// -> there is another cluster to link to its triad's children (target)
		if (/*!triad.isInternal() &&*/!triad.isOn(border)) {
		    BorderContainer targetBorder = triad.getOtherBorder(border);
		    if (targetBorder.getOrigin() == targetLevel) {
			ChainCluster otherCluster = targetBorder.getRootCluster();
			neighbours.add(otherCluster);
		    }
		}
	    }
	}
	return neighbours;
    }

    private Set<ChainCluster> getClusterNeighborhood(ChainCluster cluster) {
	Set<Integer> allLevels = cluster.getAllLevels();
	Set<ChainCluster> neighbours = new HashSet<ChainCluster>();
	for (Integer level : allLevels) {
	    Set<BorderContainer> borders = cluster.getBordersAtLevel(level);
	    for (BorderContainer border : borders) {
		Triad[] triads = border.getTriads();
		for (Triad triad : triads) {
		    if (!(triad.isInternal() /*||triad.isOn(border)*/)) {
			BorderContainer targetBorder = triad.getOtherBorder(border);
			ChainCluster otherCluster = targetBorder.getRootCluster();
			neighbours.add(otherCluster);
		    }
		}
	    }
	}
	return neighbours;
    }

    private List<ChainCluster> augmented(List<ChainCluster> clusters){

	ManyValuesMap<ChainCluster, ChainCluster> graph = clusterGraph(clusters);
	Map<ChainCluster,Integer> clusterPosition = new HashMap<ChainCluster, Integer>();
	for (int i = 0; i < clusters.size(); i++) {
	    clusterPosition.put(clusters.get(i), i);
	}

	List<ChainCluster> layout = new LinkedList<ChainCluster>();
	layout.add(clusters.get(0));
	int left = -1;
	int right = 1;
	for (int i = 1; i < clusters.size(); i++) {
	    ChainCluster chainCluster = clusters.get(i);
	    int leftIncrement = increment(chainCluster, left, graph, layout, clusterPosition);
	    int rightIncrement = increment(chainCluster, right, graph, layout, clusterPosition);
	    if (leftIncrement < rightIncrement) {
		clusterPosition.put(chainCluster, left);
		layout.add(0, chainCluster);
		left--;
	    } else if (leftIncrement > rightIncrement){
		clusterPosition.put(chainCluster, right);
		layout.add(layout.size(), chainCluster);
		right++;
	    } else {
		Random random = new Random();
		if (random.nextBoolean()) {
		    clusterPosition.put(chainCluster, left);
		    layout.add(0, chainCluster);
		    left--;
		} else {
		    clusterPosition.put(chainCluster, right);
		    layout.add(layout.size(), chainCluster);
		    right++;
		}
	    }
	}
	return layout;
    }
    
    private int increment(ChainCluster cluster, int newPosition, ManyValuesMap<ChainCluster, ChainCluster> graph, List<ChainCluster> layout,Map<ChainCluster,Integer> positions){
	int c = 0;
	for (int i = 0; i < layout.size(); i++) {
	    ChainCluster chainCluster = layout.get(i);
	    if(graph.hasValue(chainCluster,cluster)){
		c+=Math.abs(positions.get(chainCluster)-newPosition);
	    }
	}
	return c;
    }



}
