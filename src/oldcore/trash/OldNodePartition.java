package oldcore.trash;

import static oldcore.trash.Mat.percent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;



/**
 * This class defines partitions of standard (vertex) networks. 
 * @author Klaus Hamberger
 * @since Version 0.4
 */
@SuppressWarnings("serial")
public class OldNodePartition extends Partition {


	/**
	 * true if cluster values are normalized
	 */
//	private boolean normalize;


	   

    
//    private NetSystem netSystem = null;



	
	
	/**
	 * returns the ith headline for this class
	 * @param i the headline index
	 * @return the headline
	 * @see elements.nodes.Vertex#affiliationStatistics(String, String)
	 */
/*	private String h(int i){
//    	String[] t = (String[])Puck.getParam("Headlines",5);
		return Puck.getHeadline(5,i);
	}*/

	/**
	 * gets the kinship affiliation statistics (the aggregate affiliation profile of the vertices for each cluster of the partition
	 * @param lb1 the label of the relative's attribute
	 * @param lb2 the label of the vertice's attribute which is to coincide with the relative's attribute
	 * @return the affiliation statistics as a string list
	 * @see gui.screens.StatsScreen#show(String[])
	 * @see elements.nodes.NewIndividual#affiliationStatistics(String, String)
	 */
/*	public ArrayList<String> affiliationStatistics (String lb1, String lb2) {
    	ArrayList<String> list = new ArrayList<String>();
    	list.add(h(10));
		int k = 16;
    	int tot = 0;
    	int totm = 0;
		int c[][]=new int[k][2];
    	for (Cluster clu : this){
    		if (clu.isNullCluster()) continue;
    		int b[][]=new int[k][2];
    		int n = clu.size();
    		int m = clu.size(Gender.MALE);
    		tot = tot+n;
    		totm = totm+m;
    		for (Individual v : clu){
    			Gender g = v.getGender();
    			int[] a = v.affiliationStatistics(lb1,lb2);
    			for (int i=0;i<k;i++){
    				if (a[i]>0) {
    					b[i][g]++;
    					c[i][g]++;
    				}
    			}
    		}
    		list.add(clu.getVal()+"\t"+n+"\t"+m+"\t"+(n-m)+"\t"+tabString(b));
    	}
    	list.add(1,h(8)+"\t"+tot+"\t"+totm+"\t"+(tot-totm)+"\t"+tabString(c));
    	list.add(2,h(5)+"\t"+percent(tot,size()*100)+"\t"+percent(totm,size()*100)+"\t"+percent(tot-totm,size()*100)+"\t"+tabString(c,tot,totm));
    	return list;
    }*/
	

	//Already included in CircuitFinder - Test!
	//Parametrize headlines; harmonize with other censuses
	/**
	 * effectuates a mixed matrimonial census of a partitioned Subnet 
	 * @param subnet the Subnet 
	 * @return the ChainGroupMap containing the census
	 * @see partitions.OldNodePartition#getCensuses(NodeMap)
	 */
/*	private ChainGroupMap<Object, Chain<?>>getCensus (Net subnet){
    	ChainGroupMap<Object, Chain<?>> census = new VertexChainGroupMap<Object, Chain<?>>(originNet, 0); //new ListMap(false);  
    	final String[][] clu = {{"H~W","HF~W","HM~W"},{"H~WF","HF~WF","HM~WF"},{"H~WM","HF~WM","HM~WM"}};
    	for (Path c : ((Net)originNet).getCouples()) {
    		for (int i=-1;i<2;i++){
    			for (int j=-1;j<2;j++){
    				Path p = match(c,i,j,subnet);
    				if (p!=null) census.add(clu[j+1][i+1], p);
    			}
    		}
    	}
    	return census;
    }*/


	
/*	private Object get (Vertex v, String label) { //remove "label"

		if (label.equals("PEDG")) return v.getNrLinKin(deg);
		if (label.equals("PROG")) return v.getNrLinKin(-deg);
		if (label.equals("MDEPTH")) return v.meanDepth();
		if (label.equals("SEX")) return v.getGender(4);
		if (label.equals("SPOU")) return v.nrSpouses();
		if (label.equals("FIRSTN")) return v.getFirstName();
		if (label.equals("LASTN")) return v.getLastName();
	    //	    if (label.equals("GEN")) return v.level;
	    if (label.equals("SUB")) return v.getColor();*
		if (label.equals("SIMPLE")) return v.getLabel();
		if (callsNet()) return v.getCluster();
	    return v.getCluster(label,bin,deg);
	}*/
	


	//simplify
	//add possibility of cumulation in the case of transversal partitions
	/**
	 * transforms the partition into a dataset for diagram visualization
	 * @param split the command for a second (transversal) partitioning
	 * @return the dataset
	 * @see gui.screens.ChartScreen#Chart(Partition, String, boolean)
	 */
/*	public CategoryDataset getDataset(String split) {
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		if (Trafo.isNull(split)){
			int cum = 0;
			int tot = 0;
            if (cumul==-1) tot=countAll();
			for (Cluster<Individual> clu : this){
				cum = clu.toDataset(dataset, cumul, cum, tot, normalize, originNet, null);
/*				if (clu.isNullCluster()) continue;
	            int count = clu.count()+cum;
	            int cnt = count;
	            if (cumul==-1 && !clu.isNullCluster()) cnt=tot-cum;
	            if (normalize) cnt = 100*cnt/net.size();
	            dataset.addValue(cnt,"",asString(clu.getHeading()));
	            if (cumul!=0) cum=count;
	            System.out.println(cnt+" "+cum);*/
/*
			}
			return dataset;
		}
		for (Partition part : partitionSystem(split)){
			if (part.getName().equals("0")) continue;
			Collections.sort(part);
			int cum = 0;
			int tot = 0;
            if (cumul==-1) tot=countAll();
			for (Cluster<Individual> clu : part){
				cum = clu.toDataset(dataset, cumul, cum, tot, normalize, originNet, part);
/*				if (clu.isNullCluster()) continue;
//	            if (!clu.hasHeading()) continue;
//	            if (clu.hasHeading(0)) continue;
				int cnt = clu.count();
	            if (normalize) cnt = 100*cnt/net.size();
	            dataset.addValue(cnt,asString(clu.getHeading()),part.title);*/
/*			}
		}
	    return dataset;
	}*/

	



    
    /**
     * get the export mode for the partition in Pajek format
     * <p> 0 Network, 1 Partition, 2 Vector
     * @return the partition mode
     * @see io.write.AbstractWriter#writePartition(Partition)
     */
/*    public int getExportMode(){
    	int t = getNet().getType();
    	if (t==2) return 0;
    	if (t==1) return 1;
    	if (numeric()) return 2;
    	return 1;
    }*/
    
    
/*    private void augment(Map<int[],int[][]> map,int i, int j, int a, int b){
    	int[] idx = new int[]{i,j};
    	int[][] mar = map.get(idx);
    	if (mar==null) {
    		mar = new int[2][size()-1];
        	map.put(idx,mar);
    	}
    	mar[0][a-1]++;
    	mar[1][b-1]++;
    }*/


    
    /**
     * gets the cluster heading of the vertices' parent
     * @param v the vertex
     * @param g the gender of the parent (or -1 if the vertice's own cluster shall be returned)
     * @return the cluster heading of the vertice's parent
     * @see partitions.OldNodePartition#match(Individual, Individual, int, int)
     */
/*    private Object getParentCluster (Individual v, int g){
    	if (g==-1) return get(v);
    	Individual p = v.getParent(g);
    	if (p==null) return null;
    	return get(p);
    }*/
    
    /**
     * gets the subnetwork to which a given vertex belongs
     * @param v the vertex
     * @return the subnetwork to which the vertex belongs
     * @see gui.screens.DataScreen#downstep(Partition)
     */
/*    public PartNet getSubNet (Individual v){
    	if(netSystem == null) netSystem = new NetSystem(this, true);  
		return netSystem.getSubNet(getVal(v));
	}*/
    
/*    public String getSubtitle() {
		return subtitle;
    }*/
    

	


	
	/**
	 * checks whether the parents of two vertices belong to the same cluster
	 * @param c a path containing ego and alter as first and second vertex
	 * @param i the gender of the parent of ego
	 * @param j the gender of the parent of alter
	 * @param subnet the subnetwork to which ego or alter belong
	 * @return the path containing ego and alter if their parents belong to the same cluster, null otherwise
	 * @see partitions.OldNodePartition#getCensus(NodeMap)
	 */
/*	private Path match (Path c, int i, int j, Net subnet){
    	Individual v = c.get(0);
    	Individual w = c.get(1);
    	if (subnet!=null && subnet.get(v)==null && subnet.get(w)==null) return null;
    	return match(v,w,i,j);
    }*/
	
	/**
	 * checks whether the parents of two vertices belong to the same cluster
	 * @param v the ego vertex
	 * @param w the alter vertex
	 * @param i the gender of the parent of ego
	 * @param j the gender of the parent of alter
	 * @return the path containing ego and alter if their parents belong to the same cluster, null otherwise
	 * @see partitions.OldNodePartition#match(Path, int, int, NodeMap)
	 */
/*	private Path match (Individual v, Individual w, int i, int j){
    	Object p = getParentCluster(v,i);
    	if (p==null) return null;
    	if (!p.equals(getParentCluster(w,j))) return null;
    	Path c = new Path(v,w);
    	c.setValueTo(p);
    	return c;
    }*/
	
	

	

    
    /**
     * puts a vertex into the appropriate cluster
     * @param v the vertex
     * @see partitions.NodePartition#fill(Collection)
     */
/*    public void put (IVertex v){
		v.setCurrent(getValues().size()+1); //still necessary? 
		super.put(v);
	}*/


    // inverse method extendTitle would be better...
    /**
     * reduces the partition title
     * @see io.write.AbstractWriter#writeNet1(NodeMap, RingGroupMap,int,String,int,boolean)
     */
/*    public void reduceName () {       
        title = title.substring(title.indexOf("(")+1,title.lastIndexOf(")"));
     }*/
    

    
    /**
     * relationalizes a partition by defining an equivalence relation between any two vertices of the same cluster
     * @see maps.Net#relationalize()
     * @since 11-10-04
     */
/*    public void affinalize () {
    	for (Cluster clu : this){
    		if (clu.isNullCluster()) continue;
    		for (IVertex v : clu){
    			for (IVertex w : clu){
    				if (w.isSingle()) continue;
    				for (IVertex a : w.getSpouses())
    				v.setAttribute(a.getID()+"",getLabel()+"-affine", 3);
    			}
			}
		}
	}*/
    

	

    
    /**
     * produces the chart of the partition and adds it to a list
     * @param charts the list of charts to which the partition chart shall be added
     * @param split the transversal partition command
     * @see gui.screens.StatsScreen#show(String[])
     */
    
/*    public void show (List<Screen> charts, String split) {
        charts.add(new ChartScreen(this,split,true));
     }*/
     

    /**
     * produces a string of numbers and percentages from an integer array
     * @param a the integer array
     * @return the string of numbers and percentages
     * @see partitions.OldNodePartition#affiliationStatistics(String, String)
     */
/*    private static String tabString(int[][] a){
    	return tabString(a,0,0);
    }*/

    /**
     * produces a string of numbers and percentages from an integer array and two denominators
     * @param a the integer array
     * @param n the first denominator
     * @param m the second denominator
     * @return the string of numbers and percentages
     * @see partitions.OldNodePartition#tabString(int[][])
     * @see partitions.OldNodePartition#affiliationStatistics(String, String)
     */
/*	private static String tabString(int[][] a, int n, int m){
    	String s = new String();
    	for (int[] b : a){
   			if (n==0) s = s+(b[0]+b[1])+"\t"+b[0]+"\t"+b[1]+"\t";
   			else s = s+percent(b[0]+b[1],n)+"\t"+percent(b[0],m)+"\t"+percent(b[1],(n-m))+"\t";
    	}
    	return s;
    }*/

}
	

