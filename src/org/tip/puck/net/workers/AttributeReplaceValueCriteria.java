package org.tip.puck.net.workers;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.workers.AttributeWorker.CaseOption;

/**
 * 
 * @author TIP
 */
public class AttributeReplaceValueCriteria {

	private AttributeWorker.Scope scope;
	private String optionalRelationName;
	private String label;
	private String targetValue;
	private AttributeWorker.CaseOption caseOption;
	private String newValue;

	/**
	 * 
	 */
	public AttributeReplaceValueCriteria() {
		this.scope = AttributeWorker.Scope.NONE;
		this.optionalRelationName = null;
		this.label = null;
		this.targetValue = null;
		this.caseOption = AttributeWorker.CaseOption.CASE_SENSITIVE;
		this.newValue = null;
	}

	public AttributeWorker.CaseOption getCaseOption() {
		return this.caseOption;
	}

	public String getLabel() {
		return this.label;
	}

	public String getNewValue() {
		return this.newValue;
	}

	public String getOptionalRelationName() {
		return this.optionalRelationName;
	}

	public AttributeWorker.Scope getScope() {
		return this.scope;
	}

	public String getTargetValue() {
		return this.targetValue;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isCaseSensitive() {
		boolean result;

		if (this.caseOption == CaseOption.CASE_SENSITIVE) {
			//
			result = true;

		} else {
			//
			result = false;
		}

		//
		return result;
	}

	public void setCaseOption(final AttributeWorker.CaseOption caseOption) {
		this.caseOption = caseOption;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public void setNewValue(final String newValue) {
		this.newValue = newValue;
	}

	public void setOptionalRelationName(final String optionalRelationName) {
		this.optionalRelationName = optionalRelationName;
	}

	public void setScope(final AttributeWorker.Scope scope) {
		this.scope = scope;
	}

	public void setTargetValue(final String value) {
		this.targetValue = value;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public static boolean isNotValid(final AttributeReplaceValueCriteria criteria) {
		boolean result;

		result = !isValid(criteria);

		//
		return result;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public static boolean isValid(final AttributeReplaceValueCriteria criteria) {
		boolean result;

		if ((criteria == null) || (criteria.getScope() == null) || (criteria.getScope() == AttributeWorker.Scope.NONE)
				|| (StringUtils.isBlank(criteria.getLabel()))) {
			//
			result = false;

		} else {
			//
			result = true;
		}

		//
		return result;
	}
}
