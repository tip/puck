package org.tip.puck.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;

public class RandomUtils {
	
	/**
	 * returns a binary random value (0 vs 1) with a probability of p
	 * @param proba the probability of event 0
	 * @return the random value 0 or 1
	 */
	public static int alea (double proba) {
		if (event(proba)) return 0;
		return 1;
	}
	
	/**
	 * returns a boolean random value (occurence or non-occurence of an event) with a probability of p 
	 * @param proba the probability of the event 
	 * @return true if the random event occurs
	 */
	public static boolean event (double proba) {
		boolean result;
		
		if (proba == 0.){
			result = false;
		} else if (proba == 1.){
			result = true;
		} else {
			result = (Math.random() <= proba);
		}
		
		//
		return result;
	}
	
	public static boolean event (double proba, Random randGen) {
		boolean result;
		
		if (proba == 0.){
			result = false;
		} else if (proba == 1.){
			result = true;
		} else {
			result = (randGen.nextDouble() <= proba);
		}
		
		//
		return result;
	}
	

	
	private static List<Integer> distIndex(int max){
		List<Integer> result = new ArrayList<Integer>();
		for (int i=0;i<max;i++){
			for (int j=0;j<i;j++){
				result.add(i);
			}
		}
		//
		return result;
	}
	
	public static<E> E draw (Map<E,Double> weights, Random randGen){
		E result;
		
		result = null;
		
        double totalWeight = 0;
        for (E item : weights.keySet()) {
            totalWeight += weights.get(item);
        }
        
        // random position in [0, totalWeight]
        double pos = randGen.nextDouble() * totalWeight;
        
        totalWeight = 0;
        for (E item : weights.keySet()) {
            totalWeight += weights.get(item);
            if (totalWeight > pos) {
                result = item;
                break;
            }
        }
		//
		return result;
	}
	
	public static int randomRound (double d, Random randGen){
		int result;
		
        double floor = Math.floor(d);
        double fract = d - floor;
        
        result = (int)floor;
        
        if ((fract > 0 && randGen.nextDouble() <= fract) || fract > 1){
            result += 1;
        }
        
        //
        return result;
        

	}
	
	
	public static<E> E draw (Collection<E> allItems, Set<E> weightedItems, Random randGen, double weight){
		E result;
		
		result = null;
		
        double totalWeight = 0;
        for (E item : allItems) {
        	if (weightedItems.contains(item)){
                totalWeight += weight;
        	} else {
        		totalWeight += 1.;
        	}
        }
        
        // random position in [0, totalWeight]
        double pos = randGen.nextDouble() * totalWeight;
        
        totalWeight = 0;
        for (E item : allItems) {
        	if (weightedItems.contains(item)){
                totalWeight += weight;
        	} else {
        		totalWeight += 1.;
        	}
            if (totalWeight > pos) {
                result = item;
                break;
            }
        }
		//
		return result;
	}
	
	
	public static <E> TreeMap<Double,E> getChoiceMap (Map<E,Double> weights){
		TreeMap<Double,E> result;
		
		result = new TreeMap<Double, E>();
		
		double maxValue = 0.;
		for (E item : weights.keySet()){
			if (weights.get(item)>0){
				maxValue += weights.get(item);
				result.put(maxValue,item);
			}
		}
		//
		return result;
	}

	
/*	public static <E> E draw (TreeMap<Double,E> choiceMap){
		E result;
		
		result = null;
		double max = choiceMap.lastKey();
		double value = draw(max);
		for (Double limit : choiceMap.keySet()){
			if (value <= limit) {
				result = choiceMap.get(limit);
				break;
			}
		}
				
		//
		return result;
	}*/
	

	private static double draw (double max){
		return (double)(Math.random()*max);
	}
	
	/**
	 * returns a random integer below an upper limit
	 * @param max the upper limit
	 * @return
	 */
	private static int draw (int max){
		return (int)(Math.random()*max);
	}
	
	public static <E> E draw (List<E> source){
		E result;
		
		if (source==null || source.size()==0){
			result = null;
		} else {
			result = source.get(draw(source.size()));
		}
		
		//
		return result;
	}

}
