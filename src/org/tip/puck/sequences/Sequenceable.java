package org.tip.puck.sequences;

import java.util.List;
import java.util.Map;

import org.tip.puck.util.Numberable;

public interface Sequenceable<E> extends Numberable {

	List<Ordinal> getTimes();
	
	Ordinal getFirstTime();
	
	Map<Ordinal,E> getStations();
	
	E getStation(Ordinal time);
	
	E getPreviousStation(Ordinal time);
	
	String idLabel();
	
	String getLabel();
		
}
