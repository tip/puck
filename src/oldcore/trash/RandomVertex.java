package oldcore.trash;

import java.util.ArrayList;



import org.tip.puck.net.Attribute;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;

/**
 * This class defines a random vertex
 * @author Klaus Hamberger
 * @since Version 0.6
 * @see maps#RandomNet
 */
public class RandomVertex extends Individual {

	/**
	 * The default constructor
	 */
	public RandomVertex (){
		super();
	}

	/**
	 * The standard constructor
	 * @param i the current index of the vertex
	 * @param g the gender of the vertex
	 */
	public RandomVertex(int i, int g) {
		super(i,"",g);
	}
	
	/**
	 * checks whether a RandomVertex is admissible as a parent or spouse, according to his/her relative age
	 * @param v the potential spouse vertex
	 * @param i the index of the kinship tie (-1 child, 0 mother, 1 father, 2 spouse, 3 observer)
	 * @return true if the spouse vertex is admissible
	 */
	private boolean accepts (RandomVertex v, int i) {
		   
		  final int marAge = ((RandomNet)home).getMarriageAge(v.getGender());
		  final int tol = ((RandomNet)home).getTolerance(i);
		  
	      if ((v.deathYear()<getValue()) && (v.deathYear()>0)) return false;
	      if (util.Mat.outOfBounds(v.getValue(),idealBirthyear(i),tol)) return false;
	      if (i<0) return true;
	      if ((i==0) && (v.deathYear()<v.getValue()+marAge)&& (v.deathYear()>0)) return false;
	      return (v.getGender()==idealGender(i));
	   }
	
	/**
	 * returns the death seedReferenceYear of the (random) vertex
	 * @return
	 */
	private int deathYear () {
		return Integer.parseInt(attributes().get(1).getDate());
	}

	/**
	 * randomly draws a random vertex from a list
	 * @param list the list of admissible random vertices
	 * @return the selected random vertex
	 */
	private RandomVertex draw (ArrayList<Vertex> list) {
		if (list==null || list.size()==0) return null;
		int i = util.Mat.randomNumber(0,list.size());
		return (RandomVertex)list.get(i);
	}	

	/**
	 * randomly draws a random vertex from a list within a given horizon
	 * @param list the list of admissible random vertices
	 * @hor the upper bound of the random vertex index
	 * @return the selected random vertex
	 */
	private RandomVertex draw (ArrayList<Vertex> list, int hor) {
		int i = util.Mat.randomNumber(0,hor);
		if (i<list.size()) return (RandomVertex)list.get(i);
		return null;
	}

	/**
	 * gets the signature of a random vertex
	 * @return the signature of a random vertex
	 */
	public String dsignature () { 
		if (deathYear() == 0) return getName(1)+" (* "+getValue()+")";
		return getName(1)+" ("+getValue()+" - "+deathYear()+")";
	}
   
	/**
	 * gets a random vertex or creates it if it does not yet exist
	 * @param i the index of the kinship tie (-1 child, 0 mother, 1 father, 2 spouse, 3 observer)
	 * @param hor the horizon of the random vertex search (upper bound of the vertex index)
	 * @param mem the probability of recall
	 * @return the random vertex
	 */
	public RandomVertex find (int i, int hor, double mem) {
		if (!(util.Mat.event(mem))) return null;
		RandomVertex v = oldFriend(i, hor);
		if (v!=null) return v;
		return newFriend(i);
	}

	//Check and test
	/**
	 * gets the (matrimonial or kinship) horizon (the number of potential parents and marriage partners)
	 * @return the (matrimonial or kinship) horizon 
	 */
	private int getHor() {
		return ((RandomNet)home).getHor();
	}
   
	/**
	 * gets the required birth seedReferenceYear of a random vertex
	 * @param i the index of the kinship tie (-1 child, 0 father, 1 mother, 2 spouse, 3 observer)
	 * @return the required birth seedReferenceYear of the random vertex
	 */
	private int idealBirthyear (int i) {
		int k = -1;
		if (i==-1) {
			i=getGender();
			k = 1;
		}
		final int dist = ((RandomNet)home).getDist(i);
		if (i==2) return getValue()+(1-2*getGender())*dist;
		return getValue()+k*dist; 
	}

	/**
	 * returns the required gender of a random vertex
	 * @param i the index of the kinship tie (-1 child, 0 mother, 1 father, 2 spouse, 3 observer)
	 * @return the required gender of the random vertex (0 male, 1 female)
	 */
	private int idealGender (int i) {
		final double propMales = ((RandomNet)home).getMaleProp();
		if ((i<0) || (i==3)) return util.Mat.alea(propMales);
		if (i<2) return i;
		if (i==2) return alterGender();
		return -1;
	}
	
	/**
	 * creates a new random vertex
	 * @param i the index of the kinship tie (-1 child, 0 mother, 1 father, 2 spouse, 3 observer)
	 * @return the new random vertex
	 */
	private RandomVertex newFriend (int i) {
		final int tol = ((RandomNet)home).getTolerance(i);
		RandomVertex v = new RandomVertex(home.size()+1,idealGender(i));
		v.setHome(home);
		v.setRelationAttributeLabel(v.randomName());
		v.setValue(util.Mat.randomNumber(idealBirthyear(i)-tol,idealBirthyear(i)+tol));
		v.setAttribute(new Attribute(v.getValue()+"","BIRT",2));
		v.setDeathYear(getValue());
		v.home=home;
		((Net)home).put(v);
		return v;
	}
	
	/**
	 * adds a line to the protocol of a random net
	 * @param str the protocol line to be noted
	 */
	private void note (String str) {
		((Net)home).getProtocol().add(str);
	}
	
	/**
	 * selects an existing random vertex from part of a list
	 * @param i the index of the kinship tie (-1 child, 0 mother, 1 father, 2 spouse, 3 observer)
	 * @param hor the horizon of the random vertex search (upper bound of the vertex index)
	 * @return the selected random vertex
	 */
	private RandomVertex oldFriend (int i, int hor) {
		return draw(select(i), hor);
	}
	
	/**
	 * gets the list of potential spouses of a random vertex
	 * @return the list of potential spouses
	 */
	private ArrayList<Vertex> potSpouses () {
		if (wantsSpouse()) return select(2);
		return new ArrayList<Vertex>();
	}
	
/*	private String randomName () {
		return ((RandomNet)home).randomName(this);
	}*/

	/**
	 * gets a selection of admissible random vertices according to their birth seedReferenceYear
	 */
	ArrayList<Vertex> select (int i) {
		ArrayList<Vertex> select = new ArrayList<Vertex>();
		for (Vertex v : ((RandomNet)home).values()) {
			if (accepts((RandomVertex)v,i)) select.add(v);
		}
		return select;
	}

	/**
	 * sets the death seedReferenceYear of a random vertex
	 * @param min the lower bound of the death seedReferenceYear
	 */
	private void setDeathYear (int min) {
		int dy = getValue();
		double p = 1.;
		double i = 0.0005;
		while ((util.Mat.event(p)) || (dy<min)) {
			dy=dy+1;
			p=p-i;
		}
		if (dy>2007) dy=0;
		setAttribute(new Attribute(dy+"","DEAT",2));
	}

	/**
	 * sets the relative of a random vertex and augments a kinship chain accordingly
	 * @param v the random vertex
	 * @param i the index of the kinship tie (-1 child, 0 mother, 1 father, 2 spouse, 3 observer)
	 * @param mem the probability of recall
	 * @param pref a string representation of the preceding kinship chain
	 */
	private void setRelative (RandomVertex v, int i, double mem, String pref) {
		if (v==null) return;
		String letter;
		int g = v.getGender();
		final double gm = ((RandomNet)home).getGenMem(g,i);
		if (i==2) letter = Puck.getGenderLabels()[2][g];
		else letter = Puck.getGenderLabels()[5][g];
		note(pref+letter+" "+v.dsignature());
		setLink(v,i,true);
		if (mem==0) return;
		v.tell(mem*gm,pref+letter);
	}

	/**
	 * creates random parent and spouse vertices and augments a kinship chain accordingly
	 * @param mem the probability of parent and spouse recall
	 * @param pref a string representation of the preceding kinship chain
	 */
	public void tell (double mem, String pref) {
		tellParents(mem,pref);
		tellSpouses(mem,0,pref);
	}

	/**
	 * creates random parent vertices and augments a kinship chain accordingly
	 * @param mem the probability of parent recall
	 * @param pref a string representation of the preceding kinship chain
	 */
	public void tellParents (double mem, String pref) {
		RandomVertex[] p = new RandomVertex[2];
		int c = 0;
		for (int i=0;i<2;i++) {
			RandomVertex v = (RandomVertex)(getParents().get(i));
			if (v==null) continue;
			c = c+1;
			p[i]=v;setRelative(v,i,0,pref);
		}        
		if (c==2) return;
		if (c==0) {
			int i = util.Mat.alea(0.5); 
			RandomVertex v = find(i,getHor(),mem); 
			p[i]=v;setRelative(v,i,mem,pref);
		}
		for (int i=0;i<2;i++) {
			if (p[i]==null) continue;
			RandomVertex v = draw(p[i].spouses());
			setRelative(v,(i+1)%2,mem,pref);  
		}
	}
	
	// ev. drop rep restriction (just to avoid infinite search for spouses)
	/**
	 * creates random spouse vertices and augments a kinship chain accordingly
	 * @param mem the probability of spouse recall
	 * @param rep the number of preceding spouses
	 * @param pref a string representation of the preceding kinship chain
	 */
	public void tellSpouses (double mem, int rep, String pref) {
		if ((!(wantsSpouse())) || (rep==5)) return; 
		RandomVertex sp = find (2,getHor(),mem);
		if (sp==null) {
			tellSpouses(mem,rep+1,pref); 
			return;
		}
		if (!(sp.potSpouses().contains(this))) {
			tellSpouses(mem,rep+1,pref);      
			return;
		}
		setRelative(sp,2,mem,pref);
		tellSpouses(mem,rep,pref);
	}

	/**
	 * checks whether a given random vertex is searching for a spouse
	 * @return true if the vertex searches a spouse
	 */
	private boolean wantsSpouse () {
		double m = ((RandomNet)home).getMarriageRate(getGender());
		final double rem = ((RandomNet)home).getRemarriageRate(getGender());
		for (int i=0;i<nrSpouses();i++) {
			m = m*rem;
		}
		return util.Mat.event(m);
	}

	/**
	 * gets a random letter 
	 * @param i the letter category
	 * @return a random letter
	 * @see elements.nodes.RandomVertex#randomName()
	 */
	private static String rl (int i) {
		final String[][] letters = (String[][])Puck.getParam("Letters");
		try{
			return letters[i][util.Mat.randomNumber(0,letters[i].length)];
		} catch (IndexOutOfBoundsException ioe){
			return rl(i);
		}
	}
	
	/**
	 * gets a random name
	 * @return a random name
	 * @see elements.nodes.RandomVertex#newFriend(int)
	 */
	private String randomName () {
		return rl(0)+rl(1)+rl(2)+rl(1)+rl(2)+rl(3+getGender());
	}
	
	
}
