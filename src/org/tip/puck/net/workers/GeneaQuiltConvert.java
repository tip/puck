package org.tip.puck.net.workers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Families;
import org.tip.puck.net.Family;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puckgui.views.geneaquilt.Event;
import org.tip.puckgui.views.geneaquilt.EventQuiltCriteria;

import fr.devinsy.util.StringList;
import fr.inria.aviz.geneaquilt.model.DateRange;
import fr.inria.aviz.geneaquilt.model.Edge;
import fr.inria.aviz.geneaquilt.model.Fam;
import fr.inria.aviz.geneaquilt.model.Indi;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.Vertex;

/**
 * 
 * Methods added in this classes has to be static and autonomous.
 * 
 * @author TIP
 */
public class GeneaQuiltConvert {

	private static final Logger logger = LoggerFactory.getLogger(GeneaQuiltConvert.class);

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Network convertToEventQuilt(final Individuals individuals, final Families families, final Relations relations) {
		Network result;

		result = new Network();
		try {
			//
			for (Individual individual : individuals) {
				//
				Indi targetIndividual = new Indi();

				//
				targetIndividual.setId("I" + String.valueOf(individual.getId()));
				result.addVertex(targetIndividual);

				//
				targetIndividual.setProperty("NAME", individual.getName());

				if (StringUtils.isNotBlank(individual.getFirstName())) {
					targetIndividual.setProperty("NAME.GIVN", individual.getFirstName());
				}

				if (StringUtils.isNotBlank(individual.getLastName())) {
					targetIndividual.setProperty("NAME.SURN", individual.getLastName());
				}

				//
				if (individual.getGender() != null) {
					targetIndividual.setSex(String.valueOf(individual.getGender().toGedChar()));
				}

				//
				if (individual.getOriginFamily() != null) {
					targetIndividual.addFams("F" + String.valueOf(individual.getOriginFamily().getId()));
				}

				//
				for (Family familySpouse : individual.getPersonalFamilies()) {
					targetIndividual.addFams("F" + String.valueOf(familySpouse.getId()));
				}

				//
				for (Relation relation : individual.relations()) {
					targetIndividual.addFams("R" + relation.getId() + "-" + relation.getTypedId());
				}

				//
				for (Attribute attribute : individual.attributes()) {
					if ((attribute.getLabel().contains("_DATE")) && (!attribute.getLabel().startsWith("CHAN_"))) {
						targetIndividual.setDate(attribute.getLabel(), attribute.getValue().trim());
					} else {
						targetIndividual.setProperty(attribute.getLabel(), attribute.getValue());
					}
				}
			}

			//
			for (Family family : families) {
				//
				Event event = new Event();

				//
				event.setId("F" + family.getId());
				result.addVertex(event);

				//
				event.setProperty("ID", "F" + family.getId());

				//
				for (Attribute attribute : family.attributes()) {
					if ((StringUtils.equals(attribute.getLabel(), "DATE"))
							|| ((attribute.getLabel().contains("_DATE")) && (!attribute.getLabel().startsWith("CHAN_")))) {
						event.setDate(attribute.getLabel(), attribute.getValue().trim());
					} else {
						event.setProperty(attribute.getLabel(), attribute.getValue());
					}
				}

				DateRange dateRange = event.getDateRange();
				if (dateRange.isValid()) {
					Calendar calendar = Calendar.getInstance();
					calendar.setTimeInMillis(dateRange.getStart() * 1000);
					int year = calendar.get(Calendar.YEAR);

					event.setLabel(String.valueOf(year));
				} else {
					event.setLabel(" F ");
				}

				event.setProperty("MODEL", "Family");
			}

			//
			for (Relation relation : relations) {
				//
				Event event = new Event();

				//
				event.setId("R" + relation.getId() + "-" + relation.getTypedId());
				result.addVertex(event);

				//
				event.setProperty("ID", "R" + relation.getId() + "-" + relation.getTypedId());
				event.setProperty("NAME", relation.getName());

				//
				for (Attribute attribute : relation.attributes()) {
					if ((StringUtils.equals(attribute.getLabel(), "DATE"))
							|| ((attribute.getLabel().contains("_DATE")) && (!attribute.getLabel().startsWith("CHAN_")))) {
						event.setDate(attribute.getLabel(), attribute.getValue().trim());
					} else {
						event.setProperty(attribute.getLabel(), attribute.getValue());
					}
				}

				//
				DateRange dateRange = event.getDateRange();
				if (dateRange.isValid()) {
					Calendar calendar = Calendar.getInstance();
					calendar.setTimeInMillis(dateRange.getStart() * 1000);
					int year = calendar.get(Calendar.YEAR);

					event.setLabel(String.valueOf(year));
				} else {
					event.setLabel(" R ");
				}

				event.setProperty("MODEL", relation.getModel().getName());
			}

			for (Vertex vertex : result.getVertices()) {
				//
				if (vertex instanceof Indi) {
					//
					Indi indi = (Indi) vertex;

					if (indi.getFams() != null) {
						//
						for (String fid : indi.getFams()) {
							//
							Vertex fam = result.getVertex(fid);
							if (fam != null) {
								//
								Edge edge = new Edge(fam.getId(), vertex.getId());
								if (!result.containsEdge(edge)) {
									//
									result.addEdge(edge, fam, indi);
									edge.setFromVertex(fam);
									edge.setToVertex(indi);
								}
							}
						}
					}
				}
			}
		} catch (Exception exception) {
			//
			exception.printStackTrace();

			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Network convertToEventQuilt(final Individuals individuals, final Families families, final Relations relations,
			final EventQuiltCriteria criteria) {
		Network result;

		result = new Network();
		try {
			//
			for (Individual individual : individuals) {
				//
				Indi targetIndividual = new Indi();

				//
				targetIndividual.setId("I" + String.valueOf(individual.getId()));
				result.addVertex(targetIndividual);

				//
				targetIndividual.setProperty("NAME", individual.getName());

				if (StringUtils.isNotBlank(individual.getFirstName())) {
					targetIndividual.setProperty("NAME.GIVN", individual.getFirstName());
				}

				if (StringUtils.isNotBlank(individual.getLastName())) {
					targetIndividual.setProperty("NAME.SURN", individual.getLastName());
				}

				//
				if (individual.getGender() != null) {
					targetIndividual.setSex(String.valueOf(individual.getGender().toGedChar()));
				}

				//
				if (individual.getOriginFamily() != null) {
					targetIndividual.addFams("F" + String.valueOf(individual.getOriginFamily().getId()));
				}

				//
				for (Family familySpouse : individual.getPersonalFamilies()) {
					targetIndividual.addFams("F" + String.valueOf(familySpouse.getId()));
				}

				//
				for (Relation relation : individual.relations()) {
					targetIndividual.addFams("R" + relation.getId() + "-" + relation.getTypedId());
				}

				//
				for (Attribute attribute : individual.attributes()) {
					if ((attribute.getLabel().contains("_DATE")) && (!attribute.getLabel().startsWith("CHAN_"))) {
						targetIndividual.setDate(attribute.getLabel(), attribute.getValue().trim());
					} else {
						targetIndividual.setProperty(attribute.getLabel(), attribute.getValue());
					}
				}
			}

			//
			for (Family family : families) {
				//
				Event event = new Event();

				//
				event.setId("F" + family.getId());

				//
				event.setProperty("ID", "F" + family.getId());

				//
				if (family.getHusband() != null) {
					//
					event.setHusb("I" + String.valueOf(family.getHusband().getId()));
				}

				//
				if (family.getWife() != null) {
					//
					event.setWife("I" + String.valueOf(family.getWife().getId()));
				}

				//
				for (Attribute attribute : family.attributes()) {
					if ((StringUtils.equals(attribute.getLabel(), "DATE"))
							|| ((attribute.getLabel().contains("_DATE")) && (!attribute.getLabel().startsWith("CHAN_")))) {
						event.setDate(attribute.getLabel(), attribute.getValue().trim());
					} else {
						event.setProperty(attribute.getLabel(), attribute.getValue());
					}
				}

				DateRange dateRange = event.getDateRange();
				if (dateRange.isValid()) {
					Calendar calendar = Calendar.getInstance();
					calendar.setTimeInMillis(dateRange.getStart() * 1000);
					int year = calendar.get(Calendar.YEAR);

					event.setLabel(String.valueOf(year));
				} else {
					event.setLabel(" F ");
				}

				event.setProperty("MODEL", "Family");

				if ((event.getDateRange().isValid()) || (criteria.isShowUndatedEvent())) {
					result.addVertex(event);
				}
			}

			//
			for (Relation relation : relations) {
				//
				Event event = new Event();

				//
				event.setId("R" + relation.getId() + "-" + relation.getTypedId());

				//
				event.setProperty("ID", "R" + relation.getId() + "-" + relation.getTypedId());
				event.setProperty("NAME", relation.getName());

				//
				for (Attribute attribute : relation.attributes()) {
					if ((StringUtils.equals(attribute.getLabel(), "DATE"))
							|| ((attribute.getLabel().contains("_DATE")) && (!attribute.getLabel().startsWith("CHAN_")))) {
						event.setDate(attribute.getLabel(), attribute.getValue().trim());
					} else {
						event.setProperty(attribute.getLabel(), attribute.getValue());
					}
				}

				//
				DateRange dateRange = event.getDateRange();
				if (dateRange.isValid()) {
					Calendar calendar = Calendar.getInstance();
					calendar.setTimeInMillis(dateRange.getStart() * 1000);
					int year = calendar.get(Calendar.YEAR);

					event.setLabel(String.valueOf(year));
				} else {
					event.setLabel(" R ");
				}

				event.setProperty("MODEL", relation.getModel().getName());

				if ((event.getDateRange().isValid()) || (criteria.isShowUndatedEvent())) {
					result.addVertex(event);
				}
			}

			for (Vertex vertex : result.getVertices()) {
				//
				if (vertex instanceof Indi) {
					//
					Indi indi = (Indi) vertex;

					if (indi.getFams() != null) {
						//
						for (String fid : indi.getFams()) {
							//
							Vertex fam = result.getVertex(fid);
							if (fam != null) {
								//
								Edge edge = new Edge(fam.getId(), vertex.getId());
								if (!result.containsEdge(edge)) {
									//
									result.addEdge(edge, fam, indi);
									edge.setFromVertex(fam);
									edge.setToVertex(indi);
								}
							}
						}
					}
				}
			}

			//
			if (!criteria.isShowUnlinkedIndividual()) {
				//
				List<Vertex> targetVertices = new ArrayList<Vertex>();

				//
				for (Vertex vertex : result.getVertices()) {
					//
					if (vertex instanceof Indi) {
						//
						if (result.getInEdges(vertex).isEmpty()) {
							//
							targetVertices.add(vertex);
						}
					}
				}

				//
				for (Vertex vertex : targetVertices) {
					//
					result.removeVertex(vertex);
				}
			}
		} catch (Exception exception) {
			//
			exception.printStackTrace();

			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Network convertToEventQuilt(final Net source) {
		Network result;

		result = convertToEventQuilt(source.individuals(), source.families(), source.relations());

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Network convertToEventQuilt(final Segmentation source) {
		Network result;

		result = convertToEventQuilt(source.getCurrentIndividuals(), source.getCurrentFamilies(), source.getCurrentRelations());

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Network convertToGeneaQuilt(final Individuals individuals, final Families families) {
		Network result;

		result = new Network();
		try {
			//
			for (Individual individual : individuals) {
				//
				Indi targetIndividual = new Indi();

				//
				targetIndividual.setId("I" + String.valueOf(individual.getId()));
				result.addVertex(targetIndividual);

				//
				targetIndividual.setProperty("NAME", individual.getName());

				if (StringUtils.isNotBlank(individual.getFirstName())) {
					targetIndividual.setProperty("NAME.GIVN", individual.getFirstName());
				}

				if (StringUtils.isNotBlank(individual.getLastName())) {
					targetIndividual.setProperty("NAME.SURN", individual.getLastName());
				}

				//
				if (individual.getGender() != null) {
					targetIndividual.setSex(String.valueOf(individual.getGender().toGedChar()));
				}

				//
				if (individual.getOriginFamily() != null) {
					targetIndividual.setFamc("F" + String.valueOf(individual.getOriginFamily().getId()));
				}

				//
				for (Family familySpouse : individual.getPersonalFamilies()) {
					targetIndividual.addFams("F" + String.valueOf(familySpouse.getId()));
				}

				//
				for (Attribute attribute : individual.attributes()) {
					if (attribute.getValue() == null) {
						targetIndividual.setProperty(attribute.getLabel(), attribute.getValue());
					} else {
						if ((StringUtils.equals(attribute.getLabel(), "DATE"))
								|| ((attribute.getLabel().contains("_DATE")) && (!attribute.getLabel().startsWith("CHAN_")))) {
							targetIndividual.setDate(attribute.getLabel(), attribute.getValue().trim());
						} else {
							targetIndividual.setProperty(attribute.getLabel(), attribute.getValue());
						}
					}
				}
			}

			//
			for (Family family : families) {
				//
				Fam fam = new Fam();

				//
				fam.setId(String.valueOf("F" + family.getId()));

				result.addVertex(fam);

				//
				if (family.getHusband() != null) {
					//
					fam.setHusb("I" + String.valueOf(family.getHusband().getId()));
				}

				//
				if (family.getWife() != null) {
					//
					fam.setWife("I" + String.valueOf(family.getWife().getId()));
				}

				//
				for (Attribute attribute : family.attributes()) {
					if (attribute.getValue() == null) {
						fam.setProperty(attribute.getLabel(), attribute.getValue());
					} else {
						if ((StringUtils.equals(attribute.getLabel(), "DATE"))
								|| ((attribute.getLabel().contains("_DATE")) && (!attribute.getLabel().startsWith("CHAN_")))) {
							fam.setDate(attribute.getLabel(), attribute.getValue().trim());
						} else {
							fam.setProperty(attribute.getLabel(), attribute.getValue());
						}
					}
				}
			}

			for (Vertex vertex : result.getVertices()) {
				//
				if (vertex instanceof Indi) {
					//
					Indi indi = (Indi) vertex;
					if (indi.getFamc() != null) {
						//
						Vertex fam = result.getVertex(indi.getFamc());
						if (fam != null) {
							//
							Edge edge = new Edge(vertex.getId(), indi.getFamc());
							if (!result.containsEdge(edge)) {
								//
								result.addEdge(edge, indi, fam);
								edge.setFromVertex(vertex);
								edge.setToVertex(fam);
							}
						}
					}

					if (indi.getFams() != null) {
						//
						for (String fid : indi.getFams()) {
							//
							Vertex fam = result.getVertex(fid);
							if (fam != null) {
								//
								Edge edge = new Edge(fam.getId(), vertex.getId());
								if (!result.containsEdge(edge)) {
									//
									result.addEdge(edge, fam, indi);
									edge.setFromVertex(fam);
									edge.setToVertex(indi);
								}
							}
						}
					}
				}
			}
		} catch (Exception exception) {
			//
			exception.printStackTrace();

			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Network convertToGeneaQuilt(final Net source) {
		Network result;

		result = convertToGeneaQuilt(source.individuals(), source.families());

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Network convertToGeneaQuilt(final Segmentation source) {
		Network result;

		result = convertToGeneaQuilt(source.getCurrentIndividuals(), source.getCurrentFamilies());

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param pattern
	 * @return
	 */
	private static StringList match(final StringList source, final String pattern) {
		StringList result;

		result = new StringList();

		for (String string : source) {
			if (string.matches(pattern)) {
				result.add(string);
			}
		}

		//
		return result;
	}
}
