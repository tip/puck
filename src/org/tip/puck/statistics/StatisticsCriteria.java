package org.tip.puck.statistics;

import java.util.ArrayList;
import java.util.List;

import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionCriteriaList;

/**
 * 
 * @author TIP
 */
public class StatisticsCriteria {
	private boolean genderBIASWeight;
	private boolean genderBIASNetWeight;
	private boolean components;
	private boolean differentialDensity;
	private boolean genealogicalCompleteness;
	private boolean sibsetDistribution;
	private boolean meanClusterValues;
	private boolean fourCousinMarriages;
	private boolean ancestorTypes;
	private boolean consanguineChains;
	private int ancestorTypesDegree;
	private int consanguineDegree;
	private PartitionCriteriaList partitionCriteriaList;
	private PartitionCriteria splitCriteria;

	/**
	 * 
	 */
	public StatisticsCriteria() {
		// Default value.
		this.genderBIASWeight = true;
		this.genderBIASNetWeight = true;
		this.components = true;
		this.differentialDensity = true;
		this.genealogicalCompleteness = true;
		this.sibsetDistribution = true;
		this.meanClusterValues = true;
		this.fourCousinMarriages = true;
		this.ancestorTypes = true;
		this.consanguineChains = true;
		this.ancestorTypesDegree = 3;
		this.consanguineDegree = 3;
		this.partitionCriteriaList = new PartitionCriteriaList();
		this.partitionCriteriaList.add(PartitionCriteria.createRaw("GENDER"));
		this.partitionCriteriaList.add(PartitionCriteria.createRaw("PEDG", "2"));
		this.partitionCriteriaList.add(PartitionCriteria.createRaw("PEDG", "3"));
		this.splitCriteria = PartitionCriteria.createRaw("GENDER");
	}

	public int getAncestorTypesDegree() {
		return this.ancestorTypesDegree;
	}

	public int getConsanguineDegree() {
		return this.consanguineDegree;
	}

	public PartitionCriteriaList getPartitionCriteriaList() {
		return this.partitionCriteriaList;
	}

	public List<String> getPartitionLabels() {
		List<String> result;

		result = new ArrayList<String>();

		for (PartitionCriteria criteria : this.partitionCriteriaList) {
			result.add(criteria.getLabel());
		}
		//
		return result;
	}

	public PartitionCriteria getSplitCriteria() {
		return this.splitCriteria;
	}

	public boolean isAncestorTypes() {
		return this.ancestorTypes;
	}

	public boolean isComponents() {
		return this.components;
	}

	public boolean isConsanguineChains() {
		return this.consanguineChains;
	}

	public boolean isDifferentialDensity() {
		return this.differentialDensity;
	}

	public boolean isFourCousinMarriages() {
		return this.fourCousinMarriages;
	}

	public boolean isSibsetDistribution() {
		return this.sibsetDistribution;
	}

	public boolean isGenderBIASNetWeight() {
		return this.genderBIASNetWeight;
	}

	public boolean isGenderBIASWeight() {
		return this.genderBIASWeight;
	}

	public boolean isGenealogicalCompleteness() {
		return this.genealogicalCompleteness;
	}

	public boolean isMeanClusterValues() {
		return this.meanClusterValues;
	}

	public void setAncestorTypes(final boolean value) {
		this.ancestorTypes = value;
	}

	public void setAncestorTypesDegree(final int value) {
		this.ancestorTypesDegree = value;
	}

	public void setComponents(final boolean components) {
		this.components = components;
	}

	public void setConsanguineChains(final boolean consanguineChains) {
		this.consanguineChains = consanguineChains;
	}

	public void setConsanguineDegree(final int consanguineDegree) {
		this.consanguineDegree = consanguineDegree;
	}

	public void setDifferentialDensity(final boolean differentialDensity) {
		this.differentialDensity = differentialDensity;
	}

	public void setFourCousinMarriages(final boolean fourCousinRelations) {
		this.fourCousinMarriages = fourCousinRelations;
	}

	public void setSibsetDistribution(final boolean sibsetDistribution) {
		this.sibsetDistribution = sibsetDistribution;
	}

	public void setGenderBIASNetWeight(final boolean genderBIASNetWeight) {
		this.genderBIASNetWeight = genderBIASNetWeight;
	}

	public void setGenderBIASWeight(final boolean genderBIASWeight) {
		this.genderBIASWeight = genderBIASWeight;
	}

	public void setGenealogicalCompleteness(final boolean genealogicalCompleteness) {
		this.genealogicalCompleteness = genealogicalCompleteness;
	}

	public void setMeanClusterValues(final boolean meanClusterValues) {
		this.meanClusterValues = meanClusterValues;
	}

	public void setSplitCriteria(final PartitionCriteria splitPartitionCriteria) {
		this.splitCriteria = splitPartitionCriteria;
	}

	/**
	 * 
	 * @return
	 */
	public static StatisticsCriteria getDefaultCriteria() {
		StatisticsCriteria result;

		result = new StatisticsCriteria();

		//
		return result;
	}
}
