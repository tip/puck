package org.tip.puck.graphs.workers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.tip.puck.PuckException;
import org.tip.puck.PuckManager;
import org.tip.puck.geo.Geography;
import org.tip.puck.graphs.ClusterPair;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.Node;
import org.tip.puck.io.dat.DATFile;
import org.tip.puck.matrix.MatrixStatistics;
import org.tip.puck.matrix.MatrixStatistics.Indicator;
import org.tip.puck.matrix.SparseMatrix;
import org.tip.puck.net.Family;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.workers.FamilyValuator;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.graphs.ClusterNetworkUtils.AllianceType;
import org.tip.puck.report.Report;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.util.Chronometer;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.PuckUtils;
import org.tip.puck.util.Value;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class GraphReporter {
	

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static StringList getCoupleList(final Graph<Cluster<Individual>> source, AllianceType allianceType) {
		StringList result;

		result = new StringList(100);

		//
		result.appendln("Couple list");
		for (Node<Cluster<Individual>> sourceNode : source.getNodes().toListSortedByLabel()) {
			if (sourceNode.getLabel() != null) {
				for (Node<Cluster<Individual>> targetNode : sourceNode.getOutLinks().getTargetNodesSortedByLabel()) {
					if (targetNode.getLabel() != null) {
						//
						StringList nodeCoupleList = new StringList();
						for (Individual individual : sourceNode.getReferent().getItems()) {
							if (allianceType == AllianceType.WIFE_HUSBAND){
								for (Family family : individual.getPersonalFamilies()) {
									if (family.getWife() == individual) {
										Individual spouse = family.getOtherParent(individual);
										if ((spouse != null) && (targetNode.getReferent().getItems().contains(spouse))) {
											nodeCoupleList.appendln(String.format("\t%d %s\t%d %s", individual.getId(),
													individual.getName(), spouse.getId(), spouse.getName()));
										}
									}
								}
							} else if (allianceType == AllianceType.SISTER_BROTHER){
								Family family = individual.getOriginFamily();
								if (family!=null && individual.isFemale() && individual.isNotSingle()){
									for (Individual sibling : family.getChildren()) {
										if ((sibling != null && sibling.isMale() && sibling.isNotSingle()) && (targetNode.getReferent().getItems().contains(sibling))) {
											nodeCoupleList.appendln(String.format("\t%d %s\t%d %s", individual.getId(),
													individual.getName(), sibling.getId(), sibling.getName()));
										}
									}
								}
							} else if (allianceType == AllianceType.PARENT_CHILD){
								for (Individual child : individual.children()) {
									if (child != null && targetNode.getReferent().getItems().contains(child)) {
										nodeCoupleList.appendln(String.format("\t%d %s\t%d %s", individual.getId(),
												individual.getName(), child.getId(), child.getName()));
									}
								}
							}
						}

						//
						if (nodeCoupleList.size() > 0) {
							//
							result.appendln(sourceNode.getLabel() + "-" + targetNode.getLabel()+"\t"+new Double(sourceNode.getArcWeight(targetNode)).intValue());
							result.addAll(nodeCoupleList);
							result.appendln();
						}
					}
				}
			}
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @param source
	 * @return
	 */
	public static StringList getSides(final Graph<Cluster<Individual>> source) {
		StringList result;

		result = new StringList(100);

		//
		result.appendln("Sides");
		
		Partition<Node<Cluster<Individual>>> sides = GraphUtils.sides(source);
		double sidedness = GraphUtils.sidedness(source,sides);
		
		result.appendln("Sidedness: "+sidedness);
		for (Cluster<Node<Cluster<Individual>>> side : sides.getClusters()){
			result.appendln("Side "+side.getValue()+"\t"+side.count()+" nodes");
		}
		result.appendln();
		for (Node<Cluster<Individual>> node : source.getNodes()){
			result.appendln(node.getId()+"\t"+node.getReferent().getLabel()+"\t"+sides.getValue(node));
		}
		
		//
		return result;

	}
	
	
	/**
	 * 
	 * @param source
	 * @return
	 */
	public static StringList getSortableList(final Graph<Cluster<Individual>> source, final Geography geography) {
		StringList result;

		result = new StringList(100);

		//
		result.appendln("Sortable list");
		result.appendln("Wife\tHusband\tMarriage Year\tWife's Cluster\tHusband's Cluster\tTotal Marriage Links");
		for (Node<Cluster<Individual>> sourceNode : source.getNodes().toListSortedByLabel()) {
			if (sourceNode.getLabel() != null) {
				for (Node<Cluster<Individual>> targetNode : sourceNode.getOutLinks().getTargetNodesSortedByLabel()) {
					if (targetNode.getLabel() != null) {
						//
						StringList nodeCoupleList = new StringList();
						for (Individual wife : sourceNode.getReferent().getItems()) {
							for (Family family : wife.getPersonalFamilies()) {
								if (family.getWife() == wife) {
									Individual husband = family.getOtherParent(wife);
									if ((husband != null) && (targetNode.getReferent().getItems().contains(husband))) {
										String marrYear = "";
										String husbandBirthPlace = "";
										String wifeBirthPlace = "";
										Value marrDate = FamilyValuator.get(family, "MARR_DATE", geography); 
										Value husbandBirtPlaceValue = IndividualValuator.get(husband, "BIRT_PLACE", geography);
										Value wifeBirtPlaceValue = IndividualValuator.get(wife, "BIRT_PLACE", geography);
										if (marrDate!=null){
											marrYear = FamilyValuator.extractYear(marrDate.stringValue());
										}
										nodeCoupleList.append(String.format("%d %s\t%d %s\t%s", wife.getId(),
												wife.getName(), husband.getId(), husband.getName(), marrYear));
									}
								}
							}
						}

						//
						for (String line : nodeCoupleList){
							result.appendln(line+"\t"+sourceNode.getLabel() + "\t" + targetNode.getLabel()+"\t"+new Double(sourceNode.getArcWeight(targetNode)).intValue());
						}
					}
				}
			}
		}

		//
		return result;
	}
	
	
	/**
	 * 
	 * @param source
	 * @return
	 */
	public static StringList getIndividualFlowList(final Graph<ClusterPair<Individual>> source) {
		StringList result;

		result = new StringList(100);

		//
		result.appendln("Individual Flow list");
		result.appendln();
		
		for (Node<ClusterPair<Individual>> sourceNode : source.getNodes().toListSortedByLabel()) {
			if (sourceNode.getLabel() != null) {
				for (Node<ClusterPair<Individual>> targetNode : sourceNode.getOutLinks().getTargetNodesSortedByLabel()) {
					if (targetNode.getLabel() != null) {
						//
						Individuals flows = new Individuals();
						for (Individual individual : sourceNode.getReferent().getLeft().getItems()) {
							if ((individual != null) && (targetNode.getReferent().getRight().getItems().contains(individual))) {
								flows.add(individual);
							}
						}
						//
						if (flows.size() > 0) {
							//
							result.appendln(sourceNode.getLabel() + " > " + targetNode.getLabel());

							for (Individual individual : flows.toSortedList()){
								result.appendln(String.format("\t%d %s", individual.getId(), individual.getName()));
							}
							result.appendln();
						}
					}
				}
			}
		}
		//
		return result;
	}
	
	/**
	 * 
	 * @param source
	 * @return
	 */
	public static StringList getRelationFlowList(final Graph<ClusterPair<Relation>> source) {
		StringList result;

		result = new StringList(100);

		//
		result.appendln("Relation Flow list");
		result.appendln();
		
		for (Node<ClusterPair<Relation>> sourceNode : source.getNodes().toListSortedByLabel()) {
			if (sourceNode.getLabel() != null) {
				for (Node<ClusterPair<Relation>> targetNode : sourceNode.getOutLinks().getTargetNodesSortedByLabel()) {
					if (targetNode.getLabel() != null) {
						//
						Relations flows = new Relations();
						for (Relation relation : sourceNode.getReferent().getLeft().getItems()) {
							if ((relation != null) && (targetNode.getReferent().getRight().getItems().contains(relation))) {
								flows.add(relation);
							}
						}
						//
						if (flows.size() > 0) {
							//
							
							result.appendln(sourceNode.getLabel() + " > " + targetNode.getLabel()+"\t("+flows.size()+")");

							for (Relation relation : flows.toSortedList()){
								result.append(String.format("\t%d %s\t", relation.getId(), relation.getName()));
								for (Actor actor: relation.actors()){
									if (actor.getRole().getName().equals("MIG")){
										result.append(actor.getName()+"; ");
									}
								}
								result.appendln();
							}
							result.appendln();
						}
					}
				}
			}
		}
		//
		return result;
	}

	public static <E> MatrixStatistics getMatrixStatistics(final Graph<E> source){
		MatrixStatistics result;
		
		//
		result = new MatrixStatistics(source);
		
		//
		return result;
	}

	
	public static <E> MatrixStatistics getMatrixStatistics(final List<Graph<E>> sources){
		MatrixStatistics result;
		
		//
		result = new MatrixStatistics();
		List<MatrixStatistics> statistics = new ArrayList<MatrixStatistics>();
		
		// aggregate values
		for (Graph<E> source : sources) {
			MatrixStatistics stats = new MatrixStatistics(source);
			statistics.add(stats);
			result.incrementValues(stats);
		}

		// normalize values
		result.normalizeValues(sources.size());

		// choose optimal example graph
		double target = result.getNumber(Indicator.CONCENTRATION);
		double shortestDistance = 1.;
		for (MatrixStatistics stats : statistics){
			double distance = Math.abs(target - stats.getNumber(Indicator.CONCENTRATION));
			if (distance < shortestDistance){
				shortestDistance = distance;
				result.setGraph(stats.getGraph());
			}
		}
		
		//
		return result;
	}
		
	
/*	public static <E> StringList reportMatrixStatistics(final List<Graph<E>> sources){
		StringList result;

		result = getMatrixStatistics(sources).reportValues(null);
		
		//
		return result;
		
	}
	
	public static <E> StringList reportMatrixStatistics(final List<Graph<E>> sources, final Graph<E> source){
		StringList result;

		MatrixStatistics statistics = getMatrixStatistics(sources);
		result = statistics.reportValues(getMatrixStatistics(source));
		
		//
		return result;
		
	}
	
	public static <E> StringList getGraphStatsAsLine (final Graph<E> source){
		StringList result;

		result = new StringList(100);

		MatrixStatistics stats = new MatrixStatistics(source);

		result.appendln("m\tn\tcx\tsx\tep0\tcx0\tcx*\tsx*\tep0*\tcx0*");
		result.append(source.nodeCount()+"\t");
		result.append(stats.getSum()+"\t");
		result.append(stats.getConcentrationIndex()+"\t");
		result.append(stats.getSymmetryIndex()+"\t");
		result.append(stats.getEndogamyIndex()+"\t");
		result.append(stats.getEndogamicConcentrationIndex()+"\t");
		result.append(stats.getExpectedConcentrationIndex() +"\t");
		result.append(stats.getExpectedSymmetryIndex()+"\t");
		result.append(stats.getExpectedEndogamyIndex()+"\t");
		result.append(stats.getExpectedEndogamicConcentrationIndex()+"\t");
		result.appendln();

		//
		return result;
	}*/
	
	/**
	 * temporary method
	 */
	public static <E> StringList getGraphStats(final Graph<E> source) {
		StringList result;

		result = new StringList(100);

		MatrixStatistics stats = new MatrixStatistics(source);
		MatrixStatistics rstats = null;
//		rstats = RandomGraphReporter.createRandomGraphStatisticsByRandomDistribution(source,100);

		result.appendln("alliance network:\t" + source.getLabel());
		result.appendln();
		result.append(MatrixStatistics.statistics(stats,rstats));
//		result.append(MatrixStatistics.statistics(stats, rstats));
/*		result.appendln("number of nodes:\t" + source.nodeCount());
		result.appendln("number of arcs:\t" + stats.getSum());
		result.appendln("maximal number of circuits:\t" + stats.getMaximalNumberOfCircuits());
		result.appendln();
		result.appendln("Strength distribution (marriages by clusters)");
		result.appendln("strength concentration index (mininimum: " + stats.getMinimumConcentration() + ")\t" + stats.getStrengthConcentrationIndex());
		result.appendln("endogamic strength concentration index: " + stats.getEndogamicStrengthConcentrationIndex());
		result.appendln("strength symmetry index: " + stats.getStrengthSymmetryIndex());
		result.appendln();
		result.appendln("Weight distribution (marriages by cluster pairs)\tobserved\texpected\tdivergence");
		result.appendln("endogamy index:\t" + stats.getEndogamyIndex() + "\t" + stats.getExpectedEndogamyIndex() + "\t"
				+ ((stats.getEndogamyIndex() - stats.getExpectedEndogamyIndex()) / stats.getExpectedEndogamyIndex()));
		result.appendln("concentration index (mininimum: " + stats.getMinimumConcentration() + "):\t" + stats.getConcentrationIndex() + "\t"
				+ stats.getExpectedConcentrationIndex() + "\t"
				+ ((stats.getConcentrationIndex() - stats.getExpectedConcentrationIndex()) / stats.getExpectedConcentrationIndex()));
		result.appendln("endogamic concentration index:\t"
				+ stats.getEndogamicConcentrationIndex()
				+ "\t"
				+ stats.getExpectedEndogamicConcentrationIndex()
				+ "\t"
				+ ((stats.getEndogamicConcentrationIndex() - stats.getExpectedEndogamicConcentrationIndex()) / stats
						.getExpectedEndogamicConcentrationIndex()));
		result.appendln("symmetry index:\t" + stats.getSymmetryIndex() + "\t" + stats.getExpectedSymmetryIndex() + "\t"
				+ ((stats.getSymmetryIndex() - stats.getExpectedSymmetryIndex()) / stats.getExpectedSymmetryIndex()));
		result.appendln("Circuit census\tobserved\texpected\tdivergence");
		result.appendln("number of loops\t" + stats.getNumberOfLoops()+"\t"+stats.getExpectedNumberOfLoops());
		result.appendln("number of circuits:\t" + stats.getNumberOfCircuits() + "\t" + stats.getExpectedNumberOfCircuits() + "\t"
				+ stats.getSurplusOfCircuits()+"\t"+rstats.getNumberOfCircuits()+ "\t" + rstats.getExpectedNumberOfCircuits() + "\t"
						+ rstats.getSurplusOfCircuits());
		result.appendln("number of parallel circuits:\t" + stats.getNumberOfParallelCircuits() + "\t" + stats.getExpectedNumberOfParallelCircuits() + "\t"
				+ stats.getSurplusOfParallelCircuits());
		result.appendln("number of cross circuits:\t" + stats.getNumberOfCrossCircuits() + "\t" + stats.getExpectedNumberOfCrossCircuits() + "\t"
				+ stats.getSurplusOfCrossCircuits());
		result.appendln("balance of parallel vs cross circuits:\t" + stats.getBalanceOfParallelVsCrossCircuits() + "\t"
				+ stats.getExpectedBalanceOfParallelVsCrossCircuits());
		result.appendln("number of triangles:\t" + stats.getNumberOfTriangles());
		result.appendln("number of cyclic triangles:\t" + stats.getNumberOfCyclicTriangles());
		result.appendln("number of transitive triangles:\t" + stats.getNumberOfTransitiveTriangles());
		result.appendln();
		result.appendln("normalized number of circuits:\t" + stats.getNormalizedNumberOfCircuits() + "\t" + stats.getExpectedNormalizedNumberOfCircuits());
		result.appendln("normalized number of parallel circuits:\t" + stats.getNormalizedNumberOfParallelCircuits() + "\t"
				+ stats.getExpectedNormalizedNumberOfParallelCircuits());
		result.appendln("normalized number of cross circuits:\t" + stats.getNormalizedNumberOfCrossCircuits() + "\t"
				+ stats.getExpectedNormalizedNumberOfCrossCircuits());
		result.appendln("normalized balance of parallel vs cross circuits:\t" + stats.getNormalizedBalanceOfParallelVsCrossCircuits() + "\t"
				+ stats.getExpectedNormalizedBalanceOfParallelVsCrossCircuits());
		result.appendln();*/

		//
		return result;
	}
	
	

	/**
	 * writes the matrix of an alliance network
	 * 
	 * @param net
	 *            the underlying alliance network
	 * @throws IOException
	 * @since 11-05-21
	 */
	public static <E> StringList getMatrixStrings(final Graph<E> source) {
		StringList result;

		//
		SparseMatrix sparse = source.getSquareMatrix();
//		SparseMatrix sparse = source.getMatrix();

		//
		result = new StringList(100);

		//
		result.appendln("sources=" + sparse.getSourceCount());
		result.appendln("targets=" + sparse.getTargetCount());
		result.appendln();

		//
		result.append("m=" + sparse.getTargetCount() + "\t");
		for (int columnIndex = 0; columnIndex < sparse.getMatrix().getColDim(); columnIndex++) {
			result.append(source.getNode(sparse.getTargetId(columnIndex)).getLabel()).append("\t");
		}
		result.appendln("Pop");

		//
		for (int rowIndex = 0; rowIndex < sparse.getMatrix().getRowDim(); rowIndex++) {
			//
			result.append(source.getNodes().get(sparse.getSourceId(rowIndex)).getLabel() + "\t");

			//
			for (int columnIndex = 0; columnIndex < sparse.getMatrix().getColDim(); columnIndex++) {
				result.append(MathUtils.toString(sparse.getMatrix().get(rowIndex, columnIndex)) + "\t");
			}
			result.appendln(sparse.getMatrix().getRowSum(rowIndex));
		}

		//
		result.append("Pop" + "\t");
		for (int columnIndex = 0; columnIndex < sparse.getMatrix().getColDim(); columnIndex++) {
			result.append(sparse.getMatrix().getColSum(columnIndex) + "\t");
		}
		result.appendln("n=" + sparse.getSum());
		result.appendln();

		//
		return result;
	}

	/**
	 * writes the matrix of an alliance network
	 * 
	 * @param net
	 *            the underlying alliance network
	 * @throws IOException
	 * @since 11-05-21
	 */
	public static <E> StringList getRawMatrixStrings(final Graph<E> source) {
		StringList result;

		//
		SparseMatrix sparse = source.getSquareMatrix();
//		SparseMatrix sparse = source.getMatrix();

		//
		result = new StringList(100);
		
		//
		for (int rowIndex = 0; rowIndex < sparse.getMatrix().getRowDim(); rowIndex++) {
			//
			for (int columnIndex = 0; columnIndex < sparse.getMatrix().getColDim(); columnIndex++) {
				result.append(MathUtils.toString(sparse.getMatrix().get(rowIndex, columnIndex)));
				if (columnIndex< sparse.getMatrix().getColDim()-1) {
					result.append("\t");
				}

			}
			if (rowIndex< sparse.getMatrix().getRowDim()-1) {
				result.appendln();
			}
		}
		//
		return result;
	}
	

	/**
	 * Generates a report.
	 * 
	 * @FIXME Temporary code
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
/*	public static Report reportCircuitInducedFrameNetwork(final Segmentation source, final File currentCorpus) throws PuckException {
		Report result;

		if ((source == null) || (currentCorpus == null)) {
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
		} else {
			result = new Report();

			Chronometer chrono = new Chronometer();

			// Compute.

			// Variable should be the CircuitFinder...
			// Report should be started AFTER census
			CensusCriteria criteria = new CensusCriteria();
			criteria.setPattern("3 1");
			criteria.setChainClassification("CLASSIC");
			CircuitFinder3 finder = new CircuitFinder3(source.getCurrentIndividuals(), source.getAllIndividuals(), criteria);
			finder.findCircuits();

			// Code for Reporter should start here
			Graph<Individual> graph = GraphMaker.createCircuitInducedFrameNetwork(finder);

			StringList partitionLabels = new StringList();

			// Build Pajek data.
			ReportRawData rawData = new ReportRawData("Export to Pajek", "Pajek", "paj", new File(currentCorpus.getParent() + File.separator
					+ ToolBox.clean(graph.getLabel()) + ".paj"));

			rawData.setData(writePajekNetwork(graph, partitionLabels).toString());

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}*/
 
	/**
	 * Generates a report.
	 * 
	 * @FIXME Temporary code
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
/*	public static Report reportCircuitInducedNetwork(final Segmentation source, final File currentCorpus) throws PuckException {
		Report result;

		if ((source == null) || (currentCorpus == null)) {
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
		} else {
			result = new Report();

			Chronometer chrono = new Chronometer();

			// Compute.

			// Variable should be the CircuitFinder...
			// Report should be started AFTER census
			CensusCriteria criteria = new CensusCriteria();
			criteria.setPattern("3 1");
			criteria.setChainClassification("CLASSIC");
			CircuitFinder3 finder = new CircuitFinder3(source.getCurrentIndividuals(), source.getAllIndividuals(), criteria);
			finder.findCircuits();

			// Code for Reporter should start here
			Graph<Individual> graph = GraphMaker.createCircuitInducedNetwork(finder);

			StringList partitionLabels = new StringList();

			// Build Pajek data.
			ReportRawData rawData = new ReportRawData("Export to Pajek", "Pajek", "paj", new File(currentCorpus.getParent() + File.separator
					+ ToolBox.clean(graph.getLabel()) + ".paj"));

			rawData.setData(writePajekNetwork(graph, partitionLabels).toString());

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}*/

	/**
	 * Generates a report.
	 * 
	 * @FIXME Temporary code
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
/*	public static Report reportCircuitIntersectionNetwork(final Segmentation source, final File currentCorpus) throws PuckException {
		Report result;

		if ((source == null) || (currentCorpus == null)) {
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
		} else {
			result = new Report();

			Chronometer chrono = new Chronometer();

			// Compute.

			// Variable should be the CircuitFinder...
			// Report should be started AFTER census
			CensusCriteria criteria = new CensusCriteria();
			criteria.setPattern("3 1");
			criteria.setChainClassification("CLASSIC");
			CircuitFinder3 finder = new CircuitFinder3(source.getCurrentIndividuals(), source.getAllIndividuals(), criteria);
			finder.findCircuits();

			// Code for Reporter should start here
			Graph<Cluster<Chain>> graph = GraphMaker.createCircuitIntersectionNetwork(finder);

			StringList partitionLabels = new StringList();
			partitionLabels.append("INDEGREE");
			partitionLabels.append("OUTDEGREE");
			partitionLabels.append("SIZE");
			partitionLabels.append("ORDER");
			partitionLabels.append("DRAV");

			// Build Pajek data.
			ReportRawData rawData = new ReportRawData("Export to Pajek", "Pajek", "paj", new File(currentCorpus.getParent() + File.separator
					+ ToolBox.clean(graph.getLabel()) + ".paj"));

			rawData.setData(writePajekNetwork(graph, partitionLabels).toString());

			// Build report.
			result = new Report();
			result.setTitle("Circuit Intersection Network Report.");
			result.setOrigin("GraphReport.reportCircuitIntersectionNetwork()");
			result.setTarget(source.getLabel());

			//
			// result.inputs().add()

			//
			result.outputs().appendln(getGraphStats(graph));
			result.outputs().appendln(getMatrixStrings(graph));

			result.outputs().appendln(" ");

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}*/

	/**
	 * Generates a report.
	 * 
	 * @FIXME Temporary code
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
/*	public static Report reportCircuitNetworks(final Segmentation source, final File currentCorpus) throws PuckException {
		Report result;

		if ((source == null) || (currentCorpus == null)) {
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
		} else {
			result = new Report();

			Chronometer chrono = new Chronometer();

			// Compute.

			// Variable should be the CircuitFinder...
			// Report should be started AFTER census
			CensusCriteria criteria = new CensusCriteria();
			criteria.setPattern("3 1");
			criteria.setChainClassification("CLASSIC");
			CircuitFinder3 finder = new CircuitFinder3(source.getCurrentIndividuals(), source.getAllIndividuals(), criteria);
			finder.findCircuits();

			// Code for Reporter should start here

			StringList partitionLabels = new StringList();

			// Provisory
			// Build Pajek data.
			ReportRawData rawData = new ReportRawData("Export to Pajek", "Pajek", "paj", new File(currentCorpus.getParent() + File.separator
					+ ToolBox.clean("Circuits") + ".paj"));

			for (Chain circuit : finder.getCircuits().getItems()) {
				Graph<Individual> graph = GraphMaker.createCircuitNetwork(circuit);
				rawData.appendData(writePajekNetwork(graph, partitionLabels).toString());
			}

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}*/

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static void test() throws PuckException {
		// SET YOUR OWN PATH HERE.
		// Net net = PuckManager.loadCorpus(new
		// File("C:\\Documents and Settings\\Klaus Hamberger\\Bureau\\Ebrei 2009.ged"));
		Net net = PuckManager.loadNet(new File("/Users/klaushamberger/Desktop/Ebrei 2009.ged"));

		// Compute.
		Segmentation source = new Segmentation(net);

		// Code for Reporter should start here
		Graph<Family> graph = NetUtils.createPGraph(source);

		StringList partitionLabels = new StringList();
		System.out.println("==================================");
		System.out.println(PuckUtils.writePajekNetwork(graph, partitionLabels).toString());
		System.out.println("==================================");
	}
	
	public static Report reportSynopsis (String directory){
		Report result;

		//
		Chronometer chrono = new Chronometer();
		result = new Report();

		//
		result.setTitle("Basic statistics about a corpus.");
		result.setOrigin("Statistics reporter");
		
		StringList listStatistics = new StringList();
		String headline = "General statistics";
		for (MatrixStatistics.Indicator indicator : MatrixStatistics.Indicator.values()){
			headline += "\t"+indicator.toString();
		}
		result.outputs().appendln(headline);
		
/*		StringList listBias = new StringList();
		listBias.appendln("Gender bias\tA1\tA2\tA3\tA4\tA5\tU1\tU2\tU3\tU4\tU5");
		
		StringList listCensus = new StringList();
		listCensus.appendln("First cousin marriages\tParPat\tCross\tParMat");
		
		String pattern = "XX(X)XX";
		String classificationType = "LINE";*/
		
			File folder = new File(directory);
			for (File file : folder.listFiles()) {
				try {
					Graph<Cluster<Individual>> graph = DATFile.load(file);
					listStatistics.appendln(MatrixStatistics.getValueString(graph));
//					listBias.appendln(StatisticsWorker.listBiasWeights(net));
//					listCensus.appendln(StatisticsWorker.listCircuitFrequencies(net,classificationType,pattern));
				} catch (PuckException e) {
					System.err.println("Not a corpus file: " + file.getName());
				}
			}
		result.outputs().append(listStatistics);
		result.outputs().appendln();
/*		result.outputs().append(listBias);
		result.outputs().appendln();
		result.outputs().append(listCensus);
		result.outputs().appendln();*/

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}	
	
	public static <E> Report reportStrengthsByTags (Graph<E> graph){
		Report result;
		
		result = new Report(graph.getLabel()+" Strengh Census");
		
		List<String> tags = graph.getArcs().getTags();
		Collections.sort(tags);
		
		String headLine = "Node\tSTRENGTH\tINSTRENGTH\tOUTSTRENGTH\tSTRENGTHBALANCE\tLOOPSTRENGTH\t";
		for(String tag : tags){
			headLine += tag+"_INSTRENGTH\t"+tag+"_OUTSTRENGTH\t";
		}
		result.outputs().appendln(headLine);

		for (Node<E> node : graph.getNodes()){
			String valueLine = node.toString()+"\t" + node.getForce()+ "\t" + node.getInForce()+ "\t" + node.getOutForce()+ "\t" + node.getForceBalance()+ "\t" + node.getLoopForce()+ "\t";
			for(String tag : tags){
				valueLine += node.getInArcsByTag(tag).sumWeight() + "\t" + node.getOutArcsByTag(tag).sumWeight() + "\t";
			}
			result.outputs().appendln(valueLine);
		}
		result.outputs().appendln();
		
		//
		return result;
	}
	
	

}
