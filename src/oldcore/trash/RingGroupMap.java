package oldcore.trash;

/**
 * The class for maps of RingGroups
 * @author Klaus Hamberger
 * @since Version 0.5
 */
public class RingGroupMap  {

	/**
	 * true if a the number of possible chain types is fixed from the outset
	 */
//   private boolean fixedNrTypes;
 
   /**
    * the associated closedNet used as a search space
    */
//   private ClosedNet closedNet;
   
   /**
    * the criterion for kinship chain aggregation (circuit clustering)
    */
//   private String clusterLabel;
   //check count types
   /**
    * the array of counts, by type of count (0, 1, 2 number of rings, 3 number of ring types) and order of the ring
    */
//   private int[][] count;


   /**
    * true if chains between given pivotal pairs shall be counted, according to the search options in the census screen
    */
//   private boolean egoToAlter;


   /**
    * true if matrimonial circuits are to be counted
    */
//   private boolean matrim;


   /**
    * the name of the Census
    */
//   private String name;
   /**
    * the Network containing the Chains to be counted
    */
//   private Net net;
   /**
    * the restriction level as to cluster anchorage (0 no limitation, 1 at least one pivot in the cluster, 
	 * 2 all pivots in the cluster, 3 the last married pivot in the cluster (for periodization),
	 * 4 ego in cluster)
    */
//   private int restriction;



   /**
    * the supernet of the underlying Network
    */
//   private Net supernet;

	
	

	/**
	 * constructs a VertexChainGroupMap for an equivalence relation census
	 * @param map the underlying map (Net or RingGroupMap)
	 * @param lb the partition commande
	 */
/*	public RingGroupMap (RingGroupMap map, String cmd){
		super(new KeyComparator(6));
		((NodePartition)map.getPartition(cmd)).relationalize();
		relation = "co-"+cmd;
		initialize(map, 6);
		census(map, 6);
	}*/
	
	
	/**
	 * gets the ith key of the GroupMap
	 * @param the index of the key
	 * @return the key
	 * @see maps.GroupNet#ClassNet(ChainGroupMap, int[])
	 */
/*	public K getKey (int i){
		int j=0;
		for (K k : keySet()){
			if (i==j) return k;
			j++;
		}
		return null;
	}*/


		

   
   //not possible after model search - use a special filter field!!
   /**
    * gets the filter criterion from the right field in the census screen 
    * @return the filter criterion
    */
/*	private static String getFilter(){
		String filter = ""; 
		return filter;
	}*/
   

   /**
    * returns the unilinear code for ascendant lines <p>
    * (0 agnatic, 1 uterine, -1 cognatic)
    * @return the unilinear code
    */
/*	private static int line(){
		int result;
		result = 0; // (0 agnatic, 1 uterine, -1 cognatic)
		return result;
	}*/
   
   /**
    * a shortcut for headlines
    * @param i the headline index
    * @return the required headline
    */
/*   private static String t(int i){
	   return "";
   }*/
	


   /**
    * checks whether a vertex is admissible according to cluster affiliation
    * @param i the vertex ID
    * @return true if cluster affiliation is required but the vertex does not belong to the cluster
    * @see chains.NumberChain#expand1(RingGroupMap,int)
    */
/*   public boolean cannotPass(int i){
	   if (restriction==2) return (!individuals.contains(i));
	   return false;
   }*/
   

   

   

   
   /**
 * augments the count of type i of rings of order j by the value k
 * @param i the type of the count (0, 1, 2 number of rings, 3 number of ring types)
 * @param j the order of the ring
 * @param k the value to be added
 * @see RingGroupMap#countRings(Ring)
 * @see RingGroupMap#countTypes(Ring)
 */
/*private void count (int i, int j, int k) {
      if (count==null) return;
      count[i][j-1] = count[i][j-1]+k;
   }*/
   
   /**
 * counts affinal chains that involve only one marriage and one consanguineous relation (such as WZ or BW)
 * <p> a provisional method to extend the open chain census beyong consanguinity
 * @see RingGroupMap#census(Map, int) 
 */
/*private void countAffinalChains (){
	for (Vertex v: supernet.values()){
		if (v.ignore()) continue;
		if (v.isSingle()) continue;
		int i = v.getID();
		if (cannotPass(i)) continue;
		for (Vertex u : v.getSpouses()){
			int k = u.getID();
			for (int j : closedNet.links(k,deg[1])){
				if (j==k) continue;
				if (cannotPass(j)) continue;
//					Vertex w = supernet.get(j);
//					   if (ignore(w)) continue;
//					   if (ringType>0 && directLink(v,w)) continue; 
				if (ringType>0 && directLink(i,j)) continue; 
				else {
					NumberChain p = new NumberChain(i,k);
					p.add(j);
					p.add(i);
					p.complete(this,line);
				}	
			}
		}
	}
}*/
   
	//extremely heavy and time-consuming! replace be the old dfs method and implement the restrictions! Try the same for the affinal chain search, or for chain search in general
	//insert AggregationFunction analogous to RingGroupMap.aggregate!
	/**
	 * counts the consanguineous chains between two vertices
	 * @param v the ego vertex
	 * @param w the alter vertex
	 * @param map the map used for counting
	 * @see RingGroupMap#countConsanguineChains(CountMap)
	 */
/*	private void countBridges (Vertex v, Vertex w, CountMap<int[]> map){
		if (cannotPass(new NumberChain(v.getID(),w.getID()))) return;  
		for (Ring t : v.getBridges(w,deg[0],line,sib)) {
			if (t.size()==1) continue;
			if (ringType>0 && t.isRoundabout(sib)) continue;
			if (cannotPass(t)) continue;
//			 if (ringType==2 && t.hasShortcut(false)) continue;
//			 else if (ringType==3 && t.hasShortcut(true)) continue;
//			 if (!t.isModel(models,(restriction==4))) continue; //simplify: continue with r = t.standard(models), and stop if r = null...
//			 if (restriction==4 && !net.containsKey(t.standard().getID(0))) continue;
//-->		 insert AggregationFunction analogous to RingGroupMap.aggregate!
//			 if (restriction==5) map.count(t.standard(models).getVector());
//			 else map.count(t.standard(crossSex).getVector());
			if (restriction==5) {
				Ring r = t.standard(models); 
				if (r!=null) map.add(r.getVector());
			}
			else map.add(t.standard(sym).getVector());
//			else map.add(t.standard(crossSex).getVector());
		}
	}*/
	
//replaced by method "byAttributes" in CircuitFinder
	/**
	 * aggregates chains into a new RingNet according to clusters (instead of characteristic vectors) 
	 * @param rn the underlying RingNet
	 */
/*	private void countCircuitClusters(String str) {
		for (Cluster<Chain> rings : sorter) {
			boolean exists = false;
			Object clu = rings.getCluster(str);
			for (Cluster<Chain> crings : sorter){
				if (crings.getCluster(str).equals(clu)) {
					crings.addAll(rings);
					exists = true;
					continue;
				}
			}
			if (exists) continue;
			Cluster<Chain> cRings = rings.getModel().newGroup();
			cRings.setLabel(Trafo.asString(clu));
			cRings.addAll(rings);
			cRings.setNumber(size()+1);  //formerly setNr
			put(rings.getModel().getVector(),cRings);
		}
		for (Cluster<Chain> crings : sorter){
			int d = ((RingGroup)crings).dim()-1;
			count[2][d]=count[2][d]+crings.size();
			count[3][d]++;
		}
	}*/

	
	//check and replace eventually with old method
	/**
	 * searches and/or counts the consanguinuous chains in a net
	 * @param map the map for counting (if null, a standard census is performed)
	 * @see RingGroupMap#census(Map, int)
	 * @see RingGroupMap#countConsanguines()
	 */
/*	private void countConsanguineChains (CountMap<int[]> map){
		for (Vertex v: supernet.values()){
			if (v.ignore()) continue;
			int i = v.getID();
			if (cannotPass(i)) continue;
			if (cannotPass(v)) continue;
			for (int j : closedNet.links(i,deg[0])){
				if (cannotPass(j)) continue;
				Vertex w = supernet.get(j);
				if (w.ignore()) continue;
				if (cannotPass(w)) continue;
				if (w.precedes(v,0) && restriction<4) continue;
				if (map!=null) countBridges(v,w,map);
				else new NumberChain(i,j).complete(this,line);
			}
		}
	}*/



	
	

	

	
	/**
	 * checks whether the models contain only linear chains
	 * @return true if the models contain only linear chains
	 * @see chains.Ring#searchChains(RingGroupMap, java.util.List)
	 * @since 10/04/12
	 */
/*	public boolean isLinear(){
		return linearOnly;
	}*/
	

	
/*	public CountMap<int[]> getRelationFrequencies (){
		return new RingGroupMap(this,9).getFrequencies();
	}*/
	
	//check for nonPermutability conditions 4 and 5
	/**
	 * counts the consanguinuous chains in a net
	 * @return the count map for consanguineous chains
	 * @see gui.screens.ChartScreen#Chart(Net, String, int, boolean)
	 * @see io.write.AbstractWriter#writeTxtCensus(RingGroupMap,int)
	 */
/*	public CountMap<int[]> getFrequencies (){
		CountMap<int[]> map = new CountMap<int[]>(1);
//		countConsanguineChains(map);
		for (int[] a : keySet()){
			map.put(a, get(a).size());
		}
//		if (restriction==4 || restriction==5) map.div(2*dim);
		return map;
	}*/
	
/*	public void count (int i, int j) {
		   if (count!=null) count(i,j,1);
	   }*/

	/**
	 * counts the number of couples for each order of chains
	 * @return the count array for couples
	 * @see gui.TipWriter#writeSurvey(RingGroupMap, CountMap<int[],int)
	 */
/*	public int[] countCouples () {
		int[] cc = new int[dim];
		ArrayList<int[]> couples = new ArrayList<int[]>();
		int d=0;
		for (int[] key : keySet()) {
			if (key.length/2>d) {
				if (d>0) cc[d-1]=couples.size();
				d = key.length/2;
				couples = new ArrayList<int[]>();
			}	
			try {
				((RingGroup)get(key)).countCouples(couples,false);
			} catch (NullPointerException npe) {
				continue;
			}
		}
		if (d!=0) cc[d-1]=couples.size();
		return cc;
	}*/
	

	

	
	//check
	/**
	 * counts the chains according to their order. In case of connubial rings (in alliance networks) alliances are weighted with the number of marriages
	 * @param r the chain to be counted
	 * @see RingGroupMap#add(int[], Ring)
	 */
/*	private void countRings (Chain r){
		int c = 1;
	    if (net.getType()==4 && r.dim()>1) c=r.getValue(); 
	    count(2,r.dim(),c);
	}*/
	
	/**
	 * counts the chain types from the model set according to their order. 
	 * @see RingGroupMap#census(Map, int)
	 */
/*	private void countTypes () {
		for (Chain r : models) {
			countTypes(r);
			put(r.getVector(),r.newGroup(count[3][r.dim()-1]));
		}
	}*/
	
	/**
	 * counts the chain types according to their order
	 * @param r the model chain
	 * @see RingGroupMap#countTypes()
	 * @see RingGroupMap#openRingClass(int[], Ring)
	 */
/*	private void countTypes (Chain r){
		if (count!=null) count(3,r.dim(),1);
	}*/
	
	/**
	    * checks whether chains are expected to be heterosexual according to chosen options
	    * @return true if chains are expected to be heterosexual
	    */
/*	public boolean crossSex(){
		if (optionCrossSex()) return true;
		if (marriedOnly) return true;
		return false;
	}*/


	
	/**
	 * determines the degree array (maximal canonic degree for chains of different widths) from the code entered in the left field of the census screen <p>
	 * (e.g. 3 2 - consanguineous chains up to degree 1, simple affinal chains up to degree 2)
	 * @return the degree array
	 */
/*	private int[] deg(){
		String str = Puck.getCensusCommand(1);
//		String str = Puck.getText (3, 1);
		try {
			return intArray(str+" 0");
		} catch (NumberFormatException nfe) {
			return null;
		}
	}*/
	
	/**
	 * gets the maximal canonic degree of bridges in affinal circuits 
	 * @return the maximal canonic degree 
	 */
/*	private int degMax (){
		if (maxDeg.length<2) return 0;
		return maxDeg[1];*/
/*		int k=0;
		for (int i : deg){
			if (i>k) k=i;
		}
		return k;
	}*/
	

	
	/**
	 * checks whether the affinal chains contain only one consanguinous bridge
	 * @return true if the affinal chains contain only one consanguinous bridge
	 * @see chains.Ring#searchChains(RingGroupMap, java.util.List)
	 * @since 10/04/17 
	 */
/*	public boolean isSolo(){
		return solo;
	}*/

	
	/**
	 * gets the maximal order of the rings
	 * @return the maximal ring order
	 */
/*	public int dim(){
		return dim;
	}*/
	
	/**
	 * gets the maximal order (number of consanguineous components) of the chains from the degree array
	 * @param z the degree array
	 * @return the maximal order of chains
	 */
/*	private int dim (int[] z) {
		int n = z.length;
		while (z[n-1]==0) {
			n = n-1;
		}
		return n;
	}*/
	

	
	   
	   
/*   private boolean directLink (Vertex v, Vertex w){
	   if (v.isSpouse(w)) return true;
	   Vertex vp = v.getParent(w.getGender());
	   if (vp!=null && vp.equals(w)) return true;
	   Vertex wp = w.getParent(v.getGender());
	   return (wp!=null && wp.equals(v));
   }*/
	   


   /**
    * gets the chosen criterion for kinship chain aggregation (circuit clustering)
    * @return the chosen aggregation criterion 
    */
/*   public String getClusterLabel(){
	   if (!Puck.searchOption(3)) return null;
	   clusterLabel = Puck.getLabels(5)[0];
	   return clusterLabel;
   }*/
   
   //Verify count types
   /**
    * gets the count results according to count type and chain width (order) 
    * @param i the count type ***
    * @param j the chain order
    * @return the count result
    */
/*   public int getCount(int i, int j){
	   return count[i][j];
   }*/
  
   //cf. ClusterNetwork#decompose
   /**
    * gets the array of circuit group decomposition rates
    * @return the array of circuit group decomposition rates
    * @see gui.screens.StatsScreen#show(String[])
    */
/*   public double[][] getDecomp (){
	   return decomp;
   }*/
   


   //Rename
/**
 * returns the kinship link map
 * @return the kinship link map
 */
/*   public ClosedNet getMatrix() {
	   return closedNet;
   }*/

  
   /**
    * gets the title of the census
    * @return the title of the census
    */
/*   public String getName() {
	   if (name!=null) return name;
	   if (schema!=null) return net.getName()+" "+schema;
	   return net.getName()+" "+Arrays.toString(deg);
   }*/
   
/*   public void getRings () {
	   
	   if (matrim) countCircuits();
	   else if (dim==1) countConsanguineChains(null);
	   else countAffinalChains();
   }*/

   //Rename
   /**
    * gets the underlying map
    * @return the underlying map
    */
/*	public Net getNet (){
		return net;
	}*/
   
	/**
	 * the restriction level as to cluster anchorage (0 no limitation, 1 at least one pivot in the cluster, 
	 * 2 all pivots in the cluster, 3 the last married pivot in the cluster (for periodization),
	 * 4 ego in cluster)
	 * @return the restriction level
	 */
/*	public int getRestriction() {
		return restriction;
	}*/	

	/**
	    * gets the required ring type 
	    * <p> 0 all circuits, 1 only rings, 2 only minor rings, 3 only minimal rings
	    * @return the required ring type 
	    * @see chains.Ring#expand(int,int,NumberChain,RingGroupMap,int)
	    * @see chains.NumberChain#getMap(RingGroupMap)
	    * @see io.write.TxtWriter#writeCensusHead(RingGroupMap)
	    */
/*	   public int getRingType(){
		   return ringType;
	   }*/

	/**
	    * gets the chain schema (in positional notation)
	    * @return the chain schema
	    * @see io.write.TxtWriter#writeCensusHead(RingGroupMap)
	    */
/*	   public String getSchema(){
		   return schema;
	   }*/

   /**
    * gets the sibling mode
    * <p> 1 all siblings assimilated, 2 no siblings assimilated, 3 full siblings assimilated
    * @return the sibling mode
    * @see chains.NumberChain#complete(RingGroupMap, int)
    * @see chains.Ring#expand(int,int,NumberChain,RingGroupMap,int)
    * @see io.write.TxtWriter#writeCensusHead(RingGroupMap)
    */
/*   public int getSib(){
	   return sib;
   }*/
   


/*   private TxtWriter getWriter(String adress, Screen screen) throws IOException{
	   if (Puck.startOption(13)) return new XlsWriter(adress,screen);
	   return new TxtWriter(adress,screen);
   }*/
   
   /**
 * initializes the count array
    */
/*private void initCount () {
	count = new int[4][dim]; 
	for (int i=0;i<4;i++) {
		count[i] = new int[dim];
	}
}*/



	

	
   /**
    * checks whether only cross-sex chains are admitted
    * @return true if only cross-sex chains are admitted
    * @see chains.Ring#expand(int,int,NumberCHain,RingGroupMap,int)
    */
/*   public boolean isCrossSex() {
	   return crossSex;
   }*/

   
/*   public void reduceToMinimals (){
	   for (ChainGroup<Ring> c : values()){
		   for (Ring r : c) {
			   if (r.hasIntersectionIn(this)) System.out.println(r.signature());
		   }
	   }
   }*/



   /**
    * checks whether chains between given pivotal pairs shall be counted, according to the search options in the census screen
    * @return true if chains between given pivotal pairs shall be counted, according to the search options in the census screen
    * @see chains.NumberChain#complete(RingGroupMap, int)
    * @see chains.NumberChain#getMap(RingGroupMap)
    * @see chains.NumberChain#violatesORder(int,RingGroupMap)
    */
/*   public boolean isEgoToAlter() {
	   return egoToAlter;
   }*/
   
   /**
    * checks whether the census is matrimonial
    * @return true if matrimonial circuits are to be counted
    */
/*   public boolean isMatrim (){
	   return matrim;
   }
   

   /**
    * returns the symmetry index of the rings
    * <p> 0 not permutable, 1 ego-alter reflection possible, 2 fully permutable
    * @return the symmetry index of the rings
    */
/*   public int getSymmetry (){
	   return sym;
   }*/

   /**
    * checks whether marriage is a condition for admissibility of a vertex as a start vertex
    * @return true if marriage is a condition for a start vertex 
    */
/*	private boolean marriedOnly(){
	   if (optionCircuits()) return !optionEgoToAlter();
	   return (optionMarriedOnly());
	}*/





   
   /**
 * checks whether only circuits (matrimonial or relational) or open chains shall be searched, according to the chosen search option in the census screen
 * @return true if only circuits are searched
 */
/*   private static boolean optionCircuits(){
	   boolean result;
	   
	   result = true; 
			   
			   
	   return result;
   }*/
   
   /**
    * checks whether only cross sex rings are allowed according to chosen options
    * @return true if only cross sex rings are allowed
    */
/*   private static boolean optionCrossSex(){
	   boolean result;
	   
	   result = true; 
	   
	   return result;
   }*/
   
   /**
    * checks whether chains between given pivotal pairs shall be counted, according to the search options in the census screen
    * @return true if chains between given pivotal pairs shall be counted
    */
/*   public static boolean optionEgoToAlter(){
	   return Puck.searchOption(11);
   }*/
   
   /**
    * 
   * checks whether only married individuals are admitted as pivots according to the search options in the census screen
   * @return true if only married pivots are admitted according to the search options
   */
/*   private static boolean optionMarriedOnly(){
	   boolean result;
	   
	   result = true;
	   
	   return result;
   }*/
   
   /**
    * checks whether matrimonial circuits are searched
    * @return if matrimonial circuits are searched
    */
/*   public static boolean optionMatrimonialCensus(){
	   return optionCircuits() && !optionEgoToAlter();
   }*/
   
   /**
    * checks whether the ego-alter-relation is symmetric
    * @return true if the ego-alter-relation is symmetric
    */
/*   private static boolean optionSymmetry(){
	   boolean result;
	   
	   result = true;
	   
	   //
	   return result;
   }*/



   
   /**
    * Renumbers the ChainGroups of the map
    */
/*   private void renumber () {
      int j = 1;
      for (Cluster<Chain> c : sorter) {
         c.setNumber(j);  //formerly setNr
         j++;
      }
   }*/

   //explicit ego and alter in cluster (restriction 6) is missing
   /**
    * returns the code for cluster anchorage  <p>
    * 0 no limitation, 1 at least one pivot in the cluster, 
    * 2 all pivots in the cluster, 3 the last married pivot in the cluster (for periodization),
    * 4 ego in cluster
    * @return the code of cluster anchorage
    */
/*   private int restriction(){
	   if (!(net instanceof PartNet)) return 0;
	   if (Puck.searchOption(6)) return 4;
	   if (Puck.searchOption(5)) return 3;
	   if (Puck.searchOption(4)) return 2;
	   if (optionEgoToAlter()) return 6;
	   return 1; 
   }*/

   /**
    * returns the required ring type 
    * <p> 0 all circuits, 1 only rings, 2 only minor rings, 3 only minimal rings
    * @return the ring type 
    */
/*   private int ringType(){
	   if (Puck.searchOption(9)) return 3;
	   if (Puck.searchOption(8)) return 2;
	   if (Puck.searchOption(1)) return 1;  
	   return 0;
   }*/
   
   /**
    * returns the kinship relation schema in positional notation
    * @param census true if the relation schema is to be taken from the census screen (for counting and storing of chains), false if it is to be taken from the stats screen (for counting only)
    * @return the kinship relation schema
    * @see RingGroupMap#initialize(Map, int)
    */
/*   private String schema(boolean census){
	   if (!census) return Puck.getStatsCommand(1);
	   String str = Puck.getCensusCommand(2);
//	   if (!census) return Puck.getText(4, 1);
//	   String str = Puck.getText (3, 2);
	   if (str==null || str.equals("") || str.charAt(0)=='-'|| str.charAt(0)=='+') return null;
	   return str;
   }*/
   

   
   //do not create space in case of dim==1, solve via NodeMap interface!
   /**
    * creates the ClosedNet corresponding to the Net 
    */
/*   private void setClosedNet(){
	   if (dim>1 || !matrim || Puck.reportOption(4)) closedNet = new ClosedNet(supernet,degMax(),line,nonlin(),marriedOnly);
	   else closedNet = new ClosedNet(supernet);
   }*/


   

   /**
    * sets the parameters of the kinship chain search according to chosen options
    */
/*   private void setParams(){
	   matrim = optionCircuits();
	   sib = sib();
	   line = line();
	   ringType = ringType();
	   restriction = restriction();
	   schema = schema(true);
	   marriedOnly = marriedOnly();
	   crossSex = crossSex();
	   egoToAlter = optionEgoToAlter();
	   sym = symmetry();
	   setBounds(schema);
   }*/
   
   /**
    * prints the parameters of the census (shortcut for code control)
    */
/*	private void printParams(){
		System.out.println(net.size()+" "+supernet.size()+" "+matrim+" "+sib+" "+line+" "+ringType+" "+restriction+" "+schema+" "+marriedOnly+" "+crossSex+" "+egoToAlter+" "+sym);
	}*/
	

   
   
   /**
    * determines the the maximal encompassing supernet (in case of a search restricted to a cluster)
    * <p> (restrictions concern only pivot anchorage; intermediate chains run through the global network)
    */
/*   private void setSuperNet(){
	   if (net instanceof PartNet) {
		   supernet = net.getFinalOrigin();
		   net.renumber(supernet);
	   } else supernet = net;
   }*/

   
   /**
    * returns the sibling mode (number of distinct sibling types)
    * <p> (1 all siblings assimilated, 2 no siblings assimilated, 3 full siblings assimilated)
    * @return the sibling mode
    */
/*   private int sib(){
	   return Puck.getCensusChoice(0)+1;
//	   return Puck.getIndex(3,0)+1;
   }*/


   
   //The simple partition (sorter)
   /**
    * creates a GroupNet with ChainGroups as vertices, and without any lines
    * <p> used for diagrammatical representation of census results
    * @return a lineless GroupNet with ChainGroups as vertices
    */
/*   public GroupNet toClassVertexSet () {
      return new GroupNet(this,false);
   }*/
   

   

   
   
   /**
    * transfers sibling mode, degree and order parameters from a simple RingGroupMap to the derived aggregate RingGroupMap
    * @param rn the underlying RingGroupMap
    * @since last modified 10-05-12
    */
/*   private void transferParams (RingGroupMap rn){
	   sib = rn.sib;
	   dim = rn.dim;
	   line = rn.line;
	   deg = rn.deg;
	   sym = rn.sym;
	   maxDeg = rn.maxDeg;
	   ringType = rn.ringType;
	   restriction = rn.restriction;
	   schema = rn.schema;

   }*/
   

   


   /**
    * gets the ego-alter relation label
    * @return the ego-alter relation label
    */
/*   public String getRelation() {
	   return relation;
   }*/





   
}
