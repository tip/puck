package org.tip.puck.census.workers;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author TIP
 */
public class CensusDetails extends ArrayList<CensusDetail> {

	private static final long serialVersionUID = 4957914015339797096L;

	/**
	 * 
	 */
	public CensusDetails() {
		super();
	}

	/**
	 * 
	 */
	public CensusDetails(final int capacity) {
		super(capacity);
	}

	/**
	 * 
	 */
	public CensusDetails(final int capacity, final int stuff) {
		super(capacity);

		add(new CensusDetail("LENGTH", false, false));
		add(new CensusDetail("DEPTH", false, false));
		add(new CensusDetail("ORDER", false, false));
		add(new CensusDetail("LINE", false, false));
		add(new CensusDetail("SKEW", false, false));
		add(new CensusDetail("DEGREE_ROM", false, false));
		add(new CensusDetail("DEGREE_GER", false, false));
		add(new CensusDetail("ENDS", false, false));
		add(new CensusDetail("HETERO", false, false));
		add(new CensusDetail("SWITCHES", false, false));
	}

	/**
	 * 
	 * @param label
	 * @param report
	 * @param diagram
	 * @return
	 */
	public CensusDetail add(final String label, final boolean report, final boolean diagram) {
		CensusDetail result;

		result = new CensusDetail(label, report, diagram);

		this.add(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getDiagramLabels() {
		List<String> result;

		result = new ArrayList<String>();

		for (CensusDetail detail : this) {
			if ((detail.isAvailable()) && (detail.isDiagram())) {
				result.add(detail.getLabel());
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getReportLabels() {
		List<String> result;

		result = new ArrayList<String>();

		for (CensusDetail detail : this) {
			if ((detail.isAvailable()) && (detail.isReport())) {
				result.add(detail.getLabel());
			}
		}

		//
		return result;
	}
}
