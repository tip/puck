package org.tip.puck.graphs.onemode;

import org.tip.puck.net.Net;
import org.tip.puck.report.Report;
import org.tip.puck.util.Chronometer;

/**
 * This class represents a reporter for generators.
 * 
 * @author TIP
 */
public class ShuffleReporter {

	/**
	 * Generates a report for shuffle P-Graph.
	 * 
	 * @param net
	 *            source to analyze.
	 * @param edgePermutations
	 *            Number of edge permutations per step.
	 * @param maximumGenerationalDistance
	 *            Maximum generational distance.
	 * @param minimumShufflePercentage
	 *            Minimum shuffle percentage (stop condition).
	 * @param minimumStableIterations
	 *            Minimum stable iterations (stop condition).
	 * 
	 * @return a report.
	 */
	public static Report reportReshuffling(final Net sourceNet, final ShuffleCriteria criteria) {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Shuffle One Mode Graph.");
		result.setOrigin("Shuffle reporter");
		result.setTarget(sourceNet.getLabel());

		//
		result.inputs().add("Edge permutations per step", criteria.getSwitchesPerIteration());
		result.inputs().add("Maximum generational distance", criteria.getMaxGenerationalDistance());
		result.inputs().add("Minimum shuffle percentage (stop condition)", criteria.getMinShufflePercentage());
		result.inputs().add("Minimum stable iterations (stop condition)", criteria.getMinStableIterations());
		result.inputs().add("Graph mode",criteria.getMode().toString());

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}
}
