package org.tip.puckgui.views;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.tip.puck.net.relations.roles.RoleRelationMaker.RoleRelationRule;
import org.tip.puck.net.relations.roles.RoleRelationRules;

/**
 * 
 * @author TIP
 */
public class RoleRelationRuleInputDialog extends JDialog {

	private static final long serialVersionUID = 2988885981449304451L;
	private RoleRelationRules dialogCriteria;
	private static RoleRelationRules lastCriteria = new RoleRelationRules();
	private JCheckBox chckbxChildChildSibling;
	private JCheckBox chckbxSiblingSiblingSibling;
	private JCheckBox chckbxSpouseChildParent;
	private JCheckBox chckbxChildSpouseChild;
	private JCheckBox chckbxSiblingParentChild;

	/**
	 * Create the dialog.
	 */
	public RoleRelationRuleInputDialog() {
		super();

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Role Relation Rules");
		setIconImage(Toolkit.getDefaultToolkit().getImage(RoleRelationRuleInputDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			/**
			 * 
			 */
			@Override
			public void windowClosing(final WindowEvent event) {
				// Closing window.
				// Cancel button.
				RoleRelationRuleInputDialog.this.dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 366, 235);

		getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		{
			this.chckbxChildChildSibling = new JCheckBox("Child's Sibling = Child");
			this.chckbxChildChildSibling.setSelected(true);
			getContentPane().add(this.chckbxChildChildSibling);
		}
		{
			this.chckbxSiblingSiblingSibling = new JCheckBox("Sibling's Sibling = Sibling");
			this.chckbxSiblingSiblingSibling.setSelected(true);
			getContentPane().add(this.chckbxSiblingSiblingSibling);
		}
		{
			this.chckbxSpouseChildParent = new JCheckBox("Child's Parent = Spouse");
			this.chckbxSpouseChildParent.setSelected(true);
			getContentPane().add(this.chckbxSpouseChildParent);
		}
		{
			this.chckbxChildSpouseChild = new JCheckBox("Spouse's Child = Child");
			this.chckbxChildSpouseChild.setSelected(true);
			getContentPane().add(this.chckbxChildSpouseChild);
		}
		{
			this.chckbxSiblingParentChild = new JCheckBox("Parent's Child = Sibling");
			getContentPane().add(this.chckbxSiblingParentChild);
		}
		{
			JLabel label = new JLabel("");
			getContentPane().add(label);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						RoleRelationRuleInputDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						RoleRelationRules criteria = getCriteria();
						//
						lastCriteria = criteria;
						RoleRelationRuleInputDialog.this.dialogCriteria = criteria;

						//
						setVisible(false);

					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}

		// ////////////////////////
		setCriteria(lastCriteria);
	}

	/**
	 * 
	 * @return
	 */
	public RoleRelationRules getCriteria() {
		RoleRelationRules result;

		result = new RoleRelationRules();

		if (RoleRelationRuleInputDialog.this.chckbxChildChildSibling.isSelected()) {
			result.add(RoleRelationRule.CHILD_CHILD_SIBLING);
		}

		if (RoleRelationRuleInputDialog.this.chckbxSiblingSiblingSibling.isSelected()) {
			result.add(RoleRelationRule.SIBLING_SIBLING_SIBLING);
		}

		if (RoleRelationRuleInputDialog.this.chckbxSpouseChildParent.isSelected()) {
			result.add(RoleRelationRule.SPOUSE_CHILD_PARENT);
		}

		if (RoleRelationRuleInputDialog.this.chckbxChildSpouseChild.isSelected()) {
			result.add(RoleRelationRule.CHILD_SPOUSE_CHILD);
		}

		if (RoleRelationRuleInputDialog.this.chckbxSiblingParentChild.isSelected()) {
			result.add(RoleRelationRule.SIBLING_PARENT_CHILD);
		}

		//

		//

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public RoleRelationRules getDialogCriteria() {
		RoleRelationRules result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final List<RoleRelationRule> source) {
		//

	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		RoleRelationRules criteria = showDialog();
	}

	/**
	 * Launch the application.
	 */
	public static RoleRelationRules showDialog() {
		RoleRelationRules result;

		//
		RoleRelationRuleInputDialog dialog = new RoleRelationRuleInputDialog();
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
