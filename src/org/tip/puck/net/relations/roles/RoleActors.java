package org.tip.puck.net.relations.roles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.Individual;
import org.tip.puck.net.relations.Roles;

/**
 * 
 * @author TIP
 */
public class RoleActors extends ArrayList<RoleActor> implements Comparable<RoleActors>{

	private static final long serialVersionUID = 2565759920581565424L;

	/**
	 * 
	 */
	public RoleActors() {
		super();
	}

	/**
	 * 
	 */
	public RoleActors(final RoleActors source) {
		super(source);
	}
	
	public RoleActors clone(){
		RoleActors result;
		
		result = new RoleActors();
		for (RoleActor actor : this){
			result.add(actor.clone());
		}		
		//
		return result;
	}
	
	public boolean addNew(final RoleActor actor){
		boolean result;
		
		if (contains(actor)){
			result = false;
		} else {
			result = add(actor);
		}
		//
		return result;
	}
	
	public boolean addNonRedundant(final RoleActor actor){
		boolean result;
		
		if (containsOrImplies(actor)){
			result = false;
		} else {
			result = add(actor);
		}
		//
		return result;
	}
	
	public boolean containsOrImplies (final RoleActor actor){
		boolean result;
		
		result = false;
		
		for (RoleActor otherActor : this){
			if (actor.specifies(otherActor)){
				result = true;
				break;
			}
		}
		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public RoleActor get(final int individualId, final MetaRole role) {
		RoleActor result;

		boolean ended = false;
		result = null;
		Iterator<RoleActor> iterator = this.iterator();
		while (!ended) {
			//
			if (iterator.hasNext()) {
				//
				RoleActor actor = iterator.next();

				if ((actor.getId() == individualId) && ((actor.getRole() == role))) {
					//
					ended = true;
					result = actor;
				}
			} else {
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public RoleActor get(final int individualId, final String roleName) {
		RoleActor result;

		boolean ended = false;
		result = null;
		Iterator<RoleActor> iterator = this.iterator();
		while (!ended) {
			//
			if (iterator.hasNext()) {
				//
				RoleActor actor = iterator.next();

				if ((actor.getId() == individualId) && (StringUtils.equals(actor.getRole().getName(), roleName))) {
					//
					ended = true;
					result = actor;
				}
			} else {
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public RoleActors getByIndividual(final Individual individual) {
		RoleActors result;

		result = new RoleActors();
		for (RoleActor actor : this) {
			if (actor.getIndividual().equals(individual)) {
				result.add(actor);
			}
		}

		//
		return result;
	}



	/**
	 * 
	 * @param id
	 * @return
	 */
	public RoleActors getById(final int id) {
		RoleActors result;

		result = new RoleActors();
		for (RoleActor actor : this) {
			if (actor.getId() == id) {
				result.add(actor);
			}
		}

		//
		return result;
	}
	
	public RoleActors getByImpliedAbsoluteRole(final MetaRole role) {
		RoleActors result;

		result = new RoleActors();
		for (RoleActor actor : this) {
			if (role.specifiesAbsolute(actor.getRole())) {
				result.add(actor);
			}
		}

		//
		return result;
	}

	

	/**
	 * 
	 * @param key
	 * @return
	 */
	public RoleActors getByRole(final MetaRole role) {
		RoleActors result;

		result = new RoleActors();
		for (RoleActor actor : this) {
			if (actor.getRole() == role) {
				result.add(actor);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public RoleActors getByRole(final String role) {
		RoleActors result;

		result = new RoleActors();
		for (RoleActor actor : this) {
			if (StringUtils.equals(actor.getRole().getName(), role)) {
				result.add(actor);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public RoleActors getOthers(final int id) {
		RoleActors result;

		result = new RoleActors();
		for (RoleActor actor : this) {
			if (actor.getId() != id) {
				result.add(actor);
			}
		}

		//
		return result;
	}

	public Roles getRoles(final int id) {
		Roles result;

		result = new Roles();
		for (RoleActor actor : this) {
			if (actor.getId() == id) {
				result.add(actor.getRole());
			}
		}
		//
		return result;
	}
	
	public Roles getRoles(){
		Roles result;

		result = new Roles();
		for (RoleActor actor : this) {
			if (!result.contains(actor.getRole())){
				result.add(actor.getRole());
			}
		}
		//
		return result;
	}
	
	public Roles getIndividuals(){
		Roles result;
		
		result = new Roles();
		for (RoleActor actor : this) {
			if (!result.contains(actor.getIndividual())){
				result.add(actor.getIndividual());
			}
		}
		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public boolean hasActor(final int id) {
		boolean result;

		boolean ended = false;
		result = false;
		Iterator<RoleActor> iterator = this.iterator();
		while (!ended) {
			if (iterator.hasNext()) {
				RoleActor actor = iterator.next();
				if (actor.getId() == id) {
					ended = true;
					result = true;
				}
			} else {
				ended = true;
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @param role
	 * @return
	 */
	public boolean hasActor(final int id, final String role) {
		boolean result;

		boolean ended = false;
		result = false;
		Iterator<RoleActor> iterator = this.iterator();
		while (!ended) {
			if (iterator.hasNext()) {
				RoleActor actor = iterator.next();
				if ((actor.getId() == id) && (StringUtils.equals(actor.getRole().getName(), role))) {
					ended = true;
					result = true;
				}
			} else {
				ended = true;
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public RoleActor[] toArray() {
		RoleActor[] result;

		result = new RoleActor[this.size()];
		int actorCount = 0;
		for (RoleActor actor : this) {
			//
			result[actorCount] = actor;
			actorCount += 1;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<RoleActor> toList() {
		List<RoleActor> result;

		result = new ArrayList<RoleActor>(this.size());
		for (RoleActor actor : this) {
			result.add(actor);
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<RoleActor> toSortedList() {
		List<RoleActor> result;

		result = toList();
		Collections.sort(result);

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<RoleActor> toSortedListReverse() {
		List<RoleActor> result;

		result = toList();
		Collections.sort(result,Collections.reverseOrder());

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<RoleActor> toListSortedByRoles() {
		List<RoleActor> result;

		result = toList();
		Collections.sort(result, new RoleActorComparator());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<RoleActor> toListSortedByDistances(RoleRelations relations) {
		List<RoleActor> result;

		result = toList();
		Collections.sort(result, new RoleActorComparator(relations));

		//
		return result;
	}
	

	public boolean containsAbsolute (RoleActor actor){
		boolean result;
		
		result = false;
		
		for (RoleActor otherActor : this){
			if (otherActor.matchesAbsolute(actor)){
				result = true;
				break;
			}
		}
		//
		return result;
	}
	
	public int compareTo(RoleActors others){
		int result;
		
		result = 0;
		
		List<RoleActor> sortedActors = toListSortedByRoles();
		List<RoleActor> sortedOthers = others.toListSortedByRoles();
		
		for (int i=0;i<size();i++){
			if (i==others.size()){
				result = 1;
				break;
			} else {
				result = new RoleActorComparator().compare(sortedActors.get(i),sortedOthers.get(i));
				if (result!=0){
					break;
				}
			}
		}
		
		if (result==0 && size() < others.size()){
			result = -1;
		}
		//
		return result;
	}
	
	public String toLetters(){
		String result;
		
		result = null;
		
		for (RoleActor actor : this){
			if (result==null){
				String symbol="";
				if (!actor.getEgoGender().isUnknown()){
					symbol+=actor.getEgoGender().toSymbol();
				}
				result=symbol+actor.getRole().toLetter();
			} else {
				result+=actor.getRole().toLetter();
			}
		}
		//
		return result;
	}
	
	public RoleActor getLast(){
		RoleActor result;
		
		if (isEmpty()){
			result = null;
		} else {
			result = get(size()-1);
		}
		//
		return result;
	}
	
	public RoleActor getFirst(){
		RoleActor result;
		
		if (isEmpty()){
			result = null;
		} else {
			result = get(0);
		}
		//
		return result;
	}
	



}
