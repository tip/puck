package org.tip.puckgui.views;


/**
 * 
 * @author TIP
 */
public class ReshufflingCriteria {
	private int numberOfRuns;
	private boolean extractRepresentative;

	public ReshufflingCriteria() {
		this.numberOfRuns = 100;
		this.extractRepresentative = false;
	}

	public int getNumberOfRuns() {
		return numberOfRuns;
	}

	public boolean isExtractRepresentative() {
		return extractRepresentative;
	}

	public void setExtractRepresentative(final boolean extractRepresentative) {
		this.extractRepresentative = extractRepresentative;
	}

	public void setNumberOfRuns(final int numberOfRuns) {
		this.numberOfRuns = numberOfRuns;
	}
}
