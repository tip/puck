package org.tip.puck.sequences.workers;

/**
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class MissingTestimoniesCriteria {

	private String relationModelName;
	private String egoRoleName;

	/**
	 * 
	 */
	public MissingTestimoniesCriteria() {
		//
		this.relationModelName = null;
		this.egoRoleName = null;
	}

	public String getEgoRoleName() {
		return this.egoRoleName;
	}

	public String getRelationModelName() {
		return this.relationModelName;
	}

	public void setEgoRoleName(final String egoRoleName) {
		this.egoRoleName = egoRoleName;
	}

	public void setRelationModelName(final String relationModelName) {
		this.relationModelName = relationModelName;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		result = String.format("[%s][%s]", this.relationModelName, this.egoRoleName);

		//
		return result;
	}

}
