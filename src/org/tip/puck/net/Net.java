package org.tip.puck.net;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.geo.Geography;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Actors;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.RelationModels;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.statistics.StatisticsWorker;
import org.tip.puck.util.NumberablesHashMap.IdStrategy;
import org.tip.puck.util.NumberedIntegers;

/**
 * 
 * @author TIP
 */
public class Net {

	private String label;
	private Attributes attributes;
	private Individuals individuals;
	private Families families;
	private IdStrategy defaultIdStrategy;
	private RelationModels relationModels;
	private Relations relations;
	private Geography geography;

	/**
	 * 
	 */
	public Net() {
		this.label = "";
		this.individuals = new Individuals();
		this.families = new Families();
		this.attributes = new Attributes();
		this.relationModels = new RelationModels();
		this.relations = new Relations();
		this.defaultIdStrategy = IdStrategy.APPEND;
	}

	/**
	 * 
	 */
	public Net(final Net source) {
		//
		this.label = source.getLabel();

		//
		this.attributes = new Attributes(source.attributes());

		//
		this.individuals = new Individuals(source.individuals().size());
		for (Individual sourceIndividual : source.individuals()) {

			//
			Individual targetIndividual = new Individual(sourceIndividual.getId());
			targetIndividual.setGender(sourceIndividual.getGender());
			targetIndividual.setName(sourceIndividual.getName());
			targetIndividual.setBirthOrder(sourceIndividual.getBirthOrder());
			targetIndividual.setOriginFamily(sourceIndividual.getOriginFamily());
			targetIndividual.getPersonalFamilies().add(sourceIndividual.getPersonalFamilies());
			targetIndividual.relations().add(sourceIndividual.relations());
			targetIndividual.attributes().addAll(sourceIndividual.attributes());

			//
			this.individuals.add(targetIndividual);
		}

		//
		this.families = new Families(source.families().size());
		for (Family sourceFamily : source.families()) {

			Family targetFamily = new Family(sourceFamily.getId());
			targetFamily.setHusband(sourceFamily.getHusband());
			targetFamily.setHusbandOrder(sourceFamily.getHusbandOrder());
			targetFamily.setWife(sourceFamily.getWife());
			targetFamily.setWifeOrder(sourceFamily.getWifeOrder());
			targetFamily.setUnionStatus(sourceFamily.getUnionStatus());
			targetFamily.getChildren().add(sourceFamily.getChildren());
			targetFamily.attributes().addAll(sourceFamily.attributes());

			this.families.add(targetFamily);
		}

		//
		this.relationModels = new RelationModels(source.relationModels());

		//
		this.relations = new Relations();
		for (Relation sourceRelation : source.relations()) {
			//
			Relation targetRelation = new Relation(sourceRelation.getId(), sourceRelation.getTypedId(), this.relationModels().getByName(
					sourceRelation.getModel().getName()), sourceRelation.getName());
			this.relations().add(targetRelation);

			//
			for (Actor sourceActor : sourceRelation.actors()) {
				Actor targetActor = new Actor(this.individuals().getById(sourceActor.getId()), targetRelation.getModel().roles()
						.getByName(sourceActor.getRole().getName()));
				targetActor.setRelationOrder(sourceActor.getRelationOrder());
				targetActor.attributes().addAll(sourceActor.attributes());
				targetRelation.actors().add(targetActor);
			}

			//
			targetRelation.attributes().addAll(sourceRelation.attributes());
		}
		
		this.geography = source.getGeography();

		// Fill individuals information.
		for (Individual individual : this.individuals) {
			//
			if (individual.getOriginFamily() != null) {
				individual.setOriginFamily(this.families.getById(individual.getOriginFamily().getId()));
			}

			//
			for (Family family : individual.getPersonalFamilies()) {
				individual.getPersonalFamilies().put(this.families.getById(family.getId()));
			}

			//
			for (Relation relation : individual.relations()) {
				individual.relations().put(this.relations().getById(relation.getId()));
			}
		}

		//
		for (Family family : this.families) {

			if (family.getHusband() != null) {
				family.setHusband(this.individuals.getById(family.getHusband().getId()));
			}
			if (family.getWife() != null) {
				family.setWife(this.individuals.getById(family.getWife().getId()));
			}
			for (Individual child : family.getChildren()) {
				family.getChildren().put(this.individuals.getById(child.getId()));
			}
		}
	}

	/**
	 * 
	 * @param family
	 * @param child
	 */
	public void addChild(final Family family, final Individual child) {
		//
		if ((family != null) && (child != null)) {
			//
			if (child.getOriginFamily() == null) {
				//
				family.getChildren().add(child);
				child.setOriginFamily(family);

			} else {
				//
				throw new IllegalArgumentException("Child parameter has already a family.");
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	public Net addRelationActors(final Relation relation, final Actor... actors) {
		Net result;

		if (relation == null) {
			//
			throw new IllegalArgumentException("relation is null.");

		} else if (this.relations.getById(relation.getId()) == null) {
			//
			throw new IllegalArgumentException("This relation is not in this net.");

		} else {
			//
			for (Actor actor : actors) {
				//
				relation.actors().add(actor);
				actor.getIndividual().relations().add(relation);
			}
		}

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void adjustIndividuals(final Individuals source) {
		//
		for (Individual individual : individuals()) {
			//
			individual.adjust(source.getById(individual.getId()));
		}
	}

	/**
	 * 
	 * @return
	 */
	public Attributes attributes() {
		return this.attributes;
	}

	/**
	 * 
	 * @param target
	 * @param targetId
	 */
	public void changeId(final Family target, final int targetId) {
		if ((target != null) && (targetId >= 0)) {
			this.families.removeById(target.getId());
			target.setId(targetId);
			this.families.add(target);
		}
	}

	/**
	 * 
	 * @param target
	 * @param targetId
	 */
	public void changeId(final Individual target, final int targetId) {
		if ((target != null) && (targetId >= 0)) {
			this.individuals.removeById(target.getId());
			target.setId(targetId);
			this.individuals.add(target);
		}
	}

	/**
	 * 
	 */
	public boolean contains(final int id) {
		boolean result;

		result = (get(id) != null);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Net copy() {
		Net result;

		result = copy(null);

		//
		return result;
	}

	/**
	 * 
	 * @param suffix
	 * @return
	 */
	public Net copy(final String suffix) {
		Net result;

		result = new Net(this);
		if (suffix != null) {
			result.setLabel(result.getLabel() + suffix);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param husband
	 * @return
	 */
	public Family createFamily(final IdStrategy idStrategy, final Individual husband, final Individual wife, final Individual... children) {
		Family result;

		//
		result = new Family(this.families.nextFreeId(idStrategy), husband, wife);
		this.families.add(result);

		//
		if (husband != null) {
			husband.getPersonalFamilies().add(result);
		}

		//
		if (wife != null) {
			wife.getPersonalFamilies().add(result);
		}

		//
		for (Individual child : children) {
			result.getChildren().put(child);
			child.setOriginFamily(result);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param husband
	 * @return
	 */
	public Family createFamily(final IdStrategy idStrategy, final Individual husband, final Individual wife, final Individuals children) {
		Family result;

		//
		result = new Family(this.families.nextFreeId(idStrategy), husband, wife);
		this.families.add(result);

		//
		if (husband != null) {
			husband.getPersonalFamilies().add(result);
		}

		//
		if (wife != null) {
			wife.getPersonalFamilies().add(result);
		}

		//
		for (Individual child : children) {
			result.getChildren().put(child);
			child.setOriginFamily(result);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param husband
	 * @return
	 */
	public Family createFamily(final Individual husband, final Individual wife, final Individual... children) {
		Family result;

		result = createFamily(this.defaultIdStrategy, husband, wife, children);

		//
		return result;
	}

	/**
	 * 
	 * @param husband
	 * @return
	 */
	public Family createFamily(final Individual husband, final Individual wife, final Individuals children) {
		Family result;

		result = createFamily(this.defaultIdStrategy, husband, wife, children);

		//
		return result;
	}

	/**
	 * 
	 * @param husband
	 * @return
	 * @throws PuckException
	 */
	public Family createFamily(final int id) throws PuckException {
		Family result;

		result = createFamily(id, null, null, UnionStatus.UNMARRIED);

		//
		return result;
	}

	/**
	 * 
	 * @param husband
	 * @return
	 * @throws PuckException
	 */
	public Family createFamily(final int id, final Individual husband, final Individual wife, final UnionStatus unionStatus) throws PuckException {
		Family result;

		result = this.families.getById(id);
		if (result == null) {
			result = new Family(id, husband, wife);
			result.setUnionStatus(unionStatus);
			this.families.add(result);
			if (husband != null) {
				husband.addPersonalFamily(result);
			}
			if (wife != null) {
				wife.addPersonalFamily(result);
			}
		} else {
			throw PuckExceptions.INVALID_PARAMETER.create("Family with id [" + id + "] is already existing.");
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individual createIndividual() {
		Individual result;

		result = createIndividual("", Gender.UNKNOWN);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individual createIndividual(final IdStrategy idStrategy, final String name, final Gender gender) {
		Individual result;

		result = new Individual(this.individuals().nextFreeId(idStrategy), name, gender);
		this.individuals.add(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public Individual createIndividual(final int id) throws PuckException {
		Individual result;

		result = createIndividual(id, "", Gender.UNKNOWN);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public Individual createIndividual(final int id, final String name, final Gender gender) throws PuckException {
		Individual result;

		result = this.individuals.getById(id);
		if (result == null) {
			result = new Individual(id, name, gender);
			this.individuals.add(result);
		} else {
			throw PuckExceptions.INVALID_PARAMETER.create("Individual with id [" + id + "] is already existing.");
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individual createIndividual(final String name, final Gender gender) {
		Individual result;

		result = createIndividual(this.defaultIdStrategy, name, gender);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Relation createRelation(final int typedId, final String name, final RelationModel model) {
		Relation result;

		result = new Relation(this.relations().nextFreeId(IdStrategy.FILL), typedId, model, name);
		this.relations.add(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Relation createRelation(final String name, final RelationModel model, final Actor... actors) {
		Relation result;

		result = new Relation(this.relations().nextFreeId(IdStrategy.FILL), this.relations.getByModel(model).nextFreeTypedId(IdStrategy.FILL), model, name);

		if (name == null) {
			//
			result.setName("R" + result.getTypedId());
		}

		for (Actor actor : actors) {
			//
			result.actors().add(actor);
			actor.getIndividual().relations().add(result);
		}

		this.relations.add(result);

		//
		return result;
	}
	

	/**
	 * 
	 * @return
	 */
	public Relation createRelation(final String name, final RelationModel model, final Actors actors) {
		Relation result;

		result = new Relation(this.relations().nextFreeId(IdStrategy.FILL), this.relations.getByModel(model).nextFreeTypedId(IdStrategy.FILL), model, name);

		if (name == null) {
			//
			result.setName("R" + result.getTypedId());
		}

		for (Actor actor : actors) {
			//
			result.actors().add(actor);
			actor.getIndividual().relations().add(result);
		}

		this.relations.add(result);

		//
		return result;
	}

	/**
	 * 
	 * @param relation
	 * @param individualId
	 * @param roleName
	 * @return
	 * @throws PuckException
	 */
	public Actor createRelationActor(final Relation relation, final int individualId, final String roleName) throws PuckException {
		Actor result;

		if (relation == null) {
			throw PuckExceptions.INVALID_PARAMETER.create("Invalid null relation.");
		} else if (individualId < 1) {
			throw PuckExceptions.INVALID_PARAMETER.create("Invalid individual id " + individualId + " for role " + roleName + " in relation " + relation);
		} else if (StringUtils.isBlank(roleName)) {
			throw PuckExceptions.INVALID_PARAMETER.create("Invalid null role name.");
		} else {
			//
			Role role = relation.getModel().roles().getByName(roleName);
			if (role == null) {
				throw PuckExceptions.INVALID_PARAMETER.create("Unknown role [" + roleName + "].");
			} else {
				//
				Individual individual = this.individuals().getById(individualId);
				if (individual == null) {
					individual = new Individual(individualId);
					this.individuals.put(individual);
				}

				result = relation.actors().get(individualId, role);
				if (result == null) {
					//
					result = new Actor(individual, role);

					//
					relation.actors().add(result);
					individual.relations().add(relation);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param relation
	 * @param individualId
	 * @param roleName
	 * @return
	 * @throws PuckException
	 */
	public Actor createRelationActorForced(final Relation relation, final int individualId, final String roleName) throws PuckException {
		Actor result;

		if (relation == null) {
			throw PuckExceptions.INVALID_PARAMETER.create("Invalid null relation.");
		} else if (individualId < 1) {
			throw PuckExceptions.INVALID_PARAMETER.create("Invalid individual id.");
		} else if (StringUtils.isBlank(roleName)) {
			throw PuckExceptions.INVALID_PARAMETER.create("Invalid null role name.");
		} else {
			//
			Role role = relation.getModel().roles().getByName(roleName);
			if (role == null) {
				throw PuckExceptions.INVALID_PARAMETER.create("Unknown role [" + roleName + "].");
			} else {
				//
				Individual individual = this.individuals().getById(individualId);
				if (individual == null) {
					individual = new Individual(individualId);
					this.individuals.put(individual);
				}

				result = new Actor(individual, role);

				relation.actors().add(result);
				individual.relations().add(relation);

				//
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public RelationModel createRelationModel(final RelationModel source) throws PuckException {
		RelationModel result;

		if (source == null) {
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter.");
		} else if (this.relationModels.getByName(source.getName()) != null) {
			throw PuckExceptions.INVALID_PARAMETER.create("Relation model name already in use.");
		} else {
			//
			result = new RelationModel(source);

			//
			this.relationModels.add(result);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public RelationModel createRelationModel(final String name) throws PuckException {
		RelationModel result;

		// Check.
		if (this.relationModels.getByName(name) != null) {
			throw PuckExceptions.INVALID_PARAMETER.create("Relation model name already in use.");
		} else {
			// Create.
			result = new RelationModel(name);
			this.relationModels.add(result);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Role createRelationRole(final RelationModel model, final String roleName) {
		Role result;

		if ((model == null) || (StringUtils.isBlank(roleName))) {
			result = null;
		} else {
			result = model.roles().getByName(roleName);
			if (result == null) {
				result = new Role(roleName);
				model.roles().add(result);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int depth() {
		int result;

		result = StatisticsWorker.depth(this);

		//
		return result;
	}

	/**
	 * 
	 */
	public Families families() {
		Families result;

		result = this.families;

		//
		return result;
	}


	/**
	 * 
	 */
	public Individual get(final int id) {
		Individual result;

		result = this.individuals.getById(id);

		//
		return result;
	}

	public Individual getCloneWithAttributes(final Individual individual) {
		Individual result;

		result = get(individual.getId());
		if (result == null) {
			result = individual.cloneWithAttributes();
			this.individuals.put(result);
		}
		//
		return result;
	}

	public IdStrategy getDefaultIdStrategy() {
		return this.defaultIdStrategy;
	}

	public Relations getFamilyEvents(final Family family) throws PuckException {
		Relations result;

		result = new Relations();

		RelationModel model = this.relationModels.getByName("FAMEVENT");
		if (model == null) {
			model = new RelationModel("FAMEVENT");
			model.roles().add(new Role("SPOUSE", 0));
			model.roles().add(new Role("PARENT", 0));
			model.roles().add(new Role("CHILD", 0));
			model.roles().add(new Role("SIBLING", 0));
			model.roles().add(new Role("EGO", 0));
			createRelationModel(model);
		}

		String marriageDate = family.getAttributeValue("MARR_DATE");
		String divorceDate = family.getAttributeValue("DIV_DATE");

		if (marriageDate != null) {
			Relation marriage = createRelation("Marriage " + family.getHusband() + " - " + family.getWife(), model);
			marriage.setAttribute("DATE", marriageDate);
			if (family.getHusband() != null) {
				createRelationActor(marriage, family.getHusband().getId(), "SPOUSE");
			}
			if (family.getWife() != null) {
				createRelationActor(marriage, family.getWife().getId(), "SPOUSE");
			}
		}
		if (divorceDate != null) {
			Relation divorce = createRelation("Divorce " + family.getHusband() + " - " + family.getWife(), model);
			divorce.setAttribute("DATE", divorceDate);
			if (family.getHusband() != null) {
				createRelationActor(divorce, family.getHusband().getId(), "SPOUSE");
			}
			if (family.getWife() != null) {
				createRelationActor(divorce, family.getWife().getId(), "SPOUSE");
			}
		}
		for (Individual individual : family.getChildren()) {
			Integer birtYear = IndividualValuator.getBirthYear(individual);
			String birtPlace = individual.getAttributeValue("BIRT_PLAC");
			if (birtYear != null) {
				Relation birth = createRelation("Birth " + individual.getName(), model);
				birth.setAttribute("DATE", individual.getAttributeValue("BIRT_DATE"));
				for (Individual relative : family.getMembers()) {
					if (IndividualValuator.aliveAtYear(relative, birtYear)) {
						createRelationActor(birth, relative.getId(), family.getRoleName(relative, individual));
					}
				}
				if (birtPlace != null) {
					birth.setAttribute("PLACE", birtPlace);
					birth.setName(birtPlace + " " + birth.getName());
				}
			}
		}
		for (Individual individual : family.getMembers()) {
			Integer deatYear = IndividualValuator.getDeathYear(individual);
			if (deatYear != null) {
				Relation death = createRelation("Death " + individual.getName(), model);
				death.setAttribute("DATE", individual.getAttributeValue("DEAT_DATE"));
				for (Individual relative : family.getMembers()) {
					if (IndividualValuator.aliveAtYear(relative, deatYear)) {
						if (individual != relative || individual.relations().getByName(death.getName()).size() == 0) {
							createRelationActor(death, relative.getId(), family.getRoleName(relative, individual));
						}
					}
				}
			}
		}

		//
		return result;
	}

	public Relations getFamilyEvents(final Segmentation segmentation) throws PuckException {
		Relations result;

		result = new Relations();

		Families families = new Families();
		families.add(segmentation.getCurrentFamilies());
		for (Individual individual : segmentation.getCurrentIndividuals()) {
			families.add(individual.getOriginFamily());
		}

		for (Family family : families) {
			result.add(getFamilyEvents(family));
		}

		//
		return result;
	}

	public Geography getGeography() {
		return this.geography;
	}

	/**
	 * 
	 */
	public String getLabel() {
		String result;

		result = this.label;

		//
		return result;
	}

	/**
	 * @param individuals
	 * @return
	 */
	public List<String> getRelationLabels() {
		List<String> result;

		//
		result = new ArrayList<String>();

		//
		for (Relation relation : this.relations) {
			result.add(relation.getName());
		}

		//
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * @param individuals
	 * @return
	 */
	public List<String> getRelationModelLabels() {
		List<String> result;

		//
		result = new ArrayList<String>();

		//
		for (RelationModel model : this.relationModels) {
			result.add(model.getName());
		}

		//
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public NumberedIntegers heightData() {
		NumberedIntegers result;

		result = new NumberedIntegers(this.individuals.size());
		for (Individual individual : this.individuals) {
			if (result.get(individual.getId()) == null) {
				result.put(individual.getId(), individual.height(result));
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public Individuals individuals() {
		Individuals result;

		result = this.individuals;

		//
		return result;
	}

	/**
	 * 
	 */
	public RelationModels relationModels() {
		RelationModels result;

		result = this.relationModels;

		//
		return result;
	}

	/**
	 * 
	 */
	public Relations relations() {
		Relations result;

		result = this.relations;

		//
		return result;
	}

	/**
	 * FIXME TO BE CHECKED
	 * 
	 * @param source
	 */
	public void remove(final Family source) {

		if (source != null) {
			//
			Individual husband = source.getHusband();
			if (husband != null) {
				husband.getPersonalFamilies().removeById(source.getId());
				source.setHusband(null);
			}

			//
			Individual wife = source.getWife();
			if (wife != null) {
				wife.getPersonalFamilies().removeById(source.getId());
				source.setWife(null);
			}

			//
			for (Individual child : source.getChildren()) {
				child.setOriginFamily(null);
			}
			source.getChildren().clear();

			//
			this.families().removeById(source.getId());
		}
	}

	/**
	 * FIXME TO BE CHECKED
	 * 
	 * @param source
	 */
	public void remove(final Individual source) {
		//
		if (source != null) {
			// Remove reference in origin family.
			Family originFamily = source.getOriginFamily();
			if (originFamily != null) {
				originFamily.getChildren().removeById(source.getId());
				source.setOriginFamily(null);
			}

			//
			for (Family family : source.getPersonalFamilies()) {

				//
				if (family.getHusband() == source) {
					family.setHusband(null);
				} else if (family.getWife() == source) {
					family.setWife(null);
				}

				//
				if (family.isEmpty()) {
					remove(family);
				}
			}
			source.getPersonalFamilies().clear();

			//
			for (Relation relation : source.relations()) {
				relation.removeActor(source.getId());
			}
			source.relations().clear();

			//
			this.individuals().removeById(source.getId());
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void remove(final Relation source) {
		if (source != null) {
			//
			for (Actor actor : source.actors().toList()) {
				actor.getIndividual().relations().remove(source);
			}

			//
			this.relations.removeById(source.getId());
		}
	}

	/**
	 * 
	 * @param source
	 */
	public boolean remove(final RelationModel source) {
		boolean result;
		
		result = false;
		
		if (source != null) {

			//
			Relations relations = this.relations.getByModel(source);
			if (relations.isEmpty()) {
				result = this.relationModels.remove(source);
			} else {
				System.err.println("Non-empty relations: "+relations);
			}
		}
		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void remove(final Relations source) {
		if (source != null) {
			//
			for (Relation relation : source) {
				remove(relation);
			}
		}
	}

	/**
	 * Set orphan, an individual.
	 * 
	 * @param child
	 */
	public void removeChild(final Individual child) {
		if ((child != null) && (child.getOriginFamily() != null)) {
			child.getOriginFamily().getChildren().removeById(child.getId());
			child.setOriginFamily(null);
		}
	}

	public void removeFamilyEvents() {

		remove(relations().getByModelName("FAMEVENT"));
		remove(relationModels().getByName("FAMEVENT"));
	}

	/**
	 * 
	 * @param relation
	 * @param actor
	 * @return
	 */
	public void removeRelationActor(final Relation relation, final Actor actor) {
		relation.actors().remove(actor);
		actor.getIndividual().relations().remove(relation);
	}

	/**
	 * 
	 * @param model
	 * @param role
	 */
	public void removeRelationRole(final RelationModel model, final Role role) {
		this.relations.removeRelationRole(model, role);
	}

	/**
	 * 
	 * @param relation
	 * @param currentRole
	 * @param newIndividual
	 */
	public void replaceIndividualInActor(final Relation relation, final Actor actor, final Individual newIndividual) {
		if ((relation != null) && (actor != null) && (newIndividual != null)) {
			// Note old individual.
			Individual oldIndividual = actor.getIndividual();

			// replace individual in actor.
			actor.setIndividual(newIndividual);

			//
			if (!relation.hasActor(oldIndividual)) {
				oldIndividual.relations().remove(relation);
			}

			//
			if (!newIndividual.relations().contains(relation)) {
				newIndividual.relations().add(relation);
			}
		}
	}

	public void setDefaultIdStrategy(final IdStrategy idStrategy) {
		this.defaultIdStrategy = idStrategy;
	}


	public void setGeography(final Geography geography) {
		this.geography = geography;
	}

	/**
	 * 
	 * @param label
	 */
	public void setLabel(final String label) {
		this.label = label;
	}

	/**
	 * 
	 */
	public int size() {
		int result;

		result = this.individuals().size();

		//
		return result;
	}

}
