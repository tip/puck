package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.workers.ControlReporter;
import org.tip.puck.net.workers.ControlReporter.ControlType;
import org.tip.puck.report.Report;
import org.tip.puckgui.NetGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class ControlInputWindow extends JFrame {
	
	private static final long serialVersionUID = -6817947282978329451L;
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.tip.puckgui.messages"); //$NON-NLS-1$
	private static final Logger logger = LoggerFactory.getLogger(ControlInputWindow.class);

	private JFrame thisJFrame;
	private JPanel contentPane;
	private JCheckBox chckbxSameSexSpouses;
	private JCheckBox chckbxFemaleFathersOrMaleMothers;
	private JCheckBox chckbxCyclicDescentCases;
	private JCheckBox chckbxUnknownSexPersons;
	private JCheckBox chckbxNamelessPersons;
	private JCheckBox chckbxParentChildMarriages;
	private JCheckBox chckbxMultipleParents;
	private JCheckBox chckbxAutomarriages;
	private JCheckBox chckbxInconsistentDates;
	private JCheckBox chckbxMissingDates;
	private JCheckBox chckbxUnknownSexParentsSpouses;
	private JCheckBox chckbxMissingDatesCompact;

	/**
	 * Create the frame.
	 */
	public ControlInputWindow(final NetGUI netGUI) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(FooReporterInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		setTitle(BUNDLE.getString("ControlReporterInputWindow.this.title")); //$NON-NLS-1$
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 395, 439);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel inputPanel = new JPanel();
		this.contentPane.add(inputPanel, BorderLayout.CENTER);
		inputPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));

		this.chckbxSameSexSpouses = new JCheckBox(BUNDLE.getString("ControlReporterInputWindow.chckbxSameSexSpouses.text")); //$NON-NLS-1$
		this.chckbxSameSexSpouses.setSelected(true);
		inputPanel.add(this.chckbxSameSexSpouses, "4, 2");

		this.chckbxFemaleFathersOrMaleMothers = new JCheckBox(BUNDLE.getString("ControlReporterInputWndow.chckbxFemalFathersOrMaleMothers")); //$NON-NLS-1$
		this.chckbxFemaleFathersOrMaleMothers.setSelected(true);
		inputPanel.add(this.chckbxFemaleFathersOrMaleMothers, "4, 4");

		this.chckbxMultipleParents = new JCheckBox(BUNDLE.getString("ControlInputWindow.chckbxMultipleFathersOr.text")); //$NON-NLS-1$
		this.chckbxMultipleParents.setSelected(true);
		inputPanel.add(this.chckbxMultipleParents, "4, 6");

		this.chckbxCyclicDescentCases = new JCheckBox(BUNDLE.getString("ControlReporterInputWindow.chckbxCyclicDescentCases.text")); //$NON-NLS-1$
		this.chckbxCyclicDescentCases.setSelected(true);
		inputPanel.add(this.chckbxCyclicDescentCases, "4, 8");

		this.chckbxUnknownSexPersons = new JCheckBox(BUNDLE.getString("ControlReporterInputWindow.chckbxUnknownSexPersons.text")); //$NON-NLS-1$
		this.chckbxUnknownSexPersons.setSelected(true);
		inputPanel.add(this.chckbxUnknownSexPersons, "4, 10");

		this.chckbxNamelessPersons = new JCheckBox(BUNDLE.getString("ControlReporterInputWindow.chckbxNamelessPersons.text")); //$NON-NLS-1$
		this.chckbxNamelessPersons.setSelected(true);
		inputPanel.add(this.chckbxNamelessPersons, "4, 14");

		this.chckbxParentChildMarriages = new JCheckBox(BUNDLE.getString("ControlReporterInputWindow.chckbxParentChildMarriages.text")); //$NON-NLS-1$
		this.chckbxParentChildMarriages.setSelected(true);
		inputPanel.add(this.chckbxParentChildMarriages, "4, 16");

		this.chckbxAutomarriages = new JCheckBox(BUNDLE.getString("ControlInputWindow.chckbxAutomarriages.text")); //$NON-NLS-1$
		this.chckbxAutomarriages.setSelected(true);
		inputPanel.add(this.chckbxAutomarriages, "4, 18");

		this.chckbxInconsistentDates = new JCheckBox(BUNDLE.getString("ControlInputWindow.chckbxInconsistentDates.text")); //$NON-NLS-1$
		inputPanel.add(this.chckbxInconsistentDates, "4, 20");

		this.chckbxMissingDates = new JCheckBox(BUNDLE.getString("ControlInputWindow.chckbxMissingDates.text")); //$NON-NLS-1$
		inputPanel.add(this.chckbxMissingDates, "4, 22");

		this.chckbxUnknownSexParentsSpouses = new JCheckBox(BUNDLE.getString("ControlInputWindow.chckbxSameparentsex.text")); //$NON-NLS-1$
		this.chckbxUnknownSexParentsSpouses.setSelected(true);
		inputPanel.add(this.chckbxUnknownSexParentsSpouses, "4, 12");

		this.chckbxMissingDatesCompact = new JCheckBox(BUNDLE.getString("ControlInputWindow.chckbxMissingDatescompact.text_1")); //$NON-NLS-1$
		inputPanel.add(this.chckbxMissingDatesCompact, "4, 24");

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton(BUNDLE.getString("ControlReporterInputWindow.btnCancel.text")); //$NON-NLS-1$
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton(BUNDLE.getString("ControlReporterInputWindow.btnLaunch.text")); //$NON-NLS-1$
		getRootPane().setDefaultButton(btnLaunch);
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					List<ControlType> controlTypes = new ArrayList<ControlType>();
					if (ControlInputWindow.this.chckbxCyclicDescentCases.isSelected()) {
						controlTypes.add(ControlType.CYCLIC_DESCENT_CASES);
					}

					if (ControlInputWindow.this.chckbxFemaleFathersOrMaleMothers.isSelected()) {
						controlTypes.add(ControlType.FEMALE_FATHERS_OR_MALE_MOTHERS);
					}

					if (ControlInputWindow.this.chckbxMultipleParents.isSelected()) {
						controlTypes.add(ControlType.MULTIPLE_FATHERS_OR_MOTHERS);
					}

					if (ControlInputWindow.this.chckbxNamelessPersons.isSelected()) {
						controlTypes.add(ControlType.NAMELESS_PERSONS);
					}

					if (ControlInputWindow.this.chckbxParentChildMarriages.isSelected()) {
						controlTypes.add(ControlType.PARENT_CHILD_MARRIAGES);
					}

					if (ControlInputWindow.this.chckbxAutomarriages.isSelected()) {
						controlTypes.add(ControlType.AUTO_MARRIAGE);
					}

					if (ControlInputWindow.this.chckbxSameSexSpouses.isSelected()) {
						controlTypes.add(ControlType.SAME_SEX_SPOUSES);
					}

					if (ControlInputWindow.this.chckbxUnknownSexPersons.isSelected()) {
						controlTypes.add(ControlType.UNKNOWN_SEX_PERSONS);
					}

					if (ControlInputWindow.this.chckbxUnknownSexParentsSpouses.isSelected()) {
						controlTypes.add(ControlType.UNKNOWN_SEX_PARENTS_SPOUSES);
					}

					if (ControlInputWindow.this.chckbxInconsistentDates.isSelected()) {
						controlTypes.add(ControlType.INCONSISTENT_DATES);
					}

					if (ControlInputWindow.this.chckbxMissingDates.isSelected()) {
						controlTypes.add(ControlType.MISSING_DATES);
					}

					if (ControlInputWindow.this.chckbxMissingDatesCompact.isSelected()) {
						controlTypes.add(ControlType.MISSING_DATES_COMPACT);
					}

					ControlType[] controls = new ControlType[controlTypes.size()];
					controlTypes.toArray(controls);
					Report report = ControlReporter.reportControls(netGUI.getSegmentation(), BUNDLE, controls);

					if (report.status() == 0) {
						//
						String title = "Control special features report";
						String message = "No special features found.";

						//
						JOptionPane.showMessageDialog(ControlInputWindow.this.thisJFrame, message, title, JOptionPane.INFORMATION_MESSAGE);
					} else {
						//
						netGUI.addReportTab(report);
					}

					dispose();

				} catch (final Exception exception) {
					//
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(ControlInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);
	}
}
