package org.tip.puck.net.relations.workers;

import java.util.List;

import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.report.Report;
import org.tip.puck.util.Chronometer;

/**
 * 
 */
public class RelationModelReporter {

	public enum ControlType {
		GENERAL_CHECK
	}

	/**
	 * 
	 */
	private RelationModelReporter() {
	}

	/**
	 * 
	 * @param model
	 * @param controlType
	 * @return
	 */
	public static Report reportControl(final RelationModel model, final ControlType controlType) {
		Report result;

		switch (controlType) {
			case GENERAL_CHECK:
				result = reportGeneral(model);
			break;

			default:
				result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static Report reportControls(final RelationModel model) {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		Report report = reportControls(model, ControlType.values());

		result = new Report("Problems");
		result.outputs().appendln("Relation Model " + model.getName());
		result.outputs().appendln();
		result.outputs().append(report.outputs());

		if (report.status() == 0) {
			result.outputs().appendln("No problems found.");
		}

		//
		result.setStatus(report.status());
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static Report reportControls(final RelationModel model, final ControlType... controlTypes) {
		Report result;

		//
		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Checks for possible undefinedRoles (special features).");
		result.setOrigin("Relation Model Reporter");
		result.setTarget(model.getName());

		//
		for (ControlType type : controlTypes) {
			result.inputs().add(type.toString(), "true");
		}

		//
		int problemCount = 0;
		for (ControlType controlType : controlTypes) {
			Report report = reportControl(model, controlType);
			if ((report != null) && (report.status() != 0)) {
				problemCount += report.status();
				result.outputs().append(report.outputs());
				result.outputs().appendln();
			}
		}

		//
		result.setStatus(problemCount);
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param locale
	 * @param controls
	 * @return
	 */
	public static Report reportControls(final RelationModel model, final List<ControlType> controls) {
		Report result;

		result = reportControls(model, controls.toArray(new ControlType[0]));

		//
		return result;
	}

	/**
	 * 
	 * @param maxIterations
	 * @return
	 */
	public static Report reportGeneral(final RelationModel model) {
		Report result;

		Chronometer chrono = new Chronometer();

		result = new Report("General");

		result.outputs().appendln("General control of " + model.getName());
		result.outputs().appendln();
		result.setStatus(0);

		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

}
