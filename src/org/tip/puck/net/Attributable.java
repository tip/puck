package org.tip.puck.net;

public interface Attributable {
	
	String getAttributeValue(String label);

}
