package org.tip.puck.report;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author TIP
 */
public class DataLines {
	private String legend;
	private List<DataLine> lines;

	/**
	 * 
	 */
	public DataLines() {
		this.lines = new ArrayList<DataLine>();
	}

	/**
	 * 
	 * @return
	 */
	public String getLegend() {
		return legend;
	}

	/**
	 * 
	 */
	public DataLine getLine(final int rowIndex) {
		DataLine result;

		if (rowIndex >= this.lines.size()) {
			this.lines.add(rowIndex, new DataLine());
		}

		result = this.lines.get(rowIndex);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int getLineCount() {
		int result;

		result = this.lines.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int getMaxSize() {
		int result;

		result = 0;
		for (DataLine line : this.lines) {
			if (line.size() > result) {
				result = line.size();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param legend
	 */
	public void setLegend(final String legend) {
		this.legend = legend;
	}
}
