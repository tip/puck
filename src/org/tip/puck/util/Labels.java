package org.tip.puck.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author TIP
 */
public class Labels {

	private String name;
	private List<String> strings;

	/**
	 * 
	 */
	public Labels(final String name) {
		this.name = name;
		strings = new ArrayList<String>();
	}

	/**
	 * 
	 * @param value
	 */
	void add(final String value) {
		if (value != null) {
			this.strings.add(value);
		}
	}

	/**
	 * 
	 * @param value
	 */
	public void addAll(final List<String> value) {
		if (value != null) {
			this.strings.addAll(value);
		}
	}

	/**
	 * 
	 * @param value
	 */
	public void addAtTheBeginning(final String value) {
		if (value != null) {
			if (this.strings.size() == 0) {
				this.strings.add(value);
			} else {
				this.strings.add(0, value);
			}
		}
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * 
	 */
	public void sort() {
		Collections.sort(this.strings);
	}

	public List<String> strings() {
		return strings;
	}

	/**
	 * 
	 * @return
	 */
	public Object[] toArray() {
		Object[] result;

		result = this.strings.toArray();

		//
		return result;
	}
}
