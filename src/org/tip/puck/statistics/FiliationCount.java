package org.tip.puck.statistics;

/**
 * 
 * @author TIP
 */
public class FiliationCount {

	private double agnatic;
	private double uterine;
	private double cognatic;

	/**
	 * 
	 */
	public FiliationCount() {
	}

	/**
	 * 
	 */
	public FiliationCount(final double agnaticValue, final double uterineValue, final double cognaticValue) {
		set(agnaticValue, uterineValue, cognaticValue);
	}

	/**
	 * 
	 * @return
	 */
	public FiliationCount add(final FiliationCount source) {
		FiliationCount result;

		this.agnatic += source.getAgnatic();
		this.uterine += source.getUterine();
		this.cognatic += source.getCognatic();

		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double addAgnatic(final double value) {
		double result;

		this.agnatic += value;
		result = this.agnatic;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double addCognatic(final double value) {
		double result;

		this.cognatic += value;
		result = this.cognatic;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double addUterine(final double value) {
		double result;

		this.uterine += value;
		result = this.uterine;

		//
		return result;
	}

	public double getAgnatic() {
		return agnatic;
	}

	public double getCognatic() {
		return cognatic;
	}

	public double getUterine() {
		return uterine;
	}
	
	/**
	 * 
	 * @return
	 */
	public double incAgnatic() {
		double result;

		this.agnatic += 1;
		result = this.agnatic;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double incCognatic() {
		double result;

		this.cognatic += 1;
		result = this.cognatic;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double incUterine() {
		double result;

		this.uterine += 1;
		result = this.uterine;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isPositive() {
		boolean result;

		if ((this.agnatic > 0) || (this.uterine > 0) || (this.cognatic > 0)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isZero() {
		boolean result;

		if ((this.agnatic == 0) && (this.uterine == 0) && (this.cognatic == 0)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param agnaticValue
	 * @param uterineValue
	 * @param cognaticValue
	 * @return
	 */
	public FiliationCount set(final double agnaticValue, final double uterineValue, final double cognaticValue) {
		FiliationCount result;

		this.agnatic = agnaticValue;
		this.uterine = uterineValue;
		this.cognatic = cognaticValue;

		result = this;

		//
		return result;
	}

	public void setAgnatic(final double agnaticValue) {
		this.agnatic = agnaticValue;
	}

	public void setCognatic(final double cognaticValue) {
		this.cognatic = cognaticValue;
	}

	public void setUterine(final double uterineValue) {
		this.uterine = uterineValue;
	}

}
