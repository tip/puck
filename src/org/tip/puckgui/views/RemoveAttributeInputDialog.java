package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.workers.AttributeWorker.EmptyType;
import org.tip.puck.net.workers.AttributeWorker.Scope;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class RemoveAttributeInputDialog extends JDialog {

	private static final long serialVersionUID = -3335511828858070221L;
	private final JPanel contentPanel = new JPanel();
	private RemoveAttributeCriteria dialogCriteria;
	private static RemoveAttributeCriteria lastCriteria = new RemoveAttributeCriteria();
	private final ButtonGroup buttonGroupType = new ButtonGroup();
	private JComboBox cmbbxTarget;
	private JComboBox cmbbxLabel;
	private JRadioButton rdbtnVoid;
	private JRadioButton rdbtnBlanked;
	private JRadioButton rdbtnForcedBlank;

	/**
	 * Create the dialog.
	 */
	public RemoveAttributeInputDialog(final List<String> relationModelNames, final List<String> attributeNames) {
		super();

		//
		List<String> targetLabels = new ArrayList<String>();
		targetLabels.add("ALL");
		targetLabels.add("CORPUS");
		targetLabels.add("INDIVIDUALS");
		targetLabels.add("FAMILIES");
		targetLabels.add("RELATIONS");
		if (relationModelNames != null) {
			for (String name : relationModelNames) {
				targetLabels.add(name);
			}
		}

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Remove Attribute Input");
		setIconImage(Toolkit.getDefaultToolkit().getImage(RemoveAttributeInputDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 366, 235);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));
		{
			JLabel lblTarget = new JLabel("Target:");
			contentPanel.add(lblTarget, "2, 2, right, default");
		}
		{
			cmbbxTarget = new JComboBox(targetLabels.toArray());
			contentPanel.add(cmbbxTarget, "4, 2, fill, default");
		}
		{
			JLabel lblLabel = new JLabel("Label:");
			contentPanel.add(lblLabel, "2, 4, right, default");
		}
		{
			List<String> names;
			if (attributeNames == null) {
				names = new ArrayList<String>();
			} else {
				names = attributeNames;
			}
			cmbbxLabel = new JComboBox(names.toArray());
			cmbbxLabel.setEditable(true);
			contentPanel.add(cmbbxLabel, "4, 4, fill, default");
		}
		{
			JLabel lblType = new JLabel("Type:");
			contentPanel.add(lblType, "2, 6, right, default");
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, "4, 6, fill, fill");
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			{
				rdbtnVoid = new JRadioButton("removed");
				buttonGroupType.add(rdbtnVoid);
				rdbtnVoid.setSelected(true);
				panel.add(rdbtnVoid);
			}
			{
				rdbtnBlanked = new JRadioButton("replace by an empty string");
				buttonGroupType.add(rdbtnBlanked);
				panel.add(rdbtnBlanked);
			}
			{
				rdbtnForcedBlank = new JRadioButton("set to an empty string");
				buttonGroupType.add(rdbtnForcedBlank);
				panel.add(rdbtnForcedBlank);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						RemoveAttributeCriteria criteria = getCriteria();
						if (StringUtils.isBlank(criteria.getLabel())) {
							//
							String title = "Invalid input";
							String message = "Please, enter a label not empty.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						} else {
							//
							lastCriteria = criteria;
							dialogCriteria = criteria;

							//
							setVisible(false);
						}

					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}

		// ////////////////////////
		setCriteria(lastCriteria);
	}

	/**
	 * 
	 * @return
	 */
	public RemoveAttributeCriteria getCriteria() {
		RemoveAttributeCriteria result;

		result = new RemoveAttributeCriteria();

		//
		Scope scope;
		String optionalName;
		switch (cmbbxTarget.getSelectedIndex()) {
			case 0:
				scope = Scope.ALL;
				optionalName = null;
			break;

			case 1:
				scope = Scope.CORPUS;
				optionalName = null;
			break;

			case 2:
				scope = Scope.INDIVIDUALS;
				optionalName = null;
			break;

			case 3:
				scope = Scope.FAMILIES;
				optionalName = null;
			break;

			case 4:
				scope = Scope.RELATIONS;
				optionalName = null;
			break;

			default:
				scope = Scope.RELATION;
				optionalName = (String) cmbbxTarget.getSelectedItem();
			break;
		}
		result.setScope(scope);
		result.setOptionalRelationName(optionalName);

		//
		result.setLabel((String) cmbbxLabel.getSelectedItem());

		//
		if (rdbtnVoid.isSelected()) {
			result.setType(EmptyType.VOID);
		} else if (rdbtnBlanked.isSelected()) {
			result.setType(EmptyType.BLANK);
		} else {
			result.setType(EmptyType.FORCED_BLANK);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public RemoveAttributeCriteria getDialogCriteria() {
		RemoveAttributeCriteria result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final RemoveAttributeCriteria source) {
		//
		if (source != null) {
			switch (source.getScope()) {
				case ALL:
					cmbbxTarget.setSelectedIndex(0);
				break;

				case CORPUS:
					cmbbxTarget.setSelectedIndex(1);
				break;

				case INDIVIDUALS:
					cmbbxTarget.setSelectedIndex(2);
				break;

				case FAMILIES:
					cmbbxTarget.setSelectedIndex(3);
				break;

				case RELATIONS:
					cmbbxTarget.setSelectedIndex(4);
				break;

				default:
					cmbbxTarget.setSelectedItem(source.getOptionalRelationName());
				break;
			}

			//
			cmbbxLabel.setSelectedItem(source.getLabel());

			//
			switch (source.getType()) {
				case VOID:
					rdbtnVoid.setSelected(true);
					rdbtnBlanked.setSelected(false);
					rdbtnForcedBlank.setSelected(false);
				break;

				case BLANK:
					rdbtnVoid.setSelected(false);
					rdbtnBlanked.setSelected(true);
					rdbtnForcedBlank.setSelected(false);
				break;

				case FORCED_BLANK:
					rdbtnVoid.setSelected(false);
					rdbtnBlanked.setSelected(false);
					rdbtnForcedBlank.setSelected(true);
				break;
			}
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		RemoveAttributeCriteria criteria = showDialog(null, null);
	}

	/**
	 * Launch the application.
	 */
	public static RemoveAttributeCriteria showDialog(final List<String> relationModelNames, final List<String> attributeNames) {
		RemoveAttributeCriteria result;

		//
		RemoveAttributeInputDialog dialog = new RemoveAttributeInputDialog(relationModelNames, attributeNames);
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
