package oldcore.trash;

import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;



import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;


public class PartitionMaker {
	
	Net net;
	Map<Individual,Object> values;
	
/*	public PartitionMaker(Net net){
		this.net = net;
		values = new TreeMap<Individual,Object>();
	}*/
	
	/**
	 * 
	 */
/*	public Partition<Individual> makePartition(String label){
		Partition<Individual> result = null;
		
		if (label.equals("GEN")) {
			result = makeGenerations();
		} else if (label.equals("PATRIC")) {
			result = makeAncestors(FiliationType.AGNATIC);
		} else if (label.equals("MATRIC")) {
			result = makeAncestors(FiliationType.UTERINE);
		} else if (label.equals("PATRID")) {
			result = makeDepths(FiliationType.AGNATIC);
		} else if (label.equals("MATRID")) {
			result = makeDepths(FiliationType.UTERINE);
		} else if (label.equals("DEPTH")) {
			result = makeDepths(FiliationType.COGNATIC);
		} else if (label.equals("ORD")){
//			result = rankChildren();
		}
		
		return result;
		
	}*/
	



	   /**
	    * organizes the Net into generational levels
	    * <p> sets the values of the GEN partition
	    * <p> implementation of the Pajek Algorithm for generational layers 
	    */
	public Partition<Individual> makeGenerations(){
		
		Partition<Individual> result = new Partition<Individual>(values);
		
		   int n = net.size()/2;
		   int[] m = {n};
		   for (Individual v : net.individuals()){
			   if ((Integer)(values.get(v))==0) setGeneration(values,v,m,n);
		   }
		   n = m[0];
		   for (Individual v: net.individuals()){
			   int d = (Integer)values.get(v);
			   if (d>0) values.put(v, d-n+1);
		   }
		
		   //
		return result;
	}

	   
	/**
	 * 
	 * @param type
	 * @deprecated
	 */
/*	public Partition<Individual> makeDepths(final FiliationType type){
		
		Partition<Individual> result = new Partition<Individual>(values);

		for (Individual individual : net.individuals()) {
			if (values.get(individual.getId()) == null) {
				depth(individual,type);
			}
		}
		
		return result;
	}*/
	
	/**
	 * 
	 * @param type
	 * @deprecated
	 */
/*	public Partition<Individual> makeAncestors(final FiliationType type){
		
		Partition<Individual> result = new Partition<Individual>(values);

		for (Individual individual : net.individuals()) {
			if (values.get(individual.getId()) == null) {
				ancestor(individual,type);
			}
		}
		
		return result;
	}*/
	
	/**
	 * gets the most remote ancestor
	 */
/*	public Individual ancestor (Individual ego, final FiliationType type){
		Individual result;
		
		if (ego.getOriginFamily() == null) {
			result = ego;
		} else {
			switch (type){
			case AGNATIC: {
				Individual father = ego.getFather();
				if (father==null) {
					result = ego;
				} else {
					result = ancestor(father, type);
				}
			}
			case UTERINE: {
				Individual mother = ego.getMother();
				if (mother==null) {
					result = ego;
				} else {
					result = ancestor(mother, type);
				}
			}
			default: result = ego;
			}
		}
		
		//put dans le cache !! 
		return result;
		
	}	*/
	
	/**
	 * Gets the maximal generational depth of an individual.
	 * 
	 * @param the
	 *            filter criterion for ascendant lines (0 agnatic, 1 uterine, 2
	 *            cognatic)
	 * @return the maximal generational depth
	 */
/*	public int depth(Individual ego, final FiliationType type) {
		int result;

		if (ego.getOriginFamily() == null) {
			result = 0;
		} else {
			switch (type){
			case AGNATIC: {
				Individual father = ego.getFather();
				if (father==null) {
					result = 0;
				} else {
					result = depth(father, type) + 1;
				}
			}
			case UTERINE: {
				Individual mother = ego.getMother();
				if (mother==null) {
					result = 0;
				} else {
					result = depth(mother, type) + 1;
				}
			}
			case COGNATIC: {
				Individual father = ego.getFather();
				Individual mother = ego.getMother();
				if (father==null && mother==null){
					result = 0;
				} 
				else if (father==null) {
					result = depth(mother, type) + 1;
				} 
				else if (mother==null) {
					result = depth(father, type) + 1;
				} else {
					result = Math.max(depth(mother, type), depth(father, type)) + 1;
				}
			}
			default: result = 0;
			}
		}

		values.put(ego, result);

		//
		return result;
	}*/
	
	
	
	

	/**
	 * sets the generational depth of the vertex
	 * @param m the minimal generational level (as a pseudo-array with only one field)
	 * @param n the default level
	 */
	public void setGeneration (Map<Individual,Object> generations, Individual ego, int[]m, int n){
		Stack<Individual> stack = new Stack<Individual>();
		generations.put(ego, n);
		stack.push(ego);
		while (!stack.isEmpty()){
			setGeneration(generations, stack.pop(), m, stack);
		}
	}
	
	/**
	 * sets the generational depth of the neighbors of the vertex
	 * @param m the minimal generational level (as a pseudo-array with only one field)
	 * @param stack the stack of neighbors still to be examined
	 * @since last modification 10-05-14, 10-11-26 KH
	 */
	public void setGeneration (Map<Individual,Object> generations, Individual ego, int[] m, Stack<Individual> stack){
//		int constraint = 1; //change this index according to the constraint chosen
//		int nstep=0;
		int d = (Integer)generations.get(ego);
		int[] kn = {0,-1,1};
		for (int k:kn){
			if (ego.getKin(k)==null) continue;
			for (Individual alter : ego.getKin(k)){
				if (alter==null) continue;
				if ((Integer)(generations.get(alter))!=0) continue;
//				if (postponeGenerationAssignment(nstep,k,alter,constraint)) continue; 
//				nstep++;
				int e = d-k;
				generations.put(alter,e);
				if (m[0]>e) m[0]=e;
				stack.push(alter);
			}
		}
	}	

}
