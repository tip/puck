package org.tip.puck.util;

public class Distributions {
    public static double normal(double x, double mean, double stdev) {
        double y = 1.0 / (stdev * Math.sqrt(2.0 * Math.PI));
        double exp = (x - mean) / stdev;
        exp *= exp;
        exp /= -2;
        y *= Math.exp(exp);
        return y;
    }
}