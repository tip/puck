package org.tip.puck.util;


import java.util.Random;


/**
 * Singleton to encapsulate a random number generator.
 * 
 * All stochastic processes should depend on this class,
 * so that we have control over the random number generator seed.
 * 
 * @author Telmo Menezes (telmo@telmomenezes.com)
 */
public class RandomGenerator {

	private static RandomGenerator _instance = null;
	public Random random;
	
	private RandomGenerator()
	{
		random = new Random();
	}
	
	public static RandomGenerator instance()
	{
		if (_instance == null)
			_instance = new RandomGenerator();

		return _instance;
	}
}
