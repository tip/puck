package org.tip.puck.io.selz;

import java.io.File;
import java.security.InvalidParameterException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.net.Net;

/**
 * This class represents a TXT File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class SELZFile {

	private static final Logger logger = LoggerFactory.getLogger(SELZFile.class);

	/**
	 * Loads a BAR file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File source, final String charsetName) throws PuckException {
		Net result;

		//
		if (source == null) {
			//
			throw new InvalidParameterException("Null parameter");

		} else {
			//
			String fileName = source.getName().toLowerCase();

			//
			if (fileName.endsWith(".ods")) {
				//
				result = SELZODSFile.load(source);

			} else if (fileName.endsWith(".txt")) {
				//
				result = SELZTXTFile.load(source, charsetName);

			} else if (fileName.endsWith(".xls")) {
				//
				result = SELZXLSFile.load(source);

			} else {
				//
				throw new InvalidParameterException("Unknown extension.");
			}
		}

		//
		return result;
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File target, final Net source) throws PuckException {
		//
		if (source == null) {
			//
			throw new InvalidParameterException("Null parameter");

		} else {
			//
			String fileName = target.getName().toLowerCase();

			//
			if (fileName.endsWith(".ods")) {
				//
				SELZODSFile.save(target, source);

			} else if (fileName.endsWith(".txt")) {
				//
				SELZTXTFile.save(target, source);

			} else if (fileName.endsWith(".xls")) {
				//
				SELZXLSFile.save(target, source);

			} else {
				//
				throw new InvalidParameterException("Unknown extension.");
			}
		}
	}
}
