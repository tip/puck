package org.tip.puck.sequences;

import java.util.List;

import org.tip.puck.net.Populatable;
import org.tip.puck.util.Numberable;
import org.tip.puck.util.Numberables;

public interface Sequenceables<S extends Sequenceable<E>,E> extends Numberables<S>, Populatable {

	List<S> toSortedList();
	
	List<E> getStations(Ordinal time);
	
	boolean isPopulatable();
	

	
}
