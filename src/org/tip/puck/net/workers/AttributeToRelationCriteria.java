package org.tip.puck.net.workers;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.workers.AttributeWorker.CaseOption;

/**
 * 
 * @author TIP
 */
public class AttributeToRelationCriteria {

	private AttributeWorker.Scope scope;
	private String targetRelationName;
	private String sourceRelationName;
	private String label;
	private String roleName;
	private String eponymRoleName;
	private String dateSeparator;
	private AttributeWorker.CaseOption caseOption;

	/**
	 * 
	 */
	public AttributeToRelationCriteria() {
		this.scope = AttributeWorker.Scope.NONE;
		this.targetRelationName = null;
		this.sourceRelationName = null;
		this.label = null;
		this.roleName = null;
		this.caseOption = AttributeWorker.CaseOption.CASE_SENSITIVE;
	}

	public AttributeWorker.CaseOption getCaseOption() {
		return this.caseOption;
	}

	public String getLabel() {
		return this.label;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public String getTargetRelationName() {
		return this.targetRelationName;
	}

	public AttributeWorker.Scope getScope() {
		return this.scope;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isCaseSensitive() {
		boolean result;

		if (this.caseOption == CaseOption.CASE_SENSITIVE) {
			//
			result = true;

		} else {
			//
			result = false;
		}

		//
		return result;
	}

	public void setCaseOption(final AttributeWorker.CaseOption caseOption) {
		this.caseOption = caseOption;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public void setRoleName(final String newLabel) {
		this.roleName = newLabel;
	}

	public void setTargetRelationName(final String optionalRelationName) {
		this.targetRelationName = optionalRelationName;
	}

	public void setScope(final AttributeWorker.Scope scope) {
		this.scope = scope;
	}
	
	

	public String getEponymRoleName() {
		return eponymRoleName;
	}

	public void setEponymRoleName(String eponymRoleName) {
		this.eponymRoleName = eponymRoleName;
	}

	public String getDateSeparator() {
		return dateSeparator;
	}

	public void setDateSeparator(String dateSeparator) {
		this.dateSeparator = dateSeparator;
	}

	public String getSourceRelationName() {
		return sourceRelationName;
	}

	public void setSourceRelationName(String sourceRelationName) {
		this.sourceRelationName = sourceRelationName;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public static boolean isNotValid(final AttributeToRelationCriteria criteria) {
		boolean result;

		result = !isValid(criteria);

		//
		return result;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public static boolean isValid(final AttributeToRelationCriteria criteria) {
		boolean result;

		if ((criteria == null) || (criteria.getScope() == null) || (criteria.getScope() == AttributeWorker.Scope.NONE) || (criteria.getLabel() == null)
				|| (StringUtils.isBlank(criteria.getRoleName()))) {
			//
			result = false;

		} else {
			//
			result = true;
		}

		//
		return result;
	}

}
