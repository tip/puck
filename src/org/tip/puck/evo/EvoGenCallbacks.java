package org.tip.puck.evo;


/**
 * @author Telmo Menezes
 *
 */
public interface EvoGenCallbacks {
    public Generator baseGenerator();
    public double computeFitness(Generator gen);
	public void onNewBest(EvoGen evo);
    public void onGeneration(EvoGen evo);
	public String infoString();
}
