package org.tip.puck.report;

import java.io.File;

/**
 * 
 * @author TIP
 */
public class ReportRawData {

	private String title;
	private String format;
	private String extension;
	private File defaultFile;
	private String data;

	/**
	 * 
	 * @param title
	 * @param format
	 * @param extension
	 */
	public ReportRawData(final String title, final String format, final String extension, final File defaultFile) {
		this.title = title;
		this.format = format;
		this.extension = extension;
		this.defaultFile = defaultFile;
	}

	public String getData() {
		return data;
	}

	public File getDefaultFile() {
		return defaultFile;
	}

	public String getExtension() {
		return extension;
	}

	public String getFormat() {
		return format;
	}

	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @return
	 */
	public long getLength() {
		long result;

		if (this.data == null) {
			result = 0;
		} else {
			result = this.data.length();
		}

		//
		return result;
	}

	public void setData(final String data) {
		this.data = data;
	}
	
	public void appendData(final String data) {
		this.data = this.data + data;
	}


	public void setDefaultFile(final File defaultFile) {
		this.defaultFile = defaultFile;
	}

	public void setExtension(final String extension) {
		this.extension = extension;
	}

	public void setFormat(final String format) {
		this.format = format;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

}
