package org.tip.puck.net.workers;

import java.util.Comparator;

import org.tip.puck.util.MathUtils;

/**
 * 
 * @author TIP
 * 
 */
public class AttributeTypeComparator implements Comparator<AttributeType> {

	public enum Sorting {
		DEFAULT,
		SCOPE,
		RELATION_MODEL_NAME
	}

	private Sorting sorting;

	/**
	 * 
	 * @param sorting
	 */
	public AttributeTypeComparator(final Sorting sorting) {
		this.sorting = sorting;
	}

	/**
	 * 
	 */
	@Override
	public int compare(final AttributeType alpha, final AttributeType bravo) {
		int result;

		result = compare(alpha, bravo, this.sorting);

		//
		return result;
	}

	/**
	 * 
	 */
	public static int compare(final AttributeType alpha, final AttributeType bravo, final Sorting sorting) {
		int result;

		//
		switch (sorting) {
			case DEFAULT:
			default:
				//
				result = compare(alpha, bravo, Sorting.SCOPE);
				if (result == 0) {
					result = compare(alpha, bravo, Sorting.RELATION_MODEL_NAME);
				}
			break;

			case SCOPE:
				//
				result = MathUtils.compare(getScopeOrder(alpha), getScopeOrder(bravo));
			break;

			case RELATION_MODEL_NAME:
				//
				result = compare(getRelationModelname(alpha), getRelationModelname(bravo));
			break;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param alpha
	 * @param bravo
	 * @return
	 */
	public static int compare(final String alpha, final String bravo) {
		int result;

		if (alpha == null) {
			result = -1;
		} else {
			result = alpha.compareTo(bravo);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String getRelationModelname(final AttributeType source) {
		String result;

		//
		if (source == null) {
			//
			result = null;
		} else {
			//
			result = source.getRelationModelName();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Integer getScopeOrder(final AttributeType source) {
		Integer result;

		//
		if (source == null) {
			//
			result = null;

		} else {
			//
			result = source.getScope().ordinal();
		}

		//
		return result;
	}
}
