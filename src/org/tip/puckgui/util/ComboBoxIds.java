package org.tip.puckgui.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.util.NumberablesHashMap.IdStrategy;

/**
 * 
 */
public class ComboBoxIds {
	private static ComboBoxIds instance = null;
	private List<String> items;

	/**
	 * 
	 */
	private ComboBoxIds() {

		this.items = null;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> items() {
		List<String> result;

		result = this.items;

		//
		return result;
	}

	/**
	 * 
	 * @param individuals
	 */
	public void update(final List<Individual> individuals) {

		if (individuals == null) {
			//
			this.items = new ArrayList<String>(0);

		} else {
			//
			this.items = new ArrayList<String>(individuals.size());
			for (Individual individual : individuals) {
				//
				this.items.add(individual.getId() + " " + individual.getName());
			}
		}
	}

	/**
	 * 
	 * @param combo
	 */
	public static String extractId(final String input) {
		String result;

		if (input == null) {
			result = null;
		} else {
			boolean ended = false;
			int index = 0;
			while (!ended) {
				if (index < input.length()) {
					if (Character.isDigit(input.charAt(index))) {
						index += 1;
					} else {
						ended = true;
					}
				} else {
					ended = true;
				}
			}

			//
			result = input.substring(0, index);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param combo
	 */
	public static String extractName(final String input) {
		String result;

		if (input == null) {
			result = null;
		} else {
			boolean ended = false;
			int index = 0;
			while (!ended) {
				if (index < input.length()) {
					if (Character.isDigit(input.charAt(index))) {
						index += 1;
					} else {
						ended = true;
					}
				} else {
					ended = true;
				}
			}

			//
			result = input.substring(index);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param input
	 * @return
	 */
	public static Individual getIndividualFromInput(final Net net, final Object value) {
		Individual result;

		//
		String input;
		if (value == null) {
			input = null;
		} else if (value instanceof String) {
			input = (String) value;
		} else {
			input = value.toString();
		}

		//
		String newIndividualIdInput = ComboBoxIds.extractId(input);
		if (StringUtils.isBlank(newIndividualIdInput)) {
			result = null;
		} else {
			int newIndividualId = Integer.parseInt(newIndividualIdInput);
			result = net.individuals().getById(newIndividualId);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param input
	 * @return
	 */
	public static Individual getOrCreateIndividualFromInput(final IdStrategy idStrategy, final Net net, final Object value, final Gender gender) {
		Individual result;

		//
		String input;
		if (value == null) {
			input = null;
		} else if (value instanceof String) {
			input = (String) value;
		} else {
			input = value.toString();
		}

		//
		String newIndividualIdInput = ComboBoxIds.extractId(input);
		if (StringUtils.isBlank(newIndividualIdInput)) {
			String newIndividualNameInput = ComboBoxIds.extractName(input);
			if ((StringUtils.isBlank(newIndividualNameInput)) || (StringUtils.equals(newIndividualNameInput, "---"))) {
				result = null;
			} else {
				// Create new individual from name.
				result = net.createIndividual(idStrategy, newIndividualNameInput, gender);
			}
		} else {
			int newIndividualId = Integer.parseInt(newIndividualIdInput);
			result = net.individuals().getById(newIndividualId);
			if (result == null) {
				// Create new individual from id.
				result = new Individual(newIndividualId, "?", gender);
				net.individuals().put(result);

				//
				String newIndividualNameInput = ComboBoxIds.extractName(input);
				if (StringUtils.isNotBlank(newIndividualNameInput)) {
					result.setName(newIndividualNameInput);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param input
	 * @return
	 */
	public static Individual getOrCreateIndividualFromInput(final Net net, final Object value, final Gender gender) {
		Individual result;

		result = getOrCreateIndividualFromInput(net.getDefaultIdStrategy(), net, value, gender);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static ComboBoxIds instance() {
		ComboBoxIds result;

		if (instance == null) {
			//
			instance = new ComboBoxIds();
		}

		result = instance;

		//
		return result;
	}

}
