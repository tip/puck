package org.tip.puckgui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.sequences.workers.SequenceCriteria;
import org.tip.puck.util.ToolBox;

/**
 * This class represents a File reader and writer for preferences.
 * 
 * Note: in file, first entry is the older.
 * 
 * @author TIP
 */
public class PreferencesFile {

	private static final Logger logger = LoggerFactory.getLogger(PreferencesFile.class);

	/**
	 * 
	 * @param namespace
	 * @param bean
	 * @return
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	public static Map<String, String> convertObjectToProperties(final String namespace, final Object bean) throws IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		Map<String, String> result;

		//
		BeanUtilsBean bub = new BeanUtilsBean(new ConvertUtilsBean() {
			@Override
			public String convert(final Object value) {
				String result;

				if (value == null) {
					//
					result = "";

				} else if (StringUtils.equals(value.getClass().getName(), "java.util.ArrayList") && (((ArrayList) value).isEmpty())) {
					//
					result = "";

				} else {
					//
					result = super.convert(value);

					if (result == null) {
						result = "";
					}
				}

				//
				return result;
			}
		});

		//
		Map<?, ?> properties = bub.describe(bean);

		//
		result = new HashMap<String, String>();
		for (Object key : properties.keySet()) {
			result.put(namespace + "." + (String) key, (String) properties.get(key));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param namespace
	 * @return
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	public static void convertPropertiesToObject(final String namespace, final Object bean, final Properties sourceProperties) throws IllegalAccessException,
			InvocationTargetException {
		//
		BeanUtilsBean bub = new BeanUtilsBean(new ConvertUtilsBean() {
			@Override
			public Object convert(final String value, final Class clazz) {
				Object result;

				if (clazz.isEnum()) {
					//
					result = Enum.valueOf(clazz, value);

				} else {
					//
					result = super.convert(value, clazz);
				}

				//
				return result;
			}
		});

		//
		Map<String, String> beanProperties = new HashMap<String, String>();
		String namespacePattern = namespace + ".";
		for (Object key : sourceProperties.keySet()) {
			if (((String) key).startsWith(namespacePattern)) {
				beanProperties.put(((String) key).substring(namespacePattern.length()), (String) sourceProperties.get(key));
			}
		}

		//
		bub.populate(bean, beanProperties);
	}

	/**
	 * 
	 * @param namespace
	 * @param source
	 * @return
	 */
	public static Map<String, String> convertSpaceTimeAnalysisCriteria(final String namespace, final SequenceCriteria source) {
		Map<String, String> result;

		result = new HashMap<String, String>();

		//
		if (source.getEgoRoleName() != null) {
			result.put(namespace + ".egoRoleName", source.getEgoRoleName());
		}
		if (source.getLevel() != null) {
			result.put(namespace + ".level", source.getLevel().name());
		}
		if (source.getDateLabel() != null) {
			result.put(namespace + ".dateLabel", source.getDateLabel());
		}
		if (source.getDefaultReferentRoleName() != null) {
			result.put(namespace + ".defaultReferentRoleName", source.getDefaultReferentRoleName());
		}
		if (source.getPattern() != null) {
			result.put(namespace + ".pattern", source.getPattern());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param namespace
	 * @param target
	 */
	public static void convertToSpaceTimeAnalysisCriteria(final Properties source, final String namespace, final SequenceCriteria target) {
		//
		String egoRoleName = source.getProperty(namespace + ".egoRoleName");
		if (egoRoleName != null) {
			target.setEgoRoleName(egoRoleName);
		}

		//
		try {
			GeoLevel level = GeoLevel.valueOf(source.getProperty(namespace + ".level"));
			if (level != null) {
				target.setLevel(level);
			}
		} catch (Exception exception) {
			logger.error("Bad value for {}{}", namespace, ".level");
		}

		//
		String dateLabel = source.getProperty(namespace + ".dateLabel");
		if (dateLabel != null) {
			target.setDateLabel(dateLabel);
		}

		//
		String defaultReferentRoleName = source.getProperty(namespace + ".defaultReferentRoleName");
		if (defaultReferentRoleName != null) {
			target.setDefaultReferentRoleName(defaultReferentRoleName);
		}

		//
		String pattern = source.getProperty(namespace + ".pattern");
		if (pattern != null) {
			target.setPattern(pattern);
		}
	}

	/**
	 * Loads.
	 * 
	 * @param file
	 *            file to load.
	 * 
	 * @return the loaded data.
	 * 
	 * @throws PuckException
	 */
	public static Preferences load(final File file) throws PuckException {
		Preferences result;

		try {
			//
			result = new Preferences();

			//
			Properties properties = new Properties();
			properties.load(new FileInputStream(file));

			//
			String languageLabel = properties.getProperty("language");
			if (languageLabel != null) {
				Language language = Language.valueOf(languageLabel);
				if (language == null) {
					logger.warn("Unknown language in preference file.");
				} else {
					result.setLanguage(language);
				}
			}

			//
			String autoLoadLastFileValue = (String) properties.get("autoloadlastfile");
			if (autoLoadLastFileValue != null) {
				result.setAutoLoadLastFile(Boolean.parseBoolean(autoLoadLastFileValue));
			}
			convertPropertiesToObject("inputSettings", result.getInputSettings(), properties);

			//
			for (int detailIndex = 1; detailIndex <= 10; detailIndex++) {
				String rootLabel = "census.details." + detailIndex;

				if (properties.get(rootLabel + ".label") != null) {
					result.getCensusCriteria().getCensusDetails().get(detailIndex - 1).setLabel((String) properties.get(rootLabel + ".label"));
				}
				if (properties.get(rootLabel + ".report") != null) {
					result.getCensusCriteria().getCensusDetails().get(detailIndex - 1)
							.setReport(Boolean.parseBoolean((String) properties.get(rootLabel + ".report")));
				}
				if (properties.get(rootLabel + ".diagram") != null) {
					result.getCensusCriteria().getCensusDetails().get(detailIndex - 1)
							.setDiagram(Boolean.parseBoolean((String) properties.get(rootLabel + ".diagram")));
				}
			}

			//
			convertPropertiesToObject("census", result.getCensusCriteria(), properties);
			convertPropertiesToObject("differentialCensus", result.getDifferentialCensusCriteria(), properties);
			convertPropertiesToObject("agentSimulation", result.getAgentSimulationCriteria(), properties);
			convertPropertiesToObject("virtualFieldwork", result.getVirtualFieldworkCriteria(), properties);
			convertPropertiesToObject("agentSimulationVariations", result.getAgentSimulationVariationsCriteria(), properties);
			convertPropertiesToObject("virtualFieldworkVariations", result.getVirtualFieldworkVariationsCriteria(), properties);
			convertPropertiesToObject("randomPermutation", result.getRandomPermutationCriteria(), properties);
			convertPropertiesToObject("reshuffling", result.getReshufflingCriteria(), properties);
			convertPropertiesToObject("randomDistribution", result.getRandomDistributionCriteria(), properties);
			convertPropertiesToObject("generateRules", result.getGenerateRulesCriteria(), properties);
			convertPropertiesToObject("randomCorpusMas", result.getRandomCorpusMASCriteria(), properties);

			//
			convertToSpaceTimeAnalysisCriteria(properties, "spaceTimeAnalysisCriteria", result.getSpaceTimeAnalysisCriteria());

			//
			result.setFlatDB4GeoNamesDirectory((String) properties.get("flatdb4geonames.database"));

			//
			{
				String value = (String) properties.get("terminology.selfname");
				if (!StringUtils.isBlank(value)) {
					result.setTerminologySelfName(value);
				}

				value = (String) properties.get("terminology.maxiterations");
				if (NumberUtils.isDigits(value)) {
					result.setTerminologyMaxIterations(Integer.parseInt(value));
				}

				value = (String) properties.get("terminology.lastdirectory");
				if (!StringUtils.isBlank(value)) {
					result.setTerminologyLastDirectory(value);
				}
			}

			//
			{
				String value = (String) properties.get("filebatchconverter.sourceDirectory");
				if (!StringUtils.isBlank(value)) {
					result.getFileBatchConverterCriteria().setSourceDirectory(new File(value));
				}

				value = (String) properties.get("filebatchconverter.targetDirectory");
				if (!StringUtils.isBlank(value)) {
					result.getFileBatchConverterCriteria().setTargetDirectory(new File(value));
				}
			}

		} catch (FileNotFoundException exception) {
			//
			throw PuckExceptions.FILE_NOT_FOUND.create("Preferences file not found: " + file.getAbsolutePath());

		} catch (IOException exception) {
			//
			throw PuckExceptions.BAD_FILE_FORMAT.create("Error reading (1) preferences file: " + exception.getMessage());

		} catch (IllegalAccessException exception) {
			//
			throw PuckExceptions.BAD_FILE_FORMAT.create("Error reading (2) preferences file: " + exception.getMessage());

		} catch (InvocationTargetException exception) {
			//
			throw PuckExceptions.BAD_FILE_FORMAT.create("Error reading (3) preferences file: " + exception.getMessage());
		}

		//
		return result;
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File file, final Preferences source) throws PuckException {

		logger.info("Saving preferences in [" + file.getAbsolutePath() + "]");

		//
		try {
			Properties properties = new Properties();
			properties.put("language", source.getLanguage().toString());
			properties.put("autoloadlastfile", Boolean.toString(source.isAutoLoadLastFile()));
			properties.putAll(convertObjectToProperties("inputSettings", source.getInputSettings()));

			//
			for (int detailIndex = 1; detailIndex <= 10; detailIndex++) {
				properties.put("census.details." + detailIndex + ".label",
						ToolBox.toString(source.getCensusCriteria().getCensusDetails().get(detailIndex - 1).getLabel()));
				properties.put("census.details." + detailIndex + ".report",
						Boolean.toString(source.getCensusCriteria().getCensusDetails().get(detailIndex - 1).isReport()));
				properties.put("census.details." + detailIndex + ".diagram",
						Boolean.toString(source.getCensusCriteria().getCensusDetails().get(detailIndex - 1).isDiagram()));
			}

			//
			// for (int detailIndex = 1; detailIndex <= 10; detailIndex++) {
			// properties.putAll(convertObjectToProperties("census.details2." +
			// detailIndex, source.getCensusCriteria().getCensusDetails()
			// .get(detailIndex - 1)));
			// }
			properties.putAll(convertObjectToProperties("census", source.getCensusCriteria()));
			properties.putAll(convertObjectToProperties("differentialCensus", source.getDifferentialCensusCriteria()));
			properties.putAll(convertObjectToProperties("agentSimulation", source.getAgentSimulationCriteria()));
			properties.putAll(convertObjectToProperties("virtualFieldwork", source.getVirtualFieldworkCriteria()));
			properties.putAll(convertObjectToProperties("agentSimulationVariations", source.getAgentSimulationVariationsCriteria()));
			properties.putAll(convertObjectToProperties("virtualFieldworkVariations", source.getVirtualFieldworkVariationsCriteria()));
			properties.putAll(convertObjectToProperties("randomPermutation", source.getRandomPermutationCriteria()));
			properties.putAll(convertObjectToProperties("reshuffling", source.getReshufflingCriteria()));
			properties.putAll(convertObjectToProperties("randomDistribution", source.getRandomDistributionCriteria()));
			properties.putAll(convertObjectToProperties("generateRules", source.getGenerateRulesCriteria()));
			properties.putAll(convertObjectToProperties("randomCorpusMas", source.getRandomCorpusMASCriteria()));

			//
			properties.putAll(convertSpaceTimeAnalysisCriteria("spaceTimeAnalysisCriteria", source.getSpaceTimeAnalysisCriteria()));

			//
			if (source.getFlatDB4GeonamesDirectory() != null) {
				properties.put("flatdb4geonames.database", source.getFlatDB4GeonamesDirectory());
			}

			//
			properties.put("terminology.selfname", source.getTerminologySelfName());
			properties.put("terminology.maxiterations", String.valueOf(source.getTerminologyMaxIterations()));
			properties.put("terminology.lastdirectory", source.getTerminologyLastDirectory());

			//
			if (source.getFileBatchConverterCriteria() != null) {
				if (source.getFileBatchConverterCriteria().getSourceDirectory() != null) {
					properties.put("filebatchconverter.sourceDirectory", source.getFileBatchConverterCriteria().getSourceDirectory().getAbsolutePath());
				}
				if (source.getFileBatchConverterCriteria().getTargetDirectory() != null) {
					properties.put("filebatchconverter.targetDirectory", source.getFileBatchConverterCriteria().getTargetDirectory().getAbsolutePath());
				}
			}

			//
			properties.store(new FileOutputStream(file), null);

		} catch (IllegalAccessException exception) {
			throw PuckExceptions.BAD_FILE_FORMAT.create("Error reading (1) preferences file: " + exception.getMessage());
		} catch (InvocationTargetException exception) {
			throw PuckExceptions.BAD_FILE_FORMAT.create("Error reading (2) preferences file: " + exception.getMessage());
		} catch (NoSuchMethodException exception) {
			throw PuckExceptions.BAD_FILE_FORMAT.create("Error reading (3) preferences file: " + exception.getMessage());
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Preferences file not found: " + file.getAbsolutePath());
		} catch (IOException exception) {
			throw PuckExceptions.BAD_FILE_FORMAT.create("Error reading (4) preferences file: " + exception.getMessage());
		}
	}
}
