package org.tip.puck.alliancenets;

import java.util.Vector;

import org.tip.puck.evo.Generator;
import org.tip.puck.evo.GenericFunSet;
import org.tip.puck.evo.ProgSet;
import org.tip.puck.util.RandomGenerator;


/**
 * @author Telmo Menezes
 *
 */
public class AllianceGen extends Generator {
    // generated alliance net
    private AllianceNet allianceNet;
    
    // topological indices
    private TopologicalIndices indices;
    private TopologicalIndices targetIndices;
    
    public AllianceGen(int nodeCount, int edgeCount, TopologicalIndices targetIndices) {
        super(nodeCount, edgeCount);
        
        this.targetIndices = targetIndices;
        indices = null;
    }

    @Override
    public void createProgSet() {
        progcount = 1;
        
        Vector<String> variableNames = new Vector<String>();
        variableNames.add("origId");
        variableNames.add("targId");
        variableNames.add("origInDeg");
        variableNames.add("origOutDeg");
        variableNames.add("targInDeg");
        variableNames.add("targOutDeg");
        variableNames.add("origInStren");
        variableNames.add("origOutStren");
        variableNames.add("targInStren");
        variableNames.add("targOutStren");
        variableNames.add("undirDist");
        variableNames.add("dirDist");
        variableNames.add("revDist");
        variableNames.add("dirStren");
        variableNames.add("revStren");
        
        progset = new ProgSet(progcount, variableNames);
        
        progset.varcounts.set(0, 15);
        progset.funsets.set(0, GenericFunSet.instance().getFunset());
        progset.prognames.set(0, "Prog\n");
    }

    @Override
    public void run() {
        // reset eval stats
        progset.clearEvalStats();
        
        // init DistMatrix
        DistMatrix.instance().setNodes(nodeCount);

        allianceNet = new AllianceNet(true);

        // create nodes
        Group[] nodeArray = new Group[nodeCount];
        double[][] weightArray = new double[nodeCount][nodeCount];
        for (int i = 0; i < nodeCount; i++) {
            nodeArray[i] = allianceNet.addNodeWithId(i);
        }

        // create edges
        for (int i = 0; i < targetIndices.getN(); i++) {
            double totalWeight = 0;
            for (int origIndex = 0; origIndex < nodeCount; origIndex++) {
                for (int targIndex = 0; targIndex < nodeCount; targIndex++) {
                    Group origNode = nodeArray[origIndex];
                    Group targNode = nodeArray[targIndex];
        
                    double undirectedDistance = DistMatrix.instance().getUDist(origNode.getId(), targNode.getId());
                    double directDistance = DistMatrix.instance().getDDist(origNode.getId(), targNode.getId());
                    double reverseDistance = DistMatrix.instance().getDDist(targNode.getId(), origNode.getId());
                    
                    double directStrength = 0;
                    Alliance alliance = allianceNet.getEdge(origNode, targNode);
                    if (alliance != null) {
                        directStrength = alliance.getWeight();
                    }
                    double reverseStrength = 0;
                    alliance = allianceNet.getEdge(targNode, origNode);
                    if (alliance != null) {
                        reverseStrength = alliance.getWeight();
                    }
                    
                    progset.progs[0].vars[0] = (double)origIndex;
                    progset.progs[0].vars[1] = (double)targIndex;
                    progset.progs[0].vars[2] = (double)origNode.getInDegree();
                    progset.progs[0].vars[3] = (double)origNode.getOutDegree();
                    progset.progs[0].vars[4] = (double)targNode.getInDegree();
                    progset.progs[0].vars[5] = (double)targNode.getOutDegree();
                    progset.progs[0].vars[6] = origNode.getTotalInputWeight();
                    progset.progs[0].vars[7] = origNode.getTotalOutputWeight();
                    progset.progs[0].vars[8] = targNode.getTotalInputWeight();
                    progset.progs[0].vars[9] = targNode.getTotalOutputWeight();
                    progset.progs[0].vars[10] = undirectedDistance;
                    progset.progs[0].vars[11] = directDistance;
                    progset.progs[0].vars[12] = reverseDistance;
                    progset.progs[0].vars[13] = directStrength;
                    progset.progs[0].vars[14] = reverseStrength;
                    
                    double weight = progset.progs[0].eval(i);
                    if (weight < 0) {
                        weight = 0;
                    }
        
                    weightArray[origIndex][targIndex] = weight;
                    totalWeight += weight;
                }
            }

            // if total weight is zero, make every pair's weight = 1
            if (totalWeight == 0) {
                for (int x = 0; x < nodeCount; x++) {
                    for (int y = 0; y < nodeCount; y++) {
                        weightArray[x][y] = 1.0;
                        totalWeight += 1.0;
                    }
                }
            }

            double weight = RandomGenerator.instance().random.nextDouble() * totalWeight;
            int origIndex = 0;
            int targIndex = 0;
            totalWeight = weightArray[origIndex][targIndex];
            while (totalWeight < weight) {
                origIndex++;
                if (origIndex >= nodeCount) {
                    targIndex++;
                    origIndex = 0;
                }
                totalWeight += weightArray[origIndex][targIndex];
            }

            Group origNode = nodeArray[origIndex];
            Group targNode = nodeArray[targIndex];

            Alliance alliance = allianceNet.getEdge(origNode, targNode);
            if (alliance == null) {
                allianceNet.addEdge(origNode, targNode, i);
                alliance = allianceNet.getEdge(origNode, targNode);
            }

            alliance.setWeight(alliance.getWeight() + 1.0);
            
            // update distances
            DistMatrix.instance().updateDistances(origIndex, targIndex);
        }
    }
    
    public double distance(Generator generator) {
        return 0;
    }
    
    @Override
    public Generator clone() {
        return new AllianceGen(nodeCount, edgeCount, targetIndices);
    }

    public TopologicalIndices getIndices() {
        return indices;
    }

    public void setIndices(TopologicalIndices indices) {
        this.indices = indices;
    }

    public TopologicalIndices getTargetIndices() {
        return targetIndices;
    }

    public void setTargetIndices(TopologicalIndices targetIndices) {
        this.targetIndices = targetIndices;
    }

    public AllianceNet getNet() {
        return allianceNet;
    }
}