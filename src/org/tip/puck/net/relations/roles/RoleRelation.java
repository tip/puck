package org.tip.puck.net.relations.roles;

import org.tip.puck.net.Attributes;
import org.tip.puck.net.Individual;
import org.tip.puck.net.KinType;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.relations.Roles;
import org.tip.puck.util.Numberable;

// Transform into extension of Relation<E>

/**
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class RoleRelation implements Numberable, Comparable<RoleRelation> {

	private int id;
	private int typedId;
	private RelationModel model;
	private String name;
	private RoleActors actors;
	private Attributes attributes;
	
	
	public RoleRelation(int id){
		
		this.id = id;
		this.actors = new RoleActors();
	}
	

	@Override
	public RoleRelation clone() {
		RoleRelation result;
		
		result = new RoleRelation(id);
		for (RoleActor actor : actors){
			result.getActors().add(actor);
		}

		//
		return result;
	}
	
	public RoleRelation clone(int id){
		RoleRelation result;
		
		result = clone();
		result.setId(id);
		
		//
		return result;
	}

	
	public int compareTo (RoleRelation other){
		int result;
		
		RoleActor self = getSelf();
		RoleActor otherSelf = other.getSelf();
		
		if (self.hasSelfName() && !otherSelf.hasSelfName()){
			
			result = -1;
			
		} else if (!self.hasSelfName() && otherSelf.hasSelfName()){
			
			result = 1;
			
		} else {
		
			result = new RoleActorComparator().compare(getSelf(),other.getSelf());
			
			if (result == 0){
				result = actors.compareTo(other.getActors());
			}
		}

		//
		return result;
	}
	
	@Override
	public boolean equals(final Object obj) {
		boolean result;

		result = actors.equals(((RoleRelation) obj).getActors());
		
		//
		return result;
	}
	

	public RoleActors getActors() {
		return actors;
	}
	

	public RoleActors getActors(final Role role) {
		RoleActors result;

		result = new RoleActors();
		for (RoleActor actor : getActors()){
			if (actor.getIndividual().equals(role)){
				result.addNew(actor);
			}
			
		}
		//
		return result;
	}
	
	public RoleActors getActorsByRole(final MetaRole metaRole) {
		RoleActors result;

		result = new RoleActors();
		for (RoleActor actor : getActors()){
			if (actor.getRole().equals(metaRole)){
				result.addNew(actor);
			}
			
		}
		//
		return result;
	}
	
	public RoleActors getActorsByRoleName (String roleName){
		RoleActors result;
		
		if (roleName==null){
			
			result = actors;
			
		} else {
			
			result = new RoleActors();
			
			for (RoleActor actor : actors){
				if (actor.getRole().getName().equals(roleName)){
					result.add(actor);
				}
			}
		}
		//
		return result;
	}
	
	public RoleActors getActorsByRoleNameWithUnspecific(String roleName, RoleRelations relations){
		RoleActors result;
		
		result = getSelf().getActorsByRoleNameWithUnspecific(roleName, relations);
		
		//
		return result;
	}

	
	public Role getByRoleName (final String metaRoleName){
		Role result;
		
		result = null;
		
		for (RoleActor actor : actors){
			if (actor.getRole().getName().equals(metaRoleName)){
				result = actor.getIndividual();
				break;
			}
		}
		//
		return result;
	}
	
	@Override
	public int getId() {
		return this.id;
	}
	
	public Roles getRoles(){
		Roles result;
		
		result = new Roles();
		
		for (RoleActor actor : getActors()){
			result.addNew(actor.getRole());
		}
		//
		return result;
	}
	
	public RoleActor getPrimaryAlter(){
		RoleActor result;
		
		if (actors.size()<2){
			
			result = null;
			
		} else {
			
			result = actors.get(1);
		}
		//
		return result;
	}
	
	public RoleActor getSelf(){
		RoleActor result;
		
		result = actors.get(0);
		
/*		RoleActors selfs = getActorsByRoleName(KinType.SELF.toString());
		
		if (selfs.isEmpty()){
			
			result = null;
			
		} else {
			
			if (selfs.size()>1){
				System.err.println("Multiple selfs "+selfs.toSortedList());
			}
			
			result = selfs.get(0);
		}*/
		//
		return result;
	}
	
	public String getSelfAsString(){
		String result;
		
		RoleActor self = getSelf();
		
		if (self == null){
			
			result = null;
		} else {
			result = self.toString();
		}
		//
		return result;
	}

	public Role getSelfTerm() {
		Role result;
		
		RoleActor self = getSelf();
		
		if (self == null){
			result = null;
		} else {
			result = getSelf().getIndividual();
		}
		//
		return result;
	}

	public boolean hasActor(final Role role, final MetaRole metaRole){
		boolean result;
		
		result = false;
		
		for (RoleActor actor : actors){
			if (actor.getIndividual().equals(role) && actor.getRole().equals(metaRole)){
				result = true;
				break;
			}
		}
		//
		return result;
	}


	public boolean hasActor (final RoleActor actor){
		boolean result;
		
		if (actor == null){
			
			result = false;
			
		} else {
			
			result = hasActor(actor.getIndividual(),actor.getRole());
		}
		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String hashKey() {
		String result;

		result = "" + this.id;

		//
		return result;
	}

	@Override
	public void setId(final int id) {
		this.id = id;
	}
	

	@Override
	public String toString() {
		String result;
		
		result = id+"\t"+actors.toListSortedByRoles()+"";
		
		//
		return result;
	}
	
	int getActorCount(){
		
		return actors.size();
	}
	
	
	
	

}
