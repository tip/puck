package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;
import java.util.ResourceBundle;
import java.util.jar.Manifest;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.util.BuildInformation;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.StringList;

public class AboutPopup extends JDialog {

	private static final long serialVersionUID = -4659873135163125543L;
	private static final Logger logger = LoggerFactory.getLogger(AboutPopup.class);

	/**
	 * Create the dialog.
	 */
	public AboutPopup() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setResizable(true);
		setIconImage(Toolkit.getDefaultToolkit().getImage(AboutPopup.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		setTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("AboutPopup.this.title"));
		setModal(true);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 653, 347);
		setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout(0, 0));

		JPanel panelContent = new JPanel();
		panelContent.setBackground(Color.WHITE);
		getContentPane().add(panelContent, BorderLayout.CENTER);
		panelContent.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("210dlu"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				RowSpec.decode("4dlu:grow"), }));

		JLabel lblNewLabel = new JLabel();
		panelContent.add(lblNewLabel, "2, 2");
		lblNewLabel.setIcon(new ImageIcon(AboutPopup.class.getResource("/org/tip/puckgui/images/logo-puck-250x.jpg")));
		JTextPane textPane = new JTextPane();
		panelContent.add(textPane, "4, 2");
		textPane.setEditable(false);
		textPane.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("AboutPopup.textPane.text")); //$NON-NLS-1$ //$NON-NLS-2$

		// //////////////////////
		System.out.println(puckFullVersion());
		textPane.setText(textPane.getText().replace("$PUCK_VERSION", puckFullVersion()) + "\n" + infos());

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		getContentPane().add(panel_1, BorderLayout.SOUTH);
		JButton button = new JButton("OK");
		panel_1.add(button);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				System.out.println("OK pressed.");
				dispose();
			}
		});
		button.setActionCommand("OK");
	}

	/**
	 * 
	 * @return
	 */
	public static String infos() {
		String result;

		//
		StringList text = new StringList();
		text.append("JVM = ").append(System.getProperty("java.version")).append(" ").appendln(System.getProperty("java.vm.version"));
		text.append("XMX = ");
		text.append(Runtime.getRuntime().freeMemory() / 1024 / 1024);
		text.append("M/");
		text.append(Runtime.getRuntime().totalMemory() / 1024 / 1024);
		text.append("M/");
		text.append(Runtime.getRuntime().maxMemory() / 1024 / 1024);
		text.appendln("M");
		text.append("OS = ").append(System.getProperty("os.name")).append(" ").append(System.getProperty("os.version")).append(" ")
				.append(System.getProperty("sun.arch.data.model")).appendln("bits");

		//
		result = text.toString();

		//
		return result;
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		try {
			AboutPopup.showDialog();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	/**
	 * 
	 */
	public static String puckFullVersion() {
		String result;

		result = "Puck " + (new BuildInformation()).version();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static String puckImplementationVersion() {
		String result;

		try {
			InputStream in = AboutPopup.class.getResourceAsStream("/META-INF/MANIFEST.MF");
			if (in == null) {
				//
				result = null;

			} else {
				//
				Manifest manifest = new Manifest(in);
				result = manifest.getMainAttributes().getValue("Implementation-Version");
			}
		} catch (final Exception exception) {
			//
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * In the build.xml, ImplementVersion is set as "Puck ${version} ${TODAY}"
	 * and TODAY is a datetime so with date and time => four tokens.
	 * 
	 * @return
	 */
	public static String puckTime() {
		String result;

		String implementationVersion = puckImplementationVersion();

		if (implementationVersion == null) {
			result = "NOW";
		} else {
			String[] tokens = implementationVersion.split(" ");

			if (tokens.length == 4) {
				result = tokens[2] + " " + tokens[3];
			} else {
				result = implementationVersion;
			}
		}

		//
		return result;
	}

	/**
	 * In the build.xml, ImplementVersion is set as "Puck ${version} ${TODAY}"
	 * and TODAY is a datetime so with date and time => four tokens.
	 * 
	 * @return
	 */
	public static String puckVersion() {
		String result;

		String implementationVersion = puckImplementationVersion();

		if (implementationVersion == null) {
			result = "2.0.dev";
		} else {
			String[] tokens = implementationVersion.split(" ");

			if (tokens.length == 4) {
				result = tokens[1];
			} else {
				result = implementationVersion;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public static void showDialog() {
		AboutPopup dialog = new AboutPopup();
		dialog.pack();
		dialog.setVisible(true);
	}
}
