package org.tip.puck.graphs;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.PuckException;
import org.tip.puck.graphs.Link.LinkType;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Attributes;

/**
 * 
 * @author TIP
 */
public class Node<E> implements Comparable<Node<E>>{
	public static final int NO_ID = -1;
	private int id;
	private E referent;
	private String tag;
	private double weight;
	private double secondWeight;
	private Attributes attributes;
	private RootedLinks<E> edges;
	private RootedLinks<E> outArcs;
	private RootedLinks<E> inArcs;
	private String label;

	/**
	 * 
	 * @param idLabel
	 */
	public Node(final int id, final E referent) {
		this.id = id;
		this.referent = referent;
		this.outArcs = new RootedLinks<E>(this);
		this.inArcs = new RootedLinks<E>(this);
		this.edges = new RootedLinks<E>(this);
	}

	/**
	 * 
	 * @param targetNode
	 * @param value
	 * @throws PuckException
	 */
	public double addArcWeight(final Node<E> otherNode, final double value) {
		double result;

		//
		Link<E> link = this.outArcs.getLinkWith(otherNode);
		if (link == null) {
			link = addArcWith(otherNode);
		}

		//
		result = link.addWeight(value);

		//
		return result;
	}

	/**
	 * 
	 * @param targetNode
	 * @throws PuckException
	 */
	public Link<E> addArcWith(final Node<E> otherNode) {
		Link<E> result;

		result = addArcWith(otherNode, 0);

		//
		return result;
	}

	/**
	 * @param targetNode
	 * @throws PuckException
	 */
	public Link<E> addArcWith(final Node<E> otherNode, final double weight) {
		Link<E> result;

		//
		result = new Link<E>(this, otherNode, LinkType.ARC, weight);

		//
		this.outArcs.add(result);
		otherNode.getInArcs().add(result);

		//
		return result;
	}

	/**
	 * 
	 * @param targetNode
	 * @param value
	 * @throws PuckException
	 */
	public double addEdgeWeight(final Node<E> otherNode, final double value) {
		double result;

		//
		Link<E> link = getEdgeWith(otherNode);
		if (link == null) {
			link = addEdgeWith(otherNode);
		}

		//
		result = link.addWeight(value);

		//
		return result;
	}

	/**
	 * 
	 * @param targetNode
	 * @throws PuckException
	 */
	public Link<E> addEdgeWith(final Node<E> otherNode) {
		Link<E> result;

		result = addEdgeWith(otherNode, 0);

		//
		return result;
	}

	/**
	 * 
	 * @param targetNode
	 * @throws PuckException
	 */
	public Link<E> addEdgeWith(final Node<E> otherNode, final double weight) {
		Link<E> result;

		//
		result = new Link<E>(this, otherNode, LinkType.EDGE, weight);

		//
		this.edges.add(result);
		if (otherNode != this) {
			otherNode.getEdges().add(result);
		}

		//
		return result;
	}

	public Nodes<E> getAllNeighbors(final Nodes<E> nodes, final int limit) {

		Map<Integer, Nodes<E>> result;

		//
		result = new HashMap<Integer, Nodes<E>>();

		Nodes<E> interior = new Nodes<E>();
		Nodes<E> ego = new Nodes<E>();
		ego.add(this);
		result.put(0, ego);

		for (int i = 1; i <= limit; i++) {
			Nodes<E> neighbors = new Nodes<E>();
			Nodes<E> source;

			if (i < limit) {
				source = result.get(i - 1).getDirectNeighbors();
			} else {
				source = nodes;
			}

			for (Node<E> neighbor : source) {
				if (!interior.contains(neighbor)) {
					neighbors.add(neighbor);
					interior.add(neighbor);
				}
			}
			result.put(i, neighbors);
		}

		//
		return interior;
	}

	/**
	 * 
	 * @param otherNode
	 * @return
	 */
	public double getArcWeight(final Node<E> otherNode) {
		double result;

		Link<E> link = this.getArcWith(otherNode);
		if (link == null) {
			result = 0;
		} else {
			result = link.getWeight();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param otherNode
	 * @return
	 */
	public Link<E> getArcWith(final Node<E> otherNode) {
		Link<E> result;

		result = this.outArcs.getLinkWith(otherNode);

		//
		return result;
	}

	public int getDegree() {
		return getLinks().size();
	}
	
	public int getDegreeWithoutLoops() {
		int result;
		
		result = 0;
		for (Link<E> link : getLinks()){
			if (!link.isLoop()){
				result++;
			}
		}
		//
		return result;
	}

	public Nodes<E> getInNodes() {
		Nodes<E> result;

		//
		result = new Nodes<E>();
		for (Link<E> link : getInLinks()) {
			result.add(link.getOtherNode(this));
		}

		//
		return result;
	}

	/**
	 * gets in- and outnodes 
	 * 
	 * @return
	 */
	public Nodes<E> getOtherNodes() {
		Nodes<E> result;

		//
		result = new Nodes<E>();
		for (Link<E> link : getLinks()) {
			result.add(link.getOtherNode(this));
		}

		//
		return result;
	}
	
	/**
	 * counts neighbors (without multiple lines and loops)
	 * @return
	 */
	public int tieCountWithoutLoops(){
		int result;
		
		result = getDirectNeighbors().size();
		
		//
		return result;
	}
	
	/**
	 * counts neighbors (without multiple)
	 * @return
	 */
	public int tieCount(){
		int result;
		
		result = getOtherNodes().size();
		
		//
		return result;
	}
	

	/**
	 * gets direct neighbors (in- and outnodes without ego)
	 * 
	 * @return
	 */
	public Nodes<E> getDirectNeighbors() {
		Nodes<E> result;

		//
		result = new Nodes<E>();
		for (Link<E> link : getLinks()) {
			Node<E> other = link.getOtherNode(this);
			if (other != this) {
				result.add(other);
			}
		}

		//
		return result;
	}

	public Nodes<E> getOutNodes() {
		Nodes<E> result;

		//
		result = new Nodes<E>();
		for (Link<E> link : getOutLinks()) {
			result.add(link.getOtherNode(this));
		}

		//
		return result;
	}

	public int getEdgeDegree() {
		return getEdges().size();
	}

	public double getEdgeForce() {
		double result;

		result = 0.;
		for (Link<E> edge : getEdges()) {
			result += edge.getWeight();
		}
		//
		return result;
	}

	public RootedLinks<E> getEdges() {
		return edges;
	}

	/**
	 * 
	 * @param otherNode
	 * @return
	 */
	public double getEdgeWeight(final Node<E> otherNode) {
		double result;

		Link<E> link = this.getEdgeWith(otherNode);
		if (link == null) {
			result = 0;
		} else {
			result = link.getWeight();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param otherNode
	 * @return
	 */
	public Link<E> getEdgeWith(final Node<E> otherNode) {
		Link<E> result;

		result = this.edges.getLinkWith(otherNode);

		//
		return result;
	}

	public double getForce() {
		double result;

		result = 0.;
		for (Link<E> link : getLinks()) {
			result += link.getWeight();
		}
		//
		return result;
	}

	public int getId() {
		return id;
	}

	public RootedLinks<E> getInArcs() {
		return inArcs;
	}
	
	/**
	 * 
	 * @param tag
	 * @return
	 */
	public RootedLinks<E> getInArcsByTag(final String pattern) {
		RootedLinks<E> result;

		//
		result = new RootedLinks<E>(this);

		//
		for (Link<E> arc : inArcs) {
			for (String tag : arc.getTags()){
				if (StringUtils.equals(tag, pattern)) {
					result.add(arc);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param pattern
	 * @return
	 */
	public RootedLinks<E> getOutArcsByTag(final String pattern) {
		RootedLinks<E> result;

		//
		result = new RootedLinks<E>(this);

		//
		for (Link<E> arc : outArcs) {
			for (String tag : arc.getTags()){
				if (StringUtils.equals(tag, pattern)) {
					result.add(arc);
				}
			}
		}

		//
		return result;
	}

	public int getInDegree() {
		return inArcs.size();
	}

	public RootedLinks<E> getInferiorEdges() {
		RootedLinks<E> result;

		result = new RootedLinks<E>(this);
		for (Link<E> edge : edges) {
			if (edge.getOtherNode(this).getId() >= getId()) {
				result.add(edge);
			}
		}

		//
		return result;
	}

	public double getInForce() {
		double result;

		result = 0.;
		for (Link<E> arc : getInArcs()) {
			result += arc.getWeight();
		}
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Links<E> getInLinks() {
		Links<E> result;

		//
		result = new Links<E>();

		//
		result.addAll(this.inArcs.getLinks());
		result.addAll(this.edges.getLinks());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public String getLabel() {
		String result;

		if (label != null) {
			result = label;
		} else if (referent == null) {
			result = "node " + this.id;
		} else {
			result = this.referent.toString();
		}

		//
		return result;
	}
	
	
	
	public void setLabel(String label) {
		this.label = label;
	}

	public String toString(){
		String result;
		
		result = getLabel();
				
		if (result==null && referent!=null){
			result=referent.toString();
		}
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Links<E> getLinks() {
		Links<E> result;

		//
		result = new Links<E>();

		//
		result.addAll(this.inArcs.getLinks());
		for (Link<E> outArc : this.outArcs.getLinks()){
			if (!result.contains(outArc)){
				result.add(outArc);
			}
		}
		result.addAll(this.edges.getLinks());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int getLoopDegree() {
		int result;

		Link<E> loop = getLoopLink();
		if (loop == null) {
			result = 0;
		} else {
			result = 1;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double getLoopForce() {
		double result;

		Link<E> loop = getLoopLink();
		if (loop == null) {
			result = 0;
		} else {
			result = loop.getWeight();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Link<E> getLoopLink() {
		Link<E> result;

		boolean ended = false;
		int linkIndex = 0;
		Links<E> links = getOutLinks();
		result = null;
		while (!ended) {
			if (linkIndex < links.size()) {
				Link<E> link = links.get(linkIndex);
				if (link.isLoop()) {
					ended = true;
					result = link;
				} else {
					linkIndex += 1;
				}
			} else {
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Double getMaxLinkWeight() {
		Double result;

		Links<E> links = getLinks();

		if (links.isEmpty()) {
			result = null;
		} else {
			result = Double.MIN_NORMAL;
			for (Link<E> link : links) {
				if (link.getWeight() > result) {
					result = link.getWeight();
				}
			}
		}

		//
		return result;
	}

	public Map<Integer, Nodes<E>> getNeighbors(final Nodes<E> nodes, final int limit) {
		Map<Integer, Nodes<E>> result;

		//
		result = new HashMap<Integer, Nodes<E>>();
		for (int i = 0; i < limit; i++) {
			if (i == 0) {
				result.put(0, new Nodes<E>());
				result.get(0).add(this);
			} else if (i < limit) {
				result.put(i, result.get(i - 1).getDirectNeighbors());
			}
		}

		//
		return result;
	}

	public boolean[] getNeighborStatus(final Node<E> neighbor, final int limit) {
		boolean[] result;
		//
		result = new boolean[limit];

		Nodes<E> source = new Nodes<E>();
		source.add(this);

		for (int i = 0; i < limit; i++) {
			if (source.contains(neighbor)) {
				result[i] = true;
			}
			source = source.getDirectNeighbors();
		}
		//
		return result;
	}

	public RootedLinks<E> getOutArcs() {
		return outArcs;
	}

	public int getOutDegree() {
		return outArcs.size();
	}

	public double getOutForce() {
		double result;

		result = 0.;
		for (Link<E> arc : getOutArcs()) {
			result += arc.getWeight();
		}
		//
		return result;
	}
	
	public double getForceBalance(){
		return getInForce() - getOutForce();
	}

	/**
	 * 
	 * @return
	 */
	public Links<E> getOutLinks() {
		Links<E> result;

		//
		result = new Links<E>();

		//
		result.addAll(this.outArcs.getLinks());
		result.addAll(this.edges.getLinks());

		//
		return result;
	}

	public E getReferent() {
		return referent;
	}

	public double getSecondWeight() {
		return secondWeight;
	}

	public String getTag() {
		return tag;
	}

	public double getWeight() {
		return weight;
	}

	/**
	 * 
	 * @param otherNode
	 * @return
	 */
	public double incArcWeight(final Node<E> otherNode) {
		double result;

		//
		result = addArcWeight(otherNode, 1);

		//
		return result;
	}

    /**
     * 
     * @param otherNode
     * @return
     */
    public double incWeight() {
        double result;

        //
        result = incWeight(1);

        //
        return result;
    }

    /**
     * 
     * @param otherNode
     * @return
     */
    public double incWeight(double value) {
        double result;

        //
        this.weight += value;
    
        result = this.weight;

        //
        return result;
    }

	/**
	 * 
	 * @param otherNode
	 * @return
	 */
	public double incEdgeWeight(final Node<E> otherNode) {
		double result;

		//
		result = addEdgeWeight(otherNode, 1);

		//
		return result;
	}

	/**
	 * 
	 * @param otherNode
	 * @param weight
	 * @throws PuckException
	 */
	public void setArcWeight(final Node<E> otherNode, final double weight) {
		//
		Link<E> link = getArcWith(otherNode);
		if (link == null) {
			link = addArcWith(otherNode);
		}

		//
		link.setWeight(weight);
	}

	/**
	 * 
	 * @param otherNode
	 * @param weight
	 * @throws PuckException
	 */
	public void setEdgeWeight(final Node<E> otherNode, final double weight) {
		//
		Link<E> link = getEdgeWith(otherNode);
		if (link == null) {
			link = addEdgeWith(otherNode);
		}

		//
		link.setWeight(weight);
	}

	public void setId(final int id) {
		this.id = id;
	}

	public void setReferent(final E referent) {
		this.referent = referent;
	}

	public void setSecondWeight(final double secondWeight) {
		this.secondWeight = secondWeight;
	}

	public void setTag(final String tag) {
		this.tag = tag;
	}

	public void setWeight(final double weight) {
		this.weight = weight;
	}

	@Override
	public int compareTo(Node<E> other) {
		int result;

		if (other == null) {
			result = 1;
		} else {
			result = new Integer(getId()).compareTo(other.getId());
		}

		//
		return result;
	}
	
	@Override
	public boolean equals(final Object obj) {
		boolean result;

		result = obj != null && getId() == ((Node<E>) obj).getId();

		//
		return result;
	}
	
	/**
	 * 
	 * @param label
	 * @param value
	 */
	public void setAttribute(final String label, final String value) {
		if (attributes == null){
			attributes = new Attributes();
		}
		this.attributes().put(label, value);
	}
	
	/**
	 * 
	 * @return
	 */
	public Attributes attributes() {
		return this.attributes;
	}
	
	/**
	 * 
	 */
	public String getAttributeValue(final String label) {
		String result;

		if (attributes==null){
			result = null;
		} else {
			Attribute attribute = this.attributes().get(label);
			if (attribute == null) {
				result = null;
			} else {
				result = attribute.getValue();
			}
		}

		//
		return result;
	}
	
	public void removeLinksToNode(Node<E> node){
		
		this.inArcs.removeLinkToNode(node);
		this.outArcs.removeLinkToNode(node);
		this.edges.removeLinkToNode(node);
	}
	
	public double getOrientation(){
		double result;
		
		double inStrength = getInForce();
		double outStrength = getOutForce();
		
		result = (inStrength-outStrength)/(inStrength+outStrength);
		
		//
		return result;
	}
	
	public double getMaxInWeight (){
		double result;
		
		result = 0.;
		
		for (Link<E> arc : this.getInArcs()){
			double weight = arc.getWeight();
			if (weight>result){
				result = weight;
			}
		}
		//
		return result;
	}
	
	public double getMaxOutWeight (){
		double result;
		
		result = 0.;
		
		for (Link<E> arc : this.getOutArcs()){
			double weight = arc.getWeight();
			if (weight>result){
				result = weight;
			}
		}
		//
		return result;
	}
	
	public Nodes<E> getMaxPredecessors(){
		Nodes<E> result;
		
		result = new Nodes<E>();
		double maxWeight= 0.;
		
		for (Link<E> arc : this.getInArcs()){
			double weight = arc.getWeight();
			if (weight>maxWeight){
				result = new Nodes<E>();
				maxWeight = weight;
			}
			if (weight>=maxWeight){
				result.add(arc.getOtherNode(this));
				maxWeight = weight;
			}
		}
		//
		return result;
		
	}
	
	public Nodes<E> getMaxSuccessors(){
		Nodes<E> result;
		
		result = new Nodes<E>();
		double maxWeight= 0.;
		
		for (Link<E> arc : this.getOutArcs()){
			double weight = arc.getWeight();
			if (weight>maxWeight){
				result = new Nodes<E>();
				maxWeight = weight;
			}
			if (weight>=maxWeight){
				result.add(arc.getOtherNode(this));
				maxWeight = weight;
			}
		}
		//
		return result;
		
	}





}
