package org.tip.puckgui.views.mas;

/**
 * 
 * @author TIP
 */
public class CousinsWeightFactor implements WeightFactor {
	private final Type type = Type.COUSINS;
	private double first;
	private double second;
	private double third;

	/**
	 * 
	 */
	public CousinsWeightFactor() {
		this.first = 1;
		this.second = 1;
		this.third = 1;
	}

	public double getFirst() {
		return first;
	}

	public double getSecond() {
		return second;
	}

	public double getThird() {
		return third;
	}

	@Override
	public Type getType() {
		return type;
	}

	public void setFirst(final double first) {
		this.first = first;
	}

	public void setSecond(final double second) {
		this.second = second;
	}

	public void setThird(final double third) {
		this.third = third;
	}
}
