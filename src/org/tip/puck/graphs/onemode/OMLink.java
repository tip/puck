/**
 * 
 */
package org.tip.puck.graphs.onemode;

/**
 * @author Telmo Menezes
 *
 */
public class OMLink {
	private OMNode origin;
	private OMNode target;
	private boolean wifeLink;
	private boolean shuffled;
	
	public OMLink(OMNode origin, OMNode target, boolean wifeLink) {
		this.origin = origin;
		this.target = target;
		this.wifeLink = wifeLink;
		this.shuffled = false;
	}

	public OMNode getOrigin() {
		return origin;
	}

	public void setOrigin(OMNode origin) {
		this.origin = origin;
	}

	public OMNode getTarget() {
		return target;
	}

	public void setTarget(OMNode target) {
		this.target = target;
	}

	public boolean isWifeLink() {
		return wifeLink;
	}

	public void setWifeLink(boolean wifeLink) {
		this.wifeLink = wifeLink;
	}

	public boolean isShuffled() {
		return shuffled;
	}

	public void setShuffled(boolean shuffled) {
		this.shuffled = shuffled;
	}
}
