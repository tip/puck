package org.tip.puck.report;

import java.util.ArrayList;
import java.util.List;

import org.tip.puck.graphs.Graph;

/**
 * 
 * @author TIP
 */
public class ReportList extends ArrayList<Report> {

	private static final long serialVersionUID = -4178724747329327263L;
	
	private List<Graph> graphs;

	/**
	 * 
	 */
	public ReportList() {
		super();
		graphs = new ArrayList<Graph>();
	}

	public List<Graph> getGraphs() {
		return graphs;
	}
	

	
}
