package org.tip.puck.net;

import java.util.List;

import org.tip.puck.PuckException;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.Ordinal;

public interface Populatable {
	
	public Individuals getIndividuals() throws PuckException;
	
	public Individuals getIndividuals(Segmentation segmentation) throws PuckException;

	public Individuals getAllIndividuals(List<Ordinal> times) throws PuckException;


}
