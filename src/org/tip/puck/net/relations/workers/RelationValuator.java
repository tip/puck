package org.tip.puck.net.relations.workers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.tip.puck.PuckException;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.geo.Geography;
import org.tip.puck.geo.Place;
import org.tip.puck.graphs.workers.GraphValuator;
import org.tip.puck.net.Attributable;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.RelationEnvironment;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionMaker;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.workers.SequenceCriteria;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.NumberedValues;
import org.tip.puck.util.PuckUtils;
import org.tip.puck.util.ToolBox;
import org.tip.puck.util.Trafo;
import org.tip.puck.util.Value;

/**
 * 
 * @author TIP
 */
public class RelationValuator {

	public enum EndogenousLabel {
		ID,
		TYPEDID,
		DISTANCE,
		DISTANCEDYNAMIC,
		NRINDIVIDUALS,
		NRACTORS,
		NRROLES,
		GENDER_RATIO,
		
//		MOVEMENT,
		TURNOVER,
		
		HOST,
		MIG,
		HOSTMIG,
		MIGRATIONTYPE,
		CHILDMIGRATIONTYPE,
		
		TREES,  // Duplicate see above
//		PLACE,  // Duplicate see above
//		DATE,
		REGION,
		AGE
	}

	public static final Pattern YEAR_PATTERN = Pattern.compile("(\\d\\d\\d\\d)");

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String extractYear(final String source) {
		String result;

		Matcher matcher = YEAR_PATTERN.matcher(source);
		if ((matcher.find()) && (matcher.groupCount() > 0)) {
			//
			result = matcher.group(1);
		} else {
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param individual
	 * @param label
	 * @return
	 */
	public static Value get(final Relation source, final String label, final Geography geography) {
		Value result;

		result = get(source, label, null, geography);

		//
		return result;
	}

	/**
	 * 
	 * @param individual
	 * @param label
	 * 
	 * @return
	 */
	public static Value get(final Relation source, final String label, final Object parameter, final Geography geography) {
		Value result;
		
		result = null;

		//
		EndogenousLabel endogenousLabel;
		try {
			endogenousLabel = EndogenousLabel.valueOf(label.replace(" ", "_"));
		} catch (IllegalArgumentException exception) {
			endogenousLabel = null;
		}

		if (endogenousLabel == null) {
			
			String attributeValue = source.getAttributeValue(label);
			
			if (attributeValue == null) {
				
				if (label.toUpperCase().contains("PLAC")){
					
					String level;
					if (parameter==null){
						level = "LOCAL";
					} else {
						level = parameter+"";
					}
					String homonym = source.getAttributeValue(label);
					
					if (geography!=null){
						Place place = geography.getPlace(homonym, level);

						if (place == null || place.getToponym() == null){
							result = null;
						} else {
//							result = new Value(place.getName());
							result = new Value(place.getToponym());
						}
					}
				}

			} else if (label.toUpperCase().contains("DATE")) {
				String year = extractYear(attributeValue);
				if (year == null) {
					result = null;
				} else {
					result = new Value(Integer.parseInt(year));
				}
				
			} else if (label.toUpperCase().equals("TIME")){
				
				if (source.getAttributeValue("TIME")!=null){
					try {
						result = new Value(Integer.parseInt(source.getAttributeValue("TIME")));
					} catch (NumberFormatException nfe){
						System.err.println("time value is not an integer");
						result = null;
					}
				} else {
					result = null;
				}

			} else if (label.toUpperCase().contains("PLAC")){

//				if (RelationValuator.isDeath(source)){
//					result = new Value("DEATH");
//				} else {

				String level;
				if (parameter==null){
					level = "LOCAL";
				} else {
					level = parameter+"";
				}
				String homonym = source.getAttributeValue(label);
				
				if (geography!=null){
					Place place = geography.getPlace(homonym, level);

					if (place == null || place.getToponym() == null){
						result = null;
					} else {
//						result = new Value(place.getName());
						result = new Value(place.getToponym());
					}
				} else {
					result = new Value(attributeValue);
				}
				
				
			} else {
				result = new Value(attributeValue);
			}
			
		} else {
			
			switch (endogenousLabel) {
				case ID:
					result = new Value(source.getId());
				break;
				case TYPEDID:
					result = new Value(source.getTypedId());
					break;
				case NRINDIVIDUALS:
					result = new Value(source.getIndividuals().size());
					break;
				case NRACTORS:
					result = new Value(source.actors().size());
					break;
				case NRROLES:
					result = new Value(source.actors().getRoles().size());
					break;
				case GENDER_RATIO:
					result = new Value(source.getIndividuals().genderRatio(Gender.FEMALE));
					break;					
				case TREES:
					result = new Value(RelationWorker.getLinkTrees(source, true, "GENDER", (String)parameter, null, "LINE"));
					break;
				case DISTANCE:
/*						if (RelationValuator.isBirth(source)){
							result = new Value("BIRTH");
						} else if (RelationValuator.isDeath(source)){
							result = new Value("DEATH");
						} else {*/
						GeoLevel distance = getDistance(geography,source, "START_PLACE", "END_PLACE");
						if (distance == null) {
							result = null;
						} else {
							result = new Value(distance);
//							result = new Value(distance.dynamic());
						}
					break;
				case DISTANCEDYNAMIC:
/*						if (RelationValuator.isBirth(source)){
							result = new Value("BIRTH");
						} else if (RelationValuator.isDeath(source)){
							result = new Value("DEATH");
						} else {*/
						GeoLevel distance2 = getDistance(geography,source,"START_PLACE", "END_PLACE");
						if (distance2 == null) {
							result = null;
						} else {
							result = new Value(distance2.dynamic());
						}
					break;
						
/*					case MOVEMENT:
						result = new Value(SequenceWorker.getMovement(placeList, i));	
						break;
						
					case DATE:
						result = Value.valueOf(source.getAttributeValue((String)parameter));
						break;*/
						

						
				default:
					result = null;
			}
		}

		//
		return result;
	}
	
	/**
	 * @param geography
	 * @param event
	 * @return
	 */
	public static GeoLevel getDistance (Geography geography, Relation event, String startPlaceLabel, String endPlaceLabel){
		GeoLevel result;
		
		
		try {
			result = geography.getDistance(event.getAttributeValue(startPlaceLabel), event.getAttributeValue(endPlaceLabel));
		} catch (NullPointerException e) {
			result = GeoLevel.UNDEFINED;
		}
		
/*		if (event.getAttributeValue("START_PLACE")!=null && event.getAttributeValue("END_PLACE")!=null){
			Place start = geography.getByHomonym(event.getAttributeValue("START_PLACE"));
			Place end = geography.getByHomonym(event.getAttributeValue("END_PLACE"));
			result = geography.getDistance(start, end);
			if (start != null && end != null){
				Place commonAncestor = geography.getCommonAncestor(start, end);
				result = commonAncestor.getLevel();
			}
		}*/
		
		//
		return result;
	}
	
	
	/**
	 * 
	 * @param individual
	 * @param label
	 * @return
	 */
/*	public static NumberedValues get(final RelationSet slice, final String label, final Object parameter) {
		NumberedValues result;

		//
		result = new NumberedValues();

		for (Individual individual : slice.getIndividuals()) {
			
			result.put(individual.getId(), get(slice.getRelation(individual), label, parameter));
		}

		//
		return result;
	}*/

	/**
	 * Presupposes unique relation by individual
	 * @param individual
	 * @param label
	 * @return
	 */
	public static NumberedValues getByIndividuals(final Relations source, final String label, final Object parameter, final Geography geography) {
		NumberedValues result;

		//
		result = new NumberedValues();

		for (Relation relation : source) {
			for (Individual individual : relation.getIndividuals()){
				if (result.containsKey(individual.getId())){
					System.err.println("Multiple relation entries for "+individual);
				}
				result.put(individual.getId(), get(relation, label, parameter, geography));
			}
		}

		//
		return result;
	}
	
	
	public static String getEgoRolePartners(Relation relation, Individual ego, String relationModelName, String egoRoleName){
		String result;
		
		result = "";
		if (relation.getModel().getName().equals(relationModelName)){
			Individuals partners = new Individuals();
			for (Individual partner : relation.getIndividuals(egoRoleName)){
				if (partner!=ego){
					partners.put(partner);
				}
			}
			
			result = partners.toStringAsNameList();
		}
		
		//
		return result;
	}
	
	public static Value get (final Relation relation, final Individual ego, final String label, final RelationEnvironment egoEnvironment, final String impersonalAlterLabel) throws PuckException {
		Value result;
				
		result = null;
		
		if (label.equals("HOST")) {
			
			result = Value.valueOf(getReducedRelationType(relation, ego, label, egoEnvironment.getEgoRoleName(), egoEnvironment.getRelationsByAlter(), impersonalAlterLabel));
			
		} else if (label.equals("MIG")) {

			result = Value.valueOf(getRelationTypesAsShortCutString(relation,ego,label,egoEnvironment.getEgoRoleName(),egoEnvironment.getRelationsByAlter()));

		} else if (label.equals("HOSTMIG")) {

			result = Value.valueOf(getRelationTypesAsShortCutString(relation,ego,"HOST",egoEnvironment.getEgoRoleName(),egoEnvironment.getRelationsByAlter())+":"+getRelationTypesAsShortCutString(relation,ego,"MIG",egoEnvironment.getEgoRoleName(),egoEnvironment.getRelationsByAlter()));

		} else if (label.equals("MIGRATIONTYPE")) {

			result = Value.valueOf(getMigrationType(relation,ego,egoEnvironment.getEgoRoleName(),egoEnvironment.getRelationsByAlter()));
			
		} else if (label.equals("CHILDMIGRATIONTYPE")) {
						
			result = Value.valueOf(getChildMigrationType(ego, relation, egoEnvironment.getEgoRoleName(), egoEnvironment.getThreshold(),egoEnvironment.getRelationsByAlter()));
		}
		//
		return result;
	}

	public static Value get (final Relation relation, final Segmentation segmentation, final Individual ego, final String label, final Object labelParameter, final SequenceCriteria criteria) throws PuckException {
		Value result;
				
		result = null;
		
		if (!segmentation.getCurrentRelations().contains(relation)){
			
			result = null;
			
		} else if (labelParameter instanceof RelationEnvironment){
			
			result = get(relation, ego, label, (RelationEnvironment)labelParameter, criteria.getImpersonalAlterLabel());
			
		} else if (label.equals("AGE") && ego!=null){
		
			result = Value.valueOf(getAge(ego,relation));

		} else if (ego!=null && label.contains("REFERENT")){
			
			Actor actor = relation.getActor(ego, criteria.getEgoRoleName());
						
			if (actor!=null){

				if (label.equals("REFERENT")){
					
					result = Value.valueOf(actor.getReferent());
					
				} else if (label.equals("REFERENT_KIN")){
					
					result = Value.valueOf(RelationWorker.getReferentRole(actor, criteria.getPattern(), criteria.getGroupAffiliationLabel(), criteria.getReferentRelationLabel(), criteria.getRelationModelNames(), criteria.getChainClassification(), relation));
				
				} else if (label.equals("REFERENT_CHAIN")){
					
					result = Value.valueOf(RelationWorker.getReferentChainGenderString(actor, criteria.getGroupAffiliationLabel(), criteria.getReferentRelationLabel(), relation));
				
				} else if (label.equals("REFERENT_KIN_TYPE")){
					
					result = Value.valueOf(RelationWorker.getReferentRoleShort(actor, criteria.getPattern(), criteria.getGroupAffiliationLabel(), criteria.getReferentRelationLabel(), criteria.getRelationModelNames(), criteria.getChainClassification(), relation));
				
				} else if (label.equals("REFERENT_CHAIN_TYPE")){
					
					result = Value.valueOf(RelationWorker.getReferentChainNumber(actor, relation));
				
				}
			}
			
		} else if (label.equals("REFERENT_CHAIN")){
			
			result = Value.valueOf(RelationWorker.getReferentChainCensus(relation, ego, criteria.getEgoRoleName(), criteria.getGroupAffiliationLabel(), criteria.getReferentRelationLabel()));
			
		} else if (label.equals("REFERENT_KIN")){
			
			result = Value.valueOf(RelationWorker.getReferentKinCensus(relation, ego, criteria.getEgoRoleName(), criteria.getPattern(), criteria.getGroupAffiliationLabel(), criteria.getReferentRelationLabel(), criteria.getRelationModelNames(), criteria.getChainClassification()));

		} else if (label.equals("REFERENT_CHAIN_TYPES")){
			
			result = Value.valueOf(RelationWorker.getReferentChainVectorString(relation));

		} else if (IndividualValuator.isIndividualAttributeLabel(label, relation.getIndividuals())){
			
			if (IndividualValuator.isTimeDependent(label)){
				
				Integer time = relation.getTime(criteria.getDateLabel());
				result = Value.valueOf(PartitionMaker.create(label+"_"+time, relation.getIndividuals(segmentation), new PartitionCriteria(label,ToolBox.asString(labelParameter)), segmentation.getGeography()));
				
			} else {
				
				result = Value.valueOf(PartitionMaker.create(label+"_"+relation, relation.getIndividuals(segmentation), new PartitionCriteria(label,ToolBox.asString(labelParameter)), segmentation.getGeography()));
			}

//			Enlarge for dated attributes			
/*			PartitionCriteria partitionCriteria = new PartitionCriteria(label, ToolBox.asString(labelParameter));
			partitionCriteria.setTime(relation.getTime(criteria.getDateLabel()));
			partitionCriteria.setEgoRoleName(criteria.getEgoRoleName());
			partitionCriteria.setRelationModelName(criteria.getRelationModelName());*/

			
		} else if (GraphValuator.getAttributeLabels().contains(label)){

			result = GraphValuator.get(RelationWorker.getReferentGraph(relation), label);
			
		} else	if (label.equals("TREES_BY_ID")){
				
			result = Value.valueOf(RelationWorker.getLinkTrees(relation, false, "ID", null, null, null));
				
		} else if (label.equals("TREES_BY_GENDER")){
			
			result = Value.valueOf(RelationWorker.getLinkTrees(relation, false, "GENDER", null, null, null));
				
		} else if (label.equals("TREES_BY_GENDER_REDUCED")){
			
			result = Value.valueOf(RelationWorker.getLinkTrees(relation, true, "GENDER", null, null, null));
				
		} else if (label.equals("TREES_BY_KIN")){
				
			result = Value.valueOf(RelationWorker.getLinkTrees(relation, false, "KIN", criteria.getPattern(), criteria.getRelationModelNames(), criteria.getChainClassification()));
				
			// replace by "UNIT?"
		} else if  (label.equals("PLACE")){
				
			String placeValue = relation.getAttributeValue(criteria.getLocalUnitLabel());
			
			if (placeValue!=null){
				
				result = Value.valueOf(placeValue);
				
			} else {
				
				result = get(relation,criteria.getPlaceLabel(),criteria.getLevel(), criteria.getGeography());

			}

		} else if (label.equals ("TURNOVER")) {
			
			result = Value.valueOf(MathUtils.round(getTurnover(relation,(Relation)labelParameter,criteria.getRoleNames()), 2));

		} else if (label.equals("REGION")){
		
			Place region = criteria.getGeography().getSuperiorPlace(relation.getAttributeValue("END_PLACE"),criteria.getMinimalPlaceNames());
			
			if (region!=null){
				result = Value.valueOf(region.getToponym());
			}
			
		} else {
			
				result = get(relation, label, labelParameter, criteria.getGeography());			

		}
		
		//
		return result;
	}




	/**
	 * This method is a helper one.
	 * 
	 * @param individual
	 * @param label
	 * @return
	 */
	public static NumberedValues get(final Relations source, final String label, final Geography geography) {
		NumberedValues result;

		result = get(source, label, null, geography);

		//
		return result;
	}

	/**
	 * 
	 * @param individual
	 * @param label
	 * @return
	 */
	public static NumberedValues get(final Relations source, final String label, final Object parameter, final Geography geography) {
		NumberedValues result;

		//
		result = new NumberedValues();

		for (Relation relation : source) {
			result.put(relation.getId(), get(relation, label, parameter, geography));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param individuals
	 * @return
	 */
	public static List<String> getAttributeLabels(final Relations source) {
		List<String> result;

		result = getAttributeLabels(source, null);

		//
		return result;
	}

	/**
	 * @param individuals
	 * @return
	 */
	public static List<String> getAttributeLabels(final Relations source, final Integer limit) {
		List<String> result;

		//
		result = new ArrayList<String>(20);

		//
		for (EndogenousLabel label : EndogenousLabel.values()) {
			result.add(label.toString());
		}

		//
		result.addAll(getExogenousAttributeLabels(source, limit));

		//
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * 
	 * @param individuals
	 * @return
	 */
	public static List<String> getAttributeLabelSample(final Relations source) {
		List<String> result;

		result = getAttributeLabels(source, 10000);

		//
		return result;
	}

	/**
	 * @param individuals
	 * @return
	 */
	public static List<String> getExogenousAttributeLabels(final Relations source) {
		List<String> result;

		//
		result = getExogenousAttributeLabels(source, null);

		//
		return result;
	}

	/**
	 * @param individuals
	 * @return
	 */
	public static List<String> getExogenousAttributeLabels(final Relations source, final Integer limit) {
		List<String> result;

		//
		result = new ArrayList<String>(20);

		//
		HashSet<String> buffer = new HashSet<String>();
		if (source != null) {
			int index = 0;
			Iterator<Relation> iterator = source.iterator();
			while ((iterator.hasNext()) && ((limit == null) || (index < limit))) {
				Relation relation = iterator.next();
				for (Attribute attribute : relation.attributes()) {
					buffer.add(attribute.getLabel());
				}
			}
		}

		for (String string : buffer) {
			result.add(string);
		}

		//
		Collections.sort(result);

		//
		return result;
	}
	
	public static boolean isBirth (Object source) {
		boolean result;
		
		if (!(source instanceof Attributable)){
			result = false;
		} else {
			result = ((Attributable)source).getAttributeValue("END_PLACE")!=null && ((Attributable)source).getAttributeValue("START_PLACE")==null;
		}
		//
		return result;
	}
	
	public static boolean isDeath (Object source) {
		boolean result;
		
		if (!(source instanceof Attributable)){
			result = false;
		} else {
			result = ((Attributable)source).getAttributeValue("END_PLACE")==null && ((Attributable)source).getAttributeValue("START_PLACE")!=null;
		}
		//
		return result;
	}
	
	private static String getReducedRelationType (Relation event, Individual ego, String alterRoleName, String egoRoleName, Map<Individual,List<String>> relationsByAlter, String impersonalAlterLabel){
		String result;
		
		List<String> alters = new ArrayList<String>();
		List<String> allAlters = reduceRelations(getRelationTypes(event, ego, alterRoleName,egoRoleName,impersonalAlterLabel,relationsByAlter),alterRoleName);
		
		for (String alter : allAlters){
			boolean consider = true;
			if (alter.contains("FRIEND")){
				for (String otherAlter: allAlters){
					if (!otherAlter.contains("FRIEND")){
						consider = false;
						break;
					}
				}
				if (!consider){
					continue;
				}
			}
			alters.add(alter);
		}

		if (RelationValuator.isBirth(event)){
			result = "PARENTS";
		} else if (RelationValuator.isDeath(event)){
			result = "DEATH";
		} else if (alters.contains("TRANSITION")){
			result = "TRANSITION";
		} else if (PuckUtils.containsStrings(alters, "FATHER OR PATERNAL HOME;MOTHER OR MATERNAL HOME;PARENTS")){
			result = "PARENTS";
		} else if (PuckUtils.containsStrings(alters, "SIBLING OR FRATERNAL HOME;SIBLING")){
			result = "RELATIVE";
		} else if (PuckUtils.containsStrings(alters, "SPOUSE OR MARITAL HOME;SPOUSE")){
			result = "SPOUSE";
		} else if (PuckUtils.containsStrings(alters, "CHILD OR FILIAL HOME;CHILD")){
			result = "CHILD";
		} else if (PuckUtils.containsStrings(alters, "MASTER")){
			result = "MASTER";
		} else if (PuckUtils.containsStrings(alters, "EMPLOYER")){
			result = "EMPLOYER";
		} else if (PuckUtils.containsStrings(alters, "LANDLORD")){
			result = "RENT";
		} else if (PuckUtils.containsStrings(alters, "PRIEST")){
			result = "PRIEST";
		} else if (PuckUtils.containsStrings(alters, "RELATIVE;RELATIVE_AGNATIC;RELATIVE_UTERINE;RELATIVE_COGNATIC;RELATIVE_OR_AFFINE")){
			result = "RELATIVE";
		} else if (alters.contains("FRIEND")){
			result = "FRIEND";
		} else if (alters.contains("AFFINE")){
			result = "AFFINE";
		} else if (alters.contains("UNRELATED")){
			result = "UNRELATED";
		} else if (alters.contains("HOTEL-HOSTEL") || (alters.contains("WITHOUT FIXED DOMICILE"))){
			result = "PUBLIC";
		} else {
			result = "";
			for (String host : alters){
				host = host.replaceAll("RELATIVE_OR_AFFINE","RELATIVE").replaceAll("SIBLING", "RELATIVE").replaceAll("FATHER", "PARENTS").replaceAll("MOTHER", "PARENTS");
				if (host.equals("RELATIVES_MARITAL HOME")){
					result = "AFFINE";
				} else if (host.equals("SPOUSES_PATERNAL HOME")){
					result = "SPOUSE";
				} else if (host.equals("SPOUSES_MARITAL HOME")){
					result = "PARENTS";
				} else {
					String[] hostParts = host.split("S_");
					if (hostParts.length>1){
						host = "via "+hostParts[0];
					}
					result += host+" ";
				}
				// simplify
				if (result.contains("via PARENTS")){
					result = "via PARENTS";
				} else if (result.contains("via SPOUSE")){
					result = "via SPOUSE";
				} else if (result.contains("via RELATIVE")){
					result = "via RELATIVE";
				} else if (result.contains("via MASTER")){
					result = "via MASTER";
				} else if (result.lastIndexOf("via")>0){
					System.err.println("double reference "+result);
				}
			}
			if (result.contains("null")){
				result = "UNKNOWN";
				System.err.println(result+" "+event);
			}
		}
		if (result.contains("LODGER")){
			System.err.println("Inverted landlord-lodger relation for "+event);
		}
		//
		result = result.trim();
		
		//
		return result;
		
	}
	
	private static String getMigrationType (Relation event, Individual ego, String egoRoleName, Map<Individual,List<String>> relationsByAlter){
		String result;
		
		List<String> hosts = getRelationTypes(event,ego,"HOST",egoRoleName,"UNKNOWN",relationsByAlter);
		List<String> migs = getRelationTypes(event,ego,"MIG",egoRoleName,"UNKNOWN",relationsByAlter);
		
		if (RelationValuator.isBirth(event)){
			result = "BIRTH";
		} else if (RelationValuator.isDeath(event)){
			result = "DEATH";
		} else {

			String hostType = "OTHER";
			if (hosts.size()==0){
				hostType = "NOBODY";
			} else {
				String[] types = new String[]{"EGO","FATHER","MOTHER","SPOUSE","CHILD","SIBLING"};
				boolean end = false;
				for (String type : types){
					if (hosts.contains(type)){
						hostType = type;
						end = true;
						break;
					}
				}
				if (!end && (hosts.contains("RELATIVE_AGNATIC") || hosts.contains("RELATIVE_UTERINE") || hosts.contains("RELATIVE_COGNATIC")|| hosts.contains("RELATIVE"))){
					hostType = "KIN";
				} else if (!end && hosts.contains("AFFINE")){
					hostType = "AFFINE";
				}
			}
			
			String migType = "OTHER";
			if (migs.size()==0){
				migType = "NOBODY";
			} else {
				String[] types = new String[]{"EGO","FATHER","MOTHER","SPOUSE","CHILD","SIBLING"};
				boolean end = false;
				for (String type : types){
					if (migs.contains(type)){
						migType = type;
						end = true;
						break;
					}
				}
				if (!end && (migs.contains("RELATIVE_AGNATIC") || migs.contains("RELATIVE_UTERINE") || migs.contains("RELATIVE_COGNATIC") || migs.contains("RELATIVE"))){
					migType = "KIN";
				} else if (!end && migs.contains("AFFINE")){
					migType = "AFFINE";
				}

			}
			result = "WITH_"+migType+"_TO_"+hostType;
			result = result.replaceAll("WITH_NOBODY", "ALONE");
			
		}
		
		//
		return result;
	}
	
	private static List<String> getRelationTypes (Individuals alters, String roleName, Map<Individual,List<String>> relationsByAlter){
		List<String> result;
		
		result = new ArrayList<String>();
		for (Individual alter : alters){
			List<String> relations = relationsByAlter.get(alter);
			if (relations!=null){
				for (String relation : relations){
					if (!result.contains(relation)){
						result.add(relation);
					}
				}
			}
		}
		//
		Collections.sort(result);
		
		//
		return result;
	}
	
	public static List<String> getImpersonalRelationTypes (Relation event, Individual ego, String egoRoleName, String impersonalLabel, Map<Individual,List<String>> relationsByAlter){
		List<String> result;
		
		result = new ArrayList<String>();
		
		Actor egoActor = event.actors().get(ego.getId(), egoRoleName);
		
		if (egoActor.attributes()!=null && egoActor.getAttributeValue(impersonalLabel)!=null){
			
			result.add(egoActor.getAttributeValue(impersonalLabel));
						
		} else {
			
			for (Actor alterActor : event.actors().getByRole(egoRoleName)){
				
				if (alterActor!=null && !alterActor.equals(egoActor) && alterActor.attributes()!=null){
					
					String relation = alterActor.getAttributeValue(impersonalLabel);
					
					if (relation!=null){
						
						List<String> relations = relationsByAlter.get(alterActor.getIndividual());
						
						if (relations!=null){
						
							for (String egoRelation : relations){
							
								egoRelation+="S_"+relation;
								
								if (egoRelation.equals("SPOUSES_PATERNAL HOME")){
									egoRelation = "MARITAL HOME";
								} else if (egoRelation.equals("SPOUSES_MARITAL HOME")){
									egoRelation = "PATERNAL HOME";
								}

								
								if (!result.contains(egoRelation)){
									result.add(egoRelation);
								}
							}
						}
					}
				}
			}
		}
		//
		return result;
	}
	
	private static List<String> getRelationTypes (Relation event, Individual ego, String alterRoleName, String egoRoleName, String impersonalLabel, Map<Individual,List<String>> relationsByAlter){
		List<String> result;
		
		Individuals alters = event.getIndividuals(alterRoleName);
		if (alterRoleName.equals("MIG")){
			alters.removeById(ego.getId());
		}

		result = getRelationTypes(alters, alterRoleName, relationsByAlter);

		if (alters.size()==0){
			
			result.addAll(getImpersonalRelationTypes(event,ego,egoRoleName,impersonalLabel,relationsByAlter));
		}  
		

		//
		return result;
	}
	
	private static String getChildMigrationType (Individual ego, Relation event, String egoRoleName, Integer threshold, Map<Individual,List<String>> relationsByAlter) {
		String result;
		
		List<String> hosts = getRelationTypes(event,ego,"HOST",egoRoleName,"UNKNOWN",relationsByAlter);
		List<String> migs = getRelationTypes(event,ego,"MIG",egoRoleName,"UNKNOWN",relationsByAlter);
		
		result = null;
		
		if (RelationValuator.isBirth(event)){
			result = "BIRTH";
		} else if (getAge(ego,event)<=threshold){
			if (!hosts.contains("FATHER") && !hosts.contains("MOTHER") && !migs.contains("FATHER") && !migs.contains("MOTHER")){
				result = "NOPARENTS";
			}
		}
		//
		return result;
	}
	
	private static Double getTurnover (Relation event, Relation previousEvent, List<String> roleNames){
		Double result;
		
		result = 0.;
		
//		Relation previousEvent = getPreviousEvent(event);
		
		if (previousEvent!=null){
			
			Individuals previousAlters = previousEvent.getIndividuals(roleNames);
			Individuals currentAlters = event.getIndividuals(roleNames);

			Double n = new Double(previousAlters.size());
			
			for (Individual individual: previousAlters){
				if (currentAlters.contains(individual)){
					result++;
				}
			}
			
			result = result/n;
			
		}
		
		//
		return result;
		
	}
	
	
	public static Integer getAge (Individual ego, Relation event){
		Integer result;
		
		result = null;

		if (event!=null && getYear(event)!=null){
			result = IndividualValuator.ageAtYear(ego, getYear(event));
		}
		//
		return result;
	}
	
	public static Integer getYear (Relation event){
		Integer result;
		
		result = null;
		
		if (event!=null){
			result = IndividualValuator.extractYearAsInt(event.getAttributeValue("DATE"));
		}
		//
		return result;
	}
	
	private static String getRelationTypesAsShortCutString (Relation event, Individual ego, String roleName, String egoRoleName, Map<Individual,List<String>> relationsByAlters){
		String result;
		
		result = Trafo.asShortCutString(getRelationTypes(event, ego, roleName, egoRoleName, "UNKNOWN",relationsByAlters), 2);
		
		if (result == null && roleName.equals("MIG")){
			result = "SG";
		}
		
		//
		return result;
	}

	public static List<String> reduceRelations (List<String> relations, String alterRole){
		List<String> result;
		
		result = new ArrayList<String>();
		
		for (String relation : relations){
			String reducedRelation = null;
			if (relation.equals("EGO")){
				reducedRelation = "OWN HOME";
			} else if ((alterRole.equals("HOST") && relation.equals("FATHER")) || relation.equals("PATERNAL HOME")){
				reducedRelation = "FATHER OR PATERNAL HOME";
			} else if ((alterRole.equals("HOST") && relation.equals("CHILD")) ||  relation.equals("FILIAL HOME")){
				reducedRelation = "CHILD OR FILIAL HOME";
			} else if ((alterRole.equals("HOST") && relation.equals("MOTHER")) || relation.equals("MATERNAL HOME")){
				reducedRelation = "MOTHER OR MATERNAL HOME";
			} else if ((alterRole.equals("HOST") && relation.equals("SIBLING")) || relation.equals("FRATERNAL HOME")){
				reducedRelation = "SIBLING OR FRATERNAL HOME";
			} else if ((alterRole.equals("HOST") && relation.equals("SPOUSE")) || relation.equals("MARITAL HOME")){
				reducedRelation = "SPOUSE OR MARITAL HOME";
			} else if (relation.equals("STATE") || relation.equals("NGO")){
				reducedRelation = "STATE OR NGO";
			} else if (relation.equals("WORKPLACE") || relation.equals("HOMELESS")){
				reducedRelation = "WITHOUT FIXED DOMICILE";
			} else if (relation.equals("FRIEND") || relation.equals("SUPPORTER") || relation.equals("SUPPORTED") || relation.equals("EMPLOYERS_EMPLOYEE") || relation.equals("LANDLORDS_LODGER") || relation.equals("MASTERS_APPRENTICE") || relation.equals("FRIENDS_FRIEND")){
				reducedRelation = "FRIEND OR COLLEGUE";
			} else if (relation.contains("UNRELATEDS_")){
				reducedRelation = "UNRELATED";
			} else if (relation.contains("S_")){
				reducedRelation = relation.replaceAll("_AGNATIC", "").replaceAll("_UTERINE", "").replaceAll("_COGNATIC", "").replaceAll("RELATIVE", "RELATIVE_OR_AFFINE");
			} else {
				reducedRelation = relation;
			}

			reducedRelation = reduceRelation(reducedRelation);
			if (!result.contains(reducedRelation)){
				result.add(reducedRelation);
			}
		}
		//
		return result;
	}
	
	private static String reduceRelation (String relation){
		String result;
		
		result = null;
		
		if (relation.equals("FATHER OR PATERNAL HOME") || relation.equals("MOTHER OR MATERNAL HOME")){
			result = "PARENTS";
		} else if (relation.equals("SPOUSE OR MARITAL HOME")){
			result = "SPOUSE";
		} else if (relation.equals("CHILD OR FILIAL HOME")){
			result = "CHILD";
		} else if (relation.equals("LANDLORD")){
			result = "RENT";
		} else if (relation.equals("HOTEL-HOSTEL") || (relation.equals("WITHOUT FIXED DOMICILE"))){
			result = "PUBLIC";
		} else {
			result = relation.replaceAll("RELATIVE_OR_AFFINE","RELATIVE").replaceAll("FATHER", "PARENTS").replaceAll("MOTHER", "PARENTS");

			if (result.equals("RELATIVES_MARITAL HOME")){
				result = "AFFINE";
			} else if (result.equals("SIBLINGS_MARITAL HOME")){
				result = "AFFINE";
			} else if (result.equals("SPOUSES_PATERNAL HOME")){
				result = "SPOUSE";
			} else if (result.equals("SPOUSES_MARITAL HOME")){
				result = "PARENTS";
			} else {
				String[] relationParts = result.split("S_");
				if (relationParts.length>1){
					result = "via "+relationParts[0];
					// simplify
					if (result.contains("via PARENTS")){
						result = "via PARENTS";
					} else if (result.contains("via SPOUSE")){
						result = "via SPOUSE";
					} else if (result.contains("via SIBLING")){
						result = "via SIBLING";
					} else if (result.contains("via RELATIVE")){
						result = "via RELATIVE";
					} else if (result.contains("via MASTER")){
						result = "via MASTER";
					} else if (result.lastIndexOf("via")>0){
						System.err.println("double reference "+result);
					}
				}
			}  
		}
		//
		return result;
	}
	

	


}
