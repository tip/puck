package org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.containers;

import java.util.*;
import org.gephi.graph.api.Node;
import org.tip.puck.visualization.layouts.GraphSource;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.ColoredChain;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.NodeLayoutData;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.Triad;

/**
 * @date Jun 24, 2012
 *
 * @author Edoardo Savoia
 */
public class BorderContainer extends ChainContainer {

    private Map<Node, Triad> triads = new HashMap<Node, Triad>();
    

    protected BorderContainer(ColoredChain chain) {
	super(chain);
    }

    @Override
    public void populateChildren(GraphSource graphSource) {
	children = new HashSet<ChainContainer>();
	final Node bottomNode = chain.getBottomNode();
	Set<Node> bootomChildren = graphSource.getChildren(bottomNode);
	for (Node child : bootomChildren) {
	    if (NodeLayoutData.isColored(child, NodeLayoutData.Color.RED)) {
		// child has two parents (bottomNode, otherParent)
		// cases:
		//	otherParent belongs to a cluster
		//	    create triad
		//	otherParent belongs to the same cluster of bottomNode
		//	    create an internal triad (left or right)
		//	otherParent doesn't belong to a cluster
		//	    annotate bootomNode with its own cluster
		//	    (triad will be created by otherParent cluster)
		Node otherParent = graphSource.getOtherParent(child, bottomNode);
		NodeLayoutData.setContainer(bottomNode, this);
		BorderContainer container = NodeLayoutData.getContainer(otherParent);
		if (container != null) {
		    BorderContainer source;
		    BorderContainer target;

		    int thisLevel = this.chain.getHeight()+this.origin;
		    int otherLevel = container.chain.getHeight()+container.origin;
		    if (thisLevel > otherLevel) {
			source = this;
			target = container;
		    }else if (thisLevel < otherLevel){
			source = container;
			target = this;
		    }else {
			int thisSize = rootCluster.getSize();
			int otherSize = container.getRootCluster().getSize();
			if (thisSize > otherSize) {
			    source = this;
			    target = container;
			} else {
			    source = container;
			    target = this;
			}
		    }
		    Triad triad = new Triad(source, target);
		    triad.addChildren(child);
		    addTriad(triad, otherParent);
		    container.addTriad(triad, bottomNode);
		}
	    }
	    else {
		ChainContainer container = getContainer(child, graphSource);
		container.setRootCluster(rootCluster);
		container.setOrigin(origin+chain.getHeight()+1);
		if (container instanceof BorderContainer) {
		    rootCluster.addBorder((BorderContainer) container);
		}
		this.children.add(container);
	    }
	}
	// propagate
	for (ChainContainer child : children) {
	    child.populateChildren(graphSource);
	}

	populateTriadsClusters(graphSource);
    }

    public void populateTriadsClusters(GraphSource graphSource) {
	Collection<Triad> values = triads.values();
	for (Triad triad : values) {
	    triad.buildClusters(graphSource);
	}
    }

    public Node getBottomNode() {
	return chain.getBottomNode();
    }

    @Override
    public Set<ChainContainer> getChildren() {
	Set<ChainContainer> superChildren = super.getChildren();
	Set<ChainContainer> newChildren = new HashSet<ChainContainer>(superChildren);
	for (Map.Entry<Node, Triad> entry : triads.entrySet()) {
	    Triad triad = entry.getValue();
	    if (triad.isOn(this) && triad.isBuilt()) {
		Set<ChainContainer> clusters = triad.getClusters();
		newChildren.addAll(clusters);
	    }
	}
	return newChildren;
    }

    public void addTriad(Triad triad, Node otherParent) {
	if (!triads.containsKey(otherParent)) {
	    triads.put(otherParent, triad);
	}
	else {
	    Triad otherTriad = triads.get(otherParent);
	    Set<Node> childrenT = triad.getChildren();
	    for (Node node : childrenT) {
		otherTriad.addChildren(node);
	    }
	}
    }

    public Triad[] getTriads(){
	Collection<Triad> values = triads.values();
	Triad[] toArray = values.toArray(new Triad[values.size()]);
	Arrays.sort(toArray, new Comparator<Triad>(){
	    @Override
	    public int compare(Triad o1, Triad o2) {
		return o2.getChildren().size()-o1.getChildren().size();
	    }

	});

	return toArray;
    }

    public Map<Node, Triad> getTriadsMap() {
	return Collections.unmodifiableMap(triads);
    }

    @Override
    public int getHeight() {
	int result = chain.getHeight();
	int maxHeight = 0;
	if (!isLeaf()) {
	    Set<ChainContainer> newChildren = getChildren();
	    for (ChainContainer child : newChildren) {
		maxHeight = Math.max(maxHeight, child.getHeight());
	    }
	}
	return result + maxHeight;
    }

    @Override
    public int getWidth() {
	int width = 0;
	if (!isLeaf()) {
	    Set<ChainContainer> newChildren = getChildren();
	    for (ChainContainer child : newChildren) {
		width += child.getWidth();
	    }
	}
	return Math.max(1, width);
    }

    @Override
    public int getSize() {
	int size = chain.getHeight();
	if (!isLeaf()) {
	    Set<ChainContainer> newChildren = getChildren();
	    for (ChainContainer chainContainer : newChildren) {
		size += chainContainer.getSize();
	    }
	}
	return size;
    }

}
