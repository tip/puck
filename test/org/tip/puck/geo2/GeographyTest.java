package org.tip.puck.geo2;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.geo.Coordinate2;
import org.tip.puck.geo.Geography;
import org.tip.puck.geo.Place;

/**
 * 
 * @author TIP
 */
public class GeographyTest {
	private static final Logger logger = LoggerFactory.getLogger(GeographyTest.class);

	/**
     */
	@Test
	public void testGeography01() throws Exception {

		logger.debug("========================== testLoad01");

		Geography source = new Geography();

		Place massy = new Place("Massy");
		massy.setCoordinate2(new Coordinate2(48.730946, 2.271316, 100));
		source.addPlace(massy);

		Place nice = new Place("Nice");
		nice.setCoordinate2(new Coordinate2(43.695949, 7.271413, 100));
		source.addPlace(nice);

		Assertions.assertThat(source).isNotNull();
	}
}
