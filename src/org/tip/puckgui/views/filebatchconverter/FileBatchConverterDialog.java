package org.tip.puckgui.views.filebatchconverter;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.tip.puck.net.workers.FileBatchConverterCriteria;
import org.tip.puck.net.workers.FileBatchConverterCriteria.Mode;
import org.tip.puck.net.workers.FileBatchConverterCriteria.TargetFormat;
import org.tip.puckgui.PuckGUI;
import org.tip.puckgui.views.OpenEncodingWindow;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class FileBatchConverterDialog extends JDialog {

	private static final long serialVersionUID = -1172607875942100423L;
	private final JPanel contentPanel = new JPanel();
	private final ButtonGroup btngrpMode = new ButtonGroup();
	private JTextField txtfldTargetDirectory;
	private JTextField txtfldSourceDirectory;
	private JRadioButton rdbtnSkip;
	private JRadioButton rdbtnPUC;
	private JRadioButton rdbtnIURODS;
	private final ButtonGroup btngrpTargetFormat = new ButtonGroup();
	private JRadioButton rdbtnIURTXT;
	private JRadioButton rdbtnIURXLS;
	private JRadioButton rdbtnIURTXTS;
	private JRadioButton rdbtnBARODS;
	private JRadioButton rdbtnBARTXT;
	private JRadioButton rdbtnBARXLS;
	private JRadioButton rdbtnGED;
	private JRadioButton rdbtnOverwrite;
	private FileBatchConverterCriteria dialogCriteria;
	private JComboBox cmbxCharset;

	/**
	 * Create the dialog.
	 */
	public FileBatchConverterDialog() {
		super();

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("File Batch Converter");
		setIconImage(Toolkit.getDefaultToolkit().getImage(FileBatchConverterDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent event) {
				// Closing window.
				// Cancel button.
				setVisible(false);
			}
		});

		setBounds(100, 100, 549, 443);
		getContentPane().setLayout(new BorderLayout());
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "About File Batch Converter", TitledBorder.LEADING, TitledBorder.TOP,
					null, new Color(51, 51, 51)));
			getContentPane().add(panel, BorderLayout.NORTH);
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			{
				JTextPane txtpnIntroduction = new JTextPane();
				txtpnIntroduction.setEditable(false);
				txtpnIntroduction.setText("File Batch Converter allows to convert several files to one target format.");
				panel.add(txtpnIntroduction);
			}
			{
				JPanel panel_1 = new JPanel();
				panel.add(panel_1);
				panel_1.setLayout(new BorderLayout(0, 0));
				{
					JPanel panel_2 = new JPanel();
					panel_1.add(panel_2);
					panel_2.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.LABEL_COMPONENT_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
							FormFactory.LABEL_COMPONENT_GAP_COLSPEC, ColumnSpec.decode("350px:grow"), FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
							FormFactory.DEFAULT_COLSPEC, FormFactory.LABEL_COMPONENT_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
							FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
							RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, }));
					{
						JLabel lblSource = new JLabel("Source:");
						panel_2.add(lblSource, "2, 2, left, center");
					}
					{
						this.txtfldSourceDirectory = new JTextField();
						this.txtfldSourceDirectory.setEditable(false);
						panel_2.add(this.txtfldSourceDirectory, "4, 2, fill, center");
						this.txtfldSourceDirectory.setColumns(10);
					}
					{
						JButton button = new JButton("…");
						button.addActionListener(new ActionListener() {
							/**
							 * 
							 */
							@Override
							public void actionPerformed(final ActionEvent event) {
								// Source Directory Selector.
								File source = FileBatchConverterSourceDirectorySelector.showSelectorDialog(FileBatchConverterDialog.this, new File(
										FileBatchConverterDialog.this.txtfldSourceDirectory.getText()));

								FileBatchConverterDialog.this.txtfldSourceDirectory.setText(source.getAbsolutePath());
							}
						});
						panel_2.add(button, "6, 2, left, top");
					}
					{
						JLabel lblCharset = new JLabel("Charset:");
						panel_2.add(lblCharset, "2, 4, right, default");
					}
					{
						this.cmbxCharset = new JComboBox(buildCharsetLabels().toArray());
						this.cmbxCharset.setSelectedItem("Standard UTF-8 (UTF8)");
						panel_2.add(this.cmbxCharset, "4, 4, fill, default");
					}
					{
						JLabel lblTarget = new JLabel("Target:");
						panel_2.add(lblTarget, "2, 6, right, center");
					}
					{
						this.txtfldTargetDirectory = new JTextField();
						this.txtfldTargetDirectory.setEditable(false);
						this.txtfldTargetDirectory.setColumns(10);
						panel_2.add(this.txtfldTargetDirectory, "4, 6, fill, center");
					}
					{
						JButton button = new JButton("…");
						button.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(final ActionEvent event) {
								// Target Directory Selector.
								File target = FileBatchConverterTargetDirectorySelector.showSelectorDialog(FileBatchConverterDialog.this, new File(
										FileBatchConverterDialog.this.txtfldTargetDirectory.getText()));

								FileBatchConverterDialog.this.txtfldTargetDirectory.setText(target.getAbsolutePath());
							}
						});
						panel_2.add(button, "6, 6, left, top");
					}
				}
			}
		}
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new BoxLayout(this.contentPanel, BoxLayout.X_AXIS));
		{
			JPanel panel = new JPanel();
			this.contentPanel.add(panel);
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			{
				JPanel panel_1 = new JPanel();
				panel.add(panel_1);
				panel_1.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));
				{
					JLabel lblTargetFormat = new JLabel("Target format:");
					panel_1.add(lblTargetFormat, "2, 2");
				}
				{
					this.rdbtnPUC = new JRadioButton("PUC ( .puc)");
					this.btngrpTargetFormat.add(this.rdbtnPUC);
					panel_1.add(this.rdbtnPUC, "2, 4");
				}
				{
					this.rdbtnIURODS = new JRadioButton("IUR ODS ( .iur.ods)");
					this.btngrpTargetFormat.add(this.rdbtnIURODS);
					panel_1.add(this.rdbtnIURODS, "2, 6");
				}
				{
					this.rdbtnBARODS = new JRadioButton("BAR ODS ( .bar.ods)");
					this.btngrpTargetFormat.add(this.rdbtnBARODS);
					panel_1.add(this.rdbtnBARODS, "4, 6");
				}
				{
					this.rdbtnIURTXT = new JRadioButton("IUR TXT ( .iur.txt)");
					this.btngrpTargetFormat.add(this.rdbtnIURTXT);
					panel_1.add(this.rdbtnIURTXT, "2, 8");
				}
				{
					this.rdbtnBARTXT = new JRadioButton("BAR TXT ( .bar.txt)");
					this.btngrpTargetFormat.add(this.rdbtnBARTXT);
					panel_1.add(this.rdbtnBARTXT, "4, 8");
				}
				{
					this.rdbtnIURXLS = new JRadioButton("IUR XLS ( .iur.xls)");
					this.btngrpTargetFormat.add(this.rdbtnIURXLS);
					panel_1.add(this.rdbtnIURXLS, "2, 10");
				}
				{
					this.rdbtnBARXLS = new JRadioButton("BAR XLS ( .bar.xls)");
					this.btngrpTargetFormat.add(this.rdbtnBARXLS);
					panel_1.add(this.rdbtnBARXLS, "4, 10");
				}
				{
					this.rdbtnIURTXTS = new JRadioButton("IUR TXT Splitted ( .iurs.txt)");
					this.btngrpTargetFormat.add(this.rdbtnIURTXTS);
					panel_1.add(this.rdbtnIURTXTS, "2, 12");
				}
				{
					this.rdbtnGED = new JRadioButton("Gedcom ( .ged)");
					this.btngrpTargetFormat.add(this.rdbtnGED);
					panel_1.add(this.rdbtnGED, "4, 12");
				}
			}
			{
				JSeparator separator = new JSeparator();
				separator.setOrientation(SwingConstants.VERTICAL);
				panel.add(separator);
			}
			{
				JPanel panel_1 = new JPanel();
				panel.add(panel_1);
				panel_1.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, }));
				{
					JLabel lblMode = new JLabel("Mode:");
					panel_1.add(lblMode, "2, 2");
				}
				{
					this.rdbtnSkip = new JRadioButton("Skip");
					panel_1.add(this.rdbtnSkip, "2, 4");
					this.btngrpMode.add(this.rdbtnSkip);
				}
				{
					this.rdbtnOverwrite = new JRadioButton("Overwrite");
					this.btngrpMode.add(this.rdbtnOverwrite);
					panel_1.add(this.rdbtnOverwrite, "2, 6");
				}
				{
					JPanel panel_3 = new JPanel();
					panel_1.add(panel_3, "2, 7");
					panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.Y_AXIS));
				}
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton btnCancel = new JButton("Cancel");
				btnCancel.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						FileBatchConverterDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				btnCancel.setActionCommand("Cancel");
				buttonPane.add(btnCancel);
			}
			{
				JButton btnConvert = new JButton("Convert");
				btnConvert.addActionListener(new ActionListener() {
					/**
					 * 
					 */
					@Override
					public void actionPerformed(final ActionEvent event) {
						//
						FileBatchConverterDialog.this.dialogCriteria = getCriteria();

						if (FileBatchConverterDialog.this.dialogCriteria != null) {
							PuckGUI.instance().getPreferences().setFileBatchConverterCriteria(FileBatchConverterDialog.this.dialogCriteria);
							setVisible(false);
						}

						// try {
						//
						// } catch (Exception exception) {
						// //
						// String title = "Invalid input";
						// String message = "Please, select a dataset.";
						//
						// //
						// JOptionPane.showMessageDialog(null, message, title,
						// JOptionPane.ERROR_MESSAGE);
						// }
					}
				});
				{
					Component horizontalStrut = Box.createHorizontalStrut(20);
					buttonPane.add(horizontalStrut);
				}
				{
					JButton btnReset = new JButton("Reset");
					btnReset.addActionListener(new ActionListener() {
						/**
						 * 
						 */
						@Override
						public void actionPerformed(final ActionEvent event) {
							// Browse dataset.
							setCriteria(new FileBatchConverterCriteria());
						}
					});
					btnReset.setActionCommand("OK");
					buttonPane.add(btnReset);
				}
				{
					Component horizontalStrut = Box.createHorizontalStrut(20);
					buttonPane.add(horizontalStrut);
				}
				btnConvert.setActionCommand("OK");
				buttonPane.add(btnConvert);
				getRootPane().setDefaultButton(btnConvert);
			}
		}

		// ////////////////////////
		this.dialogCriteria = null;
		FileBatchConverterCriteria criteria = PuckGUI.instance().getPreferences().getFileBatchConverterCriteria();

		if (criteria == null) {
			setCriteria(new FileBatchConverterCriteria());
		} else {
			setCriteria(criteria);
		}
	}

	/**
	 * 
	 * @return
	 */
	public FileBatchConverterCriteria getCriteria() {
		FileBatchConverterCriteria result;

		result = new FileBatchConverterCriteria();

		result.setSourceDirectory(new File(this.txtfldSourceDirectory.getText()));
		result.setTargetDirectory(new File(this.txtfldTargetDirectory.getText()));

		if (this.rdbtnOverwrite.isSelected()) {
			result.setMode(Mode.OVERWRITE);
		} else {
			result.setMode(Mode.SKIP);
		}

		if (this.rdbtnBARODS.isSelected()) {
			result.setTargetFormat(TargetFormat.BARODS);
		} else if (this.rdbtnBARTXT.isSelected()) {
			result.setTargetFormat(TargetFormat.BARTXT);
		} else if (this.rdbtnBARXLS.isSelected()) {
			result.setTargetFormat(TargetFormat.BARXLS);
		} else if (this.rdbtnIURODS.isSelected()) {
			result.setTargetFormat(TargetFormat.IURODS);
		} else if (this.rdbtnIURTXT.isSelected()) {
			result.setTargetFormat(TargetFormat.IURTXT);
		} else if (this.rdbtnIURXLS.isSelected()) {
			result.setTargetFormat(TargetFormat.IURXLS);
		} else if (this.rdbtnIURTXTS.isSelected()) {
			result.setTargetFormat(TargetFormat.IURTXTS);
		} else if (this.rdbtnGED.isSelected()) {
			result.setTargetFormat(TargetFormat.GEDCOM);
		} else if (this.rdbtnPUC.isSelected()) {
			result.setTargetFormat(TargetFormat.PUC);
		}

		String charsetName = OpenEncodingWindow.charsets()[this.cmbxCharset.getSelectedIndex()][0];
		result.setCharsetName(charsetName);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public FileBatchConverterCriteria getDialogCriteria() {
		FileBatchConverterCriteria result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final FileBatchConverterCriteria source) {
		//
		if (source != null) {

			if (source.getSourceDirectory() != null) {
				this.txtfldSourceDirectory.setText(source.getSourceDirectory().getAbsolutePath());
			}

			if (source.getTargetDirectory() != null) {
				this.txtfldTargetDirectory.setText(source.getTargetDirectory().getAbsolutePath());
			}

			switch (source.getMode()) {
				case OVERWRITE:
					this.rdbtnOverwrite.setSelected(true);
				break;

				case SKIP:
					this.rdbtnSkip.setSelected(true);
				break;
			}

			switch (source.getTargetFormat()) {
				case BARODS:
					this.rdbtnBARODS.setSelected(true);
				break;

				case BARTXT:
					this.rdbtnBARTXT.setSelected(true);
				break;

				case BARXLS:
					this.rdbtnBARXLS.setSelected(true);
				break;

				case GEDCOM:
					this.rdbtnGED.setSelected(true);
				break;

				case IURODS:
					this.rdbtnIURODS.setSelected(true);
				break;

				case IURTXT:
					this.rdbtnIURTXT.setSelected(true);
				break;

				case IURTXTS:
					this.rdbtnIURTXTS.setSelected(true);
				break;

				case IURXLS:
					this.rdbtnIURXLS.setSelected(true);
				break;

				case PUC:
					this.rdbtnPUC.setSelected(true);
				break;
			}
		}
	}

	/**
	 * Builds the charset labels.
	 * 
	 * @return the string list
	 */
	public static StringList buildCharsetLabels() {
		StringList result;

		String[][] charsets = OpenEncodingWindow.charsets();

		result = new StringList(charsets.length);

		for (String[] charset : charsets) {
			String label = String.format("%s (%s)", charset[1], charset[0]);
			result.add(label);
		}

		result.sort();

		//
		return result;
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		/* AttributeFilter criteria = */showDialog();
	}

	/**
	 * Launch the application.
	 */
	public static FileBatchConverterCriteria showDialog() {
		FileBatchConverterCriteria result;

		//
		FileBatchConverterDialog dialog = new FileBatchConverterDialog();
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
