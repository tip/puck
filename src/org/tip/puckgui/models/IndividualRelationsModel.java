package org.tip.puckgui.models;

import java.util.List;
import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;

import org.tip.puck.net.Individual;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Actors;
import org.tip.puck.net.relations.Relation;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class IndividualRelationsModel extends AbstractTableModel {

	private static final long serialVersionUID = -2390171347020856596L;

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.tip.puckgui.messages");

	protected Individual source;
	protected List<Relation> delegate;

	/**
	 * 
	 */
	public IndividualRelationsModel(final Individual source) {
		super();
		setSource(source);
	}

	/**
	 * 
	 */
	@Override
	public Class getColumnClass(final int columnIndex) {
		Class result;

		switch (columnIndex) {
			case 0:
				result = Number.class;
			break;
			case 1:
				result = String.class;
			break;
			case 2:
				result = String.class;
			break;
			case 3:
				result = String.class;
			break;
			case 4:
				result = String.class;
			break;
			default:
				result = String.class;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getColumnCount() {
		int result;

		result = 5;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String getColumnName(final int columnIndex) {
		String result;

		switch (columnIndex) {
			case 0:
				result = BUNDLE.getString("MainWindow.individual.relation.id");
			break;
			case 1:
				result = BUNDLE.getString("MainWindow.individual.relation.model");
			break;
			case 2:
				result = BUNDLE.getString("MainWindow.individual.relation.name");
			break;
			case 3:
				result = BUNDLE.getString("MainWindow.individual.relation.role");
			break;
			case 4:
				result = BUNDLE.getString("MainWindow.individual.relation.others");
			break;
			default:
				result = "";
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Relation> getDelegate() {
		List<Relation> result;

		result = this.delegate;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getRowCount() {
		int result;

		if (this.source == null) {
			result = 0;
		} else {
			result = this.delegate.size();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individual getSource() {
		Individual result;

		result = this.source;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Object getValueAt(final int rowIndex, final int columnIndex) {
		Object result;

		if (this.source == null) {
			//
			result = null;

		} else if (rowIndex >= this.delegate.size()) {
			//
			result = null;

		} else {
			//
			switch (columnIndex) {
				case 0:
					result = this.delegate.get(rowIndex).getTypedId();
				break;
				case 1:
					result = this.delegate.get(rowIndex).getModel().getName();
				break;
				case 2:
					result = this.delegate.get(rowIndex).getName();
				break;
				case 3:
					Actors actors = this.delegate.get(rowIndex).actors().getById(this.source.getId());
					StringList roleNames = new StringList();
					for (Actor actor : actors) {
						//
						String formattedRole;
						if (actor.getRelationOrder() == null) {
							//
							formattedRole = actor.getRole().getName();
						} else {
							//
							formattedRole = String.format("%s(%d)", actor.getRole().getName(), actor.getRelationOrder());
						}
						roleNames.append(formattedRole);
					}
					result = roleNames.toStringWithCommas();
				break;
				case 4:
					Actors others = this.delegate.get(rowIndex).actors().getOthers(this.source.getId());
					switch (others.size()) {
						case 0:
							result = "";
						break;
						case 1:
							result = String.format("(%d) %s", others.get(0).getId(), others.get(0).getName());
						break;
						default:
							result = String.format("%d actors", others.size());
					}
				break;
				default:
					result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public boolean isCellEditable(final int row, final int col) {
		boolean result;

		result = false;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void setSource(final Individual source) {
		//
		this.source = source;
		if (source != null) {
			//
			this.delegate = source.relations().toList();
		}

		//
		fireTableDataChanged();
	}
}
