package org.tip.puckgui.views;

import java.awt.Component;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puckgui.NetGUI;

/**
 * 
 * @author cpm
 */
public class ConfirmMainWindowCloseDialog {

	private static final Logger logger = LoggerFactory.getLogger(ConfirmMainWindowCloseDialog.class);

	/**
	 * 
	 * @param parent
	 * @return
	 */
	public static boolean showDialog(final Component parent, final NetGUI netGUI) {
		boolean result;

		//
		String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.closeConfirm.existingChange.title");
		String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.closeConfirm.existingChange.text");
		Object optionPaneIsYesLast = UIManager.get("OptionPane.isYesLast");
		UIManager.put("OptionPane.isYesLast", "false");
		// Warning: Bug with … UTF-8 character. So, use of "...".
		String buttons[] = { "Close without Saving", "Cancel", "Save As..." };
		int response = JOptionPane.showOptionDialog(null, message, title, JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, buttons,
				buttons[1]);
		UIManager.put("OptionPane.isYesLast", optionPaneIsYesLast);

		switch (response) {
			case 0:
				//
				logger.debug("Close without Saving");
				result = true;
			break;

			case 1:
				//
				logger.debug("Cancel overwrite");
				result = false;
			break;

			case 2:
				//
				logger.debug("Save As…");
				result = MainWindow.performSaveAs(parent, netGUI);
			break;

			default:
				result = false;
		}

		//
		return result;
	}
}