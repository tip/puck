package oldcore.trash;






public class RoleRelationWorker {
	
/*	private static boolean isComposable(RoleRelations target, RoleRelation relation, RoleActor alter,String alterRoleName,String mediusRoleName, String mediusAlterRoleName, Map<Role,Roles> genderConfigs, Report report){
		boolean result;
				
		RoleActor self = relation.getSelf();
				
		result = false;

		// Check sytematically correspondance of import and export criteria! 
		if (alter.getRole().getName().equals(alterRoleName)){
			
			RoleActors medii = relation.getActorsByRoleNameWithUnspecific(mediusRoleName,target);
			
			for (RoleActor medius: medii){
				
				RoleRelation mediusRelation = target.getSelfRelation(medius);
				
				if (medius.getRole().getName().equals(mediusRoleName)){
//				if (mediusRelation!=null && !medius.equalsAbsolute(alter) && mediusRelation.getActorsByRoleNameWithUnspecific(mediusAlterRoleName, target).containsAbsolute(alter)){
					
					result = true;
					
					// Priority criterion for symmetric relations (the older relation remains, the more recent is cut)
					if (alterRoleName.equals(mediusAlterRoleName) && mediusRelation!=null && mediusRelation.getId()<relation.getId()){
						result = false;
						break;
					}
					
					result = RelationModelMaker.isComposable(target, medius, alter, self, alterRoleName, mediusRoleName, mediusAlterRoleName, genderConfigs);
//					result = RelationModelMaker.isValidComposition(target, medius, alter, self, alterRoleName, mediusRoleName, mediusAlterRoleName, genderConfigs);
							
					// Special criteria:
					// 1. Half-orphan criterion for spouse's children: check uniqueness of medius
					if (alterRoleName.equals("CHILD") && mediusRoleName.equals("SPOUSE") && mediusAlterRoleName.equals("CHILD")){
							// Check for uniqueness of self for medius
							for (RoleActor otherSelf : mediusRelation.getActorsByRoleNameWithUnspecific(medius.getRole().invertName(), target)){
//								System.out.println(alter+" as child of "+medius+" or "+secondMedius+" "+secondMedius.getRole().isParallel()+" "+secondMedius.equalsAbsolute(medius)+" "+secondMedius.getAlterGender().matchs(medius.getAlterGender()));
								if (!otherSelf.equalsAbsolute(self) && otherSelf.getEgoGender().matchs(self.getEgoGender()) && otherSelf.getAlterGender().matchs(self.getAlterGender())){
									result = false;
									break;
								}
							}
							// Check for uniqueness of self for alter
							for (RoleActor otherSelf : alterRelation.getActorsByRoleNameWithUnspecific(alter.getRole().invertName(), target)){
//								System.out.println(alter+" as child of "+medius+" or "+secondMedius+" "+secondMedius.equalsAbsolute(medius)+" "+secondMedius.getAlterGender().matchs(medius.getAlterGender()));
								if (!otherSelf.equalsAbsolute(self) && otherSelf.getEgoGender().matchs(self.getEgoGender()) && otherSelf.getAlterGender().matchs(self.getAlterGender())){
									result = false;
									break;
								}
							}
						
						
					}
					
				}
			}
			if(result) {
				report.outputs().appendln(alter.getName()+"\tas "+alter.getRole()+" of\t"+relation.getSelfRole()+"\tredundant as "+mediusAlterRoleName+" of "+mediusRoleName+"\t"+medii);
			}
		}
		//
		return result;
	}

	
	private static boolean isComposedRelative(boolean previousResult, RoleRelations source, RoleRelations target, RoleRelation selfRelation, RoleActor actor,String targetRelation,String firstRelation, String secondRelation, Report report){
		boolean result;
		
		result = previousResult;
		
		if (result==false && actor.getRole().getName().equals(targetRelation)){
			RoleActors medii = selfRelation.getActorsByRoleNameWithUnspecific(firstRelation, target);
			RoleActors allMedii = selfRelation.getActorsByRoleNameWithUnspecific(firstRelation, source);
			if (!medii.isEmpty()){
				// Check for presence of generative basis
				for (RoleActor medius : medii){
					RoleRelation mediusRelation = target.getSelfRelation(medius);
//					if (selfRelation.getSelf().getName().equals("j�ye'") && targetRelation.equals("CHILD") && firstRelation.equals("SPOUSE")) report.outputs().appendln("wos is "+selfRelation+" "+medius+" "+mediusRelation.getActorsByRoleNameWithUnspecific(secondRelation, target));
					if (medius!=actor && mediusRelation!=null && mediusRelation.getActorsByRoleNameWithUnspecific(secondRelation, target).containsAbsolute(actor)){
						result = true;
						break;
					}
				}
				if (result){
					// Check for overall consistency
					for (RoleActor medius : allMedii){
						RoleRelation mediusRelation = source.getSelfRelation(medius);
						if (medius!=actor && mediusRelation!=null && !mediusRelation.getActorsByRoleNameWithUnspecific(secondRelation, target).containsAbsolute(actor)){
							result = false;
							break;
						}
					}
				}
			}
			if (result) report.outputs().appendln("\t"+actor+" as "+targetRelation+" of "+selfRelation.getSelf()+" redundant as "+secondRelation+" of "+firstRelation+" "+medii);
		}
		//
		return result;
	}
	
	private static boolean isRedundant(RoleRelations source, RoleRelations target, RoleActor self, RoleActor actor, Report report){
		boolean result;
				
		result = false;
						
		RoleRelation selfRelation = target.getSelfRelation(self);
		RoleRelation alterRelation = target.getSelfRelation(actor);
		
		// Role already defined in reciprocal way [retrieved by "add reciprocals" method)
		if (result==false && alterRelation!=null && actor!=self){
			RoleActors reciprocals = alterRelation.getActorsByRoleNameWithUnspecific(actor.getRole().invertName(), target);
			for (RoleActor reciprocal : reciprocals){
				if (self.equalsAbsolute(reciprocal)){
					result = true;
					report.outputs().appendln("\t"+actor+" for "+self+" redundant as reciprocal");
				}
			}
		}
		
		// Retrieved by transfer of uniform parents and siblings to siblings
		result = isComposedRelative(result,source,target,selfRelation,actor,"PARENT","SIBLING","PARENT",report);
		result = isComposedRelative(result,source,target,selfRelation,actor,"SIBLING","SIBLING","SIBLING",report);
		result = isComposedRelative(result,source,target,selfRelation,actor,"CHILD","CHILD","SIBLING",report);
		
		// Retrieved by transfer of half-orphan children to spouses (*check)
		result = isComposedRelative(result,source,target,selfRelation,actor,"CHILD","SPOUSE","CHILD",report);
		
		// Retrieved by closure of spouse relation for uniform co-parents
		result = isComposedRelative(result,source,target,selfRelation,actor,"SPOUSE","CHILD","PARENT",report);
		
		// Retrieved by closure of sibling relation for uniform co-children
		result = isComposedRelative(result,source,target,selfRelation,actor,"SIBLING","PARENT","CHILD",report);

		
		return result;
	}*/
	


	

}
