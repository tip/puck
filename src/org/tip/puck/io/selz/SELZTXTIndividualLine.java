package org.tip.puck.io.selz;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a line of individual from a TXT file.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class SELZTXTIndividualLine {
	public static final int NOID = 0;

	private int id;
	private String name;
	private char gender;
	private List<String> attributesValues;
	private int familyId;
	private int familyOrd;

	/**
	 * 
	 */
	public SELZTXTIndividualLine() {
		this.id = NOID;
		this.name = null;
		this.gender = 'X';
		this.attributesValues = new ArrayList<String>();
	}

	/**
	 * 
	 * @return
	 */
	public List<String> attributeValues() {
		List<String> result;

		result = this.attributesValues;

		//
		return result;
	}

	public int getFamilyId() {
		return familyId;
	}

	public int getFamilyOrd() {
		return familyOrd;
	}

	public void setFamilyOrd(int familyOrd) {
		this.familyOrd = familyOrd;
	}

	public char getGender() {
		return gender;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public void setFamilyId(int familyId) {
		this.familyId = familyId;
	}

	public void setGender(final char gender) {
		this.gender = gender;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public void setName(final String name) {
		this.name = name;
	}
}
