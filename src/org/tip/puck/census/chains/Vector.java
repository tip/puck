package org.tip.puck.census.chains;

import java.util.Arrays;

import org.tip.puck.util.MathUtils;

public class Vector implements Comparable<Vector> {
	
	private int[] numbers;
	int[] reducedNumbers;
	int[] apexGenders;
	
	private enum CompareCriterion {
		ORDER,
		LENGTH,
		LEFT_BRANCH_LENGTH,
		HETERO,
		EGOGENDER,
		GENDER_PROFILE;
	}
	
	public Vector(int order){
		setNumbers(new int[2*order]);
	}
	
	/**
	 * gets the length of a kinship chain with given characteristic number
	 * @param i the characteristic number of the chain
	 * @return the length of the chain
	 */
   private int getLength (int i){
	   return new Double(Math.floor(Math.log(number(i)+1)/Math.log(2))).intValue()-1;
   }
   
   public int order(){
	   return getNumbers().length/2;
   }
   
   private int number(int i){
	   return Math.abs(getNumbers()[i]);
   }
   
   private int reducedNumber(int i){
	   return reducedNumbers[i];
   }

   private int apexGender(int i){
	   return apexGenders[i];
   }
   
   public Vector (int[] numbers){
	   this.setNumbers(numbers);
   }
   
   public Vector (Vector vector){
	   this.setNumbers(vector.getNumbers());
   }
   
   public int hashCode (){
	   return toString().hashCode();
   }
   
   public boolean equals (Object otherVector){
	   boolean result = true;
	   
	   Vector other = (Vector)otherVector;
	   
	   if (order()!=other.order()){
		   result = false;
	   } else {
			for (int i=0;i<2*order();i++){
				if (number(i)!=other.number(i)) {
					result = false;
					break;
				}
			}
	   }
	   //
	   return result;
   }
   
   public String toString(){
	   return Arrays.toString(getNumbers());
   }

   /**
	 * gets the length of a kinship chain with given characteristic vector
	 * @param v the characteristic vector of the chain
	 * @return the length of the chain
	 */
  private int length (){
	   int j = 0;
	   for (int i=0;i<getNumbers().length;i++){
		   j = j + getLength(i);
	   }
	   return j + order() - 1;
  }
  
	/**
	 * permutes the vector of the ring
	 * @param i the position permuted to the first position
	 * @return the permuted vector

	 */
	public Vector transform (int i){
		int n = getNumbers().length;
		int[] v = new int[n];
		int j=0;
		if (i%2==0) {
			for (int k=i;k<i+n;k++){
				v[j]=getNumbers()[k%n];
				j++;
			}
		} else {
			for (int k=i;k>i-n;k--){
				v[j]=getNumbers()[(k+n)%n];
				j++;
			}
		}
		return new Vector(v);
	}
	
	//checks the egoGender criterion first;
	/**
	 * 
	 * @param other
	 * @return
	 */
	public int genderedCompareTo (Vector other){
		int result;
		
		result = compareTo(other,CompareCriterion.EGOGENDER);
		if (result == 0){
			result = compareTo(other);
		}
		//
		return result;
	}
	
	@Override
	public int compareTo(Vector other) {
		int result = 0;
		
		for (CompareCriterion criterion : CompareCriterion.values()){
			result = compareTo(other,criterion);
			if (result!=0){
				break;
			}
		}
		
		//
		return result;
	}
	
	private static int compare (int a, int b){
		return new Integer(a).compareTo(b);
	}
	
	// harmonize with Gender...
	/**
	 * 
	 * @return 1 for male, 0 for female
	 */
	private int egoGenderInt (){
		return Math.abs(getNumbers()[0]%2);
	}

	// harmonize with Gender...
	/**
	 * 
	 * @return 1 for male, 0 for female
	 */
	private int alterGenderInt (){
		return Math.abs(getNumbers()[getNumbers().length-1]%2);
	}
	
	private int compareGender (){
		return Math.abs(compare(egoGenderInt(),alterGenderInt()));
	}
	
	/**
	 * reduces the characteristic vector to a vector of characteristic numbers of linear chains up to the apical ancestors and adds an array of apical ancestor gender numbers
	 * <p> kinship relations that differ only in the sex of apical ancestors thus have the same reduced vector
	 * @param vec  the characteristic vector
	 * @return a table consisting of (1) the reduced vector and (2) the array of apical gender numbers
	 * <p> 0 male, 1 female, 2 indeterminate (for full sibling relations)
	 */
	private void reduce (){
		
		reducedNumbers = new int[getNumbers().length];
		apexGenders = new int[getNumbers().length];
		
		for (int i=0;i<getNumbers().length;i++){
			int a = number(i);
			int d = new Double(Math.floor(Math.log(a+1)/Math.log(2))).intValue();
			int k = MathUtils.pow2(d);
			int r = a-k+1;
			apexGenders[i] = new Long(Math.round(new Double(r)/new Double(k))).intValue();
			reducedNumbers[i] = a-(apexGenders[i]+1)*k/2;
			if (getNumbers()[i]<0) {
				apexGenders[i]=-1;
			}
		}
	}
	
	private int compareTo(Vector other, CompareCriterion criterion) {
		int result = 0;
		
		switch(criterion){
		
		// narrower chains first
		case ORDER:
			result = compare(order(),other.order()); // mode 1
			break;
			
		// shorter chains first
		case LENGTH:
			result = compare(length(),other.length()); // mode 2
			break;
			
		// ascending branches first
		case LEFT_BRANCH_LENGTH:
			 for (int i=0;i<order();i++) {
				 result = compare(getLength(2*i),other.getLength(2*i)); // mode 3
				 if (result != 0){
					 break; 
				 }
			 }
			 break;
			 
	    // Same-Sex before cross-sex relations
		case HETERO:
			result = -compare(compareGender(),other.compareGender()); // mode 4
			break;
			
		// chains with male ego first
		case EGOGENDER:
			result = egoGenderInt()-other.egoGenderInt(); // mode 0, 5
			break;
				
		// Gender profile comparison
		case GENDER_PROFILE:  // mode 6
			
			reduce();
			other.reduce();
			
			// comparison of gender-profiles (except nonlinear apices)
			for (int i=0;i<getNumbers().length;i++) {
				int number;
				int otherNumber;
				
				if (linear(i) || other.linear(i)) {  
					number = number(i);
					otherNumber = other.number(i);
				} else {                          
					number = reducedNumber(i);
					otherNumber = other.reducedNumber(i);
				}
				
				result = compare(number,otherNumber); 
				if (result != 0) {
		     		 break;
		     	}
			}
			if (result != 0) {
	     		 break;
	     	}
			
			// comparison of nonlinear apices
			// full siblings first, then agnatic and uterine half siblings
			for (int i=0;i<getNumbers().length;i++) { 
				result = compare(apexGender(i),other.apexGender(i)); 
				if (result != 0) {
		     		 break;
		     	}
			}
			break;

		}

		//
		return result;
	}
	
	private boolean linear(int i){
		boolean result;
		
		int otherBranch = i+1-2*(i%2);
		result = getNumbers()[otherBranch] == 0;
		
		//
		return result;
	}

	public int[] getNumbers() {
		return numbers;
	}

	public void setNumbers(int[] numbers) {
		this.numbers = numbers;
	}
	

	




}
