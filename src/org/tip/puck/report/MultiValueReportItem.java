package org.tip.puck.report;



public class MultiValueReportItem extends ReportAttribute {
	
	protected String[] values;

	/**
	 * 
	 */
	public MultiValueReportItem(int length) {
		setLabel("");
		setValues(new String[length]);
	}

	/**
	 * 
	 */
	public MultiValueReportItem(final String label, final String[] values) {
		setLabel(label);
		setValues(values);
	}
	
	public void setValues(final String[] values) {
		this.values = values;
	}

	public String[] values() {
		return values;
	}

	


}
