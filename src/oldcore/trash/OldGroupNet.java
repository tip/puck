package oldcore.trash;


/**
 * This class defines Nets whose vertices represent groups (of vertices, chains, etc.)
 * @author Klaus Hamberger
 * @since Version 0.6
 */
@SuppressWarnings("serial")
public class OldGroupNet  {
	
	
	//Check for function of the factor array!
	/**
	 * constructs a GroupNet from a ChainGroupMap
	 * <p> used for constructing mixed circuit intersection networks 
	 * @param map the underlying ChainGroupMap
	 * @param factors an array of divisors for vertex values
	 * @see gui.TipWriter#writePajCensus(Part,ChainGroupMap<Object,Chain<?>>,GroupNet)
	 */
/*	public OldGroupNet (ChainGroupMap<Object,Chain<?>> map, int[] factors){
		int i=1;
		for (Object clu : map.keySet()){
			int k = 1;
			if (factors.length>0) k = factors[i-1];
			put(new GroupNode(i,map.get(clu).size(1)/k,asString(clu)));
//			put(new GroupNode(i,map.getFrequency(clu)/k,asString(clu)));
			i++;
		}
		i=1;
		for (Object a : map.keySet()) {
			for (int j=i+1;j<size()+1;j++) {
				Object b = map.getKey(j-1);
				int k = map.getIntersectionSize(a, b); 
				if (k>0) get(i).setLink(get(j),k);
			}
			i++;
		}
	}*/

	

	
	/**
	 * the constructor for Nets consisting of matrimonial circuits (such as circuit intersection networks)
	 * @param rn the underlying RingGroupMap
	 * @param simple true if the Net contains only vertices no links 
	 * @see maps.groupmaps.RingGroupMap#toClassVertexSet()
	 * @see maps.groupmaps.RingGroupMap#toNet(String, gui.screens.Screen)
	 */
/*	public OldGroupNet (RingGroupMap rn, boolean simple) {
	    setName(rn.getName());
	    for (ChainGroup<OldRing> c : rn.values()) {
	    	put(new GroupNode(c));
	    }
	    if (simple) return;
	    for (ChainGroup<OldRing> cp : rn.byCouples().values()) {
	    	cp = cp.getModels();
	    	for (int i=0;i<cp.size();i++) {
	    		GroupNode e = (GroupNode)get(cp.get(i).getID());
	    		e.augment(cp.size()==1);
	    		for (int j=i;j<cp.size();j++) {//i+1...
	    			GroupNode a = (GroupNode)get(cp.get(j).getID());
	    			e.setLink(a);
	    		}
	    	}               
	    }
	}*/
	



	
	
}
