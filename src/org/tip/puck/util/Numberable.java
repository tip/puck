package org.tip.puck.util;

/**
 * 
 * 
 * @author TIP
 */
public interface Numberable {

	/**
	 * 
	 * @return
	 */
	int getId();

	/**
	 * 
	 */
	void setId(int id);
	
	/**
	 * 
	 * @return
	 */
	String hashKey();
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	Numberable clone(int id);
}