package org.tip.puck.io.csv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.geo.Place;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.Node;

import com.vividsolutions.jts.geom.Coordinate;

/**
 * This class represents a XLS File reader and writer.
 * 
 * @author Éric Mermet
 */
public class CSVFile {

	private static final Logger logger = LoggerFactory.getLogger(CSVFile.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	public static final int MAX_LINE_SIZE = 2048;
	public static final int GEONAMES_FILE = 0;
	public static final int PERSONNAL_FILE = 1;

	/**
	 * Loads a XLS file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Graph<Place> load(final File csvFile, Graph<?> graph, int typeOfCSV) throws PuckException {

		Graph<Place> returnHashedPlaces = new Graph<Place>();

		//Parse with TDf tab delimited format relative to geonames
		try {

			BufferedReader br = new BufferedReader(new FileReader(csvFile.getAbsolutePath()));

			returnHashedPlaces = readLines(br, graph, typeOfCSV);

		} catch (IOException e) {
			e.printStackTrace();
		}	

		//
		return returnHashedPlaces;
	}

	/**
	 * 
	 * @param file
	 * @return
	 * @throws IOException 
	 */
	public static Graph<Place> readLines(BufferedReader reader, Graph<?> placesGraph, int typeOfCSV) throws IOException {

		String nextLine;
		String[] columnDetail;

		Graph<Place> graphPlaces = new Graph<Place>("GraphPlaces");
		ArrayList<String> places = new ArrayList<String>();

		//Build list to compare nodes labels
		//		for (Node<?> node : placesGraph.getNodes())
		//			places.add(node.getLabel());

		int i = 0;
		while ((nextLine = reader.readLine()) != null) {
			// nextLine[] is an array of values from the line

			columnDetail = nextLine.split("\t", -1);

//			if( i%100000 == 0) {
//				for (int j = 0; j < columnDetail.length; j++) {
//					System.out.println( j + " " + columnDetail[j]);
//				}
//				System.out.println(columnDetail[0] + " " + columnDetail[1] + " " + columnDetail[4] + " " + columnDetail[5] );
//			}

			for (Node<?> node : placesGraph.getNodes()) {

				String stringNode = Arrays.asList(node.getLabel().split(",")).get(0);

				if( typeOfCSV == GEONAMES_FILE ) {
					if( stringNode.compareTo(columnDetail[1]) == 0 | stringNode.compareTo(columnDetail[2]) == 0 | stringNode.compareTo(columnDetail[3]) == 0) {
						Coordinate c = new Coordinate(Double.parseDouble(columnDetail[4]), Double.parseDouble(columnDetail[5]));
						graphPlaces.addNode(new Place(Integer.parseInt(columnDetail[0]), columnDetail[1], columnDetail[1], columnDetail[2], columnDetail[3], columnDetail[8], c));
						continue;
					}
				}
				if( typeOfCSV == PERSONNAL_FILE) {

					if( stringNode.compareTo(columnDetail[1]) == 0 ) {
						Coordinate c = new Coordinate(Double.parseDouble(columnDetail[3]), Double.parseDouble(columnDetail[4]));
						graphPlaces.addNode(new Place(Integer.parseInt(columnDetail[0]), columnDetail[1], "", "", "", columnDetail[2], c));
						continue;
					}
				}

			}

			i++;
		}
		System.out.println("End csv file : " + placesGraph.nodeCount() + " nodes found");

		//
		return graphPlaces;
	}

}
