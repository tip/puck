package org.tip.puck.net;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.PuckUtils;

/**
 * 
 * @author TIP
 */
public class IndividualComparator implements Comparator<Individual> {

	public enum Sorting {
		ID,
		BIRT_YEAR,
		AGE,
		BIRTH_ORDER,
		BIRTH_YEAR_OR_ORDER,
		FIRSTN,
		GENDER,
		LASTN,
	}

	private Sorting sorting;
	private List<Sorting> multiSorting;

	/**
	 * 
	 * @param multiSorting
	 */
	public IndividualComparator(final List<Sorting> multiSorting) {
		//
		this.multiSorting = multiSorting;
	}

	/**
	 * 
	 * @param sorting
	 */
	public IndividualComparator(final Sorting... sortings) {
		//
		if ((sortings == null) || (sortings.length == 0)) {
			//
			this.sorting = null;
			this.multiSorting = null;

		} else if (sortings.length == 1) {
			//
			this.sorting = sortings[0];
			this.multiSorting = null;

		} else {
			//
			this.sorting = null;
			this.multiSorting = new ArrayList<Sorting>(sortings.length);

			this.multiSorting.add(this.sorting);

			//
			for (Sorting item : sortings) {
				//
				this.multiSorting.add(item);
			}
		}
	}

	/**
	 * 
	 */
	@Override
	public int compare(final Individual alpha, final Individual bravo) {
		int result;

		if (this.multiSorting == null) {
			//
			result = compare(alpha, bravo, this.sorting);

		} else {
			//
			result = 0;
			for (Sorting currentSorting : this.multiSorting) {
				//
				result = compare(alpha, bravo, currentSorting);

				//
				if (result != 0) {
					break;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public static int compare(final Individual alpha, final Individual bravo, final Sorting sorting) {
		int result;

		//
		if (sorting == null) {
			result = 0;
		} else {
			//
			switch (sorting) {
				case ID:
					result = MathUtils.compare(getId(alpha), getId(bravo));
				break;

				case FIRSTN:
					result = PuckUtils.compare(getFirstName(alpha), getFirstName(bravo));
				break;

				case LASTN:
					result = PuckUtils.compare(getLastName(alpha), getLastName(bravo));
				break;

				case GENDER:
					result = Gender.compare(getGender(alpha), getGender(bravo));
				break;

				case BIRT_YEAR:
					Integer firstBirtYear = IndividualValuator.getBirthYear(alpha);
					Integer secondBirtYear = IndividualValuator.getBirthYear(bravo);

					if (firstBirtYear == null && secondBirtYear == null) {
						//
						result = 0;
					} else if (firstBirtYear == null){
						//
						result = -1;
					} else if (secondBirtYear == null){
						//
						result = 1;

					} else {
						//
						result = firstBirtYear.compareTo(secondBirtYear);
					}
				break;

				case AGE:
					
					result = -compare(alpha,bravo,Sorting.BIRT_YEAR);
					
				break;

				case BIRTH_ORDER:
					
					result = MathUtils.compareOrder(alpha.getBirthOrder(), bravo.getBirthOrder());
					
				break;
				
				case BIRTH_YEAR_OR_ORDER:
					
					result = compare(alpha,bravo,Sorting.BIRT_YEAR);
					if (result == 0){
						result = compare(alpha,bravo,Sorting.BIRTH_ORDER);
					}

				break;
	
				default:
					result = new Integer(alpha.getId()).compareTo(bravo.getId());
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Integer getBirthOrder(final Individual source) {
		Integer result;

		//
		if (source == null) {
			//
			result = null;
		} else {
			//
			result = source.getBirthOrder();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String getFirstName(final Individual source) {
		String result;

		//
		if (source == null) {
			//
			result = null;
		} else {
			//
			result = source.getFirstName();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Gender getGender(final Individual source) {
		Gender result;

		//
		if (source == null) {
			//
			result = null;
		} else {
			//
			result = source.getGender();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Integer getId(final Individual source) {
		Integer result;

		//
		if (source == null) {
			//
			result = null;
		} else {
			//
			result = source.getId();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String getLastName(final Individual source) {
		String result;

		//
		if (source == null) {
			//
			result = null;
		} else {
			//
			result = source.getLastName();
		}

		//
		return result;
	}
}
