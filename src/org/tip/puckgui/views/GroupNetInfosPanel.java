package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.Attributes;
import org.tip.puckgui.GroupNetGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class GroupNetInfosPanel extends JPanel {

	private static final long serialVersionUID = 6566488417300342417L;

	private static final Logger logger = LoggerFactory.getLogger(GroupNetInfosPanel.class);

	int reportCounter = 0;
	private JPanel thisJPanel;
	private GroupNetGUI groupNetworkGUI;
	JLabel lblLabelValue;
	JLabel lblNodeCountValue;
	JLabel lblLinkCountValue;
	private AttributesPanel attributesPanel;

	/**
	 * 
	 * @param guiManager
	 */
	public GroupNetInfosPanel(final GroupNetGUI guiManager) {
		//
		this.thisJPanel = this;
		this.groupNetworkGUI = guiManager;
		setVisible(true);
		setLayout(new BorderLayout(0, 0));

		JPanel generalPanel = new JPanel();
		add(generalPanel, BorderLayout.NORTH);
		generalPanel.setLayout(new BoxLayout(generalPanel, BoxLayout.Y_AXIS));

		JPanel corpusStaticDataPanel = new JPanel();
		corpusStaticDataPanel.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "General", TitledBorder.LEADING, TitledBorder.TOP, null,
				new Color(51, 51, 51)));
		corpusStaticDataPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		generalPanel.add(corpusStaticDataPanel);
		corpusStaticDataPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblLabel = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("GroupNetInfosPanel.lblLabel.text")); //$NON-NLS-1$ //$NON-NLS-2$
		lblLabel.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(lblLabel, "2, 2");

		this.lblLabelValue = new JLabel("-");
		this.lblLabelValue.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(this.lblLabelValue, "4, 2");

		JLabel lblNodeCount = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("GroupNetInfosPanel.lblNodeCount.text")); //$NON-NLS-1$ //$NON-NLS-2$
		lblNodeCount.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(lblNodeCount, "2, 4");

		this.lblNodeCountValue = new JLabel("-");
		this.lblNodeCountValue.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(this.lblNodeCountValue, "4, 4");

		JLabel lblLinkCount = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("GroupNetInfosPanel.lblLinkCount.text")); //$NON-NLS-1$ //$NON-NLS-2$
		lblLinkCount.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(lblLinkCount, "2, 6");

		this.lblLinkCountValue = new JLabel("-");
		this.lblLinkCountValue.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(this.lblLinkCountValue, "4, 6");

		Component verticalStrut_10 = Box.createVerticalStrut(10);
		generalPanel.add(verticalStrut_10);

		JSeparator separator_5 = new JSeparator();
		generalPanel.add(separator_5);

		Component verticalStrut_9 = Box.createVerticalStrut(10);
		generalPanel.add(verticalStrut_9);

		this.attributesPanel = new AttributesPanel(this.groupNetworkGUI, (Attributes) null, null);
		add(this.attributesPanel, BorderLayout.CENTER);

		// ///////////////////
		if (guiManager != null) {
			update();
		}
	}

	/**
	 * 
	 */
	public void update() {
		//
		this.lblLabelValue.setText(this.groupNetworkGUI.getGroupNet().getLabel());
		this.lblNodeCountValue.setText(String.valueOf(this.groupNetworkGUI.getGroupNet().nodeCount()));
		this.lblLinkCountValue.setText(String.valueOf(this.groupNetworkGUI.getGroupNet().lineCount()));

		//
		this.attributesPanel.setSource(this.groupNetworkGUI.getGroupNet().attributes());
	}

}
