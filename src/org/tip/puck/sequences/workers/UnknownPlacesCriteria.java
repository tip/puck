package org.tip.puck.sequences.workers;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class UnknownPlacesCriteria {

	private boolean includedIndividual;
	private boolean includedAllRelations;
	private StringList relationNames;

	/**
	 * 
	 */
	public UnknownPlacesCriteria() {
		this.includedIndividual = true;
		this.includedAllRelations = true;
		this.relationNames = new StringList();
	}

	public StringList getRelationNames() {
		return this.relationNames;
	}

	public boolean isIncludedAllRelations() {
		return this.includedAllRelations;
	}

	public boolean isIncludedIndividual() {
		return this.includedIndividual;
	}

	public void setIncludedAllRelations(final boolean includedAllRelations) {
		this.includedAllRelations = includedAllRelations;
		this.relationNames.clear();
	}

	public void setIncludedIndividual(final boolean includedIndividual) {
		this.includedIndividual = includedIndividual;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		result = String.format("[%s][%s][%s]", this.includedIndividual, this.includedAllRelations, this.relationNames.toStringWithCommas());

		//
		return result;
	}
}
