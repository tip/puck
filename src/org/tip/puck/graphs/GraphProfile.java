package org.tip.puck.graphs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tip.puck.graphs.workers.GraphUtils;
import org.tip.puck.matrix.MatrixStatistics;
import org.tip.puck.matrix.MatrixStatistics.Indicator;
import org.tip.puck.matrix.MatrixStatistics.Mode;
import org.tip.puck.net.Individual;
import org.tip.puck.partitions.Partition;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.Value;

public class GraphProfile<E> {
	
	private Graph<E> mainGraph;
	private List<Node<E>> nodes;
	
	private Graph<E> graphWithoutEgo;
	private Graph<E> graphWithoutNullValueLines;
	private double[] betweenness;
	private double maxBetweenness;
	private double maxNonEgoBetweenness;
	private double meanBetweenness;
	
	private Partition<Node<E>> nonEgoComponents;
	private List<E> centralAlters;
	private List<E> centralReferents;
	
	MatrixStatistics statistics;
	
	private Node<E> egoNode;
	private int egoNodeId;
	
	private Map<Value,Double[]> aggregateWeights;
	private Partition<Link<E>> linkPartition;
	
	private Map<String,Double> specificDensities;
	
	private List<String> partitionLabels;

	public GraphProfile (Graph<E> graph, List<String> partitionLabels){
		
		this.mainGraph = graph;
		this.nodes = graph.getNodes().toListSortedById();
		this.partitionLabels = partitionLabels;
		
		this.egoNodeId = -1;
	}
	
	public GraphProfile (Graph<E> graph, E referent, List<String> partitionLabels){
		
		this.mainGraph = graph;
		this.nodes = graph.getNodes().toListSortedById();
		this.partitionLabels = partitionLabels;
		
		this.egoNode = mainGraph.getNode(referent);
		this.egoNodeId = nodes.indexOf(egoNode);
		
	}
	
	public Graph<E> getGraph(){
		return mainGraph;
	}

	
	public Graph<E> getGraphWithoutEgo (){
		
		if (graphWithoutEgo == null){
			graphWithoutEgo = GraphUtils.cloneWithoutEgo(getGraphWithoutNullValueLines (), egoNode);
		}
		//
		return graphWithoutEgo;
		
	}
	
	public int getEgoNodeId(){
		return egoNodeId;
	}
	
	public Graph<E> getGraphWithoutNullValueLines (){
		if (graphWithoutNullValueLines==null){
			graphWithoutNullValueLines = GraphUtils.cloneWithoutNullValueLines(mainGraph);
		}
		//
		return graphWithoutNullValueLines;
	}
	
	private void setBetweenness (){
		
		betweenness = GraphUtils.betweenness(getGraphWithoutNullValueLines());
		
		centralAlters = new ArrayList<E>();
		centralReferents = new ArrayList<E>();
		maxNonEgoBetweenness = -1.;
		maxBetweenness = -1.;
		meanBetweenness = 0.;
		
		int i=0;
		for (Node<E> node : nodes){
			double nodeBetweenness = betweenness[i];
			node.setAttribute("BETWEENNESS", nodeBetweenness+"");
			meanBetweenness += nodeBetweenness;
			if ((i!=egoNodeId) && (nodeBetweenness >= maxNonEgoBetweenness)){
				if (nodeBetweenness > maxNonEgoBetweenness){
					centralAlters = new ArrayList<E>();
				}
				centralAlters.add(node.getReferent());
				maxNonEgoBetweenness = nodeBetweenness;
			}
			if (nodeBetweenness >= maxBetweenness){
				if (nodeBetweenness > maxBetweenness){
					centralReferents = new ArrayList<E>();
				}
				centralReferents.add(node.getReferent());
				maxBetweenness = nodeBetweenness;
			}
			
			i++;
		}
		meanBetweenness = MathUtils.percent(meanBetweenness, 100*i);
		//
	}
	
	private void setNonEgoComponents(){
		
		nonEgoComponents = GraphUtils.components(getGraphWithoutEgo());
		for (Node<E> avatarNode: nonEgoComponents.getItems()){
			Node<E> node = mainGraph.getNode(avatarNode.getId());
			if (node!=null){
				node.setAttribute("COMPONENT", nonEgoComponents.getValue(avatarNode)+"");
			}
		}

	}
	
	public double getStatistics(Indicator indicator, Mode mode){
		double result;
		
		if (statistics == null){
			statistics = new MatrixStatistics(mainGraph);
		}
		
		result = statistics.get(indicator, mode);
		//
		return result;
	}
	

	public Partition<Node<E>> getNonEgoComponents(){
		
		if (nonEgoComponents==null){
			setNonEgoComponents();
		}
		//
		return nonEgoComponents;
	}
	
	public double getEgoBetweenness(){
		
		if(betweenness == null){
			setBetweenness();
		}
		
		return betweenness[egoNodeId];
	}
	
	
	
	public double getMaxBetweenness() {

		if(betweenness == null){
			setBetweenness();
		}
		
		return maxBetweenness;
	}

	
	public double getMaxNonEgoBetweenness() {

		if(betweenness == null){
			setBetweenness();
		}
		
		return maxNonEgoBetweenness;
	}

	public double getMeanBetweenness() {
		
		if(betweenness == null){
			setBetweenness();
		}
		
		return meanBetweenness;
	}

	public double getEccentricity(){
		
		return MathUtils.jacquard(getEgoBetweenness(), getMaxNonEgoBetweenness()); 
	}
	
	public double nodeCount(){
		
		return mainGraph.nodeCount();
	}
	
	public double getSizeWithoutEgo(){
		
		return graphWithoutEgo.nodeCount();
	}

	public List<E> getCentralAlters() {

		if(betweenness == null){
			setBetweenness();
		}
		
		return centralAlters;
	}
	
	public List<E> getCentralReferents() {
		
		if(betweenness == null){
			setBetweenness();
		}
		
		return centralReferents;
	}
	
	public int lineCount(){
		return mainGraph.lineCount();
	}
	
	public int nonNullLineCount(){
		return getGraphWithoutNullValueLines().lineCount();
	}
	
	public double density(){
		return GraphUtils.density(mainGraph);
	}
	
	public double densityWithoutLoops(){
		return GraphUtils.densityWithoutLoops(mainGraph);
	}
	
	public double meanDegree(){
		return GraphUtils.meanDegree(mainGraph);
	}
	
	

	
	public double meanDegreeWithoutLoops(){
		return GraphUtils.meanDegreeWithoutLoops(mainGraph);
	}
	
	public double meanDegreeNormalized(){
		return GraphUtils.meanDegreeNormalized(mainGraph);
	}
	
	public double meanDegreeWithoutLoopsNormalized(){
		return GraphUtils.meanDegreeWithoutLoopsNormalized(mainGraph);
	}
	
	public double brokerage(){
		return GraphUtils.unconnectedPairsNormalized(graphWithoutEgo);
	}
	
	public Double efficientSize(){
		return GraphUtils.efficientSize(graphWithoutEgo);
	}

	public Double efficiency(){
		return GraphUtils.efficiency(graphWithoutEgo);
	}
	
	public Map<Value,Double[]> aggregateWeights(){
		
		if (aggregateWeights==null){
			
			aggregateWeights = new HashMap<Value,Double[]>();
			Map<Value,Double[]> sumMap = new HashMap<Value,Double[]>();
			
			Links<E> links = mainGraph.getArcs();
			
			for (Link<E> link : links){
				
				double value = link.getWeight();
				Value label = linkPartition.getValue(link);
				
				Double[] values = aggregateWeights.get(label);
				Double[] sums = sumMap.get(label);
				
				if (values == null){
					
					values = new Double[]{0.,0.,0.,0.,0.};
					aggregateWeights.put(label, values);

					sums = new Double[]{0.,0.,0.,0.,0.};
					sumMap.put(label, sums);
				} 

				// Has to be moved to another class later
				int egoGender = ((Individual)link.getSourceNode().getReferent()).getGender().toInt();
				int alterGender = ((Individual)link.getTargetNode().getReferent()).getGender().toInt();
				
				if (egoGender<2 && alterGender<2){
					
					// 0 HH, 1 FH, 2 HF, 3 FF
					int idx = egoGender + 2 * alterGender;
					values[idx] += value;
					values[4] += value;
					sums[idx]++;
					sums[4]++;
					
				}
			}
			
			for (Value label : aggregateWeights.keySet()){
				
				Double[] values = aggregateWeights.get(label);
				Double[] sums = sumMap.get(label);

				for (int i=0;i<sums.length;i++){
					values[i] = MathUtils.percent(values[i],100*sums[i]);
				}

				aggregateWeights.put(label, values);
			}
		}
		//
		return aggregateWeights;
		
	}

	public Partition<Link<E>> getLinkPartition() {
		return linkPartition;
	}

	public void setLinkPartition(Partition<Link<E>> linkPartition) {
		this.linkPartition = linkPartition;
	}

	public void setSpecificDensities (){
		
		specificDensities = new HashMap<String,Double>();
		specificDensities.put("PARENT-CHILD", 0.);
		specificDensities.put("SPOUSE", 0.);
		specificDensities.put("SIBLING", 0.);
		specificDensities.put("RELATIVE", 0.);
		specificDensities.put("AFFINE", 0.);
		specificDensities.put("EMPLOYMENT", 0.);
		specificDensities.put("RENT", 0.);
		
		for (Link<Individual> link : ((Graph<Individual>)mainGraph).getLinks()){
			String tag = link.getTag();
			tag = tag.substring(tag.indexOf("'")+1,tag.lastIndexOf("'"));
			String key = null;
			if (tag.equals("FATHER") || tag.equals("MOTHER") || tag.equals("CHILD")){
				key = "PARENT-CHILD";
			} else if (tag.equals("SPOUSE") || tag.equals("SIBLING") || tag.equals("AFFINE")){
				key = tag;
			} else if (tag.contains("RELATIVE")){
				key = "RELATIVE";
			} else if (tag.contains("EMPLOY")){
				key = "EMPLOYMENT";
			} else if (tag.equals("LANDLORD") || tag.equals("LODGER")){
				key = "RENT";
			}
			
			if (key!=null){
				specificDensities.put(key, specificDensities.get(key)+1.);
			}
		}
		
		for (String key : specificDensities.keySet()){
			specificDensities.put(key, MathUtils.percent(specificDensities.get(key),mainGraph.lineCount()));
		}
		
	}

	public Double getSpecificDensity(String relation) {
		Double result;
		
		if (specificDensities == null){
			
			result = null;
		} else {
			
			result = specificDensities.get(relation);
		}
		//
		return result;
	}

	public List<String> getPartitionLabels() {
		return partitionLabels;
	}
	
	
	
	

}
