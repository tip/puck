package org.tip.puck.stag;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.commons.io.comparator.NameFileComparator;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.tip.puck.PuckException;
import org.tip.puck.io.puc.PUCFile;
import org.tip.puck.net.Net;
import org.tip.puck.net.workers.ControlReporter;
import org.tip.puck.net.workers.ControlReporter.ControlType;
import org.tip.puck.report.Report;
import org.tip.puck.stag.StatisticTag.Scope;
import org.tip.puck.stag.StatisticTag.Type;
import org.tip.puck.util.Chronometer;

/**
 * The Class StagChecker.
 */
public class StagChecker {

	/**
	 * Builds the stats.
	 *
	 * @param net the net
	 * @return the properties
	 */
	private static Properties buildStats(Net net)
	{
		Properties result;

		result = new Properties();
		
		// Build basics statistics.
		{
			result.putAll(Stag.buildControlStatistics(net));
			
			Report report = ControlReporter.reportControls(net, (ResourceBundle) null, ControlType.MISSING_DATES);
			//System.out.println("REPORT:" + report.status());
			
			result.putAll(Stag.buildBasicStatistics(net));
			//Stag.buildAttributeDescriptorStatistics(net);
		}

		// Build geography statistics.
		{
			result.putAll(Stag.buildGeographyStatistics(net));
		}

		// Build circuit statistics.
		{
			result.putAll(Stag.buildCircuitStatistics(net));
		}	

		//
		return result;
	}
	

	/**
	 * Check.
	 *
	 * @param net the net
	 * @param refStats the stats
	 */
	private static void check(Net net, Properties refStats, Properties newStats)
	{
		for (Object key : refStats.keySet())
		{
			String name = (String) key;

			if ((!StringUtils.contains(name, "TIME")) && (!StringUtils.contains(name, "GRAPHICS_COUNT")))
			{
				String refValue = String.valueOf(refStats.get(name));
				String newValue = String.valueOf(newStats.get(name));
				
				if (!StringUtils.equals(refValue, newValue))
				{
					System.out.println(net.getLabel() + ": " + name + " " + refValue + " " + newValue);
				}
			}
		}
		
		//
		// Genealogy statistics.
		//		NUM_OF_INDIVIDUALS(Scope.GENEALOGY, Type.LONG),
		//		NUM_OF_MEN(Scope.GENEALOGY, Type.LONG),
		//		NUM_OF_WOMEN(Scope.GENEALOGY, Type.LONG),
		//		NUM_OF_UNKNOWN(Scope.GENEALOGY, Type.LONG),
		//
		//		RATE_OF_MEN(Scope.GENEALOGY, Type.DOUBLE),
		//		RATE_OF_WOMEN(Scope.GENEALOGY, Type.DOUBLE),
		//		RATE_OF_UNKNOWN(Scope.GENEALOGY, Type.DOUBLE),
		//
		//		NUM_OF_UNIONS(Scope.GENEALOGY, Type.LONG),
		//		NUM_OF_MARRIAGES(Scope.GENEALOGY, Type.LONG),
		//		DENSITY_OF_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),
		//		NUM_OF_NON_SINGLE_MEN(Scope.GENEALOGY, Type.LONG),
		//		NUM_OF_NON_SINGLE_WOMEN(Scope.GENEALOGY, Type.LONG),
		//
		//		NUM_OF_PARENT_CHILD_TIES(Scope.GENEALOGY, Type.LONG),
		//		DENSITY_OF_FILIATION(Scope.GENEALOGY, Type.DOUBLE),
		//		NUM_OF_FERTILE_MARRIAGES(Scope.GENEALOGY, Type.LONG),
		//		RATE_OF_FERTILE_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),
		//		NUM_OF_COWIFE_RELATIONS(Scope.GENEALOGY, Type.LONG),
		//		NUM_OF_COHUSBAND_RELATIONS(Scope.GENEALOGY, Type.LONG),
		//
		//		NUM_OF_RELATIONS(Scope.GENEALOGY, Type.LONG),
		//		NUM_OF_RELATION_MODELS(Scope.GENEALOGY, Type.LONG),
		//
		//		NUM_OF_COMPONENTS(Scope.GENEALOGY, Type.LONG),
		//		MAX_OF_COMPONENTS(Scope.GENEALOGY, Type.LONG),
		//
		//		MEAN_OF_COMPONENTS_SHARE_AGNATIC(Scope.GENEALOGY, Type.DOUBLE),
		//		MAX_OF_COMPONENTS_SHARE_AGNATIC(Scope.GENEALOGY, Type.DOUBLE),
		//		MEAN_OF_COMPONENTS_SHARE_AGNATIC_WO_SINGLETON(Scope.GENEALOGY, Type.DOUBLE),
		//
		//		MEAN_OF_COMPONENTS_SHARE_UTERINE(Scope.GENEALOGY, Type.DOUBLE),
		//		MAX_OF_COMPONENTS_SHARE_UTERINE(Scope.GENEALOGY, Type.DOUBLE),
		//		MEAN_OF_COMPONENTS_SHARE_UTERINE_WO_SINGLETON(Scope.GENEALOGY, Type.DOUBLE),
		//
		//		NUM_OF_ELEMENTARY_CYCLES(Scope.GENEALOGY, Type.LONG),
		//
		//		DEPTH(Scope.GENEALOGY, Type.LONG),
		//		DEPTH_MEAN(Scope.GENEALOGY, Type.DOUBLE),
		//
		//		MEAN_SPOUSE_OF_MEN(Scope.GENEALOGY, Type.DOUBLE),
		//		MEAN_SPOUSE_OF_WOMEN(Scope.GENEALOGY, Type.DOUBLE),
		//
		//		MEAN_AGNATIC_FRATRY_SIZE(Scope.GENEALOGY, Type.DOUBLE),
		//		MEAN_UTERINE_FRATRY_SIZE(Scope.GENEALOGY, Type.DOUBLE),
		//		MEAN_CHILDREN_PER_FERTILE_COUPLE(Scope.GENEALOGY, Type.DOUBLE),
		//
		//		NUM_OF_CROSS_FIRST_COUSIN_MARRIAGES(Scope.GENEALOGY, Type.LONG),
		//		RATE_OF_CROSS_FIRST_COUSIN_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),
		//
		//		NUM_OF_LEVIRATE_MARRIAGES(Scope.GENEALOGY, Type.LONG),
		//		RATE_OF_LEVIRATE_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),
		//
		//		NUM_OF_SORORATE_MARRIAGES(Scope.GENEALOGY, Type.LONG),
		//		RATE_OF_SORORATE_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),
		//
		//		NUM_OF_DOUBLE_OR_EXCHANGE_MARRIAGES(Scope.GENEALOGY, Type.LONG),
		//		RATE_OF_DOUBLE_OR_EXCHANGE_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),
		//
		//		NUM_OF_NIECE_NEPHEW_MARRIAGES(Scope.GENEALOGY, Type.LONG),
		//		RATE_OF_NIECE_NEPHEW_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),
		//
		//		NUM_OF_PARALLEL_FIRST_COUSIN_MARRIAGES(Scope.GENEALOGY, Type.LONG),
		//		RATE_OF_PARALLEL_FIRST_COUSIN_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),
		//
		//		NUM_OF_FIRST_COUSIN_MARRIAGES(Scope.GENEALOGY, Type.LONG),
		//		RATE_OF_FIRST_COUSIN_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),
		//
		//		NUM_OF_DOUBLE_MARRIAGES(Scope.GENEALOGY, Type.LONG),
		//		RATE_OF_DOUBLE_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),
		//
		//		NUM_OF_EXCHANGE_MARRIAGES(Scope.GENEALOGY, Type.LONG),
		//		RATE_OF_EXCHANGE_MARRIAGES(Scope.GENEALOGY, Type.DOUBLE),
		//
		//		BASIC_GSTATISTICS_ERROR(Scope.GENEALOGY, Type.LONG),
		//
		//		// --------------------------------------------------------------
		//		// Terminology controls.
		//		GENERAL_CHECK(Scope.TERMINOLOGY, Type.LONG),
		//
		//		// Terminology statistics.
		//		NUM_OF_TERMS(Scope.TERMINOLOGY, Type.LONG),
		//		SELF_NAME(Scope.TERMINOLOGY, Type.STRING),
		//		NUM_OF_FEMALES(Scope.TERMINOLOGY, Type.LONG),
		//		NUM_OF_FEMALES_EXCLUSIVE(Scope.TERMINOLOGY, Type.LONG),
		//		NUM_OF_FEMALES_SPEAKER(Scope.TERMINOLOGY, Type.LONG),
		//		NUM_OF_FEMALES_SPEAKER_EXCLUSIVE(Scope.TERMINOLOGY, Type.LONG),
		//		NUM_OF_MALES(Scope.TERMINOLOGY, Type.LONG),
		//		NUM_OF_MALES_EXCLUSIVE(Scope.TERMINOLOGY, Type.LONG),
		//		NUM_OF_MALES_SPEAKER(Scope.TERMINOLOGY, Type.LONG),
		//		NUM_OF_MALES_SPEAKER_EXCLUSIVE(Scope.TERMINOLOGY, Type.LONG),
		//		GENERATION_PATTERN_MALE(Scope.TERMINOLOGY, Type.STRING),
		//		GENERATION_PATTERN_FEMALE(Scope.TERMINOLOGY, Type.STRING),
		//		GENERATION_PATTERN_TOTAL(Scope.TERMINOLOGY, Type.STRING),
		//		NUM_OF_AUTORECIPROCAL_TERMS(Scope.TERMINOLOGY, Type.LONG),
		//		RECURSIVE_TERMS(Scope.TERMINOLOGY, Type.STRING),
		//		ASCENDANT_CLASSIFICATION(Scope.TERMINOLOGY, Type.STRING),
		//		COUSIN_CLASSIFICATION(Scope.TERMINOLOGY, Type.STRING),
		//		KIN_TERM_NETWORK_FEMALE_COMPONENTS(Scope.TERMINOLOGY, Type.LONG),
		//		KIN_TERM_NETWORK_FEMALE_CONCENTRATION(Scope.TERMINOLOGY, Type.DOUBLE),
		//		KIN_TERM_NETWORK_FEMALE_SPEAKER_DENSITY(Scope.TERMINOLOGY, Type.DOUBLE),
		//		KIN_TERM_NETWORK_FEMALE_SPEAKER_MEAN_CLUSTERING_COEFFICIENT(Scope.TERMINOLOGY, Type.DOUBLE),
		//		KIN_TERM_NETWORK_FEMALE_SPEAKER_MEAN_DEGREE(Scope.TERMINOLOGY, Type.DOUBLE),
		//		KIN_TERM_NETWORK_MALE_COMPONENTS(Scope.TERMINOLOGY, Type.LONG),
		//		KIN_TERM_NETWORK_MALE_CONCENTRATION(Scope.TERMINOLOGY, Type.DOUBLE),
		//		KIN_TERM_NETWORK_MALE_SPEAKER_DENSITY(Scope.TERMINOLOGY, Type.DOUBLE),
		//		KIN_TERM_NETWORK_MALE_SPEAKER_MEAN_CLUSTERING_COEFFICIENT(Scope.TERMINOLOGY, Type.DOUBLE),
		//		KIN_TERM_NETWORK_MALE_SPEAKER_MEAN_DEGREE(Scope.TERMINOLOGY, Type.DOUBLE),
		//
		//		BASIC_TSTATISTICS_ERROR(Scope.TERMINOLOGY, Type.LONG),
		//
		//		// --------------------------------------------------------------
		//		GEOGRAPHY_AVAILABLE(Scope.GEOGRAPHY, Type.BOOLEAN),
		
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		// Argument is directory where test files are.
		if (args.length == 1)
		{
			File directory = new File(args[0]);

			if ((directory.exists()) && (directory.isDirectory()))
			{ 
				File[] files = directory.listFiles();
				Arrays.sort(files, NameFileComparator.NAME_COMPARATOR);
				for (File file : files)
				{
					if (StringUtils.endsWith(file.getName(), ".stats"))
					{
						File pucFile = new File(file.getAbsolutePath().replace(".stats", ".puc"));
						File statFile = file;

						System.out.println("=== " + pucFile.getName());
						try 
						{
							Net net = PUCFile.load(pucFile);
							Properties refStats = new Properties();
							refStats.load(new FileReader(statFile));
							Properties newStats = buildStats(net);
							
							check(net, refStats, newStats);
						}
						catch (PuckException exception) {
							exception.printStackTrace();
						} catch (FileNotFoundException exception) {
							exception.printStackTrace();
						} catch (IOException exception) {
							exception.printStackTrace();
						}
					}
				}
			}
		}
		else
		{
			System.out.println("Please, set parameter.");
		}
	}
}
