package org.tip.puck.visualization.layouts.hierarchical.datastructs.chain;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.gephi.graph.api.Node;
import org.tip.puck.visualization.layouts.GraphSource;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.containers.ChainContainer;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.containers.BorderContainer;

/**
 * @date Jun 25, 2012
 * @author Edoardo Savoia
 */
public class Triad {
    private BorderContainer source;
    private BorderContainer target;
    private Set<Node> children = new HashSet<Node>();
    private Set<ChainContainer> clusters = null;
    

    public Triad(BorderContainer source, BorderContainer target) {
	this.source = source;
	this.target = target;
    }

    public BorderContainer getSource() {
	return source;
    }

    public BorderContainer getTarget() {
	return target;
    }

    public void setTarget(BorderContainer target) {
	if (this.target != null) {
	    System.out.println("Overwriting target");
	}
	this.target = target;
    }

    private void swap(){
	BorderContainer tmp = source;
	source = target;
	target = tmp;
	// update children's origins
	// source.setOrigin(source.getOrigin());
	// target.setOrigin(target.getOrigin());
    }

    public boolean isOn(BorderContainer cc) {
	return (cc.equals(source));
    }

    public boolean isBuilt(){
	return !(clusters == null);
    }

    public boolean isInternal(){
	return (source.getRootCluster() == target.getRootCluster());
    }

    public boolean addChildren(Node e) {
	if (clusters != null) {
	    System.out.println("Triad cannot be modified after buildCluster is called");
	}
	return children.add(e);
    }

    public Set<Node> getChildren() {
	return Collections.unmodifiableSet(children);
    }

    public void buildClusters(GraphSource graphSource) {
	if (clusters != null) {
	    System.out.println("clusters already created");
	    return;
	}
	clusters = new HashSet<ChainContainer>(children.size());
	for (Node child : children) {
	    // ChainCluster cc = new ChainCluster(graphSource, child);
	    ChainContainer container = ChainContainer.getContainer(child, graphSource);
	    container.setRootCluster(source.getRootCluster());
	    container.setOrigin(source.getOrigin()+source.getChain().getHeight()+1);
	    if (container instanceof BorderContainer) {
		    source.getRootCluster().addBorder((BorderContainer) container);
	    }
	    clusters.add(container);
	}
	for (ChainContainer container : clusters) {
	    container.populateChildren(graphSource);
	}
    }

    public Set<ChainContainer> getClusters() {
	return clusters;
    }

    public BorderContainer getOtherBorder(BorderContainer cc){
	if (cc.equals(source)) {
	    return target;
	} else if(cc.equals(target)) {
	    return source;
	} else {
	    return null;
	}
    }

}
