package org.tip.puck.net.relations.roles;

import org.tip.puck.net.Individual;
import org.tip.puck.net.relations.Actor;

public class ExtendedActor extends Actor {

	MetaRole metaRole;
	String selfName;
	
	public ExtendedActor (Individual individual, RoleActor roleActor){
		super(individual,roleActor.getIndividual());
		this.metaRole = roleActor.getRole();
		this.selfName = roleActor.selfName;
	}

	MetaRole getMetaRole() {
		return metaRole;
	}
	
	RoleActor getRoleActor(){
		return new RoleActor(role,metaRole,selfName);
	}
	
}
