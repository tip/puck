package org.tip.puck.mas;



/**
 * @author Telmo Menezes
 *
 */
public class WFAgeLimits implements WeightFactor {

    int minAge;
    int maxAge;
    
    public WFAgeLimits(int minAge, int maxAge) {
        this.minAge = minAge;
        this.maxAge = maxAge;
    }
    
    public String factorName(){
    	return "agelimits";
    }
    
    @Override
    public double factor(Agent mother, Agent father) {
    	int ageFather = father.getAge();
    	int ageMother = mother.getAge();
    	
    	if (ageFather < minAge) return 0;
    	if (ageMother < minAge) return 0;
    	if (ageFather > maxAge) return 0;
    	if (ageMother > maxAge) return 0;
    	
        return 1;
    }
    
    @Override
    public boolean appliesToMarriage() {return true;}
    
    @Override
    public boolean appliesToDivorce() {return true;}
}
