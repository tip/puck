package org.tip.puck.net.workers;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.geo.Geography;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Families;
import org.tip.puck.net.Family;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Individual;
import org.tip.puck.net.IndividualComparator.Sorting;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.KinType;
import org.tip.puck.net.UnionStatus;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.workers.RelationValuator;
import org.tip.puck.sequences.Ordinal;
import org.tip.puck.statistics.StatisticsWorker;
import org.tip.puck.util.NumberedIntegers;
import org.tip.puck.util.NumberedValues;
import org.tip.puck.util.Value;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class IndividualValuator {

	public enum EndogenousLabel {
		AGE,
		MINIMAL_AGE,
		REFERENT,
		BIRTH_ORDER,
		DEPTH,
		FIRST_MARR_DATE,
		FIRSTN,
		SIBSETM,
		SIBSETP,
		GEN,
		GENDER,
		ID,
		LASTN,
		LIFESTATUS,
		MATRISTATUS,
		MDEPTH,
		MATRIC,
		MATRID,
		OCCUPATION,
		PARTN,
		PATRIC,
		PATRID,
		PEDG,
		PROG,
		SIMPLE,
		SPOU,
		UNIONSTATUS;

		// SUB
		
	}

	public static final Pattern YEAR_PATTERN = Pattern.compile("(\\d\\d\\d\\d)");
	
	public static boolean isTimeDependent(String label){
		boolean result;
		
		if (label.equals("AGE") || label.equals("MATRISTATUS") || (label.equals("OCCUPATION"))){
			result = true;
		} else {
			result = false;
		}
		//
		return result;
	}

	public static Integer ageAtYear(final Individual source, final int year) {
		Integer result;
		
		Integer birtYear = getBirthYear(source);
		Integer deatYear = getDeathYear(source);

		result = -1;

		if (birtYear != null && birtYear > year) {
			result = -1;
		} else if (deatYear != null && deatYear < year) {
			result = -1;
		} else if (birtYear != null) {
			result = (year - birtYear);
		}

		//
		return result;
	}

	public static boolean aliveAtYear(final Individual source, final int year) {
		boolean result;

		result = false;

		Integer birtYear = getBirthYear(source);
		Integer deatYear = getDeathYear(source);

		if (birtYear != null && birtYear <= year && (deatYear == null || deatYear >= year)) {
			result = true;
		}

		//
		return result;
	}
	
	public static boolean hasMinimalAgeAtYear(final Individual source, final int year, final int minimalAge) {
		boolean result;

		result = false;

		Integer birtYear = getBirthYear(source);
		Integer deatYear = getDeathYear(source);

		if (birtYear != null && birtYear + minimalAge <= year && (deatYear == null || deatYear >= year)) {
			result = true;
		}

		//
		return result;
	}

	public static Individuals extract(final Individuals source, final String label, final Object parameter, final Geography geography, final Value value) {
		Individuals result;

		result = new Individuals();
		for (Individual individual : source) {
			Value thisValue = get(individual, label, parameter, geography);
			if (thisValue != null && thisValue.equals(value)) {
				result.put(individual);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String extractYear(final String source) {
		String result;

		if (source == null) {
			
			result = null;
			
		} else {
			
			try {

				result = Integer.parseInt(source) + "";
			
			} catch (NumberFormatException nfe) {
				
				Matcher matcher = YEAR_PATTERN.matcher(source);
								
				if ((matcher.find()) && (matcher.groupCount() > 0)) {
					//
					result = matcher.group(1);

				} else {
					result = null;
				}
			}
		}

		//
		return result;
	}

	public static Integer extractYearAsInt(final String source) {
		Integer result;

		String year = extractYear(source);

		if (year == null) {
			result = null;
		} else {
			result = Integer.parseInt(year);
		}
		//
		return result;
	}

	/**
	 * 
	 * @param individual
	 * @param label
	 * @return
	 */
	public static Value get(final Individual source, final String label, final Geography geography) {
		Value result;

		result = get(source, label, null, geography);

		//
		return result;
	}
	
/*	public static Value get(final Individual member, final String relationModelName, final String relationAttributeLabel, final String dateLabel, final Ordinal time, final Geography geography){
		Value result;
		
		// Warning! supposes that there is only one relation by year...
		Relation relation = member.relations().getByModelName(relationModelName).getByTime(dateLabel, time.getYear()).getFirst();
		
		result = get(member, relation, relationAttributeLabel, geography);
		
		//
		return result;
	}*/

	public static Value get(final Individual member, final Relation relation, final String relationAttributeLabel, final Geography geography){
		Value result;
		
		if (relationAttributeLabel == null){
			if (relation==null){
				result = null;
			} else {
				result = new Value(relation);
			}
		} else {
			result = RelationValuator.get(relation, relationAttributeLabel,geography);
		}
		//
		return result;
	}

	/**
	 * 
	 * WARNING: because this method is not the primary one, it does not value
	 * all endogenous labels.
	 * 
	 * @param individual
	 * @param label
	 * 
	 * @return
	 */
	public static Value get(final Individual source, final String label, final Object parameter, final Geography geography) {
		Value result;
		
		//
		EndogenousLabel endogenousLabel;
		try {
			endogenousLabel = EndogenousLabel.valueOf(label.replace(" ", "_"));
		} catch (IllegalArgumentException exception) {
			endogenousLabel = null;
		}

		if (endogenousLabel == null) {
			
			String attributeValue = source.getAttributeValue(label);
			
			if (attributeValue == null) {
				if (label.toUpperCase().contains("FIRST_CHILD")) {
					String firstChildLabel = label.replaceFirst("FIRST_CHILD_", "");
					result = getFirstChildAttribute(source, firstChildLabel, geography);
				} else if (label.toUpperCase().contains("_POPULATION")){
					Relation relation = source.relations().getById(Integer.parseInt(((String)parameter)));
					String relationAttributeLabel = label.substring(0, label.indexOf("_POPULATION")).trim();
					attributeValue = relation.getAttributeValue(relationAttributeLabel);
					result = Value.valueOf(attributeValue);
				} else if (label.toUpperCase().contains("YEAR")) {
						String dateLabel = label.replaceAll("YEAR", "DATE");					
						String year = extractYear(source.getAttributeValue(dateLabel));
						if (year == null) {
							result = null;
						} else {
							result = new Value(Integer.parseInt(year));
						}
				} else if (label.toUpperCase().contains("PLAC")) {
					String homonym = source.getAttributeValue(label);
					if (geography != null) {
						String toponym = null;
						if (parameter == null || StringUtils.isBlank(((String)parameter))) {
							toponym = geography.getToponym(homonym);
						} else {
							toponym = geography.getToponym(homonym,(String)parameter);
						}
						if (toponym==null){
							if (homonym==null){
								result = null;
							} else {
								result = new Value(homonym);
							}
						} else {
							result = new Value(toponym);
						}
					} else {
						result = null;
					}
				} else {
					result = null;
				}
			} else if (valueOf(GeoLevel.class, attributeValue) != null) {
				result = new Value(GeoLevel.valueOf(attributeValue));
			} else {
				if (label.toUpperCase().contains("YEAR")) {
					String dateLabel = label.replaceAll("YEAR", "DATE");					
					String year = extractYear(source.getAttributeValue(dateLabel));
					if (year == null) {
						result = null;
					} else {
						result = new Value(Integer.parseInt(year));
					}
				} else if (label.toUpperCase().contains("PLAC")) {
					String homonym = source.getAttributeValue(label);
					if (geography != null) {
						String toponym = null;
						if (parameter == null || StringUtils.isBlank(((String)parameter))) {
							toponym = geography.getToponym(homonym);
						} else {
							toponym = geography.getToponym(homonym,(String)parameter);
						}
						if (toponym==null){
							if (homonym==null){
								result = null;
							} else {
								result = new Value(homonym);
							}
						} else {
							result = new Value(toponym);
						}
					} else {
						result = new Value(attributeValue);
					}
				} else {
					result = new Value(attributeValue);
				}
			}
		} else {
			switch (endogenousLabel) {
				case BIRTH_ORDER:
					//
					if (source.getBirthOrder() == null) {
						//
						result = null;

					} else {
						//
						result = new Value(source.getBirthOrder());
					}
				break;
				case DEPTH:
					// if (label.equals("DEPTH")) v.getDepth(2);
					result = new Value(StatisticsWorker.depth(source));
				break;
				case FIRSTN:
					if (source.getFirstName() == null) {
						result = null;
					} else {
						result = new Value(source.getFirstName());
					}
				break;
				case SIBSETM: {
					// return getSibset(1);
					Individual parent = source.getMother();
					if (parent == null) {
						result = new Value(-source.getId());;
					} else {
						result = new Value(parent.getId());
					}
				}
				break;
				case SIBSETP: {
					// return getSibset(0);
					Individual parent = source.getFather();
					if (parent == null) {
						result = new Value(-source.getId());;
					} else {
						result = new Value(parent.getId());
					}
				}
				break;
				// case GEN:
				// result = new Value(StatisticsWorker.gen(source));
				// break;
				case ID:
					result = new Value(source.getId());
				break;
				case LASTN:
					if (source.getLastName() == null) {
						result = null;
					} else {
						result = new Value(source.getLastName());
					}
				break;
				case PARTN:
					// Near from SPOU but different.
					result = new Value(source.getPartners().size());
				break;
				case MATRIC:
					result = new Value(StatisticsWorker.ancestor(source, FiliationType.UTERINE));
				break;
				case MATRID:
					// if (label.equals("MATRID")) v.getDepth(1);
					result = new Value(StatisticsWorker.depth(source, FiliationType.UTERINE));
				break;
				case MDEPTH:
					// return meanDepth();
					result = new Value(StatisticsWorker.meanDepth(source));
				break;
				case PATRIC:
					result = new Value(StatisticsWorker.ancestor(source, FiliationType.AGNATIC));
				break;
				case PATRID:
					// if (label.equals("PATRID")) v.getDepth(0);
					result = new Value(StatisticsWorker.depth(source, FiliationType.AGNATIC));
				break;
				case PEDG:
					// return getNrLinKin(deg);
					if ((parameter == null) || (!(parameter instanceof String)) || (!NumberUtils.isNumber((String) parameter))) {
						result = null;
					} else {
						result = new Value(StatisticsWorker.numberOfLinearKin(source, Integer.parseInt((String) parameter)));
					}
				break;
				case PROG:
					// return getNrLinKin(-deg);
					if ((parameter == null) || (!(parameter instanceof String)) || (!NumberUtils.isNumber((String) parameter))) {
						result = null;
					} else {
						result = new Value(StatisticsWorker.numberOfLinearKin(source, -1 * Integer.parseInt((String) parameter)));
					}
				break;
				case GENDER:
					result = new Value(source.getGender());
				break;
				case SPOU:
					// return nrSpouses();
					result = new Value(source.spouses().size());
				break;
				case SIMPLE:
					result = new Value(source.getName());
				break;
				case LIFESTATUS:
					if (parameter == null) {
						result = new Value(lifeStatusAtYear(source, Calendar.getInstance().get(Calendar.YEAR)));
					} else if ((!(parameter instanceof String)) || (!NumberUtils.isNumber((String) parameter))) {
						result = null;
					} else {
						result = new Value(lifeStatusAtYear(source, Integer.parseInt((String) parameter)));
					}
				break;
				case MATRISTATUS:
					if (parameter == null) {
						result = new Value(matrimonialStatusAtYear(source, Calendar.getInstance().get(Calendar.YEAR)));
					} else if ((!(parameter instanceof String)) || (!NumberUtils.isNumber((String) parameter))) {
						result = null;
					} else {
						result = new Value(matrimonialStatusAtYear(source, Integer.parseInt((String) parameter)));
					}
				break;
				case REFERENT:
					result = null;
					if (parameter != null){
						String[] parameters = ((String)parameter).split("\\s");
						String modelName = parameters[0];
						String roleName = parameters[1];
						String time = parameters[2];
						result = null;
						for (Relation relation : source.relations().getByModelName(modelName)){
							if (relation.getAttributeValue("DATE")!=null && relation.getAttributeValue("DATE").equals(time)){
								Actor ego = relation.getActor(source, roleName);
								if (ego!=null && ego.getAttributeValue("REF")!=null){
									result = new Value(Integer.parseInt(ego.getAttributeValue("REF")));
								}
							}
						}
					}
				break;
				case OCCUPATION: 
					result = null;
					if (parameter != null){
						for (Relation occupation : source.relations().getByModelName("OCCUPATION")){
							if (occupation!=null && occupation.getAttributeValue("DATE")!=null && occupation.getAttributeValue("DATE").equals(parameter)){
								if (occupation.getAttributeValue("OCCU")!=null){
									result = new Value(occupation.getAttributeValue("OCCU"));
								}
							}
						}
					}
				break;
				case AGE:
					if (parameter == null) {
						result = new Value(ageAtYear(source, Calendar.getInstance().get(Calendar.YEAR)));
					} else if ((!(parameter instanceof String)) || (!NumberUtils.isNumber((String) parameter))) {
						result = null;
					} else {
						Integer age = ageAtYear(source, Integer.parseInt((String) parameter));
						if (age == -1) {
							result = null;
						} else {
							result = new Value(age);
						}
					}
				break;
				case MINIMAL_AGE:
					if (parameter == null) {
						result = null;
					} else {
						result = new Value(hasMinimalAgeAtYear(source, Calendar.getInstance().get(Calendar.YEAR),Integer.parseInt((String) parameter)));
					}
				break;
				case UNIONSTATUS:
					result = new Value(getMatrimonialStatus(source));
				break;
				case FIRST_MARR_DATE:
					result = null;
					if (!source.isUnmarried()) {
						Integer firstMarrYear = null;
						for (Family family : source.getPersonalFamilies()) {
							String marrDate = family.getAttributeValue("MARR_DATE");
							Integer marrYear = extractYearAsInt(marrDate);
							if (marrDate != null && (firstMarrYear == null || marrYear < firstMarrYear)) {
								firstMarrYear = marrYear;
								result = new Value(marrDate);
							}
						}
					}
				break;
				default:
					result = null;
			}
		}

		//
		return result;
	}

	/**
	 * This method is a helper one.
	 * 
	 * @param individual
	 * @param label
	 * @return
	 */
	public static NumberedValues get(final Individuals source, final String label, final Geography geography) {
		NumberedValues result;

		result = get(source, label, null, geography);

		//
		return result;
	}

	/**
	 * 
	 * @param individual
	 * @param label
	 * @return
	 */
	public static NumberedValues get(final Individuals source, final String label, final Object parameter, final Geography geography) {
		NumberedValues result;

		//
		result = new NumberedValues();

		//
		if (label.equals("GEN")) {
			// Compute.
			NumberedIntegers data = StatisticsWorker.genData(source);

			// Translate;
			for (Integer key : data.keySet()) {
				result.addValue(key, data.get(key));
			}
		} else if (label.equals("DEPTH")) {
			// Compute.
			NumberedIntegers data = StatisticsWorker.depthData(source, FiliationType.COGNATIC);

			// Translate;
			for (Integer key : data.keySet()) {
				result.addValue(key, data.get(key));
			}
		} else if (label.equals("MATRIC")) {
			// Compute.
			HashMap<Individual, Individual> ancestors = StatisticsWorker.ancestors(source, FiliationType.UTERINE);

			// Translate;
			for (Individual ego : ancestors.keySet()) {
				result.addValue(ego.getId(), ancestors.get(ego));
			}
		} else if (label.equals("MATRID")) {
			// Compute.
			NumberedIntegers depths = StatisticsWorker.depthData(source, FiliationType.UTERINE);

			// Translate;
			for (Integer individualId : depths.keySet()) {
				result.addValue(individualId, depths.get(individualId));
			}
		} else if (label.equals("PATRIC")) {
			// Compute.
			HashMap<Individual, Individual> ancestors = StatisticsWorker.ancestors(source, FiliationType.AGNATIC);

			// Translate;
			for (Individual ego : ancestors.keySet()) {
				result.addValue(ego.getId(), ancestors.get(ego));
			}
		} else if (label.equals("PATRID")) {
			// Compute.
			NumberedIntegers depths = StatisticsWorker.depthData(source, FiliationType.AGNATIC);

			// Translate;
			for (Integer individualId : depths.keySet()) {
				result.addValue(individualId, depths.get(individualId));
			}
		} else {
			for (Individual individual : source) {
				result.put(individual.getId(), get(individual, label, parameter, geography));
			}
		}

		//
		return result;
	}
	
	public static boolean isIndividualAttributeLabel (String label, final Individuals individuals){
		boolean result;
		
		result = getAttributeLabels(individuals).contains(label) || label.contains("_POPULATION");
		
		//
		return result;
	}

	/**
	 * 
	 * @param individuals
	 * @return
	 */
	public static StringList getAttributeLabels(final Individuals individuals) {
		StringList result;

		result = getAttributeLabels(individuals, null);

		//
		return result;
	}

	/**
	 * @param individuals
	 * @return
	 */
	public static StringList getAttributeLabels(final Individuals individuals, final Integer limit) {
		StringList result;

		//
		result = new StringList(20);

		//
		for (EndogenousLabel label : EndogenousLabel.values()) {
			result.add(label.toString());
		}

		//
		result.addAll(getExogenousAttributeLabels(individuals, limit));

		//
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * 
	 * @param individuals
	 * @return
	 */
	public static StringList getAttributeLabelSample(final Individuals individuals) {
		StringList result;

		result = getAttributeLabels(individuals, 10000);

		//
		return result;
	}

	public static Integer getBirthYear(final Individual individual) {
		Integer result;

		if (individual == null) {
			result = null;
		} else {
			String birtYear = extractYear(individual.getAttributeValue("BIRT_DATE"));
			if (birtYear == null) {
				result = null;
			} else {
				result = Integer.parseInt(birtYear);
			}
		}

		//
		return result;
	}

	public static Integer getDeathYear(final Individual individual) {
		Integer result;

		if (individual == null) {
			result = null;
		} else {
			String deatYear = extractYear(individual.getAttributeValue("DEAT_DATE"));
			if (deatYear == null) {
				result = null;
			} else {
				result = Integer.parseInt(deatYear);
			}
		}

		//
		return result;
	}

	/**
	 * @param individuals
	 * @return
	 */
	public static StringList getEndogenousAttributeLabels() {
		StringList result;

		//
		result = new StringList(20);

		//
		for (EndogenousLabel label : EndogenousLabel.values()) {
			result.add(label.toString());
		}

		//
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * @param individuals
	 * @return
	 */
	public static StringList getExogenousAttributeLabels(final Individuals individuals) {
		StringList result;

		//
		result = getExogenousAttributeLabels(individuals, null);

		//
		return result;
	}

	/**
	 * @param individuals
	 * @return
	 */
	public static StringList getExogenousAttributeLabels(final Individuals individuals, final Integer limit) {
		StringList result;

		//
		result = new StringList(20);

		//
		HashSet<String> buffer = new HashSet<String>();
		if (individuals != null) {
			int index = 0;
			Iterator<Individual> iterator = individuals.iterator();
			while ((iterator.hasNext()) && ((limit == null) || (index < limit))) {
				Individual individual = iterator.next();
				for (Attribute attribute : individual.attributes()) {
					buffer.add(attribute.getLabel());
				}
				index += 1;
			}
		}

		for (String string : buffer) {
			result.add(string);
		}

		//
		Collections.sort(result);

		//
		return result;
	}

	public static Individual getFirstChild(final Individual individual) {
		Individual result;

		result = individual.getKin(KinType.CHILD, Sorting.BIRT_YEAR, 1);

		//
		return result;
	}

	public static Value getFirstChildAttribute(final Individual individual, final String label, final Geography geography) {
		Value result;

		result = null;
		Individual firstChild = getFirstChild(individual);
		if (firstChild != null) {
			result = IndividualValuator.get(firstChild, label, geography);
		}

		//
		return result;
	}

	public static Integer getMarriageYear(final Family family) {
		Integer result;

		result = null;

		if (family != null) {
			String year = extractYear(family.getAttributeValue("MARR_DATE"));
			if (year != null) {
				result = Integer.parseInt(year);
			}
		}
		//
		return result;
	}

	public static Integer getMarriageYear(final Individual ego, final Individual alter) {
		Integer result;

		result = null;
		Family family = getUnion(ego, alter);

		if (family != null) {
			String year = extractYear(family.getAttributeValue("MARR_DATE"));
			if (year != null) {
				result = Integer.parseInt(year);
			}
		}
		//
		return result;
	}

	public static String getMatrimonialStatus(final Individual source) {
		String result;

		result = "SINGLE";
		if (source.getPersonalFamilies() != null) {
			for (Family family : source.getPersonalFamilies()) {
				result = "NOT_SINGLE";
				if (family.getUnionStatus().equals(UnionStatus.MARRIED)) {
					if (result.equals("MARRIED")) {
						result = "REMARRIED";
					} else {
						result = "MARRIED";
					}
				} else if (family.getUnionStatus().equals(UnionStatus.DIVORCED)) {
					result = "DIVORCED";
				}
			}
		}
		//
		return result;
	}

	public static Family getUnion(final Individual ego, final Individual alter) {
		Family result;

		result = null;
		Families families = ego.getPersonalFamilies();

		if (families != null) {
			for (Family family : families) {
				if (family.getOtherParent(ego) != alter) {
					result = family;
					break;
				}
			}
		}
		//
		return result;
	}

	public static String lifeStatusAtYear(final Individual source, final int year) {
		String result;

		Integer birtYear = getBirthYear(source);
		Integer deatYear = getDeathYear(source);

		result = "UNKNOWN";

		if (birtYear != null && birtYear > year) {
			result = "UNBORN";
		} else if (deatYear != null && deatYear < year) {
			result = "DEAD";
		} else if (birtYear != null) {
			result = (year - birtYear) + "";
		}

		//
		return result;
	}

	public static String matrimonialStatusAtYear(final Individual source, final int year) {
		String result;

		Integer birtYear = getBirthYear(source);
		Integer deatYear = getDeathYear(source);

		result = "UNKNOWN";

		if (birtYear != null && birtYear > year) {
			result = "UNBORN";
		} else if (deatYear != null && deatYear < year) {
			result = "DEAD";
		} else {
			Families families = source.getPersonalFamilies();
				int nrSpouses = 0;
				int divorced = 0;
				int widowed = 0;
				int married = 0;
				for (Family family : families) {
					Integer marrYear = FamilyValuator.getMarrYear(family);
					Integer divYear = FamilyValuator.getMarrYear(family);
					Integer spouseDeathYear = IndividualValuator.getDeathYear(family.getOtherParent(source));

					if (family.getOtherParent(source) != null && (marrYear == null || marrYear <= year)) {
						nrSpouses++;
						if (family.getUnionStatus() == UnionStatus.DIVORCED && (divYear == null || divYear <= year)) {
							divorced++;
						} else if (spouseDeathYear != null && spouseDeathYear <= year) {
							widowed++;
						} else {
							married++;
						}
					}
				}
				if (nrSpouses == 0){
					result = "CELIBATARY";
				} else if (divorced == nrSpouses) {
					result = "DIVORCED";
				} else if (widowed == nrSpouses) {
					result = "WIDOWED";
				} else if (married == nrSpouses) {
					result = "MARRIED";
				} else if (married == 0) {
					result = "DIVORCED-WIDOWED";
				} else if (divorced == 0) {
					result = "WIDOWED-(RE)MARRIED";
				} else if (widowed == 0) {
					result = "DIVORCED-(RE)MARRIED";
				} else {
					result = "DIVORCED-WIDOWED-(RE)MARRIED";
				}
				if (married > 1) {
					result += "-POLYGAMOUS";
				}
		}

		//
		return result;
	}

	public static <T extends Enum<T>> T valueOf(final Class<T> cls, final String name) {
		T result;

		result = null;

		try {
			result = Enum.valueOf(cls, name);
		} catch (IllegalArgumentException iae) {
			result = null;
		}
		//
		return result;
	}

}
