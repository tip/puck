package org.tip.puck.io.selz;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a line of individual from a TXT file.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class SELZTXTFamilyLine {
	public static final int NOID = 0;
	public static final char UNMARRIED = 'U';
	public static final char MARRIED = 'M';
	public static final char DIVORCED = 'D';

	private int id;
	private int fatherId;
	private int motherId;
	private int fatherFamilyId;
	private int motherFamilyId;
	private int fatherOrd;
	private int motherOrd;
	private char status;
	private String childIds;
	private List<String> attributesValues;

	/**
	 * 
	 */
	public SELZTXTFamilyLine() {
		this.id = NOID;
		this.status = UNMARRIED;
		this.fatherId = NOID;
		this.motherId = NOID;
		this.fatherFamilyId = NOID;
		this.motherFamilyId = NOID;
		this.fatherOrd = NOID;
		this.motherOrd = NOID;
		this.childIds = null;
		this.attributesValues = new ArrayList<String>();
	}

	public int getFatherFamilyId() {
		return fatherFamilyId;
	}

	public void setFatherFamilyId(int fatherFamilyId) {
		this.fatherFamilyId = fatherFamilyId;
	}

	public int getMotherFamilyId() {
		return motherFamilyId;
	}

	public void setMotherFamilyId(int motherFamilyId) {
		this.motherFamilyId = motherFamilyId;
	}

	public int getFatherOrd() {
		return fatherOrd;
	}

	public void setFatherOrd(int fatherOrd) {
		this.fatherOrd = fatherOrd;
	}

	public int getMotherOrd() {
		return motherOrd;
	}

	public void setMotherOrd(int motherOrd) {
		this.motherOrd = motherOrd;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> attributeValues() {
		List<String> result;

		result = this.attributesValues;

		//
		return result;
	}

	public String getChildIds() {
		return childIds;
	}

	public int getFatherId() {
		return fatherId;
	}

	public int getId() {
		return id;
	}

	public int getMotherId() {
		return motherId;
	}

	public char getStatus() {
		return status;
	}

	public void setChildIds(final String childIds) {
		this.childIds = childIds;
	}

	public void setFatherId(final int fatherId) {
		this.fatherId = fatherId;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public void setMotherId(final int motherId) {
		this.motherId = motherId;
	}

	public void setStatus(final char status) {
		this.status = status;
	}
}
