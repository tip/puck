package org.tip.puck.census.chains;

import java.util.ArrayList;
import java.util.List;

import org.tip.puck.census.workers.CircuitFinder;
import org.tip.puck.census.workers.SiblingMode;
import org.tip.puck.census.workers.SymmetryType;
import org.tip.puck.net.Families;
import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.Numberable;
import org.tip.puck.util.Value;

import oldcore.trash.OldRing;
import oldcore.trash.RingGroupMap;



public class Chain extends ArrayList<Individual> implements Comparable<Chain>, Numberable {


/*	private static int compareSubchains (Chain chain1, Chain chain2) {
		int result;
		
		result = new Integer(chain2.get(0).getGender().toInt()).compareTo(chain1.get(0).getGender().toInt());
		
		if (result==0){

			result = new Integer(chain1.size()).compareTo(chain2.size());
			
			if (result==0){
				
				result = new Integer(chain1.getCharacteristicNumber()).compareTo(chain2.getCharacteristicNumber());
			}
		}
		//
		return result;
	}*/


	/**
	 * 
	 */
	private static final long serialVersionUID = -7338230463348261936L;

	int id;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private static int getSpouseIndex(int i, int n){
		int result;
		
		if (i%2==0) {
			result = (i+n-1)%n;
		} else {
			result = (i+1)%n;
		}
		return result;
	}
	
	/**
	 * determines the type of notation of a ring
	 * @param str the ring notation
	 */
	public static Notation notation (String str) {
		Notation result = null;

		if ((Character.isDigit(str.charAt(0)) || Character.isDigit(str.charAt(1)))) {
			result = Notation.VECTOR;
		} else if (str.charAt(1)==' ' || Gender.isGenderSymbol(str.charAt(0))) {
			result = Notation.CLASSIC_GENDERED;
		} else if ((Character.isLetter(str.charAt(0))) || (str.charAt(0)=='(')) {
			result = Notation.POSITIONAL;
		}
	      return result;
	}
	
		
	
	
	
/*	private static String toString(Gender g){
		if (g == Gender.MALE) return "H";
		if (g == Gender.FEMALE) return "F";
		return "X";
	}*/
	
	
	/**
	 * the sequence of direction indices (1 ascending, -1 descending, O marriage)
	 */
	private List<Integer> directions = new ArrayList<Integer>();
	//	String signature;
	
	//replace by dim()? - for the moment only calculated for chains that are constructed incrementally and not by subchain composition
	/**
	 * the number of consanguine components of the chain
	 */
	private int order;
	
//	int depth;
	
	/**
	 * the sequence of indices of pivot vertices
	 */
	List<Integer> pivots;
	
	/**
	 * the sequence of indices of apical vertices
	 */
	List<Integer> apices;

	/**
	 * the array of linear subchain lengths
	 */
	private int[] depths;
	
	/**
	 * the characteristic vector of the chain
	 */
	Vector vector;
	
	/**
	 * the symmetry type of the chain
	 */
	private SymmetryType symmetry;
	
	public String closingRelation;
	

	public List<Chain> subchains;
	
	
	/**
	 * standard constructor for chains
	 */
	public Chain (){
	}	
	
	/**
	 * constructs a chain containing only one individual
	 * @param ego the single member of the chain
	 */
	public Chain (Individual ego){
		if (ego!=null) {
			add(ego);
		}
//		add(ego,0);
	}
	
	/**
	 * constructs a chain containing only one individual, with the link to the following individual
	 * @param ego the single member of the chain
	 */
	public Chain (Individual ego, int dir){
		if (ego!=null) {
			add(ego);
			directions.add(dir);
		}
	}
	
	
	public Chain (Individual ego, Individual alter){
		add(ego);
		add(alter);
		directions.add(0);
		directions.add(0);
	}
	
	
	/**
	 * adds an individual to a chain
	 * @param indi the vertex to be added
	 * @param direction the direction at the vertex
	 */
	public boolean add(Individual indi, int direction){

		if (indi==null) {
			return false;
		}

		if (size()>0) {
			directions.add(direction);
		}

		return super.add(indi);
	}
	
	public boolean addAll(Chain c){
		for (int dir : c.directions){
			directions.add(dir);
		}
		return super.addAll(c);
	}
	
	public boolean addAllWithMarriage (Chain c){
		if (size()>0 && !getLast().equals(c.getFirst())) {
			directions.add(0);
		}
		for (int dir : c.directions){
			directions.add(dir);
		}
		return super.addAll(c);
	}
	
	//more uses? 
	/**
	 * adds an apex index
	 * @param i the apex index
	 */
	void addApex(int i){
		if (apices==null) apices=new ArrayList<Integer>();
		apices.add(i);
	}
	
	/**
	 * adds an apex index
	 * @param i the apex index
	 */
	void addApices(List<Integer> list){
		if (apices==null) apices=new ArrayList<Integer>();
		apices.addAll(list);
	}
	
	public boolean addInv (Chain c){
		for (int i=c.size()-2;i>-1;i--){
			add(c.get(i));
			directions.add(-c.dir(i+1));
		}
		return true;
	}
	
	// add directions!
	public Chain inverse (){
		Chain result = new Chain();
		for (int i=size()-1;i>-1;i--){
			result.add(get(i));
//			directions.add(-dir(i+1));   
		}
		//
		return result;
	}
	
	// first part: addAll, second part: addInv
	/**
	 * adds an ascending or descending path to a ring<p>
	 * used to construct rings from paths
	 * @param p the path to be added
	 * @param i the direction of the path (1 ascending, -1 descending)
	 * @see OldRing#Ring(Path, Path)
	 */
/*	private void add (Chain p, int i){
		if (i>0) {
			addAll (p);
			directions.add(0);
		} else {
			for (int j=p.size()-2;j>=0;j--) {
				add(p.get(j));
			}
		}
		for (int j=1;j<p.size();j++) {
			directions.add(i);
		}
	}*/
	
	/**
	 * returns the gender combination of the ith apical sibling pair<p>
	 * value of the "ARCH" property
	 * @param i the number of the sibling pair
	 * @return the gender combination of the ith apical sibling pair
	 */
/*	private String arch (int i){
		if (lin(i,0)==0 || lin(i,1)==0) return "-";
		int a=getNextApex(pivot(2*i));
		int g = gender(a-1).toInt()+gender(a+1).toInt();
		if (g==0) return "HH";
		if (g==2) return "FF";
		return "HF";
	}*/	

	
	public String baseSignature(){
		String t = "";
		for (int i=0;i<size();i++){
			t=t+getId(i)+" ";
		}
		return t;
	}
	
	public Chain clone (){
		Chain chain = new Chain();
		chain.setSymmetry(symmetry);
		
		chain.addAll(this);
		if (subchains!=null){
			chain.subchains = new ArrayList<Chain>();
			for (Chain subchain : subchains){
				chain.subchains.add(subchain.clone());
			}
//			chain.subchains.addAll(subchains);
		}
//		chain.order = order;
//		chain.pivots = pivots;
		return chain;
	}
	
	public Chain clone (int id){
		Chain result;
		
		result = clone();
		result.setId(id);
		
		//
		return result;
	}
	
	public int compareTo(Chain c){
		int result;
		
		result = new Integer(size()).compareTo(c.size());
		if (result==0){
			for (int i=0;i<size();i++){
				result = get(i).compareTo(c.get(i));
				if (result!=0) {
					break;
				}
			}
		}
		//
		return result;
	}
	
	//integrated in KeyComparator 
	/**
	 * compares two rings according to length and (ascending) direction
	 * @param r the Ring to be compared
	 * @return the comparison index (1, 0 or -1)
	 */
/*	public int compareTo(Element e){
		Chain r = (Chain)e;
		int i=size()-r.size();
		if (i!=0) return i;
		for (int j=0;j<size();j++){
			i = dir(j)-r.dir(j);
			if (i!=0) return i;
		}
		return 0;
	}*/	
	
	

	//contained in concatenate2
	/**
	 * adds another ring to the ring under the possible condition that all vertices are distinct
	 * @param chain2 the ring to be added
	 * @param noRepeat condition that all vertices have to be distinct
	 * @return true if the ring has been added, else false
	 * @see OldRing#concatenate(OldRing, OldRing)
	 */
/*	private boolean add (Chain chain2, boolean noRepeat) {
		int n = size();
		for (int i=0;i<chain2.size();i++){
			if (noRepeat && contains(chain2.get(i),n)) return false;
			add(chain2.get(i),chain2.dir(i)); 
		}
		return true;
	}*/

	
	/**
	 * checks whether a ring segment contains a given vertex
	 * @param v the vertex to be searched
	 * @param n the length of the segment (from 0)
	 * @return true if the ring segment contains the vertex
	 * @see OldRing#add(OldRing, boolean)
	 */
	boolean contains (Individual v, int n){
		for (int i=0;i<n;i++){
			if (get(i).getId()!=0 && get(i).equals(v)) return true;
		}
		return false;
	}	
	


    
	public String coupleSignature (){
		boolean cons = false;
		String s = getFirst().getName();
		Individual lastAlter = getFirst();
		for (int i=1;i<size();i++){
			if (dir(i)!=0) continue;
			cons=true;
			if (!get(i-1).equals(lastAlter)) s = s+" - "+ get(i-1).getName();
			lastAlter = get(i);
			s=s+" = "+lastAlter.getName();
		}
		if (cons) s = s+" - "+ getLast().getName();
		else s = s+" = "+ getLast().getName();
		return s;
	}
	
	public void removeLast (){
		directions.remove(size()-2);
		remove(size()-1);
	}

	private void removeFirst (){
		directions.remove(0);
		remove(0);
	}

	
	/**
	 * returns the order (number of consanguinous components) of the ring
	 * @return the number of consanguinous components of the ring
	 */
	public int dim(){
		int result;
		
		if (subchains!=null){
			result = subchains.size()/2;
		} else {
			result = 1;
			for (int d : directions){
				if (d==0){
					result++;
				}
			}
		}
		//
		return result;
	}	
	
	 /**
	  *	returns the (in-)direction at the ith position of the chain
	  * @param i the position in the ring
	  * @return the direction at the ith position of the chain (1 ascending, -1 descending, 0 marriage)
	  */
	public int dir(int i){
		return directions.get(i-1);
	}	
	
	
	//simplify and adapt commentary
	/**
	 * checks whether the ith position is not descending and followed by a marriage position
	 * @param i the position to be checked
	 * @return true if the ith position is not descending and followed by a marriage position
	 * @see OldRing#profile(boolean)
	 */
/*	private boolean doublestop (int i){
		if (i<size()-1 && dir(i)==0 && dir(i+1)!=1)	return true;
		return dir(i)!=-1 && (i==length() || dir(i+1)==0);
	}*/	
	
	public boolean equals (Object e){
		Chain c = (Chain)e;
		if (size()!=c.size()) return false;
//		if (size()==0) return last == c.last;
		for (int i=0;i<size();i++){
			if (!get(i).equals(c.get(i))) return false;
		}
		return true;
	}
	
	/**
	 * checks equality with another Ring, with or without consideration of apical membersByRelationId
	 * @param r the ring to be compared with this ring
	 * @param withoutApex true if apical membersByRelationId are not to be considered
	 * @return true if the two rings are equal
	 * @see OldRing#neutralize(OldRing, int)
	 */
/*	private boolean equals (Chain c, boolean withoutApex){
		if (size()!=c.size()) return false;
		for (int i=0;i<size();i++){ 
			if (withoutApex && apices.contains(i)) continue;//i==firstApex()
			if (getId(i)!=c.getId(i)) return false;
		}
		return true;
	}*/
	
	/**
	 * expands the ring by adding a parent of the last ring member and returns true if there is such a parent<p>
	 * used for searching kinship chains between two vertices
	 * @param t the type of relative (1 parent, 0 spouse, -1 child)
	 * @param j the number of the relative of a given kind
	 * @return true if a parent has been added
	 * @see Ring#search(Vertex, int, int, ArrayList)
	 */
/*	public boolean expand (int t, int j){
		if (lastDir()==-1 && t==1) return false;
		Individual v = getLast().getKin(t, j);
		if (v==null) return false;
		if (v.isVisited1()) return false;
		add(v);
		directions.add(t);
		return true;
	}*/
	
	//Important! Check use and adapt!
	//Check the standardization procedures at the end!
	/**
	 * determines the rings which correspond to this ring up to the ith position of their pivot chain<p>
	 * used for determining all rings of a network
	 * @see NumberChain#complete(RingGroupMap, int)
	 * @param i the position up to which the bridges of a pivot chain have been specified
	 * @param d the maximal degree of consanguineous partial rings
	 * @param p the pivot chain of the ring
	 * @param rings the ringmap where found rings are to be stored
	 * @param g the unilinear filter key (-1 no restriction, 0 agnatic, 1 uterine)
	 */
/*	void expand (int i, int d, Chain p, CircuitFinder3 rings, int g) {

		if (rings.ringType>0 && CircuitFinder3.isRoundabout(this,rings.sib)) return;
//	    if (isRoundabout(-1)) return;
		int n = p.size();
		int j = (i+n-2)%n;
		if (j > 0) {   
			for (Chain r : p.getBridges(j,d,g,rings.sib,rings)) {
				Chain t = ChainMaker.concatenate(this,r);
				if (t!=null) t.expand(j,d,p,rings,g);
			}
		} else if (size()>1) {
			rings.add(standard(rings.symmetry);
		}
	}*/	
	
	/**
	 * gets the characteristic number corresponding to a chain of gender numbers
	 * @return the characteristic number
	 * @see chains.OldRing#setVector()
	 */
/*	 public static int number (String str) {
		 int n = 0;
		 for (int i=0;i<str.length();i++) {
			 int a = 1;
			 if (str.charAt(i)=='F') a = 2;
//			 int b = 1; Negative sign in case of unknown apical gender
//			 if (str.charAt(i)=='X') b = -1;
			 n = n + new Double(Math.pow(2,i)*a).intValue();
		 }
		 return n;
	 }*/
	
/*	private int firstApex(){
		if (apices==null) return -1;
		return apices.get(0);
	}*/
	
	/**
	 * gets the next apical position following a given position
	 * @see OldRing#getApexGender(int)
	 * @param i the given position
	 * @return the next apical position
	 */
/*	private int getApex (int i){
		return getNextApex(pivot(2*i));
	}*/
	
	/**
	 * gets the gender of the next apical member following a given position<p>
	 * used for determining the linear type of the ring
	 * @see OldRing#lineType(ArrayList, int)
	 * @param i the given position
	 * @return the gender of the next apical member
	 */
/*	private Gender getApexGender(int i){
		return gender(getApex(i));
	}*/
	 
	//to ChainValuator    
	//simplify
	/**
	 * gets the cluster value of the ring according to a given property code
	 * @param propertyLabel the property code
	 * @return the cluster value of the ring 
	 */
/*	public Object getAttributeValue (String propertyLabel) {
		String[] s = propertyLabel.split(" ");
		if (s.length>1) {
			String c = "";
			for (String d : s){
				c = c+" "+getHeading(d);
			}
			return c;
		}
    	if (propertyLabel.equals("SIMPLE")) return subchains;
		if (propertyLabel.equals("LENGTH")) return length();
		if (propertyLabel.equals("HEIGTH")) return getDepth();//degMax();
		if (propertyLabel.equals("WIDTH")) return dim(); 
		if (propertyLabel.equals("SYM")) return sym();      
		if (propertyLabel.equals("HETERO")) return isHetero();
		if (propertyLabel.equals("DEGREE")) return profile(0);      
		if (propertyLabel.equals("ENDS")) return profile(1);      
		if (propertyLabel.equals("SKEW")) return profile(2);      
		if (propertyLabel.equals("LINE")) return profile(3);      
		if (propertyLabel.equals("AGNA")) return profile(4);      
		if (propertyLabel.equals("UTER")) return profile(9);      
		if (propertyLabel.equals("DRAV")) return profile(5);  
		if (propertyLabel.equals("DRAV-H")) return profile(6);  
		if (propertyLabel.equals("DRAV-O")) return profile(10);  
		if (propertyLabel.equals("SWITCHES")) return profile(7);  
		if (propertyLabel.equals("ARCH")) return profile(8);  
		if (propertyLabel.equals("FORM")) return getDepth()+"�"+profile(2); //degMax()+...  
		if (propertyLabel.equals("DRAVLINE")) return profile(3)+"�"+profile(5);  
		if (propertyLabel.equals("DRAVSKEW")) return profile(2)+"�"+profile(5);  
		if (propertyLabel.equals("SKEW+")) return profile(11);
		return null;    
	}*/    
	
	/**
	 * gets the characteristic number of a linear chain
	 * @return
	 */
	public int getCharacteristicNumber(){
		int result;
		
		result = 0;
				
		for (int i=0;i<size();i++) {
			
			 int a = 0;
			 
			 if (get(i).getGender().isMale()) {
				 
				 a = 1;
				 
			 } else if (get(i).getGender().isFemale()) {
				 
				 a = 2;
				 
			 } else if (get(i) instanceof Couple){
				 
				 a = 1;
				 
			 } else {
				 
				 result = 0;
				 break;
			 }
			 //
			 result = result + new Double(Math.pow(2,i)*a).intValue();
		 }
		
		 if (getLast() instanceof Couple) {
			 
			 result = -1*result;
		 }
		 //
		 return result;
	}
	
	public Vector getCharacteristicVector(){
		
		if (vector == null){
			
			int n = subchains.size();
			int[] v = new int[n];
			
			for (int i=0;i<n;i++){
				
				v[i] = subchains.get(i).getCharacteristicNumber();
				
			}
			//
			vector = new Vector(v);
		}
		//
		return vector;
	}
	
	//to ChainValuator
	/**
	 * gets a string representation of the cluster values of a ring according to a list of property codes
	 * @see AbstractWriter#writeSurvey
	 * @param list the list of property codes
	 * @return the string representation of the cluster values of the ring 
	 */
/*	public String getClusters(List<String> list) {
		if (list==null) return "";
		String str = "";
		for (String clu : list) {
			str = str+"\t"+getAttributeValue(clu);
		}
		return str;
	}*/	
	
	/**
	 * gets a copy of the ring in the perspective of the ith pivot
	 * @see OldRing#getDoubles(boolean)
	 * @param i the number of the pivot chosen as ego
	 * @return the copy of the ring
	 */
	private Chain getCopy(int i){
		return clone().transform(i);
	}	

	
	public Chain getCouple (int i){
		Chain result;
		
		result = new Chain();
		int j = getSpouseIndex(i,2*dim());
		result = new Chain(subchains.get(i).getFirst(), subchains.get(j).getFirst());
		
		//
		return result;
	}
	
/*	public List<Chain> getCouples(Gender firstGender){
		List<Chain> result;
		
		result= new ArrayList<Chain>();
		
		for (int i=0;i<2*dim();i++){
			Chain couple = getCouple(i);
			if (couple.getFirst().getGender()==firstGender) {
				result.add(couple);
			}
		}
		return result;
	}*/
	
	public static Chain getCouple (Individual ego, Individual alter, boolean crossSex, SymmetryType symmetry, Gender firstGender){
		Chain result;
		
		if (symmetry == SymmetryType.INVARIABLE){
			result = new Chain(ego,alter);
		} else if (crossSex){
			if (ego.getGender()==firstGender){
				result = new Chain(ego,alter);
			} else {
				result = new Chain(alter,ego);
			}
		} else if (ego.getId()<alter.getId()) {
			result = new Chain(ego,alter);
		} else {
			result = new Chain(alter,ego);
		}
		
		result.setSymmetry(SymmetryType.INVERTIBLE);
		
		//
		return result;
		
	}
	
	public List<Chain> getCouples(boolean crossSex, SymmetryType symmetry, Gender firstGender){
		List<Chain> result;
		
		result= new ArrayList<Chain>();
		
		for (int i=0;i<2*dim();i++){
			if (i>0 && symmetry!=SymmetryType.PERMUTABLE){
				break;
			}
			Chain couple = getCouple(i);
			if (symmetry == SymmetryType.INVARIABLE){
				result.add(couple);
			} else if (crossSex){
				if (couple.getFirst().getGender()==firstGender){
					result.add(couple);
				} else {
					result.add(couple.inverse());
				}
			} else if (couple.getFirst().getId()<couple.getLast().getId()) {
				result.add(couple);
			} else {
				result.add(couple.inverse());
			}
		}
		return result;
	}

	
	public List<Chain> getCouples(){
		List<Chain> result;
		
		result= new ArrayList<Chain>();
		int dim = dim();
		
		for (int i=0;i<dim;i++){
			Chain couple = new Chain();
//			Individual first = subchains.get(2*i).getFirst();
//			Individual second = subchains.get((2*dim+2*i-1)%(2*dim)).getFirst();
			Individual first = getPivot(2*i);
			Individual second = getPivot(2*i-1);
			
			if (first.isMale()) {
				couple.add(first);
				couple.add(second);
			} else {
				couple.add(second);
				couple.add(first);
			}
			result.add(couple);
		}
		return result;
	}
	
	public int depth() {
		int result = 0;
		
		if (subchains!=null){
			for (Chain subchain : subchains){
				int length = subchain.length();
				if (length>result) {
					result = length;
				}
			}
		} else {
			int length = 0;
			for (int i = 1; i<size();i++) {
				if (dir(i)>0) {
					length += dir(i);
				} else if (dir(i)<0) {
					if (i>1 && dir(i-1)>0){
						length = 0;
					}
					length -= dir(i);
				} else {
					if (length>result) {
						result = length;
					}
					length = 0;
				}
				if (length>result) {
					result = length;
				}
			}
		}
		
		//
		return result;
	}

	
	/**
	 * returns the depth of the ring
	 * @return the ring depth
	 */
/*	public int getDepth (){
		int result = 0;
		for (Chain subchain : getSubchains()){
			if (subchain.length()>result){
				result = subchain.length();
			}
		}
		//
		return result;

/*		int d=0; 
		int k=0; 
		for (int i=1;i<size();i++){
			if (dir(i)==0 || (dir(i)==-1 && dir(i-1)==1)) k=0;
			if (dir(i)==1 || dir(i)==-1) k++;
			if (k>d) d=k;
		}
		return d;
	}*/
	
	/**
	 * returns the height (canonic degree) of the highest partial consanguineous ring, with minimum 1 (identity as consanguinity)
	 * @return the maximal canonic degree of the ring
	 * @deprecated
	 */
/*	private int getDepth2(){
		int k = 0;  // k = 1; changed 11.4.10 
		for (int i : profile(true)){
			if (i>k) k=i;
		}
		return k;
	}*/	
	
	//check "model" function
	/**
	 * gets the isomorphs of a ring or its model
	 * @param model true if only the isomorphs of a model (without identity numbers) are required
	 * @return the isomorphs of the ring
	 * @see OldRing#reportDoubles(int)
	 * @see io.write.AbstractWriter#writeRingLine(RingGroup,int,int,boolean)
	 * Replaced by {@link ChainMaker#getPermutations(Chain)}
	 */
/*	public List<Chain> getDoubles (boolean model) {
		if (model) return new ChainCluster (this,-1); 
		List<Chain> c = new ArrayList<Chain>();
		for (int i=0;i<2*dim();i++) {
			c.add(getCopy(i));
		}
		return c;
	}*/	
	
	public Individual getFirst(){
		return get(0);

	}
	
	public int getGenderInt(int i){
		Gender gender = get(i).getGender();
		if (gender == Gender.MALE) return 0;
		if (gender == Gender.FEMALE) return 1;
		return -1;
	}
	
	public int getId(int i){
		return get(i).getId();
	}

	
	public Individual getLast(){
		return get(size()-1);
	}
	
	/**
	 * gets the size of the last branch of the chain
	 * @return the size of the last branch of the chain
	 * @see chains.OldRing#searchChains(RingGroupMap, List)
	 * @since 10/04/12
	 */
	int getLastBranchSize(){
		int n = length();
		if (dir(n)==0) return 0;
		int k = 1;
		int i = n;
		while (dir(i-1)==dir(i)){
			k++;
			i--;
		}
		return k;
	}
	
	/**
	 * gets the last married vertex
	 * @param the underlying net
	 * @return the ID number corresponding to the last married vertex
	 * @see maps.groupmaps.RingGroupMap#incorrectProperties(NumberChain)
	 */
	public Individual getLastMarried (){
		Individual result;
		
		result = null;
		int lastMarriageYear = -100000000;
		
		for (int i=0;i<size();i++){
			if (dir(i)==0) {
				Integer year = IndividualValuator.getMarriageYear(get(i), get(i+1));
				if (year!=null && year > lastMarriageYear) {
					lastMarriageYear = year;
					result = get(i);
				}
			}
		}
		//
		return result;
	}
	
	// applied to bases
	public Family getLastMarriage (Families domain){
		Family result;
		result = null;
		
		int lastMarriageYear = -100000000;
		
		for (int i=0;i<size()/2;i++){
			Family family = domain.getBySpouses(get(2*i),get(2*i+1));
			Integer year = IndividualValuator.getMarriageYear(family);
			if (year!=null && year > lastMarriageYear) {
				lastMarriageYear = year;
				result = family;
			}
		}
		
		//
		return result;
	}
	
	// applied to whole chains
	public List<Integer> getMarriageYears (Families domain){
		List<Integer> result;
		result = new ArrayList<Integer>();
		
		for (int i=0;i<dim();i++){
			Family family = domain.getBySpouses(getPivot(2*i),getPivot(2*i-1));
			result.add(IndividualValuator.getMarriageYear(family));
		}
		
		//
		return result;
	}
	
	public List<Family> getFamilyChain(Families domain, String relationType){
		List<Family> result;
		
		result = new ArrayList<Family>();
		if (size()>1) {
			int i=0;
			if (relationType.equals("OPEN")){
				i=1;
			} else if (!relationType.equals("SPOUSE") && !relationType.equals("PARTN")){
				i=2;
			}
			for (int j=0;j<size()/2-1;j++){
				Individual ego = get(i+2*j);
				Individual alter = get(i+2*j+1);
				Family family = domain.getBySpouses(ego, alter);
				result.add(family);
			}
		}
		
		//
		return result;
	}
	
	public boolean hasNoFamiliesInDomain(Families domain, String relationType){
		boolean result;
		
	    if (relationType.equals("SPOUSE")  || relationType.equals("PARTN")){
	    	result = true;
			for (int j=0;j<size()/2;j++){
				Individual ego = get(2*j);
				Individual alter = get(2*j+1);
				Family family;
				if (ego.isMale()){
					family = domain.getBySpouses(ego, alter);
				} else {
					family = domain.getBySpouses(alter, ego);
				}
				if (family!=null){
					result = false;
					break;
				}
			}
	    } else {
	    	result = false;
	    }
		
		//
		return result;
	}
	
	public boolean hasFamiliesOutOfDomain(Families domain, String relationType){
		boolean result;
	    if (relationType.equals("SPOUSE")  || relationType.equals("PARTN")){
	    	result = false;
			for (int j=0;j<size()/2;j++){
				Individual ego = get(2*j);
				Individual alter = get(2*j+1);
				Family family;
				if (ego.isMale()){
					family = domain.getBySpouses(ego, alter);
				} else {
					family = domain.getBySpouses(alter, ego);
				}
				if (family==null){
					result = true;
					break;
				}
			}
	    } else {
	    	result = false;
	    }
		
		//
		return result;
	}

	
	/**
	 * gets the type of kin relation between a ring member and its successor
	 * @param i the position of the ring member
	 * @return the type of kin relation (-1 ch, 0 f, 1 m, 2 sp)
	 * @see trash.RingAsNet#RingAsNet(maps.Net, OldRing)
	 */
	public int getLink (int i){
		if (i==length()) return 2;
		int j = dir(i+1);
		if (j==0) return 2;
		if (j==-1) return -1;
		return get(i+1).getGender().toInt()%2;
	}
	
	// necessary?
	public Gender gender (int i){
		Gender result;
		
		result = get(i).getGender();
		
		//
		return result;
	}

	
	public int getMarrPos (int pivotNr){
		int marrNr = pivotNr/2;
		if (pivotNr%2!=0) marrNr = (pivotNr+1)/2;
		int count=0;
		int pos=0;
		for (int dir : directions){
			if (dir==0) {
				count++;
				if (count==marrNr) break;
			}
			pos++;
		}
		if (pivotNr%2==0) pos++;
		return pos;
	}
	
	
	//check
	//rename to "cloneWithElements"
	/**
	 * gets a model of the ring
	 * @return a model of the ring
	 * @see OldRing#combine(OldRing)
	 */
/*	public Chain getModel (){
		Chain t = new Chain();
		t.setSymmetry(symmetry);
		
		int k = 0;
		for (Individual v : this) {
			t.add(v.clone(),dir(k));
			k++;
		}
		return t.standard();
	}*/
	
	/**
	 * gets the next ring member in apical position
	 * @param i the current position // only in ascending direction ??
	 * @return the next ring member in apical position
	 */
/*	private int getNextApex(int i) {
		if (i==size()-1 || dir(i+1)!=1) return i;
		return getNextApex(i+1);
	}*/
	
	/**
	 * gets the subchains of the ring
	 * @return the linear paths of the ring
	 */
/*	private List<Chain> getPaths (){
		List<Chain> paths = new ArrayList<Chain>();
		Chain q = new Chain(getFirst());
		int j = 0;
		for (int i=1;i<size();i++){
			if (turn (i)){
				if (j%2==1) q = q.inverse();
				paths.add(q);
				j++;
				q = new Chain();				
				if (toJoin(i-1)) q.add(get(i-1));
				if (single(i-1)) {
					paths.add(new Chain(get(i-1)));
					j++;
				}
			}
			q.add(get(i));
		}
		if (dir(length())>=0) {
			paths.add(q);
			paths.add(new Chain(getLast()));
//			paths.add(new Path(get(size()-1)));
		} else paths.add(q.inverse());
		return paths;
	}*/	
	
	/**
	 * gets the ith pivot of the ring
	 * @param i the position of the pivot
	 * @return the required pivot 
	 */
	public Individual getPivot (int i){
		Individual result;
		
		int n = 2*dim();
		result = subchains.get((i+n)%n).getFirst();
		
		//
		return result;
		
//		if (i==2*dim()) i=0;
//		return get(pivot(i));
	}

	/**
	 * returns the current index of the ith pivot of the ring
	 * @param i the position of the pivot
	 * @return the current index of the ith pivot
	 * @see maps.AbstractNet#getArcs(RingGroupMap, int)
	 */
/*	public int getPivotIndex (int i) {
		return getPivot(i).getCurrent();
	}*/
	
	/**
	 * gets the ith pivot pair of the ring
	 * @param i the position of the pivot pair
	 * @return the required pivot pair
	 * @see maps.groupmaps.RingGroupMap#countMembers(boolean)
	 */
/*	public Individual[] getPivots (int i) {
		Individual[] p = new Individual[2];
		p[0] = getPivot(i);
		int j = i+2*(i%2)-1;
		p[1] = getPivot(j);
//		if (!p[0].equals(p[1])) return p;
//		p[1] = getPivot(2*(i%2));
		return p;
	}*/	
	
	// harmonize with getCharacteristicVector
	/**
	 * gets the characteristic vector of the ring
	 * @return the characteristic vector of the ring
	 */
/*	public int[] getVector(){
	   if (vector==null) setVector();
	   return vector;
	}*/
	
/*	public int[][] getSemivector(){
		int n = vector.length;
		int[][] v = new int[n][n];
		
		
		return v;
	}*/	
	
	boolean hasElementsInCommon (Chain chain){
		for (Individual individual : chain){
			if (contains(individual)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * checks if the ring intersects with a ring of a ringclass c
	 * @param c the ringclass to be checked
	 * @return true if the ring intersects with a ring of that class
	 * @see OldRing#hasIntersectionIn(RingGroupMap)
	 */
	private boolean hasIntersectionIn (Cluster<Chain> c) {
		for (Chain r : c.getItems()) {
			if (intersects(r)) return true;
		}
		return false;
	}
	
	/**
	 * checks whether the ring intersects with a shorter ring in the ringnet rn
	 * @param rn the ringnet to be checked
	 * @return true if there is no shorter ring intersecting with the ring
	 * @see maps.groupmaps.RingGroupMap#reduceToMinimals()
	 */
	public boolean hasIntersectionIn (CircuitFinder rn) {
		for (Cluster<Chain> c : rn.getCircuits().getClusters()){
			if (c.getFirstItem().length()<length() && hasIntersectionIn(c)) return true;
		}
		return false;
	}	
	
    /**
	 * checks if the ring has a marriage in common with the ring r
	 * @param r the ring to be compared
	 * @return true if the two rings have a marriage in common
	 * @see OldRing#hasIntersectionIn(RingGroup)
	 */
	private boolean intersects (Chain r) {
		for (int i = 0;i<2*r.dim();i++) {
			if (intersects(r,i)) return true; 
		}
		return false;
    }

	/**
	 * checks if the ring connects the same individuals than another ring in position i
	 * @param r the ring to be compared
	 * @param i the position of the second ring
	 * @return true if the ring connects the same individuals
	 * @see Ring#intersects(Ring);
	 */
	private boolean intersects (Chain r, int i) {
		Chain s = r.transform(i);
		return getFirst().equals(s.getFirst()) && getLast().equals(s.getLast());
	}	

/*	private boolean apical (int i){
		return dir(i-1)==1 && dir(i)==-1;
	}*/
	
	/**
	 * checks whether a given chain position is (bilaterally) apical (ascent before, descent after)
	 * @param i the chain position
	 * @return true if the position is apical
	 */
/*	private boolean apical(int i){
		boolean result;
		if (i==0 || i==size()-1){
			result = false;
		} else if (dir(i)==1 && dir(i+1)==-1) {
			result = true;
		} else {
			result = false;
		}
		//
		return result;
	}*/
	
	/**
     * checks whether all pivot pairs of the ring are heterosexual
     * @return true if all pivot pairs of the ring are heterosexual
     * @see groups.RingGroup#develop(boolean, String, int, int)
     */
	public boolean isHetero () {
		for (int i=0;i<dim();i++) {
			if (getPivot(2*i).getGender()==getPivot(2*i-1).getGender()) return false;
		}
		return true;
	}	
	
	//rename
	//harmonize with isHetero()
	/**
	 * checks whether the corner pivots are of the same gender and adjusts the gender of genderless pivots to the partner pivot
	 * <p> used for canonical closure
	 * @return true if the pivots are of the same gender (or if their gender has been adjusted)
	 * @see OldRing#zip()
	 */
	private boolean sameSex (){
		Gender g1 = getFirst().getGender();
		Gender g2 = getLast().getGender();
		if (g1==g2) return true;
		if (g1.isUnknown()) {
			getFirst().setGender(g2);
			return true;
		} else if (g2.isUnknown()) {
			getLast().setGender(g1);
			return true;
		}
		return false;
	}
	
	/**
	 * checks whether this ring is isomorph to a ring from a model class<p>
	 * used for census by kinship schema 
	 * @param models the model class
	 * @param exact true if order of vertices matters
	 * @return true if there is an isomorphic ring in the model class
	 */
	public boolean isModel (List<Chain> models,boolean exact){
		if (models==null) return true;
		for (Chain r : models){
			if (modelEquals(r,exact)) return true;
		}
		return false;
	}	
	
	/**
	 * checks if there is a twin ring of this ring in a list and neutralizes the ring if required 
	 * @param list the ring list 
	 * @param sib the sibling mode of the census
	 * @return true if there is no twin ring in the list
	 * @see OldRing#getBridges(ArrayList, Individual, int, int, int)
	 */
/*	public boolean isOriginal (List<Chain> list, int sib) {
		if (sib==2) return true;
		if (noApex()) return true;
		if (sib==1) neutralize();
		int k=0;
		for (Chain s : list) {
			k++;
			if (s.noApex()) continue;
			if (!apices.equals(s.apices)) continue;
			if (s.neutralize(this,sib)) return false;
		}
		return true;
	}*/
	
	/**
	 * checks the isomorphy of the ring with another ring
	 * @param t the ring to be compared
	 * @param exact true if order matters
	 * @return true if the rings are isomorph
	 * @see OldRing#standard(boolean)
	 * @see OldRing#isModel(RingGroup, boolean)
	 */
	public boolean modelEquals (Chain t, boolean exact){
		Vector v = standard().getCharacteristicVector();
		Vector w = t.getCharacteristicVector();
		if (!exact) w = t.standard().getCharacteristicVector();
		return (v.equals(w));
		
/*		if (dim()!=t.dim()) return false;
		for (int i=0;i<2*dim();i++){
			if (v.number(i)!=w.number(i)) return false;
		}
		return true;*/
	}	
	
	//rename in isPivot
	/**
	 * checks whether a ring position is a pivot position
	 * @param i the position index
	 * @return true if the position i is a pivot position
	 */
	public boolean pivotal (int i){
		if (i==0 || i==size()-1) return true;
		if (pivots==null) return false;
		for (int j : pivots){
			if (j==i || j-1==i) return true;
		}
		return false;
	}	
	
	/**
	 * direction of the last link in the chain
	 * @return
	 */
	public int lastDir (){
		int result; 
		if (size()<2){
			result = 0;
		} else {
			result = directions.get(size()-2);
		}
		return result;
	}
	
	public int length(){
		return size()-1;
	}

	
	private String letter (int i) {
		Gender gender = get(i).getGender();
		int dir = dir(i);
		if (gender.isMale()){
			if (dir==1 && get(i) instanceof Couple) return "X";
			if (dir==1) return "F";
			if (dir==-1) return "S";
			if (dir==0) return "H";
		} else if (gender.isFemale()){
			if (dir==1) return "M";
			if (dir==-1) return "D";
			if (dir==0) return "W";
		} else if (gender.isUnknown()){
			if (dir==1) return "Pa";
			if (dir==-1) return "Ch";
			if (dir==0) return "Sp";
		}
		return "";
	}
	
	/**
	 * get a letter representation of the ith ring position, according to notation
	 * @param i the position
	 * @param key the notation key (0 track, 1 positional, 2 standard, 3 numeric, 4 nominative pivots)  
	 * @return a letter representation of the position
	 * @see OldRing#signature(int)
	 */
	private String lit (int i, int key){
		String s = "";
		switch (key){
		case 0: if (getId(i)==0) s = get(i).getName();
				else s = getId(i)+"";
				return surround(s," ",i);
		case 1: if (gender(i).isUnknown() && getId(i)!=0);
				return surround(get(i).getGender().toChar()+"","",i);
		case 2: if (i==0) return "";
				final String[][] e = {{"S","D",""},{"H","W",""},{"F","M","X"}};
				return e[dir(i)+1][gender(i).toInt()];
		case 3: return vector.getNumbers()[i]+" ";
		case 4: final String[] t = {" - "," = "};
				if (i<2*dim()) s = t[i%2];
				return getPivot(i).getName()+s;
		case 7: return lit(i,2);
		case 8: return lit(i,2);
		case 9: final String[][] p = {{"g","f",""},{"G.g","F.f",""},{"G","F",""}};
				int k = dir(i)+1;
				if (k==1) k=2;
				if (quasiApical(i)) k=1;
				return p[k][gender(i).toInt()];
		}
		return null;
	}
	
	/**
	 * checks the existence of an alter different from ego in the ith bridge of the ring
	 * @param i the position of the bridge
	 * @param j the branch of the bridge 
	 * @return 1 if there is a true alter, 0 if the bridge is identity
	 */
/*	private int lin (int i, int j) {
		ArrayList<Integer> p = profile(true);
		int a = p.get(2*i+j);
		if (a!=0) return 1;
		return 0;
	}*/	
	
	/**
	 * checks the linear character of a consanguineous ring
	 * @return true if the ring is linear
	 * @see RingGroup#linear()
	 */
/*	private boolean linear (){
		if (size()<2) return true;
		for (int i=1;i<size();i++){
			if (dir(i)!=dir(1)) return false;
		}
		return true;
	}*/
	
	/**
	 * returns the proportion of unilinear filiation ties in a consanguinous bridge 
	 * @param p the profile of component path length of the ring
	 * @param j the position of the bridge in the ring
	 * @param g the type of filiation (0 paternal, 1 maternal)
	 * @return the proportion of unilinear filiation ties (as a percentage of total ties)
	 */
/*	private double linear(List<Integer> p, int j, Gender g) {
		double result;
		
		double k = 0;
		int x = lin(j,0);
		int y = lin(j,1);
		int n = p.get(j)+1-x-y;
		if (n==1) return 100.; 
		int a = pivot(2*j)+x;
		int b = pivot(2*(j+1))-y;
		for (int i=a;i<b;i++) {
			if (gender(i).matchs(g)) k = k+1.;
		}
		return percent(k,n);
	}*/
	
	/**
	 * returns the linear type of the ith bridge of the ring
	 * <p> value of the "LINE" property
	 * @param p the profile of component path length of the ring
	 * @param i the position of the ring
	 * @return the kinship type (I identity, A agnatic, U uterine, B bilateral, C cognatic)
	 */
/*	public FiliationType lineType (List<Integer> p, int i) {
		      if (p.get(i) == 0) return FiliationType.IDENTITY;
		      if (switches(i)!=0) return FiliationType.COGNATIC;
		      if (getApexGender(i).isMale()) return FiliationType.AGNATIC;
		      if (getApexGender(i).isFemale()) return FiliationType.UTERINE;
		      if (p.get(i) == 2) return FiliationType.BILATERAL;
		      if (get(pivot(2*i)+1).getGender().isMale() || get(pivot(2*i+1)-1).getGender().isMale()) return FiliationType.AGNATIC;
		      return FiliationType.UTERINE;
	}*/
	
	public Chain neutralize (SiblingMode sib) {
		return neutralize(sib,0);
	}
	
	public boolean containsDoubleElements (){
		boolean result;
		
		result = false;
		List<Individual> previous = new ArrayList<Individual>();
		for (Individual individual : this){
			if (previous.contains(individual)){
				result = true;
				break;
			}
			previous.add(individual);
		}
		//
		return result;
	}


	
	/**
	 * neutralizes the ring without labeling the neutral firstApex
	 * @see OldRing#isOriginal(ArrayList, int)
	 * @since last modified 10/04/12
	 */
	public Chain neutralize (SiblingMode sib, int lastId) {
		Chain result;
		if (sib==SiblingMode.NONE) {
			result = this;
		} else {
			if (subchains==null){
				getSubchains();
			}
			
			result = clone();
			for (int i = 0; i < dim(); i++){
				Chain subchain1 = result.subchains.get(2*i);
				Chain subchain2 = result.subchains.get(2*i+1);
				
				if (subchain1.size()<2 || subchain2.size()<2) {
					continue;
				}

				Couple neutralApex = new Couple();

				if (sib==SiblingMode.FULL) {
					
					Family first = subchain1.get(subchain1.size()-2).getOriginFamily();
					Family second = subchain2.get(subchain2.size()-2).getOriginFamily();
					if (first==second) {
						Individual husband = first.getHusband();
						Individual wife = first.getWife();

						//full-sibling fratries with unknown parent may happen if corpus in gedcom format
						if (first.getHusband()==null || first.getWife()==null) {
							if (lastId==0){
								continue;
							} else {
								if(husband==null){
									husband = first.getMissingHusband(lastId);
								}
								if (wife==null){
									wife = first.getMissingWife(lastId);
								}
							}
						}
						neutralApex = new Couple(first.getHusband(),first.getWife());
					} else {
						continue;
					}
				}

				result.set(result.indexOf(subchain1.getLast()),neutralApex);
				subchain1.set(subchain1.size()-1, neutralApex);
				subchain2.set(subchain2.size()-1, neutralApex);
//				set(apex,new Couple());
//	    	set(i,a);
			}
	    }
	    //	    
		return result;
	}
	
	

	
	
	/**
	 * compares the ring with a potential twin ring and neutralizes it if required 
	 * @param r the potential twin ring
	 * @param sib the sib mode
	 * @return true if the ring has been neutralized
	 * @see OldRing#isOriginal(ArrayList, int)
	 * @since last modified 10/04/12
	 */
/*	private boolean neutralize (Chain r, int sib) {
	//	   	if (sib==2) return false;
//		if (firstApex==-1) firstApex = getApex();
//		if (apices==null) return false;
		if (!equals(r,true)) return false;
		if (sib==1) return true;
		for (int i : apices){
			Individual v = get(i);
			Individual w = r.get(i);
			if (v.equals(w)) continue;
//			if (v.getGender()==2 || w.getGender()==2) continue; //already neutralized
			Individual[] cp = new Individual[2];
//			Vertex v = get(firstApex-1);
//			Vertex w = r.get(firstApex-1);
			try{
				cp[v.getGender().toInt()]=v;
				cp[w.getGender().toInt()]=w;
			} catch (IndexOutOfBoundsException iob) {
				continue; //already neutralized
			}
			neutralize (cp,i);
		}
		return true;
	}*/
	
	/**
	 * neutralizes the ring and labels the neutral firstApex with the numbers of the apical couple
	 * @param cp the numbers of the apical couple
	 * @see OldRing#neutralize(OldRing, int)
	 * @since last modified 10/04/12
	 */
/*	private void neutralize (Individual[] cp, int i) {
		String lb = "";
	    if (cp!=null) lb = cp[0].getID()+" "+cp[1].getID();
	    Individual a = new Individual(0,lb,2);
		a.setHome(ego().getHome());
		a.setApiCouple(cp);
		set(i,a);
//	    if (firstApex>0) set(firstApex-1,a);
	}*/

	
/*	private boolean noApex(){
		if (apices==null) return true;
		return apices.size()==0;
	}*/
	
	/**
	 * get the ith pivot position
	 * @ the number of the pivot 
	 */
/*	private int pivot (int i){
		if (i==0) return 0;
		if (i==2*dim() || i==-1) return length(); 
		if (i>2*dim()) return size();               //check!
		return pivots.get((i-1)/2) - i%2;
	}*/
	
	//full?
	/**
	 * gets the lengths of the component paths of the ring
	 * @param full
	 * @return the lengths of the component paths of the ring
	 */
/*	private ArrayList<Integer> profile (boolean full){
		ArrayList<Integer> p = new ArrayList<Integer>();
		int j=0;
		int k=-1;
		for (int i=0;i<size();i++){
			if (dir(i)==0 || (full && apical(i))) {
				k++;
				j=0;
				if (full && apical(i)) j=1;
				p.add(j);
		   } else {
			   	j++;
			   	p.set(k,j);
		   }
		   if (full && doublestop(i)) {
			   	k++;
			   	j=0;
			   	p.add(j);
		   }
		}
		return p;
	}*/
	
	//keys in String form? 
	/**
	 * gets a property profile of the ring
	 * @param key the property key
	 * @return the property profile of the ring
	 */
/*	public String profile (int key) {        
		boolean full = false;
		if (key == 2 || key==5 || key == 6 || key == 11) full = true;
		List<Integer> p = profile(full);
		
		List<Chain> q = new Cluster<Chain>();
		if (key == 5 || key ==6 || key == 10) q = getSubchains();
		
		String s = "";
		for (int i=0;i<dim();i++) {
			s = s + profile(q,p,key,i);   
		}
		return s;
	}*/	
	

	
	
	/**
	 * checks whether a given ring position is quasi-apical (no descent before, no ascent after)
	 * @param i the ring position
	 * @return true if the position is quasi-apical
	 */
	private boolean quasiApical (int i){
//		Alternative (check equivalence):		
//		if (i==length()) return dir(i)!=-1;
//		return dir(i)!=-1 && dir(i+1)!=1;

//		if (dir(i)==0 && dir(i+1)==0) return true;
//		return (dir(i)==1 || dir(i+1)==-1) && (dir(i)!=dir(i+1));
		
		return ((dir(i)==0 && dir(i+1)==0) || (dir(i)==1 || dir(i+1)==-1) && (dir(i)!=dir(i+1)));
	}
	
	
	//check
	/**
	 * rearranges a (not necessarily heterosexual) ring so as to begin with the first pivot
	 * @return the rearranged ring
	 * @see OldRing#zip()
	 */
	private Chain recombine (){
		setOrder(getOrder() - 1);
		Chain r = new Chain();
		setPivots();
		if (pivots.size()==0) return r;
		int j = pivots.get(0);
		for (int i = j;i<size();i++){
			r.add(get(i),directions.get(i));
		}
		for (int i = 0;i<j;i++){
			r.add(get(i),directions.get(i));
		}
		return r;
	}	
	
	// make static
	/**
	 * reflects the ring (inverts ego and alter position)
	 * @return the ring after reflection
	 */
	public Chain reflect () {
		return transform(dim()*2-1);  // width*2+1
	}

	
/*	public String signature (int k, boolean neutral){
		String t = "";
		if (k==0) t = " ";
		String[] a = subChains[k];
		String s = "";
		for (int i=0;i<a.length;i++){
			String[] b = a[i].split("\\s");
			int n = b.length;
			for (int j=0;j<n;j++){
				int u = j;
				if (i%2==1) u = n-1-j;
				if (i%2==0 && j==n-1) {
					if (neutral){
						int m = a[i+1].split("\\s").length;
						if (n>1 && m>1) s = s+t+"()";
					}
					else s = s+t+"("+b[j]+")";
				}
				else if (j>0 || i%2==0)  s = s+t+b[u];
			}
			if (i%2==1 && i<a.length-1) s=s+t+".";
		}

		return s;
	}*/
	
	//headlines 14
	/**
	 * gets a report on the basic ring properties
	 * @param str a string representation of the ring (only for report purposes, does not enter in any operation)
	 * @return the report on the basic ring properties as a string list
	 */
	public ArrayList<String> report (String str) {
		ArrayList<String> rp = new ArrayList<String>();
/*		rp.add(t(0)+" "+str+":");      
		rp.add("");      
		rp.add(t(1)+" \t"+signature(2));      
		rp.add(t(2)+" \t"+signature(1));      
		rp.add(t(3)+" \t"+signature(3));  
		rp.add(t(30)+" \t"+signature(13));
		rp.add("");      
		rp.add(t(4)+" ("+t(5)+") \t"+length());      
		rp.add(t(6)+" ("+t(7)+") \t"+getDepth());//+degMax());      
		rp.add(t(8)+" ("+t(9)+") \t"+dim());      
		rp.add(t(10)+" \t"+sym());      
		rp.add("");      	
		rp.add(hetero());
		rp.add(t(11)+" \t"+profile(0));      
		rp.add(t(12)+" \t"+profile(1));      
		rp.add(t(13)+" \t"+profile(2));      
		rp.add(t(14)+" \t"+profile(3));      
		rp.add(t(15)+" \t"+profile(8)); 
		rp.add(t(16)+" \t"+profile(4)); 
		rp.add(t(17)+" \t"+profile(9)); 
		rp.add(t(18)+" \t"+profile(5)); 
		rp.add(t(19)+" \t"+profile(6)); 
		rp.add(t(20)+" \t"+profile(7)); */
		return rp;   
	}
	
	//headlines 14
	/**
	 * gets the list of isomorphs of the ring
	 * @param k the ring signature key
	 * @return the isomorphs of the ring as a string list
	 * see ChainMaker.getPermutations
	 */
/*	public ArrayList<String> reportDoubles (int k) {
		ArrayList<String> report = new ArrayList<String>();
		report.add(t(21)+" "+signature1(k));
		report.add("");
		for (Chain r : getDoubles(true)) {
			report.add(r.signature1(k));
		}
		return report;
	}*/	
	
	/**
	 * rotates the ring once (moves the ego position to the next even pivot)
	 * @return the ring after rotation by 1
	 */
	public Chain rotate () {
		return transform(2);
	}
	
	/**
	 * rotates the ring i times (moves the ego position i times to the next even pivot)
	 * @param i the number of rotations
	 * @return the ring after i rotations
	 */
	private Chain rotate (int i) {
		return transform(2*i);
	}
	
	/**
	 * sets the apical positions of the ring
	 * @return the array of apical positions
	 */
/*	private void setApices (){
		for (int i=0;i<size();i++){
			if (apical(i)) {
				apices.add(i);
			}
		}
	}*/

	
	public List<Chain> getSubchains (){
		if (subchains==null){
			subchains = new ArrayList<Chain>();
			Chain subchain = new Chain();
			boolean up = true;
			for (int i=0;i<size();i++){
				subchain.add(get(i));
				if (turn (i)){
					// close a subchain 
					if (up){
					    subchains.add(subchain.clone());
					} else {
						subchains.add(subchain.inverse());
					}
					up = !up;
					
					subchain = new Chain();
					
					// redouble the last individual if in apical position (following an ascending or marriage link)
					if (i==0 || dir(i)>=0) {
						subchain.add(get(i));
						if (i==length() || dir(i+1)==0) {
							// close the single-individual chain
							subchains.add(subchain.clone());
							if (i<length()){
								up = !up;
								subchain = new Chain();
							}
						}
						
					}
//					if (single(i)) {
//						subchains.add(new Chain(get(i)));
//						up = !up;
//					}
				}
			}
/*			subchain.add(getLast());
			if (dir(length())>=0) {
				subchains.add(subchain.clone());
				subchains.add(new Chain(get(length())));
			} else {
				subchains.add(subchain.inverse());
			}*/
		}
		// add directions
		for (Chain subchain : subchains){
			for (int i=0;i<subchain.length();i++){
				subchain.directions.add(1);
			}
		}
		
		
		return subchains;
	}	
	

	/**
	 * adds a pivot index to the pivot index chain
	 * @param i the pivot index
	 * @see OldRing#Ring(String)
	 */
/*	void setPivot (int i){
		if (pivots==null) pivots = new ArrayList<Integer>();
		pivots.add(i);
	}*/
	
	//more general use?
	/**
	 * sets the pivot index chain
	 * @see OldRing#recombine()
	 */
	public void setPivots(){
		pivots = new ArrayList<Integer>();
		if (dim()==1) return;
//		if (length()==0) return;
		for (int i=1;i<size();i++){
			if (dir(i)==0) pivots.add(i);
		}
	}
	
	//Generalize; check for compose and concatenate
	/**
	 * sets the pivot index chain after concatenation with another ring r
	 * @param r the second ring
	 * @see OldRing#Ring(OldRing, OldRing, boolean)
	 * @see OldRing#concatenate(OldRing)
	 */
	void setPivots (Chain r){
		pivots = new ArrayList<Integer>();
		if (r.pivots!=null) pivots.addAll(r.pivots); 
		pivots.add(r.size());
	}	
	
	//harmonize with getCharacteristicVector
	/**
	 * sets the characteristic vector of the ring
	 */
/*	private void setVector (){
		if (vector!=null) return;
		int n = dim()*2;
		NumberChain[] p = new NumberChain[n];
		NumberChain q = new NumberChain(gender(0));
		int j = 0;
		for (int i=1;i<size();i++){
			if (turn (i)){
				if (j%2==0) p[j] = q;
				else if (j%2==1) p[j] = q.inverse();
				j++;
				q = new NumberChain();				
				if (toJoin(i-1)) q.add(gender(i-1));
				if (single(i-1)) {
					p[j]=new NumberChain(gender(i-1));
					j++;
				}
			}
			q.add(gender(i));
		}
		if (dir(length())>=0) {
//			if (dir(size()-1)>=0) {
			p[j]=q;
			p[j+1]=new NumberChain(gender(length()));
//			p[j+1]=new NumberChain(gender(size()-1));
		} else p[j]=q.inverse();
		vector = new int[n];
		depths = new int[n];
		for (int i=0;i<n;i++){
			vector[i]=p[i].number();
			depths[i]=p[i].length();
		}
	}*/
	
	private String sig(int i, Notation type){
		String result = "";
		if (!(get(i) instanceof Couple)){
			if (type==Notation.NUMBERS) {
				result = getId(i)+"";
			} else {
				result = get(i).getGender().toChar()+"";
			}
		} else if (type==Notation.NUMBERS) {
			result = ((Couple)get(i)).signature();
		}
		
		// check apical position
		if ((size()==1) ||          // single-individual chain
			(i==0 && dir(1)==-1) || // first link descending   	
			(i==size()-1 && dir(size()-1)==1) ||  // last link ascending
			(i>0 && i<size()-1 && dir(i)!=dir(i+1) && (dir(i)==1 || dir(i+1)==-1))){  // preceding link ascending, succeding link descending
			result = "("+result+")";
		}
			
		/*		String par = "("+result+")";
		if (size()==1) return par;
		if (i==0 && dir(1)==-1) return par;
		if (size()==2 && i==1) return par;
		if (i==size()-1 && dir(size()-1)==1) return par;
		if (i>0 && i<size()-1 && dir(i)!=dir(i+1) && (dir(i)==1 || dir(i+1)==-1)) return par;*/
		return result;
	}
	
	
	/**
	 * gets a plain signature of the ring (standard and character)
	 * @return a plain signature of the ring (standard and character)
	 */
	public String signature () {
		return signature(Notation.CLASSIC_GENDERED)+"\t"+signature(Notation.POSITIONAL);
	}
	
	//for the moment only keys 0 and 1 (attention - old methods in ChainCluster expect much more keys)
/*	public String signature(int key){
		
		String sep = " ";
		if (key == 1) sep = "";
		String s = sig(0,key)+sep;
		for (int i=1;i<size();i++){
			if (dir(i)==0) s = s+"."+sep;
			s = s + sig(i,key)+sep;
		}
		return s;
	}*/
	
	public String getPrologString(){
		String result = "";
		
		List<Character> var = new ArrayList<Character>();
		var.add(' ');
		for (char c='A'; c<'Z';c++){
			var.add(c);
		}
		
		boolean ascending = true;
		int lastId = 0;
		
		for (Chain subchain : subchains) {
			String relation = "";
			for (int i=0;i<subchain.size();i++){
				if (i>0) {
					if (subchain.get(i).isMale()){
						relation = "father";
					} else if (subchain.get(i).isFemale()){
						relation = "mother";
					} else  {
						relation = "parent";
					}
					relation = relation+"("+var.get(subchain.getId(i))+","+var.get(subchain.getId(i-1))+")";
				} else {
					if (ascending && lastId > 0){
						relation="married("+lastId+","+var.get(subchain.getId(0))+")";
					}
				}
				if (result.length()==0){
					result = relation;
				} else if (relation.length()>0)  {
					result = result+","+relation;
				}
			}
			if (!ascending){
				lastId = subchain.getId(0);
			}
			if (!ascending) {
				ascending = true;
			} else {
				ascending = false;
			}
//			ascending = (ascending = false);
		}
		
		//
		return result;
	}
	
	
	public String signature(Notation type) {
		String result;
		
		result= "";
		switch (type){
		
		case NUMBERS:
			
			result = sig(0,type)+" ";
			for (int i=1;i<size();i++){
				if (dir(i)==0) result = result+". ";
				result = result + sig(i,type)+" ";
			}
			break;
			
		case CLASSIC:
			
			for (int i=1;i<size();i++){
				result = result+letter(i);
			}
			result = result.replaceAll("XS", "B").replaceAll("XD", "Z").replaceAll("PaS", "B").replaceAll("PaD", "Z");
			break;
			
		case CLASSIC_AGED:
			
			for (int i=1;i<size();i++){
				String age = "";
				if (letter(i).equals("X") && i>0 && i<length()){
					Integer firstAge = Integer.parseInt(get(i-1).getAttributeValue("POS"));
					Integer secondAge = Integer.parseInt(get(i+1).getAttributeValue("POS"));
					int comp = firstAge.compareTo(secondAge);
					if (comp == 1){
						age = "e";
					} else if (comp == -1){
						age = "y";
					}
				}
				
				result = result+age+letter(i);
			}
			result = result.replaceAll("XS", "B").replaceAll("XD", "Z").replaceAll("PaS", "B").replaceAll("PaD", "Z");
			break;
			
		case CLASSIC_GENDERED:
			
			result = getFirst().getGender().toSymbol()+signature(Notation.CLASSIC);
			break;
			
		case CLASSIC_GENDERED_AGED:
			
			result = getFirst().getGender().toSymbol()+signature(Notation.CLASSIC_AGED);
			break;
			
		case POSITIONAL:
			
			result = sig(0,type);
			for (int i=1;i<size();i++){
				if (dir(i)==0) result = result+".";
				result = result + sig(i,type);
			}
			break;
			
		case POSITIONAL_NEUTRAL:
			
			result = signature(Notation.POSITIONAL).replaceAll("H", "X").replaceAll("F", "X");
			
			break;
		
		case VECTOR:
			
			result = getCharacteristicVector().toString();
			break;
			
		case COUPLE:
			
			result = getFirst().getName()+" ("+getFirst().getId()+")"+" = "+getLast().getName()+" ("+getLast().getId()+")";
			break;
			
		case PIVOTS:
			
			result = coupleSignature();
			break;
			
		case CLOSING_RELATION:
			
			if (closingRelation!=null) {
				result = closingRelation;
			}
			break;
			
		case GROUPS:
			
			for (int i=1;i<size();i++){
				if (get(i) instanceof Couple) result = result + get(i).getName();
				if (dir(i)==0) result = result + " / ";
			}
			break;
		}
		//
		return result;
	}

	
	/**
	 * checks whether the ith consanguineous component consists of a single vertex
	 * @return true if the ith consanguineous component consists of a single vertex
	 * @see OldRing#getPaths()
	 * @see OldRing#setVector()
	 */
/*	private boolean single (int i){
		return dir(i)>=0 && dir(i+1)==0;
	}*/
	
	/**
	 * gets the size of the ith consanguinous component
	 * @param i the index of the consanguinous component
	 * @return the size of the ith consanguinous component
	 * @see OldRing#transformPivots(int)
	 */
/*	private int size (int i){
		int n = pivots.size();
		i = (i+n+1)%(n+1);
		if (i==0) return pivots.get(i);
		if (i<n) return pivots.get(i)-pivots.get(i-1);
		return size()-pivots.get(i-1);
	}*/	

	
	public Chain standard (){
		Chain result;
		
		result = transform(standardEgoPosition());

		//
		return result;
	}
	

	
	/**
	 * returns a standard representation of the circuit 
	 * @param sym the symmetry index
	 * <p> 0 not permutable, 1 ego-alter reflection possible, 2 fully permutable
	 * @return the standard representation of the ring
	 */
/*	public Chain standard (SymmetryType sym) {
		if (sym== SymmetryType.INVARIABLE) return this;
		setVector();
		return transform(max(sym));
	}*/
	
    /**
	 * gets the representation of the ring as precribed by a list of ring models
	 * @param models the list of ring models
	 * @return the representation of the ring which corresponds to the models, or null if there is no model
	 * @see maps.RingGroupMap#countBridges(Vertex,Vertex,CountMap<int[]>)
	 */
	public Chain standard (List<Chain> models) {
		for (Chain r : models){
			if (modelEquals(r,false)) return r;
		}
	    return null;
	}	
	
	public List<Chain> getPermutations(SymmetryType sym){
		List<Chain> result;
		
		result = new ArrayList<Chain>();
		result.add(this);
		
		getSubchains();
		
		switch (sym){
		case INVARIABLE:
			break;
		case INVERTIBLE:
			result.add(transform(2*dim()-1));
			break;
		case PERMUTABLE:
			for (int i=1;i<2*dim();i++){
				result.add(transform(i));
			}
			break;
		}
		//
		return result;
	}

	private int standardEgoPosition (){
		int result = 0;
		
		if (getSymmetry() == SymmetryType.INVERTIBLE){
			int i = 2*dim()-1;
			Vector standardVector = getCharacteristicVector();
			int comp = standardVector.genderedCompareTo(vector.transform(i));
			if (comp<0){
				result = i;
			}
			
/*			int comp = compareSubchains(subchains.get(i),subchains.get(result));
			if (comp>0) {
				result = i;
			} else if (comp ==0){
				int comp2 = compareSubchains(subchains.get(getSpouseIndex(i,i+1)),subchains.get(getSpouseIndex(result,i+1)));
				if (comp2>0) {
					result = i; 
				}
			}*/
		} else if (getSymmetry() == SymmetryType.PERMUTABLE){
			int w = 2*dim();
			Vector standardVector = getCharacteristicVector();
			for (int i=1;i<w;i++){
				Vector transformedVector = vector.transform(i);
				int comp = standardVector.genderedCompareTo(transformedVector);
				if (comp<0){
					result = i;
					standardVector = transformedVector;
				}
				
/*				int comp = compareSubchains(subchains.get(i),subchains.get(result));
				if (comp>0) {
					result = i;
				} else if (comp ==0){
					int comp2 = compareSubchains(subchains.get(getSpouseIndex(i,w)),subchains.get(getSpouseIndex(result,w)));
					if (comp2>0) {
						result = i; 
					}
				}*/
			}
		}
		
		//
		return result;
	}
	
	/**
	/**
	 * 	 * get the standard ego position
		 * @param sym the symmetry index of the ring
		 * <p> 0 not permutable, 1 ego-alter reflection possible, 2 fully permutable
		 * @return the standard ego position
		 * @see OldRing#standard(boolean)
		 */
/*		public int max(SymmetryType sym){
			if (sym==SymmetryType.INVARIABLE) return 0;
			int[] v = vector;
			int n = vector.length;
			if (sym==SymmetryType.INVERTIBLE){
				KeyComparator c = new KeyComparator(6);
//				if (hasPolygamPoles()) return 0;
				int[] w = transformVector(n-1);
				if (c.compare(v,w,false)==1) return n-1;
				else return 0;
			}
			KeyComparator c = new KeyComparator(7);
			int j = 0;
			for (int i=0;i<n;i++){
				int[] w = transformVector(i);
				if (c.compare(v,w)==1) {
					v = w;
					j = i;
				}
			}
			return j;
		}*/

	
	/**
	 * returns the basic notation of the ring
	 * @return the ring in basic notation
	 * @see chains.OldRing#standardize(String)
	 * @see chains.OldRing#signature(int)
	 */
	private String standardize(){
		String s = "";
		for (int i=0;i<size();i++){
			Individual v = get(i);
			s=s+v.getGender();
			if ((dir(i)!=0 || i==size()-1) && dir(i)!=dir(i+1)) s = s+".";
			if (dir(i)==1 && dir(i+1)==0) s = s+".";
		}
		return s;
	}
	
	/**
	 * adds diacritical characters (parentheses and points) to the string representation of a vertex according to its position
	 * <p> used for positional notation
	 * @param s the representation of the vertex
	 * @param t the separator ("" or " ")
	 * @param i the position index of the vertex
	 * @return the vertex representation with diacritical characters
	 * @see OldRing#lin(int, int)
	 */
	private String surround (String s, String t, int i){
		if ((dir(i)==1 || dir(i+1)==-1) && (dir(i)!=dir(i+1))) s = "("+s+")";
		if (dir(i)==0 && i!=0) s = "."+t+s;
		return s+t;
    }
	
	/**
	 * gets the symetry index of the ring (the number of automorphs as a percentage of isomorphs)
	 * <p> value of the SYM property
	 * @return the symetry index
	 */
	public double sym () {
		
		int nrPermutations = ChainMaker.getPermutations(this).size();
		
		return 100.-MathUtils.percent(nrPermutations-1,2*dim()-1);
	}
	
	
	/**
	 * gets the number of gender switches in the jth consanguineal component
	 * <p> value of the SWITCHES property
	 * @param j the index of the consanguineal component
	 * @return the number of gender switches in the jth consanguineal component
	 * @see OldRing#lineType(ArrayList, int)
	 */
/*	private int switches(int j) {
		int k = 0;
		int a = pivot(2*j)+lin(j,0)+1;
		int b = pivot(2*(j+1))-lin(j,1);
		for (int i=a;i<b;i++) {
			if (gender(i).isUnknow()) {
				if (gender(i-1)!=gender(i+1)) k++;
			}
			else if (gender(i-1).isUnknow()) continue;
			else if (gender(i)!=gender(i-1)) k++;
		}
		return k;
	}*/
	
	//harmonize with apical, quasi-apical
    /**
     * checks whether a position is neither apical nor followed by an firstApex
     * @param i the position to be checked
     * @return if the position is neither apical nor followed by an firstApex
     */
/*	private boolean toJoin (int i){
		return dir(i)>=0 && dir(i+1)!=0;
	}*/
   
/*	private String toGenderString (){
		String s = "";
		for (Individual v : this){
			s = s+v.getGender().toChar();
		}
		return s;
	}*/


	public Chain transform(int i) {
		
		if (i==0) return this;
		Chain result = new Chain();
		result.setSymmetry(symmetry);
		result.subchains = new ArrayList<Chain>();

		if (i==2*dim()-1) {
			result.directions.add(-dir(size()-1));
			for (int j=size()-1;j>0;j--){
				result.add(get(j),-dir(j));
			}
			result.add(get(0));
			for (int j=i;j>-1;j--){
				result.subchains.add(subchains.get(j));
			}
			return result;
		}

		int m = getMarrPos(i);
		if (i%2==0) {
			for (int j=m;j<size();j++){
				result.add(get(j),dir(j));
			}
			result.add(get(0),0);
			for (int j=1;j<m;j++){
				result.add(get(j),dir(j));
			}
			for (int j=i;j<i+2*dim();j++){
				result.subchains.add(subchains.get(j%(2*dim())));
			}
			
		} else {
			for (int j=m;j>-1;j--){
				result.add(get(j),-dir(j+1));
			}
			result.add(get(size()-1),0);
			for (int j=size()-2;j>m;j--){
				result.add(get(j),-dir(j+1));
			}
			for (int j=i+2*dim();j>i;j--){
				result.subchains.add(subchains.get(j%(2*dim())));
			}
		}

	    return result;                     
	}
	
	//old transform method
	/**
	 * permutes the ring represetnation
	 * @param i the position permuted to the first position
	 * @return the permuted ring representation
	 */
/*	public Chain transform1 (int i){
		if (i==0 || i==2*order) return this;
		int j = pivot(i);
		Chain r = new Chain(get(j));
		r.order=order;
		r.setNumber(getId());
		r.vector=transformVector(i);
//		r.pivots=transformPivots(i);
		if (i%2==0) {
			for (int k=j+1;k<j+size();k++){
				r.add(this,k%size(),false);
			}
		} else {
			for (int k=j-1;k>j-size();k--){
				r.add(this,(k+size())%size(),true);
			}
		}
		r.setPivots();
		return r;
	}*/
	
	/**
	 * permutes the chain of pivotal indices
	 * @param i the position permuted to the first position
	 * @return the permuted pivotal index chain
	 * @see OldRing#transform(int)
	 */
/*	private List<Integer> transformPivots (int i){
		if (pivots==null) return null;
		if (i==0) return pivots;
		ArrayList<Integer> p = new ArrayList<Integer>();
		int k = i/2;
		int n = pivots.size();
		int z = 0;
		for (int j=0;j<n;j++){
			if (i%2==0) z = z + size(k+j);
			else z = z + size(k-j);
			p.add(z);
		}
		return p;
	}*/
	
	/**
	 * permutes the vector of the ring
	 * @param i the position permuted to the first position
	 * @return the permuted vector

	 */
/*	private int[] transformVector (int i){
		int n = 2*dim();
		int[] v = new int[n];
		int j=0;
		if (i%2==0) {
			for (int k=i;k<i+n;k++){
				v[j]=vector[k%n];
				j++;
			}
		} else {
			for (int k=i;k>i-n;k--){
				v[j]=vector[(k+n)%n];
				j++;
			}
		}
		return v;
	}*/
	
	/**
	 * removes the first vertex of the ring
	 * @param reset true if the direction index of the new first vertex has to be reset (to 0)
	 */
	public void truncate(boolean reset) {
		removeFirst();
		if (pivots!=null){
			for (int i=0;i<pivots.size();i++){
				int p = pivots.get(i)-1;
				pivots.set(i,p);
			}
		}
		if (reset && size()>0) {
//			directions.set(0,0);
		}
	}

	//Rename
	//Harmonize with apical and quasi-apical
	/**
	 * checks the pivotal or apical character of a position
	 * @return true if the position is pivotal or apical
	 * @see OldRing#getPaths()
	 * @see OldRing#setVector()
	 */
	private boolean turn (int i){
//		return dir(i)==0 || (dir(i)!=dir(i-1) && dir(i)==-1);
		return i==length() || dir(i+1)==0 || (i>0 && dir(i+1)!=dir(i) && dir(i+1)==-1) || (i==0 && dir(i+1)<0);

	}
	
	/**
	 * cuts the end vertices of a homosexual ring until they are heterosexual or quasi-apical
	 * <p> corresponds to the canonical closure operation on matrimonial circuits 
	 * @return the ring after canonical closure
	 */
	public Chain close(){
		Chain result;
		if (!sameSex() || size()<3) {
			result = this;
		} else {
			if (dir(length())!=-1) {
				removeLast();
				result = recombine();
			} else if (dir(1)!=1) {
				truncate(false);
				result = recombine();
			}
			removeLast();
			truncate(false);
			result = close();
		}
		//
		return result;
	}

	public SymmetryType getSymmetry() {
		return symmetry;
	}

	public void setSymmetry(SymmetryType symmetry) {
		this.symmetry = symmetry;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	@Override
	public String hashKey() {
		return signature(Notation.NUMBERS);
	}	
	
	public void setDir(int position, int dir){
		
		directions.set(position, dir);
	}

	
	// Old constructors and add methods (harmonize!)
	
	// replaced by ChainMaker#concatenateInv
	/**
	 * constructs a consanguineous ring from two linear paths 
	 * @param p1 the ascending path
	 * @param p2 the descending path
	 * @see chains.Path#getBridges(ArrayList, Individual, int, int, int)
	 */
/*	public Chain (Chain p1, Chain p2) {
		add(p1,1);
		add(p2,-1);
		if (p1.size()>1 && p2.size()>1) addApex(p1.length());
	}*/
	


	//used for old variant of getKin in ChainFinder
	/**
	 * constructs a ring by adding a path to a given ring
	 * @param r the original ring
	 * @param p the path to be added
	 * @see Vertex#getKin(TreeMap<Vertex,String>,int,Path,Ring)
	 */
/*	public Chain (Chain r, Chain p){
		addAll(r);
		directions.addAll(r.directions);
		for (int i=0;i<p.size();i++){
			set(i,p.get(i));
		}
	}*/




	/**
	 * adds another ring to the ring and returns true
	 * @param r the ring to be added
	 * @return true
	 */
/*	private boolean add (Chain r) {
		for (int i=0;i<r.size();i++){
			add(r.get(i),r.dir(i)); 
		}
		return true;
	}*/
	
	
	// for old transform method
	/**
	 * adds a vertex from another ring to the ring
	 * @see OldRing#transform(int)
	 * @param r the ring from which a vertex is to be added
	 * @param i the position of the vertex in the donor ring
	 * @param inverse the inversion of direction
	 * @see OldRing#transform(int)
	 */
/*	private void add(Chain r, int i, boolean inverse){
		add(r.get(i));
		if (inverse) directions.add(-r.dir(i+1,false));
		else directions.add(r.dir(i));
	}*/

	/**
	  *	returns the direction at the ith position of the ring, the last position may or may not be defined as a marriage position
	  * @param i the position in the ring
	  * @param open true if the ring is not considered as closed by a marriage
	  * @return the direction at the ith position of the ring (1 ascending, -1 descending, 0 marriage)
	  * @see OldRing#add(OldRing, int, boolean)
	  */
/*	private int dir(int i, boolean open){
		try {
			return directions.get(i);
		} catch (IndexOutOfBoundsException iob){
			if (open) return -2;
			return 0;
		}
	}*/

}
