package org.tip.puck.census.workers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.census.chains.Chain;
import org.tip.puck.census.chains.ChainFinder;
import org.tip.puck.census.chains.Notation;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Partition;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.util.Value;

public class TermCensus {
	
	Partition<Chain> chains;
	Partition<String> consanguinealTerms;
	Partition<String> affinalTerms;
	
	List<String> terms;
	
	Individual maleEgo;
	Individual femaleEgo;
	
	Map<String,boolean[]> termProperties;
	String cousinTerminology;

	
	public TermCensus (Segmentation segmentation){
		
		chains = new Partition<Chain>();
		chains.setLabel("ALTERNAME");
		
		for (Individual ego : segmentation.getCurrentIndividuals()){
			if (ego.getName().equals("[Ego]")){
				if (ego.isMale()){
					maleEgo = ego;
				} else if (ego.isFemale()){
					femaleEgo = ego;
				}
			}
		}
		
		for (Individual alter : segmentation.getCurrentIndividuals()){
			if (StringUtils.isNotEmpty(alter.getName()) && !alter.getName().equals("[Ego]")){
				Set<Individual> visited = new HashSet<Individual>();
				visited.add(maleEgo);
				ChainFinder.expandShortest(chains, new Chain(maleEgo),alter,1000,100,visited);
				visited = new HashSet<Individual>();
				visited.add(femaleEgo);
				ChainFinder.expandShortest(chains, new Chain(femaleEgo),alter,1000,100,visited);
			}
		}
	}
	
	public void analyze (){
		
		consanguinealTerms = new Partition<String>();
		affinalTerms = new Partition<String>();
		terms = new ArrayList<String>();
		
		termProperties = new TreeMap<String,boolean[]>();
		cousinTerminology = "";
		
		for (Cluster<Chain> cluster : chains.getClusters()){
			
			terms.add(cluster.getValue().toString());
			
			boolean[] properties = new boolean[4];
			
			boolean parallelCousinTerm = false;
			boolean crossCousinTerm = false;
			boolean siblingTerm = false;
						
			Set<Gender> gender = new HashSet<Gender>();
			Set<Value> generation = new HashSet<Value>();
			Set<Integer> length = new HashSet<Integer>();
			Set<Value> bifurcation = new HashSet<Value>();
			
			for (Chain chain : cluster.getItems()) {
				
				if (chain.dim()==1){
					consanguinealTerms.put(cluster.getValue().toString(),ChainValuator.get(chain, "SKEW"));
				} else {
					affinalTerms.put(cluster.getValue().toString(),ChainValuator.get(chain, "SKEW"));
				}

				gender.add(chain.getLast().getGender());
				generation.add(ChainValuator.get(chain, "SKEWSUM"));
				length.add(chain.size());
				bifurcation.add(ChainValuator.get(chain,"LINE"));
				
				if (ChainValuator.get(chain,"SKEW").equals(new Value(0))) {
					if (ChainValuator.get(chain, "DEGREE_ROM").equals(new Value(2))){
						siblingTerm = true;
					} else if (ChainValuator.get(chain,"LINE").equals(new Value(FiliationType.COGNATIC))){
						crossCousinTerm = true;
					} else {
						parallelCousinTerm = true;
					}
				}
			}			
			
			if (gender.size()==1){
				properties[0] = true;
			}
			
			if (generation.size()==1){
				properties[1] = true;
			}
			
			if (bifurcation.size()==1 || bifurcation.contains(new Value(FiliationType.COGNATIC))){
				properties[2] = true;
			}
			
			if (length.size()>1) {
				properties[3] = true;
			}
			
			termProperties.put(cluster.getValue().toString(),properties);
			
			if (parallelCousinTerm && crossCousinTerm && siblingTerm){
				cousinTerminology = "GENERATIONAL";
			} else if (parallelCousinTerm && crossCousinTerm) {
				if (!cousinTerminology.equals("GENERATIONAL")){
					cousinTerminology = "LINEAL";
				}
			} else if (parallelCousinTerm && siblingTerm) {
				if (!cousinTerminology.equals("GENERATIONAL")){
					cousinTerminology = "BIFURCATE-MERGING";
				}
			} else {
				if (cousinTerminology.equals("")){
					cousinTerminology = "DESCRIPTIVE";
				}
			}
		}
		
		Collections.sort(terms);
	}
	
	public Partition<String> findReciprocalTerms (){
		Partition<String> result;
		
		result = new Partition<String>();
		
		for (Chain chain : chains.getItems()){
			Chain inverseChain = chain.reflect();
			Individual ego = null;
			if (inverseChain.getFirst().isMale()){
				ego = maleEgo;
			} else if(inverseChain.getFirst().isFemale()){
				ego = femaleEgo;
			}
			Partition<Chain> reciprocalChains = ChainFinder.getKin(ego, inverseChain.signature(Notation.POSITIONAL));
			for (Chain reciprocalChain : reciprocalChains.getItems()){
				result.put(reciprocalChain.getLast().getName(),new Value(chain.getLast().getName()));
			}
		}
		
		//
		return result;
	}
	
	public Partition<String> findTermProducts (){
		Partition<String> result;
		
		result = new Partition<String>();
		
		for (Chain chain1 : chains.getItems()){
			for (Chain chain2 : chains.getItems()){
				String[] terms = new String[]{chain1.getLast().getName(),chain2.getLast().getName()};
				Partition<Chain> productChains = ChainFinder.getKin(chain1.getLast(), chain2.signature(Notation.POSITIONAL));
				for (Chain productChain : productChains.getItems()){
					result.put(productChain.getLast().getName(),new Value(terms));
				}
			}
		}
		
		//
		return result;
		
	}

	
	
	public List<String> getTerms() {
		return terms;
	}

	public Map<String, boolean[]> getTermProperties() {
		return termProperties;
	}

	public Partition<String> getConsanguinealTerms() {
		return consanguinealTerms;
	}

	public Partition<String> getAffinalTerms() {
		return affinalTerms;
	}

	public String getCousinTerminology() {
		return cousinTerminology;
	}

	public Partition<Chain> getChains() {
		return chains;
	}	
	
	
	

}
