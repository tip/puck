package org.tip.puck.census.workers;

import java.util.List;

import org.tip.puck.geo.Geography;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Gender;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class CensusCriteria {

	private String pattern;
	private String filter;
	private String classificatoryLinking;
	private String closingRelation;
	private String closingRelationEgoRole;
	private String closingRelationAlterRole;
	private String ascendingRelation;
	private String ascendingRelationEgoRole;
	private String ascendingRelationAlterRole;
	private String chainClassification;
	private boolean crossSexChainsOnly;
	private boolean markIndividuals;
	private boolean marriedOnly;
	private boolean openChainFrequencies;
	private CircuitType circuitType;
	private FiliationType filiationType;
	private RestrictionType restrictionType;
	private SiblingMode siblingMode;
	private SymmetryType symmetryType;
	private CensusDetails censusDetails;
	private Gender firstGender;
	private String individualPartitionLabel;
	private boolean circuitIntersectionNetwork;
	private boolean circuitInducedNetwork;
	private boolean circuitInducedFrameNetwork;
	private boolean circuitNetworks;
	private boolean circuitsAsRelations;
	private boolean withOutOfCircuitCouples;
	private boolean withAllPerspectives;
	private Integer relationTime;
	private String relationAttributeLabel;
	private StringList partitionLabels;
	private String dateLabel;

	/**
	 * 
	 */
	public CensusCriteria() {
		this.pattern = "";
		this.filter = "";
		this.classificatoryLinking = "";
		this.closingRelation = "SPOUSE";
		this.closingRelationEgoRole = "";
		this.closingRelationAlterRole = "";
		this.ascendingRelation = "PARENT";
		this.ascendingRelationEgoRole = "";
		this.ascendingRelationAlterRole = "";
		this.chainClassification = "SIMPLE";
		this.crossSexChainsOnly = false;
		this.markIndividuals = false;
		this.marriedOnly = false;
		this.openChainFrequencies = false;
		this.circuitType = CircuitType.CIRCUIT;
		this.filiationType = FiliationType.COGNATIC;
		this.restrictionType = RestrictionType.NONE;
		this.siblingMode = SiblingMode.FULL;
		this.symmetryType = SymmetryType.PERMUTABLE;
		this.censusDetails = new CensusDetails(10, 10);
		this.firstGender = Gender.MALE;
		this.individualPartitionLabel = "";
		this.circuitIntersectionNetwork = false;
		this.circuitInducedNetwork = false;
		this.circuitInducedFrameNetwork = false;
		this.circuitNetworks = false;
		this.withOutOfCircuitCouples = false;
		this.partitionLabels = new StringList();
	}

	public CensusDetails getCensusDetails() {
		return this.censusDetails;
	}

	public String getChainClassification() {
		return this.chainClassification;
	}

	public CircuitType getCircuitType() {
		return this.circuitType;
	}

	public String getClassificatoryLinking() {
		return this.classificatoryLinking;
	}

	public String getClosingRelation() {
		return this.closingRelation;
	}

	public String getClosingRelationAlterRole() {
		return this.closingRelationAlterRole;
	}

	public String getClosingRelationEgoRole() {
		return this.closingRelationEgoRole;
	}

	public String getAscendingRelation() {
		return this.ascendingRelation;
	}

	public String getAscendingRelationAlterRole() {
		return this.ascendingRelationAlterRole;
	}

	public String getAscendingRelationEgoRole() {
		return this.ascendingRelationEgoRole;
	}

	public FiliationType getFiliationType() {
		return this.filiationType;
	}

	public String getFilter() {
		return this.filter;
	}

	public Gender getFirstGender() {
		return this.firstGender;
	}

	public String getIndividualPartitionLabel() {
		return this.individualPartitionLabel;
	}

	public StringList getPartitionLabels() {
		return this.partitionLabels;
	}

	public String getPattern() {
		return this.pattern;
	}

	public String getRelationAttributeLabel() {
		return this.relationAttributeLabel;
	}

	public Integer getRelationTime() {
		return this.relationTime;
	}

	public RestrictionType getRestrictionType() {
		return this.restrictionType;
	}

	public SiblingMode getSiblingMode() {
		return this.siblingMode;
	}

	public SymmetryType getSymmetryType() {
		return this.symmetryType;
	}

	public boolean isCircuitInducedFrameNetwork() {
		return this.circuitInducedFrameNetwork;
	}

	public boolean isCircuitInducedNetwork() {
		return this.circuitInducedNetwork;
	}

	public boolean isCircuitIntersectionNetwork() {
		return this.circuitIntersectionNetwork;
	}

	public boolean isCircuitNetworks() {
		return this.circuitNetworks;
	}

	public boolean isCircuitsAsRelations() {
		return this.circuitsAsRelations;
	}

	public boolean isCouplesOnly() {
		return this.marriedOnly;
	}

	public boolean isCrossSexChainsOnly() {
		return this.crossSexChainsOnly;
	}

	public boolean isMarkIndividuals() {
		return this.markIndividuals;
	}

	public boolean isOpenChainFrequencies() {
		return this.openChainFrequencies;
	}

	public boolean isWithAllPerspectives() {
		return this.withAllPerspectives;
	}

	public boolean isWithOutOfCircuitCouples() {
		return this.withOutOfCircuitCouples;
	}

	public void setChainClassification(final String value) {
		this.chainClassification = value;
	}

	public void setCircuitInducedNetwork(final boolean circuitInduceNetwork) {
		this.circuitInducedNetwork = circuitInduceNetwork;
	}

	public void setCircuitInduceFrameNetwork(final boolean circuitInduceFrameNetwork) {
		this.circuitInducedFrameNetwork = circuitInduceFrameNetwork;
	}

	public void setCircuitIntersectionNetwork(final boolean circuitInterserctionNetwork) {
		this.circuitIntersectionNetwork = circuitInterserctionNetwork;
	}

	public void setCircuitNetworks(final boolean circuitNetworks) {
		this.circuitNetworks = circuitNetworks;
	}

	public void setCircuitsAsRelations(final boolean circuitsAsRelations) {
		this.circuitsAsRelations = circuitsAsRelations;
	}

	public void setCircuitType(final CircuitType value) {
		if (value == null) {
			this.circuitType = CircuitType.CIRCUIT;
		} else {
			this.circuitType = value;
		}
	}

	public void setClassificatoryLinking(final String value) {
		this.classificatoryLinking = value;
	}

	public void setClosingRelation(final String value) {
		this.closingRelation = value;
	}

	public void setClosingRelationAlterRole(final String value) {
		if (value == null) {
			this.closingRelationAlterRole = "";
		} else {
			this.closingRelationAlterRole = value;
		}
	}

	public void setClosingRelationEgoRole(final String value) {
		if (value == null) {
			this.closingRelationAlterRole = "";
		} else {
			this.closingRelationEgoRole = value;
		}
	}

	public void setAscendingRelation(final String value) {
		this.ascendingRelation = value;
	}

	public void setAscendingRelationAlterRole(final String value) {
		if (value == null) {
			this.ascendingRelationAlterRole = "";
		} else {
			this.ascendingRelationAlterRole = value;
		}
	}

	public void setAscendingRelationEgoRole(final String value) {
		if (value == null) {
			this.ascendingRelationAlterRole = "";
		} else {
			this.ascendingRelationEgoRole = value;
		}
	}

	public void setCrossSexChainsOnly(final boolean value) {
		this.crossSexChainsOnly = value;
	}

	public void setFiliationType(final FiliationType value) {
		if (value == null) {
			this.filiationType = FiliationType.COGNATIC;
		} else {
			this.filiationType = value;
		}
	}

	public void setFilter(final String filter) {
		this.filter = filter;
	}

	public void setFirstGender(final Gender firstGender) {
		this.firstGender = firstGender;
	}

	public void setIndividualPartitionLabel(final String value) {
		this.individualPartitionLabel = value;
	}

	public void setMarkIndividuals(final boolean markIndividuals) {
		this.markIndividuals = markIndividuals;
	}

	public void setMarriedOnly(final boolean marriedOnly) {
		this.marriedOnly = marriedOnly;
	}

	public void setOpenChainFrequencies(final boolean openChainFrequencies) {
		this.openChainFrequencies = openChainFrequencies;
	}

	public void setPartitionLabels(final List<String> partitionLabels) {
		this.partitionLabels = new StringList(partitionLabels);
	}

	public void setPattern(final String pattern) {
		this.pattern = pattern;
	}

	public void setRelationAttributeLabel(final String relationAttributeValue) {
		this.relationAttributeLabel = relationAttributeValue;
	}

	public void setRelationTime(final Integer relationTime) {
		this.relationTime = relationTime;
	}

	public void setRestrictionType(final RestrictionType value) {
		if (value == null) {
			this.restrictionType = RestrictionType.NONE;
		} else {
			this.restrictionType = value;
		}
	}

	public void setSiblingMode(final SiblingMode value) {
		if (value == null) {
			this.siblingMode = SiblingMode.FULL;
		} else {
			this.siblingMode = value;
		}
	}

	public void setSymmetryType(final SymmetryType value) {
		if (value == null) {
			this.symmetryType = SymmetryType.PERMUTABLE;
		} else {
			this.symmetryType = value;
		}
	}

	public void setWithAllPerspectives(final boolean withAllPerspectives) {
		this.withAllPerspectives = withAllPerspectives;
	}

	public void setWithOutOfCircuitCouples(final boolean withOutOfCircuitCouples) {
		this.withOutOfCircuitCouples = withOutOfCircuitCouples;
	}

	public String getDateLabel() {
		return dateLabel;
	}

	public void setDateLabel(String dateLabel) {
		this.dateLabel = dateLabel;
	}
	
	public static CensusCriteria getDefaultCriteria(){
		CensusCriteria result;
		
		result = new CensusCriteria();
		result.setPattern("3 1");
		
		
		//
		return result;

	}

	
	
	

}
