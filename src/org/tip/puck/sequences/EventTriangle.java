package org.tip.puck.sequences;

import java.util.Arrays;
import java.util.TreeMap;

import org.tip.puck.net.Individual;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.workers.IndividualValuator;

/**
 * 
 * @author Klaus Hamberger
 *
 */
public class EventTriangle {
	
	Individual[] individuals;
	String[][] roleNamePairs;
	Relation[] events;
	int n;
	
	public EventTriangle(int n) {
		
		individuals = new Individual[n];
		roleNamePairs = new String[n][2];
		events = new Relation[n];
		this.n = n;
	}
	
	public EventTriangle (Individual[] individuals, String[][] roleNamePairs, Relation[] events){
		
		this.individuals = individuals;
		this.roleNamePairs = roleNamePairs;
		this.events = events;
		this.n = events.length;
	}
	
	public String[][] inverseRoleNamePairs(){
		String[][] result;
		
		result = new String[n][2];
		
		for (int i=0;i<n;i++){
			result[i][0] = roleNamePairs[i][1];
			result[i][1] = roleNamePairs[i][0];
		}
		//
		return result;
	}

	public EventTriangle sortByYear(){
		
		EventTriangle result;
		
		result = new EventTriangle(n);
		TreeMap<String,Integer> permutation = new TreeMap<String,Integer>();
		
		int i = 0;
		for (Relation event : events){
			String year = IndividualValuator.extractYear(event.getAttributeValue("DATE"));
			if (year == null){
				year = "";
			}
			permutation.put (year+" "+event.getId()+" "+i,i);
			i++;
		}
		
		boolean invert;
		
		try {
			invert = (n+permutation.get(permutation.higherKey(permutation.firstKey()))-permutation.get(permutation.firstKey()))%n!=1;
		} catch (NullPointerException npe) {
			invert = false;
		}
				
		i = 0;
		for (int j : permutation.values()){
			if (invert){
				result.roleNamePairs[i] = inverseRoleNamePairs()[j];
				result.individuals[(i+1)%n] = individuals[j];
			} else {
				result.roleNamePairs[i] = roleNamePairs[j];
				result.individuals[i] = individuals[j];
			}
			result.events[i] = events[j];
			i++;
		}

		//
		return result;
	}
	
	public boolean equals (Object obj){
		boolean result;
		
		result = obj != null && Arrays.deepEquals(individuals,((EventTriangle)obj).individuals) && Arrays.deepEquals(roleNamePairs,((EventTriangle)obj).roleNamePairs) && Arrays.deepEquals(events,((EventTriangle)obj).events);

		//
		return result;
	}
	
	public String getRolePattern (){
		String result;
		
		result = "";
		for (int i=0;i<n;i++){
			result+=roleNamePairs[i][0]+roleNamePairs[i][1];
			if (i<n-1){
				result+=" - ";
			}
		}
		return result;
	}
	
	public String getEventPattern (){
		String result;
		
		result = "";
		for (int i=0;i<n;i++){
			result+=individuals[i]+" - "+IndividualValuator.extractYear(events[i].getAttributeValue("DATE"))+" "+events[i].getAttributeValue("END_PLACE");
			if (i<n-1){
				result+=" - ";
			}
		}
		//
		return result;
	}

}
