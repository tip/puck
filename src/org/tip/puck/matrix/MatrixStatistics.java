package org.tip.puck.matrix;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.tip.puck.PuckException;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.random.RandomCriteria;
import org.tip.puck.graphs.random.RandomGraphMaker;
import fr.devinsy.util.StringList;
import org.tip.puck.util.Value;

import umontreal.iro.lecuyer.probdist.StudentDist;
import umontreal.iro.lecuyer.stat.Tally;

public class MatrixStatistics {
	
	public enum DistributionObject {
		WEIGHTS,
		STRENGTHS;
	}
	
	public enum Indicator {
		CONCENTRATION,
		SYMMETRY,
		ENDOGAMIC_CONCENTRATION,
		STRENGTH_CONCENTRATION,
		STRENGTH_SYMMETRY,
		ENDOGAMIC_STRENGTH_CONCENTRATION,
		LOOPS,
		DUAL_CIRCUITS,
		PARALLEL_CIRCUITS,
		CROSS_CIRCUITS,
		BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS,
		TRIANGLES,
		TRANSITIVE_TRIANGLES,
		CYCLIC_TRIANGLES,
		MAX_WEIGHT,
		MAX_STRENGTH;
	}
	
	public enum Mode {
		SIMPLE,
		NORMALIZED,
		EXPECTED,
		EXPECTED_NORMALIZED,
		DIVERGENCE,
		DIVERGENCE_NORMALIZED,
		OVERESTIMATION,
		OVERESTIMATION_EXPECTED,
		OVERESTIMATION_DIVERGENCE;
	}
	
	public static String getValueString (Graph graph) throws PuckException {
		String result;
		
		result = graph.getLabel();
		MatrixStatistics worker = new MatrixStatistics(graph);
		for (Indicator indicator : Indicator.values()){
			result += "\t";
			Value value = new Value(worker.getNumber(indicator));
			if (value!=null){
				result += value.toString();
			}
		}
		
		//
		return result;
	}

	public static Map<Double,Double> getDistribution (DistributionObject dist, Matrix source){
		Map<Double,Double> result;
		
		result = null;
		
		switch (dist){
		case WEIGHTS:
			result = getDistributionOfWeights (source);
			break;
		case STRENGTHS:
			result = getDistributionOfStrengths (source);
			break;
		}
		//
		return result;
	}
	
	
	public static Map<Double,Double> getDistribution (DistributionObject dist, Graph source){
		Map<Double,Double> result;
		
		result = null;
		
		switch (dist){
		case WEIGHTS:
			result = getDistributionOfWeights (source);
			break;
		case STRENGTHS:
			result = getDistributionOfStrengths (source);
			break;
		}
		//
		return result;
	}
	
	public static Map<Double,Double> getDistributionOfStrengths(Graph source){
		Map<Double,Double> result;
		
//		result = new HashMap<Double,Double>();
		
		Matrix matrix = source.getSquareMatrix().getMatrix();
		
		result = getDistributionOfStrengths(matrix);
		
/*		double inc = new Double(1./matrix.getRowDim());
		for (int i=0; i<matrix.getRowDim();i++){
			double value = matrix.getRowSum(i)+matrix.getColSum(i);
			Double frequency = result.get(value);
			if (frequency==null){
				frequency = inc;
			} else {
				frequency += inc;
			}
			result.put(value, frequency);
		}*/
		//
		return result;
	}
	
	public static Map<Double,Double> getDistributionOfStrengths(Matrix matrix){
		Map<Double,Double> result;
		
		result = new HashMap<Double,Double>();
		
		double inc = new Double(1./matrix.getRowDim());
		for (int i=0; i<matrix.getRowDim();i++){
			double value = matrix.getRowSum(i)+matrix.getColSum(i);
			Double frequency = result.get(value);
			if (frequency==null){
				frequency = inc;
			} else {
				frequency += inc;
			}
			result.put(value, frequency);
		}
		//
		return result;
	}
	
	
	public static Map<Double,Double> getDistributionOfWeights(Graph source){
		Map<Double,Double> result;
		
//		result = new HashMap<Double,Double>();
		
		Matrix matrix = source.getSquareMatrix().getMatrix();
		
		result = getDistributionOfWeights(matrix);
		
/*		double inc = new Double(1./(matrix.getRowDim()*matrix.getColDim()));
		for (int i=0; i<matrix.getRowDim();i++){
			for (int j=0;j<matrix.getColDim();j++){
				double value = matrix.get(i,j);
				Double frequency = result.get(value);
				if (frequency==null){
					frequency = inc;
				} else {
					frequency += inc;
				}
				result.put(value, frequency);
			}
		}*/
		//
		return result;
	}
	
	public static Map<Double,Double> getDistributionOfWeights(Matrix matrix){
		Map<Double,Double> result;
		
		result = new HashMap<Double,Double>();
		
		double inc = new Double(1./(matrix.getRowDim()*matrix.getColDim()));
		for (int i=0; i<matrix.getRowDim();i++){
			for (int j=0;j<matrix.getColDim();j++){
				double value = matrix.get(i,j);
				Double frequency = result.get(value);
				if (frequency==null){
					frequency = inc;
				} else {
					frequency += inc;
				}
				result.put(value, frequency);
			}
		}
		//
		return result;
	}
	
	
	
	public static <E> Map<Double,Double> getRandomDistribution(DistributionObject dist, Graph source, RandomCriteria criteria, int interval, int runs){
		Map<Double,Double> result;
		
		result = new HashMap<Double,Double>();
		
		// Aggregate Values
		for (int run = 0; run < runs; run++) {
			
			RandomGraphMaker<E> randomGraphMaker = new RandomGraphMaker<E>(criteria);
			Graph<E> randomGraph ;
			
			if (source !=null) {
				randomGraph = randomGraphMaker.createRandomGraphByObserverSimulation(source);
			} else {
				randomGraph = randomGraphMaker.createRandomGraphByAgentSimulation();
			}

			Map<Double, Double> distribution = MatrixStatistics.getDistribution(dist,randomGraph);
			for (Double key : distribution.keySet()) {
				Double value = result.get(key);
				if (value == null) {
					value = 0.;
				} 
				value += distribution.get(key);
				result.put(key, value);
			}
		}
			
		// Normalize Values
		
		for (Double key : result.keySet()) {
			Double value = result.get(key);
			if (value != null) {
				value = value / runs;
			}
		}

		//
		return result;
	}
	
	
	public static <E> Map<Double,Double[]> getRandomDistributions(DistributionObject dist, Graph source, RandomCriteria criteria, int runs, int nrSeries, int variableIndex, int variableIntervalFactor){
		Map<Double,Double[]> result;
		
		result = new HashMap<Double,Double[]>();
		int start = 0;
		
		if (source != null){
			start = 1;
			Map<Double, Double> distribution = MatrixStatistics.getDistribution(dist,source);
			for (Double key : distribution.keySet()) {
				Double[] value = new Double[nrSeries];
				value[0] = distribution.get(key);
				result.put(key,value);
			}
		}
		
		for (int series = start; series < nrSeries; series++){
			
			// Aggregate Values
			for (int run = 0; run < runs; run++) {
				
				RandomGraphMaker<E> randomGraphMaker = new RandomGraphMaker<E>(criteria);
				Graph<E> randomGraph;
				
				if (source!=null){
					randomGraph = randomGraphMaker.createRandomGraphByObserverSimulation(source);
				} else {
					randomGraph = randomGraphMaker.createRandomGraphByAgentSimulation();
				}

				Map<Double, Double> distribution = MatrixStatistics.getDistribution(dist,randomGraph);
				for (Double key : distribution.keySet()) {
					Double[] value = result.get(key);
					if (value == null) {
						value = new Double[nrSeries];
						value[series] = 0.;
						result.put(key,value);
					} else if (value[series] == null) {
						value[series] = 0.;
					}
					value[series] += distribution.get(key);
				}
			}
				
			// Normalize Values
			for (Double key : result.keySet()) {
				Double[] value = result.get(key);
				if (value != null && value[series]!=null) {
					value[series] = value[series] / runs;
				}
			}
			
			criteria.getInertia()[variableIndex] = criteria.getInertia()[variableIndex]
					* variableIntervalFactor;
		}

		//
		return result;
	}
	
	private Tally tally; 
	
	private double sum = 0.;
	private double arcCount= 0.;
	private double nodeCount = 0.;
	private double maxNumberOfCircuits = 0.;

	private int runs;
	
	private double endogamyIndex= 0.;
	private double concentrationIndex = 0.;
	private double minimumConcentration;
	private double symmetryIndex = 0.;
	private double endogamicConcentrationIndex = 0.;
	
	private double strengthConcentrationIndex = 0.;
	private double endogamicStrengthConcentrationIndex = 0.;
	private double strengthSymmetryIndex = 0.;
	private double strengthProductSum = 0.;
	private double endogamousClosureRate = 0;

	private double numberOfLoops = 0.;
	private double numberOfCircuits = 0.;
	private double numberOfParallelCircuits = 0.;
	private double numberOfCrossCircuits = 0.;
	private double balanceOfParallelVsCrossCircuits = 0.;
	private double numberOfTriangles = 0.;
	private double numberOfCyclicTriangles = 0.;
	private double numberOfTransitiveTriangles = 0.;
	
	private double maxWeight = 0.;
	private double maxStrength = 0.;

	private double normalizedNumberOfCircuits = 0.;
	private double normalizedNumberOfParallelCircuits = 0.;
	private double normalizedNumberOfCrossCircuits = 0.;
	private double normalizedBalanceOfParallelVsCrossCircuits = 0.;
	private double normalizedNumberOfTriangles = 0.;
	private double normalizedNumberOfCyclicTriangles = 0.;
	private double normalizedNumberOfTransitiveTriangles = 0.;
	
	private double expectedConcentrationIndex = 0.;
	private double expectedEndogamicConcentrationIndex = 0.;
	private double expectedSymmetryIndex = 0.;
	private double expectedEndogamyIndex = 0.;
	
	private double expectedNumberOfLoops = 0.;
	private double expectedNumberOfCircuits = 0.;
	private double expectedNumberOfParallelCircuits = 0.;
	private double expectedNumberOfCrossCircuits = 0.;
	private double expectedBalanceOfParallelVsCrossCircuits = 0.;
	private double expectedNumberOfTriangles = 0.;
	private double expectedNumberOfCyclicTriangles = 0.;
	private double expectedNumberOfTransitiveTriangles = 0.;
	
	private double expectedNormalizedNumberOfCircuits = 0.;
	private double expectedNormalizedNumberOfParallelCircuits = 0.;
	private double expectedNormalizedNumberOfCrossCircuits = 0.;
	private double expectedNormalizedBalanceOfParallelVsCrossCircuits = 0.;
	private double expectedNormalizedNumberOfTriangles = 0.;
	private double expectedNormalizedNumberOfCyclicTriangles = 0.;
	private double expectedNormalizedNumberOfTransitiveTriangles = 0.;
	
	private double divergenceOfConcentration = 0.;
	private double divergenceOfEndogamicConcentration = 0.;
	private double divergenceOfSymmetry = 0.;

	private double divergenceOfLoops = 0.;
	private double divergenceOfCircuits = 0.;
	private double divergenceOfParallelCircuits = 0.;
	private double divergenceOfCrossCircuits = 0.;
	private double divergenceOfBalanceOfParallelVsCrossCircuits = 0.;
	private double divergenceOfTriangles = 0.;
	private double divergenceOfCyclicTriangles = 0.;
	private double divergenceOfTransitiveTriangles = 0.;

	private double divergenceOfNormalizedLoops = 0.;
	private double divergenceOfNormalizedCircuits = 0.;
	private double divergenceOfNormalizedParallelCircuits = 0.;
	private double divergenceOfNormalizedCrossCircuits = 0.;
	private double divergenceOfNormalizedBalanceOfParallelVsCrossCircuits = 0.;
	private double divergenceOfNormalizedTriangles = 0.;
	private double divergenceOfNormalizedCyclicTriangles = 0.;
	private double divergenceOfNormalizedTransitiveTriangles = 0.;
	
	private double overestimationOfLoops = 0.;
	private double overestimationOfCircuits = 0.;
	private double overestimationOfParallelCircuits = 0.;
	private double overestimationOfCrossCircuits = 0.;
	private double overestimationOfBalanceOfParallelVsCrossCircuits = 0.;
	private double overestimationOfTriangles = 0.;
	private double overestimationOfCyclicTriangles = 0.;
	private double overestimationOfTransitiveTriangles = 0.;
	private double overestimationOfConcentration = 0;
	private double overestimationOfEndogamicConcentration = 0.;
	private double overestimationOfSymmetry = 0.;
	private double overestimationOfStrengthConcentration = 0;
	private double overestimationOfEndogamicStrengthConcentration = 0.;
	private double overestimationOfStrengthSymmetry = 0.;

	private double overestimationOfExpectedLoops = 0.;
	private double overestimationOfExpectedCircuits = 0.;
	private double overestimationOfExpectedParallelCircuits = 0.;
	private double overestimationOfExpectedCrossCircuits = 0.;
	private double overestimationOfExpectedBalanceOfParallelVsCrossCircuits = 0.;
	private double overestimationOfExpectedTriangles = 0.;
	private double overestimationOfExpectedCyclicTriangles = 0.;
	private double overestimationOfExpectedTransitiveTriangles = 0.;
	private double overestimationOfExpectedConcentration = 0;
	private double overestimationOfExpectedEndogamicConcentration = 0.;
	private double overestimationOfExpectedSymmetry = 0.;
	private double overestimationOfExpectedStrengthConcentration = 0;
	private double overestimationOfExpectedEndogamicStrengthConcentration = 0.;
	private double overestimationOfExpectedStrengthSymmetry = 0.;
	
	
	private double overestimationOfDivergenceOfLoops = 0.;
	private double overestimationOfDivergenceOfCircuits = 0.;
	private double overestimationOfDivergenceOfParallelCircuits = 0.;
	private double overestimationOfDivergenceOfCrossCircuits = 0.;
	private double overestimationOfDivergenceOfBalanceOfParallelVsCrossCircuits = 0.;
	private double overestimationOfDivergenceOfTriangles = 0.;
	private double overestimationOfDivergenceOfCyclicTriangles = 0.;
	private double overestimationOfDivergenceOfTransitiveTriangles = 0.;
	private double overestimationOfDivergenceOfConcentration = 0;
	private double overestimationOfDivergenceOfEndogamicConcentration = 0.;
	private double overestimationOfDivergenceOfSymmetry = 0.;
	private double overestimationOfDivergenceOfStrengthConcentration = 0;
	private double overestimationOfDivergenceOfEndogamicStrengthConcentration = 0.;
	private double overestimationOfDivergenceOfStrengthSymmetry = 0.;
	
	private double squareDivergenceOfConcentration = 0.;
	private double squareDivergenceOfEndogamicConcentration = 0.;
	private double squareDivergenceOfSymmetry = 0.;

	private double squareDivergenceOfLoops = 0.;
	private double squareDivergenceOfCircuits = 0.;
	private double squareDivergenceOfParallelCircuits = 0.;
	private double squareDivergenceOfCrossCircuits = 0.;
	private double squareDivergenceOfBalanceOfParallelVsCrossCircuits = 0.;
	private double squareDivergenceOfTriangles = 0.;
	private double squareDivergenceOfCyclicTriangles = 0.;
	private double squareDivergenceOfTransitiveTriangles = 0.;

	private double squareDivergenceOfNormalizedLoops = 0.;
	private double squareDivergenceOfNormalizedCircuits = 0.;
	private double squareDivergenceOfNormalizedParallelCircuits = 0.;
	private double squareDivergenceOfNormalizedCrossCircuits = 0.;
	private double squareDivergenceOfNormalizedBalanceOfParallelVsCrossCircuits = 0.;
	private double squareDivergenceOfNormalizedTriangles = 0.;
	private double squareDivergenceOfNormalizedCyclicTriangles = 0.;
	private double squareDivergenceOfNormalizedTransitiveTriangles = 0.;
	
	


	private Graph graph;
	
	public MatrixStatistics(){
	}
	
	public MatrixStatistics(Graph source){
		graph = source;
		Matrix matrix = source.getSquareMatrix().getMatrix();
		analyze(matrix);
	}

	private void analyze (Matrix matrix){
		
		sum = matrix.getSum();
		nodeCount = matrix.getRowDim();
		maxNumberOfCircuits = new Double(Math.pow(sum, 2))/2;
		
		double squareSum = 0.;
		double crossSum = 0.;
		double loopSum = 0.;
		double squareLoopSum = 0.;
		double sumSquare = Math.pow(sum,2);
		double sumDoubleSquare = Math.pow(sum,4);
		double squareOutStrengthSum = 0.;
		double squareInStrengthSum = 0.;
		double squareStrengthProductSum = 0.;
		double cubeStrengthProductSum = 0.;
		double triStrengthProductSum = 0.;
		double triStrengthComplexProductSum = 0.;
		double biStrengthComplexProductSum = 0.;
		
		strengthProductSum = 0.;
		
		numberOfCyclicTriangles = 0;
		numberOfTransitiveTriangles = 0;
		
		for (int i=0;i<matrix.getRowDim();i++){
			loopSum = loopSum + matrix.get(i,i);
			squareLoopSum = squareLoopSum+Math.pow(matrix.get(i,i), 2);
			long rowSum = matrix.getRowSum(i);
			long colSum = matrix.getColSum(i);
			double strength = rowSum + colSum;
			if (strength > maxStrength){
				maxStrength = strength;
			}
			squareOutStrengthSum += Math.pow(rowSum, 2);
			squareInStrengthSum += Math.pow(colSum, 2);
			strengthProductSum += rowSum*colSum;
			squareStrengthProductSum += Math.pow(rowSum*colSum, 2);
			cubeStrengthProductSum += Math.pow(rowSum*colSum, 3);
			
			for (int j=0;j<matrix.getColDim();j++){
				squareSum = squareSum+Math.pow(matrix.get(i,j), 2);
				crossSum = crossSum+matrix.get(i,j)*matrix.get(j,i);
				long rowSum2 = matrix.getRowSum(j);
				long colSum2 = matrix.getColSum(j);
				biStrengthComplexProductSum += Math.pow(rowSum,2)*((Math.pow(colSum,2)*rowSum2*colSum2 + rowSum2*Math.pow(colSum2,3) + rowSum*colSum*Math.pow(colSum2,2)));					

				if (matrix.get(i,j)>maxWeight){
					maxWeight = matrix.get(i,j);
				}
				
/*				if (matrix.get(i,j)>0){
					for (int k=0; k<matrix.getRowDim();k++){
						int indirectPaths = matrix.get(i,j)*matrix.get(j,k);
						if (i<j && j<k) numberOfCyclicTriangles = numberOfCyclicTriangles + indirectPaths*matrix.get(k,i);
						if (i!=j && j!=k && k!=i) numberOfTransitiveTriangles = numberOfTransitiveTriangles + indirectPaths*matrix.get(i,k);
					}
				}*/

				for (int k=0; k<matrix.getRowDim();k++){
					long rowSum3 = matrix.getRowSum(k);
					long colSum3 = matrix.getColSum(k);
					triStrengthProductSum += rowSum*colSum*rowSum2*colSum2*rowSum3*colSum3;
					triStrengthComplexProductSum += Math.pow(rowSum,2)*Math.pow(colSum2,2)*rowSum3*colSum3;
					if (matrix.get(i,j)>0){
						int indirectPaths = matrix.get(i,j)*matrix.get(j,k);
						if (i<j && j<k) numberOfCyclicTriangles += indirectPaths*matrix.get(k,i);
						if (i!=j && j!=k && k!=i) numberOfTransitiveTriangles += indirectPaths*matrix.get(i,k);
					}
				}
			}
		}
		
		concentrationIndex = squareSum/sumSquare;
		minimumConcentration = 1.0/Math.pow(matrix.getRowDim(),2);
		endogamousClosureRate = loopSum/strengthProductSum;
		
		symmetryIndex = crossSum/squareSum;
		if (loopSum>0){
			endogamicConcentrationIndex = squareLoopSum/Math.pow(loopSum,2);
		} else {
			endogamicConcentrationIndex = 0.;
		}
		endogamyIndex = loopSum/sum;
		
		numberOfLoops = new Double(loopSum).intValue();
		numberOfParallelCircuits = new Double(squareSum - sum - squareLoopSum + loopSum).intValue()/2;
		normalizedNumberOfParallelCircuits = (squareSum - sum - squareLoopSum + loopSum)/sumSquare;
		numberOfCrossCircuits = new Double(crossSum - squareLoopSum).intValue()/2;
		normalizedNumberOfCrossCircuits = (crossSum - squareLoopSum)/sumSquare;
		balanceOfParallelVsCrossCircuits = numberOfParallelCircuits - numberOfCrossCircuits;
		numberOfCircuits = numberOfParallelCircuits + numberOfCrossCircuits;
		numberOfTriangles = numberOfCyclicTriangles + numberOfTransitiveTriangles;
		
		normalizedNumberOfCyclicTriangles = numberOfCyclicTriangles*3/Math.pow(sum, 3);
		normalizedNumberOfTransitiveTriangles = numberOfTransitiveTriangles*3/Math.pow(sum, 3);
		normalizedNumberOfCircuits = normalizedNumberOfParallelCircuits + normalizedNumberOfCrossCircuits;
		normalizedNumberOfTriangles = normalizedNumberOfCyclicTriangles + normalizedNumberOfTransitiveTriangles;
		
		strengthConcentrationIndex = squareInStrengthSum*squareOutStrengthSum/sumDoubleSquare;
		endogamicStrengthConcentrationIndex = squareStrengthProductSum/Math.pow(strengthProductSum,2);
		strengthSymmetryIndex = Math.pow(squareStrengthProductSum, 0.5)/strengthProductSum;
		
		double factor0 = new Double(1.0/sum);
		double factor1 = sum*(sum-1)/2;
		double factor2 = (1.0 - factor0); // 2*factor1/n^2

		double factor3 = factor2*Math.pow(strengthProductSum,2)/sumDoubleSquare + factor0*strengthProductSum/sumSquare;
		double factor4 = factor2*squareStrengthProductSum/sumDoubleSquare + factor0*strengthProductSum/sumSquare;

		expectedEndogamyIndex = strengthProductSum/sumSquare;
		expectedNumberOfLoops = expectedEndogamyIndex*sum;
		expectedConcentrationIndex = factor2*strengthConcentrationIndex + factor0;
		expectedSymmetryIndex = (factor3)/expectedConcentrationIndex;
		expectedEndogamicConcentrationIndex = factor4/factor3;
		
		double factor5 = (sum-1)*(sum-2)/Math.pow(sum, 5);
		double factor6 = 3*(sum-1)*(sum-2)/Math.pow(sum, 2); // 3*factor5/n^3
		
		double expectedRelativeNumberOfParallelCircuits = strengthConcentrationIndex - squareStrengthProductSum/sumDoubleSquare;
		double expectedRelativeNumberOfCrossCircuits = Math.pow(strengthProductSum,2)/sumDoubleSquare - squareStrengthProductSum/sumDoubleSquare;

		double expectedRelativeNumberofCyclicTriangles = triStrengthProductSum - 3*squareStrengthProductSum*strengthProductSum + 2*cubeStrengthProductSum;
		double expectedRelativeNumberofTransitiveTriangles = triStrengthComplexProductSum - biStrengthComplexProductSum + 2*cubeStrengthProductSum;
		
		expectedNumberOfParallelCircuits = factor1*(expectedRelativeNumberOfParallelCircuits);
		expectedNumberOfCrossCircuits = factor1*(expectedRelativeNumberOfCrossCircuits);
		expectedNumberOfCircuits = expectedNumberOfParallelCircuits + expectedNumberOfCrossCircuits;
		expectedBalanceOfParallelVsCrossCircuits = expectedNumberOfParallelCircuits - expectedNumberOfCrossCircuits;

		expectedNormalizedNumberOfParallelCircuits = factor2*(expectedRelativeNumberOfParallelCircuits);
		expectedNormalizedNumberOfCrossCircuits = factor2*(expectedRelativeNumberOfCrossCircuits);
		expectedNormalizedNumberOfCircuits = expectedNormalizedNumberOfParallelCircuits + expectedNormalizedNumberOfCrossCircuits;

		expectedNumberOfCyclicTriangles = factor5*expectedRelativeNumberofCyclicTriangles;
		expectedNumberOfTransitiveTriangles = factor5*expectedRelativeNumberofTransitiveTriangles;
		expectedNumberOfTriangles = expectedNumberOfCyclicTriangles + expectedNumberOfTransitiveTriangles;
		
		expectedNormalizedNumberOfCyclicTriangles = factor6*expectedRelativeNumberofCyclicTriangles;
		expectedNormalizedNumberOfTransitiveTriangles = factor6*expectedRelativeNumberofTransitiveTriangles;
		expectedNormalizedNumberOfTriangles = expectedNormalizedNumberOfCyclicTriangles + expectedNormalizedNumberOfTransitiveTriangles;
		
		setDivergences();
//		divergenceOfCircuits = (numberOfCircuits-expectedNumberOfCircuits)/expectedNumberOfCircuits;

//		testValue = matrix.getRowSum(0);
	}
	
	public void setOverestimation (Indicator indicator, MatrixStatistics sourceStats){
		switch (indicator){
		case CONCENTRATION:
			overestimationOfConcentration = concentrationIndex/sourceStats.concentrationIndex;
			break;
		case SYMMETRY:
			overestimationOfSymmetry = symmetryIndex/sourceStats.symmetryIndex;
			break;
		case ENDOGAMIC_CONCENTRATION:
			overestimationOfEndogamicConcentration = endogamicConcentrationIndex/sourceStats.endogamicConcentrationIndex;
			break;
		case STRENGTH_CONCENTRATION:
			overestimationOfStrengthConcentration = strengthConcentrationIndex/sourceStats.strengthConcentrationIndex;
			break;
		case STRENGTH_SYMMETRY:
			overestimationOfStrengthSymmetry = strengthSymmetryIndex/sourceStats.strengthSymmetryIndex;
			break;
		case ENDOGAMIC_STRENGTH_CONCENTRATION:
			overestimationOfEndogamicStrengthConcentration = endogamicStrengthConcentrationIndex/sourceStats.endogamicStrengthConcentrationIndex;
			break;
		case LOOPS:
			overestimationOfLoops = endogamyIndex/sourceStats.endogamyIndex;
			break;
		case DUAL_CIRCUITS:
			overestimationOfCircuits = normalizedNumberOfCircuits/sourceStats.normalizedNumberOfCircuits;
			break;
		case PARALLEL_CIRCUITS:
			overestimationOfParallelCircuits = normalizedNumberOfParallelCircuits/sourceStats.normalizedNumberOfParallelCircuits;
			break;
		case CROSS_CIRCUITS:
			overestimationOfCrossCircuits = normalizedNumberOfCrossCircuits/sourceStats.normalizedNumberOfCrossCircuits;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			overestimationOfBalanceOfParallelVsCrossCircuits = normalizedBalanceOfParallelVsCrossCircuits/sourceStats.normalizedBalanceOfParallelVsCrossCircuits;
			break;
		case TRIANGLES:
			overestimationOfTriangles = normalizedNumberOfTriangles/sourceStats.normalizedNumberOfTriangles;
			break;
		case TRANSITIVE_TRIANGLES:
			overestimationOfTransitiveTriangles = normalizedNumberOfTransitiveTriangles/sourceStats.normalizedNumberOfTransitiveTriangles;
			break;
		case CYCLIC_TRIANGLES:
			overestimationOfCyclicTriangles = normalizedNumberOfCyclicTriangles/sourceStats.normalizedNumberOfCyclicTriangles;
			break;
		}

	}
	
	public void setOverestimationOfExpectation (Indicator indicator, MatrixStatistics sourceStats){
		switch (indicator){
		case CONCENTRATION:
			overestimationOfExpectedConcentration = expectedConcentrationIndex/sourceStats.expectedConcentrationIndex;
			break;
		case SYMMETRY:
			overestimationOfExpectedSymmetry = expectedSymmetryIndex/sourceStats.expectedSymmetryIndex;
			break;
		case ENDOGAMIC_CONCENTRATION:
			overestimationOfExpectedEndogamicConcentration = expectedEndogamicConcentrationIndex/sourceStats.expectedEndogamicConcentrationIndex;
			break;
		case LOOPS:
			overestimationOfExpectedLoops = expectedEndogamyIndex/sourceStats.expectedEndogamyIndex;
			break;
		case DUAL_CIRCUITS:
			overestimationOfExpectedCircuits = expectedNormalizedNumberOfCircuits/sourceStats.expectedNormalizedNumberOfCircuits;
			break;
		case PARALLEL_CIRCUITS:
			overestimationOfExpectedParallelCircuits = expectedNormalizedNumberOfParallelCircuits/sourceStats.expectedNormalizedNumberOfParallelCircuits;
			break;
		case CROSS_CIRCUITS:
			overestimationOfExpectedCrossCircuits = expectedNormalizedNumberOfCrossCircuits/sourceStats.expectedNormalizedNumberOfCrossCircuits;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			overestimationOfExpectedBalanceOfParallelVsCrossCircuits = expectedNormalizedBalanceOfParallelVsCrossCircuits/sourceStats.expectedNormalizedBalanceOfParallelVsCrossCircuits;
			break;
		case TRIANGLES:
			overestimationOfExpectedTriangles = expectedNormalizedNumberOfTriangles/sourceStats.expectedNormalizedNumberOfTriangles;
			break;
		case TRANSITIVE_TRIANGLES:
			overestimationOfExpectedTransitiveTriangles = expectedNormalizedNumberOfTransitiveTriangles/sourceStats.expectedNormalizedNumberOfTransitiveTriangles;
			break;
		case CYCLIC_TRIANGLES:
			overestimationOfExpectedCyclicTriangles = expectedNormalizedNumberOfCyclicTriangles/sourceStats.expectedNormalizedNumberOfCyclicTriangles;
			break;
		}

	}
	
	
	public void compareValues (MatrixStatistics sourceStats){
		if (sourceStats != null){
			for (Indicator indicator : Indicator.values()){
				setOverestimation(indicator,sourceStats);
				setOverestimationOfExpectation(indicator,sourceStats);
				setOverestimationOfDivergence(indicator);
			}
/*			overestimationOfLoops = endogamyIndex/sourceStatistics.endogamyIndex;
			overestimationOfCircuits = normalizedNumberOfCircuits/sourceStatistics.getNormalizedNumberOfCircuits();
			overestimationOfTriangles = normalizedNumberOfTriangles/sourceStatistics.getNormalizedNumberOfTriangles();
			overestimationOfConcentration = concentrationIndex/sourceStatistics.getConcentrationIndex();
			overestimationOfSymmetry = symmetryIndex/sourceStatistics.getSymmetryIndex();*/
		}
	}
	
	public double getArcCount() {
		return arcCount;
	}
	
	
	public Graph getGraph() {
		return graph;
	}
	public double getMaximalNumberOfCircuits() {
		return maxNumberOfCircuits;
	}
	public double getMinimumConcentration(){
		return minimumConcentration;
	}	
	
	public double getSum(){
		return sum;
	}


/*	public double getExpectedNumberOfTriangles() {
		return expectedNumberOfTriangles;
	}

	public double getExpectedNumberOfCyclicTriangles() {
		return expectedNumberOfCyclicTriangles;
	}

	public double getExpectedNumberOfTransitiveTriangles() {
		return expectedNumberOfTransitiveTriangles;
	}

	public double getExpectedNormalizedNumberOfTriangles() {
		return expectedNormalizedNumberOfTriangles;
	}
	
	public double getBalanceOfParallelVsCrossCircuits(){
		return balanceOfParallelVsCrossCircuits;
	}

	public double getConcentrationIndex() {
		return concentrationIndex;
	}

	public double getEndogamicConcentrationIndex() {
		return endogamicConcentrationIndex;
	}

	public double getEndogamicStrengthConcentrationIndex() {
		return endogamicStrengthConcentrationIndex;
	}

	public double getEndogamyIndex() {
		return endogamyIndex;
	}

	public double getExpectedBalanceOfParallelVsCrossCircuits(){
		return expectedBalanceOfParallelVsCrossCircuits;
	}
	public double getExpectedConcentrationIndex() {
		return expectedConcentrationIndex;
	}
	public double getExpectedEndogamicConcentrationIndex() {
		return expectedEndogamicConcentrationIndex;
	}
	public double getExpectedEndogamyIndex() {
		return expectedEndogamyIndex;
	}

	public double getExpectedNormalizedBalanceOfParallelVsCrossCircuits(){
		return expectedNormalizedNumberOfParallelCircuits - expectedNormalizedNumberOfCrossCircuits;
	}
	public double getExpectedNormalizedNumberOfCircuits() {
		return expectedNormalizedNumberOfCircuits;
	}
	public double getExpectedNormalizedNumberOfCrossCircuits() {
		return expectedNormalizedNumberOfCrossCircuits;
	}
	public double getExpectedNormalizedNumberOfParallelCircuits() {
		return expectedNormalizedNumberOfParallelCircuits;
	}
	public double getExpectedNumberOfCircuits() {
		return expectedNumberOfCircuits;
	}
	public double getExpectedNumberOfCrossCircuits() {
		return expectedNumberOfCrossCircuits;
	}
	public double getExpectedNumberOfParallelCircuits() {
		return expectedNumberOfParallelCircuits;
	}
	
	public double getExpectedNumberOfLoops(){
		return expectedNumberOfLoops;
	}

	public double getExpectedSymmetryIndex() {
		return expectedSymmetryIndex;
	}


	public double getNormalizedBalanceOfParallelVsCrossCircuits(){
		return normalizedNumberOfParallelCircuits - normalizedNumberOfCrossCircuits;
	}
	public double getNormalizedNumberOfCircuits() {
		return normalizedNumberOfCircuits;
	}
	public double getNormalizedNumberOfCrossCircuits() {
		return normalizedNumberOfCrossCircuits;
	}
	public double getNormalizedNumberOfParallelCircuits() {
		return normalizedNumberOfParallelCircuits;
	}
	public double getNormalizedNumberOfTriangles() {
		return normalizedNumberOfTriangles;
	}
	public double getNumberOfCircuits() {
		return numberOfCircuits;
	}
	public double getNumberOfCrossCircuits() {
		return numberOfCrossCircuits;
	}
	public double getNumberOfCyclicTriangles() {
		return numberOfCyclicTriangles;
	}
	public double getNumberOfLoops(){
		return numberOfLoops;
	}
	public double getNumberOfParallelCircuits() {
		return numberOfParallelCircuits;
	}
	public double getNumberOfTransitiveTriangles() {
		return numberOfTransitiveTriangles;
	}

	public double getNumberOfTriangles() {
		return numberOfTriangles;
	}
	public double getOverestimationOfCircuits() {
		return overestimationOfCircuits;
	}
	
	public double getOverestimationOfConcentration() {
		return overestimationOfConcentration;
	}
	public double getOverestimationOfLoops() {
		return overestimationOfLoops;
	}
	public double getOverestimationOfTriangles() {
		return overestimationOfTriangles;
	}
	
	public double getStrengthConcentrationIndex() {
		return strengthConcentrationIndex;
	}
	
	public double getStrengthSymmetryIndex() {
		return strengthSymmetryIndex;
	}
	
	public double getSurplusOfCircuits(){
		return (numberOfCircuits - expectedNumberOfCircuits)/expectedNumberOfCircuits;
	}
	
	public double getSurplusOfCrossCircuits(){
		return (numberOfCrossCircuits - expectedNumberOfCrossCircuits)/expectedNumberOfCrossCircuits;
	}

	public double getSurplusOfParallelCircuits(){
		return (numberOfParallelCircuits - expectedNumberOfParallelCircuits)/expectedNumberOfParallelCircuits;
	}

	public double getSymmetryIndex() {
		return symmetryIndex;
	}*/
	
	private void incrementSquareDivergences (MatrixStatistics stats){
		
		squareDivergenceOfConcentration += Math.pow(stats.divergenceOfConcentration, 2);
		squareDivergenceOfEndogamicConcentration += Math.pow(stats.divergenceOfEndogamicConcentration, 2);
		squareDivergenceOfSymmetry += Math.pow(stats.divergenceOfSymmetry, 2);
		squareDivergenceOfLoops += Math.pow(stats.divergenceOfLoops, 2);
		squareDivergenceOfCircuits += Math.pow(stats.divergenceOfCircuits, 2);
		squareDivergenceOfParallelCircuits += Math.pow(stats.divergenceOfParallelCircuits, 2);
		squareDivergenceOfCrossCircuits += Math.pow(stats.divergenceOfCrossCircuits, 2);
		squareDivergenceOfBalanceOfParallelVsCrossCircuits += Math.pow(stats.divergenceOfBalanceOfParallelVsCrossCircuits, 2);
		squareDivergenceOfTriangles += Math.pow(stats.divergenceOfTriangles, 2);
		squareDivergenceOfCyclicTriangles += Math.pow(stats.divergenceOfCyclicTriangles, 2);
		squareDivergenceOfTransitiveTriangles += Math.pow(stats.divergenceOfTransitiveTriangles, 2);
		squareDivergenceOfNormalizedLoops += Math.pow(stats.divergenceOfNormalizedLoops, 2);
		squareDivergenceOfNormalizedCircuits += Math.pow(stats.divergenceOfNormalizedCircuits, 2);
		squareDivergenceOfNormalizedParallelCircuits += Math.pow(stats.divergenceOfNormalizedParallelCircuits, 2);
		squareDivergenceOfNormalizedCrossCircuits += Math.pow(stats.divergenceOfNormalizedCrossCircuits, 2);
		squareDivergenceOfNormalizedBalanceOfParallelVsCrossCircuits += Math.pow(stats.divergenceOfNormalizedBalanceOfParallelVsCrossCircuits, 2);
		squareDivergenceOfNormalizedTriangles += Math.pow(stats.divergenceOfNormalizedTriangles, 2);
		squareDivergenceOfNormalizedCyclicTriangles += Math.pow(stats.divergenceOfNormalizedCyclicTriangles, 2);
		squareDivergenceOfNormalizedTransitiveTriangles += Math.pow(stats.divergenceOfNormalizedTransitiveTriangles, 2);
	}
	
	public void incrementValues (MatrixStatistics stats){

		sum += stats.getSum();
		nodeCount += stats.getNodeCount();
		arcCount += stats.getArcCount();
		maxNumberOfCircuits += stats.getMaximalNumberOfCircuits();
		
/*		endogamyIndex += stats.getEndogamyIndex();
		endogamicConcentrationIndex += stats.getEndogamicConcentrationIndex();
		concentrationIndex += stats.getConcentrationIndex();
		symmetryIndex += stats.getSymmetryIndex();*/
		
		for (Indicator indicator : Indicator.values()){
			for (Mode mode : Mode.values()){
				set(indicator,mode,get(indicator,mode)+stats.get(indicator, mode));
			}
		}
		
		incrementSquareDivergences(stats);
		
/*		numberOfLoops += stats.getNumberOfLoops();
		numberOfCircuits += stats.getNumberOfCircuits();
		numberOfCrossCircuits += stats.getNumberOfCrossCircuits();
		numberOfParallelCircuits += stats.getNumberOfParallelCircuits();
		balanceOfParallelVsCrossCircuits += stats.getBalanceOfParallelVsCrossCircuits();
		numberOfTriangles += stats.getNumberOfTriangles();
		numberOfCyclicTriangles += stats.getNumberOfCyclicTriangles();
		numberOfTransitiveTriangles += stats.getNumberOfTransitiveTriangles();
		
		normalizedNumberOfCircuits += stats.getNormalizedNumberOfCircuits();
		normalizedNumberOfTriangles += stats.getNormalizedNumberOfTriangles();

		expectedEndogamyIndex += stats.getExpectedEndogamyIndex();
		expectedEndogamicConcentrationIndex += stats.getExpectedEndogamicConcentrationIndex();
		expectedConcentrationIndex += stats.getExpectedConcentrationIndex();
		expectedSymmetryIndex += stats.getExpectedSymmetryIndex();
		expectedNumberOfLoops += stats.getExpectedNumberOfLoops();
		expectedNumberOfCircuits += stats.getExpectedNumberOfCircuits();
		expectedNumberOfCrossCircuits += stats.getExpectedNumberOfCrossCircuits();
		expectedNumberOfParallelCircuits += stats.getExpectedNumberOfParallelCircuits();
		expectedBalanceOfParallelVsCrossCircuits += stats.getExpectedBalanceOfParallelVsCrossCircuits();
		expectedNumberOfTriangles += stats.getExpectedNumberOfTriangles();
		expectedNumberOfCyclicTriangles += stats.getExpectedNumberOfCyclicTriangles();
		expectedNumberOfTransitiveTriangles += stats.getExpectedNumberOfTransitiveTriangles();
		
		expectedNormalizedNumberOfCircuits += stats.getExpectedNormalizedNumberOfCircuits();
		expectedNormalizedNumberOfTriangles += stats.getExpectedNormalizedNumberOfTriangles();
		
		divergenceOfCircuits += stats.getDivergenceOfCircuits();
		
		testValue += stats.getTestValue();*/
	}
	
/*	public double getDivergenceOfCircuits() {
		return divergenceOfCircuits;
	}*/


	public void setGraph(Graph graph) {
		this.graph = graph;
	}
	
	public void normalizeValues (int runs){
		
		double n = new Double(runs);

		this.runs = runs;

		sum = sum/n;
		nodeCount = nodeCount/n;
		arcCount = arcCount/n;
		maxNumberOfCircuits = maxNumberOfCircuits/n;

/*		endogamyIndex = endogamyIndex/new Double(sum);
		endogamicConcentrationIndex = endogamicConcentrationIndex/new Double(sum);
		concentrationIndex = concentrationIndex/new Double(sum);
		symmetryIndex = symmetryIndex/new Double(sum);*/
		
		for (Indicator indicator : Indicator.values()){
			for (Mode mode : Mode.values()){
				// calculate Standard Deviation (only for divergences)
				if (mode == Mode.DIVERGENCE) {
					double squareSum = getSquareDivergence(indicator);
					double sumSquare = Math.pow(getDivergence(indicator),2);
					
					double sigma = Math.pow((squareSum - sumSquare/n)/(n-1),0.5);
					
					setSquareDivergence(indicator,sigma);
				}
				// calculate Means
				set(indicator,mode,get(indicator,mode)/n);
			}
		}
		
/*		numberOfLoops = numberOfLoops/new Double(sum);
		numberOfCircuits = numberOfCircuits/new Double(sum);
		numberOfCrossCircuits = numberOfCrossCircuits/new Double(sum);
		numberOfParallelCircuits = numberOfParallelCircuits/new Double(sum);
		balanceOfParallelVsCrossCircuits = balanceOfParallelVsCrossCircuits/new Double(sum);
		numberOfTriangles = numberOfTriangles/new Double(sum);
		numberOfCyclicTriangles = numberOfCyclicTriangles/new Double(sum);
		numberOfTransitiveTriangles = numberOfTransitiveTriangles/new Double(sum);
		normalizedNumberOfCircuits = normalizedNumberOfCircuits/new Double(sum);
		normalizedNumberOfTriangles = normalizedNumberOfTriangles/new Double(sum);

		expectedEndogamyIndex = expectedEndogamyIndex/new Double(sum);
		expectedEndogamicConcentrationIndex = expectedEndogamicConcentrationIndex/new Double(sum);
		expectedConcentrationIndex = expectedConcentrationIndex/new Double(sum);
		expectedSymmetryIndex = expectedSymmetryIndex/new Double(sum);
		expectedNumberOfLoops = expectedNumberOfLoops/new Double(sum);
		expectedNumberOfCircuits = expectedNumberOfCircuits/new Double(sum);
		expectedNumberOfCrossCircuits = expectedNumberOfCrossCircuits/new Double(sum);
		expectedNumberOfParallelCircuits = expectedNumberOfParallelCircuits/new Double(sum);
		expectedBalanceOfParallelVsCrossCircuits = expectedBalanceOfParallelVsCrossCircuits/new Double(sum);
		expectedNumberOfTriangles = expectedNumberOfTriangles/new Double(sum);
		expectedNumberOfCyclicTriangles = expectedNumberOfCyclicTriangles/new Double(sum);
		expectedNumberOfTransitiveTriangles = expectedNumberOfTransitiveTriangles/new Double(sum);
		expectedNormalizedNumberOfCircuits = expectedNormalizedNumberOfCircuits/new Double(sum);
		expectedNormalizedNumberOfTriangles = expectedNormalizedNumberOfTriangles/new Double(sum);
		
		divergenceOfCircuits = divergenceOfCircuits/new Double(sum);

		
		testValue = testValue/new Double(sum);*/

	}
	
	public double getOverestimationOfSymmetry() {
		return overestimationOfSymmetry;
	}

	
	public double get(Indicator indicator, Mode mode){
		double result;
		
		result = 0;
		switch (mode){
		case SIMPLE:
			result = getNumber(indicator);
			break;
		case EXPECTED:
			result = getExpectedNumber(indicator);
			break;
		case NORMALIZED:
			result = getNormalizedNumber(indicator);
			break;
		case EXPECTED_NORMALIZED:
			result = getExpectedNormalizedNumber(indicator);
			break;
		case DIVERGENCE:
			result = getDivergence(indicator);
			break;
		case DIVERGENCE_NORMALIZED:
			result = getDivergenceNormalized(indicator);
			break;
		case OVERESTIMATION:
			result = getOverestimation(indicator);
			break;
		case OVERESTIMATION_EXPECTED:
			result = getOverestimationOfExpectation(indicator);
			break;
		case OVERESTIMATION_DIVERGENCE:
			result = getOverestimationOfDivergence(indicator);
			break;
		}
		
		//
		return result;
	}
	
	public double set(Indicator indicator, Mode mode, double value){
		double result;
		
		result = 0;
		switch (mode){
		case SIMPLE:
			setNumber(indicator,value);
			break;
		case EXPECTED:
			setExpectedNumber(indicator,value);
			break;
		case NORMALIZED:
			setNormalizedNumber(indicator,value);
			break;
		case EXPECTED_NORMALIZED:
			setExpectedNormalizedNumber(indicator,value);
			break;
		case DIVERGENCE:
			setDivergence(indicator,value);
			break;
		case DIVERGENCE_NORMALIZED:
			setDivergenceNormalized(indicator,value);
			break;
		}
		
		//
		return result;
	}
	

	
	public double getNumber (Indicator indicator){
		double result;
		
		result = 0.;
		switch (indicator){
		case CONCENTRATION:
			result = concentrationIndex;
			break;
		case SYMMETRY:
			result = symmetryIndex;
			break;
		case ENDOGAMIC_CONCENTRATION:
			result = endogamicConcentrationIndex;
			break;
		case STRENGTH_CONCENTRATION:
			result = strengthConcentrationIndex;
			break;
		case STRENGTH_SYMMETRY:
			result = strengthSymmetryIndex;
			break;
		case ENDOGAMIC_STRENGTH_CONCENTRATION:
			result = endogamicStrengthConcentrationIndex;
			break;
		case LOOPS:
			result = numberOfLoops;
			break;
		case DUAL_CIRCUITS:
			result = numberOfCircuits;
			break;
		case PARALLEL_CIRCUITS:
			result = numberOfParallelCircuits;
			break;
		case CROSS_CIRCUITS:
			result = numberOfCrossCircuits;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			result = balanceOfParallelVsCrossCircuits;
			break;
		case TRIANGLES:
			result = numberOfTriangles;
			break;
		case TRANSITIVE_TRIANGLES:
			result = numberOfTransitiveTriangles;
			break;
		case CYCLIC_TRIANGLES:
			result = numberOfCyclicTriangles;
			break;
		case MAX_WEIGHT:
			result = maxWeight;
			break;
		case MAX_STRENGTH:
			result = maxStrength;
			break;
		}
		//
		return result;
	}
	
	public double getOverestimation (Indicator indicator){
		double result;
		
		result = 0.;
		switch (indicator){
		case CONCENTRATION:
			result = overestimationOfConcentration;
			break;
		case SYMMETRY:
			result = overestimationOfSymmetry;
			break;
		case ENDOGAMIC_CONCENTRATION:
			result = overestimationOfEndogamicConcentration;
			break;
		case STRENGTH_CONCENTRATION:
			result = overestimationOfStrengthConcentration;
			break;
		case STRENGTH_SYMMETRY:
			result = overestimationOfStrengthSymmetry;
			break;
		case ENDOGAMIC_STRENGTH_CONCENTRATION:
			result = overestimationOfEndogamicStrengthConcentration;
			break;
		case LOOPS:
			result = overestimationOfLoops;
			break;
		case DUAL_CIRCUITS:
			result = overestimationOfCircuits;
			break;
		case PARALLEL_CIRCUITS:
			result = overestimationOfParallelCircuits;
			break;
		case CROSS_CIRCUITS:
			result = overestimationOfCrossCircuits;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			result = overestimationOfBalanceOfParallelVsCrossCircuits;
			break;
		case TRIANGLES:
			result = overestimationOfTriangles;
			break;
		case TRANSITIVE_TRIANGLES:
			result = overestimationOfTransitiveTriangles;
			break;
		case CYCLIC_TRIANGLES:
			result = overestimationOfCyclicTriangles;
			break;
		}
		//
		return result;
	}
	
	public double getOverestimationOfExpectation (Indicator indicator){
		double result;
		
		result = 0.;
		switch (indicator){
		case CONCENTRATION:
			result = overestimationOfExpectedConcentration;
			break;
		case SYMMETRY:
			result = overestimationOfExpectedSymmetry;
			break;
		case ENDOGAMIC_CONCENTRATION:
			result = overestimationOfExpectedEndogamicConcentration;
			break;
		case STRENGTH_CONCENTRATION:
			result = overestimationOfExpectedStrengthConcentration;
			break;
		case STRENGTH_SYMMETRY:
			result = overestimationOfExpectedStrengthSymmetry;
			break;
		case ENDOGAMIC_STRENGTH_CONCENTRATION:
			result = overestimationOfExpectedEndogamicStrengthConcentration;
			break;
		case LOOPS:
			result = overestimationOfExpectedLoops;
			break;
		case DUAL_CIRCUITS:
			result = overestimationOfExpectedCircuits;
			break;
		case PARALLEL_CIRCUITS:
			result = overestimationOfExpectedParallelCircuits;
			break;
		case CROSS_CIRCUITS:
			result = overestimationOfExpectedCrossCircuits;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			result = overestimationOfExpectedBalanceOfParallelVsCrossCircuits;
			break;
		case TRIANGLES:
			result = overestimationOfExpectedTriangles;
			break;
		case TRANSITIVE_TRIANGLES:
			result = overestimationOfExpectedTransitiveTriangles;
			break;
		case CYCLIC_TRIANGLES:
			result = overestimationOfExpectedCyclicTriangles;
			break;
		}
		//
		return result;
	}
	
	public double getOverestimationOfDivergence (Indicator indicator){
		double result;
		
		result = 0.;
		switch (indicator){
		case CONCENTRATION:
			result = overestimationOfDivergenceOfConcentration;
			break;
		case SYMMETRY:
			result = overestimationOfDivergenceOfSymmetry;
			break;
		case ENDOGAMIC_CONCENTRATION:
			result = overestimationOfDivergenceOfEndogamicConcentration;
			break;
		case STRENGTH_CONCENTRATION:
			result = overestimationOfDivergenceOfStrengthConcentration;
			break;
		case STRENGTH_SYMMETRY:
			result = overestimationOfDivergenceOfStrengthSymmetry;
			break;
		case ENDOGAMIC_STRENGTH_CONCENTRATION:
			result = overestimationOfDivergenceOfEndogamicStrengthConcentration;
			break;
		case LOOPS:
			result = overestimationOfDivergenceOfLoops;
			break;
		case DUAL_CIRCUITS:
			result = overestimationOfDivergenceOfCircuits;
			break;
		case PARALLEL_CIRCUITS:
			result = overestimationOfDivergenceOfParallelCircuits;
			break;
		case CROSS_CIRCUITS:
			result = overestimationOfDivergenceOfCrossCircuits;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			result = overestimationOfDivergenceOfBalanceOfParallelVsCrossCircuits;
			break;
		case TRIANGLES:
			result = overestimationOfDivergenceOfTriangles;
			break;
		case TRANSITIVE_TRIANGLES:
			result = overestimationOfDivergenceOfTransitiveTriangles;
			break;
		case CYCLIC_TRIANGLES:
			result = overestimationOfDivergenceOfCyclicTriangles;
			break;
		}
		//
		return result;
	}
	
	
	public double getDivergence (Indicator indicator){
		double result;
		
		result = 0.;
		switch (indicator){
		case CONCENTRATION:
			result = divergenceOfConcentration;
			break;
		case SYMMETRY:
			result = divergenceOfSymmetry;
			break;
		case ENDOGAMIC_CONCENTRATION:
			result = divergenceOfEndogamicConcentration;
			break;
		case LOOPS:
			result = divergenceOfLoops;
			break;
		case DUAL_CIRCUITS:
			result = divergenceOfCircuits;
			break;
		case PARALLEL_CIRCUITS:
			result = divergenceOfParallelCircuits;
			break;
		case CROSS_CIRCUITS:
			result = divergenceOfCrossCircuits;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			result = divergenceOfBalanceOfParallelVsCrossCircuits;
			break;
		case TRIANGLES:
			result = divergenceOfTriangles;
			break;
		case TRANSITIVE_TRIANGLES:
			result = divergenceOfTransitiveTriangles;
			break;
		case CYCLIC_TRIANGLES:
			result = divergenceOfCyclicTriangles;
			break;
		}
		//
		return result;
	}
	
	public double getSquareDivergence (Indicator indicator){
		double result;
		
		result = 0.;
		switch (indicator){
		case CONCENTRATION:
			result = squareDivergenceOfConcentration;
			break;
		case SYMMETRY:
			result = squareDivergenceOfSymmetry;
			break;
		case ENDOGAMIC_CONCENTRATION:
			result = squareDivergenceOfEndogamicConcentration;
			break;
		case LOOPS:
			result = squareDivergenceOfLoops;
			break;
		case DUAL_CIRCUITS:
			result = squareDivergenceOfCircuits;
			break;
		case PARALLEL_CIRCUITS:
			result = squareDivergenceOfParallelCircuits;
			break;
		case CROSS_CIRCUITS:
			result = squareDivergenceOfCrossCircuits;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			result = squareDivergenceOfBalanceOfParallelVsCrossCircuits;
			break;
		case TRIANGLES:
			result = squareDivergenceOfTriangles;
			break;
		case TRANSITIVE_TRIANGLES:
			result = squareDivergenceOfTransitiveTriangles;
			break;
		case CYCLIC_TRIANGLES:
			result = squareDivergenceOfCyclicTriangles;
			break;
		}
		
		//
		return result;
	}
	
	public double getDivergenceNormalized (Indicator indicator){
		double result;
		
		result = 0.;
		switch (indicator){
		case LOOPS:
			result = divergenceOfNormalizedLoops;
			break;
		case DUAL_CIRCUITS:
			result = divergenceOfNormalizedCircuits;
			break;
		case PARALLEL_CIRCUITS:
			result = divergenceOfNormalizedParallelCircuits;
			break;
		case CROSS_CIRCUITS:
			result = divergenceOfNormalizedCrossCircuits;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			result = divergenceOfNormalizedBalanceOfParallelVsCrossCircuits;
			break;
		case TRIANGLES:
			result = divergenceOfNormalizedTriangles;
			break;
		case TRANSITIVE_TRIANGLES:
			result = divergenceOfNormalizedTransitiveTriangles;
			break;
		case CYCLIC_TRIANGLES:
			result = divergenceOfNormalizedCyclicTriangles;
			break;
		}
		//
		return result;
	}
	
	public void setNumber (Indicator indicator, double value){
		
		switch (indicator){
		case CONCENTRATION:
			concentrationIndex = value;
			break;
		case SYMMETRY:
			symmetryIndex = value;
			break;
		case ENDOGAMIC_CONCENTRATION:
			endogamicConcentrationIndex = value;
			break;
		case STRENGTH_CONCENTRATION:
			strengthConcentrationIndex = value;
			break;
		case STRENGTH_SYMMETRY:
			strengthSymmetryIndex = value;
			break;
		case ENDOGAMIC_STRENGTH_CONCENTRATION:
			endogamicStrengthConcentrationIndex = value;
			break;
		case LOOPS:
			numberOfLoops = value;
			break;
		case DUAL_CIRCUITS:
			numberOfCircuits = value;
			break;
		case PARALLEL_CIRCUITS:
			numberOfParallelCircuits = value;
			break;
		case CROSS_CIRCUITS:
			numberOfCrossCircuits = value;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			balanceOfParallelVsCrossCircuits = value;
			break;
		case TRIANGLES:
			numberOfTriangles = value;
			break;
		case TRANSITIVE_TRIANGLES:
			numberOfTransitiveTriangles = value;
			break;
		case CYCLIC_TRIANGLES:
			numberOfCyclicTriangles = value;
			break;
		case MAX_WEIGHT:
			maxWeight = value;
			break;
		case MAX_STRENGTH:
			maxStrength = value;
			break;
		}
	}
	
	public void setNormalizedNumber (Indicator indicator, double value){
		
		switch (indicator){
		case LOOPS:
			endogamyIndex = value;
			break;
		case DUAL_CIRCUITS:
			normalizedNumberOfCircuits = value;
			break;
		case PARALLEL_CIRCUITS:
			normalizedNumberOfParallelCircuits = value;
			break;
		case CROSS_CIRCUITS:
			normalizedNumberOfCrossCircuits = value;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			normalizedBalanceOfParallelVsCrossCircuits = value;
			break;
		case TRIANGLES:
			normalizedNumberOfTriangles = value;
			break;
		case TRANSITIVE_TRIANGLES:
			normalizedNumberOfTransitiveTriangles = value;
			break;
		case CYCLIC_TRIANGLES:
			normalizedNumberOfCyclicTriangles = value;
			break;
		}
	}
	
	public void setExpectedNormalizedNumber (Indicator indicator, double value){
		
		switch (indicator){
		case LOOPS:
			expectedEndogamyIndex = value;
			break;
		case DUAL_CIRCUITS:
			expectedNormalizedNumberOfCircuits = value;
			break;
		case PARALLEL_CIRCUITS:
			expectedNormalizedNumberOfParallelCircuits = value;
			break;
		case CROSS_CIRCUITS:
			expectedNormalizedNumberOfCrossCircuits = value;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			expectedNormalizedBalanceOfParallelVsCrossCircuits = value;
			break;
		case TRIANGLES:
			expectedNormalizedNumberOfTriangles = value;
			break;
		case TRANSITIVE_TRIANGLES:
			expectedNormalizedNumberOfTransitiveTriangles = value;
			break;
		case CYCLIC_TRIANGLES:
			expectedNormalizedNumberOfCyclicTriangles = value;
			break;
		}
	}

	
	public void setExpectedNumber (Indicator indicator, double value){
		
		switch (indicator){
		case CONCENTRATION:
			expectedConcentrationIndex = value;
			break;
		case SYMMETRY:
			expectedSymmetryIndex = value;
			break;
		case ENDOGAMIC_CONCENTRATION:
			expectedEndogamicConcentrationIndex = value;
			break;
		case LOOPS:
			expectedNumberOfLoops = value;
			break;
		case DUAL_CIRCUITS:
			expectedNumberOfCircuits = value;
			break;
		case PARALLEL_CIRCUITS:
			expectedNumberOfParallelCircuits = value;
			break;
		case CROSS_CIRCUITS:
			expectedNumberOfCrossCircuits = value;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			expectedBalanceOfParallelVsCrossCircuits = value;
			break;
		case TRIANGLES:
			expectedNumberOfTriangles = value;
			break;
		case TRANSITIVE_TRIANGLES:
			expectedNumberOfTransitiveTriangles = value;
			break;
		case CYCLIC_TRIANGLES:
			expectedNumberOfCyclicTriangles = value;
			break;
		}
	}
	
	public void setDivergences (){
		
		for (Indicator indicator : Indicator.values()){
			setDivergence(indicator,getNumber(indicator)/getExpectedNumber(indicator));
			setDivergenceNormalized(indicator,getNormalizedNumber(indicator)/getExpectedNormalizedNumber(indicator));
		}
		
/*		divergenceOfLoops = numberOfLoops - expectedNumberOfLoops;
		divergenceOfCircuits = numberOfCircuits - expectedNumberOfCircuits;
		divergenceOfParallelCircuits = numberOfParallelCircuits - expectedNumberOfParallelCircuits;
		divergenceOfCrossCircuits = numberOfCrossCircuits - expectedNumberOfCrossCircuits;
		divergenceOfTriangles = numberOfTriangles - expectedNumberOfTriangles;
		divergenceOfTransitiveTriangles = numberOfTransitiveTriangles - expectedNumberOfTransitiveTriangles;
		divergenceOfCyclicTriangles = numberOfCyclicTriangles - expectedNumberOfCyclicTriangles;
		divergenceOfBalanceOfParallelVsCrossCircuits = balanceOfParallelVsCrossCircuits - expectedBalanceOfParallelVsCrossCircuits;

		divergenceOfNormalizedLoops = endogamyIndex - expectedEndogamyIndex;
		divergenceOfNormalizedCircuits = normalizedNumberOfCircuits - expectedNormalizedNumberOfCircuits;
		divergenceOfNormalizedParallelCircuits = normalizedNumberOfParallelCircuits - expectedNormalizedNumberOfParallelCircuits;
		divergenceOfNormalizedCrossCircuits = normalizedNumberOfCrossCircuits - expectedNormalizedNumberOfCrossCircuits;
		divergenceOfNormalizedTriangles = normalizedNumberOfTriangles - expectedNormalizedNumberOfTriangles;
		divergenceOfNormalizedTransitiveTriangles = normalizedNumberOfTransitiveTriangles - expectedNormalizedNumberOfTransitiveTriangles;
		divergenceOfNormalizedCyclicTriangles = normalizedNumberOfCyclicTriangles - expectedNormalizedNumberOfCyclicTriangles;
		divergenceOfNormalizedBalanceOfParallelVsCrossCircuits = normalizedBalanceOfParallelVsCrossCircuits - expectedNormalizedBalanceOfParallelVsCrossCircuits;*/
		
	}

	public void setDivergence (Indicator indicator, double value){
		
		switch (indicator){
		case CONCENTRATION:
			divergenceOfConcentration = value;
			break;
		case SYMMETRY:
			divergenceOfSymmetry = value;
			break;
		case ENDOGAMIC_CONCENTRATION:
			divergenceOfEndogamicConcentration = value;
			break;
		case LOOPS:
			divergenceOfLoops = value;
			break;
		case DUAL_CIRCUITS:
			divergenceOfCircuits = value;
			break;
		case PARALLEL_CIRCUITS:
			divergenceOfParallelCircuits = value;
			break;
		case CROSS_CIRCUITS:
			divergenceOfCrossCircuits = value;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			divergenceOfBalanceOfParallelVsCrossCircuits = value;
			break;
		case TRIANGLES:
			divergenceOfTriangles = value;
			break;
		case TRANSITIVE_TRIANGLES:
			divergenceOfTransitiveTriangles = value;
			break;
		case CYCLIC_TRIANGLES:
			divergenceOfCyclicTriangles = value;
			break;
		}
	}
	
	public void setOverestimationOfDivergence (Indicator indicator){
		
		double value = getOverestimation(indicator)/getOverestimationOfExpectation(indicator);
		
		switch (indicator){
		case CONCENTRATION:
			overestimationOfDivergenceOfConcentration = value;
			break;
		case SYMMETRY:
			overestimationOfDivergenceOfSymmetry = value;
			break;
		case ENDOGAMIC_CONCENTRATION:
			overestimationOfDivergenceOfEndogamicConcentration = value;
			break;
		case LOOPS:
			overestimationOfDivergenceOfLoops = value;
			break;
		case DUAL_CIRCUITS:
			overestimationOfDivergenceOfCircuits = value;
			break;
		case PARALLEL_CIRCUITS:
			overestimationOfDivergenceOfParallelCircuits = value;
			break;
		case CROSS_CIRCUITS:
			overestimationOfDivergenceOfCrossCircuits = value;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			overestimationOfDivergenceOfBalanceOfParallelVsCrossCircuits = value;
			break;
		case TRIANGLES:
			overestimationOfDivergenceOfTriangles = value;
			break;
		case TRANSITIVE_TRIANGLES:
			overestimationOfDivergenceOfTransitiveTriangles = value;
			break;
		case CYCLIC_TRIANGLES:
			overestimationOfDivergenceOfCyclicTriangles = value;
			break;
		}
	}
	
	
	public void setSquareDivergence (Indicator indicator, double value){
		
		switch (indicator){
		case CONCENTRATION:
			squareDivergenceOfConcentration = value;
			break;
		case SYMMETRY:
			squareDivergenceOfSymmetry = value;
			break;
		case ENDOGAMIC_CONCENTRATION:
			squareDivergenceOfEndogamicConcentration = value;
			break;
		case LOOPS:
			squareDivergenceOfLoops = value;
			break;
		case DUAL_CIRCUITS:
			squareDivergenceOfCircuits = value;
			break;
		case PARALLEL_CIRCUITS:
			squareDivergenceOfParallelCircuits = value;
			break;
		case CROSS_CIRCUITS:
			squareDivergenceOfCrossCircuits = value;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			squareDivergenceOfBalanceOfParallelVsCrossCircuits = value;
			break;
		case TRIANGLES:
			squareDivergenceOfTriangles = value;
			break;
		case TRANSITIVE_TRIANGLES:
			squareDivergenceOfTransitiveTriangles = value;
			break;
		case CYCLIC_TRIANGLES:
			squareDivergenceOfCyclicTriangles = value;
			break;
		}

	}
	

	public void setDivergenceNormalized (Indicator indicator, double value){
		
		switch (indicator){
		case LOOPS:
			divergenceOfNormalizedLoops = value;
			break;
		case DUAL_CIRCUITS:
			divergenceOfNormalizedCircuits = value;
			break;
		case PARALLEL_CIRCUITS:
			divergenceOfNormalizedParallelCircuits = value;
			break;
		case CROSS_CIRCUITS:
			divergenceOfNormalizedCrossCircuits = value;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			divergenceOfNormalizedBalanceOfParallelVsCrossCircuits = value;
			break;
		case TRIANGLES:
			divergenceOfNormalizedTriangles = value;
			break;
		case TRANSITIVE_TRIANGLES:
			divergenceOfNormalizedTransitiveTriangles = value;
			break;
		case CYCLIC_TRIANGLES:
			divergenceOfNormalizedCyclicTriangles = value;
			break;
		}
		
	}

	public double getNormalizedNumber (Indicator indicator){
		double result;
		
		result = 0.;
		switch (indicator){
		case LOOPS:
			result = endogamyIndex;
			break;
		case DUAL_CIRCUITS:
			result = normalizedNumberOfCircuits;
			break;
		case PARALLEL_CIRCUITS:
			result = normalizedNumberOfParallelCircuits;
			break;
		case CROSS_CIRCUITS:
			result = normalizedNumberOfCrossCircuits;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			result = normalizedBalanceOfParallelVsCrossCircuits;
			break;
		case TRIANGLES:
			result = normalizedNumberOfTriangles;
			break;
		case TRANSITIVE_TRIANGLES:
			result = normalizedNumberOfTransitiveTriangles;
			break;
		case CYCLIC_TRIANGLES:
			result = normalizedNumberOfCyclicTriangles;
			break;
		}
		//
		return result;
	}
	
	
	public double getExpectedNumber (Indicator indicator){
		double result;
		
		result = 0.;
		switch (indicator){
		case CONCENTRATION:
			result = expectedConcentrationIndex;
			break;
		case SYMMETRY:
			result = expectedSymmetryIndex;
			break;
		case ENDOGAMIC_CONCENTRATION:
			result = expectedEndogamicConcentrationIndex;
			break;
		case LOOPS:
			result = expectedNumberOfLoops;
			break;
		case DUAL_CIRCUITS:
			result = expectedNumberOfCircuits;
			break;
		case PARALLEL_CIRCUITS:
			result = expectedNumberOfParallelCircuits;
			break;
		case CROSS_CIRCUITS:
			result = expectedNumberOfCrossCircuits;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			result = expectedBalanceOfParallelVsCrossCircuits;
			break;
		case TRIANGLES:
			result = expectedNumberOfTriangles;
			break;
		case TRANSITIVE_TRIANGLES:
			result = expectedNumberOfTransitiveTriangles;
			break;
		case CYCLIC_TRIANGLES:
			result = expectedNumberOfCyclicTriangles;
			break;
		}
		//
		return result;
	}
	
	public double getExpectedNormalizedNumber (Indicator indicator){
		double result;
		
		result = 0.;
		switch (indicator){
		case LOOPS:
			result = expectedEndogamyIndex;
			break;
		case DUAL_CIRCUITS:
			result = expectedNormalizedNumberOfCircuits;
			break;
		case PARALLEL_CIRCUITS:
			result = expectedNormalizedNumberOfParallelCircuits;
			break;
		case CROSS_CIRCUITS:
			result = expectedNormalizedNumberOfCrossCircuits;
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			result = expectedNormalizedBalanceOfParallelVsCrossCircuits;
			break;
		case TRIANGLES:
			result = expectedNormalizedNumberOfTriangles;
			break;
		case TRANSITIVE_TRIANGLES:
			result = expectedNormalizedNumberOfTransitiveTriangles;
			break;
		case CYCLIC_TRIANGLES:
			result = expectedNormalizedNumberOfCyclicTriangles;
			break;
		}
		//
		return result;
	}
	
	private static String lineTitle (Indicator indicator){
		String result;
		
		result = "";
		switch (indicator){
		case CONCENTRATION:
			result = "concentration";
			break;
		case ENDOGAMIC_CONCENTRATION:
			result = "endogamic concentration";
			break;
		case SYMMETRY:
			result = "symmetry";
			break;
		case STRENGTH_CONCENTRATION:
			result = "strength concentration";
			break;
		case ENDOGAMIC_STRENGTH_CONCENTRATION:
			result = "endogamic strength concentration";
			break;
		case STRENGTH_SYMMETRY:
			result = "strength symmetry";
			break;
		case LOOPS:
			result = "loops";
			break;
		case DUAL_CIRCUITS:
			result = "dual circuits";
			break;
		case PARALLEL_CIRCUITS:
			result = "parallel circuits";
			break;
		case CROSS_CIRCUITS:
			result = "cross circuits";
			break;
		case BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS:
			result = "balance parallel-cross circuits";
			break;
		case TRIANGLES:
			result = "triangles";
			break;
		case TRANSITIVE_TRIANGLES:
			result = "transitive triangles";
			break;
		case CYCLIC_TRIANGLES:
			result = "cyclic triangles";
			break;
		}
		//
		return result;
	}
	
	public double getStrengthProductSum() {
		return strengthProductSum;
	}

	public double getEndogamousClosureRate() {
		return endogamousClosureRate;
	}


	
	public static String statisticsLineRandom (Indicator indicator, Mode mode, MatrixStatistics randStats, MatrixStatistics sourceStats){
		String result;

		if (mode == Mode.SIMPLE){
			result = lineTitle(indicator)+"\t"+
					randStats.getNumber(indicator)+"\t"+
					randStats.getNormalizedNumber(indicator)+"\t";
			if (sourceStats!=null){
				result += 
					sourceStats.getNumber(indicator)+"\t"+
					sourceStats.getNormalizedNumber(indicator)+"\t"+
					randStats.getOverestimation(indicator)+"\t";
			}
		} else if (mode == Mode.EXPECTED){
			result = lineTitle(indicator)+"\t"+
					randStats.getExpectedNumber(indicator)+"\t"+
					randStats.getExpectedNormalizedNumber(indicator)+"\t";
			if (sourceStats!=null){
				result += 
					sourceStats.getExpectedNumber(indicator)+"\t"+
					sourceStats.getExpectedNormalizedNumber(indicator)+"\t";
//					stats1.getOverestimationExpected(indicator)+"\t";
			}
		} else{
			result = "";
		}
		
		//
		return result;
		
	}
	
	public static String statisticsLine(Indicator indicator, Mode mode, MatrixStatistics sourceStats, MatrixStatistics randStats){
		String result;
		
		double alpha = 0.1;
		
		if (mode == Mode.SIMPLE){
			result = lineTitle(indicator)+"\t"+
					sourceStats.getNumber(indicator)+"\t"+
					sourceStats.getExpectedNumber(indicator)+"\t"+
					sourceStats.getDivergence(indicator)+"\t";
			if (randStats!=null){
				result += 
					randStats.getNumber(indicator)+"\t"+
					randStats.getExpectedNumber(indicator)+"\t"+
					randStats.getDivergence(indicator)+"\t"+
				Arrays.toString(randStats.getConfidenceInterval(indicator,alpha))+"\t";
				
				if (isInConfidenceInterval(sourceStats,randStats,indicator,alpha)){
					result += "GREEN\t";
				} else {
					result += "RED\t";
				}
				
			}
		} else if (mode == Mode.NORMALIZED){
			result = lineTitle(indicator)+"\t"+
					sourceStats.getNormalizedNumber(indicator)+"\t"+
					sourceStats.getExpectedNormalizedNumber(indicator)+"\t"+
					sourceStats.getDivergenceNormalized(indicator)+"\t";
			if (randStats!=null){
				result += 
					randStats.getNormalizedNumber(indicator)+"\t"+
					randStats.getExpectedNormalizedNumber(indicator)+"\t"+
					randStats.getDivergenceNormalized(indicator)+"\t";
			}
		} else{
			result = "";
		}
		
		
		//
		return result;
	}
	
	private static boolean isInConfidenceInterval (MatrixStatistics sourceStats, MatrixStatistics randStats, Indicator indicator, double alpha){
		boolean result;
		
		double[] interval = randStats.getConfidenceInterval(indicator, alpha);
		double value = sourceStats.getDivergence(indicator);
		
		result = (interval[0]<=value && value<=interval[1]);
		
		//
		return result;
	}
	
	private double[] getConfidenceInterval(Indicator indicator, double alpha){
		double[] result;
		
		double c = StudentDist.inverseF(runs-1,1-(alpha/2.));
		double s = getSquareDivergence(indicator)/Math.pow(runs, 0.5);
		double m = getDivergence(indicator);

		result = new double[]{m-c*s,m+c*s};
		
		//
		return result;
	}
	
	public static StringList statistics(MatrixStatistics sourceStats, MatrixStatistics randStats){
		StringList result;
		
		result = new StringList(100);
		
		result.appendln("number of nodes:\t" + sourceStats.getNodeCount());
		result.appendln("number of arcs:\t" + sourceStats.getSum());
		result.appendln("maximal number of circuits:\t" + sourceStats.getMaximalNumberOfCircuits());
		result.appendln("maximal weight (marriages per arc):\t"+sourceStats.getNumber(Indicator.MAX_WEIGHT));
		result.appendln("maximal strength (marriages per node):\t"+sourceStats.getNumber(Indicator.MAX_STRENGTH));
		result.appendln("potential endogamic pairs:\t" + sourceStats.getStrengthProductSum()+"\tEndogamic closure rate:\t"+sourceStats.getEndogamousClosureRate());
		result.appendln();

		result.appendln("Strength distribution (marriages by clusters)");
		result.appendln("\tobserved\t\t\trd mean observed\t\t");
		result.appendln();
		result.appendln(statisticsLine(Indicator.STRENGTH_CONCENTRATION,Mode.SIMPLE,sourceStats,randStats));
		result.appendln(statisticsLine(Indicator.ENDOGAMIC_STRENGTH_CONCENTRATION,Mode.SIMPLE,sourceStats,randStats));
		result.appendln(statisticsLine(Indicator.STRENGTH_SYMMETRY,Mode.SIMPLE,sourceStats,randStats));
		result.appendln();
		
		result.appendln("Weight distribution (marriages by cluster pairs)");
		result.appendln("\tobserved\texpected\tdivergence\trd mean observed\trd mean expected\trd divergence mean\trd divergence ci (0.1)");
		result.appendln();
		result.appendln(statisticsLine(Indicator.CONCENTRATION,Mode.SIMPLE,sourceStats,randStats));
		result.appendln(statisticsLine(Indicator.ENDOGAMIC_CONCENTRATION,Mode.SIMPLE,sourceStats,randStats));
		result.appendln(statisticsLine(Indicator.SYMMETRY,Mode.SIMPLE,sourceStats,randStats));
		result.appendln();
		
		result.appendln("Circuit census");
		result.appendln("\tobserved\texpected\tdivergence\trd mean observed\trd mean expected\trd divergence mean\trd divergence ci (0.1)");
		result.appendln();
		result.appendln(statisticsLine(Indicator.LOOPS,Mode.SIMPLE,sourceStats,randStats));
		result.appendln(statisticsLine(Indicator.DUAL_CIRCUITS,Mode.SIMPLE,sourceStats,randStats));
		result.appendln(statisticsLine(Indicator.PARALLEL_CIRCUITS,Mode.SIMPLE,sourceStats,randStats));
		result.appendln(statisticsLine(Indicator.CROSS_CIRCUITS,Mode.SIMPLE,sourceStats,randStats));
		result.appendln(statisticsLine(Indicator.BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS,Mode.SIMPLE,sourceStats,randStats));
		result.appendln(statisticsLine(Indicator.TRIANGLES,Mode.SIMPLE,sourceStats,randStats));
		result.appendln(statisticsLine(Indicator.TRANSITIVE_TRIANGLES,Mode.SIMPLE,sourceStats,randStats));
		result.appendln(statisticsLine(Indicator.CYCLIC_TRIANGLES,Mode.SIMPLE,sourceStats,randStats));
		result.appendln();

		result.appendln("Circuit census (normalized)");
		result.appendln("\tobserved\texpected\tdivergence\trd mean observed\trd mean expected\trd mean divergence");
		result.appendln();
		result.appendln(statisticsLine(Indicator.LOOPS,Mode.NORMALIZED,sourceStats,randStats));
		result.appendln(statisticsLine(Indicator.DUAL_CIRCUITS,Mode.NORMALIZED,sourceStats,randStats));
		result.appendln(statisticsLine(Indicator.PARALLEL_CIRCUITS,Mode.NORMALIZED,sourceStats,randStats));
		result.appendln(statisticsLine(Indicator.CROSS_CIRCUITS,Mode.NORMALIZED,sourceStats,randStats));
		result.appendln(statisticsLine(Indicator.BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS,Mode.NORMALIZED,sourceStats,randStats));
		result.appendln(statisticsLine(Indicator.TRIANGLES,Mode.NORMALIZED,sourceStats,randStats));
		result.appendln(statisticsLine(Indicator.TRANSITIVE_TRIANGLES,Mode.NORMALIZED,sourceStats,randStats));
		result.appendln(statisticsLine(Indicator.CYCLIC_TRIANGLES,Mode.NORMALIZED,sourceStats,randStats));
		result.appendln();

		
		//
		return result;
	}
	
	
	public static StringList statisticsRandom(MatrixStatistics randStats, MatrixStatistics sourceStats){
		StringList result;
		
		result = new StringList(100);
		
		randStats.compareValues(sourceStats);
		
		result.appendln("mean number of nodes:\t" + randStats.getNodeCount());
		result.appendln("mean number of arcs:\t" + randStats.getSum());
		result.appendln("mean maximal number of circuits:\t" + randStats.getMaximalNumberOfCircuits());
		result.appendln("maximal weight (marriages per arc):\t"+randStats.getNumber(Indicator.MAX_WEIGHT));
		result.appendln("maximal strength (marriages per node):\t"+randStats.getNumber(Indicator.MAX_STRENGTH));
		result.appendln();

		result.appendln("Strength distribution (marriages by clusters)");
		result.appendln("\tmean\t\torig\t\toverestimation");
		result.appendln();
		result.appendln(statisticsLineRandom(Indicator.STRENGTH_CONCENTRATION,Mode.SIMPLE,randStats,sourceStats));
		result.appendln(statisticsLineRandom(Indicator.ENDOGAMIC_STRENGTH_CONCENTRATION,Mode.SIMPLE,randStats,sourceStats));
		result.appendln(statisticsLineRandom(Indicator.STRENGTH_SYMMETRY,Mode.SIMPLE,randStats,sourceStats));
		result.appendln();
		
		result.appendln("Weight distribution (marriages by cluster pairs)");
		result.appendln("\tmean\t\torig\t\toverestimation");
		result.appendln();
		result.appendln(statisticsLineRandom(Indicator.CONCENTRATION,Mode.SIMPLE,randStats,sourceStats));
		result.appendln(statisticsLineRandom(Indicator.ENDOGAMIC_CONCENTRATION,Mode.SIMPLE,randStats,sourceStats));
		result.appendln(statisticsLineRandom(Indicator.SYMMETRY,Mode.SIMPLE,randStats,sourceStats));
		result.appendln();
		
		result.appendln("Circuit census");
		result.appendln("\tmean\tnormalized mean\torig\torig normalized\toverestimation");
		result.appendln();
		result.appendln(statisticsLineRandom(Indicator.LOOPS,Mode.SIMPLE,randStats,sourceStats));
		result.appendln(statisticsLineRandom(Indicator.DUAL_CIRCUITS,Mode.SIMPLE,randStats,sourceStats));
		result.appendln(statisticsLineRandom(Indicator.PARALLEL_CIRCUITS,Mode.SIMPLE,randStats,sourceStats));
		result.appendln(statisticsLineRandom(Indicator.CROSS_CIRCUITS,Mode.SIMPLE,randStats,sourceStats));
		result.appendln(statisticsLineRandom(Indicator.BALANCE_OF_PARALLEL_VS_CROSS_CIRCUITS,Mode.SIMPLE,randStats,sourceStats));
		result.appendln(statisticsLineRandom(Indicator.TRIANGLES,Mode.SIMPLE,randStats,sourceStats));
		result.appendln(statisticsLineRandom(Indicator.TRANSITIVE_TRIANGLES,Mode.SIMPLE,randStats,sourceStats));
		result.appendln(statisticsLineRandom(Indicator.CYCLIC_TRIANGLES,Mode.SIMPLE,randStats,sourceStats));
		result.appendln();

		
		//
		return result;
	}	

	public double getNodeCount() {
		return nodeCount;
	}
	
/*	public StringList reportValues (MatrixStatistics sourceStats){
		StringList result;

		result = new StringList(100);
		result.appendln("mean Number of arcs\t"+arcCount);
		result.appendln();
		result.appendln("\tObserved\tExpected (Multinomial)");
		result.appendln("mean Number of loops\t"+numberOfLoops+"\t"+expectedNumberOfLoops);
		result.appendln("mean Number of circuits\t"+numberOfCircuits+"\t"+expectedNumberOfCircuits);
		result.appendln("mean Number of parallel circuits\t"+numberOfParallelCircuits+"\t"+expectedNumberOfParallelCircuits);
		result.appendln("mean Number of cross circuits\t"+numberOfCrossCircuits+"\t"+expectedNumberOfCrossCircuits);
		result.appendln("mean Balance of parallel vs cross circuits\t"+balanceOfParallelVsCrossCircuits+"\t"+expectedBalanceOfParallelVsCrossCircuits);
		result.appendln("mean Number of triangles\t"+numberOfTriangles+"\t"+expectedNumberOfTriangles);
		result.appendln("mean Number of cyclic triangles\t"+numberOfCyclicTriangles+"\t"+expectedNumberOfCyclicTriangles);
		result.appendln("mean Number of transitive triangles\t"+numberOfTransitiveTriangles+"\t"+expectedNumberOfTransitiveTriangles);
		result.appendln();
		result.appendln("mean Normalized Number of loops\t"+endogamyIndex+"\t"+expectedEndogamyIndex);
		result.appendln("mean Normalized Number of circuits\t"+normalizedNumberOfCircuits+"\t"+expectedNormalizedNumberOfCircuits);
		result.appendln("mean Normalized Number of triangles\t"+normalizedNumberOfTriangles+"\t"+expectedNormalizedNumberOfTriangles);
		result.appendln();
		result.appendln("mean Concentration index\t"+concentrationIndex+"\t"+expectedConcentrationIndex);
		result.appendln("mean Symmetry index\t"+symmetryIndex+"\t"+expectedSymmetryIndex);
		result.appendln("mean Endogamic Concentration index\t"+endogamicConcentrationIndex+"\t"+expectedEndogamicConcentrationIndex);
		result.appendln();
		if (sourceStats!=null){
			
			compareValues(sourceStats);

			result.appendln("overestimation of loops\t"+overestimationOfLoops);
			result.appendln("overestimation of circuits\t"+overestimationOfCircuits);
			result.appendln("overestimation of triangles\t"+overestimationOfTriangles);
			result.appendln("overestimation of concentration\t"+overestimationOfConcentration);
			result.appendln("overestimation of symmetry\t"+overestimationOfSymmetry);
		}
		//
		return result;
	}*/




	

}	