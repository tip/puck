package org.tip.puck.report;

/**
 * 
 * @author TIP
 */
public class XYData {

	private Double x;
	private Double y;

	/**
	 * 
	 */
	public XYData() {
		this.x = null;
		this.y = null;
	}

	/**
	 * 
	 * @param y
	 */
	public XYData(final Double y) {
		this.x = null;
		this.y = y;
	}

	/**
	 * 
	 * @param x
	 */
	public XYData(final Double x, final Double y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * 
	 * @return
	 */
	public Double getXValue() {
		Double result;

		result = this.x;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Double getYValue() {
		Double result;

		result = this.y;

		//
		return result;
	}

	/**
	 * 
	 * @param x
	 * @param y
	 */
	public void set(final Double x, final Double y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * 
	 * @param y
	 */
	public void setXValue(final Double x) {
		this.x = x;
	}

	/**
	 * 
	 * @param y
	 */
	public void setYValue(final Double y) {
		this.y = y;
	}
}
