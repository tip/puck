package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Collections;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.workers.TransmitAttributeValueCriteria;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class TransmitAttributeValueInputDialog extends JDialog {

	private static final long serialVersionUID = 5815549753188795132L;
	private final JPanel contentPanel = new JPanel();
	private TransmitAttributeValueCriteria dialogCriteria;
	private static TransmitAttributeValueCriteria lastCriteria = new TransmitAttributeValueCriteria();
	private JComboBox cmbbxAttributeLabel;
	private JRadioButton rdbtnAgnatic;
	private final ButtonGroup buttonGroupFiliationType = new ButtonGroup();
	private JRadioButton rdbtnCognatic;
	private JRadioButton rdbtnUterine;
	private JSpinner spnrMaxGenerations;

	/**
	 * Create the dialog.
	 */
	public TransmitAttributeValueInputDialog(final List<String> labels) {
		super();

		Collections.sort(labels);

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Transmit Attribute Value Input");
		setIconImage(Toolkit.getDefaultToolkit().getImage(TransmitAttributeValueInputDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				TransmitAttributeValueInputDialog.this.dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 420, 225);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));
		{
			JLabel lblTarget = new JLabel("Label:");
			this.contentPanel.add(lblTarget, "2, 2, right, default");
		}
		{
			this.cmbbxAttributeLabel = new JComboBox(labels.toArray());
			this.contentPanel.add(this.cmbbxAttributeLabel, "4, 2, fill, default");
		}
		{
			JLabel lblLabel = new JLabel("FiliationType:");
			this.contentPanel.add(lblLabel, "2, 4, right, default");
		}
		{
			JPanel panel = new JPanel();
			this.contentPanel.add(panel, "4, 4, fill, fill");
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			{
				this.rdbtnAgnatic = new JRadioButton("Agnatic");
				this.buttonGroupFiliationType.add(this.rdbtnAgnatic);
				panel.add(this.rdbtnAgnatic);
			}
			{
				this.rdbtnCognatic = new JRadioButton("Cognatic");
				this.buttonGroupFiliationType.add(this.rdbtnCognatic);
				panel.add(this.rdbtnCognatic);
			}
			{
				this.rdbtnUterine = new JRadioButton("Uterine");
				this.buttonGroupFiliationType.add(this.rdbtnUterine);
				panel.add(this.rdbtnUterine);
			}
		}
		{
			JLabel lblNewLabel = new JLabel("<html>Max generations:<br/>(0 = no limit)</html>");
			this.contentPanel.add(lblNewLabel, "2, 6, right, default");
		}
		{
			this.spnrMaxGenerations = new JSpinner();
			this.spnrMaxGenerations.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
			this.contentPanel.add(this.spnrMaxGenerations, "4, 6");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						TransmitAttributeValueInputDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("Transmit");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						TransmitAttributeValueCriteria criteria = getCriteria();
						if (StringUtils.isBlank(criteria.getAttributeLabel())) {
							//
							String title = "Invalid input";
							String message = "Please, enter an attribute label not empty.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

						} else {
							//
							lastCriteria = criteria;
							TransmitAttributeValueInputDialog.this.dialogCriteria = criteria;

							//
							setVisible(false);
						}

					}
				});
				{
					JButton btnDefault = new JButton("Reset");
					btnDefault.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(final ActionEvent e) {
							// Button "Default".
							setCriteria(new TransmitAttributeValueCriteria());
						}
					});
					{
						Component horizontalStrut = Box.createHorizontalStrut(20);
						buttonPane.add(horizontalStrut);
					}
					buttonPane.add(btnDefault);
				}
				{
					Component horizontalStrut = Box.createHorizontalStrut(20);
					buttonPane.add(horizontalStrut);
				}
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}

		// ////////////////////////
		setCriteria(lastCriteria);
	}

	/**
	 * 
	 * @return
	 */
	public TransmitAttributeValueCriteria getCriteria() {
		TransmitAttributeValueCriteria result;

		result = new TransmitAttributeValueCriteria();

		//
		result.setAttributeLabel((String) this.cmbbxAttributeLabel.getSelectedItem());

		//
		FiliationType filiationType;
		if (this.rdbtnAgnatic.isSelected()) {
			//
			filiationType = FiliationType.AGNATIC;

		} else if (this.rdbtnCognatic.isSelected()) {
			//
			filiationType = FiliationType.COGNATIC;

		} else if (this.rdbtnUterine.isSelected()) {
			//
			filiationType = FiliationType.UTERINE;

		} else {
			//
			filiationType = null;
		}
		result.setFiliationType(filiationType);

		//
		Integer max;
		if ((this.spnrMaxGenerations.getValue() == null) || ((Integer) this.spnrMaxGenerations.getValue() == 0)) {
			//
			max = null;

		} else {
			//
			max = (Integer) this.spnrMaxGenerations.getValue();
		}
		result.setMaxGenerations(max);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public TransmitAttributeValueCriteria getDialogCriteria() {
		TransmitAttributeValueCriteria result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final TransmitAttributeValueCriteria source) {
		//
		if (source != null) {

			//
			this.cmbbxAttributeLabel.setSelectedItem(source.getAttributeLabel());

			//
			switch (source.getFiliationType()) {
				case AGNATIC:
					this.rdbtnAgnatic.setSelected(true);
					this.rdbtnCognatic.setSelected(false);
					this.rdbtnUterine.setSelected(false);
				break;

				case COGNATIC:
					this.rdbtnAgnatic.setSelected(false);
					this.rdbtnCognatic.setSelected(false);
					this.rdbtnUterine.setSelected(true);
				break;

				case UTERINE:
					this.rdbtnAgnatic.setSelected(false);
					this.rdbtnCognatic.setSelected(false);
					this.rdbtnUterine.setSelected(true);
				break;

				default:
					this.rdbtnAgnatic.setSelected(false);
					this.rdbtnCognatic.setSelected(false);
					this.rdbtnUterine.setSelected(false);
				break;
			}

			//
			if (source.getMaxGenerations() == null) {
				//
				this.spnrMaxGenerations.setValue(new Integer(0));

			} else {
				//
				this.spnrMaxGenerations.setValue(source.getMaxGenerations());
			}
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		/* TransmitAttributeValueCriteria criteria = */showDialog(null);
	}

	/**
	 * Launch the application.
	 */
	public static TransmitAttributeValueCriteria showDialog(final List<String> labels) {
		TransmitAttributeValueCriteria result;

		//
		TransmitAttributeValueInputDialog dialog = new TransmitAttributeValueInputDialog(labels);
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
