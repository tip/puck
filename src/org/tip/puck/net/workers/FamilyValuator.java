package org.tip.puck.net.workers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.tip.puck.geo.Geography;
import org.tip.puck.geo.Place;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Families;
import org.tip.puck.net.Family;
import org.tip.puck.util.NumberedValues;
import org.tip.puck.util.Value;

/**
 * 
 * @author TIP
 */
public class FamilyValuator {

	public enum EndogenousLabel {
		CHILDCOUNT,
		FATHERNAME,
		HASFATHER,
		HASMOTHER,
		HUSBAND_ORDER,
		ID,
		ISFERTILE,
		ISDIVORCED,
		ISMARRIED,
		MOTHERNAME,
		PARENTCOUNT,
		UNIONSTATUS,
		WIFE_ORDER,
		DISTANCE
	}

	public static final Pattern YEAR_PATTERN = Pattern.compile("(\\d\\d\\d\\d)");

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String extractYear(final String source) {
		String result;

		if (source == null) {
			result = null;
		} else {
			try {
				result = Integer.parseInt(source) + "";
			} catch (NumberFormatException nfe) {
				Matcher matcher = YEAR_PATTERN.matcher(source);
				if ((matcher.find()) && (matcher.groupCount() > 0)) {
					//
					result = matcher.group(1);
				} else {
					result = null;
				}
			}
		}

		//
		return result;
	}

	/**
	 * This method is a helper one.
	 * 
	 * @param individual
	 * @param label
	 * @return
	 */
	public static NumberedValues get(final Families source, final String label) {
		NumberedValues result;

		result = get(source, label, null, null);

		//
		return result;
	}

	/**
	 * 
	 * @param individual
	 * @param label
	 * @return
	 */
	public static NumberedValues get(final Families source, final String label, final Object parameter, final Geography geography) {
		NumberedValues result;

		//
		result = new NumberedValues();

		for (Family family : source) {
			result.put(family.getId(), get(family, label, parameter, geography));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param individual
	 * @param label
	 * @return
	 */
	public static Value get(final Family source, final String label, final Geography geography) {
		Value result;

		result = get(source, label, null, geography);

		//
		return result;
	}

	/**
	 * 
	 * @param individual
	 * @param label
	 * 
	 * @return
	 */
	public static Value get(final Family source, final String label, final Object parameter, final Geography geography) {
		Value result;

		//
		EndogenousLabel enogenousLabel;
		try {
			enogenousLabel = EndogenousLabel.valueOf(label.replace(" ", "_"));
		} catch (IllegalArgumentException exception) {
			enogenousLabel = null;
		}

		if (enogenousLabel == null) {
			String attributeValue = source.getAttributeValue(label);

			if (attributeValue == null) {
				result = null;
			} else {
				if (label.toUpperCase().contains("YEAR")) {
					attributeValue = source.getAttributeValue(label.replaceAll("DATE", "YEAR"));
					String year = extractYear(attributeValue);
					if (year == null) {
						result = null;
					} else {
						result = new Value(Integer.parseInt(year));
					}
				} else {
					result = new Value(attributeValue);
				}
			}
		} else {
			switch (enogenousLabel) {
				case CHILDCOUNT:
					result = new Value(source.getChildren().size());
				break;
				case FATHERNAME:
					if (source.getFather() == null) {
						result = null;
					} else {
						result = new Value(source.getFather().getName());
					}
				break;
				case HASFATHER: {
					if (source.getFather() == null) {
						result = new Value("WITH FATHER");
					} else {
						result = new Value("WITHOUT FATHER");
					}
				}
				break;
				case HASMOTHER: {
					if (source.getMother() == null) {
						result = new Value("WITH MOTHER");
					} else {
						result = new Value("WITHOUT MOTHER");
					}
				}
				break;
				case HUSBAND_ORDER: {
					if (source.getHusbandOrder() == null) {
						result = null;
					} else {
						result = new Value(source.getHusbandOrder());
					}
				}
				break;
				case ID:
					result = new Value(source.getId());
				break;
				case ISFERTILE:
					if (source.isFertile()) {
						result = new Value("FERTILE");
					} else {
						result = new Value("INFERTILE");
					}
				break;
				case ISDIVORCED:
					if (source.hasMarried()) {
						result = new Value("DIVORCED");
					} else {
						result = new Value("NOT DIVORCED");
					}
				break;
				case ISMARRIED:
					if (source.hasMarried()) {
						result = new Value("MARRIED");
					} else {
						result = new Value("NOT MARRIED");
					}
				break;
				case MOTHERNAME:
					if (source.getMother() == null) {
						result = null;
					} else {
						result = new Value(source.getMother().getName());
					}
				break;
				case PARENTCOUNT: {
					//
					int count = 0;
					if (source.getFather() != null) {
						count += 1;
					}
					if (source.getMother() != null) {
						count += 1;
					}

					//
					result = new Value(count);
				}
				break;
				case UNIONSTATUS:
					result = new Value(source.getUnionStatus());
				break;
				case WIFE_ORDER: {
					if (source.getWifeOrder() == null) {
						result = null;
					} else {
						result = new Value(source.getWifeOrder());
					}
				}
				break;
				case DISTANCE: 
					
					//* @param unit = 'M' is statute miles, 'K' is kilometers (default), 'N' is nautical miles  
					result = null;

					if (source.getHusband()!=null && source.getWife()!=null){
						
						Value husbandPlaceName = IndividualValuator.get(source.getHusband(), parameter.toString(), geography);
						Value wifePlaceName = IndividualValuator.get(source.getWife(), parameter.toString(), geography);
						
						if (husbandPlaceName!=null && wifePlaceName!=null){
							result = Value.valueOf(geography.getDistance(husbandPlaceName.stringValue(), wifePlaceName.stringValue(), 'K'));
						}
					}
					
				break;
				default:
					result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param individuals
	 * @return
	 */
	public static List<String> getAttributeLabels(final Families source) {
		List<String> result;

		result = getAttributeLabels(source, null);

		//
		return result;
	}

	/**
	 * @param individuals
	 * @return
	 */
	public static List<String> getAttributeLabels(final Families source, final Integer limit) {
		List<String> result;

		//
		result = new ArrayList<String>(20);

		//
		for (EndogenousLabel label : EndogenousLabel.values()) {
			result.add(label.toString());
		}

		//
		result.addAll(getExogenousAttributeLabels(source, limit));

		//
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * 
	 * @param individuals
	 * @return
	 */
	public static List<String> getAttributeLabelSample(final Families source) {
		List<String> result;

		result = getAttributeLabels(source, 10000);

		//
		return result;
	}

	/**
	 * @param individuals
	 * @return
	 */
	public static List<String> getExogenousAttributeLabels(final Families source) {
		List<String> result;

		//
		result = getExogenousAttributeLabels(source, null);

		//
		return result;
	}

	/**
	 * @param individuals
	 * @return
	 */
	public static List<String> getExogenousAttributeLabels(final Families source, final Integer limit) {
		List<String> result;

		//
		result = new ArrayList<String>(20);

		//
		if (source != null) {

			//
			HashSet<String> buffer = new HashSet<String>();
			int index = 0;
			Iterator<Family> iterator = source.iterator();
			while ((iterator.hasNext()) && ((limit == null) || (index < limit))) {
				Family family = iterator.next();
				for (Attribute attribute : family.attributes()) {
					buffer.add(attribute.getLabel());
				}
				index += 1;
			}

			//
			for (String string : buffer) {
				result.add(string);
			}

			//
			Collections.sort(result);
		}

		//
		return result;
	}
	
	public static Integer getMarrYear(final Family family) {
		Integer result;

		if (family == null) {
			result = null;
		} else {
			String marrYear = extractYear(family.getAttributeValue("MARR_DATE"));
			if (marrYear == null) {
				result = null;
			} else {
				result = Integer.parseInt(marrYear);
			}
		}

		//
		return result;
	}

	public static Integer getDivYear(final Family family) {
		Integer result;

		if (family == null) {
			result = null;
		} else {
			String divYear = extractYear(family.getAttributeValue("DIV_DATE"));
			if (divYear == null) {
				result = null;
			} else {
				result = Integer.parseInt(divYear);
			}
		}

		//
		return result;
	}


	public static Double getSpouseDistance(final Family family, final NumberedValues data) {
		Double result;

		result = null;

		if (family.getHusband() != null && family.getWife() != null) {
			Value husbandValue = data.get(family.getHusband().getId());
			Value wifeValue = data.get(family.getWife().getId());

			if (husbandValue != null && wifeValue != null && husbandValue.isNumber() && wifeValue.isNumber()) {
				result = husbandValue.doubleValue() - wifeValue.doubleValue();
			}
		}

		//
		return result;
	}

	public static Double getSpouseDistance(final Family family, final String label, final Geography geography) {
		Double result;

		result = null;

		if (family.getHusband() != null && family.getWife() != null) {
			Value husbandValue = IndividualValuator.get(family.getHusband(), label, geography);
			Value wifeValue = IndividualValuator.get(family.getWife(), label, geography);

			if (husbandValue != null && wifeValue != null && husbandValue.isNumber() && wifeValue.isNumber()) {
				result = husbandValue.doubleValue() - wifeValue.doubleValue();
			}
		}

		//
		return result;
	}

}
