package org.tip.puck.graphs.onemode;

import org.tip.puck.graphs.onemode.OMGraph.GraphMode;

public class ShuffleCriteria {

	 int switchesPerIteration;
	 int maxGenerationalDistance;
	 double minShufflePercentage;
	 int minStableIterations;
	 GraphMode mode;
	 
	 public ShuffleCriteria (){
		 switchesPerIteration = 2;
		 maxGenerationalDistance = 2;
		 minShufflePercentage = 100;
		 minStableIterations = 1000;
		 mode = GraphMode.PGRAPH;
	 }

	public int getSwitchesPerIteration() {
		return switchesPerIteration;
	}

	public void setSwitchesPerIteration(int switchesPerIteration) {
		this.switchesPerIteration = switchesPerIteration;
	}

	public int getMaxGenerationalDistance() {
		return maxGenerationalDistance;
	}

	public void setMaxGenerationalDistance(int maxGenerationalDistance) {
		this.maxGenerationalDistance = maxGenerationalDistance;
	}

	public double getMinShufflePercentage() {
		return minShufflePercentage;
	}

	public void setMinShufflePercentage(double minShufflePercentage) {
		this.minShufflePercentage = minShufflePercentage;
	}

	public int getMinStableIterations() {
		return minStableIterations;
	}

	public void setMinStableIterations(int minStableIterations) {
		this.minStableIterations = minStableIterations;
	}

	public GraphMode getMode() {
		return mode;
	}

	public void setMode(GraphMode mode) {
		this.mode = mode;
	}
		
		
}
