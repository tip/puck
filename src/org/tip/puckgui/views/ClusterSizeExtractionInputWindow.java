package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.Net;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.segmentation.Segment;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.PuckGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class ClusterSizeExtractionInputWindow extends JFrame {
	
	private static final long serialVersionUID = -1671417115241702229L;
	private static final Logger logger = LoggerFactory.getLogger(ClusterSizeExtractionInputWindow.class);

	private JFrame thisJFrame;
	private JPanel contentPane;
	private static int lastMinimalValue = 1;
	private JSpinner spinnerMinimalNumberOfElements;

	/**
	 * Create the frame.
	 */
	public ClusterSizeExtractionInputWindow(final NetGUI netGUI) {
		// /////////////////////////////////
		setIconImage(Toolkit.getDefaultToolkit().getImage(ClusterSizeExtractionInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		setTitle("Extract Large Clusters");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 365, 175);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		this.contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));

		JLabel lblMinimalNumberOf = new JLabel("<html><div style=\"text-align:right\">Minimal number:<br/>of membersByRelationId </div></html>");
		panel.add(lblMinimalNumberOf, "2, 4, center, default");

		this.spinnerMinimalNumberOfElements = new JSpinner();
		this.spinnerMinimalNumberOfElements.setModel(new SpinnerNumberModel(new Integer(2), new Integer(0), null, new Integer(1)));
		panel.add(this.spinnerMinimalNumberOfElements, "4, 4");

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		getRootPane().setDefaultButton(btnLaunch);
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					//
					int minimalNumberOfElements = (Integer) ClusterSizeExtractionInputWindow.this.spinnerMinimalNumberOfElements.getValue();

					//
					lastMinimalValue = minimalNumberOfElements;

					//
					if (netGUI.getSegmentation().isAtTheTop()) {
						//
						String title = "Cluster Size Extraction";
						String message = "Please, select a sub segment before the use of this form.";

						//
						JOptionPane.showMessageDialog(ClusterSizeExtractionInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);

					} else {
						//
						Segment segment = netGUI.getSegmentation().getCurrentSegment();
						String segmentLabels = netGUI.getSegmentation().getSegmentLabels().toStringSeparatedBy("; ");
						Net target = NetUtils.extractByClusterSize(netGUI.getNet(), segment, segmentLabels, minimalNumberOfElements);

						//
						NetGUI newNetGui = PuckGUI.instance().createNetGUI(netGUI.getFile(), target);
						newNetGui.setChanged(true, "-" + netGUI.getSegmentation().getCurrentSegment().getCriteria().getLabel() + "_sizeAbove_"
								+ minimalNumberOfElements);

						dispose();
					}

				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(ClusterSizeExtractionInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);
		this.spinnerMinimalNumberOfElements.setValue(lastMinimalValue);
	}
}
