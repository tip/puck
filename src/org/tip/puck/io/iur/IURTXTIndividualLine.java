package org.tip.puck.io.iur;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a line of individual from a TXT file.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class IURTXTIndividualLine {
	public static final int NOID = 0;

	private int id;
	private String name;
	private char gender;
	private List<String> attributesValues;

	/**
	 * 
	 */
	public IURTXTIndividualLine() {
		this.id = NOID;
		this.name = null;
		this.gender = 'X';
		this.attributesValues = new ArrayList<String>();
	}

	/**
	 * 
	 * @return
	 */
	public List<String> attributeValues() {
		List<String> result;

		result = this.attributesValues;

		//
		return result;
	}

	public char getGender() {
		return this.gender;
	}

	public int getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public void setGender(final char gender) {
		this.gender = gender;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public void setName(final String name) {
		this.name = name;
	}
}
