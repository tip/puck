package org.tip.puck.graphs;

import java.util.Comparator;

public class GraphComparatorByArcCount<E> implements Comparator<Graph<E>> {

	/**
	 * 
	 */
	@Override
	public int compare(final Graph<E> alpha, final Graph<E> bravo) {
		int result;
		
		result = new Integer(alpha.arcCount()).compareTo(bravo.arcCount());

		//
		return result;
	}
}
