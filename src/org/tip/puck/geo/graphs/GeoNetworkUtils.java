package org.tip.puck.geo.graphs;

import org.tip.puck.geo.GeoLevel;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.geo.Geography;
import org.tip.puck.geo.Place;
import org.tip.puck.geo.Place;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.Link;
import org.tip.puck.graphs.Node;
import org.tip.puck.partitions.Cluster;

/**
 * 
 */
public class GeoNetworkUtils {

	/**
	 * 
	 * @param clusterNetwork
	 * @param level
	 * @return
	 */
	public static <E> Graph<Place> createGeoNetwork(final Graph<Cluster<E>> clusterNetwork, final Geography geography, final GeoLevel level) {
		Graph<Place> result;

		result = new Graph<Place>();
		for (Node<Cluster<E>> node : clusterNetwork.getNodes()) {
			// result.addNode(node.getId(),Geography.getInstance().getPlace(node.getReferent().getLabel(),
			// level));
			result.addNode(node.getId(), geography.get(node.getReferent().getLabel()));
		}
		for (Link<Cluster<E>> link : clusterNetwork.getLinks()) {
			if (link.isArc()) {
				result.addArc(link.getSourceId(), link.getTargetId(), link.getWeight());
			}
			if (link.isEdge()) {
				result.addEdge(link.getSourceId(), link.getTargetId(), link.getWeight());
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param events
	 * @param level
	 * @return
	 */
/*	public static <E> Graph<Place> createGeoNetwork(final Relations events, final GeoLevel level) {
		Graph<Place> result;

		result = new Graph<Place>();

		for (Relation event : events) {
			Place startPlace = Geography.getInstance().getPlace(event.getAttributeValue("START_PLACE"), level);
			Place endPlace = Geography.getInstance().getPlace(event.getAttributeValue("END_PLACE"), level);
			result.addArc(startPlace, endPlace);
		}

		//
		return result;
	}*/

	/**
	 * 
	 * @param clusterNetwork
	 * @param level
	 * @return
	 */
	public static <E> Graph<String> createGeoNetwork2(final Graph<Cluster<E>> clusterNetwork, final GeoLevel level) {
		Graph<String> result;

		result = new Graph<String>();
		result.setLabel(clusterNetwork.getLabel()+"_"+level);

		for (Node<Cluster<E>> node : clusterNetwork.getNodes()) {
			// result.addNode(node.getId(),Geography.getInstance().getPlace(node.getReferent().getLabel(),
			// level));
			result.addNode(node.getId(), node.getReferent().getLabel());
		}

		for (Link<Cluster<E>> link : clusterNetwork.getLinks()) {
			if (link.isArc()) {
				result.addArc(link.getSourceId(), link.getTargetId(), link.getWeight());
			}
			if (link.isEdge()) {
				result.addEdge(link.getSourceId(), link.getTargetId(), link.getWeight());
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param geoNetwork
	 */
	public static void writeGeoNetwork(final Graph<Place> geoNetwork) {
		/**
		 * to be filled...
		 */
	}
}
