package org.tip.puck.geo.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ResourceBundle;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.geo.Geography;
import org.tip.puck.io.ods.ODSBufferedReader;
import org.tip.puck.io.ods.ODSWriter;
import org.tip.puck.net.Net;

/**
 * This class represents a TXT File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class GEOODSFile {

	protected enum Status {
		MANDATORY,
		OPTIONAL
	}

	public static final int MAX_LINE_SIZE = 1024;;

	private static final Logger logger = LoggerFactory.getLogger(GEOODSFile.class);

	/**
	 * Loads a IUR ODS file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Geography load(final File file) throws PuckException {
		Geography result;

		BufferedReader in = null;
		try {
			in = new ODSBufferedReader(file);
			result = GEOTXTFile.readGeography(in);

			result.setLabel(file.getName());

		} catch (UnsupportedEncodingException exception) {
			//
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");

		} catch (FileNotFoundException exception) {
			//
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");

		} catch (Exception exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.IO_ERROR.create("Opening file [" + file + "]");

		} finally {
			//
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File file, final Geography source) throws PuckException {
		PrintWriter out = null;
		try {
			//
			out = new PrintWriter(new ODSWriter(file));

			//
			GEOTXTFile.writeGeography(out, source);

		} catch (IOException exception) {
			//
			throw PuckExceptions.IO_ERROR.create(exception, "Opening file [" + file + "].");

		} catch (Exception exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.IO_ERROR.create(exception, "Opening file [" + file + "].");

		} finally {
			//
			IOUtils.closeQuietly(out);
		}
	}

	/**
	 * 
	 * @param file
	 * @param source
	 * @param bundle
	 * 
	 * @throws PuckException
	 */
	public static void save(final File file, final Net source, final ResourceBundle bundle) throws PuckException {

	}
}
