package org.tip.puck.mas;

/**
 * @author Telmo Menezes
 *
 */
public interface WeightFactor {
    public double factor(Agent mother, Agent father);
    
    public boolean appliesToMarriage();
    public boolean appliesToDivorce();
    
    public String factorName();
}