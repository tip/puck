package org.tip.puck.census.workers;


import org.tip.puck.PuckException;
import org.tip.puck.census.chains.Chain;
import org.tip.puck.census.chains.Couple;
import org.tip.puck.census.chains.Notation;
import org.tip.puck.graphs.Graph;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.net.workers.NetUtils;


/**
 * 
 * @author TIP
 */
public class CensusUtils {

	public static Graph<Individual> createCircuitNetwork(final Chain circuit) throws PuckException {
		Graph<Individual> result;
	
		if (circuit == null) {
			throw new NullPointerException("Null parameter detected.");
		} else {
			//
			result = new Graph<Individual>("Circuit " + circuit.signature(Notation.CLASSIC) + " " + circuit.signature(Notation.NUMBERS));
	
			result.incEdgeWeight(circuit.getLast(), circuit.getFirst());
			for (int i = 0; i < circuit.length(); i++) {
	
				Individual first = circuit.get(i);
				Individual second = circuit.get(i + 1);
	
				Individuals egos;
				Individuals alters;
				if (first instanceof Couple) {
					egos = ((Couple) first).individuals();
				} else {
					egos = new Individuals();
					egos.add(first);
				}
				if (second instanceof Couple) {
					alters = ((Couple) second).individuals();
				} else {
					alters = new Individuals();
					alters.add(second);
				}
	
				int dir = circuit.dir(i + 1);
	
				for (Individual ego : egos) {
					for (Individual alter : alters) {
						if (dir == 0) {
							result.incEdgeWeight(ego, alter);
						} else if (dir == -1) {
							result.incArcWeight(ego, alter);
						} else if (dir == 1) {
							result.incArcWeight(alter, ego);
						}
					}
				}
			}
	
		}
		//
		return result;
	}
	
	public static Net createCircuitInducedNet(final CircuitFinder source) throws PuckException {
		Net result;
		
		result = new Net();
	
		if (source == null) {
			throw new NullPointerException("Null parameter detected.");
		} else {
			//
			for (Chain circuit : source.getCircuits().getItems()) {
				
				Individual ego = result.getCloneWithAttributes(circuit.get(circuit.length()));
				Individual alter = result.getCloneWithAttributes(circuit.get(0));
				NetUtils.setKinSpouse(result, ego, alter);

				for (int i = 0; i < circuit.length(); i++) {
	
					Individual first = circuit.get(i);
					Individual second = circuit.get(i + 1);
	
					Individuals egos;
					Individuals alters;
					if (first instanceof Couple) {
						egos = ((Couple) first).individuals();
					} else {
						egos = new Individuals();
						egos.add(first);
					}
					if (second instanceof Couple) {
						alters = ((Couple) second).individuals();
					} else {
						alters = new Individuals();
						alters.add(second);
					}
	
					int dir = circuit.dir(i + 1);
	
					for (Individual protoEgo : egos) {
						ego = result.getCloneWithAttributes(protoEgo);
						for (Individual protoAlter : alters) {
							alter = result.getCloneWithAttributes(protoAlter);
							if (dir == 0) {
								NetUtils.setKinSpouse(result, ego, alter);
							} else if (dir == -1) {
								NetUtils.setKinParent(result, ego, alter);
							} else if (dir == 1) {
								NetUtils.setKinParent(result, alter, ego);
							}
						}
					}
				}
			}
			result.setLabel(result.getLabel()+" Circuit Induced Network "+source.censusDetails);
	
		}
		//
		return result;
	}

	public static Graph<Individual> createCircuitInducedNetwork(final CircuitFinder source) throws PuckException {
		Graph<Individual> result;
	
		if (source == null) {
			throw new NullPointerException("Null parameter detected.");
		} else {
			//
			result = new Graph<Individual>("Circuit-Induced Network " + source.getLabel());
	
			for (Chain circuit : source.getCircuits().getItems()) {
				result.incEdgeWeight(circuit.getFirst(), circuit.getLast());
				for (int i = 0; i < circuit.length(); i++) {
	
					Individual first = circuit.get(i);
					Individual second = circuit.get(i + 1);
	
					Individuals egos;
					Individuals alters;
					if (first instanceof Couple) {
						egos = ((Couple) first).individuals();
					} else {
						egos = new Individuals();
						egos.add(first);
					}
					if (second instanceof Couple) {
						alters = ((Couple) second).individuals();
					} else {
						alters = new Individuals();
						alters.add(second);
					}
	
					int dir = circuit.dir(i + 1);
	
					for (Individual ego : egos) {
						for (Individual alter : alters) {
							if (dir == 0) {
								result.incEdgeWeight(ego, alter);
							} else if (dir == -1) {
								result.incArcWeight(ego, alter);
							} else if (dir == 1) {
								result.incArcWeight(alter, ego);
							}
						}
					}
				}
			}
	
			NetUtils.setGenderShapes(result);
	
		}
		//
		return result;
	}

	public static Graph<Individual> createCircuitInducedFrameNetwork(final CircuitFinder source) throws PuckException {
		Graph<Individual> result;
	
		if (source == null) {
			throw new NullPointerException("Null parameter detected.");
		} else {
			//
			result = new Graph<Individual>("Circuit-Induced Frame Network " + source.getLabel());
	
			for (Chain circuit : source.getCircuits().getItems()) {
				for (int i = 0; i < 2 * circuit.dim(); i++) {
	
					Individual first = circuit.getPivot(i);
					Individual second = circuit.getPivot(i + 1);
	
					if (first != second) {
						result.addEdge(first, second, 1 - 2 * ((i+1) % 2));
					}
	
				}
			}
	
		}
	
		NetUtils.setGenderShapes(result);
	
		//
		return result;
	}

}
