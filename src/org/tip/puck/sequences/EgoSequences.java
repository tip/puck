package org.tip.puck.sequences;

import org.tip.puck.net.Individuals;
import org.tip.puck.net.relations.Relation;

/**
 * 
 * @author Klaus Hamberger
 *
 */
public class EgoSequences extends Sequences<EgoSequence,Relation>{
	
	
/*	public EgoSequences(){
	}*/
	
/*	public <S extends Sequenceable<E>,E extends Numberable> EgoSequences(Sequenceables<S,E> sequences){
		for (S sequence : sequences){
			put(new EgoSequence(sequence));
		}
	}
	
	public Sequences (Segmentation segmentation, SequenceCriteria criteria){
		
		label = criteria.getSequencesLabel();
		
		Relations relations = segmentation.getCurrentRelations().getByModelName(criteria.getRelationModelName());
		
		for (Relation relation : relations){
			
			Sequence sequence = null;
			
			String sequenceId = relation.getAttributeValue(label);
			
			if (StringUtils.isNumeric(sequenceId)){
				sequence = getById(Integer.parseInt(sequenceId));
				if (sequence == null){
					sequence = new Sequence(Integer.parseInt(sequenceId));
					sequence.setDateLabel(criteria.getDateLabel());
					put(sequence);
				}
			}
			
			sequence.putInOrder(relation);
		}
		
	}


	public EgoSequence getByEgo(Individual ego){
		EgoSequence result;
		
		result = null;
		
		for (EgoSequence sequence : this){
			if (sequence.getEgo()==ego){
				result = sequence;
				break;
			}
		}
		//
		return result;
	}*/
	
	public Individuals getEgos(){
		Individuals result;
		
		result = new Individuals();
		
		for (EgoSequence sequence : this){
			result.put(sequence.getEgo());
		}
		//
		return result;
	}
	
	public EgoSequence getByEgoId(int egoId){
		EgoSequence result;
		
		result = null;
		
		for (EgoSequence sequence : this){
			if (sequence.getEgo()!=null && sequence.getEgo().getId()==egoId){
				result = sequence;
				break;
			}
		}
		//
		return result;
	}
	
/*	public Partition<Chain> getChildHostKinshipChains (int maxDepth, int maxOrder, String chainClassification){
		Partition<Chain> result;
		
		result = new Partition<Chain>();
		
		for (EgoRelationSequence sequence : this){
			for (Relation event : sequence.childMigrationsWithoutParents()){
				for (Individual host : event.getIndividuals("HOST")){
					Partition<Chain> chains = ChainFinder.findChains(sequence.getEgo(), host, maxDepth, maxOrder, chainClassification);
					if (chains.size()==0){
						result.put(new Chain (sequence.getEgo(),host), null);
					} else {
						result.add(chains);
					}
				}
			}
		}
		//
		return result;
	}
	
	public void addRenumbered(EgoSequence sequence){
		sequence.setId(size());
		add(sequence);
	}
	
	public int getNrEvents(){
		int result;
		
		result = 0;
		for (EgoSequence sequence : this){
			result += sequence.getStations().size();
		}
		
		//
		return result;
	}
	

	public Individuals getIndividuals(Segmentation segmentation) throws PuckException{
		Individuals result;
		
		result = new Individuals();
		
		for (EgoSequence sequence : this){
			for (Individual individual : sequence.getIndividuals()){
				if (segmentation.getCurrentIndividuals().contains(individual)){
					result.add(individual);
				}
			}
		}

		//
		return result;
	}
	
	public Individuals getIndividuals() throws PuckException{
		Individuals result;
		
		result = new Individuals();
		
		for (EgoSequence sequence : this){
			for (Individual individual : sequence.getIndividuals()){
				result.add(individual);
			}
		}

		//
		return result;
	}
	
	public List<Relation> getStations(Ordinal time){
		List<Relation> result;
		
		result = new ArrayList<Relation>();
		
		for (EgoSequence sequence : this){
			result.add(sequence.getStation(time));
		}
		//
		return result;
	}
	
	
	public boolean isPopulatable(){
		boolean result;
		
		result = false;
		
		for (Sequence<Relation> sequence : this){

			for (Relation station : sequence.getStations().values()){
				
				if (station != null){

					if (station instanceof Populatable) {
						result = true;
					}
					//
					return result;
				}
			}

		}
		//
		return result;
	}*/

}
