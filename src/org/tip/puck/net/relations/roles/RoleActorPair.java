package org.tip.puck.net.relations.roles;

import org.tip.puck.net.Gender;

public class RoleActorPair {
	
	RoleActor self;
	RoleActor alter;
	Adjustable adjustable;
	
	enum Adjustable {
		
		SELF,
		ALTER,
		NONE;
		
		Adjustable invert(){
			
			Adjustable result;
			
			switch(this){
			case SELF:
				result = ALTER;
				break;
			case ALTER:
				result = SELF;
				break;
			default:
				result = NONE;
				break;
			}
			//
			return result;
		}
	}
	
	RoleActorPair (RoleActor self, RoleActor alter){
		
		this.self = self;
		this.alter = alter;
	}
	
	RoleActorPair (RoleRelations relations, RoleActor alpha, RoleActor beta, Adjustable adjustable){

		if (alpha!=null && beta!=null){
						
			this.self = alpha.clone();
			this.alter = beta.clone();
//			this.relations = relations;
			this.adjustable = adjustable;
			
			// Adjust speaker gender
			
			if (self.getEgoGender().matchs(alter.getEgoGender())){
				
				// Adjust crossness (and adjust genders accordingly)
				
				self = self.withCrossness(relations);
				alter = alter.withCrossness(relations);
				
				if (self!=null && alter!=null){
					

					// Adjust speaker gender (and adjust gender accordingly for cross and self cases)
					
					self = self.withEgoGender(alter.getEgoGender());
					alter = alter.withEgoGender(self.getEgoGender());
					
					// Adjust self gender according to role with respect to alter and general configurations
					
					self = self.cloneAs(beta.getRole().invertName()).withAlterGender(relations);
					
					// Adjust self role and age (gender has already been set according to alter relation and will be ignored!)	

					self = self.asSelf(relations);
					
					// Adjust alter gender and age according to general configurations
					
					alter = alter.withAlterGender(relations);
					alter = alter.withAge(relations);
					
					// Adjust alter age of sibling
					
					if (alter.isSibling()){
						
						alter = alter.asSiblingOf(self, relations);
					}
					
					// Adjust gender of spouses
					
					if (alter.isSpouse() && relations.heteroMarriage()){
						
						adjustCouple();
					}
				}
				
			} else {
				
//				System.err.println("Incompatible ego gender configuration: "+this);
				self = null;
				alter = null;
			}
		}
	}
	
	private void adjustCouple(){
		
		Gender selfGender = self.getAlterGender();
		Gender alterGender = alter.getAlterGender();
		
		if ((!selfGender.isUnknown() || !alterGender.isUnknown())) {
			
			if (!selfGender.isUnknown() && (alterGender.isUnknown() || adjustable == Adjustable.ALTER)){
				
				alter = alter.withAlterGender(selfGender.invert());
				
			} else if (!alterGender.isUnknown() && (selfGender.isUnknown() || adjustable == Adjustable.SELF)){
				
				self = self.withAlterGender(alterGender.invert());
				
			} else if (alterGender!=selfGender.invert()){
				
//				System.err.println("Invalid spouse configuration "+this);
				self = null;
				alter = null;
			}
		}
	}
	
	RoleActorPair reciprocal (RoleRelations relations){
		RoleActorPair result;
		
		
		RoleActor reciprocal = self.cloneAs(alter.getRole().invertName());
		reciprocal.setAlterGender(null);

		result = new RoleActorPair(relations, alter, reciprocal,adjustable.invert());
		
		//
		return result;
	}
	
	boolean isValid(){
		
		return self!=null && alter!=null;
	}

	RoleActor getSelf() {
		return self;
	}

	RoleActor getAlter() {
		return alter;
	}
	
	RoleDefinition getRoleDefinition(){
		RoleDefinition result;
		
		if (isValid()){
			
			result = new RoleDefinition(self.getIndividual(),alter);
			
		} else {
			
			result = null;
		}
		
		return result;
	}
	
	RoleDefinition getStandardRoleDefinition(RoleRelations relations){
		RoleDefinition result;
		
		if (isStandardPosition(relations)){
			result = getRoleDefinition();
		} else {
			result = reciprocal(relations).getRoleDefinition();
		}
		//
		return result;
	}

	
	boolean isStandardPosition(RoleRelations relations){
		boolean result;

		result = new RoleActorComparator(relations).compare(alter, self)>=0 || self.isHiddenCross() || self.hasSelfName();
		
		return result;
	}
	
	public String toString(){
		
		return "["+self+" ; "+alter+"]";
	}

	Adjustable getAdjustable() {
		return adjustable;
	}

	public void setSelf(RoleActor self) {
		this.self = self;
	}

	public void setAlter(RoleActor alter) {
		this.alter = alter;
	}
	
	
	

}
