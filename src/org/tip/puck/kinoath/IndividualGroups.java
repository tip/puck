package org.tip.puck.kinoath;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author TIP
 */
public class IndividualGroups extends ArrayList<IndividualGroup> {

	private static final long serialVersionUID = 7668861953902853130L;
	private static final Logger logger = LoggerFactory.getLogger(IndividualGroups.class);

	private static String label;

	/**
	 * 
	 */
	public IndividualGroups() {
		//
		this(null);
	}

	/**
	 * 
	 */
	public IndividualGroups(final String label) {
		//
		super();

		this.label = label;
	}

	/**
	 * 
	 * @return
	 */
	public IndividualGroupList toList() {
		IndividualGroupList result;

		result = new IndividualGroupList(this.size());
		for (IndividualGroup group : this) {
			//
			result.add(group);
		}

		//
		return result;
	}

	public static String getLabel() {
		return label;
	}

	public static void setLabel(final String label) {
		IndividualGroups.label = label;
	}

}
