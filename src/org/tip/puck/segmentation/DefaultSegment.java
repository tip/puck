package org.tip.puck.segmentation;

import java.util.ArrayList;
import java.util.List;

import org.tip.puck.geo.Geography;
import org.tip.puck.net.Families;
import org.tip.puck.net.Family;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionCriteria;

/**
 * The default segment is used as no segmentation.
 * 
 * @author TIP
 */
public class DefaultSegment implements Segment {

	private Individuals currentIndividuals;
	private Families currentFamilies;
	private Relations currentRelations;
	private Geography geography;

	/**
	 * 
	 * @param individuals
	 * @param families
	 * @param relations
	 */
	public DefaultSegment(final Individuals individuals, final Families families, final Relations relations, final Geography geography) {
		this.currentIndividuals = individuals;
		this.currentFamilies = families;
		this.currentRelations = relations;
		this.geography = geography;
	}

	/**
	 * 
	 */
	@Override
	public int getClusterCount() {
		int result;

		result = 0;

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getClusterLabels() {
		List<String> result;

		//
		result = new ArrayList<String>();

		//
		result.add("*");

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public PartitionCriteria getCriteria() {
		PartitionCriteria result;

		result = null;

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getCurrentClusterIndex() {
		int result;

		result = 0;

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Families getCurrentFamilies() {
		Families result;

		result = this.currentFamilies;

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Individuals getCurrentIndividuals() {
		Individuals result;

		result = this.currentIndividuals;

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Relations getCurrentRelations() {
		Relations result;

		result = this.currentRelations;

		//
		return result;
	}

	/**
	 * In default segment, this method is not significant. It could return
	 * individual cardinal or family cardinal or relation cardinal. We make the
	 * choice to return individual cardinal.
	 * 
	 * @return the individuals cardinal.
	 * 
	 */
	@Override
	public int getCurrentSize() {
		int result;

		result = this.currentIndividuals.size();

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLabel() {
		String result;

		result = "ALL";

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Families getOutOfCurrentClusterFamilies() {
		Families result;

		//
		result = new Families();

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Individuals getOutOfCurrentClusterIndividuals() {
		Individuals result;

		//
		result = new Individuals();

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Relations getOutOfCurrentClusterRelations() {
		Relations result;

		//
		result = new Relations();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Partition<?> getPartition() {
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getShortLabel() {
		String result;

		result = "ALL";

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getShortShortLabel() {
		String result;

		//
		result = "ALL";

		//
		return result;
	}

	/**
	 * Nothing to do.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public void refresh(final Segment source) {
	}

	/**
	 * Nothing to do.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public void selectCluster(final Family family) {
	}

	/**
	 * Nothing to do.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public void selectCluster(final Individual individual) {
	}

	/**
	 * Nothing to do.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public void selectCluster(final int index) {
	}

	/**
	 * Nothing to do.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public void selectCluster(final Relation relation) {
	}

	/**
	 * Nothing to do.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public void selectOutOfPartitionCluster() {
	}

	public Geography getGeography() {
		return geography;
	}

	public void setGeography(Geography geography) {
		this.geography = geography;
	}
	
	
}
