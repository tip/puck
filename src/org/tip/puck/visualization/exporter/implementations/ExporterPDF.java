package org.tip.puck.visualization.exporter.implementations;

import com.itextpdf.text.PageSize;
import java.io.File;
import java.io.IOException;
import org.gephi.io.exporter.api.ExportController;
import org.gephi.io.exporter.preview.PDFExporter;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;
import org.tip.puck.visualization.exporter.Exporter;


/**
 * @date May 12, 2012
 * @author Edoardo Savoia
 */
public class ExporterPDF implements Exporter {

    @Override
    public void exportToFile(File file, Workspace workspace) {
	ExportController ec = Lookup.getDefault().lookup(ExportController.class);
	//PDF Exporter config and export to Byte array
	PDFExporter pdfExporter = (PDFExporter) ec.getExporter("pdf");
	pdfExporter.setPageSize(PageSize.A0);
	pdfExporter.setWorkspace(workspace);
	try {
	    ec.exportFile(file, pdfExporter);
	} catch (IOException ex) {
	    ex.printStackTrace();
	    return;
	}
    }

}
