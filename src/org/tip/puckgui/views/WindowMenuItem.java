package org.tip.puckgui.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

import org.tip.puckgui.WindowGUI;

/**
 * 
 * @author TIP
 */
public class WindowMenuItem extends JMenuItem {

	private static final long serialVersionUID = -7558974509720699542L;
	protected WindowGUI windowGUI;

	/**
	 * 
	 * @param title
	 */
	public WindowMenuItem(final WindowGUI windowGUI) {
		super();
		this.windowGUI = windowGUI;
		setText(windowGUI.getTitle());

		this.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				//
				System.out.println("Selecting item in Windows menu.");

				windowGUI.toFront();
			}
		});
	}
}
