package org.tip.puck.visualization.layouts.hierarchical.datastructs.chain;

import java.util.EnumSet;
import org.gephi.graph.api.Node;
import org.gephi.graph.spi.LayoutData;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.containers.BorderContainer;


/**
 * @date Jun 25, 2012
 * @author Edoardo Savoia
 */


public class NodeLayoutData implements LayoutData {
    public enum Color {
	//NULL,
	RED {
	    @Override
	    public Color negate(){
		return BLACK;
	    }
	},BLACK {
	    @Override
	    public Color negate(){
		return RED;
	    }
	
	};

	abstract public Color negate();
    }

    // private Value color = Value.NULL;
    public EnumSet<Color> colors = EnumSet.noneOf(Color.class);
    private BorderContainer container;
    private int level = -1;

    private static void initData(Node node) {
	NodeLayoutData nodeData = (NodeLayoutData) node.getNodeData().getLayoutData();
	if (nodeData == null) {
	    nodeData = new NodeLayoutData();
	    node.getNodeData().setLayoutData(nodeData);
	}
    }

    private static NodeLayoutData getData(Node node) {
	return (NodeLayoutData) node.getNodeData().getLayoutData();
    }

    private static EnumSet<Color> getColorValues(Node node) {
	EnumSet<Color> result = null;
	NodeLayoutData data = getData(node);
	if (data != null) {
	    result = data.colors;
	}
	return result;
    }

    public static int getLevel(Node node) {
	initData(node);
	NodeLayoutData data = getData(node);
	return data.level;
    }

    public static void setLevel(Node node, int level) {
	initData(node);
	NodeLayoutData data = getData(node);
	data.level = level;
    }

    public static boolean hasContainter(Node node) {
	return (getData(node) != null) && (getData(node).container != null);
    }

    public static void setContainer(Node node,BorderContainer container){
	initData(node);
	NodeLayoutData data = getData(node);
	data.container = container;
    }

    public static BorderContainer getContainer(Node node){
	NodeLayoutData data = getData(node);
	if (data == null) {
	    return null;
	}
	return data.container;
    }
    
    public static void color(Node node, NodeLayoutData.Color color) {
	initData(node);
	NodeLayoutData nodeData = getData(node);
	// nodeColor.color = color;
	nodeData.colors.add(color);
	nodeData.colors.remove(color.negate());
    }

    public static void removeColor(Node node, NodeLayoutData.Color color) {
	NodeLayoutData nodeData = getData(node);
	if (nodeData !=null) {
	    nodeData.colors.remove(color);
	}
    }

    public static void clean(Node node) {
	NodeLayoutData nodeData = getData(node);
	if (nodeData != null) {
	    node.getNodeData().setLayoutData(null);
	}
    }

    public static boolean isColored(Node node){
	return (getData(node) !=null) && getData(node).colors.isEmpty();
	// (getColor(node).color != Value.NULL);
    }

    public static boolean isColored(Node node, Color color){
	if (getData(node) ==null) {
	    return false;
	} else {
	    return getData(node).colors.contains(color);
	}

	//return (getData(node) !=null) && getData(node).colors.contains(color);
	// (getColor(node).color == color);
    }

    public static boolean isColored(Node node, EnumSet<? extends Color> colors){
	return (getData(node) !=null) && getData(node).colors.containsAll(colors);
    }

}
