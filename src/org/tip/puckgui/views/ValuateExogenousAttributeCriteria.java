package org.tip.puckgui.views;

import org.tip.puck.net.workers.AttributeWorker.Scope;

/**
 * 
 * @author TIP
 */
public class ValuateExogenousAttributeCriteria {

	private Scope scope;
	private String sourceLabel;
	private String targetLabel;
	private String valuePrefix;

	/**
	 * 
	 */
	public ValuateExogenousAttributeCriteria() {
		this.scope = Scope.INDIVIDUALS;
		this.sourceLabel = null;
		this.targetLabel = null;
		this.valuePrefix = null;
	}

	public Scope getScope() {
		return scope;
	}

	public String getSourceLabel() {
		return sourceLabel;
	}

	public String getTargetLabel() {
		return targetLabel;
	}

	public String getValuePrefix() {
		return valuePrefix;
	}

	public void setScope(final Scope scope) {
		this.scope = scope;
	}

	public void setSourceLabel(final String sourceLabel) {
		this.sourceLabel = sourceLabel;
	}

	public void setTargetLabel(final String targetLabel) {
		this.targetLabel = targetLabel;
	}

	public void setValuePrefix(final String valuePrefix) {
		this.valuePrefix = valuePrefix;
	}

}