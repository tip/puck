package org.tip.puck.partitions.workers;

import org.tip.puck.PuckException;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionMaker;
import org.tip.puck.report.Report;
import org.tip.puck.report.ReportAttributes;
import org.tip.puck.util.Chronometer;
import org.tip.puck.util.Value;

/**
 * 
 * @author TIP
 */
public class PartitionReporter {

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportBasicInformation(final Net net, final PartitionCriteria criteria) throws PuckException {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		Partition<Individual> partition = PartitionMaker.create(net, criteria);

		//
		result = new Report();
		result.setTitle("Basic statistics about a partition");
		result.setOrigin("Partition reporter");
		result.setTarget(net.getLabel());

		//
		result.setInputComment("Criteria: " + criteria.toString());

		//
		ReportAttributes items = new ReportAttributes();
		items.add("Label", partition.getLabel());
		items.add("Criteria label", partition.getCriteria().getLabel());
		items.add("Number of clusters", partition.getClusters().size());
		for (Cluster<Individual> cluster : partition.getClusters().toListSortedByValue()) {
			items.add("- " + Value.stringValue(cluster.getValue()), cluster.size());
		}

		result.outputs().append(items);
		result.outputs().appendln();

		// statistics.add(attributeLabel);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}
}
