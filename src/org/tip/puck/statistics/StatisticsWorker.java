package org.tip.puck.statistics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.Stack;
import java.util.TreeSet;

import org.tip.puck.PuckException;
import org.tip.puck.census.workers.CensusCriteria;
import org.tip.puck.census.workers.CircuitFinder;
import org.tip.puck.geo.Geography;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Families;
import org.tip.puck.net.Family;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.KinType;
import org.tip.puck.net.Net;
import org.tip.puck.net.workers.FamilyValuator;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Clusters;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionCriteria.ValueFilter;
import org.tip.puck.partitions.PartitionMaker;
import org.tip.puck.report.Report;
import org.tip.puck.report.ReportAttributes;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.util.IntWithMax;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.NumberedIntegers;
import org.tip.puck.util.NumberedValues;
import org.tip.puck.util.Value;

/**
 * 
 * @author TIP
 */
public class StatisticsWorker {
	
	

	Individuals individuals;
	Families families;
	GenderedInt genderDistribution;
	int partnershipCount;
	int marriageCount;
	int fertileCount;
	int filiationCount;
	IntWithMax components;
	Partition<Individual> agnaticPartition;
	Partition<Individual> uterinePartition;
	
	enum Indicator {
		NR_INDIVIDUALS,
		NR_MEN,
		NR_WOMEN,
		NR_UNKNOWN,
		NR_MARRIAGES,
		NR_UNIONS,
		NR_NONSINGLE_MEN,
		NR_NONSINGLE_WOMEN,
		NR_FILIATION_TIES,
		NR_FERTILE_UNIONS,
		NR_COWIFE_RELIATIONS,
		NR_COHUSBAND_RELIATIONS,
		NR_COMPONENTS,
		MEAN_COMPONENT_SHARE_AGNATIC,
		MEAN_COMPONENT_SHARE_UTERINE,
		MAX_COMPONENT_SHARE_AGNATIC,
		MAX_COMPONENT_SHARE_UTERINE,
		CYCLOMATIC_NR,
		MARRIAGE_DENSITY,
		FILIATION_DENSITY,
		DEPTH,
		MEAN_NR_SPOUSES_MEN,
		MEAN_NR_SPOUSES_WOMEN,
		MEAN_AGNATIC_SIBSET_SIZE,
		MEAN_UTERINE_SIBSET_SIZE, 
		MEAN_FULL_SIBSET_SIZE, // Nr of children per fertile couple
		MEAN_DEPTH,
		MEAN_SPOUSE_DISTANCE_GEN;
		
		public String toString(){
			String result;
			switch (this){
			case NR_INDIVIDUALS:
				result = "individuals";
				break;
			case NR_MEN:
				result = "men";
				break;
			case NR_WOMEN:
				result = "women";
				break;
			case NR_UNKNOWN:
				result = "unknown";
				break;
			case NR_MARRIAGES:
				result = "marriages";
				break;
			case NR_UNIONS:
				result = "unions";
				break;
			case NR_NONSINGLE_MEN:
				result = "non-single men";
				break;
			case NR_NONSINGLE_WOMEN:
				result = "non-single women";
				break;
			case NR_FILIATION_TIES:
				result = "parent-child ties";
				break;
			case NR_FERTILE_UNIONS:
				result = "fertile unions";
				break;
			case NR_COWIFE_RELIATIONS:
				result = "co-wife relations";
				break;
			case NR_COHUSBAND_RELIATIONS:
				result = "co-husband relations";
				break;
			case NR_COMPONENTS:
				result = "components";
				break;
			case MEAN_COMPONENT_SHARE_AGNATIC:
				result = "mean component share (agnatic)";
				break;
			case MEAN_COMPONENT_SHARE_UTERINE:
				result = "mean component share (uterine)";
				break;
			case MAX_COMPONENT_SHARE_AGNATIC:
				result = "max component share (agnatic)";
				break;
			case MAX_COMPONENT_SHARE_UTERINE:
				result = "max component share (uterine)";
				break;
			case CYCLOMATIC_NR:
				result = "elementary cycles";
				break;
			case MARRIAGE_DENSITY:
				result = "density (marriages)";
				break;
			case FILIATION_DENSITY:
				result = "density (filiation)";
				break;
			case DEPTH:
				result = "depth";
				break;
			case MEAN_DEPTH:
				result = "mean depth";
				break;
			case MEAN_NR_SPOUSES_MEN:
				result = "mean spouse number of men";
				break;
			case MEAN_NR_SPOUSES_WOMEN:
				result = "mean spouse number of women";
				break;
			case MEAN_AGNATIC_SIBSET_SIZE:
				result = "mean sibset size agnatic";
				break;
			case MEAN_UTERINE_SIBSET_SIZE:
				result = "mean sibset size uterine";
				break;
			case MEAN_FULL_SIBSET_SIZE:
				result = "mean nr of children per fertile couple";
				break;
			case MEAN_SPOUSE_DISTANCE_GEN:
				result = "mean generational distance of spouses";
				break;
			default:
				result = null;
			}
			//
			return result;
		}
	}
	
	public void addItem(ReportAttributes items, Indicator indicator){
		items.add(indicator.toString(), getValue(indicator));
	}

	
	StatisticsWorker (Individuals individuals, Families families) throws PuckException {
		this.individuals = individuals;
		this.families = families;
		genderDistribution = StatisticsWorker.genderDistribution(individuals);
		partnershipCount = StatisticsWorker.numberOfPartnerships(families);
		marriageCount = StatisticsWorker.numberOfMarriages(families);
		fertileCount = StatisticsWorker.numberOfFertileMarriages(families);
		filiationCount = StatisticsWorker.numberOfFiliationTies(families);
		components = StatisticsWorker.numberOfComponents(individuals);
		agnaticPartition = PartitionMaker.createRaw("PATRIC partition", individuals, "PATRIC",null);
		uterinePartition = PartitionMaker.createRaw("MATRIC partition", individuals, "MATRIC",null);
	}
	
	StatisticsWorker (Net net) throws PuckException {
		this (net.individuals(),net.families());
	}
	
	StatisticsWorker (Segmentation segmentation) throws PuckException{
		this (segmentation.getCurrentIndividuals(), segmentation.getCurrentFamilies());
	}
	
	public Value getValue (Indicator indicator){
		Value result;
		
		switch (indicator){
		case NR_INDIVIDUALS:
			result = new Value(individuals.size());
			break;
		case NR_MEN:
			result = new Value(genderDistribution.getMenValue());
			break;
		case NR_WOMEN:
			result = new Value(genderDistribution.getWomenValue());
			break;
		case NR_UNKNOWN:
			result = new Value(genderDistribution.getUnknownValue());
			break;
		case NR_MARRIAGES:
			result = new Value(marriageCount);
			break;
		case NR_UNIONS:
			result = new Value(partnershipCount);
			break;
		case NR_NONSINGLE_MEN:
			result = new Value(numberOfNotSingles(individuals, Gender.MALE));
			break;
		case NR_NONSINGLE_WOMEN:
			result = new Value(numberOfNotSingles(individuals, Gender.FEMALE));
			break;
		case NR_FILIATION_TIES:
			result = new Value(filiationCount);
			break;
		case NR_FERTILE_UNIONS:
			result = new Value(String.format("%d (%.2f%%)", fertileCount, MathUtils.percent(fertileCount, marriageCount)));
			break;
		case NR_COWIFE_RELIATIONS:
			result = new Value(numberOfCoSpouseRelations(individuals, Gender.FEMALE));
			break;
		case NR_COHUSBAND_RELIATIONS:
			result = new Value(numberOfCoSpouseRelations(individuals, Gender.MALE));
			break;
		case NR_COMPONENTS:
			result = new Value(String.format("%d (max. %d)", components.value(), components.max()));
			break;
		case MEAN_COMPONENT_SHARE_AGNATIC:
			result = new Value(String.format("%.2f%% (without singletons: %.2g%%)", agnaticPartition.meanShare(), agnaticPartition.meanShare(2)));
			break;
		case MEAN_COMPONENT_SHARE_UTERINE:
			result = new Value(String.format("%.2f%% (without singletons: %.2g%%)", uterinePartition.meanShare(), uterinePartition.meanShare(2)));
			break;
		case MAX_COMPONENT_SHARE_AGNATIC:
			result = new Value(String.format("%.2f%%" ,agnaticPartition.maxShare()));
			break;
		case MAX_COMPONENT_SHARE_UTERINE:
			result = new Value(String.format("%.2f%%" ,uterinePartition.maxShare()));
			break;
		case CYCLOMATIC_NR:
			result = new Value(marriageCount + filiationCount - individuals.size() + components.value());
			break;
		case MARRIAGE_DENSITY:
			result = new Value(String.format("%.2f%%", densityOfMarriages(marriageCount, individuals.size())));
			break;
		case FILIATION_DENSITY:
			result = new Value(String.format("%.2f%%", densityOfFiliations(filiationCount, individuals.size())));
			break;
		case DEPTH:
			result = new Value(depth(individuals));
			break;
		case MEAN_DEPTH:
			result = new Value(meanDepth(individuals));
			break;
		case MEAN_NR_SPOUSES_MEN:
			result = new Value(meanNumberOfSpouses(individuals, Gender.MALE));
			break;
		case MEAN_NR_SPOUSES_WOMEN:
			result = new Value(meanNumberOfSpouses(individuals, Gender.FEMALE));
			break;
		case MEAN_AGNATIC_SIBSET_SIZE:
			result = new Value(meanSibsetSize(individuals, Gender.MALE));
			break;
		case MEAN_UTERINE_SIBSET_SIZE:
			result = new Value(meanSibsetSize(individuals, Gender.FEMALE));
			break;
		case MEAN_FULL_SIBSET_SIZE:
			result = new Value(meanSibsetSize(families));
			break;
		case MEAN_SPOUSE_DISTANCE_GEN:
			result = new Value(MathUtils.round(meanSpouseDistance(individuals,families,"GEN",null),2));
			break;
		default:
			result = null;
		}
		//
		return result;
	}
	

	public static String getValueString (Net net) throws PuckException {
		String result;
		
		result = net.getLabel();
		StatisticsWorker worker = new StatisticsWorker(net);
		for (Indicator indicator : Indicator.values()){
			result += "\t";
			Value value = worker.getValue(indicator);
			if (value!=null){
				result += value.toString();
			}
		}
		
		//
		return result;
	}
	
	public static void getDistances (Individual ego, int maxDistance, FiliationType filiationType, Report report){
		
		Map<Individual,Integer> distances = distances(ego,maxDistance,filiationType);
		for (Individual individual : distances.keySet()){
			individual.setAttribute("DIST_"+ego.getId(), distances.get(individual)+"");
		}
		
	    SortedSet<Map.Entry<Individual,Integer>> sortedEntries = new TreeSet<Map.Entry<Individual,Integer>>(
	            new Comparator<Map.Entry<Individual,Integer>>() {
	                @Override public int compare(Map.Entry<Individual,Integer> e1, Map.Entry<Individual,Integer> e2) {
	                    int res = e1.getValue().compareTo(e2.getValue());
	                    return res != 0 ? res : 1;
	                }
	            }
	        );
	        sortedEntries.addAll(distances.entrySet());
	        
	        report.outputs().appendln("Relatives "+filiationType+" of "+ego);
	        report.outputs().appendln();
	        for (Map.Entry<Individual, Integer> entry: sortedEntries){
	        	report.outputs().appendln(entry.getValue()+"\t"+entry.getKey());
	        }
	}
	
	public static Map<Individual,Set<Individual>> neighborSets (final Individuals individuals, int maxDistance, FiliationType filiationType){
		Map<Individual,Set<Individual>> result;
		
		result = new HashMap<Individual,Set<Individual>>();
		for (Individual ego : individuals){
			result.put(ego,new HashSet<Individual>());
		}
		
//		boolean residential = filiationType == FiliationType.VIRI || filiationType == FiliationType.UXORI;
		
		for (Individual ego : individuals){
			
//			if (!residential || filiationType.hasLinkingGender(ego.getGender())){
				Map<Individual,Integer> distances = distances(ego,maxDistance,filiationType);
				Set<Individual> egosNeighbors = result.get(ego);
				for (Individual alter : individuals){
					if (alter.getId()>ego.getId() && distances.get(alter)!=null && distances.get(alter)>0){
//						if (!residential || filiationType.hasLinkingGender(alter.getGender())){
							egosNeighbors.add(alter);
							result.get(alter).add(ego);
						}
					}
//				}
//			}
		}
		System.out.println("neighborSet finished");
		//
		return result;
	}
	
	
	public static Map<Individual,Map<Individual,Double>> neighborWeights (final Individuals individuals, int maxDistance, FiliationType filiationType, double weight){
		Map<Individual,Map<Individual,Double>> result;
		
		result = new HashMap<Individual,Map<Individual,Double>>();
		for (Individual ego : individuals){
			result.put(ego,new HashMap<Individual,Double>());
		}
		
		for (Individual ego : individuals){
			Map<Individual,Integer> distances = distances(ego,maxDistance,filiationType);
			Map<Individual,Double> weights = result.get(ego);
			for (Individual alter : individuals){
				if (alter.getId()>ego.getId()){
					Map<Individual,Double> alterWeights = result.get(alter);
					if (distances.get(alter)!=null && distances.get(alter)>0 && filiationType.hasLinkingGender(alter.getGender())){
						weights.put(alter, weight);
						alterWeights.put(ego, weight);
					} else {
						weights.put(alter, 1.);
						alterWeights.put(ego, 1.);
					}
				}
			}
		}
		System.out.println("weightMap finished");
		//
		return result;
	}
	
	
	public static Map<Individual,Integer> distances (final Individual ego, int maxDistance, FiliationType filiationType){
		Map<Individual,Integer> result;
		
		result = new HashMap<Individual,Integer>();
		
		result.put(ego,0);
		
		if (!filiationType.residential() || filiationType.hasLinkingGender(ego.getGender())){
			addUpDistances(result,ego,0,maxDistance, filiationType);
			addDownDistances(result,ego,0,maxDistance, filiationType);
		} else {
			for (Individual spouse : ego.spouses()){
				result.put(spouse, 1);
				addUpDistances(result,spouse,1,maxDistance, filiationType);
				addDownDistances(result,spouse,1,maxDistance, filiationType);
			}
		}
	
		//
		return result;
	}
	
	private static void addUpDistances(Map<Individual,Integer> distances, Individual ego, int distance, int maxDistance, FiliationType filiationType){
		distance++;

		Individual father = ego.getFather();
		if (filiationType.hasLinkingGender(Gender.MALE) && father != null && (distances.get(father)==null || distances.get(father)>distance)){
			distances.put(father, distance);
			if (distance<maxDistance){
				addUpDistances(distances,father,distance,maxDistance, filiationType);
				addDownDistances(distances,father,distance,maxDistance, filiationType);
			}
		}
		Individual mother = ego.getMother();
		if (filiationType.hasLinkingGender(Gender.FEMALE) && mother != null && (distances.get(mother)==null || distances.get(mother)>distance)){
			distances.put(mother, distance);
			if (distance<maxDistance){
				addUpDistances(distances,mother,distance,maxDistance, filiationType);
				addDownDistances(distances,mother,distance,maxDistance, filiationType);
			}
		}
	}
	
	private static void addDownDistances(Map<Individual,Integer> distances, Individual ego, int distance, int maxDistance, FiliationType filiationType){
		if (filiationType.hasLinkingGender(ego.getGender())){
			distance++;
			for (Individual child : ego.children()){
				if (distances.get(child)==null || distances.get(child)>distance){
					if (!filiationType.residential() || filiationType.hasLinkingGender(child.getGender())){
						distances.put(child, distance);
						if (distance<maxDistance){
							addDownDistances(distances,child,distance,maxDistance, filiationType);
						}
					}
				}
			}
			if (filiationType.residential()){
				for (Individual spouse : ego.spouses()){
					if (distances.get(spouse)==null || distances.get(spouse)>distance){
						distances.put(spouse, distance);
					}
				}
			}
		}
	}
	
	/**
	 * Gets the most remote ancestor.
	 * 
	 * @param ego
	 * @param type
	 *            The filter criterion for ascendant lines. Only AGNATIC and
	 *            UTERINE are available.
	 * @return
	 */
	public static Individual ancestor(final Individual ego, final FiliationType type) {
		Individual result;

		result = ancestor(ego, type, new HashMap<Individual, Individual>());

		//
		return result;
	}

	/**
	 * Gets the most remote ancestor.
	 * 
	 * @param ego
	 * @param type
	 *            The filter criterion for ascendant lines. Only AGNATIC and
	 *            UTERINE are available.
	 * @return
	 */
	public static Individual ancestor(final Individual ego, final FiliationType type, final HashMap<Individual, Individual> stack) {
		Individual result;

		if ((type == null) || ((type != FiliationType.AGNATIC) && (type != FiliationType.UTERINE))) {
			result = ego;
		} else {
			//
			result = stack.get(ego);

			//
			if (result == null) {
				//
				Individual parent;
				if (ego.getOriginFamily() == null) {
					parent = null;
				} else if (type == FiliationType.AGNATIC) {
					parent = ego.getFather();
				} else if (type == FiliationType.UTERINE){
					parent = ego.getMother();
				} else {
					parent = null;
				}

				//
				if (parent == null) {
					result = ego;
				} else {
					result = ancestor(parent, type, stack);
				}

				//
				stack.put(ego, result);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param individuals
	 * @param type
	 *            The filter criterion for ascendant lines. Only AGNATIC and
	 *            UTERINE are available.
	 * @return
	 */
	public static HashMap<Individual, Individual> ancestors(final Individuals individuals, final FiliationType type) {
		HashMap<Individual, Individual> result;

		result = new HashMap<Individual, Individual>(individuals.size());

		for (Individual individual : individuals) {
			if (result.get(individual) == null) {
				ancestor(individual, type, result);
			}
		}

		return result;
	}

	/**
	 * Migration not ended.
	 * 
	 * Sets the numbers of known (agnatic, uterine and cognatic) ascendants in
	 * each ascending generation for the vertices of the Net.
	 * 
	 * @param maxAscending
	 *            the maximal number of ascending generations to be considered
	 */
	public static NumberedFiliationCountLists ascendantsCounts(final Individuals source, final int maxAscending) {
		NumberedFiliationCountLists result;

		if (source == null) {
			result = null;
		} else {
			// In case of segmentation, source could not contain all parents. So
			// we have to count them.
			Individuals extendedSource = new Individuals(source.size());
			for (Individual individual : source) {
				extendedSource.add(individual);
				for (Individual parent : individual.getParents()) {
					if (parent != null) {
						extendedSource.add(parent);
					}
				}
			}

			//
			result = new NumberedFiliationCountLists(extendedSource.size());

			//
			for (Individual individual : extendedSource) {
				//
				FiliationCounts counts = new FiliationCounts(maxAscending, maxAscending);

				//
				FiliationCount firstCount = counts.get(0);
				if (individual.getFather() != null) {
					firstCount.incAgnatic();
					firstCount.incCognatic();
				}
				if (individual.getMother() != null) {
					firstCount.incUterine();
					firstCount.incCognatic();
				}

				//
				result.put(individual.getId(), counts);
			}

			//
			for (int ascendingIndex = 1; ascendingIndex < maxAscending; ascendingIndex++) {
				for (Individual individual : source) {
					FiliationCounts counts = result.get(individual.getId());
					for (Individual parent : individual.getParents()) {
						//
						FiliationCounts parentCounts = result.get(parent.getId());
						counts.get(ascendingIndex).addCognatic(parentCounts.get(ascendingIndex - 1).getCognatic());

						//
						switch (parent.getGender()) {
							case FEMALE:
								counts.get(ascendingIndex).addUterine(parentCounts.get(ascendingIndex - 1).getUterine());
							break;
							case MALE:
								counts.get(ascendingIndex).addAgnatic(parentCounts.get(ascendingIndex - 1).getAgnatic());
							break;
						}
					}
				}
			}

		}

		//
		return result;
	}

	/**
	 * Computes the agnatic and uterine weight degree (for degrees < 10).
	 * 
	 * @return The array of agnatic and uterine weight or net weight.
	 * 
	 * @throws PuckException
	 * 
	 */
	public static FiliationCounts biasDegrees(final Individuals individuals) throws PuckException {
		FiliationCounts result;

		Partition<Individual> matridPartition = PartitionMaker.createRaw("MATRID partition", individuals, "MATRID",null);
		Partition<Individual> patridPartition = PartitionMaker.createRaw("PATRID partition", individuals, "PATRID",null);

		result = new FiliationCounts(10, 10);

		for (Individual individual : individuals) {
			//
			int uterineMin;
			Value value = matridPartition.getValue(individual);
			if ((value == null) || (value.isNotNumber())) {
				uterineMin = 0;
			} else {
				uterineMin = Math.min(9, value.intValue());
			}

			//
			int agnaticMin;
			value = patridPartition.getValue(individual);
			if ((value == null) || (value.isNotNumber())) {
				agnaticMin = 0;
			} else {
				agnaticMin = Math.min(9, value.intValue());
			}

			//
			int cognaticMin = Math.min(uterineMin, agnaticMin);

			//
			for (int degreeIndex = 0; degreeIndex < uterineMin; degreeIndex++) {
				result.get(degreeIndex + 1).incUterine();
			}
			for (int degreeIndex = 0; degreeIndex < agnaticMin; degreeIndex++) {
				result.get(degreeIndex + 1).incAgnatic();
			}
			for (int degreeIndex = 0; degreeIndex < cognaticMin; degreeIndex++) {
				result.get(degreeIndex + 1).incCognatic();
			}
		}

		//Test for detailed agnatic bias evolution
/*		FiliationCount testCount = result.get(3);
		Double diff = testCount.getAgnatic()-testCount.getUterine();
		Double nom = testCount.getAgnatic()+testCount.getUterine()-testCount.getCognatic();
		Double bias = Mat.percent(diff, nom);
		System.out.println("bias\t"+testCount.getAgnatic()+ "\t "+testCount.getUterine()+"\t "+testCount.getCognatic()+"\t "+diff+"\t "+nom+"\t "+bias);
		*/
		
		//
		return result;
	}

	/**
	 * Computes the agnatic and uterine weight or netweight of the Net (for
	 * degrees < 10).
	 * 
	 * @return The array of agnatic and uterine weight or net weight.
	 * 
	 * @throws PuckException
	 * 
	 */
	public static BIASCounts biasNetWeights(final Individuals individuals) throws PuckException {
		BIASCounts result;

		FiliationCounts counts = biasDegrees(individuals);

		//
		result = new BIASCounts(10, 10);

		//
		for (int degreeIndex = 1; degreeIndex < 10; degreeIndex++) {
			//
			FiliationCount sourceCount = counts.get(degreeIndex);
			BIASCount targetCount = result.get(degreeIndex);

			//
			double coo = sourceCount.getUterine() + sourceCount.getAgnatic() - sourceCount.getCognatic();

			//
			targetCount.setCoo(coo);
			targetCount.setUterine(MathUtils.percent(sourceCount.getUterine() - sourceCount.getCognatic(), coo));
			targetCount.setAgnatic(MathUtils.percent(sourceCount.getAgnatic() - sourceCount.getCognatic(), coo));
			targetCount.setCognatic(MathUtils.percent(0, coo));
		}

		//
		return result;
	}
	
	public static String listCircuitFrequencies (final Segmentation segmentation, String classificationType, String pattern) throws PuckException {
		String result = segmentation.getLabel(); // previously net.getLabel;

		CensusCriteria criteria = new CensusCriteria();
		criteria.setChainClassification(classificationType);
		criteria.setPattern(pattern);
		CircuitFinder finder = new CircuitFinder(segmentation,criteria);
		finder.findCircuits();
		finder.count();
		
		result += finder.listCircuitFrequencies();
		
		//
		return result;
	}
	
	public static boolean hasSameGenderMarriages (final Net net){
		boolean result;
		
		result = false;
		
		for (Family family : net.families()){
			if (family.isSameGender()){
				result = true;
				break;
			}
		}
		//
		return result;
	}
	
	public static String listBiasWeights (final Net net) throws PuckException{
		String result = net.getLabel(); 
		BIASCounts biasCounts = biasWeights(net.individuals());
		for (int i=1;i<6;i++){
			result += "\t"+biasCounts.get(i).getAgnatic();
		}
		for (int i=1;i<6;i++){
			result += "\t"+biasCounts.get(i).getUterine();
		}
		//
		return result;
	}

	/**
	 * Computes the agnatic and uterine weight or netweight of the Net (for
	 * degrees < 10).
	 * 
	 * @return The array of agnatic and uterine weight or net weight.
	 * 
	 * @throws PuckException
	 * 
	 */
	public static BIASCounts biasWeights(final Individuals individuals) throws PuckException {
		BIASCounts result;

		FiliationCounts counts = biasDegrees(individuals);

		//
		result = new BIASCounts(10, 10);

		// TODO Useful lines?
		result.get(0).setUterine(50);
		result.get(0).setAgnatic(50);
		result.get(0).setCognatic(50);

		//
		for (int degreeIndex = 1; degreeIndex < 10; degreeIndex++) {
			//
			FiliationCount sourceCount = counts.get(degreeIndex);
			BIASCount targetCount = result.get(degreeIndex);

			//
			double coo = sourceCount.getUterine() + sourceCount.getAgnatic() - sourceCount.getCognatic();

			//
			targetCount.setCoo(coo);
			targetCount.setUterine(MathUtils.percent(sourceCount.getUterine(), coo));
			targetCount.setAgnatic(MathUtils.percent(sourceCount.getAgnatic(), coo));
			targetCount.setCognatic(MathUtils.percent(sourceCount.getCognatic(), coo));
		}

		//
		return result;
	}

	/**
	 * Gets the distribution of cluster sizes
	 * 
	 * @return the distribution of cluster sizes
	 * @since 10/04/13
	 */
	public static <E> NumberedIntegers clusterSizeDistribution(final Partition<E> source) {
		NumberedIntegers result;

		if (source == null) {
			result = null;
		} else {
			//
			result = new NumberedIntegers();

			//
			for (Cluster<E> cluster : source.getClusters()) {
				if (cluster.getValue() != null) {
					Integer count = result.get(cluster.size());
					if (count == null) {
						count = 0;
					}

					result.put(cluster.size(), count + 1);
				}
			}
		}

		//
		return result;
	}

	/**
	 * Migration not ended.
	 * 
	 * Computes the average genealogical completeness of the vertices of the
	 * Net.
	 * 
	 * @param maxAscending
	 *            The maximal number of ascending generations to be considered.
	 * 
	 * @return an Array containing the average agnatic, uterine and cognatic
	 *         genealogical completeness.
	 */
	public static FiliationCounts completeness(final Individuals individuals, final int maxAscending) {
		FiliationCounts result;

		//
		NumberedFiliationCountLists ascendantsCounts = ascendantsCounts(individuals, maxAscending);

		//
		int sterileCount = 0;
		FiliationCounts ascendantSum = new FiliationCounts(maxAscending, maxAscending);
		for (Individual individual : individuals) {
			if (individual.isSterile()) {
				sterileCount += 1;
				FiliationCounts ascendantCounts = ascendantsCounts.get(individual.getId());
				for (int ascendingIndex = 0; ascendingIndex < maxAscending; ascendingIndex++) {
					ascendantSum.get(ascendingIndex).add(ascendantCounts.get(ascendingIndex));
				}
			}
		}

		//
		result = new FiliationCounts(maxAscending + 1);
		for (int ascendingIndex = 1; ascendingIndex < maxAscending + 1; ascendingIndex++) {
			if (ascendantSum.get(ascendingIndex - 1).getCognatic() != 0) {
				FiliationCount previousSum = ascendantSum.get(ascendingIndex - 1);
				FiliationCount count = result.get(ascendingIndex);

				count.setCognatic(MathUtils.percent(previousSum.getCognatic(), sterileCount * Math.pow(2,ascendingIndex)));
				count.setAgnatic(MathUtils.percent(previousSum.getAgnatic(), sterileCount));
				count.setUterine(MathUtils.percent(previousSum.getUterine(), sterileCount));
			}
		}

		//
		return result;
	}

	/**
	 * Gets the distribution of agnatic and uterine components for variable size
	 * (as an array containing the relative number of components for ascending
	 * component shares).
	 * 
	 * @param resolution
	 *            The fineness of the distribution (number of distinct columns).
	 * 
	 * @return An array containing the relative number of components (as a
	 *         percentage of total components) for ascending component shares
	 *         (component size as a percentage of network size).
	 * 
	 * @throws PuckException
	 */
	public static FiliationCounts components(final Individuals individuals, final int resolution) throws PuckException {
		FiliationCounts result;

		if (individuals == null) {
			result = null;
		} else {
			result = new FiliationCounts(resolution);

			//
			Partition<Individual> agnaticPartition = PartitionMaker.createRaw("PATRIC partition", individuals, "PATRIC",null);
			for (Cluster<Individual> cluster : agnaticPartition.getClusters()) {
				int share = MathUtils.toIntAmplified(agnaticPartition.share(cluster), resolution);
				result.get(share).incAgnatic();
			}

			//
			Partition<Individual> uterinePartition = PartitionMaker.createRaw("MATRIC partition", individuals, "MATRIC",null);
			for (Cluster<Individual> cluster : uterinePartition.getClusters()) {
				int share = MathUtils.toIntAmplified(uterinePartition.share(cluster), resolution);
				result.get(share).incUterine();
			}

			//
			for (int share = 0; share < resolution; share++) {
				FiliationCount count = result.get(share);
				count.setAgnatic(MathUtils.percent(count.getAgnatic(), agnaticPartition.size()));
				count.setUterine(MathUtils.percent(count.getUterine(), uterinePartition.size()));
			}

		}

		//
		return result;
	}
	
	/**
	 * Computes the size of the component for a given root vertex.
	 * 
	 * @param Root
	 *            the root vertex.
	 * 
	 * @return the size of the component
	 * 
	 *         see OldNet#getNrComponents()
	 */
	protected static int componentSize(final Individual root, final Set<Individual> visited) {
		int result;

		Stack<Individual> stack = new Stack<Individual>();

		result = 1;
		visited.add(root);
		stack.push(root);
		while (!stack.isEmpty()) {
			Individual individual = stack.pop();
			for (KinType kinType : KinType.basicTypes()) {
				Individuals kins = individual.getKin(kinType);
				if (!kins.isEmpty()) {
					for (Individual kin : kins) {
						if (!visited.contains(kin)) {
							visited.add(kin);
							result += 1;
							stack.push(kin);
						}
					}
				}
			}
		}
		
		//
		return result;
	}

	/**
	 * Counts the number of known ascendents of a given generational layer an
	 * intermediate method used for the computation of mean generational depth.
	 * 
	 * @param depthData
	 *            a list of numbers of known ascendants of ascending
	 *            generational layers
	 * 
	 * @param level
	 *            the generational layer
	 * 
	 *            see OldIndividual#meanDepth()
	 */
	public static void count(final Individual individual, final ArrayList<Integer> depthData, final int level) {
		if (level > depthData.size()) {
			depthData.add(0);
		}
		if (level > 0) {
			depthData.set(level - 1, depthData.get(level - 1) + 1);
		}

		Individual father = individual.getFather();
		if (father != null) {
			count(father, depthData, level + 1);
		}

		Individual mother = individual.getMother();
		if (mother != null) {
			count(mother, depthData, level + 1);
		}
	}

	/**
	 * 
	 * @param segmentation
	 * @param maxDegree
	 * @return
	 * @throws PuckException 
	 */
	public static double[][] countConsanguinePairs(final Segmentation segmentation, final int maxDegree) throws PuckException {
		double[][] result;

		result = new double[maxDegree][4];
		int[][] count = new int[maxDegree][4];

		int hommes = 0;
		int femmes = 0;
		
		CensusCriteria criteria = new CensusCriteria();
		criteria.setPattern(maxDegree+"");
		criteria.setClosingRelation("OPEN");

		CircuitFinder finder = new CircuitFinder(segmentation, criteria);
		finder.setLinkDomain(finder.getSearchDomain());
		
		Map<Individual, Map<Individual, Integer>> consanguineMap = finder.getConsanguines(maxDegree);

		for (Individual ego : segmentation.getCurrentIndividuals()) {
			if (ego.isMale()) {
				hommes++;
			} else if (ego.isFemale()) {
				femmes++;
			}

			Map<Individual, Integer> consanguines = consanguineMap.get(ego);
			for (Individual alter : consanguines.keySet()) {
				if (alter.getId() > ego.getId()) {
					int degree = consanguines.get(alter);
					if (alter.getGender() != ego.getGender()) {
						count[degree - 1][2]++;
					} else {
						count[degree - 1][alter.getGender().toInt()]++;
					}
				}
			}
		}

		for (int i = 0; i < maxDegree; i++) {
			result[i][0] = MathUtils.percent(2 * count[i][0], 100 * hommes);
			result[i][1] = MathUtils.percent(2 * count[i][1], 100 * femmes);
			result[i][2] = MathUtils.percent(count[i][2], 100 * hommes);
			result[i][3] = MathUtils.percent(count[i][2], 100 * femmes);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param individuals
	 * @param maxDegree
	 * @return
	 */
/*	public static MultiPartition<Chain> createConsanguinePairs(final Individuals individuals, final int maxDegree) {
		MultiPartition<Chain> result;

		result = new MultiPartition<Chain>();
		for (int i = 1; i <= maxDegree; i++) {
			result.put(new Partition<Chain>(), new Value(i));
		}

		CircuitFinder finder = new CircuitFinder(individuals);
		Map<Individual, Map<Individual, Integer>> consanguineMap = finder.getConsanguines(maxDegree);

		for (Individual ego : consanguineMap.keySet()) {
			Map<Individual, Integer> consanguines = consanguineMap.get(ego);
			for (Individual alter : consanguines.keySet()) {
				if (alter.getId() > ego.getId()) {
					Chain chain;
					if (alter.isMale() && ego.isFemale()) {
						chain = new Chain(alter, ego);
					} else {
						chain = new Chain(ego, alter);
					}
					int degree = consanguines.get(alter);
					result.getRow(new Value(degree)).put(chain, new Value(ego.getGender().toString() + alter.getGender().toString()));
				}
			}
		}
		//
		return result;
	}*/

	/**
	 * 
	 * @return
	 */
	public static int depth(final Individual individual) {
		int result;

		result = depth(individual, new NumberedIntegers(1000), FiliationType.COGNATIC);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static int depth(final Individual individual, final FiliationType type) {
		int result;

		result = depth(individual, new NumberedIntegers(1000), type);

		//
		return result;
	}

	/**
	 * Gets the maximal generational depth of an individual.
	 * 
	 * @param the
	 *            filter criterion for ascendant lines (0 agnatic, 1 uterine, 2
	 *            cognatic)
	 * @return the maximal generational depth
	 */
	public static int depth(final Individual individual, final NumberedIntegers depthData, final FiliationType type) {
		int result;

		Integer value = depthData.get(individual.getId());
		if (value == null) {
			if (individual.getOriginFamily() == null) {
				result = 0;
			} else {
				switch (type) {
					case AGNATIC: {
						Individual father = individual.getFather();
						if (father == null) {
							result = 0;
						} else {
							result = depth(father, depthData, type) + 1;
						}
					}
					break;
					case UTERINE: {
						Individual mother = individual.getMother();
						if (mother == null) {
							result = 0;
						} else {
							result = depth(mother, depthData, type) + 1;
						}
					}
					break;
					case COGNATIC: {
						Individual father = individual.getFather();
						Individual mother = individual.getMother();
						if (father == null && mother == null) {
							result = 0;
						} else if (father == null) {
							result = depth(mother, depthData, type) + 1;
						} else if (mother == null) {
							result = depth(father, depthData, type) + 1;
						} else {
							result = Math.max(depth(mother, depthData, type), depth(father, depthData, type)) + 1;
						}
					}
					break;
					default:
						result = 0;
				}
			}

			//
			depthData.put(individual.getId(), result);
		} else {
			result = value;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static int depth(final Individuals source) {
		int result;

		result = depthData(source, FiliationType.COGNATIC).max();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static int depth(final Net source) {
		int result;

		result = depth(source.individuals());

		//
		return result;
	}

	/**
	 * Gets the generational depth of the Net (i.e. the maximal generational
	 * depth of its vertices)
	 * 
	 * @param type
	 *            the filter criterion for ascendant lines (AGNATIC, UTERIN,
	 *            COGNATIC)
	 * 
	 * @return the generational depth of the Net
	 */
	public static int depth(final Net net, final FiliationType type) {
		int result;

		result = depthData(net, type).max();

		//
		return result;
	}

	/**
	 * 
	 * @return A list of numbered depth. The number is the individual id.
	 */
	public static NumberedIntegers depthData(final Individuals source, final FiliationType type) {
		NumberedIntegers result;

		//
		result = new NumberedIntegers(source.size());

		// As source can be a part of all individual visited, the result
		// variable is not used as stack.
		NumberedIntegers depthData = new NumberedIntegers(source.size());

		//
		for (Individual individual : source) {
			//
			Integer value = depthData.get(individual.getId());
			if (value == null) {
				value = depth(individual, depthData, type);
			}

			//
			result.put(individual.getId(), value);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static NumberedIntegers depthData(final Net source, final FiliationType type) {
		NumberedIntegers result;

		//
		result = new NumberedIntegers(source.individuals().size());

		//
		for (Individual individual : source.individuals()) {
			if (result.get(individual.getId()) == null) {
				depth(individual, result, type);
			}
		}

		//
		return result;
	}

	/**
	 * Computes the sibset distribution of a individual list.
	 * 
	 * @param individuals
	 * 
	 * @return The array of frequencies of agnatic and uterine fratries of given
	 *         size.
	 * 
	 * @throws PuckException
	 */
	public static FiliationCounts sibsetDistribution(final Individuals individuals) throws PuckException {
		FiliationCounts result;

		//
		Partition<Individual> mPartition = PartitionMaker.createRaw("SIBSETM partition", individuals, "SIBSETM",null);
		Partition<Individual> pPartition = PartitionMaker.createRaw("SIBSETP partition", individuals, "SIBSETP",null);

		NumberedIntegers mDistribution = clusterSizeDistribution(mPartition);
		NumberedIntegers pDistribution = clusterSizeDistribution(pPartition);

		int max = Math.max(Collections.max(mDistribution.keySet()), Collections.max(pDistribution.keySet()));

		result = new FiliationCounts(max, max);
		for (int countIndex = 0; countIndex < result.size(); countIndex++) {
			//
			FiliationCount count = result.get(countIndex);

			//
			Integer value = pDistribution.get(countIndex);
			if (value != null) {
				count.setAgnatic(value);
			}

			//
			value = mDistribution.get(countIndex);
			if (value != null) {
				count.setUterine(value);
			}
		}

		//
		return result;
	}

	/**
	 * @author Floriana Gargiulo
	 * 
	 * @param nstep
	 *            the number of the iteration cycle
	 * @param k
	 *            the kin relation index (-1 child, 0 spouse, 1 parent)
	 * @param adjustedAlter
	 *            the alter vertex
	 * @since 10-11-26 FG, modif. 10-11-26 KH
	 */
	private static boolean fulfillsDistanceCriteria(final KinType k, final int egoLevel, final int alterLevel) {
		boolean result;

		int maximalDistance = 3;
		if (Math.abs(egoLevel - alterLevel) > maximalDistance) {
			result = false; // maximal distance constraint
		} else if (k.genDistance() * (egoLevel - alterLevel) < 0) {
			result = false;// parents above children constraint
		} else {
			result = true;
		}

		//
		return result;
	}

	/**
	 * Gets the maximal generational depth of an individual.
	 * 
	 * @param the
	 *            filter criterion for ascendant lines (0 agnatic, 1 uterine, 2
	 *            cognatic)
	 * @return the maximal generational depth
	 */
	public static int gen(final Individual individual, final NumberedIntegers genData, final int defaultLevel, final int[] minimalLevel) {
		Integer result;

		result = genData.get(individual.getId());
		if (result == null) {
			Stack<Individual> stack = new Stack<Individual>();
			genData.put(individual.getId(), defaultLevel);
			stack.push(individual);
			while (!stack.isEmpty()) {
				setGeneration(stack.pop(), genData, minimalLevel, stack);
			}
			//
			result = genData.get(individual.getId());
		}

		//
		return result;
	}
	
	public static NumberedValues getSpouseDistances (final Families source, final String label, final Geography geography){
		NumberedValues result;
		//
		result = new NumberedValues(source.size());
		for (Family family : source){
			Double distance = FamilyValuator.getSpouseDistance(family, label, geography);
			if (distance!=null){
				result.put(family.getId(), new Value(distance));
			}
		}
		
		//
		return result;
	}
	
	public static NumberedValues getSpouseDistances (final Families source, final Individuals individuals, final String label, final Geography geography){
		NumberedValues result;
		//
		result = new NumberedValues(source.size());
		NumberedValues individualValues = IndividualValuator.get(individuals, label, geography);
		
		for (Family family : source){
			Double distance = FamilyValuator.getSpouseDistance(family, individualValues);
			if (distance!=null){
				result.put(family.getId(), new Value(distance));
			}
		}
		
		//
		return result;
	}
	
	public static double meanSpouseDistance (final Individuals individuals, final Families families, final String label, final Geography geography){
		double result;
		
		result = getSpouseDistances(families,individuals,label,geography).average();
		
		//
		return result;
	}

	

	/**
	 * 
	 * @return A list of numbered depth. The number is the individual id.
	 * <p> adapted implementation of the Pajek Algorithm for generational layers 
	 */
	public static NumberedIntegers genData(final Individuals source) {
		NumberedIntegers result;

		//
		result = new NumberedIntegers(source.size());

		// As source can be a part of all individual visited, the result
		// variable is not used as stack.
		NumberedIntegers genData = new NumberedIntegers(source.size());
		int defaultLevel = source.size() / 2;
		int[] minimalLevel = { defaultLevel };

		//
		for (Individual individual : source) {
			//
			Integer value = genData.get(individual.getId());
			if (value == null) {
				value = gen(individual, genData, defaultLevel, minimalLevel);
			}

			//
			result.put(individual.getId(), value);
		}

		int adjustment = minimalLevel[0] - 1;

		//
		for (Individual individual : source) {
			//
			Integer value = genData.get(individual.getId());
			//
			result.put(individual.getId(), value - adjustment);
		}

		//
		return result;
	}

	/**
	 * This methods generate the gender density.
	 * 
	 * @return the array containing the numbers of male and female vertices
	 */
	public static GenderedDouble genderPercentageDistribution(GenderedInt source) {
		GenderedDouble result;

		result = new GenderedDouble();
		
		int total = source.total();
		
		result.setMenValue(MathUtils.percent(source.getMenValue(), total));
		result.setWomenValue(MathUtils.percent(source.getWomenValue(), total));
		result.setUnknownValue(MathUtils.percent(source.getUnknownValue(), total));

		//
		return result;
	}


	/**
	 * Counts the male and female vertices of the Net.
	 * 
	 * @return the array containing the numbers of male and female vertices
	 */
	public static GenderedInt genderDistribution(final Individuals individuals) {
		GenderedInt result;

		result = new GenderedInt();
		for (Individual individual : individuals) {
			switch (individual.getGender()) {
				case FEMALE:
					result.incWomen();
				break;

				case MALE:
					result.incMen();
				break;

				case UNKNOWN:
					result.incUnknown();
				break;
			}
		}

		//
		return result;
	}

	/**
	 * Counts the male and female vertices of the Net.
	 * 
	 * @return the array containing the numbers of male and female vertices
	 */
	public static GenderedInt genderDistribution(final Net net) {
		GenderedInt result;

		result = genderDistribution(net.individuals());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public static Map<Cluster<Individual>, Double> meanClusterValues(final Clusters<Individual> source, final PartitionCriteria valueCriteria, final Geography geography)
			throws PuckException {
		Map<Cluster<Individual>, Double> result;

		//
		result = new HashMap<Cluster<Individual>, Double>();

		//
		Individuals individuals = new Individuals();
		for (Cluster<Individual> cluster : source) {
			individuals.put(cluster.getItems());
		}

		//
		Partition<Individual> valuesPartition = PartitionMaker.createRaw("values", individuals, valueCriteria.getLabel(), valueCriteria.getLabelParameter(), geography);

		//
		for (Cluster<Individual> cluster : source) {
			int count = 0;
			double sum = 0.;
			for (Individual individual : cluster.getItems()) {
				//
				int value = valuesPartition.getValue(individual).intValue();

				//
				if ((valueCriteria.getValueFilter() != ValueFilter.ZERO) || (value != 0)) {
					sum += valuesPartition.getValue(individual).intValue();
					count += 1;
				}
			}

			if (count == 0) {
				result.put(cluster, 0.);
			} else {
				result.put(cluster, sum / count);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 * @Deprecated
	 */
	@Deprecated
	public static Map<Cluster<Individual>, Double> meanClusterValues(final Individuals source, final PartitionCriteria partitionCriteria, final String label,
			final String labelParameter, final Geography geography) throws PuckException {
		Map<Cluster<Individual>, Double> result;

		result = new HashMap<Cluster<Individual>, Double>();

		Partition<Individual> partition = PartitionMaker.create("partition", source, partitionCriteria, geography);
		Partition<Individual> valuesPartition = PartitionMaker.createRaw("values", source, label, labelParameter, geography);

		for (Cluster<Individual> cluster : partition.getClusters()) {
			double sum = 0.;
			for (Individual individual : cluster.getItems()) {
				sum += valuesPartition.getValue(individual).intValue();
			}
			result.put(cluster, sum / cluster.count());
		}

		//
		return result;
	}

	/**
	 * Computes the mean generational depth of the pedigree, according to the
	 * formula of Cazes and Cazes 1996
	 * <p>
	 * see Cazes, Marie-Hélène et Pierre Cazes (1996),
	 * "Comment mesurer la profondeur généalogique d'une ascendance ?",
	 * Population, 51 (1), 117-140.
	 * 
	 * @return the mean generational depth
	 */
	public static double meanDepth(final Individual individual) {
		double result;

		//
		ArrayList<Integer> depthData = new ArrayList<Integer>();
		count(individual, depthData, 0);

		//
		result = 0.;
		for (int depth = 0; depth < depthData.size(); depth++) {
			int value = depthData.get(depth);
			result += MathUtils.percent(value, 100 * Math.pow(2,depth + 1));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param individuals
	 * @return
	 */
	public static double meanDepth(final Individuals individuals) {
		double result;
		
		double count = 0.;
		int number = 0;
		for (Individual individual : individuals.toSortedList()) {
			if (individual.isSterile()) {
				count += meanDepth(individual);
				number++;
			}
		}

		result = MathUtils.percent(count, 100 * number);

		//
		return result;
	}

	/**
	 * 
	 * @param individuals
	 * @param gender
	 * @return
	 */
	public static double meanSibsetSize(final Individuals individuals, final Gender gender) {
		double result;

		int count = 0;
		int number = 0;

		for (Individual individual : individuals) {
			if (individual.getGender().matchs(gender) && !individual.isSterile()) {
				count += individual.children().size();
				number++;
			}
		}

		result = MathUtils.percent(count, 100 * number);

		//
		return result;
	}

	/**
	 * 
	 * @param individuals
	 * @param gender
	 * @return
	 */
	public static double meanSibsetSize(final Families families) {
		double result;

		int count = 0;
		int number = 0;

		for (Family family : families) {
			if (family.getChildren().size()>0){
				count += family.getChildren().size();
				number++;
			}
		}

		result = MathUtils.percent(count, 100 * number);

		//
		return result;
	}

	/**
	 * 
	 * @param individuals
	 * @param gender
	 * @return
	 */
	public static double meanNumberOfSpouses(final Individuals individuals, final Gender gender) {
		double result;

		int count = 0;
		int number = 0;

		for (Individual individual : individuals) {
			if (individual.getGender() == gender) {
				int spouses = individual.spouses().size();
				if (spouses > 0) {
					count += spouses;
					number++;
				}

			}
		}

		result = MathUtils.percent(count, 100 * number);

		//
		return result;
	}

	/**
	 * Counts the number of attributes and attribute-bearing vertices (male and
	 * female) of a given label in a Net.
	 * <p>
	 * used for counting relations other than marriage
	 * 
	 * @param label
	 *            the label of the attribute
	 * @param sym
	 *            true if the attribute is a symmetric relation
	 * 
	 * @return The array of number of attributes, number of men bearing the
	 *         attribute and number of women bearing the attribute
	 * 
	 *         see io.write.TxtWriter#writeSurvey(RingGroupMap,CountMap,int)
	 */
	public static GenderedInt numberOfAttributes(final Net net, final String label, final boolean sym) {
		GenderedInt result;

		result = new GenderedInt();
		for (Individual ego : net.individuals()) {
			for (Attribute attribute : ego.attributes()) {
				if (attribute.getLabel().equals(label)) {
					result.inc();
					switch (ego.getGender()) {
						case FEMALE:
							result.incWomen();
						break;
						case MALE:
							result.incMen();
						break;
						case UNKNOWN:
							result.incUnknown();
						break;
					}
				}
			}
		}

		if (sym) {
			result.set(result.get() / 2);
		}

		//
		return result;
	}
	
	/**
	 * Gets the number of (cognatic) components as well as the size of the
	 * maximal component.
	 * 
	 * @return an integer array containing the number of components as first
	 *         element and the size of the maximal component as second element
	 * 
	 *         see OldNet#getIdentityCard()
	 */
	public static IntWithMax numberOfComponents(final Individuals source) {
		IntWithMax result;

		result = new IntWithMax();
		Set<Individual> visited = new HashSet<Individual>();
		for (Individual individual : source.toSortedList()) {
			if (!visited.contains(individual)) {
				result.inc();
				result.challengeMax(componentSize(individual, visited));
			}
		}

		//
		return result;
	}

	
	public static Partition<Individual> components (final Individuals source){
		Partition<Individual> result;
		
		result = new Partition<Individual>();
		for (Individual root : source.toSortedList()) {
			if (result.getItems().contains(root)){
				continue;
			} 			
			Value value = new Value(root);
			result.put(root, value);

			Stack<Individual> stack = new Stack<Individual>();
			stack.push(root);
			
			while (!stack.isEmpty()) {
				Individual individual = stack.pop();
				for (KinType kinType : KinType.basicTypes()) {
					Individuals kins = individual.getKin(kinType);
					if (!kins.isEmpty()) {
						for (Individual kin : kins) {
							if (!result.getCluster(value).getItems().contains(kin)) {
								result.put(kin, value);
								stack.push(kin);
							}
						}
					}
				}
			}
		}

		//
		return result;

	}



	/**
	 * Gets the number of co-spouse relations in the Net.
	 * 
	 * @param g
	 *            the gender of co-spouses (0 male - co-husbands, 1 female -
	 *            co-wives)
	 * @return the number of co-spouses
	 */
	public static int numberOfCoSpouseRelations(final Individuals source, final Gender gender) {
		int result;

		if ((gender == null) || (gender.isUnknown())) {
			result = 0;
		} else {
			result = 0;
			for (Individual commonSpouse : source) {
				if (commonSpouse.getGender() == gender.invert() && commonSpouse.isNotSingle()) {
					int coSpouses = commonSpouse.spouses().size();
					int coSpouseRelations = coSpouses * (coSpouses - 1) / 2;
					result += coSpouseRelations;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Gets the number of co-spouse relations in the Net.
	 * 
	 * @param g
	 *            the gender of co-spouses (0 male - co-husbands, 1 female -
	 *            co-wives)
	 * @return the number of co-spouses
	 */
	public static int numberOfCoSpouses(final Net net, final Gender gender) {
		int result;

		result = numberOfCoSpouseRelations(net.individuals(), gender);

		//
		return result;
	}

	/**
	 * Counts the marriage ties in the Net.
	 * 
	 * @return The number of marriage ties.
	 */
	public static int numberOfFertileMarriages(final Families families) {
		int result;

		result = 0;
		for (Family family : families) {
			if (family.hasMarried() && family.isFertile() && family.getHusband()!=null && family.getWife()!=null) {
				result++;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param marriageCount
	 * @param individualCount
	 * @return
	 */
	public static double densityOfMarriages(long marriageCount, long individualCount)
	{
		double result;
		
		result = MathUtils.percent(100 * marriageCount, individualCount * (individualCount - 1));
		
		//
		return result;
	}

	/**
	 * 
	 * @param marriageCount
	 * @param individualCount
	 * @return
	 */
	public static double densityOfFiliations(long filiationCount, long individualCount)
	{
		double result;
		
		result = MathUtils.percent(100 * filiationCount, individualCount * (individualCount - 1));
		
		//
		return result;
	}

	/**
	 * Counts the filiation ties in the Net.
	 * 
	 * @return The number of filiation ties.
	 */
	public static int numberOfFiliationTies(final Families families) {
		int result;

		result = 0;
		for (Family family : families) {
			result += family.getChildren().size() * family.numberOfParents();
		}

		//
		return result;
	}

	/**
	 * Counts the filiation ties in the Net.
	 * 
	 * @return The number of filiation ties.
	 */
	public static int numberOfFiliationTies(final Net net) {
		int result;

		result = numberOfFiliationTies(net.families());

		//
		return result;
	}

	/**
	 * Gets the number of linear ascendants or descendants of a given degree
	 * used for PEDG and PROG partitions.
	 * 
	 * @param deg
	 *            The degree of ascendance (>0) or descendance (<0).
	 * 
	 * @return The number of linear ascendants or descendants of degree deg.
	 */
	public static int numberOfLinearKin(final Individual source, final int deg) {
		int result;

		if (deg == 1) {
			result = source.getParents().size();
		} else if (deg == -1) {
			result = source.children().size();
		} else if (deg < 1) {
			result = 0;
			for (Individual individual : source.children()) {
				result += numberOfLinearKin(individual, deg + 1);
			}
		} else if (deg > 1) {
			result = 0;
			for (Individual individual : source.getParents()) {
				result += numberOfLinearKin(individual, deg - 1);
			}
		} else {
			result = 0;
		}

		//
		return result;
	}
	
	/**
	 * Counts the marriage ties in the Net.
	 * 
	 * @return The number of marriage ties.
	 */
	public static int numberOfMarriages(final Families families) {
		int result;

		result = 0;
		for (Family family : families) {
			if (family.hasMarried() && family.getHusband()!=null && family.getWife()!=null) {
				result++;
			}
		}

		//
		return result;
	}
	
	/**
	 * Counts the unions in the Net.
	 * 
	 * @return The number of marriage ties.
	 */
	public static int numberOfUnions(final Families families) {
		int result;

		result = 0;
		for (Family family : families) {
			if (family.getHusband()!=null && family.getWife()!=null) {
				result++;
			}
		}

		//
		return result;
	}
	
	/**
	 * Counts the marriage ties in the Net.
	 * 
	 * @return The number of marriage ties.
	 */
	public static int numberOfPartnerships(final Families families) {
		int result;

		result = 0;
		for (Family family : families) {
			if (family.getHusband()!=null && family.getWife()!=null) {
				result++;
			}
		}

		//
		return result;
	}
	

	/**
	 * Gets the number of...
	 * 
	 * @param g
	 *            the selected gender
	 * @return the number of vertices of the required gender (and matrimonial
	 *         status)
	 */
	public static int numberOfNotSingles(final Individuals source, final Gender gender) {
		int result;

		result = 0;
		for (Individual individual : source) {
			if (individual.getGender() == gender && individual.isNotSingle()) {
				result += 1;
			}
		}

		//
		return result;
	}

	/**
	 * Gets the number of individuals.
	 * 
	 * @param g
	 *            the selected gender
	 * @return the number of vertices of the required gender (and matrimonial
	 *         status)
	 */
	public static int numberOfNotSingles(final Net net, final Gender gender) {
		int result;

		result = numberOfNotSingles(net.individuals(), gender);

		//
		return result;
	}

	/**
	 * A filter for generation assignment, which is postponed when the return is
	 * true.
	 * 
	 * @author Klaus
	 * @param nstep
	 *            the number of the iteration cycle
	 * @param kinType
	 *            the kin relation index (-1 child, 0 spouse, 1 parent)
	 * @param adjustedAlter
	 *            the vertex whose generation shall be assigned
	 * @param constraint
	 *            the index of the constraint chosen
	 * @return true if generation assignment shall be postponed
	 * @since 10-11-26
	 */
	private static boolean postponeGenerationAssignment(final KinType kinType, final int egoLevel, final int alterLevel, final NumberedIntegers genData,
			final int constraint) {
		boolean result;

		switch (constraint) {
			case 0:
				result = false;
			break;
			case 1:
				result = fulfillsDistanceCriteria(kinType, egoLevel, alterLevel);
			break;
			default:
				result = true;
		}

		//
		return result;
	}

	/**
	 * sets the generational depth of the neighbors of the vertex
	 * 
	 * @param minimalLevel
	 *            the minimal generational level (as a pseudo-array with only
	 *            one field)
	 * @param stack
	 *            the stack of neighbors still to be examined
	 * @since last modification 10-05-14, 10-11-26 KH
	 */
	private static void setGeneration(final Individual individual, final NumberedIntegers genData, final int[] minimalLevel, final Stack<Individual> stack) {
		int constraint = 0; // change this index according to the constraint
							// chosen
		Integer egoLevel = genData.get(individual.getId());
		for (KinType type : KinType.basicTypes()) {
			if (individual.getKin(type) != null) {
				for (Individual relative : individual.getKin(type)) {
					if (relative != null) {
						if (genData.get(relative.getId()) == null) {
							int alterLevel = egoLevel - type.genDistance();
							if (!postponeGenerationAssignment(type, egoLevel, alterLevel, genData, constraint)) {
								genData.put(relative.getId(), alterLevel);
								if (minimalLevel[0] > alterLevel) {
									minimalLevel[0] = alterLevel;
								}
								stack.push(relative);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Gets the number of vertices of a given gender,
	 * 
	 * @param g
	 *            the selected gender
	 * @return the number of vertices of the required gender (and matrimonial
	 *         status)
	 * 
	 *         see OldNet#getIdentityCard() see OldNet#getNrMarried(int, int)
	 *         see OldNet#size(int)
	 */
	public static int size(final Individuals individuals, final Gender gender) {
		int result;

		result = 0;
		for (Individual individual : individuals) {
			if (individual.getGender().matchs(gender)) {
				result += 1;
			}
		}

		//
		return result;
	}

	/**
	 * Gets the number of vertices of a given gender,
	 * 
	 * @param g
	 *            the selected gender
	 * @return the number of vertices of the required gender (and matrimonial
	 *         status)
	 */
	public static int size(final Net net, final Gender gender) {
		int result;

		result = size(net.individuals(), gender);

		//
		return result;
	}
	
	
	/**
	 * 
	 * @param marriageCount
	 * @return
	 */
	public static long cyclomatic(int marriageCount, int filiationCount, int individualCount, int componentCount)
	{
		long result;
	
		result =  marriageCount + filiationCount - individualCount + componentCount;
		
		//
		return result;
	}
	
	
}
