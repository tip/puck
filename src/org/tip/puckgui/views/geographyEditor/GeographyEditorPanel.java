package org.tip.puckgui.views.geographyEditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.text.JTextComponent;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.FlatDB4GeoNames;
import org.tip.flatdb4geonames.model.FlatDB4GeoNamesException;
import org.tip.flatdb4geonames.model.GeoNamesLine;
import org.tip.flatdb4geonames.model.GeoNamesLines;
import org.tip.flatdb4geonames.model.GeoNamesSearchCriteria;
import org.tip.flatdb4geonames.model.index.FeatureClass;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.geo.Geography;
import org.tip.puck.geo.Place;
import org.tip.puck.geo.Places;
import org.tip.puck.geo.Geography.Status;
import org.tip.puck.geo.io.GEOFile;
import org.tip.puck.net.workers.AttributeValueDescriptor;
import org.tip.puck.net.workers.AttributeValueDescriptors;
import org.tip.puck.net.workers.AttributeWorker;
import org.tip.puck.report.Report;
import org.tip.puck.statistics.StatisticsReporter;
import org.tip.puck.util.ToolBox;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.util.ProgressStatus;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.StringList;

public class GeographyEditorPanel extends JPanel {

	private static final long serialVersionUID = -5236087853282989572L;

	public enum Filter {
		ALL,
		BLANK,
		CONFLICTUAL,
		UNGEOCODED,
		UNUSED
	}

	private static final Logger logger = LoggerFactory.getLogger(GeographyEditorPanel.class);

	private NetGUI netGUI;
	private Filter filter;

	private AttributeValueDescriptors valueDescriptors;
	private JTable tblPlaceLines;
	private final ButtonGroup buttonGroupFilter = new ButtonGroup();
	private JLabel lblStatusValue;
	private JLabel lblConflictedValue;

	private JLabel lblUngeocodedValue;
	private JLabel lblSelectionValue;
	private JTable tblResults;
	private JTextField txtfldInput;
	private JCheckBox chckbxCountryStateRegion;
	private JCheckBox chckbxStreamLake;
	private JCheckBox chckbxParkArea;
	private JCheckBox chckbxCityVillage;
	private JCheckBox chckbxSpotBuildingFarm;
	private JCheckBox chckbxMountainHillRock;
	private JCheckBox chckbxUndersea;
	private JCheckBox chckbxForestHeath;
	private JButton btnSearch;
	private JButton btnSearchToponyms;
	private JButton btnSearchToponym;
	private JButton btnAutoFill;

	/**
	 * Create the panel.
	 */
	public GeographyEditorPanel(final NetGUI parentGUI) {

		// Initialize specific variables.
		this.netGUI = parentGUI;

		if (parentGUI.getNet().getGeography() == null) {
			parentGUI.getNet().setGeography(new Geography());
		}

		this.filter = Filter.ALL;
		this.valueDescriptors = AttributeWorker.getExogenousAttributeValueDescriptors(parentGUI.getNet(), ".*_PLAC.*");

		// /////////////////////////////////////////////

		setLayout(new BorderLayout(0, 0));

		JPanel panelContent = new JPanel();
		add(panelContent);
		panelContent.setLayout(new BoxLayout(panelContent, BoxLayout.Y_AXIS));

		JSplitPane splitPane = new JSplitPane();
		panelContent.add(splitPane);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);

		JPanel panelGeography = new JPanel();
		panelGeography.setMinimumSize(new Dimension(10, 300));
		splitPane.setLeftComponent(panelGeography);
		panelGeography.setLayout(new BorderLayout(0, 0));

		JPanel panelControls = new JPanel();
		panelControls.setBorder(new EmptyBorder(0, 0, 5, 0));
		panelGeography.add(panelControls, BorderLayout.NORTH);
		panelControls.setLayout(new BoxLayout(panelControls, BoxLayout.X_AXIS));

		JPanel panelStats = new JPanel();
		panelStats.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Stats", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelControls.add(panelStats);
		panelStats.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblStatus = new JLabel("Status:");
		panelStats.add(lblStatus, "2, 2, right, default");

		this.lblStatusValue = new JLabel("");
		this.lblStatusValue.setOpaque(true);
		panelStats.add(this.lblStatusValue, "4, 2");

		JLabel lblConflicted = new JLabel("Conflicted:");
		panelStats.add(lblConflicted, "2, 4, right, default");

		this.lblConflictedValue = new JLabel("");
		panelStats.add(this.lblConflictedValue, "4, 4");

		JLabel lblUngeocoded = new JLabel("Ungeocoded:");
		panelStats.add(lblUngeocoded, "2, 6, right, default");

		this.lblUngeocodedValue = new JLabel("");
		panelStats.add(this.lblUngeocodedValue, "4, 6");

		JPanel panelButtons = new JPanel();
		panelControls.add(panelButtons);
		panelButtons.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Actions", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelButtons.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JButton btnClearAll = new JButton("Clear All");
		btnClearAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Clear all.
				String message = "CAUTION: this action will delete all places.\n\nDo you really want delete all places?";
				int answer = JOptionPane.showConfirmDialog(GeographyEditorPanel.this, message, "Confirm dialog", JOptionPane.OK_CANCEL_OPTION);

				if (answer == JOptionPane.OK_OPTION) {
					//
					Geography geography = parentGUI.getNet().getGeography();
					geography.clear();
					updatePlaceLines();

					//
					// refreshStats();
					// ((PlaceLineModel)
					// GeographyEditorPanel.this.tblPlaceLines.getModel()).fireTableDataChanged();
				}
			}
		});
		panelButtons.add(btnClearAll, "2, 2");

		JButton btnImport = new JButton("Import…");
		btnImport.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Button import.
				try {

					File sourceFile = GeographyImportFileSelector.showSelectorDialog(GeographyEditorPanel.this, parentGUI.getFile().getParentFile());

					if (sourceFile != null) {

						Geography geography = GEOFile.load(sourceFile);

						if (geography != null) {

							logger.debug("geography.place count=" + geography.countOfPlaces());

							parentGUI.getNet().setGeography(geography);

							GeographyEditorPanel.this.updatePlaceLines();

							parentGUI.setChanged(true);
						}
					}

				} catch (final PuckException exception) {
					//
					exception.printStackTrace();

					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case FILE_NOT_FOUND:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.fileNotFound");
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						break;
						case BAD_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.badFileFormat");
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						break;
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.unsupportedFileFormat");
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.notAFile");
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.unsupportedEncoding");
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.default");
							JOptionPane.showMessageDialog(GeographyEditorPanel.this, message, title, JOptionPane.ERROR_MESSAGE);
					}
				} catch (final Exception exception) {
					//
					exception.printStackTrace();

					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.title");
					String message = "Error loading file.";
					JOptionPane.showMessageDialog(GeographyEditorPanel.this, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JButton btnAdd = new JButton("Add from attributes");
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Button Add from attributes.
				StringList message = new StringList();
				message.append("<html>");
				message.append("<br/>");
				message.append("<p>This action will add places automatically.</p>");
				message.append("<p>Places are added exploring attributes from individuals, <br/>");
				message.append("families and relations of the current net.</p>");
				message.append("<p>For each attribute with label containing \"_PLAC\" pattern,<br/> a place will be added. ");
				message.append("If a place is already existing<br/> then its uses counter is incremented.<p/>");
				message.append("<br/>");
				message.append("<p>Run auto add from attributes now?</p>");
				message.append("</html>");

				int answer = JOptionPane.showConfirmDialog(GeographyEditorPanel.this, message.toString(), "Confirm dialog", JOptionPane.OK_CANCEL_OPTION);

				if (answer == JOptionPane.OK_OPTION) {
					//
					Geography geography = parentGUI.getNet().getGeography();

					//
					int count = 0;
					for (AttributeValueDescriptor valueDescriptor : GeographyEditorPanel.this.valueDescriptors) {

						if (!geography.contains(valueDescriptor.getValue())) {

							geography.addPlace(new Place(valueDescriptor.getValue()));
							count += 1;
						}
					}

					//
					((PlaceLineModel) GeographyEditorPanel.this.tblPlaceLines.getModel()).setSource(geography.getPlaces());

					//
					refreshStats();
					((PlaceLineModel) GeographyEditorPanel.this.tblPlaceLines.getModel()).fireTableDataChanged();

					//
					String title = "Action done";
					String endMessage = "Added places: " + count;
					JOptionPane.showMessageDialog(GeographyEditorPanel.this, endMessage, title, JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});

		JButton btnNewPlace = new JButton("New Place");
		btnNewPlace.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Button new place.
				if (((PlaceLineModel) GeographyEditorPanel.this.tblPlaceLines.getModel()).isSet()) {
					// Add PlaceLine.

					Place newPlace = GeographyEditorPanel.this.netGUI.getNet().getGeography().addPlace(new Place("New place"));

					if (GeographyEditorPanel.this.tblPlaceLines.getCellEditor() != null) {

						GeographyEditorPanel.this.tblPlaceLines.getCellEditor().stopCellEditing();
					}

					((PlaceLineModel) GeographyEditorPanel.this.tblPlaceLines.getModel()).setNewItem(newPlace);

					GeographyEditorPanel.this.tblPlaceLines.scrollRectToVisible(GeographyEditorPanel.this.tblPlaceLines.getCellRect(
							GeographyEditorPanel.this.tblPlaceLines.getModel().getRowCount() - 1, 0, true));
					GeographyEditorPanel.this.tblPlaceLines.setRowSelectionInterval(GeographyEditorPanel.this.tblPlaceLines.getModel().getRowCount() - 1,
							GeographyEditorPanel.this.tblPlaceLines.getModel().getRowCount() - 1);
					GeographyEditorPanel.this.tblPlaceLines.setColumnSelectionInterval(0, 0);
					GeographyEditorPanel.this.tblPlaceLines.editCellAt(GeographyEditorPanel.this.tblPlaceLines.getModel().getRowCount() - 1, 0);
					GeographyEditorPanel.this.tblPlaceLines.getEditorComponent().requestFocus();

					refreshStats();
				}
			}
		});
		panelButtons.add(btnNewPlace, "4, 2");
		panelButtons.add(btnAdd, "6, 2");
		panelButtons.add(btnImport, "8, 2");

		JButton btnExport = new JButton("Export…");
		btnExport.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Button export.
				try {
					//
					String targetFileName;
					if (parentGUI.getFile() == null) {

						targetFileName = "untitled.geo.csv";
					} else {

						targetFileName = FilenameUtils.removeExtension(parentGUI.getFile().getAbsolutePath()) + ".geo.csv";
					}

					File targetFile = GeographyExportFileSelector.showSelectorDialog(GeographyEditorPanel.this, new File(targetFileName));

					if (targetFile != null) {
						//
						GEOFile.save(targetFile, parentGUI.getNet().getGeography());
					}

				} catch (final PuckException exception) {
					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedFileFormat");
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.notAFile");
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedEncoding");
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.default");
					}

					//
					JOptionPane.showMessageDialog(GeographyEditorPanel.this, message, title, JOptionPane.ERROR_MESSAGE);

				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(GeographyEditorPanel.this, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		this.btnAutoFill = new JButton("Auto Fill…");
		this.btnAutoFill.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Button Auto Fill.
				AutoFillProgressDialog.showDialog(GeographyEditorPanel.this, ((PlaceLineModel) GeographyEditorPanel.this.tblPlaceLines.getModel()).getSource());

				//
				refreshStats();
				((PlaceLineModel) GeographyEditorPanel.this.tblPlaceLines.getModel()).fireTableDataChanged();

			}
		});

		JButton btnDeletePlace = new JButton("Delete Place");
		btnDeletePlace.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Button delete place.
				logger.debug("Delete place" + ArrayUtils.toString(GeographyEditorPanel.this.tblPlaceLines.getSelectedRows()));

				if (GeographyEditorPanel.this.tblPlaceLines.getCellEditor() != null) {

					GeographyEditorPanel.this.tblPlaceLines.getCellEditor().stopCellEditing();
				}

				int[] selectedRowIds = GeographyEditorPanel.this.tblPlaceLines.getSelectedRows();
				if (selectedRowIds.length > 0) {
					ArrayUtils.reverse(selectedRowIds);
					for (int rowIndex : selectedRowIds) {

						Place deletedPlace = ((PlaceLineModel) GeographyEditorPanel.this.tblPlaceLines.getModel()).removeItem(rowIndex);
						GeographyEditorPanel.this.netGUI.getNet().getGeography().removePlace(deletedPlace);
					}

					GeographyEditorPanel.this.tblPlaceLines.changeSelection(selectedRowIds[0], 0, false, false);
				}

				refreshStats();
			}
		});

		JButton btnStatistics = new JButton("Statistics");
		btnStatistics.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Statistics button.
				Report report = StatisticsReporter.reportGeographyStatistics(parentGUI.getNet());
				parentGUI.addReportTab(report);
			}
		});
		panelButtons.add(btnStatistics, "2, 4");
		panelButtons.add(btnDeletePlace, "4, 4");
		panelButtons.add(this.btnAutoFill, "6, 4");
		panelButtons.add(btnExport, "8, 4");

		JPanel panelFilters = new JPanel();
		panelControls.add(panelFilters);
		panelFilters.setBorder(new TitledBorder(null, "Filters", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelFilters.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("max(50dlu;default)"),
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), }));

		JRadioButton rdbtnAll = new JRadioButton("All");
		rdbtnAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				setFilter(Filter.ALL);
			}
		});
		rdbtnAll.setSelected(true);
		this.buttonGroupFilter.add(rdbtnAll);
		panelFilters.add(rdbtnAll, "2, 2");

		JRadioButton rdbtnConflictual = new JRadioButton("Conflictual");
		rdbtnConflictual.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				setFilter(Filter.CONFLICTUAL);
			}
		});
		this.buttonGroupFilter.add(rdbtnConflictual);
		panelFilters.add(rdbtnConflictual, "4, 2");

		JRadioButton rdbtnBlank = new JRadioButton("Blank");
		rdbtnBlank.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				setFilter(Filter.BLANK);
			}
		});

		JRadioButton rdbtnUnused = new JRadioButton("Unused");
		rdbtnUnused.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				setFilter(Filter.UNUSED);
			}
		});
		this.buttonGroupFilter.add(rdbtnUnused);
		panelFilters.add(rdbtnUnused, "6, 2");
		this.buttonGroupFilter.add(rdbtnBlank);
		panelFilters.add(rdbtnBlank, "2, 4");

		JRadioButton rdbtnUngeocoded = new JRadioButton("Ungeocoded");
		rdbtnUngeocoded.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				setFilter(Filter.UNGEOCODED);
			}
		});
		this.buttonGroupFilter.add(rdbtnUngeocoded);
		panelFilters.add(rdbtnUngeocoded, "4, 4");

		JPanel panel = new JPanel();
		panelFilters.add(panel, "2, 6, 5, 1, fill, fill");
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

		JLabel lblSelection = new JLabel("Selection:");
		panel.add(lblSelection);

		this.lblSelectionValue = new JLabel("");
		panel.add(this.lblSelectionValue);

		JScrollPane scrollPanePlaces = new JScrollPane();
		panelGeography.add(scrollPanePlaces, BorderLayout.CENTER);
		scrollPanePlaces.setMaximumSize(new Dimension(600, 32767));
		scrollPanePlaces.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPanePlaces.setAlignmentX(Component.LEFT_ALIGNMENT);

		this.tblPlaceLines = new JTable() {
			// To select all text when enter edit of a cell.
			@Override
			public boolean editCellAt(final int row, final int column, final java.util.EventObject event) {
				boolean result;

				result = super.editCellAt(row, column, event);

				final Component editor = getEditorComponent();

				if ((editor != null) && (editor instanceof JTextComponent)) {

					if (event == null) {

						((JTextComponent) editor).selectAll();

					} else if (event instanceof KeyEvent) {
						// Typing in the cell was used to activate the editor
						((JTextComponent) editor).selectAll();

					} else if (event instanceof ActionEvent) {
						// F2 was used to activate the editor
						((JTextComponent) editor).selectAll();
					} else if (event instanceof MouseEvent) {
						// A mouse click was used to activate the editor.
						// Generally this is a double click and the second mouse
						// click is
						// passed to the editor which would remove the text
						// selection unless
						// we use the invokeLater()

						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								((JTextComponent) editor).selectAll();
							}
						});
					}
				}

				//
				return result;
			}
		};
		this.tblPlaceLines.setCellSelectionEnabled(true);
		this.tblPlaceLines.setFillsViewportHeight(true);
		this.tblPlaceLines.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPanePlaces.setViewportView(this.tblPlaceLines);
		this.tblPlaceLines.setModel(new PlaceLineModel(parentGUI.getNet().getGeography().getPlaces(), this.valueDescriptors));
		this.tblPlaceLines.getModel().addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(final TableModelEvent event) {
				GeographyEditorPanel.this.netGUI.setChanged(true);
				GeographyEditorPanel.this.netGUI.getNet().getGeography().rebuildIndexes();
				refreshStats();
			}
		});

		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);

		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_USES).setMinWidth(50);
		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_USES).setMaxWidth(50);
		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_USES).setResizable(false);

		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_LATITUDE).setMinWidth(90);
		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_LATITUDE).setMaxWidth(90);
		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_LATITUDE).setResizable(false);
		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_LATITUDE).setCellRenderer(new CoordinateValueRenderer());

		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_LONGITUDE).setMinWidth(90);
		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_LONGITUDE).setMaxWidth(90);
		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_LONGITUDE).setResizable(false);
		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_LONGITUDE).setCellRenderer(new CoordinateValueRenderer());

		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_ELEVATION).setMinWidth(50);
		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_ELEVATION).setMaxWidth(50);
		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_ELEVATION).setResizable(false);

		TableCellEditor editor = new DefaultCellEditor(new JComboBox(GeoLevel.values()));
		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_LEVEL).setCellEditor(editor);
		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_LEVEL).setCellRenderer(centerRenderer);
		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_LEVEL).setMinWidth(100);
		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_LEVEL).setMaxWidth(130);
		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_LEVEL).setResizable(true);

		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_EXTRA_DATA).setMinWidth(50);
		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_EXTRA_DATA).setPreferredWidth(80);
		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_EXTRA_DATA).setMaxWidth(200);
		this.tblPlaceLines.getColumnModel().getColumn(PlaceLineModel.COLUMN_EXTRA_DATA).setResizable(true);

		JPanel panelGeoNames = new JPanel();
		panelGeoNames.setBorder(new TitledBorder(null, "GeoNames", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		splitPane.setRightComponent(panelGeoNames);
		panelGeoNames.setLayout(new BorderLayout(0, 0));

		JPanel panelGeonamesControls = new JPanel();
		panelGeoNames.add(panelGeonamesControls, BorderLayout.NORTH);
		panelGeonamesControls.setLayout(new BoxLayout(panelGeonamesControls, BoxLayout.X_AXIS));

		JPanel panelGeonamesButtons = new JPanel();
		panelGeonamesControls.add(panelGeonamesButtons);
		panelGeonamesButtons.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, },
				new RowSpec[] { FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, }));

		this.btnSearchToponym = new JButton("Search Toponym");
		this.btnSearchToponym.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Button search toponym.
				Place place = getSelectedPlace();
				if (place == null) {

					String title = "Search Issue";
					String message = "Unselected place. Please select a place in Geography table.";
					JOptionPane.showMessageDialog(GeographyEditorPanel.this, message, title, JOptionPane.ERROR_MESSAGE);
				} else {

					GeographyEditorPanel.this.txtfldInput.setText(place.getToponym());
					performSearch();
				}
			}
		});
		panelGeonamesButtons.add(this.btnSearchToponym, "2, 2, default, top");

		JPanel panelFeatureClass = new JPanel();
		panelFeatureClass.setBorder(new TitledBorder(null, "Feature Class", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelGeonamesButtons.add(panelFeatureClass, "4, 2, 1, 5, fill, fill");
		panelFeatureClass.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		this.chckbxCountryStateRegion = new JCheckBox("Country State Region");
		this.chckbxCountryStateRegion.setSelected(true);
		panelFeatureClass.add(this.chckbxCountryStateRegion, "2, 2");

		this.chckbxParkArea = new JCheckBox("Park Area");
		panelFeatureClass.add(this.chckbxParkArea, "4, 2");

		this.chckbxSpotBuildingFarm = new JCheckBox("Spot Building Farm");
		panelFeatureClass.add(this.chckbxSpotBuildingFarm, "6, 2");

		this.chckbxUndersea = new JCheckBox("Undersea");
		panelFeatureClass.add(this.chckbxUndersea, "8, 2");

		this.chckbxStreamLake = new JCheckBox("Stream Lake");
		panelFeatureClass.add(this.chckbxStreamLake, "2, 4");

		this.chckbxCityVillage = new JCheckBox("City Village");
		panelFeatureClass.add(this.chckbxCityVillage, "4, 4");

		this.chckbxMountainHillRock = new JCheckBox("Mountain Hill Rock");
		panelFeatureClass.add(this.chckbxMountainHillRock, "6, 4");

		this.chckbxForestHeath = new JCheckBox("Forest Heath");
		panelFeatureClass.add(this.chckbxForestHeath, "8, 4");

		JButton btnSetCoordinate = new JButton("Set Coordinate");
		btnSetCoordinate.setToolTipText("Set Coordinate");
		btnSetCoordinate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Button Set Coordinate.
				GeoNamesLine result = getSelectedResult();
				Place place = getSelectedPlace();

				if ((result == null) && (((GeoNamesLineModel) GeographyEditorPanel.this.tblResults.getModel()).getRowCount() == 1)) {
					//
					result = ((GeoNamesLineModel) GeographyEditorPanel.this.tblResults.getModel()).getSource().get(0);
				}

				if (result == null) {
					String title = "Set Issue";
					String message = "Unselected result. Please select a result in GeoNames table.";
					JOptionPane.showMessageDialog(GeographyEditorPanel.this, message, title, JOptionPane.ERROR_MESSAGE);

				} else if (place == null) {

					String title = "Set Issue";
					String message = "Unselected place. Please select a place in Geography table.";
					JOptionPane.showMessageDialog(GeographyEditorPanel.this, message, title, JOptionPane.ERROR_MESSAGE);

				} else {
					//
					place.setLatitude(result.getLatitude());
					place.setLongitude(result.getLongitude());
					place.setElevation(result.getElevation());

					//
					int rowIndex = GeographyEditorPanel.this.tblPlaceLines.getSelectedRow();
					if (rowIndex != -1) {
						((PlaceLineModel) GeographyEditorPanel.this.tblPlaceLines.getModel()).fireTableRowsUpdated(rowIndex, rowIndex);
					}
				}
			}
		});
		panelGeonamesButtons.add(btnSetCoordinate, "6, 2");

		this.btnSearchToponyms = new JButton("Search Toponyms");
		this.btnSearchToponyms.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Button search toponyms.
				Place place = getSelectedPlace();
				if (place == null) {

					String title = "Search Issue";
					String message = "Unselected place. Please select a place in Geography table.";
					JOptionPane.showMessageDialog(GeographyEditorPanel.this, message, title, JOptionPane.ERROR_MESSAGE);

				} else {

					GeographyEditorPanel.this.txtfldInput.setText(place.getToponyms().toStringWithCommas());
					performSearch();
				}
			}
		});
		panelGeonamesButtons.add(this.btnSearchToponyms, "2, 4");

		this.txtfldInput = new JTextField();
		this.txtfldInput.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent event) {
				//
				if (event.getKeyCode() == KeyEvent.VK_ENTER) {
					logger.debug("enter pressed");
					GeographyEditorPanel.this.btnSearch.doClick();
				}
			}
		});

		JButton btnSetAll = new JButton("Set All");
		btnSetAll.setToolTipText("Set Coordinate, Homonyms and Toponym");
		btnSetAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Button Set All.
				GeoNamesLine result = getSelectedResult();
				Place place = getSelectedPlace();

				if ((result == null) && (((GeoNamesLineModel) GeographyEditorPanel.this.tblResults.getModel()).getRowCount() == 1)) {
					//
					result = ((GeoNamesLineModel) GeographyEditorPanel.this.tblResults.getModel()).getSource().get(0);
				}

				if (result == null) {
					String title = "Set Issue";
					String message = "Unselected result. Please select a result in GeoNames table.";
					JOptionPane.showMessageDialog(GeographyEditorPanel.this, message, title, JOptionPane.ERROR_MESSAGE);

				} else if (place == null) {

					String title = "Set Issue";
					String message = "Unselected place. Please select a place in Geography table.";
					JOptionPane.showMessageDialog(GeographyEditorPanel.this, message, title, JOptionPane.ERROR_MESSAGE);

				} else {
					//
					place.setToponym(result.getName());
					place.setAlternateToponyms(result.getAlternateNames());
					place.setLatitude(result.getLatitude());
					place.setLongitude(result.getLongitude());
					place.setElevation(result.getElevation());
					place.setExtraData(buildExtraDataValue(result));

					//
					int rowIndex = GeographyEditorPanel.this.tblPlaceLines.getSelectedRow();
					if (rowIndex != -1) {
						((PlaceLineModel) GeographyEditorPanel.this.tblPlaceLines.getModel()).fireTableRowsUpdated(rowIndex, rowIndex);
					}
				}
			}
		});

		JButton btnSetData = new JButton("Set Data");
		btnSetData.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Button set data.
				GeoNamesLine result = getSelectedResult();
				Place place = getSelectedPlace();

				if ((result == null) && (((GeoNamesLineModel) GeographyEditorPanel.this.tblResults.getModel()).getRowCount() == 1)) {
					//
					result = ((GeoNamesLineModel) GeographyEditorPanel.this.tblResults.getModel()).getSource().get(0);
				}

				if (result == null) {
					String title = "Set Issue";
					String message = "Unselected result. Please select a result in GeoNames table.";
					JOptionPane.showMessageDialog(GeographyEditorPanel.this, message, title, JOptionPane.WARNING_MESSAGE);

				} else if (place == null) {

					String title = "Set Issue";
					String message = "Unselected place. Please select a place in Geography table.";
					JOptionPane.showMessageDialog(GeographyEditorPanel.this, message, title, JOptionPane.WARNING_MESSAGE);

				} else {
					//
					place.setAlternateToponyms(result.getAlternateNames());
					place.setLatitude(result.getLatitude());
					place.setLongitude(result.getLongitude());
					place.setElevation(result.getElevation());
					place.setExtraData(buildExtraDataValue(result));

					//
					int rowIndex = GeographyEditorPanel.this.tblPlaceLines.getSelectedRow();
					if (rowIndex != -1) {
						((PlaceLineModel) GeographyEditorPanel.this.tblPlaceLines.getModel()).fireTableRowsUpdated(rowIndex, rowIndex);
					}
				}
			}
		});
		btnSetData.setToolTipText("Set Coordinate and Homonyms");
		panelGeonamesButtons.add(btnSetData, "6, 4");
		panelGeonamesButtons.add(btnSetAll, "6, 6");
		panelGeonamesButtons.add(this.txtfldInput, "4, 8, fill, default");
		this.txtfldInput.setColumns(10);

		this.btnSearch = new JButton("Search");
		this.btnSearch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Button search.
				performSearch();

			}
		});
		panelGeonamesButtons.add(this.btnSearch, "2, 8");

		JScrollPane scrollPaneResults = new JScrollPane();
		scrollPaneResults.setPreferredSize(new Dimension(3, 300));
		panelGeoNames.add(scrollPaneResults, BorderLayout.CENTER);
		scrollPaneResults.setAlignmentX(Component.LEFT_ALIGNMENT);
		scrollPaneResults.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		this.tblResults = new JTable() {
			/**
			 * Implements table cell tool tips.
			 */
			@Override
			public String getToolTipText(final MouseEvent event) {
				String result = null;

				java.awt.Point p = event.getPoint();
				int rowIndex = rowAtPoint(p);
				int colIndex = columnAtPoint(p);
				int realColumnIndex = convertColumnIndexToModel(colIndex);

				GeoNamesLine line;
				if (rowIndex == -1) {

					line = null;
				} else {

					line = ((GeoNamesLineModel) GeographyEditorPanel.this.tblResults.getModel()).getSource().get(rowIndex);
				}

				if (line == null) {

					result = super.getToolTipText(event);

				} else {
					switch (realColumnIndex) {
						case GeoNamesLineModel.COLUMN_TOPONYM:
							result = line.getName();
						break;

						case GeoNamesLineModel.COLUMN_TIMEZONE:
							result = line.getTimeZone();
						break;

						case GeoNamesLineModel.COLUMN_ALTERNATE_NAMES:
							if (line.getAlternateNames().isEmpty()) {
								result = super.getToolTipText(event);
							} else {
								result = line.getAlternateNames().toStringList().toStringWithCommas();
							}
						break;

						case GeoNamesLineModel.COLUMN_FEATURE:
							if (FlatDB4GeoNames.isOpened()) {

								StringList html = new StringList();
								html.append("<html>");
								html.append(line.getFeatureCodePath()).append("<br/>");
								html.append(FlatDB4GeoNames.instance().searchFeatureShortDescription(line.getFeatureCodePath())).append("<br/>");
								html.append(FlatDB4GeoNames.instance().searchFeatureDescription(line.getFeatureCodePath()));
								html.append("</html>");

								result = html.toString();

							} else {
								result = super.getToolTipText(event);
							}
						break;

						case GeoNamesLineModel.COLUMN_ADMINCODES:
							if (FlatDB4GeoNames.isOpened()) {

								String description = FlatDB4GeoNames.instance().searchAdministrativeToponymPath(line.getAdministrativeShortCodePath());

								if (description == null) {
									result = super.getToolTipText(event);
								} else {
									result = description;
								}
							} else {
								result = super.getToolTipText(event);
							}
						break;

						default:
							result = super.getToolTipText(event);
					}
				}

				//
				return result;
			}
		};
		this.tblResults.setFillsViewportHeight(true);
		scrollPaneResults.setViewportView(this.tblResults);
		this.tblResults.setModel(new GeoNamesLineModel(null));

		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_GEONAMEID).setMinWidth(80);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_GEONAMEID).setMaxWidth(80);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_GEONAMEID).setResizable(false);

		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_LATITUDE).setMinWidth(90);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_LATITUDE).setMaxWidth(90);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_LATITUDE).setResizable(false);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_LATITUDE).setCellRenderer(new CoordinateValueRenderer());

		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_LONGITUDE).setMinWidth(90);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_LONGITUDE).setMaxWidth(90);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_LONGITUDE).setResizable(false);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_LONGITUDE).setCellRenderer(new CoordinateValueRenderer());

		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_ELEVATION).setMinWidth(50);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_ELEVATION).setMaxWidth(50);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_ELEVATION).setResizable(false);

		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_POPULATION).setMinWidth(80);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_POPULATION).setMaxWidth(80);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_POPULATION).setResizable(false);

		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_ADMINCODES).setMinWidth(130);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_ADMINCODES).setMaxWidth(130);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_ADMINCODES).setResizable(false);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_ADMINCODES).setCellRenderer(centerRenderer);

		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_FEATURE).setMinWidth(80);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_FEATURE).setMaxWidth(80);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_FEATURE).setResizable(false);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_FEATURE).setCellRenderer(centerRenderer);

		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_TIMEZONE).setMinWidth(110);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_TIMEZONE).setMaxWidth(110);
		this.tblResults.getColumnModel().getColumn(GeoNamesLineModel.COLUMN_TIMEZONE).setResizable(false);

		JPanel panelClose = new JPanel();
		add(panelClose, BorderLayout.SOUTH);
		panelClose.setLayout(new BoxLayout(panelClose, BoxLayout.X_AXIS));

		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				parentGUI.closeCurrentTab();
			}
		});
		panelClose.add(btnClose);

		Component horizontalGlue = Box.createHorizontalGlue();
		panelClose.add(horizontalGlue);

		JButton btnSaveButton = new JButton("Save");
		btnSaveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Save.
			}

		});
		panelClose.add(btnSaveButton);

		// ///////////////////////////////////////////
		refreshStats();

		if (!FlatDB4GeoNames.isOpened()) {
			this.btnSearchToponym.setEnabled(false);
			this.btnSearchToponyms.setEnabled(false);
			this.btnSearch.setEnabled(false);
			this.btnAutoFill.setEnabled(false);

			String title = "Configuration missing";
			StringList message = new StringList();
			message.append("<html>");
			message.append("<br/>");
			message.append("<p>Some buttons will be disabled because it is required<br/>a FlatDB4GeoNames configuration.</p>");
			message.append("<br/>");
			message.append("<p>Please, in order to do this action available:<br/>");
			message.append("1. Download database using Menu > Tools > Download database…<br/>");
			message.append("2. Set the database directory in Menu > Edit > Preferences.</p>");
			message.append("<br/>");
			message.append("Then you can use these functionalities.");
			message.append("</html>");
			JOptionPane.showMessageDialog(GeographyEditorPanel.this, message, title, JOptionPane.WARNING_MESSAGE);
		}
	}

	/**
	 * 
	 * @return
	 */
	public Place getSelectedPlace() {
		Place result;

		int rowIndex = GeographyEditorPanel.this.tblPlaceLines.getSelectedRow();
		if (rowIndex == -1) {

			result = null;
		} else {

			result = ((PlaceLineModel) GeographyEditorPanel.this.tblPlaceLines.getModel()).getSource().get(rowIndex);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public GeoNamesLine getSelectedResult() {
		GeoNamesLine result;

		int rowIndex = GeographyEditorPanel.this.tblResults.getSelectedRow();
		if (rowIndex == -1) {

			result = null;
		} else {

			result = ((GeoNamesLineModel) GeographyEditorPanel.this.tblResults.getModel()).getSource().get(rowIndex);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public AttributeValueDescriptors getValuesDescriptors() {
		return this.valueDescriptors;
	}

	/**
	 * 
	 */
	public void performSearch() {
		//
		try {
			//
			GeoNamesSearchCriteria criteria = new GeoNamesSearchCriteria();

			//
			criteria.setInput(this.txtfldInput.getText());

			//
			if (this.chckbxCityVillage.isSelected()) {

				criteria.featureClasses().add(FeatureClass.CITY_VILLAGE);
			}
			if (this.chckbxCountryStateRegion.isSelected()) {

				criteria.featureClasses().add(FeatureClass.COUNTRY_STATE_REGION);
			}
			if (this.chckbxForestHeath.isSelected()) {

				criteria.featureClasses().add(FeatureClass.FOREST_HEATH);
			}
			if (this.chckbxMountainHillRock.isSelected()) {

				criteria.featureClasses().add(FeatureClass.MOUNTAIN_HILL_ROCK);
			}
			if (this.chckbxParkArea.isSelected()) {

				criteria.featureClasses().add(FeatureClass.PARK_AREA);
			}
			if (this.chckbxSpotBuildingFarm.isSelected()) {

				criteria.featureClasses().add(FeatureClass.SPOT_BUILDING_FARM);
			}
			if (this.chckbxStreamLake.isSelected()) {

				criteria.featureClasses().add(FeatureClass.STREAM_LAKE);
			}
			if (this.chckbxUndersea.isSelected()) {

				criteria.featureClasses().add(FeatureClass.UNDERSEA);
			}

			//
			GeoNamesLines lines = FlatDB4GeoNames.instance().search(criteria);

			((GeoNamesLineModel) this.tblResults.getModel()).setSource(lines);

		} catch (IOException exception) {
			//
			exception.printStackTrace();

			//
			String title = "Search Error";
			String message = "Error searching.";
			JOptionPane.showMessageDialog(GeographyEditorPanel.this, message, title, JOptionPane.ERROR_MESSAGE);

		} catch (FlatDB4GeoNamesException exception) {
			//
			exception.printStackTrace();

			//
			String title = "Search Error";
			String message = "Error searching.";
			JOptionPane.showMessageDialog(GeographyEditorPanel.this, message, title, JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * 
	 */
	public void refreshStats() {

		Status status = this.netGUI.getNet().getGeography().getStatus();
		this.lblStatusValue.setText(String.format(" %s ", status.toString()));
		Color statusColor;
		switch (status) {
			case PERFECT:
			case COMPLETE:
				statusColor = Color.GREEN;
			break;

			case WORKABLE:
				statusColor = Color.ORANGE;
			break;

			case UNWORKABLE:
			default:
				statusColor = Color.RED;
			break;
		}
		this.lblStatusValue.setBackground(statusColor);

		int conflictedCount = this.netGUI.getNet().getGeography().countOfConflictedPlaces();
		int nameCount = this.netGUI.getNet().getGeography().countOfToponyms();
		this.lblConflictedValue.setText(String.format("%d / %d names (%s)", conflictedCount, nameCount,
				ToolBox.buildReadablePercentage(conflictedCount, nameCount)));

		int ungeocodedCount = this.netGUI.getNet().getGeography().countOfUngeocodedPlaces();
		int placeCount = this.netGUI.getNet().getGeography().countOfPlaces();
		this.lblUngeocodedValue.setText(String.format("%d / %d places (%s)", ungeocodedCount, placeCount,
				ToolBox.buildReadablePercentage(ungeocodedCount, placeCount)));
	}

	/**
	 * 
	 * @param filter
	 */
	public void setFilter(final Filter value) {

		if (value != null) {
			this.filter = value;

			updatePlaceLines();
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void updatePlaceLines() {

		Geography geography = this.netGUI.getNet().getGeography();

		Places places;

		switch (this.filter) {
			case BLANK:
				places = geography.getBlankPlaces();
			break;

			case CONFLICTUAL:
				places = geography.getConflictedPlaces();
			break;

			case UNGEOCODED:
				places = geography.getUngeocodedPlaces();
			break;

			case UNUSED:
				places = new Places(geography.getPlaces().size());

				for (Place place : geography.getPlaces()) {

					if (this.valueDescriptors.getCountOf(place.getToponyms()) == 0) {

						places.add(place);
					}
				}
			break;

			case ALL:
			default:
				places = geography.getPlaces();
		}

		((PlaceLineModel) GeographyEditorPanel.this.tblPlaceLines.getModel()).setSource(places);

		this.lblSelectionValue.setText(String.format(" %d / %d (%s)", places.size(), geography.countOfPlaces(),
				ToolBox.buildReadablePercentage(places.size(), geography.countOfPlaces())));
	}

	/**
	 * 
	 * @param line
	 * @return
	 */
	public static String buildExtraDataValue(final GeoNamesLine line) {
		String result;

		//
		long id = line.getGeoNameId();

		//
		String feature = line.getFeatureCodePath();
		String admin = line.getAdministrativeCodePath();

		//
		result = String.format("geonames=[id=%d;feature=%s;admin=%s]", id, StringUtils.defaultIfBlank(feature, ""), StringUtils.defaultIfBlank(admin, ""));

		//
		return result;
	}

	/**
	 * 
	 * This methods is compatible with Thread.interrupted() call.
	 * 
	 * @return
	 * @throws FlatDB4GeoNamesException
	 * @throws IOException
	 */
	public static int fillAutomatically(final Places places, final ProgressStatus progressStatus) throws IOException, FlatDB4GeoNamesException {
		int result;

		result = 0;
		FeatureClass[] featureClasses = { FeatureClass.COUNTRY_STATE_REGION, FeatureClass.CITY_VILLAGE };

		if (progressStatus != null) {
			progressStatus.setMax(places.size() * featureClasses.length);
		}

		for (FeatureClass featureClass : featureClasses) {

			for (Place place : places) {

				// Manage "Cancel" capability.
				if (Thread.interrupted()) {
					return result;
				}

				if (progressStatus != null) {
					progressStatus.inc();
				}

				if (place.getCoordinate2() == null) {

					GeoNamesSearchCriteria criteria = new GeoNamesSearchCriteria();
					criteria.setInput(place.getToponym());
					criteria.featureClasses().add(featureClass);

					GeoNamesLines lines = FlatDB4GeoNames.instance().search(criteria);

					GeoNamesLine line;
					if (lines.size() == 1) {
						//
						line = lines.get(0);

					} else {
						// Search for perfect match (keeps the first found only
						// if there is no one after).
						boolean ended = false;
						line = null;
						Iterator<GeoNamesLine> iterator = lines.iterator();
						while (!ended) {
							if (iterator.hasNext()) {
								GeoNamesLine subLine = iterator.next();
								if (StringUtils.equalsIgnoreCase(subLine.getName(), place.getToponym())) {
									if (line == null) {
										line = subLine;
									} else {
										line = null;
										ended = true;
									}
								}
							} else {
								ended = true;
							}
						}
						/*
						GeoNamesLines subLines = new GeoNamesLines(lines.size());
						for (GeoNamesLine subLine : lines) {
							if (StringUtils.equalsIgnoreCase(subLine.getName(), place.getToponym())) {
								subLines.add(subLine);
							}
						}

						if (subLines.size() == 1) {
							//
							line = subLines.get(0);
						} else {
							line = null;
						}
						*/
					}

					//
					if (line != null) {

						if (progressStatus != null) {
							progressStatus.incExtra1();
						}

						place.setLatitude(line.getLatitude());
						place.setLongitude(line.getLongitude());
						place.setElevation(line.getElevation());
						place.setAlternateToponyms(line.getAlternateNames());
						place.setExtraData(GeographyEditorPanel.buildExtraDataValue(line));

						result += 1;
					}
				}
			}
		}

		//
		return result;
	}
}