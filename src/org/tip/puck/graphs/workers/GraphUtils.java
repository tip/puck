package org.tip.puck.graphs.workers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.Graph.GraphMode;
import org.tip.puck.graphs.GraphMaker;
import org.tip.puck.graphs.Link;
import org.tip.puck.graphs.Links;
import org.tip.puck.graphs.Node;
import org.tip.puck.graphs.Nodes;
import org.tip.puck.matrix.Matrix;
import org.tip.puck.net.Individual;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionMaker;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.Numberable;
import org.tip.puck.util.Value;
import org.tip.puck.util.Values;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class GraphUtils {

	 private static final Logger logger = LoggerFactory.getLogger(GraphUtils.class);

	/**
	 * 
	 * @param nodes
	 * @param matrix
	 * @return
	 */
	public static <E> Graph<E> createGraphFromMatrix(final Nodes<E> nodes, final Matrix matrix) {
		Graph<E> result;

		result = new Graph<E>(0, nodes.size());

		result.addNodesWithId(nodes);
		result.addArcs(matrix);

		//
		return result;
	}
	
	public static<E> Graph<E> cloneWithoutNullValueLines(final Graph<E> source){
		Graph<E> result;
		
		result = new Graph<E>(source.getLabel());
		
		for (Node<E> node : source.getNodes()) {
			result.addNode(node.getId(), node.getReferent());
		}
		
		for (Link<E> link : source.getLinks()){
			if (link.getWeight()!=0.){
				if (link.isArc()){
					result.addArc(link.getSourceNode().getId(), link.getTargetNode().getId(), link.getWeight());
				} else if (link.isEdge()){
					result.addEdge(link.getSourceNode().getId(), link.getTargetNode().getId(), link.getWeight());
				}
			}
		}
		//
		return result;
	}
	
	public static<E> double getLocalClusteringCoefficient (final Graph<E> source, final Node<E> ego){
		double result;
		
		result = density(createEgoNetworkWithoutEgo(source,ego));
		
		//
		return result;
	}
	
	public static<E> double getMeanClusteringCoefficient (final Graph<E> source){
		double result;
		
		result = 0.;
		for (Node<E> ego : source.getNodes()){
			result += getLocalClusteringCoefficient(source,ego);
		}
		result = result / new Double(source.nodeCount());
		
		//
		return result;
	}

	public static<E> Graph<E> createEgoNetworkWithoutEgo(final Graph<E> source, final Node<E> ego){
		Graph<E> result;
		
		result = createEgoNetwork(source,ego);
		result.removeNode(ego);
		
		//
		return result;
	}
		

	
	public static<E> Graph<E> createEgoNetwork(final Graph<E> source, final Node<E> ego){
		Graph<E> result;
		
		logger.debug("create ego network for "+ego);

		result = new Graph<E>(source.getLabel());
		
		result.addNode(ego.getReferent());
		for (Node<E> node : ego.getOtherNodes()) {
			result.addNode(node.getReferent());
		}
			
		//
		for (Node<E> newSourceNode : result.getNodes()) {
			//
			Node<E> oldSourceNode = source.getNode(newSourceNode.getReferent());

			//
			for (Link<E> oldLink : oldSourceNode.getOutArcs()) {
				//
				Node<E> oldTargetNode = oldLink.getTargetNode();
				Node<E> newTargetNode = result.getNode(oldTargetNode.getReferent());
				//
				if (newTargetNode != null) {
					result.addArc(newSourceNode, newTargetNode, oldLink.getWeight());
				}
			}
			
			//
			for (Link<E> oldLink : oldSourceNode.getEdges()) {
				//
				Node<E> oldTargetNode = oldLink.getTargetNode();
				Node<E> newTargetNode = result.getNode(oldTargetNode.getReferent());
				//
				if (newTargetNode != null) {
					result.addEdge(newSourceNode, newTargetNode, oldLink.getWeight());
				}
			}
		}

		//
		return result;
	}
	
	private static <E> void putComponentItem (Partition<Node<E>> components, Node<E> node, Value value){
		if (!components.getItems().contains(node)){
			components.put(node, value);
			for (Node<E> neighbor : node.getDirectNeighbors()){
				putComponentItem(components,neighbor,value);
			}
		}
	}
	
	private static <E> int unconnectedPairs (final Graph<E> source){
		int result;
		
		result = 0;
		
		List<Node<E>> nodes = source.getNodes().toListSortedById();
		
		for (Node<E> firstNode : nodes){
			for (Node<E> secondNode : source.getNodes()){
				if (firstNode.getId()>secondNode.getId()){
					if (!firstNode.getDirectNeighbors().contains(secondNode)){
						result++;
					}
				} else {
					break;
				}
			}
		}
		
		//
		return result;
	}
	
	public static <E> double unconnectedPairsNormalized (final Graph<E> source){
		double result;
		
		result = MathUtils.percent(unconnectedPairs(source),(source.nodeCount())*(source.nodeCount()-1));
		
		//
		return result;
	}
	
	public static <E> Graph<E> fuse (final List<Graph<E>> source){
		Graph<E> result;
		
		result = new Graph<E>();
		
		for (Graph<E> graph : source){
			result.addNodes(graph.getNodes());
			result.addLinks(graph.getLinks());
		}
		
		//
		return result;

	}

	public static <E> Graph<E> insideClusters (final Graph<E> source, final String label){
		Graph<E> result;
		
		result = new Graph<E>();
		result.setLabel(source.getLabel()+" Inside Clusters "+label);
		
		for (Node<E> node : source.getNodes()){
			result.addNode(node.getId(), node.getReferent());
		}
		
		for (Link<E> arc : source.getArcs()){
			if (StringUtils.equals(arc.getSourceNode().getAttributeValue(label),arc.getTargetNode().getAttributeValue(label))){
				result.addArc(arc.getSourceNode().getId(), arc.getTargetNode().getId(), arc.getWeight());
			}
		}
		
		for (Link<E> edge : source.getEdges()){
			if (StringUtils.equals(edge.getSourceNode().getAttributeValue(label),edge.getTargetNode().getAttributeValue(label))){
				result.addEdge(edge.getSourceNode().getId(), edge.getTargetNode().getId(), edge.getWeight());
			}
		}
		
		//
		return result;
	}
	
	public static <E> Graph<E> cloneWithoutEgo (final Graph<E> source, final Node<E> ego){
		Graph<E> result;
		
		result = new Graph<E>();
		result.setLabel(source.getLabel()+" Without Ego");
		
		for (Node<E> node : source.getNodes()){
			if (!node.equals(ego)){
				result.addNode(node.getId(), node.getReferent());
			}
		}
		for (Link<E> arc : source.getArcs()){
			if (!arc.getSourceNode().equals(ego) && !arc.getTargetNode().equals(ego)){
				result.addArc(arc.getSourceNode().getId(), arc.getTargetNode().getId(), arc.getWeight());
			}
		}
		for (Link<E> edge : source.getEdges()){
			if (!edge.getSourceNode().equals(ego) && !edge.getTargetNode().equals(ego)){
				result.addEdge(edge.getSourceNode().getId(), edge.getTargetNode().getId(), edge.getWeight());
			}
		}
		
		//
		return result;
	}
	
	public static <E> int nrComponents (final Graph<E> source) {
		int result;
		
		result = components(source).size();
		
		//
		return result;
	}
	
	public static<E> Cluster<Node<E>> maxComponent (final Graph<E> source){
		Cluster<Node<E>> result;
		
		result = components(source).maxCluster();
		
		//
		return result;
	}
	
	public static <E> Partition<Node<E>> components(final Graph<E> source){
		Partition<Node<E>> result;
		
		result = new Partition<Node<E>>();
		
		int i = 1;
		for (Node<E> node : source.getNodes()){
			putComponentItem(result,node,new Value(i));
			i++;
		}
		
		//
		return result;
	}
	
	public static<E> List<Graph<E>> componentGraphs(final Graph<E> source){
		List<Graph<E>> result;
		
		result = new ArrayList<Graph<E>>();
		
		for (Cluster<Node<E>> cluster : components(source).getClusters().toListSortedByDescendingSize()){
			
			result.add(GraphMaker.extractSubgraph(source, cluster.getItems(), cluster.getValue()+""));
		}
		
		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param cluster1
	 * @param cluster2
	 * @return
	 */
	private static <E> Node<E> maxStrengthNode(final List<Node<E>> source, final Cluster<Node<E>> cluster1, final Cluster<Node<E>> cluster2) {
		Node<E> result = null;

		double maxStrength = 1.;

		for (Node<E> node : source) {
			double strength = strengthRate(node, cluster1, cluster2);
			if (strength >= maxStrength) {
				maxStrength = strength;
				result = node;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param minimalNumberOfLinks
	 * @param minimalNodeStrength
	 * @param minimalLinkWeight
	 * @return
	 */
	public static <E> Graph<E> reduce(final Graph<E> source, final int minimalNumberOfLinks, final double minimalNodeStrength, final double minimalLinkWeight) {
		Graph<E> result;

		logger.debug("reduce starting... [minimalNumberOfLinks={}][minimalNodeStrength={}][minimalLinkWeight={}]", minimalNumberOfLinks, minimalNodeStrength,
				minimalLinkWeight);

		if ((minimalNumberOfLinks == 0) && (minimalLinkWeight == 0)&& (minimalNodeStrength == 0)) {
			result = source;
		} else {
			//
			result = new Graph<E>(source.getLabel());

			//
			for (Node<E> node : source.getNodes()) {
				if (((minimalNumberOfLinks == 0) || (node.getDegree() >= minimalNumberOfLinks))
						&& (((minimalNodeStrength == 0) || (node.getForce() >= minimalNodeStrength)))
						&& (((minimalLinkWeight == 0) || (MathUtils.compare(node.getMaxLinkWeight(), minimalLinkWeight) >= 0)))) {
					result.addNode(node.getReferent());
				}
			}
			
			//
			for (Node<E> newSourceNode : result.getNodes()) {
				//
				Node<E> oldSourceNode = source.getNode(newSourceNode.getReferent());

				//
				for (Link<E> oldLink : oldSourceNode.getOutArcs()) {
					//
					Node<E> oldTargetNode = oldLink.getTargetNode();
					Node<E> newTargetNode = result.getNode(oldTargetNode.getReferent());

					//
					if (newTargetNode != null) {
						if (oldLink.getWeight() >= minimalLinkWeight) {
							result.addArc(newSourceNode, newTargetNode, oldLink.getWeight());
						}
					}
				}

				//
				for (Link<E> oldLink : oldSourceNode.getEdges()) {
					//
					Node<E> oldTargetNode = oldLink.getTargetNode();
					Node<E> newTargetNode = result.getNode(oldTargetNode.getReferent());

					//
					if (newTargetNode != null) {
						if (oldLink.getWeight() >= minimalLinkWeight) {
							result.addEdge(newSourceNode, newTargetNode, oldLink.getWeight());
						}
					}
				}
			}
		}

		logger.debug("reduce done.");

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static <E> Graph<E> reduceArcToEdge(final Graph<E> source) {
		Graph<E> result;

		if (source == null) {
			result = null;
		} else {
			//
			result = new Graph<E>(source.getLabel());

			//
			for (Node<E> node : source.getNodes()) {
				result.addNode(node.getReferent());
			}

			//
			for (Node<E> newSourceNode : result.getNodes()) {
				//
				Node<E> oldSourceNode = source.getNode(newSourceNode.getReferent());

				//
				for (Link<E> oldLink : oldSourceNode.getOutArcs()) {
					//
					Node<E> oldTargetNode = oldLink.getTargetNode();
					Node<E> newTargetNode = result.getNode(oldTargetNode.getReferent());

					//
					if (result.getEdge(newSourceNode, newTargetNode) == null) {
						//
						double newWeight = oldLink.getWeight();
						if (newSourceNode != newTargetNode) {
							Link<E> oldReverseLink = oldTargetNode.getArcWith(oldSourceNode);
							if (oldReverseLink != null) {
								newWeight += oldReverseLink.getWeight();
							}
						}

						//
						result.addEdge(newSourceNode, newTargetNode, newWeight);
					}
				}
			}

		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param sides
	 * @return
	 */
	public static <E> double sidedness(final Graph<E> source, final Partition<Node<E>> sides) {
		double result;

		double exoWeight = 0.;
		double totalWeight = 0;

		for (Link<E> link : source.getLinks()) {
			if (sides.getCluster(link.getSourceNode()) != sides.getCluster(link.getTargetNode())) {
				exoWeight += link.getWeight();
			}
			totalWeight += link.getWeight();

		}

		result = exoWeight / totalWeight;
		//
		return result;

	}

	/**
	 * sidedness algorithm of Douglas R. White
	 * 
	 * @param source
	 * @return
	 */
	public static <E> Partition<Node<E>> sides(final Graph<E> source) {
		Partition<Node<E>> result;

		result = new Partition<Node<E>>();

		Cluster<Node<E>> side1 = new Cluster<Node<E>>(new Value(1));
		Cluster<Node<E>> side2 = new Cluster<Node<E>>(new Value(2));
		Cluster<Node<E>> side3 = new Cluster<Node<E>>(new Value(3));

		result.getClusters().put(side1);
		result.getClusters().put(side2);

		double maxStrength = 0;
		Node<E> maxStrengthNode = null;

		for (Node<E> node : source.getNodes()) {
			double strength = node.getForce();
			if (strength > 0.) {
				side3.put(node);
				if (strength > maxStrength) {
					maxStrength = strength;
					maxStrengthNode = node;
				}
			}
		}
		result.swap(maxStrengthNode, side3, side1);
		result.swap(maxStrengthNode(side3.getItems(), side1, side3), side3, side2);

		int count = side3.count() + 1;

		while (side3.count() < count) {
			count = side3.count();
			result.swap(maxStrengthNode(side3.getItems(), side1, side2), side3, side2);
			result.swap(maxStrengthNode(side3.getItems(), side2, side1), side3, side1);
		}

		double sidedness = sidedness(source, result) - 1;

		while (sidedness < sidedness(source, result)) {

			sidedness = sidedness(source, result);

			result.swap(maxStrengthNode(side1.getItems(), side1, side2), side1, side2);
			result.swap(maxStrengthNode(side2.getItems(), side2, side1), side2, side1);

		}

		//
		return result;

	}

	/**
	 * 
	 * @param node
	 * @param cluster1
	 * @param cluster2
	 * @return
	 */
	private static <E> double strengthRate(final Node<E> node, final Cluster<Node<E>> cluster1, final Cluster<Node<E>> cluster2) {
		double result;

		double strength1 = 0.;
		double strength2 = 0;
		for (Link<E> link : node.getLinks()) {
			Node<E> other = link.getOtherNode(node);
			if (cluster1.getItems().contains(other)) {
				strength1 = +link.getWeight();
			} else if (cluster2.getItems().contains(other)) {
				strength2 = +link.getWeight();
			}
		}

		result = strength1 / strength2;
		//
		return result;
	}
	
	/**
	 * toDo: catch ClassCastException
	 * @param source
	 * @return
	 */
	public static<E> Map<Integer,Integer> getIds (final Graph<E> source){
		Map<Integer,Integer> result;
		
		result = new HashMap<Integer,Integer>();
		
		for (Node<E> node : source.getNodes().toList()){
			result.put(((Numberable)node.getReferent()).getId(), node.getId());
		}
		
		//
		return result;
	}
	
	public static Partition<Link<Individual>> getLinkPartitionByKinship(Graph<Individual> source){
		Partition<Link<Individual>> result;
		
		result = new Partition<Link<Individual>>();
		
		for (Link<Individual> link : source.getLinks()){
			
			Individual ego = link.getSourceNode().getReferent();
			Individual alter = link.getTargetNode().getReferent();
			
			String label = null;
			
			if (ego.isChildOf(alter)){
				label = "PARENT";
			} else if (alter.isChildOf(ego)){
				label = "CHILD";
			} else if (ego.isPartnerWith(alter)){
				label = "SPOUSE";
			} else if (ego.siblings().contains(alter)){
				label = "SIBLING";
			}
			
			if (label!=null){
				result.put(link, new Value(label));
			} else {
				result.put(link,null);
			}
		}
		//
		return result;
	}
	
	public static<E> double[] betweenness (final Graph<E> source){
		
		return betweenness (source, GraphMode.UNDIRECTED);
		
	}

	
	/**
	 * Algorithm by Ulrik Brandes, A Faster Algorithm for Betweenness Centrality, Journal of Mathematical Sociology 25(2):163-177, (2001)
	 * @param node
	 * @param source
	 * @return
	 */
	public static<E> double[] betweenness (final Graph<E> source, GraphMode graphMode){
		
		double[] result;
		int n = source.nodeCount();
		result = new double[n];
		
		// IndexMap
		Map<Node<E>,Integer> id = new HashMap<Node<E>,Integer>();
		int k = 0;
		for (Node<E> node : source.getNodes().toListSortedById()){
			id.put(node,k);
			k++;
		}
		
		for (Node<E> s : source.getNodes().toListSortedById()){
			int si = id.get(s);
			Stack<Node<E>> stack = new Stack<Node<E>>();
			Partition<Node<E>> predecessors = new Partition<Node<E>>();
			double[] nrShortestPathsThrough = new double[n];
			double[] distance = new double[n];

			int i=0;
			for (Node<E> node : source.getNodes().toListSortedById()){
				nrShortestPathsThrough[i]=0;
				distance[i]=-1;
				predecessors.putCluster(new Value(node.getId()));
				i++;
			}
			
/*			for (int i=0;i<n;i++){
				nrShortestPathsThrough[i]=0;
				distance[i]=-1;
				predecessors.putCluster(new Value(i+1));
			}*/
			
			nrShortestPathsThrough[si]=1;
			distance[si]=0;
			Queue<Node<E>> queue = new LinkedList<Node<E>>();
			queue.add(s);
			while (!queue.isEmpty()){
				Node<E> v = queue.remove();
				stack.push(v);
				int vi = id.get(v);
				
				Nodes<E> neighbors = null;
				if (graphMode == GraphMode.UNDIRECTED){
					neighbors = v.getDirectNeighbors();
				} else if (graphMode == GraphMode.DIRECTED){
					neighbors = v.getOutNodes();
				}
				
				for (Node<E> w : neighbors){
					int wi = id.get(w);
					// w found for the first time?
					if (distance[wi]<0){
						queue.add(w);
						distance[wi] = distance[vi] + 1;
					}
					// shortest path to w via v? 
					if (distance[wi] == distance[vi] + 1){
						nrShortestPathsThrough[wi] += nrShortestPathsThrough[vi];
						predecessors.put(v, new Value(w.getId()));
					}
				}
			}
			double[] pairDepencency = new double[n];
			for (int j=0;j<n;j++){
				pairDepencency[j]=0;
			}
			
			//stack returns vertices in order of non-increasing distance from s
			while (!stack.isEmpty()){
				Node<E> w = stack.pop();
				int wi = id.get(w);
				for (Node<E> v : predecessors.getCluster(new Value(w.getId())).getItems()){
					int vi = id.get(v);
					pairDepencency[vi] += (nrShortestPathsThrough[vi]/nrShortestPathsThrough[wi])*(1 + pairDepencency[wi]);
				}
				if (w!=s){
					result[wi] = result[wi] + pairDepencency[wi];
				}
			}
		}
		for (int i=0;i<n;i++){
			if (n>1){
				result[i]=MathUtils.percent(result[i],((n-1)*(n-2)));
			}
		}
		
		//
		return result;
	}
	
	public static<E> void setNodeLabelsFromPartition (final Graph<E> source, final String partitionLabel){
		
		for (Node<E> node : source.getNodes().toListSortedById()){
			String label = node.getAttributeValue(partitionLabel);
			if (label != null){
				node.setLabel(label);
			}
		}
	}

	public static<E> void addNodeLabelsFromPartition (final Graph<E> source, final String partitionLabel){
		
		for (Node<E> node : source.getNodes().toListSortedById()){
			String label = node.getAttributeValue(partitionLabel);
			if (label != null){
				node.setLabel(node.getLabel()+" "+label);
			}
		}
	}

	/**
	 * Generates a Pajek file content from a graph.
	 * 
	 * @param source
	 *            A graph to write in Pajek format.
	 * 
	 * @param partitionLabels
	 *            Labels of endogenous attributes of a graph node.
	 * 
	 * @return The Pajek content for this graph.
	 * @throws PuckException
	 */
	public static <E> StringList writePajekNetwork(final Graph<E> source, final List<String> partitionLabels, final Map<String,Map<Value,Integer>> partitionNumbersMaps) throws PuckException {
		StringList result;

		//
		result = new StringList(100);

		//
		result.appendln("*Network " + source.getLabel());
		result.appendln();
		
		Map<Integer,Integer> idToIndex = new HashMap<Integer,Integer>();

		// Node list
		result.appendln("*vertices " + source.nodeCount());
		for (int nodeIndex = 1; nodeIndex <= source.nodeCount(); nodeIndex++) {
			Node<E> node = source.getNodes().toListSortedById().get(nodeIndex-1);
			idToIndex.put(node.getId(), nodeIndex);
			if (StringUtils.isBlank(node.getTag())) {
				result.appendln((nodeIndex) + " '" + node.getLabel().replaceAll("\\'", "*") + "'");
			} else {
				result.appendln((nodeIndex) + " '" + node.getLabel().replaceAll("\\'", "*") + "' " + node.getTag());
			}
		}

		int relationNumber = 1;

		// Arc List
		Links<E> arcs = source.getArcs();
		if (arcs.isNotEmpty()) {
			//
			List<String> tags = arcs.getTags();
			if (tags.isEmpty()) {
				result.appendln("*arcs");
				for (Link<E> arc : arcs) {
					result.appendln(idToIndex.get(arc.getSourceNode().getId()) + " " + idToIndex.get(arc.getTargetNode().getId()) + " " + arc.getWeightAsInt());
				}
			} else {
				Collections.sort(tags);
				for (String tag : tags) {
					result.appendln("*arcs :"+relationNumber+" " + tag);
					for (Link<E> arc : arcs.getByTag(tag)) {
						result.appendln(idToIndex.get(arc.getSourceNode().getId()) + " " + idToIndex.get(arc.getTargetNode().getId()) + " " + arc.getWeightAsInt());
					}
					relationNumber++;
				}
			}
		}

		// Edge List
		Links<E> edges = source.getEdges();
		if (edges.isNotEmpty()) {
			//
			List<String> tags = edges.getTags();
			if (tags.isEmpty()) {
				result.appendln("*edges");
				for (Link<E> edge : edges) {
					result.appendln(idToIndex.get(edge.getSourceNode().getId()) + " " + idToIndex.get(edge.getTargetNode().getId()) + " " + edge.getWeightAsInt());
				}
			} else {
				Collections.sort(tags);
				for (String tag : tags) {
					result.appendln("*edges :"+relationNumber+" " + tag);
					for (Link<E> edge : edges.getByTag(tag)) {
						result.appendln(idToIndex.get(edge.getSourceNode().getId()) + " " + idToIndex.get(edge.getTargetNode().getId()) + " " + edge.getWeightAsInt());
					}
					relationNumber++;
				}
			}
		}

		// Partitions
		for (String label : partitionLabels) {
			result.appendln();
//			logger.debug("label=[" + label + "]");
			Partition<Node<E>> partition = PartitionMaker.create(label, source, PartitionCriteria.createRaw(label));

			String type;
			if (partition.isNumeric()) {
				type = "Vector";
			} else {
				type = "Partition";
				if (partitionNumbersMaps!=null){
					partition = PartitionMaker.createNumerized(partition,partitionNumbersMaps.get(label));
				} else {
					partition = PartitionMaker.createNumerized(partition,null);
				}
			}

			result.appendln("*" + type + " " + partition.getLabel()+" "+source.getLabel());
			result.appendln("*vertices " + source.nodeCount());
			for (int nodeIndex = 1; nodeIndex <= source.nodeCount(); nodeIndex++) {
				Node<E> node = source.getNodes().toListSortedById().get(nodeIndex-1);
				if (partition.isNumeric()){
					result.appendln(partition.getValue(node).doubleValue());
				} else {
					result.appendln(partition.getValue(node).intValue());
				}
			}
		}

		//
		return result;
	}

	public static <E> int cyclomaticNumber(Graph<E> graph){
		int result = 0;
		
		result = graph.lineCountWithoutLoops() - graph.nodeCount() + GraphUtils.nrComponents(graph);
		
		//
		return result;
	}
		
	/**
	 * 
	 * @param graph
	 * @return
	 */
	public static <E> double density(Graph<E> graph){
		double result;
		
		int nodeCount = graph.nodeCount();
		result = MathUtils.percent(graph.tieCount(), nodeCount*nodeCount);
		
		//
		return result;
	}
	
	public static <E> double densityWithoutLoops(Graph<E> graph){
		double result;
		
		int nodeCount = graph.nodeCount();
		result = MathUtils.percent(graph.tieCountWithoutLoops(), nodeCount*(nodeCount-1));
		
		//
		return result;
	}
	
	public static <E> double meanDegreeWithoutLoopsAndDoubleLines(Graph<E> graph){
		double result;
		
		int nodeCount = graph.nodeCount();
		int degreeSum = 0;
				
		for (Node<E> node : graph.getNodes()){
			degreeSum += node.getDirectNeighbors().size();
		}
		
		result = MathUtils.percent(degreeSum, 100*nodeCount);

		//
		return result;
		
	}
	
	public static <E> double meanDegreeWithoutLoops(Graph<E> graph){
		double result;
		
		int nodeCount = graph.nodeCount();
		int degreeSum = 0;
				
		for (Node<E> node : graph.getNodes()){
			degreeSum += node.getDegreeWithoutLoops();
		}
		
		result = MathUtils.percent(degreeSum, 100*nodeCount);

		//
		return result;
	}
	
	public static <E> double meanInDegree(Graph<E> graph){
		double result;
		
		int nodeCount = graph.nodeCount();
		int degreeSum = 0;
				
		for (Node<E> node : graph.getNodes()){
			degreeSum += node.getInDegree();
		}
		
		result = MathUtils.percent(degreeSum, 100*nodeCount);

		//
		return result;
	}
	
	public static <E> double meanOutDegree(Graph<E> graph){
		double result;
		
		int nodeCount = graph.nodeCount();
		int degreeSum = 0;
				
		for (Node<E> node : graph.getNodes()){
			degreeSum += node.getOutDegree();
		}
		
		result = MathUtils.percent(degreeSum, 100*nodeCount);

		//
		return result;
	}
	
	
	public static <E> double meanDegree(Graph<E> graph){
		double result;
		
		int nodeCount = graph.nodeCount();
		int degreeSum = 0;
				
		for (Node<E> node : graph.getNodes()){
			degreeSum += node.getDegree();
		}
		
		result = MathUtils.percent(degreeSum, 100*nodeCount);

		//
		return result;
	}
	
	public static <E> double meanDegreeNormalized(Graph<E> graph){
		double result;
		
		result = MathUtils.percent(meanDegree(graph), 100*graph.nodeCount());

		//
		return result;
	}

	public static <E> double meanDegreeWithoutLoopsNormalized(Graph<E> graph){
		double result;
		
		result = MathUtils.percent(meanDegreeWithoutLoops(graph), 100*(graph.nodeCount()-1));

		//
		return result;
	}


	public static <E> Double efficientSize(Graph<E> graph){
		Double result;
		
		if (graph.nodeCount()<3){
			
			result = null;
			
		} else {
			
			int nodeCount = graph.nodeCount();
			int neighborSum = 0;
					
			for (Node<E> node : graph.getNodes()){
				neighborSum += node.getDirectNeighbors().size();
			}
			
			result = new Double(nodeCount)-MathUtils.percent(neighborSum, 100*nodeCount); 

		}

		//
		return result;
	}
	
	public static <E> Double efficiency(Graph<E> graph){
		Double result;
		
		if (graph.nodeCount()<3){
			
			result = null;
			
		} else {
			
			result = MathUtils.percent(efficientSize(graph), graph.nodeCount()); 
			
		}

		//
		return result;
	}
	
	private static <E> int getMaxDownDistance (Node<E> root){
		int result;
		
		result = 0;
		
		for (Node<E> node : root.getInNodes()){
			int distance = getMaxDownDistance(node) + 1;
			if (distance > result){
				result = distance;
			}
		}
		
		//
		return result;
	}
	
	public static<E> int getTreeDiameter (Graph<E> source){
		int result;
		
		result = 0;
		
		for (Node<E> root : source.getNodes()){
			if (root.getOutDegree()==0) {
				int distance = getMaxDownDistance(root);
				if (distance > result){
					result = distance;
				}
				if (root.getInDegree()>1){
					List<Node<E>> list = root.getInNodes().toListSortedById();
					for (Node<E> alpha : list){
						for (Node<E> beta : list){
							if (beta.equals(alpha)){
								break;
							}
							distance = getMaxDownDistance(alpha)+getMaxDownDistance(beta)+2;
							if (distance > result){
								result = distance;
							}
						}
					}
				}
			}
		}
		//
		return result;
	}
	
	
	public static <E,T> double[][] createDistanceMatrix (List<E> items){
		double[][] result;
		
		result = null;
		
		if (items.get(0) instanceof Graph<?>){
			result = createGraphDistanceMatrix((List<Graph<T>>)items);
		}
		//
		return result;
	}
	
	public static <E> double[][] createGraphDistanceMatrix (List<Graph<E>> graphs){
		double[][] result;

		result = new double[graphs.size()][graphs.size()];
		
		for (int i=0;i<graphs.size();i++){
			result[i][i] = 0.;
			for (int j=0;j<i;j++){
				double distance = 0.;
				Graph<E> alpha = graphs.get(i);
				Graph<E> beta = graphs.get(j);
				double nrArcs = new Double(alpha.arcCount()+beta.arcCount()).doubleValue();
				for (Link<E> alphaLink : alpha.getLinks()){
					if (beta.getArc(alphaLink.getSourceNode().getReferent(), alphaLink.getTargetNode().getReferent())==null){
						distance+=1.;
					}
				}
				for (Link<E> betaLink : beta.getLinks()){
					if (alpha.getArc(betaLink.getSourceNode().getReferent(), betaLink.getTargetNode().getReferent())==null){
						distance+=1.;
					}
				}
				
				double normalizedDistance = distance/nrArcs;
				System.err.println(distance+"\t"+nrArcs+"\t"+normalizedDistance);
				if (nrArcs==0){
					normalizedDistance = 0.;
				}

				result[i][j]=normalizedDistance;
				result[j][i]=result[i][j];
								
			}
		}
		
		for (int i=0;i<graphs.size();i++){
			String line = graphs.get(i)+"\t";
			for (int j=0;j<graphs.size();j++){
				line+=result[i][j]+"\t";
			}
		}

		//
		return result;
	}
	
	public static<E> Graph<Set<Graph<E>>> createPhylogeneticTree (List<Graph<E>> graphs){
		return createPhylogeneticTree(graphs,createGraphDistanceMatrix(graphs));
	}
	
	public static<E> Graph<Graph<E>> createDistanceGraph (List<Graph<E>> graphs){
		return createDistanceGraph(graphs,createGraphDistanceMatrix(graphs));
	}
	
	
	private static <E> Node<E> getParentNode (Node<E> childNode){
		Node<E> result;

		result = null;
		for (Node<E> parentNode : childNode.getInNodes()){
			result = parentNode;
			break;
		}
		
		//
		return result;
	}
	
	/**
	 * works only for trees!
	 * @param childNode
	 * @param barredLabel
	 * @return
	 */
	private static <E> Node<E> getParentNode (Node<E> childNode, String barredLabel){
		Node<E> result;
		
		result = childNode;
		Node<E> parentNode = getParentNode(result);
		
		while (parentNode!=null && parentNode.getLabel().equals(barredLabel)){
			result = parentNode;
			parentNode = getParentNode(result);
		}
		
		if (result==childNode){
			result=null;
		}
		//
		return result;
	}
	
	public static<E> Graph<E> createDistanceGraph (List<E> referents, double[][] distanceMatrix){
		Graph<E> result;
		
		result = new Graph<E>();
		
		List<E> items = new ArrayList<E>();
		for (E referent : referents){
			items.add(referent);
			result.addNode(referent);
			result.getNode(referent).setLabel(referent.toString());
		}
		
		for (int i=0; i<items.size();i++){
			for (int j=0;j<i;j++){
				E first = items.get(i);
				E second = items.get(j);
				double value = distanceMatrix[i][j];
				if (value>0){
					result.addEdge(first, second, value);
				}
			}
		}
		//
		return result;
	}
	
	/**
	 * Neighbor-joining algorithm by Saitou and Nei, 1987
	 * http://www.icp.ucl.ac.be/~opperd/private/neighbor.html
	 * @param referents
	 * @param distanceMatrix
	 * @return
	 */
	public static <E> Graph<Set<E>> createPhylogeneticTree (List<E> referents, double[][] distanceMatrix){
		Graph<Set<E>> result;
		
		result = new Graph<Set<E>>();
		
		List<Set<E>> items = new ArrayList<Set<E>>();
		for (E referent : referents){
			Set<E> set = new HashSet<E>();
			set.add(referent);
			items.add(set);
			result.addNode(set);
			result.getNode(set).setLabel(referent.toString());
		}
		
		int step = 0;
		while (distanceMatrix.length>1) {
			distanceMatrix = updateMatrix(step, distanceMatrix,items,result);
			step++;
		}
		
		// Simplify (fuse zero-distance nodes)
		Map<Node<Set<E>>,Node<Set<E>>> parents = new HashMap<Node<Set<E>>,Node<Set<E>>>();
		for (Node<Set<E>> node : result.getNodes().toListSortedByLabel()){
			if (node.getLabel().equals("0.0")){
				Node<Set<E>> highestParentNode = getParentNode(node,"0.0");
				if (highestParentNode!=null){
					parents.put(node, highestParentNode);
				}
			}
		}
		for (Node<Set<E>> node : parents.keySet()){
			Node<Set<E>> highestParentNode = parents.get(node);
			result.transferLinks(node,highestParentNode);
		}
		result.removeIsolates();
		
		
		for (Node<Set<E>> node : result.getNodes().toList()){
			node.setAttribute("SIZE", node.getReferent().size()+"");
/*			if (node.getLabel().equals("100.0")){
				result.removeNode(node);
			}*/
/*			String label = null;
			for (E element : node.getReferent()){
				if (label == null){
					label = element.toString();
				} else {
					label += " - "+ element.toString();
				}
			}
			node.setLabel(label);*/
		}
		
		//
		return result;
	}
	
	/**
	 * Neighbor-joining algorithm by Saitou and Nei, 1987
	 * http://www.icp.ucl.ac.be/~opperd/private/neighbor.html
	 * @param referents
	 * @param distanceMatrix
	 * @return
	 */
	private static <E> double[][] updateMatrix (int step, double[][] distanceMatrix, List<Set<E>> items, Graph<Set<E>> graph){
		double[][] result;
		
		int n = distanceMatrix.length;
		
		double[][] auxMatrix = new double[n][n];
		
		// Calculate net divergences
		double[] r = new double[n];
		for (int i=0;i<n;i++){
			r[i] = 0.;
			for (int j=0;j<n;j++){
				r[i] += distanceMatrix[i][j];
			}
		}

		// Calculate auxiliary distances for comparison
		for (int i=0;i<n;i++){
			for (int j=0;j<n;j++){
				auxMatrix[i][j] = distanceMatrix[i][j] - new Double(r[j] + r[i])/new Double(n - 2);
			}
		}

		// Find neighbors (minimal distance-pair)
		int[] minIndexPair = new int[2];
		Double minDistance = null;
		for (int i=0;i<n;i++){
			for (int j=0;j<i;j++){
				if (minDistance==null || auxMatrix[i][j]<minDistance){
					minIndexPair = new int[]{i,j};
					minDistance = auxMatrix[i][j];
				} 
			}
		}
		
		for (int i=0;i<n;i++){
			for (int j=0;j<i;j++){
				if (auxMatrix[i][j]==minDistance){
					minIndexPair = new int[]{i,j};
					minDistance = auxMatrix[i][j];
				} 
			}
		}
		
		
		
		
		int i = minIndexPair[0];
		int j = minIndexPair[1];
		
		// Create the neighbor-joining item
		Set<E> first = items.get(i);
		Set<E> second = items.get(j);
		Set<E> third = new HashSet<E>();
		third.addAll(first);
		third.addAll(second);
		
		// Calculate branch lengths
		double firstDistance = new Double(distanceMatrix[i][j])/new Double(2) + new Double(r[i]-r[j]) / new Double (2*(n-2));
		double secondDistance = new Double(distanceMatrix[i][j]) - firstDistance;
		
		// Create joining node and branches (distance set as tag, not as weight, since pajek does not accept zero weights)
		Link<Set<E>> firstLink = graph.addArc(third,first, 1);
		firstLink.setTag(firstDistance+"");
		Link<Set<E>> secondLink = graph.addArc(third,second, 1);
		secondLink.setTag(secondDistance+"");
		Node<Set<E>> thirdNode = graph.getNode(third);
		thirdNode.setLabel(distanceMatrix[i][j]+"");
		
		// Save the indices of the items in the old distance matrix
		Map<Set<E>,Integer> old = new HashMap<Set<E>,Integer>();
		for (int k=0;k<n;k++){
			old.put(items.get(k),k);
		}
		
		// Replace the two neighbors by the joining item
		items.remove(first);
		items.remove(second);
		items.add(third);
		
		// Calculate new distance matrix
		result = new double[n-1][n-1];
		
		for (int u=0;u<n-1;u++){
			result[u][u] = 0.;
			for (int v=0;v<u;v++){
				int vold = old.get(items.get(v));
				// transport old distances to new matrix
				if (u<n-2) {
					result[u][v] = distanceMatrix[old.get(items.get(u))][vold];
//				} else if (distanceMatrix[i][vold] == 100. && distanceMatrix[j][vold] == 100.){
//					result[u][v] = 100.;
				// calculate new distances to joining point
				} else { 
					result[u][v] = (distanceMatrix[i][vold] + distanceMatrix[j][vold] - distanceMatrix[i][j])/2;
				}
				result[v][u] = result[u][v];
			}
		}
		
		//
		return result;

	}
	
	public static <E> Graph<Cluster<E>> fuseGraphs (List<Graph<Cluster<E>>> graphs){
		Graph<Cluster<E>> result;
		
		result = new Graph<Cluster<E>>();
		
		Partition<E> partition = new Partition<E>();
		
		Map<String,Integer> numbers = new HashMap<String,Integer>();
				
		for (Graph<Cluster<E>> graph : graphs){
			for (Link<Cluster<E>> arc : graph.getArcs()){
				Cluster<E> source = partition.addCluster(arc.getSourceNode().getReferent());
				Cluster<E> target = partition.addCluster(arc.getTargetNode().getReferent());
				result.incArcWeight(source, target);
			}
			for (Link<Cluster<E>> edge : graph.getEdges()){
				Cluster<E> source = partition.addCluster(edge.getSourceNode().getReferent());
				Cluster<E> target = partition.addCluster(edge.getTargetNode().getReferent());
				result.incEdgeWeight(source, target);
			}
			for (Node<Cluster<E>> node : graph.getNodes()){
				Integer number = numbers.get(node.getLabel());
				if (number==null){
					number = 0;
				} 
				numbers.put(node.getLabel(), number+1);
			}
		}
		
		for (Node<Cluster<E>> node : result.getNodes()){
			node.setAttribute("NUMBER", numbers.get(node.getLabel())+"");
		}
		//
		return result;
	}
	
	private static <E> void addDepth(Map<Node<E>,Integer> depthMap, Node<E> root, int depth, int limit){
		
		depthMap.put(root, depth);
		for (Node<E> node : root.getInNodes()){
			if (depth<limit && (!depthMap.containsKey(node) || depthMap.get(node)<depth+1)){
				addDepth(depthMap,node,depth+1,limit);
			}
		}
	}
	
	/**
	 * Optionalize limit! 
	 * @param source
	 * @return
	 */
	private static <E> Map<Node<E>,Integer> depthMap (Graph<E> source){
		Map<Node<E>,Integer> result;
		
		result = new HashMap<Node<E>,Integer>();
		int limit = 1000;
		
		for (Node<E> node : GraphMaker.removeLoops(source).getNodes()){
			if ((node.getOutDegree())==0){
				addDepth(result,node, 0, limit);
			}
		}
		
		//
		return result;
		
	}
	
	public static <E> int getMaxDepth (Graph<E> source){
		int result;
		
		result = 0;
		
		Map<Node<E>,Integer> depthMap = depthMap(source);
		
		for (Node<E> node : depthMap.keySet()){
			Integer depth = depthMap.get(node);
			if (depth==null){
				System.err.println("missing depth value (suspect cyclic referent) for "+node);
			} else if (depth>result){
				result = depth;
			}
		}
		
		//
		return result;
	}
	
	public static<E> double getMaxDepthNormalized (Graph<E> source){
		double result;
		
		result = MathUtils.percent(getMaxDepth(source),new Double(source.nodeCount()-1));
		
		//
		return result;
	}
	
	public static<E> double getMeanDepthNormalized (Graph<E> source){
		double result;
		
		result = MathUtils.percent(getMeanDepth(source),new Double(source.nodeCount()-1)/2.);
		
		//
		return result;
	}
	
	public static <E> double getMeanDepth (Graph<E> source){
		double result;
		
		result = 0;
		
		Map<Node<E>,Integer> depthMap = depthMap(source);
		
		for (Node<E> node : depthMap.keySet()){
			Integer depth = depthMap.get(node);
			if (depth==null){
				System.err.println("missing depth value (suspect cyclic referent) for "+node);
			} else {
				result += depth.doubleValue();
			}
		}
		
		result = result/new Double(source.nodeCount());
		
		//
		return result;
	}
	
	public static <E> Partition<Node<E>> getDepthPartition(Graph<E> source){
		Partition<Node<E>>  result;
		
		result = new Partition<Node<E>>();
		result.setLabel(source.getLabel());
		
		Nodes<E> roots = new Nodes<E>();
		for (Node<E> node : source.getNodes()){
			if (node.getInDegree()==0){
				roots.add(node);
			}
		}
		
		for (Node<E> root : roots){
			Stack<Node<E>> stack = new Stack<Node<E>>();
			int step = 0;

			stack.push(root);
			result.put(root,new Value(step));
			root.setAttribute("STEP", step+"");
			
			while (!stack.isEmpty()){
				Node<E> parent = stack.pop();
				step = Integer.parseInt(parent.getAttributeValue("STEP"))+1;
				for (Node<E> child : parent.getOutNodes()){
					if (!result.getItems().contains(child)){
						stack.push(child);
						result.put(child, new Value(step));
						child.setAttribute("STEP", step+"");
					}
				}
			}
		}
		
		//
		return result;
		
	}
	
	public static<E> Nodes<E> getLoops (Graph<E> source){
		Nodes<E> result;
		
		result = new Nodes<E>();
		
		for (Node<E> node : source.getNodes()){
			
			if (node.getLoopDegree()>0){
				
				result.add(node);
			}
		}
		//
		return result;
	}
	
	public static<E> Nodes<E> getTops (Graph<E> source){
		Nodes<E> result;
		
		result = new Nodes<E>();
		
		for (Node<E> node : GraphMaker.removeLoops(source).getNodes()){
			
			if (node.getInDegree()==0 && node.getOutDegree()>0){
				
				result.add(node);
			}
		}
		//
		return result;
	}
	
	public static<E> Nodes<E> getIsolates (Graph<E> source){
		Nodes<E> result;
		
		result = new Nodes<E>();
		
		for (Node<E> node : GraphMaker.removeLoops(source).getNodes()){
			
			if (node.getDegree()==0){
				
				result.add(node);
			}
		}
		//
		return result;
	}
	
	public static<E> Nodes<E> getBottoms (Graph<E> source){
		Nodes<E> result;
		
		result = new Nodes<E>();
		
		for (Node<E> node : GraphMaker.removeLoops(source).getNodes()){
						
			if (node.getOutDegree()==0 && node.getInDegree()>0){
								
				result.add(node);
			}
		}
		//
		return result;
	}
	
	public static<E> Nodes<E> getDownForks (Graph<E> source){
		Nodes<E> result;
		
		result = new Nodes<E>();
		
		for (Node<E> node : GraphMaker.removeLoops(source).getNodes()){
			
			if (node.getOutDegree()>1){
				
				result.add(node);
			}
		}
		//
		return result;
	}
	
	public static<E> Nodes<E> getUpForks (Graph<E> source){
		Nodes<E> result;
		
		result = new Nodes<E>();
		
		for (Node<E> node : GraphMaker.removeLoops(source).getNodes()){
			
			if (node.getInDegree()>1){
				
				result.add(node);
			}
		}
		//
		return result;
	}
	
	public static<E> Map<Node<E>,Map<String,Value>> getNodeStatistics(Graph<E> source, List<String> labels){
		Map<Node<E>,Map<String,Value>> result;
		
		result = new TreeMap<Node<E>,Map<String,Value>>();
		List<Node<E>> nodes = source.getNodes().toListSortedById();
				
		for (Node<E> node : nodes){
			result.put(node,new TreeMap<String,Value>());
		}
		
		for (String label : labels){
			Values values = NodeValuator.get(source, label);
			for (int idx=0;idx<source.nodeCount();idx++){
				result.get(nodes.get(idx)).put(label, values.get(idx));
			}
		}
		
		//
		return result;
	}
	
	public static<E> Map<String,Map<String,Value>> getNodeStatisticsByLabel(Graph<E> source, List<String> labels){
		Map<String,Map<String,Value>> result;
		
		result = new TreeMap<String,Map<String,Value>>();
		List<Node<E>> nodes = source.getNodes().toListSortedById();
				
		for (Node<E> node : nodes){
			result.put(node.getLabel(),new TreeMap<String,Value>());
		}
		
		if (labels!=null){
			for (String label : labels){
				Values values = NodeValuator.get(source, label);
				for (int idx=0;idx<source.nodeCount();idx++){
					result.get(nodes.get(idx).getLabel()).put(label, values.get(idx));
				}
			}
		}
		
		//
		return result;
	}


}
