package org.tip.puckgui.views;

import java.util.ArrayList;
import java.util.List;

import org.tip.puck.census.chains.Chain;
import org.tip.puck.census.chains.ChainMaker;
import org.tip.puck.net.FiliationType;
import org.tip.puckgui.views.mas.AgnaticCousinsWeightFactor;
import org.tip.puckgui.views.mas.CousinsWeightFactor;
import org.tip.puckgui.views.mas.Divorce2WeightFactor;
import org.tip.puckgui.views.mas.DivorceWeightFactor;
import org.tip.puckgui.views.mas.NormalAgeDifferenceWeightFactor;
import org.tip.puckgui.views.mas.NormalAgeWeightFactor;
import org.tip.puckgui.views.mas.PregnancyWeightFactor;
import org.tip.puckgui.views.mas.UterineCousinsWeightFactor;
import org.tip.puckgui.views.mas.WeightFactor;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class RandomCorpusCriteria {

	private int year;
	private int initialPopulation;
	private int maxAge;
	private int minAge;
	private int samples;
	double meanAgeDifference;
	double stdevAgeDifference;
	double marriageRate;
	double fertilityRate;
	double divorceRate;
	double polygamyRateMen;
	double polygamyRateWomen;
	FiliationType cousinPreferenceType;
	double cousinPreferenceWeight;

	private double sameMarriageProbability;
	private List<WeightFactor> weightFactors;
	private String illegalMarriages;
	private boolean mas;

	/**
	 * 
	 */
	public RandomCorpusCriteria() {

		this.initialPopulation = 500;
		this.year = 300;
		this.maxAge = 50;
		this.minAge = 15;
		this.meanAgeDifference = 0.;
		this.stdevAgeDifference = 7.;
		this.marriageRate = 0.2;
		this.fertilityRate = 2.;
		this.divorceRate = 0.01;

		this.samples = 10000;

		this.cousinPreferenceType = FiliationType.COGNATIC;
		this.cousinPreferenceWeight = 1;

		this.sameMarriageProbability = 0.5;
		this.weightFactors = new ArrayList<WeightFactor>();
		this.illegalMarriages = "X(X)\nXX(X)\nX(X)X";
	}

	public FiliationType getCousinPreferenceType() {
		return this.cousinPreferenceType;
	}

	public double getCousinPreferenceWeight() {
		return this.cousinPreferenceWeight;
	}

	public double getDivorceRate() {
		return this.divorceRate;
	}

	public double getFertilityRate() {
		return this.fertilityRate;
	}

	public String getIllegalMarriages() {
		return this.illegalMarriages;
	}

	public int getInitialPopulation() {
		return this.initialPopulation;
	}

	public double getMarriageRate() {
		return this.marriageRate;
	}

	public int getMaxAge() {
		return this.maxAge;
	}

	public double getMeanAgeDifference() {
		return this.meanAgeDifference;
	}

	public int getMinAge() {
		return this.minAge;
	}

	public double getPolygamyRateMen() {
		return this.polygamyRateMen;
	}

	public double getPolygamyRateWomen() {
		return this.polygamyRateWomen;
	}

	public double getSameMarriageProbability() {
		return this.sameMarriageProbability;
	}

	public int getSamples() {
		return this.samples;
	}

	public double getStdevAgeDifference() {
		return this.stdevAgeDifference;
	}

	public WeightFactor getWeightFactor(final WeightFactor.Type type) {
		WeightFactor result;

		result = null;
		for (WeightFactor factor : this.weightFactors) {
			if (factor.getType() == type) {
				result = factor;
				break;
			}
		}
		//
		return result;
	}

	public int getYear() {
		return this.year;
	}

	public boolean isMas() {
		return this.mas;
	}

	public void setCousinPreferenceType(final FiliationType cousinPreferenceType) {
		this.cousinPreferenceType = cousinPreferenceType;
	}

	public void setCousinPreferenceWeight(final double cousinPreferenceWeight) {
		this.cousinPreferenceWeight = cousinPreferenceWeight;
	}

	public void setDivorceRate(final double divorceRate) {
		this.divorceRate = divorceRate;
	}

	public void setFertilityRate(final double fertilityRate) {
		this.fertilityRate = fertilityRate;
	}

	public void setIllegalMarriages(final String illegalMarriages) {
		this.illegalMarriages = illegalMarriages;
	}

	public void setInitialPopulation(final int initialPopulation) {
		this.initialPopulation = initialPopulation;
	}

	public void setMarriageRate(final double marriageRate) {
		this.marriageRate = marriageRate;
	}

	public void setMas(final boolean mas) {
		this.mas = mas;
	}

	public void setMaxAge(final int maxAge) {
		this.maxAge = maxAge;
	}

	public void setMeanAgeDifference(final double meanAgeDifference) {
		this.meanAgeDifference = meanAgeDifference;
	}

	public void setMinAge(final int minAge) {
		this.minAge = minAge;
	}

	public void setPolygamyRateMen(final double polygamyRateMen) {
		this.polygamyRateMen = polygamyRateMen;
	}

	public void setPolygamyRateWomen(final double polygamyRateWomen) {
		this.polygamyRateWomen = polygamyRateWomen;
	}

	public void setSameMarriageProbability(final double sameMarriageProbability) {
		this.sameMarriageProbability = sameMarriageProbability;
	}

	public void setSamples(final int samples) {
		this.samples = samples;
	}

	public void setStdevAgeDifference(final double stdevAgeDifference) {
		this.stdevAgeDifference = stdevAgeDifference;
	}

	public void setYear(final int year) {
		this.year = year;
	}

	/**
	 * 
	 * @return
	 */
	public String toMASConfig() {
		String result;

		//
		StringList buffer = new StringList();
		buffer.appendln("# simulation parameters");
		buffer.append("years ").appendln(this.year);
		buffer.append("initialpopulation ").appendln(this.initialPopulation);
		buffer.append("probsamemarriage ").appendln(this.sameMarriageProbability);
		buffer.append("fertilityrate ").appendln(this.fertilityRate);
		buffer.append("maxage ").appendln(this.maxAge);
		buffer.append("samples ").appendln(this.samples);
		buffer.appendln();
		buffer.appendln("# weight factors");
		for (WeightFactor factor : this.weightFactors) {
			switch (factor.getType()) {
				case DIVORCE:
					DivorceWeightFactor divorceFactor = (DivorceWeightFactor) factor;
					buffer.append("factor Divorce(").append(divorceFactor.getProbability()).appendln(")");
				break;
				case DIVORCE2:
					Divorce2WeightFactor divorce2Factor = (Divorce2WeightFactor) factor;
					buffer.append("factor Divorce2(").append(divorce2Factor.getFemaleProbability()).append(", ").append(divorce2Factor.getMaleProbability())
							.append(", ").append(divorce2Factor.getBothProbability()).appendln(")");
				break;
				case NORMAL_AGE:
					NormalAgeWeightFactor normalAgeFactor = (NormalAgeWeightFactor) factor;
					buffer.append("factor NormalAge(").append(normalAgeFactor.getGender().toString()).append(", ").append(normalAgeFactor.getMean())
							.append(", ").append(normalAgeFactor.getStdev()).appendln(")");
				break;
				case NORMAL_AGE_DIFFERENCE:
					NormalAgeDifferenceWeightFactor differenceFactor = (NormalAgeDifferenceWeightFactor) factor;
					buffer.append("factor NormalDifferenceAge(").append(differenceFactor.getMean()).append(", ").append(differenceFactor.getStdev())
							.appendln(")");
				break;

				case COUSINS:
					CousinsWeightFactor cousinsFactor = (CousinsWeightFactor) factor;
					buffer.append("factor Cousins(").append(cousinsFactor.getFirst()).append(", ").append(cousinsFactor.getSecond()).append(", ")
							.append(cousinsFactor.getThird()).appendln(")");
				break;

				case AGNATICCOUSINS:
					AgnaticCousinsWeightFactor agnaticCousinsFactor = (AgnaticCousinsWeightFactor) factor;
					buffer.append("factor Cousins(").append(agnaticCousinsFactor.getFirst()).append(", ").append(agnaticCousinsFactor.getSecond()).append(", ")
							.append(agnaticCousinsFactor.getThird()).appendln(")");
				break;

				case UTERINECOUSINS:
					UterineCousinsWeightFactor uterineCousinsFactor = (UterineCousinsWeightFactor) factor;
					buffer.append("factor Cousins(").append(uterineCousinsFactor.getFirst()).append(", ").append(uterineCousinsFactor.getSecond()).append(", ")
							.append(uterineCousinsFactor.getThird()).appendln(")");
				break;

				case PREGNANCY:
					PregnancyWeightFactor pregnancyFactor = (PregnancyWeightFactor) factor;
					buffer.append("factor Pregnancy(").append(pregnancyFactor.getFirst()).appendln(")");
				break;
			}
		}
		// buffer.append("cousins 1.0 1.0 1.0");
		buffer.appendln();
		buffer.appendln(positionalNotationToPrologCode(this.illegalMarriages));

		//
		result = buffer.toString();

		//
		return result;
	}

	public List<WeightFactor> weightFactors() {
		return this.weightFactors;
	}

	/**
	 * This method translates a position notation relation in Prolog code.
	 * 
	 * @param formula
	 *            "XX(F)X"
	 */
	public static String positionalNotationToPrologCode(final String formula) {
		String result;

		StringList buffer = new StringList();

		List<Character> var = new ArrayList<Character>();
		var.add(' ');
		for (char c = 'A'; c <= 'Z'; c++) {
			var.add(c);
		}

		buffer.appendln("# illegal marriages");
		buffer.appendln("start prolog");
		buffer.appendln("parent(X, Y) :- mother(X, Y).\nparent(X, Y) :- father(X, Y).");

		String[] kinStrings = formula.split("\\s");
		for (String kinString : kinStrings) {
			Chain chain = ChainMaker.fromString(kinString);
			buffer.appendln("illegal(" + var.get(chain.getId(0)) + "," + var.get(chain.getLast().getId()) + ") :- " + chain.getPrologString() + ".");
		}

		//
		result = buffer.toString();

		//
		return result;
	}
}
