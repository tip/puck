package org.tip.puckgui.views.kinoath;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.kinoath.KinOathDiagram;
import org.tip.puck.kinoath.Puck2KinOath;
import org.tip.puck.net.Individuals;
import org.tip.puckgui.NetGUI;

/**
 * 
 * @author TIP
 */
public class GlobalDiagramPanel extends JPanel {

	private static final long serialVersionUID = 7330248673179338202L;

	private static final Logger logger = LoggerFactory.getLogger(GlobalDiagramPanel.class);

	int reportCounter = 0;
	private JPanel thisJPanel;
	private NetGUI netGUI;
	KinOathDiagram diagram;
	private KinOathDiagramPanel diagramScrollPanel;

	/**
	 * Initialize the contents of the frame.
	 */
	public GlobalDiagramPanel(final NetGUI guiManager, final Individuals source) {

		//
		this.thisJPanel = this;
		this.netGUI = guiManager;

		//
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		JPanel diagramZonePanel = new JPanel();
		add(diagramZonePanel);
		diagramZonePanel.setLayout(new BorderLayout(0, 0));

		JPanel parametersPanel = new JPanel();
		diagramZonePanel.add(parametersPanel, BorderLayout.NORTH);
		parametersPanel.setBorder(new EmptyBorder(0, 0, 2, 0));
		parametersPanel.setLayout(new BoxLayout(parametersPanel, BoxLayout.X_AXIS));

		JButton btnResetZoom = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("GlobalDiagramPanel.btnResetZoom.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnResetZoom.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Center button.
				GlobalDiagramPanel.this.diagramScrollPanel.resetZoom();
			}
		});

		Component horizontalGlue_1 = Box.createHorizontalGlue();
		parametersPanel.add(horizontalGlue_1);
		parametersPanel.add(btnResetZoom);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		parametersPanel.add(horizontalStrut);

		JButton btnExport = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("GlobalDiagramPanel.btnExport.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnExport.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Export.
				try {
					GlobalDiagramPanel.this.diagramScrollPanel.export();

				} catch (IOException exception) {
					// TODO Auto-generated catch block
					exception.printStackTrace();
				} catch (PuckException exception) {
					// TODO Auto-generated catch block
					exception.printStackTrace();
				}
			}
		});

		JButton btnCopy = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("GlobalDiagramPanel.btnCopy.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnCopy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Copy to clipboard.
				GlobalDiagramPanel.this.diagramScrollPanel.copyToClipboard();
			}
		});
		parametersPanel.add(btnCopy);

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		parametersPanel.add(horizontalStrut_2);
		parametersPanel.add(btnExport);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		parametersPanel.add(horizontalStrut_1);

		JPanel diagramPanel = new JPanel();
		diagramZonePanel.add(diagramPanel, BorderLayout.CENTER);

		// ///////////////////////////////////////////////
		this.diagramScrollPanel = new KinOathDiagramPanel(Puck2KinOath.convert(source));

		diagramPanel.setLayout(new BorderLayout(0, 0));
		diagramPanel.add(this.diagramScrollPanel);

		JLabel lblHelp = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("GlobalDiagramPanel.lblHelp.text")); //$NON-NLS-1$ //$NON-NLS-2$
		diagramZonePanel.add(lblHelp, BorderLayout.SOUTH);

		JPanel buttonsPanel = new JPanel();
		add(buttonsPanel);
		buttonsPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));

		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		buttonsPanel.add(horizontalStrut_6);

		JButton btnClose = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualDiagramPanel.btnClose.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close tab.
				GlobalDiagramPanel.this.netGUI.closeCurrentTab();
			}
		});
		buttonsPanel.add(btnClose);

		Component horizontalGlue = Box.createHorizontalGlue();
		buttonsPanel.add(horizontalGlue);

		JPanel panel_13 = new JPanel();
		buttonsPanel.add(panel_13);
		panel_13.setLayout(new BoxLayout(panel_13, BoxLayout.X_AXIS));
	}

	/**
	 * 
	 */
	public void update() {
		//
		updateIndividualDiagram();
	}

	/**
	 * 
	 */
	public void updateIndividualDiagram() {
		//
		updateIndividualDiagram(this.netGUI.getCurrentIndividuals());
	}

	/**
	 * 
	 */
	public void updateIndividualDiagram(final Individuals source) {
		//
		if (source == null) {
			//
			logger.debug("updateIndividualDiagram(null)");

		} else {
			//
			logger.debug("updateIndividualDiagram " + source.size());

			//
			this.diagramScrollPanel.updateSource(Puck2KinOath.convert(source));
		}
	}
}
