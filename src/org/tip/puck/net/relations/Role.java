package org.tip.puck.net.relations;

import org.tip.puck.net.Individual;
import org.tip.puck.util.Numberable;


/**
 * 
 * @author TIP
 */
public class Role implements Comparable<Role>, Numberable {

	protected String name;
	protected int defaultCardinality;
	protected int id;
	String selfName;
//	protected RoleRelations relations;
	
	public Role(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * 
	 */
	@Override
	public String hashKey() {
		String result;

		result = name + this.id;

		//
		return result;
	}

	/**
	 * 
	 * @param role
	 */
	public Role(final Role role) {

		if (role == null) {
			//
			this.name = "";
			this.defaultCardinality = 0;

		} else {
			//
			this.name = role.getName();
			this.defaultCardinality = role.getDefaultCardinality();
		}
	}
	
	public Role clone(){
		return new Role(this);
	}
	
	public Role clone(int id){
		Role result;
		
		result = clone();
		result.setId(id);
		
		//
		return result;
	}


	/**
	 * 
	 * @param name
	 * @param defaultCardinality
	 */
	public Role(final String name) {

		this.name = name;
		this.defaultCardinality = 0;
	}

	/**
	 * 
	 * @param name
	 * @param defaultCardinality
	 */
	public Role(final String name, final int defaultCardinality) {
		this.name = name;
		this.defaultCardinality = defaultCardinality;
	}

	/**
	 * 
	 */
	@Override
	public boolean equals(final Object role) {
		boolean result;
		
		result = role!=null && this.name.equals(((Role) role).name);
		
		//
		return result;
	}
	
	public static boolean equalOrBothNull(Role alpha, Role beta){
		return (alpha==null && beta==null) || (alpha!=null && alpha.equals(beta));
	}


	public int getDefaultCardinality() {
		return this.defaultCardinality;
	}

	public String getName() {
		return this.name;
	}
	
	public boolean hasName(String name){
		return this.name!=null && this.name.equals(name);
	}

	public void setDefaultCardinality(final int defaultCardinality) {
		this.defaultCardinality = defaultCardinality;
	}

	public void setName(final String name) {
		this.name = name;
	}
	
	@Override
	public String toString(){
		return this.name;
	}
	
	public int compareTo (Role other){
		int result;
		
		if (other==null || name == null || other.name == null){
			result = 0;
		} else if (hasName(selfName) && !other.hasName(selfName)){
			result = -1;
		} else if (!hasName(selfName) && other.hasName(selfName)){
			result = 1;
		} else {
			result = name.compareTo(other.name);
		}
		
		//
		return result;
	}
	
	@Override
	public int hashCode(){
		return toString().hashCode();
	}
	
	
	
	public String getSelfName() {
		return selfName;
	}

	public boolean hasSelfName(){
		return name.equals(selfName);
	}

	public void setSelfName(String selfName) {
		this.selfName = selfName;
	}
	
	
	
	
}
