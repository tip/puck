package org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.containers;

import org.tip.puck.visualization.layouts.GraphSource;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.ColoredChain;

/**
 * @date Jun 24, 2012
 * @author Edoardo Savoia
 */
public class LeafContainer extends ChainContainer {

    public LeafContainer(ColoredChain chain) {
	super(chain);
    }

    @Override
    public boolean isLeaf() {
	return true;
    }

    @Override
    public int getWidth() {
	return 1;
    }

    @Override
    public void populateChildren(GraphSource graphSource) {
	// do nothing
    }

    
}
