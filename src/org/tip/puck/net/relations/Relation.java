package org.tip.puck.net.relations;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.PuckException;
import org.tip.puck.net.Attributable;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Attributes;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Populatable;
import org.tip.puck.net.relations.workers.RelationValuator;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.Ordinal;
import org.tip.puck.util.Numberable;
import org.tip.puck.util.Value;

/**
 * 
 * @author TIP
 */
public class Relation implements Numberable, Attributable, Populatable {

	private int id;
	private int typedId;
	private RelationModel model;
	private String name;
	private Actors actors;
	private Attributes attributes;

	
	public Relation(final RelationModel model){
		
		this.model = model;
		this.actors = new Actors();
		this.attributes = new Attributes();

	}
	
	
	/**
	 * 
	 * @param id
	 * @param model
	 * @param name
	 * @param actors
	 */
	public Relation(final int id, final int typedId, final RelationModel model, final String name, final Actor... actors) {
		this.id = id;
		this.typedId = typedId;
		this.model = model;
		this.name = name;
		this.actors = new Actors();
		for (Actor actor : actors) {
			this.actors.add(actor);
		}
		this.attributes = new Attributes();
	}

	/**
	 * 
	 * @return
	 */
	public Actors actors() {
		return this.actors;
	}

	public boolean addActor(final Individual individual, final Role role) {
		boolean result;
		
		result = actors().add(new Actor(individual, role));
		individual.relations().add(this);
		
		//
		return result;
	}
	

	public boolean addNewActor(final Individual individual, final Role role) {
		boolean result;
		
		Actor actor = new Actor(individual, role);

		result = actors().addNew(actor);
		if (result){
			individual.relations().add(this);
		}
		
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Attributes attributes() {
		Attributes result;

		result = this.attributes;

		//
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		boolean result;

		result = obj != null && getId() == ((Relation) obj).getId();
		//
		return result;
	}

	public Actor getActor(final Actor actor) {
		Actor result;

		result = null;
		for (Actor thisActor : this.actors) {
			if (thisActor.equals(actor)) {
				result = thisActor;
				break;
			}
		}
		//
		return result;
	}

	public Actor getActor(final Individual individual, final String roleName) {
		Actor result;

		result = null;
		Actors actors = actors().getById(individual.getId()).getByRole(roleName);
		if (actors.size() > 0) {
			result = actors.get(0);
		}
		//
		return result;
	}

	/**
	 * 
	 */
	public String getAttributeValue(final String label) {
		String result;

		Attribute attribute = this.attributes().get(label);
		if (attribute == null) {
			result = null;
		} else {
			result = attribute.getValue();
		}

		//
		return result;
	}

	public Actors getDependants(final Actor referent) {
		Actors result;

		result = new Actors();

		if (referent != null) {
			for (Actor actor : this.actors) {
				if (actor.getReferent() != null && actor.getReferent().equals(referent.getIndividual())) {
					result.add(actor);
				}
			}
		}

		//
		return result;
	}

	public Individual getFirstIndividual() {
		Individual result;

		result = null;
		Individuals individuals = getIndividuals();

		if (individuals != null) {
			for (Individual individual : individuals) {
				result = individual;
				break;
			}
		}
		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getId() {
		return this.id;
	}

	public Individuals getIndividuals() {
		Individuals result;

		result = new Individuals();

		for (Actor actor : this.actors) {
			result.add(actor.getIndividual());
		}

		//
		return result;
	}
	
	public Individuals getIndividuals(Segmentation segmentation){
		Individuals result;
		
		result = new Individuals();
		
		for (Individual individual : getIndividuals()){
			if (segmentation.getCurrentIndividuals().contains(individual)){
				result.add(individual);
			}
		}
		
		//
		return result;

	}
	
	
	public Individuals getAllIndividuals (List<Ordinal> times) {
		return getIndividuals();
	}

	public Individuals getIndividuals(final List<String> roleNames) {
		Individuals result;

		result = new Individuals();

		for (Actor actor : this.actors) {
			if (roleNames.contains(actor.getRole().getName())) {
				result.add(actor.getIndividual());
			}
		}

		//
		return result;
	}

	public Individuals getIndividuals(final String roleName) {
		Individuals result;

		result = new Individuals();

		for (Actor actor : this.actors) {
			if (actor.getRole().getName().equals(roleName)) {
				result.add(actor.getIndividual());
			}
		}

		//
		return result;
	}

	public RelationModel getModel() {
		return this.model;
	}

	public String getName() {
		return this.name;
	}

	public Individuals getReferents(final Individual ego) {
		Individuals result;

		result = new Individuals();

		if (ego != null) {
			for (Actor actor : this.actors) {
				if (actor.getIndividual() == ego) {
					if (actor.getReferent() != null && !result.contains(actor.getReferent())) {
						result.add(actor.getReferent());
					}
				}
			}
		}
		//
		return result;
	}

	public List<String> getRoleNames(final Individual individual) {
		List<String> result;

		result = new ArrayList<String>();

		for (Actor actor : this.actors) {
			if (actor.getIndividual().equals(individual)) {
				result.add(actor.getRole().getName());
			}
		}

		//
		return result;
	}

	public String getRoleNamesAsString(final Individual individual) {
		String result;

		result = "";

		for (Actor actor : this.actors) {
			if (actor.getIndividual().equals(individual)) {
				result += actor.getRole().getName() + " ";
			}
		}

		//
		return result;
	}

	public Roles getRoles(final Individual individual) {
		Roles result;

		result = new Roles();

		for (Actor actor : this.actors) {
			if (actor.getIndividual().equals(individual)) {
				result.add(actor.getRole());
			}
		}

		//
		return result;
	}

	public Integer getTime(final String dateLabel) {
		Integer result;

		Value timeValue = RelationValuator.get(this, dateLabel, null);
		if (timeValue != null) {
			result = timeValue.intValue();
		} else {
			result = null;
		}
		//
		return result;
	}

	public int getTypedId() {
		return this.typedId;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public boolean hasActor(final Actor actor) {
		boolean result;

		if (actor == null) {
			result = false;
		} else {
			result = this.actors.hasActor(actor.getId(), actor.getRole().getName());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public boolean hasActor(final Individual individual) {
		boolean result;

		if (individual == null) {
			result = false;
		} else {
			result = this.actors.hasActor(individual.getId());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public boolean hasActor(final Individual individual, final String role) {
		boolean result;

		if (individual == null) {
			result = false;
		} else {
			result = this.actors.hasActor(individual.getId(), role);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public boolean hasActor(final int id) {
		boolean result;

		result = this.actors.hasActor(id);

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @param role
	 * @return
	 */
	public boolean hasActor(final int id, final String role) {
		boolean result;

		result = this.actors.hasActor(id, role);

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public boolean hasActors(final String... roleNames) {
		boolean result;

		result = false;

		for (String roleName : roleNames) {
			if (this.actors.getByRole(roleName).size() > 0) {
				result = true;
				break;
			}
		}

		//
		return result;
	}
	
	
	/***
	 * gets all actors that participate in relation  but not in other relation
	 * @param alpha
	 * @param other
	 * @return
	 */
	public Actors getDifferentwActors (Relation other){
		Actors result;
		
		result = new Actors();
		
		if (other==null){
			
			result.addAll(actors);

		} else {
		
			for (Actor actor: actors){
				if (!other.actors().contains(actor)){
					result.add(actor);
				}
			}
		}
		//
		return result;
	}

	/**
	 * note: each relation is supposed to have "null" as attribute value
	 * 
	 * @param label
	 * @return
	 */
	public boolean hasAttributeValue(final String label) {
		boolean result;

		if (label == null) {
			result = true;
		} else {
			result = (getAttributeValue(label) != null);
		}

		return result;
	}

	/**
	 * 
	 */
	@Override
	public String hashKey() {
		String result;

		result = "" + this.id;

		//
		return result;
	}

	public boolean hasRole(final Individual individual, final String roleName) {
		boolean result = false;

		for (Actor actor : this.actors) {
			if (actor.getIndividual() == individual && actor.getRole().getName().equals(roleName)) {
				result = true;
				break;
			}
		}
		//
		return result;
	}

	public boolean hasTime(final String dateLabel, final Integer time) {
		boolean result;

		result = (getTime(dateLabel) != null && getTime(dateLabel).equals(time));

		//
		return result;
	}

	/**
	 * 
	 * @param pattern
	 * @return
	 */
	public boolean matches(final String pattern) {
		boolean result;

		if (StringUtils.isBlank(pattern)) {
			result = false;
		} else {
			String targetPattern = pattern.toLowerCase();

			if (this.name.toLowerCase().contains(targetPattern)) {
				result = true;
			} else {
				boolean ended = false;
				result = false;
				Iterator<Actor> iterator = this.actors.iterator();
				while (!ended) {
					if (iterator.hasNext()) {
						Actor actor = iterator.next();
						if (actor.getIndividual().getName().toLowerCase().contains(targetPattern)) {
							ended = true;
							result = true;
						}
					} else {
						ended = true;
						result = false;
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 */
	public void removeActor(final Actor actor) {
		this.actors.remove(actor);
		actor.getIndividual().relations().remove(this);
	}

	/**
	 * 
	 * @param id
	 */
	public void removeActor(final int id) {
		for (Actor actor : this.actors.toList()) {
			if (actor.getId() == id) {
				this.actors.remove(actor);
			}
		}
	}

	/**
	 * 
	 * @param label
	 * @param value
	 */
	public void setAttribute(final String label, final String value) {
		this.attributes().put(label, value);
	}

	/**
	 * 
	 */
	@Override
	public void setId(final int id) {
		this.id = id;
	}

	public void setModel(final RelationModel model) {
		this.model = model;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setTypedId(final int typedId) {
		this.typedId = typedId;
	}

	@Override
	public String toString() {
		String result;
		
		result = this.model + " " + this.id + " " + this.name;
		
		if (this.id!=this.typedId){
			result += " (" + this.typedId + ")";
		}
		//
		return result;
	}

	public void updateReferents(final String roleName) {
		Individual referent = null;
		for (Actor actor : this.actors) {
			if (actor.getRole().getName().equals(roleName) && actor.getReferent() == null) {
				referent = actor.getIndividual();
				break;
			}
		}
		if (referent != null) {
			for (Actor actor : this.actors) {
				if (actor.getReferent() == null && actor.getIndividual() != referent && !actor.getRole().getName().equals(roleName)) {
					actor.setReferent(referent);
				}
			}
		}
	}
	
	public String getActorsAsString(String roleName, RelationEnvironment egoEnvironment) throws PuckException{
		String result;
		
		result = "";
		
		List<String> actorNames = new ArrayList<String>();
		
		for (Actor actor: actors().getByRole(roleName).toSortedList()){
			if (actor.getIndividual()!=egoEnvironment.getEgo() || !roleName.equals("MIG")){
				List<String> relations = egoEnvironment.getRelationsByAlter().get(actor.getIndividual());
				relations.remove("KIN");
				actorNames.add(actor.getIndividual()+" "+relations);
				
			}
		}
		
		if (!actorNames.isEmpty()){
			result = actorNames.toString();
		} else if (roleName.equals("HOST")){
			if (RelationValuator.isBirth(this) && egoEnvironment.getEgo().getMother()!=null){
				result += egoEnvironment.getEgo().getMother()+" [MOTHER]";
			} else {
				for (String impersonalRelation : RelationValuator.getImpersonalRelationTypes(this, egoEnvironment.getEgo(), egoEnvironment.getEgoRoleName(), egoEnvironment.getImpersonalAlterLabel(), egoEnvironment.getRelationsByAlter())){
					result += impersonalRelation+" ";
				}
			}
		}
		
		//
		return result;
	}
	
	public String getActorsAsString(RelationEnvironment egoEnvironment) throws PuckException{
		String result;
		
		result = "";
		for (String roleName : egoEnvironment.getRoleNames()){
			result += roleName+":"+getActorsAsString(roleName,egoEnvironment)+"\t";
		}
		//
		return result;
	}
	
	public Relation clone(){
		Relation result;
		
		result = new Relation(id, typedId, model, name);
		for (Actor actor : actors){
			result.addActor(actor.getIndividual(), actor.getRole());
		}
		for (Attribute attribute : attributes) {
			result.attributes.add(attribute);
		}
		//
		return result;

	}
	
	public Relation clone(int id){
		Relation result;
		
		result = clone();
		result.setId(id);
		
		//
		return result;
	}

	public Relation ageFiltered(int threshold, int time) {
		Relation result;
		
		result = new Relation(id, typedId, model, name);
		
		for (Actor actor : this.actors) {
			
			if (IndividualValuator.ageAtYear(actor.getIndividual(),time) >= threshold) {
				result.actors.add(actor);
			} else if (IndividualValuator.getBirthYear(actor.getIndividual())==null){
				result.actors.add(actor);
			}
		}
		
		for (Attribute attribute : attributes) {
			result.attributes.add(attribute);
		}
		
		//
		return result;
	}

}
