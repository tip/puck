package org.tip.puck.util;

/**
 * 
 * @author TIP
 */
public class StatsInt {

	private int value;
	private int min;
	private int max;
	private int average;

	/**
	 * 
	 */
	public StatsInt() {
	}

	/**
	 * 
	 */
	public StatsInt(final int value, final int min, final int max, final int average) {
		set(value, min, max, average);
	}

	public int average() {
		return average;
	}

	/**
	 * 
	 * @return
	 */
	public int inc() {
		int result;

		this.value += 1;
		result = this.value;

		//
		return result;
	}

	public int max() {
		return max;
	}

	public int min() {
		return min;
	}

	public void set(final int value) {
		this.value = value;
	}

	/**
	 * 
	 * @param value
	 * @param menValue
	 * @param womenValue
	 * @param unknownValue
	 * 
	 * @return
	 */
	public StatsInt set(final int value, final int min, final int max, final int average) {
		StatsInt result;

		this.value = value;
		this.min = min;
		this.max = max;
		this.average = average;

		result = this;

		//
		return result;
	}

	public void setAverage(final int average) {
		this.average = average;
	}

	public void setMax(final int max) {
		this.max = max;
	}

	public void setMin(final int min) {
		this.min = min;
	}

	public int value() {
		return value;
	}
}
