package org.tip.puck.params;

/**
 * @author Telmo Menezes
 *
 */
public class ParamException extends Exception {
    private String msg;
    
    
    public ParamException(String msg) {
        this.msg = msg;
    }
    
    private static final long serialVersionUID = 3425301925945605993L;

    public String toString(){
        return msg;
    }
}
