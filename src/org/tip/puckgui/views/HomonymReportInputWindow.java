package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.report.Report;
import org.tip.puck.statistics.StatisticsReporter;
import org.tip.puckgui.NetGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class HomonymReportInputWindow extends JFrame {

	private static final long serialVersionUID = -2468474671430036373L;

	private JFrame thisJFrame;
	private NetGUI netGUI;
	private JPanel contentPane;
	private static int lastNrPartsValue = 0;
	private static int lastMaxCountValue = 1;
	private JSpinner spinnerNumberOfNameParts;
	private JSpinner spinnerMinimalNumberOfNames;

	/**
	 * Create the frame.
	 */
	public HomonymReportInputWindow(final NetGUI netGUI) {
		//

		// /////////////////////////////////
		setIconImage(Toolkit.getDefaultToolkit().getImage(HomonymReportInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		this.netGUI = netGUI;
		setTitle("List Homonyms");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 365, 200);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		this.contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblNameparts = new JLabel("<html><div style=\"text-align:right\">Name parts<br/>(0 = Full name) </div></html>");
		panel.add(lblNameparts, "2, 4, right, default");

		this.spinnerNumberOfNameParts = new JSpinner();
		this.spinnerNumberOfNameParts.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		panel.add(this.spinnerNumberOfNameParts, "4, 4");

		JLabel lblMinimalNumberOf = new JLabel("<html><div style=\"text-align:right\">Minimal number<br/>of names </div></html>");
		panel.add(lblMinimalNumberOf, "2, 6, center, default");

		this.spinnerMinimalNumberOfNames = new JSpinner();
		this.spinnerMinimalNumberOfNames.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		panel.add(this.spinnerMinimalNumberOfNames, "4, 6");

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		getRootPane().setDefaultButton(btnLaunch);
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					//
					int nrParts = (Integer) HomonymReportInputWindow.this.spinnerNumberOfNameParts.getValue();
					int maxCount = (Integer) HomonymReportInputWindow.this.spinnerMinimalNumberOfNames.getValue();

					//
					lastNrPartsValue = nrParts;
					lastMaxCountValue = maxCount;

					Report report = StatisticsReporter.reportHomonyms(netGUI.getSegmentation(), nrParts, maxCount);

					netGUI.addReportTab(report);

					//
					dispose();

				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(HomonymReportInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);

		// ///////////////////
		//
		this.spinnerMinimalNumberOfNames.setValue(lastMaxCountValue);
		this.spinnerNumberOfNameParts.setValue(lastNrPartsValue);

	}
}
