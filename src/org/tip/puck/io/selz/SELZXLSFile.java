package org.tip.puck.io.selz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.io.xls.XLSBufferedReader;
import org.tip.puck.io.xls.XLSWriter;
import org.tip.puck.net.Net;

/**
 * This class represents a TXT File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class SELZXLSFile {

	protected enum Status {
		MANDATORY,
		OPTIONAL
	}

	public static final int MAX_LINE_SIZE = 1024;;

	private static final Logger logger = LoggerFactory.getLogger(SELZXLSFile.class);

	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file) throws PuckException {
		Net result;

		BufferedReader in = null;
		try {
			in = new XLSBufferedReader(file);
			result = SELZTXTFile.read(in);

			result.setLabel(file.getName());

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create(exception, "Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create(exception, "Opening file [" + file + "]");
		} catch (IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Opening file [" + file + "]");
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException exception) {
				logger.warn("Not managed error.");
				exception.printStackTrace();
			}
		}

		//
		return result;
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File file, final Net source) throws PuckException {
		PrintWriter out = null;
		try {
			//
			if (source.attributes().isEmpty()) {
				//
				out = new PrintWriter(new XLSWriter(file, "Individuals", "Families"));

			} else {
				//
				out = new PrintWriter(new XLSWriter(file, "Corpus Attributes", "Individuals", "Families"));
			}

			//
			SELZTXTFile.write(out, source);

		} catch (IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Opening file [" + file + "].");
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * 
	 * @param file
	 * @param source
	 * @param bundle
	 * 
	 * @throws PuckException
	 */
	public static void save(final File file, final Net source, final ResourceBundle bundle) throws PuckException {

	}
}
