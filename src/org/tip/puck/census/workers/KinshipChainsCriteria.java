package org.tip.puck.census.workers;

/**
 * 
 * @author TIP
 */
public class KinshipChainsCriteria {

	private int alterId;
	private int maximalDepth;
	private int maximalOrder;
	private String chainClassification;

	/**
	 * 
	 */
	public KinshipChainsCriteria() {
		this.chainClassification = "SIMPLE";
	}

	public int getAlterId() {
		return alterId;
	}

	public String getChainClassification() {
		return chainClassification;
	}

	public int getMaximalDepth() {
		return maximalDepth;
	}

	public int getMaximalOrder() {
		return maximalOrder;
	}

	public void setAlterId(final int alterId) {
		this.alterId = alterId;
	}

	public void setChainClassification(final String chainClassification) {
		this.chainClassification = chainClassification;
	}

	public void setMaximalDepth(final int maximalDepth) {
		this.maximalDepth = maximalDepth;
	}

	public void setMaximalOrder(final int maximalOrder) {
		this.maximalOrder = maximalOrder;
	}
}
