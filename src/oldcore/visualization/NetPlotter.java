package oldcore.visualization;

import java.io.File;
import java.io.IOException;

import org.gephi.data.attributes.api.AttributeController;
import org.gephi.data.attributes.api.AttributeModel;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.io.exporter.api.ExportController;
import org.gephi.layout.plugin.force.StepDisplacement;
import org.gephi.preview.api.ColorizerFactory;
import org.gephi.preview.api.EdgeColorizer;
import org.gephi.preview.api.PreviewController;
import org.gephi.preview.api.PreviewModel;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.gephi.ranking.api.RankingController;
import org.openide.util.Lookup;
import org.tip.puck.net.Net;

public class NetPlotter {
	
	/*
	 * Plotting the Net using Gephi.
	 * */
	public void plot(Net net /*,String path*/ ){
		//HeritageType ht = HeritageType.agnatic;
		
        ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
        pc.newProject();
        Workspace workspace = pc.getCurrentWorkspace();
        
        GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getModel();
        AttributeModel attributeModel = Lookup.getDefault().lookup(AttributeController.class).getModel();
        PreviewController prev = Lookup.getDefault().lookup(PreviewController.class); 
        PreviewModel model = prev.getModel();
        
        RankingController rankingController = Lookup.getDefault().lookup(RankingController.class);
        ColorizerFactory colorizerFactory = Lookup.getDefault().lookup(ColorizerFactory.class);
        
        //VizController vc = (VizController) Lookup.getDefault().lookup(VisualizationController.class);
        //vc.initInstances();
        
        if(net == null) System.out.println("Net == null.");
        else{
        	Puck2GephiTranslator t = new Puck2GephiTranslator();
        	t.translate(net, graphModel);
        }

        /*
        //Filter      
        DegreeRangeFilter degreeFilter = new DegreeRangeFilter();
        degreeFilter.setRange(new Range(30, Integer.MAX_VALUE));     //Remove nodes with degree < 30
        Query query = filterController.createQuery(degreeFilter);
        GraphView view = filterController.filter(query);
        graphModel.setVisibleView(view);    //Set the filter result as the visible view
         */

        SimPaLayout layout = new SimPaLayout(null, new StepDisplacement(1f));
        layout.setGraphModel(graphModel);
        layout.resetPropertiesValues();
        layout.setOptimalDistance(80f);
        layout.setHeritageType(PlotOptions.getInstance().getHeritageType());
        layout.initAlgo();
        layout.goAlgo();
        if(PlotOptions.getInstance().getEqualizeGenerations()) layout.equalizeGenerations(net.depth());
        layout.endAlgo();
        
        /*
        LabelAdjust lal = new LabelAdjust(null);
        lal.setGraphModel(graphModel);
        lal.initAlgo();
        for(int i=0; i<300 && lal.canAlgo(); i++){
        	lal.goAlgo();
        }
        lal.endAlgo();
        */

        /*
        //Rank color by Degree
        NodeRanking nbrSpousesRanking = new NbrSpousesRanking(graphModel.getGraph(), PlotOptions.getInstance().getHeritageType());
        ColorTransformer nbrSpColorTransformer = rankingController.getObjectColorTransformer(nbrSpousesRanking);
        nbrSpColorTransformer.setColors(new Color[]{new Color(0xFEF0D9), new Color(0xB30000)});
        rankingController.transform(nbrSpColorTransformer);
        
        NodeRanking nbrChildrenRanking = new NbrChildrenRanking(graphModel.getGraph(), PlotOptions.getInstance().getHeritageType());
        SizeTransformer nbrChSizeTransformer = rankingController.getObjectSizeTransformer(nbrChildrenRanking);
        nbrChSizeTransformer.setMinSize(2);
        nbrChSizeTransformer.setMaxSize(9);
        rankingController.transform(nbrChSizeTransformer);
        */
        
        //EdgeRanking familyTieRanking = new FamilyTieRanking(graphModel.getGraph(), PlotOptions.getInstance().getHeritageType());
        //ColorTransformer colorTransformer = new FamilyTieColorTransformer(familyTieRanking);
        //rankingController.transform(colorTransformer);
        

        //Preview
        model.getUniEdgeSupervisor().setCurvedFlag(PlotOptions.getInstance().getParentalTiesCurved());
        model.getUniEdgeSupervisor().setShowArrowsFlag(false);
        model.getUniEdgeSupervisor().setArrowSize(2f);
        model.getUniEdgeSupervisor().setColorizer((EdgeColorizer) colorizerFactory.createEdgeOriginalColorMode());
        model.getUniEdgeSupervisor().setEdgeScale(1f);//0.1
        
        model.getBiEdgeSupervisor().setCurvedFlag(PlotOptions.getInstance().getConjugalTiesCurved());
        model.getBiEdgeSupervisor().setShowArrowsFlag(false);
        model.getBiEdgeSupervisor().setArrowSize(2f);
        model.getBiEdgeSupervisor().setColorizer((EdgeColorizer) colorizerFactory.createEdgeOriginalColorMode());
        model.getBiEdgeSupervisor().setEdgeScale(1f);//0.1
        
        model.getNodeSupervisor().setShowNodeLabels(PlotOptions.getInstance().getShowNodeLabels());
        model.getNodeSupervisor().setBaseNodeLabelFont(model.getNodeSupervisor().getBaseNodeLabelFont().deriveFont(8)); //
        
        //Export the graph to a temporary svg file
        ExportController ec = Lookup.getDefault().lookup(ExportController.class);
        try {
            ec.exportFile(new File("tmp.pdf"));
        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        }
        System.out.println("Ploting graph done");
        
        try {
        	String args = null;
        	if (System.getProperty("os.name").startsWith("Mac OS")) {
            	args = "open tmp.pdf";
    		} else if (System.getProperty("os.name").startsWith("Windows")) {
    			args = "cmd.exe /C tmp.pdf";
    		} else if (System.getProperty("os.name").startsWith("Linux")) {
    			args = "/bin/sh -c tmp.pdf";
    		}
			Runtime.getRuntime().exec(args);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        //Open a Batik SVG window
        //batikWindow();
	}
	
		
	/*
	private void batikWindow(){
        JFrame f = new JFrame("Corpus");
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                e.getWindow().dispose();
            }
        });
        final JPanel panel = new JPanel(new BorderLayout());
        JSVGCanvas svgCanvas = new JSVGCanvas();
        panel.add("Center", svgCanvas);
        File ff = new File(".tmp.svg");
        svgCanvas.setURI(ff.toURI().toString());
        //svgCanvas.flush();
        //svgCanvas.
        f.getContentPane().add(panel);
        f.setSize(400, 400);
        f.setVisible(true);
	}
	*/
}
