package org.tip.puck.partitions;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.partitions.Partition.ValueCode;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.Values;

import fr.devinsy.util.StringList;

/**
 * There is five type of criteria:
 * <ul>
 * <li>raw
 * <li>binarization
 * <li>free grouping
 * <li>sized grouping
 * <li>counted grouping
 * </ul>
 * 
 * @author TIP
 */
public class PartitionCriteria {

	public enum CumulationType {
		NONE,
		ASCENDANT,
		DESCENDANT
	}

	public enum FamilyScope {
		ALL,
		SOME
	};

	public enum PartitionType {
		RAW,
		BINARIZATION,
		FREE_GROUPING,
		SIZED_GROUPING,
		COUNTED_GROUPING,
		PARTIALIZATION;
	};

	public enum RelationModelCanonicalNames {
		INDIVIDUAL,
		FAMILY
	}

	public enum SizeFilter {
		NONE,
		TRIM,
		EMPTY,
		HOLES
	}

	public enum ValueFilter {
		NONE,
		NULL,
		ZERO
	}

	private String relationModelName;
	private String label;
	private String labelParameter;
	private PartitionType type;
	private String pattern;
	private Double start;
	private Double size;
	private Double count;
	private Double end;
	private Double min;
	private Double max;
	private Intervals intervals;
	private CumulationType cumulationType;
	private FamilyScope familyScope;
	private ValueFilter valueFilter;
	private SizeFilter sizeFilter;
	private Values values;
	private String egoRoleName;
	private Integer time;
	
	private boolean withCensus;
	private boolean withMatrix;
	private boolean withGraph;
	private boolean withDiagram;
	private boolean withTrend;
	private boolean withFlow;
	
	private ValueCode valueCode;

	/**
	 * 
	 */
	public PartitionCriteria() {
		this.type = PartitionType.RAW;
		this.cumulationType = CumulationType.NONE;
		this.familyScope = FamilyScope.SOME;
		this.valueFilter = ValueFilter.NONE;
		this.sizeFilter = SizeFilter.NONE;
		this.valueCode = ValueCode.VALUES;
	}

	/**
	 * 
	 */
	public PartitionCriteria(String label) {
		this.label = label;
		this.type = PartitionType.RAW;
		this.cumulationType = CumulationType.NONE;
		this.familyScope = FamilyScope.SOME;
		this.valueFilter = ValueFilter.NONE;
		this.sizeFilter = SizeFilter.NONE;
		this.valueCode = ValueCode.VALUES;
	}

	/**
	 * 
	 */
	public PartitionCriteria(String label, String labelParameter) {
		this.label = label;
		this.labelParameter = labelParameter;
		this.type = PartitionType.RAW;
		this.cumulationType = CumulationType.NONE;
		this.familyScope = FamilyScope.SOME;
		this.valueFilter = ValueFilter.NONE;
		this.sizeFilter = SizeFilter.NONE;
		this.valueCode = ValueCode.VALUES;

	}
	
	public PartitionCriteria clone(){
		PartitionCriteria result;
		
		result = new PartitionCriteria(label,labelParameter);
		result.type = this.type;
		result.cumulationType = this.cumulationType;
		result.familyScope = this.familyScope;
		result.valueFilter = this.valueFilter;
		result.sizeFilter = this.sizeFilter;
		result.valueCode = this.valueCode;
		
		//
		return result;
	}
	
	public PartitionCriteria cloneChangeLabel(String label){
		PartitionCriteria result;
		
		result = clone();
		result.setLabel(label);
		
		//
		return result;
	}
		


	public Double getCount() {
		return count;
	}

	public CumulationType getCumulationType() {
		return cumulationType;
	}

	public Double getEnd() {
		return end;
	}

	public FamilyScope getFamilyScope() {
		return familyScope;
	}

	public Intervals getIntervals() {
		return intervals;
	}

	public String getLabel() {
		return label;
	}

	public String getLabelParameter() {
		return labelParameter;
	}

	public Double getMax() {
		return max;
	}

	public Double getMin() {
		return min;
	}

	public String getPattern() {
		return pattern;
	}

	public String getRelationModelName() {
		return relationModelName;
	}

	public Double getSize() {
		return size;
	}

	public SizeFilter getSizeFilter() {
		return sizeFilter;
	}

	public Double getStart() {
		return start;
	}

	public PartitionType getType() {
		return type;
	}

	public ValueFilter getValueFilter() {
		return valueFilter;
	}

	public Values getValues() {
		return values;
	}

	public void setValues(Values values) {
		this.values = values;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isValid() {
		boolean result;

		if ((StringUtils.isBlank(this.label)) || (this.type == null)) {
			result = false;
		} else {
			result = true;
		}

		//
		return result;
	}

	public void setCount(final Double count) {
		this.count = count;
	}

	public void setCumulationType(final CumulationType cumulationType) {
		if (cumulationType == null) {
			this.cumulationType = CumulationType.NONE;
		} else {
			this.cumulationType = cumulationType;
		}
	}

	public void setEnd(final Double end) {
		this.end = end;
	}

	/**
	 * 
	 * @param familyScope
	 */
	public void setFamilyScope(final FamilyScope familyScope) {
		if (familyScope == null) {
			this.familyScope = FamilyScope.SOME;
		} else {
			this.familyScope = familyScope;
		}
	}

	public void setIntervals(final Intervals intervals) {
		this.intervals = intervals;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public void setLabelParameter(final String labelParameter) {
		this.labelParameter = labelParameter;
	}

	public void setMax(final Double max) {
		this.max = max;
	}

	public void setMin(final Double min) {
		this.min = min;
	}

	public void setPattern(final String pattern) {
		this.pattern = pattern;
	}

	public void setRelationModelName(final String relationModelName) {
		this.relationModelName = relationModelName;
	}

	public void setSize(final Double size) {
		this.size = size;
	}

	public void setSizeFilter(final SizeFilter sizeFilter) {
		this.sizeFilter = sizeFilter;
	}

	public void setStart(final Double start) {
		this.start = start;
	}

	public void setType(final PartitionType type) {
		this.type = type;
	}

	public void setValueFilter(final ValueFilter valueFilter) {
		this.valueFilter = valueFilter;
	}

	public Integer getTime() {
		return time;
	}

	public void setTime(Integer time) {
		this.time = time;
	}

	public String getEgoRoleName() {
		return egoRoleName;
	}

	public void setEgoRoleName(String egoRoleName) {
		this.egoRoleName = egoRoleName;
	}

	/**
	 * 
	 * @return
	 */
	public String toShortString() {
		String result;

		//
		StringList title = new StringList(20);

		//
		if (StringUtils.isNotBlank(this.relationModelName)) {
			if (this.relationModelName.equals(PartitionCriteria.RelationModelCanonicalNames.INDIVIDUAL.toString())) {
				title.append("Ind. ");
			} else if (this.relationModelName.equals(PartitionCriteria.RelationModelCanonicalNames.FAMILY.toString())) {
				title.append("Fam. ");
			} else {
				title.append(this.relationModelName).append(" ");
			}
		}

		//
		title.append(this.label);

		//
		if (StringUtils.isNotBlank(this.labelParameter)) {
			title.append(" ").append(this.labelParameter);
		}

		//
		if (this.type == PartitionType.BINARIZATION) {
			title.append(" ").append(this.pattern);
		} else if (this.type == PartitionType.FREE_GROUPING) {
			title.append(" ").append(this.intervals.toBasicStepString());
		} else if (this.type == PartitionType.COUNTED_GROUPING) {
			title.append(" ").append(MathUtils.toString(this.start)).append(",c").append(MathUtils.toString(this.count)).append(",")
					.append(MathUtils.toString(this.end));
		} else if (this.type == PartitionType.SIZED_GROUPING) {
			title.append(" ").append(MathUtils.toString(this.start)).append(",s").append(MathUtils.toString(this.size)).append(",")
					.append(MathUtils.toString(this.end));
		}

		//
		if (this.getCumulationType() != CumulationType.NONE) {
			title.append(" ").append(this.getCumulationType().toString());
		}

		//
		title.append(" ").append(this.familyScope.toString().toLowerCase());

		//
		result = title.toString();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		switch (this.type) {
			case RAW:
				result = String.format("type=%s,label=%s,parameter=%s,cumulation=%s,scope=%s", this.type.toString(), this.label, this.labelParameter,
						this.cumulationType.toString(), this.familyScope.toString());
			break;

			case BINARIZATION:
				result = String.format("type=%s,label=%s,parameter=%s,pattern=%s,cumulation=%s,scope=%s", this.type.toString(), this.label,
						this.labelParameter, this.pattern, this.cumulationType.toString(), this.familyScope.toString());
			break;

			case FREE_GROUPING:
				result = String.format("type=%s,label=%s,parameter=%s,steps=%s,cumulation=%s,scope=%s", this.type.toString(), this.label, this.labelParameter,
						this.intervals.toStepString(), this.cumulationType.toString(), this.familyScope.toString());
			break;

			case COUNTED_GROUPING:
				result = String.format("type=%s,label=%s,parameter=%s,start=%s,count=%s,end=%s,cumulation=%s,scope=%s", this.type.toString(), this.label,
						this.labelParameter, String.valueOf(this.start), String.valueOf(this.count), String.valueOf(this.end), this.cumulationType.toString(),
						this.familyScope.toString());
			break;

			case SIZED_GROUPING:
				result = String.format("type=%s,label=%s,parameter=%s,start=%s,size=%s,end=%s,cumulation=%s,scope=%s", this.type.toString(), this.label,
						this.labelParameter, String.valueOf(this.start), String.valueOf(this.size), String.valueOf(this.end), this.cumulationType.toString(),
						this.familyScope.toString());
			break;

			default:
				result = "";
				
		}
		
		if (withDiagram){
			result+=",withDiagram";
		}

		if (withMatrix){
			result+=",withMatrix";
		}

		if (withGraph){
			result+=",withGraph";
		}

		if (withCensus){
			result+=",withCensus";
		}

		//
		return result;
	}

	/**
	 * 
	 * @param label
	 * @return
	 */
	public static PartitionCriteria createBinarization(final String label, final String labelParameter, final String pattern) {
		PartitionCriteria result;

		result = new PartitionCriteria();
		result.setType(PartitionType.BINARIZATION);
		result.setLabel(label);
		result.setLabelParameter(labelParameter);
		result.setPattern(pattern);

		//
		return result;
	}

	/**
	 * 
	 * @param label
	 * @return
	 */
	public static PartitionCriteria createCountedGrouping(final String label, final String labelParameter, final Double start, final Double count,
			final Double end) {
		PartitionCriteria result;

		//
		result = new PartitionCriteria();
		result.setType(PartitionType.COUNTED_GROUPING);
		result.setLabel(label);
		result.setLabelParameter(labelParameter);
		result.setStart(start);
		result.setCount(count);
		result.setEnd(end);

		//
		return result;
	}

	/**
	 * 
	 * @param label
	 * @return
	 */
	public static PartitionCriteria createGrouping(final String label, final String steps) {
		PartitionCriteria result;

		result = createGrouping(label, null, PartitionMaker.getIntervals(steps));

		//
		return result;
	}

	/**
	 * 
	 * @param label
	 * @return
	 */
	public static PartitionCriteria createGrouping(final String label, final String labelParameter, final Intervals intervals) {
		PartitionCriteria result;

		result = new PartitionCriteria();
		result.setType(PartitionType.FREE_GROUPING);
		result.setLabel(label);
		result.setLabelParameter(labelParameter);
		result.setIntervals(intervals);

		//
		return result;
	}

	/**
	 * 
	 * @param label
	 * @return
	 */
	public static PartitionCriteria createGrouping(final String label, final String labelParameter, final String steps) {
		PartitionCriteria result;

		result = createGrouping(label, labelParameter, PartitionMaker.getIntervals(steps));

		//
		return result;
	}

	/**
	 * 
	 * @param label
	 * @return
	 */
	public static PartitionCriteria createRaw(final String label) {
		PartitionCriteria result;

		result = createRaw(label, null);

		//
		return result;
	}

	/**
	 * 
	 * @param label
	 * @return
	 */
	public static PartitionCriteria createRaw(final String label, final String labelParameter) {
		PartitionCriteria result;

		result = new PartitionCriteria();
		result.setType(PartitionType.RAW);
		result.setLabel(label);
		result.setLabelParameter(labelParameter);

		//
		return result;
	}
	
	// make analogous methods for other grouping types...
	public void setSizedGrouping(final Double start, final Double size, final Double end){

		setType(PartitionType.SIZED_GROUPING);
		setStart(start);
		setSize(size);
		setEnd(end);
	}

	public void setFreeGrouping(final String pattern){

		setType(PartitionType.FREE_GROUPING);
		setIntervals(PartitionMaker.getIntervals(pattern));
	}

	/**
	 * 
	 * @param label
	 * @return
	 */
	public static PartitionCriteria createSizedGrouping(final String label, final String labelParameter, final Double start, final Double size, final Double end) {
		PartitionCriteria result;

		//
		result = new PartitionCriteria();
		result.setType(PartitionType.SIZED_GROUPING);
		result.setLabel(label);
		result.setLabelParameter(labelParameter);
		result.setStart(start);
		result.setSize(size);
		result.setEnd(end);
		//
		return result;
	}

	public boolean isWithCensus() {
		return withCensus;
	}

	public void setWithCensus(boolean withCensus) {
		this.withCensus = withCensus;
	}

	public boolean isWithMatrix() {
		return withMatrix;
	}

	public void setWithMatrix(boolean withMatrix) {
		this.withMatrix = withMatrix;
	}

	public boolean isWithGraph() {
		return withGraph;
	}

	public void setWithGraph(boolean withGraph) {
		this.withGraph = withGraph;
	}

	public boolean isWithDiagram() {
		return withDiagram;
	}

	public void setWithDiagram(boolean withDiagram) {
		this.withDiagram = withDiagram;
	}

	public boolean isWithTrend() {
		return withTrend;
	}

	public void setWithTrend(boolean withTrend) {
		this.withTrend = withTrend;
	}

	public ValueCode getValueCode() {
		return valueCode;
	}

	public void setValueCode(ValueCode valueCode) {
		this.valueCode = valueCode;
	}

	public boolean isWithFlow() {
		return withFlow;
	}

	public void setWithFlow(boolean withFlow) {
		this.withFlow = withFlow;
	}
	
	public boolean equals(Object other){
		return (other == null ? false : toString().equals(((PartitionCriteria)other).toString()));
	}
	
	
	
	

}
