package org.tip.puck.util;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class Trafo {
	
	/**
	 * removes double blanks inside a string and trims it
	 * @param str the string
	 * @return the trimmed string without double blanks
	 */
	public static String trim (String str) {
		String result;
		
		result = str;
		
    	if (str!=null && !str.equals("")) {
    		result = str.trim().replaceAll("  "," ");
    		while (result.indexOf("  ")>-1){
    			result = trim(result);
    		}
    	}
    	//
        return result;
     }
	
	public static boolean isBlank (String string){
		return StringUtils.isBlank(string) || string.equals("null");
	}

	/**
	 * checks whether an object is null or (in case of a string) represents a null element
	 * @param obj the object to be checked
	 * @return true if the object is or represents a null element
	 */
	public static boolean isNull (Object obj){
		boolean result;
		
		if (obj==null){
			result = true;
		} else {
			try {
				String s = ((String)obj).trim();
				result = s.equals("") || s.equals("null");
			} catch (ClassCastException cce) {
				result = false;
			}
		}
		//
		return result;
	}
	
	public static String asNonNull (String source){
		String result;
		
		result = "";
		
		if (source!=null){
			result = source;
		}
		//
		return result;
	}
	
	public static String asShortCutString (List<String> list, int pos){
		String result;
		
		result = null;
		for (String string : list){
			if (result==null){
				result = string.substring(0,pos);
			} else {
				result += "-"+string.substring(0,pos);
			}
		}
		//
		return result;
	}
	
	public static String noParentheses(String content){
		String result;
		
		result = content.replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("\\[", "").replaceAll("\\]", "");
		
		//
		return result;
	}

}
