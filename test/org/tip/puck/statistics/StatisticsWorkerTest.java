package org.tip.puck.statistics;

import java.io.File;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.io.tip.TIPFile;
import org.tip.puck.io.txt.TXTFile;
import org.tip.puck.net.Net;
import org.tip.puck.util.Chronometer;
import org.tip.puck.util.LogHelper;

/**
 * 
 * @author TIP
 */
public class StatisticsWorkerTest {
	private static final Logger logger = LoggerFactory.getLogger(StatisticsWorkerTest.class);

	public static final String SIMPLE_FILE = "test/Resources/Chimane6.txt";
	public static final String COMPLEX_FILE = "test/Resources/Ebrei_08.txt";

	/**
     */
	@Test
	public void testAscendantCount() throws Exception {

		logger.debug("========================== testAscendantCount");

		//
		Chronometer chrono = new Chronometer();
		Net net = TXTFile.load(new File(SIMPLE_FILE));
		logger.debug("Net=" + LogHelper.toString(net));

		//
		NumberedFiliationCountLists target = StatisticsWorker.ascendantsCounts(net.individuals(), 10);

		logger.debug("size=" + target.size());

		//
		logger.debug("TimeSpent=" + chrono.stop().interval());

		//
	}

	/**
     */
	@Test
	public void testCompleteness() throws Exception {

		logger.debug("========================== testCompleteness");

		//
		Chronometer chrono = new Chronometer();
		Net net = TXTFile.load(new File(SIMPLE_FILE));
		logger.debug("Net=" + LogHelper.toString(net));

		//
		FiliationCounts target = StatisticsWorker.completeness(net.individuals(), 10);

		logger.debug("size=" + target.size());
		for (int ascendingIndex = 0; ascendingIndex < target.size(); ascendingIndex++) {
			logger.debug(ascendingIndex + "\t" + target.get(ascendingIndex).getCognatic() + "\t" + target.get(ascendingIndex).getAgnatic() + "\t"
					+ target.get(ascendingIndex).getUterine());
		}

		//
		logger.debug("TimeSpent=" + chrono.stop().interval());

		//
	}

	/**
     */
	@Test
	public void testMarrageCount() throws Exception {

		logger.debug("========================== testCompleteness");

		//
		Chronometer chrono = new Chronometer();
		Net net = TIPFile.load(new File("test/Resources/Ebrei_08.tip"));
		logger.debug("Net=" + LogHelper.toString(net));

		//
		logger.debug("marriedFamilies=" + net.families().getMarried().size());
		logger.debug("numberOfMarriages=" + StatisticsWorker.numberOfMarriages(net.families()));

		Assertions.assertThat(net.families().getMarried().size()).isEqualTo(StatisticsWorker.numberOfMarriages(net.families()));

		// for (Family family : net.families().getMarried()) {
		// if (family.getHusband().getId() < family.getWife().getId()) {
		// System.out.println(family.getHusband().getId() + " " +
		// family.getWife().getId());
		// } else {
		// System.out.println(family.getWife().getId() + " " +
		// family.getHusband().getId());
		// }
		// }

		//
		logger.debug("TimeSpent=" + chrono.stop().interval());

		//
	}
}
