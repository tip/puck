package org.tip.puck.graphs;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.partitions.Cluster;

/**
 * This classe represents a pair of cluster.
 * 
 * @author TIP
 */
public class ClusterPair<E> implements Comparable<ClusterPair<E>> {
	private Cluster<E> left;
	private Cluster<E> right;

	/**
	 * 
	 * @param target
	 * @return
	 */
	public ClusterPair(final Cluster<E> left, final Cluster<E> right) {
		this.left = left;
		this.right = right;
	}

	/**
	 * 
	 */
	@Override
	public int compareTo(final ClusterPair<E> other) {
		int result;

		//
		String thisString = this.toString();

		//
		String otherString;
		if (other == null) {
			otherString = null;
		} else {
			otherString = other.toString();
		}

		//
		result = thisString.compareTo(otherString);

		//
		return result;
	}

	/**
	 * 
	 * @param other
	 * @return
	 */
	@Override
	public boolean equals(final Object other) {
		boolean result;

		//
		if (other == null) {
			result = false;
		} else {
			result = StringUtils.equals(this.toString(), other.toString());
		}

		//
		return result;
	}

	public Cluster<E> getLeft() {
		return left;
	}

	public Cluster<E> getRight() {
		return right;
	}

	/**
	 * 
	 */
	@Override
	public int hashCode() {
		int result;

		String string = this.toString();
		if (string == null) {
			result = 0;
		} else {
			result = string.hashCode();
		}

		//
		return result;
	}

	public void setLeft(final Cluster<E> left) {
		this.left = left;
	}

	public void setRight(final Cluster<E> right) {
		this.right = right;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		if (this.left == null) {
			result = this.right.toString();
		} else {
			result = this.left.toString();
		}

		//
		return result;
	}
}
