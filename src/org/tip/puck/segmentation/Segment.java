package org.tip.puck.segmentation;

import java.util.List;

import org.tip.puck.PuckException;
import org.tip.puck.geo.Geography;
import org.tip.puck.net.Families;
import org.tip.puck.net.Family;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionCriteria;

/**
 * A segment manages data about a current cluster in a cluster list ordered by
 * value: current cluster index, current cluster and current individuals.
 * 
 * There is a special cluster to group individuals out of partition. It is
 * associated with current cluster null value.
 * 
 * @author TIP
 */
public interface Segment {

	/**
	 * 
	 * @return
	 */
	public int getClusterCount();

	/**
	 * 
	 * @return
	 */
	public List<String> getClusterLabels();

	/**
	 * 
	 */
	public PartitionCriteria getCriteria();

	/**
	 * 
	 * @return
	 */
	public int getCurrentClusterIndex();

	/**
	 * 
	 * @return
	 */
	public Families getCurrentFamilies();

	/**
	 * 
	 * @return
	 */
	public Individuals getCurrentIndividuals();

	/**
	 * 
	 * @return
	 */
	public Relations getCurrentRelations();

	/**
	 * 
	 * @return
	 */
	public int getCurrentSize();

	/**
	 * 
	 * @return
	 */
	public String getLabel();

	/**
	 * 
	 * @return
	 */
	public Families getOutOfCurrentClusterFamilies();

	/**
	 * 
	 * @return
	 */
	public Individuals getOutOfCurrentClusterIndividuals();

	/**
	 * 
	 * @return
	 */
	public Relations getOutOfCurrentClusterRelations();

	/**
	 * 
	 * @return
	 */
	public Partition<?> getPartition();

	/**
	 * 
	 * @return
	 */
	public String getShortLabel();

	/**
	 * 
	 * @return
	 */
	public String getShortShortLabel();

	/**
	 * 
	 * @param targetDomain
	 * @param criteria
	 * @throws PuckException
	 */
	public void refresh(final Segment source) throws PuckException;

	/**
	 * 
	 * @param family
	 */
	public void selectCluster(final Family family) throws PuckException;

	/**
	 * 
	 * @param index
	 * @throws PuckException
	 */
	public void selectCluster(final Individual individual) throws PuckException;

	/**
	 * 
	 * @param index
	 * @throws PuckException
	 */
	public void selectCluster(final int index) throws PuckException;

	/**
	 * 
	 * @param relation
	 */
	public void selectCluster(final Relation relation) throws PuckException;

	/**
	 * 
	 * @param family
	 */
	public void selectOutOfPartitionCluster() throws PuckException;
	
	public Geography getGeography();
	
	public void setGeography(Geography geography);
	
}
