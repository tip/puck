package org.tip.puckgui.views;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

/**
 * The class <code>ChartWindow</code> manage a chart panel in an independent
 * window.
 * 
 * @author TIP
 */
public class ChartWindow extends JFrame {
	private static final long serialVersionUID = 6398775471144607381L;

	/**
	 * Create the frame from a
	 */
	public ChartWindow(final JPanel chartPanel) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ChartWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		setLocationRelativeTo(null);
		setContentPane(chartPanel);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenuItem mntmQuit = new JMenuItem("Close");
		mntmQuit.setMaximumSize(new Dimension(150, 32767));
		mntmQuit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK));
		mntmQuit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				dispose();
			}
		});
		menuBar.add(mntmQuit);
	}
}
