package org.tip.puckgui.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.gui.views.DatabaseDirectorySelector;
import org.tip.flatdb4geonames.model.FlatDB4GeoNames;
import org.tip.puckgui.InputSettings;
import org.tip.puckgui.InputSettings.CheckLevel;
import org.tip.puckgui.Language;
import org.tip.puckgui.Preferences;
import org.tip.puckgui.PuckGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class PreferencesWindow extends JFrame {

	private static final Logger logger = LoggerFactory.getLogger(PreferencesWindow.class);

	private static final long serialVersionUID = -4427644262064342086L;
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.tip.puckgui.messages"); //$NON-NLS-1$

	private JPanel contentPane;
	private JComboBox languagesComboBox;
	private JCheckBox chkbxAutoLoadLastFile;
	private final ButtonGroup bttnGrpSameSexSpouse = new ButtonGroup();
	private final ButtonGroup bttnGrpFemaleFathersOrMaleMothers = new ButtonGroup();
	private final ButtonGroup bttnGrpParentChildMarriages = new ButtonGroup();
	private JRadioButton rdBttnSameSexSpouseNone;
	private JRadioButton rdBttnSameSexSpouseWarning;
	private JRadioButton rdBttnSameSexSpouseError;
	private JRadioButton rdBttnFemaleFathersOrMaleMothersNone;
	private JRadioButton rdBttnFemaleFathersOrMaleMothersWarning;
	private JRadioButton rdBttnFemaleFathersOrMaleMothersError;
	private JRadioButton rdBttnParentChildMarriagesNone;
	private JRadioButton rdBttnParentChildMarriagesWarning;
	private JRadioButton rdBttnParentChildMarriagesError;
	private JTextField txtfldFlatDB4GeoNamesDirectory;
	private JTextField txtfldSelfName;
	private JSpinner spnrMaxIterations;

	/**
	 * Create the frame.
	 */
	public PreferencesWindow() {
		setTitle(BUNDLE.getString("PreferencesWindow.this.title")); //$NON-NLS-1$
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 342, 412);
		setLocationRelativeTo(null);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BoxLayout(this.contentPane, BoxLayout.Y_AXIS));

		JPanel panel = new JPanel();
		this.contentPane.add(panel);
		panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.GLUE_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblLanguages = new JLabel(BUNDLE.getString("PreferencesWindow.language.text")); //$NON-NLS-1$
		panel.add(lblLanguages, "2, 2");

		this.languagesComboBox = new JComboBox();
		panel.add(this.languagesComboBox, "4, 2");
		List<String> languageLabels = new ArrayList<String>();
		for (Language language : Language.values()) {
			languageLabels.add(BUNDLE.getString("PreferencesWindow.language." + language.toString().toLowerCase()));
		}

		this.languagesComboBox.setModel(new DefaultComboBoxModel(languageLabels.toArray()));
		this.languagesComboBox.setSelectedIndex(ArrayUtils.indexOf(Language.values(), PuckGUI.instance().getPreferences().getLanguage()));

		JLabel lblAutoLoadLast = new JLabel(BUNDLE.getString("PreferencesWindow.lblAutoLoadLast.text")); //$NON-NLS-1$
		panel.add(lblAutoLoadLast, "2, 4");

		this.chkbxAutoLoadLastFile = new JCheckBox();
		panel.add(this.chkbxAutoLoadLastFile, "4, 4");

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Input settings", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.contentPane.add(panel_2);
		panel_2.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblNone = new JLabel(BUNDLE.getString("PreferencesWindow.lblNone.text")); //$NON-NLS-1$
		panel_2.add(lblNone, "4, 2, center, default");

		JLabel lblWarning = new JLabel(BUNDLE.getString("PreferencesWindow.lblWarning.text")); //$NON-NLS-1$
		panel_2.add(lblWarning, "6, 2, center, default");

		JLabel lblError = new JLabel(BUNDLE.getString("PreferencesWindow.lblError.text")); //$NON-NLS-1$
		panel_2.add(lblError, "8, 2, center, default");

		JLabel lblSamesexSpouses = new JLabel(BUNDLE.getString("PreferencesWindow.lblSamesexSpouses.text")); //$NON-NLS-1$
		panel_2.add(lblSamesexSpouses, "2, 4");

		this.rdBttnSameSexSpouseNone = new JRadioButton();
		this.rdBttnSameSexSpouseNone.setSelected(true);
		this.bttnGrpSameSexSpouse.add(this.rdBttnSameSexSpouseNone);
		panel_2.add(this.rdBttnSameSexSpouseNone, "4, 4, center, default");

		this.rdBttnSameSexSpouseWarning = new JRadioButton();
		this.bttnGrpSameSexSpouse.add(this.rdBttnSameSexSpouseWarning);
		panel_2.add(this.rdBttnSameSexSpouseWarning, "6, 4, center, default");

		this.rdBttnSameSexSpouseError = new JRadioButton();
		this.bttnGrpSameSexSpouse.add(this.rdBttnSameSexSpouseError);
		panel_2.add(this.rdBttnSameSexSpouseError, "8, 4, center, default");

		JLabel lblFemalFathersOr = new JLabel(BUNDLE.getString("PreferencesWindow.lblFemalFathersOr.text")); //$NON-NLS-1$
		panel_2.add(lblFemalFathersOr, "2, 6");

		this.rdBttnFemaleFathersOrMaleMothersNone = new JRadioButton();
		this.rdBttnFemaleFathersOrMaleMothersNone.setSelected(true);
		this.bttnGrpFemaleFathersOrMaleMothers.add(this.rdBttnFemaleFathersOrMaleMothersNone);
		panel_2.add(this.rdBttnFemaleFathersOrMaleMothersNone, "4, 6, center, default");

		this.rdBttnFemaleFathersOrMaleMothersWarning = new JRadioButton();
		this.bttnGrpFemaleFathersOrMaleMothers.add(this.rdBttnFemaleFathersOrMaleMothersWarning);
		panel_2.add(this.rdBttnFemaleFathersOrMaleMothersWarning, "6, 6, center, default");

		this.rdBttnFemaleFathersOrMaleMothersError = new JRadioButton();
		this.bttnGrpFemaleFathersOrMaleMothers.add(this.rdBttnFemaleFathersOrMaleMothersError);
		panel_2.add(this.rdBttnFemaleFathersOrMaleMothersError, "8, 6, center, default");

		JLabel lblParentChildMarriages = new JLabel(BUNDLE.getString("PreferencesWindow.lblParentChildMarriages.text")); //$NON-NLS-1$
		panel_2.add(lblParentChildMarriages, "2, 8");

		this.rdBttnParentChildMarriagesNone = new JRadioButton();
		this.rdBttnParentChildMarriagesNone.setSelected(true);
		this.bttnGrpParentChildMarriages.add(this.rdBttnParentChildMarriagesNone);
		panel_2.add(this.rdBttnParentChildMarriagesNone, "4, 8, center, default");

		this.rdBttnParentChildMarriagesWarning = new JRadioButton();
		this.bttnGrpParentChildMarriages.add(this.rdBttnParentChildMarriagesWarning);
		panel_2.add(this.rdBttnParentChildMarriagesWarning, "6, 8, center, default");

		this.rdBttnParentChildMarriagesError = new JRadioButton();
		this.bttnGrpParentChildMarriages.add(this.rdBttnParentChildMarriagesError);
		panel_2.add(this.rdBttnParentChildMarriagesError, "8, 8, center, default");

		JLabel lblMarriageWithOneself = new JLabel(BUNDLE.getString("PreferencesWindow.lblMarriageWithOneself.text")); //$NON-NLS-1$
		panel_2.add(lblMarriageWithOneself, "2, 10");

		JRadioButton radioButton = new JRadioButton();
		radioButton.setEnabled(false);
		panel_2.add(radioButton, "4, 10, center, default");

		JRadioButton radioButton_1 = new JRadioButton();
		radioButton_1.setEnabled(false);
		panel_2.add(radioButton_1, "6, 10, center, default");

		JRadioButton radioButton_2 = new JRadioButton();
		radioButton_2.setSelected(true);
		panel_2.add(radioButton_2, "8, 10, center, default");

		JLabel lblChildKinWith = new JLabel(BUNDLE.getString("PreferencesWindow.lblChildKinWith.text")); //$NON-NLS-1$
		panel_2.add(lblChildKinWith, "2, 12");

		JRadioButton radioButton_3 = new JRadioButton();
		radioButton_3.setHorizontalAlignment(SwingConstants.CENTER);
		radioButton_3.setEnabled(false);
		panel_2.add(radioButton_3, "4, 12");

		JRadioButton radioButton_4 = new JRadioButton();
		radioButton_4.setEnabled(false);
		panel_2.add(radioButton_4, "6, 12, center, default");

		JRadioButton radioButton_5 = new JRadioButton();
		radioButton_5.setSelected(true);
		radioButton_5.setHorizontalAlignment(SwingConstants.CENTER);
		panel_2.add(radioButton_5, "8, 12");

		JPanel panelFlatDB4GeoNames = new JPanel();
		panelFlatDB4GeoNames.setBorder(new TitledBorder(null, "FlatDB4GeoNames", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.contentPane.add(panelFlatDB4GeoNames);
		panelFlatDB4GeoNames.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblDatbaseDirectory = new JLabel(BUNDLE.getString("PreferencesWindow.lblDatbaseDirectory.text")); //$NON-NLS-1$
		panelFlatDB4GeoNames.add(lblDatbaseDirectory, "2, 2, right, default");

		this.txtfldFlatDB4GeoNamesDirectory = new JTextField();
		this.txtfldFlatDB4GeoNamesDirectory.setText(""); //$NON-NLS-1$
		panelFlatDB4GeoNames.add(this.txtfldFlatDB4GeoNamesDirectory, "4, 2, fill, default");
		this.txtfldFlatDB4GeoNamesDirectory.setColumns(10);

		JButton button = new JButton(BUNDLE.getString("PreferencesWindow.button.text")); //$NON-NLS-1$
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Select FlatDB4GeoNames database directory.
				try {
					File databaseDirectory = DatabaseDirectorySelector.showSelectorDialog(null, null);

					if (FlatDB4GeoNames.isValidDatabase(databaseDirectory)) {
						//
						PreferencesWindow.this.txtfldFlatDB4GeoNamesDirectory.setText(databaseDirectory.getAbsolutePath());

					} else {
						//
						String title = "Error computerum est";
						String message = "Invalid database.";

						//
						JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
					}

				} catch (Exception exception) {
					//
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panelFlatDB4GeoNames.add(button, "6, 2");

		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(null, "Terminologies", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.contentPane.add(panel_3);
		panel_3.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, }));

		JLabel lblSelfName = new JLabel(BUNDLE.getString("PreferencesWindow.lblSelfName.text")); //$NON-NLS-1$
		panel_3.add(lblSelfName, "2, 2, left, default");

		this.txtfldSelfName = new JTextField();
		this.txtfldSelfName.setText("");
		this.txtfldSelfName.setColumns(10);
		panel_3.add(this.txtfldSelfName, "4, 2, fill, default");

		JLabel lblMaxIterations = new JLabel(BUNDLE.getString("PreferencesWindow.lblMaxItration.text")); //$NON-NLS-1$
		panel_3.add(lblMaxIterations, "2, 4, left, default");

		this.spnrMaxIterations = new JSpinner();
		this.spnrMaxIterations.setModel(new SpinnerNumberModel(new Integer(2), new Integer(0), null, new Integer(1)));
		panel_3.add(this.spnrMaxIterations, "4, 4");

		JPanel panel_1 = new JPanel();
		this.contentPane.add(panel_1);

		JButton btnSave = new JButton(BUNDLE.getString("PreferencesWindow.btnSave.text")); //$NON-NLS-1$
		btnSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Save preferences.
				try {
					//
					Language selectedLanguage = Language.values()[PreferencesWindow.this.languagesComboBox.getSelectedIndex()];
					boolean updateLanguage;
					if (selectedLanguage != PuckGUI.instance().getPreferences().getLanguage()) {
						updateLanguage = true;
					} else {
						updateLanguage = false;
					}
					PuckGUI.instance().getPreferences().setLanguage(selectedLanguage);

					//
					PuckGUI.instance().getPreferences().setAutoLoadLastFile(PreferencesWindow.this.chkbxAutoLoadLastFile.isSelected());

					//
					PuckGUI.instance().getPreferences().setInputSettings(getInputSettings());

					//
					PuckGUI.instance().getPreferences().setFlatDB4GeoNamesDirectory(PreferencesWindow.this.txtfldFlatDB4GeoNamesDirectory.getText());

					PuckGUI.instance().getPreferences().setTerminologySelfName(PreferencesWindow.this.txtfldSelfName.getText());
					PuckGUI.instance().getPreferences().setTerminologyMaxIterations((Integer) PreferencesWindow.this.spnrMaxIterations.getValue());

					//
					try {
						//
						PuckGUI.instance().savePreferences();

					} catch (final Exception exception) {
						//
						exception.printStackTrace();
						logger.error("Error saving preferences file: " + exception.getMessage());
						logger.warn("Ignoring preferences file save.");
					}

					//
					if (updateLanguage) {
						PuckGUI.instance().updateLanguage();
					}

					//
					String databaseDirectory = PreferencesWindow.this.txtfldFlatDB4GeoNamesDirectory.getText();
					logger.debug("databaseDirectory=" + databaseDirectory);

					if (StringUtils.isBlank(databaseDirectory)) {
						//
						if (FlatDB4GeoNames.isOpened()) {
							//
							logger.debug("Close opened database.");
							FlatDB4GeoNames.close();
						}
					} else {
						//
						if (FlatDB4GeoNames.isOpened()) {
							//
							if (!StringUtils.equals(databaseDirectory, FlatDB4GeoNames.instance().getRepository().getAbsolutePath())) {
								//
								logger.debug("Close and open new database.");
								FlatDB4GeoNames.close();
								FlatDB4GeoNames.open(databaseDirectory);
							}
						} else {
							//
							logger.debug("Open new database.");
							FlatDB4GeoNames.open(databaseDirectory);
						}
					}

					//
					dispose();

				} catch (final Exception exception) {
					//
					logger.error("Unavailable to save.");
				}
			}
		});

		JButton btnClose = new JButton(BUNDLE.getString("PreferencesWindow.btnCancel.text")); //$NON-NLS-1$
		btnClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});
		panel_1.add(btnClose);
		panel_1.add(btnSave);

		// ============================================
		setCriteria(PuckGUI.instance().getPreferences());
		pack();
	}

	/**
	 * 
	 * @return
	 */
	public InputSettings getInputSettings() {
		InputSettings result;

		result = new InputSettings();

		//
		if (this.rdBttnSameSexSpouseNone.isSelected()) {
			result.setSameSexSpouses(CheckLevel.NONE);
		} else if (this.rdBttnSameSexSpouseWarning.isSelected()) {
			result.setSameSexSpouses(CheckLevel.WARNING);
		} else if (this.rdBttnSameSexSpouseError.isSelected()) {
			result.setSameSexSpouses(CheckLevel.ERROR);
		}

		//
		if (this.rdBttnFemaleFathersOrMaleMothersNone.isSelected()) {
			result.setFemaleFathersOrMaleMothers(CheckLevel.NONE);
		} else if (this.rdBttnFemaleFathersOrMaleMothersWarning.isSelected()) {
			result.setFemaleFathersOrMaleMothers(CheckLevel.WARNING);
		} else if (this.rdBttnFemaleFathersOrMaleMothersError.isSelected()) {
			result.setFemaleFathersOrMaleMothers(CheckLevel.ERROR);
		}

		//
		if (this.rdBttnParentChildMarriagesNone.isSelected()) {
			result.setParentChildMarriages(CheckLevel.NONE);
		} else if (this.rdBttnParentChildMarriagesWarning.isSelected()) {
			result.setParentChildMarriages(CheckLevel.WARNING);
		} else if (this.rdBttnParentChildMarriagesError.isSelected()) {
			result.setParentChildMarriages(CheckLevel.ERROR);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public void setCriteria(final Preferences source) {
		//
		if (source != null) {
			//
			this.languagesComboBox.setSelectedIndex(ArrayUtils.indexOf(Language.values(), PuckGUI.instance().getPreferences().getLanguage()));

			//
			this.chkbxAutoLoadLastFile.setSelected(source.isAutoLoadLastFile());

			//
			setInputSettings(source.getInputSettings());

			//
			if (!StringUtils.isBlank(source.getFlatDB4GeonamesDirectory())) {
				//
				this.txtfldFlatDB4GeoNamesDirectory.setText(source.getFlatDB4GeonamesDirectory());
			}

			//
			this.txtfldSelfName.setText(source.getTerminologySelfName());
			this.spnrMaxIterations.setValue(source.getTerminologyMaxIterations());
		}
	}

	/**
	 * 
	 * @return
	 */
	public void setInputSettings(final InputSettings source) {

		if (source != null) {
			//
			switch (source.getSameSexSpouses()) {
				case NONE:
					this.rdBttnSameSexSpouseNone.setSelected(true);
					this.rdBttnSameSexSpouseWarning.setSelected(false);
					this.rdBttnSameSexSpouseError.setSelected(false);
				break;

				case WARNING:
					this.rdBttnSameSexSpouseNone.setSelected(false);
					this.rdBttnSameSexSpouseWarning.setSelected(true);
					this.rdBttnSameSexSpouseError.setSelected(false);
				break;

				case ERROR:
					this.rdBttnSameSexSpouseNone.setSelected(false);
					this.rdBttnSameSexSpouseWarning.setSelected(false);
					this.rdBttnSameSexSpouseError.setSelected(true);
				break;
			}

			//
			switch (source.getFemaleFathersOrMaleMothers()) {
				case NONE:
					this.rdBttnFemaleFathersOrMaleMothersNone.setSelected(true);
					this.rdBttnFemaleFathersOrMaleMothersWarning.setSelected(false);
					this.rdBttnFemaleFathersOrMaleMothersError.setSelected(false);
				break;

				case WARNING:
					this.rdBttnFemaleFathersOrMaleMothersNone.setSelected(false);
					this.rdBttnFemaleFathersOrMaleMothersWarning.setSelected(true);
					this.rdBttnFemaleFathersOrMaleMothersError.setSelected(false);
				break;

				case ERROR:
					this.rdBttnFemaleFathersOrMaleMothersNone.setSelected(false);
					this.rdBttnFemaleFathersOrMaleMothersWarning.setSelected(false);
					this.rdBttnFemaleFathersOrMaleMothersError.setSelected(true);
				break;
			}

			//
			switch (source.getParentChildMarriages()) {
				case NONE:
					this.rdBttnParentChildMarriagesNone.setSelected(true);
					this.rdBttnParentChildMarriagesWarning.setSelected(false);
					this.rdBttnParentChildMarriagesError.setSelected(false);
				break;

				case WARNING:
					this.rdBttnParentChildMarriagesNone.setSelected(false);
					this.rdBttnParentChildMarriagesWarning.setSelected(true);
					this.rdBttnParentChildMarriagesError.setSelected(false);
				break;

				case ERROR:
					this.rdBttnParentChildMarriagesNone.setSelected(false);
					this.rdBttnParentChildMarriagesWarning.setSelected(false);
					this.rdBttnParentChildMarriagesError.setSelected(true);
				break;
			}
		}
	}
}
