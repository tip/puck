package oldcore.trash;


public class ListWorker {
	

/**
 * gets a List of places
 * @return a List of places
 * @see gui.components.Table#getList(int,int,int)
 */
/*public TipList getPlaces (){
	TipList list = new TipList();
	for (Individual v : this.values()) {
		if (v.getAttributes()==null) continue;
		for (Attribute a : v.getAttributes()) {
			list.append((String)a.get(1));
		}
    }
	Collections.sort(list);
	return list;
}*/


/**
 * gets a List of attribute values
 * @return a List of attribute values
 * @see gui.components.Table#getList(int,int,int)
 */
/*public TipList getValues (String label){
	TipList list = new TipList();
	for (Individual v : this.values()) {
		if (v.getAttributes()==null) continue;
		for (Attribute a : v.getAttributes()) {
			if (a.getLabel()==null) continue;
			if (!a.getLabel().equals(label)) continue;
			list.append((String)a.get(0));
		}
    }
	Collections.sort(list);
	return list;
}*/

//provisory method for private use - generalize, parametrize, make accessible and move
/**
 * writes a list of spouses and children for each member of each cluster of a given partition
 * @param screen the underlying screen
 * @param lb the partition label
 * @throws IOException
 * @since last modified 10-08-05
 */
/*public void writeRelatives (String directory, String lb, Screen screen) throws IOException{
	TxtWriter writer = new TxtWriter(directory,screen);
	NetSystem nets = new NetSystem(getPartition(lb),true);
	for (PartNet nt : nets){
		Object clu = nt.getCluster();
		if (Trafo.isNull(clu)) continue;
		writer.wr("");
		writer.wr("Maison "+clu);
		writer.wr("");
		for (Individual v : nt.values()){
			if (v.getCluster("ALL_CH_10")!=null && v.getCluster("ALL_SP_10")!=null) continue;
			int age = 2010-util.Trafo.asInt(v.getCluster("BIRT Date"));
			writer.wr(v.signature()+" ("+age+")");
			if (v.isSingle() && v.isSterile()) continue;
			String spc = ":";
			String chc = ":";
			if (v.getCluster("ALL_SP_05")!=null) spc = "(ok 05):";
			if (v.getCluster("ALL_CH_05")!=null) chc = "(ok 05):";
			writer.wr("\tSpouses"+spc+v.printRelatives(0));
			writer.wr("\tChildren"+chc+v.printRelatives(-1));
		}
	}
	writer.close();
}*/

//Render accessible (via List button)
//Check and modifiy
/**
* gets a list of vertices with their status (dead, alive, married, divorced etc.) at a given point of time (seedReferenceYear)
* @param i the point of time
*/
/*private void statusList (int i){
	for (Individual v : values()){
		String s = v.getStatus(i);
		if (s==null || s.equals(Individual.status(1))) continue;
		print(v.signature()+" "+v.getStatus(i));
	}
}*/


}
