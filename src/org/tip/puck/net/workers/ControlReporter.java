package org.tip.puck.net.workers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.geo.Geography;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.relations.workers.RelationValuator;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Partition;
import org.tip.puck.report.Report;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.workers.UnknownPlacesCriteria;
import org.tip.puck.util.Chronometer;
import org.tip.puck.util.Value;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class ControlReporter {

	public enum ControlType {
		AUTO_MARRIAGE,
		CYCLIC_DESCENT_CASES,
		FEMALE_FATHERS_OR_MALE_MOTHERS,
		MULTIPLE_FATHERS_OR_MOTHERS,
		PARENT_CHILD_MARRIAGES,
		SAME_SEX_SPOUSES,
		NAMELESS_PERSONS,
		UNKNOWN_SEX_PERSONS,
		UNKNOWN_SEX_PARENTS_SPOUSES,
		UNKNOWN_RELATIVES,
		INCONSISTENT_DATES,
		MISSING_DATES,
		MISSING_DATES_COMPACT
	}

	private static final Logger logger = LoggerFactory.getLogger(ControlReporter.class);

	private static String add(final String string1, final String string2, String sep) {
		String result;

		if (StringUtils.isBlank(string1)) {
			sep = "";
		}
		result = string1 + sep + string2;
		//
		return result;
	}

	/**
	 * checks for filiation cycles in the network
	 * 
	 * @param control
	 *            the protocol to note found cycles
	 * @param k
	 *            the maximal cycle length to be checked
	 * @param firstEgo
	 *            the last vertex of the chain
	 * @param str
	 *            a string containing the preceding kinship chain in positional
	 *            notation
	 */
	public static void checkCycles(final StringList control, final Individual ego, final Individual firstEgo, final int k, String str) {

		if (k == 0 || ego.getId() < firstEgo.getId()) {
			return;
		}
		String[] parentType = { "F", "M", "X" };

		str = str + " " + ego.getName() + " (" + ego.getId() + ")";

		if (ego.equals(firstEgo)) {
			control.appendln(str);
		}
		for (Individual alter : ego.getParents()) {
			checkCycles(control, alter, firstEgo, k - 1, str + " - " + parentType[alter.getGender().toInt()]);
		}
	}

	/**
	 * checks for filiation cycles in the network
	 * 
	 * @param control
	 *            the protocol to note found cycles
	 * @param k
	 *            the maximal cycle length to be checked
	 */
	public static void checkCycles(final StringList control, final Individual ego, final int k) {

		String[] parentType = { "F", "M", "X" };

		for (Individual alter : ego.getParents()) {
			checkCycles(control, alter, ego, k, ego.getName() + " (" + ego.getId() + ") - " + parentType[alter.getGender().toInt()]);
		}
	}

	/**
	 * 
	 * @param source
	 * @param locale
	 * @param controls
	 * @return
	 */
	public static Report controlNet(final Net source, final Locale locale, final List<ControlType> controls) {
		Report result;

		result = reportControls(source, ((ResourceBundle) (null)), controls.toArray(new ControlType[0]));

		//
		return result;
	}

	/**
	 * Checks for possible undefinedRoles (special features) of a given type in
	 * the dataset (as specified in the options) and counts them.
	 * 
	 * @param k
	 *            the index of the type of possible error
	 * 
	 * @return the number of special features in the dataset
	 * 
	 * @see maps.Net#control()
	 */
	public static Report reportAutoMarriages(final Segmentation segmentation, final ResourceBundle bundle) {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		//
		int errorCount = 0;
		StringList errors = new StringList();

		for (Family family : segmentation.getCurrentFamilies()) {
			if (family.getHusband() != null && family.getHusband().equals(family.getWife())) {
				errors.appendln(family.getHusband() + " (family " + family.getId() + ")");
				errorCount += 1;
			}
		}

		//
		result = new Report();
		result.setTitle("Checks for Auto-Marriages.");
		result.setOrigin("Control reporter");
		result.setTarget(segmentation.getLabel());

		//
		errors.add(0, errorCount + " " + Report.translate(bundle, "Auto-Marriages") + "\n");
		result.outputs().append(errors.toString());

		//
		result.setStatus(errorCount);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * Checks for possible undefinedRoles (special features) of a given type in
	 * the dataset (as specified in the options) and counts them.
	 * 
	 * @param k
	 *            the index of the type of possible error
	 * 
	 * @return the number of special features in the dataset
	 * 
	 * @see maps.Net#control()
	 */
	public static Report reportControl(final Net net, final ResourceBundle bundle, final ControlType controlType) {
		Report result;

		//
		result = reportControl(new Segmentation(net), bundle, controlType);

		//
		return result;
	}

	/**
	 * Checks for possible undefinedRoles (special features) of a given type in
	 * the dataset (as specified in the options) and counts them.
	 * 
	 * @param k
	 *            the index of the type of possible error
	 * 
	 * @return the number of special features in the dataset
	 * 
	 * @see maps.Net#control()
	 */
	public static Report reportControl(final Segmentation segmentation, final ResourceBundle bundle, final ControlType controlType) {
		Report result;

		switch (controlType) {
			case CYCLIC_DESCENT_CASES:
				result = reportCyclicDescentCases(segmentation, bundle);
			break;
			case FEMALE_FATHERS_OR_MALE_MOTHERS:
				result = reportFemaleFathersOrMaleMothers(segmentation, bundle);
			break;
			case MULTIPLE_FATHERS_OR_MOTHERS:
				result = reportMultipleFathersOrMothers(segmentation, bundle);
			break;
			case NAMELESS_PERSONS:
				result = reportNamelessPersons(segmentation, bundle);
			break;
			case AUTO_MARRIAGE:
				result = reportAutoMarriages(segmentation, bundle);
			break;
			case PARENT_CHILD_MARRIAGES:
				result = reportParentChildMarriages(segmentation, bundle);
			break;
			case SAME_SEX_SPOUSES:
				result = reportSameSexSpouses(segmentation, bundle);
			break;
			case UNKNOWN_SEX_PERSONS:
				result = reportUnknownSexPersons(segmentation, bundle);
			break;
			case INCONSISTENT_DATES:
				result = reportInconsistentDates(segmentation, bundle);
			break;
			case MISSING_DATES:
				result = reportMissingDates(segmentation, bundle);
			break;
			case MISSING_DATES_COMPACT:
				result = reportMissingDatesCompact(segmentation, bundle);
			break;
			case UNKNOWN_SEX_PARENTS_SPOUSES:
				result = reportUnknownSexParentsSpouses(segmentation, bundle);
			break;
			default:
				result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static Report reportControls(final Net net, final Locale locale, final ControlType... controlTypes) {
		Report result;

		result = reportControls(new Segmentation(net), locale, controlTypes);

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static Report reportControls(final Net net, final ResourceBundle bundle) {
		Report result;

		result = reportControls(new Segmentation(net), bundle);

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static Report reportControls(final Net net, final ResourceBundle bundle, final ControlType... controlTypes) {
		Report result;

		result = reportControls(new Segmentation(net), bundle, controlTypes);

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static Report reportControls(final Segmentation segmentation, final Locale locale, final ControlType... controlTypes) {
		Report result;

		result = reportControls(segmentation, ResourceBundle.getBundle("org.tip.puckgui.messages", locale), controlTypes);

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static Report reportControls(final Segmentation segmentation, final ResourceBundle bundle) {
		Report result;

		result = reportControls(segmentation, bundle, ControlType.values());

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static Report reportControls(final Segmentation segmentation, final ResourceBundle bundle, final ControlType... controlTypes) {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		//
		result = new Report();
		result.setTitle("Controls");
		result.setOrigin("Control reporter");
		result.setTarget(segmentation.getLabel());

		//
		for (ControlType type : controlTypes) {
			result.inputs().add(type.toString(), "true");
		}

		//
		int errorCount = 0;
		for (ControlType controlType : controlTypes) {
			Report report = reportControl(segmentation, bundle, controlType);
			if ((report != null) && (report.status() != 0)) {
				errorCount += report.status();
				result.outputs().append(report.outputs());
				result.outputs().appendln();
			}
		}

		//
		result.setStatus(errorCount);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * Checks for possible undefinedRoles (special features) of a given type in
	 * the dataset (as specified in the options) and counts them.
	 * 
	 * @param k
	 *            the index of the type of possible error
	 * 
	 * @return the number of special features in the dataset
	 * 
	 * @see maps.Net#control()
	 */
	public static Report reportCyclicDescentCases(final Segmentation segmentation, final ResourceBundle bundle) {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		//
		StringList errors = new StringList();
		for (Individual individual : segmentation.getCurrentIndividuals()) {
			checkCycles(errors, individual, 5);
		}

		// Temporary solution for Stringlist counts a carriage return as a
		// separate line!
		int errorCount = errors.size() / 2;

		//
		result = new Report();
		result.setTitle("Checks for Cases of Cyclic Descent.");
		result.setOrigin("Control reporter");
		result.setTarget(segmentation.getLabel());

		//
		errors.add(0, errorCount + " " + Report.translate(bundle, "Cases of Cyclic Descent") + "\n");
		result.outputs().append(errors.toString());

		//
		result.setStatus(errorCount);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * Checks for possible undefinedRoles (special features) of a given type in
	 * the dataset (as specified in the options) and counts them.
	 * 
	 * @param k
	 *            the index of the type of possible error
	 * 
	 * @return the number of special features in the dataset
	 * 
	 * @see maps.Net#control()
	 */
	public static Report reportFemaleFathersOrMaleMothers(final Segmentation segmentation, final ResourceBundle bundle) {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		//
		int errorCount = 0;
		StringList errors = new StringList();
		for (Individual individual : segmentation.getCurrentIndividuals()) {
			Individual father = individual.getFather();
			if ((father != null) && (father.getGender().isFemale())) {
				errors.appendln(String.format("%c %s (%d)\t%s %s (%d)", father.getGender().toChar(), father.getTrimmedName(), father.getId(),
						Report.translate(bundle, "for"), individual.getNameTrimmed(), individual.getId()));

				errorCount += 1;
			}
			Individual mother = individual.getMother();
			if ((mother != null) && (mother.getGender().isMale())) {
				errors.appendln(String.format("%c %s (%d)\t%s %s (%d)", mother.getGender().toChar(), mother.getNameTrimmed(), mother.getId(),
						Report.translate(bundle, "for"), individual.getNameTrimmed(), individual.getId()));

				errorCount += 1;
			}
		}

		//
		result = new Report();
		result.setTitle("Checks for Female Fathers or Male Mothers.");
		result.setOrigin("Control reporter");
		result.setTarget(segmentation.getLabel());

		//
		errors.add(0, errorCount + " " + Report.translate(bundle, "Female Fathers or Male Mothers") + "\n");
		result.outputs().append(errors.toString());

		//
		result.setStatus(errorCount);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * Checks for possible undefinedRoles (special features) of a given type in
	 * the dataset (as specified in the options) and counts them.
	 * 
	 * @param k
	 *            the index of the type of possible error
	 * 
	 * @return the number of special features in the dataset
	 * 
	 * @see maps.Net#control()
	 */
	public static Report reportInconsistentDates(final Segmentation segmentation, final ResourceBundle bundle) {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		//
		int errorCount = 0;
		StringList errors = new StringList();
		for (Individual individual : segmentation.getCurrentIndividuals()) {
			Integer birtYear = IndividualValuator.extractYearAsInt(individual.getAttributeValue("BIRT_DATE"));
			Integer deatYear = IndividualValuator.extractYearAsInt(individual.getAttributeValue("DEAT_DATE"));
			if (deatYear != null && deatYear != 0 && birtYear != null && deatYear < birtYear) {
				errors.appendln(individual.signatureTab() + "\tdied (" + deatYear + ")\tbefore born (" + birtYear + ")\t");
				errorCount += 1;
			}
			if (individual.getFather() != null) {
				Integer fatherDeatYear = IndividualValuator.extractYearAsInt(individual.getFather().getAttributeValue("DEAT_DATE"));
				if (birtYear != null && fatherDeatYear != null && fatherDeatYear != 0 && birtYear > fatherDeatYear) {
					errors.appendln(individual.signatureTab() + "\tborn (" + birtYear + ")\tafter father's death (" + fatherDeatYear + ")\t"+individual.getFather().signatureTab());
					errorCount += 1;
				}
				Integer fatherBirtYear = IndividualValuator.extractYearAsInt(individual.getFather().getAttributeValue("BIRT_DATE"));
				if (birtYear != null && fatherBirtYear != null && (birtYear - fatherBirtYear < 15)) {
					errors.appendln(individual.signatureTab() + "\tborn (" + birtYear + ")\tto man of age " + (birtYear - fatherBirtYear)+"\t"+individual.getFather().signatureTab());
					errorCount += 1;
				}
			}
			if (individual.getMother() != null) {
				Integer motherDeatYear = IndividualValuator.extractYearAsInt(individual.getMother().getAttributeValue("DEAT_DATE"));
				if (birtYear != null && motherDeatYear != null && motherDeatYear != 0 && birtYear > motherDeatYear) {
					errors.appendln(individual.signatureTab() + "\tborn (" + birtYear + ")\tafter mother's death (" + motherDeatYear + ")\t"+individual.getMother().signatureTab());
					errorCount += 1;
				}
				Integer motherBirtYear = IndividualValuator.extractYearAsInt(individual.getMother().getAttributeValue("BIRT_DATE"));
				if (birtYear != null && motherBirtYear != null && (birtYear - motherBirtYear < 15 || birtYear - motherBirtYear > 55)) {
					errors.appendln(individual.signatureTab() + "\tborn (" + birtYear + ")\tto woman of age " + (birtYear - motherBirtYear)+ "\t"+individual.getMother().signatureTab());
					errorCount += 1;
				}
			}
			if (individual.getOriginFamily() != null) {
				Integer parentMarriageYear = IndividualValuator.extractYearAsInt(individual.getOriginFamily().getAttributeValue("MARR_DATE"));
				if (birtYear != null && parentMarriageYear != null && birtYear < parentMarriageYear) {
					errors.appendln(individual.signatureTab() + "\tborn (" + birtYear + ")\tbefore parents' marriage (" + parentMarriageYear + ")\t"+individual.getOriginFamily().signatureTab());
					errorCount += 1;
				}
				if (individual.getBirthOrder()!=null && birtYear!=null){
					for (Individual sibling : individual.fullSiblings()){
						if (sibling.getBirthOrder()!=null && sibling.getBirthOrder()<individual.getBirthOrder()) {
							Integer siblingBirtYear = IndividualValuator.extractYearAsInt(sibling.getAttributeValue("BIRT_DATE"));
							if (siblingBirtYear!=null && siblingBirtYear > birtYear){
								errors.appendln(individual.signatureTab() + "\tborn (" + birtYear + ";"+individual.getBirthOrder()+".)\tbefore elder sibling (" + siblingBirtYear + ";"+sibling.getBirthOrder()+".)\t"+sibling.signatureTab());
								errorCount += 1;
							}
						}
					}
				}
			}
			for (Family family : individual.getPersonalFamilies()) {
				Integer marriageYear = IndividualValuator.extractYearAsInt(family.getAttributeValue("MARR_DATE"));
				if (deatYear != null && deatYear != 0 && marriageYear != null && deatYear < marriageYear) {
					
					Individual otherParent = family.getOtherParent(individual);
					String otherParentSignatureTab = "";
					if (otherParent != null){
						otherParentSignatureTab = otherParent.signatureTab();
					}
					errors.appendln(individual.signatureTab() + "\tdied (" + deatYear + ")\tbefore marriage (" + marriageYear + ")\t"+otherParentSignatureTab);
					errorCount += 1;
				}
			}
			for (Relation relation : individual.relations()) {
				for (Attribute attribute : relation.attributes()) {
					if (attribute.getLabel().contains("DATE")) {
						Integer year = IndividualValuator.extractYearAsInt(attribute.getValue());
						if (year != null && birtYear != null && year < birtYear) {
							errors.appendln(individual.signatureTab() + "\tborn (" + birtYear + ")\tafter participating as "
									+ relation.getRoleNamesAsString(individual) + " in " + relation + " (" + year + ")");
							errorCount += 1;
						}

						if (year != null && !year.equals(birtYear) && relation.getModel().getName().equals("MIGEVENT") && relation.hasRole(individual, "MIG")
								&& RelationValuator.isBirth(relation)) {
							errors.appendln(individual.signatureTab() + "\tinconsistent birth date (" + birtYear + " vs " + year + ")");
							errorCount += 1;
						}

						if (year != null && deatYear != null && deatYear != 0 && year > deatYear) {
							errors.appendln(individual.signatureTab() + "\tdied (" + deatYear + ")\tbefore participating as "
									+ relation.getRoleNamesAsString(individual) + " in " + relation + " (" + year + ")\t");
							errorCount += 1;
						}
					}
				}
			}
		}

		// undefinedRoles.sort();

		//
		result = new Report();
		result.setTitle("Checks for Inconsistent Dates.");
		result.setOrigin("Control reporter");
		result.setTarget(segmentation.getLabel());

		//
		errors.add(0, errorCount + " " + Report.translate(bundle, "Inconsistent Dates") + "\n");
		result.outputs().append(errors.toString());

		//
		result.setStatus(errorCount);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * Checks for possible undefinedRoles (special features) of a given type in
	 * the dataset (as specified in the options) and counts them.
	 * 
	 * @param k
	 *            the index of the type of possible error
	 * 
	 * @return the number of special features in the dataset
	 * 
	 * @see maps.Net#control()
	 */
	public static Report reportMissingDates(final Segmentation segmentation, final ResourceBundle bundle) {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		String birthYearLabel = "BIRT_DATE";
		String deathYearLabel = "DEAT_DATE";
		String marrYearLabel = "MARR_DATE";

		StringList labels = IndividualValuator.getAttributeLabels(segmentation.getCurrentIndividuals());
		boolean hasBirthDates = labels.contains(birthYearLabel);
		boolean hasDeathDates = labels.contains(deathYearLabel);
		boolean hasMarrDates = labels.contains(marrYearLabel);

		//
		int errorCount = 0;
		StringList errors = new StringList();
		for (Individual individual : segmentation.getCurrentIndividuals()) {
			Integer birtYear = IndividualValuator.extractYearAsInt(individual.getAttributeValue(birthYearLabel));
			Integer deatYear = IndividualValuator.extractYearAsInt(individual.getAttributeValue(deathYearLabel));
			if (hasBirthDates && birtYear == null) {
				errors.appendln(individual.signatureTab() + "\tbirth date missing");
				errorCount += 1;
			}
			if (hasDeathDates && deatYear != null && deatYear == 0) {
				errors.appendln(individual.signatureTab() + "\tdeath date missing");
				errorCount += 1;
			}
			if (hasDeathDates && individual.getFather() != null) {
				Integer fatherDeatYear = IndividualValuator.extractYearAsInt(individual.getFather().getAttributeValue(deathYearLabel));
				if (fatherDeatYear != null && fatherDeatYear == 0) {
					errors.appendln(individual.signatureTab() + "\tfather's death date missing \t(" + individual.getFather() + ")");
					errorCount += 1;
				}
			}
			if (hasDeathDates && individual.getMother() != null) {
				Integer motherDeatYear = IndividualValuator.extractYearAsInt(individual.getMother().getAttributeValue(deathYearLabel));
				if (motherDeatYear != null && motherDeatYear == 0) {
					errors.appendln(individual.signatureTab() + "\tmother's death date missing \t(" + individual.getMother() + ")");
					errorCount += 1;
				}
			}
			for (Individual child : individual.children().toListSortedByBirthYearOrOrder()) {
				Integer childBirtYear = IndividualValuator.extractYearAsInt(child.getAttributeValue(birthYearLabel));
				if (hasBirthDates && childBirtYear == null) {
					if (IndividualValuator.extractYearAsInt(child.getAttributeValue(deathYearLabel)) == null) {
						errors.appendln(individual.signatureTab() + "\tchild birth date missing \t(" + child + ")");
					} else {
						errors.appendln(individual.signatureTab() + "\tchild birth date missing \t(" + child + " (+))");
					}
					errorCount += 1;
				}
			}
			for (Family family : individual.getPersonalFamilies().toListSortedByOrder(individual)) {
				Integer marriageYear = IndividualValuator.extractYearAsInt(family.getAttributeValue(marrYearLabel));
				if (hasMarrDates && marriageYear == null) {
					errors.appendln(individual.signatureTab() + "\tmarriage date missing \t(" + family.getOtherParent(individual) + ")");
					errorCount += 1;
				}
			}
		}

		//
		result = new Report();
		result.setTitle("Checks for Missing Dates.");
		result.setOrigin("Control reporter");
		result.setTarget(segmentation.getLabel());

		//
		errors.add(0, errorCount + " " + Report.translate(bundle, "Missing Dates") + "\n");
		result.outputs().append(errors.toString());

		//
		result.setStatus(errorCount);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	public static Report reportMissingDatesCompact(final Segmentation segmentation, final ResourceBundle bundle) {
		Report result;

		//
		Chronometer chrono = new Chronometer();
		
		String birthYearLabel = "BIRT_DATE";
		String deathYearLabel = "DEAT_DATE";
		String marrYearLabel = "MARR_DATE";

		StringList labels = IndividualValuator.getAttributeLabels(segmentation.getCurrentIndividuals());
		boolean hasBirthDates = labels.contains(birthYearLabel);
		boolean hasDeathDates = labels.contains(deathYearLabel);
		boolean hasMarrDates = labels.contains(marrYearLabel);

		//
		int errorCount = 0;
		StringList errors = new StringList();
		for (Individual individual : segmentation.getCurrentIndividuals()) {
			String line = "";
			Integer birtYear = IndividualValuator.extractYearAsInt(individual.getAttributeValue(birthYearLabel));
			Integer deatYear = IndividualValuator.extractYearAsInt(individual.getAttributeValue(deathYearLabel));
			if (hasBirthDates && birtYear == null) {
				line = add(line, "birth date missing", "");
				errorCount += 1;
			}
			if (hasDeathDates && deatYear != null && deatYear == 0) {
				line = add(line, "death date missing", "; ");
				errorCount += 1;
			}
			if (individual.getFather() != null) {
				Integer fatherDeatYear = IndividualValuator.extractYearAsInt(individual.getFather().getAttributeValue(deathYearLabel));
				if (hasDeathDates && fatherDeatYear != null && fatherDeatYear == 0) {
					line = add(line, "Father's death date missing: " + individual.getFather(), "; ");
					errorCount += 1;
				}
			}
			if (individual.getMother() != null) {
				Integer motherDeatYear = IndividualValuator.extractYearAsInt(individual.getMother().getAttributeValue(deathYearLabel));
				if (hasDeathDates && motherDeatYear != null && motherDeatYear == 0) {
					line = add(line, "Mother's death date missing: " + individual.getMother(), "; ");
					errorCount += 1;
				}
			}
			if (!StringUtils.isBlank(line)) {
				errors.appendln(individual.getId() + "\t" + individual + "\t" + line);
			}

			line = "";
			String line2 = "";
			for (Individual child : individual.children().toListSortedByBirthYearOrOrder()) {
				Integer childBirtYear = IndividualValuator.extractYearAsInt(child.getAttributeValue(birthYearLabel));
				if (hasBirthDates && childBirtYear == null) {
					if (IndividualValuator.extractYearAsInt(child.getAttributeValue(deathYearLabel)) == null) {
						line = add(line, child.toString(), ", ");
					} else {
						line2 = add(line2, child.toString(), ", ");
					}
					errorCount += 1;
				}
			}
			if (!StringUtils.isBlank(line)) {
				errors.appendln(individual.getId() + "\t" + individual + "\tBirth dates missing for children: " + line);
			}
			if (!StringUtils.isBlank(line2)) {
				errors.appendln(individual.getId() + "\t" + individual + "\tBirth dates missing for deceased children: " + line2);
			}

			line = "";
			for (Family family : individual.getPersonalFamilies()) {
				Integer marriageYear = IndividualValuator.extractYearAsInt(family.getAttributeValue(marrYearLabel));
				if (hasMarrDates && marriageYear == null && family.getOtherParent(individual) != null) {
					line = add(line, family.getOtherParent(individual).toString(), ", ");
					errorCount += 1;
				}
			}
			if (!StringUtils.isBlank(line)) {
				errors.appendln(individual.getId() + "\t" + individual + "\tMarriage dates missing for spouses: " + line);
			}
		}

		//
		result = new Report();
		result.setTitle("Checks for Missing Dates.");
		result.setOrigin("Control reporter");
		result.setTarget(segmentation.getLabel());

		//
		errors.add(0, errorCount + " " + Report.translate(bundle, "Missing Dates") + "\n");
		result.outputs().append(errors.toString());

		//
		result.setStatus(errorCount);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * 
	 * @param segmentation
	 * @param bundle
	 * @return
	 */
	public static Report reportMultipleFathersOrMothers(final Segmentation segmentation, final ResourceBundle bundle) {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		//
		int errorCount = 0;
		StringList errors = new StringList();
		for (Individual individual : segmentation.getCurrentIndividuals()) {
			for (Family family : individual.getPersonalFamilies()) {
				Individual parent = family.getParent(individual.getGender());
				if (parent != null && parent != individual) {
					errors.appendln(String.format("%c %s (%d)\t%s %s (%d)", individual.getGender().toChar(), individual.getTrimmedName(), individual.getId(),
							Report.translate(bundle, "and"), parent.getNameTrimmed(), parent.getId()));
					errorCount += 1;
				}
			}
		}

		//
		result = new Report();
		result.setTitle("Checks for Multiple Fathers or Mothers.");
		result.setOrigin("Control reporter");
		result.setTarget(segmentation.getLabel());

		//
		errors.add(0, errorCount + " " + Report.translate(bundle, "Multiple Fathers/Mothers") + "\n");
		result.outputs().append(errors.toString());

		//
		result.setStatus(errorCount);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * Checks for possible undefinedRoles (special features) of a given type in
	 * the dataset (as specified in the options) and counts them.
	 * 
	 * @param k
	 *            the index of the type of possible error
	 * 
	 * @return the number of special features in the dataset
	 * 
	 * @see maps.Net#control()
	 */
	public static Report reportNamelessPersons(final Segmentation segmentation, final ResourceBundle bundle) {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		//
		int errorCount = 0;
		StringList errors = new StringList();
		for (Individual individual : segmentation.getCurrentIndividuals()) {

			if (StringUtils.isBlank(individual.getName())) {
				errors.appendln(individual.getGender().toChar() + " " + individual.getId());
				errorCount += 1;
			}
		}

		//
		result = new Report();
		result.setTitle("Checks for Nameless Persons.");
		result.setOrigin("Control reporter");
		result.setTarget(segmentation.getLabel());

		//
		errors.add(0, errorCount + " " + Report.translate(bundle, "Nameless Persons") + "\n");
		result.outputs().append(errors.toString());

		//
		result.setStatus(errorCount);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * Checks for possible undefinedRoles (special features) of a given type in
	 * the dataset (as specified in the options) and counts them.
	 * 
	 * @param k
	 *            the index of the type of possible error
	 * 
	 * @return the number of special features in the dataset
	 * 
	 * @see maps.Net#control()
	 */
	public static Report reportParentChildMarriages(final Segmentation segmentation, final ResourceBundle bundle) {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		//
		int errorCount = 0;
		StringList errors = new StringList();
		for (Individual individual : segmentation.getCurrentIndividuals()) {

			for (Individual parent : individual.getParents()) {
				if (individual.getPartners().contains(parent)) {
					errors.appendln(String.format("%s (%d) %s %s (%d)", individual.getTrimmedName(), individual.getId(), Report.translate(bundle, "and"),
							parent.getNameTrimmed(), parent.getId()));
					errorCount += 1;
				}
			}
		}

		//
		result = new Report();
		result.setTitle("Checks for Parent-Child Marriages.");
		result.setOrigin("Control reporter");
		result.setTarget(segmentation.getLabel());

		//
		errors.add(0, errorCount + " " + Report.translate(bundle, "Parent-Child Marriages") + "\n");
		result.outputs().append(errors.toString());

		//
		result.setStatus(errorCount);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * Checks for possible undefinedRoles (special features) of a given type in
	 * the dataset (as specified in the options) and counts them.
	 * 
	 * @param k
	 *            the index of the type of possible error
	 * 
	 * @return the number of special features in the dataset
	 * 
	 * @see maps.Net#control()
	 */
	public static Report reportSameSexSpouses(final Segmentation segmentation, final ResourceBundle bundle) {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		//
		int errorCount = 0;
		StringList errors = new StringList();
		for (Family family : segmentation.getCurrentFamilies()) {
			if (family.getHusband() != null && family.getWife() != null && family.getHusband().getGender() == family.getWife().getGender()) {
				errors.appendln(String.format("%c %s (%d)\t%s %s (%d)", family.getHusband().getGender().toChar(), family.getWife().getTrimmedName(), family
						.getWife().getId(), Report.translate(bundle, "for"), family.getHusband().getNameTrimmed(), family.getHusband().getId()));
				errorCount += 1;
			}
		}

		//
		result = new Report();
		result.setTitle("Checks for Same-Sex Couples.");
		result.setOrigin("Control reporter");
		result.setTarget(segmentation.getLabel());

		//
		errors.add(0, errorCount + " " + Report.translate(bundle, "Same-Sex Couples") + "\n");
		result.outputs().append(errors.toString());

		//
		result.setStatus(errorCount);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * Checks for possible undefinedRoles (special features) of a given type in
	 * the dataset (as specified in the options) and counts them.
	 * 
	 * @param k
	 *            the index of the type of possible error
	 * 
	 * @return the number of special features in the dataset
	 * 
	 * @see maps.Net#control()
	 */
	public static Report reportUnknownSexParentsSpouses(final Segmentation segmentation, final ResourceBundle bundle) {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		//
		int errorCount = 0;
		StringList errors = new StringList();
		for (Individual individual : segmentation.getCurrentIndividuals()) {
			if (individual.getGender().isUnknown() && (!individual.isSingle() || !individual.isSterile())) {
				errors.appendln(individual + "");
				errorCount += 1;
			}
		}

		//
		result = new Report();
		result.setTitle("Checks for parents or spouses of unknown sex.");
		result.setOrigin("Control reporter");
		result.setTarget(segmentation.getLabel());

		//
		errors.add(0, errorCount + " " + Report.translate(bundle, "Parents or spouses of unknown Sex") + "\n");
		result.outputs().append(errors.toString());

		//
		result.setStatus(errorCount);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * Checks for possible undefinedRoles (special features) of a given type in
	 * the dataset (as specified in the options) and counts them.
	 * 
	 * @param k
	 *            the index of the type of possible error
	 * 
	 * @return the number of special features in the dataset
	 * 
	 * @see maps.Net#control()
	 */
	public static Report reportUnknownSexPersons(final Segmentation segmentation, final ResourceBundle bundle) {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		//
		int errorCount = 0;
		StringList errors = new StringList();
		for (Individual individual : segmentation.getCurrentIndividuals()) {
			if (individual.getGender().isUnknown()) {
				errors.appendln(individual + "");
				errorCount += 1;
			}
		}

		//
		result = new Report();
		result.setTitle("Checks for Persons of unknown Sex.");
		result.setOrigin("Control reporter");
		result.setTarget(segmentation.getLabel());

		//
		errors.add(0, errorCount + " " + Report.translate(bundle, "Persons of unknown Sex") + "\n");
		result.outputs().append(errors.toString());

		//
		result.setStatus(errorCount);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}
	
	public static Report reportIncoherentKinAttributes(final Segmentation segmentation){
		Report result;

		//
		Chronometer chrono = new Chronometer();
		
		Map<Gender,String> familyParents = new HashMap<Gender,String>();
		familyParents.put(Gender.MALE, "HUSB_NOMNOM");
		familyParents.put(Gender.FEMALE, "WIFE_NOMNOM");
		
		Map<Gender,String> childParents = new HashMap<Gender,String>();
		childParents.put(Gender.MALE, "32 père: nom");
		childParents.put(Gender.FEMALE, "42 mère: nom");
		
		//
		int errorCount = 0;
		StringList errors = new StringList();
		
		for (Family family : segmentation.getCurrentFamilies()){
			for (Gender gender : Gender.values()){
				Individual parent = family.getParent(gender);
				if (parent!=null){
					String name = parent.getLastName();
					String nameByFamilyAttributeLabel = familyParents.get(parent.getGender());
					String nameByFamilyAttributeValue = family.getAttributeValue(nameByFamilyAttributeLabel);
					if (name!=null && nameByFamilyAttributeValue!=null && !name.equals(nameByFamilyAttributeValue)){
						errors.appendln("Divergent name in family attribute\t"+parent+"\tvs "+nameByFamilyAttributeLabel+"\t(family "+family+"):\t"+parent.getLastName()+"\tvs\t"+nameByFamilyAttributeValue);
						errorCount++;
					}
					for (Individual child : family.getChildren()){
						String nameByChildAttributeLabel = childParents.get(parent.getGender());
						String nameByChildAttributeValue = child.getAttributeValue(nameByChildAttributeLabel);
						if (name!=null && nameByChildAttributeValue!=null && !name.equals(nameByChildAttributeValue)){
							errors.appendln("Divergent name in child attribute\t"+parent+"\tvs "+nameByChildAttributeLabel+"\t(child "+child+"):\t"+parent.getLastName()+"\tvs\t"+nameByChildAttributeValue);
							errorCount++;
						}
					}
				}
			}
		}
		
		//
		result = new Report();
		result.setTitle("Incoherent Kin Attributes");
		result.setOrigin("Control reporter");
		result.setTarget(segmentation.getLabel());
		
		result.outputs().append(errors);
		result.setStatus(errorCount);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;


	}
	
	public static ControlType[] getDefaultControls(){
		ControlType[] result;
		
		List<ControlType> controlTypes = new ArrayList<ControlType>();
			controlTypes.add(ControlType.CYCLIC_DESCENT_CASES);
			controlTypes.add(ControlType.FEMALE_FATHERS_OR_MALE_MOTHERS);
			controlTypes.add(ControlType.MULTIPLE_FATHERS_OR_MOTHERS);
			controlTypes.add(ControlType.NAMELESS_PERSONS);
			controlTypes.add(ControlType.PARENT_CHILD_MARRIAGES);
			controlTypes.add(ControlType.AUTO_MARRIAGE);
			controlTypes.add(ControlType.SAME_SEX_SPOUSES);
			controlTypes.add(ControlType.UNKNOWN_SEX_PERSONS);
			controlTypes.add(ControlType.UNKNOWN_SEX_PARENTS_SPOUSES);
			controlTypes.add(ControlType.INCONSISTENT_DATES);
//			controlTypes.add(ControlType.MISSING_DATES);
//			controlTypes.add(ControlType.MISSING_DATES_COMPACT);

		result = new ControlType[controlTypes.size()];
		controlTypes.toArray(result);
		
		//
		return result;
	}

	/**
	 * @param segmentation
	 * @param bundle
	 * @return
	 */
	public static Report reportUnknownPlaces(final Segmentation segmentation, final Geography geography, final ResourceBundle bundle) {
		Report result;
	
		UnknownPlacesCriteria criteria = new UnknownPlacesCriteria();
		criteria.setIncludedIndividual(true);
		criteria.setIncludedAllRelations(true);
	
		result = ControlReporter.reportUnknownPlaces(segmentation, geography, criteria, bundle);
	
		//
		return result;
	}

	/**
	 * @param segmentation
	 * @param bundle
	 * @return
	 */
	public static Report reportUnknownPlaces(final Segmentation segmentation, final Geography geography, final UnknownPlacesCriteria criteria, final ResourceBundle bundle) {
		Report result;
	
		//
		Chronometer chrono = new Chronometer();
	
		Partition<String> unknownPlaces = new Partition<String>();
	
		int errorCount = 0;
		StringList errors = new StringList();
	
		//
		if (criteria.isIncludedIndividual()) {
			//
			for (Individual individual : segmentation.getCurrentIndividuals()) {
				for (Attribute attribute : individual.attributes()) {
					if (attribute.getLabel().contains("PLAC")) {
						if (geography.getByHomonym(attribute.getValue()) == null) {
							unknownPlaces.put(attribute.getValue(), new Value(attribute.getLabel()));
							errorCount++;
						}
					}
				}
			}
		}
	
		//
		Relations relations;
		if (criteria.isIncludedAllRelations()) {
			//
			relations = segmentation.getCurrentRelations();
	
		} else if (criteria.getRelationNames().isEmpty()) {
			//
			relations = new Relations();
	
		} else {
			//
			relations = segmentation.getCurrentRelations().getByModelNames(criteria.getRelationNames());
		}
	
		for (Relation relation : relations) {
			for (Attribute attribute : relation.attributes()) {
				if (attribute.getLabel().contains("PLAC")) {
					if (geography.getByHomonym(attribute.getValue()) == null) {
						unknownPlaces.put(attribute.getValue(), new Value(attribute.getLabel()));
						errorCount++;
					}
				}
			}
		}
	
		for (Cluster<String> cluster : unknownPlaces.getClusters().toListSortedByValue()) {
			errors.appendln(cluster.getValue().toString());
			for (Object placeName : cluster.getItemsAsSortedList()) {
				errors.appendln("\t" + placeName);
			}
			errors.appendln();
		}
	
		//
		result = new Report();
		result.setTitle("Unknown Places");
		result.setOrigin("Control reporter");
		result.setTarget(segmentation.getLabel());
	
		//
		errors.add(0, errorCount + " " + Report.translate(bundle, "Unknown Places") + "\n");
		result.outputs().append(errors.toString());
	
		//
		result.setStatus(errorCount);
	
		//
		result.setTimeSpent(chrono.stop().interval());
	
		//
		return result;
	}

}
