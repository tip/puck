package org.tip.puck;

import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * 
 * 
 * @author TIP
 */
public class PuckException extends Exception {

	private static final long serialVersionUID = -5156031052993724552L;

	public static final int NOCODE = 0;

	private int code;

	/**
     * 
     */
	public PuckException() {
		super();
		this.code = NOCODE;
	}

	/**
	 * 
	 * @param code
	 * @param message
	 */
	public PuckException(final int code, final String message) {
		super(message);
		this.code = code;
	}

	/**
	 * 
	 * @param code
	 * @param message
	 * @param exception
	 */
	public PuckException(final int code, final String message, final Exception exception) {
		super(message, exception);
		this.code = code;
	}

	/**
	 * 
	 * @return
	 */
	public int getCode() {
		int result;

		result = this.code;

		//
		return result;
	}

	/**
	 * 
	 * @param code
	 */
	public void setCode(final int code) {
		this.code = code;
	}

	/**
     * 
     */
	@Override
	public String toString() {
		String result;

		//
		if (this.getCause() == null) {
			result = MessageFormat.format("[code={0}]: {1}", this.getCode(), this.getMessage());
		} else {
			result = MessageFormat.format("[code={0}]: {1} <= {2}", this.getCode(), this.getMessage(), this.getCause().toString());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param caller
	 * 
	 * @return
	 */
	public String toString(final StackTraceElement caller) {
		String result;

		if (caller == null) {
			result = toString();
		} else {
			result = MessageFormat.format("{0} line {1} {2}", shrink(caller.getClassName()), caller.getLineNumber(), toString());
		}

		//
		return result;
	}

	/**
	 * This method instanciates an exception and log it.
	 * 
	 * @param caller
	 * @param code
	 * @param message
	 * @param exception
	 */
	public static PuckException getInstance(final StackTraceElement caller, final int code, final String message, final Exception exception) {
		PuckException result;

		result = new PuckException(code, message, exception);

		Logger.getLogger(PuckException.class).error(result.toString(caller));

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * 
	 * @return
	 */
	public static String shrink(final String source) {
		String result;

		if (StringUtils.isBlank(source)) {
			result = null;
		} else {
			String[] tokens = source.split("\\.");

			StringBuffer buffer = new StringBuffer(source.length());

			for (int tokenCount = 0; tokenCount < tokens.length - 2; tokenCount++) {
				buffer.append(tokens[tokenCount].substring(0, 1));
				buffer.append('.');
			}

			if (tokens.length >= 2) {
				buffer.append(tokens[tokens.length - 2]);
				buffer.append('.');
			}
			if (tokens.length >= 1) {
				buffer.append(tokens[tokens.length - 1]);
			}

			result = buffer.toString();
		}

		// LoggerFactory.getLogger(SBException.class).debug("shrink["
		// + source + "]=>[" + result + "]");

		//
		return result;
	}
}
