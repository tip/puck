package org.tip.puck.sequences;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.workers.IndividualValuator;

public class Ordinal implements Comparable<Ordinal> {
	
	String name;
	Integer year;
	Calendar calendar;
	Integer order;
	
	public Ordinal (int year){
		
		this.year = year;
		this.name = year+"";
	}
	
	public Ordinal (String date, Integer order, String start, String end, String previous, Relation event){
		
		if (date!=null){

			name = IndividualValuator.extractYear(date);
			
			if (name==null){
				name = "nd";
			} else {
				year = Integer.parseInt(name); 
			}
		}
		
		this.order = order;
		
	}
	
	public Ordinal (String name, Integer order, Integer year){
		this.name = name;
		this.order = order;
		this.year = year;
	}
	
	public boolean equals (Object obj){
		
		boolean result;
		
		result = false;
		if (obj != null){
			Ordinal other = (Ordinal)obj;
			result = (name == null ? other.name == null : name.equals(other.name) && (year==null ? other.year == null : year.equals(other.year)) && (order==null ? other.order == null : order.equals(other.order)) && (calendar == null ? other.calendar == null : calendar.equals(other.calendar)));
		}
				
		return result;
	}
	
	public int hashCode(){
		
		final int prime = 31;
		
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((order == null) ? 0 : order.hashCode());
		result = prime * result + ((year == null) ? 0 : year.hashCode());
		result = prime * result + ((calendar == null) ? 0 : calendar.hashCode());
		//
		return result;
	}
	
	public int compareTo(Ordinal other){
		int result;
		
		result = ObjectUtils.compare(calendar, other.calendar);
		
		if (result == 0){
			result = ObjectUtils.compare(year, other.year);
		}
		
/*		if (result == 0){
			if (start!=null && other.start==null){
				result = +1;
			} else if (other.start!=null && start == null) {
				result = -1;
			}
		}
		
		if (result == 0 && (order==null || other.order==null || order.equals(other.order))){
			
			if (start!=null && start.equals(other.end) && !(end!=null && end.equals(other.start))){
				result = +1;
			} else if (end!=null && end.equals(other.start) && !(start!=null && start.equals(other.end))){
				result = -1;
			}

			if (result == 0) {
				if (previous!=null && previous.equals(start) && previous.equals(other.end) && !(other.previous!=null && other.previous.equals(other.start) && other.previous.equals(end))){
					result = -1;
				} else if (other.previous!=null && other.previous.equals(other.start) && other.previous.equals(end) && !(previous!=null && previous.equals(start) && previous.equals(other.end))){
					result = +1;
				}
			}
			
		}*/
		
		if (result == 0){
			result = ObjectUtils.compare(order, other.order);
		}
		
		if (result == 0){
			result = ObjectUtils.compare(name, other.name);
		}

		
		//
		return result;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public Calendar getCalendar() {
		return calendar;
	}
	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	
	public String toString(){
		String result;
		
		result = "";
		
		if (year!=null){
			result+=year;
		}
		if (order!=null && order!=0){
			result += "/"+order;
		}
		if (name!=null && !name.equals(year+"")){
			result+= " ("+name+")";
		}
		//
		return result;
	}
	
	public String description(){
		return name+" "+order+" "+year+" "+calendar;
	}
	
	public static List<Ordinal> getOrdinals(Integer[] years){
		List<Ordinal> result;
		
		result = new ArrayList<Ordinal>();
		
		for (Integer year : years){
			result.add(new Ordinal(year));
		}
		//
		return result;
	}
	
	

}
