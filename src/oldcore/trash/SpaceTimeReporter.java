package oldcore.trash;


/**
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class SpaceTimeReporter {

	

	// For event sequences (define and enlarge for state sequences)
	/*	private static void setPartitionCriteria (SequenceCriteria criteria){
		
		// Set sequence value criteria 
		for (String label : criteria.getSequenceValueLabels()){
			
			criteria.addSequenceValueCriteria(new PartitionCriteria(label));
		}
		
		// Optionalize
		criteria.addSequenceValueCriteria(new PartitionCriteria("PROFILE#AGE"));
		criteria.addSequenceValueCriteria(new PartitionCriteria("PROFILE#"+criteria.getDateLabel()));
		
		
		// Set value sequence criteria
		
		for (String label : criteria.getSequenceValueCriteriaList().getLabels()){
			
			String[] labels = ToolBox.splitLastPart(label,"#");
			if (labels[1] != null){
				criteria.addValueSequenceCriteria(new PartitionCriteria(labels[1]));
			}
		}
		
		
		
		
		
		// Set sequence value criteria
		
		for (PartitionCriteria sequenceValueCriteria : criteria.getSequenceValueCriteriaList()){
			
			String label = sequenceValueCriteria.getLabel();

			if (label.equals("NREVENTS") || label.contains("NRSTATIONS")) {
				sequenceValueCriteria.setType(PartitionType.FREE_GROUPING);
				sequenceValueCriteria.setIntervals(PartitionMaker.getIntervals("1 5 10 15 20 25"));
				// partitionCriteria.setCumulationType(CumulationType.DESCENDANT);
			} else if (label.contains("AGEFIRST")) {
				sequenceValueCriteria.setType(PartitionType.SIZED_GROUPING);
				sequenceValueCriteria.setStart(0.);
				sequenceValueCriteria.setSize(5.);
			} else if (label.equals("ECCENTRICITY")) {
				sequenceValueCriteria.setType(PartitionType.SIZED_GROUPING);
				sequenceValueCriteria.setStart(-100.);
				sequenceValueCriteria.setSize(20.);
			} else if (label.contains("COVERAGE") || label.contains("SAME") || label.contains("NORM") || label.contains("DENSITY")
					|| label.contains("BETWEENNESS") || label.contains("EFFICIENCY") || label.contains("CONCENTRATION")) {
				sequenceValueCriteria.setType(PartitionType.SIZED_GROUPING);
				sequenceValueCriteria.setStart(0.);
				sequenceValueCriteria.setSize(20.);
			} else if (label.contains("MEAN") || label.contains("COVERAGE") || label.contains("PEREVENT") || label.contains("BETWEENNESS")
					|| label.contains("BROKERAGE") || label.contains("EFFICIENT_SIZE")) {
				sequenceValueCriteria.setType(PartitionType.SIZED_GROUPING);
				sequenceValueCriteria.setStart(0.);
				sequenceValueCriteria.setSize(1.);
			} else if (label.substring(0, 3).equals("AGE")) {
				sequenceValueCriteria.setType(PartitionType.RAW);
				sequenceValueCriteria.setSizeFilter(SizeFilter.HOLES);
				sequenceValueCriteria.setValueFilter(ValueFilter.NULL);
			} else {
				sequenceValueCriteria.setType(PartitionType.RAW);
			}
			
			if (!label.contains("ALTERS") && !label.contains("PROFILE") && !label.contains("SUPPORT")&& !label.contains("RELATIONS")) {
				sequenceValueCriteria.setWithDiagram(true);
			}
		}
		
		
		
	}*/
	
	
	

/*	public static ReportList reportStateSequenceAnalysis(final Segmentation segmentation, final SequenceCriteria criteria)
			throws PuckException {
		ReportList result;
		
		result = new ReportList();
		
		if (criteria != null) {
			
			// Kinship Circuits.
			if (criteria.getReferentNetworkOperations().contains(ReferentNetworkOperation.CIRCUITS)){
				for (Report report3 : RelationReporter.reportRelationCensus(segmentation, criteria)) {
					result.add(report3);
				}
			}

			// Differential Circuit Census.
//			if (criteria.getReferentNetworkOperations().contains(ReferentNetworkOperation.DIFFERENTIAL_CENSUS)){
				CensusCriteria censusCriteria = new CensusCriteria();

				censusCriteria.setPattern(criteria.getPattern());
				censusCriteria.setChainClassification(criteria.getChainClassification());
				censusCriteria.setRelationAttributeLabel(criteria.getLocalUnitLabel());

				Integer[] dates = criteria.getDates();
				if (dates == null) {
					Report report = CensusReporter.reportDifferentialCensus(segmentation, criteria, null,
							censusCriteria);
					result.add(report);
				} else {
					for (Integer date : dates) {
						Report report = CensusReporter.reportDifferentialCensus(segmentation, criteria, date,
								censusCriteria);
						result.add(report);
					}
				}
//			}
		}
		
		//
		return result;
		
	}*/

}
