package oldcore.trash;

import org.tip.puck.PuckException;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.Sequenceable;
import org.tip.puck.sequences.Sequenceables;
import org.tip.puck.sequences.workers.SequenceStatistics;
import org.tip.puck.sequences.workers.SequenceCriteria;
import org.tip.puck.util.Numberable;

/**
 * 
 * @author Klaus Hamberger
 *
 */
public class EgoSequenceStatistics<S extends Sequenceable<E>,E extends Numberable> extends SequenceStatistics<S,E> {
	
//	private EgoSequences sequences;
//	private List<String> labels;
//	private Map<String, NumberedValues> valuesMap;
		

		

	
//	Map<RelationClassificationType,Map<String,Integer>> nrValues;
	

	public EgoSequenceStatistics (Segmentation segmentation, Sequenceables<S,E> sequences, SequenceCriteria criteria) throws PuckException{
		super(segmentation,sequences,criteria);
		
//		this.sequences = sequences;
		
/*		this.labels = new ArrayList<String>();
		this.labels.addAll(criteria.getCensusOperationLabels());
		this.labels.add("PROFILE_AGE");
		this.labels.add("PROFILE_"+criteria.getDateLabel());*/
				
//		valuesMap = new HashMap<String, NumberedValues>();

/*		nrValues = new HashMap<RelationClassificationType,Map<String,Integer>>();

		for (RelationClassificationType relationClassificationType : criteria.getTrajectoriesRelationClassificationTypes()){
			nrValues.put(relationClassificationType, new HashMap<String,Integer>());
		}
		
		// Optionalize!
		Map<String,Map<Integer,Graph<Cluster<Relation>>>> parcoursNetworksMap = new HashMap<String,Map<Integer,Graph<Cluster<Relation>>>>();
		for (String networkTitle : criteria.getNetworkTitles()){
			if (networkTitle.contains("Parcours Network") && !networkTitle.contains("Fused")){
				parcoursNetworksMap.put(networkTitle.substring(networkTitle.lastIndexOf("_")+1),new HashMap<Integer,Graph<Cluster<Relation>>>());
			}
		}


		for (S sequence : sequences){
						
//			SequenceNetworkMaker census = new SequenceNetworkMaker((EgoSequence)sequence, criteria);
			
//			Map<Individual,List<String>> relationsByAlter = census.getRelationsByAlter();
			
			
			Map<String,GraphProfile<?>> networkProfiles = SequenceNetworkMaker.createNetworkProfiles(sequence, criteria);

			for (String label : criteria.getSequenceValueCriteriaList().getLabels()){
				if (label.contains("SIMILARITY")){
					
					RelationClassificationType relationClassificationType = RelationClassificationType.valueOf(label.substring(label.lastIndexOf("_")+1));
					GraphProfile<Individual> parcoursNetworkProfile = (GraphProfile<Individual>)networkProfiles.get("Parcours Intersection Network_"+relationClassificationType);
					
					Partition<Link<Individual>> linkPartition = parcoursNetworkProfile.getLinkPartition();
					if (linkPartition==null){
						linkPartition = GraphUtils.getLinkPartitionByKinship(parcoursNetworkProfile.getGraph());
						parcoursNetworkProfile.setLinkPartition(linkPartition);
					}
					linkPartitions.get(relationClassificationType).add(linkPartition);
//					linkPartitions.get(relationClassificationType).add(census.getParcoursLinkPartition(relationClassificationType));
					
				} 
				
//				valuesMap.get(label).put(sequence.getEgo().getId(), census.getValue(label));				
			}
			
			if (componentsMap!=null){
				
				for (String networkTitle : criteria.getNetworkTitles()){
					Map<Integer,Partition<Node<Individual>>> components = componentsMap.get(networkTitle);
					if (components!=null){
						if (networkTitle.contains("Ego Network")){
							components.put(((EgoSequence)sequence).getEgo().getId(), census.getComponents("Nonmediated Ego Network"));
						} else if (networkTitle.contains("Parcours Similarity Network")){
							RelationClassificationType relationClassificationType = RelationClassificationType.valueOf(networkTitle.substring(networkTitle.lastIndexOf("_")+1));
							components.put(((EgoSequence)sequence).getEgo().getId(), census.getComponents("Parcours Similarity Network_"+relationClassificationType));
						}
					}
				}
			}
			
			for (String networkTitle : criteria.getNetworkTitles()){
				if (networkTitle.contains("Parcours Network") && !networkTitle.contains("Fused")){
					
					GraphProfile<Cluster<Relation>> parcoursNetworkProfile = (GraphProfile<Cluster<Relation>>)networkProfiles.get(networkTitle);
					if (parcoursNetworkProfile != null){
						parcoursNetworksMap.get(networkTitle.substring(networkTitle.lastIndexOf("_")+1)).put(((EgoSequence)sequence).getEgo().getId(), parcoursNetworkProfile.getGraph());
					}
//					parcoursNetworksMap.get(networkTitle.substring(networkTitle.lastIndexOf("_")+1)).put(((EgoSequence)sequence).getEgo().getId(), (Graph<Cluster<Relation>>)census.getNetwork(networkTitle));
				}
			}


			
			for (EventType eventType : criteria.getMainEventTypes()){
				
				String eventTypeName = eventType.toString();

				if (criteria.getNetworkTitles().contains("Event Type Network")){
					
					Partition<String> eventPartition = eventPartitions.get(eventTypeName);
					Partition<String> eventPairPartition = eventPairPartitions.get(eventTypeName);
					
					census.putEvents(eventPartition,EventType.valueOf(eventTypeName));
					census.putEventPairs(eventPairPartition, EventType.valueOf(eventTypeName));

					
				} 
				
				if (criteria.getNetworkTitles().contains("Sequence Type Network")){

					Partition<String> subSequencePartition = subSequencePartitions.get(eventTypeName);
					
					census.putSubSequences(subSequencePartition,EventType.valueOf(eventTypeName));
				} 
			}
			
			
			for (String networkTitle : criteria.getNetworkTitles()){
				
				if (!networkTitle.equals("Event Type Network") && !networkTitle.equals("Sequence Type Network")){
					
					GraphProfile<Cluster<Relation>> networkProfile = (GraphProfile<Cluster<Relation>>)networkProfiles.get(networkTitle);
					if (networkProfile != null){
						Graph<?> network = networkProfile.getGraph();
						if (network.nodeCount()>0){
							pajekBuffers.get(networkTitle).addAll(PuckUtils.writePajekNetwork(network,census.getPartitionLabels(networkTitle)));
						}
					}

					Graph<?> network = (Graph<?>)census.getNetwork(networkTitle);
					if (network.nodeCount()>0){
						pajekBuffers.get(title).addAll(PuckUtils.writePajekNetwork(network,census.getPartitionLabels(networkTitle)));
					}

//					census.writePajekNetwork(pajekBuffers.get(title), title);

				}
				
			}
			
		}*/
		
		// Create union graphs, similarity networks and phylogenetic trees 
		
/*		for (String networkTitle : criteria.getNetworkTitles()){ // Add condition for the two operations
			if (networkTitle.contains("Parcours Network") && !networkTitle.contains("Fused")){
									
				Map<Integer,Graph<Cluster<Relation>>> parcoursNetworks = parcoursNetworksMap.get(networkTitle.substring(networkTitle.lastIndexOf("_")+1));
				List<Graph<String>> flatParcoursNetworksNoLoops = new ArrayList<Graph<String>>();
				
				for (Graph<Cluster<Relation>> parcoursNetwork : parcoursNetworks.values()){
					Graph<String> flatParcoursNetworkNoLoops = SequenceNetworkMaker.getFlatParcoursNetworkNoLoops(parcoursNetwork);*/
/*					Graph<String> flatParcoursNetworkNoLoops = new Graph<String>(parcoursNetwork.getLabel());
					for (Link<Cluster<Relation>> link : parcoursNetwork.getLinks()){
						if (!link.isLoop()){
							flatParcoursNetworkNoLoops.addArc(link.getSourceNode().getReferent().getLabel(),link.getTargetNode().getReferent().getLabel());
						}
					}*/
/*					flatParcoursNetworksNoLoops.add(flatParcoursNetworkNoLoops);
				}
				
				Collections.sort(flatParcoursNetworksNoLoops, new GraphComparatorByArcCount<String>());
				
				// Make phylogenetic tree
				Graph<Set<Graph<String>>> tree = GraphUtils.createPhylogeneticTree(flatParcoursNetworksNoLoops);
				tree.setLabel(networkTitle+"_Tree");
				for (Node<Set<Graph<String>>> node : tree.getNodes()){
					node.setLabel(node.getLabel().replaceAll(networkTitle+" ",""));
					if (node.getReferent().size()==1){
						node.setAttribute("TYPE", "1");
						String[] splitLabel = Trafo.noParentheses(node.getReferent().toString()).split(" ");
						Integer egoId = Integer.parseInt(splitLabel[splitLabel.length-1]);
						node.setAttribute("GENDER", ((EgoSequences)sequences).getByEgoId(egoId).getEgo().getGender().toString());
					} else {
						node.setAttribute("TYPE", "0");
					}
				}
				
				List<String> treePartitionLabels = new ArrayList<String>();
				treePartitionLabels.add("TYPE");
				treePartitionLabels.add("GENDER");
				
				pajekBuffers.get(networkTitle.replaceAll("Network", "Similarity Tree")).addAll(PuckUtils.writePajekNetwork(tree,treePartitionLabels)); 		

				// Make union graphs
				List<Graph<Cluster<Relation>>> unions = new ArrayList<Graph<Cluster<Relation>>>();
				PartitionCriteria partitionCriteria = new PartitionCriteria(criteria.getPartitionLabel());
				Partition<Individual> individualPartition = PartitionMaker.create("", ((EgoSequences)sequences).egos(), partitionCriteria);
				Partition<Graph<Cluster<Relation>>> graphPartition = new Partition<Graph<Cluster<Relation>>>();
				
				for (Individual ego : individualPartition.getItems()){
					graphPartition.put(parcoursNetworks.get(ego.getId()), individualPartition.getValue(ego));
				}
				
				parcoursNetworkStatistics = new TreeMap<String,Map<String,Map<String,Value>>>();
				
				for (Cluster<Graph<Cluster<Relation>>> graphCluster : graphPartition.getClusters()){
					Graph<Cluster<Relation>> union = GraphUtils.fuseGraphs(graphCluster.getItems());
					union.setLabel(networkTitle+"_"+graphCluster.getValue());
					unions.add(union);
					parcoursNetworkStatistics.put(graphCluster.getValue()+"",GraphUtils.getNodeStatisticsByLabel(union, criteria.getNodeStatisticsLabels()));
				}
				Graph<Cluster<Relation>> totalUnion = GraphUtils.fuseGraphs(new ArrayList<Graph<Cluster<Relation>>>(parcoursNetworks.values()));
				totalUnion.setLabel(networkTitle+"_Total");
				parcoursNetworkStatistics.put("Total",GraphUtils.getNodeStatisticsByLabel(totalUnion, criteria.getNodeStatisticsLabels()));
				unions.add(totalUnion);
				
				List<String> unionPartitionLabels = new ArrayList<String>();
				unionPartitionLabels.add("NUMBER");
				unionPartitionLabels.add("SIZE");
				unionPartitionLabels.add("BETWEENNESS");
				unionPartitionLabels.add("DEGREE");

				for (Graph<Cluster<Relation>> union : unions){
					pajekBuffers.get(networkTitle.replaceAll("Network", "Network Fused")).addAll(PuckUtils.writePajekNetwork(union,unionPartitionLabels)); 		
				}
			}
		}
		
		if (criteria.getSequenceValueCriteriaList().getLabels().contains("CONNECTED_NETWORK_RELATIONS")){

			Map<Individual,List<String>> singles = new TreeMap<Individual,List<String>>();
			Map<Individual,List<String[]>> pairs  = new TreeMap<Individual,List<String[]>>();
			
			for (S sequence : sequences){
				
				Individual ego = ((EgoSequence)sequence).getEgo();
				Value singlesValue = getValue(sequence,"NETWORK_RELATIONS");
//				Value singlesValue = valuesMap.get("NETWORK_RELATIONS").get(ego.getId());
				
				if (singlesValue != null){
					singles.put(ego, (List<String>)singlesValue.listValue());
				}
								
				Value pairsValue = getValue(sequence,"CONNECTED_NETWORK_RELATIONS");
//				Value pairsValue = valuesMap.get("CONNECTED_NETWORK_RELATIONS").get(ego.getId());
				
				if (pairsValue != null){
					pairs.put(ego, (List<String[]>)pairsValue.listValue());
				}
			}
			
			relationConnectionMatrix = new SequenceNetworkStatistics("Component connections",null,singles,pairs);
			
		}
			
		}

		for (String networkTitle : criteria.getNetworkTitles()){
			if (networkTitle.contains("Parcours Similarity Network")){
				
				for (RelationClassificationType relationClassificationType : linkPartitions.keySet()){
					Partition<Link<Individual>> linkPartition = linkPartitions.get(relationClassificationType);
					Map<Value,Double[]> similaritiesMap = similaritiesMaps.get(relationClassificationType);
					
					for (Value linkValue : linkPartition.getValues()){
						
						Double[] values = new Double[]{0.,0.,0.,0.,0.};
						Double[] sums = new Double[]{0.,0.,0.,0.,0.};

						for (Link<Individual> link : linkPartition.getCluster(linkValue).getItems()){
							Individual ego = (Individual)link.getSourceNode().getReferent();
							Individual alter = (Individual)link.getTargetNode().getReferent();
							
							int egoGender = ego.getGender().toInt();
							int alterGender = alter.getGender().toInt();
							int idx = egoGender + 2 * alterGender;
							
							values[idx] += link.getWeight();
							values[4] += link.getWeight();
							sums[idx]++;
							sums[4]++;
							
						}
						
						for (int idx=0;idx<sums.length;idx++){
							values[idx] = MathUtils.percent(values[idx], 100*sums[idx]);
						}
						
						similaritiesMap.put(linkValue, values);
					}
				}
			}*/
		}
		

/*	public NumberedValues getValues (String label){
		
		return valuesMap.get(label);
	}
	
	public Graph<Cluster<String>> getSequenceNetwork (String title, RelationClassificationType relationClassificationType, Partition<String> partition){
		Graph<Cluster<String>> result;
		
		if (sequences == null) {
			throw new IllegalArgumentException("Null parameter detected.");
		} else {
			
			result = new Graph<Cluster<String>>(title+"_"+relationClassificationType);
			
			//
			for (Cluster<String> cluster : partition.getClusters().toListSortedByDescendingSize()) {
				if (!cluster.isNull()) {
					result.addNode(cluster);
				}
			}
			
			//
			for (S sequence : sequences){
				Cluster<String> previous = null;
				for (Relation event : ((EgoSequence)sequence).getStations().values()){
					Cluster<String> next = partition.getCluster(((EgoSequence)sequence).getEgo().getId()+" "+event.getTypedId());
					if (previous!=null){
						result.incArcWeight(previous, next);
					}
					previous = next;
				}
			}
			
			for (Node<Cluster<String>> node : result.getNodes()){
				Cluster<String> referent = node.getReferent();
				if (referent !=null){
					Value clusterValue = referent.getValue();
					if (clusterValue!=null){
						String value = clusterValue.toString();
						if (value.lastIndexOf("-")>-1){
							value = value.substring(value.lastIndexOf("-")+1);
						}
						node.setAttribute(relationClassificationType.toString(), value);
					}
				}
			}
		}
		
		//
		return result;
	}




	public Graph<Cluster<String>> getEventTypeNetwork(String eventTypeName) {
		return eventTypeNetworks.get(eventTypeName);
	}




	public Graph<Cluster<String>> getSequenceTypeNetwork(String eventTypeName) {
		return sequenceTypeNetworks.get(eventTypeName);
	}




	public Partition<String> getEventPartition(String eventTypeName) {
		return eventPartitions.get(eventTypeName);
	}
	
	public Integer getNrValues (RelationClassificationType relationClassificationType, String label){
		Integer result;
		
		if (relationClassificationType==null || label==null || nrValues.get(relationClassificationType)==null){
			result = null;
		} else {
			result = nrValues.get(relationClassificationType).get(label);
		}
		
		return result;
	}




	public Partition<String> getSubSequencePartition(String eventTypeName) {
		return subSequencePartitions.get(eventTypeName);
	}
	
	public Set<Relation> events(){
		Set<Relation> result;
		
		result = new HashSet<Relation>();
		
		for (EgoSequence sequence : sequences){
			for (Relation event : sequence.getStations().values()){
				result.add(event);
			}
		}
		//
		return result;
		
	}
	
	public int nrEvents(){
		return events().size();
	}
	
	public int nrSequences(){
		return sequences.size();
	}


	public Partition<String> getEventPairPartition(String eventTypeName) {
		return eventPairPartitions.get(eventTypeName);
	}


	public Map<Value, Double[]> getSimilaritiesMap(RelationClassificationType relationClassificationType) {
		return similaritiesMaps.get(relationClassificationType);
	}

	public Partition<Node<Cluster<String>>>[] getDepthPartition(String eventTypeName) {
		return depthPartitions.get(eventTypeName);
	}

	public SequenceNetworkStatistics<S,E> getRelationConnectionMatrix() {
		return relationConnectionMatrix;
	}


	public Map<String, Map<String, Map<String, Value>>> getParcoursNetworkStatistics() {
		return parcoursNetworkStatistics;
	}*/


	

}
