package org.tip.puck.cli;

import org.apache.commons.cli.CommandLine;

abstract public class Command {
    abstract public boolean run(CommandLine cline);
    
    protected String errorMessage;
    
    public String getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
