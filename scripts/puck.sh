#!/bin/sh

# Set the current directory with the Puck directory.
cd "`dirname "$0"`"

#
echo "Information about default installed Java:"
java -version

# Try to launch Puck with Java 6.
java -Xms368m -Xmx1024m -version:1.6* -jar puck.jar $@

# If failed, try with the default installed Java.
if [ $? -ne 0 ] ; then
	#
	echo "Launching with the default installed Java."
	java -Xms368m -Xmx1024m -jar puck.jar $@
fi
