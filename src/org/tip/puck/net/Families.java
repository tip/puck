package org.tip.puck.net;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.FamilyComparator.Sorting;
import org.tip.puck.util.NumberablesHashMap;

import fr.devinsy.util.StringList;

/**
 * The <code>Families</code> class represents a family collection.
 * 
 * @author TIP
 */
public class Families extends NumberablesHashMap<Family> {

	/**
	 * 
	 */
	public Families() {
		super();
	}

	/**
	 * 
	 */
	public Families(final Families source) {
		super();

		add(source);
	}

	/**
	 * 
	 */
	public Families(final int capacity) {
		super(capacity);
	}

	/**
	 * 
	 * @param source
	 */
	public Families(final List<Family> source) {
		super();

		add(source);
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public Families add(final Families source) {
		Families result;

		result = put(source);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public Families add(final List<Family> source) {
		Families result;

		result = put(source);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individuals children() {
		Individuals result;

		result = new Individuals();
		for (Family family : this) {
			result.put(family.getChildren());
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean containsChild() {
		boolean result;

		boolean ended = false;
		result = false;
		Iterator<Family> iterator = this.iterator();
		while (!ended) {
			if (iterator.hasNext()) {
				Family family = iterator.next();
				if (family.isFertile()) {
					ended = true;
					result = true;
				}
			} else {
				ended = true;
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public StringList getAttributeLabels() {
		StringList result;

		//
		result = new StringList();

		//
		for (Family family : this) {
			for (String label : family.attributes().labels()) {
				if (!result.contains(label)) {
					result.add(label);
				}
			}
		}

		//
		return result;
	}

	/**
	 * @param parent1
	 * @param parent2
	 * @return
	 */
	public Family getBySpouses(final Individual parent1, final Individual parent2) {
		Family result;

		if ((parent1 == null) || (parent2 == null)) {
			result = null;
		} else {

			boolean ended = false;
			result = null;
			Iterator<Family> iterator = this.iterator();
			while (!ended) {
				if (iterator.hasNext()) {
					Family family = iterator.next();

					if (((family.getHusband() == parent1) && (family.getWife() == parent2))
							|| ((family.getHusband() == parent2) && (family.getWife() == parent1))) {
						ended = true;
						result = family;
					}
				} else {
					ended = true;
					result = null;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetSpouse1
	 * @param targetSpouse2
	 * @return
	 */
	public Family getBySpouses(final int targetSpouse1, final int targetSpouse2) {
		Family result;

		boolean ended = false;
		result = null;
		Iterator<Family> iterator = this.iterator();
		while (!ended) {
			if (iterator.hasNext()) {
				//
				Family family = iterator.next();

				//
				int parentId1;
				if (family.getHusband() == null) {
					parentId1 = 0;
				} else {
					parentId1 = family.getHusband().getId();
				}

				//
				int parentId2;
				if (family.getWife() == null) {
					parentId2 = 0;
				} else {
					parentId2 = family.getWife().getId();
				}

				//
				if (((targetSpouse1 == parentId1) && (targetSpouse2 == parentId2)) || ((targetSpouse1 == parentId2) && (targetSpouse2 == parentId1))) {
					ended = true;
					result = family;
				}
			} else {
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * Gets families with married attribute set to true.
	 * 
	 * @return
	 */
	public Families getMarried() {
		Families result;

		result = new Families();

		for (Family family : this) {
			//
			if (family.hasMarried()) {
				result.put(family);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param alter
	 * @return
	 */
	public Individuals getPartners(final Individual alter) {
		Individuals result;

		result = new Individuals();

		if (alter != null) {
			for (Family family : this) {
				//
				Individual ego = family.getOtherParent(alter);

				//
				if (ego != null) {
					result.add(ego);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param spouse
	 * @return
	 */
	public Individuals marriedSpouses(final Individual alter) {
		Individuals result;

		result = new Individuals();

		if (alter != null) {
			for (Family family : this) {
				//
				Individual ego = family.getOtherParent(alter);

				//
				if ((ego != null) && (family.hasMarried())) {
					result.add(ego);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public Families put(final Families source) {
		Families result;

		//
		if (source != null) {
			for (Family family : source) {
				this.add(family);
			}
		}

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public Families put(final List<Family> source) {
		Families result;

		//
		if (source != null) {
			for (Family family : source) {
				this.add(family);
			}
		}

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param pattern
	 * @return
	 */
	public Families searchByName(final String pattern) {
		Families result;

		//
		result = new Families();

		//
		if (StringUtils.isNotBlank(pattern)) {
			//
			for (Family family : this) {
				//
				String targetPattern = pattern.toLowerCase();

				//
				if (((family.getHusband() != null) && (family.getHusband().getName() != null) && (family.getHusband().getName().toLowerCase()
						.contains(targetPattern)))
						|| ((family.getWife() != null) && (family.getWife().getName() != null) && (family.getWife().getName().toLowerCase()
								.contains(targetPattern)))) {
					//
					result.add(family);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param pattern
	 * @return
	 */
	public Families singleParentFamilies() {
		Families result;

		result = new Families();
		for (Family family : this) {
			if (family.isSingleParent()) {
				result.add(family);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Family> toListSortedByOrder(final Individual parent) {
		List<Family> result;

		result = toList();
		Collections.sort(result, new FamilyOrderComparator(parent));

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Family> toSortedList(final Sorting sorting) {
		List<Family> result;

		result = toList();
		Collections.sort(result, new FamilyComparator(sorting));

		//
		return result;
	}
}
