package org.tip.puck.report;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.LogarithmicAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.Range;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.report.ReportChart.GraphType;
import org.tip.puck.report.ReportChart.LogarithmType;

import fr.devinsy.otridi.BarBoardChart;
import fr.devinsy.otridi.SurfaceChart;

/**
 * 
 * @author TIP
 */
public class ReportChartMaker {

	private static final Logger logger = LoggerFactory.getLogger(ReportChartMaker.class);

	/**
	 * 
	 * @param reportChart
	 * @return
	 */
	public static BufferedImage createBufferedImage(final ReportChart reportChart, final int width, final int height) {
		BufferedImage result;

		result = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

		if (reportChart.getType() == GraphType.SURFACE) {
			//
			double[][] matrix = new double[reportChart.getRowCount()][reportChart.getColumnCount()];
			for (int rowIndex = 0; rowIndex < reportChart.getRowCount(); rowIndex++) {
				//
				for (int columnIndex = 0; columnIndex < reportChart.getColumnCount(rowIndex); columnIndex++) {
					//
					Double value = reportChart.getYValue(rowIndex, columnIndex);
					if (value == null) {
						matrix[rowIndex][columnIndex] = 0.0;
					} else {
						matrix[rowIndex][columnIndex] = value;
					}
					System.out.println(rowIndex + " " + columnIndex + " " + value);
				}
			}

			//
			SurfaceChart.draw(result.getGraphics(), matrix, width, height);
		} else if (reportChart.getType() == GraphType.BAR_BOARD) {
			//
			double[][] matrix = new double[reportChart.getRowCount()][reportChart.getColumnCount()];
			for (int rowIndex = 0; rowIndex < reportChart.getRowCount(); rowIndex++) {
				//
				for (int columnIndex = 0; columnIndex < reportChart.getColumnCount(rowIndex); columnIndex++) {
					//
					Double value = reportChart.getYValue(rowIndex, columnIndex);
					if (value == null) {
						matrix[rowIndex][columnIndex] = 0.0;
					} else {
						matrix[rowIndex][columnIndex] = value;
					}
					System.out.println(rowIndex + " " + columnIndex + " " + value);
				}
			}

			//
			BarBoardChart.draw(result.getGraphics(), matrix, width, height);
		} else {
			JFreeChart chart = createJFreeChart(reportChart);
			chart.draw((Graphics2D) result.getGraphics(), new Rectangle2D.Double(0, 0, width, height));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param reportChart
	 * @return
	 */
	public static JFreeChart createJFreeChart(final ReportChart reportChart) {
		JFreeChart result;

		switch (reportChart.getType()) {
			case STACKED_BARS: {
				// Build dataset for JFreeChart.
				DefaultCategoryDataset dataset = new DefaultCategoryDataset();
				for (int rowIndex = 0; rowIndex < reportChart.getRowCount(); rowIndex++) {
					for (int columnIndex = 0; columnIndex < reportChart.getColumnCount(); columnIndex++) {
						//
						String category = reportChart.getLineTitle(rowIndex);
						if (category == null) {
							category = reportChart.getHeader(columnIndex);
						}

						Double value = reportChart.getYValue(rowIndex, columnIndex);
						// logger.debug("[row=" + rowIndex + "][category=" +
						// category + "][col=" + columnIndex + "][header=" +
						// reportChart.getHeader(columnIndex)
						// + "][value=" + value + "]");

						dataset.addValue(value, category, reportChart.getHeader(columnIndex));
					}
				}

				//
				boolean showLegend;
				if (reportChart.getColumnCount() > 30) {
					showLegend = false;
				} else {
					showLegend = true;
				}

				// Build JFreeChart.
				result = ChartFactory.createStackedBarChart(null, reportChart.getHeadersLegend(), reportChart.getLinesLegend(), dataset,
						PlotOrientation.VERTICAL, showLegend, true, false);
				result.setBackgroundPaint(new Color(237, 236, 235));

				// Set visible bar value.
				CategoryPlot plot = (CategoryPlot) result.getPlot();
				plot.setBackgroundPaint(Color.WHITE);
				plot.setDomainGridlinePaint(Color.LIGHT_GRAY);

				CategoryItemRenderer renderer = plot.getRenderer();
				renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
				renderer.setBaseItemLabelsVisible(true);

				BarRenderer barRenderer = (BarRenderer) plot.getRenderer();
				barRenderer.setBarPainter(new StandardBarPainter());
				barRenderer.setDrawBarOutline(true);
				barRenderer.setShadowVisible(false);
			}
			break;

			case LINES: {
				// Build dataset for JFreeChart.
				XYDataset dataset;
				dataset = new XYSeriesCollection();
				for (int rowIndex = 0; rowIndex < reportChart.getRowCount(); rowIndex++) {
					//
					XYSeries series = new XYSeries(reportChart.getLineTitle(rowIndex));
					for (int columnIndex = 0; columnIndex < reportChart.getColumnCount(rowIndex); columnIndex++) {
						//
						Double x = reportChart.getXValue(rowIndex, columnIndex);
						if (x == null) {
							x = Double.valueOf(columnIndex + 1);
						}

						// Logarithm chart needs positive values.
						if ((reportChart.getLogarithmType() == LogarithmType.NONE) || (x > 0)) {
							series.add(x, reportChart.getYValue(rowIndex, columnIndex));
						}

					}
					((XYSeriesCollection) dataset).addSeries(series);
				}

				// Build JFreeChart.
				result = ChartFactory.createXYLineChart(null, reportChart.getHeadersLegend(), reportChart.getLinesLegend(), dataset, PlotOrientation.VERTICAL,
						true, true, false);
				result.setBackgroundPaint(new Color(237, 236, 235));

				// Decor.
				XYPlot plot = result.getXYPlot();
				plot.setBackgroundPaint(Color.WHITE);
				switch (reportChart.getLogarithmType()) {
					case NONE: {
						plot.setDomainGridlinePaint(Color.LIGHT_GRAY);

						NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
						rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

						//
						if (reportChart.getVerticalMax() != null) {
							Range rangeRange = rangeAxis.getRange();
							if (rangeRange.getUpperBound() > reportChart.getVerticalMax()) {
								rangeAxis.setRange(rangeRange.getLowerBound(), reportChart.getVerticalMax());
							}
						}

						//
						if (reportChart.isIntegerHorizontalUnit()) {
							NumberAxis domainAxis = (NumberAxis) plot.getDomainAxis();
							domainAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
						}

						plot.setRangeGridlinePaint(Color.LIGHT_GRAY);
					}
					break;

					case HORIZONTAL: {
						LogarithmicAxis domainAxis = new LogarithmicAxis(reportChart.getHeadersLegend());
						plot.setDomainAxis(domainAxis);
						plot.setDomainGridlinePaint(Color.LIGHT_GRAY);
						domainAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

						plot.setRangeGridlinePaint(Color.LIGHT_GRAY);
					}
					break;

					case VERTICAL: {
						plot.setDomainGridlinePaint(Color.LIGHT_GRAY);

						LogarithmicAxis rangeAxis = new LogarithmicAxis(reportChart.getLinesLegend());
						plot.setRangeAxis(rangeAxis);
						plot.setRangeGridlinePaint(Color.LIGHT_GRAY);
						rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
					}
					break;
				}
			}
			break;

			case SCATTER: {
				// Build dataset for JFreeChart.
				XYDataset dataset;
				dataset = new XYSeriesCollection();
				for (int rowIndex = 0; rowIndex < reportChart.getRowCount(); rowIndex++) {
					XYSeries series = new XYSeries(reportChart.getLineTitle(rowIndex));
					for (int columnIndex = 0; columnIndex < reportChart.getColumnCount(rowIndex); columnIndex++) {
						Double x = reportChart.getXValue(rowIndex, columnIndex);
						if (x == null) {
							x = Double.valueOf(columnIndex + 1);
						}
						series.add(x, reportChart.getYValue(rowIndex, columnIndex));
					}
					((XYSeriesCollection) dataset).addSeries(series);
				}

				// Build JFreeChart.
				result = ChartFactory.createScatterPlot(null, reportChart.getHeadersLegend(), reportChart.getLinesLegend(), dataset, PlotOrientation.VERTICAL,
						true, true, false);
				result.setBackgroundPaint(new Color(237, 236, 235));

				// Decor.
				XYPlot plot = result.getXYPlot();
				plot.setBackgroundPaint(Color.WHITE);
				plot.setDomainGridlinePaint(Color.LIGHT_GRAY);
				NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
				rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
				plot.setRangeGridlinePaint(Color.LIGHT_GRAY);
			}
			break;
			default:
				result = null;
		}

		//
		return result;
	}
}
