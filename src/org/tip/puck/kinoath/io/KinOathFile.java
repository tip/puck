package org.tip.puck.kinoath.io;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.batik.transcoder.Transcoder;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.XMLAbstractTranscoder;
import org.apache.batik.transcoder.image.ImageTranscoder;
import org.apache.commons.io.IOUtils;
import org.apache.fop.svg.PDFTranscoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.kinoath.KinOathDiagram;

/**
 * This class represents a KinOath File writer.
 * 
 * @author TIP
 */
public class KinOathFile {
	private static final Logger logger = LoggerFactory.getLogger(KinOathFile.class);

	/**
	 * Saves a KinOath diagram in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File file, final KinOathDiagram source, final String generator) throws PuckException {
		//
		if (file.getName().endsWith(".svg")) {
			//
			saveSVG(file, source);

		} else if (file.getName().endsWith(".pdf")) {
			//
			savePDF(file, source);
		}
	}

	/**
	 * Saves a KinOath diagram in a PDF file.
	 * 
	 * @param file
	 * @param source
	 */
	public static void savePDF(final File file, final KinOathDiagram source) {

		BufferedOutputStream outputStream = null;
		try {
			//
			Transcoder transcoder = new PDFTranscoder();
			transcoder.addTranscodingHint(PDFTranscoder.KEY_STROKE_TEXT, Boolean.FALSE);
			transcoder.addTranscodingHint(ImageTranscoder.KEY_BACKGROUND_COLOR, java.awt.Color.WHITE);
			transcoder.addTranscodingHint(XMLAbstractTranscoder.KEY_XML_PARSER_VALIDATING, Boolean.FALSE);
			transcoder.addTranscodingHint(ImageTranscoder.KEY_PIXEL_UNIT_TO_MILLIMETER, new Float(4));

			TranscoderInput transcoderInput = new TranscoderInput(source.getDocument());
			outputStream = new BufferedOutputStream(new FileOutputStream(file));

			TranscoderOutput transcoderOutput = new TranscoderOutput(outputStream);
			transcoder.transcode(transcoderInput, transcoderOutput);
		} catch (FileNotFoundException exception) {
			//
			exception.printStackTrace();

		} catch (TranscoderException exception) {
			//
			exception.printStackTrace();

		} finally {
			//
			IOUtils.closeQuietly(outputStream);
		}
	}

	/**
	 * Saves a KinOath diagram in a SVG file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void saveSVG(final File file, final KinOathDiagram source) throws PuckException {
		PrintWriter out = null;

		FileOutputStream fileOutputStream = null;
		try {
			fileOutputStream = new FileOutputStream(file);
			StreamResult xmlOutput = new StreamResult(fileOutputStream);

			DOMSource dOMSource = new DOMSource(source.getDocument());

			// configure transformer
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			transformer.transform(dOMSource, xmlOutput);

		} catch (FileNotFoundException exception) {
			//
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");

		} catch (TransformerConfigurationException exception) {
			//
			exception.printStackTrace();

		} catch (TransformerFactoryConfigurationError exception) {
			//
			exception.printStackTrace();

		} catch (TransformerException exception) {
			//
			exception.printStackTrace();

		} finally {
			//
			IOUtils.closeQuietly(out);
		}
	}
}
