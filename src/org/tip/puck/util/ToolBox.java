package org.tip.puck.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 * 
 */
public class ToolBox {
	/**
	 * @author christian.momon@devinsy.fr (copied from fr.devinsy.util.ToolBox
	 *         under LGPL).
	 * 
	 * @param file
	 *            Source.
	 * 
	 * @return Extension value or null.
	 */
	public static File addToName(final File file, final String addition) {
		File result;

		if (file == null) {
			result = null;
		} else if (addition == null) {
			result = file;
		} else {
			//
			String sourceFileName = file.getAbsolutePath();
			int separatorIndex = sourceFileName.lastIndexOf('.');

			//
			String targetFileName;
			if (separatorIndex > 0) {
				targetFileName = sourceFileName.substring(0, separatorIndex) + addition + sourceFileName.substring(separatorIndex);
			} else {
				targetFileName = sourceFileName + addition;
			}

			//
			result = new File(targetFileName);
		}

		//
		return result;
	}

	public static String asString(final Object source) {
		String result;

		if (source == null) {
			result = null;
		} else {
			result = source.toString();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @param count
	 * @return
	 */
	public static String buildReadablePercentage(final int value, final int count) {
		String result;

		double percentage;
		if (count == 0) {
			percentage = 100;
		} else {
			percentage = value * 100.0 / count;
		}

		if ((percentage > 0) && (percentage < 1)) {
			result = "<1%";
		} else if ((percentage > 99) && (percentage < 100)) {
			result = ">99%";
		} else {
			result = String.format("%d%%", Math.round(percentage));
		}

		// logger.debug("{} / {} => {} ==> {}", value, count, percentage,
		// result);

		//
		return result;
	}

	public static String clean(final String source) {
		String result;

		result = source.replaceAll("[^\\w ]", " ");

		//
		return result;
	}

	/**
	 * Get the extension of a file.
	 * 
	 * @author christian.momon@devinsy.fr (copied from fr.devinsy.util.ToolBox
	 *         under LGPL).
	 * 
	 * @param file
	 *            Source.
	 * 
	 * @return Extension value or null.
	 */
	public static String getExtension(final File file) {
		String result;

		if (file == null) {
			result = null;
		} else {
			int separatorIndex = file.getName().lastIndexOf('.');
			if (separatorIndex > 0) {
				result = file.getName().substring(separatorIndex + 1).toLowerCase();
			} else {
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * @author christian.momon@devinsy.fr (copied from fr.devinsy.util.ToolBox
	 *         under LGPL).
	 * 
	 * 
	 * @param pattern
	 * @param source
	 * @return
	 */
	public static int indexOf(final String pattern, final List<String> source) {
		int result;

		if (source == null) {
			result = -1;
		} else {
			boolean ended = false;
			result = -1;
			int currentIndex = 0;
			while (!ended) {
				if (currentIndex < source.size()) {
					String sourceString = source.get(currentIndex);
					if (StringUtils.equals(sourceString, pattern)) {
						ended = true;
						result = currentIndex;
					} else {
						currentIndex += 1;
					}
				} else {
					ended = true;
					currentIndex = -1;
				}
			}
		}

		//
		return result;
	}

	/**
	 * @author christian.momon@devinsy.fr (copied from fr.devinsy.util.ToolBox
	 *         under LGPL).
	 * 
	 * @param file
	 * @throws PuckException
	 */
	public static void save(final File file, final String string) throws PuckException {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));

			out.println(string);

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * @author christian.momon@devinsy.fr (copied from fr.devinsy.util.ToolBox
	 *         under LGPL).
	 * 
	 * @param source
	 * @param extension
	 * @return
	 */
	public static File setExtension(final File source, final String extension) {
		File result;

		if ((source == null) || (extension == null)) {
			result = source;
		} else {
			String sourceFileName = source.getAbsolutePath();
			int separatorIndex = sourceFileName.lastIndexOf('.');

			//
			String targetFileName;
			if (separatorIndex > 0) {
				targetFileName = sourceFileName.substring(0, separatorIndex) + extension;
			} else {
				targetFileName = sourceFileName + extension;
			}

			//
			result = new File(targetFileName);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static Double[] sort(final Set<Double> source) {
		Double[] result;

		if (source == null) {
			result = null;
		} else {
			result = new Double[source.size()];

			source.toArray(result);
			Arrays.sort(result);
		}

		//
		return result;
	}

	public static String[] splitLastPart(final String label, final String separator) {
		String[] result;

		result = new String[2];

		int idx = label.lastIndexOf(separator);

		if (idx > -1) {

			result[0] = label.substring(0, idx);
			result[1] = label.substring(idx + 1);

		} else {

			result[0] = label;
			result[1] = null;
		}

		/*		if (label.contains("_")){
					String[] parts = label.split("_");
					if (parts.length>1){
						result = parts[parts.length-1];
					}
				}*/

		//
		return result;
	}

	/**
	 * Extracts value from a string to fill a int array. Value are separated by
	 * space, tabulation or comma.
	 * 
	 * @param source
	 * @return
	 */
	public static double[] stringsToDoubles(final String source) {
		double[] result;

		if (StringUtils.isBlank(source)) {
			result = new double[0];
		} else {
			String[] degreesString = source.split("[ \\t,]");
			List<Double> doubles = new ArrayList<Double>(degreesString.length);
			for (String string : degreesString) {
				if ((StringUtils.isNotBlank(string)) && (NumberUtils.isNumber(string))) {
					doubles.add(Double.valueOf(string));
				}
			}
			result = new double[doubles.size()];
			for (int index = 0; index < result.length; index++) {
				result[index] = doubles.get(index);
			}
		}

		//
		return result;
	}

	/**
	 * Extracts value from a string to fill a int array. Value are separated by
	 * space, tabulation or comma.
	 * 
	 * @param source
	 * @return
	 */
	public static int[] stringsToInts(final String source) {
		int[] result;

		if (StringUtils.isBlank(source)) {
			result = new int[0];
		} else {
			String[] degreesString = source.split("[ \\t,]");
			List<Integer> integers = new ArrayList<Integer>(degreesString.length);
			for (String string : degreesString) {
				if ((StringUtils.isNotBlank(string)) && (NumberUtils.isNumber(string))) {
					integers.add(Integer.valueOf(string));
				}
			}
			result = new int[integers.size()];
			for (int index = 0; index < result.length; index++) {
				result[index] = integers.get(index);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 *            all lines have the same length
	 * @return
	 */
	public static double[][] toArray(final List<double[]> source) {
		double[][] result;

		if (source == null) {
			result = null;
		} else if (source.size() == 0) {
			result = new double[0][0];
		} else {

			int rowCount = source.size();
			int columnCount = source.get(0).length;

			result = new double[rowCount][columnCount];

			for (int rowIndex = 0; rowIndex < rowCount; rowIndex++) {
				for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
					result[rowIndex][columnIndex] = source.get(rowIndex)[columnIndex];
				}
			}
		}

		//
		return result;
	}

	public static <K extends Comparable<K>, V> String toKeySetString(final Map<K, V> map) {
		String result;

		result = "";

		List<K> keyList = new ArrayList<K>(map.keySet());
		Collections.sort(keyList);

		for (K key : keyList) {
			result += key + " ";
		}
		//
		return result;
	}

	public static <E> String toLine(final ArrayList<E> list, final String header, final String starter, final String separator) {
		String result;

		result = header;
		for (E item : list) {
			if (result.equals(header)) {
				result += starter + item;
			} else {
				result += separator + item;
			}
		}
		//
		return result;
	}

	/**
	 * Concatenates int values from an array, adding decoration strings.
	 * 
	 * @author christian.momon@devinsy.fr (copied from fr.devinsy.util.ToolBox
	 *         under LGPL).
	 * 
	 * @param values
	 *            Source of int values.
	 * @param prefix
	 *            Decoration to put on start.
	 * @param separator
	 *            Decoration to put between values.
	 * @param postfix
	 *            Decoration to put on end.
	 * 
	 * @return A decorated string representing the int values.
	 */
	public static String toString(final int[] values, final String prefix, final String separator, final String postfix) {
		String result;

		StringList buffer = new StringList();

		//
		if (prefix != null) {
			buffer.append(prefix);
		}

		//
		boolean firstPassed = false;
		for (int value : values) {
			if (firstPassed) {
				buffer.append(separator);
			} else {
				firstPassed = true;
			}
			buffer.append(value);
		}

		//
		if (postfix != null) {
			buffer.append(postfix);
		}

		//
		result = buffer.toString();

		//
		return result;
	}

	/**
	 * @author christian.momon@devinsy.fr (copied from fr.devinsy.util.ToolBox
	 *         under LGPL).
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final String source) {
		String result;

		if (source == null) {
			result = "";
		} else {
			result = source;
		}

		//
		return result;
	}
}
