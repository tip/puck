package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.workers.AttributeDescriptor;
import org.tip.puck.net.workers.AttributeDescriptors;
import org.tip.puck.net.workers.AttributeFilter;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class AttributeFilterInputDialog extends JDialog {

	private static final long serialVersionUID = 3336300136508529977L;
	private final JPanel contentPanel = new JPanel();
	private AttributeFilter dialogCriteria;
	private static AttributeFilter lastCriteria = new AttributeFilter();
	private final ButtonGroup buttonGroupType = new ButtonGroup();
	private JComboBox cmbbxTarget;
	private JComboBox cmbbxLabel;
	private JRadioButton rdbtnClean;
	private JRadioButton rdbtnBlank;
	private JRadioButton rdbtnForcedBlank;
	private JRadioButton rdbtnAnonymizeByNumbering;
	private JRadioButton rdbtnAnonymizeByConsistentNumbering;
	private JRadioButton rdbtnRemove;
	private JRadioButton rdbtnCleanBlank;
	private JRadioButton rdbtnReduceDate;
	private JRadioButton rdbtnUpperCase;
	private JRadioButton rdbtnLowerCase;
	private JRadioButton rdbtnCapitalize;
	private JRadioButton rdbtnTrim;

	/**
	 * Create the dialog.
	 */
	public AttributeFilterInputDialog(final List<String> relationModelNames, final AttributeDescriptors attributeDescriptors) {
		super();

		//
		List<String> targetLabels = new ArrayList<String>();
		targetLabels.add("ALL");
		targetLabels.add("CORPUS");
		targetLabels.add("INDIVIDUALS");
		targetLabels.add("FAMILIES");
		targetLabels.add("RELATIONS");
		targetLabels.add("ACTORS");
		if (relationModelNames != null) {
			for (String name : relationModelNames) {
				targetLabels.add(name);
			}
		}

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Filter Exogenous Attribute");
		setIconImage(Toolkit.getDefaultToolkit().getImage(AttributeFilterInputDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				AttributeFilterInputDialog.this.dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 420, 439);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));
		{
			JLabel lblTarget = new JLabel("Target:");
			this.contentPanel.add(lblTarget, "2, 2, right, default");
		}
		{
			this.cmbbxTarget = new JComboBox(targetLabels.toArray());
			this.cmbbxTarget.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(final ItemEvent event) {
					// Update label list.
					//
					if (event.getStateChange() == ItemEvent.SELECTED) {
						//
						List<String> labels;
						switch (AttributeFilterInputDialog.this.cmbbxTarget.getSelectedIndex()) {
							case 0:
								labels = attributeDescriptors.labels();
							break;

							case 1:
								labels = attributeDescriptors.findByScope(AttributeDescriptor.Scope.CORPUS).labels();
							break;

							case 2:
								labels = attributeDescriptors.findByScope(AttributeDescriptor.Scope.INDIVIDUALS).labels();
							break;

							case 3:
								labels = attributeDescriptors.findByScope(AttributeDescriptor.Scope.FAMILIES).labels();
							break;

							case 4:
								labels = attributeDescriptors.findByScope(AttributeDescriptor.Scope.RELATION).labels();
							break;

							case 5:
								labels = attributeDescriptors.findByScope(AttributeDescriptor.Scope.ACTORS).labels();
							break;

							default:
								labels = attributeDescriptors.findByRelationModelName((String) AttributeFilterInputDialog.this.cmbbxTarget.getSelectedItem())
										.labels();
							break;
						}

						//
						Collections.sort(labels);

						//
						AttributeFilterInputDialog.this.cmbbxLabel.setModel(new DefaultComboBoxModel(labels.toArray()));
						AttributeFilterInputDialog.this.cmbbxLabel.setSelectedIndex(-1);
					}
				}
			});
			this.contentPanel.add(this.cmbbxTarget, "4, 2, fill, default");
		}
		{
			JLabel lblLabel = new JLabel("Label:");
			this.contentPanel.add(lblLabel, "2, 4, right, default");
		}
		{
			this.cmbbxLabel = new JComboBox(attributeDescriptors.labelsSorted().toArray());
			this.cmbbxLabel.setEditable(true);
			this.contentPanel.add(this.cmbbxLabel, "4, 4, fill, default");
		}
		{
			JLabel lblFilter = new JLabel("Filter:");
			this.contentPanel.add(lblFilter, "2, 6, right, default");
		}
		{
			JPanel panel = new JPanel();
			this.contentPanel.add(panel, "4, 6, fill, fill");
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			{
				this.rdbtnCleanBlank = new JRadioButton("clean blank value");
				this.buttonGroupType.add(this.rdbtnCleanBlank);
				panel.add(this.rdbtnCleanBlank);
			}
			{
				this.rdbtnClean = new JRadioButton("clean (blank and zero value)");
				this.buttonGroupType.add(this.rdbtnClean);
				panel.add(this.rdbtnClean);
			}
			{
				this.rdbtnAnonymizeByNumbering = new JRadioButton("anonymize by numbering");
				this.buttonGroupType.add(this.rdbtnAnonymizeByNumbering);
				panel.add(this.rdbtnAnonymizeByNumbering);
			}
			{
				this.rdbtnAnonymizeByConsistentNumbering = new JRadioButton("anonymize by consistent numbering");
				this.buttonGroupType.add(this.rdbtnAnonymizeByConsistentNumbering);
				panel.add(this.rdbtnAnonymizeByConsistentNumbering);
			}
			{
				this.rdbtnBlank = new JRadioButton("replace by an empty string");
				this.buttonGroupType.add(this.rdbtnBlank);
				panel.add(this.rdbtnBlank);
			}
			{
				this.rdbtnForcedBlank = new JRadioButton("set to an empty string");
				this.buttonGroupType.add(this.rdbtnForcedBlank);
				panel.add(this.rdbtnForcedBlank);
			}
			{
				this.rdbtnReduceDate = new JRadioButton("reduce date");
				this.buttonGroupType.add(this.rdbtnReduceDate);
				panel.add(this.rdbtnReduceDate);
			}
			{
				this.rdbtnLowerCase = new JRadioButton("lower case value");
				this.buttonGroupType.add(this.rdbtnLowerCase);
				panel.add(this.rdbtnLowerCase);
			}
			{
				this.rdbtnCapitalize = new JRadioButton("capitalize value");
				this.buttonGroupType.add(this.rdbtnCapitalize);
				panel.add(this.rdbtnCapitalize);
			}
			{
				this.rdbtnUpperCase = new JRadioButton("upper case value");
				this.buttonGroupType.add(this.rdbtnUpperCase);
				panel.add(this.rdbtnUpperCase);
			}
			{
				this.rdbtnTrim = new JRadioButton("trim value");
				this.buttonGroupType.add(this.rdbtnTrim);
				panel.add(this.rdbtnTrim);
			}
			{
				this.rdbtnRemove = new JRadioButton("remove");
				this.buttonGroupType.add(this.rdbtnRemove);
				panel.add(this.rdbtnRemove);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						AttributeFilterInputDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("Filter");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						AttributeFilter criteria = getCriteria();
						if (StringUtils.isBlank(criteria.getLabel())) {
							//
							String title = "Invalid input";
							String message = "Please, enter a label not empty.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						} else {
							//
							lastCriteria = criteria;
							AttributeFilterInputDialog.this.dialogCriteria = criteria;

							//
							setVisible(false);
						}

					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}

		// ////////////////////////
		setCriteria(lastCriteria);
	}

	/**
	 * 
	 * @return
	 */
	public AttributeFilter getCriteria() {
		AttributeFilter result;

		result = new AttributeFilter();

		//
		AttributeFilter.Scope scope;
		String optionalName;
		switch (this.cmbbxTarget.getSelectedIndex()) {
			case 0:
				scope = AttributeFilter.Scope.ALL;
				optionalName = null;
			break;

			case 1:
				scope = AttributeFilter.Scope.CORPUS;
				optionalName = null;
			break;

			case 2:
				scope = AttributeFilter.Scope.INDIVIDUALS;
				optionalName = null;
			break;

			case 3:
				scope = AttributeFilter.Scope.FAMILIES;
				optionalName = null;
			break;

			case 4:
				scope = AttributeFilter.Scope.RELATIONS;
				optionalName = null;
			break;

			case 5:
				scope = AttributeFilter.Scope.ACTORS;
				optionalName = null;
			break;

			default:
				scope = AttributeFilter.Scope.RELATION;
				optionalName = (String) this.cmbbxTarget.getSelectedItem();
			break;
		}
		result.setScope(scope);
		result.setOptionalRelationName(optionalName);

		//
		result.setLabel((String) this.cmbbxLabel.getSelectedItem());

		//
		if (this.rdbtnClean.isSelected()) {
			result.setMode(AttributeFilter.Mode.CLEAN);
		} else if (this.rdbtnCleanBlank.isSelected()) {
			result.setMode(AttributeFilter.Mode.CLEAN_BLANK);
		} else if (this.rdbtnAnonymizeByNumbering.isSelected()) {
			result.setMode(AttributeFilter.Mode.ANONYMIZE_BY_NUMBERING);
		} else if (this.rdbtnAnonymizeByConsistentNumbering.isSelected()) {
			result.setMode(AttributeFilter.Mode.ANONYMIZE_BY_CONSISTENT_NUMBERING);
		} else if (this.rdbtnBlank.isSelected()) {
			result.setMode(AttributeFilter.Mode.REPLACE_BY_BLANK);
		} else if (this.rdbtnForcedBlank.isSelected()) {
			result.setMode(AttributeFilter.Mode.FORCE_TO_BLANK);
		} else if (this.rdbtnTrim.isSelected()) {
			result.setMode(AttributeFilter.Mode.TRIM_VALUE);
		} else if (this.rdbtnCapitalize.isSelected()) {
			result.setMode(AttributeFilter.Mode.CAPITALIZE_VALUE);
		} else if (this.rdbtnLowerCase.isSelected()) {
			result.setMode(AttributeFilter.Mode.LOWERCASE_VALUE);
		} else if (this.rdbtnUpperCase.isSelected()) {
			result.setMode(AttributeFilter.Mode.UPPERCASE_VALUE);
		} else if (this.rdbtnReduceDate.isSelected()) {
			result.setMode(AttributeFilter.Mode.REDUCE_DATE);
		} else if (this.rdbtnRemove.isSelected()) {
			result.setMode(AttributeFilter.Mode.REMOVE);
		} else {
			result.setMode(AttributeFilter.Mode.NONE);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public AttributeFilter getDialogCriteria() {
		AttributeFilter result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final AttributeFilter source) {
		//
		if (source != null) {

			//
			switch (source.getScope()) {
				case ALL:
					this.cmbbxTarget.setSelectedIndex(0);
				break;

				case CORPUS:
					this.cmbbxTarget.setSelectedIndex(1);
				break;

				case INDIVIDUALS:
					this.cmbbxTarget.setSelectedIndex(2);
				break;

				case FAMILIES:
					this.cmbbxTarget.setSelectedIndex(3);
				break;

				case RELATIONS:
					this.cmbbxTarget.setSelectedIndex(4);
				break;

				case ACTORS:
					this.cmbbxTarget.setSelectedIndex(5);
				break;

				default:
					this.cmbbxTarget.setSelectedItem(source.getOptionalRelationName());
				break;
			}

			//
			this.cmbbxLabel.setSelectedItem(source.getLabel());

			//
			switch (source.getMode()) {
			case ANONYMIZE_BY_NUMBERING:
				unsetMode();
				this.rdbtnAnonymizeByNumbering.setSelected(true);
			break;

			case ANONYMIZE_BY_CONSISTENT_NUMBERING:
				unsetMode();
				this.rdbtnAnonymizeByConsistentNumbering.setSelected(true);
			break;

				case CAPITALIZE_VALUE:
					unsetMode();
					this.rdbtnCapitalize.setSelected(true);
				break;

				case CLEAN:
					unsetMode();
					this.rdbtnClean.setSelected(true);
				break;

				case CLEAN_BLANK:
					unsetMode();
					this.rdbtnCleanBlank.setSelected(true);
				break;

				case FORCE_TO_BLANK:
					unsetMode();
					this.rdbtnForcedBlank.setSelected(true);
				break;

				case LOWERCASE_VALUE:
					unsetMode();
					this.rdbtnLowerCase.setSelected(true);
				break;

				case REDUCE_DATE:
					unsetMode();
					this.rdbtnReduceDate.setSelected(false);
				break;

				case REMOVE:
					unsetMode();
					this.rdbtnForcedBlank.setSelected(false);
				break;

				case REPLACE_BY_BLANK:
					unsetMode();
					this.rdbtnBlank.setSelected(true);
				break;

				case TRIM_VALUE:
					unsetMode();
					this.rdbtnTrim.setSelected(true);
				break;

				case UPPERCASE_VALUE:
					unsetMode();
					this.rdbtnUpperCase.setSelected(true);
				break;

				default:
					unsetMode();
			}
		}
	}

	/**
	 * 
	 */
	private void unsetMode() {
		this.rdbtnClean.setSelected(false);
		this.rdbtnCleanBlank.setSelected(false);
		this.rdbtnAnonymizeByNumbering.setSelected(false);
		this.rdbtnAnonymizeByConsistentNumbering.setSelected(false);
		this.rdbtnBlank.setSelected(false);
		this.rdbtnForcedBlank.setSelected(false);
		this.rdbtnRemove.setSelected(false);
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		/* AttributeFilter criteria = */showDialog(null, null);
	}

	/**
	 * Launch the application.
	 */
	public static AttributeFilter showDialog(final List<String> relationModelNames, final AttributeDescriptors attributeDescriptors) {
		AttributeFilter result;

		//
		AttributeFilterInputDialog dialog = new AttributeFilterInputDialog(relationModelNames, attributeDescriptors);
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
