package org.tip.puck.net.workers;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author TIP
 */
public class AttributeType {

	public enum Scope {
		CORPUS,
		INDIVIDUALS,
		FAMILIES,
		RELATION,
		ACTORS
	}

	private Scope scope;
	private String relationModelName;

	/**
	 * 
	 */
	public AttributeType(final Scope scope) {
		//
		this.scope = scope;
		this.relationModelName = null;
	}

	/**
	 * 
	 */
	public AttributeType(final String relationModelName) {

		if (StringUtils.isBlank(relationModelName)) {
			//
			throw new IllegalArgumentException("Blank parameter relationModelName.");

		} else {
			//
			this.scope = Scope.RELATION;
			this.relationModelName = relationModelName;
		}
	}

	public String getRelationModelName() {
		return this.relationModelName;
	}

	public Scope getScope() {
		return this.scope;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isActors() {
		boolean result;

		result = isActors(this);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isCorpus() {
		boolean result;

		result = isCorpus(this);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isFamilies() {
		boolean result;

		result = isFamilies(this);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isIndividuals() {
		boolean result;

		result = isIndividuals(this);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isRelation() {
		boolean result;

		result = isRelation(this);

		//
		return result;
	}

	public void setRelationModelName(final String relationModelName) {
		this.relationModelName = relationModelName;
	}

	public void setScope(final Scope scope) {
		this.scope = scope;
	}

	/**
	 * 
	 * @return
	 */
	public static boolean isActors(final AttributeType source) {
		boolean result;

		if ((source != null) && (source.getScope() == Scope.ACTORS)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static boolean isCanonical(final AttributeType source) {
		boolean result;

		if (source == null) {

			result = false;

		} else {

			switch (source.getScope()) {
				case CORPUS:
				case INDIVIDUALS:
				case FAMILIES:
					result = true;
				break;

				default:
					result = false;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static boolean isCorpus(final AttributeType source) {
		boolean result;

		if ((source != null) && (source.getScope() == Scope.CORPUS)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static boolean isFamilies(final AttributeType source) {
		boolean result;

		if ((source != null) && (source.getScope() == Scope.FAMILIES)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static boolean isIndividuals(final AttributeType source) {
		boolean result;

		if ((source != null) && (source.getScope() == Scope.INDIVIDUALS)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static boolean isRelation(final AttributeType source) {
		boolean result;

		if ((source != null) && (source.getScope() == Scope.RELATION)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}
}
