package org.tip.puck.io.bar;

import java.io.File;
import java.security.InvalidParameterException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.PuckManager;
import org.tip.puck.net.Net;

/**
 * This class represents a TXT File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class BARFile {

	private static final Logger logger = LoggerFactory.getLogger(BARFile.class);

	/**
	 * This method checks if a file is a BAR one.
	 * 
	 * @param file
	 *            file to check.
	 * 
	 * @return true if the file is BAR one.
	 */
	public static boolean isBAR(final File source) {
		boolean result;

		result = isBAR(source, PuckManager.DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * This method checks if a file is a BAR one.
	 * 
	 * @param file
	 *            file to check.
	 * 
	 * @return true if the file is BAR one.
	 */
	public static boolean isBAR(final File source, final String charsetName) {
		boolean result;

		//
		result = BARDetector.isBAR(source, charsetName);

		//
		return result;
	}

	/**
	 * Loads a BAR file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File source, final String charsetName) throws PuckException {
		Net result;

		//
		if (source == null) {
			//
			throw new InvalidParameterException("Null parameter");
		} else {
			//
			String fileName = source.getName().toLowerCase();

			//
			if (fileName.endsWith(".ods")) {
				//
				result = BARODSFile.load(source);

			} else if (fileName.endsWith(".txt")) {
				//
				result = BARTXTFile.load(source, charsetName);

			} else if (fileName.endsWith(".xls")) {
				//
				result = BARXLSFile.load(source);

			} else {
				//
				throw new InvalidParameterException("Unknown extension.");
			}
		}

		//
		return result;
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File target, final Net source) throws PuckException {
		//
		if (source == null) {
			//
			throw new InvalidParameterException("Null parameter");

		} else {
			//
			String fileName = target.getName().toLowerCase();

			//
			if (fileName.endsWith(".ods")) {
				//
				BARODSFile.save(target, source);

			} else if (fileName.endsWith(".txt")) {
				//
				BARTXTFile.save(target, source);

			} else if (fileName.endsWith(".xls")) {
				//
				BARXLSFile.save(target, source);

			} else {
				//
				throw PuckExceptions.UNSUPPORTED_FILE_FORMAT.create(target.getName());
			}
		}
	}
}
