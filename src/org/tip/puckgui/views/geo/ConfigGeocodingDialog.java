package org.tip.puckgui.views.geo;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.JProgressBar;
import javax.swing.JTextField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puckgui.views.geo.Geocode;
import org.tip.puck.graphs.Graph;
import org.tip.puck.io.csv.CSVFile;
import org.tip.puckgui.GroupNetGUI;
import org.tip.puckgui.NetGUI;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.io.File;

public class ConfigGeocodingDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7468130858005234778L;
	
	private static final Logger logger = LoggerFactory.getLogger(CSVFile.class);
	
	private GroupNetGUI parentGui = null;
	private NetGUI netGUI = null;
	private Graph<?> graph = null;
	private File csvFile = null;
	
	private static final int GEONAMES_CSV = 0;
	private static final int PERSONNAL_CSV = 1;
	private static final int GEONAMES_WEB = 2;
	private static final int INTEGRATED = 3;
	
	private int geoSourceChooser = GEONAMES_WEB; //Default
	
	private final JPanel contentPanel = new JPanel();
	private JTextField textGeonamesFile;
	private JTextField txtCsv;
	JButton btnOpenGeonamesFile;
	JButton btnOpenPersonnalSources;

	/**
	 * @wbp.parser.constructor
	 */
	public ConfigGeocodingDialog(NetGUI gui, Graph<?> graph) {
		
		this.graph = graph;
		this.netGUI = gui;
		
		initialize();
	}
	
	/**
	 * Create the dialog.
	 */
	public ConfigGeocodingDialog(GroupNetGUI gui) {
		
		parentGui = gui;
		
		initialize();
	}
	
	private void initialize() {
		
		setTitle("Configure Geocoding");
		setBounds(100, 100, 450, 258);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblGeocodingSources = new JLabel("Geocoding sources :");
		lblGeocodingSources.setBounds(12, 12, 151, 15);
		contentPanel.add(lblGeocodingSources);
		
		final JCheckBox chckbxGeonames = new JCheckBox("Geonames");
		chckbxGeonames.setSelected(true);
		chckbxGeonames.setToolTipText("Web connection is needed");
		chckbxGeonames.setBounds(33, 35, 129, 23);
		contentPanel.add(chckbxGeonames);
		
		final JCheckBox chckbxPersonnalSources = new JCheckBox("Personnal sources");
		
		chckbxPersonnalSources.setBounds(33, 86, 170, 23);
		contentPanel.add(chckbxPersonnalSources);
		chckbxPersonnalSources.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if( e.getStateChange() == ItemEvent.SELECTED ) {
					txtCsv.setEnabled(true);
					btnOpenPersonnalSources.setEnabled(true);
				}else{
					txtCsv.setEnabled(false);
					btnOpenPersonnalSources.setEnabled(false);
				}
			}
		});
		
		final JCheckBox chckbxGeonamesFile = new JCheckBox("Geonames file");
		chckbxGeonamesFile.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if( e.getStateChange() == ItemEvent.SELECTED ) {
					textGeonamesFile.setEnabled(true);
					btnOpenGeonamesFile.setEnabled(true);
					geoSourceChooser = GEONAMES_WEB;
				}else{
					textGeonamesFile.setEnabled(false);
					btnOpenGeonamesFile.setEnabled(false);
				}
			}
		});

		chckbxGeonamesFile.setBounds(33, 59, 140, 23);
		contentPanel.add(chckbxGeonamesFile);
		
		final JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(12, 156, 424, 14);
		progressBar.setVisible(false);
		contentPanel.add(progressBar);
		
		textGeonamesFile = new JTextField();
		textGeonamesFile.setEnabled(false);
		textGeonamesFile.setText("Geonames CSV");
		textGeonamesFile.setBounds(206, 59, 114, 21);
		contentPanel.add(textGeonamesFile);
		textGeonamesFile.setColumns(10);
		
		btnOpenGeonamesFile = new JButton("Open");
		btnOpenGeonamesFile.setEnabled(false);
		btnOpenGeonamesFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JFileChooser fc = new JFileChooser();
				
				int returnVal = fc.showOpenDialog(ConfigGeocodingDialog.this);

		        if (returnVal == JFileChooser.APPROVE_OPTION) {
		        	csvFile = fc.getSelectedFile();
		        	geoSourceChooser = GEONAMES_CSV;
		            //This is where a real application would open the file.
		            logger.debug("Opening CSV file: " + csvFile.getName() + ".");
		        } else {
		        	logger.debug("Open CSV command cancelled by user.");
		        }
		        
		        textGeonamesFile.setText(ConfigGeocodingDialog.this.csvFile.getAbsolutePath());
				
			}
		});
		btnOpenGeonamesFile.setBounds(332, 63, 72, 15);
		contentPanel.add(btnOpenGeonamesFile);
		
		txtCsv = new JTextField();
		txtCsv.setEnabled(false);
		txtCsv.setText("Your CSV file");
		txtCsv.setBounds(206, 88, 114, 21);
		contentPanel.add(txtCsv);
		txtCsv.setColumns(10);
		
		btnOpenPersonnalSources = new JButton("Open");
		btnOpenPersonnalSources.setEnabled(false);
		btnOpenPersonnalSources.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JFileChooser fc = new JFileChooser();
				
				int returnVal = fc.showOpenDialog(ConfigGeocodingDialog.this);

		        if (returnVal == JFileChooser.APPROVE_OPTION) {
		        	csvFile = fc.getSelectedFile();
		        	geoSourceChooser = PERSONNAL_CSV;
		            //This is where a real application would open the file.
		            logger.debug("Opening CSV file: " + csvFile.getName() + ".");
		        } else {
		        	logger.debug("Open CSV command cancelled by user.");
		        }
		        
		        txtCsv.setText(ConfigGeocodingDialog.this.csvFile.getAbsolutePath());
				
			}
		});
		btnOpenPersonnalSources.setBounds(332, 90, 72, 15);
		contentPanel.add(btnOpenPersonnalSources);
		
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBounds(12, 182, 424, 35);
			contentPanel.add(buttonPane);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			{
				JButton geocodeButton = new JButton("Geocode");
				geocodeButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						//Launch geocoding
						progressBar.setVisible(true);
						progressBar.setEnabled(true);
						
						Geocode geocodingTask = null;
						if( parentGui != null & geoSourceChooser == GEONAMES_WEB )
							geocodingTask = new Geocode(ConfigGeocodingDialog.this, progressBar, "geonames", parentGui);
						if( graph != null & geoSourceChooser == GEONAMES_WEB )
							geocodingTask = new Geocode(ConfigGeocodingDialog.this, progressBar, "geonames", netGUI, graph);
						
						if( csvFile != null & geoSourceChooser == GEONAMES_CSV )
							geocodingTask = new  Geocode(ConfigGeocodingDialog.this, progressBar, "geonamesFile", netGUI, csvFile, graph);
						
						if( csvFile != null & geoSourceChooser == PERSONNAL_CSV )
							geocodingTask = new  Geocode(ConfigGeocodingDialog.this, progressBar, "personnalSources", netGUI, csvFile, graph);

						if( graph != null & geoSourceChooser == INTEGRATED )
							geocodingTask = new Geocode(ConfigGeocodingDialog.this, progressBar, "integrated", netGUI, graph);
						

						//use SwingWorker
						geocodingTask.execute();
	
					}
				});
				geocodeButton.setActionCommand("OK");
				buttonPane.add(geocodeButton);
				getRootPane().setDefaultButton(geocodeButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						ConfigGeocodingDialog.this.dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		
		JCheckBox chckbxIntegratedGeography = new JCheckBox("Integrated geography");
		chckbxGeonamesFile.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if( e.getStateChange() == ItemEvent.SELECTED ) {
					geoSourceChooser = INTEGRATED;
				}
			}
		});
		chckbxIntegratedGeography.setBounds(33, 112, 97, 23);
		contentPanel.add(chckbxIntegratedGeography);
	}
}