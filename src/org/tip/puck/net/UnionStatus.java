package org.tip.puck.net;

/**
 * Note: to avoid bad comparison, a hard parameter is setted for each enum
 * entry.
 * 
 * @author TIP
 */
public enum UnionStatus {

	UNMARRIED(0),
	MARRIED(1),
	DIVORCED(2);

	public static final char UNMARRIED_CHAR = 0x26AF;
	public static final char MARRIAGE_CHAR = 0x26AD;
	public static final char DIVORCE_CHAR = 0x26AE;

	private int code;

	/**
	 * 
	 * @param code
	 * @param message
	 */
	private UnionStatus(final int code) {
		this.code = code;
	}

	/**
	 * 
	 * @param gender
	 * @return
	 */
	public boolean canMarry() {
		boolean result;

		if (this == MARRIED) {
			result = false;
		} else {
			result = true;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isDivorced() {
		boolean result;

		result = this == DIVORCED;

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isMarried() {
		boolean result;

		result = this == MARRIED;

		//
		return result;
	}



	/**
	 * Includes married + divorced
	 * @return
	 */
	public boolean hasMarried() {
		boolean result;

		result = this != UNMARRIED;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isUnmarried() {
		boolean result;

		result = this == UNMARRIED;

		//
		return result;
	}

	/**
	 * TIP format compatibility.
	 * 
	 * @return
	 */
	public int toInt() {
		int result;

		result = this.code;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public char toSymbol() {
		char result;

		switch (this) {
			case MARRIED:
				result = MARRIAGE_CHAR;
			break;
			case DIVORCED:
				result = DIVORCE_CHAR;
			break;
			default:
				result = UNMARRIED_CHAR;
			break;
		}

		//
		return result;

	}

	/**
	 * K
	 * 
	 * @param value
	 * 
	 * @return
	 */
	public static UnionStatus valueOf(final int value) {
		UnionStatus result;

		switch (value) {
			case 0:
				result = UnionStatus.UNMARRIED;
			break;
			case 1:
				result = UnionStatus.MARRIED;
			break;
			default:
				result = UnionStatus.DIVORCED;
		}

		//
		return result;
	}

}
