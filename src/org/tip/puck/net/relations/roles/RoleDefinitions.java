package org.tip.puck.net.relations.roles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.tip.puck.net.AlterAge;
import org.tip.puck.net.Gender;
import org.tip.puck.net.relations.Role;

import fr.devinsy.util.StringList;

public class RoleDefinitions extends ArrayList<RoleDefinition>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static boolean neutralEquals (RoleDefinition alpha, RoleDefinition beta){
		
		return alpha.getSelfTerm().equals(beta.getSelfTerm()) &&
				alpha.getRoleName().equals(beta.getRoleName()) &&
				alpha.getAlterTerm().equals(beta.getAlterTerm()) &&
				alpha.getAlterGender() == beta.getAlterGender() &&
				alpha.getAlterAge() == beta.getAlterAge();
	}
	
	public RoleDefinitions get(String letters, Gender egoGender){
		RoleDefinitions result;
		
		result = new RoleDefinitions();
		
		for (RoleDefinition definition : this){
			if (definition.getLetters().equals(letters) && definition.getEgoGender()==egoGender){
				result.add(definition);
			}
		}
		
		// If no exact egoGender match, try to find more general definition 
		if (result.isEmpty() && !egoGender.isUnknown()){
			for (RoleDefinition definition : this){
				if (definition.getLetters().equals(letters) && egoGender.specifies(definition.getEgoGender())){
					result.add(definition);
				}
			}
		}

		// If no exact egoGender match, split into specific cases 
		if (result.isEmpty() && egoGender.isUnknown()){
			for (RoleDefinition definition : this){
				if (definition.getLetters().equals(letters) && definition.getEgoGender().specifies(egoGender)){
					result.add(definition);
				}
			}
		}
		//
		return result;
	}
	
	
	private RoleDefinition getBySelfAndGeneratorTerm(Role selfTerm, Role generatorTerm,  Map<String,RoleActor> elementaryTerms, StringList subReport){
		RoleDefinition result;

		result = null;
		
		RoleActor generator = elementaryTerms.get(generatorTerm.toString());
		
		if (generator!=null){
			
			RoleDefinitions definitions = getBySelfTermAndRole(selfTerm,generator.getRole());
			if (!definitions.isEmpty()){
				
				result = definitions.get(0);
				
			} else {
				
				subReport.appendln("No term defined as "+generator.getRole()+" of "+selfTerm);
			}
			
		} else {
			
			subReport.appendln("No term defined as "+generatorTerm+" of "+selfTerm);
		}

		//
		return result;
	}
	
	private RoleDefinitions getBySelfTermAndRole(Role selfTerm, MetaRole metaRole){
		RoleDefinitions result;
		
		result = new RoleDefinitions();
		
		for (RoleDefinition definition : this){
			
			if (definition.getSelfTerm().equals(selfTerm) && definition.getAlter().getRole().matches(metaRole)){
				result.add(definition);
			}
		}
		//
		return result;
	}
	

	RoleDefinition reinvert (RoleActor alter, Role inverseTerm, Map<String,RoleActor> elementaryTerms, StringList subReport){
		RoleDefinition result;
		
		result = null;
		
		Role selfTerm = new Role(alter.getSelfName()); 
		
		RoleActor inverse = elementaryTerms.get(inverseTerm);
		
		if (inverse!=null){
			
			result = new RoleDefinition(selfTerm,alter.cloneAs(inverse.getRole().getName()));
			subReport.appendln(alter.getName()+" defined as "+inverse.getRole()+" of "+selfTerm);

		} else {
			
			// Decompose inverse term
			
			RoleDefinitions inverseTermDefinitions = getByAlterTerm(inverseTerm);
			
			if (!inverseTermDefinitions.isEmpty()){
				
				RoleDefinition inverseTermDefinition = inverseTermDefinitions.get(0);
				
				Role mediusSelfTerm = inverseTermDefinition.getSelfTerm();
				MetaRole mediusAlterRole = inverseTermDefinition.getRole();
				MetaRole inverseMediusAlterRole = new MetaRole(mediusAlterRole.invertName(),mediusAlterRole.getAlterGender(),mediusAlterRole.getEgoGender(),mediusAlterRole.getAlterAge().invert());
				
				RoleDefinitions mediusDefinitions = getBySelfTermAndRole(selfTerm,inverseMediusAlterRole);
				
				if (!mediusDefinitions.isEmpty()){
					
					Role inverseMediusAlterTerm = mediusDefinitions.get(0).getAlterTerm();
					
					RoleActor mediusSelf = elementaryTerms.get(mediusSelfTerm.toString());
					
					if (mediusSelf!=null){
						
						RoleActor inverseMediusSelf = alter.cloneAs(mediusSelf.getRole().invertName());
						
						result = new RoleDefinition(inverseMediusAlterTerm,inverseMediusSelf);
						subReport.appendln(alter.getName()+" defined as "+inverseMediusSelf.getRole()+" of "+inverseMediusAlterTerm+" ... "+alter);

					} else {
						
						subReport.appendln("No term defined as inverse of "+mediusSelfTerm);

					}
										
				} else {
					
					subReport.appendln("No term defined as "+inverseMediusAlterRole+" of "+selfTerm);

				}
			}
		}
			
		
		//
		return result;
	}
	
	
	RoleDefinition recompose (Role selfTerm, Role alterTerm, Role generatorTerm, Map<String,RoleActor> elementaryTerms, StringList subReport){
		RoleDefinition result;
		
		result = null;
		
		// If generator term is elementary
		
		RoleActor generator = elementaryTerms.get(generatorTerm.toString());
		
		if (generator!=null){
			
			result = new RoleDefinition(selfTerm,new RoleActor(alterTerm,generator.getRole(),alterTerm.getSelfName()));
			subReport.appendln(alterTerm+" defined as "+generator.getRole()+" of "+selfTerm);
			
		} else {
			
			// Decompose generator term
			
			RoleDefinitions generatorDefinitions = getByAlterTerm(generatorTerm);
			
			if (!generatorDefinitions.isEmpty()){
				
				RoleDefinition generatorDefinition = generatorDefinitions.get(0);
				
				Role mediusTerm = generatorDefinition.getSelfTerm();
				RoleActor elementaryGenerator = generatorDefinition.getAlter();
				
				RoleDefinition mediusDefinition = getBySelfAndGeneratorTerm(selfTerm,mediusTerm,elementaryTerms,subReport);
				
				if (mediusDefinition != null){
					
					result = new RoleDefinition(mediusDefinition.getAlterTerm(),new RoleActor(alterTerm,elementaryGenerator.getRole(),alterTerm.getSelfName()));
					subReport.appendln(alterTerm+" defined as "+elementaryGenerator.getRole()+" of "+mediusDefinition.getAlterTerm());
				}
				
				
			} else {
				
				subReport.appendln("Undefined generator term "+generatorTerm);
			}
			
		}
			
		//
		return result;
		
	}



	
	public RoleDefinitions getByAlter(RoleActor alter){
		RoleDefinitions result;
		
		result = new RoleDefinitions();
		
		for (RoleDefinition definition : this){
			
			if (definition.getAlter().equals(alter)){
				result.add(definition);
			}
		}
		//
		return result;
	}
	
	public RoleDefinitions getBySelfTerm(Role selfTerm){
		RoleDefinitions result;
		
		result = new RoleDefinitions();
		
		for (RoleDefinition definition : this){
			
			if (definition.getSelfTerm().equals(selfTerm)){
				result.add(definition);
			}
		}
		//
		return result;
	}
	
	public RoleDefinitions getByAlterTerm(Role alterTerm){
		RoleDefinitions result;
		
		result = new RoleDefinitions();
		
		for (RoleDefinition definition : this){
			
			if (definition.getAlterTerm().equals(alterTerm)){
				result.add(definition);
			}
		}
		//
		return result;
	}
	
	public List<RoleDefinition> toSortedList (){
		List<RoleDefinition> result;
		
		result = new ArrayList<RoleDefinition>(this);
		Collections.sort(result);
		
		//
		return result;
	}
	
	public void put (RoleDefinition definition){
		
		boolean addable = true;
		
		for (RoleDefinition otherDefinition : toSortedList()){
			if (neutralEquals(definition,otherDefinition)){
				addable = false;
				if (!otherDefinition.getEgoGender().isUnknown()){
					if (definition.getEgoGender().isUnknown()){
						this.remove(otherDefinition);
						this.addNew(definition);
					} else if (definition.getEgoGender()!=otherDefinition.getEgoGender()){
						this.remove(otherDefinition);
						RoleDefinition newDefinition = definition.clone();
						newDefinition.getAlter().getRole().setEgoGender(Gender.UNKNOWN);
						this.addNew(newDefinition);
					}
				}
			}
		}
		//
		if (addable){
			add(definition);
		}
	}
	
	public boolean addNew (RoleDefinition definition){
		boolean result;
		
		if (this.contains(definition)){
			result = false;
		} else {
			result = add(definition);
		}
		//
		return result;
	}
	
	public boolean equals(Object other){
		return toString().equals(((RoleDefinition)other).toString());
	}
	
	public boolean hasAlter(Role alterRole){
		boolean result;
		
		result = false;
		
		for (RoleDefinition definition : this){
			if (definition.getAlter().getIndividual().equals(alterRole)){
				result = true;
				break;
			}
		}
		//
		return result;
	}
	
	public boolean hasAlterWithGender(Role alterRole, Gender gender){
		boolean result;
		
		result = false;
		
		for (RoleDefinition definition : this){
			if (definition.getAlter().getIndividual().equals(alterRole) && definition.getAlter().getRole().getAlterGender()==gender){
				result = true;
				break;
			}
		}
		//
		return result;
	}
	
	public RoleDefinitions compress (){
		RoleDefinitions result;
		
		result = new RoleDefinitions();
		
		for (RoleDefinition definition : this){
			boolean addable = true;
			for (RoleDefinition other : result.toSortedList()){
				if (definition.specifies(other)){
					addable = false;
					break;
				} else if (other.specifies(definition)){
					result.remove(other);
				} else if (other.isEgoGenderComplement(definition)){
					result.remove(other);
					result.put(definition.cloneWithEgoGender(Gender.UNKNOWN));
					addable = false;
					break;
				} else if (other.isAlterGenderComplement(definition)){
					result.remove(other);
					result.put(definition.cloneWithAlterGender(Gender.UNKNOWN));
					addable = false;
					break;
				} else if (other.isAlterAgeComplement(definition)){
					result.remove(other);
					result.put(definition.cloneWithAlterAge(AlterAge.UNKNOWN));
					addable = false;
					break;
				}
			}
			if (addable){
				result.put(definition);
			}
		}
		//
		return result;
	}
	
	public String toString (){
		String result;
		
		result = "";
		String separator = "";
		
		for (RoleDefinition definition : this){
			if (!result.equals("")) {
				separator = ";";
			}
			result += separator+definition.getAlter()+" ["+definition.getSelfTerm()+"]";
		}
		
		//
		return result;
		
	}
	
	public StringList toSortedStringList(){
		StringList result;
		
		result = new StringList();
		
		for (RoleDefinition definition : this){
			result.append(definition.toString());
		}
		
		Collections.sort(result);
		
		//
		return result;
	}
	
}
