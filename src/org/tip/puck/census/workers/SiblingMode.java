package org.tip.puck.census.workers;

/**
 * 
 * @author TIP
 */
public enum SiblingMode {
	NONE,
	FULL,
	ALL
}
