package org.tip.puck.io.ged;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Attributes;
import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.relations.Role;

/**
 * This class represents a GED File reader and writer.
 * 
 * It is reading the GEDCOM format with Hérédis and Geneatique fixed.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class GEDFile {

	private static final Logger logger = LoggerFactory.getLogger(GEDFile.class);
	public static final String DEFAULT_CHARSET_NAME = "UTF-8";
	public static final int MAX_LINE_SIZE = 4096;
	public static final String FAM_EVENT_LABELS = "ANUL|CENS|DIV|DIVF|ENGA|MARB|MARC|MARR|MARL|MARS|RESI|EVEN|POSITION";
	public static final String INDI_EVENT_LABELS = "BIRT|CHR|DEAT|BURI|CREM|ADOP|BAPM|BARM|BASM|BLES|CHRA|CONF|FCOM|ORDN|NATU|EMIG|IMMI|CENS|PROB|WILL|GRAD|RETI|EVEN";

	public static final Pattern GEDLINE_PATTERN = Pattern.compile("^\uFEFF?(\\d|[1-9]\\d+) (@(\\D*)(\\d*)(\\D*)@)? ?(\\w+) *((@(\\D*)(\\d*)(\\D*)@)|(.*))$");

	/**
	 * 
	 * @param source
	 */
	public static void addSubmitterData(final Net net, final GEDBlock source) {
		//
		boolean ended = false;
		int index = 1;
		while (!ended) {
			if (index < source.size()) {
				switch (source.get(index).getLevel()) {
					case 1:
						net.attributes().put("SUBM_" + source.get(index).tag(), source.get(index).trimmedValue());
					break;
					default:
						// logger.debug("source=" +
						// LogHelper.toString2(source));
						// logger.debug("index=" + index);
						if (source.get(index).tag().equals("CONT")) {
							String prefix = "SUBM_" + buildPrefix(source, index);
							String label = prefix.substring(0, prefix.length() - 1);
							Attribute attribute = net.attributes().get(label);
							attribute.setValue(attribute.getValue() + source.get(index).trimmedValue());
						} else {
							net.attributes().put("SUBM_" + buildPrefix(source, index) + source.get(index).tag(), source.get(index).trimmedValue());
						}
				}

				//
				index += 1;
			} else {
				ended = true;
			}
		}
	}

	/**
	 * 
	 * @param source
	 */
	public static void buildFamily(final Net net, final GEDBlock source) throws PuckException {
		//
		Family currentFamily = null;
		String tagLevel1 = null;
		String typeLevel1 = null;
		String tagLevel2 = null;
		Relation currentRelation = null;
		Individual alter = null;
		boolean ended = false;
		int currentLineIndex = 0;
		while (!ended) {
			//
			if (currentLineIndex < source.size()) {
				//
				GEDLine currentLine = source.get(currentLineIndex);

				//
				switch (source.get(currentLineIndex).getLevel()) {
					case 0:
						// Reset context;
						currentFamily = null;
						tagLevel1 = null;
						typeLevel1 = null;
						tagLevel2 = null;
						alter = null;
						currentRelation = null;

						//
						currentFamily = net.families().getById(source.get(0).getRefId());
						if (currentFamily == null) {
							//
							currentFamily = new Family(source.get(0).getRefId());
							net.families().add(currentFamily);
						}
					break;

					case 1:
						//
						tagLevel1 = currentLine.tag();
						typeLevel1 = null;
						tagLevel2 = null;
						alter = null;
						currentRelation = null;

						//
						if (currentLine.tag().equals("HUSB")) {
							//
							Individual spouse = net.individuals().getById(source.get(currentLineIndex).valueId());
							if (spouse == null) {
								//
								spouse = new Individual(source.get(currentLineIndex).valueId());
								net.individuals().add(spouse);
							}

							//
							currentFamily.setHusband(spouse);
							currentFamily.setMarried(true);
							spouse.getPersonalFamilies().add(currentFamily);

						} else if (currentLine.tag().equals("WIFE")) {
							//
							Individual spouse = net.individuals().getById(source.get(currentLineIndex).valueId());
							if (spouse == null) {
								//
								spouse = new Individual(source.get(currentLineIndex).valueId());
								net.individuals().add(spouse);
							}

							//
							currentFamily.setWife(spouse);
							currentFamily.setMarried(true);
							spouse.getPersonalFamilies().add(currentFamily);

						} else if (currentLine.tag().equals("CHIL")) {
							//
							// Nothing to do.

						} else if (currentLine.tag().matches(FAM_EVENT_LABELS)) {
							// GEDCOM FAMILY_EVENT_STRUCTURE
							if (StringUtils.equals(currentLine.tag(), "EVEN")) {
								//
								typeLevel1 = StringUtils.defaultString(currentLine.trimmedValue(), "EVEN");
							}

						} else if (source.get(currentLineIndex).hasValue()) {
							//
							currentFamily.attributes().put(source.get(currentLineIndex).tag(), source.get(currentLineIndex).trimmedValue());

						} else if (source.get(currentLineIndex).hasTag()) {
							//
							currentFamily.attributes().put(source.get(currentLineIndex).tag(), "");
						}
					break;

					case 2:
						//
						tagLevel2 = currentLine.tag();

						//
						if (tagLevel1.matches(FAM_EVENT_LABELS)) {
							//
							if (currentLine.tag().equals("ASSO")) {
								// This case is not available in GEDCOM 5.5.1.
								// It is managed only for load from
								// Hérédis.
								alter = net.individuals().getById(currentLine.valueId());
								if (alter == null) {
									//
									alter = net.createIndividual(currentLine.valueId());
								}

							} else {

								currentFamily.attributes().put(buildPrefix(source, currentLineIndex) + source.get(currentLineIndex).tag(),
										source.get(currentLineIndex).trimmedValue());
								//
								// In case of Hérédis file with ASSO in level 2
								// then
								// copy data in relation attributes.
								if (currentRelation != null) {
									//
									currentRelation.attributes().put(currentLine.tag(), currentLine.trimmedValue());
								}
							}

						} else {
							//
							currentFamily.attributes().put(buildPrefix(source, currentLineIndex) + source.get(currentLineIndex).tag(),
									source.get(currentLineIndex).trimmedValue());
						}

					break;

					case 3:
						//
						if (StringUtils.equals(tagLevel2, "ASSO")) {
							//
							if (currentLine.tag().equals("TYPE")) {
								// Nothing to do. Deprecated tag in GEDCOM 5.5.1

							} else if (currentLine.tag().equals("RELA")) {
								//
								if (currentRelation == null) {
									//
									String relationModelName = StringUtils.defaultString(typeLevel1, tagLevel1);

									//
									RelationModel relationModel = net.relationModels().getByName(relationModelName);
									if (relationModel == null) {
										//
										relationModel = net.createRelationModel(relationModelName);
									}

									//
									String relationName = " [" + StringUtils.defaultString(typeLevel1, tagLevel1) + "] " + currentFamily.getId();
									currentRelation = net.createRelation(relationName, relationModel);

									//
									Role husbandRole = currentRelation.getModel().roles().getByName("husband");
									if (husbandRole == null) {
										//
										husbandRole = new Role("husband");
										currentRelation.getModel().roles().add(husbandRole);
									}

									//
									if (currentFamily.getHusband() != null) {
										//
										net.addRelationActors(currentRelation, new Actor(currentFamily.getHusband(), husbandRole));
									}

									//
									Role wifeRole = currentRelation.getModel().roles().getByName("wife");
									if (wifeRole == null) {
										//
										wifeRole = new Role("wife");
										currentRelation.getModel().roles().add(wifeRole);
									}

									//
									if (currentFamily.getWife() != null) {
										//
										net.addRelationActors(currentRelation, new Actor(currentFamily.getWife(), wifeRole));
									}
								}

								//
								Role alterRole = currentRelation.getModel().roles().getByName(currentLine.trimmedValue());
								if (alterRole == null) {
									//
									alterRole = new Role(currentLine.trimmedValue());
									currentRelation.getModel().roles().add(alterRole);
								}

								//
								net.addRelationActors(currentRelation, new Actor(alter, alterRole));

							} else if (currentLine.tag().equals("SOUR")) {
								//
								if (currentRelation == null) {
									//
									throw PuckExceptions.BAD_FILE_FORMAT.create("SOURC must follow RELA in ASSO block.");

								} else {
									//
									String postfix = buildPostfix(currentRelation.attributes(), "SOURC");

									//
									currentRelation.attributes().add(new Attribute("SOURC" + postfix, String.valueOf(currentLine.getRefId())));
								}

							} else {
								//
								currentRelation.attributes().add(new Attribute(currentLine.tag(), currentLine.trimmedValue()));
							}

						} else if (source.get(currentLineIndex).tag().equals("CONT")) {
							//
							String prefix = buildPrefix(source, currentLineIndex);
							String label = prefix.substring(0, prefix.length() - 1);
							Attribute attribute = currentFamily.attributes().get(label);
							if (attribute != null) {
								attribute.setValue(attribute.getValue() + source.get(currentLineIndex).value());
							}

							// In case of Hérédis file with ASSO in level 2
							// then
							// copy data in relation attributes.
							if (currentRelation != null) {
								//
								Attribute relationAttribute = currentRelation.attributes().get(tagLevel2);
								if (relationAttribute != null) {
									//
									relationAttribute.setValue(relationAttribute.getValue() + currentLine.trimmedValue());
								}
							}

						} else {
							//
							currentFamily.attributes().put(buildPrefix(source, currentLineIndex) + source.get(currentLineIndex).tag(),
									source.get(currentLineIndex).value());
						}

					break;

					default:
						if (source.get(currentLineIndex).tag().equals("CONT")) {
							//
							String prefix = buildPrefix(source, currentLineIndex);
							String label = prefix.substring(0, prefix.length() - 1);
							Attribute attribute = currentFamily.attributes().get(label);
							attribute.setValue(attribute.getValue() + source.get(currentLineIndex).value());

						} else {
							//
							currentFamily.attributes().put(buildPrefix(source, currentLineIndex) + source.get(currentLineIndex).tag(),
									source.get(currentLineIndex).value());
						}
				}

				//
				currentLineIndex += 1;

			} else {
				//
				ended = true;
			}
		}
	}

	/**
	 * 
	 * @param source
	 */
	public static void buildHeader(final Net net, final GEDBlock source) {
		//
		boolean ended = false;
		int index = 1;
		while (!ended) {
			if (index < source.size()) {
				switch (source.get(index).getLevel()) {
					case 1:
						net.attributes().put(source.get(index).tag(), source.get(index).value());
					break;
					default:
						// logger.debug("source=" +
						// LogHelper.toString2(source));
						// logger.debug("index=" + index);
						if (source.get(index).tag().equals("CONT")) {
							String prefix = buildPrefix(source, index);
							String label = prefix.substring(0, prefix.length() - 1);
							Attribute attribute = net.attributes().get(label);
							attribute.setValue(attribute.getValue() + source.get(index).value());
						} else {
							net.attributes().put(buildPrefix(source, index) + source.get(index).tag(), source.get(index).value());
						}
				}

				//
				index += 1;
			} else {
				ended = true;
			}
		}
	}

	/**
	 * 
	 * @param source
	 * @throws PuckException
	 */
	public static void buildIndividual(final Net net, final GEDBlock source) throws PuckException {
		//
		Individual ego = null;
		boolean ended = false;
		int currentLineIndex = 0;
		String tagLevel1 = null;
		String tagLevel2 = null;
		String typeLevel2 = null;
		String tagLevel3 = null;
		Relation currentRelation = null;
		Individual alter = null;
		while (!ended) {
			//
			if (currentLineIndex < source.size()) {
				// logger.debug("current=" +
				// LogHelper.toString(source.get(index)));

				GEDLine currentLine = source.get(currentLineIndex);

				switch (source.get(currentLineIndex).getLevel()) {
					case 0:
						// Reset context.
						ego = null;
						alter = null;
						currentRelation = null;
						tagLevel1 = null;
						tagLevel2 = null;
						typeLevel2 = null;
						tagLevel3 = null;

						//
						ego = net.individuals().getById(source.get(0).getRefId());
						if (ego == null) {
							ego = new Individual(source.get(0).getRefId());
							net.individuals().add(ego);
						}
					break;

					case 1:
						// logger.debug("LVL1 : " + currentLineIndex + " " +
						// currentLine.tag());
						// Reset context.
						tagLevel1 = currentLine.tag();
						tagLevel2 = null;
						typeLevel2 = null;
						tagLevel3 = null;
						currentRelation = null;

						//
						if (source.get(currentLineIndex).tag().equals("NAME")) {
							//
							ego.setName(source.get(currentLineIndex).value());

						} else if (source.get(currentLineIndex).tag().equals("SEX")) {
							//
							if (StringUtils.isBlank(source.get(currentLineIndex).value())) {
								//
								ego.setGender(Gender.UNKNOWN);

							} else {
								//
								ego.setGender(Gender.valueOf(source.get(currentLineIndex).value().charAt(0)));
							}
						} else if (source.get(currentLineIndex).tag().equals("FAMS")) {
							//
							// Nothing to do.

						} else if (source.get(currentLineIndex).tag().equals("FAMC")) {
							// Create parent-child link only if it is not
							// adoptive link.

							//
							boolean computeThisLink;
							if (currentLineIndex < source.size() - 1) {
								GEDLine nextLine = source.get(currentLineIndex + 1);

								// PEDI values are: adopted, birth, foster and
								// sealing.
								if ((nextLine.getLevel() == 2) && (nextLine.tag().equals("PEDI")) && (nextLine.hasValue())
										&& (!nextLine.value().equals("birth"))) {
									//
									computeThisLink = false;

								} else {
									//
									computeThisLink = true;
								}

							} else {
								//
								computeThisLink = true;
							}

							//
							if (computeThisLink) {
								//
								Family currentFamily = net.families().getById(source.get(currentLineIndex).valueId());
								if (currentFamily == null) {
									//
									currentFamily = new Family(source.get(currentLineIndex).valueId());
									net.families().add(currentFamily);
								}

								//
								if ((ego.getOriginFamily() != null) && (ego.getOriginFamily().getId() != currentFamily.getId())) {
									logger.warn("Individual orginal family defined twice " + ego.getId());
									//
									String adoptionName = ego.getName() + " " + ego.getId();
									Relation adoption = net.createRelation(adoptionName, net.relationModels().getByName("Adoption"));
									net.createRelationActor(adoption, ego.getId(), "child");
									adoption.attributes().put("adoptive_family", String.valueOf(source.get(currentLineIndex).valueId()));
									adoption.attributes().put("pedigree", "duplicate");

								} else if (ego.getOriginFamily() == null) {
									//
									currentFamily.getChildren().add(ego);
									ego.setOriginFamily(currentFamily);
								}
							}

						} else if (currentLine.tag().equals("ASSO")) {
							//
							alter = net.individuals().getById(currentLine.valueId());
							if (alter == null) {
								//
								alter = net.createIndividual(currentLine.valueId());
							}

						} else if (currentLine.tag().matches(INDI_EVENT_LABELS)) {
							// GEDCOM INDIVIDUAL_EVENT_STRUCTURE
							if (currentLine.value() != null && StringUtils.isNotBlank(currentLine.trimmedValue())) {
								//
								ego.attributes().put(source.get(currentLineIndex).tag(), currentLine.trimmedValue());
							}

						} else if (source.get(currentLineIndex).hasValue()) {
							//
							ego.attributes().put(source.get(currentLineIndex).tag(), source.get(currentLineIndex).trimmedValue());

						} else if (source.get(currentLineIndex).hasTag()) {
							//
							ego.attributes().put(source.get(currentLineIndex).tag(), "");
						}
					break;

					case 2:
						// logger.debug("LVL2 : " + currentLineIndex + " " +
						// currentLine.tag());
						//
						tagLevel2 = currentLine.tag();
						tagLevel3 = null;

						//
						if ((StringUtils.equals(tagLevel1, "ADOP")) && (source.get(currentLineIndex).tag().equals("FAMC"))) {
							//
							String adoptionName = ego.getName() + " " + ego.getId();
							Relation adoption = net.createRelation(adoptionName, net.relationModels().getByName("Adoption"));
							net.createRelationActor(adoption, ego.getId(), "child");
							adoption.attributes().put("adoptive_family", String.valueOf(source.get(currentLineIndex).valueId()));
							adoption.attributes().put("pedigree", "Adoption");

						} else if (currentLine.tag().equals("CONT")) {
							//
							String prefix = buildPrefix(source, currentLineIndex);
							String label = prefix.substring(0, prefix.length() - 1);
							Attribute attribute = ego.attributes().get(label);
							attribute.setValue(attribute.getValue() + source.get(currentLineIndex).value());

						} else if (StringUtils.equals(tagLevel1, "ASSO")) {
							//
							if (currentLine.tag().equals("TYPE")) {
								// Nothing to do. Deprecated tag in GEDCOM 5.5.1

							} else if (currentLine.tag().equals("RELA")) {
								//
								String relationModelName = currentLine.trimmedValue();

								//
								RelationModel relationModel = net.relationModels().getByName(relationModelName);
								if (relationModel == null) {
									//
									relationModel = net.createRelationModel(relationModelName);
								}

								//
								Role egoRole = relationModel.roles().getByName("ego");
								if (egoRole == null) {
									//
									egoRole = new Role("ego");
									relationModel.roles().add(egoRole);
								}

								//
								Role alterRole = relationModel.roles().getByName("alter");
								if (alterRole == null) {
									//
									alterRole = new Role("alter");
									relationModel.roles().add(alterRole);
								}

								//
								String relationName = ego.getId() + " [R] " + alter.getId();
								currentRelation = net.createRelation(relationName, relationModel, new Actor(ego, egoRole), new Actor(alter, alterRole));

							} else if (currentLine.tag().equals("SOUR")) {
								//
								if (currentRelation == null) {
									//
									throw PuckExceptions.BAD_FILE_FORMAT.create("SOURC must follow RELA in ASSO block.");

								} else {
									//
									String postfix = buildPostfix(currentRelation.attributes(), "SOURC");

									//
									currentRelation.attributes().add(new Attribute("SOURC" + postfix, String.valueOf(currentLine.getRefId())));
								}
							}
						} else if (tagLevel1.matches(INDI_EVENT_LABELS)) {
							//
							if ((StringUtils.equals(tagLevel1, "EVEN")) && (currentLine.tag().equals("TYPE"))) {
								//
								typeLevel2 = currentLine.trimmedValue();

							} else if (currentLine.tag().equals("ASSO")) {
								// This case is not available in GEDCOM 5.5.1.
								// It is managed only for load from
								// Hérédis.
								alter = net.individuals().getById(currentLine.valueId());
								if (alter == null) {
									//
									alter = net.createIndividual(currentLine.valueId());
								}

							} else {
								//
								ego.attributes().put(buildPrefix(source, currentLineIndex) + source.get(currentLineIndex).tag(),
										source.get(currentLineIndex).value());

								// In case of Hérédis file with ASSO in level 2
								// then
								// copy data in relation attributes.
								if (currentRelation != null) {
									//
									currentRelation.attributes().put(currentLine.tag(), currentLine.trimmedValue());
								}
							}

                        } else if (currentLine.tag().equals("ASSO")) {
                            // These cases are not available in GEDCOM 5.5.1.
                            // It is managed only for load from GENEATIQUE 2020.
                            alter = net.individuals().getById(currentLine.valueId());
                            if (alter == null) {
                                alter = net.createIndividual(currentLine.valueId());
                            }
							
						} else if (StringUtils.equals(tagLevel1, "OBJE")) {
							// GEDCOM MULTIMEDIA_LINK
							// FIXME

						} else {
							//
							// logger.debug("==> [" + buildPrefix(source,
							// currentLineIndex) + "][" +
							// source.get(currentLineIndex).tag() + "]");
							ego.attributes().put(buildPrefix(source, currentLineIndex) + source.get(currentLineIndex).tag(),
									source.get(currentLineIndex).value());

						}
					break;

					case 3:
						// logger.debug("LVL3 : " + currentLineIndex + " " +
						// currentLine.tag());
						//
						tagLevel3 = currentLine.tag();

						//
						if (StringUtils.equals(tagLevel1, "OBJE")) {
							// Do nothing.

						} else if (StringUtils.equals(tagLevel2, "ASSO")) {
							//
							if (currentLine.tag().equals("TYPE")) {
								// Nothing to do. Deprecated tag in GEDCOM 5.5.1

							} else if (currentLine.tag().equals("RELA")) {
								//
								if (currentRelation == null) {
									//
									String relationModelName = StringUtils.defaultString(typeLevel2, tagLevel1);

									//
									RelationModel relationModel = net.relationModels().getByName(relationModelName);
									if (relationModel == null) {
										//
										relationModel = net.createRelationModel(relationModelName);
									}

									//
									String relationName = " [" + StringUtils.defaultString(typeLevel2, tagLevel1) + "] " + ego.getId();
									currentRelation = net.createRelation(relationName, relationModel);

									//
									Role egoRole = currentRelation.getModel().roles().getByName("ego");
									if (egoRole == null) {
										//
										egoRole = new Role("ego");
										currentRelation.getModel().roles().add(egoRole);
									}

									//
									net.addRelationActors(currentRelation, new Actor(ego, egoRole));
								}

								//
								Role alterRole = currentRelation.getModel().roles().getByName(currentLine.trimmedValue());
								if (alterRole == null) {
									//
									alterRole = new Role(currentLine.trimmedValue());
									currentRelation.getModel().roles().add(alterRole);
								}

								//
								net.addRelationActors(currentRelation, new Actor(alter, alterRole));

							} else if (currentLine.tag().equals("SOUR")) {
								//
								if (currentRelation == null) {
									//
									throw PuckExceptions.BAD_FILE_FORMAT.create("SOURC must follow RELA in ASSO block.");

								} else {
									//
									String postfix = buildPostfix(currentRelation.attributes(), "SOURC");

									//
									currentRelation.attributes().add(new Attribute("SOURC" + postfix, String.valueOf(currentLine.getRefId())));
								}

							} else {
								//
								currentRelation.attributes().add(new Attribute(currentLine.tag(), currentLine.trimmedValue()));
							}

						} else if (source.get(currentLineIndex).tag().equals("CONT")) {
							//
							String prefix = buildPrefix(source, currentLineIndex);
							String label = prefix.substring(0, prefix.length() - 1);
							Attribute attribute = ego.attributes().get(label);
							if (attribute != null) {
								attribute.setValue(attribute.getValue() + source.get(currentLineIndex).value());
							}

							// In case of Hérédis file with ASSO in level 2
							// then
							// copy data in relation attributes.
							if (currentRelation != null) {
								//
								Attribute relationAttribute = currentRelation.attributes().get(tagLevel2);
								if (relationAttribute != null) {
									//
									relationAttribute.setValue(relationAttribute.getValue() + currentLine.trimmedValue());
								}
							}

						} else {
							//
							ego.attributes().put(buildPrefix(source, currentLineIndex) + source.get(currentLineIndex).tag(),
									source.get(currentLineIndex).value());
						}

					break;

					default:
						if (source.get(currentLineIndex).tag().equals("CONT")) {
							//
							String prefix = buildPrefix(source, currentLineIndex);
							String label = prefix.substring(0, prefix.length() - 1);
							Attribute attribute = ego.attributes().get(label);
							if (attribute != null) {
								attribute.setValue(attribute.getValue() + source.get(currentLineIndex).value());
							}

						} else {
							//
							ego.attributes().put(buildPrefix(source, currentLineIndex) + source.get(currentLineIndex).tag(),
									source.get(currentLineIndex).value());
						}
				}

				//
				currentLineIndex += 1;

			} else {
				//
				ended = true;
			}
		}
	}

	/**
	 * 
	 * @param attributes
	 * @param label
	 * @return
	 */
	public static String buildPostfix(final Attributes attributes, final String label) {
		String result;

		if ((attributes == null) || (StringUtils.isBlank(label))) {
			//
			result = null;

		} else {
			//
			boolean ended = false;
			int index = 0;
			result = null;
			while (!ended) {
				//
				if ((index == 0) && (attributes.getValue(label) == null)) {
					//
					ended = true;
					result = "";

				} else if ((index > 0) && (attributes.getValue(label + index) == null)) {
					//
					ended = true;
					result = String.valueOf(ended);

				} else {
					//
					index += 1;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public static String buildPrefix(final GEDBlock source, final int currentIndex) {
		String result;

		boolean ended = false;
		result = "";
		int lastGoodIndex = currentIndex;
		int index = currentIndex - 1;
		while (!ended) {
			if (source.get(index).getLevel() > 0) {
				// logger.debug("===" + index + " " + source.get(index).level()
				// + " " + source.get(lastGoodIndex).level() + " " +
				// source.get(index).tag() + " "
				// + source.get(lastGoodIndex).tag());
				if ((source.get(index).getLevel() < source.get(lastGoodIndex).getLevel()) && (!source.get(index).tag().equals(source.get(lastGoodIndex)))) {
					result = source.get(index).tag() + "_" + result;
					lastGoodIndex = index;
				}
				index -= 1;
			} else {
				ended = true;
			}
		}

		//
		return result;
	}

	/**
	 * Loads a GED file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file) throws PuckException {
		Net result;

		result = load(file, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Loads a GED file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file, final String charsetName) throws PuckException {
		Net result;

		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), charsetName));
			result = read(in);

			result.setLabel(file.getName());

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException exception) {
				logger.warn("Not managed error.");
				exception.printStackTrace();
			}
		}

		//
		return result;
	}

	/**
	 * Reads a net from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static Net read(final BufferedReader in) throws PuckException {
		Net result;

		//
		result = new Net();

		//
		RelationModel adoptionModel = result.createRelationModel("Adoption");
		result.createRelationRole(adoptionModel, "child");
		result.createRelationRole(adoptionModel, "adoptive parent");
		result.createRelationRole(adoptionModel, "birth parent");

		logger.debug("Read.");
		boolean ended = false;
		while (!ended) {
			//
			GEDBlock source = readGEDBlock(in);
			// logger.debug("source=" + LogHelper.toString(source));

			//
			if (source == null) {
				//
				throw PuckExceptions.BAD_FILE_FORMAT.create("File ends too early.");

			} else if (source.get(0).tag().equals("INDI")) {
				//
				buildIndividual(result, source);

			} else if (source.get(0).tag().equals("FAM")) {
				//
				buildFamily(result, source);

			} else if (source.get(0).tag().equals("HEAD")) {
				//
				buildHeader(result, source);

			} else if (source.get(0).tag().equals("TRLR")) {
				//
				ended = true;

			} else if (source.get(0).tag().equals("CSTA")) {
				// Ignore. FIXME

			} else if (source.get(0).tag().equals("SOURC")) {
				// Ignore. FIXME

			} else if (source.get(0).tag().equals("REPO")) {
				// Ignore. FIXME

			} else if (source.get(0).tag().equals("NOTE")) {
				// Ignore. FIXME

			} else if (source.get(0).tag().equals("OBJE")) {
				// Ignore. FIXME

			} else if (source.get(0).tag().equals("SUBM")) {
				// Ignore. FIXME
				addSubmitterData(result, source);

			} else {
				//
				logger.warn("IGNORING zero level tag [" + source.get(0).tag() + "]");
			}
		}

		//
		if (result.relations().getByModel(adoptionModel).isEmpty()) {
			// Clean used adoption relation model.
			result.remove(adoptionModel);

		} else {
			// Complete adoption relations.
			Relations adoptions = result.relations().getByModel(adoptionModel);
			for (Relation adoption : adoptions) {
				//
				Individual child = adoption.actors().getByRole("child").get(0).getIndividual();

				for (Individual parent : child.getParents()) {
					result.createRelationActor(adoption, parent.getId(), "birth parent");
				}

				//
				String birthFamilyId;
				if (child.getOriginFamily() == null) {
					birthFamilyId = "";
				} else {
					birthFamilyId = String.valueOf(child.getOriginFamily().getId());
				}
				adoption.attributes().put("birth_family", birthFamilyId);

				//
				String adoptiveFamilyIdString = adoption.attributes().getValue("adoptive_family");
				if (NumberUtils.isDigits(adoptiveFamilyIdString)) {
					int adoptiveFamilyId = Integer.parseInt(adoptiveFamilyIdString);

					Family adoptiveFamily = result.families().getById(adoptiveFamilyId);
					for (Individual parent : adoptiveFamily.getParents()) {
						result.createRelationActor(adoption, parent.getId(), "adoptive parent");
					}
				}
			}
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * Reads a line of individual data from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of individual data or null.
	 * 
	 * @throws PuckException
	 */
	public static GEDBlock readGEDBlock(final BufferedReader in) throws PuckException {
		GEDBlock result;

		try {
			// Read first block datas.

			boolean ended = false;
			result = null;
			while (!ended) {
				//
				in.mark(MAX_LINE_SIZE);
				GEDLine source = readGEDLine(in);
				// logger.debug("source = " + LogHelper.toString(source));

				if (source == null) {
					ended = true;
				} else {
					if (result == null) {
						result = new GEDBlock();
						result.add(source);
					} else {
						if (source.getLevel() == 0) {
							ended = true;
							in.reset();
						} else {
							result.add(source);
						}
					}
				}
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading GEDCOM line.");
		}

		//
		return result;
	}

	/**
	 * Reads a line of individual data from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of individual data or null.
	 * 
	 * @throws PuckException
	 */
	public static GEDLine readGEDLine(final BufferedReader in) throws PuckException {
		GEDLine result;

		try {
			//
			String line = in.readLine();
			if (line != null) {
				line = line.trim();
			}

			// logger.debug("==> [" + line + "]");

			if (line == null) {
				result = null;
			} else if (line.length() == 0) {
				result = null;
			} else {
				Matcher matcher = GEDLINE_PATTERN.matcher(line);
				if ((matcher.find()) && (matcher.groupCount() == 12)) {
					// for (int i = 0; i <= matcher.groupCount(); i++) {
					// logger.debug(i + " " + matcher.group(i));
					// }
					//
					int level = Integer.parseInt(matcher.group(1));

					//
					String ref = matcher.group(2);
					String refPrefix;
					Integer refId;
					String refPostfix;
					if (ref == null) {
						refPrefix = null;
						refId = null;
						refPostfix = null;
					} else {
						refPrefix = matcher.group(3);
						if (StringUtils.isBlank(matcher.group(4))) {
							refId = null;
						} else {
							refId = Integer.valueOf(matcher.group(4));
						}
						refPostfix = matcher.group(5);
					}

					//
					String tag = matcher.group(6);

					//
					String value;
					if (StringUtils.isBlank(matcher.group(7))) {
						value = null;
					} else {
						value = matcher.group(7);
					}

					//
					String valuePrefix;
					Integer valueId;
					String valuePostfix;
					if ((value != null) && (matcher.group(12) == null)) {
						valuePrefix = matcher.group(9);
						if (StringUtils.isBlank(matcher.group(10))) {
							valueId = null;
						} else {
							valueId = Integer.valueOf(matcher.group(10));
						}
						valuePostfix = matcher.group(11);
					} else {
						valuePrefix = null;
						valueId = null;
						valuePostfix = null;
					}

					//
					if (matcher.group(12) == null) {
						result = new GEDLine(level, refPrefix, refId, refPostfix, tag, valuePrefix, valueId, valuePostfix);
					} else {
						result = new GEDLine(level, refPrefix, refId, refPostfix, tag, value);
					}
				} else {
					throw PuckExceptions.BAD_FILE_FORMAT.create("Bad line format: [" + line + "].");
				}
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading individual line.");
		}

		//
		return result;
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File file, final Net source) throws PuckException {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));

			write(out, source);

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * Write a line of attributes.
	 * 
	 * @param out
	 *            Target.
	 * @param source
	 *            Source.
	 */
	public static void write(final PrintWriter out, final GEDLine source) {

		StringBuffer line = new StringBuffer(1024);

		//
		line.append(source.getLevel()).append(" ");

		//
		if (source.getRef() != null) {
			line.append(source.getRef()).append(" ");
		}

		//
		line.append(source.tag());

		//
		if (source.hasValue()) {
			line.append(" ").append(source.value());
		}

		//
		out.println(line.toString());
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param net
	 *            Source.
	 */
	public static void write(final PrintWriter out, final Net net) {
		//
		write(out, new GEDLine(0, null, null, null, "HEAD", null));
		for (Attribute attribute : net.attributes().toList()) {
			write(out, new GEDLine(1, null, null, null, attribute.getLabel(), attribute.getValue()));
		}

		//
		for (Individual individual : net.individuals().toSortedList()) {
			//
			write(out, new GEDLine(0, "I", individual.getId(), "", "INDI", null));
			write(out, new GEDLine(1, null, null, null, "NAME", individual.getName()));
			write(out, new GEDLine(1, null, null, null, "SEX", individual.getGender().toGedChar()));

			//
			if (individual.getOriginFamily() != null) {
				write(out, new GEDLine(1, null, null, null, "FAMC", "F", individual.getOriginFamily().getId(), ""));
			}

			//
			for (Family family : individual.getPersonalFamilies()) {
				write(out, new GEDLine(1, null, null, null, "FAMS", "F", family.getId(), ""));
			}

			//
			for (Attribute attribute : individual.attributes().toList()) {
				write(out, new GEDLine(1, null, null, null, attribute.getLabel(), attribute.getValue()));
			}
		}

		//
		for (Family family : net.families().toSortedList()) {
			//
			write(out, new GEDLine(0, "F", family.getId(), "", "FAM", null));

			if (family.getHusband() != null) {
				write(out, new GEDLine(1, null, null, null, "HUSB", "I", family.getHusband().getId(), ""));
			}
			if (family.getWife() != null) {
				write(out, new GEDLine(1, null, null, null, "WIFE", "I", family.getWife().getId(), ""));
			}

			//
			for (Individual child : family.getChildren()) {
				write(out, new GEDLine(1, null, null, null, "CHIL", "I", child.getId(), ""));
			}

			//
			for (Attribute attribute : family.attributes().toList()) {
				write(out, new GEDLine(1, null, null, null, attribute.getLabel(), attribute.getValue()));
			}
		}

		//
		write(out, new GEDLine(0, null, null, null, "TRLR", null));
	}

}
