/*
 * Copyright (c) 1995 - 2008 Sun Microsystems, Inc. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * - Neither the name of Sun Microsystems nor the names of its contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.tip.puckgui.util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.plaf.basic.BasicButtonUI;

/**
 * Component to be used as tabComponent; Contains a JLabel to show the text and
 * a JButton to close the tab it belongs to
 * 
 * This code comes from Java Swing Tutorial by Sun Microsystems.
 * 
 * Adapted by Christian Pierre MOMON.
 */
public class ButtonTabComponent extends JPanel {

	/**
	 * 
	 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
	 * 
	 */
	private class TabButton extends JButton implements ActionListener {
		private static final long serialVersionUID = 721605711978453878L;

		/**
		 * 
		 */
		public TabButton() {
			setPreferredSize(new Dimension(SIZE, SIZE));
			setToolTipText("Close this tab");
			// Make the button looks the same for all Laf's
			setUI(new BasicButtonUI());
			// Make it transparent
			setContentAreaFilled(false);
			// No need to be focusable
			setFocusable(false);

			// setBorder(BorderFactory.createEtchedBorder());
			setBorder(BorderFactory.createEmptyBorder());

			setBorderPainted(false);

			// Making nice rollover effect
			// we use the same listener for all buttons
			addMouseListener(buttonMouseListener);
			setRolloverEnabled(true);
			// Close the proper tab by clicking the button
			addActionListener(this);
		}

		/**
		 * 
		 */
		@Override
		public void actionPerformed(final ActionEvent event) {

			int tabIndex = ButtonTabComponent.this.pane.indexOfTabComponent(ButtonTabComponent.this);
			if (tabIndex != -1) {

				ButtonTabComponent.this.pane.remove(tabIndex);
			}
		}

		/**
		 * Paint the cross.
		 */
		@Override
		protected void paintComponent(final Graphics sourceGraphic) {
			super.paintComponent(sourceGraphic);
			Graphics2D targetGraphic = (Graphics2D) sourceGraphic.create();
			// shift the image for pressed buttons
			if (getModel().isPressed()) {
				targetGraphic.translate(1, 1);
			}
			targetGraphic.setStroke(new BasicStroke(2));
			targetGraphic.setColor(Color.BLACK);
			if (getModel().isRollover()) {
				targetGraphic.setColor(Color.MAGENTA);
			}

			int delta = DELTA;
			targetGraphic.drawLine(delta, delta, getWidth() - delta - 1, getHeight() - delta - 1);
			targetGraphic.drawLine(getWidth() - delta - 1, delta, delta, getHeight() - delta - 1);
			targetGraphic.dispose();
		}

		/**
		 * We don't want to update UI for this button.
		 */
		@Override
		public void updateUI() {
		}
	}

	private static final long serialVersionUID = 5061553483404794020L;

	/** Original value is 17 */
	public static final int SIZE = 10;

	/** Original value is 6 */
	public static final int DELTA = 1;

	private final JTabbedPane pane;

	/**
	 * 
	 */
	private final static MouseListener buttonMouseListener = new MouseAdapter() {
		/**
		 * 
		 */
		@Override
		public void mouseEntered(final MouseEvent event) {
			Component component = event.getComponent();
			if (component instanceof AbstractButton) {
				AbstractButton button = (AbstractButton) component;
				button.setBorderPainted(true);
			}
		}

		/**
		 * 
		 */
		@Override
		public void mouseExited(final MouseEvent event) {
			Component component = event.getComponent();
			if (component instanceof AbstractButton) {
				AbstractButton button = (AbstractButton) component;
				button.setBorderPainted(false);
			}
		}
	};

	/**
	 * 
	 * @param pane
	 */
	public ButtonTabComponent(final JTabbedPane pane) {
		// unset default FlowLayout' gaps
		super(new FlowLayout(FlowLayout.LEFT, 0, 0));
		if (pane == null) {
			throw new NullPointerException("TabbedPane is null");
		} else {
			this.pane = pane;
			setOpaque(false);

			// make JLabel read titles from JTabbedPane
			JLabel label = new JLabel() {
				private static final long serialVersionUID = -374253575101128807L;

				/**
				 * 
				 */
				@Override
				public String getText() {
					String result;

					int i = pane.indexOfTabComponent(ButtonTabComponent.this);
					if (i == -1) {
						result = null;
					} else {
						result = pane.getTitleAt(i);
					}

					//
					return result;
				}
			};

			add(label);
			// add more space between the label and the button
			label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
			// tab button
			JButton button = new TabButton();
			add(button);
			// add more space to the top of the component
			setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
		}
	}
}
