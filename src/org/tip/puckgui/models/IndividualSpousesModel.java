package org.tip.puckgui.models;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puckgui.InputSettings.CheckLevel;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.util.ComboBoxIds;
import org.tip.puckgui.views.IndividualsPanel;

/**
 * 
 * @author TIP
 */
public class IndividualSpousesModel extends AbstractTableModel {
	private static final long serialVersionUID = -8018474967949155510L;
	private static final Logger logger = LoggerFactory.getLogger(IndividualSpousesModel.class);
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.tip.puckgui.messages");

	private static ImageIcon smallFemaleIcon = new ImageIcon(IndividualSpousesModel.class.getResource("/org/tip/puckgui/images/female-16x16.png"));
	private static ImageIcon smallMaleIcon = new ImageIcon(IndividualSpousesModel.class.getResource("/org/tip/puckgui/images/male-16x16.png"));
	private static ImageIcon smallUnknowIcon = new ImageIcon(IndividualSpousesModel.class.getResource("/org/tip/puckgui/images/unknown-16x16.png"));
	private static ImageIcon smallUnmarriedIcon = new ImageIcon(IndividualSpousesModel.class.getResource("/org/tip/puckgui/images/unmarried-x16.png"));
	private static ImageIcon smallMarriedIcon = new ImageIcon(IndividualSpousesModel.class.getResource("/org/tip/puckgui/images/married-x16.png"));
	private static ImageIcon smallDivorcedIcon = new ImageIcon(IndividualSpousesModel.class.getResource("/org/tip/puckgui/images/divorced-x16.png"));

	private NetGUI netGUI;
	private Individual source;
	private List<Individual> spouses;
	private List<Family> families;

	/**
	 * 
	 */
	public IndividualSpousesModel(final NetGUI netGUI, final Individual source) {
		super();
		this.netGUI = netGUI;
		this.spouses = new ArrayList<Individual>();
		this.families = new ArrayList<Family>();
		initialize(source);
	}

	/**
	 * 
	 */
	public void escapeNewEdition() {
		if (isNewEditionOn()) {
			logger.debug("ESCAPE d");
			int index = this.spouses.size() - 1;
			this.spouses.remove(index);
			fireTableRowsDeleted(index, index);
		}
	}

	/**
	 * 
	 */
	@Override
	public Class getColumnClass(final int columnIndex) {
		Class result;

		switch (columnIndex) {
			case 0:
				result = Number.class;
			break;

			case 1:
				result = ImageIcon.class;
			break;

			case 2:
				result = String.class;
			break;

			case 3:
				result = Number.class;
			break;

			case 4:
				result = ImageIcon.class;
			break;

			case 5:
				result = Number.class;
			break;

			default:
				result = String.class;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getColumnCount() {
		int result;

		result = 6;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String getColumnName(final int columnIndex) {
		String result;

		switch (columnIndex) {
			case 0:
				result = BUNDLE.getString("MainWindow.individual.id");
			break;

			case 1:
				result = BUNDLE.getString("MainWindow.individual.gender");
			break;

			case 2:
				result = BUNDLE.getString("MainWindow.individual.name");
			break;

			case 3:
				result = BUNDLE.getString("MainWindow.individual.position");
			break;

			case 4:
				result = BUNDLE.getString("MainWindow.individual.married");
			break;

			case 5:
				result = BUNDLE.getString("MainWindow.individual.family");
			break;

			default:
				result = "";
		}

		//
		return result;
	}

	/**
	 * 
	 * @param rowIndex
	 * @return
	 */
	public Family getFamily(final int rowIndex) {
		Family result;

		if ((this.families == null) || (rowIndex < 0) || (rowIndex >= this.families.size())) {
			//
			result = null;

		} else {
			//
			result = this.families.get(rowIndex);
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getRowCount() {
		int result;

		if (this.source == null) {
			//
			result = 0;

		} else {
			//
			result = this.spouses.size();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individual getSource() {
		Individual result;

		result = this.source;

		//
		return result;
	}

	/**
	 * 
	 * @param rowIndex
	 * @return
	 */
	public Individual getSpouse(final int rowIndex) {
		Individual result;

		if ((this.families == null) || (rowIndex < 0) || (rowIndex >= this.families.size())) {
			//
			result = null;

		} else {
			//
			Family family = this.families.get(rowIndex);

			result = family.getOtherParent(this.source);
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Object getValueAt(final int rowIndex, final int columnIndex) {
		Object result;

		if (this.source == null) {
			//
			result = null;

		} else if ((isNewEditionOn() && (rowIndex == this.spouses.size() - 1))) {
			//
			result = "";

		} else {
			//
			switch (columnIndex) {
				case 0:
					if (this.spouses.get(rowIndex) == null) {
						//
						result = "---";

					} else {
						//
						result = this.spouses.get(rowIndex).getId();
					}
				break;

				case 1:
					if (this.spouses.get(rowIndex) == null) {
						//
						result = smallUnknowIcon;

					} else {
						//
						switch (this.spouses.get(rowIndex).getGender()) {
							case FEMALE:
								result = smallFemaleIcon;
							break;

							case MALE:
								result = smallMaleIcon;
							break;

							case UNKNOWN:
								result = smallUnknowIcon;
							break;

							default:
								result = smallUnknowIcon;
						}
					}
				break;

				case 2:
					if (this.spouses.get(rowIndex) == null) {
						result = "Unknown";
					} else {
						result = this.spouses.get(rowIndex).getName();
					}
				break;

				case 3:
					result = this.families.get(rowIndex).getUnionOrder(this.source);
				break;

				case 4:
					switch (this.families.get(rowIndex).getUnionStatus()) {
						case UNMARRIED:
							result = smallUnmarriedIcon;
						break;

						case MARRIED:
							result = smallMarriedIcon;
						break;

						case DIVORCED:
							result = smallDivorcedIcon;
						break;

						default:
							result = smallUnknowIcon;
					}
				break;

				case 5:
					result = this.families.get(rowIndex).getId();
				break;

				default:
					result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void initialize(final Individual source) {
		//
		this.source = source;

		//
		this.spouses.clear();
		this.families.clear();

		//
		if (source != null) {
			//
			this.families.addAll(this.source.getPersonalFamilies().toListSortedByOrder(source));

			// Compute existing spouses.
			for (Family family : this.families) {
				this.spouses.add(family.getOtherParent(source));
			}
		}
	}

	/**
	 * 
	 */
	@Override
	public boolean isCellEditable(final int row, final int col) {
		boolean result;

		switch (col) {
			case 0:
			case 3:
				result = true;
			break;

			default:
				result = false;
		}

		//
		return result;
	}

	/**
	 * This method indicates if a new line is editing.
	 * 
	 * @return
	 */
	public boolean isNewEditionOn() {
		boolean result;

		if (this.source == null) {
			//
			result = false;

		} else if (this.spouses.size() == this.families.size() + 1) {
			//
			result = true;

		} else {
			//
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public void setNewItem() {
		//
		this.spouses.add(new Individual(0, "?", Gender.UNKNOWN));
		fireTableDataChanged();
	}

	/**
	 * 
	 * @param source
	 */
	public void setSource(final Individual source) {
		//
		initialize(source);
		fireTableDataChanged();
	}

	/**
	 * 
	 */
	@Override
	public void setValueAt(final Object value, final int rowIndex, final int columnIndex) {
		logger.debug("setValueAt(" + rowIndex + ", " + columnIndex + ", " + value + ")");

		//
		if (this.netGUI != null) {
			if (columnIndex == 0) {
				// In case of new partner created with id lesser than
				// the current individual, the current index
				// value becomes wrong. So we need to save the
				// previous one.
				Individual currentIndividual = this.netGUI.selectedIndividual();

				//
				if (isNewEditionOn()) {
					Individual newSpouse = ComboBoxIds.getOrCreateIndividualFromInput(this.netGUI.getNet(), value, Gender.UNKNOWN);
					if (newSpouse == null) {
						//
						if (this.source.isMale()) {
							this.netGUI.getNet().createFamily(this.source, null);
						} else {
							this.netGUI.getNet().createFamily(null, this.source);
						}

						//
						this.netGUI.setChanged(true);
						this.netGUI.updateAll();
					} else {
						// Check undefinedRoles.
						String errorMessage = IndividualsPanel.controlFixedPartners(CheckLevel.ERROR, this.source, newSpouse, this.source.children()
								.toArray());
						if (errorMessage != null) {
							JOptionPane.showMessageDialog(null, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
						} else {
							// Check warning.
							String warningMessage = IndividualsPanel.controlFixedPartners(CheckLevel.WARNING, this.source, newSpouse, this.source.children()
									.toArray());
							if (warningMessage != null) {
								JOptionPane.showMessageDialog(null, warningMessage, "Warning", JOptionPane.WARNING_MESSAGE);
							}

							//
							Family newFamily = this.netGUI.getNet().families().getBySpouses(this.source, newSpouse);
							if (newFamily == null) {
								//
								if (NetUtils.isRolesFixedByGender(this.source, newSpouse)) {
									newFamily = this.netGUI.getNet().createFamily(this.source, newSpouse);
								} else {
									newFamily = this.netGUI.getNet().createFamily(newSpouse, this.source);
								}
								newFamily.setUnionStatus(this.netGUI.getDefaultUnionStatus());

								//
								this.netGUI.setChanged(true);
								this.netGUI.updateAll();

								// In case of new partner created with id lesser
								// than
								// the current individual, the current index
								// value becomes wrong. So we need to set the
								// previous one.
								this.netGUI.selectIndividualsTab(currentIndividual);
							} else {
								JOptionPane.showMessageDialog(null, "Partners already defined.", "Error", JOptionPane.ERROR_MESSAGE);
								fireTableDataChanged();
							}
						}
					}
				} else {
					//
					Individual oldSpouse = this.spouses.get(rowIndex);
					Family oldFamily = this.families.get(rowIndex);
					Individual newSpouse = ComboBoxIds.getOrCreateIndividualFromInput(this.netGUI.getNet(), value, Gender.UNKNOWN);

					//
					if (newSpouse == null) {
						//
						NetUtils.removeSpouse(oldFamily, oldSpouse);

						//
						this.netGUI.setChanged(true);
						this.netGUI.updateAll();

					} else {
						// Check undefinedRoles.
						String errorMessage = IndividualsPanel.controlFixedPartners(CheckLevel.ERROR, this.source, newSpouse, this.source.children()
								.toArray());
						if (errorMessage != null) {
							JOptionPane.showMessageDialog(null, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
						} else {
							// Check warning.
							String warningMessage = IndividualsPanel.controlFixedPartners(CheckLevel.WARNING, this.source, newSpouse, this.source.children()
									.toArray());
							if (warningMessage != null) {
								JOptionPane.showMessageDialog(null, warningMessage, "Warning", JOptionPane.WARNING_MESSAGE);
							}

							//
							if (newSpouse != oldSpouse) {
								// Replace spouse.
								Family newFamily = this.netGUI.getNet().families().getBySpouses(this.source, newSpouse);
								if (newFamily == null) {
									//
									if (oldFamily.getFather() == this.source) {
										NetUtils.setKinMother(oldFamily, newSpouse);
									} else {
										NetUtils.setKinFather(oldFamily, newSpouse);
									}

									//
									this.netGUI.setChanged(true);
									this.netGUI.updateAll();

									// In case of new partner created with id
									// lesser
									// than
									// the current individual, the current index
									// value becomes wrong. So we need to set
									// the
									// previous one.
									this.netGUI.selectIndividualsTab(currentIndividual);
								} else {
									JOptionPane.showMessageDialog(null, "New partner already defined.", "Error", JOptionPane.ERROR_MESSAGE);
								}
							}
						}
					}
				}
			} else if (columnIndex == 3) {
				//
				Integer newUnionOrderValue;
				if (value instanceof String) {
					//
					if (NumberUtils.isDigits((String) value)) {
						//
						newUnionOrderValue = Integer.parseInt((String) value);

					} else {
						//
						newUnionOrderValue = null;
					}
				} else if (value instanceof Integer) {
					//
					newUnionOrderValue = (Integer) value;

				} else {
					//
					newUnionOrderValue = null;
				}

				//
				if ((newUnionOrderValue == null) || (newUnionOrderValue > 0)) {
					//
					Family family = this.families.get(rowIndex);

					//
					family.setUnionOrder(this.source, newUnionOrderValue);

					//
					this.netGUI.setChanged(true);
					this.netGUI.updateAll();
					this.netGUI.selectIndividualsTab(this.source);
				}
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	public Individual source() {
		Individual result;

		result = this.source;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Individual> spouses() {
		List<Individual> result;

		result = this.spouses;

		//
		return result;
	}
}
