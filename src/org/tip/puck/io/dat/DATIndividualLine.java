package org.tip.puck.io.dat;

import java.util.List;

/**
 * This class represents a line of individual from a TXT file.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class DATIndividualLine {
	public static final int NOID = 0;
	public static final int MAX_LINE_SIZE = 2048;

	protected int id;
	protected String name;
	protected char gender;
	protected int fatherId;
	protected int motherId;
	protected List<Integer> spouseIds;
	
	protected int originFamilyId;
	protected List<Integer> personalFamilyIds;
	
		
	/**
	 * 
	 */
	public DATIndividualLine() {
		this.gender = 'X';
	}

	public int fatherId() {
		return fatherId;
	}

	public char gender() {
		return gender;
	}

	public int id() {
		return id;
	}

	public int motherId() {
		return motherId;
	}

	public String name() {
		return name;
	}

	public void setFatherId(final int fatherId) {
		this.fatherId = fatherId;
	}

	public void setGender(final char gender) {
		this.gender = gender;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public void setMotherId(final int motherId) {
		this.motherId = motherId;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public List<Integer> spouseIds() {
		return spouseIds;
	}

	public int originFamilyId() {
		return originFamilyId;
	}

	public void setOriginFamilyId(int originFamilyId) {
		this.originFamilyId = originFamilyId;
	}

	public List<Integer> personalFamilyIds() {
		return personalFamilyIds;
	}

}
