package org.tip.puck.mas;

public class WFAgnaticCousins implements WeightFactor {
    private double wgtCousin1;
    private double wgtCousin2;
    
    public WFAgnaticCousins(double wgtCousin1, double wgtCousin2) {
        this.wgtCousin1 = wgtCousin1;
        this.wgtCousin2 = wgtCousin2;
    }
    
    public String factorName(){
    	return "agnaticcousins";
    }
    
    public double getWgtCousin1() {
		return wgtCousin1;
	}

	public void setWgtCousin1(double wgtCousin1) {
		this.wgtCousin1 = wgtCousin1;
	}

	public void setWgtCousin2(double wgtCousin2) {
		this.wgtCousin2 = wgtCousin2;
	}

	public double getWgtCousin2() {
		return wgtCousin2;
	}

	@Override
    public double factor(Agent mother, Agent father) {
        if (father.hasAgnaticCousin(mother, 1)) {
            return wgtCousin1;
        }
        else if (father.hasAgnaticCousin(mother, 2)) {
            return wgtCousin2;
        }
        else {
            return 1.0;
        }
    }
    
    @Override
    public boolean appliesToMarriage() {return false;}
    
    @Override
    public boolean appliesToDivorce() {return false;}
}