package org.tip.puck.graphs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.tip.puck.graphs.workers.NodeValuator;
import org.tip.puck.matrix.Matrix;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionMaker;
import org.tip.puck.util.Value;
import org.tip.puck.util.Values;

public class GraphMaker {
	
	public static Graph<Node<String>> createGraph(String name, Matrix matrix, Map<Value,Double> vector, String vectorLabel){
		Graph<Node<String>> result;
		
		result = new Graph<Node<String>>(name);
		
		for (int row=0;row<matrix.getRowDim();row++){
			Node<String> node = new Node<String>(row,matrix.getRowLabel(row));
			Double value = vector.get(new Value(matrix.getRowLabel(row)));
			if (value==null){
				node.setAttribute(vectorLabel,"0.");
			} else {
				node.setAttribute(vectorLabel, value+"");
			}
			result.addNode(row,node);
		}
		
		for (int row=0;row<matrix.getRowDim();row++){
			for (int col=0;col<matrix.getRowDim();col++){
				if (matrix.get(row, col) > 0){
					result.addArcWeight(row, col, matrix.get(row,col));
				}
			}
		}

		
		//
		return result;
	}

	public static <E> Map<String,Map<Value,Integer>> getPartitionNumbersMaps(List<String> labels, Graph<E> model){
		Map<String,Map<Value,Integer>> result;
		
		result = new HashMap<String,Map<Value,Integer>>();
		
		for (String label : labels){
			
			Values values = NodeValuator.get(model, label);
			if (!values.isNumeric()) {
				Partition<Value> partition = PartitionMaker.create(label, values);
				result.put(label, PartitionMaker.getPartitionNumbersMap(partition));
			}
	
		}
		
		//
		return result;
		
	}
	
	public static <E> Map<Value,Graph<E>> createSubgraphsByLineTags (Graph<E> source){
		Map<Value,Graph<E>> result;
		
		result = new TreeMap<Value,Graph<E>>();
		Partition<Link<E>> linkPartition = new Partition<Link<E>>();
		
		for (Link<E> link : source.getLinks()){
			for (String tag : link.getTags()){
				linkPartition.put(link, Value.valueOf(tag));
			}
		}
		
		for (Cluster<Link<E>> cluster: linkPartition.getClusters()){
			
			Graph<E> subGraph = new Graph<E>(source.getLabel()+" "+cluster.getValue());
			subGraph.addLinks(new Links<E>(cluster.getItems()));
			result.put(cluster.getValue(), subGraph);
		}
		
		//
		return result;
	}
	
	public static <E> Graph<E> extractSubgraph (Graph<E> source, List<Node<E>> nodes, String label){
		Graph<E> result;
		
		result = new Graph<E>(label);
		
		for (Node<E> node : nodes){
			
			result.addNode(node.getReferent());
		}
		
		for (Link<E> link : source.getLinks()){
			
			if (nodes.contains(link.getSourceNode()) && nodes.contains(link.getTargetNode())){
				
				result.addLink(link.getSourceReferent(), link.getTargetReferent(), link.getType(), link.getWeight());
			}
		}
		//
		return result;
	}
	
	public static <E> Graph<E> removeLoops (Graph<E> source){
		Graph<E> result;
		
		result = new Graph<E>(source.getLabel()+"_NOLOOPS");
		
		for (Node<E> node : source.getNodes()){
			
			result.addNode(node.getReferent());
		}
		
		for (Link<E> link : source.getLinks()){
			
			if (!link.isLoop()){
				
				result.addLink(link.getSourceReferent(), link.getTargetReferent(), link.getType(), link.getWeight());
			}
		}
		//
		return result;
	}
	
	public static<E> Graph<E> reduce (Graph<E> source, int minDegree){
		Graph<E> result;
		
		List<Node<E>> nodes = new ArrayList<Node<E>>();
		
		for (Node<E> node : source.getNodes()){
			
			if (node.getDegree()>=minDegree) {
				
				nodes.add(node);
			}
		}
		
		result = extractSubgraph(source, nodes, source.getLabel()+"_MINDEG_"+minDegree);
		
		//
		return result;
	}
	
	public static<E> Graph<E> getCyclicCore (Graph<E> source){
		Graph<E> result;
		
		Graph<E> previousResult = GraphMaker.removeLoops(source);
		result = reduce(previousResult,2);

		while (result.nodeCount()<previousResult.nodeCount()){
			
			previousResult = result;
			result = reduce(previousResult,2);
		}
		//
		return result;
	}
	
	
	
	
	

}
