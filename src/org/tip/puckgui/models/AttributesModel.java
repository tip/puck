package org.tip.puckgui.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.AttributeComparator;
import org.tip.puck.net.Attributes;
import org.tip.puckgui.WindowGUI;

import fr.devinsy.util.StringSet;

/**
 * 
 * @author TIP
 */
public class AttributesModel extends AbstractTableModel {

	private static final long serialVersionUID = 6819358642786087609L;

	private static final Logger logger = LoggerFactory.getLogger(AttributesModel.class);

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.tip.puckgui.messages");
	private WindowGUI guim;
	private Attributes source;
	private List<Attribute> attributes;
	private StringSet attributeTemplates;

	// FIXME Temporary glue 20130502.
	private static boolean temporaryGlueActivated = false;

	/**
	 * 
	 */
	public AttributesModel(final WindowGUI guiManager, final Attributes source, final StringSet attributeTemplates) {
		super();

		this.guim = guiManager;
		this.source = source;
		if (source == null) {
			//
			this.attributes = new ArrayList<Attribute>();

		} else {
			//
			this.attributes = this.source.toSortedList();
			if ((this.attributes.size() > 0) && (this.attributes.get(0).getLabel().equals(""))) {
				//
				this.attributes.remove(0);
				this.attributes.add(new Attribute("", ""));
			}
		}
		this.attributeTemplates = attributeTemplates;
	}

	/**
	 * 
	 */
	@Override
	public Class getColumnClass(final int columnIndex) {
		Class result;

		if (this.source == null) {
			result = String.class;
		} else {
			switch (columnIndex) {
				case 0:
					result = String.class;
				break;
				case 1:
					result = String.class;
				break;
				default:
					result = String.class;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getColumnCount() {
		int result;

		result = 2;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String getColumnName(final int columnIndex) {
		String result;

		switch (columnIndex) {
			case 0:
				result = BUNDLE.getString("MainWindow.individual.label");
			break;
			case 1:
				result = BUNDLE.getString("MainWindow.individual.value");
			break;
			default:
				result = "";
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getRowCount() {
		int result;

		result = this.attributes.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Attributes getSource() {
		Attributes result;

		result = this.source;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Object getValueAt(final int rowIndex, final int columnIndex) {
		Object result;

		if (this.source == null) {
			result = null;
		} else {
			switch (columnIndex) {
				case 0:
					result = this.attributes.get(rowIndex).getLabel();
				break;
				case 1:
					result = this.attributes.get(rowIndex).getValue();
				break;
				default:
					result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public boolean isCellEditable(final int rowIndex, final int columnIndex) {
		boolean result;

		result = true;

		//
		return result;
	}

	/**
	 * 
	 */
	public void removeBlankAttributes() {

		//
		if (this.source != null) {
			//
			this.source.removeBlankAttributes();

			//
			setSource(this.source);
		}
	}

	/**
	 * 
	 */
	public void setNewItem() {
		this.attributes.add(new Attribute("", ""));
		fireTableDataChanged();
	}

	/**
	 * 
	 * @param source
	 */
	public void setSource(final Attributes source) {

		this.source = source;
		if (source == null) {
			//
			this.attributes = new ArrayList<Attribute>();

		} else {
			//
			this.attributes = this.source.toSortedList();
			if ((this.attributes.size() > 0) && (this.attributes.get(0).getLabel().equals(""))) {
				//
				this.attributes.remove(0);
				this.attributes.add(new Attribute("", ""));
			}
		}

		// FIXME Temporary glue 20130502.
		if (temporaryGlueActivated) {
			//
			String[] extendedLabels = { "COMPL", "SHEET", "NOTE", "CODEUR", "ALIVE", "INFO", "ORDH", "ORDW", "RENVOI", "MARTYPE", "DIV" };
			for (String extendedLabel : extendedLabels) {
				//
				if ((this.source == null) || (!this.source.containsKey(extendedLabel))) {
					//
					this.attributes.add(new Attribute(extendedLabel, ""));
				}
			}
			Collections.sort(this.attributes, new AttributeComparator());
		}

		//
		fireTableDataChanged();
	}

	/**
	 * 
	 */
	@Override
	public void setValueAt(final Object value, final int rowIndex, final int columnIndex) {
		logger.debug("setValueAt " + rowIndex + " " + columnIndex);

		//
		Attribute currentAttribute = this.attributes.get(rowIndex);
		if (columnIndex == 0) {
			//
			if (rowIndex == this.source.size()) {
				if (StringUtils.isBlank((String) value)) {
					this.attributes.remove(rowIndex);
					fireTableDataChanged();
				} else {
					this.attributes.get(rowIndex).setLabel((String) value);
					this.source.add(this.attributes.get(rowIndex));
					if (this.attributeTemplates != null) {
						this.attributeTemplates.put((String) value);
					}
				}
			} else {
				if (StringUtils.isBlank((String) value)) {
					this.source.remove(currentAttribute.getLabel());
					setSource(this.source);
				} else {
					this.source.rename(currentAttribute, (String) value);
					if (this.attributeTemplates != null) {
						this.attributeTemplates.put((String) value);
					}
				}
			}

		} else if (columnIndex == 1) {

			if (StringUtils.isBlank((String) value)) {
				this.source.remove(currentAttribute.getLabel());
				setSource(this.source);
			} else {
				currentAttribute.setValue((String) value);

				// FIXME Temporary glue 20130502.
				if (temporaryGlueActivated) {
					if (StringUtils.isNotBlank(currentAttribute.getLabel())) {
						if (!this.source.containsKey(currentAttribute.getLabel())) {
							this.source.add(currentAttribute);
						}
					}
				}
			}

		}

		//
		this.guim.setChanged(true);
	}
}
