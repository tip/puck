package org.tip.puck.sequences.workers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.tip.puck.PuckException;
import org.tip.puck.graphs.Graph;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Populatable;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Actors;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.relations.Roles;
import org.tip.puck.net.relations.workers.RelationWorker;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.MultiPartition;
import org.tip.puck.partitions.Partition;
import org.tip.puck.report.ReportTable;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.EgoSequence;
import org.tip.puck.sequences.EgoSequences;
import org.tip.puck.sequences.EventTriangle;
import org.tip.puck.sequences.Ordinal;
import org.tip.puck.sequences.Sequence;
import org.tip.puck.sequences.Sequenceable;
import org.tip.puck.sequences.Sequenceables;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.ToolBox;
import org.tip.puck.util.Value;

/**
 * 
 * @author Klaus Hamberger
 *
 */
public class SequenceWorker {

	public static List<String> getProfileByTypes (EgoSequence source){
		List<String> result;
		
		result = new ArrayList<String>();
		
		for (Relation event : source.getStations().values()){
			result.add(source.getStationType(event));
		}
		
		//
		return result;
	}
	
	private static Map<Individual, Relation> getRelationsByAlter(Individual ego, String modelName, String egoRoleName, String alterRoleName){
		Map<Individual, Relation> result;

		result = new TreeMap<Individual, Relation>();
		
		for (Relation event : ego.relations().getByModelName(modelName)){
			if (event.getRoleNames(ego).contains(egoRoleName)){
				String year = IndividualValuator.extractYear(event.getAttributeValue("DATE"));
				for (Actor actor : event.actors()){
					if (actor.getRole().getName().equals(alterRoleName)){
						Individual alter = actor.getIndividual();
						Relation oldEvent = result.get(alter);
						String oldYear = null;
						if (oldEvent!=null){
							oldYear = IndividualValuator.extractYear(oldEvent.getAttributeValue("DATE"));
						}
						if (oldEvent==null || (year!=null && oldYear!=null && year.compareTo(oldYear)<0)){
							result.put(alter, event);
						}
					}
				}
			}
		}
		
		//
		return result;
	}
	
	public static String order(Relation event, Individual ego){
		String result;
		
		result = "";
		
		for (Actor actor : event.actors().getById(ego.getId())) {
			String order = actor.attributes().getValue("ORDER");
			if (order!=null){
				result = order;
				break;
			}
		}
		//
		return result;
	}
	

/*	public static List<Place> getPlaceList(EgoSequence source, Geography geography, GeoLevel level){
		List<Place> result;
		
		//
		result = new ArrayList<Place>();
		
		for (Relation relation : source.getStations().values()){
			Place place = geography.getPlace(relation.getAttributeValue("END_PLACE"),level);
			if (place==null){
				place = new Place(level,"UNKNOWN");
			}
			result.add(place);
		}
		//
		return result;
	}*/
	

	
	/**
	 * replace by SequenceCensus setEventTypes
	 * @param source
	 * @param criteria
	 * @deprecated
	 */
/*	public static void setEventTypes (Sequence source, SequenceCriteria criteria){
		
		source.eventTypes = new HashMap<Relation,String>();
		Geography geography = null;
		List<Place> placeList = null;
		
		if (criteria.getType()==EventType.HOST || criteria.getType()==EventType.MIG || criteria.getType()==EventType.HOSTMIG){
			source.setAlters();
			source.setAlterRelations(criteria.getMaxDepths(), criteria.getRelationModelNames());
		} else if (criteria.getType()==EventType.DISTANCE || criteria.getType()==EventType.PLACE || criteria.getType()==EventType.MOVEMENT){
			geography = Geography.getInstance();
			if (criteria.getType()==EventType.MOVEMENT){
				placeList = getPlaceList(source, geography, criteria.getLevel());
			}
		} 

		int i = 0;
		for (Relation event : source.getEvents().values()){
			String type;
			if(event.getAttributeValue("START_PLACE")==null){
				type = "<";
			} else if (event.getAttributeValue("START_PLACE")==null){
				type = ">";
			} else {
				if (criteria.getType()==EventType.HOST){
					type = source.getType(event,"HOST");
				} else if (criteria.getType()==EventType.MIG) {
					type = source.getType(event,"MIG");
				} else if (criteria.getType()==EventType.HOSTMIG) {
					type = source.getType(event,"HOST")+":"+source.getType(event,"MIG");
				} else if (criteria.getType()==EventType.DISTANCE){
					GeoLevel distance = Sequence.getDistance(geography, event);
					if (distance != null){
						type = distance.toString();
					} else {
						type = "UNKNOWN";
					}
				} else if (criteria.getType()==EventType.PLACE){
					Place place = geography.getPlace(event.getAttributeValue("END_PLACE"),criteria.getLevel());
					if (place!=null){
						type = place.getName();
					} else {
						type = "UNKNOWN";
					}
				} else if (criteria.getType()==EventType.MOVEMENT){
					type = getMovement(placeList, i);		
				} else {
					type = null;
				}
			}
			source.eventTypes.put(event, type);
			i++;
			
		}
	}*/
	
	public static double jacquardDistance (Individual alter, Individual ego, String relationModelName, String egoRoleName, String alterRoleName){
		double result;
		
		int totalEvents = 0;
		int totalSharedEvents = 0;
		
		for (Relation event : ego.relations().getByModelName(relationModelName)){
			if (event.getIndividuals(egoRoleName).contains(ego)){
				totalEvents++;
				if (event.getIndividuals(alterRoleName).contains(alter) || ((alterRoleName.equals("ALL") && event.getIndividuals().contains(alter)))){
					totalSharedEvents++;
				}
			}
		}
		for (Relation event : alter.relations().getByModelName(relationModelName)){
			if (event.getIndividuals(alterRoleName).contains(alter) || ((alterRoleName.equals("ALL") && event.getIndividuals().contains(alter)))){
				totalEvents++;
			}
		}
		totalEvents = totalEvents - totalSharedEvents;
		
		result = MathUtils.percent(totalSharedEvents,totalEvents);
				
		//
		return result;
		
	}
	
	public static Partition<EventTriangle> getTriangles(Individuals individuals, String relationModelName){
		Partition<EventTriangle> result;
		
		result = new Partition<EventTriangle>();
		
		String[][] types = new String[][]{{"HOST","MIG"},{"MIG","HOST"},{"MIG","MIG"}};
		
		for (Individual indi1 : individuals){
			for (String[] type1 : types){
				Map<Individual,Relation> events1 = getRelationsByAlter(indi1, relationModelName, type1[0], type1[1]);
				if (events1!=null){
					for (Individual indi2 : events1.keySet()){
						if (indi2.getId()<indi1.getId()){
							for (String[] type2 : types){
								Map<Individual,Relation> events2 = getRelationsByAlter(indi2, relationModelName, type2[0], type2[1]);
								if (events2!=null){
									for (Individual indi3 : events2.keySet()){
										if (indi3==indi1){
											if (!type1[0].equals(type2[1]) || !type1[1].equals(type2[0])){
												EventTriangle dyad = new EventTriangle(new Individual[]{indi1,indi2},new String[][]{type1,type2},new Relation[]{events1.get(indi2), events2.get(indi3)}).sortByYear(); 
												result.put(dyad, new Value(dyad.getRolePattern()));
											}
										} else if (indi3!=indi1 && indi3!=indi2){
											for (String[] type3 : types){
												Map<Individual,Relation> events3 = getRelationsByAlter(indi3,relationModelName, type3[0], type3[1]);
												if (events3!=null && events3.containsKey(indi1)){
													EventTriangle triangle = new EventTriangle(new Individual[]{indi1,indi2,indi3},new String[][]{type1,type2,type3},new Relation[]{events1.get(indi2), events2.get(indi3), events3.get(indi1)}).sortByYear();
													result.put(triangle, new Value(triangle.getRolePattern()));
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		//
		return result;
	}	
	
	public static Partition<String> getEventPartition (EgoSequences source){
		Partition<String> result;
		
		result = new Partition<String>();
		result.setLabel("Events "+source.getLabel());
		
		for (EgoSequence biography : source){
			for (Relation event : biography.getStations().values()){
				result.put(biography.getEgo().getId()+" "+event.getTypedId(), new Value(biography.getStationType(event)));
			}
		}
		
		//
		return result;
	}
	
	public static Partition<String> getSubsequencePartition (EgoSequences source){
		Partition<String> result;
		
		result = new Partition<String>();
		result.setLabel("Subsequences "+source.getLabel());
		
		for (EgoSequence biography : source){
			String type = null;
			for (Relation event : biography.getStations().values()){
				if (type == null){
					type = biography.getStationType(event);
				} else {
					type += "-"+biography.getStationType(event);
				}
				result.put(biography.getEgo().getId()+" "+event.getTypedId(), new Value(type));
			}
		}
		
		//
		return result;
	}
	
	public static Graph<Cluster<String>> getSequenceNetwork (EgoSequences source, Partition<String> partition){
		Graph<Cluster<String>> result;
		
		if (source == null) {
			throw new IllegalArgumentException("Null parameter detected.");
		} else {
			
			result = new Graph<Cluster<String>>("Network " + source.getLabel());
			
			//
			for (Cluster<String> cluster : partition.getClusters().toListSortedByDescendingSize()) {
				if (!cluster.isNull()) {
					result.addNode(cluster);
				}
			}
			
			//
			for (EgoSequence biography : source){
				Cluster<String> previous = null;
				for (Relation event : biography.getStations().values()){
					Cluster<String> next = partition.getCluster(biography.getEgo().getId()+" "+event.getTypedId());
					if (previous!=null){
						result.incArcWeight(previous, next);
					}
					previous = next;
				}
			}
		}
		
		//
		return result;
	}
	
	public static Partition<String> getSequencePartition (EgoSequences source){
		Partition<String> result;
		
		result = new Partition<String>();
		result.setLabel("Sequences "+source.getLabel());
		
		for (EgoSequence biography : source){
			String previousType = null;
			for (Relation event : biography.getStations().values()){
				if (previousType == null){
					previousType = biography.getStationType(event);
				} else {
					String nextType = biography.getStationType(event);
					result.put(biography.getEgo().getId()+" "+event.getTypedId(), new Value(new String[]{previousType,nextType}));
					previousType = nextType;
				}
			}
		}
		
		
		//
		return result;
	}
	
	public static EgoSequences getCoherentItineraries (Segmentation segmentation, SequenceCriteria criteria){
		EgoSequences result;
		
		result = new EgoSequences();
		for (Individual ego : segmentation.getCurrentIndividuals().toSortedList()) {
			result.addRenumbered(SequenceWorker.getCoherentItinerarySegment(ego, segmentation, criteria));
		}

		//
		return result;
	}
	
	
	public static EgoSequence getCoherentItinerarySegment (Individual ego, Segmentation segmentation, SequenceCriteria criteria){
		EgoSequence result;
		
		int minAge = criteria.getMinAge();
		int maxAge = criteria.getMaxAge();
		
		EgoSequence fullBiography = SequenceMaker.createPersonalEventSequence(ego, segmentation, criteria);
		EgoSequence sortedBiography = split(fullBiography).getById(1);
		result = truncate(sortedBiography, minAge, maxAge);

		//
		return result;
	}
	
	public static boolean follows (Relation firstEvent, Relation secondEvent){
		boolean result;
		
		result = false;
		
		if (firstEvent!=null && secondEvent!=null && firstEvent!=secondEvent){
			String arrival = firstEvent.getAttributeValue("END_PLACE");
			String departure = secondEvent.getAttributeValue("START_PLACE");
			String firstDate = firstEvent.getAttributeValue("DATE");
			String secondDate = secondEvent.getAttributeValue("DATE");
			if (firstDate!=null && secondDate!=null && firstDate.compareTo(secondDate)>0) {
				result = false;
			} else {
				result = (arrival !=null && arrival.equals(departure));
			}
		}
		//
		return result;
	}
	
	public static Ordinal getFollowingKey (Ordinal currentKey, EgoSequence itinerary, Set<Ordinal> filter){
		Ordinal result;
		
		result = getFirstKey(itinerary, filter);

		while (result!=null && !follows(itinerary.getStation(currentKey),itinerary.getStation(result))) {
			result = itinerary.getNextFreeTime(result, filter);
		}
		//
		return result;
	}
	
	public static Ordinal getFirstKey (EgoSequence itinerary, Set<Ordinal> keySet){
		Ordinal result;
		
		result = null;
		
		for (Ordinal key : itinerary.getStations().keySet()){
			if (!keySet.contains(key)){
				result = key;
				break;
			}
		}
		//
		return result;
	}
	
	public static EgoSequence truncate (EgoSequence source, int minAge, int maxAge){
		EgoSequence result;
		
		result = new EgoSequence(source.getEgo(),source.getId());
//		result.setEgoRoleName(source.getEgoRoleName());
		
		for (Ordinal key : source.getStations().keySet()){
			Relation event = source.getStation(key);
			if (key.getYear()!=null && source.getEgoAge(key.getYear())>=minAge && source.getEgoAge(key.getYear())<maxAge){
				result.put(key, event);
			}
		}
		
//		result.setAlters();
		
		//
		return result;
	}
	
	public static EgoSequences split (EgoSequence source){
		EgoSequences result;
		
		result = new EgoSequences();
		
		SequenceCriteria criteria = new SequenceCriteria();
				
		EgoSequence partialSequence = new EgoSequence(source.getEgo(),result.size()+1);
//		partialSequence.setEgoRoleName(source.getEgoRoleName());
//		partialSequence.setDateLabel(source.dateLabel());
		
		String previousEndPlace = null;
		
		for (Ordinal key : source.getStations().keySet()){
			
			Relation event = source.getStation(key);
			
			if (previousEndPlace!=null){
				String currentStartPlace = event.getAttributeValue(criteria.getStartPlaceLabel());
				if (!previousEndPlace.equals(currentStartPlace)){
					result.add(partialSequence.clone());
					partialSequence = new EgoSequence(source.getEgo(),result.size()+1); 
//					partialSequence.setEgoRoleName(source.getEgoRoleName());
//					partialSequence.setDateLabel(source.dateLabel());
}
			}
			partialSequence.getStations().put(key, event);

//			partialSequence.putInOrder(event, previousEndPlace, SequenceType.ALL_EVENTS, true, criteria);
			previousEndPlace = event.getAttributeValue(criteria.getEndPlaceLabel());
		}
		result.add(partialSequence);
		
		for (EgoSequence sequence : result){
//			sequence.alters = source.alters;
//			sequence.alterRelations = source.alterRelations;
			sequence.setStationTypes(source.getStationTypes());
		}
		
		//
		return result;
	}

/*	public static Sequences sortAndSplit (Sequence source){
		Sequences result;
		
		result = new Sequences();
				
		Sequence preResult = new Sequence(result.size()+1);
		preResult.setEgo(source.getEgo());
		preResult.setEgoRoleName(source.getEgoRoleName());
		preResult.setDateLabel(source.dateLabel());
		
		Ordinal currentKey = null;
		Relation currentEvent = null;
		Set<Ordinal> sortedKeys = new HashSet<Ordinal>();
		
		for (Ordinal key : source.getEvents().keySet()){
			currentEvent = source.getEvent(key);
			if (currentEvent.getAttributeValue("START_PLACE")==null){
				preResult.putInOrder(currentEvent);
				currentKey = key;
				sortedKeys.add(currentKey);
				break;
			}
		}
		
		while (source.getEvents().size()>sortedKeys.size()) {
			Ordinal nextKey = getFollowingKey(currentKey,source, sortedKeys);
			if (nextKey == null){
				result.add(preResult.clone());
				preResult = new Sequence(result.size()+1); 
				preResult.setEgo(source.getEgo());
				preResult.setEgoRoleName(source.getEgoRoleName());
				nextKey = getFirstKey(source,sortedKeys);
			}
			preResult.putInOrder(source.getEvent(nextKey));
			currentKey = nextKey;
			sortedKeys.add(currentKey);
		}
		result.add(preResult);
		
		for (Sequence sequence : result){
			sequence.alters = source.alters;
			sequence.alterRelations = source.alterRelations;
			sequence.eventTypes = source.eventTypes;
		}
		
		//
		return result;
	}*/
	
	
	public static Relations getCommonEvents (Individual ego, Individual alter, String label){
		Relations result;
		
		Relations egoEvents = ego.relations().getByModelName(label);
		Relations alterEvents = alter.relations().getByModelName(label);
		
		result = new Relations();
		for  (Relation event : egoEvents){
			if (alterEvents.contains(event)){
				result.add(event);
			}
		}
		
		//
		return result;
	}
	
	static Relations getIndividualStations (Individual individual, Ordinal time, SequenceCriteria criteria){
		Relations result;
		
		result = new Relations();
		
		for (Relation relation : individual.relations()){
			
			Integer thisTime = relation.getTime(criteria.getDateLabel());
			String thisRelationModelName = relation.getModel().getName();

			if (time.getYear().equals(thisTime) && criteria.getRelationModelName().equals(thisRelationModelName)){

				result.put(relation);	
			}
		}
		//
		return result;
	}
	
	private static  <S extends Sequenceable<E>,E extends Populatable> String getPresence(Sequenceables<S,E> slices, Individual individual, Ordinal time, Integer unitId, SequenceCriteria criteria){
		String result;
		
		result = null;

		for (Relation relation : getIndividualStations(individual, time, criteria)){
			
			String thisUnitId = relation.getAttributeValue(criteria.getLocalUnitLabel());
			result = "ABSENT";
			
			if (thisUnitId!=null && Integer.parseInt(thisUnitId)==unitId){
				
				result = "PRESENT";
				break;
			}
		}
			
		if (result==null) {
				
			String lifeStatus = SequenceValuator.getLifeStatus(individual,time,null).stringValue();
			
			if (lifeStatus.equals("DEAD") || lifeStatus.equals("UNBORN")){
				
				result = lifeStatus;
				
			} else {
				
				result = "ABSENT"; // or UNKNOWN
			}
		}
		
		//
		return result;
	}
	
	private static  <S extends Sequenceable<E>,E extends Populatable> String[] getPresenceArray (Sequenceables<S,E> slices, Individual individual, Integer unitId, SequenceCriteria criteria){
		String[] result;
		
		result = new String[criteria.getDates().length];
		int i = 0;
		
		for (Ordinal time : Ordinal.getOrdinals(criteria.getDates())){
				
			result[i] = getPresence(slices, individual, time, unitId, criteria);
			i++;
		}
		//
		return result;
	}
	
	static  <S extends Sequenceable<E>,E extends Populatable> int[][] getTurnoverStatistics (Partition<Individual> presencePartition, SequenceCriteria criteria) throws PuckException{
		int[][] result;
		
		int nrChanges = criteria.getDates().length-1;
		
		int[] stay = new int[nrChanges+1];
		int[] in = new int[nrChanges+1];
		int[] out = new int[nrChanges+1];
		int[] all = new int[nrChanges+1];
		
		for (Cluster<Individual> cluster : presencePartition.getClusters()){

			String[] presence = cluster.getValue().stringArrayValue();
			int size = cluster.size();

			for (int i=0;i<nrChanges;i++){
				
				String first = presence[i];
				String second = presence[i+1];
				
				if (first.equals("PRESENT") || second.equals("PRESENT")){
					
					all[i] += size;
					
					if (first.equals("PRESENT") && second.equals("PRESENT")){
						
						stay[i] += size;
						
					} else if (first.equals("PRESENT")){
						
						out[i] += size;
						
					} else {
						
						in[i] += size;
					}
				}
			}
			
			String first = presence[0];
			String last = presence[presence.length-1];
			
			if (first.equals("PRESENT") || last.equals("PRESENT")){
				
				all[nrChanges] += size;
				
				if (first.equals("PRESENT") && last.equals("PRESENT")){
					
					stay[nrChanges] += size;
					
				} else if (first.equals("PRESENT")){
					
					out[nrChanges] += size;
					
				} else {
					
					in[nrChanges] += size;
				}
			}

		}
		//
		result = new int[][]{stay,in,out,all};
		//
		return result;
	}

	
	static  <S extends Sequenceable<E>,E extends Populatable> Partition<Individual> getPresencePartition (Sequenceables<S,E> slices, List<Individual> members, SequenceCriteria criteria) throws PuckException{
		Partition<Individual> result;
		
		result = new Partition<Individual>();
		
		if (slices.isPopulatable()){
			
			for (S sequence : slices.toSortedList()){
				
				for (Individual member : members){
					
					result.put(member, new Value(getPresenceArray(slices, member, sequence.getId(), criteria)));
				}
			}
		}
		//
		return result;
	}
	
	static private String migration (String first, String second){
		String result;
		
		result = null;
		
		if (first != null && second != null){
			
			if (first.equals("PRESENT")){
				
				if (second.equals("PRESENT")){
					
					result = "STAY";
					
				} else if (second.equals("ABSENT")){
					
					result = "OUT";
					
				} else if (second.equals("DEAD")){
					
					result = "DIED";
				}
				
			} else if (first.equals("ABSENT")){
				
				if (second.equals("PRESENT")){
						
					result = "IN";
				}
						
			} else if (first.equals("UNBORN")){
					
				if (second.equals("PRESENT")){
							
					result = "BORN";
				}
			}
		}
		//
		return result;
	}
	
	static <S extends Sequenceable<E>,E extends Populatable> MultiPartition<Individual>[] getChainMobilityPartition (Sequenceables<S,E> slices, List<Individual> members, SequenceCriteria criteria) {
		MultiPartition<Individual>[] result;
		
		MultiPartition<Individual> positions = new MultiPartition<Individual>();
		MultiPartition<Individual> movements = new MultiPartition<Individual>();
		
		result = new MultiPartition[]{positions,movements};

		if (slices.isPopulatable()){
			
			for (S sequence : slices.toSortedList()){
				
				for (Individual individual : members){

					String previousPresence = null;
					String previousChain = "";
					
					for (Ordinal time : Ordinal.getOrdinals(criteria.getDates())){
						
						String presence = getPresence(slices, individual, time, sequence.getId(), criteria);
						
						String chain = "";

						for (Relation station : getIndividualStations(individual, time, criteria)){

							Actor actor = station.actors().getById(individual.getId()).get(0);
							String thisUnitId = station.getAttributeValue(criteria.getLocalUnitLabel());

							if (presence.equals("PRESENT") &&  thisUnitId!=null && Integer.parseInt(thisUnitId)==sequence.getId()){
							
								chain = RelationWorker.getReferentChainGenderString(actor, criteria.getGroupAffiliationLabel(), criteria.getReferentRelationLabel(), station);						
							
							} else if (presence.equals("ABSENT")) {
								
								chain += RelationWorker.getReferentChainGenderString(actor, criteria.getGroupAffiliationLabel(), criteria.getReferentRelationLabel(), station);						
							}
						}
						
						String chainChange = (previousChain+" > "+chain).trim();
						
						String presenceChange = migration(previousPresence,presence);

						if (presenceChange!=null && chainChange!=null){
							
							if (presence.equals("PRESENT")){
								
								positions.put(new Value(chain), new Value(presenceChange), individual);
							
							} else if (previousPresence.equals("PRESENT")){
								
								positions.put(new Value(previousChain), new Value(presenceChange), individual);
							}
							
							movements.put(new Value(chainChange), new Value(presenceChange), individual);
						}
						
						previousChain = chain;
						previousPresence = presence;

					}
				}
			}
		}
		//
		positions.count();
		movements.count();
		//
		return result;
	}

	static  <S extends Sequenceable<E>,E extends Populatable> List<String> getStatus(Sequenceables<S,E> slices, Individual individual, Ordinal time, SequenceCriteria criteria){
			List<String> result;
	
			result = new ArrayList<String>();
			
			for (Relation relation : individual.relations()){
				
				Integer thisTime = relation.getTime(criteria.getDateLabel());
				String thisRelationModelName = relation.getModel().getName();
				
				if (time.getYear().equals(thisTime) && criteria.getRelationModelName().equals(thisRelationModelName)){

					String line = "";
					
					String idValue = relation.getAttributeValue(criteria.getLocalUnitLabel());
					String placeValue = relation.getAttributeValue(criteria.getPlaceLabel());
					
					if (idValue!=null){
						
						line += idValue;
						
					} else if (placeValue!=null){
						
						line += placeValue;
					}
					
					Actors actors = relation.actors().getById(individual.getId());
					if (actors.size()>0){
	
						Actor actor = actors.get(0);
						
						if (actor.getAttributeValue("MODE")!=null){
							line += " "+actor.getAttributeValue("MODE");
						}
	
						line+="\t";
	
						String startDate = actor.getAttributeValue(criteria.getStartDateLabel());
						String endDate = actor.getAttributeValue(criteria.getEndDateLabel());
						String note = actor.getAttributeValue("NOTE");
						
						if (startDate!=null){
							line += startDate;
						}
						if (endDate!=null){
							line += "-"+endDate;
						}
						if (note!=null){
							line += " ["+note+"]";
						}
						
						line+="\t";
						
						Individual referent = actor.getReferent();
						
						if (referent!=null){
							
							line += referent + "\t" + NetUtils.getAlterRoles(individual, referent, ToolBox.stringsToInts(criteria.getPattern()), criteria.getRelationModelNames(), criteria.getChainClassification());
							
						} else {
							
							line+="\t";
						}
						
						line+="\t";

						String chain = RelationWorker.getReferentChainGenderString(actor, criteria.getGroupAffiliationLabel(), criteria.getReferentRelationLabel(), relation);

						if (chain!=null){
							
							line += chain;
						}
					}
					//
					result.add(line);
				}
			}
			
			//
			return result;
		}

	/**
	 * 
	 * @param partitionSequences
	 * @return
	 * @throws PuckException 
	 */
	public static <S extends Sequenceable<Relation>> ReportTable interactionTable(S sequence) throws PuckException {
		ReportTable result;
		
		Individuals individuals = ((Populatable)sequence).getIndividuals();
	
		//
		result = new ReportTable(individuals.size() + 1, individuals.size() + 1);
	
		//
		{
			result.set(0, 0, sequence.getId());
			int columnIndex = 1;
			for (Individual individual: individuals) {
				result.set(0, columnIndex, individual.getName());
				columnIndex += 1;
			}
	
			//
			int rowIndex = 1;
			for (Individual individual1: individuals) {
				result.set(rowIndex, 0, individual1.getName());
				columnIndex = 1;
				for (Individual individual2: individuals) {
					if (columnIndex>=rowIndex){
						break;
					}
					String roles1AsString = "";
					String roles2AsString = "";
					for (Ordinal time : sequence.getTimes()){
						Relation station = sequence.getStation(time);
						Roles roles1 = station.actors().getRoles(individual1.getId());
						Roles roles2 = station.actors().getRoles(individual2.getId());
						if (roles1 != null && roles1.size()>0 && roles2 != null && roles2.size()>0){
							roles1AsString += roles1.toNameList().toString();
							roles2AsString += roles2.toNameList().toString();
						}
					}
					result.set(rowIndex, columnIndex, roles1AsString);
					result.set(columnIndex, rowIndex, roles2AsString);
					columnIndex += 1;
				}
				rowIndex += 1;
			}
		}
		//
		return result;
	}

	/**
	 * 
	 * @param partitionSequences
	 * @return
	 * @throws PuckException 
	 */
	public static <S extends Sequence<Relation>> ReportTable roleTable(S sequence) throws PuckException {
		ReportTable result;
		
		Individuals individuals = ((Populatable)sequence).getIndividuals();
	
		//
		result = new ReportTable(sequence.getNrStations() + 1, individuals.size() + 1);
	
		//
		{
			result.set(0, 0, sequence.getId());
			int columnIndex = 1;
			for (Individual individual: individuals) {
				result.set(0, columnIndex, individual.getName());
				columnIndex += 1;
			}
	
			//
			int rowIndex = 1;
			for (Ordinal time : sequence.getTimes()) {
				result.set(rowIndex, 0, time);
				columnIndex = 1;
				Relation station = sequence.getStation(time);
				for (Individual individual: individuals) {
					String rolesAsString = " - ";
					Roles roles = station.actors().getRoles(individual.getId());
					if (roles != null && roles.size()>0){
						rolesAsString = roles.toNameList().toString();
					}
					result.set(rowIndex, columnIndex, rolesAsString);
					columnIndex += 1;
				}
				rowIndex += 1;
			}
		}
		//
		return result;
	}
	
	

}
