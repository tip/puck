package org.tip.puckgui.views;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.geo.Place;
import org.tip.puck.geo.graphs.GeoNetworkUtils;
import org.tip.puck.geo.workers.GeocodingWorker;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.workers.GraphReporter;
import org.tip.puck.graphs.workers.GraphUtils;
import org.tip.puck.io.dat.DATFile;
import org.tip.puck.io.gis.SIGFile;
import org.tip.puck.io.paj.PAJFile;
import org.tip.puck.net.Individual;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.graphs.ClusterNetworkReporter;
import org.tip.puck.partitions.graphs.ClusterNetworkUtils.AllianceType;
import org.tip.puck.report.Report;
import org.tip.puck.visualization.VisualizationController;
import org.tip.puck.visualization.style.implementations.AllianceNetwork;
import org.tip.puckgui.GroupNetGUI;
import org.tip.puckgui.PuckGUI;
import org.tip.puckgui.WindowGUI;
import org.tip.puckgui.util.ButtonTabComponent;
import org.tip.puckgui.util.GenericFileFilter;
import org.tip.puckgui.views.geo.MapPanel;
import org.tip.puckgui.views.geo.MapPanel_old;
import org.tip.puckgui.views.visualization.VisualizationPanel;

/**
 * 
 * @author TIP
 */
public class GroupNetWindow extends JFrame {

	private static final long serialVersionUID = 1973988817198888692L;
	private static final Logger logger = LoggerFactory.getLogger(GroupNetWindow.class);
	private static final ResourceBundle messages = ResourceBundle.getBundle("org.tip.puckgui.messages"); //$NON-NLS-1$
	private GroupNetGUI gui;
	private JFrame thisFrame;
	private JPanel contentPane;
	private int reportCounter = 0;
	private JTabbedPane tabbedPane;

	/**
	 * Create the frame.
	 */
	public GroupNetWindow(final GroupNetGUI gui) {
		this.thisFrame = this;
		this.gui = gui;

		// //////////////////////////::

		addWindowListener(new WindowAdapter() {
			/**
			 * 
			 */
			@Override
			public void windowClosing(final WindowEvent event) {
				System.out.println("Closing event.");
				performCloseWindow();
			}
		});

		setTitle("Group Network");
		setIconImage(Toolkit.getDefaultToolkit().getImage(GroupNetWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 0, 1020, MainWindow.getDesktopHeight());

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		mnFile.setMnemonic('F');
		menuBar.add(mnFile);

		JMenu mnNew = new JMenu(messages.getString("MainWindow.mnNew.text")); //$NON-NLS-1$
		mnFile.add(mnNew);

		JMenuItem mntmBlankCorpus = new JMenuItem(messages.getString("MainWindow.mntmBlankCorpus.text")); //$NON-NLS-1$
		mntmBlankCorpus.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// New Blank Corpus.
				PuckGUI.instance().createNetGUI();
			}
		});
		mnNew.add(mntmBlankCorpus);

		JMenuItem mntmRandomNetworkby = new JMenuItem(messages.getString("GroupNetWindow.mntmRandomNetworkby.text")); //$NON-NLS-1$
		mntmRandomNetworkby.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Random Alliance Network Agent Variations.
				try {
					RandomAllianceNetworkByRandomDistributionInputWindow window = new RandomAllianceNetworkByRandomDistributionInputWindow(gui);
					window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					window.setVisible(true);
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnNew.add(mntmRandomNetworkby);

		JMenuItem mntmRandomNetworkby_1 = new JMenuItem(messages.getString("GroupNetWindow.mntmRandomNetworkby_1.text")); //$NON-NLS-1$
		mntmRandomNetworkby_1.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				try {
					RandomAllianceNetworkByAgentSimulationInputWindow window = new RandomAllianceNetworkByAgentSimulationInputWindow(gui);
					window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					window.setVisible(true);
				} catch (final Exception exception) {

					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		mnNew.add(mntmRandomNetworkby_1);

		JMenuItem mntmRandomNetworkVariation = new JMenuItem(messages.getString("GroupNetWindow.mntmRandomNetworkVariation.text")); //$NON-NLS-1$
		mntmRandomNetworkVariation.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Random Alliance Network Agent Variations.
				try {
					RandomAllianceNetworkByAgentSimulationVariationsInputWindow window = new RandomAllianceNetworkByAgentSimulationVariationsInputWindow(gui);
					window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					window.setVisible(true);
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnNew.add(mntmRandomNetworkVariation);

		JSeparator separator_1 = new JSeparator();
		mnFile.add(separator_1);

		JMenuItem mntmExportToPajek = new JMenuItem(messages.getString("GroupNetWindow.mntmExportToPajek.text")); //$NON-NLS-1$
		mntmExportToPajek.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				try {
					File targetFile = new File("groupNetExport.paj");
					boolean ended = false;
					while (!ended) {
						JFileChooser chooser = new JFileChooser();
						chooser.setSelectedFile(targetFile);

						chooser.setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.saveAsFileChooser.text"));
						chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
						chooser.setAcceptAllFileFilterUsed(false);
						GenericFileFilter defaultFileFilter = new GenericFileFilter("Pajek files (*.paj)", "paj");
						chooser.addChoosableFileFilter(defaultFileFilter);
						chooser.setFileFilter(defaultFileFilter);

						//
						if (chooser.showSaveDialog(GroupNetWindow.this.thisFrame) == JFileChooser.APPROVE_OPTION) {
							System.out.println("getCurrentDirectory(): " + chooser.getCurrentDirectory());
							System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
							targetFile = chooser.getSelectedFile();

							boolean doSave;
							if (targetFile.exists()) {
								// Manage confirmation dialog.
								String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.title");
								String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.text");

								int response = JOptionPane.showConfirmDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.YES_NO_CANCEL_OPTION);

								if (response == JOptionPane.YES_OPTION) {
									doSave = true;
									ended = true;
								} else if (response == JOptionPane.NO_OPTION) {
									doSave = false;
									ended = false;
								} else {
									doSave = false;
									ended = true;
								}
							} else {
								doSave = true;
								ended = true;
							}

							if (doSave) {
								PAJFile.exportToPajek(gui.getGroupNet(), targetFile.getAbsolutePath(), ClusterNetworkReporter.ALLIANCE_NETWORK_PARTITION_LABELS);
							}

						} else {
							// Nothing to do.
							System.out.println("No Selection ");
							ended = true;
						}
					}
				} catch (final PuckException exception) {
					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedFileFormat");
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.notAFile");
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedEncoding");
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.default");
					}

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JMenuItem mntmImportFromDat = new JMenuItem(messages.getString("GroupNetWindow.mntmImportFromDat.text")); //$NON-NLS-1$
		mntmImportFromDat.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Import from Dat File.
				try {
					//
					File file = selectDatFile(GroupNetWindow.this.thisFrame, null);

					//
					if (file != null) {
						//
						Graph<Cluster<Individual>> graph = DATFile.load(file);

						//
						PuckGUI.instance().createGroupNetGUI(graph);
					}
				} catch (final PuckException exception) {
					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case FILE_NOT_FOUND:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.fileNotFound");
						break;
						case BAD_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.badFileFormat");
						break;
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.unsupportedFileFormat");
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.notAFile");
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.unsupportedEncoding");
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.default");
					}

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnFile.add(mntmImportFromDat);
		mnFile.add(mntmExportToPajek);

		JMenuItem mntmExportToMatrixDatFile = new JMenuItem(messages.getString("GroupNetWindow.mntmMatrixToDat.text")); //$NON-NLS-1$
		mntmExportToMatrixDatFile.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Export to Dat file.
				try {
					File targetFile = new File("groupNetExport.dat");
					boolean ended = false;
					while (!ended) {
						JFileChooser chooser = new JFileChooser();
						chooser.setSelectedFile(targetFile);

						chooser.setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.saveAsFileChooser.text"));
						chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
						chooser.setAcceptAllFileFilterUsed(false);
						GenericFileFilter defaultFileFilter = new GenericFileFilter("Dat files (*.dat)", "dat");
						chooser.addChoosableFileFilter(defaultFileFilter);
						chooser.setFileFilter(defaultFileFilter);

						//
						if (chooser.showSaveDialog(GroupNetWindow.this.thisFrame) == JFileChooser.APPROVE_OPTION) {
							System.out.println("getCurrentDirectory(): " + chooser.getCurrentDirectory());
							System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
							targetFile = chooser.getSelectedFile();

							boolean doSave;
							if (targetFile.exists()) {
								// Manage confirmation dialog.
								String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.title");
								String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.text");

								int response = JOptionPane.showConfirmDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.YES_NO_CANCEL_OPTION);

								if (response == JOptionPane.YES_OPTION) {
									doSave = true;
									ended = true;
								} else if (response == JOptionPane.NO_OPTION) {
									doSave = false;
									ended = false;
								} else {
									doSave = false;
									ended = true;
								}
							} else {
								doSave = true;
								ended = true;
							}

							if (doSave) {
								DATFile.exportToDAT(gui.getGroupNet(), targetFile.getAbsolutePath());
							}

						} else {
							// Nothing to do.
							System.out.println("No Selection ");
							ended = true;
						}
					}
				} catch (final PuckException exception) {
					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedFileFormat");
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.notAFile");
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedEncoding");
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.default");
					}

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JMenuItem mntmExportToPajek_1 = new JMenuItem(messages.getString("GroupNetWindow.mntmExportToPajek_1.text")); //$NON-NLS-1$
		mntmExportToPajek_1.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Export to Pajek (edge).
				try {
					File targetFile = new File("groupNetExport-edge.paj");
					boolean ended = false;
					while (!ended) {
						JFileChooser chooser = new JFileChooser();
						chooser.setSelectedFile(targetFile);

						chooser.setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.saveAsFileChooser.text"));
						chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
						chooser.setAcceptAllFileFilterUsed(false);
						GenericFileFilter defaultFileFilter = new GenericFileFilter("Pajek files (*.paj)", "paj");
						chooser.addChoosableFileFilter(defaultFileFilter);
						chooser.setFileFilter(defaultFileFilter);

						//
						if (chooser.showSaveDialog(GroupNetWindow.this.thisFrame) == JFileChooser.APPROVE_OPTION) {
							System.out.println("getCurrentDirectory(): " + chooser.getCurrentDirectory());
							System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
							targetFile = chooser.getSelectedFile();

							boolean doSave;
							if (targetFile.exists()) {
								// Manage confirmation dialog.
								String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.title");
								String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.text");

								int response = JOptionPane.showConfirmDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.YES_NO_CANCEL_OPTION);

								if (response == JOptionPane.YES_OPTION) {
									doSave = true;
									ended = true;
								} else if (response == JOptionPane.NO_OPTION) {
									doSave = false;
									ended = false;
								} else {
									doSave = false;
									ended = true;
								}
							} else {
								doSave = true;
								ended = true;
							}

							if (doSave) {
								PAJFile.exportToPajek(GraphUtils.reduceArcToEdge(gui.getGroupNet()), targetFile.getAbsolutePath(),
										ClusterNetworkReporter.ALLIANCE_NETWORK_PARTITION_LABELS);
							}

						} else {
							// Nothing to do.
							System.out.println("No Selection ");
							ended = true;
						}
					}
				} catch (final PuckException exception) {
					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedFileFormat");
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.notAFile");
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedEncoding");
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.default");
					}

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnFile.add(mntmExportToPajek_1);
		mnFile.add(mntmExportToMatrixDatFile);

		final JMenuItem mntmExportGis = new JMenuItem(messages.getString("GroupNetWindow.mntmExportGis.text")); //$NON-NLS-1$
		mntmExportGis.setEnabled(false);
		mntmExportGis.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				try {
					File targetFile = new File("allianceNetwork.shp");
					boolean ended = false;
					while (!ended) {
						JFileChooser chooser = new JFileChooser();
						chooser.setSelectedFile(targetFile);

						chooser.setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.saveAsFileChooser.text"));
						chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
						chooser.setAcceptAllFileFilterUsed(false);
						GenericFileFilter defaultFileFilter = new GenericFileFilter("Shapefile (*.shp)", "shp");
						chooser.addChoosableFileFilter(defaultFileFilter);
						chooser.setFileFilter(defaultFileFilter);

						//
						if (chooser.showSaveDialog(GroupNetWindow.this.thisFrame) == JFileChooser.APPROVE_OPTION) {
							targetFile = chooser.getSelectedFile();

							boolean doSave;
							if (targetFile.exists()) {
								// Manage confirmation dialog.
								String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.title");
								String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.text");

								int response = JOptionPane.showConfirmDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.YES_NO_CANCEL_OPTION);

								if (response == JOptionPane.YES_OPTION) {
									doSave = true;
									ended = true;
								} else if (response == JOptionPane.NO_OPTION) {
									doSave = false;
									ended = false;
								} else {
									doSave = false;
									ended = true;
								}
							} else {
								doSave = true;
								ended = true;
							}

							if (doSave) {

								Graph<Place> geoNetwork = GeoNetworkUtils.createGeoNetwork(gui.getGroupNet(), gui.getGeography(), GeoLevel.TOWN);

								SIGFile.exportToGIS(geoNetwork, targetFile.getAbsolutePath());

								MapPanel_old mapPanel = (MapPanel_old) getSelectedTab();
								SIGFile.exportToShapefile(mapPanel.getLayers(), targetFile);
							}

						} else {
							// Nothing to do.
							System.out.println("No Selection ");
							ended = true;
						}
					}
				} catch (final PuckException exception) {
					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedFileFormat");
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.notAFile");
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedEncoding");
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.default");
					}

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnFile.add(mntmExportGis);

		JSeparator separator = new JSeparator();
		mnFile.add(separator);

		JMenuItem mntmClose = new JMenuItem(messages.getString("MainWindow.mntmClose.text")); //$NON-NLS-1$
		mntmClose.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Close.
				performCloseGroupNet();
			}
		});
		mntmClose.setToolTipText(messages.getString("MainWindow.mntmClose.toolTipText")); //$NON-NLS-1$
		mntmClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK));
		mnFile.add(mntmClose);

		JMenuItem mntmQuit = new JMenuItem(messages.getString("MainWindow.mntmQuit.text")); //$NON-NLS-1$
		mntmQuit.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Quit.
				performQuit();
			}
		});

		JSeparator separator_2 = new JSeparator();
		mnFile.add(separator_2);
		mntmQuit.setMnemonic('Q');
		mntmQuit.setToolTipText(messages.getString("MainWindow.mntmQuit.toolTipText")); //$NON-NLS-1$
		mnFile.add(mntmQuit);

		JMenu mnGroupNetwork = new JMenu(messages.getString("GroupNetWindow.mnGroupNetwork.text")); //$NON-NLS-1$
		menuBar.add(mnGroupNetwork);

		JMenuItem mntmMatrix = new JMenuItem("Matrix");
		mntmMatrix.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Matrix.
				try {
					Report report = ClusterNetworkReporter.<Individual> reportMatrix(gui.getGroupNet());
					gui.addReportTab(report);
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnGroupNetwork.add(mntmMatrix);

		JMenuItem mntmCouples = new JMenuItem("Couples");
		mntmCouples.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Couples.
				try {
					AllianceType allianceType = AllianceType.valueOf(gui.getGroupNet().attributes().get("Alliance Type").getValue().toUpperCase());
					Report report = ClusterNetworkReporter.reportCouples(gui.getGroupNet(), allianceType);
					gui.addReportTab(report);
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnGroupNetwork.add(mntmCouples);

		JMenuItem mntmSortableList = new JMenuItem("Sortable List");
		mntmSortableList.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Sortable List.
				try {
					Report report = ClusterNetworkReporter.reportSortableList(gui.getGroupNet(),gui.getGeography());
					gui.addReportTab(report);
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnGroupNetwork.add(mntmSortableList);

		JMenuItem mntmSides = new JMenuItem("Sides");
		mntmSides.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Sides.
				try {
					Report report = ClusterNetworkReporter.reportSides(gui.getGroupNet());
					gui.addReportTab(report);
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnGroupNetwork.add(mntmSides);

		JMenu mnTransform = new JMenu("Transform");
		menuBar.add(mnTransform);

		JMenuItem mntmRandomPermutation = new JMenuItem(messages.getString("GroupNetWindow.mntmRandomPermutation.text"));
		mntmRandomPermutation.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Random Permutation.
				try {
					//
					RandomPermutationInputWindow window = new RandomPermutationInputWindow(gui);

					//
					window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

					//
					window.setVisible(true);
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnTransform.add(mntmRandomPermutation);

		JMenuItem mntmReshuffling = new JMenuItem(messages.getString("GroupNetWindow.mntmReshuffling.text"));
		mntmReshuffling.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Random Permutation.
				try {
					//
					ReshufflingAllianceNetworkInputWindow window = new ReshufflingAllianceNetworkInputWindow(gui);

					//
					window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

					//
					window.setVisible(true);
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnTransform.add(mntmReshuffling);

		JMenuItem mntmVirtualFieldwork = new JMenuItem(messages.getString("GroupNetWindow.mntmVirtualFieldwork.text")); //$NON-NLS-1$
		mntmVirtualFieldwork.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Virtual FieldWork
				// Random Alliance Network Observer Variations.
				try {
					//
					VirtualFieldworkInputWindow window = new VirtualFieldworkInputWindow(gui);

					//
					window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

					//
					window.setVisible(true);
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnTransform.add(mntmVirtualFieldwork);

		JMenuItem mntmVirtualFieldworkVariations = new JMenuItem(messages.getString("GroupNetWindow.mntmVirtualFiledworkVariations.text")); //$NON-NLS-1$
		mntmVirtualFieldworkVariations.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Virtual FieldWork Variations
				// Random Alliance Network Observer Variations.
				try {
					//
					VirtualFieldworkVariationsInputWindow window = new VirtualFieldworkVariationsInputWindow(gui);

					//
					window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

					//
					window.setVisible(true);
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnTransform.add(mntmVirtualFieldworkVariations);

		JMenu mnCensus = new JMenu(messages.getString("GroupNetWindow.mnCensus.text")); //$NON-NLS-1$
		menuBar.add(mnCensus);

		JMenuItem mntmAnalysis = new JMenuItem(messages.getString("GroupNetWindow.mntmAnalysis.text")); //$NON-NLS-1$
		mntmAnalysis.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Analysis.
				try {
					Report report = ClusterNetworkReporter.reportAnalysis(gui.getGroupNet());
					gui.addReportTab(report);
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnCensus.add(mntmAnalysis);

		JMenuItem mntmDistributions = new JMenuItem(messages.getString("GroupNetWindow.mntmDistributions.text")); //$NON-NLS-1$
		mntmDistributions.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Distributions.
				try {
					Report report = ClusterNetworkReporter.reportDistributions(gui.getGroupNet());
					gui.addReportTab(report);
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnCensus.add(mntmDistributions);

		JMenuItem mntmGenerateRules = new JMenuItem(messages.getString("GroupNetWindow.mntmGenerateRules.text")); //$NON-NLS-1$
		mntmGenerateRules.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Generate rules.
				try {
					//
					GenerateRulesInputWindow window = new GenerateRulesInputWindow(gui);

					//
					window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

					//
					window.setVisible(true);
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnCensus.add(mntmGenerateRules);

		JSeparator separator_3 = new JSeparator();
		mnCensus.add(separator_3);

		JMenuItem mntmSynopsis = new JMenuItem(messages.getString("GroupNetWindow.mntmSynopsis.text")); //$NON-NLS-1$
		mntmSynopsis.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent arg0) {

				File file = selectFolder(GroupNetWindow.this.thisFrame, null);

				//
				if (file != null) {
					//
					String directory = file.getAbsolutePath();
					Report report = GraphReporter.reportSynopsis(directory);
					gui.addReportTab(report);
				}

			}
		});
		mnCensus.add(mntmSynopsis);

		JMenu mnDraw = new JMenu("Draw");
		menuBar.add(mnDraw);

		JMenuItem mntmAllianceNetworkGraph = new JMenuItem(messages.getString("GroupNetWindow.mntmAllianceNetworkGraph.text")); //$NON-NLS-1$
		mntmAllianceNetworkGraph.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Alliance Network Graph visualization.
				try {
					//
					VisualizationController vizController = VisualizationController.getSharedInstance();
					VisualizationPanel buildView = vizController.buildView(gui.getGroupNet(), AllianceNetwork.class);

					addTab("Alliance Network Draw", buildView);

				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();
				}
			}
		});
		mnDraw.add(mntmAllianceNetworkGraph);

		//		JMenuItem mntmGeography = new JMenuItem(messages.getString("GroupNetWindow.mntmGeography.text")); //$NON-NLS-1$
		// mntmGeography.addActionListener(new ActionListener() {
		// /**
		// *
		// */
		// @Override
		// public void actionPerformed(final ActionEvent event) {
		// // Geography visualization
		// try {
		//
		// ConfigGeocodingDialog dialog = new ConfigGeocodingDialog(gui);
		// dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		// dialog.setVisible(true);
		//
		// mntmExportGis.setEnabled(true);
		//
		// } catch (final Exception exception) {
		// //
		// exception.printStackTrace();
		//
		// //
		// String title = "Error computerum est";
		// String message = "Error occured during working: " +
		// exception.getMessage();
		//
		// //
		// JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message,
		// title, JOptionPane.ERROR_MESSAGE);
		// }
		// }
		// });
		// mnDraw.add(mntmGeography);

		JMenuItem mntmGeography_1 = new JMenuItem(messages.getString("GroupNetWindow.mntmGeography.text")); //$NON-NLS-1$
		mntmGeography_1.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Geography visualization
				try {
					Graph<Place> geocodedGraph = GeocodingWorker.geocodeGraph(gui.getGeography(), gui.getGroupNet());

					String label = String.format("%s-%s", gui.getGroupNet().getLabel(), "GroupNet Cartography");
					logger.debug("geocoded graph label=[{}]", label);
					geocodedGraph.setLabel(label);

					MapPanel geographyMap = new MapPanel(geocodedGraph);

					addTab("Alliance Cartography", geographyMap);

				} catch (final Exception exception) {
					//
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(GroupNetWindow.this.thisFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}

			}
		});
		mnDraw.add(mntmGeography_1);

		JMenu mnWindows = new JMenu("Windows");
		mnWindows.addMenuListener(new MenuListener() {
			/**
			 * 
			 */
			@Override
			public void menuCanceled(final MenuEvent event) {
			}

			/**
			 * 
			 */
			@Override
			public void menuDeselected(final MenuEvent event) {
				// Windows menu cleaning.
				JMenu windowsMenu = (JMenu) event.getSource();
				windowsMenu.removeAll();
			}

			/**
			 * 
			 */
			@Override
			public void menuSelected(final MenuEvent event) {
				// Windows menu updating.
				JMenu windowsMenu = (JMenu) event.getSource();
				for (WindowGUI windowGUI : PuckGUI.instance().windowGUIs()) {
					windowsMenu.add(new WindowMenuItem(windowGUI));
				}
			}
		});
		menuBar.add(mnWindows);

		JMenu mnHelp = new JMenu(messages.getString("MainWindow.mnHelp.text")); //$NON-NLS-1$
		mnHelp.setMnemonic('H');
		menuBar.add(mnHelp);

		JMenuItem mntmPuckHelp = new JMenuItem(messages.getString("MainWindow.mntmOfficialWebSite.text")); //$NON-NLS-1$
		mntmPuckHelp.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Puck Help.
				MainWindow.callPuckHelp((JMenuItem) event.getSource());
			}
		});
		mnHelp.add(mntmPuckHelp);

		JMenuItem mntmAboutPuck = new JMenuItem(messages.getString("AboutPopup.this.title")); //$NON-NLS-1$
		mntmAboutPuck.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// About Puck.
				AboutPopup.showDialog();
			}
		});
		mnHelp.add(mntmAboutPuck);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BoxLayout(this.contentPane, BoxLayout.Y_AXIS));

		this.tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		this.contentPane.add(this.tabbedPane);

		GroupNetInfosPanel groupNetInfosPanel = new GroupNetInfosPanel(gui);
		this.tabbedPane.addTab(messages.getString("GroupNetWindow.groupNetworkInfosPanel.title"), null, groupNetInfosPanel, null); //$NON-NLS-1$

		// //////////////////
		updateTitle();
	}

	/**
	 * 
	 * @param report
	 */
	public void addRawTab(final String title, final JPanel panel) {
		//
		this.tabbedPane.addTab(title, null, panel, null);
		this.tabbedPane.setSelectedComponent(panel);
	}

	/**
	 * 
	 * @param report
	 */
	public void addReportTab(final Report report) {
		//
		JPanel reportPanel;
		if (report.containsSubReport()) {
			//
			reportPanel = new ReportsPanel(this.gui, report);

		} else {
			//
			reportPanel = new ReportPanel(this.gui, report);
		}

		addReportTab(report.title(), reportPanel);
	}

	/**
	 * 
	 * @param report
	 */
	public void addReportTab(final String title, final JPanel reportPanel) {
		//
		this.reportCounter += 1;

		addTab(title + " (" + this.reportCounter + ")", reportPanel);
	}

	/**
	 * 
	 */
	public void addTab(final String title, final JPanel panel) {
		this.tabbedPane.addTab(title, null, panel, null);
		this.tabbedPane.setSelectedComponent(panel);
		this.tabbedPane.setTabComponentAt(this.tabbedPane.getSelectedIndex(), new ButtonTabComponent(this.tabbedPane));
	}

	/**
	 * 
	 */
	public void closeCurrentTab() {
		this.tabbedPane.remove(this.tabbedPane.getSelectedIndex());
	}

	/**
	 * 
	 */
	public JPanel getSelectedTab() {
		return (JPanel) (this.tabbedPane.getSelectedComponent());

	}

	/**
	 * 
	 */
	private void performCloseGroupNet() {

		if (PuckGUI.instance().windowGUIs().size() > 1) {
			//
			boolean doClose;
			if (this.gui.isChanged()) {
				String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.closeConfirm.existingChange.title");
				String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.closeConfirm.existingChange.text");

				int response = JOptionPane.showConfirmDialog(this.thisFrame, message, title, JOptionPane.YES_NO_OPTION);

				if (response == JOptionPane.YES_OPTION) {
					doClose = true;
				} else {
					doClose = false;
				}
			} else {
				doClose = true;
			}

			//
			if (doClose) {
				PuckGUI.instance().close(this.gui);
				this.thisFrame.dispose();
			}
		}
	}

	/**
	 * 
	 */
	private void performCloseWindow() {
		//
		boolean doClose;
		if (this.gui.isChanged()) {
			String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.closeConfirm.existingChange.title");
			String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.closeConfirm.existingChange.text");

			int response = JOptionPane.showConfirmDialog(this.thisFrame, message, title, JOptionPane.YES_NO_OPTION);

			if (response == JOptionPane.YES_OPTION) {
				doClose = true;
			} else {
				doClose = false;
			}
		} else {
			doClose = true;
		}

		//
		if (doClose) {
			if ((PuckGUI.instance().windowGUIs().size() == 1)) {
				PuckGUI.instance().exit();
			} else {
				PuckGUI.instance().close(this.gui);
				this.thisFrame.dispose();
			}
		}
	}

	/**
	 * 
	 */
	private void performQuit() {
		//
		if ((!PuckGUI.instance().existsUnsavedChanges()) || ((PuckGUI.instance().existsUnsavedChanges()) && (ConfirmQuitDialog.showDialog(this.thisFrame)))) {
			//
			PuckGUI.instance().exit();
		}
	}

	/**
	 * 
	 * @param locale
	 */
	public void updateLocale(final Locale locale) {
		this.thisFrame.setLocale(locale);
	}

	/**
	 * 
	 */
	public void updateTitle() {
		String changeToken;
		if (this.gui.isChanged()) {
			changeToken = "*";

			// Set MAC OS X feature.
			this.thisFrame.getRootPane().putClientProperty("Window.documentModified", Boolean.TRUE);
		} else {
			changeToken = "";

			// Set MAC OS X feature.
			this.thisFrame.getRootPane().putClientProperty("Window.documentModified", Boolean.FALSE);
		}

		this.thisFrame.setTitle(changeToken + this.gui.getTitle() + " - Puck");
	}

	/**
	 * This classes is a static one to be called by OpenRecentMenuItem.java
	 * 
	 * 
	 * @param netGUI
	 * @param parentPanel
	 * @param folder
	 */
	public static File selectDatFile(final JFrame parentPanel, final File folder) {
		File result;

		//
		JFileChooser chooser = new JFileChooser();
		if ((folder != null) && (folder.exists()) && (folder.isDirectory())) {
			chooser.setCurrentDirectory(folder);
		} else if (PuckGUI.instance().recentFolders().isEmpty()) {
			chooser.setCurrentDirectory(new java.io.File("."));
		} else {
			chooser.setCurrentDirectory(PuckGUI.instance().recentFolders().getMoreRecent());
		}

		chooser.setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.openFileChooser.text"));
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setAcceptAllFileFilterUsed(true);
		GenericFileFilter defaultFileFilter = new GenericFileFilter("Matrix Dat Files (*.dat)", "dat");
		chooser.addChoosableFileFilter(defaultFileFilter);
		chooser.addChoosableFileFilter(new GenericFileFilter("Dat (*.dat)", "dat"));
		chooser.setFileFilter(defaultFileFilter);

		//
		if (chooser.showOpenDialog(parentPanel) == JFileChooser.APPROVE_OPTION) {
			System.out.println("getCurrentDirectory(): " + chooser.getCurrentDirectory());
			System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
			result = chooser.getSelectedFile();
		} else {
			logger.debug("No Selection ");
			result = null;
		}

		//
		return result;
	}

	/**
	 * This classes is a static one to be called by OpenRecentMenuItem.java
	 * 
	 * 
	 * @param netGUI
	 * @param parentPanel
	 * @param folder
	 */
	public static File selectFolder(final JFrame parentPanel, final File folder) {
		File result;

		//
		JFileChooser chooser = new JFileChooser();
		if ((folder != null) && (folder.exists()) && (folder.isDirectory())) {
			chooser.setCurrentDirectory(folder);
		} else if (PuckGUI.instance().recentFolders().isEmpty()) {
			chooser.setCurrentDirectory(new java.io.File("."));
		} else {
			chooser.setCurrentDirectory(PuckGUI.instance().recentFolders().getMoreRecent());
		}

		chooser.setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.openFileChooser.text"));
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setAcceptAllFileFilterUsed(true);

		//
		if (chooser.showOpenDialog(parentPanel) == JFileChooser.APPROVE_OPTION) {
			System.out.println("getCurrentDirectory(): " + chooser.getCurrentDirectory());
			System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
			result = chooser.getSelectedFile();
		} else {
			logger.debug("No Selection ");
			result = null;
		}

		//
		return result;
	}

}
