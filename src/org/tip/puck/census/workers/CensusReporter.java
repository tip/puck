package org.tip.puck.census.workers;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.census.chains.Chain;
import org.tip.puck.census.chains.ChainFinder;
import org.tip.puck.census.chains.Notation;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.onemode.ShuffleCriteria;
import org.tip.puck.graphs.onemode.Shuffler;
import org.tip.puck.io.paj.PAJFile;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.KinType;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.MultiPartition;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionMaker;
import org.tip.puck.partitions.graphs.ClusterNetworkReporter;
import org.tip.puck.partitions.graphs.ClusterNetworkUtils;
import org.tip.puck.report.Report;
import org.tip.puck.report.ReportChart;
import org.tip.puck.report.ReportChart.GraphType;
import org.tip.puck.report.ReportRawData;
import org.tip.puck.report.ReportTable;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.workers.SequenceCriteria;
import org.tip.puck.statistics.StatisticsReporter;
import org.tip.puck.util.Chronometer;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.PuckUtils;
import org.tip.puck.util.ToolBox;
import org.tip.puck.util.Value;

import fr.devinsy.util.StringList;

/**
 * 
 * 
 * @author TIP
 */
public class CensusReporter {

	// private static final Logger logger =
	// LoggerFactory.getLogger(CensusReporter.class);

	/**
	 * 
	 * @param partitions
	 * @return
	 */
	public static <E> ReportChart createFrequencyChart(final MultiPartition<E> partitions) {
		ReportChart result;

		result = new ReportChart("Differential Census: Frequencies", GraphType.STACKED_BARS);
		result.setHeadersLegend("Individual clusters");
		result.setLinesLegend("Chains between membersByRelationId");

		int columnIndex = 0;
		for (Value columnValue : partitions.sortedColValues()) {
			result.setLineTitle(columnValue.toString(), columnIndex);

			int rowIndex = 0;
			for (Value rowValue : partitions.rowValuesSortedBySize()) {
				result.setHeader(rowValue.toString(), rowIndex);
				result.setValue(partitions.frequency(rowValue, columnValue), columnIndex, rowIndex);
				rowIndex += 1;
			}

			columnIndex += 1;
		}
		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static <E> ReportChart createPercentageChart(final MultiPartition<E> partitions) {
		ReportChart result;

		//
		result = new ReportChart("Differential Census: Percentages", GraphType.LINES);
		result.setHeadersLegend("Individual clusters");
		result.setLinesLegend("% of chains between membersByRelationId");

		//
		for (int columnIndex = 0; columnIndex < partitions.colCount(); columnIndex++) {
			result.setLineTitle(partitions.getColValue(columnIndex).toString(), columnIndex);
		}

		//
		/*
		 * int rowIndex = 0; for (Value rowValue :
		 * partitions.getRowValuesOrderedBySize()) {
		 * result.setLineTitle(rowValue.toString(), rowIndex); for (int
		 * columnIndex=0;columnIndex<partitions.colCount(); columnIndex++){
		 * result.addValue(partitions.rowPercentage(rowValue,
		 * partitions.getColValue(columnIndex)),rowIndex); } rowIndex++; }
		 */

		//

		//
		/*
		 * for (int rowIndex=0;rowIndex<partitions.colCount(); rowIndex++){ for
		 * (Value columnValue : partitions.getRowValuesOrderedBySize()) {
		 * result.addValue(partitions.rowPercentage(columnValue,
		 * partitions.getColValue(rowIndex)),rowIndex); } }
		 */

		for (int rowIndex = 0; rowIndex < partitions.colCount(); rowIndex++) {
			for (Value columnValue : partitions.rowValuesSortedBySize()) {
				result.addValue(partitions.rowPercentage(columnValue, partitions.getColValue(rowIndex)), rowIndex);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param partitions
	 * @return
	 */
	private static ReportTable getIntervalTable(final MultiPartition<Chain> partitions) {
		ReportTable result;
		
		MultiPartition<Value> intervalPartition = partitions.getIntervalPartition();

		//
		result = new ReportTable(intervalPartition.rowCount() + 1, intervalPartition.colCount() + 1);

		//
		{
			result.set(0, 0, partitions.getLabel());
			int columnIndex = 1;
			for (Value value : intervalPartition.sortedColValues()) {
				result.set(0, columnIndex, value.toString());
				columnIndex += 1;
			}

			//
			int rowIndex = 1;
			for (Value rowValue : intervalPartition.sortedRowValues()) {
				result.set(rowIndex, 0, rowValue.toString());
				columnIndex = 1;
				for (Value colValue : intervalPartition.sortedColValues()) {
					result.set(rowIndex, columnIndex, intervalPartition.frequency(rowValue, colValue));
					columnIndex += 1;
				}
				rowIndex += 1;
			}

		}
		//
		return result;
	}
	
	/**
	 * 
	 * @param partitions
	 * @return
	 */
	private static ReportTable getIntervalTableWeighted(final MultiPartition<Chain> partitions) {
		ReportTable result;
		
		MultiPartition<Value> intervalPartition = partitions.getIntervalPartition();

		//
		result = new ReportTable(intervalPartition.rowCount() + 1, intervalPartition.colCount() + 1);

		//
		{
			result.set(0, 0, partitions.getLabel());
			int columnIndex = 1;
			for (Value value : intervalPartition.sortedColValues()) {
				result.set(0, columnIndex, value.toString());
				columnIndex += 1;
			}

			//
			int rowIndex = 1;
			for (Value rowValue : intervalPartition.sortedRowValues()) {
				result.set(rowIndex, 0, rowValue.toString());
				columnIndex = 1;
				for (Value colValue : intervalPartition.sortedColValues()) {
					
					Cluster<Value> cluster = intervalPartition.getCluster(rowValue, colValue);
					
					if (cluster!=null){
						int sum = 0;
						for (Value value1 : cluster.getItems()){
							double partsum = (1 + Math.sqrt(1 + 8*partitions.rowSum(value1)))/2;
							sum += partsum;
						}
						result.set(rowIndex, columnIndex, sum);
					}
					columnIndex += 1;
				}
				rowIndex += 1;
			}
		}
		//
		return result;
	}


	/**
	 * 
	 * @param partitions
	 * @return
	 */
	public static <E> ReportTable getFrequencyTable(final MultiPartition<E> partitions) {
		ReportTable result;

		//
		result = new ReportTable(partitions.rowCount() + 3, partitions.colCount() + 2);

		//
		{
			result.set(0, 0, partitions.getLabel());
			int columnIndex = 1;
			for (Value value : partitions.sortedColValues()) {
				result.set(0, columnIndex, value.toString());
				columnIndex += 1;
			}
			result.set(0, result.getColumnCount() - 1, "sum");

			//
			int rowIndex = 1;
			for (Value rowValue : partitions.sortedRowValues()) {
				result.set(rowIndex, 0, rowValue.toString());
				columnIndex = 1;
				for (Value colValue : partitions.sortedColValues()) {
					result.set(rowIndex, columnIndex, partitions.frequency(rowValue, colValue));
					columnIndex += 1;
				}
				result.set(rowIndex, result.getColumnCount() - 1, partitions.rowSum(rowValue));
				rowIndex += 1;
			}

			//
			result.set(result.getRowCount() - 2, 0, "sum");
			result.set(result.getRowCount() - 1, 0, "mean");
			columnIndex = 1;
			for (Value colValue : partitions.colValues()) {
				result.set(result.getRowCount() - 2, columnIndex, partitions.colSum(colValue));
				result.set(result.getRowCount() - 1, columnIndex, MathUtils.percent(partitions.colSum(colValue), 100 * partitions.rowCount()));
				columnIndex += 1;
			}
			result.set(result.getRowCount() - 2, result.getColumnCount() - 1, partitions.sum());
			result.set(result.getRowCount() - 1, result.getColumnCount() - 1, MathUtils.percent(partitions.sum(), 100 * partitions.rowCount()));
		}
		//
		return result;
	}

	/**
	 * 
	 * @param partitions
	 * @return
	 */
	private static <E> ReportTable getPercentageTable(final MultiPartition<E> partitions) {
		ReportTable result;

		//
		result = new ReportTable(partitions.rowValues().size() + 3, partitions.colValues().size() + 2);

		List<Double> percSums = new ArrayList<Double>();
		int nonZeroRowSum = 0;

		//
		{
			result.set(0, 0, partitions.getLabel());
			int columnIndex = 1;
			for (Value value : partitions.sortedColValues()) {
				result.set(0, columnIndex, value.toString());
				columnIndex += 1;
				percSums.add(0.0);
			}
			result.set(0, result.getColumnCount() - 1, "relative size");

			//
			int rowIndex = 1;
			for (Value rowValue : partitions.sortedRowValues()) {
				result.set(rowIndex, 0, rowValue.toString());
				columnIndex = 1;
				for (Value colValue : partitions.sortedColValues()) {
					result.set(rowIndex, columnIndex, partitions.rowPercentage(rowValue, colValue));
					percSums.set(columnIndex - 1, percSums.get(columnIndex - 1) + partitions.rowPercentage(rowValue, colValue));
					columnIndex += 1;
				}
				result.set(rowIndex, result.getColumnCount() - 1, MathUtils.percent(partitions.rowSum(rowValue), partitions.sum()));
				if (partitions.rowSum(rowValue) > 0) {
					nonZeroRowSum += 1;
				}
				rowIndex += 1;
			}

			//
			result.set(result.getRowCount() - 2, 0, "total share");
			result.set(result.getRowCount() - 1, 0, "mean share");
			columnIndex = 1;
			for (Value colValue : partitions.colValues()) {
				result.set(result.getRowCount() - 2, columnIndex, MathUtils.percent(partitions.colSum(colValue), partitions.sum()));
				result.set(result.getRowCount() - 1, columnIndex, MathUtils.round(percSums.get(columnIndex - 1) / nonZeroRowSum, 2));
				columnIndex += 1;
			}
		}
		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param individualPartitionLabel
	 * @param partitionCriteria
	 * @return
	 * @throws PuckException
	 */
	public static Report reportDifferentialCensus(final Segmentation segmentation, final SequenceCriteria sequenceCriteria, Integer date, final CensusCriteria censusCriteria) throws PuckException {
		Report result;

		// Constant parameters.
		censusCriteria.setRestrictionType(RestrictionType.ALL);
/*		censusCriteria.setSymmetryType(SymmetryType.INVERTIBLE);
		censusCriteria.setCrossSexChainsOnly(false);
		censusCriteria.setClosingRelation("TOTAL");*/

		//
		Chronometer chrono = new Chronometer();

		//
		MultiPartition<Chain> circuits = null;
		
		if (sequenceCriteria==null){
			circuits = CircuitFinder.createDifferentialCensus(segmentation, censusCriteria);
		} else {
			circuits = CircuitFinder.createDifferentialCensus(segmentation, sequenceCriteria.getRelationModelName(), sequenceCriteria.getLocalUnitLabel(), sequenceCriteria.getDateLabel(), date, censusCriteria);
		}
		
		MultiPartition<Chain> couples = CircuitFinder.createDifferentialCensusByCouples(circuits, censusCriteria);

		result = new Report();
		if (sequenceCriteria!=null){
			String reportName = sequenceCriteria.getRelationModelName();
			if (date!=null){
				reportName += " "+date;
			}
			result.setTitle(reportName);
		}
		
		result.inputs().add("Closing Relation",censusCriteria.getClosingRelation());
		result.inputs().add("Partition",censusCriteria.getIndividualPartitionLabel());
		result.inputs().add("Pattern",censusCriteria.getPattern());
		result.inputs().add("Chain Classification",censusCriteria.getChainClassification());
		result.inputs().add("Cross Sex Couples Only",censusCriteria.isCrossSexChainsOnly());

		//
		result.setOrigin("Partition reporter");
		result.setTarget(segmentation.getLabel() + " " + censusCriteria.getIndividualPartitionLabel());

		//
		circuits.count();
		couples.count();

		// == Build Percentage Chart.
		ReportChart chart = createFrequencyChart(couples);
		result.outputs().append(chart);

		// == Build Percentage Chart.
		ReportChart chart2 = createPercentageChart(couples);
		result.outputs().appendln(chart2);
		// result.outputs().appendln(chart2.createReportTable());
		// result.outputs().appendln(ReportTable.transpose(chart2.createReportTable()));

		// == Build Absolute Frequency Table.
		ReportTable table = getFrequencyTable(couples);
		result.outputs().appendln("Pair frequencies by cluster");
		result.outputs().appendln(table);

		// == Build Percentage Table.
		ReportTable table2 = getPercentageTable(couples);
		result.outputs().appendln("Pair percentages by cluster");
		result.outputs().appendln(table2);

		// == Build Interval Table.
		ReportTable table3 = getIntervalTable(couples);
		result.outputs().appendln("Nr of clusters in percentage intervals");
		result.outputs().appendln(table3);

		// == Build Weighted Interval Table.
		ReportTable table4 = getIntervalTableWeighted(couples);
		result.outputs().appendln("Nr of individuals in clusters in percentage intervals");
		result.outputs().appendln(table4);

		// Report 2.
		Report report = new Report("Chain list by cluster");
		report.outputs().append(CircuitFinder.reportChainListByCluster(circuits));
		result.outputs().append(report);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}
	
	public static boolean isBasicClassification(String chainClassification){	
		return (StringUtils.equals(chainClassification, "SIMPLE"))
			|| (StringUtils.equals(chainClassification, "CLASSIC"))
			|| (StringUtils.equals(chainClassification, "CLASSIC_GENDERED"))
			|| (StringUtils.equals(chainClassification, "POSITIONAL"));
	}
	
	public static Report reportShortCensus(final Segmentation segmentation, final CensusCriteria criteria) throws PuckException {
		Report result;
		
		result = new Report("Census");
		
		CircuitFinder finder = new CircuitFinder(segmentation, criteria);
		
		finder.findCircuits();
		finder.count();
		
		result.outputs().append(finder.reportSurvey());
		
		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportFindCircuit(final CircuitFinder finder, final Segmentation segmentation, final CensusCriteria criteria, final File sourceFile)
			throws PuckException {
		Report result;

		Chronometer chrono = new Chronometer();

		finder.findCircuits();

		if (criteria.isOpenChainFrequencies()) {
			finder.getOpenChains();
		}

		//
		result = new Report("Census");

		// == Report 1 (survey).
		result.setOrigin("CircuitFinder3.findCircuits()");
		result.setTarget(segmentation.getLabel());

		//
		result.inputs().add("Pattern", criteria.getPattern());
		result.inputs().add("Partition label", criteria.getClassificatoryLinking());
		result.inputs().add("Filter", criteria.getFilter());
		result.inputs().add("Closing Relation", criteria.getClosingRelation());
		result.inputs().add("Ego Role", criteria.getClosingRelationEgoRole());
		result.inputs().add("Alter Role", criteria.getClosingRelationAlterRole());
		result.inputs().add("Classification label", criteria.getChainClassification());
		result.inputs().add("Cross Sex", criteria.isCrossSexChainsOnly());
		result.inputs().add("Married Only", criteria.isCouplesOnly());
		result.inputs().add("Mark Individuals", criteria.isMarkIndividuals());
		result.inputs().add("Circuit Type", criteria.getCircuitType().toLabel());
		result.inputs().add("Filiation Type", criteria.getFiliationType().toString());
		result.inputs().add("Restriction Type", criteria.getRestrictionType().toString());
		result.inputs().add("Sibling Mode", criteria.getSiblingMode().toString());
		result.inputs().add("Symmetry Type", criteria.getSymmetryType().toString());
		result.inputs().add("Open Chain Frequencies", criteria.isOpenChainFrequencies());
		result.inputs().add("Circuit Induce Frame Network", criteria.isCircuitInducedFrameNetwork());
		result.inputs().add("Circuit Induce Network", criteria.isCircuitInducedNetwork());
		result.inputs().add("Circuit Interserction Network", criteria.isCircuitIntersectionNetwork());
		result.inputs().add("Circuit Networks", criteria.isCircuitNetworks());
		if (isBasicClassification(criteria.getChainClassification())) {
			for (CensusDetail detail : criteria.getCensusDetails()) {
				if (detail.isAvailable()) {
					String range;
					if ((detail.isReport()) && (detail.isDiagram())) {
						range = " (report and diagram)";
					} else if (detail.isReport()) {
						range = " (report)";
					} else {
						range = " (diagram)";
					}

					result.inputs().add("Census detail", detail.getLabel() + range);
				}
			}
		}

		//
		finder.count();
		result.outputs().append(finder.reportSurvey());

		// Report 2.
		Report report = new Report("Circuits");
		report.outputs().append(finder.reportCircuitTypeList());
		result.outputs().append(report);

		// Report 3.
		report = new Report("Couples");
		report.outputs().append(finder.reportCoupleList());
		result.outputs().append(report);

		// Report 4.
		report = new Report("Sortable list");
		report.outputs().append(finder.reportSortableList());
		result.outputs().append(report);

		// === Build Pajek data.
		StringList pajekBuffer = new StringList();
		
		StringList partitionLabels = criteria.getPartitionLabels();

		//
		if (criteria.isCircuitInducedFrameNetwork()) {
			Graph<Individual> graph = CensusUtils.createCircuitInducedFrameNetwork(finder);
			pajekBuffer.addAll(PuckUtils.writePajekNetwork(graph,partitionLabels));
			pajekBuffer.appendln();
		}

		//
		if (criteria.isCircuitInducedNetwork()) {
			Graph<Individual> graph = CensusUtils.createCircuitInducedNetwork(finder);
			pajekBuffer.addAll(PuckUtils.writePajekNetwork(graph,partitionLabels));
			pajekBuffer.appendln();
		}

		//
		if (criteria.isCircuitIntersectionNetwork()) {

			finder.changeCircuitLabelsToClassic();
			// Map<Chain, Set<Cluster<Chain>>> map = finder.coupleChainClusterMap(Gender.MALE);
			// Graph<Cluster<Chain>> graph =
			// ClusterNetworkUtils.createCircuitIntersectionNetwork(map,finder);
			Graph<Cluster<Chain>> graph = ClusterNetworkUtils.createCircuitIntersectionNetwork(finder);
			List<String> reportLabels = criteria.getCensusDetails().getReportLabels();
			reportLabels.add("DEGREE");
			reportLabels.add("SIZE");
			pajekBuffer.addAll(PuckUtils.writePajekNetwork(graph, reportLabels));
			pajekBuffer.appendln();

			// Report 5.
			report = ClusterNetworkReporter.reportCircuitIntersectionMatrix(graph);
			result.outputs().append(report);
			
			// Report 6.
			report = finder.decompose();
			result.outputs().append(report);

		}

		//
		if (criteria.isCircuitNetworks()) {
			for (Chain circuit : finder.getCircuits().getItems()) {
				Graph<Individual> graph = CensusUtils.createCircuitNetwork(circuit);
				pajekBuffer.addAll(PuckUtils.writePajekNetwork(graph));
				pajekBuffer.appendln();
			}
		}

		//
		if (pajekBuffer.length() != 0) {
			File targetFile = ToolBox.setExtension(ToolBox.addToName(sourceFile, "-Circuit Networks"), ".paj");
			ReportRawData rawData = new ReportRawData("Export Circuit Networks to Pajek", "Pajek", "paj", targetFile);
			rawData.setData(PAJFile.convertToMicrosoftEndOfLine(pajekBuffer.toString()));

			result.outputs().appendln();
			result.outputs().append(rawData);
		}

		// Report 6.
		report = new Report("Diagrams");
		// Compute charts and tables.
		if (isBasicClassification(criteria.getChainClassification())) {
			List<ReportChart> charts = new ArrayList<ReportChart>(20);
			List<ReportTable> tables = new ArrayList<ReportTable>(20);
			for (String label : criteria.getCensusDetails().getDiagramLabels()) {
				Partition<Chain> partition = PartitionMaker.create(label, finder.getCircuits(), PartitionCriteria.createRaw(label));

				ReportChart chart; 
				
				if (finder.crossSex || !(finder.getSymmetry()==SymmetryType.INVARIABLE)) {
					chart = StatisticsReporter.createPartitionChart(partition);
				} else {
					chart = StatisticsReporter.createPartitionChart(partition, new PartitionCriteria(partition.getLabel()), new PartitionCriteria("EGOGENDER"), segmentation.getGeography());
				}
				

				if (chart != null) {
					//
					charts.add(chart);
					tables.add(ReportTable.transpose(chart.createReportTableWithSum()));
				}
			}

			// Manage the number of chart by line.
			for (int chartIndex = 0; chartIndex < charts.size(); chartIndex++) {
				report.outputs().append(charts.get(chartIndex));
				if (chartIndex % 4 == 3) {
					report.outputs().appendln();
				}
			}

			// Add chart tables.
			for (ReportTable table : tables) {
				report.outputs().appendln(table.getTitle());
				report.outputs().appendln(table);
			}
		}
		result.outputs().append(report);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * 
	 * @param segmentation
	 * @param criteria
	 * @param sourceFile
	 * @return
	 * @throws PuckException
	 */
	public static Report reportFindCircuit(final Segmentation segmentation, final CensusCriteria criteria, final File sourceFile) throws PuckException {
		Report result;

		CircuitFinder finder = new CircuitFinder(segmentation, criteria);

		result = reportFindCircuit(finder, segmentation, criteria, sourceFile);

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param shuffleCriteria
	 * @param runs
	 * @param criteria
	 * @return
	 * @throws PuckException
	 */
	public static Report reportFindCircuitsReshuffled(final Net net, final ShuffleCriteria shuffleCriteria, final int runs, final CensusCriteria criteria)
			throws PuckException {
		Report result;

		Chronometer chrono = new Chronometer();

		CircuitFinder finder = Shuffler.findCircuits(net, shuffleCriteria, runs, criteria);

		//
		result = new Report("Census.");
		result.outputs().append(finder.reportSurvey());
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;

	}
	
	public static List<Report> reportMultipleCensus (Segmentation segmentation, List<CensusCriteria> criteriaList) throws PuckException{
		List<Report> result;
		
		result = new ArrayList<Report>();
		
		for (CensusCriteria criteria : criteriaList){
			Report report = reportFindCircuit(segmentation,criteria, new File(""));
			report.setTitle(report.title()+" "+criteria.getClosingRelationEgoRole()+"-"+criteria.getClosingRelationAlterRole());
			result.add(report);
		}
		
		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportKinshipChains(final String netLabel, final Individual ego, final Individual alter, final int maxDepth, final int maxOrder,
			final String chainClassification) {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Chains linking " + ego.getName() + " and " + alter.getName());
		result.setOrigin("ChainFinder");
		result.setTarget(netLabel);

		//
		result.inputs().add("Ego Id", ego.getId());
		result.inputs().add("Alter Id", alter.getId());
		result.inputs().add("Maximal Depth", maxDepth);
		result.inputs().add("Maximal Order", maxOrder);
		result.inputs().add("Chain Classification", chainClassification);

		//
		Partition<Chain> chains = ChainFinder.findChains(ego, alter, maxDepth, maxOrder, chainClassification);
		for (Cluster<Chain> cluster : chains.getClusters().toListSortedByValue()) {
			result.outputs().append(cluster.getValue() + "\n");
			for (Chain chain : cluster.getItems()) {
				result.outputs().append(
						chain.signature(Notation.CLASSIC_GENDERED) + "\t" + chain.signature(Notation.POSITIONAL) + "\t" + chain.signature(Notation.NUMBERS)
								+ "\n");
			}
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}
	
	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportKinshipChainsForTerms(final Segmentation segmentation) {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Kin term statistics");
		result.setOrigin("TermCensus");
		result.setTarget(segmentation.getLabel());
		
		Report report1 = new Report("Analysis");
		Report report2 = new Report("List of terms");
		
		TermCensus census = new TermCensus(segmentation);
		census.analyze();
		
		Partition<Chain> chains = census.getChains();
		Partition<String> consanguinealTerms = census.getConsanguinealTerms();
		Partition<String> affinalTerms = census.getAffinalTerms();
		
		report2.outputs().appendln("Classic\tPositional\tId\tAlterGender\tOrder\tGeneration\tDegree\tLine");

		for (Cluster<Chain> cluster : chains.getClusters().toListSortedByValue()) {
			String term = cluster.getValue().toString();
			report2.outputs().appendln(term);
			for (Chain chain : cluster.getItems()) {
				report2.outputs().appendln(chain.signature(Notation.CLASSIC_GENDERED_AGED) + "\t" + chain.signature(Notation.POSITIONAL)+ "\t" + chain.getLast().getId()+ "\t"
						+ chain.getLast().getGender()+ "\t"+ chain.dim() +"\t"+ ChainValuator.get(chain, "SKEWSUM") + "\t"+ ChainValuator.get(chain, "DEGREE_ROM") + "\t"+ ChainValuator.get(chain, "LINE"));
			}
		}
		report2.outputs().appendln();
		
		report1.outputs().appendln(consanguinealTerms.getItems().size()+" Consanguineal terms");
		report1.outputs().appendln("Gen\tNrTerms\tTerms");
		for (Cluster<String> cluster: consanguinealTerms.getClusters().toListSortedByDescendingValue()){
			report1.outputs().appendln(cluster.getValue()+"\t"+cluster.size()+"\t"+cluster.getItemsAsString());
		}
		report1.outputs().appendln();
		
		report1.outputs().appendln(affinalTerms.getItems().size()+" Affinal terms");
		report1.outputs().appendln("Gen\tNrTerms\tTerms");
		for (Cluster<String> cluster: affinalTerms.getClusters().toListSortedByDescendingValue()){
			report1.outputs().appendln(cluster.getValue()+"\t"+cluster.size()+"\t"+cluster.getItemsAsString());
		}
		report1.outputs().appendln();
		

		report1.outputs().appendln("Cousin Terminology: "+census.getCousinTerminology());
		report1.outputs().appendln();
				
		report1.outputs().appendln("Term properties");
		report1.outputs().appendln("Term\tGendered\tGenerational\tBifucate\tMerging");
		for (String term : census.getTerms()){
			report1.outputs().append(term+"\t\t");
			for (boolean property : census.getTermProperties().get(term)){
				report1.outputs().append(property+"\t");
			}
			report1.outputs().appendln();
		}
		report1.outputs().appendln();
		
		result.outputs().append(report1);
		result.outputs().append(report2);

				
		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	
	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
/*	public static Report reportKinshipChainsForTerms(final Segmentation segmentation, final String chainClassification) {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Kin term statistics");
		result.setOrigin("ChainFinder");
		result.setTarget(segmentation.getLabel());

		//
		result.inputs().add("Chain Classification", chainClassification);
		
		
		Report report1 = new Report("Analysis");
		Report report2 = new Report("List of terms");
		
		Partition<String> consanguinealTerms = new Partition<String>();
		Partition<String> affinalTerms = new Partition<String>();
		Partition<Chain> total = new Partition<Chain>();
		
		report2.outputs().appendln("Classic\tPositional\tId\tAlterGender\tOrder\tGeneration\tDegree\tLine");
		for (String term : segmentation.getCurrentIndividuals().firstNames()){

			Partition<Chain> chains = ChainFinder.findChainsForTerms(segmentation, term, chainClassification);
			if (chains.size()>0){

				report2.outputs().appendln(term);
				for (Cluster<Chain> cluster : chains.getClusters().toListSortedByValue()) {
					for (Chain chain : cluster.getItems()) {

						total.put(chain, new Value(term));
						
						if (chain.dim()==1){
							consanguinealTerms.put(term,ChainValuator.get(chain, "SKEW"));
						} else {
							affinalTerms.put(term,ChainValuator.get(chain, "SKEW"));
						}
						
						report2.outputs().appendln(chain.signature(Notation.CLASSIC_GENDERED_AGED) + "\t" + chain.signature(Notation.POSITIONAL)+ "\t" + chain.getLast().getId()+ "\t"
								+ chain.getLast().getGender()+ "\t"+ chain.dim() +"\t"+ ChainValuator.get(chain, "SKEWSUM") + "\t"+ ChainValuator.get(chain, "DEGREE_ROM") + "\t"+ ChainValuator.get(chain, "LINE"));
					}
				}
				report2.outputs().appendln();
			}
		}
		
		report1.outputs().appendln(consanguinealTerms.getItems().size()+" Consanguineal terms");
		report1.outputs().appendln("Gen\tNrTerms\tTerms");
		for (Cluster<String> cluster: consanguinealTerms.getClusters().toListSortedByDescendingValue()){
			report1.outputs().appendln(cluster.getValue()+"\t"+cluster.size()+"\t"+cluster.getItemsAsString());
		}
		report1.outputs().appendln();
		
		report1.outputs().appendln(affinalTerms.getItems().size()+" Affinal terms");
		report1.outputs().appendln("Gen\tNrTerms\tTerms");
		for (Cluster<String> cluster: affinalTerms.getClusters().toListSortedByDescendingValue()){
			report1.outputs().appendln(cluster.getValue()+"\t"+cluster.size()+"\t"+cluster.getItemsAsString());
		}
		report1.outputs().appendln();
		
		Map<String,boolean[]> termProperties = new TreeMap<String,boolean[]>();
		
		String cousinTerminology = "";
		
		for (Cluster<Chain> cluster : total.getClusters().toListSortedByValue()){
			
			boolean[] properties = new boolean[4];
			
			boolean parallelCousinTerm = false;
			boolean crossCousinTerm = false;
			boolean siblingTerm = false;
						
			Set<Gender> gender = new HashSet<Gender>();
			Set<Value> generation = new HashSet<Value>();
			Set<Integer> length = new HashSet<Integer>();
			Set<Value> bifurcation = new HashSet<Value>();
			
			for (Chain chain : cluster.getItems()) {
				gender.add(chain.getLast().getGender());
				generation.add(ChainValuator.get(chain, "SKEWSUM"));
				length.add(chain.size());
				bifurcation.add(ChainValuator.get(chain,"LINE"));
				
				if (ChainValuator.get(chain,"SKEW").equals(new Value(0))) {
					if (ChainValuator.get(chain, "DEGREE_ROM").equals(new Value(2))){
						siblingTerm = true;
					} else if (ChainValuator.get(chain,"LINE").equals(new Value(FiliationType.COGNATIC))){
						crossCousinTerm = true;
					} else {
						parallelCousinTerm = true;
					}
				}
			}
			
			
			if (gender.size()==1){
				properties[0] = true;
			}
			
			if (generation.size()==1){
				properties[1] = true;
			}
			
			if (bifurcation.size()==1 || bifurcation.contains(new Value(FiliationType.COGNATIC))){
				properties[2] = true;
			}
			
			if (length.size()>1) {
				properties[3] = true;
			}
			
			termProperties.put(cluster.getValue().toString(),properties);
			
			if (parallelCousinTerm && crossCousinTerm && siblingTerm){
				cousinTerminology = "GENERATIONAL";
			} else if (parallelCousinTerm && crossCousinTerm) {
				if (!cousinTerminology.equals("GENERATIONAL")){
					cousinTerminology = "LINEAL";
				}
			} else if (parallelCousinTerm && siblingTerm) {
				if (!cousinTerminology.equals("GENERATIONAL")){
					cousinTerminology = "BIFURCATE-MERGING";
				}
			} else {
				if (cousinTerminology.equals("")){
					cousinTerminology = "DESCRIPTIVE";
				}
			}
		}
		
		report1.outputs().appendln("Cousin Terminology: "+cousinTerminology);
		report1.outputs().appendln();
				
		report1.outputs().appendln("Term properties");
		report1.outputs().appendln("Term\tGendered\tGenerational\tBifucate\tMerging");
		for (String term : termProperties.keySet()){
			report1.outputs().append(term+"\t\t");
			for (boolean property : termProperties.get(term)){
				report1.outputs().append(property+"\t");
			}
			report1.outputs().appendln();
		}
		report1.outputs().appendln();
		
		result.outputs().append(report1);
		result.outputs().append(report2);

				
		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}*/


	/**
	 * 
	 * @param segmentation
	 * @param partitionLabel
	 * @param sorting
	 * @return
	 * @throws PuckException
	 */
	public static Report reportMissingRelativesList(final Segmentation segmentation, final String partitionLabel) throws PuckException {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("List of relatives.");
		result.setOrigin("Statistics reporter");
		result.setTarget(segmentation.getLabel());

		result.inputs().add("Partition", partitionLabel);

		Partition<Individual> partition = PartitionMaker.createRaw(partitionLabel, segmentation.getCurrentIndividuals(), segmentation.getGeography());

		for (Cluster<Individual> cluster : partition.getClusters().toListSortedByValue()) {
			if (cluster == null || cluster.getValue() == null) {
				continue;
			}
			result.outputs().append(cluster.getValue().toString() + "\t(" + cluster.count() + ")");
			result.outputs().appendln();

			for (Individual individual : new Individuals(cluster.getItems()).toSortedList()) {
				StringList missingItems = new StringList();

				if (individual.getFather() == null) {
					missingItems.appendln("\tFather missing");
				}
				if (individual.getFather() == null) {
					missingItems.appendln("\tMother missing");
				}
				if (individual.getAttributeValue("SPCOMP") == null) {
					missingItems.appendln("\tIncomplete Spouses: " + reportRelativesAsString(individual, KinType.SPOUSE));
				}
				if (individual.getAttributeValue("CHCOMP") == null) {
					missingItems.appendln("\tIncomplete Children: " + reportRelativesAsString(individual, KinType.CHILD));
				}
				if (individual.getAttributeValue("INFO") != null) {
					missingItems.appendln("\tOpen Questions");
				}
				if (missingItems.size() > 0) {
					result.outputs().appendln(individual.toString() + "\tAge " + IndividualValuator.lifeStatusAtYear(individual, 2013));
					if (individual.getAttributeValue("NOTE") != null) {
						result.outputs().appendln("\t" + individual.getAttributeValue("NOTE"));
					}
					result.outputs().appendln(missingItems);
				}
			}
			result.outputs().appendln();
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportPedigree(final String netLabel, final Individual ego, final int maxDepth) {
		Report result;

		Chronometer chrono = new Chronometer();

		result = new Report();
		result.setTitle("Pedigree of " + ego.getName());
		result.setOrigin("ChainFinder");
		result.setTarget(netLabel);

		//
		result.inputs().add("Maximal depth", maxDepth);

		//

		String reportString = ChainFinder.getPedigree(ego, maxDepth);

		result.outputs().append(reportString);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportProgeniture(final String netLabel, final Individual ego, final int maxDepth) {
		Report result;

		Chronometer chrono = new Chronometer();

		result = new Report();
		result.setTitle("Progeniture of " + ego.getName());
		result.setOrigin("ChainFinder");
		result.setTarget(netLabel);

		//
		result.inputs().add("Maximal depth", maxDepth);

		//

		String reportString = ChainFinder.getProgeniture(ego, maxDepth);

		result.outputs().append(reportString);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}
	
	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportReciprocalTerms(final Segmentation segmentation) {
		Report result;

		Chronometer chrono = new Chronometer();

		result = new Report();
		result.setTitle("Reciprocal terms ");
		result.setOrigin("ChainFinder");
		result.setTarget(segmentation.getLabel());
		
		TermCensus census = new TermCensus(segmentation);
		Partition<String> reciprocalTerms = census.findReciprocalTerms();
		
		for (Cluster<String> cluster : reciprocalTerms.getClusters()){
			result.outputs().appendln(cluster.getValue()+"\t"+cluster.getItemsAsString());
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}
	
	public static Report reportTermProducts(final Segmentation segmentation) {
		Report result;

		Chronometer chrono = new Chronometer();

		result = new Report();
		result.setTitle("Reciprocal terms ");
		result.setOrigin("ChainFinder");
		result.setTarget(segmentation.getLabel());
		
		TermCensus census = new TermCensus(segmentation);
		census.analyze();
		Partition<String> termProducts = census.findTermProducts();
		
		result.outputs().appendln("firstTerm\tSecondTerm\tProduct");
		for (String term1 : census.getTerms()){
			for (String term2 : census.getTerms()){
				Cluster<String> cluster = termProducts.getCluster(new Value(new String[]{term1,term2}));
				String items = "";
				if (cluster !=null && cluster.size()>0){
					items = cluster.getItemsAsString();
				}
				result.outputs().appendln(term1 +"\t"+term2+"\t"+items);
			}
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}



	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
/*	public static Report reportReciprocalTerms(final Segmentation segmentation) {
		Report result;

		Chronometer chrono = new Chronometer();

		result = new Report();
		result.setTitle("Reciprocal terms ");
		result.setOrigin("ChainFinder");
		result.setTarget(segmentation.getLabel());
		
		for (String term : segmentation.getCurrentIndividuals().firstNames()){
			result.outputs().append(term+"\t");
			for (String reciprocalTerm : ChainFinder.findReciprocalTerms(segmentation, term)){
				result.outputs().append(reciprocalTerm+"\n\t");
			}
			result.outputs().appendln();
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}*/
	
	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportRelatives(final String netLabel, final Individual ego, final String kinshipType) {
		Report result;

		Chronometer chrono = new Chronometer();

		result = new Report();
		result.setTitle("Relatives of " + ego.getName());
		result.setOrigin("ChainFinder");
		result.setTarget(netLabel);

		//
		result.setInputComment("No comment.");
		result.inputs().add("Kinship type: ", kinshipType);

		//
		Partition<Chain> relatives = ChainFinder.getKin(ego, kinshipType);
		for (Cluster<Chain> chains : relatives.getClusters().toListSortedByValue()) {
			Value value = chains.getValue();
			String entry = value.individualValue().getId() + "\t" + value.individualValue().getName() + "\t";
			for (Chain chain : chains.getItems()) {
				entry = entry + chain.signature(Notation.POSITIONAL) + " ";
			}
			entry = entry + "\n";
			result.outputs().append(entry);
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	private static String reportRelativesAsString(final Individual individual, final KinType type) {
		String result;

		result = "";
		List<Individual> relatives;
		if (type == KinType.PARENT) {
			relatives = individual.getKin(type).toList();
		} else {
			relatives = individual.getKin(type).toListSortedByBirthYearOrOrder();
		}

		for (Individual relative : relatives) {
			result = result + relative.toString() + "; ";
		}
		//
		return result;
	}

	/**
	 * 
	 * @param segmentation
	 * @param partitionLabel
	 * @param sorting
	 * @return
	 * @throws PuckException
	 */
	public static Report reportRelativesList(final Segmentation segmentation, final String partitionLabel) throws PuckException {
		Report result;

		int thisYear = Calendar.getInstance().get(Calendar.YEAR);
		
		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("List of relatives.");
		result.setOrigin("Statistics reporter");
		result.setTarget(segmentation.getLabel());

		result.inputs().add("Partition", partitionLabel);
		
		if (partitionLabel.isEmpty()){
			for (Individual individual : segmentation.getCurrentIndividuals().toSortedList()) {
				result.outputs().appendln(individual.toString() + "\tAge :" + IndividualValuator.lifeStatusAtYear(individual, thisYear));
				for (int i = 1; i >= -1; i--) {
					KinType kinType = KinType.valueOf(i);
					String relativesAsString = reportRelativesAsString(individual, kinType);
					String complete = "";
					if (kinType == KinType.SPOUSE && individual.getAttributeValue("SPCOMP") != null) {
						complete = " ("+individual.getAttributeValue("SPCOMP")+")";
					}
					if (kinType == KinType.CHILD && individual.getAttributeValue("CHCOMP") != null) {
						complete = " ("+individual.getAttributeValue("CHCOMP")+")";
					}
					if (relativesAsString.length() > 0) {
						result.outputs().appendln("\t" + kinType.signature() + complete + ": " + relativesAsString);
					}
				}
				result.outputs().appendln();
			}
			result.outputs().appendln();
			
		} else {
			Partition<Individual> partition = PartitionMaker.createRaw(partitionLabel, segmentation.getCurrentIndividuals(), segmentation.getGeography());
			for (Cluster<Individual> cluster : partition.getClusters().toListSortedByValue()) {
				if (cluster == null || cluster.getValue() == null) {
					continue;
				}
				result.outputs().append(cluster.getValue().toString() + "\t(" + cluster.count() + ")");
				result.outputs().appendln();

				for (Individual individual : new Individuals(cluster.getItems()).toSortedList()) {
					result.outputs().appendln(individual.toString() + "\tAge :" + IndividualValuator.lifeStatusAtYear(individual, thisYear));
					for (int i = 1; i >= -1; i--) {
						KinType kinType = KinType.valueOf(i);
						String relativesAsString = reportRelativesAsString(individual, kinType);
						String complete = "";
						if (kinType == KinType.SPOUSE && individual.getAttributeValue("SPCOMP") != null) {
							complete = " ("+individual.getAttributeValue("SPCOMP")+")";
						}
						if (kinType == KinType.CHILD && individual.getAttributeValue("CHCOMP") != null) {
							complete = " ("+individual.getAttributeValue("CHCOMP")+")";
						}
						if (relativesAsString.length() > 0) {
							result.outputs().appendln("\t" + kinType.signature() + complete + ": " + relativesAsString);
						}
					}
					result.outputs().appendln();
				}
				result.outputs().appendln();
			}
		}


		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	public static List<Report> reportRelationCensus (final Segmentation segmentation, final SequenceCriteria criteria) throws PuckException{
		List<Report> result;
		
		if ((segmentation == null) || (criteria.getRelationModelName() == null) || (criteria.getEgoRoleName() == null)) {
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected. "+segmentation+" "+criteria.getRelationModelName()+" "+criteria.getEgoRoleName());
		} else {
			result = new ArrayList<Report>();
	
			CensusCriteria censusCriteria = new CensusCriteria();
			censusCriteria.setCircuitType(CircuitType.RING);
			censusCriteria.setClosingRelation(criteria.getRelationModelName());
			censusCriteria.setClosingRelationEgoRole(criteria.getEgoRoleName());
			censusCriteria.setPattern(criteria.getPattern());
			censusCriteria.setRelationAttributeLabel(criteria.getLocalUnitLabel());
			censusCriteria.setDateLabel(criteria.getDateLabel());
			censusCriteria.setChainClassification(criteria.getChainClassification());
			censusCriteria.setRestrictionType(RestrictionType.ALL);
						
			Integer[] times = criteria.getDates();
			
			for (Relation relation : segmentation.getAllRelations().getByModelName(criteria.getRelationModelName())){
				relation.updateReferents(criteria.getDefaultReferentRoleName());
			}
			
			for (String alterRoleName : criteria.getRoleNames()){
	
				censusCriteria.setClosingRelationAlterRole(alterRoleName);
	
				if (times==null){
	
					Report report = reportFindCircuit(segmentation, censusCriteria, null);
					report.setTitle("Relations "+criteria.getEgoRoleName()+" "+alterRoleName);
					result.add(report);
	
				} else {
					
					for (int i=0;i<times.length;i++){
						
						censusCriteria.setRelationTime(times[i]);
	
						Report report = reportFindCircuit(segmentation, censusCriteria, null);
						report.setTitle("Relations "+criteria.getEgoRoleName()+" "+alterRoleName+" "+times[i]);
						result.add(report);
					}
				}
			}
			
		}
		
		//
		return result;
	}

}
