package org.tip.puck.partitions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.geo.Geography;
import org.tip.puck.matrix.Matrix;
import org.tip.puck.net.Individualizable;
import org.tip.puck.net.relations.workers.RelationValuator;
import org.tip.puck.partitions.Partition.ValueCode;
import org.tip.puck.sequences.Ordinal;
import org.tip.puck.sequences.Sequence;
import org.tip.puck.sequences.ValueSequence;
import org.tip.puck.util.Numberable;
import org.tip.puck.util.ToolBox;
import org.tip.puck.util.Value;
import org.tip.puck.workers.MetaValuator;

public class PartitionSequence<E> extends Sequence<Partition<E>>{
	
	List<Ordinal> times;	
	String label;
	Boolean numeric;
	
	public PartitionSequence(String label, List<Ordinal> times){
		super();
		
		this.times = times;
		this.label = label;
		
		for (Ordinal time : times){
			put(time, new Partition<E>(label+" "+time));
		}
	}
	
	public void put (Ordinal time, E item, Value value){
		
		if (time!=null && item !=null && value !=null){
			
			getStation(time).put(item, value);
			
		}
	}
	
	public Set<E> getItems(){
		Set<E> result;
		
		result = new HashSet<E>();
		
		for (Ordinal time : times){
			result.addAll(getStation(time).getItems());
		}
		//
		return result;
	}
	
	public List<E> getItemsAsList(){
		List<E> result;
		
		result = new ArrayList<E>(getItems());
		
		//
		return result;
	}
	
	public Value getValue (Ordinal time, E item){
		Value result;
		
		result = getStation(time).getValue(item);
		
		//
		return result;
	}
	
	public Map<Value,Double> getMeanValueFrequencies(){
		Map<Value,Double> result;
		
		result = new TreeMap<Value,Double>();

		for (E sliceable : getItems()){
			
			for (Ordinal time: times){
				
				Value value = getValue(time,sliceable);

				if (value!=null){
					Double count = result.get(value);
					if (count==null){
						count = 1.;
					} else {
						count += 1.;
					}
					result.put(value,count);
				}
			}
		}
		
		for (Value value : result.keySet()){
			if (result.get(value)==null){
				result.put(value,result.get(value)/new Double(times.size()));
			}
		}
		//
		return result;
	}
	
	public <V> Partition<E> getValueSequencePartition (){
		Partition<E> result;
		
		result = new Partition<E>();
		
		for (E member : getItems()){
			
			Sequence<Value> sequence = new Sequence<Value>();
			
			for (Ordinal time: times){
				
				sequence.put(time, getValue(time,member));
			}
			result.put(member, new Value(sequence.toValueString()));
		}
		//
		return result;
	}
	
	public ValueSequence getValueSequence(E item) throws PuckException{
		ValueSequence result;
		
		if (item instanceof Numberable){
			
			result = new ValueSequence(label, ((Numberable)item).getId());
			
			for (Ordinal time : times){
				result.put(time,getValue(time,(E)item));
			}
			//
			result.setProfile();
			
		} else {
			
			throw PuckExceptions.INVALID_PARAMETER.create(item +"is not numberable.");
		}
		
		return result;
	}
	
	public Cluster<E> getCluster(Ordinal time, Value value){
		Cluster<E> result;
		
		result = getStation(time).getCluster(value);
		
		//
		return result;
	}
	
	public int getClusterSize(Ordinal time, Value value){
		int result;
		
		Cluster<E> cluster = getCluster(time,value);
		if (cluster!=null){
			result = cluster.size();
		} else {
			result = 0;
		}
		//
		return result;

	}
	
	public double getClusterShare(Ordinal time, Value value){
		double result;
		
		Cluster<E> cluster = getCluster(time,value);
		if (cluster!=null){
			result = getStation(time).share(cluster);
		} else {
			result = 0.;
		}
		//
		return result;

	}
	
	public String getClusterItemsAsString(Ordinal time, Value value){
		String result;
		
		Cluster<E> cluster = getCluster(time,value);
		if (cluster!=null){
			result = cluster.getItemsAsString();
		} else {
			result = "";
		}
		//
		return result;

	}
	
	public Matrix getTransitionMatrix(){
		Matrix result;
		
		Map<String,Map<String,Integer>> transitionMap = new TreeMap<String,Map<String,Integer>>();
		List<String> values = new ArrayList<String>();
		
		for (E sliceable : getItems()){
			
			for (int i=1;i<times.size();i++){
				
				Value object1 = getValue(times.get(i-1),sliceable);
				Value object2 = getValue(times.get(i),sliceable);
				
				if (object1!=null && object2!=null){

					String value1 = object1.toString();
					String value2 = object2.toString();
					
					if (!values.contains(value1)){
						values.add(value1);
					}
					if (!values.contains(value2)){
						values.add(value2);
					}
					
					Map<String,Integer> targetMap = transitionMap.get(value1);
					if (targetMap==null){
						targetMap = new TreeMap<String,Integer>();
						transitionMap.put(value1,targetMap);
					}

					Integer count = targetMap.get(value2);
					if (count==null){
						count = 1;
					} else {
						count += 1;
					}
					targetMap.put(value2,count);
				}
			}
		}
		
		Collections.sort(values);
		String[] labels = new String[values.size()];
		for (int i=0;i<values.size();i++){
			labels[i] = values.get(i)+"";
		}
		
		result = new Matrix(values.size(),values.size());
		result.setRowLabels(labels);
		result.setColLabels(labels);
		
		for (Object value1 : transitionMap.keySet()){
			
			Map<String,Integer> targetMap = transitionMap.get(value1);

			for (Object value2 : targetMap.keySet()){
				
				result.augment(values.indexOf(value1), values.indexOf(value2), targetMap.get(value2));

			}
		}
		
		//
		return result;
	}
	
	public Set<Value> getValues(){
		Set<Value> result;
		
		result = new HashSet<Value>();
		
		for (Ordinal time : times){
			result.addAll(getStation(time).getValues());
		}
		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Value> getValuesAsSortedList() {
		List<Value> result;

		result = new ArrayList<Value>(getValues());
		Collections.sort(result);

		//
		return result;
	}

	public Boolean isNumeric(){
		
		if (numeric == null){

			numeric = true;
			
			for (Value value : getValues()){
				
				if (value.isNotNumber()){
					
					numeric = false;
					break;
				}
			}
		}
		//
		return numeric;
	}

	public List<?> getMapKeys() {
		List<?> result;
		
		result = null;
		
		Set<?> keySet = new HashSet();
			
		for (Value value : getValues()){
			
			if (value.isMap()){
				
				keySet.addAll(value.mapValue().keySet());
				
			} else {
				
				break;
			}
		}
		
		if (!keySet.isEmpty()){
			
			result = new ArrayList(keySet);
		}
		//
		return result;
	}
	
	public PartitionSequence<E> unpack(ValueCode mode){
		PartitionSequence<E> result;
		
		result = new PartitionSequence<E>(label, times);
		
		for (Ordinal time : times){
			
			for (E item : getStation(time).getItems()){
				
				Value value = getStation(time).getValue(item);
								
				if (value==null){
					
					continue;
					
				} else if (value.isPartition()){
					
					result.put(time, item, new Value(value.partitionValue().valueCode(mode)));
					
				} else if (value.isMap()){
					
					result.put(time, item, new Value(ToolBox.toKeySetString(value.mapValue())));
					
				} else {
					
					return this;
				}
			}
		}
		
		//
		return result;
	}
	
	public PartitionSequence<E> revalue(ValueCode mode) throws PuckException{
		PartitionSequence<E> result;
		
		result = new PartitionSequence<E>(label,times);
		
		for (Ordinal time : times){
			
			result.put(time, getStation(time).revalue(mode));
			
		}
		//
		return result;
	}

	public <V extends Numberable> PartitionSequence<V> aggregate(PartitionCriteria criteria) throws PuckException{
		PartitionSequence<V> result;
				
		result = new PartitionSequence<V>(label, times);
		
		for (Ordinal time : times){
			
			result.put(time, (Partition<V>)getStation(time).<V>aggregate(criteria));
						
		}
		//
		return result;
	}
	
	public <V extends Numberable> PartitionSequence<V> pseudo(PartitionCriteria criteria) throws PuckException{
		PartitionSequence<V> result;
				
		result = new PartitionSequence<V>(label, times);
		
		for (Ordinal time : times){
			
			result.put(time, PartitionMaker.createPseudoPartition((Partition<V>)getStation(time), criteria));
						
		}
		//
		return result;
	}
	
	public boolean isMulti (){
		boolean result;
		
		result = false;
		
		for (Ordinal time : times){
			
			if (getStation(time)!=null && getStation(time).isMulti()){
				
				result = true;
				break;
			}
		}
		//
		return result;
	}

	public boolean hasListValues (){
		boolean result;
		
		result = false;
		
		for (Ordinal time : times){
			
			if (getStation(time)!=null && getStation(time).hasListValues()){
				
				result = true;
				break;
			}
		}
		//
		return result;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public boolean hasIndividualizableItems(){
		boolean result;
		
		result = false;
		
		for (E item : getItems()){
			if (item!=null){
				if (item instanceof Individualizable){
					result = true;
				}
				break;
			}
		}
		//
		return result;
	}
	
	public Map<Value,PartitionSequence<E>> split (final PartitionCriteria criteria, final Geography geography) throws PuckException{
		Map<Value,PartitionSequence<E>> result;
		
		if (criteria == null) {
			result = null;
		} else {
			
			result = new HashMap<Value,PartitionSequence<E>>();
			
			for (E item : getItems()){
				Value splitValue = new MetaValuator<E>().get(item,criteria.getLabel(),geography);
				PartitionSequence<E> splitPartitionSequence = result.get(splitValue);
				if (splitPartitionSequence == null){
					splitPartitionSequence = new PartitionSequence<E>(getLabel()+"_"+splitValue,getTimes());
					result.put(splitValue,  splitPartitionSequence);
				}
				for (Ordinal time : getTimes()){
					splitPartitionSequence.put(time, item, getValue(time, item));
				}
			}
		}

		//
		return result;
	}
	
	


	
	

}
