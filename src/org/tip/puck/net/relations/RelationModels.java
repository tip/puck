package org.tip.puck.net.relations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.util.StringList;
import fr.devinsy.util.StringSet;

/**
 * 
 * @author TIP
 */
public class RelationModels extends ArrayList<RelationModel> {

	private static final long serialVersionUID = -6867755404579583569L;

	/**
	 * 
	 */
	public RelationModels() {
		super();
	}

	/**
	 * 
	 * @param source
	 */
	public RelationModels(final RelationModels source) {
		super();
		if (source != null) {
			for (RelationModel model : source) {
				this.add(new RelationModel(model));
			}
		}
	}

	/**
	 * This method does a deep copy of a list of RelationModel.
	 * 
	 * @param source
	 *            The list of RelationModel to copy and add.
	 */
	public void addAll(final RelationModels source) {
		//
		for (RelationModel model : source) {
			//
			add(new RelationModel(model));
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public RelationModel getByName(final String name) {
		RelationModel result;

		boolean ended = false;
		result = null;
		Iterator<RelationModel> iterator = this.iterator();
		while (!ended) {
			if (iterator.hasNext()) {
				RelationModel relationModel = iterator.next();
				if (StringUtils.equals(relationModel.getName(), name)) {
					ended = true;
					result = relationModel;
				}
			} else {
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public StringList nameList() {
		StringList result;

		result = new StringList(this.size());

		for (RelationModel relationModel : this) {
			//
			result.add(relationModel.getName());
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public StringList roleNames() {
		StringList result;

		StringSet roleNames = new StringSet();

		for (RelationModel model : this) {
			//
			roleNames.addAll(model.roles().toNameList());
		}

		result = roleNames.toStringList();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public StringList sortedNameList() {
		StringList result;

		result = nameList();
		Collections.sort(result);

		//
		return result;
	}
}
