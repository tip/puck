package org.tip.puck.net.workers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;

import org.tip.puck.PuckException;
import org.tip.puck.net.Individual;
import org.tip.puck.net.KinType;
import org.tip.puck.net.Net;

import oldcore.trash.OldIndividual;
import oldcore.trash.OldNet;

public class BicomponentWorker {
	
    /**
     * the number of steps 
     * <p> used for dsf search procedures
     */
    private int step;

	/**
	 * the stack of edges of the maximal bicomponent 
	 */
	private Stack<BicomponentEdge> bicomax;
	/**
	 * the map of bicomponent sizes
	 */
    private Map<Integer,Integer> bicomponents;
	
	/**
	 * the sum of keys
	 */
	private int volume;	

	/**
	 * the number of keys
	 */
	private int number;
	
	private Map<Individual, Status> status;
	
	private Stack<BicomponentEdge> bicomponentStack;
	private Stack<BicomponentEdge> workingStack;
	
	private Map<Individual,Integer> discovery;
	
	/**
	 * true if all bicomponents are to be kept, false if only the maximal bicomponent shall be guarded
	 */
	private boolean all;
	
    /**
     * matrimonial true if matrimonial bicomponents are computed, false if standard bicomponents are computed
     */
	private boolean matrimonial;
	   
	private enum Status {
		WAITING,
		ONGOING,
		DONE;
	}
	
	public BicomponentWorker(Net net, boolean matrimonial, boolean all){
		
		this.matrimonial = matrimonial;
		this.all = all;
		
		step = 0;
	   
		bicomax = new Stack<BicomponentEdge>();
	   
		bicomponents = new TreeMap<Integer,Integer>();
	   
		status = new HashMap<Individual,Status>();
		for (Individual individual : net.individuals()){
			status.put(individual, Status.WAITING);
		}
	}
	
	/**
	 * puts a number with its value or overwrites the value if it is lower  
	 * @param i the number to be put
	 * @param threshold the (maximum) threshold value
	 */
	static void add(Map<Integer,Integer> bicomponents, int i, int threshold){
		if (bicomponents.get(i)==null || bicomponents.get(i)> threshold) bicomponents.put(i,threshold);
	}
	
	/**
	 * copies all mappings from a second map
	 * @param map the map from which the mappings are copied
	 */
	static void addAll(Map<Integer,Integer> bicomponents, Map<Integer,Integer> map){
		for (int i : map.keySet()){
			add(bicomponents,i,map.get(i));
		}
	}
	
	
	
	   public static Net getCore(Net net){
		   Net result;
		   
		   result = new BicomponentWorker(net,true,true).getMaximalBicomponent(); 
		   
		   //
		   return result;
	   }

	   
	   /**
	    * gets the maximal (standard or matrimonial) bicomponent of the Net
	    */
	   private Net getMaximalBicomponent (){
		   Net result;
		   
		   workingStack = new Stack<BicomponentEdge>();
		   bicomponentStack = new Stack<BicomponentEdge>();
		   discovery = new HashMap<Individual,Integer>();
		   List<Individual> list = new ArrayList<Individual>(status.keySet());
		   Collections.sort(list);
		   
		   for (Individual individual: list){
			 
			   if (status.get(individual)==Status.WAITING && isPossibleStartingPoint(individual)) {
				   
				   bicomp (new BicomponentEdge(null,individual,null,0)); 
			   }
		   }
		   
		   getBicomponent(bicomponentStack,null,all);
		   
		   report(bicomponents,volume);
		   
		   result = fromEdges(bicomax);
		   return result;
	   }
	   
		/**
		 * <p>Algorithm from {@link http://www.ecst.csuchico.edu/~juliano/csci356/JAWAA/Bicomponents.html}
		 */
//		public int bicomp (final Individual ego, final Individual predecessor, int back, final KinType egoKinType){
		public int bicomp (BicomponentEdge edge){
			int back;
		
			// Preorder processing
			
			step++;
			System.out.println(step+"\t"+edge);
			
			status.put(edge.getTarget(), Status.ONGOING);
			discovery.put(edge.getTarget(), step);
			back = discovery.get(edge.getTarget());
			   
			// Standard procedure 
			
			for (KinType alterKinType : KinType.basicTypes()){
				
				// Special condition for matrimonial bicomponents
				if (!matrimonial || alterKinType != KinType.PARENT || edge.getKinType()!=KinType.CHILD) {
							
					for (Individual successor : edge.getTarget().getKin(alterKinType)){

						if (!successor.equals(edge.getSource()) && (status.get(successor)!=Status.DONE)) {
							
							System.out.println("\tNormal\t"+successor);
							back = bicomp1 (new BicomponentEdge(edge.getTarget(),successor,alterKinType,back));
						}
					}
			   }
			}
			
			// Supplementary procedure for leftovers of matrimonial bicomponents
			
			if (matrimonial && back<discovery.get(edge.getTarget()) && edge.getKinType()==KinType.CHILD) {
			
				for (Individual successor : edge.getTarget().getParents()){
				
					if (!successor.equals(edge.getSource()) && (status.get(successor)!=Status.DONE)) {
					
						System.out.println("\tSupplementary\t"+successor);
						back = bicomp1 (new BicomponentEdge(edge.getTarget(),successor,KinType.PARENT,back));
					}
				}
			}
			
			// Postorder processing
			
			status.put(edge.getTarget(),Status.DONE); 
			System.out.println(step+"\t"+edge+" "+back);

			
			//
			return back;
		}	   
		
		
//		public int bicomp1 (Individual ego, Individual successor, int back, KinType alterKinType){
		public int bicomp1 (BicomponentEdge edge){
			int result;
			
			// Inorder processing
			
			bicomponentStack.push(edge);
			
			if (status.get(edge.getTarget())==Status.WAITING) { 
				
				int cback = bicomp (edge); 
//				int cback = bicomp (successor,ego,back,alterKinType); 

				if (cback>discovery.get(edge.getSource())) {
					
					System.out.println("\tcut tail until "+edge+" "+
					clear(bicomponentStack,edge));
					
				} else if (cback==discovery.get(edge.getSource())) {
					
//					if (matrimonial && isStructuralChild(edge,bicomponentStack)) {
					if (matrimonial && edge.adjusted(edge.getSource())!=null && edge.adjusted(edge.getSource()).getKinType()==KinType.PARENT && bicomponentStack.peek().adjusted(edge.getSource())!=null && bicomponentStack.peek().adjusted(edge.getSource()).getKinType()==KinType.PARENT) {
						System.out.println("\txcut tail until "+edge+" "+
						clear(bicomponentStack,edge));
						
					} else {
						
						/**
						 * Problem: edge source may be structural child within the bicomponent; so recursive bicomponent extraction may be necessary...
						 */
						System.out.println("\tget Bicomponent until "+edge+": "+bicomponentStack);
						getBicomponent(bicomponentStack,edge,all);
					}
				}
				
				result = Math.min(edge.getBack(), cback);
				
			} else {
				
				System.out.println("\tOld "+edge);
				result = Math.min(edge.getBack(), discovery.get(edge.getTarget()));
			}
			//
			return result;
		}	
		
		/**
		 * Starting condition for matrimonial bicomponents. Starting with structural children may lead to parental triads (when they are always accessed from elsewhere they are necessarily cut)
		 * @param individual
		 * @return
		 */
		private boolean isPossibleStartingPoint(Individual individual){
			boolean result;
			
			result = !matrimonial ||
					(individual.numberOfParents()+individual.spouses().size()+individual.children().size() > 1 && 
					(individual.isFertile() || individual.isNotSingle()));
			
			//
			return result;
		}

	   
		
		/**
		 * augments the value of the ith item by one or adds the item to the list if it is not yet in it
		 * @param i the item
		 */
		void count (Map<Integer,Integer> bicomponents, int i){

			try {
				bicomponents.put(i,bicomponents.get(i)+1);
			} catch (NullPointerException npe){
				bicomponents.put (i,1);
			}
			number++;
			volume=volume+i;
		}

		//change to public report with ArrayList<String> return
		/**
		 * reports the list of items with their frequencies
		 */
		static void report (Map<Integer,Integer> bicomponents, int volume){
			
			for (int i : bicomponents.keySet()){
				System.out.println(i+"\t"+bicomponents.get(i));
			}
			System.out.println(bicomponents.size()+" "+volume);
		}
		
		


		   public static Net getMaximalBicomponent(Net net){
			   Net result;
			   
			   result = new BicomponentWorker(net,false,false).getMaximalBicomponent(); 
			   
			   //
			   return result;
		   }
		   

		   public static Net getKernel(Net net){
			   Net result;
			   
			   result = new BicomponentWorker(net,true,false).getMaximalBicomponent(); 
			   
			   //
			   return result;
		   }
		   
		   
		   public Net fromEdges (Stack<BicomponentEdge> edges) {
			   Net result;
			   
			   result = new Net();
			   while (!edges.isEmpty()){
				   BicomponentEdge edge = edges.pop();
				   Individual ego = result.getCloneWithAttributes(edge.getSource());
				   Individual alter = result.getCloneWithAttributes(edge.getTarget());
				   try {
					NetUtils.setKin(result, ego, alter, edge.getKinType().inverse());
				} catch (PuckException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			   }
			   
			   return result;
		   }
		   

			
			
		   /**
		    * Removes all the edges of a bicomponent from an edge stack and adds their number to the bicomponent census
		    * @param stack the edge stack
		    * @param edge the cut edge (down to which the edge stack is emptied)
		    * @param all true if all bicomponents are to be kept, false if only the maximal bicomponent shall be guarded
		    * @see OldNet.CopyOfNet#getMaximalBicomponent(boolean, boolean)
		    * @see elements.nodes.NewIndividual#bicomp(Individual, Stack, int, int, OldNet, boolean, boolean)
		    */
		   public void getBicomponent (Stack<BicomponentEdge> stack, BicomponentEdge e, boolean all){
			   Stack<BicomponentEdge> bico = new Stack<BicomponentEdge>();
			   if (e==null) bico = stack;
			   else {
				   bico.push(stack.pop());
				   while (bico.peek()!=e){
					   bico.push(stack.pop());
				   }
			   }
			   if (!bico.isEmpty()) count(bicomponents,bico.size());
			   if (!all && bico.size()<=bicomax.size()) return;
			   if (!all) bicomax.clear();
			   while (!bico.isEmpty()) bicomax.push(bico.pop());
		   }
		   
		    /**
		     * empties a stack of elements from top to bottom until a limiting element is reached
		     * @param <E> the element type
		     * @param stack the stack to be emptied
		     * @param e the limiting element
		     * @see OldIndividual#bicomp(OldIndividual, Stack, int, int, Net, boolean, boolean)
		     */
/*		   private static <E>void clear (Stack<E> stack, E e){
			   while (stack.pop()!=e){};
		   }	*/

		   private static <E> List<E> clear (Stack<E> stack, E e){
			   List<E> result;
			   
			   result = new ArrayList<E>();
			   E last = null;
			   
			   while (last!=e){
				   last = stack.pop();
				   result.add(last);
			   }
			   //
			   return result;
		   }	


}
