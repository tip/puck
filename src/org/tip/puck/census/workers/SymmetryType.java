package org.tip.puck.census.workers;

/**
 * 
 * @author TIP
 */
public enum SymmetryType {
	INVARIABLE,
	INVERTIBLE,
	PERMUTABLE
}
