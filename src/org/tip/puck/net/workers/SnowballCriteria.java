package org.tip.puck.net.workers;

import fr.devinsy.util.StringList;

/**
 * The {@code SnowballCriteria} class represents a criteria group for Snowball
 * Structure methods.
 * 
 * @author Klaus Hamberger
 * @author TIP
 * 
 */
public class SnowballCriteria {

	String relationModelName;
	String egoRoleName;
	String alternativeEgoRoleName;
	String seedLabel;
	String seedValue;
	String reachLabel;
	String indirectLabel;
	ExpansionMode expansionMode;
	int maxNrSteps;
	int seedReferenceYear;

	public SnowballCriteria() {
		//
		this.relationModelName = null;
		this.egoRoleName = null;
		this.alternativeEgoRoleName = null;
		this.seedLabel = null;
		this.seedValue = null;
		this.reachLabel = null;
		this.expansionMode = ExpansionMode.NONE;
		this.maxNrSteps = 0;
		this.seedReferenceYear = 0;
	}
	
	public String getRelationModelName() {
		return relationModelName;
	}

	public ExpansionMode getExpansionMode() {
		return this.expansionMode;
	}

	public int getMaxNrSteps() {
		return this.maxNrSteps;
	}

	public String getReachLabel() {
		return this.reachLabel;
	}

	public String getSeedLabel() {
		return this.seedLabel;
	}

	public int getSeedReferenceYear() {
		return this.seedReferenceYear;
	}

	public String getSeedValue() {
		return this.seedValue;
	}

	public void setRelationModelName(String relationModelName) {
		this.relationModelName = relationModelName;
	}

	public void setExpansionMode(final ExpansionMode expansionMode) {
		this.expansionMode = expansionMode;
	}

	public void setMaxNrSteps(final int maxStep) {
		this.maxNrSteps = maxStep;
	}

	public void setReachLabel(final String reachLabel) {
		this.reachLabel = reachLabel;
	}

	public void setSeedLabel(final String seedLabel) {
		this.seedLabel = seedLabel;
	}

	public void setSeedReferenceYear(final int year) {
		this.seedReferenceYear = year;
	}

	public void setSeedValue(final String seedValue) {
		this.seedValue = seedValue;
	}

	
	public String getIndirectLabel() {
		return indirectLabel;
	}

	public void setIndirectLabel(String indirectLabel) {
		this.indirectLabel = indirectLabel;
	}
	

	public String getEgoRoleName() {
		return egoRoleName;
	}

	public void setEgoRoleName(String egoRoleName) {
		this.egoRoleName = egoRoleName;
	}
	
	

	public String getAlternativeEgoRoleName() {
		return alternativeEgoRoleName;
	}

	public void setAlternativeEgoRoleName(String excludedEgoRoleName) {
		this.alternativeEgoRoleName = excludedEgoRoleName;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		StringList buffer = new StringList(20);
		buffer.append("{");
		buffer.append("relationModelName=[").append(this.relationModelName).append("], ");
		buffer.append("seedLabel=[").append(this.seedLabel).append("], ");
		buffer.append("seedValue=[").append(this.seedValue).append("], ");
		buffer.append("reachLabel=[").append(this.reachLabel).append("], ");
		buffer.append("expansionMode=[").append(this.expansionMode.name()).append("], ");
		buffer.append("maxStepCount=[").append(this.maxNrSteps).append("], ");
		buffer.append("seedReferenceYear=[").append(this.seedReferenceYear).append("]");
		buffer.append("}");

		result = buffer.toString();

		//
		return result;
	}
}
