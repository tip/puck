package org.tip.puck.visualization.exporter;

import java.io.File;
import org.gephi.project.api.Workspace;

/**
 *
 * @author Edoardo Savoia
 */
public interface Exporter {
    void exportToFile(File file, Workspace workspace);
}
