package org.tip.puck.census.workers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.census.chains.Chain;
import org.tip.puck.census.chains.ChainMaker;
import org.tip.puck.census.chains.Couple;
import org.tip.puck.census.chains.Notation;
import org.tip.puck.census.chains.Vector;
import org.tip.puck.geo.Geography;
import org.tip.puck.net.Families;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.relations.workers.RelationValuator;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.MultiPartition;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionMaker;
import org.tip.puck.report.Report;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.ToolBox;
import org.tip.puck.util.Trafo;
import org.tip.puck.util.Value;

import fr.devinsy.util.StringList;
import oldcore.trash.OldIndividual;
import oldcore.trash.OldRing;
import oldcore.trash.RingGroupMap;

public class CircuitFinder {

	Individuals targetDomain;
	private Individuals searchDomain;
	private Individuals linkDomain;
	private Families familyCompareDomain;
	Families familyDomain;
	Relations relationDomain;
	int lastId;
	
	Partition<Chain> circuits;
	
	// Warning! Do not use the itemtovalue map! This is an abuse of the partition class, since couples and pivots may have multiple values!
	Partition<Chain> couples;
	Partition<Individual> pivots;
	
	Map<Individual,List<Value>> pivotMap;
	
	List<Chain>[] couplesByOrder;
	Set<Individual>[] pivotsByOrder;
	CensusDetails censusDetails;
	
	List<Chain> forbiddenChains;
	List<Chain> chainModels;
	String forbiddenClusterLabel;
	Value forbiddenClusterValue;
	
	CensusCriteria criteria;


	boolean markIndividuals;
	boolean openChainFrequencies;
	
	List<Chain> consideredCouples;
	List<Chain> outOfCircuitCouples;
	
	int couplesConsidered;
	
	Geography geography;
	
	
	public int getCouplesConsidered() {
		return couplesConsidered;
	}

	/**
	 * the comparator for sorting chain types
	 */
//	protected KeyComparator comparator; // should be defined in Partition
										// class...

	String closingRelationType;
	String closingRelationEgoRole;
	String closingRelationAlterRole;
	
	String ascendingRelationType;
	String ascendingRelationEgoRole;
	String ascendingRelationAlterRole;
	
	String requiredAttribute;

	// fundamental fields

	Map<Individual, Map<Individual, Integer>> consanguines;
	/**
	 * the array of maximal canonic degrees of consanguineous components for
	 * given order
	 */
	protected int[] degrees;
	/**
	 * the array of maximal higher-order canonical degrees for given order
	 * <p>
	 * used for pivotchain-construction
	 */
	private int[] maxDeg;
	/**
	 * the chain schema (in positional notation)
	 */
	private String schema;

	// auxiliary fields
	
	private Gender firstGender;

	/**
	 * the maximal order of circuits
	 */
//	protected int dim; // check whether necessary

	// search parameters

	/**
	 * the group of chain models used for census by chain schema
	 */
//	protected ChainCluster models;

	/**
	 * true if the chain has order > 1 and contains at most 1 consanguineous
	 * bridge
	 */
//	private boolean solo;

	/**
	 * true if the map contains only linear chains or rings
	 */
	protected boolean linearOnly;

	/**
	 * true if only cross-sex chains are admitted
	 */
	protected boolean crossSex;

	// search paramaters in case of schema search

	/**
	 * true if all pivots have to be in a union
	 */
	protected boolean couplesOnly;
	/**
	 * the symmetry index of the rings
	 * <p>
	 * 0 not permutable, 1 ego-alter reflection possible, 2 fully permutable
	 */
	private SymmetryType symmetry;
	/**
	 * the filiation type for ascendant lines
	 */
	private FiliationType line;
	
	private String classification;

	// chain type parameters

	/**
	 * the sibling mode (1 all siblings assimilated, 2 no siblings assimilated,
	 * 3 full siblings assimilated)
	 */
	protected SiblingMode sib;
	/**
	 * the required circuit type
	 */
	private CircuitType ringType;
//	List<Individual> restrictedDomain;
	RestrictionType restriction;
	
	/**
	 * partition used to merge apical individuals
	 */
	Partition<Individual> mergingPartition; 
	/**
	 * partition used to link pivotal individuals
	 */
	Partition<Individual> linkingPartition; 
	
	private int[] nrCircuits;

	// for restricted circuit search

	private int[] nrCouples;
	private int[] nrIndividuals;

	private Map<Value,Integer> nrCircuitsForKey;
	private Map<Value,Integer> nrCouplesForKey;
	private Map<Value,Integer> nrPivotsForKey;
	private Map<Value,Integer> nrOpenChainsForKey;
	
	
	// for classificatory census

	private int[] nrCircuitTypes;

	// Output fields

	/**
	 * the array of numbers of pivotal vertices (total, male, female) for given
	 * order
	 */
//	private int[][] membersByRelationId;
	
	long time;
	
	public String getLabel(){
		String result = "";
		// to define
		//
		return result;
	}
	

	
	/**
	 * the maximal order of circuits
	 * @return
	 */
	private int dim(){
		return degrees.length;
	}
	
	public static Partition<Chain>createFirstCousinMarriages (final Segmentation segmentation) throws PuckException{
		Partition<Chain> result;
		
		CensusCriteria criteria = new CensusCriteria();

		criteria.setPattern("XX(X)XX");
		criteria.setSiblingMode(SiblingMode.ALL);
		criteria.setClosingRelation("SPOUSE");
		criteria.setChainClassification("POSITIONAL");
		criteria.setRestrictionType(RestrictionType.ALL);
		
		CircuitFinder finder = new CircuitFinder(segmentation,criteria);
		finder.findCircuits();
		result = finder.getCircuits();
		
		result.setLabel("First Cousin Marriages");

		//
		return result;
	}
	
	public static MultiPartition<Chain> createAncestorChains (final Segmentation segmentation, int degree) throws PuckException{
		MultiPartition<Chain> result;
		
		Partition<Chain> chains;

		CensusCriteria criteria = new CensusCriteria();
		
		String pattern = "";
		for (int i=0;i<degree;i++){
			pattern = pattern+"X";
		};
		pattern = pattern+"(X)";

		criteria.setPattern(pattern);
		criteria.setSiblingMode(SiblingMode.NONE);
		criteria.setSymmetryType(SymmetryType.INVARIABLE); //should be redundant
		criteria.setClosingRelation("LINEAR");
		criteria.setChainClassification("POSITIONAL");
		criteria.setRestrictionType(RestrictionType.ALL);
//		criteria.setRestrictionType(RestrictionType.EGO);

		CircuitFinder finder = new CircuitFinder(segmentation,criteria);
		finder.findCircuits();
		chains = finder.getCircuits();
		
		result = new MultiPartition<Chain>();
		
		for (Cluster<Chain> cluster : chains.getClusters().toListSortedByDescendingValue()){
			String value = cluster.getValue().stringValue();
			Value value1 = new Value("X"+value.substring(1));
			Value value2 = new Value(value.substring(0,1));
			Partition<Chain> row = result.getRow(value1);
			if (row==null){
				row = new Partition<Chain>();
			}
			row.putAll(cluster.getItems(), value2);
			result.put(row, value1);
		}
		
			
		result.setLabel("Ancestor Chains (degree=" + degree +")");
		
		//
		return result;
	}
	
	public CircuitFinder(){
		
	}
	

	
/*	public CircuitFinder (Individuals searchDomain){
		this.searchDomain = searchDomain;
		this.linkDomain = searchDomain;
	}*/


	
/*	private boolean checkForMarriage(Individual ego, Individual alter){
		boolean result = false;
		
		if (!relationType.equals("SPOUSE") && !relationType.equals("PARTN") && !relationType.equals("OPEN")){
			 if (ego.getSpouses().contains(alter)){
				 Chain chain = ChainMaker.createMarriage(ego, alter);
				 put(chain);
				 result = true;
			 }
		}
		//
		return result;
	}*/
	
	public String getPattern(){
		String result;
		
		result = criteria.getPattern().replaceAll(" ", "_");
		
		//
		return result;
	}
	

	/**
	 * 
	 * @throws PuckException
	 */
	public CircuitFinder(final Segmentation segmentation, final CensusCriteria criteria)
			throws PuckException {

/*		if (logger.isDebugEnabled()) {
			logger.debug(MessageFormat.format("[individuals=#{0},censusCriteria={1}",targetDomain.size(),LogHelper.toString(criteria)));
		}*/
		
		// Set highestIndividualId
		lastId = segmentation.getAllIndividuals().getLastId();

		//
		this.criteria = criteria;
		this.censusDetails = criteria.getCensusDetails();
			
		// Translate pattern.
			
		if (StringUtils.isBlank(criteria.getPattern())) {
			this.degrees = null;
			this.schema = null;
		} else if (Pattern.matches("^[ ,\\t\\d]+$", criteria.getPattern())) {
			this.degrees = ToolBox.stringsToInts(criteria.getPattern());
			this.schema = null;
		} else {
			this.degrees = null;
			this.schema = criteria.getPattern();
		}

//		logger.debug("[degrees=" + LogHelper.toString(this.degrees) + ",schema="+schema+"]");
		
		if ((degrees == null) && (schema == null)) {
			throw PuckExceptions.INVALID_PARAMETER.create("Pattern not defined [{0}]", criteria.getPattern());
		} else {
			
			// initialize restriction (define domains)
			restriction = criteria.getRestrictionType();
			if (restriction == RestrictionType.NONE){
				this.targetDomain = segmentation.getAllIndividuals();
				this.setSearchDomain(segmentation.getAllIndividuals());
				this.familyDomain = segmentation.getAllFamilies();
				this.relationDomain = segmentation.getAllRelations();
			} else if (restriction == RestrictionType.ALL) {
				this.targetDomain = segmentation.getCurrentIndividuals();
				this.setSearchDomain(segmentation.getCurrentIndividuals());
				this.familyDomain = segmentation.getCurrentFamilies();
				this.relationDomain = segmentation.getCurrentRelations();
			} else {
				this.targetDomain = segmentation.getCurrentIndividuals();
				this.familyDomain = segmentation.getCurrentFamilies();
				this.setSearchDomain(segmentation.getAllIndividuals());
				this.relationDomain = segmentation.getCurrentRelations();
			}
			this.familyCompareDomain = segmentation.getAllFamilies();
			
			// initialize relation and symmetry types
			this.closingRelationType = criteria.getClosingRelation();
			this.closingRelationEgoRole = criteria.getClosingRelationEgoRole();
			this.closingRelationAlterRole = criteria.getClosingRelationAlterRole();

			this.ascendingRelationType = criteria.getAscendingRelation();
			this.ascendingRelationEgoRole = criteria.getAscendingRelationEgoRole();
			this.ascendingRelationAlterRole = criteria.getAscendingRelationAlterRole();
			
			if ((StringUtils.isNotBlank(closingRelationType) && (closingRelationType.charAt(0) == '@'))) {
				linkingPartition = PartitionMaker.createRaw(closingRelationType.substring(1),getSearchDomain(), geography);
//				partition.relationalize();
				this.symmetry = SymmetryType.INVERTIBLE;
			} else if (closingRelationType.equals("SPOUSE") || closingRelationType.equals("PARTN")) {
				this.symmetry = SymmetryType.PERMUTABLE;
			} else if (closingRelationType.equals("LINEAR")) {
				this.symmetry = SymmetryType.INVARIABLE;
			} else if (closingRelationType.equals("OPEN") || closingRelationType.equals("COSPOUSE")) {
				this.symmetry = SymmetryType.INVERTIBLE;
			} else if ((closingRelationEgoRole==null && closingRelationAlterRole==null) || closingRelationEgoRole.equals(closingRelationAlterRole)){
				if (ascendingRelationType.equals("PARENT")){
					this.symmetry = SymmetryType.INVERTIBLE;
				} else {
					this.symmetry = SymmetryType.PERMUTABLE;
				}
			} else {
				this.symmetry = SymmetryType.INVARIABLE;
			}
			
			if (symmetry == SymmetryType.PERMUTABLE){
				this.setLinkDomain(this.getSearchDomain());
			} else {
				this.setLinkDomain(segmentation.getAllIndividuals());
			}
			
		    // initialize chain properties
			this.sib = criteria.getSiblingMode();
			this.line = criteria.getFiliationType();
			this.ringType = criteria.getCircuitType();
			
			// initialize gender and marriage conditions
		    if (closingRelationType.equals("SPOUSE") || closingRelationType.equals("PARTN")) {
				this.crossSex = true;
				this.couplesOnly = true;
		    } else {
				this.crossSex = criteria.isCrossSexChainsOnly();
				this.couplesOnly = criteria.isCouplesOnly();
		    }
			
			// define requiredAttribute for equivalence relations
			if (closingRelationType.charAt(0)=='@'){
				requiredAttribute = closingRelationType.substring(1);
			}

			
			// initialize report criteria
			firstGender = criteria.getFirstGender();
			
			// make partition for ancestor identification
			if (!Trafo.isNull(criteria.getClassificatoryLinking()) && !criteria.getClassificatoryLinking().equals("NONE")){
				this.mergingPartition = PartitionMaker.createRaw(criteria.getClassificatoryLinking(), targetDomain, geography);
				this.sib = SiblingMode.NONE;
			}
			
			// initialize circuit classification criteria
			this.classification = criteria.getChainClassification();
			
			// initialize partitions
			circuits = new Partition<Chain>(); 
			couples = new Partition<Chain>();
			pivots = new Partition<Individual>();
			
			// initialize filter
			String filter = criteria.getFilter();
			if (StringUtils.isNotEmpty(filter) && filter.contains("=")){
				String[] splitFilter = filter.split("=");
				forbiddenClusterLabel=splitFilter[0];
				forbiddenClusterValue=new Value(splitFilter[1]);
			} else {
				forbiddenChains = getChainModels(criteria.getFilter()); 
			}

			// initialize clusters (for search by formula)
			if (schema != null) {
				// Update dimension
				int dim = 1;
				for (String s : schema.split("\\s")) {
					int d = s.split("\\.").length;
					if (d>dim) {
						dim = d;
					}
				}
				degrees = new int[dim];
				
				chainModels = getChainModels(schema);
				
				for (Chain model : chainModels){
					// Update degrees
					int deg = model.depth();
					
					if (deg > degrees[model.dim()-1]) {
						degrees[model.dim()-1] = deg;
					}
					// Initialize cluster
					if (!isForbidden(model)){
						circuits.putCluster(ChainValuator.get(model,classification));
					}
				}
				// Filter is incorporated and no longer necessary for search by formula
				forbiddenChains = null;
				
/*				models = new ChainCluster(crossSex, schema, sib, line, restriction, symmetry);
				Collections.sort(models);
				for (Chain chain : models) {
					getCircuits().putCluster(chain.getCharacteristicVector());
				}
				this.degrees = getMaxDeg(models);
				linearOnly = models.isLinear();
				solo = solo();*/
			}
			
			// include direct marriage as a possible chain for non-matrimonial closing relations
/*		    if (!relationType.equals("SPOUSE") && !relationType.equals("PARTN")&& !relationType.equals("OPEN")) {
				if (dim()==1){
					degrees = new int[]{degrees[0],0};
				}
		    }*/
			
			
			// calculate the maximal search depth
			maxDeg = new int[dim()];
			for (int i = 0; i < dim(); i++) {
				maxDeg[i] = degrees[i];
				for (int j = i + 1; j < dim(); j++) {
					if (degrees[j] > maxDeg[i]) {
						maxDeg[i] = degrees[j];
					}
				}
			}
						
			// initialize lists for couples and pivots count
			couplesByOrder = new List[dim()+1];
			pivotsByOrder = new Set[dim()+1];
			
			for (int i = 0; i < dim()+1; i++) {
				couplesByOrder[i] = new ArrayList<Chain>();
				pivotsByOrder[i] = new HashSet<Individual>();
			}
			
			// set individual marking
			
			markIndividuals = criteria.isMarkIndividuals();
			if (markIndividuals){
				pivotMap = new HashMap<Individual,List<Value>>();
			}
			
			// set openChainFrequency option
			
			if (!closingRelationType.equals("OPEN")){
				openChainFrequencies = criteria.isOpenChainFrequencies();
			}
			
			if (criteria.isWithOutOfCircuitCouples()){
				consideredCouples = new ArrayList<Chain>();
			}
		}
		
		geography = segmentation.getGeography();
	}
	
	public Partition<Chain> getOpenChains () throws PuckException{
		Partition<Chain> result;

		//
		CircuitFinder openChainFinder = new CircuitFinder();
		
		openChainFinder.targetDomain = targetDomain;
		openChainFinder.setSearchDomain(searchDomain);
		openChainFinder.setLinkDomain(getSearchDomain());
		openChainFinder.degrees = degrees;
		openChainFinder.maxDeg = maxDeg;
		openChainFinder.schema = schema;
		openChainFinder.forbiddenChains = forbiddenChains;
		openChainFinder.chainModels = chainModels;
		openChainFinder.classification = classification;
		openChainFinder.mergingPartition = mergingPartition;
		openChainFinder.crossSex = crossSex;
		openChainFinder.couplesOnly = couplesOnly;
		openChainFinder.markIndividuals = markIndividuals;
		openChainFinder.ringType = ringType;
		openChainFinder.line = line;
		openChainFinder.restriction = restriction;
		openChainFinder.sib = sib;
		openChainFinder.censusDetails = censusDetails;
		openChainFinder.firstGender = firstGender;
		openChainFinder.linkingPartition = linkingPartition;
		openChainFinder.requiredAttribute = requiredAttribute;
		openChainFinder.ascendingRelationType = ascendingRelationType;
		openChainFinder.ascendingRelationEgoRole = ascendingRelationEgoRole;
		openChainFinder.ascendingRelationAlterRole = ascendingRelationAlterRole;
		openChainFinder.lastId = lastId;
		
		openChainFinder.circuits = new Partition<Chain>(); 
		if (schema!=null){
			for (Cluster<Chain> circuitCluster : circuits.getClusters()){
				openChainFinder.circuits.putCluster(circuitCluster.getValue());
			}
		}
		
		openChainFinder.closingRelationType = "OPEN";
		openChainFinder.symmetry = SymmetryType.INVERTIBLE;
		
		openChainFinder.findCircuits();
		
		result = openChainFinder.getCircuits();

		nrOpenChainsForKey = new HashMap<Value,Integer>();
		for (Value key : result.getValues()){
			nrOpenChainsForKey.put(key, result.getValueFrequency(key));
		}
		
		//
		return result;
	}
	
	private List<Chain> getChainModels(String schema){
		
		List<Chain> result = new ArrayList<Chain>();
		
		if (schema!=null){
			
			for (String subSchema : schema.split("\\s")) {
				
				if (subSchema!=null && subSchema.length() > 0) {
					
					if (subSchema.charAt(0) == '<') {
						
						for (String partSchema : formulae(subSchema.substring(1))) {
							
							develop(result, ChainMaker.transform(partSchema), 0);
						}
						
					} else {
						
						develop(result, ChainMaker.transform(subSchema), 0);
					}
				}
			}
		}
		
		//
		return result;
	}
	


/*	private List<Chain> getChainModels(String schema){
		List<Chain> result = new ArrayList<Chain>();
		
		if (schema!=null){
			
			for (String subSchema : schema.split("\\s")) {
				
				if (subSchema!=null && subSchema.length() > 0) {
					
					if (subSchema.charAt(0) == '<') {
						
						String[] consanguineSchemas = subSchema.substring(1).split("\\.");
						List<Chain>[] consanguineResults = new ArrayList[consanguineSchemas.length];
						
						int i = 0;
						for (String consanguineSchema : consanguineSchemas){
							
							consanguineResults[i] = new ArrayList<Chain>();
							
							for (String partSchema : formulae(consanguineSchema)) {
								
								System.out.println("Part "+consanguineSchema+" "+partSchema);
								develop(consanguineResults[i], ChainMaker.transform(partSchema), 0);
							}
							//
							i++;
						}
						
						List<Chain> preresult = new ArrayList<Chain>();
						result.addAll(consanguineResults[0]);
						
						for (int j=1; j<consanguineSchemas.length;j++){

							preresult = result;
							result = new ArrayList<Chain>();
							
							for (Chain chain1 : preresult){
								for (Chain chain2 : consanguineResults[j]){
									
									// Warning: not checked for order > 2, perhaps higher renumbering necessary
									
									Chain chain3 = ChainMaker.concatenateWithMarriage(chain1, chain2.cloneFromId(chain1.size()+1));
									result.add(chain3);
								}
							}
						}
						
					} else {
						
						System.out.println("Part "+subSchema);
						develop(result, ChainMaker.transform(subSchema), 0);
					}
				}
			}
		}

		//
		return result;
	}*/
	
	
	private static ArrayList<String> formulae(String str) {
		ArrayList<String> result;
		
		result = new ArrayList<String>();
		
		String[] strs = str.split("\\.");

		ArrayList<String>[] consanguineFormulae = new ArrayList[strs.length];
		
		for (int i=0;i<strs.length;i++){
			
			consanguineFormulae[i] = consanguineFormulae(strs[i]);
		}
		
		result.addAll(consanguineFormulae[0]);
		
		for (int i=1;i<strs.length;i++){
			
			ArrayList<String> truncateResult = result;
			result = new ArrayList<String>();

			for (String trunk : truncateResult){

				for (String tip : consanguineFormulae[i]){
					
					result.add(trunk+"."+tip);
				}
			}
		}


		//
		return result;
	}

	
	
	/**
	 * gets the list of all formulae for chains whose formula are contained in a
	 * given formula
	 * <p>
	 * corresponds the list of all kinship relations up to an upper limit
	 * 
	 * @param str
	 *            the englobing Chain formula
	 * @return the list of included formulae
	 * @see groups.RingGroup#RingGroup(boolean, String, int, int, int)
	 */
	private static ArrayList<String> consanguineFormulae(String str) {
		ArrayList<String> result;
		
		int left = 0;
		int right = 0;
		
		if (str.indexOf("(")>-1){
			
			left = str.indexOf("(");
			
		}
		if (str.indexOf(")")>-1){
			
			right = str.length()-1-str.indexOf(")");
			
		}

		result = new ArrayList<String>();
		
		str = str.replaceAll("\\(\\)", "\\(X\\)");
		
		char first = str.charAt(0);
		char last = str.charAt(str.length()-1);
			
		ArrayList<String> leftStrings = new ArrayList<String>();
		ArrayList<String> rightStrings = new ArrayList<String>();
		
		String leftString = "";
		String rightString = "";
		
		leftStrings.add(leftString);
		rightStrings.add(rightString);
		
		for (int i=0;i<left;i++){
			
			leftString += "X";
			leftStrings.add(first+leftString.substring(1));
		}
		
		for (int i=0;i<right;i++){
			
			rightString += "X";
			rightStrings.add(rightString.substring(0, rightString.length()-1)+last);
		}
		
		for (String leftStr : leftStrings){
			
			for (String rightStr : rightStrings){
				
				String totalStr = leftStr+"(X)"+rightStr;
				
				if (totalStr.equals("(X)") && first==last){
					totalStr = "("+first+")";
				}
				
				if (!result.contains(totalStr) && !result.contains(inverse(totalStr))){

					result.add(totalStr);
				}
			}
		}
		
		//
		return result;
	}
	
	/**
	 * inverts a string
	 * 
	 * @param s
	 *            the string to be inverted
	 * @return the inverted string
	 * @see groups.RingGroup#formulae(String)
	 */
	private static String inverse(final String s) {
		String str = "";
		for (char c : s.toCharArray()) {
			if (c == '(') {
				str = ')' + str;
			} else if (c == ')') {
				str = '(' + str;
			} else {
				str = c + str;
			}
		}
		return str;
	}	
	
	/**
	 * replaces chain formula variables
	 * 
	 * @param str
	 *            the Chain formula (in positional notation)
	 * @param x
	 *            the variable to be replaced
	 * @param y
	 *            the letter to replace it
	 * @return the new Chain formula
	 * @see groups.RingGroup#develop(boolean, String, int, int)
	 */
	private String fill(final String str, final String x, final String y) {
		if (line == FiliationType.COGNATIC || x.equals("Y")
				|| (line == FiliationType.AGNATIC && y.equals("H") || (line == FiliationType.UTERINE && y.equals("F")))) {
			return str.replaceFirst(x, y);
		}
		int i = str.indexOf(x);
		if (i == 0 || i == str.length() - 1 || str.charAt(i + 1) == '.' || str.charAt(i - 1) == '.') {
			return str.replaceFirst(x, y);
		}
		return null;
	}	
	
	/**
	 * FIXME Refined crossSex condition to add
	 * FIXME Replace by static method in ChainMaker
	 * @param models
	 * @param str
	 * @param k
	 */
	private void develop(List<Chain> models, final String str, final int k) {
		
		if (ChainMaker.isWellFormed(str)) {
			
			for (int i = k; i < str.length(); i++) {
				
				if (str.charAt(i) == 'X') {
					
					develop(models, fill(str, "X", "H"), i);
					develop(models, fill(str, "X", "F"), i);
					
					//
					return;
				}
				
				if (str.charAt(i) == 'Y') {
					
					if (sib != SiblingMode.ALL) {
						develop(models, fill(str, "Y", "X"), k);
					}
					
					if (sib != SiblingMode.NONE) {
						develop(models, fill(str, "Y", ""), k);
					}
					//
					return;
				}
			}
			
			// introduce refined homosexuality definition (inner or outer)
			if (!ChainMaker.containsHomosexualMarriages(crossSex, str)) {
				
				Chain model = ChainMaker.fromString(str);			
				model.setSymmetry(symmetry);
				models.add(model.standard());
			}
		}

		// Update linearity
//		if (r.linear()) linearOnly = false;

		// Moved to intializeClusters
		// Update degrees
/*		int deg = r.depth();
		int dim = r.dim();
		if (deg > degrees[dim-1]) {
			degrees[dim-1] = deg;
		}
		circuits.putCluster(ChainValuator.get(r.standard(),classification));*/
		
	}	
	

	void begin() {
		time = System.currentTimeMillis();
	}

	private void buildCircuits(final List<Chain> chains, final List<Chain>[] br) {
		
		if (chains.size() == br.length) {
			
			Chain circuit = ChainMaker.concatenate(chains, degrees[chains.size() - 1]);
			
			put(circuit);
			return;
		}
		for (Chain chain : br[chains.size()]) {
			chains.add(chain);
			buildCircuits(chains, br);
			chains.remove(chain);
		}
	}


	/**
	 * aggregates chains into a new RingNet according to clusters (instead of
	 * characteristic vectors)
	 * 
	 * @param rn
	 *            the underlying RingNet
	 */
/*	public Partition<Chain> byAttributes(final String propertyLabel) {
		Partition<Chain> result = new Partition<Chain>();
		// Define comparator for partition (KeyComparator(1))

		for (Cluster<Chain> rings : circuits.getClusters()) {
			Object clu = rings.getFirstItem().getAttributeValue(propertyLabel);
			result.putAll(rings.getItems(), clu);
		}
		//
		return result;
	}*/
	
/*	public Partition<Chain> byCouples(final Gender firstGender) {
		Partition<Chain> result = new Partition<Chain>();
		// Define comparator for partition (KeyComparator(1))

		for (Cluster<Chain> cluster : circuits.getClusters()) {
			for (Chain r : cluster.getItems()) {
				for (int i = 0; i < 2 * r.dim(); i++) {
					if (symmetry != SymmetryType.PERMUTABLE && i > 0) {
						continue;
					}
					
					Chain couple = r.getCouple(i);
					
					Chain s = r.transform(i);
					if (s.getFirst().getGender() == firstGender) {
						result.put(s,new Value(couple));
					}
				}
			}
		}

		//
		return result;
	}*/
	
	
	public Map<Chain,Set<Cluster<Chain>>> coupleChainClusterMap (final Gender firstGender){
		Map<Chain,Set<Cluster<Chain>>> result = new HashMap<Chain,Set<Cluster<Chain>>>();
		
		for (Cluster<Chain> cluster : circuits.getClusters()) {
			for (Chain chain : cluster.getItems()) {
				for (int i = 0; i < 2 * chain.dim(); i++) {
					if (symmetry != SymmetryType.PERMUTABLE && i > 0) {
						continue;
					}
					
					Chain couple = chain.getCouple(i);
					
					if (couple.getFirst().getGender() == firstGender) {
						Set<Cluster<Chain>> clusters = result.get(couple);
						if (clusters == null){
							clusters = new HashSet<Cluster<Chain>>();
							result.put(couple, clusters);
						}
						clusters.add(cluster);
					}
				}
			}
		}
		
		//
		return result;
		
	}
	
	private int getCouplePosition(Chain couple, Chain circuit){
		int result;
		
		Individual ego = couple.getFirst();
		Individual alter = couple.getLast();
		
		for (result=0;result<2*circuit.dim();result++){
			
			if (circuit.getPivot(result).equals(ego)){
				if ((result%2==0 && circuit.getPivot(result-1).equals(alter)) ||
					(result%2==1 && circuit.getPivot(result+1).equals(alter)))
					break;
			}
			
		}
		//
		return result;
	}
	
	public static Partition<Chain> createCouplePartition (final Partition<Chain> circuits, final Gender firstGender, final boolean crossSex, final SymmetryType symmetry){
		Partition<Chain> result;
		
		result = new Partition<Chain>();
		
		for (Cluster<Chain> cluster : circuits.getClusters()) {
			for (Chain circuit : cluster.getItems()) {
				for (Chain couple : circuit.getCouples(crossSex, symmetry, firstGender)){
					result.put(couple, cluster.getValue());
				}
			}
		}
		
		//
		return result;
	}
	
	public Map<Chain,Set<Chain>> coupleChainMap (final Gender firstGender){
		Map<Chain,Set<Chain>> result = new HashMap<Chain,Set<Chain>>();
		
		for (Cluster<Chain> cluster : circuits.getClusters()) {
			for (Chain circuit : cluster.getItems()) {
				for (Chain couple : circuit.getCouples(crossSex, symmetry, firstGender)){
					Set<Chain> clusters = result.get(couple);
					if (clusters == null){
						clusters = new HashSet<Chain>();
						result.put(couple, clusters);
					}
					clusters.add(circuit.transform(getCouplePosition(couple,circuit)));
				}
				
				
/*				for (int i = 0; i < 2 * circuit.dim(); i++) {
					if (symmetry != SymmetryType.PERMUTABLE && i > 0) {
						continue;
					}
					
					Chain couple = circuit.getCouple(i);
					
					if (couple.getFirst().getGender() == firstGender) {
						Set<Chain> clusters = result.get(couple);
						if (clusters == null){
							clusters = new HashSet<Chain>();
							result.put(couple, clusters);
						}
						clusters.add(circuit.transform(i));
					}
				}*/
			}
		}
		
		//
		return result;
		
	}
	
	public List<Chain> getCouples (final Chain circuit, final Gender firstGender){
		List<Chain> result;
		
		result = new ArrayList<Chain>();
		
		for (int i = 0; i < 2 * circuit.dim(); i++) {
			if (symmetry != SymmetryType.PERMUTABLE && i > 0) {
				continue;
			}
			
			Chain couple = circuit.getCouple(i);
			if (couple.getFirst().getGender() == firstGender) {
				result.add(couple);
			}
		}
		//
		return result;
	}
	
	public Map<Chain,List<Cluster<Chain>>> clustersByCouples(final Gender firstGender) {
		Map<Chain,List<Cluster<Chain>>> result = new TreeMap<Chain,List<Cluster<Chain>>>();

		for (Cluster<Chain> cluster : circuits.getClusters()) {
			for (Chain circuit : cluster.getItems()) {
				for (Chain couple : getCouples(circuit,firstGender)){
					List<Cluster<Chain>> clusters = result.get(couple);
					if (clusters==null){
						clusters = new ArrayList<Cluster<Chain>>();
						result.put(couple, clusters);
					}
					if (!clusters.contains(cluster)){
						clusters.add(cluster);
					}
				}
			}
		}

		//
		return result;
	}
	
	/**
	 * decomposes a circuit intersection network
	 */
	public Report decompose () {
		Report result;
		
		result = new Report("Decomposition");
		
		List<Cluster<Chain>> clusters = getCircuits().getClusters().toListSortedByDescendingSize();

		StringList protocol = new StringList();
		Map<Cluster<Chain>,Integer> originalSizes = new HashMap<Cluster<Chain>,Integer>(); 
		
		int originalNrItems = 0;
		int originalNrClusters = 0;
				
		for (Cluster<Chain> cluster : clusters) {
			originalSizes.put(cluster,cluster.size());
			originalNrItems += cluster.size();
			originalNrClusters += 1;
		}
		
		int k = 1;
		
		protocol.appendln("Decomposition of circuit cluster network "+getPattern());
		protocol.appendln("Nr\tmaxCluster\tsize (% of original)\tremaining circuits");
		
		while (clusters.size()>0) {

			Cluster<Chain> maxCluster = clusters.get(0);
			
			// Count
			int itemsCount = 0;
			for (Cluster<Chain> cluster : clusters){
				itemsCount += cluster.size();
			}
			double survivingItems = MathUtils.percent(itemsCount,originalNrItems);
			double survivingClusters = MathUtils.percent(clusters.size(),originalNrClusters);
			int maxClusterOriginalSize = originalSizes.get(maxCluster);
			double survivalRate = MathUtils.percent(maxCluster.size(),maxClusterOriginalSize);
			
			protocol.appendln(k+"\t"+maxCluster+"\t"+maxCluster.size()+" circuits of "+maxClusterOriginalSize+" ("+survivalRate+"%)\t"+itemsCount+" circuits ("+survivingItems+"% of circuits in "+survivingClusters+"% of types)");
	        
			clusters.remove(maxCluster);
			
			// Remove circuits that share chain with removedCluster
			for (Chain r : maxCluster.getItems()) {
				for (Chain couple : getCouples(r, Gender.MALE)){
					for (Cluster<Chain> cluster: clusters){
						for (Chain otherCircuit : new ArrayList<Chain>(cluster.getItems())) {
							for (Chain otherCouple : getCouples(otherCircuit, Gender.MALE)){
								if (couple.equals(otherCouple)){
									cluster.remove(otherCircuit);
								}
							}
						}
					}
				}
			}
			
			// Remove empty clusters
			for (Cluster<Chain> cluster : new ArrayList<Cluster<Chain>>(clusters)){
				if (cluster.size()==0){
					clusters.remove(cluster);
				}
			}
						
			k = k+1;
		}
		
		result.outputs().append(protocol);

		//
		return result;
	}	

	

/*	public Partition<Chain> byCouplesWithoutTransformation(final Gender firstGender) {
		Partition<Chain> result = new Partition<Chain>();
		// Define comparator for partition (KeyComparator(1))

		for (Cluster<Chain> cluster : circuits.getClusters()) {
			for (Chain r : cluster.getItems()) {
				for (int i = 0; i < 2 * r.dim(); i++) {
					if (symmetry != SymmetryType.PERMUTABLE && i > 0) {
						continue;
					}
					
					Chain couple = r.getCouple(i);
					
					if (couple.getFirst().getGender() == firstGender) {
						result.put(r,new Value(couple));
					}
				}
			}
		}

		//
		return result;
	}*/
	
	private boolean isForbidden (final Chain chain){
		boolean result = false;
		
		if (forbiddenChains != null){
			for (Chain model : forbiddenChains){
				if (chain.getCharacteristicVector().equals(model.getCharacteristicVector())){
					result = true;
					break;
				}
			}
		}
		
		if (forbiddenClusterLabel !=null){
			Value value = ChainValuator.get(chain, forbiddenClusterLabel);
			if (value!=null && value.equals(forbiddenClusterValue)){
				result = true;
			}
		}
		//
		return result;
	}
	
	private boolean hasCorrectForm (final Chain chain){
		boolean result = false;
		if (chainModels != null){
			for (Chain model : chainModels){
				if (chain.getCharacteristicVector().equals(model.getCharacteristicVector())){
					result = true;
					break;
				}
			}
		}
		//
		return result;
	}
	
	
	
	/**
	 * checks whether a NumberChain is admissible according to cluster
	 * affiliations
	 * for relations of order 1
	 * @param chain
	 *            the NumberChain to be checked
	 * @return true if the NumberChain is not admissible
	 */
	private boolean notInDomain(final Individual ego, final Individual alter) {
		boolean result = false;
		
			switch (restriction){
			case NONE:
				result = false;
				break;
			case ALL:
				if (symmetry == SymmetryType.INVARIABLE){
					result = !targetDomain.contains(ego);	
				} else {
					result = !targetDomain.contains(ego) || !targetDomain.contains(alter);
				}
				break;
/*			case LASTMARRIED:
				result = !targetDomain.contains(ego) && !targetDomain.contains(alter);
				break;
			case EGO:
				result = !targetDomain.contains(ego);
				break;*/
			case SOME:
				result = !targetDomain.contains(ego) && !targetDomain.contains(alter);
				break;
			}
		return result;
	}
	
	private boolean notInFamilyDomain(final Individual ego, final Individual alter) {
		boolean result = false;
		
		result = ((closingRelationType.equals("SPOUSE") || closingRelationType.equals("PARTN")) && familyDomain.getBySpouses(ego, alter)==null);
		//
		return result;
	}
	
	
	/**
	 * checks whether a NumberChain is admissible according to cluster
	 * affiliations
	 * @param chain
	 *            the NumberChain to be checked
	 * @return true if the NumberChain is not admissible
	 */
	private boolean notInDomain(final Chain chain) {
		boolean result = false;
		
/*		if (violatesGenderRelation(chain)) {
			result = true; 
		} else if (relationType.equals("OPEN") && chain.getFirst()==chain.getLast()){
			result = true;
		} else {*/
			switch (restriction){
			case NONE:
				result = false;
				break;
			case LASTMARRIED:
//				result = !targetDomain.contains(chain.getLastMarried());
				result = (familyDomain!=familyCompareDomain && !familyDomain.contains(chain.getLastMarriage(familyCompareDomain)));
				
				break;
/*			case EGO:
				result = !targetDomain.contains(chain.getFirst());
				break;
			case EGOALTER:
				result = !targetDomain.contains(chain.getFirst()) && !targetDomain.contains(chain.get(1));
				break;*/
			case SOME:
				result = true;
				switch (symmetry){
				case PERMUTABLE:
					for (Individual i : chain) {
						if (targetDomain.contains(i)) {
							result = false;
							break;
						}
					}
					break;
				case INVERTIBLE:
					
					if (closingRelationType.equals("OPEN")){
						result = !targetDomain.contains(chain.getFirst()) && !targetDomain.contains(chain.getLast());
					} else {
						result = !targetDomain.contains(chain.getFirst()) && !targetDomain.contains(chain.get(1));
					}
					
					break;
				case INVARIABLE:
					result = !targetDomain.contains(chain.getFirst());
					break;
				}
				if (result==false){
					result = chain.hasNoFamiliesInDomain(familyDomain, closingRelationType);					
				}
				break;
			case ALL:
				switch (symmetry){
				case PERMUTABLE:
					result = chain.hasFamiliesOutOfDomain(familyDomain,closingRelationType);
					break;
				case INVERTIBLE: // formerly "EGOALTER"
					result = !targetDomain.contains(chain.getFirst()) || !targetDomain.contains(chain.get(1));
					break;
				case INVARIABLE: // formerly "EGO"
					result = !targetDomain.contains(chain.getFirst());
					break;
				}
				break;
			}
//		}
		return result;
	}

	/**
	 * checks whether a vertex is unadmissible according to his/her matrimonial
	 * or other status
	 * 
	 * @return true if marriage is a condition and the vertex is not married
	 */
	protected boolean incorrectProperties(final Individual individual) {
		boolean result;
		
		result = false;
//		if (individual.getGender().isUnknown()){
//			result = true; 
//		}
		if (couplesOnly && individual.isSingle(closingRelationType)) {
			result = true; 
		}
		// and other conditions for personal status...
		//
		return result;
	}

	/**
	 * checks whether a vertex is admissible according to cluster affiliation
	 * 
	 * @param ego
	 *            the vertex ID
	 * @param k
	 *            the position requirement (0 ego position, 1 alter position, 2
	 *            any position)
	 * @return true if cluster affiliation is required but the vertex does not
	 *         belong to the cluster
	 * @since 11-10-25
	 */
/*	public boolean cannotPass(final Individual ego, final Individual alter) {
		boolean result = false;

		if (restriction == RestrictionType.SOME) {
			result = !targetDomain.contains(ego) && !targetDomain.contains(alter);
		}

		//
		return result;

	}*/

	/**
	 * checks whether an individual is admissible according to cluster
	 * affiliation
	 * 
	 * @param indi
	 *            the individual
	 * @param k
	 *            the position requirement (0 ego position, 1 alter position, 2
	 *            any position)
	 * @return true if cluster affiliation is required but the vertex does not
	 *         belong to the cluster
	 */
/*	public boolean cannotPass(final Individual indi, final int k) {
		if (restriction == RestrictionType.NONE) {
			return false;
		}
		if (restriction == RestrictionType.ALL) {
			return (!individuals.contains(indi));
		}
		// if (k==0 && egoToAlter) return (!restrictedDomain.contains(indi));
		return false;
	}*/

	// check
	/**
	 * checks whether a chain violates the conditions of an open chain census
	 * 
	 * @param rings
	 *            the underlying RingGroupMap
	 * @return true if the chain violates the conditions of an open chain census
	 * @since 10/04/12, last mosified 11-10-25
	 */
/*	boolean cannotPass1(final Chain p) {
		if (p.length() == 0) {
			return true;
		}
		Individual ego = p.getFirst();
		Individual alter = p.getLast();
		
		if (symmetry != SymmetryType.INVARIABLE && alter.getId() < ego.getId()) {
			return true;
		}
		if (cannotPass(alter)) {
			return true;
		}
		if (cannotPass(ego, alter)) {
			return true;
		}
		return false;
	}*/

	/**
	 * gets all circuits with a given pivot chain (fills in the bridges)
	 * 
	 * @param chain
	 *            the pivot chain
	 */
	private void completeCircuits(final Chain chain) {
		
		int n = chain.size();
		
		if (n % 2 == 1) {
			return;
		}
		
		// for type PERMUTABLE this condition is already checked during the chain construction (find bases)
		if (symmetry==SymmetryType.INVERTIBLE && chain.getFirst().getId()>chain.getLast().getId()){
			return;
		}

		if (notInDomain(chain)) {
			return; // restriction condition for chains (if not already contained in the condition for intermediate pivots)
		}

		if (closingRelationType.equals("OPEN") && incorrectRelation(chain.getFirst(),chain.getLast())){
			return;
		}
		
		if (requiredAttribute != null && Trafo.isBlank(chain.getLast().getAttributeValue(requiredAttribute))){
			return;
		} 

		int degree = degrees[n/2-1];
		
		List<Chain>[] br = new List[n / 2];
		
		for (int i = 0; i < n / 2; i++) { // find Bridges for each couple
			int e;
			int a;
			
			if (closingRelationType.equals("OPEN")){
				e = 2*i;
				a = 2*i+1;
			} else {
				e = (n + 1 + 2 * i) % n;   // (n - 1 + 2 * i) % n;
				a = (n + 2 + 2 * i) % n;   // (n + 2 * i) % n; 
			}
			
/*			if (i==0 && symmetry!=SymmetryType.PERMUTABLE && incorrectRelation(chain.get(0),chain.get(1))) {
				return; 
			}*/
			
			br[i] = new ArrayList<Chain>(findBridges(chain.get(e), chain.get(a), degree));
			if (br[i].size() == 0) {
				// no closing relation (for i == (n/2)-1) or the depth of the linking consanginous chains exceed the maximal depth for chains of this order
				return;
			}
		}
		
		buildCircuits(new ArrayList<Chain>(), br);
		
	}
	
	/**
	 * counts and/or marks the pivots of the circuits in a RingGroupMap
	 * (individuals that form part of the circuits in pivotal position)
	 * 
	 * @param mark
	 *            true if the pivots shall be marked
	 * @see gui.screens.CensusScreen#report()
	 */
	@SuppressWarnings("unchecked")
/*	public void countMembers(final boolean mark) {
		Set<Individual>[] sets = new HashSet[dim()];
		List<Integer> pivotNumbers = new ArrayList<Integer>(getCircuits().size());
		membersByRelationId = new int[dim()][3];
		for (int i = 0; i < dim(); i++) {
			sets[i] = new HashSet<Individual>();
		}
		int k = 0;
		for (Cluster<Chain> c : circuits.getClusters()) {
			Set<Individual> set = new HashSet<Individual>();
			for (Chain r : c.getItems()) {
				for (int i = 0; i < 2 * r.dim(); i++) {
					Individual[] piv = r.getPivots(i);
					Individual v = piv[0];
					int j = piv[1].getId();
					set.add(v);
					sets[r.dim() - 1].add(v);
					if (mark) {
						v.setAttribute(j + "", "#" + r.signature(2).substring(2));
					}
				}
			}
			pivotNumbers.set(k, set.size());
			k++;
		}
		for (int i = 0; i < dim(); i++) {
			membersByRelationId[i][0] = sets[i].size();
			for (Individual v : sets[i]) {
				int j = v.getGender().toInt();
				if (j < 2) {
					membersByRelationId[i][j + 1]++;
				}
			}
		}
	}*/
	
	public void countCircuits(){
		nrCircuitsForKey = new HashMap<Value,Integer>();
		for (Value value : circuits.getValues()){
			int circuitClusterSize = circuits.getCluster(value).size();
			if (circuitClusterSize!=0){
				nrCircuitsForKey.put(value, circuitClusterSize);
			}
		}
	}
	
	
	public void count() {
		
		int n = dim()+1;
		nrCircuits = new int[n];
		nrCouples = new int[n];
		nrIndividuals = new int[n];
		nrCircuitTypes = new int[n];
		nrCircuitsForKey = new HashMap<Value,Integer>();
		nrCouplesForKey = new HashMap<Value,Integer>();
		nrPivotsForKey = new HashMap<Value,Integer>();
/*		List<Chain>[] couples = new List[n];
		Set<Individual>[] pivots = new Set[n];
		
		for (int i = 0; i < dim()+1; i++) {
			couples[i] = new ArrayList<Chain>();
			pivots[i] = new HashSet<Individual>();
		}*/
		
		for (Value value : circuits.getValues()){
			int circuitClusterSize = circuits.getCluster(value).size();
			if (circuitClusterSize!=0){
				int d = circuits.getCluster(value).getFirstItem().dim();
				int coupleClusterSize = couples.getCluster(value).size();
				int pivotClusterSize = pivots.getCluster(value).size();

				nrCircuitsForKey.put(value, circuitClusterSize);
				nrCouplesForKey.put(value, coupleClusterSize);
				nrPivotsForKey.put(value, pivotClusterSize);
				
				nrCircuits[0] += circuitClusterSize;
				nrCircuits[d] += circuitClusterSize;
				nrCircuitTypes[0]++;
				nrCircuitTypes[d]++;
			}
		}

/*		for (Cluster<Chain> cluster : circuits.getClusters()) {
			if (cluster.size()==0) continue;
			int d = cluster.getFirstItem().dim();
			nrCircuits[0] += cluster.size();
			nrCircuits[d] += cluster.size();
			nrCircuitTypes[0]++;
			nrCircuitTypes[d]++;*/
			
/*			for (Chain chain : cluster) {
				for (Chain subchain : chain.subchains){
					Individual pivot = subchain.getFirst();
					pivots[d].add(pivot);
					pivots[0].add(pivot);
				}
				for (Chain couple : chain.getCouples(Gender.MALE)) {
					if (!(ChainCluster.contains(couples[d], couple))) {
						couples[d].add(couple);
					}
					if (!(ChainCluster.contains(couples[0], couple))) {
						couples[0].add(couple);
					}
				}
			}*/


/*			for (Chain chain : cluster) {
				for (Chain couple : chain.getCouples(Gender.MALE)) {
					boolean notContained = true;
					for (Chain otherCouple : couples[d]){
						if (otherCouple.equals(couple)){
							notContained = false;
							break;
						}
					}
					if (notContained) {
						couples[d].add(couple); 
						couples[0].add(couple); 
					}
				}
			}
		}*/
		
		for (int i = 0; i < dim()+1; i++) {
			nrCouples[i] = couplesByOrder[i].size();
			nrIndividuals[i] = pivotsByOrder[i].size();
		}
	}


	/**
	 * creates the ascendant ties of a given ego vertex
	 * 
	 * @param ego
	 *            the ego vertex
	 * @param alter
	 *            the current vertex
	 * @param k
	 *            the current degree
	 * @param max
	 *            the maximal degree
	 */
	private void createAscendantTies(final Map<Individual, Map<Individual, Integer>> asc, final Individual ego, final Individual alter, final int k,
			final int max) {
		set(asc, ego, alter, k);
		set(asc, alter, ego, -k);
		if (k < max) {
			Individuals parents = getAscendingAlters(alter);
			if (parents == null) {
				return;
			}
			for (Individual newAlter : parents) {
				if (newAlter != null) {
					createAscendantTies(asc, ego, newAlter, k + 1, max);
				}
			}
		}
	}


	/**
	 * finds and counts all the chains of the required type in the underlying
	 * network
	 * 
	 * @param map
	 *            the underlying map (Net or RingGroupMap)
	 * @param k
	 *            the census type:
	 *            <p>
	 *            1 [byCouples - resorts the result of a census by couples] 2
	 *            [byAttributes - resorts the result of a census according to
	 *            chain attributes] 3 census of open kinship chains 4 census of
	 *            open kinship chains that are not be counted (for charts) 5
	 *            first cousin marriage census, 6 standardized census of
	 *            equivalence relation circuits, 7 census of matrimonial or
	 *            other circuits, 8 standardized census of open chains within a
	 *            given cluster, 9 census of cross-sex open chains corresponding
	 *            to a previous matrimonial census
	 */
	/*
	 * void census (RingGroupMap map, int t){ // initCount(); // if
	 * (schema!=null) countTypes(); if (relationType!=null) findCircuits(); else
	 * findOpenChains(); // else if (dim==1) countConsanguineChains(null); //
	 * else countAffinalChains(); // if (schema==null) renumber(); //
	 * filter(getFilter()); }
	 */
	
	/**
	 * Removes all chains that do or do not have a given attribute value
	 * 
	 * @param filter
	 *            a string expression of the filter criterion (attribute label +
	 *            value, preceded by a '+' or '-' sign according as the
	 *            attribute is or is not required
	 * @see RingGroupMap#census(Map, int)
	 */
/*	public void initializeFilter(final String filter) {
		
		char filterType = filter.charAt(0);
		String filterSchema = filter.substring(1, filter.indexOf(" "));
		String targetValue = filter.substring(filter.indexOf(" ") + 1);
		
		Iterator<Cluster<Chain>> it = circuits.getClusters().iterator();
			
			while (it.hasNext()) {
				Cluster<Chain> c = it.next();
				Object value = c.getFirstItem().getAttributeValue(filterSchema);
				if (filterType == '-' && !value.equals(targetValue)) {
					continue;
				} else if (filterType == '+' && value.equals(targetValue)) {
					continue;
				}
				if (filterType == '-') {
					for (Cluster<Chain> d : circuits.getClusters()) {
						reduce(d, c);
					}
				}
				it.remove();
			}
	}*/
	
	private boolean potentialAlterPosition(int pos){
		boolean result;
		
		//
		if (symmetry == SymmetryType.PERMUTABLE) {
			result = true;
		} else if (symmetry == SymmetryType.INVERTIBLE){
			if (closingRelationType.equals("OPEN")){
				result = (pos == 2*dim()-1);
			} else {
				result = (pos == 1);
			}
		} else {
			result = false;
		}
		//
		return result;
	}

	/**
	 * checks whether the addition of a new element to the chain violate the
	 * order condition for pivot chains of matrimonial circuits
	 * <p>
	 * 1. the element must not precede ego
	 * <p>
	 * 2. if the element is identical to ego then the last element (ego's second
	 * spouse in the chain) must not precede alter
	 * <p>
	 * 3. the element must not be identical to any element other than its
	 * immediate predecessor or ego
	 */
	private void findBases(final Individual ego, final Chain chain, final Set<Individual> vertices, final int rmax, final int egoPos) {

		completeCircuits(chain);
		if (egoPos == rmax) {
			return;
		}
		vertices.add(ego);
		Individuals alters = getAlters(ego, egoPos);
		
		//verify conditions for case of invariable relations!
		for (Individual alter : alters) {
			
			if (egoPos==0 &&  !closingRelationType.equals("OPEN") &&  !closingRelationType.equals("TOTAL") && (alter.getId()>ego.getId() || symmetry == SymmetryType.INVARIABLE) && !notInDomain(ego,alter) && !notInFamilyDomain(ego,alter)){
				couplesConsidered++;
				if (consideredCouples!=null){
					consideredCouples.add(Chain.getCouple(ego, alter, crossSex, symmetry, firstGender));
				}
			}

			// marriage coinciding with non-marriage closing relation
/*			if (relationType.equals("SPOUSE") && egoPos==0 && checkForMarriage(ego,alter)){
				continue;
			}*/
									
			// all pivots permutable with ego fulfill the eligibility conditions
			if (potentialAlterPosition(egoPos+1) && incorrectProperties(alter)) {
				continue; 
			}
			
			// all pivots have to be in the restricted domain if demanded
			if (restriction == RestrictionType.ALL  && !targetDomain.contains(alter)){
				if (symmetry == SymmetryType.PERMUTABLE){
					continue;
				} else if (symmetry == SymmetryType.INVERTIBLE && egoPos == 0){
					continue;
				}
			}
						
			// all marriages have to be in the restricted domain if demanded
			if (restriction == RestrictionType.ALL && isCouplePosition(egoPos) && (closingRelationType.equals("SPOUSE") || closingRelationType.equals("PARTN")) && familyDomain.getBySpouses(ego, alter)==null){
					continue;
			}

			// all pivots have to be in the restricted domain if demanded
/*			if (restriction == RestrictionType.EGOALTER && egoPos == 0 && !targetDomain.contains(alter)){
				continue; 
			}*/
			
			// no pivot permutable with ego must precede ego (avoidance of duplicates)
			if (alter.getId() < chain.getFirst().getId()){
				if (symmetry == SymmetryType.PERMUTABLE && potentialAlterPosition(egoPos+1)) {
					continue; 
/*				} else if (symmetry == SymmetryType.INVERTIBLE && egoPos==0 && (alter.getId() < chain.getFirst().getId())) {
					continue; */
				}
			}
			
			// if polygamous ego, 2nd vertex must be lower (avoidance of duplicates)
			if (symmetry == SymmetryType.PERMUTABLE && alter.equals(chain.getFirst()) && ego.getId() <= chain.getId(1)) {
				continue; 
			}
			
			// no element must appear twice (but artificial repetition permitted)
			if (vertices.contains(alter) && alter != ego && alter != chain.getFirst()) {
				continue; 
			}
			
			if (!chain.add(alter, 0)) {
				continue;
			}
			
			findBases(alter, chain, vertices, rmax, egoPos + 1);
			chain.removeLast();
		}
		vertices.remove(ego);
	}

	private Set<Chain> findBridges(final Individual ego, final Individual alter, final int r) {
		Set<Chain> result;
		
		result = new HashSet<Chain>();
		findBridges(ego, alter, result, new Chain(ego), new Chain(alter), new HashSet<Individual>(), r, r);
		
		return result;
	}

	private void findBridges(final Individual ego, final Individual alter, final Set<Chain> bridges, final Chain leftChain, final Chain rightChain,
			final Set<Individual> vertices, final int rmax, final int r) {
		findLeftBridges(ego, alter, bridges, leftChain, rightChain, vertices, rmax);
		if (r == 0) {
			return;
		}
		if (matches(ego,alter)) {
			return; 
		}
		
		Individuals parents = getAscendingAlters(alter);
//		Individuals parents = alter.getParents();
		if (parents == null) {
			return;
		}
		vertices.add(alter);
		for (Individual newAlter : parents) {
			if (vertices.contains(newAlter)) {
				continue;
			}
			if (!rightChain.add(newAlter, 1)) {
				continue;
			}
			findBridges(ego, newAlter, bridges, leftChain, rightChain, vertices, rmax, r - 1);
			rightChain.removeLast();
		}
		vertices.remove(alter);
	}
	
	/**
	 * searches and counts the open kinship chains in a net
	 * 
	 * @since last modified 10/04/12
	 */
/*	private void findOpenChains() {
		for (Individual ego : individuals) {
			if (!cannotPass(ego) && !cannotPass(ego, 0)) {
				List<Chain> list = new ArrayList<Chain>();
				searchChains(new Chain(ego), list, new ArrayList<Individual>());
				for (Chain r : list) {
//					r.setPivots();
					put(r.standard());
				}
			}
		}
	}*/
	


	public void findCircuits() {
//		begin();
		couplesConsidered = 0;
		
		// calculate consanguine map
		if (closingRelationType.equals("OPEN")) {
			getConsanguines(maxDeg[0]);
		} else if (dim() > 1) {
			getConsanguines(maxDeg[1]);
		}
		
		for (Individual ego : getSearchDomain().toSortedList()) {
			
			// filter for starting point status
			if (incorrectProperties(ego)) {
				continue;
			}
			
			if (requiredAttribute != null && Trafo.isBlank(ego.getAttributeValue(requiredAttribute))){
				continue;
			} 


			// linear chains search
			if (closingRelationType.equals("LINEAR")) {
				findLinearChains(new Chain(ego));
			}
			
			// open chains search
			else if (closingRelationType.equals("OPEN")) {
				
				findBases(ego, new Chain(ego), new HashSet<Individual>(), 2 * dim() - 1, 0);

				
/*				List<Chain> list = new ArrayList<Chain>();
				searchChains(new Chain(ego), list, new ArrayList<Individual>());
				for (Chain r : list) {
					put(r.standard());
				}*/
				
			}

				// if only Consanguine Circuits
			else if (dim() == 1) {
				for (Individual alter : getAlters(ego,0)) {
					
					// required pivots (according to restriction condition) are in domain (check for ego in closed cases earlier)
					if (notInDomain(ego,alter)){
						continue;
					}
					
					// all marriages have to be in the restricted domain if demanded
					if (notInFamilyDomain(ego, alter)){
						continue;
					}
					
					// marriage coinciding with non-marriage closing relation
/*					if (checkForMarriage(ego,alter)){
						continue;
					}*/
								
					// alter fulfills the eligibility conditions
					if (incorrectProperties(alter)) {
						continue; 
					}

					// both pivots have to be in the restricted domain if demanded
/*					if ((restriction == RestrictionType.ALL || restriction == RestrictionType.EGOALTER) && !targetDomain.contains(alter)){
						continue; 
					}*/
					
					if (alter.getId()> ego.getId() || symmetry == SymmetryType.INVARIABLE){
					
						couplesConsidered++;
						if (consideredCouples!=null){
							consideredCouples.add(Chain.getCouple(ego, alter, crossSex, symmetry, firstGender));
						}
						
						Set<Chain> bridges = findBridges(ego, alter, degrees[0]); 
						for (Chain c : bridges) {
							// No link redoubling for relational census with identical closing and ascending relation
							if (closingRelationType==ascendingRelationType && closingRelationEgoRole == ascendingRelationEgoRole  && closingRelationAlterRole == ascendingRelationAlterRole && c.length()==1){
								continue;
							}
							put(c);
						}
						
					}
					
				}
				// if including Relinking Circuits
			} else {
				findBases(ego, new Chain(ego), new HashSet<Individual>(), 2 * dim() - 1, 0);
			}
		}
		
		if (closingRelationType.equals("TOTAL")){
			couplesConsidered = targetDomain.size()*(targetDomain.size()-1);
			if (symmetry==SymmetryType.INVERTIBLE){
				couplesConsidered = couplesConsidered/2;
			}
		}

//		circuits.getClusters().sort();
//		end();
	}
	
/*	private Individuals getParents(Individual ego){
		Individuals result;
		
		if (line==FiliationType.COGNATIC){
			result = ego.getParents();
		} else {
			result = ego.getParents(line);
		}
		//
		return result;
	}*/

	//Simplifie (ego,alter == leftChain.getLast(), rightChain.getLast())
	private void findLeftBridges(final Individual ego, final Individual alter, final Set<Chain> bridges, final Chain leftChain, final Chain rightChain,
			final Set<Individual> vertices, final int r) {

		if (matches(ego, alter)) {
			
			if (mergingPartition!=null){
				
				if (rightChain.size()<2 || !matches(ego,rightChain.get(rightChain.size()-2))){
					
					Cluster<Individual> cluster = mergingPartition.getCluster(ego);
					Chain leftChain1 = leftChain.clone();
					Chain rightChain1 = rightChain.clone();
					
					if (cluster!=null){
						
						Individual clusterAsAncestor = new Couple(lastId+mergingPartition.indexOf(cluster)+1,cluster.getLabel());
						leftChain1.add(clusterAsAncestor,1);
						rightChain1.add(clusterAsAncestor,1);
						
					} else {
						
						if (!ego.equals(alter)) System.err.println("Error in brige construction: "+ego+" vs "+alter);
					
					}
					bridges.add(ChainMaker.concatenateInv(leftChain1, rightChain1));
				}
			} else {
				bridges.add(ChainMaker.concatenateInv(leftChain, rightChain));
			}
			return;
		}
		if (r == 0) {
			return;
		}
		Individuals parents = getAscendingAlters(ego);
//		Individuals parents = ego.getParents();
		if (parents == null) {
			return;
		}
		vertices.add(ego);
		for (Individual newEgo : parents) {
			if (vertices.contains(newEgo)) {
				continue;
			}
			if (!leftChain.add(newEgo, 1)) {
				continue;
			}
			findLeftBridges(newEgo, alter, bridges, leftChain, rightChain, vertices, r - 1);
			leftChain.removeLast();
		}
		vertices.remove(ego);
	}



	/**
	 * gets the group of rings of a given type
	 * 
	 * @param str
	 *            the signature of the ring type
	 */
	public Cluster<Chain> get(final String str) {
		return getCircuits().getCluster(ChainMaker.fromString(str));
	}
	
	private String getRelationNames (final Individual ego, final Individual alter){
		String result;
		
		result = "";
		for (Relation relation : ego.relations().getByModelName(closingRelationType)){
			if (relation!=null){
				if (StringUtils.isBlank(closingRelationEgoRole) || relation.hasRole(ego,closingRelationEgoRole)){
					if (StringUtils.isBlank(closingRelationAlterRole) || relation.hasRole(alter,closingRelationAlterRole)){
						if (result.length()>0){
							result+=",";
						}
						result+=relation.getName();
					}
				}
			}
		}
		//
		return result;

	}
	
	private boolean isCouplePosition (final int pos){
		boolean result;
		
		if (closingRelationType.equals("OPEN")){
			result = (pos % 2 != 0);
		} else if (closingRelationType.equals("SPOUSE") || closingRelationType.equals("PARTN") || !ascendingRelationType.equals("PARENT")){
			result = (pos % 2 == 0);
		} else {
			result = ((pos > 0) && (pos % 2 == 0));
		}
		//
		return result;
	}
	

	private Individuals getAlters(final Individual ego, final int pos) {
		Individuals result;
		
		if (closingRelationType.equals("OPEN")){
			// unpair (matrimonial) relations
			if (pos % 2 != 0) {
				if (closingRelationType.equals("PARTN")){
					result =  ego.getPartners();
				} else {
					result =  ego.spouses();
				}
			// pair (consanguineal) relations
			} else {
				result =  links(ego, maxDeg[pos / 2]);
			}
		}
		
		// closing relation 
		else if (pos == 0){
			// spouses
			if (closingRelationType.equals("SPOUSE")){
				result =  ego.spouses();
			// partners
			} else if (closingRelationType.equals("PARTN")) {
				result =  ego.getPartners();
			// coSpouses
			} else if (closingRelationType.equals("COSPOUSE")) {
				result =  ego.coSpouses();
			// co-cluster-membersByRelationId	
			} else if (closingRelationType.charAt(0) == '@') {
				result = new Individuals();
				Cluster<Individual> linkingCluster = linkingPartition.getCluster(ego);
				if (linkingCluster.getLabel()!=null) {
					for (Individual alter : linkingCluster.getItems()){
						if (!incorrectRelation(ego,alter)) { //alter!=ego
							result.add(alter);
						}
					}
				}
			} else if (closingRelationType.equals("TOTAL")) {
				result = new Individuals();
				for (Individual alter : targetDomain){
					if (!incorrectRelation(ego,alter)) { //alter!=ego
						if (symmetry == SymmetryType.INVARIABLE || alter.getId() > ego.getId()){
							result.add(alter);	
						}
					}
				}
				// other relations	
			} else {
				result = getClosingAlters(ego);
			}
		// pair (matrimonial) relations 	
		} else if (pos % 2 == 0) {
			if (closingRelationType.equals("PARTN")){
				result =  ego.getPartners();
			} else if (!ascendingRelationType.equals("PARENT") && !closingRelationType.equals("SPOUSE")){
				result = getClosingAlters(ego);
			} else {
				result =  ego.spouses();
			}
		// unpair (consanguineal) relations
		} else {
			result =  links(ego, maxDeg[(pos + 1) / 2]);
		}
		//
		
		return result;
	}
	
	private Individuals getClosingAlters(Individual ego){
		Individuals result;
		
		result = new Individuals();
		
		for (Relation relation : ego.relations().getByModelName(closingRelationType)){
			
			if (relation!=null && relationDomain.contains(relation)){
				
				// Check for birth events
				if (closingRelationType.equalsIgnoreCase("MIGEVENT") && RelationValuator.isBirth(relation)){
					continue;
				}
				
				// Check for dated or valued relations
				if (criteria.getRelationTime()!=null){
					Value timeValue = RelationValuator.get(relation, criteria.getDateLabel(), geography);
					if (timeValue==null || timeValue.intValue()!=criteria.getRelationTime()){
						continue;
					}
				}
				if (criteria.getRelationAttributeLabel()!=null && relation.getAttributeValue(criteria.getRelationAttributeLabel())==null){
					continue;
				}
				
				if (StringUtils.isBlank(closingRelationEgoRole) || relation.hasRole(ego,closingRelationEgoRole)){
					if (closingRelationAlterRole.equals("REFERENT")){
						for (Individual alter: relation.getReferents(ego)){
							if (!incorrectRelation(ego,alter)) { //alter!=ego
								result.add(alter);
							}
						}
					} else {
						for (Actor actor : relation.actors()){
							Individual alter = actor.getIndividual();
							if (StringUtils.isBlank(closingRelationAlterRole) || actor.getRole().getName().equals(closingRelationAlterRole)){
								if (!incorrectRelation(ego,alter)) { //alter!=ego
									result.add(alter);
								}
							}
						}
					}
				}
			}
		}

		//
		return result;
	}
	
	private Individuals getAscendingAlters(Individual ego){
		Individuals result;

		result = new Individuals();

		if (ascendingRelationType.equals("PARENT")){
			if (ego.getOriginFamily()!=null){
				if (line==FiliationType.COGNATIC  || line.hasLinkingGender(Gender.MALE)){
					Individual father = ego.getOriginFamily().getHusband();
					if (father==null){
						father = ego.getOriginFamily().getMissingHusband(lastId);
					}
					result.add(father);
				}
				if (line==FiliationType.COGNATIC  || line.hasLinkingGender(Gender.FEMALE)){
					Individual mother = ego.getOriginFamily().getWife();
					if (mother==null){
						mother = ego.getOriginFamily().getMissingWife(lastId);
					}
					result.add(mother);
				}
			}
/*			if (line==FiliationType.COGNATIC){
				result = ego.getParents();
			} else {
				result = ego.getParents(line);
			}*/
		} else {
			for (Relation relation : ego.relations().getByModelName(ascendingRelationType)){
				if (relation!=null){
					if (StringUtils.isBlank(ascendingRelationEgoRole) || relation.hasRole(ego,ascendingRelationEgoRole)){
						for (Actor actor : relation.actors()){
							Individual alter = actor.getIndividual();
							if (StringUtils.isBlank(ascendingRelationAlterRole) || actor.getRole().getName().equals(ascendingRelationAlterRole)){
								if (ego!=alter){
									result.add(alter);
								}
							}
						}
					}
				}
			}
		}
		
		//
		return result;
	}
	

	/**
	 * creates a second order Net with ChainGroups as vertices, and writes the
	 * circuit intersection closedNet
	 * 
	 * @return a second order Net with ChainGroups as vertices
	 */
/*	public ClusterNetwork<Chain> getCircuitIntersectionNetwork() {
		ClusterNetwork<Chain> result = new ClusterNetwork<Chain>(getCircuits());
		for (Cluster<Chain> cp : byCouples(Gender.MALE).getClusters()) {
			Set<int[]> vectors = new HashSet<int[]>();
			for (Chain r : cp.getItems()) {
				vectors.add(r.getCharacteristicVector());
			}
			List<int[]> vectorList = new ArrayList<int[]>(vectors);
			for (int i = 0; i < vectorList.size(); i++) {
				Cluster<Chain> cluster1 = getCircuits().getCluster(new Value(vectorList.get(i)));
				for (int j = 0; j < i; j++) {
					Cluster<Chain> cluster2 = getCircuits().getCluster(new Value(vectorList.get(j)));
					result.addLink(cluster1, cluster2);
				}
			}
		}
		return result;
	}*/

	/**
	 * gets a string representation of the degree array
	 * 
	 * @return a string representation of the degree array
	 */
/*	public String getDeg() {
		return Arrays.toString(degrees);
	}*/

	/**
	 * gets the maximum degree of chains of ith order
	 * 
	 * @param i
	 *            the order of chains
	 */
/*	public int getDeg(final int i) {
		return degrees[i];
	}*/

	/**
	 * gets the frequency of a relation or circuit with given vector
	 * 
	 * @param vector
	 *            the relation vector
	 * @return the frequency
	 */
/*	public int getFrequency(final Vector vector) {
		return circuits.getValueFrequency(new Value(vector));
	}*/

	/**
	 * gets the unilinear code for ascendant lines (0 agnatic, 1 uterine, -1
	 * cognatic)
	 * 
	 * @return the unilinear code for ascendant lines (0 agnatic, 1 uterine, -1
	 *         cognatic)
	 * @see io.write.TxtWriter#writeCensusHead(RingGroupMap)
	 */
/*	public FiliationType getLine() {
		return line;
	}*/

	/**
	 * gets the maximum degree of chains of ith and higher order
	 * 
	 * @param i
	 *            the order of chains
	 */
/*	public int getMaxDeg(final int i) {
		return maxDeg[i];
	}*/



	/**
	 * gets the number of pivotal vertices of given type and chain order
	 * 
	 * @param i
	 *            the order of the chains
	 * @param j
	 *            the type of individuals (0 all, 1 male, 2 female)
	 * @return the number of pivotal vertices
	 * @see io.write.TxtWriter#writeSurvey(RingGroupMap,CountMap,int)
	 */
/*	public int getMembers(final int i, final int j) {
		return membersByRelationId[i][j];
	}*/

	/**
	 * gets the ID number set of consanguineous relatives of given maximal
	 * degree
	 * 
	 * @param ego
	 *            the ID of ego
	 * @param d
	 *            the maximal degree
	 * @see chains.NumberChain#links(Map,int)
	 */
	public Individuals links(final Individual ego, final int d) {
		Individuals result = new Individuals();
		Map<Individual, Integer> cons = consanguines.get(ego);
		for (Individual alter : cons.keySet()) {
			if (cons.get(alter) <= d) {
				result.put(alter);
			}
		}
		return result;
	}

	private boolean matches(final Individual ego, final Individual alter) {
		boolean result = false;
		
		if (ego.equals(alter)){
			result = true;
		} else if (mergingPartition != null) {
			Object egoGroup = mergingPartition.getValue(ego);
			Object alterGroup = mergingPartition.getValue(alter);
			if (egoGroup!=null && alterGroup!=null && egoGroup.equals(alterGroup)){
				result = true;
			}
		}
		//
		return result;
	}

	/**
	 * checks whether also nonlinear relations shall be searched (restrictions
	 * to linear relations are only possible via the kinship schema in the right
	 * hand field of the census screen)
	 * 
	 * @return true if there is no restriction to linear relations
	 */
/*	private boolean nonlin() {
		if (models == null) {
			return true;
		}
		for (Chain r : models) {
			if (!r.linear()) {
				return true;
			}
		}
		return false;
	}*/
	
	void changeCircuitLabelsToClassic (){
		if (classification == "SIMPLE"){
			for (Cluster<Chain> cluster : circuits.getClusters()){
				Chain chain = cluster.getFirstItem();
				if (crossSex){
					cluster.setValue(new Value(chain.signature(Notation.CLASSIC)));
				} else {
					cluster.setValue(new Value(chain.signature(Notation.CLASSIC_GENDERED)));
				}
			}
		}
	}
	
	/**
	 * checks whether a given 2-field-integer array is contained in a list of
	 * such arrays
	 * 
	 * @param list
	 *            the list of arrays
	 * @param a
	 *            the array to be checked
	 * @return true if the array a is contained in the list
	 */
	private static boolean contains(final List<Chain> list, final Chain a) {
		for (Chain b : list) {
			if (a.equals(b)) {
				return true;
			}
		}
		return false;
	}
	 	

	protected void put(final Chain c) {
		
		if (c == null) {
			return; // can happen in case of intersecting bridges
		}
		
		c.setSymmetry(symmetry);
		Chain cs;
		
		if (closingRelationType.equals("LINEAR")){
			cs = c;
		} else if (symmetry == SymmetryType.INVARIABLE && dim()>1){
			cs = c.neutralize(sib,lastId).transform(2*c.dim()-1);
		} else {
			cs = c.neutralize(sib,lastId).standard();
		}
		
		Value k = ChainValuator.get(cs,classification);
		
		int dim = c.dim();
		
		// checks whether neutralized chain contains double elements (possible if the same apical couple has four children in the chain)
		if (sib == SiblingMode.FULL && cs.containsDoubleElements()){
			return;
		}

		// checks whether the chain passes the chain filter
		if (isForbidden(cs)){
			return;
		}
		
		// checks whether the chain has the required linetype
		if (line!=FiliationType.COGNATIC && !ChainValuator.hasLineValue(cs, "LINE",new Value(line))){
			return;
		}

		// checks whether the chain has the required ringtype 
		if (violatesRingType(cs)) {
			return;
		}

		// checks whether the chain has the required form (only in case of search by schema)
//		if (schema != null && !circuits.containsValue(k)) {
		if (schema != null && !hasCorrectForm(cs)){
			return;
		}

		// checks whether a chain has already be counted in another perspective.
		// This check is only necessary in the case where a pivot is anchored in
		// the cluster and the other pivots may be out of the cluster
		if (restriction == RestrictionType.LASTMARRIED) {
			
			if (circuits.getCluster(k)!=null && circuits.getCluster(k).getItems().contains(cs)) {
				return;
			}
		}

		if (linkingPartition != null){
			cs.closingRelation = linkingPartition.getCluster(cs.getFirst()).getLabel();
		} else if (!closingRelationType.equals("SPOUSE") && !closingRelationType.equals("PARTN") && !closingRelationType.equals("OPEN") && !closingRelationType.equals("COSPOUSE")){
			cs.closingRelation = getRelationNames(cs.getFirst(),cs.getLast());
		}
		
		//store circuits
		circuits.put(cs,k);
		
		//store marriages
		if (couples!=null){
			for (Chain couple : cs.getCouples(crossSex, symmetry, firstGender)) {
				if (!notInDomain(couple)){
					couples.put(couple,k);
					if (!(contains(couplesByOrder[dim], couple))) {
						couplesByOrder[dim].add(couple);
					}
					if (!(contains(couplesByOrder[0], couple))) {
						couplesByOrder[0].add(couple);
					}
				}
			}
		}
		
		//store individuals
		if (pivots != null){
			
			for (Chain subchain : cs.subchains){
				Individual pivot = subchain.getFirst();
				
				if (targetDomain.contains(pivot)){
					pivots.put(pivot,k);
					pivotsByOrder[dim].add(pivot);
					pivotsByOrder[0].add(pivot);
					
					if (markIndividuals){
						List<Value> values = pivotMap.get(pivot);
						if (values == null){
							values = new ArrayList<Value>();
							pivotMap.put(pivot,values);
						}
						if (!values.contains(k)){
							values.add(k);
						}
					}
				}
			}

			// mark individuals
			if (markIndividuals) {
				String chainLabel = "CENSUS_"+getPattern().replaceAll(" ", "_");
				for (Individual pivot : pivotMap.keySet()){
					String chainValue = pivotMap.get(pivot).toString();
					pivot.setAttribute(chainLabel,chainValue);
				}
			}
		}
		


	}
	
	private String clusterSignature(Cluster<Chain> cluster){
		String result;
		
		if (classification.equals("SIMPLE")){
			Chain chain = cluster.getFirstItem();
			if (crossSex){
				result = chain.signature(Notation.CLASSIC);
			} else {
				result = chain.signature(Notation.CLASSIC_GENDERED);
			}
			result = result + "\t" + chain.signature(Notation.POSITIONAL) + "\t" + chain.signature(Notation.VECTOR);
		} else {
			result = cluster.getValue().toString();
		}
		return result;
	}
	
	private String groupSignature (Chain chain){
		String result;
		
		if (mergingPartition==null){
			result = "";
		} else {
			result = chain.signature(Notation.GROUPS)+"\t";
		}
		//
		return result;
	}
	
	
	
	private Double[] getClosureRate(Partition<Chain> openChains, Cluster<Chain> circuit){
		Double[] result;
		
		if (openChains == null){
			result = new Double[]{0.,0.,0.};
		} else {
			result = new Double[3];
					
			int circuitFrequency = circuit.count();
			int openChainFrequency = 0;

			if (!circuit.getValue().isVector()){
				openChainFrequency = openChains.getCluster(circuit.getValue()).count();
			} else {
				Vector vector = circuit.getValue().vectorValue();
				if (vector.order()==1){
					openChainFrequency = openChains.getCluster(circuit.getValue()).count();
				} else {
					for (int i=0;i<2*vector.order();i++){
						Cluster<Chain> openCluster = openChains.getCluster(new Value(vector.transform(i)));
						if (openCluster != null){
							openChainFrequency += openCluster.count();
						}
					}
				}
			}
			result[0] = MathUtils.percent(circuitFrequency,couplesConsidered);
			result[1] = MathUtils.percent(2*openChainFrequency,targetDomain.size());
//			result[2] = MathUtils.percent(result[0], result[1]);
			result[2] = MathUtils.percent(circuitFrequency, openChainFrequency);
		}
		//
		return result;
	}
	
	public Double[] getClosureRatePercentages(Value key){
		Double[] result = new Double[3];
		
		Double[] closureRates = getClosureRate(key);
		
		result[0] = MathUtils.percent(closureRates[0],couplesConsidered);
		result[1] = MathUtils.percent(2*closureRates[1],100*targetDomain.size());
		result[2] = closureRates[2];
		//
		return result;
		
	}
	
	public Double[] getClosureRate(Value key){
		Double[] result;
		
		if (!openChainFrequencies){
			
			result = null;
		
		} else {

			int order = ChainMaker.getOrder(key);
			int nrPerspectives = 1;

			result = new Double[3];
			int circuitFrequency = getNrCircuitsForKey(key);
			int openChainFrequency = 0;

			if (!key.isVector()){
				openChainFrequency = getNrOpenChainsForKey(key);
			} else {
				Vector vector = key.vectorValue();
				
				if (order==1){
					openChainFrequency = getNrOpenChainsForKey(key);
				} else {
					Set<Vector> checked = new HashSet<Vector>();
					for (int i=0;i<2*order;i++){
						Vector perspective = vector.transform(i);
						if (!checked.contains(perspective)){
							openChainFrequency += getNrOpenChainsForKey(new Value(perspective));
							checked.add(perspective);
						}
					}
					nrPerspectives = checked.size()/2;
				}
			}
			result[0] = new Double(circuitFrequency);
			result[1] = new Double(openChainFrequency);
			result[2] = MathUtils.percent(result[0], result[1]/new Double(nrPerspectives));
		}
		
		//
		return result;
	}
	
	private String getOpenChainSequence(Value key){
		String result;
		
		Double[] closure = getClosureRate(key);
		if (closure == null){
			result = "";
		} else {
			result = String.format ("%d \t%.2f%%", closure[1].intValue(), closure[2])+"\t";
		}

		//
		return result;
	}
	

	
	private static String getOpenChainSequence(Partition<Chain> openChains, Cluster<Chain> circuit){
		String result;
		
		if (openChains == null){
			result = "";
		} else {
			int circuitFrequency = circuit.count();
			int openChainFrequency = 0;

			if (!circuit.getValue().isVector()){
				openChainFrequency = openChains.getCluster(circuit.getValue()).count();
			} else {
				Vector vector = circuit.getValue().vectorValue();
				if (vector.order()==1){
					openChainFrequency = openChains.getCluster(circuit.getValue()).count();
				} else {
					for (int i=0;i<2*vector.order();i++){
						Cluster<Chain> openCluster = openChains.getCluster(new Value(vector.transform(i)));
						if (openCluster != null){
							openChainFrequency += openCluster.count();
						}
					}
				}
			}
			result = String.format ("%d \t%.2f%%", openChainFrequency, MathUtils.percent(circuitFrequency, openChainFrequency))+"\t";
		}
		//
		return result;
	}
	
	public String listCircuitFrequencies(){
		String result = "";
		for (Cluster<Chain> circuitCluster : circuits.getClusters().toListSortedByValue()) {
			Value key = circuitCluster.getValue();
			result += "\t"+getNrCircuitsForKey(key);
		}
		//
		return result;
	}
	
	public int getNrOpenChainsForKey(Value key) {
		int result = 0;
		if (nrOpenChainsForKey.get(key)!=null){
				result = nrOpenChainsForKey.get(key);
			}
		return result;
	}
	
	public int getNrCircuits (int dimension){
		return nrCircuits[dimension];
	}
	
	public int getNrCircuitsForKey (Value key){
		int result = 0;
		if (nrCircuitsForKey.get(key)!=null){
			result = nrCircuitsForKey.get(key);
		}
		return result;
	}
	
	public int getNrCouplesForKey (Value key){
		int result = 0;
		if (nrCouplesForKey.get(key)!=null){
			result = nrCouplesForKey.get(key);
		}
		return result;
	}
	
	public int getNrPivotsForKey (Value key){
		int result = 0;
		if (nrPivotsForKey.get(key)!=null){
			result = nrPivotsForKey.get(key);
		}
		return result;
	}
	
	public Map<Value,Double[]> getClosureRates() throws PuckException{
		Map<Value,Double[]> result;
		
		result = new HashMap<Value,Double[]>(); 
		
//		Partition<Chain> openChains = getOpenChains();
		
		for (Cluster<Chain> circuitCluster : circuits.getClusters().toListSortedByValue()) {
			Value key = circuitCluster.getValue();
//			result.put(circuitCluster.getValue(),getClosureRate(openChains,circuitCluster));			
			result.put(key,getClosureRatePercentages(key));			
		}
		//
		return result;
	}
	
	public Map<Value,Double> getCircuitFrequencies() throws PuckException{
		Map<Value,Double> result;
		
		result = new HashMap<Value,Double>(); 
		
		for (Cluster<Chain> circuitCluster : circuits.getClusters().toListSortedByValue()) {
			result.put(circuitCluster.getValue(),new Double(circuitCluster.count()));			
		}
		//
		return result;
	}
	
	public static Map<Value,Double[]> getClosureRates(Segmentation segmentation, String chainClassification, FiliationType line, int maxGen) throws PuckException{
		Map<Value,Double[]> result;
		
		CensusCriteria criteria = new CensusCriteria();
		criteria.setChainClassification(chainClassification);
//		criteria.setPattern(maxGen+"");
		criteria.setPattern("XX(X)XX");
		criteria.setFiliationType(line);
		criteria.setOpenChainFrequencies(true);
		criteria.setSiblingMode(SiblingMode.FULL);
		criteria.setRestrictionType(RestrictionType.ALL);
		
//		Individuals segment = IndividualValuator.extract(individuals, "PEDG", "2", new Value(4));
		
		CircuitFinder finder = new CircuitFinder(segmentation,criteria);
		
		finder.findCircuits();
		finder.getOpenChains();
		finder.countCircuits();
		
		
//		result = finder.getCircuitFrequencies();
		result = finder.getClosureRates();
		
		//
		return result;
	}
	
	public SymmetryType getSymmetry() {
		return symmetry;
	}


	
	/**
	 * 
	 * @return
	 * @throws PuckException 
	 */
	public StringList reportSurvey() 
	{
		StringList result;
		
		result = new StringList();
		
		// Count open chains
/*		Partition<Chain> openChains = null;
		if (openChainFrequencies) {
			try {
				openChains = getOpenChains();
			} catch (PuckException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}*/

		StringList labels = new StringList();

		// No chain properties are listed if the census is aggregate.
		if (CensusReporter.isBasicClassification(classification)) {
			labels.addAll(this.censusDetails.getReportLabels());
		}
		
		//
		String signatureSequence = "";
		if (classification.equals("SIMPLE")){
			signatureSequence = t("Standard")+"\t"+t("Positional")+"\t"+t("Vector");
		} else {
			signatureSequence = classification;
		}
		
		//
		String openChainSequence = "";
		if (openChainFrequencies){
			openChainSequence = t("#Open chains")+"\t"+t("Closure Rate")+"\t";
		}
		
		String totalCouples = "";
		String totalIndividuals = "";
		
		if (!closingRelationType.equals("OPEN") && !closingRelationType.equals("LINEAR")){
			totalCouples = "("+MathUtils.percent(nrCouples[0], couplesConsidered)+" % of "+couplesConsidered+" couples examined)";
			totalIndividuals = "("+MathUtils.percent(nrIndividuals[0], targetDomain.size())+" % of "+targetDomain.size()+" individuals examined)";
		}
		
		// General Survey
		result.appendln();
		result.appendln(nrCircuits[0] + " " + t(ringType.toString()+"s") + " ("+t("maximal depths = ") + Arrays.toString(degrees) + ")" );
		result.appendln(nrCircuitTypes[0] + " " + t("circuit types") + " (" +t("average frequency = ") + MathUtils.round(new Double(nrCircuits[0])/new Double(nrCircuitTypes[0]),2) + ")");
		result.appendln(nrCouples[0] + " " + t("couples concerned") + " "+totalCouples);
		result.appendln(nrIndividuals[0] + " " + t("individuals concerned") + " "+totalIndividuals);
		result.appendln();

		// Detailed Survey
		int d = 0;
		int id = 0;
		for (Cluster<Chain> circuitCluster : circuits.getClusters().toListSortedByValue()) {
			Value key = circuitCluster.getValue();
			int nrTheseCircuits = getNrCircuitsForKey(key);
			int nrTheseCouples = getNrCouplesForKey(key);
			int nrThesePivots = getNrPivotsForKey(key);

			if (nrTheseCircuits==0) continue;
			id++;
			
			
//			Cluster<Chain> coupleCluster = couples.getCluster(key);
//			Cluster<Individual> pivotCluster = pivots.getCluster(key);
			
			int valueDim = key.dimension();
			
			if (valueDim > d) {
				d = valueDim;

				result.appendln();
				result.appendln(nrCircuits[d] + " " + t(ringType.toString()+"s of order") + " " + d + " ("+t("maximal depth = ") + degrees[d-1] + ")" + " ("+MathUtils.percent(nrCircuits[d], nrCircuits[0])+" %)");
				result.appendln(nrCircuitTypes[d] + " " + t("circuit types") + " ("+MathUtils.percent(nrCircuitTypes[d], nrCircuitTypes[0])+" %)"+ " (" +t("average frequency = ") + MathUtils.round(new Double(nrCircuits[d])/new Double(nrCircuitTypes[d]),2) + ")");
				result.appendln(nrCouples[d] + " " + t("couples concerned") + " ("+MathUtils.percent(nrCouples[d], nrCouples[0])+" %)"+", "+MathUtils.percent(nrCouples[d],couplesConsidered)+"% of all couples");
				result.appendln(nrIndividuals[d] + " " + t("individuals concerned") + " ("+MathUtils.percent(nrIndividuals[d], nrIndividuals[0])+" %)"+", "+MathUtils.percent(nrIndividuals[d],targetDomain.size())+"% of all individuals");
				result.appendln();
				result.appendln("Nr"+"\t"+signatureSequence+"\t"+t("#Circuits")+"\t"+t("% Circuits")+"\t"+openChainSequence+t("# Couples")+"\t"+t("% Couples")+"\t"+t("% All Couples")+"\t"+t("# Individuals")+"\t"+t("% Individuals")+"\t"+t("% All Individuals")+"\t"+labels.toStringSeparatedBy("\t"));
			}

			result.appendln(id + "\t" + clusterSignature(circuitCluster) + "\t"
					+ nrTheseCircuits + "\t"+ MathUtils.percent(nrTheseCircuits,nrCircuits[d]) + "\t"+ getOpenChainSequence(key)+ nrTheseCouples  + "\t"+ MathUtils.percent(nrTheseCouples,nrCouples[d]) + "\t"+ MathUtils.percent(nrTheseCouples,couplesConsidered) + "\t"+ nrThesePivots + "\t"+ MathUtils.percent(nrThesePivots,nrIndividuals[d]) + "\t"+ MathUtils.percent(nrThesePivots,targetDomain.size()) + "\t"
					+ ChainValuator.getValueString(circuitCluster.getFirstItem(),labels,null));
			
		}
		result.appendln();

		
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public StringList reportCircuitTypeList()
	{
		StringList result;

		result = new StringList();

		result.appendln("List by circuit types");
		result.appendln();
		int clusterCount = 1;
		for (Cluster<Chain> cluster : circuits.getClusters().toListSortedByValue()) {
			if (cluster.isNotEmpty()) {
				List<Chain> list = asSortedList(cluster.getItems());

				result.append(clusterCount);
				result.append(". ");
				result.append(clusterSignature(cluster));
				result.append(" (");
				result.append(list.size());
				result.appendln(")");
				for (Chain chain : list) {
					result.append("\t");
					result.append(chain.signature(Notation.PIVOTS));
					result.append("\t");
					result.append(chain.signature(Notation.NUMBERS));
					result.append("\t");
					result.append(groupSignature (chain));
					result.appendln(chain.signature(Notation.CLOSING_RELATION));
				}
				clusterCount += 1;
				result.appendln();
			}
		}
		result.appendln();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static StringList reportChainListByCluster(MultiPartition<Chain> partitions)
	{
		StringList result;

		result = new StringList();

		result.appendln("List by circuit types");
		result.appendln();
		int partitionCount = 1;
		for (Value rowValue : partitions.sortedRowValues()) {
			result.append(partitionCount);
			result.append(". ");
			result.append(rowValue.toString());
			result.append(" (");
			result.append(partitions.rowSum(rowValue));
			result.appendln(")");
			result.appendln();
			int clusterCount = 1;
			for (Value colValue : partitions.sortedColValues()) {
				Cluster<Chain> cluster = partitions.getCluster(rowValue, colValue);

				if (cluster !=null && cluster.isNotEmpty()) {
					List<Chain> list = asSortedList(cluster.getItems());
					result.append("\t");
					result.append(clusterCount);
					result.append(". ");
					result.append(cluster.getValue().toString());
					result.append(" (");
					result.append(list.size());
					result.appendln(")");
					for (Chain chain : list) {
						result.append("\t\t");
						result.append(chain.signature(Notation.PIVOTS));
						result.append("\t");
						result.append(chain.signature(Notation.NUMBERS));
						result.appendln();
					}
					clusterCount += 1;
					result.appendln();
				}
			}
			result.appendln();
		}
		result.appendln();

		//
		return result;
	}
	
	public List<Chain> getOutOfCircuitCouples(){

		if (outOfCircuitCouples==null &&  consideredCouples!=null){

			outOfCircuitCouples = new ArrayList<Chain>();
			for (Chain couple : consideredCouples){
				if (!couples.getItems().contains(couple)){
					outOfCircuitCouples.add(couple);
				}
			}
			outOfCircuitCouples = asSortedList(outOfCircuitCouples);
		}
		
		//
		return outOfCircuitCouples;
	}
	
	/**
	 * 
	 * @return
	 */
	public StringList reportCoupleList() {
		StringList result;

		result = new StringList();

		result.appendln("List by couples");
		result.appendln();
		
		Map<Chain,Set<Chain>> couples = coupleChainMap(Gender.MALE);
		
		int clusterCount=1;
		for (Chain couple : asSortedList(couples.keySet())) {
				List<Chain> list = asSortedList(couples.get(couple));

				result.append(clusterCount);
				result.append(". ");
				result.append(couple.signature(Notation.COUPLE));
				result.append(" (");
				result.append(list.size());
				result.append(") ");
				result.append("\t");
				result.appendln(list.get(0).signature(Notation.CLOSING_RELATION));

				for (Chain chain : list) {
					result.append("\t");
					result.append(chain.signature(Notation.CLASSIC));
					result.append("\t");
					result.append(chain.signature(Notation.NUMBERS));
					result.appendln(groupSignature (chain));
				}

				clusterCount += 1;
				result.appendln();
			}
		result.appendln();
		
		// OutOfCircuitCouples
		
		if (consideredCouples!=null){
			
			int outOfCircuitCount = getOutOfCircuitCouples().size();
			
			result.appendln(outOfCircuitCount+" Couples out of circuits ("+MathUtils.percent(outOfCircuitCount, couplesConsidered)+"% of all couples examined):");
			result.appendln();
			
			int count = 1;
			
			for (Chain couple : getOutOfCircuitCouples()){
				result.appendln(count+"\t"+couple.getFirst()+"\t"+couple.getLast());
				count++;
			}
		}
		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
/*	public StringList reportCoupleListOld() {
		StringList result;

		result = new StringList();

		result.appendln("List by couples");
		result.appendln();

		Partition<Chain> byCouples = byCouples(Gender.MALE);

		int clusterCount=1;
		for (Cluster<Chain> cluster : byCouples.getClusters().toListSortedByValue()) {
			if (cluster.isNotEmpty()) {
				List<Chain> list = asSortedList(cluster.getItems());

				Chain couple = (Chain)cluster.getValue().chainValue();

				result.append(clusterCount);
				result.append(". ");
				result.append(couple.signature(Notation.COUPLE));
				result.append(" (");
				result.append(list.size());
				result.append(") ");
				result.append("\t");
				result.appendln(list.get(0).signature(Notation.CLOSING_RELATION));

				for (Chain chain : list) {
					result.append("\t");
					result.append(chain.signature(Notation.CLASSIC));
					result.append("\t");
					result.append(chain.signature(Notation.NUMBERS));
					result.appendln(groupSignature (chain));
				}

				clusterCount += 1;
				result.appendln();
			}
		}
		result.appendln();

		//
		return result;
	}*/
	
	/**
	 * 
	 * @return
	 */
	public StringList reportSortableList() {
		StringList result;

		result = new StringList();

		result.appendln("Sortable list");
		result.appendln();
		result.append("Nr.").append("\t");
		if (classification.equals("SIMPLE")){
			result.append("Standard\tPositional\tVector").append("\t");
		} else {
			result.append("Circuit Type").append("\t");
		}
		result.append("Perspective").append("\t");
		result.append("Ego").append("\t");
		result.append("Alter").append("\t");
		result.append("Pivots").append("\t");
		result.append("Chain").append("\t");
		result.append("Marriage Years").appendln();

		int clusterCount = 1;
		for (Cluster<Chain> cluster : circuits.getClusters().toListSortedByValue()) {
			//
			if (cluster.isNotEmpty()) {
				List<Chain> list = asSortedList(cluster.getItems());
				
				for (Chain chain : list) {
					List<Chain> perspectives;
					if (criteria.isWithAllPerspectives()){
						perspectives = chain.getPermutations(symmetry);
					} else {
						perspectives = new ArrayList<Chain>();
						perspectives.add(chain);
					}
										
					for (Chain perspective : perspectives){

						result.append(clusterCount).append("\t");
						result.append(clusterSignature(cluster)).append("\t");
						result.append(perspective.signature(Notation.POSITIONAL)).append("\t");
						result.append(perspective.getFirst().getId()).append("\t");
						result.append(perspective.getLast().getId()).append("\t");
						result.append(perspective.signature(Notation.PIVOTS)).append("\t");
						result.append(perspective.signature(Notation.NUMBERS)).append("\t");
						result.append(groupSignature (perspective)).append(perspective.signature(Notation.CLOSING_RELATION)).append("\t");
						result.appendln(perspective.getMarriageYears(familyCompareDomain).toString());
					}
				}
				
				clusterCount += 1;
			}
		}
		
		if (criteria.isWithOutOfCircuitCouples()){
		
			for (Chain chain : getOutOfCircuitCouples()) {
				
				List<Chain> perspectives;
				if (criteria.isWithAllPerspectives()){
					perspectives = chain.getPermutations(SymmetryType.INVERTIBLE);
				} else {
					perspectives = new ArrayList<Chain>();
					perspectives.add(chain);
				}
				
				for (Chain perspective : perspectives){

					result.append(clusterCount).append("\t");
					result.append("Out of circuit").append("\t");
					if (classification.equals("SIMPLE")){
						result.append("\t\t");
					}
					result.append("\t");
					result.append(perspective.getFirst().getId()).append("\t");
					result.append(perspective.getLast().getId()).append("\t");
					result.append(perspective.signature(Notation.PIVOTS)).append("\t");
					result.append(perspective.signature(Notation.NUMBERS)).append("\t");
					result.append(groupSignature (perspective)).appendln(perspective.signature(Notation.CLOSING_RELATION));
				}
			}

		}

		result.appendln();

		//
		return result;
	}

	
	private static Chain createLinearChain(Chain chain){
		Chain result;
		
		result = chain.clone();
		result.subchains = new ArrayList<Chain>();
		result.subchains.add(chain.clone());
		result.subchains.add(new Chain(chain.getLast()));
		
		//
		return result;
	}
	
	private void findLinearChains(final Chain chain){
		if (chain.length() <= maxDeg[0]) {
			if (chain.length()>0) {
				put(createLinearChain(chain));
			}
			for (Individual parent : chain.getLast().getParents()) {
				if (chain.add(parent, 1)){
					findLinearChains(chain);
					chain.removeLast();
				}
			}
		}
	}

	/**
	 * expands an open chain and adds the found chains to a chain list
	 * 
	 * @param rings
	 *            the underlying RingGroupMap
	 * @param list
	 *            the chain list
	 * @since 10/04/12, last modified 10/04/17
	 */
/*	public void searchChains(final Chain chain, final List<Chain> list, final List<Individual> visited) {
		int d = chain.depth();
		if (!cannotPass1(chain) && d <= degrees[chain.dim() - 1]) {
			Chain r = chain.neutralize(sib);
			if (chain.apices != null) {
				r.addApices(chain.apices);
			}
			if (!list.contains(r)) {
				list.add(r); // or put?
			}
		}
		// if (isSolo() && p.dim()>1 && p.dir(1)!=0) return;
		visited.add(chain.getLast());
		for (int t = -1; t < 2; t++) {
			if (t == 0) {
				if (chain.dim() + 1 > dim()) {
					continue;
				}
				if (d > maxDeg[chain.dim()]) {
					continue;
				}
				chain.order++;
			} else if (t == chain.lastDir()) {
				if (chain.getLastBranchSize() >= maxDeg[chain.dim() - 1]) {
					continue;
				}
			} else if (chain.lastDir() == 1 && t == -1) {
				if (linearOnly) {
					continue;
				}
				chain.addApex(chain.length());
			} else if (chain.lastDir() == -1 && t == 1) {
				continue;
			}
			for (Individual v : chain.getLast().getKin(t)) {
				if (v == null || visited.contains(v)) {
					continue;
				}
				chain.add(v, t);
				searchChains(chain, list, visited);
				chain.cut();
			}
			if (t == 0) {
				chain.order--;
			} else if (t == -1 && chain.dir(chain.length()) == 1) {
				chain.apices.remove(chain.apices.size() - 1);
			}
		}
		visited.remove(chain.getLast());
	}*/
	


	public Map<Individual, Map<Individual, Integer>> getConsanguines(final int max) {

		// Create map of linear distances
		Map<Individual, Map<Individual, Integer>> linearRelatives = new HashMap<Individual, Map<Individual, Integer>>(); // Map
		

		for (Individual ego : getLinkDomain()) {
			createAscendantTies(linearRelatives, ego, ego, 0, max);
		}

		// Create map of consanguineal distances
		consanguines = new HashMap<Individual, Map<Individual, Integer>>(); 
		
		for (Individual ego : linearRelatives.keySet()) {
			if (incorrectProperties(ego)) {
				continue;  
			}

			Map<Individual, Integer> ascendants = linearRelatives.get(ego);
			for (Individual ascendant : ascendants.keySet()) {
				int upDistance = ascendants.get(ascendant);
				if (upDistance < 0) {
					continue; 
				}

				Individuals intermediates = new Individuals();
				intermediates.put(ascendant);
				if (mergingPartition !=null) {
					Cluster<Individual> ascendantsCluster = mergingPartition.getCluster(ascendant);
					if (ascendantsCluster!=null && ascendantsCluster.getLabel()!=null) {
						for (Individual ascendantEquivalent : ascendantsCluster.getItems()){
							intermediates.put(ascendantEquivalent);
						}
					}
				}
				for (Individual ancestor : intermediates){
					Map<Individual, Integer> ascendantsDescendants = linearRelatives.get(ancestor);
					for (Individual alter : ascendantsDescendants.keySet()) {
						if (incorrectProperties(alter)) {
							continue;
						}
						int downDistance = ascendantsDescendants.get(alter);
						if (downDistance > 0) {
							continue; 
						}
						set(consanguines, ego, alter, Math.max(upDistance, -downDistance));
					}
				}
			}
		}

		//
		return consanguines;

	}

	/**
	 * checks whether the affinal chains contain only one consanguinous bridge
	 * 
	 * @return true if the affinal chains contain only one consanguinous bridge
	 */
/*	public boolean solo() {
		if (models == null) {
			return false;
		}
		if (dim() < 2) {
			return false;
		}
		for (Chain r : models) {
			boolean allZero = true;
			for (int i : r.profile(false)) {
				if (i == 0) {
					continue;
				}
				if (!allZero) {
					return false;
				}
				allZero = false;
			}
		}
		return true;
	}*/

	/*
	 * private void setGender (Chain c){ for (Link e : c){ e.setGender(net); } }
	 */

	/*
	 * private static boolean add(Collection<Chain> set, Chain c){ for (Chain e
	 * : set){ if (e.equals(c)) return false; } return set.add(c); }
	 */

	/**
	 * gets the sum of chains or circuits in the groupmap
	 * 
	 * @return the sum of chains or circuits
	 */
	public int sum() {
		return circuits.itemsCount();
	}

	// translatable
	public String t(final String string) {
		return string;
	}

	/**
	 * creates a matrimonial network (where marriage ties are marked according
	 * to circuits)
	 * 
	 * @return a matrimonial network
	 * @see io.write.AbstractWriter#writeNet(Net, RingGroupMap, int, int)
	 */
	public Net toNet() {
		Net net = new Net();
		List<Individual> pivotals = new ArrayList<Individual>();
		for (Cluster<Chain> c : circuits.getClusters()) {
			for (Chain t : c.getItems()) {
				int n = t.size();
				for (int i = 0; i < n; i++) {
					int j = (i + 1) % n;
					Individual e = t.get(i).clone();
					Individual a = t.get(j).clone();
					e.getKin(t.dir(j)).add(a);
					if (t.pivotal(i)) {
						pivotals.add(e);
						// int[] h = v.getLink(w); // geht direkter via dir!!
						// if (h!=null) e.addKin(a, h[0], h[1], true);
						// if (t.pivotal(i)) e.setCluster(1);
						// else if (e.getCluster()==null) e.setCluster(0);
					}
				}
			}
		}
		return net;
	}

	// allow homosexual marriage
	// Check for restriction 5
	/**
	 * checks whether a NumberChain violates a conditition of different gender
	 * for ego and alter
	 * <p>
	 * only important for non-matrimonial censuses! (if only married couples are
	 * allowed from the outset, the condition cannot be violated
	 * 
	 * @param p
	 *            the NumberChain to be checked
	 * @return true if the NumberChain violates the cross-sex condition
	 * @see RingGroupMap#notInDomain(NumberChain)
	 */
/*	private boolean violatesGenderRelation(final Chain p) {
		boolean result;
		if (marriedOnly || !crossSex) {
			result = false;
		} else if (p.get(0).getGender() == p.getLast().getGender()){
			result = true;
		} else {
			result = false;
		}
		//
		return result;
	}*/
	
	private boolean incorrectRelation(Individual ego, Individual alter) {
		boolean result;
		
	    if (ego==alter){ // relationType.equals("OPEN") && 
	    	result = true;
	    } else if (crossSex) {
			result = (ego.getGender() == alter.getGender());
		} else {
			result = false;
		}
		//
		return result;
	}


	//check - ringtype conditions are not mutually exclusive (roundabout interdiciton 
	/**
	 * checks whether a circuit violates a non-inclusion condition
	 * <p>
	 * this check concerns only the minor and minimal ring property. the ring
	 * property (ring type 1, no roundabouts) has already been checked during
	 * bridge construction
	 * 
	 * @param r
	 *            the ring to be checked
	 * @return true if the ring is not of the required type
	 */
	private boolean violatesRingType(final Chain r) {
		boolean result = false;
		
		// Ring and stronger
		if (ringType != CircuitType.CIRCUIT) {
			if (isRoundabout(r, sib)) result = true;
			// Minor ring and stronger
			if (ringType != CircuitType.RING) {
				if (hasShortcut(r, false)) result = true;
				// Minimal ring
				if (ringType != CircuitType.MINOR) {
					if (hasShortcut(r, true)) result = true;
				}
			}
		}
			
		return result;
	}

	/**
	 * checks whether two positions of a circuit have a given distance
	 * 
	 * @param start
	 *            the first position
	 * @param end
	 *            the second position
	 * @param distance
	 *            the required distance of the two vertices
	 * @return true if the distance has the desired value
	 * @see OldRing#isRoundabout(int)
	 */
	static boolean adjacent(final int start, final int end, final int distance, final int size) {
		boolean result = false;

		if (start == end) {
			result = true;
		} else {
			result = distance == (size + start - end) % size;

			// int d = Math.abs(start-end);
			// if (d==distance) return true;
			// if (d==size-distance) return true;
		}

		//
		return result;
	}

	public static <T extends Comparable<? super T>> List<T> asSortedList(final Collection<T> c) {
		List<T> list = new ArrayList<T>(c);
		java.util.Collections.sort(list);
		return list;
	}

	// move to chainFinder
	// harmonize with other chainFinders
	/**
	 * checks whether the ego of the circuit can be connected to an alter vertex
	 * via expansion of the circuit up to given length and width
	 * 
	 * @param target
	 *            the alter vertex
	 * @param maxLength
	 *            the maximal length of the kinship chain
	 * @param maxOrder
	 *            the maximal order (width) of the kinship chain
	 * @return true if ego and alter can be connected by expansion of circuit
	 * @see OldIndividual.nodes.Vertex#isConnected(Individual, int, int)
	 */
	public static boolean connects(final Chain chain, final Individual target, final int maxLength, final int maxOrder, final List<Individual> visited) {
		if (chain.dim() > maxOrder + 1) {
			return false;
		}
		if (chain.getLast() == target && chain.length() > 1) {
			return true;
		}
		if (chain.length() >= maxLength) {
			return false;
		}
		visited.add(chain.getLast());
		for (int t = -1; t < 2; t++) {
			if (t == 0) {
				chain.setOrder(chain.getOrder() + 1);
			}
			if (chain.lastDir() == -1 && t == 1) {
				continue;
			}
			for (Individual v : chain.getLast().getKin(t)) {
				if (v == null || visited.contains(v)) {
					continue;
				}
				chain.add(v, t);
				if (connects(chain, target, maxLength, maxOrder, visited)) {
					chain.removeLast();
					if (t == 0) {
						chain.setOrder(chain.getOrder() - 1);
					}
					visited.remove(chain.getLast());
					return true;
				}
				chain.removeLast();
			}
			if (t == 0) {
				chain.setOrder(chain.getOrder() - 1);
			}
		}
		visited.remove(chain.getLast());
		return false;
	}

	static boolean connects(final Individual ego, final Individual alter, final int maxLength, final int maxOrder) {
		return connects(new Chain(ego), alter, maxLength, maxOrder, new ArrayList<Individual>());
	}

	/**
	 * checks whether the Rings of the Group have couples in common with a given
	 * Ring
	 * 
	 * @param r
	 *            the Ring to be checked
	 * @return true if the Rings of the Group have couples in common with that
	 *         Ring
	 * @see groups.RingGroup#reduce(RingGroup)
	 */
	static boolean hasCouplesInCommon(final Cluster<Chain> clu, final Chain r) {
		for (Chain s : clu.getItems()) {
			for (Chain c1 : s.getCouples()) {
				for (Chain c2 : r.getCouples()) {
					if (c1.equals(c2)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	// check definition of minor and minimal rings
	// check harmony of definitions of w in hasShortCut and isConnected
	/**
	 * checks whether the ring has a (strict or weak) shortcut, i.e. whether
	 * there is a shorter (and narrower) chain connecting an ego-alter pair
	 * <p>
	 * used for determining minor rings and minimal rings
	 * 
	 * @param strict
	 *            true if no narrower chain is allowed, false if only shorter
	 *            chains are excluded
	 * @return true if there is a (strict or weak) shortcut
	 */
	public static boolean hasShortcut(final Chain r, final boolean strict) {
		int w = r.dim() - 1; // check (w = r.dim()?)
		if (strict) {
			w = 100;
		}
		for (int i = 0; i < 2 * r.dim(); i++) {
			Chain s = r.transform(i);
			if (connects(s.getFirst(), s.getLast(), s.length() - 1, w)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * checks whether a vertex is linked to another via the apical couple that
	 * one of them represents (after sibling tie neutralization)
	 * <p>
	 * used for eliminating circuit inclusion in ring search
	 * 
	 * @param alter
	 *            the alter vertex
	 * @return true if one of the vertices is linked to the other as a
	 *         representative of a hidden apical couple
	 * @see chains.Ring#isRoundabout(int)
	 */
	public boolean haveApicalLink(final Individual ego, final Individual alter) {
		boolean result = false;

		if (!(ego instanceof Couple) && !(alter instanceof Couple)) {
			result = haveDirectLink(ego, alter);
		} else if (!(alter instanceof Couple)) {
			for (Individual w : ((Couple) ego).individuals()) {
				if (haveDirectLink(w, alter)) {
					result = true;
					break;
				}
			}
		} else if (!(ego instanceof Couple)) {
			for (Individual w : ((Couple) alter).individuals()) {
				if (haveDirectLink(ego, w)) {
					result = true;
					break;
				}
			}
		} else {
			for (Individual w : ((Couple) ego).individuals()) {
				for (Individual u : ((Couple) alter).individuals()) {
					if (haveDirectLink(w, u)) {
						return true; // don't know how to write jump out of
										// double loop with break condition,
										// sorry...
					}
				}
			}
		}
		//
		return result;
	}

	/**
	 * @return true if the two vertices are linked by a simple kinship tie
	 */
	private boolean haveDirectLink(final Individual ego, final Individual alter) {
		boolean result = false;

		Individuals spouses = ego.spouses();
		
		if (!ascendingRelationType.equals("PARENT")){
			spouses = getClosingAlters(ego);
		}
		
		result = (spouses.contains(alter) || getAscendingAlters(ego).contains(alter) || getAscendingAlters(alter).contains(ego));

		
/*		if (ego.spouses().contains(alter) || ego.getParents().contains(alter) || ego.children().contains(alter)) {
			result = true;
		}*/

		//
		return result;
	}
	
	/**
	 * @return true if the two vertices are linked by a simple kinship tie
	 */
	private boolean haveDirectLinearLink(final Individual ego, final Individual alter) {
		boolean result = false;

		result = (getAscendingAlters(ego).contains(alter) || getAscendingAlters(alter).contains(ego));
		
/*		if (ego.getParents().contains(alter) || ego.children().contains(alter)) {
			result = true;
		}*/

		//
		return result;
	}
	

	/**
	 * gets an integer array representation of a string of numbers
	 * 
	 * @param str
	 *            the string of numbers
	 * @return the integer array
	 * @see maps.groupmaps#RingGroupMap#deg()
	 */
	private static int[] intArray(final String str) {
		String[] s = Trafo.trim(str).split(" ");
		int[] a = new int[s.length];
		for (int i = 0; i < s.length; i++) {
			a[i] = Integer.parseInt(s[i].trim());
		}
		return a;
	}

	/**
	 * checks if there are shorter connections between ring membersByRelationId
	 * 
	 * @param sib
	 *            the sibling mode of the census
	 * @return true if there are shorter connections between ring membersByRelationId
	 */
	public boolean isRoundabout(final Chain chain, final SiblingMode sib) {
		boolean result = false;

		// check for shortcuts between ego and alter
		
		if (chain.length()>1){
			if (chain.getSymmetry()==SymmetryType.PERMUTABLE){
				if (haveDirectLinearLink(chain.getFirst(),chain.getLast())){
					return true;
				}
			} else {
				if (haveDirectLink(chain.getFirst(),chain.getLast())){
					return true;
				}
			}
		}
		
		// check for shortcuts involving intermediary links
		int n = chain.size();
		if (n >= 3) {
			for (int i = 2; i < n; i++) {
				int s = 0;
				if (i == n - 1) {
					s = 1;
				}
				for (int j = s; j < i - 1; j++) {

					Individual ego = chain.get(i);
					Individual alter = chain.get(j);
					
					// check apical link between non-adjacent individuals
					if (haveApicalLink(ego, alter)) {
						return true;
					// check full sibling link between individuals distant by more than two links
					} else if (sib != SiblingMode.ALL || adjacent(i, j, 2, n)) {
						continue;
					} else if (siblings(ego, alter)) { 
						return true;
					}
				}
			}
		}

		return result;
	}

	/**
	 * removes all Chains from the Group that have couples in common with the
	 * Chains of another Group
	 * 
	 * @param clu2
	 *            the RingGroup to be compared
	 * @see maps.groupmaps.RingGroupMap#reduce(ChainGroup)
	 * @see maps.groupmaps.RingGroupMap#filter(String)
	 */
	public static void reduce(final Cluster<Chain> clu1, final Cluster<Chain> clu2) {
		if (clu1.equals(clu2)) {
			return;
		}
		Iterator<Chain> it = clu1.getItems().iterator();
		while (it.hasNext()) {
			if (CircuitFinder.hasCouplesInCommon(clu2, it.next())) {
				it.remove();
			}
		}
	}

	/**
	 * sets a link of given degree between two vertices
	 * 
	 * @param ego
	 *            the ID number of ego
	 * @param alter
	 *            the Id number of alter
	 * @param distance
	 *            the consanguineous distance (canonic degree)
	 */
	private static void set(final Map<Individual, Map<Individual, Integer>> distMap, final Individual ego, final Individual alter, final int distance) {
		if (distMap.get(ego) == null) {
			distMap.put(ego, new HashMap<Individual, Integer>());
		}
		Map<Individual, Integer> map = distMap.get(ego);
		if (map.get(alter) == null || distance < map.get(alter)) {
			map.put(alter, distance);
		}
	}

	/**
	 * checks whether the vertex is sibling of another vertex
	 * 
	 * @param alter
	 *            the alter vertex
	 * @return true if ego and alter are siblings
	 * @see chains.Ring#isRoundabout(int)
	 */
	static boolean siblings(final Individual ego, final Individual alter) {
		for (int i = 0; i < 2; i++) {
			Individual egoParent = ego.getParent(i);
			Individual alterParent = alter.getParent(i);
			if (egoParent != null && alterParent != null && egoParent.equals(alterParent)) {
				return true;
			}
		}
		return false;
	}

	public Partition<Chain> getCircuits() {
		return circuits;
	}

	public void setCircuits(Partition<Chain> circuits) {
		this.circuits = circuits;
	}

	/**
	 * 
	 * @param segmentation
	 * @param relationModelName
	 * @param relationIdLabel
	 * @param censusCriteria
	 * @return
	 * @throws PuckException
	 */
	// Check segmentation method for "domain"
	public static MultiPartition<Chain> createDifferentialCensus(final Segmentation segmentation, String relationModelName, String relationIdLabel, final String dateLabel, final Integer date, final CensusCriteria censusCriteria) throws PuckException {
	
		MultiPartition<Chain> result = new MultiPartition<Chain>();
		result.setLabel(censusCriteria.getIndividualPartitionLabel());

		Relations relations = segmentation.getCurrentRelations().getByModelName(relationModelName);
			
		for (Relation relation : relations) {
			String relationName = relation.getAttributeValue(relationIdLabel);
/*			if (relationName==null){
				relationName = relation.getName();
			}*/
			Integer time = relation.getTime(dateLabel);
						
			if (relationName!=null && (date==null || (time!=null && time.equals(date)))){
				Individuals individuals = relation.getIndividuals();
				Segmentation domain = new Segmentation(individuals,segmentation.getAllFamilies(),segmentation.getAllRelations(), segmentation.getGeography());
				CircuitFinder finder = new CircuitFinder(domain,censusCriteria);
				finder.findCircuits();
				result.put(finder.getCircuits(), new Value(relationName));
				
				int rowSum = individuals.size()*(individuals.size()-1);
				if (censusCriteria.getSymmetryType()==SymmetryType.INVERTIBLE){
					rowSum = rowSum/2;
				}
				result.putRowSum(new Value(relationName), rowSum);
			}
		}
	
		return result;
	}


	/**
	 * 
	 * @param individuals
	 * @param criteria
	 * @return
	 * @throws PuckException
	 */
	// Check segmentation method for "domain"
	public static MultiPartition<Chain> createDifferentialCensus(final Segmentation segmentation, final CensusCriteria censusCriteria) throws PuckException {
	
		MultiPartition<Chain> result = new MultiPartition<Chain>();
		result.setLabel(censusCriteria.getIndividualPartitionLabel());
	
		Partition<Individual> partition = PartitionMaker.createRaw(censusCriteria.getIndividualPartitionLabel(), segmentation.getCurrentIndividuals(),
				censusCriteria.getIndividualPartitionLabel(), segmentation.getGeography());
	
		for (Cluster<Individual> individualCluster : partition.getClusters()) {
			if (!individualCluster.isNull()) {
				Individuals individuals = new Individuals(individualCluster.getItems());
				Segmentation domain = new Segmentation(individuals,segmentation.getAllFamilies(),segmentation.getAllRelations(), segmentation.getGeography());
				CircuitFinder finder = new CircuitFinder(domain,censusCriteria);
				finder.findCircuits();
				result.put(finder.getCircuits(), individualCluster.getValue());
				result.putRowSum(individualCluster.getValue(), finder.getCouplesConsidered());
			}
		}
	
		return result;
	}
	
	public static MultiPartition<Chain> createDifferentialCensusByCouples (final MultiPartition<Chain> census, final CensusCriteria censusCriteria) throws PuckException {
		
		MultiPartition<Chain> result = new MultiPartition<Chain>();
		result.setLabel(censusCriteria.getIndividualPartitionLabel());
	
		for (Value rowValue : census.rowValues()) {
			
			Partition<Chain> circuits = census.getRow(rowValue);
			Partition<Chain> couples = CircuitFinder.createCouplePartition(circuits, Gender.MALE, censusCriteria.isCrossSexChainsOnly(), censusCriteria.getSymmetryType());
			result.put(couples, rowValue);
			result.putRowSum(rowValue, census.rowSum(rowValue));
		}
	
		return result;
	}

	
	private static void increment (Map<Value,Integer> sumMap, Map<Value,Integer> incMap, Value key){
		if (incMap.get(key)!=null){
			if (sumMap.get(key)==null){
				sumMap.put(key, incMap.get(key));
			} else {
				sumMap.put(key, sumMap.get(key)+incMap.get(key));
			}
		}
	}
	
	public void initializeCounts(){
		int n = dim()+1;
		nrCircuits = new int[n];
		nrCouples = new int[n];
		nrIndividuals = new int[n];
		nrCircuitTypes = new int[n];
		nrCircuitsForKey = new HashMap<Value,Integer>();
		nrCouplesForKey = new HashMap<Value,Integer>();
		nrPivotsForKey = new HashMap<Value,Integer>();
		if (openChainFrequencies){
			nrOpenChainsForKey = new HashMap<Value,Integer>();
		}
	}
	
	public void incrementCounts (CircuitFinder finder){
		finder.count();
		for (Value value : finder.getCircuits().getValues()){
			Cluster<Chain> cluster = finder.getCircuits().getCluster(value);
			if (cluster.size()>0){
				circuits.put(cluster.getFirstItem(), value);
			}
		}
		
		for (int i=0;i<dim()+1;i++){
			nrCircuits[i] += finder.nrCircuits[i];
			nrCouples[i] += finder.nrCouples[i];
			nrIndividuals[i] += finder.nrIndividuals[i];
			nrCircuitTypes[i] += finder.nrCircuitTypes[i];
		}
		
		for (Value key : finder.circuits.getValues()){
			increment(nrCircuitsForKey,finder.nrCircuitsForKey,key);
			increment(nrCouplesForKey,finder.nrCouplesForKey,key);
			increment(nrPivotsForKey,finder.nrPivotsForKey,key);
			if (openChainFrequencies){
				increment(nrOpenChainsForKey,finder.nrOpenChainsForKey,key);
			}
		}
	}
	
	public void normalizeCounts (int runs){
		for (int i=0;i<dim()+1;i++){
			nrCircuits[i] = nrCircuits[i]/runs;
			nrCouples[i] = nrCouples[i]/runs;
			nrIndividuals[i] = nrIndividuals[i]/runs;
			nrCircuitTypes[i] = nrCircuitTypes[i]/runs;
		}
		for (Value key : circuits.getValues()){
			nrCircuitsForKey.put(key,nrCircuitsForKey.get(key)/runs);
			nrCouplesForKey.put(key,nrCouplesForKey.get(key)/runs);
			nrPivotsForKey.put(key,nrPivotsForKey.get(key)/runs);
			if (openChainFrequencies){
				nrOpenChainsForKey.put(key,nrOpenChainsForKey.get(key)/runs);
			}
		}
	}
	
	
/*	public void add (CircuitFinder3 finder){

		circuits.add(finder.getCircuits());
		couples.add(finder.couples);
		pivots.add(finder.pivots);
		
		for (int i = 0; i < dim()+1; i++) {
			for (Chain chain : finder.couplesByOrder[i]){
				couplesByOrder[i].add(chain);
			}
			for (Individual individual: finder.pivotsByOrder[i]){
				pivotsByOrder[i].add(individual);
			}
		}
	}*/

	/*
	 * private boolean isSingle(int id){ Vertex get = net.get(id); if (get ==
	 * null) { get = net.get(id); } return get.isSingle(); }
	 */


	/**
	 * After findCircuits() and count(), this method returns the nrCouples[0] value.
	 * This is the number of couples.
	 * 
	 * This method is called by Kinsources (Stag).
	 * 
	 * @return
	 */
	public long getCircuitCoupleCount()
	{
		long result;

		result = this.nrCouples[0];

		//
		return result;
	}

	/**
	 * After findCircuits() and count(), this method returns the density of couples.
	 * 
	 * This method is called by Kinsources (Stag).
	 * 
	 * @return
	 */
	public double getCircuitCoupleDensity()
	{
		double result;

		result = MathUtils.percent(this.nrCouples[0], this.couplesConsidered);

		//
		return result;
	}



	public Individuals getLinkDomain() {
		return linkDomain;
	}



	public void setLinkDomain(Individuals linkDomain) {
		this.linkDomain = linkDomain;
	}



	public Individuals getSearchDomain() {
		return searchDomain;
	}



	public void setSearchDomain(Individuals searchDomain) {
		this.searchDomain = searchDomain;
	}

}
