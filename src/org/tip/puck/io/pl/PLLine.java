package org.tip.puck.io.pl;

import org.apache.commons.lang3.math.NumberUtils;

/**
 * This class represents a line of individual from a TXT file.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class PLLine {

	private String label;
	private int value1;
	private String value2;
	private boolean isValue2Number;

	/**
	 * 
	 */
	public PLLine(final String label, final int value1, final char value2) {
		this.label = label;
		this.value1 = value1;
		this.value2 = String.valueOf(value2);
		this.isValue2Number = false;
	}

	/**
	 * 
	 */
	public PLLine(final String label, final int value1, final int value2) {
		this.label = label;
		this.value1 = value1;
		this.value2 = String.valueOf(value2);
		this.isValue2Number = true;
	}

	/**
	 * 
	 */
	public PLLine(final String label, final int value1, final String value2) {
		this.label = label;
		this.value1 = value1;
		this.value2 = value2;
		this.isValue2Number = false;
	}

	public boolean isValue2Number() {
		return this.isValue2Number;
	}

	public String label() {
		return this.label;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public void setValue1(final int value1) {
		this.value1 = value1;
	}

	public void setValue2(final String value2) {
		this.value2 = value2;
	}

	public int value1() {
		return this.value1;
	}

	/**
	 * 
	 * @return
	 */
	public int value2() {
		int result;

		if (NumberUtils.isNumber(this.value2)) {
			result = Integer.parseInt(this.value2);
		} else {
			result = -1;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public char value2ToChar() {
		char result;

		if (this.value2.length() == 1) {
			result = this.value2.charAt(0);
		} else {
			result = 0;
		}

		//
		return result;
	}

	public String value2ToString() {
		return this.value2;
	}

}
