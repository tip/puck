package org.tip.puck.util;

import java.util.ArrayList;
import java.util.List;

import org.tip.puck.geo.Geography;

/**
 * 
 * @author TIP
 */
public abstract class Valuator<E> {

	/**
	 * 
	 * @param source
	 * @param label
	 * @return
	 */
	public abstract Value get(final E source, final String label, final Geography geography);

	/**
	 * 
	 * @param node
	 * @param label
	 * @return
	 */
	public Values get(final List<E> source, final String label, final Geography geography) {
		Values result;

		if ((source == null) || (label == null)) {
			result = new Values();
		} else {
			result = new Values(source.size());
			for (E node : source) {
				result.add(get(node, label, geography));
			}
		}

		//
		return result;
	}

	/**
	 * Builds endogenous labels as list of string.
	 * 
	 * @return The list of endogenous labels.
	 */
	public abstract List<String> getAttributeLabels();

	/**
	 * Extracts endogenous labels from a label list.
	 * 
	 * @param sourceLabels
	 *            From where extract endogenous labels.
	 * 
	 * @return Endogenous labels extracted from the source.
	 */
	public List<String> getMatchingLabels(final List<String> sourceLabels) {
		List<String> result;

		if (sourceLabels == null) {
			result = new ArrayList<String>();
		} else {
			result = new ArrayList<String>(sourceLabels);
			result.retainAll(getAttributeLabels());
		}

		//
		return result;
	}

	/**
	 * Extracts not endogenous labels from a label list.
	 * 
	 * @param sourceLabels
	 *            From where extract not endogenous labels.
	 * 
	 * @return Not endogenous labels extracted from the source.
	 */
	public List<String> getNotMatchingLabels(final List<String> sourceLabels) {
		List<String> result;

		if (sourceLabels == null) {
			result = new ArrayList<String>();
		} else {
			result = new ArrayList<String>(sourceLabels);
			result.removeAll(getAttributeLabels());
		}

		//
		return result;
	}
}
