package org.tip.puck.net.workers;

/**
 * 
 * 
 * @author TIP
 */
public class AttributeValueDescriptor {

	private String value;
	private long count;

	/**
	 * 
	 * @param scope
	 * @param label
	 */
	public AttributeValueDescriptor(final String value) {
		//
		this.value = value;
		this.count = 0;
	}

	public long getCount() {
		return this.count;
	}

	public String getValue() {
		return this.value;
	}

	public void inc() {
		this.count += 1;
	}

	public void inc(final long value) {
		this.count += value;
	}

	public void setCount(final long count) {
		this.count = count;
	}

	public void setValue(final String value) {
		this.value = value;
	}
}
