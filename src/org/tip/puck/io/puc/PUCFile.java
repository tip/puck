package org.tip.puck.io.puc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import javax.xml.bind.DatatypeConverter;
import javax.xml.stream.XMLStreamException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.geo.Geography;
import org.tip.puck.geo.io.GEOTXTFile;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Attributes;
import org.tip.puck.net.Families;
import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.net.UnionStatus;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Actors;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.RelationModels;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.relations.Roles;
import org.xml.sax.SAXException;

import fr.devinsy.util.FileTools;
import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLBadFormatException;
import fr.devinsy.util.xml.XMLReader;
import fr.devinsy.util.xml.XMLTag;
import fr.devinsy.util.xml.XMLTag.TagType;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.util.xml.XMLWriter;

/**
 * This class represents a PUC File reader and writer.
 * 
 * @author TIP
 */
public class PUCFile {
	private static final Logger logger = LoggerFactory.getLogger(PUCFile.class);

	public static final String XSD_FILE = "/org/tip/puck/io/puc/puck-1.3.xsd";
	public static final String PUCK_FORMAT_VERSION = "1.3";
	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	/**
	 * This method converts a String to a Integer in applying the rules relating
	 * to order values.
	 * 
	 * @param value
	 *            the value to convert.
	 * 
	 * @return the value converted.
	 * 
	 * @throws PuckException
	 *             BAD_FILE_FORMAT if the value is not a number or a positive
	 *             integer.
	 */
	public static Integer convertOrderValue(final String value) throws PuckException {
		Integer result;

		//
		if (value == null) {
			//
			result = null;

		} else {
			//
			if (NumberUtils.isDigits(value)) {
				//
				result = Integer.parseInt(value);

				if (result < 1) {
					//
					throw PuckExceptions.BAD_FILE_FORMAT.create("Order value [" + value + "] is not a positive integer.");
				}

			} else {
				//
				throw PuckExceptions.BAD_FILE_FORMAT.create("Order value [" + value + "] is not an integer.");
			}
		}

		//
		return result;
	}

	/**
	 * Loads a PUC file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	public static Net load(final File file) throws PuckException {
		Net result;

		ZipFile zipIn = null;
		try {
			//
			zipIn = new ZipFile(file);
			Enumeration<? extends ZipEntry> entries = zipIn.entries();

			//
			{
				ZipEntry entry = entries.nextElement();
				XMLReader xmlIn = new XMLReader(zipIn.getInputStream(entry));
				result = read(xmlIn);
				result.setLabel(file.getName());
				xmlIn.close();
			}

			//
			if (entries.hasMoreElements()) {

				ZipEntry entry = entries.nextElement();
				logger.debug("Second file detected in puc file [name={}][size={}].", entry.getName(), entry.getSize());

				if ((entry != null) && (StringUtils.endsWith(entry.getName(), ".geo.txt"))) {
					//
					BufferedReader geoIn = new BufferedReader(new InputStreamReader(zipIn.getInputStream(entry)));
					Geography geography = GEOTXTFile.readGeography(geoIn);
					result.setGeography(geography);
				}
			}
		} catch (UnsupportedEncodingException exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]:" + exception.getMessage(), exception);

		} catch (FileNotFoundException exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]: " + exception.getMessage(), exception);

		} catch (IOException exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.IO_ERROR.create("Error reading file [" + file + "]: " + exception.getMessage(), exception);

		} catch (XMLStreamException exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.BAD_FILE_FORMAT.create("Unexpected process error on [" + file + "]:" + exception.getMessage(), exception);

		} catch (XMLBadFormatException exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.BAD_FILE_FORMAT.create("Bad XML format in file [" + file + "]: " + exception.getMessage(), exception);

		} finally {
			//
			if (zipIn != null) {
				try {
					zipIn.close();
				} catch (IOException exception) {
					// Ignore.
					exception.printStackTrace();
				}
			}
		}

		//
		return result;
	}

	/**
	 * Reads a net from a XMLReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static Net read(final XMLReader in) throws PuckException, XMLStreamException, XMLBadFormatException {
		Net result;

		//
		result = new Net();

		//
		in.readXMLHeader();

		//
		in.readStartTag("corpus");

		//
		readAttributes(result.attributes(), in);

		//
		readIndividuals(result, in);

		//
		readFamilies(result, in);

		//
		readRelationModels(result, in);

		//
		readRelations(result, in);

		//
		in.readEndTag("corpus");

		//
		in.readXMLFooter();

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws PuckException
	 */
	public static void readActors(final Net net, final Relation relation, final XMLReader in) throws XMLStreamException, XMLBadFormatException, PuckException {

		// <xs:element name="actors" type="tns:Actors" />
		XMLTag list = in.readListTag("actors");

		//
		if (list.getType() != TagType.EMPTY) {

			while (in.hasNextStartTag("actor")) {

				// <xs:element name="actor" type="tns:Actor" />
				in.readStartTag("actor");

				//
				{
					// <xs:element name="role" type="xs:string" />
					String roleName = in.readContentTag("role").getContent();

					if (relation.getModel().roles().getByName(roleName) == null) {
						throw PuckExceptions.BAD_FILE_FORMAT.create("Undefined role [" + roleName + "] in relation model [" + relation.getModel().getName()
								+ "].");
					}

					// <xs:element name="individualId" type="tns:PuckId" />
					int individualId = Integer.parseInt(in.readContentTag("individualId").getContent());

					//
					Actor actor = net.createRelationActor(relation, individualId, roleName);

					// <xs:element name="relationOrder" type="puc:OrderValue"
					// nillable="true" minOccurs="0" />
					if (in.hasNextStartTag("relationOrder")) {
						//
						String relationOrderValue = in.readNullableContentTag("relationOrder").getContent();

						Integer relationOrder = convertOrderValue(relationOrderValue);

						actor.setRelationOrder(relationOrder);
					}

					// <xs:element name="attributes" type="tns:Attributes" />
					if (in.hasNextStartTag("attributes")) {

						readAttributes(actor.attributes(), in);
					}
				}

				//
				in.readEndTag("actor");
			}

			//
			in.readEndTag("actors");
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static void readAttributes(final Attributes target, final XMLReader in) throws XMLStreamException, XMLBadFormatException {

		// <xs:element name="attributes" type="tns:Attributes" />
		XMLTag list = in.readListTag("attributes");

		//
		if (list.getType() != TagType.EMPTY) {

			while (in.hasNextStartTag("attribute")) {

				// <xs:element name="attribute" type="tns:Attribute" />
				in.readStartTag("attribute");

				//
				{
					// <xs:element name="label" type="xs:token" />
					String label = in.readContentTag("label").getContent();

					// <xs:element name="value" type="xs:string" />
					String value = in.readNullableContentTag("value").getContent();
					if (value == null) {
						value = "";
					}

					//
					target.put(label, value);
				}

				//
				in.readEndTag("attribute");
			}

			//
			in.readEndTag("attributes");
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws PuckException
	 * @throws Exception
	 */
	public static void readFamilies(final Net target, final XMLReader in) throws XMLStreamException, XMLBadFormatException, PuckException {

		// <xs:element name="families" type="tns:Families" />
		XMLTag list = in.readListTag("families");

		//
		if (list.getType() != TagType.EMPTY) {

			while (in.hasNextStartTag("family")) {

				// <xs:element name="family" type="tns:Family" />
				XMLTag tag = in.readStartTag("family");
				int id = Integer.parseInt(tag.attributes().getByLabel("id").getValue());

				//
				{
					// <xs:element name="unionStatus" type="tns:UnionStatus
					String statusToken = in.readContentTag("unionStatus").getContent();
					UnionStatus status;
					if (StringUtils.equals(statusToken, "UNMARRIED")) {
						//
						status = UnionStatus.UNMARRIED;

					} else if (StringUtils.equals(statusToken, "MARRIED")) {
						//
						status = UnionStatus.MARRIED;

					} else if (StringUtils.equals(statusToken, "DIVORCED")) {
						//
						status = UnionStatus.DIVORCED;

					} else {
						//
						throw PuckExceptions.BAD_FILE_FORMAT.create("Unknown union status [" + statusToken + "]");
					}

					// <xs:element name="father" type="puc:PuckId"
					// nillable="true"
					XMLTag fatherTag = in.readNullableContentTag("father");

					Individual father;
					if (fatherTag.getContent() == null) {
						//
						father = null;

					} else {
						//
						int fatherId = Integer.parseInt(fatherTag.getContent());
						father = target.individuals().getById(fatherId);
						if (father == null) {
							//
							father = target.createIndividual(fatherId);
						}
					}

					// <xs:element name="fatherUnionOrder" type="puc:OrderValue"
					// nillable="true" minOccurs="0" />
					Integer fatherUnionOrder;
					if (in.hasNextStartTag("fatherUnionOrder")) {
						//
						String fatherUnionOrderValue = in.readNullableContentTag("fatherUnionOrder").getContent();

						fatherUnionOrder = convertOrderValue(fatherUnionOrderValue);

					} else {
						//
						fatherUnionOrder = null;
					}

					// <xs:element name="mother" type="puc:PuckId"
					// nillable="true"
					XMLTag motherTag = in.readNullableContentTag("mother");

					Individual mother;
					if (motherTag.getContent() == null) {
						//
						mother = null;

					} else {
						//
						int motherId = Integer.parseInt(motherTag.getContent());
						mother = target.individuals().getById(motherId);
						if (mother == null) {
							//
							mother = target.createIndividual(motherId);
						}
					}

					// <xs:element name="motherUnionOrder" type="puc:OrderValue"
					// nillable="true" minOccurs="0" />
					Integer motherUnionOrder;
					if (in.hasNextStartTag("motherUnionOrder")) {
						//
						String motherUnionOrderValue = in.readNullableContentTag("motherUnionOrder").getContent();

						motherUnionOrder = convertOrderValue(motherUnionOrderValue);

					} else {
						//
						motherUnionOrder = null;
					}

					//
					Family family = target.createFamily(id, father, mother, status);
					family.setHusbandOrder(fatherUnionOrder);
					family.setWifeOrder(motherUnionOrder);

					// <xs:element name="children" type="tns:PuckIds"
					XMLTag childrenTag = in.readNullableContentTag("children");

					if (childrenTag.getContent() != null) {
						//
						String[] childIds = childrenTag.getContent().split("[ ,;]");
						for (String childId : childIds) {
							//
							Individual child = target.individuals().getById(Integer.parseInt(childId));
							if (child == null) {
								target.createIndividual(id);
							}

							if (child.getOriginFamily() == null) {
								//
								child.setOriginFamily(family);
								family.getChildren().add(child);
							} else {
								//
								throw PuckExceptions.BAD_FILE_FORMAT.create("Child [" + child.getId() + "] defined in two different families ["
										+ child.getOriginFamily().getId() + "][" + family.getId() + "]");
							}
						}
					}

					// <xs:element name="attributes" type="tns:Attributes" />
					readAttributes(family.attributes(), in);
				}

				//
				in.readEndTag("family");
			}

			//
			in.readEndTag("families");
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws PuckException
	 * @throws Exception
	 */
	public static void readIndividuals(final Net target, final XMLReader in) throws XMLStreamException, XMLBadFormatException, PuckException {

		// <xs:element name="individuals" type="tns:Individuals" />
		XMLTag list = in.readListTag("individuals");

		//
		if (list.getType() != TagType.EMPTY) {

			while (in.hasNextStartTag("individual")) {

				// <xs:element name="individual" type="tns:Individual" />
				XMLTag tag = in.readStartTag("individual");
				int id = Integer.parseInt(tag.attributes().getByLabel("id").getValue());
				Individual individual = target.createIndividual(id);

				//
				{
					// <xs:element name="name" type="xs:normalizedString" />
					String name = in.readNullableContentTag("name").getContent();
					if (name == null) {
						name = "";
					}
					individual.setName(name);

					// <xs:element name="gender" type="tns:Gender" />
					String genderToken = in.readContentTag("gender").getContent();
					Gender gender;
					if (StringUtils.equals(genderToken, "MALE")) {
						//
						gender = Gender.MALE;

					} else if (StringUtils.equals(genderToken, "FEMALE")) {
						//
						gender = Gender.FEMALE;

					} else if (StringUtils.equals(genderToken, "UNKNOWN")) {
						//
						gender = Gender.UNKNOWN;

					} else {
						//
						throw PuckExceptions.BAD_FILE_FORMAT.create("Unknown gender [" + genderToken + "]");
					}
					individual.setGender(gender);

					// <xs:element name="birthOrder" type="puc:OrderValue"
					// nillable="true" minOccurs="0" />
					if (in.hasNextStartTag("birthOrder")) {
						//
						String birthOrderValue = in.readNullableContentTag("birthOrder").getContent();

						Integer birthOrder = convertOrderValue(birthOrderValue);

						individual.setBirthOrder(birthOrder);
					}

					// <xs:element name="attributes" type="tns:Attributes" />
					readAttributes(individual.attributes(), in);
				}

				//
				in.readEndTag("individual");
			}

			//
			in.readEndTag("individuals");
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws PuckException
	 */
	public static void readRelationModels(final Net target, final XMLReader in) throws XMLStreamException, XMLBadFormatException, PuckException {

		// <xs:element name="relationModels" type="tns:RelationModels" />
		XMLTag list = in.readListTag("relationModels");

		//
		if (list.getType() != TagType.EMPTY) {

			while (in.hasNextStartTag("relationModel")) {

				// <xs:element name="relationModel" type="tns:RelationModel" />
				in.readStartTag("relationModel");

				//
				{
					// <xs:element name="name" type="xs:token" />
					String name = in.readContentTag("name").getContent();

					RelationModel relationModel;
					if (target.relationModels().getByName(name) == null) {
						//
						relationModel = target.createRelationModel(name);

					} else {
						//
						throw PuckExceptions.BAD_FILE_FORMAT.create("Relation model [" + name + "] is defined more than once.");
					}

					// <xs:element name="roles" type="tns:Roles" />
					readRoles(relationModel.roles(), in);
				}

				//
				in.readEndTag("relationModel");

			}

			//
			in.readEndTag("relationModels");
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws PuckException
	 * @throws Exception
	 */
	public static void readRelations(final Net target, final XMLReader in) throws XMLStreamException, XMLBadFormatException, PuckException {

		// <xs:element name="relations" type="tns:Relations" />
		XMLTag list = in.readListTag("relations");

		//
		if (list.getType() != TagType.EMPTY) {

			while (in.hasNextStartTag("relation")) {

				// <xs:element name="relation" type="tns:Relation" />
				in.readStartTag("relation");

				//
				{
					// <xs:attribute name="id" type="tns:PuckId" />
					int typedId = Integer.parseInt(in.readContentTag("id").getContent());

					// <xs:element name="name" type="tns:token" />
					String name = in.readContentTag("name").getContent();

					// <xs:element name="model" type="puc:RelationModelName" />
					String relationModelName = in.readContentTag("model").getContent();
					RelationModel model = target.relationModels().getByName(relationModelName);
					if (model == null) {
						throw PuckExceptions.BAD_FILE_FORMAT.create("Undefined relation model [" + name + "].");
					}

					//
					Relation relation = target.createRelation(typedId, name, model);

					// <xs:element name="actors" type="tns:Actors" />
					readActors(target, relation, in);

					// <xs:element name="attributes" type="tns:Attributes" />
					readAttributes(relation.attributes(), in);
				}

				//
				in.readEndTag("relation");
			}

			//
			in.readEndTag("relations");
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws PuckException
	 */
	public static void readRoles(final Roles target, final XMLReader in) throws XMLStreamException, XMLBadFormatException, PuckException {

		// <xs:element name="roles" type="tns:Roles" />
		XMLTag list = in.readListTag("roles");

		//
		if (list.getType() != TagType.EMPTY) {

			while (in.hasNextStartTag("role")) {

				// <xs:element name="role" type="tns:Role" />
				in.readStartTag("role");

				{
					// <xs:element name="name" type="xs:NMTOKEN" />
					String name = in.readContentTag("name").getContent();

					if (target.getByName(name) == null) {
						target.add(new Role(name));
					} else {
						throw PuckExceptions.BAD_FILE_FORMAT.create("Role [" + name + "] is defined more than once.");
					}
				}
				//
				in.readEndTag("role");
			}

			//
			in.readEndTag("roles");
		}
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param targetFile
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File targetFile, final Net source, final String generator) throws PuckException {

		if ((targetFile == null) || (source == null)) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Null detected");

		} else {

			ZipOutputStream zipOut = null;
			try {
				//
				zipOut = new ZipOutputStream(new FileOutputStream(targetFile));
				zipOut.setLevel(Deflater.BEST_COMPRESSION);
				zipOut.setMethod(ZipOutputStream.DEFLATED);
				zipOut.setComment(generator);

				//
				zipOut.putNextEntry(new ZipEntry(FileTools.setExtension(targetFile, ".xml").getName()));
				XMLWriter xmlOut = new XMLWriter(zipOut);
				write(xmlOut, source, generator, targetFile.getName());
				xmlOut.flush();
				zipOut.closeEntry();

				//
				if ((source.getGeography() != null) && (!source.getGeography().isEmpty())) {
					//
					zipOut.putNextEntry(new ZipEntry(FileTools.setExtension(targetFile, ".geo.txt").getName()));
					PrintWriter geoOut = new PrintWriter(new OutputStreamWriter(zipOut, DEFAULT_CHARSET_NAME));
					GEOTXTFile.writeGeography(geoOut, source.getGeography());
					geoOut.flush();
					zipOut.closeEntry();
				}
			} catch (IOException exception) {
				//
				exception.printStackTrace();
				throw PuckExceptions.IO_ERROR.create("Error saving file [" + targetFile + "]", exception);

			} finally {
				try {
					if (zipOut != null) {
						zipOut.flush();
						zipOut.close();
					}
				} catch (IOException subException) {
					subException.printStackTrace();
					// Ignore.
				}
			}

			// FIXME
			try {
				System.out.println("XSD validation result: " + XMLTools.isValid(targetFile, PUCFile.class.getResourceAsStream(XSD_FILE)));
			} catch (SAXException exception) {
				// TODO Auto-generated catch block
				exception.printStackTrace();
			} catch (IOException exception) {
				// TODO Auto-generated catch block
				exception.printStackTrace();
			}
		}
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 * @throws PuckException
	 */
	public static void write(final XMLWriter out, final Net source, final String generator, final String sourceFileName) {

		out.writeXMLHeader();
		out.writeStartTag("corpus", "xmlns", "urn:schema:PUCK/" + PUCK_FORMAT_VERSION, "xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance", "version",
				"PUCK-" + PUCK_FORMAT_VERSION, "generator", "PUCK", "date", DatatypeConverter.printDateTime(new GregorianCalendar()), "filename",
				sourceFileName);

		// <xs:element name="attributes" type="tns:Attributes" />
		writeAttributes(out, source.attributes());

		// <xs:element name="individuals" type="tns:Individuals" />
		writeIndividuals(out, source.individuals());

		// <xs:element name="families" type="tns:Families" />
		writeFamilies(out, source.families());

		// <xs:element name="relationModels" type="tns:RelationModels" />
		writeRelationModels(out, source.relationModels());

		// <xs:element name="relations" type="tns:Relations" />
		writeRelations(out, source.relations());

		out.writeEndTag("corpus");
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void writeActors(final XMLWriter out, final Actors source) {

		if ((source == null) || (source.isEmpty())) {
			// <xs:element name="actors" type="tns:Actors" />
			out.writeEmptyTag("actors");
		} else {
			// <xs:element name="actors" type="tns:Actors" />
			out.writeStartTag("actors", "size", String.valueOf(source.size()));

			for (Actor actor : source) {
				// <xs:element name="actor" type="tns:Actor" />
				out.writeStartTag("actor");

				//
				{
					// <xs:element name="role" type="xs:string" />
					out.writeTag("role", actor.getRole().getName());

					// <xs:element name="individualId" type="tns:PuckId" />
					out.writeTag("individualId", String.valueOf(actor.getIndividual().getId()));

					// <xs:element name="birthOrder" type="puc:OrderValue"
					// nillable="true" minOccurs="0" />
					if (actor.getRelationOrder() == null) {
						//
						out.writeTag("relationOrder", null, "xsi:nil", "true");

					} else {
						//
						out.writeTag("relationOrder", String.valueOf(actor.getRelationOrder()));
					}

					// <xs:element name="attributes" type="tns:Attributes" />
					writeAttributes(out, actor.attributes());
				}

				//
				out.writeEndTag("actor");
			}

			//
			out.writeEndTag("actors");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void writeAttributes(final XMLWriter out, final Attributes source) {

		if ((source == null) || (source.isEmpty())) {
			// <xs:element name="attributes" type="tns:Attributes" />
			out.writeEmptyTag("attributes");
		} else {
			// <xs:element name="attributes" type="tns:Attributes" />
			out.writeStartTag("attributes", "size", String.valueOf(source.size()));

			//
			for (Attribute attribute : source) {
				// <xs:element name="attribute" type="tns:Attribute" />
				out.writeStartTag("attribute");

				//
				{
					// <xs:element name="label" type="xs:token" />
					out.writeTag("label", attribute.getLabel());

					// <xs:element name="value" type="xs:string" />
					out.writeTag("value", attribute.getValue());
				}

				//
				out.writeEndTag("attribute");
			}

			//
			out.writeEndTag("attributes");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static void writeFamilies(final XMLWriter out, final Families source) {

		if ((source == null) || (source.isEmpty())) {
			// <xs:element name="families" type="tns:Families" />
			out.writeEmptyTag("families");
		} else {
			// <xs:element name="families" type="tns:Families" />
			out.writeStartTag("families", "size", String.valueOf(source.size()));

			//
			for (Family family : source) {
				// <xs:element name="family" type="tns:Family" />
				out.writeStartTag("family", "id", String.valueOf(family.getId()));

				//
				{
					// <xs:element name="unionStatus" type="tns:UnionStatus
					String unionStatusString;
					switch (family.getUnionStatus()) {
						case UNMARRIED:
							unionStatusString = "UNMARRIED";
						break;
						case MARRIED:
							unionStatusString = "MARRIED";
						break;
						case DIVORCED:
							unionStatusString = "DIVORCED";
						break;
						default:
							unionStatusString = family.getUnionStatus().name();
					}
					out.writeTag("unionStatus", unionStatusString);

					// <xs:element name="father" type="tns:PuckId"
					if (family.getFather() == null) {
						//
						out.writeTag("father", null, "xsi:nil", "true");

					} else {
						//
						out.writeTag("father", String.valueOf(family.getFather().getId()));
					}

					// <xs:element name="fatherUnionOrder" type="puc:OrderValue"
					// nillable="true" minOccurs="0" />
					if (family.getHusbandOrder() == null) {
						//
						out.writeTag("fatherUnionOrder", null, "xsi:nil", "true");

					} else {
						//
						out.writeTag("fatherUnionOrder", String.valueOf(family.getHusbandOrder()));
					}

					// <xs:element name="mother" type="tns:PuckId"
					if (family.getMother() == null) {
						//
						out.writeTag("mother", null, "xsi:nil", "true");

					} else {
						//
						out.writeTag("mother", String.valueOf(family.getMother().getId()));
					}

					// <xs:element name="motherUnionOrder" type="puc:OrderValue"
					// nillable="true" minOccurs="0" />
					if (family.getWifeOrder() == null) {
						//
						out.writeTag("motherUnionOrder", null, "xsi:nil", "true");

					} else {
						//
						out.writeTag("motherUnionOrder", String.valueOf(family.getWifeOrder()));
					}

					// <xs:element name="children" type="tns:PuckIds"
					StringList ids = new StringList();
					for (Individual individual : family.getChildren()) {
						//
						ids.add(String.valueOf(individual.getId()));
						ids.add(" ");
					}
					if (!ids.isEmpty()) {
						//
						ids.removeLast();
					}

					if (ids.isEmpty()) {
						//
						out.writeEmptyTag("children");

					} else {
						//
						out.writeTag("children", ids.toString());
					}

					// <xs:element name="attributes" type="tns:Attributes"
					writeAttributes(out, family.attributes());
				}

				//
				out.writeEndTag("family");
			}

			//
			out.writeEndTag("families");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static void writeIndividuals(final XMLWriter out, final Individuals source) {

		if ((source == null) || (source.isEmpty())) {
			// <xs:element name="individuals" type="tns:Individuals" />
			out.writeEmptyTag("individuals");

		} else {
			// <xs:element name="individuals" type="tns:Individuals" />
			out.writeStartTag("individuals", "size", String.valueOf(source.size()));

			for (Individual individual : source) {
				// <xs:element name="individual" type="tns:Individual" />
				out.writeStartTag("individual", "id", String.valueOf(individual.getId()));

				//
				{
					// <xs:element name="name" type="xs:normalizedString" />
					out.writeTag("name", individual.getName());

					// <xs:element name="gender" type="tns:Gender" />
					String gender;
					switch (individual.getGender()) {
						case FEMALE:
							gender = "FEMALE";
						break;

						case MALE:
							gender = "MALE";
						break;

						case UNKNOWN:
							gender = "UNKNOWN";
						break;

						default:
							gender = individual.getGender().name();
					}
					//
					out.writeTag("gender", gender);

					// <xs:element name="birthOrder" type="puc:OrderValue"
					// nillable="true" minOccurs="0" />
					if (individual.getBirthOrder() == null) {
						//
						out.writeTag("birthOrder", null, "xsi:nil", "true");

					} else {
						//
						out.writeTag("birthOrder", String.valueOf(individual.getBirthOrder()));
					}

					// <xs:element name="attributes" type="tns:Attributes" />
					writeAttributes(out, individual.attributes());
				}

				//
				out.writeEndTag("individual");
			}

			//
			out.writeEndTag("individuals");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void writeRelationModels(final XMLWriter out, final RelationModels source) {

		if ((source == null) || (source.isEmpty())) {
			// <xs:element name="relationModels" type="tns:RelationModels" />
			out.writeEmptyTag("relationModels");

		} else {
			// <xs:element name="relationModels" type="tns:RelationModels" />
			out.writeStartTag("relationModels", "size", String.valueOf(source.size()));

			for (RelationModel relationModel : source) {
				// <xs:element name="relationModel" type="tns:RelationModel" />
				out.writeStartTag("relationModel");

				//
				{
					// <xs:element name="name" type="xs:token" />
					out.writeTag("name", relationModel.getName());

					// <xs:element name="roles" type="tns:Roles" />
					writeRoles(out, relationModel.roles());
				}

				//
				out.writeEndTag("relationModel");
			}

			//
			out.writeEndTag("relationModels");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void writeRelations(final XMLWriter out, final Relations source) {

		if ((source == null) || (source.isEmpty())) {
			// <xs:element name="relations" type="tns:Relations" />
			out.writeEmptyTag("relations");

		} else {
			// <xs:element name="relations" type="tns:Relations" />
			out.writeStartTag("relations", "size", String.valueOf(source.size()));

			for (Relation relation : source) {
				// <xs:element name="relation" type="tns:Relation" />
				out.writeStartTag("relation");

				//
				{
					// <xs:attribute name="id" type="tns:PuckId" />
					// The relation id is the id about all relations.
					// The relation typeId is the id for a specific relation
					// model.
					// Only the typeId is useful to store.
					out.writeTag("id", String.valueOf(relation.getTypedId()));

					// <xs:element name="name" type="tns:token" />
					out.writeTag("name", relation.getName());

					// <xs:element name="relationModel" type="tns:token" />
					out.writeTag("model", relation.getModel().getName());

					// <xs:element name="actors" type="tns:Actors" />
					writeActors(out, relation.actors());

					// <xs:element name="attributes" type="tns:Attributes" />
					writeAttributes(out, relation.attributes());
				}

				//
				out.writeEndTag("relation");
			}

			//
			out.writeEndTag("relations");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void writeRoles(final XMLWriter out, final Roles source) {

		if ((source == null) || (source.isEmpty())) {
			// <xs:element name="roles" type="tns:Roles" />
			out.writeEmptyTag("roles");

		} else {
			// <xs:element name="roles" type="tns:Roles" />
			out.writeStartTag("roles", "size", String.valueOf(source.size()));

			//
			for (Role role : source) {
				// <xs:element name="role" type="tns:Role" />
				out.writeStartTag("role");

				//
				{
					// <xs:element name="name" type="xs:NMTOKEN" />
					out.writeTag("name", role.getName());
				}

				//
				out.writeEndTag("role");
			}

			//
			out.writeEndTag("roles");
		}
	}
}
