package org.tip.puck.net.workers;

import org.tip.puck.net.FiliationType;

/**
 * 
 * @author TIP
 */
public class ExpandCriteria {

	private String subTitle;
	private ExpansionMode expansionMode;
	private FiliationType filiationType;
	private int maxStep;

	/**
	 * 
	 */
	public ExpandCriteria() {

		this.subTitle = "";
		this.expansionMode = ExpansionMode.NONE;
		this.filiationType = null;
		this.maxStep = 0;
	}

	public ExpansionMode getExpansionMode() {
		return this.expansionMode;
	}

	public FiliationType getFiliationType() {
		return this.filiationType;
	}

	public int getMaxStep() {
		return this.maxStep;
	}

	public String getSubTitle() {
		return this.subTitle;
	}

	public void setExpansionMode(final ExpansionMode expansionMode) {
		this.expansionMode = expansionMode;
	}

	public void setFiliationType(final FiliationType filiationType) {
		this.filiationType = filiationType;
	}

	public void setMaxStep(final int maxStep) {
		this.maxStep = maxStep;
	}

	public void setSubTitle(final String subTitle) {
		this.subTitle = subTitle;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public static boolean isNotValid(final ExpandCriteria criteria) {
		boolean result;

		result = !isValid(criteria);

		//
		return result;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public static boolean isValid(final ExpandCriteria criteria) {
		boolean result;

		if ((criteria == null) || (criteria.getSubTitle() == null) || (criteria.getExpansionMode() == ExpansionMode.NONE) || (criteria.getMaxStep() < 0)) {
			//
			result = false;

		} else {
			//
			result = true;
		}

		//
		return result;
	}

}
