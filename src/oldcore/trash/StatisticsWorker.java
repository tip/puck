package oldcore.trash;

import static oldcore.trash.Mat.percent;
import static oldcore.trash.Trafo.asString;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;

import oldcore.calc.partitions.Cluster;

import org.tip.puck.PuckException;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Attributes;
import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;


/**
 * 
 * @author TIP
 */
public class StatisticsWorker {
	
	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 *
	 * @deprecated MIGRATED
	 */
        @Deprecated
/*	public Report reportBasicInformation() throws PuckException {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Basic statistis about a corpus.");
		result.setOrigin("Statistics worker");
		result.setTarget(net.getLabel());
		result.setInputComment("No input data.");

		//
		result.outputs().add("individuals", net.individuals().size());
		result.outputs().add("men", net.individuals().size(Gender.MALE));
		result.outputs().add("women", net.individuals().size(Gender.FEMALE));
		result.outputs().add("unknown", net.individuals().size(Gender.UNKNOWN));

		//
		result.outputs().add("mariage relation", "XXX");
		result.outputs().add("mariage relation of men", "XXX");
		result.outputs().add("mariage relation of women", "XXX");

		//
		{
			int fertileCount = net.families().fertileCount();
			double percentage = MathUtils.percent(fertileCount, net.families().size());
			result.outputs().add("fertile marriages", String.format("%d (%.2f%%)", fertileCount, percentage));
		}

		//
		result.outputs().add("co-wife relations", "XXX");
		result.outputs().add("co-husband relations", "XXX");
		result.outputs().add("components", "XXX (max: YYY)");
		result.outputs().add("component share (agnatic)", "mean XXX, max YYY (marr. ZZZ)");
		result.outputs().add("component share (uterine)", "mean XXX, max YYY (marr. ZZZ)");
		result.outputs().add("elementary cycles", "XXX");
		result.outputs().add("density", "XXX");
		result.outputs().add("depth", net.depth());
		result.outputs().add("mean depth", mean(-1,"MDEPTH",false));
		result.outputs().add("mean generational distance of spouses", "XXX");
		result.outputs().add("mean spouse number of men", "XXX");
		result.outputs().add("mean spouse number of women", "XXX");
		result.outputs().add("mean fratry size agnatic", "XXX");
		result.outputs().add("mean fratry size uterine", "XXX");

		//
		result.setOutputComment("This worker is under construction.");

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	} */
	
//	private Net net;
	
	
/*	public StatisticsWorker (Net net){
		this.net = net;
	}*/
	   
	// should return the translation
/*	private static String t (String originalString){
		return originalString;
	}*/
	
/*	public Report genderBiasReport() throws PuckException {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		
		result.setTitle(t("Gender bias")+" ("+t("weight")+").");
		String[] headline = {"U","A","A "+t("and")+" U",t("difference")};
		result.inputs().add(t("Generation"), headline);

		result.setOrigin("Statistics worker");
		result.setTarget(net.getLabel());
		result.setInputComment("...");

		double[][] bias = bias(0);
		
		for (int i=0;i<bias[0].length;i++){
			String[] values = new String[4];
			for (int j=0;j<3;j++){
				values[j]= String.valueOf(bias[i][j]); 
			}
			values[4]=String.valueOf(Math.round(100.*(bias[0][i]-bias[1][i]))/100.);
			result.outputs().add(i+"", values);
		}
		//
		result.setOutputComment("This worker is under construction.");

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	} 
		
	public Report netGenderBiasReport() throws PuckException {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		
		result.setTitle(t("Gender bias")+" ("+t("net weight")+").");
		String[] headline = {"U "+t("not")+" A","A "+t("not")+" U",t("difference")};
		result.inputs().add(t("Generation"), headline);

		result.setOrigin("Statistics worker");
		result.setTarget(net.getLabel());
		result.setInputComment("...");

		double[][] bias = bias(2);
		
		for (int i=0;i<bias[0].length;i++){
			String[] values = new String[3];
			for (int j=0;j<2;j++){
				values[j]= String.valueOf(bias[i][j]); 
			}
			values[3]=String.valueOf(Math.round(100.*(bias[0][i]-bias[1][i]))/100.);
			result.outputs().add(i+"", values);
		}
		//
		result.setOutputComment("This worker is under construction.");

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}*/ 
		
	   	   
	/**
	 * computes the agnatic and uterine weight or netweight of the Net (for degrees < 10) and adds the results to a protocol
	 * @param mode the parameter for weight (0) and netweight (1) calculus
	 * @return the array of agnatic and uterine weight or net weight
	 * @see gui.screens.StatsScreen#show(String[])
	 */
/*	public double[][] bias (int mode) {
	

		int[][] b = new int[10][3];
		Partition<Individual>[] part = new Partition[3];
		part[0] = new Partition<Individual>(net, "MATRID");
		part[1] = new Partition<Individual>(net, "PATRID");
	
		for (Individual v : net.getFinalOrigin().individuals()) {
//			if (v.ignore()) continue;
			int[] d = new int[3];
			for (int i=0;i<2;i++) {//<3
				try {
					d[i] = Math.min(9, (Integer)part[i].getVal(v));
				} catch (NullPointerException npe) {
					d[i] = 0;
				}
	         }
	         d[2] = Math.min(d[0],d[1]);
	         for (int i=0;i<3;i++) {
	            for (int j = 0;j<d[i];j++){
	                b[j+1][i]++;
	            }
	         }
		}
		double[][] bias = new double[3][10];
		double s = 50.;
		if (mode == 1) s = 0.0;
		bias[0][0]=50.; 
		for (int j=0;j<3;j++) {
			bias[j][0]=s;
			for (int i=1;i<10;i++) {
				int c = b[i][0]+b[i][1]-b[i][2];
				if (mode==0) bias[j][i]=percent(b[i][j],c);
				else if (mode==1) bias[j][i]=percent(b[i][j]-b[i][2],c);//b[i][j]
				if ((j<2) || (i==0)) continue;
				if (bias[0][i]+bias[1][i]==0) continue;
	          }
	      }
	      return bias;
	}*/

	   /**
	 * computes the average genealogical completeness of the vertices of the Net and prints it in a protocol
	 * @param stat the string list used as a protocol
	 * @param n the maximal number of ascending generations to be considered 
	 * @return an array containing the average agnatic, uterine and cognatic genealogical completeness
	 */
/*	public double[][] completeness (ArrayList<String> stat, int n){
		int s = 0;
		int[][] ascendants = new int[3][n];
		double[][] comp = new double[3][n+1];
		Map<Integer,int[][]> ascendantCount = ascendantCount(n);
		
		for (Individual v: net.individuals()){
			if (!v.isSterile()) continue;
			s++;
			int[][] count = ascendantCount.get(v.getId());
			for (int i=0;i<n;i++){
				for (int g=0;g<3;g++){
					ascendants[g][i]+=count[g][i];
				}
			}
		}
		for (int i=1;i<n+1;i++){
			if (ascendants[0][i-1]==0) continue;
			comp[0][i] = percent(ascendants[0][i-1],s*pow2(i));
			for (int g=1;g<3;g++){
				comp[g][i] = percent(ascendants[g][i-1],s);
			}
			stat.add(i+"\t"+comp[0][i]+"\t"+comp[1][i]+"\t"+comp[2][i]);
		}
		return comp;
	} */ 
	   
	/**
	 * gets the distribution of agnatic and uterine components for variable size (as an array containing the relative number of components for ascending component shares) 
	 * @param k the fineness of the distribution (number of distinct columns)
	 * @return an array containing the relative number of components (as a percentage of total components) for ascending component shares (component size as a percentage of network size)
	 */
/*   public double[][] components (int k) {
	   int[][] b = new int[2][k];
	   double[][] comp = new double[2][k];
	   Partition<Individual>[] p = new Partition[2];
	   p[0] = new Partition<Individual> (net, "PATRIC");
	   p[1] = new Partition<Individual> (net, "MATRIC");
	   for (int i=0;i<2;i++){
		   int n = p[i].size();
		   for (Cluster<Individual> c : p[i]) {
			   int s = p[i].share(c,k);              // simplify
			   b[i][s]=b[i][s]+1;
		   }  
		   for (int j=0;j<k;j++) {
			   comp[i][j]=percent(b[i][j],n);
		   }
	   }
	   return comp;
   } */

		/**
		 * computes the size of the component for a given root vertex 
		 * @param root the root vertex
		 * @return the size of the component
		 * @see OldNet#getNrComponents()
		 *
	     * @deprecated MIGRATED to StatisticsWorker#componentSize(Individual)
	     */
/*		private int componentSize (Individual root){
		   Stack<Individual> stack = new Stack<Individual>();
		   Set<Individual> visited = new HashSet<Individual>();
		   
		   
		   int n = 1;
		   visited.add(root);
		   stack.push(root);
		   while (!stack.isEmpty()) {
			   Individual v = stack.pop();
			   for (int i=-1;i<2;i++){
				   if (v.getKin(i)==null) continue;
				   for (Individual w : v.getKin(i)){
					   if (w==null) continue;
					   if (visited.contains(w)) continue;
					   visited.add(w);
					   n++;
					   stack.push(w);
				   }
			   }
		   }
		   return n;
		}*/
		

	   
	   
	//error types
	   /*
	    * checks for possible undefinedRoles (unusual features) of a given type in the dataset (as specified in the options) and counts them 
	    * @param k the index of the type of possible error
	    * @return the number of unusual features in the dataset
	    * @see maps.Net#control()
	    * @deprecated MIGRATED to ControlReporter#reportControls
	    */
	   @Deprecated
/*	   private int control (ArrayList<String> protocol, int k) {
		   
		  int n = protocol.size();
	      int i = 0;
	      protocol.add("");
	      for (Individual v : net.individuals()) {
	         if (k==3) {
	            if ((v.getGender()==Gender.MALE) || (v.getGender()==Gender.FEMALE)) continue;
	            protocol.add(v.getName().trim() + " (" + v.getId() + ")");
	            i++;
	            continue;
	         }
	
	         if (k==4) {
	            if (v.getName()!=null) {
	               if (v.getName().length()>0) continue;
	            }
	            protocol.add(v.getGender().toChar() + " " + v.getId());
	            i++;
	            continue;
	         }
	
	         if (k==2) {
	            int h=protocol.size();
	            checkCycles(protocol,v,5);
	            i=i+protocol.size()-h;
	            continue;
	         }
	
	         if (k==5) {
	        	 for (Individual w: v.getParents()) {
	        		 if (v.getSpouses()==null) continue;
	        		 if (v.getSpouses().contains(w)) {
	        			 protocol.add(v.getName().trim() + " (" + v.getId() + ")"+" "+t("and")+" "+w.getName().trim() + " (" + w.getId() + ")");
	        			 i++;
	        		 }
	        	 }
				 continue;
	         }
	         if (k == 0) {
	        	 if (v.getSpouses()==null) continue;
	        	 for (Individual spouse : v.getSpouses()){
	        		 if (spouse == null) continue;
	        		 if (v.getGender().equals(spouse.getGender())) {
	        			 protocol.add(spouse.getGender().toChar()+" "+spouse.getName().trim() + " (" + spouse.getId() + ")"+"\t"+t("for")+" "+v.getName().trim() + " (" + v.getId() + ")");
		        		 i++;
	        		 }
	        	 }
	         }
	         if (k == 1) {
	        	 Individual father = v.getFather();
	        	 if (father!=null && father.getGender().isFemale()) {
	        		 protocol.add(father.getGender().toChar()+" "+father.getName().trim() + " (" + father.getId() + ")"+"\t"+t("for")+" "+v.getName().trim() + " (" + v.getId() + ")");
		        		 i++;
	        	 }
	        	 Individual mother = v.getMother();
	        	 if (mother!=null && father.getGender().isMale()) {
	        		 protocol.add(mother.getGender().toChar()+" "+mother.getName().trim() + " (" + mother.getId() + ")"+"\t"+t("for")+" "+v.getName().trim() + " (" + v.getId() + ")");
		        		 i++;
	        	 }
	         }
	      }       
	      
	      String[] unusual = {t("Unknown Relatives"),t("Same-Sex Spouses"),t("Female Fathers or Male Mothers"),t("Cases of Cyclic Descent"),t("Persons of unknown Sex"),t("Nameless Persons"),t("Parent-Child Marriages")};

	
	      if (i==0) return i;
	      protocol.set(n,i+" "+unusual[k+1]);
	      protocol.add("");
	      return i;
	   }*/
		
	//check for meaning of density (-1,1?) cf. nrArcs(int,int)
	   /**
	    * computes the density of a Net for given degree and prints it in a protocol
	    * @param stat the string list used as a protocol
	    * @param dmax the maximal (canonic) degree for which consanguineous density shall be computed 
	    * @return an array containing the agnatic, uterine and cognatic density of the Net for degrees up to the given maximum
	    * @see gui.screens.StatsScreen#show(String[])
	    */
/*	   public double[][] density (ArrayList<String> stat, int dmax){
			String kinMode[] = {t("Overall"),t("Agnatic"),t("Uterine")};
			double[][] dens = new double[3][dmax];
			for (int i=-1;i<2;i++){
			     stat.add(kinMode[i+1]);
				 int[] arcs = nrRelations(dmax,i);
				 for (int j=2;j<dmax+2;j++) {
					 dens[1-i][j-2]=density(arcs)[j];
					 stat.add(arcs[j]+" "+relationType(j)+"\t"+t("density")+": "+dens[1-i][j-2]);
				 }
			}
			return dens;
		}*/
	   
	   /**
	    * gets the label of the relation type for a given integer code k
	    * <p> 0 marriage, 1 parent-child, 2 and above consanguinity of degree k-1 
	    * @param k the relation type parameter
	    * @return the label of the relation type
	    * @see Net#density(ArrayList, int)
	    */
/*	   private static String relationType (int k){
	   	if (k==1) return t("marriage relations");
	   	if (k==2) return t("parent relations");
	   	return t("consanguineous relations")+" "+t("of degree")+" "+k;
	   }*/
	
	   /**
	    * computes the density of the Net, given the number of lines (i.e., the line number as a percentage of possible vertex pairs) 
	    * @param lines the number of lines
	    * @return the density of the Net as a function of the number of lines
	    * @see OldNet.CopyOfNet#getIdentityCard()
	    *
	    * @deprecated MIGRATED to StatisticsWorker#density(Net,lines)
	    */
/*	   private double density (int lines){
		   return Mat.percent(lines,net.size()*(net.size()-1));
	   }*/
	
	/**
	    * computes the density of a Net, given variable numbers of lines
	    * <p> the first density returned is computed as the basis of heterosexual vertex pairs, all others on the basis of ungendered vertex pairs
	    * @param arcs the array of line numbers
	    * @return the array of densities
	    * @see OldNet.CopyOfNet#density(ArrayList, int)
	    */
/*	   private double[] density (int[] arcs){
		   double[] dens = new double[arcs.length];
		   dens[0] = percent(arcs[0],nrGender()[0]*nrGender()[1]);
		   int vol = net.size()*(net.size()-1);
		   for (int i=1;i<dens.length;i++){
			   dens[i]= Mat.percent(arcs[i],vol);
		   }
		   return dens;
	   }*/

	/**
	    * computes the fratry distribution of a Net and prints it in a protocol
	    * @param stat the string list used as a protocol
	    * @return the array of frequencies of agnatic and uterine fratries of given size
	    * @see gui.screens.StatsScreen#show(String[])
	    * @since 10/04/18, last modified 10/06/18
	    */
/*	   public double[][] fratryDistribution (ArrayList<String> stat){
	       Map<Integer,Integer> map1 = new Partition<Individual>(net, "SIBSETM").getClusterSizes();
	       Map<Integer,Integer> map2 = new Partition<Individual>(net,"SIBSETP").getClusterSizes();
	       int max = Math.max(((TreeMap<Integer,Integer>)map1).lastKey(), ((TreeMap<Integer,Integer>)map2).lastKey());
		   double[][] dist = new double[2][max];
		   stat.add(t("size")+"\t"+t("uterine")+"\t"+t("agnatic"));
		   double[] sum = new double[2];
	       for (int k=0;k<max;k++){
	    	   Integer[] a = {0,0};
	    	   if (map1.get(k)!=null) a[0] = map1.get(k); 
	    	   if (map2.get(k)!=null) a[1] = map2.get(k); 
	    	   stat.add(k+"\t"+a[0]+"\t"+a[1]);
	    	   for (int j=0;j<2;j++){
	    		   dist[j][k] = a[j].doubleValue();
	    		   sum[j] = sum[j]+dist[j][k];
	    	   }
	       }
	       for (int k=0;k<max;k++){
	    	   for (int j=0;j<2;j++){
	    		   if (k>1) dist[j][k] = 100*dist[j][k]/sum[j];
	    		   else dist[j][k] = 0;
	    	   }
	       }
	       return dist;
	   }*/
	

	   

	   /**
	    * gets the map of mean generational levels of clusters)
	    * @param cmd the partition command
	    * @return the map of mean generational levels
	    * @since 10-11-06
	    */
/*	   public double[][] getMeanClusterLevels (String cmd, ArrayList<String> stat){
		   Partition<Individual> p = new Partition<Individual>(net,cmd);
		   Partition<Individual> generations = new PartitionMaker(net).makeGenerations();
		   int n = p.size();
		   double[][] d = new double[2][n];
		   stat.add(t("Mean cluster levels")+"\n");
		   stat.add(t("Cluster")+"\t"+t("Name")+"\t"+t("Mean level")+"\t"+t("Size")+"\n");
		   for (int i=0;i<n;i++){
			   Cluster<Individual> clu = p.get(i);
			   d[0][i]=clu.getMeanLevel(generations);
			   d[1][i]=clu.size();
			   stat.add(i+"\t"+asString(clu.getVal())+"\t"+d[0][i]+" "+d[1][i]);
		   }
		   stat.add("\n");
		   return d;
	   */
	   
	   

	   /**
	    * gets the mean generational distance between spouses
	    * @return the mean generational distance between spouses
	    * Migrated to StatisticsWorker
	    */
/*	   public double getMeanGenerationalDistance(){
		   Partition<Individual> generations = new PartitionMaker(net).makeGenerations();
		   int distanceSum = 0;
		   for (Family family : net.families()){
			   int husbandGeneration = (Integer)generations.getVal(family.getHusband());
			   int wifeGeneration = (Integer)generations.getVal(family.getWife());
			   distanceSum += Math.abs(husbandGeneration - wifeGeneration);
		   }
		   return Math.round(100.*(new Double(distanceSum)/new Double(net.families().size())))/100.;
	   }*/
	   
	   /**
	 * gets the number of (cognatic) components as well as the size of the maximal component
	 * @return an integer array containing the number of components as first element and the size of the maximal component as second element
	 * @see OldNet#getIdentityCard()
	 *
	 * @deprecated MIGRATED to StatisticsWorker#getNumberOfComponents
	  */
/*	   private int[] getNrComponents(){
		   int[] c = new int[2];
		   Set<Individual> visited = new HashSet<Individual>();
		   for (Individual v: net.individuals()) {
			   if (visited.contains(v)) continue;
			   c[0]++;
			   c[1]=Math.max(c[1],componentSize(v));
		   }
		   return c;
	   }*/

	   //Simplify (including submethods)
	   //Correct for gender condition and restriction code!
	   /**
	    * gets the number of nodes of the Net who have spouses in the Net
	    * @param g the gender of the vertex (-1 if any gender is allowed)
	    * @param r the restriction code
	    * @return the number of nodes of the network who have spouses in the network
	    * @see gui.TipWriter#writeSurvey(RingGroupMap, CountMap<int>,int)
	    */
/*	   public int getNrMarried (int g, int r){
		   if (r!=1) return size(g,true);
		   int s = 0;
		   for (Individual v : net.getFinalOrigin().individuals()){
			   if (wrongSex(v,g) || v.isSingle()) continue; 
			   if (net.contains(v.getId()) || net.containsSpouse(v)) {
				   s++;
				   continue;
			   }
		   }
		   return s;
	   }*/
	   
       /**
	    * @deprecated MIGRATED to Gender#matchs(Gender)
	    */
/*	   private static boolean wrongSex (Individual v, int key){
		   boolean result;
		   if (key > 1) {
			   result = false;
		   } else {
			   result = (v.getGender().toInt()!=key);
		   }
		   
		   //
		   return result;
	   }*/
	   
   
	   
	   //Simplify
	   /**
	    * computes the mean generational depth of the network, as an average of the (mean) generational depths of its "zero generation" vertices
	    * @param g the required gender of the vertex
	    * @param lb "MDEPTH" if the average of mean generational depths shall be computed
	    * @param positive if zero values are not to be considered
	    * @see elements.nodes.NewIndividual#meanDepth()
	    * @see OldNet.CopyOfNet#getIdentityCard()
	    * 
	    * @deprecated MIGRATED
	    */
/*	    private double mean (int g, String lb, boolean positive){
		   double h = 0.;
		   int n = 0;
		   for (Individual v : net.individuals()){
			   if (wrongSex(v,g)) continue; 
			   if (lb.equals("MDEPTH") && !v.isSterile()) continue;
			   Object c = v.getAttributeValue(lb);
			   if (positive && (Integer)c==0) continue;
			   if (c instanceof Double) h=h+(Double)c;
			   else h = h+((Integer)c).doubleValue();
			   n++;
		   }
		   return Mat.percent(h/100,n);
	   }*/
	   
	   // replace old CountConsanguines-Method?
	   /**
	    * gets the number of consanguineous collateral ties up to a given canonic degree (where degree = 0 gives the number of parent-child relations)
	    * @param dmax the maximal canonic degree
	    * @param g the filter criterion for ascendant lines (0 agnatic, 1 uterine, 2 cognatic) 
	    * @return the array of numbers of lines for each degree up to dmax
	    * @see OldNet.CopyOfNet#density(ArrayList, int)
	    * @see OldNet.CopyOfNet#getIdentityCard()
	    */
/*	   private int[] nrRelations (int dmax, int g){
		   int[] relations = new int[dmax+2];
		   for (Individual v: net.individuals()){
//			   if (v.ignore()) continue;
			   relations[0] = relations[0]+v.getSpouses().size();
			   if (dmax==0) {
				   if ((g==-1 || g==0) && v.getFather()!=null) relations[1]++;
				   if ((g==-1 || g==1) && v.getMother()!=null) relations[1]++;
				   continue;
			   }
			   int[] cons = getNrConsanguines(v,dmax, g);
			   for (int i=1;i<dmax+2;i++){
				   relations[i]=relations[i]+cons[i-1];
			   }
		   }
		   for (int i=0;i<dmax+2;i++){
			   if (i!=1) relations[i]=relations[i]/2;
		   }
		   return relations;
	   }*/
	   
	   
	   /**
	    * counts the number of attributes and attribute-bearing vertices (male and female) of a given label in a Net 
	    * <p> used for counting relations other than marriage
	    * @param label the label of the attribute
	    * @param sym true if the attribute is a symmetric relation
	    * @return the array of number of attributes, number of men bearing the attribute and number of women bearing the attribute
	    * @see io.write.TxtWriter#writeSurvey(RingGroupMap,CountMap,int)
	    *
	    * @deprecated MIGRATED to StatisticsWorker.numberOfAttribues(Net, String, boolean)
	    */
/*	   public int[] nrAttributes (String label, boolean sym){
		   int[] s = {0,0,0};
		   for (Individual v : net.individuals()){
			   Attributes att = v.getAttributes();
			   if (att==null) continue;
			   for (Attribute a : att.toList()){
				   if (a.label().equals(label)) {
					   s[0]++;
					   int g = v.getGender().toInt();
					   if (g<2) s[g+1]++;
				   }
			   }
		   }
		   if (sym) s[0] = s[0]/2;
		   return s;
	   }*/
	   
	   
	   /**
	    * gets the number of co-spouse relations in the Net
	    * @param g the gender of co-spouses (0 male - co-husbands, 1 female - co-wives)
	    * @return the number of co-spouses
	    *
	    * @deprecated MIGRATED to StatisticsWorker.numberOfCoSpouses(Gender)
	    */
/*	   public int nrCoSpouses (int g){
		   int n = 0;
		   for (Individual u : net.individuals()) {
			   if (wrongSex(u,g)) continue;
			   if (u.isSingle()) continue;
			   for (Individual v : u.getSpouses()) {
				   for (Individual w : v.getSpouses()){
					   if (w.getId()>u.getId()) {
						   if (!net.contains(w.getId())) continue;
						   n++;
					   }
				   }
			   }
		   }
		   return n;
	   }*/

	   /**
	    * counts the male and female vertices of the Net
	    * @return the array containing the numbers of male and female vertices
	    *
	    * @deprecated MIGRATED to StatisticsWorker.genderDistribution(Net)
	    */
/*	   private int[] nrGender () {
		   int[] k = new int[2];
		   for (Individual v : net.individuals()){
			   int g = v.getGender().toInt();
			   if (g<2) k[g]++;
		   }
		   return k;
	   }*/
	   
		/**
	 * checks for filiation cycles in the network
	 * @param control the protocol to note found cycles
	 * @param k the maximal cycle length to be checked
	 *
	 * @deprecated MIGRATED to ControlReporter#checkCycle
	 */
/*	public void checkCycles (ArrayList<String> control, Individual ego, int k) {
		
		String[] parentType = {"F","M","X"};
		
		for (Individual alter : ego.getParents()) {
			if (alter==null) continue;
			checkCycles(control,alter,ego,k,parentType[alter.getGender().toInt()]);
       }
	}*/
	
	/**
	 * checks for filiation cycles in the network
	 * @param control the protocol to note found cycles
	 * @param k the maximal cycle length to be checked
	 * @param firstEgo the last vertex of the chain
	 * @param str a string containing the preceding kinship chain in positional notation 
	 *
	 * @deprecated MIGRATED to ControlReporter#checkCycle
	 */
/*	private void checkCycles (ArrayList<String> control, Individual ego, Individual firstEgo, int k, String str) {

		if (k==0) return;
		String[] parentType = {"F","M","X"};

		if (ego.equals(firstEgo)) control.add(str+" "+ego.getName()+" ("+ego.getId()+")");
		for (Individual alter : ego.getParents()) {
			if (alter==null) continue;
			checkCycles(control,alter,firstEgo,k-1,parentType[alter.getGender().toInt()]);
		}
	}*/
	   
	   /**
	    * counts the marriage ties in the Net
	    * @return the number of marriage ties
	    * @see OldNet.CopyOfNet#nrMarriages(int)
	    * @see trash.Part#shares()
	    *
	    * @deprecated MIGRATED to StatisticsWorker#numberOfMarriages(Net)
	    */
/*	   public int nrMarriages (){
		   int s = 0;
		   for (Individual v: net.individuals()){
			   if (v.isFemale() ) continue;
			   s = s+v.getSpouses().size();
		   }
		   return s;
	   }*/

	   //Change from integer to boolean condition? Or generalize to other values of r? 
	   /**
	    * counts the number of marriages of a Net, or the number of marriages involving at least one vertex in a SubNet 
	    * @param r = 1 if the SubNet condition is valid
	    * @return the number of marriages
	    * @see gui.TipWriter#writeSurvey(RingGroupMap, CountMap<int>,int)
	    */
/*	   public int nrMarriages (int r){
		   int s = 0;
		   if (r!=1) return nrMarriages();
		   for (Individual v: net.getFinalOrigin().individuals()){
			   if (v.isSingle() || v.isFemale()) continue;
			   for (Individual w : v.getSpouses()){
				   if (net.contains(v.getId()) || net.contains(w.getId())) s++;
			   }
		   }
		   return s;
	   }*/
	   

	   
	   /**
	 * sets the numbers of known (agnatic, uterine and cognatic) ascendants in each ascending generation for the vertices of the Net 
	 * @param n the maximal number of ascending generations to be considered 
	 */
/*	private Map<Integer,int[][]>  ascendantCount (int n){
		
		Map<Integer,int[][]> result = new HashMap<Integer,int[][]>();
		for (Individual v: net.individuals()){
			int[][] count = new int[3][n];
			if (v.getFather()!=null) {
				count[0][0]++;
				count[1][0]++;
			}
			if (v.getFather()!=null) {
				count[0][0]++;
				count[2][0]++;
			}
			result.put(v.getId(), new int[3][n]);
		}
		for (int i=1;i<n;i++){
			for (Individual v: net.individuals()){
				int[][] count = result.get(v.getId());
				for (Individual p : v.getParents()){
					if (p==null) continue;
					int[][] parentsCount = result.get(p.getId());
					for (int g=0;g<3;g++){
						if (wrongSex(v,g)) continue;
						count[g][i]=count[g][i]+parentsCount[g][i-1];
					}
				}
			}
		}
		return result;
	}*/

	   //Check functionality! For the moment not called
	   /**
		 * computes the distribution of (agnatic, uterine and cognatic) sibling numbers and prints it in a protocol
		 * @param stat the string list used as a protocol
		 * @param n the maximal size of a sibling group to be considered 
		 * @return an array containing the number of vertices having a given number agnatic, uterine and cognatic siblings, up to number n
	    */
/*	   public double[][] siblingDistribution (ArrayList<String> stat, int n) {

		   stat.add(t("Sibling Group Distribution")+"\n");
		   double[][] sb = new double[2][n];
		   for (Individual v : net.individuals()) {
//			   if (v.ignore()) continue;
			   int s = v.fullSiblings().size();
		       if ((s>=n) || v.getGender().isUnknown()) continue;
		       sb[v.getGender().toInt()][s]++;
		   }
		   for (int i=0;i<2;i++) {
			   for (int j=0;j<n;j++){
				   sb[i][j]=Mat.percent(sb[i][j],size(i));
				   if ((i>0) && (sb[0][j]+sb[1][j]>0)) stat.add(j+"\t"+sb[0][j]+"\t"+sb[1][j]);
			   }
		   }	
		   stat.add("\n");
		   return sb;
	   }*/
	   
	   /**
	    * gets the number of vertices of a given gender
	    * @param g the selected gender
	    * @return the number of vertices of the selected gender
	    * @see OldNet#getIdentityCard()
	    * @see OldNet#siblingDistribution(ArrayList, int)
	    *
	    * @deprecated MIGRATED to StatisticsWorker#size(Net, Gender)
	    */
/*	   public int size (int g){
		   return size(g,false);
	   }*/
	   
	   /**
	   * gets the number of vertices of a given gender, with possible restriction to married individuals
	   * @param g the selected gender
	   * @param onlyMarr the marriage restriction (true if only married individuals are counted)
	   * @return the number of vertices of the required gender (and matrimonial status)
	   * @see OldNet#getIdentityCard()
	   * @see OldNet#getNrMarried(int, int)
	   * @see OldNet#size(int)
	   * 
	   * @deprecated MIGRATED to StatisticsWorker#numberOfNotSingles
	   */
	   
/*	   private int size (int g, boolean onlyMarr) {
		   int i=0;
//		   boolean ign = Puck.startOption(11);
		   for (Individual v : net.individuals()) {
//			   if (v.ignore(ign)) continue;
			   if (onlyMarr && v.isSingle()) continue;
			   if (!wrongSex(v,g)) i++;
		   }
		   return i;
	   }*/
	   
	   /**
	    * a shortcut for combinations of info options
	    * @param i the index of the info option
	    * @return true if the info option or combination of options is chosen
	    * @see OldNet#getIdentityCard()
	    */
/*	   private boolean tell(int i){
		   boolean b = Puck.infoOption(i);
		   if (b) return b;
		   if (i==1) return tell(3) || tell(4);
		   if (i==2) return tell(3);
		   return b;
	   }*/
	   
		// for old CountConsanguines-Method, replace!
		/**
		 * gets the number of consanguines
		 * @param dmax the maximal depth of the consanguine relations
		 * @param g the filter criterion for ascendant lines (0 agnatic, 1 uterine, 2 cognatic)
		 * @see Vertex#nrArcs(int, int) 
		 */
/*		public int[] getNrConsanguines (Individual ego, int dmax, int g) {	
			int[] cons = new int[dmax+1];
			nrConsanguines(new HashMap<Integer,Integer>(),ego, ego.getId(),cons,0,0,dmax,dmax,g);
			if (dmax<2) return cons;
			for (int i=2;i<dmax+1;i++){
				cons[i]=cons[i]+cons[i-1];
			}
			return cons;
		}*/
		
		// old CountConsanguines-Method, replace!
		/**
		 * counts the consanguines of a vertex (recursive part)
		 * @param ego the ego vertex
		 * @param cons an integer array counting the number of consanguines of given canonic degree
		 * @param up the number of steps in upward direction
		 * @param down the number of steps in downward direction
		 * @param upmax the upper bound for ascending search
		 * @param downmax the lower bound for descending search
		 * @param g the filter criterion for ascendant lines (0 agnatic, 1 uterine, 2 cognatic) 
		 * @see Vertex#getNrConsanguines(int, int)
		 */
/*		private void nrConsanguines (Map<Integer, Integer> visitor, Individual indi, int ego, int[] cons, int up, int down, int upmax, int downmax, int g) {	
			int d = Math.max(up,down);
			if (visitor.get(indi.getId())==ego) return;
			if (up>0){
				if (wrongSex(indi,g)) return;
			}
			if (d>0) cons[d]++;
			visitor.put(indi.getId(), ego);
			if ((down==0) && (up<upmax)) {
				for (Individual v : indi.getParents()){
					if (v==null) continue;
					if ((up==0) && (!wrongSex(indi,g))) cons[0]++;
					nrConsanguines(visitor,v,ego,cons,up+1,down,upmax,downmax,g);
				}
			}
			if (wrongSex(indi,g)) return;
			if (down<downmax) {
				if (indi.isSterile()) return;
				for (Individual v : indi.getChildren()){
					nrConsanguines(visitor, v,ego,cons,up,down+1,upmax,downmax,g); 
				}
			}
		}*/		

}
