package org.tip.puck.visualization.layouts.hierarchical.datastructs.chain;

import java.util.Map.Entry;
import java.util.*;
import org.gephi.graph.api.Node;
import org.tip.puck.visualization.layouts.GraphSource;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.containers.ChainContainer;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.containers.BorderContainer;

/**
 * @date Jun 23, 2012
 *
 * @author Edoardo Savoia
 */
public class ChainCluster {

    private final GraphSource graphSource;
    private ChainContainer rootContainer;

    private Map<Integer,Set<BorderContainer>> levelBorderContainers = new HashMap<Integer, Set<BorderContainer>>();

    public static ChainCluster createCluster(GraphSource graphSource, Node root , int origin){
	ChainCluster cc = new ChainCluster(graphSource);
	cc.init(root, origin);
	return cc;
    }

    private ChainCluster(GraphSource graphSource) {
	this.graphSource = graphSource;
    }

    private void init(Node root , int origin){
	rootContainer = ChainContainer.getContainer(root, this.graphSource);
	rootContainer.setOrigin(origin);
	if (rootContainer instanceof BorderContainer) {
	    addBorder((BorderContainer) rootContainer);
	}
	rootContainer.setRootCluster(this);
	rootContainer.populateChildren(graphSource);
    }

    public void addBorder(BorderContainer border){
	addBorderAtLevel(border, border.getOrigin());
    }
    
    private void addBorderAtLevel(BorderContainer border, int level){
	if (!levelBorderContainers.containsKey(level)) {
	    Set<BorderContainer> borderSet = new HashSet<BorderContainer>();
	    levelBorderContainers.put(level, borderSet);
	}
	Set<BorderContainer> borderSet = levelBorderContainers.get(level);
	borderSet.add(border);

    }

    public Set<BorderContainer> getBordersAtLevel(int level){
	return levelBorderContainers.get(level);
    }

    public int getBorderLevel(BorderContainer border) {
	int level = -1;
	Set<Entry<Integer, Set<BorderContainer>>> entrySet = levelBorderContainers.entrySet();
	boolean canGo = true;
	for (Iterator<Entry<Integer, Set<BorderContainer>>> it = entrySet.iterator(); canGo && it.hasNext();) {
	    Entry<Integer, Set<BorderContainer>> entry = it.next();
	    Set<BorderContainer> borderSet = entry.getValue();
	    if (borderSet.contains(border)) {
		level = entry.getKey();
		canGo = false;
	    }
	}
	return level;
    }

    public Set<Integer> getAllLevels(){
	return Collections.unmodifiableSet(levelBorderContainers.keySet());
    }

    public ChainContainer getCluster() {
	return rootContainer;
    }

    public int getOrigin() {
	return rootContainer.getOrigin();
    }

    public int getHeight() {
	return rootContainer.getHeight();
    }

    public int getWidth() {
	return rootContainer.getWidth();
    }

    public int getArea() {
	return rootContainer.getArea();
    }

    public int getSize() {
	return rootContainer.getSize();
    }

    public float getDensity() {
	return rootContainer.getDensity();
    }

    public GraphSource graphSource() {
	return graphSource;
    }

}
