package org.tip.puck.net.relations.workers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang3.ArrayUtils;
import org.tip.puck.PuckException;
import org.tip.puck.census.chains.Chain;
import org.tip.puck.census.chains.Notation;
import org.tip.puck.census.workers.CensusCriteria;
import org.tip.puck.census.workers.CircuitFinder;
import org.tip.puck.graphs.Graph;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Actors;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.workers.SequenceNetworkMaker;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.ToolBox;
import org.tip.puck.util.Value;

import fr.devinsy.util.StringList;

/**
 * @author Klaus Hamberger
 * @author TIP
 */
public class RelationWorker {
	
	public enum TransformationType {
		CONSTANT,
		GAIN,
		LOSS,
		CHANGE;
	}

	/**
	 * 
	 * @param net
	 * @param criteria
	 */
	public static void addChildRoles(final Net net, final AddChildRolesCriteria criteria) {
		//
		if ((net != null) && (criteria != null)) {
			//
			RelationModel model = net.relationModels().getByName(criteria.getRelationModelName());
			String egoRoleName = criteria.getEgoRoleName();
			StringList roleNames = criteria.getAlterRoleNames();
			int maxAge = criteria.getMaxAge();
			String dateLabel = criteria.getDateLabel();

			if (model != null) {

				model.roles().add(new Role(egoRoleName + "_CHILD", 0));
				Relations relations = net.relations().getByModel(model);

				for (Relation event : relations) {
					if (!RelationValuator.isBirth(event) && !RelationValuator.isDeath(event)) {
						Integer eventYear = IndividualValuator.extractYearAsInt(event.getAttributeValue(dateLabel));
						if (eventYear != null) {
							for (Actor actor : event.actors().toList()) {
								if (actor.getRole().getName().equals(egoRoleName)) {
									Individual ego = actor.getIndividual();
									int age = IndividualValuator.ageAtYear(ego, eventYear);
									if (age > -1 && age < maxAge && event.getRoleNames(ego).contains(egoRoleName) && !RelationValuator.isBirth(event)
											&& !RelationValuator.isDeath(event)) {
										for (String alterRoleName : roleNames) {
											if (!event.getIndividuals(alterRoleName).contains(ego.getFather())
													&& !event.getIndividuals(alterRoleName).contains(ego.getMother())) {
												event.actors().add(new Actor(ego, new Role(egoRoleName + "_CHILD")));
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param ancestorList
	 * @param referent
	 * @param ascendants
	 * @param relation
	 * @param treeType
	 * @param maxDegrees
	 * @return
	 */
	private static Set<List<String>> getLinkChains(final List<String> ancestorList, final Actor referent, final Actors ascendants, final Relation relation,
			final String treeType, final int[] maxDegrees) {
		Set<List<String>> result;

		result = new HashSet<List<String>>();

		Actors dependants = relation.getDependants(referent);
		ascendants.add(referent);

		if (dependants.size() > 0) {
			for (Actor descendant : dependants) {
				if (ascendants.contains(descendant)) {
					System.err.println("Cyclic referent structure : " + referent + " for " + descendant + " " + referent + " in " + relation);
				} else {
					List<String> list = new ArrayList<String>();
					list.addAll(ancestorList);

					Individual individual = descendant.getIndividual();
					if (treeType.equals("GENDER")) {
						list.add(individual.getGender().toChar() + "");
					} else if (treeType.equals("ID")) {
						list.add(individual.getId() + "");
					} else if (treeType.equals("KIN")) {
						list.add(individual.getGender().toChar() + "");
					}

					for (List<String> chain : getLinkChains(list, descendant, ascendants, relation, treeType, maxDegrees)) {
						result.add(chain);
					}
				}
			}
		} else {
			result.add(ancestorList);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param relation
	 * @param treeType
	 * @param pattern
	 * @return
	 */
	public static Set<List<String>> getLinkChains(final Relation relation, final String treeType, final String pattern) {
		Set<List<String>> result;

		result = new HashSet<List<String>>();

		int[] maxDegrees = ToolBox.stringsToInts(pattern);

		for (Actor actor : relation.actors()) {
			Individual referent = actor.getReferent();
			if (referent == null) {
				List<String> ancestorList = new ArrayList<String>();
				Individual individual = actor.getIndividual();
				if (treeType.equals("GENDER")) {
					ancestorList.add(individual.getGender().toChar() + "");
				} else if (treeType.equals("ID")) {
					ancestorList.add(individual.getId() + "");
				} else if (treeType.equals("KIN")) {
					ancestorList.add(individual.getGender().toChar() + "");
				}

				for (List<String> chain : getLinkChains(ancestorList, actor, new Actors(), relation, treeType, maxDegrees)) {
					result.add(chain);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param relation
	 * @param treeType
	 * @param pattern
	 * @return
	 */
	public static String getLinkChainsAsString(final Relation relation, final String treeType, final String pattern) {
		String result;

		result = "";

		Set<List<String>> chains = getLinkChains(relation, treeType, pattern);
		List<String> chainsAsStrings = new ArrayList<String>();
		for (List<String> chain : chains) {
			chainsAsStrings.add(chain.toString());
		}
		Collections.sort(chainsAsStrings);

		result = chainsAsStrings.toString();
		//
		return result;
	}

	/**
	 * 
	 * @param referent
	 * @param ascendants
	 * @param relation
	 * @param reduced
	 * @param treeType
	 * @param maxDegrees
	 * @return
	 */
	private static String getLinkTree(final Actor referent, final Individuals ascendants, final Relation relation, final boolean reduced, final String treeType,
			final int[] maxDegrees, final List<String> relationModelNames, final String chainClassification) {
		String result;

		result = "";

		Actors dependants = relation.getDependants(referent);
		ascendants.add(referent.getIndividual());
		
		if (dependants.size() > 0) {
			result = "[";
			List<String> list = new ArrayList<String>();
			for (Actor descendant : dependants.toSortedList()) {
				if (ascendants.contains(descendant.getIndividual())) {
					System.err.println("Cyclic referent structure : " + referent + " for " + descendant + " in " + relation);
				} else {
					String item = getLinkTree(descendant, ascendants, relation, reduced, treeType, maxDegrees, relationModelNames, chainClassification);
					if (treeType.equals("GENDER")) {
						item = descendant.getIndividual().getGender().toChar() + item;
					} else if (treeType.equals("ID")) {
						item = descendant.getIndividual().getId() + " " + item;
					} else if (treeType.equals("KIN")) {
						item = NetUtils.getAlterRole(descendant.getIndividual(), referent.getIndividual(), maxDegrees, relationModelNames, chainClassification) + " " + item;
					}
					if (!reduced || !list.contains(item)) {
						list.add(item);
					}
				}
			}
			Collections.sort(list);
			for (String item : list) {
				result += item;
			}
			result += "]";
		}
		//
		return result;
	}

	/**
	 * 
	 * @param relation
	 * @param reduced
	 * @param treeType
	 * @param pattern
	 * @return
	 */
	public static List<String> getLinkTrees(final Relation relation, final boolean reduced, final String treeType, final String pattern, final List<String> relationModelNames, final String chainClassification) {
		List<String> result;

		result = new ArrayList<String>();

		int[] maxDegrees = ToolBox.stringsToInts(pattern);

		if (relation != null){
			for (Actor actor : relation.actors().toSortedList()) {
				
				Individual individual = actor.getIndividual();
				Individual referent = actor.getReferent();
				Actors referentActors = relation.actors().getByIndividual(referent);
				
				if (referent == null || referentActors.isEmpty()) {
					String item = getLinkTree(actor, new Individuals(), relation, reduced, treeType, maxDegrees, relationModelNames, chainClassification);
					if (treeType.equals("GENDER")) {
						item = individual.getGender().toChar() + item;
					} else if (treeType.equals("ID")) {
						item = individual.getId() + " " + item;
					} else if (treeType.equals("KIN")) {
						item = individual.getGender().toChar() + " " + item;
					}
					if (!reduced || !result.contains(item)) {
						result.add(item);
					}
					
				} else {
					
					for (Actor referentActor : referentActors){
						if (referentActor.getReferent()!=null && referentActor.getReferent().equals(individual)){
							System.err.println("Cyclic referent structure : " + referent + " for " + individual + " in " + relation);
						}
					}
				}
			}
		}
		Collections.sort(result);
/*		for (String item : list) {
			result += item + " ";
		}*/

		//
		return result;
	}
	
	public static int[] getTreeDimensions (String tree){
		int[] result;
		
		int maxDepth = 0;
		int depth = 0;
		int width = 1;
		
		for (char c : tree.toCharArray()){
			
			if (c=='[') {
				
				depth++;
				
				if (depth > maxDepth){
					maxDepth = depth;
				}
				
			} else if (c==']'){
				
				depth--;
				
			} else if (c==','){

				width++;
			}
		}
		
		result = new int[]{maxDepth,width};
		
		//
		return result;
	}

	/**
	 * 
	 * @param relation
	 * @return
	 */
	public static Graph<Individual> getReferentGraph(final Relation relation) {
		Graph<Individual> result;

		result = new Graph<Individual>();
		
		if (relation!=null){
			
			for (Actor actor : relation.actors()) {
				Individual ego = actor.getIndividual();
				Individual alter = actor.getReferent();
				if (ego!=null && alter!=null){
					result.addArc(ego, alter,1);
				} else if (ego!=null){
					result.addNode(ego);
				}
			}
			result.setLabel(relation.getName());

		}
		
		//
		return result;
	}
	
	public static Map<String,Value> getAllKinCensus (final Segmentation segmentation, final Relation relation, final CensusCriteria censusCriteria){
		Map<String,Value> result;
		
		result = new HashMap<String,Value>();
		
		if (relation!=null){
			
			Segmentation domain = new Segmentation(relation.getIndividuals(),segmentation.getAllFamilies(),segmentation.getAllRelations(), segmentation.getGeography());
			int size = relation.getIndividuals().size();
			
			try {
				
				CircuitFinder finder = new CircuitFinder(domain,censusCriteria);
				finder.findCircuits();

				for (Cluster<Chain> cluster : finder.getCircuits().getClusters()){
					result.put(cluster.getValue()+"", new Value(cluster.size()));
					result.put(cluster.getValue()+"%", new Value(new Double(cluster.size()).doubleValue()/new Double(1.*size*(size-1)/2).doubleValue()));
				}

			} catch (PuckException e) {
				e.printStackTrace();
			}
		}
		//
		return result;
		
	}
	
	public static Actor getClosestHomologue (Relation relation, Actor actor, String dateLabel, String direction){
		Actor result;
		
		result = null;
		
		if (relation!=null && actor!=null) {

			Relation closestRelation = null;
			Integer date = relation.getTime(dateLabel);
			Integer closestDate = null;
			
			if (date!=null){
				
				for (Relation otherRelation : actor.getIndividual().relations().getByModel(relation.getModel())){
					if (!otherRelation.equals(relation)){
						Integer otherDate = otherRelation.getTime(dateLabel);
						if (otherDate!=null && ((direction.equals("OUT") && otherDate > date && (closestDate == null || otherDate < closestDate)) || ((direction.equals("IN") && otherDate < date && (closestDate == null || otherDate > closestDate))))){
							closestRelation = otherRelation;
							closestDate = otherDate;
						}
					}
				}
				
				if (closestRelation != null){
					result = closestRelation.actors().get(actor.getIndividual().getId(), actor.getRole());
				}
			}
		}
		
		//
		return result;
		
	}
		

	public static Map<String,Value> getReferentChainCensus (final Relation relation, final Individual ego, final String egoRoleName, final String affiliationLabel, final String referentRelationLabel){
		Map<String,Value> result;
		
		result = new HashMap<String,Value>();
		
		if (relation!=null){
			
//			result.put("Types", Value.valueOf(getReferentChainTypes(relation)));
//			result.put("Types_Order", Value.valueOf(getReferentChainTypeOrder(relation)));
//			result.put("Types_Vector", Value.valueOf(Arrays.toString(getReferentChainPercentageVector(relation))));

			for (Actor actor : getActors(relation,ego,egoRoleName)) {
				
				String chain = getReferentChainGenderString(actor, affiliationLabel, referentRelationLabel, relation);
						
				Value count = result.get(chain);
				if (count == null){
					result.put(chain, new Value(1.));
				} else {
					result.put(chain, new Value(result.get(chain).doubleValue()+1.));
				}
			}
			
		}
		
		//
		return result;
	}
	
	public static String getReferentChainTypeOrder (final Relation relation){
		String result;
		
		int[] vector = getReferentChainVector(relation);
		
		Map<Integer,String> countMap = new TreeMap<Integer,String>(Collections.reverseOrder());
		
		for (int i=0;i<vector.length;i++){
			if (vector[i]!=0){
				String numbers = countMap.get(vector[i]);
				if (numbers == null){
					numbers = i+"";
				} else {
					numbers += "-"+i;
				}
				countMap.put(vector[i], numbers);
			}
		}
		
		result = "";
		for (String numbers : countMap.values()){
			result += numbers+">";
		}
		
		//
		return result;
	}
	
	public static String getReferentChainVectorString (final Relation relation){
		String result;
		
		int[] vector = getReferentChainVector(relation);
		
		result = "";
		for (int i=0;i<vector.length;i++){
			if (vector[i]!=0){
				result += i+" ";
			}
		}
		
		//
		return result;
	}
	
	/**
	 * shortCut to facilitate code
	 * @param individual
	 * @return
	 */
	private static List<Actor> getActors(Relation relation, Individual individual, String roleName){
		List<Actor> result;
		
		result = new ArrayList<Actor>();
		
		if (individual!=null){
			
			result.add(relation.getActor(individual, roleName));
			
		} else {
			
			result.addAll(relation.actors().toSortedList());
		}
		
		//
		return result;
	}
	
	public static int[] getReferentChainVector (final Relation relation){
		int[] result;
		
		Map<Integer,Integer> countMap = new TreeMap<Integer,Integer>();
		int max = -1;
		
		if (relation!=null){
			
			for (Actor actor : relation.actors().toSortedList()) {
				
				int number = getReferentChainNumber(actor, relation);
						
				Object count = countMap.get(number);
				if (count == null){
					countMap.put(number, 1);
				} else {
					countMap.put(number, ((Integer)countMap.get(number))+1);
				}
				
				if (number > max){
					max = number;
				}
			}
		}
		
		result = new int[max+1];
		for (int key : countMap.keySet()){
			result[key]=countMap.get(key);
		}
		
		//
		return result;
	}
	
	public static double[] getReferentChainPercentageVector (final Relation relation){
		double[] result;
		
		int[] vector = getReferentChainVector(relation);
		result = new double[vector.length];
		
		int sum = 0;
		for (int number : vector){
			sum += number;
		}
		for (int i=0;i<vector.length;i++){
			result[i] = MathUtils.percent(vector[i], sum);
		}
		//
		return result;
		
	}
	
	public static Chain getReferentChain(final Actor actor, final Relation relation){
		Chain result;
		
		Individual ego = actor.getIndividual();
		Individual alter = actor.getReferent();
		Individuals previous = new Individuals();
		previous.add(ego);
		
		result = new Chain(ego);
		
		while (alter!=null){
			if (previous.contains(alter)){
//				System.err.println("Cyclic referent chain: "+previous);
				break;
			}
			result.add(alter, 1);
			Actor alterActor = relation.actors().get(alter.getId(),actor.getRole());
			if (alterActor !=null){
				previous.add(alter);
				ego = alter; 
				alter = alterActor.getReferent();
			} else {
//				System.err.println("Nonresident referent for "+relation+"\t"+alter);
				alter = null;
			}
		}
		
		//
		return result;
	}

	public static String getReferentChainGenderString(final Actor actor, final String affiliationLabel, final String referentRelationLabel, final Relation relation){
		String result;
		
		Chain chain = getReferentChain(actor,relation);
		result = chain.signature(Notation.POSITIONAL).replaceAll("\\(", "").replaceAll("\\)", "");
				
		if (result.length()>0){
			String status = getHouseStatus(chain.getLast(),affiliationLabel, referentRelationLabel, relation);
			if (status != null){
				if (status.equals("OWNER")){
					result += "*";
				} else if (status.equals("OWNER_SPOUSE")){
					result += "°";
				}
			}
		}
		//
		return result;
	}
	
	public static int getReferentChainNumber(final Actor actor, final Relation relation){
		int result;
		
		Chain chain = getReferentChain(actor,relation);
		result = chain.getCharacteristicNumber();
				
		//
		return result;
	}

/*	public static String getReferentChainGenderString1(final Actor actor, final String affiliationLabel, final Relation relation){
		String chain;
		
		Individual ego = actor.getIndividual();
		Individual alter = actor.getReferent();
		Individuals previous = new Individuals();
		previous.add(ego);
		
		chain = ego.getGender().toChar()+"";
		
		if (alter==null){
			String status = getHouseStatus(ego,affiliationLabel, relation);
			if (status.equals("OWNER")){
				chain = "+";
			} else if (status.equals("OWNER_SPOUSE")){
				chain = "-";
			}
		}

		while (alter!=null){
			if (previous.contains(alter)){
				System.err.println("Cyclic referent chain: "+previous);
				break;
			}
			chain += alter.getGender().toChar();
			Actor alterActor = relation.actors().get(alter.getId(),actor.getRole());
			if (alterActor !=null){
				previous.add(alter);
				ego = alter; 
				alter = alterActor.getReferent();
				if (alter==null){
					String status = getHouseStatus(ego,affiliationLabel, relation);
					if (status.equals("OWNER")){
						chain += "+";
					} else if (status.equals("OWNER_SPOUSE")){
						chain += "-";
					}
				}
			} else {
				System.err.println("Nonresident referent for "+relation+"\t"+alter);
				alter = null;
			}
		}

		//
		return chain;
	}*/
	
	private static String getHouseStatus(final Individual ego, final String affiliationLabel, final String referentRelationLabel, final Relation relation){
		String result;
		
		result = null;
				
		String egoAttributeValue = ego.getAttributeValue(affiliationLabel);
		String relationAttributeValue = relation.getAttributeValue(affiliationLabel);
		
		if (egoAttributeValue!=null && egoAttributeValue.equals(relationAttributeValue)){
			result = affiliationLabel;
		} else {
			for (Individual spouse : ego.spouses()){
				String spouseAttributeValue = spouse.getAttributeValue(affiliationLabel);
				if (spouseAttributeValue!=null && spouseAttributeValue.equals(relationAttributeValue)){
					result = affiliationLabel+"_OF_SPOUSE";
					break;
				}
			}
			
			if (result==null && referentRelationLabel!=null){
				for (Actor actor : relation.actors().getByIndividual(ego)){
					String referentRelation = actor.getAttributeValue(referentRelationLabel);
					if (referentRelation!=null){
						result = affiliationLabel+"_OF_"+referentRelation;
						break;
					}
				}
			}
		} 
		
		if (result == null){
			result = "UNKNOWN";
		}

		//
		return result;
	}
	
	public static String getReferentRole(final Actor actor, final String pattern, final String affiliationLabel, final String referentRelationLabel, final List<String> relationModelNames, final String chainClassification, Relation relation){
		String result;

		int[] maxDegrees = ToolBox.stringsToInts(pattern);

		Individual ego = actor.getIndividual();
		Individual alter = actor.getReferent();
				
		if (alter==null){
			
			result = getHouseStatus(ego, affiliationLabel, referentRelationLabel, relation);
			
		} else {
			
			result = NetUtils.getAlterRole(ego, alter, maxDegrees, relationModelNames, chainClassification);
		}
		
//		System.out.println(ego+" "+alter+" "+result);
		
		//
		return result;
	}

	public static String getReferentRoleShort(final Actor actor, final String pattern, final String affiliationLabel, final String referentRelationLabel, final List<String> relationModelNames, final String chainClassification, Relation relation){
		String result;

		result = shortCut(getReferentRole(actor,pattern,affiliationLabel,referentRelationLabel, relationModelNames, chainClassification, relation), affiliationLabel);
		//
		return result;
	}

	
	public static Map<String,Value> getReferentKinCensus (final Relation relation, final Individual ego, final String egoRoleName, final String pattern, final String affiliationLabel, final String referentRelationLabel, final List<String> relationModelNames, final String chainClassification){
		Map<String,Value> result;
		
		result = new HashMap<String,Value>();
		
		if (relation!=null){

//			result.put("Types", Value.valueOf(getReferentKinTypes(relation, pattern, affiliationLabel)));

			for (Actor actor : getActors(relation,ego,egoRoleName)) {
				
				String alterRole = getReferentRole(actor,pattern, affiliationLabel, referentRelationLabel, relationModelNames, chainClassification, relation);
				
				Value count = result.get(alterRole);
				if (count == null){
					result.put(alterRole, new Value(1.));
				} else {
					result.put(alterRole, new Value(count.doubleValue() + 1.));
				}
			}
			
/*			for (String key : result.keySet()){
				if (!key.equals("Types")){
					result.put(key, new Value(MathUtils.percent(result.get(key).doubleValue(),relation.actors().size())));
				}
			}*/
		}
		
		
		
		//
		return result;
	}
	
	private static String shortCut (String kinType, String affiliationLabel){
		String result;
		
		if (kinType==null || kinType.equals("UNKNOWN")){
			result = "-";
		} else if (kinType.equals("UNRELATED")){
			result = "X";
		} else if (kinType.equals("FATHER") || kinType.equals("MOTHER") || kinType.equals("SPOUSE") || kinType.equals("CHILD")){
			result = kinType.charAt(0)+"";
		} else if (kinType.equals("AFFINE")){
			result = "N";
		} else if (kinType.contains("RELATIVE_AGNATIC") || kinType.equals("RELATIVE_PATERNAL_AUNT") || kinType.equals("RELATIVE_PATERNAL_UNCLE") || kinType.equals("RELATIVE_PATERNAL_GRANDFATHER")){
			result = "A";
		} else if (kinType.contains("RELATIVE_COGNATIC") || kinType.equals("RELATIVE_MATERNAL_GRANDFATHER") || kinType.equals("RELATIVE_PATERNAL_GRANDMOTHER")){
			result = "O";
		} else if (kinType.contains("RELATIVE_UTERINE")  || kinType.equals("RELATIVE_MATERNAL_AUNT") || kinType.equals("RELATIVE_MATERNAL_UNCLE") || kinType.equals("RELATIVE_MATERNAL_GRANDMOTHER")){
			result = "U";
		} else if (kinType.equals("RELATIVE")){
			result = "R";
		} else if (kinType.equals("SIBLING")){
			result = "G";
		} else if (kinType.equals("FRIEND")){
			result = "D";
		} else if (kinType.equals("MASTER") || kinType.equals("EMPLOYER")){
			result = "E";
		} else if (kinType.equals("SUPPORTER")){
			result = "T";
		} else if (kinType.equals("PRIEST")){
			result = "I";
		} else if (kinType.equals("LANDLORD")){
			System.err.println("Landlord relation without location ");
			result = "L";
		} else if (kinType.equals(affiliationLabel)){
			result = "F";
		} else if (kinType.contains(affiliationLabel)){
			String houseType = kinType.replaceAll(affiliationLabel+"_OF_", "");
			if (houseType.equals("E") || houseType.equals("F") || houseType.equals("L") || houseType.equals("P")){
				result = houseType;
			} else if (houseType.equals("B") || houseType.equals("Z")){
				result = "G";
			} else if (houseType.equals("H") || houseType.equals("W") || houseType.equals("SPOUSE")){
				result = "S";
			} else if (houseType.contains("H") || houseType.contains("W")){
				result = "N";
			} else if (houseType.equals("M")  || houseType.equals("MM") || houseType.equals("MB") || houseType.equals("M*")){
				result = "U";
			} else if (houseType.equals("FF") || houseType.equals("FZ")){
				result = "A";
			} else if (houseType.contains("MF") || houseType.contains("FM")){
				result = "O";
			} else if (houseType.equals("T")){
				result = "H";
			} else if (houseType.equals("SDF") || houseType.equals("Q")){
				result = "Y";
			} else {
				result = kinType;
			}
		} else {
			result = kinType;
		}
		
		//
		return result;
	}
		
	public static String getReferentKinTypes (final Relation relation, final String pattern, final String affiliationLabel, final String referentRelationLabel, final List<String> relationModelNames, final String chainClassification){
		String result;
		
		result = "";
		Set<String> kinTypes = new HashSet<String>();
		
		if (relation!=null){
			for (Actor actor : relation.actors().toSortedList()) {
				kinTypes.add(getReferentRoleShort(actor,pattern, affiliationLabel, referentRelationLabel, relationModelNames, chainClassification, relation));
			}
		}
		
		List<String> kinTypesList = new ArrayList<String>(kinTypes);
		Collections.sort(kinTypesList);
		
		for (String kinType : kinTypesList){
			result += kinType+" ";
		}
				
		//
		return result;
	}
	
/*	public static Map<String,Value> getStatistics (final Relation relation, final Individual ego, final SequenceCriteria criteria, final List<String> indicators) {
		Map<String,Value> result;
		
		result = new HashMap<String,Value>();
		
		Graph<Individual> graph = RelationWorker.getReferentGraph(relation);
		
		for (String indicator : indicators){
			
			if (indicator.equals("GRAPH")){
				
				result.put(indicator, Value.valueOf(graph));
				
			} else if (indicator.equals("SIZE")){
				
				result.put(indicator, Value.valueOf(graph.nodeCount()));
				
			} else if (indicator.equals("MAXDEPTH")){
				
				result.put(indicator, Value.valueOf(GraphUtils.getMaxDepth(graph)));
				
			} else if (indicator.equals("MEANDEPTH")){
				
				result.put(indicator, Value.valueOf(MathUtils.round(GraphUtils.getMeanDepth(graph),2)));
				
			} else if (indicator.equals("MEANINDEGREE")){
				
				result.put(indicator, Value.valueOf(MathUtils.round(GraphUtils.meanInDegree(graph),2)));
				
			} else if (indicator.equals("DIAMETER")){
				
				result.put(indicator, Value.valueOf(GraphUtils.getTreeDiameter(graph)));
				
			} else if (indicator.contains("COMPONENT") || indicator.equals("CONCENTRATION")){
				
				Partition<Node<Individual>> components = GraphUtils.components(graph);
				
				if (indicator.equals("NRCOMPONENTS")){
					
					result.put(indicator, Value.valueOf(components.size()));

				} else if (indicator.equals("MAXCOMPONENT")){
					
					result.put(indicator, Value.valueOf(components.maxClusterSize()));

				} else if (indicator.equals("CONCENTRATION")){
					
					result.put(indicator, Value.valueOf(components.concentration()));
					
				}
				
			} else if (indicator.equals("TREES_BY_ID")){
				
				result.put(indicator, Value.valueOf(getLinkTrees(relation, false, "ID", null)));
				
			} else if (indicator.equals("TREES_BY_GENDER")){
				
				result.put(indicator, Value.valueOf(getLinkTrees(relation, false, "GENDER", null)));
				
			} else if (indicator.equals("TREES_BY_KIN")){
				
				result.put(indicator, Value.valueOf(getLinkTrees(relation, false, "KIN", criteria.getPattern())));
				
			} else if  (indicator.equals("PLACE")){
				
				String placeValue = relation.getAttributeValue(criteria.getLocalUnitLabel());
				if (placeValue==null){
					placeValue = relation.getAttributeValue(criteria.getPlaceLabel());
				}
				result.put(indicator, Value.valueOf(placeValue));
				
			} else if (indicator.contains("REFERENT")){
				
				Actor actor = relation.getActor(ego, criteria.getEgoRoleName());
				
				if (actor!=null){

					if (indicator.equals("REFERENT")){
						
						result.put(indicator, Value.valueOf(actor.getReferent()));
						
					} else if (indicator.equals("REFERENT_KIN")){
						
						result.put(indicator, Value.valueOf(RelationWorker.getReferentRole(actor, criteria.getPattern(), criteria.getGroupAffiliationLabel(), relation)));
					
					} else if (indicator.equals("REFERENT_CHAIN")){
						
						result.put(indicator, Value.valueOf(RelationWorker.getReferentChainGenderString(actor, criteria.getGroupAffiliationLabel(), relation)));
					
					} else if (indicator.equals("REFERENT_KIN_TYPE")){
						
						result.put(indicator, Value.valueOf(RelationWorker.getReferentRoleShort(actor, criteria.getPattern(), criteria.getGroupAffiliationLabel(), relation)));
					
					} else if (indicator.equals("REFERENT_CHAIN_TYPE")){
						
						result.put(indicator, Value.valueOf(RelationWorker.getReferentChainNumber(actor, relation)));
					}
				}
			}
		}
		//
		return result;
	}*/
	
	public static int[] getTransformation (Object[] first, Object[] second){
		int[] result;
		
		result = new int[2];
		
		for (Object c : first){
			if (!ArrayUtils.contains(second, c)){
				result[0]++;
			}
		}
		for (Object c : second){
			if (!ArrayUtils.contains(first, c)){
				result[1]++;
			}
		}
		//
		return result;
	}
	
	public static TransformationType getTransformationType (Object[] first, Object[] second){
		TransformationType result;
		
		int[] transformation = getTransformation(first,second);
		
		if (transformation[0]==0 && transformation[1]>0){
			result = TransformationType.GAIN;
		} else if (transformation[0]>0 && transformation[1]==0){
			result = TransformationType.LOSS;
		} else if (transformation[0]>0 && transformation[1]>0){
			result = TransformationType.CHANGE;
		} else {
			result = TransformationType.CONSTANT;
		}
		
		//
		return result;
	}



}
