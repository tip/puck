package org.tip.puck.geo.tools;

import java.util.ArrayList;

import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.CRS;
import org.geotools.referencing.GeodeticCalculator;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.TransformException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.geo.Coordinate2;
import org.tip.puck.geo.Place;
import org.tip.puck.geo.Place;
import org.tip.puck.geo.io.BuildingGeoLinks;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.Node;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;

/**
 * 
 */
public class GeotoolsUtils {

	private static final Logger logger = LoggerFactory.getLogger(GeotoolsUtils.class);

	private GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory(null);

	/**
	 * 
	 * @param geoNetwork
	 * @param geoFlowNetwork
	 * @return
	 */
	public ArrayList<DefaultFeatureCollection> getFeaturesForNodesPlaces(final Graph<Place> geoNetwork, final Graph<?> geoFlowNetwork) {

		ArrayList<DefaultFeatureCollection> collectionsPointsAndLines = new ArrayList<DefaultFeatureCollection>();

		DefaultFeatureCollection featurePointsCollection;
		DefaultFeatureCollection featureLinesCollection;
		//		SimpleFeatureBuilder featureBuilder;

		//		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory(null);

		SimpleFeatureType pointFeatureType;

		SimpleFeatureTypeBuilder b = new SimpleFeatureTypeBuilder();

		// set the name
		b.setName("SCHEMA_POINT");

		// add a geometry property
		b.setCRS(DefaultGeographicCRS.WGS84); // set crs first
		b.add("id_geonames", Integer.class);
		b.add("name", String.class);
		b.add("country", String.class);
		b.add("the_geom", Point.class); // then add geometry

		// build the type
		pointFeatureType = b.buildFeatureType();

		featurePointsCollection = new DefaultFeatureCollection("internal", pointFeatureType);

		//		featureBuilder = new SimpleFeatureBuilder(pointFeatureType);

		// Add nodes and build geometry
		for (Node<Place> node : geoNetwork.getNodes()) {

			Point point = this.geometryFactory.createPoint(convert(node.getReferent().getCoordinate2()));

			SimpleFeature f = SimpleFeatureBuilder.build(pointFeatureType, new Object[] { node.getReferent().getIdGeonames(), node.getReferent().getToponym(),
					node.getReferent().getCountry_ISO2(), point }, null);

			if (f != null) {
				featurePointsCollection.add(f);
			}

			// Place p;
			// if( f != null ) {
			//
			// featurePointsCollection.add(f);
			//
			// Geometry g = (Geometry) f.getAttribute( "the_geom" );
			// p = new Place(node.getLabel(), g.getCoordinate());
			//
			// }
			// else {
			// p = new Place(node.getLabel());
			//
			// }
			// graphPlaces.addNode(node.getId(),p);
			//
			// //prepare for clustering
			// placesList.add(p); //hashset guarantee to avoid duplicate value
			// placesListDuplicate.add(p); //list conserve all values

		}
		collectionsPointsAndLines.add(featurePointsCollection);

		// Add links
		geoNetwork.addLinksByIds(geoNetwork.getLinks());

		BuildingGeoLinks linksFactory = new BuildingGeoLinks();
		featureLinesCollection = linksFactory.buildLinkGeometry(null, geoNetwork);

		collectionsPointsAndLines.add(featureLinesCollection);

		return collectionsPointsAndLines;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static com.vividsolutions.jts.geom.Coordinate convert(final Coordinate2 source) {
		com.vividsolutions.jts.geom.Coordinate result;

		if (source == null) {
			result = null;
		} else {
			result = new com.vividsolutions.jts.geom.Coordinate(source.getLongitude(), source.getLatitude(), source.getElevation());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param geoNetwork
	 * @param geoFlowNetwork
	 * @return
	 */
	public static ArrayList<DefaultFeatureCollection> getFeaturesForNodesPlaces(final Graph<Place> graph) {
		ArrayList<DefaultFeatureCollection> result;

		result = new ArrayList<DefaultFeatureCollection>();

		//		SimpleFeatureBuilder featureBuilder;

		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory(null);

		SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();

		// Set the name.
		builder.setName("SCHEMA_POINT");

		// Add a geometry property.
		builder.setCRS(DefaultGeographicCRS.WGS84); // set crs first
		builder.add("id_geonames", String.class);
		builder.add("name", String.class);
		builder.add("weight", Double.class);
		builder.add("the_geom", Point.class); // then add geometry

		// build the type
		SimpleFeatureType pointFeatureType = builder.buildFeatureType();

		DefaultFeatureCollection featurePointsCollection = new DefaultFeatureCollection("internal", pointFeatureType);
		//		featureBuilder = new SimpleFeatureBuilder(pointFeatureType);

		// Add nodes and build geometry
		for (Node<Place> node : graph.getNodes()) {
			logger.debug("node.getReferent={}", node.getReferent());
			Point point = geometryFactory.createPoint(convert(node.getReferent().getCoordinate2()));

			SimpleFeature feature = SimpleFeatureBuilder.build(pointFeatureType, new Object[] { node.getReferent().getExtraData(),
					node.getReferent().getToponym(), node.getWeight(), point }, null);

			if (feature != null) {
				featurePointsCollection.add(feature);
			}
		}
		result.add(featurePointsCollection);

		// Add links
		BuildingGeoLinks linksFactory = new BuildingGeoLinks();

		DefaultFeatureCollection featureLinesCollection;
		featureLinesCollection = linksFactory.buildLinkGeometry2(null, graph);
		result.add(featureLinesCollection);

		//
		return result;
	}

	/**
	 *http://www.geodatasource.com/developers/java
	 * @param unit = 'M' is statute miles, 'K' is kilometers (default), 'N' is nautical miles  
	 * @return
	 */
	public static Double distance(Place first, Place second, char unit) {
		Double result = null;

		if (first.getCoordinate2()!=null && second.getCoordinate2()!=null){

			CoordinateReferenceSystem crs = null;
			try {
				crs = CRS.decode("EPSG:4326");
			} catch (NoSuchAuthorityCodeException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (FactoryException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if( crs != null) {
				GeodeticCalculator gc = new GeodeticCalculator(crs);
				Coordinate temp_coord_start = new Coordinate(first.getCoordinate2().getLongitude(), first.getCoordinate2().getLatitude());
				Coordinate temp_coord_end = new Coordinate(second.getCoordinate2().getLongitude(), second.getCoordinate2().getLatitude());
				try {
					gc.setStartingPosition( JTS.toDirectPosition( temp_coord_start, crs ) );
					gc.setDestinationPosition( JTS.toDirectPosition( temp_coord_end, crs ) );
				} catch (TransformException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//Wrong distance computation
				//result = convert(first.getCoordinate2()).distance(convert(second.getCoordinate2()));
				
				result = gc.getOrthodromicDistance();
				
				switch (unit) {
				default:
				case 'K':
					result = result / 1000;
					break;
				case 'M':
					result = result / 1000 * 0.621371;
					break;
				case 'N':
					result = result / 1000 * 0.539957;
					break;
				}
				
			}
		} else {

			if (first.getCoordinate2()==null){
				System.err.println("Coordinates missing for "+first);
			}
			if (second.getCoordinate2()==null){
				System.err.println("Coordinates missing for "+second);
			}

			result = null;
		}

		//
		return result;

		//		double result;
		//		double theta = first.longitude() - second.longitude();
		//		
		//		result = Math.sin(deg2rad(first.latitude())) * Math.sin(deg2rad(second.latitude())) + Math.cos(deg2rad(first.latitude())) * Math.cos(deg2rad(second.latitude())) * Math.cos(deg2rad(theta));
		//		result = Math.acos(result);
		//		result = rad2deg(result);
		//		
		//		result = result * 60 * 1.1515;
		//		if (unit == 'K') {
		//			result = result * 1.609344;
		//		} else if (unit == 'N') {
		//			result = result * 0.8684;
		//		}
		//		//
		//		return result;
	}
}