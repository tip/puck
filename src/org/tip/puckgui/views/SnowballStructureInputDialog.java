package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.relations.Actors;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.RelationModels;
import org.tip.puck.net.workers.AttributeDescriptors;
import org.tip.puck.net.workers.AttributeWorker;
import org.tip.puck.net.workers.ExpansionMode;
import org.tip.puck.net.workers.SnowballCriteria;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class SnowballStructureInputDialog extends JDialog {

	private static final long serialVersionUID = -8101768566224840177L;
	private final JPanel contentPanel = new JPanel();
	private SnowballCriteria dialogCriteria;
	private static SnowballCriteria lastCriteria = new SnowballCriteria();
	private JComboBox cmbbxExpansionMode;
	private JSpinner spnrSeedReferenceYear;
	private JSpinner spnrMaxStepCount;
	private JComboBox cmbxRelationModelName;
	private JComboBox cmbxEgoRoleName;
	private JComboBox cmbxAlternativeEgoRoleName;
	private JComboBox cmbxSeedLabel;
	private JComboBox cmbxReachLabel;
	private JComboBox cmbxIndirectLabel;
	private JTextField txtfldSeedValue;
	private RelationModels relationModels;

	/**
	 * Create the dialog.
	 */
	public SnowballStructureInputDialog(final RelationModels relationModels, final AttributeDescriptors attributeDescriptors) {
		super();

		this.relationModels = relationModels;
		
		//
		List<String> targetLabels = new ArrayList<String>();
		if (relationModels != null) {
			//
			for (String name : relationModels.nameList()) {
				//
				targetLabels.add(name);
			}
		}
	
		
		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Snowball Structure Input Dialog");
		setIconImage(Toolkit.getDefaultToolkit().getImage(SnowballStructureInputDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				SnowballStructureInputDialog.this.dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 400, 270);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		{
			JLabel lblRelationmodelname = new JLabel("RelationModelName:");
			contentPanel.add(lblRelationmodelname, "2, 2, right, default");
		}
		{
			this.cmbxRelationModelName = new JComboBox(targetLabels.toArray());
			contentPanel.add(this.cmbxRelationModelName, "4, 2, fill, default");
			this.cmbxRelationModelName.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(final ItemEvent event) {
					// Update label list.
					//
					if (event.getStateChange() == ItemEvent.SELECTED) {
						//
						updateRelationModel();
					}
				}
			});
		}
		{
			JLabel lblEgoRoleName = new JLabel("Active Ego Role:");
			contentPanel.add(lblEgoRoleName, "2, 4, right, default");
		}
		{
			this.cmbxEgoRoleName = new JComboBox();
			contentPanel.add(this.cmbxEgoRoleName, "4, 4, fill, default");
		}
		{
			JLabel lblAlternativeEgoRoleName = new JLabel("Passive Ego Role:");
			contentPanel.add(lblAlternativeEgoRoleName, "2, 6, right, default");
		}
		{
			this.cmbxAlternativeEgoRoleName = new JComboBox();
			contentPanel.add(this.cmbxAlternativeEgoRoleName, "4, 6, fill, default");
		}
		{
			{
				JLabel lblSeedLabel = new JLabel("Seed Label:");
				this.contentPanel.add(lblSeedLabel, "2, 8, right, default");
			}
		}
		{
			this.cmbxSeedLabel = new JComboBox();
			this.contentPanel.add(this.cmbxSeedLabel, "4, 8, fill, default");
		}
		{
			JLabel lblSeedValue = new JLabel("Seed Value:");
			this.contentPanel.add(lblSeedValue, "2, 10, right, default");
		}
		{
			this.txtfldSeedValue = new JTextField();
			this.contentPanel.add(this.txtfldSeedValue, "4, 10, fill, default");
			this.txtfldSeedValue.setColumns(10);
		}
		{
			JLabel lblReachLabel = new JLabel("Reach Label:");
			this.contentPanel.add(lblReachLabel, "2, 12, right, default");
		}
		{
			this.cmbxReachLabel = new JComboBox();
			this.contentPanel.add(this.cmbxReachLabel, "4, 12, fill, default");
		}
		{
			JLabel lblIndirectLabel = new JLabel("Indirect Label:");
			contentPanel.add(lblIndirectLabel, "2, 14, right, default");
		}
		{
			this.cmbxIndirectLabel = new JComboBox();
			this.contentPanel.add(this.cmbxIndirectLabel, "4, 14, fill, default");
		}
		{
			JLabel lblExpansionmode = new JLabel("ExpansionMode:");
			this.contentPanel.add(lblExpansionmode, "2, 16, right, default");
		}
		{
			this.cmbbxExpansionMode = new JComboBox(ExpansionMode.getActiveLabels().toArray());
			this.contentPanel.add(this.cmbbxExpansionMode, "4, 16, fill, default");
		}
		{
			JLabel lblMaxStepCount = new JLabel("Max Step Count:");
			this.contentPanel.add(lblMaxStepCount, "2, 19, right, default");
		}
		{
			this.spnrMaxStepCount = new JSpinner();
			this.spnrMaxStepCount.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
			this.contentPanel.add(this.spnrMaxStepCount, "4, 19");
		}
		{
			JLabel lblSeedReferenceYear = new JLabel("Seed Reference Year:");
			this.contentPanel.add(lblSeedReferenceYear, "2, 21, right, default");
		}
		{
			this.spnrSeedReferenceYear = new JSpinner();
			this.spnrSeedReferenceYear.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
			this.contentPanel.add(this.spnrSeedReferenceYear, "4, 21");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						SnowballStructureInputDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("Launch");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						SnowballCriteria criteria = getCriteria();

						//
						lastCriteria = criteria;
						SnowballStructureInputDialog.this.dialogCriteria = criteria;

						//
						setVisible(false);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}

		// ////////////////////////
		Object[] labels = attributeDescriptors.labelsSorted().toArray();
		this.cmbxSeedLabel.setModel(new DefaultComboBoxModel(labels));
		this.cmbxReachLabel.setModel(new DefaultComboBoxModel(labels));
		this.cmbxIndirectLabel.setModel(new DefaultComboBoxModel(labels));
		setCriteria(lastCriteria);
	}

	
	private void updateRelationModel() {
		//
		RelationModel currentRelationModel = this.relationModels.getByName((String) this.cmbxRelationModelName.getSelectedItem());

		StringList currentRoles;
		if (currentRelationModel == null) {
			//
			currentRoles = new StringList();

		} else {
			//
			currentRoles = currentRelationModel.roles().toNameList().sort();
			currentRoles.add("");
		}
		
		//
		if (currentRoles.isEmpty()) {
			//
			this.cmbxEgoRoleName.setEnabled(false);
			this.cmbxAlternativeEgoRoleName.setEnabled(false);

		} else {
			//
			this.cmbxEgoRoleName.setEnabled(true);
			this.cmbxEgoRoleName.setModel(new DefaultComboBoxModel(currentRoles.toArray()));
			this.cmbxAlternativeEgoRoleName.setEnabled(true);
			this.cmbxAlternativeEgoRoleName.setModel(new DefaultComboBoxModel(currentRoles.toArray()));
		}

	}

	/**
	 * 
	 * @return
	 */
	public SnowballCriteria getCriteria() {
		SnowballCriteria result;

		result = new SnowballCriteria();

		//
		result.setRelationModelName((String) this.cmbxRelationModelName.getSelectedItem());

		//
		result.setEgoRoleName((String) this.cmbxEgoRoleName.getSelectedItem());

		//
		result.setAlternativeEgoRoleName((String) this.cmbxAlternativeEgoRoleName.getSelectedItem());

		//
		result.setSeedLabel((String) this.cmbxSeedLabel.getSelectedItem());

		//
		result.setSeedValue(StringUtils.trim(this.txtfldSeedValue.getText()));

		//
		result.setReachLabel((String) this.cmbxReachLabel.getSelectedItem());

		//
		result.setIndirectLabel((String) this.cmbxIndirectLabel.getSelectedItem());

		//
		result.setExpansionMode(ExpansionMode.valueOf((String) this.cmbbxExpansionMode.getSelectedItem()));

		//
		result.setMaxNrSteps((Integer) this.spnrMaxStepCount.getValue());

		//
		result.setSeedReferenceYear((Integer) this.spnrSeedReferenceYear.getValue());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public SnowballCriteria getDialogCriteria() {
		SnowballCriteria result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final SnowballCriteria source) {
		//
		if (source != null) {

			//
			this.cmbxRelationModelName.setSelectedItem(source.getRelationModelName());

			//
			this.cmbxEgoRoleName.setSelectedItem(source.getEgoRoleName());

			//
			this.cmbxAlternativeEgoRoleName.setSelectedItem(source.getAlternativeEgoRoleName());

			//
			this.cmbxSeedLabel.setSelectedItem(source.getSeedLabel());

			//
			this.txtfldSeedValue.setText(source.getSeedValue());

			//
			this.cmbxReachLabel.setSelectedItem(source.getReachLabel());

			//
			this.cmbxIndirectLabel.setSelectedItem(source.getIndirectLabel());

			//
			this.cmbbxExpansionMode.setSelectedItem(source.getExpansionMode().name());

			//
			this.spnrMaxStepCount.setValue(source.getMaxNrSteps());

			//
			this.spnrSeedReferenceYear.setValue(source.getSeedReferenceYear());
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		/* AttributeFilter criteria = */showDialog(null, null);
	}

	/**
	 * Launch the application.
	 */
	public static SnowballCriteria showDialog(final RelationModels relationModels, final AttributeDescriptors attributeDescriptors) {
		SnowballCriteria result;

		//
		SnowballStructureInputDialog dialog = new SnowballStructureInputDialog(relationModels,attributeDescriptors);
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
