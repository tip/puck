package oldcore.trash;

import java.util.Collection;
import java.util.List;
import java.util.TreeMap;



/**
 * This class maps elements to integer values
 * @author Klaus Hamberger
 * @param <T> the class of the key elements
 */
@SuppressWarnings("serial")
public class CountMap<T> extends TreeMap<T,Integer> {

	/**
	 * the default constructor
	 * @see trash.ArcMap#ArcMap(elements.nodes.Node, String)
	 * @see util.Enum#combine(int,int)
	 */
/*	public CountMap (){
		super();
	}*/
	
	/**
	 * the standard constructor for a CountMap with key type t
	 * @param k the key type
	 * @see maps.groupmaps.RingGroupMap#getFrequencies()
	 */
/*	public CountMap (int k){
		super(new VectorComparator(k));
//		super(new VectorComparator<T>());
	}*/
	
	/**
	 * augments the value of the key by 1 or adds it to the map if it is not yet contained in it
	 * @param t the key (element to be counted)
	 * @see maps.groupmaps.RingGroupMap#countBridges(Vertex,Vertex,CountMap<int[]>)
	 */
/*	public void add (T t){
		add(t,1);
	}*/

	/**
	 * diminishes the value of the key by k and (optional) removes keys with negative or zero values
	 * @param t  the key
	 * @param k the value to be substracted
	 * @param nonneg nonnegativity condition: if true, keys with negative or zero values will be removed
	 * @since 11-07-02 
	 */
/*	public void diminish (T t, int k, boolean pos){
		if (get(t)==null) return;
		put(t,get(t)-k);
		if (pos && get(t)<=0) remove(t);
	}*/
	
	/**
	 * diminishes the value of the key by 1 and removes keys with negative or zero values
	 * @param t the key
	 * @since 11-07-02 
	 */
/*	public void diminish (T t){
		diminish (t,1,true);
	}*/
	
	/**
	 * augments the value of the key by k or adds it if it is not yet in the map
	 * @param t the key
	 * @param k the value increment
	 * @see maps.CountMap#add(int)
	 * @see elements.nodes.GroupNode#setLink(elements.nodes.GroupNode, int)
	 */
/*	public void add (T t, int k){
		if (get(t)==null) put(t,k);
		else put(t,get(t)+k);
	}
	
	public void count (T t){
		Integer i = get(t);
		if (i==null) i=0;
		put(t,i+1);
	}*/
	
	/**
	 * divides the values by a constant
	 * @param i the divisor
	 * @see maps.groupmaps.RingGroupMap#getFrequencies()
	 */
/*	public void div (int i){
		for (T t : keySet()){
			put(t,get(t)/i);
		}
	}*/
	
	/**
	 * transforms the map into a list of arcs
	 * @param e the index of ego
	 * @param arcs the arc list
	 * @see elements.nodes.GroupNode#getArcs(List, int)
	 * @since last modified 11-05-21
	 */
/*	public void getArcs (int e, List<Arc> arcs) {
		for (T t : keySet()){
			int a = ((Node)t).getCurrent();
			if (a>0 && get(t)>0) arcs.add(new Arc(e,a,get(t),""));
		}
	}*/

	/**
	 * sums up the values of all the elements in the map
	 * @return the value sum
	 * @see maps.Net#nrTies(CountMap)
	 * @see gui.TipWriter#writeSurvey(RingGroupMap, CountMap<int[]>, int)
	 */
/*	public int sum (){
		int i = 0;
		for (int j : values()){
			i = i+j;
		}
		return i;
	}*/
	
/*	public Integer get(Object key){
		if (super.get(key)==null) return 0;
		return super.get(key);
	}*/
	
}
