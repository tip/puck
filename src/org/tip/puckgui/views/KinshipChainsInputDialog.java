package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.tip.puck.census.workers.ChainValuator;
import org.tip.puck.census.workers.KinshipChainsCriteria;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class KinshipChainsInputDialog extends JDialog {

	private static final long serialVersionUID = -4525810575253517598L;
	private final JPanel contentPanel = new JPanel();
	private static KinshipChainsCriteria defaultCriteria = new KinshipChainsCriteria();;
	private KinshipChainsCriteria inputedCriteria;
	private JTextField txtfldAlterId;
	private JTextField txtfldMaximalDepth;
	private JTextField txtfldMaximalOrder;
	private JComboBox cmbbxChainClassification;

	/**
	 * Create the dialog.
	 */
	public KinshipChainsInputDialog() {
		super();
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Kinship Chains Input");
		setIconImage(Toolkit.getDefaultToolkit().getImage(KinshipChainsInputDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				inputedCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 284, 194);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));
		{
			JLabel lblAlterId = new JLabel("Alter Id:");
			contentPanel.add(lblAlterId, "2, 2, right, default");
		}
		{
			txtfldAlterId = new JTextField();
			contentPanel.add(txtfldAlterId, "4, 2, fill, default");
			txtfldAlterId.setColumns(10);
		}
		{
			JLabel lblMaximalDepth = new JLabel("Maximal Depth:");
			contentPanel.add(lblMaximalDepth, "2, 4, right, default");
		}
		{
			txtfldMaximalDepth = new JTextField();
			contentPanel.add(txtfldMaximalDepth, "4, 4, fill, default");
			txtfldMaximalDepth.setColumns(10);
		}
		{
			JLabel lblMaximalOrder = new JLabel("Maximal Order:");
			contentPanel.add(lblMaximalOrder, "2, 6, right, default");
		}
		{
			txtfldMaximalOrder = new JTextField();
			contentPanel.add(txtfldMaximalOrder, "4, 6, fill, default");
			txtfldMaximalOrder.setColumns(10);
		}
		{
			JLabel lblChainClassification = new JLabel("Chain Classification:");
			contentPanel.add(lblChainClassification, "2, 8, right, default");
		}
		{
			cmbbxChainClassification = new JComboBox();
			cmbbxChainClassification.setModel(new DefaultComboBoxModel(new String[] { "SIMPLE" }));
			contentPanel.add(cmbbxChainClassification, "4, 8, fill, default");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						inputedCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						if ((!NumberUtils.isNumber(txtfldAlterId.getText().trim())) || (!NumberUtils.isNumber(txtfldMaximalDepth.getText().trim()))
								|| (!NumberUtils.isNumber(txtfldMaximalOrder.getText().trim()))) {
							//
							String title = "Error computerum est";
							String message = "Please, enter numeric value.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						} else {
							//
							inputedCriteria = new KinshipChainsCriteria();
							inputedCriteria.setAlterId(Integer.parseInt(txtfldAlterId.getText().trim()));
							inputedCriteria.setMaximalDepth(Integer.parseInt(txtfldMaximalDepth.getText().trim()));
							inputedCriteria.setMaximalOrder(Integer.parseInt(txtfldMaximalOrder.getText().trim()));
							inputedCriteria.setChainClassification((String) cmbbxChainClassification.getSelectedItem());

							//
							defaultCriteria = inputedCriteria;

							//
							setVisible(false);
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}

		// ////////////////
		cmbbxChainClassification.setModel(new DefaultComboBoxModel());
		setCriteria(defaultCriteria);
	}

	/**
	 * 
	 * @return
	 */
	public KinshipChainsCriteria getCriteria() {
		KinshipChainsCriteria result;

		result = inputedCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final KinshipChainsCriteria source) {
		//
		if (source != null) {
			//
			if (source.getAlterId() == 0) {
				txtfldAlterId.setText("");
			} else {
				txtfldAlterId.setText(String.valueOf(source.getAlterId()));
			}

			//
			if (source.getMaximalDepth() == 0) {
				txtfldMaximalDepth.setText("");
			} else {
				txtfldMaximalDepth.setText(String.valueOf(source.getMaximalDepth()));
			}

			//
			if (source.getMaximalOrder() == 0) {
				txtfldMaximalOrder.setText("");
			} else {
				txtfldMaximalOrder.setText(String.valueOf(source.getMaximalOrder()));
			}

			//
			String[] endogenousLabels = ChainValuator.getEndogenousLabels();
			cmbbxChainClassification.setModel(new DefaultComboBoxModel(endogenousLabels));

			int chainClassificationLabelIndex = ArrayUtils.indexOf(endogenousLabels, source.getChainClassification());
			if (chainClassificationLabelIndex == -1) {
				chainClassificationLabelIndex = ArrayUtils.indexOf(endogenousLabels, "SIMPLE");
			}
			cmbbxChainClassification.setSelectedIndex(chainClassificationLabelIndex);
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		KinshipChainsCriteria criteria = showDialog();
		System.out.println("return=" + criteria.getAlterId());
		System.out.println("return=" + criteria.getMaximalDepth());
		System.out.println("return=" + criteria.getMaximalOrder());
		System.out.println("return=" + criteria.getChainClassification());
	}

	/**
	 * Launch the application.
	 */
	public static KinshipChainsCriteria showDialog() {
		KinshipChainsCriteria result;

		//
		KinshipChainsInputDialog dialog = new KinshipChainsInputDialog();
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getCriteria();

		//
		return result;
	}
}
