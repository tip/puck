package org.tip.puckgui.views;

import org.tip.puck.net.workers.AttributeWorker.EmptyType;
import org.tip.puck.net.workers.AttributeWorker.Scope;

/**
 * 
 * @author TIP
 */
public class RemoveAttributeCriteria {

	private Scope scope;
	private String label;
	private EmptyType type;
	private String optionalRelationName;

	/**
	 * 
	 */
	public RemoveAttributeCriteria() {
		this.scope = Scope.ALL;
		this.label = null;
		this.type = EmptyType.VOID;
		this.optionalRelationName = null;
	}

	public String getLabel() {
		return label;
	}

	public String getOptionalRelationName() {
		return optionalRelationName;
	}

	public Scope getScope() {
		return scope;
	}

	public EmptyType getType() {
		return type;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public void setOptionalRelationName(final String optionalRelationName) {
		this.optionalRelationName = optionalRelationName;
	}

	public void setScope(final Scope scope) {
		this.scope = scope;
	}

	public void setType(final EmptyType type) {
		this.type = type;
	}
}