package org.tip.puck.net.relations;

import java.util.Comparator;

import org.tip.puck.net.Individual;
import org.tip.puck.util.MathUtils;

/**
 * This class compares two relations with one actor in common relative with an
 * role.
 * 
 * 
 * @author TIP
 * 
 */
public class RelationOrderComparator implements Comparator<Relation> {

	private Individual referent;
	private Role role;

	/**
	 * 
	 * @param sorting
	 */
	public RelationOrderComparator(final Individual referent, final Role role) {
		//
		if (referent == null) {
			//
			throw new IllegalArgumentException("referent is null.");

		} else if (role == null) {
			//
			throw new IllegalArgumentException("role is null.");

		} else {
			//
			this.referent = referent;
		}
	}

	/**
	 * 
	 */
	@Override
	public int compare(final Relation alpha, final Relation bravo) {
		int result;

		result = compare(alpha, bravo, this.referent, this.role);

		//
		return result;
	}

	/**
	 * 
	 */
	public static int compare(final Relation alpha, final Relation bravo, final Individual referent, final Role role) {
		int result;

		if (referent == null) {
			throw new IllegalArgumentException("referent is null.");
		} else if (role == null) {
			throw new IllegalArgumentException("role is null.");
		} else {
			//
			Integer alphaValue;
			if (alpha == null) {
				//
				alphaValue = null;

			} else {
				//
				Actors actors = alpha.actors().getByRole(role).getById(referent.getId());

				if (actors.isEmpty()) {
					//
					alphaValue = null;

				} else {
					//
					alphaValue = actors.get(0).getRelationOrder();
				}
			}

			//
			Integer bravoValue;
			if (bravo == null) {
				//
				bravoValue = null;

			} else {
				//
				Actors actors = bravo.actors().getByRole(role).getById(referent.getId());

				if (actors.isEmpty()) {
					//
					bravoValue = null;

				} else {
					//
					bravoValue = actors.get(0).getRelationOrder();
				}
			}

			//
			result = MathUtils.compareOrder(alphaValue, bravoValue);
		}

		//
		return result;
	}
}
