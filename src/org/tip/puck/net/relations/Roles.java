package org.tip.puck.net.relations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class Roles extends ArrayList<Role> {

	private static final long serialVersionUID = 8486402856820979867L;

	/**
	 * 
	 */
	public Roles() {
		super();
	}

	/**
	 * 
	 * @param source
	 */
	public Roles(final Roles source) {
		if (source != null) {
			for (Role role : source) {
				this.add(new Role(role));
			}
		}
	}
	
	public Roles clone(){
		Roles result;
		
		result = new Roles();
		for (Role role : this){
			result.add(role.clone());
		}
		
		//
		return result;
	}

	/**
	 * 
	 * @param role
	 */
	public boolean exists(final String roleName) {
		boolean result;

		if (getByName(roleName) == null) {
			result = false;
		} else {
			result = true;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Role getByName(final String name) {
		Role result;

		if (name == null) {
			//
			result = null;

		} else {
			//
			boolean ended = false;
			result = null;
			Iterator<Role> iterator = this.iterator();
			while (!ended) {
				//
				if (iterator.hasNext()) {
					//
					Role role = iterator.next();
					if (StringUtils.equals(role.getName(), name)) {
						//
						ended = true;
						result = role;
					}
				} else {
					//
					ended = true;
					result = null;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public StringList toNameList() {
		StringList result;

		result = new StringList(this.size());
		for (Role role : this) {
			result.add(role.getName());
		}

		//
		return result;
	}


	/**
	 * 
	 * @return
	 */
	public StringList toSortedNameList() {
		StringList result;

		result = toNameList();
		Collections.sort(result);

		//
		return result;
	}
	
	public List<Role> toSortedList(){
		List<Role> result;
		
		result = new ArrayList<Role>(this);
		
		Collections.sort(result);
		
		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Role[] toArray() {
		Role[] result;

		result = new Role[this.size()];
		int roleCount = 0;
		for (Role role : this) {
			result[roleCount] = role;
			roleCount += 1;
		}

		//
		return result;
	}

	@Override
	public String toString() {
		return Arrays.toString(toArray());
//		return Arrays.toString(toSortedNameList().toArray());
	}

	public static boolean equalOrBothNull(final Roles alpha, final Roles beta) {
		return (alpha == null && beta == null) || (alpha != null && alpha.equals(beta));
	}
	
	public boolean addNew (Role role){
		boolean result;
		
		if (contains(role)){
			result = false;
		} else {
			result = add(role);
		}
		//
		return result;
	}

}
