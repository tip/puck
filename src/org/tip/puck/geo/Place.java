package org.tip.puck.geo;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.vividsolutions.jts.geom.Coordinate;

import fr.devinsy.util.StringList;
import fr.devinsy.util.StringSet;

/**
 * 
 * The <i>externalId</i> can be GeoNames one.
 * 
 */
public class Place {

	private String toponym;
	private AlternateToponyms alternateToponyms;
	private GeoLevel geoLevel;
	private Coordinate coordinate;
	private Coordinate2 coordinate2;
	private String extraData;
	private String comment;
	private String superiorToponym;
	private Place superiorPlace;
	private String shortName;
	
	String geonames1, geonames2, geonames3;
	String countryISO2;
	Integer idGeonames;


	/** 
	 * 
	 */
	public Place(final String toponym) {
		this.toponym = toponym;
		this.extraData = null;
		this.alternateToponyms = new AlternateToponyms();
		this.geoLevel = GeoLevel.LOCAL;
	}
	
	public Place(final GeoLevel level, final String shortName, final Place superiorPlace){
		this.shortName = shortName;
		this.superiorPlace = superiorPlace;
		this.toponym = getToponym(shortName,level,superiorPlace);
		this.extraData = null;
		this.alternateToponyms = new AlternateToponyms();
		this.geoLevel = level;
	}

	public String getSuperiorToponym() {
		return this.superiorToponym;
	}

	public String getComment() {
		return this.comment;
	}
	
	public Coordinate getCoordinate() {
		return coordinate;
	}

	public Coordinate2 getCoordinate2() {
		return this.coordinate2;
	}
	

	public String getCountry_ISO2() {
		return countryISO2;
	}


	/**
	 * 
	 * @return
	 */
	public Double getElevation() {
		Double result;

		if (this.coordinate2 == null) {

			result = null;

		} else {

			result = this.coordinate2.getElevation();
		}

		//
		return result;
	}

	public String getExtraData() {
		return this.extraData;
	}

	public GeoLevel getGeoLevel() {
		return this.geoLevel;
	}

	public AlternateToponyms getHomonyms() {
		return this.alternateToponyms;
	}
	
	public String getGeonames1() {
		return geonames1;
	}

	public void setGeonames1(String geonames1) {
		this.geonames1 = geonames1;
	}

	public String getGeonames2() {
		return geonames2;
	}

	public Integer getIdGeonames() {
		return idGeonames;
	}



	/**
	 * 
	 * @return
	 */
	public Double getLatitude() {
		Double result;

		if (this.coordinate2 == null) {

			result = null;

		} else {

			result = this.coordinate2.getLatitude();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Double getLongitude() {
		Double result;

		if (this.coordinate2 == null) {

			result = null;

		} else {

			result = this.coordinate2.getLongitude();
		}

		//
		return result;
	}

	public String getToponym() {
		return this.toponym;
	}

	/**
	 * 
	 * @return
	 */
	public StringList getToponyms() {
		StringList result;

		result = new StringList();

		result.add(this.toponym);
		result.addAll(this.getHomonyms());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isGeocoded() {
		boolean result;

		if (this.coordinate2 == null) {
			result = false;
		} else {
			result = true;
		}

		//
		return result;
	}

	public void setSuperiorToponym(final String abovePlace) {
		this.superiorToponym = abovePlace;
	}

	/**
	 * 
	 * @param homonyms
	 */
	public void setAlternateToponyms(final String homonyms) {

		if (StringUtils.isBlank(homonyms)) {

			this.alternateToponyms.clear();

		} else {
			this.alternateToponyms.clear();
			for (String homonym : homonyms.split("[;]")) {

				this.alternateToponyms.add(homonym);
			}
		}
	}

	/**
	 * 
	 * @param alternateToponyms
	 */
	public void setAlternateToponyms(final StringSet source) {

		if (source == null) {

			this.alternateToponyms.clear();

		} else {

			this.alternateToponyms.clear();
			for (String homonym : source.toStringList().sort()) {

				if (!StringUtils.equals(this.toponym, homonym)) {

					this.alternateToponyms.add(homonym);
				}
			}
		}
	}

	public void setComment(final String comment) {
		this.comment = comment;
	}

	public void setCoordinate2(final Coordinate2 coordinate) {
		this.coordinate2 = coordinate;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public Place setElevation(final Double value) {

		if (value == null) {

			this.coordinate2 = null;

		} else {

			if (this.coordinate2 == null) {

				this.coordinate2 = new Coordinate2();
			}

			this.coordinate2.setElevation(value);
		}

		//
		return this;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public Place setElevation(final Integer value) {
		Place result;

		if (value == null) {

			result = setElevation((Double) null);
		} else {

			result = setElevation(Double.valueOf(value));
		}

		//
		return result;
	}

	public void setExtraData(final String extraData) {
		this.extraData = extraData;
	}

	public void setGeoLevel(final GeoLevel geoLevel) {
		this.geoLevel = geoLevel;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public Place setLatitude(final Double value) {

		if (value == null) {

			this.coordinate2 = null;

		} else {

			if (this.coordinate2 == null) {

				this.coordinate2 = new Coordinate2();
			}

			this.coordinate2.setLatitude(value);
		}

		//
		return this;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public Place setLongitude(final Double value) {

		if (value == null) {

			this.coordinate2 = null;

		} else {

			if (this.coordinate2 == null) {

				this.coordinate2 = new Coordinate2();
			}

			this.coordinate2.setLongitude(value);
		}

		//
		return this;
	}

	/**
	 * 
	 * @param toponym
	 */
	public void setToponym(final String toponym) {
		this.toponym = toponym;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		result = this.toponym;

		//
		return result;
	}

	public Place getSuperiorPlace() {
		return superiorPlace;
	}

	public void setSuperiorPlace(Place superiorPlace) {
		this.superiorPlace = superiorPlace;
	}
	
	public String getBitoponym() {
		String result;

		if (this.superiorPlace == null){
			result = this.toponym;
		} else {
			result = this.toponym + " / " + this.superiorPlace.toponym;
		}
		//
		return result;
	}

	public String getShortName() {
		return shortName;
	}
	
	public static String getToponym (String shortName, GeoLevel level, Place supPlace){
		String result;
		
		switch (level){
		case INTERCONTINENTAL:
			result = shortName;
			break;
		case CONTINENT:
			result = shortName;
			break;
		case COUNTRY:
			result = shortName;
			break;
		case DEPARTMENT:
			result = shortName+" (Dept.)";
			break;
		case TOWNSHIP:
			if (supPlace!=null && supPlace.getGeoLevel()==GeoLevel.DEPARTMENT){
				result = shortName+", "+supPlace.getShortName();
			} else {
				result = shortName;
			}
			break;
		case TOWN:
			if (supPlace!=null && supPlace.getGeoLevel()==GeoLevel.TOWNSHIP){
				Place dept = supPlace.getSuperiorPlace();
				if (dept!=null && dept.getGeoLevel()==GeoLevel.DEPARTMENT){
					result = shortName+" ("+supPlace.getShortName()+", "+dept.getShortName()+")";
				} else {
					result = shortName+" ("+supPlace.getShortName()+")";
				}
			} else {
				result = shortName;
			}
			break;
		case QUARTER:
			if (supPlace!=null && supPlace.getGeoLevel()==GeoLevel.TOWN){
				result = supPlace.getShortName()+"-"+shortName;
			} else {
				result = shortName;
			}
			break;
		case SUBQUARTER:
			if (supPlace!=null && supPlace.getGeoLevel()==GeoLevel.QUARTER){
				Place town = supPlace.getSuperiorPlace();
				if (town!=null && town.getGeoLevel()==GeoLevel.TOWN){
					result = town.getShortName()+"-"+supPlace.getShortName()+"-"+shortName;
				} else {
					result = supPlace.getShortName()+"-"+shortName;
				}
			} else {
				result = shortName;
			}
			break;
			default: 
				result = shortName;
		}
		
		//
		return result;
	}
	
	public int compareByLevel(Place place){
		int result;
		
		result = this.geoLevel.compareTo(place.geoLevel);
		
		//
		return result;
	}
	
	public Place getSuperiorPlace(List<String> placenames){
		Place result;
		
		result = this;
		while (placenames != null && !placenames.isEmpty() && !placenames.contains(result.getShortName())){
			result = result.getSuperiorPlace();
		}
		//
		return result;
	}
	
	public Place atLevel(GeoLevel level) {
		Place result;
		
		result = this;
		
		if (level != GeoLevel.LOCAL){
			
			while (result != null && result.getGeoLevel() != level){
				result = result.getSuperiorPlace();
			}
			//
		}
		
		return result;
	}
	
	public Place (String name, Coordinate coord){
		this(name);
		this.coordinate = coord;
	}
	
	
	public Place (Integer idGeonames, String name, String geonames1, String geonames2, String geonames3, String countryCode, Coordinate coord) {
		this(name, coord);
		this.geonames1 = geonames1;
		this.geonames2 = geonames2;
		this.geonames3 = geonames3;
		this.countryISO2 = countryCode;
		this.idGeonames = idGeonames;
	}
	
	
	
}
