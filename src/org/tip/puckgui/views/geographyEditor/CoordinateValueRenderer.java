package org.tip.puckgui.views.geographyEditor;

import java.awt.Component;
import java.text.DecimalFormat;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class CoordinateValueRenderer extends DefaultTableCellRenderer {

	private static final long serialVersionUID = -283182938904460364L;

	// 5 decimals ~ 1m
	// 6 decimals ~ 11cm
	// 7 decimals ~ 1cm
	private static final DecimalFormat formatter = new DecimalFormat("#.#######");

	/**
 	 * 
 	 */
	public CoordinateValueRenderer() {
		super();
		formatter.setMinimumFractionDigits(7);
		setHorizontalAlignment(JLabel.RIGHT);
	}

	/**
	 * 
	 */
	@Override
	public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row,
			final int column) {
		Component result;

		// First format the cell value as required.
		Object targetValue;
		if (value instanceof Double) {

			targetValue = formatter.format(value);
		} else {

			targetValue = value;
		}

		result = super.getTableCellRendererComponent(table, targetValue, isSelected, hasFocus, row, column);

		//
		return result;
	}
}
