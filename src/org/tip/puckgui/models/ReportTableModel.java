package org.tip.puckgui.models;

import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;

import org.tip.puck.report.ReportTable;

/**
 * 
 * @author TIP
 */
public class ReportTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 6545691079892650773L;

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.tip.puckgui.messages");

	protected ReportTable source;

	/**
	 * 
	 */
	public ReportTableModel(final ReportTable source) {
		super();
		this.source = source;
	}

	/**
	 * 
	 */
	@Override
	public Class getColumnClass(final int columnIndex) {
		Class result;

		switch (columnIndex) {
			case 0:
				result = String.class;
			break;
			case 1:
				result = String.class;
			break;
			default:
				result = String.class;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getColumnCount() {
		int result;

		result = source.getColumnCount();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String getColumnName(final int columnIndex) {
		String result;

		switch (columnIndex) {
			case 0:
				// result = BUNDLE.getString("MainWindow.individual.id");
				result = "Label";
			break;
			case 1:
				result = "Value";
			break;
			default:
				result = "";
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getRowCount() {
		int result;

		if (this.source == null) {
			result = 0;
		} else {
			result = this.source.getRowCount();
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Object getValueAt(final int rowIndex, final int columnIndex) {
		Object result;

		if (this.source == null) {
			result = null;
		} else {
			result = this.source.get(rowIndex, columnIndex);
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public boolean isCellEditable(final int row, final int col) {
		boolean result;

		result = false;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void setSource(final ReportTable source) {
		this.source = source;
		fireTableStructureChanged();
	}

	/**
	 * 
	 * @return
	 */
	public ReportTable source() {
		ReportTable result;

		result = this.source;

		//
		return result;
	}
}
