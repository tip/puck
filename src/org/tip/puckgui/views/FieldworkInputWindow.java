package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.KinType;
import org.tip.puck.net.random.RandomNetExplorer;
import org.tip.puck.net.random.RandomNetReporter;
import org.tip.puck.net.workers.MemoryCriteria;
import org.tip.puck.report.Report;
import org.tip.puck.statistics.StatisticsWorker;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.PuckGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

public class FieldworkInputWindow extends JFrame {

	private static final long serialVersionUID = 2736899602941682669L;
	private static final Logger logger = LoggerFactory.getLogger(AllianceNetworkInputWindow.class);

	private JFrame thisJFrame;
	private NetGUI netGUI;
	private JPanel contentPane;
	private JSlider mmF;
	private JSlider mmM;
	private JSlider mmS;
	private JSlider mmD;
	private JSlider mmW;
	private JSlider fmF;
	private JSlider fmM;
	private JSlider fmS;
	private JSlider fmD;
	private JSlider fmW;
	private JSlider mfF;
	private JSlider mfM;
	private JSlider mfS;
	private JSlider mfD;
	private JSlider mfH;
	private JSlider ffF;
	private JSlider ffM;
	private JSlider ffS;
	private JSlider ffD;
	private JSlider ffH;
	private JSlider mAccept;
	private JSlider fAccept;
	private JSpinner nrInformantsSpinner;
	private JLabel lblNewLabel;
	private JLabel lblMother;
	private JLabel lblSon;
	private JLabel lblMother_1;
	private JLabel lblSon_1;
	private JLabel lblKinProximity;
	private JComboBox cbBoxDistanceType;
	private JLabel lblKinDegree;
	private JSpinner spinnerKinDegree;
	private JLabel lblNearKinPropensity;
	private JSpinner spinnerDistanceWeight;
	private JLabel lbMemory;
	private JSpinner spinnerDistanceFactor;

	/**
	 * Create the frame.
	 */
	public FieldworkInputWindow(final NetGUI netGUI) {

		this.thisJFrame = this;
		this.netGUI = netGUI;
		setTitle("Virtual Fieldwork");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 656);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		this.contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

		JButton btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				// provisory, non-parametrized method
				try {
					Report report = new Report();

					MemoryCriteria criteria = new MemoryCriteria();

					// inf ego rel

					criteria.setMemoryProb(Gender.MALE, Gender.MALE, KinType.PARENT, Gender.MALE, decimal(FieldworkInputWindow.this.mmF.getValue()));
					criteria.setMemoryProb(Gender.FEMALE, Gender.MALE, KinType.PARENT, Gender.MALE, decimal(FieldworkInputWindow.this.mfF.getValue()));
					criteria.setMemoryProb(Gender.MALE, Gender.FEMALE, KinType.PARENT, Gender.MALE, decimal(FieldworkInputWindow.this.mmM.getValue()));
					criteria.setMemoryProb(Gender.FEMALE, Gender.FEMALE, KinType.PARENT, Gender.MALE, decimal(FieldworkInputWindow.this.mfM.getValue()));
					criteria.setMemoryProb(Gender.MALE, Gender.MALE, KinType.CHILD, Gender.MALE, decimal(FieldworkInputWindow.this.mmS.getValue()));
					criteria.setMemoryProb(Gender.FEMALE, Gender.MALE, KinType.CHILD, Gender.MALE, decimal(FieldworkInputWindow.this.mfS.getValue()));
					criteria.setMemoryProb(Gender.MALE, Gender.FEMALE, KinType.CHILD, Gender.MALE, decimal(FieldworkInputWindow.this.mmD.getValue()));
					criteria.setMemoryProb(Gender.FEMALE, Gender.FEMALE, KinType.CHILD, Gender.MALE, decimal(FieldworkInputWindow.this.mfD.getValue()));
					criteria.setMemoryProb(Gender.MALE, Gender.FEMALE, KinType.SPOUSE, Gender.MALE, decimal(FieldworkInputWindow.this.mmW.getValue()));
					// criteria.setWomansWifeByMale(decimal(mfW.getValue()));
					// criteria.setMansHusbandByMale(decimal(mmH.getValue()));
					criteria.setMemoryProb(Gender.FEMALE, Gender.MALE, KinType.SPOUSE, Gender.MALE, decimal(FieldworkInputWindow.this.mfH.getValue()));
					criteria.setMemoryProb(Gender.MALE, Gender.MALE, KinType.PARENT, Gender.FEMALE, decimal(FieldworkInputWindow.this.fmF.getValue()));
					criteria.setMemoryProb(Gender.FEMALE, Gender.MALE, KinType.PARENT, Gender.FEMALE, decimal(FieldworkInputWindow.this.ffF.getValue()));
					criteria.setMemoryProb(Gender.MALE, Gender.FEMALE, KinType.PARENT, Gender.FEMALE, decimal(FieldworkInputWindow.this.fmM.getValue()));
					criteria.setMemoryProb(Gender.FEMALE, Gender.FEMALE, KinType.PARENT, Gender.FEMALE, decimal(FieldworkInputWindow.this.ffM.getValue()));
					criteria.setMemoryProb(Gender.MALE, Gender.MALE, KinType.CHILD, Gender.FEMALE, decimal(FieldworkInputWindow.this.fmS.getValue()));
					criteria.setMemoryProb(Gender.FEMALE, Gender.MALE, KinType.CHILD, Gender.FEMALE, decimal(FieldworkInputWindow.this.ffS.getValue()));
					criteria.setMemoryProb(Gender.MALE, Gender.FEMALE, KinType.CHILD, Gender.FEMALE, decimal(FieldworkInputWindow.this.fmD.getValue()));
					criteria.setMemoryProb(Gender.FEMALE, Gender.FEMALE, KinType.CHILD, Gender.FEMALE, decimal(FieldworkInputWindow.this.ffD.getValue()));
					criteria.setMemoryProb(Gender.MALE, Gender.FEMALE, KinType.SPOUSE, Gender.FEMALE, decimal(FieldworkInputWindow.this.fmW.getValue()));
					// criteria.setWomansWifeByFemale(decimal(mfW.getValue()));
					// criteria.setMansHusbandByFemale(decimal(mmH.getValue()));
					criteria.setMemoryProb(Gender.FEMALE, Gender.MALE, KinType.SPOUSE, Gender.FEMALE, decimal(FieldworkInputWindow.this.ffH.getValue()));
					criteria.setDistanceType((FiliationType) FieldworkInputWindow.this.cbBoxDistanceType.getSelectedItem());
					criteria.setMaxDistance((Integer) FieldworkInputWindow.this.spinnerKinDegree.getValue());
					criteria.setDistanceWeight((Double) FieldworkInputWindow.this.spinnerDistanceWeight.getValue());
					criteria.setDistanceFactor((Double) FieldworkInputWindow.this.spinnerDistanceFactor.getValue());

					criteria.setNrInformants((Integer) FieldworkInputWindow.this.nrInformantsSpinner.getValue());

					criteria.setMaleAcceptance(decimal(FieldworkInputWindow.this.mAccept.getValue()));
					criteria.setFemaleAcceptance(decimal(FieldworkInputWindow.this.fAccept.getValue()));

					report.inputs().add("nrInformants", criteria.getNrInformants());
					report.inputs().add("acceptance rate male", criteria.getMaleAcceptance());
					report.inputs().add("acceptance rate female", criteria.getFemaleAcceptance());
					report.inputs().add("loss rate", criteria.getDistanceFactor());

					for (int i = 0; i < 2; i++) {
						for (int j = 0; j < 2; j++) {
							for (int k = 0; k < 3; k++) {
								for (int l = 0; l < 2; l++) {
									String desc = "recall rate ";
									desc += MemoryCriteria.describe(i, j, k, l);
									double prob = criteria.getMemory(i, j, k, l);
									report.inputs().add(desc, prob);
								}
							}
						}
					}

					//

					Map<Individual, Set<Individual>> neighborSets = StatisticsWorker.neighborSets(netGUI.getSegmentation().getCurrentIndividuals(),
							criteria.getMaxDistance(), criteria.getDistanceType());

					RandomNetExplorer randomNetExplorer = new RandomNetExplorer(netGUI.getSegmentation(), criteria, null, neighborSets);
					randomNetExplorer.setReport(report);

					int runs = 1;
					int gen = 10;

					Report statsReport = RandomNetReporter.reportVirtualFieldwork(randomNetExplorer, runs, gen);

					NetGUI newNetGui = PuckGUI.instance().createNetGUI(netGUI.getFile(), randomNetExplorer.getVirtualNet());
					newNetGui.setChanged(netGUI.isChanged());

					newNetGui.addReportTab(report);

					newNetGui.addReportTab(statsReport);

				} catch (final Exception exception) {

					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(FieldworkInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}

			}
		});
		panel.add(btnStart);

		JPanel panel_1 = new JPanel();
		this.contentPane.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblNrinformants = new JLabel("Nr of Informants");
		panel_1.add(lblNrinformants, "4, 2, 3, 1");

		this.nrInformantsSpinner = new JSpinner();
		this.nrInformantsSpinner.setModel(new SpinnerNumberModel(new Integer(100), new Integer(0), null, new Integer(100)));
		panel_1.add(this.nrInformantsSpinner, "8, 2");

		this.lblKinProximity = new JLabel("Kin proximity");
		panel_1.add(this.lblKinProximity, "4, 4");

		this.cbBoxDistanceType = new JComboBox();
		this.cbBoxDistanceType.setModel(new DefaultComboBoxModel(FiliationType.values()));
		panel_1.add(this.cbBoxDistanceType, "8, 4, fill, default");

		this.lblKinDegree = new JLabel("Kin degree");
		panel_1.add(this.lblKinDegree, "4, 6");

		this.spinnerKinDegree = new JSpinner();
		this.spinnerKinDegree.setModel(new SpinnerNumberModel(new Integer(6), null, null, new Integer(1)));
		panel_1.add(this.spinnerKinDegree, "8, 6");

		this.lblNearKinPropensity = new JLabel("Near kin weight");
		panel_1.add(this.lblNearKinPropensity, "4, 8");

		this.spinnerDistanceWeight = new JSpinner();
		this.spinnerDistanceWeight.setModel(new SpinnerNumberModel(new Double(1), new Double(1), null, new Double(10)));
		panel_1.add(this.spinnerDistanceWeight, "8, 8");

		this.lbMemory = new JLabel("Memory");
		panel_1.add(this.lbMemory, "4, 10");

		this.spinnerDistanceFactor = new JSpinner();
		this.spinnerDistanceFactor.setModel(new SpinnerNumberModel(0.0, 0.0, 1.0, 0.0));
		panel_1.add(this.spinnerDistanceFactor, "8, 10");

		Label label_3 = new Label("Male Informant");
		panel_1.add(label_3, "6, 12, center, default");

		Label label_4 = new Label("Female Informant");
		panel_1.add(label_4, "8, 12, center, default");

		JLabel lblNrInformants = new JLabel("Acceptance");
		panel_1.add(lblNrInformants, "4, 14, right, default");

		this.mAccept = new JSlider();
		this.mAccept.setValue(100);
		panel_1.add(this.mAccept, "6, 14");

		this.fAccept = new JSlider();
		this.fAccept.setValue(100);
		panel_1.add(this.fAccept, "8, 14");

		this.lblNewLabel = new JLabel("Kin Recall Rates");
		this.lblNewLabel.setBackground(new Color(255, 255, 255));
		this.lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		panel_1.add(this.lblNewLabel, "6, 16, 3, 1");

		Label label_1 = new Label("Men's Kin");
		panel_1.add(label_1, "6, 18, 3, 1, center, default");

		JLabel mFatherLabel = new JLabel("Father");
		mFatherLabel.setHorizontalAlignment(SwingConstants.LEFT);
		panel_1.add(mFatherLabel, "4, 20, left, default");

		this.mmF = new JSlider();
		this.mmF.setValue(100);
		panel_1.add(this.mmF, "6, 20");

		this.fmF = new JSlider();
		this.fmF.setValue(100);
		panel_1.add(this.fmF, "8, 20");

		this.lblMother = new JLabel("Mother");
		panel_1.add(this.lblMother, "4, 22");

		this.mmM = new JSlider();
		this.mmM.setValue(100);
		panel_1.add(this.mmM, "6, 22");

		this.fmM = new JSlider();
		this.fmM.setValue(100);
		panel_1.add(this.fmM, "8, 22");

		this.lblSon = new JLabel("Son");
		panel_1.add(this.lblSon, "4, 24");

		this.mmS = new JSlider();
		this.mmS.setValue(100);
		panel_1.add(this.mmS, "6, 24");

		this.fmS = new JSlider();
		this.fmS.setValue(100);
		panel_1.add(this.fmS, "8, 24");

		JLabel mDaughterLabel = new JLabel("Daughter");
		panel_1.add(mDaughterLabel, "4, 26");

		this.mmD = new JSlider();
		this.mmD.setValue(100);
		panel_1.add(this.mmD, "6, 26");

		this.fmD = new JSlider();
		this.fmD.setValue(100);
		panel_1.add(this.fmD, "8, 26");

		JLabel lblWife = new JLabel("Wife");
		panel_1.add(lblWife, "4, 28");

		this.mmW = new JSlider();
		this.mmW.setValue(100);
		panel_1.add(this.mmW, "6, 28");

		this.fmW = new JSlider();
		this.fmW.setValue(100);
		panel_1.add(this.fmW, "8, 28");

		Label label_6 = new Label("Women's Kin");
		panel_1.add(label_6, "6, 30, 3, 1, center, default");

		JLabel fFatherLabel = new JLabel("Father");
		fFatherLabel.setHorizontalAlignment(SwingConstants.LEFT);
		panel_1.add(fFatherLabel, "4, 32, left, default");

		this.mfF = new JSlider();
		this.mfF.setValue(100);
		panel_1.add(this.mfF, "6, 32");

		this.ffF = new JSlider();
		this.ffF.setValue(100);
		panel_1.add(this.ffF, "8, 32");

		this.lblMother_1 = new JLabel("Mother");
		panel_1.add(this.lblMother_1, "4, 34");

		this.mfM = new JSlider();
		this.mfM.setValue(100);
		panel_1.add(this.mfM, "6, 34");

		this.ffM = new JSlider();
		this.ffM.setValue(100);
		panel_1.add(this.ffM, "8, 34");

		this.lblSon_1 = new JLabel("Son");
		panel_1.add(this.lblSon_1, "4, 36");

		this.mfS = new JSlider();
		this.mfS.setValue(100);
		panel_1.add(this.mfS, "6, 36");

		this.ffS = new JSlider();
		this.ffS.setValue(100);
		panel_1.add(this.ffS, "8, 36");

		JLabel fDaughterLabel = new JLabel("Daughter");
		panel_1.add(fDaughterLabel, "4, 38");

		this.mfD = new JSlider();
		this.mfD.setValue(100);
		panel_1.add(this.mfD, "6, 38");

		this.ffD = new JSlider();
		this.ffD.setValue(100);
		panel_1.add(this.ffD, "8, 38");

		JLabel lblHusband = new JLabel("Husband");
		panel_1.add(lblHusband, "4, 40");

		this.mfH = new JSlider();
		this.mfH.setValue(100);
		panel_1.add(this.mfH, "6, 40");

		this.ffH = new JSlider();
		this.ffH.setValue(100);
		panel_1.add(this.ffH, "8, 40");

	}

	private double decimal(final Object number) {
		return new Double((Integer) number) / 100.;
	}

}
