package org.tip.puck.alliancenets;

import java.io.BufferedWriter;
import java.io.FileWriter;

import org.tip.puck.evo.EvoGen;
import org.tip.puck.evo.EvoGenCallbacks;
import org.tip.puck.evo.Generator;


/**
 * @author Telmo Menezes
 *
 */
public class EvoAllianceGen implements EvoGenCallbacks {
    private String outDir;
    private AllianceGen gen;
    private TopologicalIndices targIndices;
    private int targNodeCount;
    private int targEdgeCount;
    private int nodeCount;
    private int edgeCount;
    private int bestCount;
    
    
    public EvoAllianceGen(AllianceNet targNet, String outDir) {
        this.outDir = outDir;
        targIndices = genIndices(targNet);
        System.out.println(targIndices);
        
        nodeCount = targNet.getNodeCount();
        edgeCount = targNet.getEdgeCount();
        targNodeCount = nodeCount;
        targEdgeCount = edgeCount;
        
        gen = new AllianceGen(nodeCount, edgeCount, targIndices);
        
        bestCount = 0;
        
        // write header of evo.csv
        try {
            FileWriter fwriter = new FileWriter(outDir + "/evo.csv");
            BufferedWriter writer = new BufferedWriter(fwriter);
            writer.write("gen,best_fit,best_gen_fit,best_ep,best_nc,best_enc,best_ns,best_parallels,best_crosses,best_tri_loops,best_tri_trans,best_geno_size,mean_geno_size,gen_comp_time,sim_comp_time,fit_comp_time\n");
            writer.close() ;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public Generator baseGenerator() {
        return gen;
    }

    public double computeFitness(Generator gen) {
        AllianceGen allGen = (AllianceGen)gen;
        allGen.run();
        AllianceNet allianceNet = allGen.getNet();
        TopologicalIndices indices = new TopologicalIndices(allianceNet);
        allGen.setIndices(indices);
        
        double dist = indices.distance(targIndices);
        
        if (Double.isNaN(dist)) {
            return Double.POSITIVE_INFINITY;
        }
        else {
            return dist;
        }
    }
    
    public void onNewBest(EvoGen evo) {
        String suffix = "" + bestCount + "_gen" + evo.getCurgen();
        Generator bestGen = evo.getBestGenerator();
        
        System.out.println("targ: " + targIndices);
        System.out.println("best: " + ((AllianceGen)bestGen).getIndices());
        
        // write new best log
        try {
            FileWriter fwriter = new FileWriter(outDir + "/newbest.log", true);
            BufferedWriter writer = new BufferedWriter(fwriter);
            writer.write("#gen " + evo.getCurgen() + "\n");
            writer.write("targ: " + targIndices + "\n");
            writer.write("best: " + ((AllianceGen)bestGen).getIndices() + "\n");
            writer.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
        // write net
        ((AllianceGen)bestGen).getNet().save(outDir + "/bestnet" + suffix + ".txt");
        ((AllianceGen)bestGen).getNet().save(outDir + "/bestnet" + ".txt");
        
        // write progs
        bestGen.getProgset().write(outDir + "/bestprog" + suffix + ".txt");
        bestGen.getProgset().write(outDir + "/bestprog" + ".txt");
        bestCount++;
    }

    public void onGeneration(EvoGen evo) {
        AllianceGen bestGen = (AllianceGen)evo.getBestGenerator();
        
        // write evo log
        try {
            FileWriter fwriter = new FileWriter(outDir + "/evo.csv", true);
            BufferedWriter writer = new BufferedWriter(fwriter);
            writer.write("" + evo.getCurgen() + ","
                    + evo.getBestFitness() + ","
                    + evo.getBestGenFitness() + ","
                    + bestGen.getIndices().getEndogamousPercentage() + ","
                    + bestGen.getIndices().getNetworkConcentration() + ","
                    + bestGen.getIndices().getEndogamicNetworkConcentration() + ","
                    + bestGen.getIndices().getNetworkSymmetry() + ","
                    + bestGen.getIndices().getParallels() + ","
                    + bestGen.getIndices().getCrosses() + ","
                    + bestGen.getIndices().getLoopTri() + ","
                    + bestGen.getIndices().getTranTri() + ","
                    + evo.getBestGenerator().genotypeSize() + ","
                    + evo.getMeanGenoSize() + ","
                    + evo.getGenTime() + ","
                    + evo.getSimTime() + ","
                    + evo.getFitTime() + "\n");
            writer.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
        System.out.println("" + evo.getCurgen() + ","
                + evo.getBestFitness() + ","
                + evo.getBestGenFitness() + ","
                + bestGen.getIndices().getEndogamousPercentage() + ","
                + bestGen.getIndices().getNetworkConcentration() + ","
                + bestGen.getIndices().getEndogamicNetworkConcentration() + ","
                + bestGen.getIndices().getNetworkSymmetry() + ","
                + bestGen.getIndices().getParallels() + ","
                + bestGen.getIndices().getCrosses() + ","
                + bestGen.getIndices().getLoopTri() + ","
                + bestGen.getIndices().getTranTri() + ","
                + evo.getBestGenerator().genotypeSize() + ","
                + evo.getMeanGenoSize() + ","
                + evo.getGenTime() + ","
                + evo.getSimTime() + ","
                + evo.getFitTime() + "\n");
    }

    private TopologicalIndices genIndices(AllianceNet allianceNet) {
        return new TopologicalIndices(allianceNet);
    }
    
    public String infoString() {
        String str = "target net node count: " + targNodeCount + "\n";
        str += "target net edge count: " + targEdgeCount + "\n";
        str += "generated nets node count: " + nodeCount + "\n";
        str += "generated nets edge count: " + edgeCount + "\n";
        
        return str;
    }
}