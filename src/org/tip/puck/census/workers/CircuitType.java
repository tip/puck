package org.tip.puck.census.workers;

/**
 * 
 * @author cpm
 * 
 */
public enum CircuitType {

	CIRCUIT,
	RING,
	MINOR,
	MINIMAL;

	/**
	 * This method displays a human label for the enum.
	 * 
	 * It does not replace toString method to avoid automatic convert
	 * operations.
	 * 
	 * @return the label corresponding to the enum.
	 */
	public String toLabel() {
		String result;

		switch (this) {
			case CIRCUIT:
				result = "circuit";
			break;
			case RING:
				result = "ring";
			break;
			case MINOR:
				result = "minor ring";
			break;
			case MINIMAL:
				result = "minimal ring";
			break;
			default:
				result = "circuit";
		}
		//
		return result;
	}

}
