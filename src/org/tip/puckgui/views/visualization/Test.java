package org.tip.puckgui.views.visualization;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.graphs.Graph;
import org.tip.puck.net.Individual;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.visualization.VisualizationController;
import org.tip.puck.visualization.style.implementations.OreGraph;
import org.tip.puckgui.NetGUI;

/**
 * 
 * @author TIP
 */
public class Test {

	private static final Logger logger = LoggerFactory.getLogger(Test.class);

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static void test(final NetGUI netGUI) throws PuckException, URISyntaxException, MalformedURLException {
		// SET YOUR OWN PATH HERE.
		// Net net = PuckManager.loadCorpus(new
		// File("C:\\Documents and Settings\\Klaus Hamberger\\Bureau\\Ebrei 2009.ged"));
		File here = new File(".");
		System.out.println("========>" + here.getAbsolutePath());

		URL url = new URL("file:/Users/klaushamberger/Documents/Ebrei2009.ged");
		if (url == null) {
			System.out.println("File not found");
		}
		// Net net = netGUI.getNet();

		// PuckManager.loadCorpus(new File(url.toURI()));

		// Compute.
		Segmentation source = netGUI.getSegmentation();

		// new Segmentation(net);

		VisualizationController vizController = VisualizationController.getSharedInstance();

		Graph<Individual> graph = NetUtils.createOreGraph(source);
		VisualizationPanel buildView = vizController.buildView(graph, OreGraph.class);

		// Graph<Family> graph = NetUtils.createPGraph(source);
		// VisualizationPanel buildView = vizController.buildView(graph,
		// PGraph.class);

		// Graph<Cluster<Individual>> graph =
		// ClusterNetworkUtils.createAllianceNetwork(source, "PATRID",
		// ClusterNetworkUtils.LineType.WEIGHTED_ARC);
		// VisualizationPanel buildView = vizController.buildView(graph,
		// AllianceNetwork.class);

		// String pajekContent = PuckUtils.writePajekNetwork(graph,
		// null).toString();
		/*
		//Add the applet to a JFrame and display
		final JFrame frame = new JFrame("Test Preview");
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(buildView.getView(), BorderLayout.CENTER);
		JButton btn = new JButton("Export");
		btn.setActionCommand("export");
		final Workspace workspace = buildView.getWorkspace();
		btn.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
		Exporter exporter = VisualizationController.getSharedInstance().getExporter(ExporterJPG.class);
		// get file
		JFileChooser chooser = new JFileChooser();
		int returnVal = chooser.showSaveDialog(frame);
		if (returnVal != JFileChooser.APPROVE_OPTION) {
		return;
		}
		File selectedFile = chooser.getSelectedFile();
		exporter.exportToFile(selectedFile, workspace);
		}
		});
		frame.add(btn, BorderLayout.SOUTH);
		frame.pack();
		frame.setVisible(true);
		 */

		netGUI.addTab("Visualization", buildView);

	}
}
