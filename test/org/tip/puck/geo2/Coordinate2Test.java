package org.tip.puck.geo2;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.geo.Coordinate2;

/**
 * 
 * @author TIP
 */
public class Coordinate2Test {
	private static final Logger logger = LoggerFactory.getLogger(Coordinate2Test.class);

	/**
     */
	@Test
	public void testCoordinate01() throws Exception {

		logger.debug("========================== testCoordinate01");

		Assertions.assertThat(Coordinate2.isCoordinateValue(null)).isFalse();
		Assertions.assertThat(Coordinate2.isCoordinateValue("")).isFalse();
		Assertions.assertThat(Coordinate2.isCoordinateValue("-")).isFalse();
		Assertions.assertThat(Coordinate2.isCoordinateValue("a42")).isFalse();
		Assertions.assertThat(Coordinate2.isCoordinateValue("42a")).isFalse();
		Assertions.assertThat(Coordinate2.isCoordinateValue("42")).isTrue();
		Assertions.assertThat(Coordinate2.isCoordinateValue("-42")).isTrue();
		Assertions.assertThat(Coordinate2.isCoordinateValue("42.23232")).isTrue();
		Assertions.assertThat(Coordinate2.isCoordinateValue("-42.23232")).isTrue();
	}
}
