package org.tip.puck.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.tip.puck.partitions.Partition;

/**
 * 
 * @author TIP
 */
public class NumberedValues extends HashMap<Integer, Value> implements Iterable<Value> {

	private static final long serialVersionUID = 1752108749127806802L;

	/**
	 *
	 */
	public NumberedValues() {
		super();
	}

	/**
	 *
	 */
	public NumberedValues(final int initialCapacity) {
		super(initialCapacity);
	}
	
	public <E extends Numberable> NumberedValues(final Partition<E> partition){
		super();
		
		for (E item : partition.getItems()){
			put(item.getId(), partition.getValue(item));
		}
		
	}

	/**
	 * 
	 * @param number
	 * @param value
	 */
	public void addValue(final int number, final Double value) {
		if (value == null) {
			this.put(number, null);
		} else {
			this.put(number, new Value(value));
		}
	}

	/**
	 * 
	 * @param number
	 * @param value
	 */
	public void addValue(final int number, final Integer value) {
		if (value == null) {
			this.put(number, null);
		} else {
			this.put(number, new Value(value));
		}
	}

	/**
	 * 
	 * @param number
	 * @param value
	 */
	public void addValue(final int number, final Object value) {
		if (value == null) {
			this.put(number, null);
		} else {
			this.put(number, new Value(value));
		}
	}

	/**
	 * 
	 * @param number
	 * @param value
	 */
	public void addValue(final int number, final List<?> value) {
		if (value == null) {
			this.put(number, null);
		} else {
			this.put(number, new Value(value));
		}
	}

	/**
	 * 
	 * @param number
	 * @param value
	 */
	public void addValue(final int number, final String value) {
		if (value == null) {
			this.put(number, null);
		} else {
			this.put(number, new Value(value));
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public double sum() {
		double result;

		//
		result = 0;
		for (Value value : this) {
			if ((value != null) && (value.isNumber())) {
				result += value.doubleValue();
			}
		}

		//
		return result;
	}


	/**
	 * 
	 * @return
	 */
	public double average() {
		double result;

		//
		double sum = 0;
		int count = 0;
		for (Value value : this) {
			if ((value != null) && (value.isNumber())) {
				sum += value.doubleValue();
				count += 1;
			}
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum * 1.0 / count;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double averagePositives() {
		double result;

		//
		double sum = 0;
		int count = 0;
		for (Value value : this) {
			if ((value != null) && (value.isNumber())) {
				if (value.doubleValue() > 0) {
					sum += value.doubleValue();
					count += 1;
				}
			}
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum * 1.0 / count;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
/*	public boolean isNumeric() {
		boolean result;

		boolean ended = false;
		result = false;
		Iterator<Value> iterator = this.iterator();
		while (!ended) {
			if (iterator.hasNext()) {
				Value value = iterator.next();
				if (value != null && value.isNotNumber()) {
					result = false;
					ended = true;
				}
			} else {
				result = true;
				ended = true;
			}
		}

		//
		return result;
	}*/

	/**
	 * 
	 */
	@Override
	public Iterator<Value> iterator() {
		Iterator<Value> result;

		result = super.values().iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double max() {
		double result;

		Double maxValue = null;
		for (Value value : this) {
			if ((value != null) && (value.isNumber())) {
				if (maxValue == null) {
					maxValue = value.doubleValue();
				} else if (value.doubleValue() > maxValue) {
					maxValue = value.doubleValue();
				}
			}
		}

		if (maxValue == null) {
			result = 0;
		} else {
			result = maxValue;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double min() {
		double result;

		Double minValue = null;
		for (Value value : this) {
			if ((value != null) && (value.isNumber())) {
				if (minValue == null) {
					minValue = value.doubleValue();
				} else if (value.doubleValue() < minValue) {
					minValue = value.doubleValue();
				}
			}
		}

		if (minValue == null) {
			result = 0;
		} else {
			result = minValue;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Value> toList() {
		List<Value> result;

		result = new ArrayList<Value>(values());

		//
		return result;
	}
	
	// has to eliminate zero values
	public List<Value> toSortedList() {
		List<Value> result;

		result = new ArrayList<Value>();
		for (Value value : this){
			if (value!=null){
				result.add(value);
			}
		}
		Collections.sort(result);

		//
		return result;
	}
	
	public int sizeNonNull(){
		int result;
		
		result = 0;
		
		for (Integer key : keySet()){
			if (get(key)!=null){
				result++;
			}
		}
		//
		return result;
	}
	
	
	
	public Value median (){
		Value result;
		
		result = null;
		
		int count = 0;
		int half = sizeNonNull()/2;
		
		for (Value value : toSortedList()){
			result = value;
			if (count > half){
				break;
			}
			count++;
		}
		//
		return result;
	}
	
	public String getValueAsString (String indicator){
		String result;
		
		if (indicator.equals("SUM")){
			result = sum()+"";
		} else if (indicator.equals("AVERAGE")){
			result = average()+"";
		} else if (indicator.equals("MIN")){
			result = min()+"";
		} else if (indicator.equals("MAX")){
			result = min()+"";
		} else if (indicator.equals("MEDIAN")){
			result = median()+"";
		} else if (indicator.equals("AVERAGE_POSITIVE")){
			result = averagePositives()+"";
		} else {
			result = null;
		}
		//
		return result;
	}
	
	
}
