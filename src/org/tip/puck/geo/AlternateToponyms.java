package org.tip.puck.geo;

import fr.devinsy.util.StringList;

/**
 * 
 */
public class AlternateToponyms extends StringList {

	private static final long serialVersionUID = 5812540834504330135L;

	/**
	 * 
	 */
	public AlternateToponyms() {
		super();
	}

	/**
	 * 
	 */
	@Override
	public boolean add(final String value) {
		boolean result;

		if (value == null || contains(value)) {

			result = false;

		} else {

			super.add(value);
			result = true;
		}

		//
		return result;
	}
}
