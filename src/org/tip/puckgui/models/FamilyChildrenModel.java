package org.tip.puckgui.models;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puckgui.InputSettings.CheckLevel;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.PuckGUI;
import org.tip.puckgui.util.ComboBoxIds;

/**
 * 
 * @author TIP
 */
public class FamilyChildrenModel extends AbstractTableModel {
	private static final long serialVersionUID = 1679859239901365968L;
	private static final Logger logger = LoggerFactory.getLogger(FamilyChildrenModel.class);
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.tip.puckgui.messages");

	private static ImageIcon smallFemaleIcon = new ImageIcon(IndividualChildrenModel.class.getResource("/org/tip/puckgui/images/female-16x16.png"));
	private static ImageIcon smallMaleIcon = new ImageIcon(IndividualChildrenModel.class.getResource("/org/tip/puckgui/images/male-16x16.png"));
	private static ImageIcon smallUnknowIcon = new ImageIcon(IndividualChildrenModel.class.getResource("/org/tip/puckgui/images/unknown-16x16.png"));

	private Family source;
	private List<Individual> children;
	private NetGUI netGUI;

	/**
	 * 
	 */
	public FamilyChildrenModel(final NetGUI netGUI, final Family source) {
		super();

		this.netGUI = netGUI;
		this.source = source;
		if (source == null) {
			//
			this.children = new ArrayList<Individual>();

		} else {
			//
			this.children = this.source.getChildren().toListSortedByOrder();
		}
	}

	/**
	 * 
	 * @return
	 */
	public List<Individual> children() {
		List<Individual> result;

		result = this.children;

		//
		return result;
	}

	/**
	 * 
	 */
	public void escapeNewEdition() {
		//
		if (isNewEditionOn()) {
			//
			logger.debug("ESCAPE d");
			int index = this.children.size() - 1;
			this.children.remove(index);
			fireTableRowsDeleted(index, index);
		}
	}

	/**
	 * 
	 */
	@Override
	public Class getColumnClass(final int columnIndex) {
		Class result;

		switch (columnIndex) {
			case 0:
				result = Number.class;
			break;

			case 1:
				result = ImageIcon.class;
			break;

			case 2:
				result = String.class;
			break;

			case 3:
				result = Number.class;
			break;

			case 4:
				result = String.class;
			break;

			case 5:
				result = String.class;
			break;

			default:
				result = String.class;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getColumnCount() {
		int result;

		result = 6;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String getColumnName(final int columnIndex) {
		String result;

		switch (columnIndex) {
			case 0:
				result = BUNDLE.getString("MainWindow.individual.id");
			break;

			case 1:
				result = BUNDLE.getString("MainWindow.individual.gender");
			break;

			case 2:
				result = BUNDLE.getString("MainWindow.individual.name");
			break;

			case 3:
				result = BUNDLE.getString("MainWindow.individual.position");
			break;

			case 4:
				result = BUNDLE.getString("MainWindow.individual.birth");
			break;

			case 5:
				result = BUNDLE.getString("MainWindow.individual.death");
			break;

			default:
				result = "";
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getRowCount() {
		int result;

		result = this.children.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Family getSource() {
		Family result;

		result = this.source;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Object getValueAt(final int rowIndex, final int columnIndex) {
		Object result;

		if (this.source == null) {
			//
			result = null;

		} else if ((isNewEditionOn() && (rowIndex == this.children.size() - 1))) {
			//
			result = "";

		} else {
			//
			switch (columnIndex) {
				case 0:
					result = this.children.get(rowIndex).getId();
				break;

				case 1:
					switch (this.children.get(rowIndex).getGender()) {
						case FEMALE:
							result = smallFemaleIcon;
						break;
						case MALE:
							result = smallMaleIcon;
						break;
						case UNKNOWN:
							result = smallUnknowIcon;
						break;
						default:
							result = smallUnknowIcon;
					}
				break;

				case 2:
					result = this.children.get(rowIndex).getName();
				break;

				case 3:
					result = this.children.get(rowIndex).getBirthOrder();
				break;

				case 4:
					result = null;
				break;

				case 5:
					result = null;
				break;

				default:
					result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public boolean isCellEditable(final int row, final int col) {
		boolean result;

		switch (col) {
			case 0:
			case 3:
				result = true;
			break;

			default:
				result = false;
		}

		//
		return result;
	}

	/**
	 * This method indicates if a new line is editing.
	 * 
	 * @return
	 */
	public boolean isNewEditionOn() {
		boolean result;

		if (this.source == null) {
			//
			result = false;

		} else if (this.children.size() == this.source.getChildren().size() + 1) {
			//
			result = true;

		} else {
			//
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public void setNewItem() {
		//
		this.children.add(new Individual(0, "?", Gender.UNKNOWN));
		fireTableDataChanged();
	}

	/**
	 * 
	 * @param source
	 */
	public void setSource(final Family source) {
		//
		this.source = source;

		//
		if (source == null) {
			//
			this.children = new ArrayList<Individual>();

		} else {
			//
			this.children = this.source.getChildren().toListSortedByOrder();
		}

		//
		fireTableDataChanged();
	}

	/**
	 * 
	 */
	@Override
	public void setValueAt(final Object value, final int rowIndex, final int columnIndex) {
		logger.debug("setValueAt(" + rowIndex + ", " + columnIndex + " " + value + ")");

		//
		if (columnIndex == 0) {
			//
			if (this.netGUI != null) {
				//
				if (isNewEditionOn()) {
					//
					Individual newChild = ComboBoxIds.getOrCreateIndividualFromInput(this.netGUI.getNet(), value, Gender.UNKNOWN);
					if (newChild == null) {
						//
						this.children.remove(rowIndex);
						fireTableDataChanged();

					} else {
						// Check undefinedRoles.
						String errorMessage = controlChildKinError(this.source, newChild);
						if (errorMessage != null) {
							//
							JOptionPane.showMessageDialog(null, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);

						} else {
							// Check warning.
							if (controlChildKinWarningOK(this.source, newChild)) {
								//
								this.netGUI.getNet().removeChild(newChild);
								this.netGUI.getNet().addChild(this.source, newChild);

								//
								this.netGUI.setChanged(true);
								this.netGUI.updateAll();
							}
						}
					}
				} else {
					//
					Individual oldChild = this.children.get(rowIndex);
					Individual newChild = ComboBoxIds.getOrCreateIndividualFromInput(this.netGUI.getNet(), value, Gender.UNKNOWN);

					//
					if (newChild == null) {
						//
						this.netGUI.getNet().removeChild(oldChild);

						//
						this.netGUI.setChanged(true);
						this.netGUI.updateAll();

					} else if (newChild != oldChild) {
						// Check undefinedRoles.
						String errorMessage = controlChildKinError(this.source, newChild);
						if (errorMessage != null) {
							//
							JOptionPane.showMessageDialog(null, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);

						} else {
							//
							if (controlChildKinWarningOK(this.source, newChild)) {
								//
								Family currentFamily = oldChild.getOriginFamily();
								this.netGUI.getNet().removeChild(oldChild);
								this.netGUI.getNet().removeChild(newChild);
								this.netGUI.getNet().addChild(currentFamily, newChild);

								//
								this.netGUI.setChanged(true);
								this.netGUI.updateAll();
							}
						}
					}
				}
			}
		} else if (columnIndex == 3) {
			//
			Integer newBirthOrderValue;
			if (value instanceof String) {
				//
				if (NumberUtils.isDigits((String) value)) {
					//
					newBirthOrderValue = Integer.parseInt((String) value);

				} else {
					//
					newBirthOrderValue = null;
				}
			} else if (value instanceof Integer) {
				//
				newBirthOrderValue = (Integer) value;

			} else {
				//
				newBirthOrderValue = null;
			}

			//
			if ((newBirthOrderValue == null) || (newBirthOrderValue > 0)) {
				//
				Individual currentIndividual = this.netGUI.selectedIndividual();
				Individual child = this.children.get(rowIndex);

				//
				child.setBirthOrder(newBirthOrderValue);

				//
				this.netGUI.setChanged(true);
				this.netGUI.updateAll();
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	public Family source() {
		Family result;

		result = this.source;

		//
		return result;
	}

	/**
	 * 
	 * @param family
	 * @param newChild
	 * @return
	 */
	public static String controlChildKinError(final Family family, final Individual newChild) {
		String result;

		// Check undefinedRoles.
		if (family.getChildren().contains(newChild)) {
			//
			result = "Already child detected.";

		} else if ((newChild == family.getFather()) || (newChild == family.getMother())) {
			//
			result = "This new child already is parent in this family.";

		} else if ((PuckGUI.instance().getPreferences().getInputSettings().getParentChildMarriages() == CheckLevel.ERROR)
				&& (NetUtils.isParentChildMarriage(family, newChild))) {
			//
			result = "Parent-child marriages detected.\nThis new child already is married with a parent of this family.";

		} else {
			//
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param family
	 * @param newChild
	 * @return
	 */
	public static boolean controlChildKinWarningOK(final Family family, final Individual newChild) {
		boolean result;

		//
		result = true;

		//
		if ((PuckGUI.instance().getPreferences().getInputSettings().getParentChildMarriages() == CheckLevel.WARNING)
				&& (NetUtils.isParentChildMarriage(family, newChild))) {
			//
			String buttons[] = { "Continue", "Abort" };
			int response = JOptionPane.showOptionDialog(null,
					"Parent-child marriages detected.\nThis new child already is married with a parent of this family.", "Warning",
					JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null, buttons, buttons[0]);

			if (response == 0) {
				//
				result = true;

			} else {
				//
				result = false;
			}

		}

		//
		if ((result == true) && (newChild.getOriginFamily() != null)) {
			//
			String buttons[] = { "Continue", "Abort" };
			int response = JOptionPane.showOptionDialog(null, "This new child already has a family.\nThe action will remove him of this family.", "Warning",
					JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null, buttons, buttons[0]);

			if (response == 0) {
				//
				result = true;

			} else {
				//
				result = false;
			}
		}

		//
		return result;
	}
}
