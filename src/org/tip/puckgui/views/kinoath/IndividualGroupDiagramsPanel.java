package org.tip.puckgui.views.kinoath;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.kinoath.IndividualGroup;
import org.tip.puck.kinoath.IndividualGroups;
import org.tip.puck.kinoath.KinOathDiagram;
import org.tip.puck.kinoath.Puck2KinOath;
import org.tip.puckgui.NetGUI;

/**
 * 
 * @author TIP
 */
public class IndividualGroupDiagramsPanel extends JPanel {

	private static final long serialVersionUID = -9219009867524172056L;

	private static final Logger logger = LoggerFactory.getLogger(IndividualGroupDiagramsPanel.class);

	int reportCounter = 0;
	private JPanel thisJPanel;
	private NetGUI netGUI;
	JList individualGroupList;
	JScrollPane individualGroupsScrollPane;
	KinOathDiagram diagram;
	private KinOathDiagramPanel diagramScrollPanel;

	/**
	 * Initialize the contents of the frame.
	 */
	public IndividualGroupDiagramsPanel(final NetGUI guiManager, final IndividualGroups source) {

		//
		this.thisJPanel = this;
		this.netGUI = guiManager;

		//
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		JSplitPane split = new JSplitPane();
		add(split);
		split.setAlignmentX(Component.CENTER_ALIGNMENT);

		JPanel individualGroupsPanel = new JPanel();
		individualGroupsPanel.setMinimumSize(new Dimension(200, 10));
		split.setLeftComponent(individualGroupsPanel);
		individualGroupsPanel.setLayout(new BoxLayout(individualGroupsPanel, BoxLayout.Y_AXIS));

		this.individualGroupsScrollPane = new JScrollPane();
		this.individualGroupsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		individualGroupsPanel.add(this.individualGroupsScrollPane);

		this.individualGroupList = new JList();
		this.individualGroupList.setDoubleBuffered(true);
		this.individualGroupList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(final ListSelectionEvent event) {
				logger.debug("event = " + event.getValueIsAdjusting() + " " + event.getFirstIndex() + " " + event.getLastIndex() + " "
						+ IndividualGroupDiagramsPanel.this.individualGroupList.getSelectedIndex());

				if (!event.getValueIsAdjusting()) {
					// Selected.
					if (IndividualGroupDiagramsPanel.this.individualGroupList.getSelectedIndex() != -1) {
						//
						IndividualGroup group = (IndividualGroup) ((JList) event.getSource()).getModel().getElementAt(
								IndividualGroupDiagramsPanel.this.individualGroupList.getSelectedIndex());
						updateIndividualGroupDiagram(group);
					}
				}
			}
		});
		this.individualGroupList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		this.individualGroupList.setCellRenderer(new IndividualGroupsCellRenderer());
		this.individualGroupList.setModel(new IndividualGroupsModel(source));
		this.individualGroupsScrollPane.setViewportView(this.individualGroupList);

		JPanel diagramZonePanel = new JPanel();
		split.setRightComponent(diagramZonePanel);
		diagramZonePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		diagramZonePanel.setAlignmentY(Component.TOP_ALIGNMENT);
		diagramZonePanel.setLayout(new BorderLayout(0, 0));

		JPanel parametersPanel = new JPanel();
		parametersPanel.setBorder(new EmptyBorder(0, 0, 2, 0));
		diagramZonePanel.add(parametersPanel, BorderLayout.NORTH);
		parametersPanel.setLayout(new BoxLayout(parametersPanel, BoxLayout.X_AXIS));

		Component horizontalGlue_2 = Box.createHorizontalGlue();
		parametersPanel.add(horizontalGlue_2);

		JButton btnCenter = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("GlobalDiagramPanel.btnResetZoom.text")); //$NON-NLS-1$ //$NON-NLS-2$
		parametersPanel.add(btnCenter);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		parametersPanel.add(horizontalStrut_1);

		JButton btnExport = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualGroupsDiagramPanel.btnExport.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnExport.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Export.
				try {
					IndividualGroupDiagramsPanel.this.diagramScrollPanel.export();

				} catch (IOException exception) {
					// TODO Auto-generated catch block
					exception.printStackTrace();
				} catch (PuckException exception) {
					// TODO Auto-generated catch block
					exception.printStackTrace();
				}
			}
		});

		JButton btnCopy = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualGroupsDiagramPanel.btnCopy.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnCopy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Copy to clipboard.
				IndividualGroupDiagramsPanel.this.diagramScrollPanel.copyToClipboard();
			}
		});
		parametersPanel.add(btnCopy);

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		parametersPanel.add(horizontalStrut_2);
		parametersPanel.add(btnExport);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		parametersPanel.add(horizontalStrut);
		btnCenter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				IndividualGroupDiagramsPanel.this.diagramScrollPanel.resetZoom();
			}
		});

		JPanel diagramPanel = new JPanel();
		diagramZonePanel.add(diagramPanel, BorderLayout.CENTER);

		JPanel buttonsPanel = new JPanel();
		add(buttonsPanel);
		buttonsPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));

		JButton btnGroupsPrevious = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnPrevious.text"));
		btnGroupsPrevious.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Previous individual group.
				int selectedIndex = IndividualGroupDiagramsPanel.this.individualGroupList.getSelectedIndex();
				if (selectedIndex == -1) {
					int size = IndividualGroupDiagramsPanel.this.individualGroupList.getModel().getSize();
					if (size != 0) {
						selectedIndex = size - 1;
						IndividualGroupDiagramsPanel.this.individualGroupList.setSelectedIndex(selectedIndex);
						IndividualGroupDiagramsPanel.this.individualGroupList.ensureIndexIsVisible(selectedIndex);
					}
				} else if (selectedIndex > 0) {
					selectedIndex -= 1;
					IndividualGroupDiagramsPanel.this.individualGroupList.setSelectedIndex(selectedIndex);
					IndividualGroupDiagramsPanel.this.individualGroupList.ensureIndexIsVisible(selectedIndex);
				}
			}
		});

		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		buttonsPanel.add(horizontalStrut_6);

		JButton btnClose = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualDiagramPanel.btnClose.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				IndividualGroupDiagramsPanel.this.netGUI.closeCurrentTab();
			}
		});
		buttonsPanel.add(btnClose);

		Component horizontalGlue_1 = Box.createHorizontalGlue();
		buttonsPanel.add(horizontalGlue_1);

		JButton btnSort = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.btnSort.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnSort.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Sort individual group list.

				IndividualGroup selectedGroup = getSelectedGroup();
				((IndividualGroupsModel) IndividualGroupDiagramsPanel.this.individualGroupList.getModel()).touchSorting();
				if (selectedGroup != null) {
					//
					select(selectedGroup);
				}
			}
		});
		buttonsPanel.add(btnSort);

		Component horizontalGlue_4 = Box.createHorizontalGlue();
		buttonsPanel.add(horizontalGlue_4);
		buttonsPanel.add(btnGroupsPrevious);

		JButton btnGroupNext = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnNext.text"));
		btnGroupNext.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Next group.
				int selectedIndex = IndividualGroupDiagramsPanel.this.individualGroupList.getSelectedIndex();
				int size = IndividualGroupDiagramsPanel.this.individualGroupList.getModel().getSize();
				if (selectedIndex == -1) {
					if (size != 0) {
						selectedIndex = 0;
						IndividualGroupDiagramsPanel.this.individualGroupList.setSelectedIndex(selectedIndex);
						IndividualGroupDiagramsPanel.this.individualGroupList.ensureIndexIsVisible(selectedIndex);
					}
				} else if (selectedIndex < size - 1) {
					selectedIndex += 1;
					IndividualGroupDiagramsPanel.this.individualGroupList.setSelectedIndex(selectedIndex);
					IndividualGroupDiagramsPanel.this.individualGroupList.ensureIndexIsVisible(selectedIndex);
				}
			}
		});
		buttonsPanel.add(btnGroupNext);

		Component horizontalGlue = Box.createHorizontalGlue();
		buttonsPanel.add(horizontalGlue);

		JPanel panel_13 = new JPanel();
		buttonsPanel.add(panel_13);
		panel_13.setLayout(new BoxLayout(panel_13, BoxLayout.X_AXIS));

		this.diagramScrollPanel = new KinOathDiagramPanel();

		diagramPanel.setLayout(new BorderLayout(0, 0));
		diagramPanel.add(this.diagramScrollPanel);

		JLabel lblScrollShiftmouseLeft = new JLabel(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("IndividualGroupDiagramsPanel.lblScrollShiftmouseLeft.text")); //$NON-NLS-1$ //$NON-NLS-2$
		diagramZonePanel.add(lblScrollShiftmouseLeft, BorderLayout.SOUTH);

		// ////////////////////
		selectByIndex(1);
	}

	/**
	 * 
	 * @return
	 */
	public IndividualGroup getSelectedGroup() {
		IndividualGroup result;

		if (this.individualGroupList.getSelectedIndex() == -1) {
			result = null;
		} else {
			result = (IndividualGroup) ((IndividualGroupsModel) this.individualGroupList.getModel()).getElementAt(this.individualGroupList.getSelectedIndex());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 */
	public void select(final IndividualGroup group) {
		int groupIndex = ((IndividualGroupsModel) this.individualGroupList.getModel()).indexOf(group);

		if ((groupIndex >= 0) && (groupIndex < ((IndividualGroupsModel) this.individualGroupList.getModel()).getSize())) {
			//
			this.individualGroupList.setSelectedIndex(groupIndex);
			this.individualGroupList.ensureIndexIsVisible(groupIndex);

		} else if (((IndividualGroupsModel) this.individualGroupList.getModel()).getSize() != 0) {
			//
			this.individualGroupList.setSelectedIndex(0);
			this.individualGroupList.ensureIndexIsVisible(0);

		} else {
			//
			updateIndividualGroupDiagram(null);
		}
	}

	/**
	 * 
	 * @param target
	 */
	public void selectByIndex(final int groupIndex) {

		if ((groupIndex >= 0) && (groupIndex < ((IndividualGroupsModel) this.individualGroupList.getModel()).getSize())) {
			//
			this.individualGroupList.setSelectedIndex(groupIndex);
			this.individualGroupList.ensureIndexIsVisible(groupIndex);

		} else if (((IndividualGroupsModel) this.individualGroupList.getModel()).getSize() != 0) {
			//
			this.individualGroupList.setSelectedIndex(0);
			this.individualGroupList.ensureIndexIsVisible(0);

		} else {
			//
			updateIndividualGroupDiagram(null);
		}
	}

	/**
	 * 
	 */
	public void updateGroupDiagram() {
		//
		updateIndividualGroupDiagram(getSelectedGroup());
	}

	/**
	 * 
	 */
	public void updateIndividualGroupDiagram(final IndividualGroup source) {
		//
		if (source == null) {
			//
			logger.debug("updateIndividualGroupDiagram(null)");

		} else {
			//
			logger.debug("updateIndividualGroupDiagram(size=" + source.size() + ")");

			//
			this.diagramScrollPanel.updateSource(Puck2KinOath.convert(source));
		}
	}
}
