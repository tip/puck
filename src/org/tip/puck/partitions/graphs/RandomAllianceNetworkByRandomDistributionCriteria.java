package org.tip.puck.partitions.graphs;

import org.tip.puck.graphs.random.DistributionType;

/**
 * 
 * @author TIP
 */
public class RandomAllianceNetworkByRandomDistributionCriteria {

	private int numberOfNodes;
	private int numberOfArcs;
	private DistributionType distributionType;
	private int powerFactor;
	private int numberOfRuns;
	private boolean extractRepresentative;

	/**
	 * 
	 */
	public RandomAllianceNetworkByRandomDistributionCriteria() {
		this.numberOfNodes = 100;
		this.numberOfArcs = 1000;
		this.distributionType = DistributionType.FREE;
		this.powerFactor = 1;
		this.numberOfRuns = 100;
		this.extractRepresentative = false;
	}

	public DistributionType getDistributionType() {
		return distributionType;
	}

	public int getNumberOfArcs() {
		return numberOfArcs;
	}

	public int getNumberOfNodes() {
		return numberOfNodes;
	}

	public int getNumberOfRuns() {
		return numberOfRuns;
	}

	public int getPowerFactor() {
		return powerFactor;
	}

	public boolean isExtractRepresentative() {
		return extractRepresentative;
	}

	public void setDistributionType(final DistributionType distributionType) {
		this.distributionType = distributionType;
	}

	public void setExtractRepresentative(final boolean extractRepresentative) {
		this.extractRepresentative = extractRepresentative;
	}

	public void setNumberOfArcs(final int numberOfArcs) {
		this.numberOfArcs = numberOfArcs;
	}

	public void setNumberOfNodes(final int numberOfNodes) {
		this.numberOfNodes = numberOfNodes;
	}

	public void setNumberOfRuns(final int numberOfRuns) {
		this.numberOfRuns = numberOfRuns;
	}

	public void setPowerFactor(final int powerFactor) {
		this.powerFactor = powerFactor;
	}
}
