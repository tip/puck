package org.tip.puck.net;

import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.util.Numberable;

import fr.devinsy.util.StringList;

/**
 * 
 * 
 * @author TIP
 */
public class Family implements Numberable, Attributable {

	private int id;
	private Individual husband;
	private Individual wife;
	private UnionStatus unionStatus;
	private Individuals children;
	private Attributes attributes;
	private Integer husbandOrder;
	private Integer wifeOrder;

	/**
	 * 
	 * @param husband
	 * @param wife
	 */
	public Family(final Individual husband, final Individual wife) {
		this.unionStatus = UnionStatus.UNMARRIED;
		this.husband = husband;
		this.wife = wife;
		this.children = new Individuals();
		this.attributes = new Attributes();
	}

	/**
	 * 
	 */
	public Family(final int id) {
		this.id = id;
		this.unionStatus = UnionStatus.UNMARRIED;
		this.children = new Individuals();
		this.attributes = new Attributes();
	}

	/**
	 * 
	 * @param id
	 * @param husband
	 * @param wife
	 */
	public Family(final int id, final Individual husband, final Individual wife) {
		this.id = id;
		this.unionStatus = UnionStatus.UNMARRIED;
		this.husband = husband;
		this.wife = wife;
		this.children = new Individuals();
		this.attributes = new Attributes();
	}

	/**
	 * 
	 * @param id
	 * @param husband
	 * @param wife
	 */
	public Family(final int id, final Individual husband, final Individual wife, final UnionStatus status) {
		this.id = id;
		if (status == null) {
			this.unionStatus = UnionStatus.UNMARRIED;
		} else {
			this.unionStatus = status;
		}
		this.husband = husband;
		this.wife = wife;
		this.children = new Individuals();
		this.attributes = new Attributes();
	}

	/**
	 * 
	 * @return
	 */
	public Attributes attributes() {
		return this.attributes;
	}

	/**
	 * 
	 */
	@Override
	public Family clone() {
		Family result;

		//
		Individual husbandClone = null;
		if (this.husband != null) {
			husbandClone = this.husband.clone();
		}

		//
		Individual wifeClone = null;
		if (this.wife != null) {
			wifeClone = this.wife.clone();
		}

		//
		result = new Family(this.id, husbandClone, wifeClone);

		//
		return result;
	}

	public Family clone(int id){
		Family result;
		
		result = clone();
		result.setId(id);
		
		//
		return result;
	}

	/**
	 * 
	 */
	public String getAttributeValue(final String label) {
		String result;

		Attribute attribute = this.attributes().get(label);
		if (attribute == null) {
			result = null;
		} else {
			result = attribute.getValue();
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public Individuals getChildren() {
		Individuals result;

		result = this.children;

		//
		return result;
	}

	public Individuals getChildren(final Gender gender) {
		Individuals result;

		result = new Individuals();
		for (Individual child : this.children) {
			if (child.getGender() == gender) {
				result.add(child);
			}
		}
		//
		return result;
	}

	/**
	 * Returns the parent playing the paternel role.
	 */
	public Individual getFather() {
		Individual result;

		result = this.husband;
		//
		return result;
	}

	/**
	 * Returns the parent playing the paternel role.
	 */
	public Individual getHusband() {
		Individual result;

		result = this.husband;

		//
		return result;
	}

	public Integer getHusbandOrder() {
		return this.husbandOrder;
	}

	public Family getHusbandsOriginFamily() {
		Family result;

		if (this.husband == null) {
			result = null;
		} else {
			result = this.husband.getOriginFamily();
		}
		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getId() {
		int result;

		result = this.id;

		//
		return result;
	}

	public Integer getMaxEnd() {
		Integer result;

		result = IndividualValuator.extractYearAsInt(getAttributeValue("DIV_DATE"));
		if (result == null) {
			Integer husbandDeatYear = null;
			if (this.husband != null) {
				husbandDeatYear = IndividualValuator.extractYearAsInt(this.husband.getAttributeValue("DEAT_DATE"));
			}
			Integer wifeDeatYear = null;
			if (this.wife != null) {
				wifeDeatYear = IndividualValuator.extractYearAsInt(this.wife.getAttributeValue("DEAT_DATE"));
			}
			if (husbandDeatYear == null) {
				result = wifeDeatYear;
			} else if (wifeDeatYear == null) {
				result = husbandDeatYear;
			} else {
				result = Math.min(husbandDeatYear, wifeDeatYear);
			}
		}
		//
		return result;
	}

	public Individuals getMembers() {
		Individuals result;

		result = new Individuals();

		result.add(this.husband);
		result.add(this.wife);
		result.add(this.children);

		//
		return result;
	}

	public Integer getMinStart() {
		Integer result;

		result = IndividualValuator.extractYearAsInt(getAttributeValue("MARR_DATE"));
		if (result == null) {
			for (Individual child : this.getChildren()) {
				Integer birtYear = IndividualValuator.extractYearAsInt(child.getAttributeValue("BIRT_DATE"));
				if (birtYear != null && (result == null || birtYear < result)) {
					result = birtYear;
				}
			}
		}
		//
		return result;
	}

	public Individual getMissingHusband(final int lastIndId) {
		Individual result;

		result = new Individual(lastIndId + 2 * this.id - 1, "Father of family " + this.id, Gender.MALE);
		//
		return result;
	}

	public Individual getMissingWife(final int lastIndId) {
		Individual result;

		result = new Individual(lastIndId + 2 * this.id, "Mother of family " + this.id, Gender.FEMALE);
		//
		return result;
	}

	/**
	 * Returns the parent playing the paternel role.
	 */
	public Individual getMother() {
		Individual result;

		result = this.wife;

		//
		return result;
	}

	/**
	 * 
	 * @param parent
	 * @return
	 */
	public Individual getOtherParent(final Individual parent) {
		Individual result;

		if (parent == this.husband) {
			//
			result = this.wife;

		} else if (parent == this.wife) {
			//
			result = this.husband;

		} else {
			//
			result = null;
		}

		//
		return result;
	}

	public Individual getParent(final Gender gender) {
		Individual result;

		switch (gender) {
			case MALE:
				result = this.husband;
			break;
			case FEMALE:
				result = this.wife;
			break;
			default:
				result = null;
		}
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individuals getParents() {
		Individuals result;

		result = new Individuals();
		if (this.husband != null) {
			result.add(this.husband);
		}
		if (this.wife != null) {
			result.add(this.wife);
		}

		//
		return result;
	}

	public String getRoleName(final Individual ego, final Individual alter) {
		String result;

		result = null;

		if (ego == alter) {
			result = "EGO";
		} else if (ego == this.husband) {
			if (alter == this.wife) {
				result = "SPOUSE";
			} else if (this.children.contains(alter)) {
				result = "PARENT";
			}
		} else if (ego == this.wife) {
			if (alter == this.husband) {
				result = "SPOUSE";
			} else if (this.children.contains(alter)) {
				result = "PARENT";
			}
		} else if (this.children.contains(ego)) {
			if (alter == this.husband || alter == this.wife) {
				result = "CHILD";
			} else if (this.children.contains(alter)) {
				result = "SIBLING";
			}
		}
		//
		return result;
	}

	/**
	 * This method returns the union order of a parent.
	 * 
	 * @param parent
	 *            the individual who searching for union order.
	 * 
	 * @return the union order.
	 */
	public Integer getUnionOrder(final Individual parent) {
		Integer result;

		if (parent == this.husband) {
			//
			result = this.husbandOrder;

		} else if (parent == this.wife) {
			//
			result = this.wifeOrder;

		} else {
			//
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public UnionStatus getUnionStatus() {
		return this.unionStatus;
	}

	/**
	 * Returns the parent playing the paternel role.
	 */
	public Individual getWife() {
		Individual result;

		result = this.wife;

		//
		return result;
	}

	public Integer getWifeOrder() {
		return this.wifeOrder;
	}

	public Family getWifesOriginFamily() {
		Family result;

		if (this.wife == null) {
			result = null;
		} else {
			result = this.wife.getOriginFamily();
		}
		//
		return result;
	}

	@Override
	public String hashKey() {
		String wifeStr = "?";
		String husbandStr = "?";
		if (this.wife != null) {
			wifeStr = "" + this.wife.getId();
		}
		if (this.husband != null) {
			husbandStr = "" + this.husband.getId();
		}
		return wifeStr + " [+] " + husbandStr;
	}

	/**
	 * includes married + divorced
	 * 
	 * @return
	 */
	public boolean hasMarried() {
		boolean result;

		result = this.unionStatus.hasMarried();

		//
		return result;
	}

	public boolean isActiveAt(final int year) {
		boolean result;

		Integer minStart = getMinStart();
		Integer maxEnd = getMaxEnd();

		result = (minStart != null && minStart <= year) && (maxEnd == null || year <= maxEnd);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isDivorced() {
		boolean result;

		result = this.unionStatus.isDivorced();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		boolean result;

		if ((isOrphan()) && (isSterile()) && (this.attributes.isEmpty())) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmptyAndSingleParent() {
		boolean result;

		if ((isSingleParent()) && (isSterile()) && (this.attributes.isEmpty())) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isFather(final Individual parent) {
		boolean result;

		if (parent == null) {
			//
			result = false;

		} else if (this.husband == parent) {
			//
			result = true;

		} else {
			//
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isFertile() {
		boolean result;

		result = this.children.size() != 0;

		//
		return result;
	}
	
	public boolean isSameGender(){
		boolean result;
		
		result = (this.husband!=null && this.wife!=null && this.husband.getGender()==this.wife.getGender());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isMother(final Individual parent) {
		boolean result;

		if (parent == null) {
			//
			result = false;

		} else if (this.wife == parent) {
			//
			result = true;

		} else {
			//
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotSingleParent() {
		boolean result;

		result = !isSingleParent();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isOrphan() {
		boolean result;

		if ((this.husband == null) && (this.wife == null)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isParent(final Individual parent) {
		boolean result;

		if (parent == null) {
			//
			result = false;

		} else if ((parent == this.husband) || (parent == this.wife)) {
			//
			result = true;

		} else {
			//
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isSingleParent() {
		boolean result;

		if (((this.husband == null) && (this.wife != null)) || ((this.husband != null) && (this.wife == null))) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isSterile() {
		boolean result;

		result = this.children.size() == 0;

		//
		return result;
	}

	public int numberOfParents() {
		int result;

		if (this.husband != null && this.wife != null) {
			result = 2;
		} else if (this.husband == null && this.wife == null) {
			result = 0;
		} else {
			result = 1;
		}
		//
		return result;
	}

	/**
	 * 
	 * @param label
	 * @param value
	 */
	public void setAttribute(final String label, final String value) {
		this.attributes().put(label, value);
	}

	/**
	 * 
	 * @param married
	 */
	public void setDivorced() {
		this.unionStatus = UnionStatus.DIVORCED;
	}

	public void setFather(final Individual father) {
		this.husband = father;
	}

	public void setHusband(final Individual husband) {
		this.husband = husband;
	}

	/**
	 * This method sets the union order for the husband of this family.
	 * 
	 * @param order
	 *            positive integer (> 0), any else value sets null.
	 */
	public void setHusbandOrder(final Integer order) {
		//
		if ((order == null) || (order < 1)) {
			//
			this.husbandOrder = null;

		} else {
			//
			this.husbandOrder = order;
		}
	}

	/**
	 * 
	 */
	@Override
	public void setId(final int id) {
		this.id = id;
	}

	/**
	 * 
	 * @param married
	 */
	public void setMarried() {
		this.unionStatus = UnionStatus.MARRIED;
	}

	/**
	 * 
	 * @param married
	 */
	public void setMarried(final boolean married) {
		if (married == true) {
			this.unionStatus = UnionStatus.MARRIED;
		} else {
			this.unionStatus = UnionStatus.UNMARRIED;
		}
	}

	public void setMother(final Individual mother) {
		this.wife = mother;
	}

	/**
	 * 
	 * @param parent
	 */
	public void setSpouses(final Individual husband, final Individual wife) {
		this.husband = husband;
		this.wife = wife;
	}

	/**
	 * This method sets the union order of a parent.
	 * 
	 * @param parent
	 *            the individual target of the action.
	 * 
	 * @param order
	 *            value to set.
	 * 
	 */
	public void setUnionOrder(final Individual parent, final Integer order) {
		//
		if (parent == this.husband) {
			//
			setHusbandOrder(order);

		} else if (parent == this.wife) {
			//
			setWifeOrder(order);
		}
	}

	/**
	 * 
	 * @param value
	 */
	public void setUnionStatus(final UnionStatus value) {
		if (value == null) {
			this.unionStatus = UnionStatus.UNMARRIED;
		} else {
			this.unionStatus = value;
		}
	}

	/**
	 * 
	 */
	public void setUnmarried() {
		this.unionStatus = UnionStatus.UNMARRIED;
	}

	public void setWife(final Individual wife) {
		this.wife = wife;
	}

	/**
	 * This method sets the union order for the wife of this family.
	 * 
	 * @param order
	 *            positive integer (> 0), any else value sets null.
	 */
	public void setWifeOrder(final Integer order) {
		//
		if ((order == null) || (order < 1)) {
			//
			this.wifeOrder = null;

		} else {
			//
			this.wifeOrder = order;
		}
	}

	/**
	 * 
	 * @return
	 */
	public String toNameString() {
		String result;

		String husbandString;
		if (this.husband == null) {
			husbandString = "";
		} else {
			husbandString = this.husband.getName();
		}

		String wifeString;
		if (this.wife == null) {
			wifeString = "";
		} else {
			wifeString = this.wife.getName();
		}

		String separator;
		if (this.husband == null || this.wife == null) {
			separator = "";
		} else {
			separator = " & ";
		}

		result = husbandString + separator + wifeString;

		//
		return result;
	}
	
	public String signatureTab() {
		String result;

		String husbandString;
		if (this.husband == null) {
			husbandString = "";
		} else {
			husbandString = this.husband.toString();
		}

		String wifeString;
		if (this.wife == null) {
			wifeString = "";
		} else {
			wifeString = this.wife.toString();
		}

		String separator;
		if (this.husband == null || this.wife == null) {
			separator = "";
		} else {
			separator = " & ";
		}

		StringList buffer = new StringList(5);
		buffer.append(this.id);
		buffer.append("\t");
		buffer.append(husbandString);
		buffer.append(separator);
		buffer.append(wifeString);

		result = buffer.toString();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		String husbandString;
		if (this.husband == null) {
			husbandString = "";
		} else {
			husbandString = this.husband.toString();
		}

		String wifeString;
		if (this.wife == null) {
			wifeString = "";
		} else {
			wifeString = this.wife.toString();
		}

		String separator;
		if (this.husband == null || this.wife == null) {
			separator = "";
		} else {
			separator = " & ";
		}

		StringList buffer = new StringList(5);
		buffer.append(this.id);
		buffer.append(" ");
		buffer.append(husbandString);
		buffer.append(separator);
		buffer.append(wifeString);

		result = buffer.toString();

		//
		return result;
	}
}
