package org.tip.puck.partitions;

import java.util.Comparator;

import org.tip.puck.util.Value;

/**
 * 
 * @author TIP
 * 
 */
public class ClusterComparatorByValue implements Comparator<Cluster<?>> {
	/**
	 * 
	 */
	@Override
	public int compare(final Cluster<?> alpha, final Cluster<?> bravo) {
		int result;

		//
		Value value1;
		if (alpha == null) {
			value1 = null;
		} else {
			value1 = alpha.getValue();
		}

		//
		Value value2;
		if (bravo == null) {
			value2 = null;
		} else {
			value2 = bravo.getValue();
		}

		//
		result = Value.compare(value1, value2);

		//
		return result;
	}
}
