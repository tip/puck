package org.tip.puck.mas;

/**
 * @author Telmo Menezes
 *
 */
public class WFDivorce implements WeightFactor {
    private double wgtDivorce;
    
    public WFDivorce(double wgtDivorce) {
        this.wgtDivorce = wgtDivorce;
    }
    
    public String factorName(){
    	return "divorce";
    }
    
    @Override
    public double factor(Agent mother, Agent father) {
        if ((father.getCurrentMarriage() == null) && (mother.getCurrentMarriage() == null)) {
            // first marriage
            return 1.0;
        }
        if (father.isPartnerOf(mother)) {
            // stay in marriage
            return 1.0;
        }
        else {
            // divorce
            return wgtDivorce;
        }
    }
    
    @Override
    public boolean appliesToMarriage() {return false;}
    
    @Override
    public boolean appliesToDivorce() {return false;}
}