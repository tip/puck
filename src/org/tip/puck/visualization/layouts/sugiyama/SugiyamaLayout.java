package org.tip.puck.visualization.layouts.sugiyama;

import java.awt.Color;
import java.util.*;
import org.gephi.graph.api.*;
import org.gephi.layout.plugin.AbstractLayout;
import org.gephi.layout.spi.LayoutBuilder;
import org.gephi.layout.spi.LayoutProperty;
import org.tip.puck.visualization.layouts.GraphSource;

/**
 * @date Jun 25, 2012
 *
 * @author Edoardo Savoia
 */
public class SugiyamaLayout extends AbstractLayout {

    /**
     * Implementation.
     *
     * First of all, the Algorithm searches for the Graph's roots. Then populate
     * levels and stores them in
     * <code>levels</code>. The Member levels contains LinkedList Objects and
     * the LinkedList per level contains Nodes. After that Algorithm tries to
     * solve the edge crosses from level to level and goes top down and bottom
     * up. After minimization of the edge crosses the algorithm moves each node
     * to its barycenter.
     */

    private static final Orientation DEFAULT_ORIENTATION = Orientation.TOP;
    private static final int DEFAULT_HORIZONTAL_SPACING = 100;
    private static final int DEFAULT_VERTICAL_SPACING = -500;
    private static final int DEFAULT_MARRIED_HORIZONTAL_SPACING = 50;
    private static final int DEFAULT_MARRIED_VERTICAL_SPACING = 100;

    public static enum Orientation {
	TOP, LEFT
    };

    private int gridAreaSize = Integer.MIN_VALUE;
    private int horzSpacing = DEFAULT_HORIZONTAL_SPACING;
    private int vertSpacing = DEFAULT_VERTICAL_SPACING;
    private Orientation orientation = DEFAULT_ORIENTATION;

    private boolean executing = false;
    private GraphSource graphSource;
    private Set<Node> traversalSet = new HashSet<Node>();
    private LinkedList<LinkedList<Node>> levels;
    private LinkedList<Node> roots;
    private Set<Node> marriedNodes = new HashSet<Node>();

    public SugiyamaLayout(LayoutBuilder layoutBuilder) {
	super(layoutBuilder);
    }

    @Override
    public void setGraphModel(GraphModel graphModel) {
	super.setGraphModel(graphModel);
	graphSource = new GraphSource(graphModel.getMixedGraph());
    }

    @Override
    public LayoutProperty[] getProperties() {
	return null;
    }

    @Override
    public void resetPropertiesValues() {
    }

    /**
     * Sets node's color.
     */
    private void setColor(Node node, float[] rgbColor) {
	node.getNodeData().setR(rgbColor[0]);
	node.getNodeData().setG(rgbColor[1]);
	node.getNodeData().setB(rgbColor[2]);
    }

    /**
     * Gets node's color.
     */
    private float[] getColor(Node node) {
	float[] rgbColor = new float[3];
	rgbColor[0] = node.getNodeData().r();
	rgbColor[1] = node.getNodeData().g();
	rgbColor[2] = node.getNodeData().b();
	return rgbColor;
    }

    /**
     * Generates a rainbow pastel color. Colors are represented by 3 float
     * values from 0 to 1 one for each principal color: red, green, blue.
     */
    private float[] randomPastelColor() {
	Random random = new Random();
	final float hue = random.nextFloat();
	final float saturation = 0.9f;//1.0 for brilliant, 0.0 for dull
	final float luminance = 1.0f; //1.0 for brighter, 0.0 for black
	Color hsbColor = Color.getHSBColor(hue, saturation, luminance);

	float[] rgb = new float[3];
	rgb[0] = (1.0f * (float) hsbColor.getRed()) / 255.0f;
	rgb[1] = (1.0f * (float) hsbColor.getGreen()) / 255.0f;
	rgb[2] = (1.0f * (float) hsbColor.getBlue()) / 255.0f;
	return rgb;
    }

    @Override
    public void initAlgo() {
	executing = true;
	// search all roots
	// populate roots and marriedNodes
	searchRoots();
    }

    @Override
    public void goAlgo() {
	if (!executing) {
	    return;
	}
	// fill levels and color nodes
	fillLevels(roots); // populate graphLevels

	System.out.println("levels height: " + levels.size());
	for (LinkedList<Node> linkedList : levels) {
	    System.out.println("level size " + linkedList.size());
	}

	// solves the edge crosses
	solveEdgeCrosses(levels);

	// move all nodes into the barycenter
	moveToBarycenter(levels);

	System.out.println("Place");
	for (LinkedList<Node> level : levels) {
	    for (Node node : level) {
		NodeWrapper wrapper = NodeWrapper.getData(node);

		if (orientation.equals(Orientation.TOP)) {
		    float xCoordinate = 10.0f + 1.0f * (wrapper.getGridPosition() * horzSpacing);
		    float yCoordinate = 10.0f + 1.0f * (wrapper.getLevel() * vertSpacing);
		    node.getNodeData().setX(xCoordinate);
		    node.getNodeData().setY(yCoordinate);
		}
		else {
		    float yCoordinate = 10.0f + 1.0f * (wrapper.getGridPosition() * vertSpacing);
		    float xCoordinate = 10.0f + 1.0f * (wrapper.getLevel() * horzSpacing);
		    node.getNodeData().setX(xCoordinate);
		    node.getNodeData().setY(yCoordinate);
		}
	    }
	}
	moveMarriedNodes();
    }

    @Override
    public void endAlgo() {
	traversalSet.clear();
	MixedGraph graph = graphModel.getMixedGraph();
	graph.readLock();

	Node[] nodes = graph.getNodes().toArray();
	for (Node node : nodes) {
	    NodeWrapper.clean(node);
	}
	graph.readUnlock();
	roots.clear();
	levels.clear();
	marriedNodes.clear();
	executing = false;
    }

    private void moveMarriedNodes() {
	MixedGraph graph = graphModel.getMixedGraph();
	for (Node node : marriedNodes) {

	    float cX = 0.0f;
	    float cY = 0.0f;
	    int counter = 0;

	    EdgeIterable edges = graph.getEdges(node);
	    for (Edge edge : edges) {
		if (!edge.isDirected()) {
		    Node opposite = graph.getOpposite(node, edge);
		    cX += opposite.getNodeData().x();
		    cY += opposite.getNodeData().y();
		    counter++;
		}
	    }
	    if (counter > 1) {
		node.getNodeData().setX(cX / (counter * 1.0f));
		node.getNodeData().setY(cY / (counter * 1.0f));
	    }
	    else if (counter == 1) {
		node.getNodeData().setX(cX + (DEFAULT_MARRIED_HORIZONTAL_SPACING * 1.0f));
		node.getNodeData().setY(cY + (DEFAULT_MARRIED_VERTICAL_SPACING * 1.0f));
	    }


	}

    }

    /**
     * Searches all Roots and Married nodes for the current Graph. Populates
     * <code>roots</code> and
     * <code>marriedNodes</code>.
     */
    private void searchRoots() {
	roots = new LinkedList<Node>();
	MixedGraph graph = graphModel.getMixedGraph();
	graph.readLock();
	Node[] nodes = graph.getNodes().toArray();
	for (Node node : nodes) {
	    node.getNodeData().setX(0.0f);
	    node.getNodeData().setY(5000.0f);

	    Set<Node> parents = graphSource.getParents(node);
	    if (parents.isEmpty()) {
		if (!graphSource.getChildren(node).isEmpty()) {
		    roots.add(node);
		}
		else {
		    // married nodes
		    marriedNodes.add(node);
		    node.getNodeData().setColor(0.0f, 0.0f, 0.0f);
		}
	    }
	}
	graph.readUnlock();
    }

    /**
     * Method fills the levels and stores them in
     * <code>levels</code>. Each level is represented by a LinkedList. These
     * LinkedLists are elements in
     * <code>levels</code> LinkedList.
     *
     */
    private void fillLevels(LinkedList<Node> roots) {
	levels = new LinkedList<LinkedList<Node>>();
	// clear visited nodes
	traversalSet.clear();

	for (Node root : roots) {
	    setColor(root, randomPastelColor());
	    fillLevels(levels, 0, root); // 0 indicates level 0 i.e root level
	}
    }

    /**
     * Fills the LinkedList for the specified level and initialize wrapper for
     * nodes. After that method is called for each child node.
     */
    private void fillLevels(LinkedList<LinkedList<Node>> levels, int level, Node rootNode) {
	// this is a recursive function
	// precondition control
	if (rootNode == null) {
	    return;
	}

	// be sure that a LinkedList container exists for the current level
	if (levels.size() == level) {
	    levels.add(level, new LinkedList<Node>());
	}

	// if node is already visited return
	if (traversalSet.contains(rootNode)) {
	    return;
	}

	// mark node as visited for cycle tests
	traversalSet.add(rootNode);

	// get the Level LinkedList
	LinkedList<Node> currentLevel = levels.get(level);


	int numberForTheEntry = currentLevel.size();

	// Create a wrapper for the node
	NodeWrapper wrapper = new NodeWrapper(level, numberForTheEntry);
	NodeWrapper.initData(rootNode, wrapper);

	// put the current node into the current level
	currentLevel.add(rootNode);

	Set<Node> children = graphSource.getChildren(rootNode);
	for (Node child : children) {
	    setColor(child, getColor(rootNode));
	    fillLevels(levels, level + 1, child);
	}

	if (currentLevel.size() > gridAreaSize) {
	    gridAreaSize = currentLevel.size();
	}

    }

    private void solveEdgeCrosses(LinkedList<LinkedList<Node>> levels) {
	int movementsCurrentLoop = -1;

	while (movementsCurrentLoop != 0) {
	    // reset the movements per loop count
	    movementsCurrentLoop = 0;

	    // top down
	    for (int i = 0; i < levels.size() - 1; i++) {
		movementsCurrentLoop += solveEdgeCrosses(true, levels, i);
	    }

	    // bottom up
	    for (int i = levels.size() - 1; i >= 0; i--) {
		movementsCurrentLoop += solveEdgeCrosses(false, levels, i);
	    }
	}
    }

    private int solveEdgeCrosses(boolean down, LinkedList<LinkedList<Node>> levels, int levelIndex) {
	// Get the current level
	LinkedList<Node> currentLevel = levels.get(levelIndex);
	int movements = 0;

	// restore the old sort
	Node[] levelSortBefore = currentLevel.toArray(new Node[currentLevel.size()]);

	// new sort
	Collections.sort(currentLevel, new Comparator<Node>() {

	    @Override
	    public int compare(Node o1, Node o2) {
		return NodeWrapper.getData(o1).compareTo(NodeWrapper.getData(o2));
	    }
	});

	// test for movements
	for (int j = 0; j < levelSortBefore.length; j++) {
	    if (NodeWrapper.getData(levelSortBefore[j]).getEdgeCrossesIndicator() != NodeWrapper.getData(currentLevel.get(j)).getEdgeCrossesIndicator()) {
		movements++;
	    }
	}
	// Collections Sort() sorts the highest value to the first value
	for (int j = currentLevel.size() - 1; j >= 0; j--) {
	    Node sourceNode = currentLevel.get(j);

	    NodeWrapper sourceWrapper = NodeWrapper.getData(sourceNode);

	    Collection<Edge> edgeList = graphSource.getNeighbourEdges(sourceNode);

	    for (Edge edge : edgeList) {
		// if it is a forward edge follow it
		Node targetNode = null;
		if (down && sourceNode == edge.getSource()) {
		    targetNode = edge.getTarget();
		}
		if (!down && sourceNode == edge.getTarget()) {
		    targetNode = edge.getSource();
		}
		if (targetNode != null) {
		    NodeWrapper targetWrapper = NodeWrapper.getData(targetNode);

		    // do it only if the edge is a forward edge to a deeper level
		    if (down && targetWrapper != null && targetWrapper.getLevel() > levelIndex) {
			targetWrapper.addToEdgeCrossesIndicator(sourceWrapper.getEdgeCrossesIndicator());
		    }
		    if (!down && targetWrapper != null && targetWrapper.getLevel() < levelIndex) {
			targetWrapper.addToEdgeCrossesIndicator(sourceWrapper.getEdgeCrossesIndicator());
		    }
		}
	    }
	}
	return movements;
    }

    private void moveToBarycenter(LinkedList<LinkedList<Node>> levels) {
	System.out.println("moveToBarycenter");
	MixedGraph graph = graphModel.getMixedGraph();
	graph.readLock();
	Node[] nodes = graph.getNodes().toArray();
	for (Node node : nodes) {

	    NodeWrapper currentwrapper = NodeWrapper.getData(node);

	    Collection<Edge> edgeList = graphSource.getNeighbourEdges(node);

	    for (Edge edge : edgeList) {
		Node neighbourNode = null;

		if (node == edge.getSource()) {
		    neighbourNode = edge.getTarget();
		}
		else {
		    if (node == edge.getTarget()) {
			neighbourNode = edge.getSource();
		    }
		}

		if ((neighbourNode != null) && (neighbourNode != node)) {

		    NodeWrapper neighbourWrapper = NodeWrapper.getData(neighbourNode);

		    if (!(currentwrapper == null || neighbourWrapper == null || currentwrapper.getLevel() == neighbourWrapper.getLevel())) {
			currentwrapper.incrementPriority();
		    }
		}
	    }
	}
	graph.readUnlock();
	for (LinkedList<Node> level : levels) {
	    int pos = 0;
	    for (Node node : level) {
		// calculate the initial Grid Positions 1, 2, 3, .... per Level
		NodeWrapper.getData(node).setGridPosition(pos++);
	    }
	}

	int movementsCurrentLoop = -1;

	while (movementsCurrentLoop != 0) {
	    // reset movements
	    movementsCurrentLoop = 0;

	    // top down
	    for (int i = 0; i < levels.size(); i++) {
		movementsCurrentLoop += moveToBarycenter(levels, i);
	    }
	    // bottom up
	    for (int i = levels.size() - 1; i >= 0; i--) {
		movementsCurrentLoop += moveToBarycenter(levels, i);
	    }
	}
    }

    private int moveToBarycenter(LinkedList<LinkedList<Node>> levels, int levelIndex) {
	// Counter for the movements
	int movements = 0;

	// Get the current level
	LinkedList<Node> currentLevel = levels.get(levelIndex);

	for (int currentIndexInTheLevel = 0; currentIndexInTheLevel < currentLevel.size(); currentIndexInTheLevel++) {
	    Node node = currentLevel.get(currentIndexInTheLevel);

	    NodeWrapper sourceWrapper = NodeWrapper.getData(node);

	    float gridPositionsSum = 0.0f;
	    float countNodes = 0.0f;


	    Collection<Edge> edgeList = graphSource.getNeighbourEdges(node);

	    for (Edge edge : edgeList) {
		// if it is a forward edge follow it
		Node neighbourNode = null;
		if (node == edge.getSource()) {
		    neighbourNode = edge.getTarget();
		}
		else {
		    if (node == edge.getSource()) {
			neighbourNode = edge.getTarget();
		    }
		}

		if (neighbourNode != null) {

		    NodeWrapper targetWrapper = NodeWrapper.getData(neighbourNode);

		    if (!(targetWrapper == sourceWrapper) || (targetWrapper == null || targetWrapper.getLevel() == levelIndex)) {
			gridPositionsSum += targetWrapper.getGridPosition();
			countNodes++;
		    }
		}
	    }

	    if (countNodes > 0) {
		float tmp = (gridPositionsSum / countNodes);
		int newGridPosition = Math.round(tmp);
		boolean toRight = (newGridPosition > sourceWrapper.getGridPosition());

		boolean moved = true;

		while (newGridPosition != sourceWrapper.getGridPosition() && moved) {
		    moved = move(toRight, currentLevel, currentIndexInTheLevel, sourceWrapper.getPriority());
		    if (moved) {
			movements++;
		    }
		}
	    }
	}
	return movements;
    }

    /**
     * @param toRight 
     *	<tt>true</tt> = try to move the currentWrapper to right;
     *	<tt>false</tt> = try to move the currentWrapper to left;
     *
     * @param currentLevel LinkedList which contains nodes for the current level
     *
     * @return The free GridPosition or -1 is position is not free.
     */
    private boolean move(boolean toRight, LinkedList<Node> currentLevel, int currentIndexInTheLevel, int currentPriority) {

	NodeWrapper currentWrapper = NodeWrapper.getData(currentLevel.get(currentIndexInTheLevel));

	boolean moved = false;
	int neighbourIndexInTheLevel = currentIndexInTheLevel + (toRight ? 1 : -1);
	int newGridPosition = currentWrapper.getGridPosition() + (toRight ? 1 : -1);

	if (0 > newGridPosition || newGridPosition >= gridAreaSize) {
	    return false;
	}

	// if the node is the first or the last we can move
	if (toRight && currentIndexInTheLevel == currentLevel.size() - 1 || !toRight && currentIndexInTheLevel == 0) {
	    moved = true;
	}
	else {
	    // else get the neighbour and ask his gridposition
	    // if he has the requested new grid position
	    // check the priority

	    NodeWrapper neighbourWrapper = NodeWrapper.getData(currentLevel.get(neighbourIndexInTheLevel));

	    int neighbourPriority = neighbourWrapper.getPriority();

	    if (neighbourWrapper.getGridPosition() == newGridPosition) {
		if (neighbourPriority >= currentPriority) {
		    return false;
		}
		else {
		    moved = move(toRight, currentLevel, neighbourIndexInTheLevel, currentPriority);
		}
	    }
	    else {
		moved = true;
	    }
	}

	if (moved) {
	    currentWrapper.setGridPosition(newGridPosition);
	}
	return moved;
    }

}
