package org.tip.puck.report;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.devinsy.util.StringList;

/**
 * 
 * Note: label and value are not nullable.
 * 
 * @author TIP
 */
public class ReportOutputs implements Iterable<Object> {

	public static final String LINE_SEPARATOR = "\n";
	private List<Object> objects;

	/**
	 * 
	 */
	public ReportOutputs() {
		this.objects = new ArrayList<Object>();
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	public ReportOutputs append(final Object object) {
		ReportOutputs result;

		if (object != null) {
			StringList strings = new StringList();
			strings.append(object.toString());
			append(strings);
		}

		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	public ReportOutputs append(final Report report) {
		ReportOutputs result;

		if (report != null) {
			this.objects.add(report);
		}

		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	public ReportOutputs append(final ReportAttributes items) {
		ReportOutputs result;

		if (items != null) {
			this.objects.add(items);
		}

		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	public ReportOutputs append(final ReportChart graph) {
		ReportOutputs result;

		if (graph != null) {
			this.objects.add(graph);
		}

		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	public ReportOutputs append(final ReportOutputs outputs) {
		ReportOutputs result;

		if (outputs != null) {
			for (Object output : outputs) {
				append(output);
			}
		}

		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	public ReportOutputs append(final ReportRawData rawData) {
		ReportOutputs result;

		if (rawData != null) {
			this.objects.add(rawData);
		}

		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	public ReportOutputs append(final ReportTable table) {
		ReportOutputs result;

		if (table != null) {
			this.objects.add(table);
		}

		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	public ReportOutputs append(final StringList strings) {
		ReportOutputs result;

		if (strings != null) {
			if ((this.objects.isEmpty() || (!(last() instanceof StringList)))) {
				this.objects.add(strings);
			} else if (last() instanceof StringList) {
				setLast(((StringList) last()).append(strings));
			}
		}

		result = this;

		//
		return result;
	}

	/**
	 * 
	 */
	public ReportOutputs appendln() {
		ReportOutputs result;

		append(LINE_SEPARATOR);

		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	public ReportOutputs appendln(final Object object) {
		ReportOutputs result;

		append(object).appendln();

		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	public ReportOutputs appendln(final ReportAttributes items) {
		ReportOutputs result;

		append(items);
		appendln();

		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	public ReportOutputs appendln(final ReportChart graph) {
		ReportOutputs result;

		append(graph);
		appendln();

		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	public ReportOutputs appendln(final ReportRawData rawData) {
		ReportOutputs result;

		append(rawData);
		appendln();

		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	public ReportOutputs appendln(final ReportTable table) {
		ReportOutputs result;

		append(table);
		appendln();

		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param string
	 * @return
	 */
	public ReportOutputs appendln(final StringList strings) {
		ReportOutputs result;

		append(strings).appendln();

		result = this;

		//
		return result;
	}


	/**
	 * 
	 * @return
	 */
	public boolean containsSubReport() {
		boolean result;

		boolean ended = false;
		result = false;
		Iterator<Object> iterator = this.iterator();
		while (!ended) {
			if (iterator.hasNext()) {
				Object item = iterator.next();
				if (item instanceof Report) {
					ended = true;
					result = true;
				}
			} else {
				ended = true;
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public Object get(final int index) {
		Object result;

		result = this.objects.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int getCount() {
		int result;

		result = this.objects.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		boolean result;

		result = this.objects.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty() {
		boolean result;

		result = !this.objects.isEmpty();

		//
		return result;
	}

	@Override
	public Iterator<Object> iterator() {
		Iterator<Object> result;

		result = this.objects.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	protected Object last() {
		Object result;

		if (this.objects.isEmpty()) {
			result = null;
		} else {
			result = this.objects.get(this.objects.size() - 1);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param object
	 */
	protected void setLast(final Object object) {
		if (object != null) {
			this.objects.set(this.objects.size() - 1, object);
		}
	}
	

}
