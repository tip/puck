package org.tip.puck.graphs.onemode;

import org.tip.puck.PuckException;
import org.tip.puck.graphs.onemode.OMGraph.GraphMode;
import org.tip.puck.net.Family;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.net.workers.NetUtils;

public class GraphModeTransformer {
	
	// Move to Net class
	public static Individuals monogamousSameSexSiblings(Individual individual) {
		Individuals result;
		
		result = new Individuals();
		Family family = individual.getOriginFamily();
		if (family != null) {
		    for (Individual sibling : family.getChildren()){
		        if (sibling!=individual && sibling.getGender()==individual.getGender() && sibling.spouses().size()==1){
		            result.add(sibling);
		        }
		    }
		}
		//
		return result;
	}
	
	// Replace by RandomUtils draw
	private static Individual randomDraw (Individuals individuals){
		int randomIndex = (int)(Math.random()*individuals.size());
		return individuals.toList().get(randomIndex);
	}
	
	public static Net omGraphToNet (OMGraph omGraph, GraphMode mode) throws PuckException{
		Net result;
		
		switch (mode){
		case OREGRAPH:
			result = oreGraphToNet(omGraph);
			break;
		case PGRAPH:
			result = pGraphToNet(omGraph);
			break;
		default:
			result = null;
		}
		//
		return result;
	}
	
	public static OMGraph netToOmGraph (Net net, GraphMode mode){
		OMGraph result;
		
		switch (mode){
		case OREGRAPH:
			result = netToOreGraph(net);
			break;
		case PGRAPH:
			result = netToPGraph(net);
			break;
		default:
			result = null;
		}
		//
		return result;
	}

	public static Net oreGraphToNet (OMGraph oreGraph){
		Net result;
		
		result = new Net();
		
		//extract Individuals
		for (OMNode node : oreGraph.getNodes()){
			if (!node.isVirtual()){
				Individual individual = node.getIndividual().clone();
				result.individuals().add(individual);
			}
		}
		
		//extract Individuals and OriginFamilies
		int id = 0;
		for (OMNode node : oreGraph.getNodes()){
			Individual father = null;
			Individual mother = null;
			Family family = null;
				Individual child = result.individuals().getById(node.getIndividual().getId());
				if (node.getMaleLink()!=null) {
					father = result.individuals().getById(node.getMaleLink().getIndividual().getId());
				}
				if (node.getFemaleLink()!=null) {
					mother = result.individuals().getById(node.getFemaleLink().getIndividual().getId());
				}
				if (father!=null && mother!=null){
					family = result.families().getBySpouses(father, mother);
					if (family == null){
						family = new Family(id,father,mother);
						result.families().add(family);
						id++;
					}
				} else if (father!=null || mother!=null){
					family = new Family(id,father,mother);
					result.families().add(family);
					id++;
				}
				
				if (family !=null){
					family.setMarried(node.hasMarriedParents());
					if (!node.isVirtual()){
						child.setOriginFamily(family);
						family.getChildren().add(child);
					}
				}
		}
		
		// set PersonalFamilies
		for (Family family : result.families()){
			Individual husband = family.getHusband();
			if (husband!=null){
				husband.addPersonalFamily(family);
			}
			Individual wife = family.getWife();
			if (wife !=null){
				wife.addPersonalFamily(family);
			}
		}
		//
		return result;
	}
	
	public static Net pGraphToNet (OMGraph pGraph) throws PuckException{
		Net result;
		
		result = new Net();
		
		//extract Individuals and PersonalFamilies
		int id = 1;
		for (OMNode node: pGraph.getNodes()){
			Family family = null;
			if (node.getFamily()!=null){
				family = node.getFamily().clone();
				if (node.getFamily().hasMarried()) {
					family.setMarried(true);
				}
				result.families().add(family);
				node.setReferent(family);
			}
			if (node.getHusband()!=null){
				Individual husband = node.getHusband().clone();
				husband.setId(id);
				node.setHusband(husband);
				result.individuals().add(husband);
				if (family!=null) {
					husband.addPersonalFamily(family);
				}
				id++;
			}
			if (node.getWife()!=null){
				Individual wife = node.getWife().clone();
				wife.setId(id);
				node.setWife(wife);
				result.individuals().add(wife);
				if (family!=null) {
					wife.addPersonalFamily(family);
				}
				id++;
			}
		}
		
		//extract OriginFamilies
		for (OMNode node: pGraph.getNodes()){
			
			Individual husband = node.getHusband();
			Individual wife =node.getWife();
			OMNode husbandLink = node.getMaleLink();
			OMNode wifeLink = node.getFemaleLink();
			
			if (husbandLink!=null) {
				Family husbandsParents = husbandLink.getFamily();
				husband.setOriginFamily(husbandsParents);
				husbandsParents.getChildren().add(husband);
			}
			if (wifeLink!=null) {
				Family wifesParents = wifeLink.getFamily();
				wife.setOriginFamily(wifesParents);
				wifesParents.getChildren().add(wife);
			}
		}
		
		//fuse same-sex Siblings
		for (Individual individual : result.individuals().toList()){
			int spouses = pGraph.getProbableNumberOfSpouses(individual.getGender(), monogamousSameSexSiblings(individual).size());
			while (spouses - individual.spouses().size() > 1 && monogamousSameSexSiblings(individual).size()>0) {
				Individual sibling = randomDraw(monogamousSameSexSiblings(individual));
				NetUtils.fuseIndividuals(result, sibling, individual);
			}
		}
		
		//
		return result;
	}
	
	public static OMGraph netToOreGraph (final Net net){
		OMGraph result;
		
		result = new OMGraph(GraphMode.OREGRAPH);
		
		// Put nodes
		for (Individual individual : net.individuals()){
			result.putNode(individual, GraphMode.OREGRAPH);
		}
		
		// Put links
		for (OMNode node : result.getNodes()){
			Individual individual = node.getIndividual();
			Individual father = individual.getFather();
			if (father != null){
				node.setMaleLink(result.getNode(father));
			}
			Individual mother = individual.getMother();
			if (mother != null){
				node.setFemaleLink(result.getNode(mother));
			}
			Family originFamily = individual.getOriginFamily();
			if (originFamily!=null){
				node.setMarriedParents(originFamily.hasMarried());
			}
		}

		// Put virtual individual nodes (for storing sterile marriages)
		int id = net.individuals().getLastId()+1;
		for (Family family : net.families()){
			if (family.getChildren().size()==0 && family.getHusband()!=null && family.getWife()!=null){
				OMNode node = new OMNode(new Individual(id));
				node.setVirtual(true);
				node.setMaleLink(result.getNode(family.getHusband()));
				node.setFemaleLink(result.getNode(family.getWife()));
				node.setMarriedParents(family.hasMarried());
				result.putNode(node);
				id++;
			}
		}
		
		// create links vector
		result.putLinks();

		// compute out degrees and ancestors
		result.computeOutDegrees();
		result.computeAncestors();
		
		//
		return result;
		
	}
	
	public static OMGraph netToPGraph (final Net net) {
		OMGraph result;
		
		result = new OMGraph(GraphMode.PGRAPH);

		result.setSiblingMarriageDistribution(net);

		// Put couple nodes
		for (Family family : net.families()) {
			result.putNode(family);  
		}
		
		// Put single individual nodes
		for (Individual individual : net.individuals()){
			if (individual.getPersonalFamilies().size()==0){
				result.putNode(individual, GraphMode.PGRAPH);
			}
		}
		
		// Put links 
		for (OMNode node : result.getNodes()){
			Family family = node.getFamily();
			if (family != null) {
				Family husbandsFamily = family.getHusbandsOriginFamily();
				if (husbandsFamily != null){
					node.setMaleLink(result.getNode(husbandsFamily));
				}
				Family wifesFamily = family.getWifesOriginFamily();
				if (wifesFamily != null){
					node.setFemaleLink(result.getNode(wifesFamily));
				}
			}
			
/*			Individual husband = node.getHusband();
			Individual wife = node.getWife();
			if (husband!=null && husband.getOriginFamily()!=null){
				OMNode husbandsParents = result.getNode(husband.getOriginFamily());
				node.setHusbandLink(husbandsParents);
//				totalLinks++;
			}
			if (wife!=null && wife.getOriginFamily()!=null){
				OMNode wifesParents = result.getNode(wife.getOriginFamily());
				node.setWifeLink(wifesParents);
//				totalLinks++;
			}*/
		}

		// create links vector
		result.putLinks();

		// compute out degrees and ancestors
		result.computeOutDegrees();
		result.computeAncestors();
		
		//
		return result;
		
	}
	

}
