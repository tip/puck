package org.tip.puck.net.relations.roles;

import java.util.Comparator;

public class RoleActorsComparator implements Comparator<RoleActors>{

	
	public int compare(RoleActors alpha, RoleActors beta){
		int result;
		
		result = alpha.size() - beta.size();
		
		if (result==0){
			for (int i=0;i<alpha.size();i++){
				result = alpha.get(i).compareTo(beta.get(i));
				if (result!=0){
					break;
				}
			}
		}
		//
		return result;
	}
}
