package oldcore.trash;


//Rename?
/**
 * This class defines vertex chains composed of several linear paths. It is used for representing kinship chains that imply consanguineal and affinal ties, in particular the path representations of matrimonial circuits
 * @author Klaus Hamberger
 * @since Version 0.5
 *
 */
@SuppressWarnings("serial")
public class OldRing {//implements Comparable<Ring>{


	/**
	 * the rate of decomposition of a circuit group whose model is the given circuit
	 */
//	private double decomp;
	/**
	 * the index of the first apical vertex in the circuit
	 */
//	private int firstApex=-1;
	//replace by type?
	/**
	 * the type of circuit 
	 * <p> corresponds to its order - 1: 0 consanguineous marriage, 1 redoubling, etc.
	 */
//	private int width;  //type
//	public int count;  //value
	/**
	 * the maximal canonic degree of the ring
	 */
	//private int depth;

	



	



	

	

	
	/**
	 * an auxiliary parameter function returning a headline parameter
	 * @param i the number of the parameter
	 * @return the required headline parameter
	 */
/*    private static String t(int i){
		   return ((String[])Puck.getParam("Headlines",14))[i];
	   }*/
		



	

	
	

	





	
/*	private Ring concatenate (Ring r, Ring s){
		Ring t = new Ring(r);
		if (!t.add(s,true)) return null;
		t.width = r.width+1;
		t.setPivots(r);
		return t;
	}*/
		   

	


	

	

	



	



	
	

	

	
	//for internal tests
/*	public String dirSignature (){
		String s = "";
		for (int i=0;i<size();i++){
			s=s+dir(i)+"";
		}
		return s;
	}*/
	


/*	public int dravidian (ArrayList<Integer> p, int i){
		return getRing().dravidian(i);
	}*/
	






	



	

		

	
	/**
	 * gets the first ring member in apical position
	 * @return the first ring member in apical position
	 * @see Ring#linear()
	 */
/*	private int getApex() {
		return getNextApex(0);
	}*/




	

	
	//see clone()
	/**
	 * gets a copy of the ring
	 * @return a copy of the ring 
	 */
/*	public OldRing getCopy(){   
		return new OldRing(this);
	}*/
	

   
	/**
	 * gets the decomposition rate of the group whose model is the given circuit
	 * @return the decomposition rate of the group whose model is the given circuit
	 */
/*	public double getDecomp() {
	
		return decomp;
	}*/
	
	/**
	 * gets the array of linear subchain lengths
	 * @return the array of linear subchain lengths
	 */
/*	public int[] getDepths(){
		return depths;
	}*/






	

   
/*	public Path getPivots (){
		Path p = new Path();
		for (int i=0;i<2*dim();i++) {
			p.add(getPivot(i));
		}
		return p;
	}*/
   

   

   

	

	

	
	/**
	 * checks whether ego or alter has two spouses within the ring
	 * @return true if ego or alter has two spouses within the ring
	 */
/*	private boolean hasPolygamPoles (){
		if (width==0) return false;
		if (get(0)==get(1) || getLast()==get(size()-2)) return true;
		return false;
	}*/
   

	
	/**
	 * gets a string representation of the sexual mode of the ring
	 * <p> value of the "HETERO" property
	 * @return a string representation of the sexual mode of the ring
	 */
/*	private String hetero () {
		if (isHetero()) return t(22);
		return t(23);
	}*/



  

	

	



	

	

	

	

	

	


	/**
	 * gets the position index of the male pivot closest to ego, or -1 if there is no male pivot 
	 * @return the position index of the closest male pivot, or -1 if there is none
	 * @see Ring#max(boolean)
	 */
/*	private int malePosition (){
		int n = gender(0)-gender(length());
//		int n = gender(0)-gender(size()-1);
		if (n==-1) return 0;
		if (n==1) return vector.length-1;
		return -1;
	}*/

	

	
	//Check, simplify and revise with more transparency for the diverse census alternatives
	/**
	 * get the standard ego position
	 * @param heteroOnly true if the chain is heterosexual
	 * @return the standard ego position
	 * @see Ring#standard(boolean)
	 */
/*	private int max (){
/*		if (!allPositions) {
			int n = malePosition();
			if (n>-1) return n;
			else return 0;
		}*/
/*		int[] v = vector;
		int j = 0;
		KeyComparator c = new KeyComparator(1);
//		VectorComparator<int[]> c = new VectorComparator<int[]>();
		int n = vector.length;
		for (int i=0;i<n;i++){
//			if (!allPositions && i<n-1) continue;
			int[] w = transformVector(i);
			if (c.compare(v,w)==1) {
				v = w;
				j = i;
			}
		}
		return j;
	}*/
	

	

	
/*	int nxt (int i) { 
		   int n = 2*(width+1);
		   return (i-1+n+2*(i%2))%n;
	   }*/
	

	
	/**
	 * sets the first apex
	 */
/*	private void setFirstApex(){
		if (length()==0) return;
		if (dir(1)==-1) return;
		for (int i=1;i<size();i++){
			if (dir(i)==0) return;
			if (dir(i)==-1) {
				addApex(i-1);
				return;
			}
		}
	}*/


	


	/**
	 * gets a ringclass with this ring as a model  
	 * @return a ringclass with this ring as a model
	 * @see maps.groupmaps.RingGroupMap#openRingClass(int[], OldRing)
	 */
/*	public RingGroup newGroup () {
		return newGroup (0);
	}*/
	
	/**
	 * gets a ringclass with this ring as a model and a given class number 
	 * @param i the class number
	 * @return a ringclass with this ring as a model
	 * @see maps.groupmaps.RingGroupMap#countRingTypes()
	 */
/*	public RingGroup newGroup (int i) {
		return new RingGroup (this,i);
	}*/
	

	

	

	

	

	


	


	


	
	
/*	private void save (ArrayList<Ring> list){
		list.add(getCopy());
	}*/


	
	/**
	 * sets the class number of the ring
	 * @param i the class number
	 */
/*	public void setClassNr (int i) {
	      number = i;
	   }*/
	
	
	/**
	 * sets the decomposition rate of the group whose model is the given circuit
	 * @param decomp the decomposition rate of the group whose model is the given circuit
	 */
/*	public void setDecomp(double decomp) {
		this.decomp = decomp;
	}*/

	//Test replacement by 10 and 11
/*	public String signature (boolean withEgo) { //Test
		if (withEgo) return signature(2)+"\t"+signature(1);
		return signature(8)+"\t"+signature(1);//+"\t"+signature(9);
	}*/
   

   
	//Test replacement by 12
/*	public String signature1 (){
		return signature(1)+"\t"+signature(0);
	}*/
   


	

	

	

	
	//really necessary for sym=2??
	/**
	 * returns a representation of the circuit with male Ego if this representation exists, the initial representation otherwise
	 * @param sym the symmetry index
	 * <p> 0 not permutable, 1 ego-alter reflection possible, 2 fully permutable
	 * @return the male ego representation of the circuit
	 */
/*	private Ring maleEgo (int sym){
		if (get(0).getGender()==0 || getLast().getGender()==0) return this;
		if (sym==1 && hasPolygamPoles()) return this;
		return transform(2*width+1);
	}*/
	
	
	/**
	 * returns a standard representation of the circuit 
	 * @param crossSex true if the ring is heterosexual
	 * @return the standard representation of the ring
	 */
/*	public Ring standard (boolean crossSex) {
		setVector();
//		return transform(max(matrim));
//		if (matrim) return transform (max(matrim));
//		int k = 0;
//		if (dim()>1) k=1;
//		return transform(k).transform(max(matrim));
		Ring t = transform(max());
		if (crossSex && t.get(0).getGender()==1 && t.getLast().getGender()==0) return t.transform(1);
		return t;
	}*/
	

	

	

	



	
	
	
/*	private boolean isLinked (int i, int j){
		return (get(i).isUniLinked(get(j)) || get(j).isUniLinked(get(i)));
	}*/
	

	
/*	public boolean isRoundabout (int sib) {
		      for (int i=0;i<size();i++) {
		         for (int j=0;j<size();j++) {
		            if (adjacent(i,j)) continue;
		            if ((get(j).getID()!=0) && (get(i).equals(get(j)))) return true;
		        	if (sib==-1) continue;
		            if (get(j).getID()!=0) {
		            	if (get(i).isUnilaterallyLinked(get(j))) return true;
		            } else if (sib!=1){
		            	for (int k=0;k<2;k++) {
		                	if (get(i).isUnilaterallyLinked(get(j).apiCouple[k])) return true;
		            	}
		            } 
		            if (get(j).getID()==0) continue;
		            if ((sib!=1) || (adjacent(i,j,2))) continue;
		          	if (get(i).isSibling(get(j))) return true;
		         }
		      }
		      return false;
		   }*/
	

	


	/**
	 * gets the ith vector component
	 * @return the ith vector component
	 * @see OldRing#transformVector(int)
	 */
/*	private int v (int i){
		return vector[i];
	}*/
	

	




	
	/**
	@author: Andrzej Kabat
	*/
/*	public void dispose(){
		dir.clear();
		dir = null;
		if (pivots != null){
			pivots.clear();
			pivots = null;
		}
		if (apices != null){
			apices.clear();
			apices = null;
		}
		super.dispose();
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getWidth() {
		return width;
	}*/
}
	   