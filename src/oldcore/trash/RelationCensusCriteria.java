package oldcore.trash;

import java.util.List;

import org.tip.puck.sequences.workers.SequenceCriteria.ValueSequenceLabel;

public class RelationCensusCriteria {
	
	Integer[] times;
	String keyLabel;
	List<ValueSequenceLabel>  mainEventTypes; 
	List<String> labels;
	
	public RelationCensusCriteria(){
		
	}

	public Integer[] getTimes() {
		return times;
	}

	public void setTimes(Integer[] times) {
		this.times = times;
	}

	public String getKeyLabel() {
		return keyLabel;
	}

	public void setKeyLabel(String keyLabel) {
		this.keyLabel = keyLabel;
	}

	public List<ValueSequenceLabel> getMainEventTypes() {
		return mainEventTypes;
	}

	public void setMainEventTypes(List<ValueSequenceLabel> mainEventTypes) {
		this.mainEventTypes = mainEventTypes;
	}

	public List<String> getLabels() {
		return labels;
	}

	public void setLabels(List<String> labels) {
		this.labels = labels;
	}
	
	
	
	

}
