package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Net;
import org.tip.puck.net.random.RandomNetMaker;
import org.tip.puck.net.workers.NetReporter;
import org.tip.puck.report.Report;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.PuckGUI;
import org.tip.puckgui.views.mas.NormalAgeDifferenceWeightFactor;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class RandomCorpusInputWindow extends JFrame {

	private static final long serialVersionUID = -8387535543195985632L;
	private static final Logger logger = LoggerFactory.getLogger(RandomCorpusInputWindow.class);

	private JFrame thisJFrame;
	private JPanel contentPane;
	private JSpinner spnrInitialPopulation;
	private JSpinner spnrFertilityRate;
	private JSpinner spnrMaxAge;
	private JSpinner spnrYear;
	private JSpinner spnrMarriage;
	private JCheckBox chckbxMarriage;
	private JCheckBox chckbxDivorce;
	private JCheckBox chckbxPolygamyMen;
	private JCheckBox chckbxPolygamyWomen;
	private JCheckBox chckbxNormalAgeDifference;
	private JCheckBox chckbxCousins;
	private JPanel panelDivorce;
	private JPanel panelDivorce2;
	private JPanel panelNormalAgeDifference;
	private JPanel panelCousins;
	private JSpinner spnrNormalAgeDifferenceStdev;
	private JSpinner spnrNormalAgeDifferenceMean;
	private JSpinner spnrDivorce;
	private JSpinner spnrPolygamyMen;
	private JSpinner spnrPolygamyWomen;
	private JSpinner spnrCousinsWeight;
	private JSpinner spnrMinAge;
	private JComboBox cmbxCousinsType;

	/**
	 * Similar with VirtualFielworkInputWindow.
	 */
	public RandomCorpusInputWindow(final NetGUI gui) {

		// /////////////////////////////////
		setIconImage(Toolkit.getDefaultToolkit().getImage(RandomCorpusInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		setTitle("Random Corpus");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 676, 468);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});

		JButton btnRestoreDefaults = new JButton("Restore defaults");
		btnRestoreDefaults.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Restore Defaults.
				setDefaultCriteria();
			}
		});
		buttonPanel.add(btnRestoreDefaults);
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		getRootPane().setDefaultButton(btnLaunch);
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					//
					RandomCorpusCriteria criteria = getCriteria();
					criteria.setMas(false);

					//
					PuckGUI.instance().getPreferences().setRandomCorpusCriteria(criteria);
					//
					Net targetNet = new RandomNetMaker(criteria).createRandomNet();
					targetNet.setLabel("randomNet");

					// Build report.
					Report report = NetReporter.reportRandomCorpus(criteria, targetNet);

					//
					NetGUI newGUI = PuckGUI.instance().createNetGUI(new File(targetNet.getLabel()), targetNet);
					newGUI.addReportTab(report);

					//
					dispose();

				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(RandomCorpusInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Simulation parameters", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.contentPane.add(panel, BorderLayout.NORTH);
		panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("100dlu"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, ColumnSpec.decode("150dlu"), }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));

		JLabel lblYear = new JLabel("Year:");
		lblYear.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblYear, "2, 2, right, default");

		this.spnrYear = new JSpinner();
		this.spnrYear.setModel(new SpinnerNumberModel(new Integer(300), new Integer(1), null, new Integer(1)));
		panel.add(this.spnrYear, "4, 2");

		JLabel lblInitialPopulation = new JLabel("Initial population:");
		panel.add(lblInitialPopulation, "2, 4, right, default");

		this.spnrInitialPopulation = new JSpinner();
		this.spnrInitialPopulation.setModel(new SpinnerNumberModel(new Integer(100), new Integer(1), null, new Integer(1)));
		panel.add(this.spnrInitialPopulation, "4, 4");

		this.spnrFertilityRate = new JSpinner();
		this.spnrFertilityRate.setModel(new SpinnerNumberModel(new Double(2.), new Double(0.0), new Double(100.0), new Double(1.)));
		((JSpinner.NumberEditor) this.spnrFertilityRate.getEditor()).getFormat().setMinimumFractionDigits(5);

		JLabel lblFertilityRate = new JLabel("Fertility rate:");
		panel.add(lblFertilityRate, "2, 6, right, default");
		panel.add(this.spnrFertilityRate, "4, 6");

		JLabel lblMaxAge = new JLabel("Max. age:");
		panel.add(lblMaxAge, "2, 8, right, default");

		this.spnrMaxAge = new JSpinner();
		this.spnrMaxAge.setModel(new SpinnerNumberModel(new Integer(70), new Integer(1), null, new Integer(1)));
		panel.add(this.spnrMaxAge, "4, 8");

		JLabel lblMinage = new JLabel("Min.age:");
		panel.add(lblMinage, "2, 10, right, default");

		this.spnrMinAge = new JSpinner();
		panel.add(this.spnrMinAge, "4, 10");

		JPanel panel_3 = new JPanel();
		this.contentPane.add(panel_3, BorderLayout.CENTER);
		panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.Y_AXIS));

		JPanel panel_1 = new JPanel();
		panel_3.add(panel_1);

		JPanel panelWeightFactors = new JPanel();
		panel_3.add(panelWeightFactors);
		panelWeightFactors.setBorder(new TitledBorder(null, "Weight factors", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelWeightFactors.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), },
				new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));

		JLabel lblDivorce = new JLabel("Marriage");
		panelWeightFactors.add(lblDivorce, "2, 2");

		this.chckbxMarriage = new JCheckBox("");
		this.chckbxMarriage.setSelected(true);
		this.chckbxMarriage.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				//
				RandomCorpusInputWindow.this.panelDivorce.setVisible(RandomCorpusInputWindow.this.chckbxMarriage.isSelected());
			}
		});
		panelWeightFactors.add(this.chckbxMarriage, "4, 2");

		this.panelDivorce = new JPanel();
		this.panelDivorce.setEnabled(false);
		panelWeightFactors.add(this.panelDivorce, "6, 2, fill, fill");
		this.panelDivorce.setLayout(new FormLayout(
				new ColumnSpec[] { FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("50dlu"), },
				new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, }));

		JLabel label = new JLabel("Probability:");
		this.panelDivorce.add(label, "1, 1");

		this.spnrMarriage = new JSpinner();
		this.spnrMarriage.setModel(new SpinnerNumberModel(new Double(0.01), new Double(0.0), new Double(1.0), new Double(0.01)));
		((JSpinner.NumberEditor) this.spnrMarriage.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelDivorce.add(this.spnrMarriage, "3, 1");

		JLabel lblDivorce_1 = new JLabel("Divorce");
		panelWeightFactors.add(lblDivorce_1, "2, 4");

		this.chckbxDivorce = new JCheckBox("");
		this.chckbxDivorce.setSelected(true);
		this.chckbxDivorce.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				//
				RandomCorpusInputWindow.this.panelDivorce2.setVisible(RandomCorpusInputWindow.this.chckbxDivorce.isSelected());
			}
		});
		panelWeightFactors.add(this.chckbxDivorce, "4, 4");

		this.panelDivorce2 = new JPanel();
		panelWeightFactors.add(this.panelDivorce2, "6, 4, fill, fill");
		this.panelDivorce2.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu"), }, new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblProbability = new JLabel("Probability:");
		this.panelDivorce2.add(lblProbability, "1, 1");

		this.spnrDivorce = new JSpinner();
		this.spnrDivorce.setModel(new SpinnerNumberModel(new Double(0.01), new Double(0.0), new Double(1.0), new Double(0.01)));
		((JSpinner.NumberEditor) this.spnrDivorce.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelDivorce2.add(this.spnrDivorce, "3, 1");

		JLabel lblPolygyny = new JLabel("Polygyny");
		panelWeightFactors.add(lblPolygyny, "2, 6");

		this.chckbxPolygamyMen = new JCheckBox("");
		panelWeightFactors.add(this.chckbxPolygamyMen, "4, 6");

		this.spnrPolygamyMen = new JSpinner();
		this.spnrPolygamyMen.setModel(new SpinnerNumberModel(new Double(0.0), new Double(0.0), new Double(1.0), new Double(0.01)));
		((JSpinner.NumberEditor) this.spnrMarriage.getEditor()).getFormat().setMinimumFractionDigits(3);
		panelWeightFactors.add(this.spnrPolygamyMen, "6, 6");

		JLabel lblPolyandry = new JLabel("Polyandry");
		panelWeightFactors.add(lblPolyandry, "2, 8");

		this.chckbxPolygamyWomen = new JCheckBox("");
		panelWeightFactors.add(this.chckbxPolygamyWomen, "4, 8");

		this.spnrPolygamyWomen = new JSpinner();
		this.spnrPolygamyWomen.setModel(new SpinnerNumberModel(new Double(0.0), new Double(0.0), new Double(1.0), new Double(0.01)));
		((JSpinner.NumberEditor) this.spnrMarriage.getEditor()).getFormat().setMinimumFractionDigits(3);
		panelWeightFactors.add(this.spnrPolygamyWomen, "6, 8");

		JLabel lblNormalAgeDifference = new JLabel("Normal age difference");
		panelWeightFactors.add(lblNormalAgeDifference, "2, 10");

		this.chckbxNormalAgeDifference = new JCheckBox("");
		this.chckbxNormalAgeDifference.setSelected(true);
		this.chckbxNormalAgeDifference.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				//
				RandomCorpusInputWindow.this.panelNormalAgeDifference.setVisible(RandomCorpusInputWindow.this.chckbxNormalAgeDifference.isSelected());
			}
		});
		panelWeightFactors.add(this.chckbxNormalAgeDifference, "4, 10");

		this.panelNormalAgeDifference = new JPanel();
		panelWeightFactors.add(this.panelNormalAgeDifference, "6, 10, fill, fill");
		this.panelNormalAgeDifference.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu"), }, new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, }));

		JLabel label_10 = new JLabel("Mean:");
		this.panelNormalAgeDifference.add(label_10, "1, 1");

		this.spnrNormalAgeDifferenceMean = new JSpinner();
		this.spnrNormalAgeDifferenceMean.setModel(new SpinnerNumberModel(new Double(0.0), new Double(0.0), null, new Double(1)));
		((JSpinner.NumberEditor) this.spnrNormalAgeDifferenceMean.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelNormalAgeDifference.add(this.spnrNormalAgeDifferenceMean, "3, 1");

		JLabel label_11 = new JLabel("Stdev:");
		this.panelNormalAgeDifference.add(label_11, "5, 1");

		this.spnrNormalAgeDifferenceStdev = new JSpinner();
		this.spnrNormalAgeDifferenceStdev.setModel(new SpinnerNumberModel(new Double(5), new Double(0.0), null, new Double(1)));
		((JSpinner.NumberEditor) this.spnrNormalAgeDifferenceStdev.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelNormalAgeDifference.add(this.spnrNormalAgeDifferenceStdev, "7, 1");

		JLabel lblCousins = new JLabel("First Cousins");
		panelWeightFactors.add(lblCousins, "2, 12");

		this.chckbxCousins = new JCheckBox("");
		this.chckbxCousins.setSelected(true);
		this.chckbxCousins.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				//
				RandomCorpusInputWindow.this.panelCousins.setVisible(RandomCorpusInputWindow.this.chckbxCousins.isSelected());
			}
		});
		panelWeightFactors.add(this.chckbxCousins, "4, 12");

		this.panelCousins = new JPanel();
		panelWeightFactors.add(this.panelCousins, "6, 12, fill, fill");
		this.panelCousins.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("50dlu"),
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("50dlu:grow"),
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("50dlu"), },
				new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblFirst = new JLabel("Weight:");
		this.panelCousins.add(lblFirst, "1, 1");

		this.spnrCousinsWeight = new JSpinner();
		this.spnrCousinsWeight.setModel(new SpinnerNumberModel(new Double(1), new Double(0.0), null, new Double(1)));
		((JSpinner.NumberEditor) this.spnrCousinsWeight.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelCousins.add(this.spnrCousinsWeight, "3, 1");

		JLabel lblBravo = new JLabel("Type:");
		this.panelCousins.add(lblBravo, "5, 1, right, default");

		this.cmbxCousinsType = new JComboBox();
		this.cmbxCousinsType.setModel(new DefaultComboBoxModel(FiliationType.values()));
		this.cmbxCousinsType.setToolTipText("Type");
		this.panelCousins.add(this.cmbxCousinsType, "7, 1, fill, default");

		// //////////////////
		setDefaultCriteria(); // Useful to set default weight factors values.
		setCriteria(PuckGUI.instance().getPreferences().getRandomCorpusMASCriteria());
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public RandomCorpusCriteria getCriteria() throws PuckException {
		RandomCorpusCriteria result;

		//
		result = new RandomCorpusCriteria();

		//
		result.setYear((Integer) this.spnrYear.getValue());
		result.setInitialPopulation((Integer) this.spnrInitialPopulation.getValue());
		result.setFertilityRate((Double) this.spnrFertilityRate.getValue());
		result.setMaxAge((Integer) this.spnrMaxAge.getValue());
		result.setMinAge((Integer) this.spnrMinAge.getValue());

		if (this.chckbxMarriage.isSelected()) {
			result.setMarriageRate(((Double) this.spnrMarriage.getValue()));
		}

		if (this.chckbxDivorce.isSelected()) {
			result.setDivorceRate(((Double) this.spnrDivorce.getValue()));
		}

		if (this.chckbxPolygamyMen.isSelected()) {
			result.setPolygamyRateMen(((Double) this.spnrPolygamyMen.getValue()));
		}

		if (this.chckbxPolygamyWomen.isSelected()) {
			result.setPolygamyRateWomen(((Double) this.spnrPolygamyWomen.getValue()));
		}

		if (this.chckbxNormalAgeDifference.isSelected()) {
			NormalAgeDifferenceWeightFactor factor = new NormalAgeDifferenceWeightFactor();
			factor.setMean((Double) this.spnrNormalAgeDifferenceMean.getValue());
			factor.setStdev((Double) this.spnrNormalAgeDifferenceStdev.getValue());
			result.weightFactors().add(factor);
		}

		if (this.chckbxCousins.isSelected()) {
			result.setCousinPreferenceType((FiliationType) this.cmbxCousinsType.getSelectedItem());
			result.setCousinPreferenceWeight(((Double) this.spnrCousinsWeight.getValue()));
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public void setCriteria(final RandomCorpusCriteria source) {
		//
		if (source == null) {
			setDefaultCriteria();
		} else {
			//
			this.spnrYear.setValue(source.getYear());
			this.spnrInitialPopulation.setValue(source.getInitialPopulation());
			this.spnrFertilityRate.setValue(source.getFertilityRate());
			this.spnrMaxAge.setValue(source.getMaxAge());
			this.spnrMinAge.setValue(source.getMinAge());
			this.spnrMarriage.setValue(((source.getMarriageRate())));
			this.spnrDivorce.setValue(((source.getDivorceRate())));
			this.spnrNormalAgeDifferenceStdev.setValue(source.getStdevAgeDifference());
			this.spnrNormalAgeDifferenceMean.setValue(source.getMeanAgeDifference());
			this.spnrCousinsWeight.setValue(source.getCousinPreferenceWeight());
			this.cmbxCousinsType.setSelectedItem(source.cousinPreferenceType);

			this.chckbxDivorce.setSelected(true);
			this.chckbxNormalAgeDifference.setSelected(true);
			this.chckbxCousins.setSelected(true);
			this.chckbxMarriage.setSelected(true);

		}
	}

	/**
	 * 
	 */
	public void setDefaultCriteria() {
		RandomCorpusCriteria defaultCriteria = new RandomCorpusCriteria();
		setCriteria(defaultCriteria);

	}
}
