package org.tip.puckgui.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JMenuItem;

import org.tip.puck.PuckManager;
import org.tip.puckgui.NetGUI;


/**
 * 
 * @author TIP
 */
public class OpenRecentFolderMenuItem extends JMenuItem {
	private static final long serialVersionUID = -2068397343657608721L;
	private static final int FOLDER_DISPLAY_LENGTH = 40;

	/**
	 * 
	 * @param title
	 */
	public OpenRecentFolderMenuItem(final JFrame window, final NetGUI netGUI, final File folder) {
		super();
		setText(endString(folder.getAbsolutePath(), FOLDER_DISPLAY_LENGTH));
		if (!folder.exists()) {
			setEnabled(false);
		}

		this.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				//
				File file = MainWindow.selectFile(window, folder);

				//
				MainWindow.openSelectedFile(netGUI, window, file, PuckManager.DEFAULT_CHARSET_NAME);
			}
		});
	}

	/**
	 * This method replace the beginning of a string if this is too long.
	 * 
	 * @param source
	 * @param length
	 * @return
	 */
	public String endString(final String source, final int length) {
		String result;

		if (source.length() < length + 3) {
			result = source;
		} else {
			result = "…" + source.substring(source.length() - length);
		}

		//
		return result;
	}
}
