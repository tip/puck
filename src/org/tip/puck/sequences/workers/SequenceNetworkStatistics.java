package org.tip.puck.sequences.workers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.tip.puck.PuckException;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.GraphProfile;
import org.tip.puck.graphs.Node;
import org.tip.puck.graphs.workers.GraphUtils;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individualizable;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Partition;
import org.tip.puck.report.ReportChart;
import org.tip.puck.report.ReportChart.GraphType;
import org.tip.puck.report.ReportTable;
import org.tip.puck.sequences.Sequenceable;
import org.tip.puck.statistics.StatisticsReporter;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.Value;

/**
 * 
 * @author Klaus Hamberger
 *
 */
public class SequenceNetworkStatistics<S extends Sequenceable<E>,E> {

	String type;
	String eventTypeName;
	Map<S,List<String>> singles;
	Map<S,List<String[]>> pairs;
	Map<Value,Double[]> singleProbas;
	Map<Value,Double[]> pairProbas;
	Map<Value,Double[]> confidence;
	Map<Value,Double[]> lift;
	Partition<String>[] partitions;
	Graph<Cluster<String>>[] sequenceNetworks;
	Partition<Node<Cluster<String>>>[] depthPartitions;
	
	GraphProfile<Cluster<String>>[] sequenceNetworkProfiles;
	
	List<String> names;
	int maxDepth;
	String[] genderStrings;
	boolean ungendered;
	
	
	private int gender(S sequence){
		int gender = 0;
		
		if (sequence instanceof Individualizable){
			gender = ((Individualizable)sequence).getEgo().getGender().toInt();
		} else {
			ungendered = true;
		}
		//
		return gender;
	}
	
	public SequenceNetworkStatistics (String type, String eventTypeName, Map<S,List<String>> singles, Map<S,List<String[]>> pairs){
		
		this.type = type;
		this.eventTypeName = eventTypeName;
		this.singles = singles;
		this.pairs = pairs;
		
		singleProbas = new TreeMap<Value,Double[]>();
		pairProbas = new TreeMap<Value,Double[]>();
		maxDepth = 0;
		
		ungendered = false;
		
		Double[] sums = new Double[]{0.,0.,0.};

		
		partitions = (Partition<String>[])new Partition[3];
		for (int i=0;i<partitions.length;i++){
			partitions[i] = new Partition<String>();
		}
		
		Set<String> nameSet = new HashSet<String>();
		
		for (S sequence : singles.keySet()){
			
			int gender = gender(sequence);

			
			/*			if (gender<2){
				sums[gender] += 1;
			}
			sums[2] += 1;*/

			List<String> singleList = singles.get(sequence);
			if (singleList.size()>maxDepth){
				maxDepth = singleList.size();
			}
			
			int i=1;
			for (String single : singleList){
				nameSet.add(single);
				Value singleValue = new Value(single);
				Double[] singleProba = singleProbas.get(singleValue);
				if (singleProba == null){
					singleProba = new Double[]{0.,0.,0.};
					singleProbas.put(singleValue, singleProba);
				} 
				if (gender<2){
					singleProba[gender] += 1;
					sums[gender] +=1;
					partitions[gender].put(sequence.getId()+"-"+i, singleValue);
				}
				singleProba[2] += 1;
				sums[2] +=1;
				partitions[2].put(sequence.getId()+"-"+i, singleValue);
				i++;
			}
			
			List<String[]> pairList = pairs.get(sequence);
			for (String[] pair : pairList){
				Value pairValue = new Value(pair);
				Double[] pairProba = pairProbas.get(pairValue);
				if (pairProba == null){
					pairProba = new Double[]{0.,0.,0.};
					pairProbas.put(pairValue, pairProba);
				} 
				if (gender<2){
					pairProba[gender] += 1;
				}
				pairProba[2] += 1;
			}
		}
		
		for (Double[] singleProba : singleProbas.values()){
			for (int i=0;i<singleProba.length;i++){
				singleProba[i] = MathUtils.percent(singleProba[i], sums[i]);
			}
		}
		
		for (Double[] pairProba : pairProbas.values()){
			for (int i=0;i<pairProba.length;i++){
				pairProba[i] = MathUtils.percent(pairProba[i], sums[i]);
			}
		}
		
		names = new ArrayList<String>(nameSet);
		Collections.sort(names);
	}
	
	public Graph<Cluster<String>>[] getSequenceNetworks(){
		
		if (sequenceNetworks == null){

			sequenceNetworks = (Graph<Cluster<String>>[])new Graph[3];
			sequenceNetworkProfiles = (GraphProfile<Cluster<String>>[])new GraphProfile[3];
			
			for (Gender gender : Gender.values()){
				sequenceNetworks[gender.toInt()] = getSequenceNetwork(type,gender);
				sequenceNetworkProfiles[gender.toInt()] = new GraphProfile<Cluster<String>>(sequenceNetworks[gender.toInt()],null);
			}
		}
		
		//
		return sequenceNetworks;
	}
	
	public Partition<Node<Cluster<String>>>[] getDepthPartitions(){
		
		if (depthPartitions == null){
			
			if (sequenceNetworks == null){
				getSequenceNetworks();
			}

			depthPartitions = (Partition<Node<Cluster<String>>>[])new Partition[3];
			for (Gender gender : Gender.values()){
				depthPartitions[gender.toInt()] = GraphUtils.getDepthPartition(sequenceNetworks[gender.toInt()]);
			}
		}
		
		//
		return depthPartitions;
	}
	
	
	
	
	public Graph<Cluster<String>> getSequenceNetwork (String type, Gender gender){
		Graph<Cluster<String>> result;
			
		String genderName = "ALL";
		if (gender != Gender.UNKNOWN){
			genderName = gender.toString();
		}
		
		result = new Graph<Cluster<String>>(type+"_"+eventTypeName+"_"+genderName);
		
		Partition<String> partition = partitions[gender.toInt()];
		
		//
		for (String name : names){
			Cluster<String> cluster = partition.getCluster(new Value(name));
			if (cluster!=null){
				result.addNode(cluster);
			} else if (type.equals("Aggregate Sequence Network")){
				result.addNode(new Cluster<String>(new Value(name)));
			}
			
		}
		
		//
		for (S sequence : pairs.keySet()){
			if (gender.isUnknown() || ((!gender.isUnknown() && gender.toInt() == gender(sequence)))){
				for (String[] pair : pairs.get(sequence)){
					Cluster<String> previous = partition.getCluster(new Value(pair[0]));
					Cluster<String> next = partition.getCluster(new Value(pair[1]));
					result.incArcWeight(previous, next);
				}
			}
		}
		
//		Set<String> values = new HashSet<String>();
		
		for (Node<Cluster<String>> node : result.getNodes()){
			Cluster<String> referent = node.getReferent();
			if (referent !=null){
				Value clusterValue = referent.getValue();
				if (clusterValue!=null){
					String value = clusterValue.toString();
					if (value.lastIndexOf("-")>-1){
						value = value.substring(value.lastIndexOf("-")+1);
					}
					node.setAttribute(eventTypeName, value);
				}
			}
		}
		
		//
		return result;
	}
	
	private Double getConfidence (String[] pairLabel, int gender){
		Double result;
		
		result = MathUtils.percent(pairProbas.get(new Value(pairLabel))[gender], singleProbas.get(new Value(pairLabel[0]))[gender]);
		
		//
		return result;
	}
	
	private Double getLift (String[] pairLabel, int gender){
		Double result;
		
		result = MathUtils.percent(pairProbas.get(new Value(pairLabel))[gender], singleProbas.get(new Value(pairLabel[0]))[gender] * singleProbas.get(new Value(pairLabel[1]))[gender]);
		
		//
		return result;
	}
	
	private Double getPairProba (String[] pairLabel, int gender){
		Double result;
		
		if (pairLabel == null){
			result = null;
		} else {
			result = pairProbas.get(new Value(pairLabel))[gender];
		}
		
		
		//
		return result;
	}
	
	private Double getSingleProba (String singleLabel, int gender){
		Double result;
		
		if (singleLabel == null){
			result = null;
		} else {
			result = singleProbas.get(new Value(singleLabel))[gender];
		}
		
		
		//
		return result;
	}
	
	
	private ReportChart getSingleProbaChart() throws PuckException{
		ReportChart result;
		
		result = StatisticsReporter.createMapChart(singleProbas, type+" "+eventTypeName+" (Single Frequencies)", genderStrings, GraphType.STACKED_BARS); 
		
		//
		return result;
	}
	
	private ReportChart getPairProbaChart() throws PuckException{
		ReportChart result;
		
		result = StatisticsReporter.createMapChart(pairProbas, type+" "+eventTypeName+" (Connected Frequencies)", genderStrings, GraphType.STACKED_BARS); 
		
		//
		return result;
	}
	
	private ReportChart getConfidenceChart() throws PuckException{
		ReportChart result;
		
		result = StatisticsReporter.createMapChart(pairProbas, type+" "+eventTypeName+" (Confidence)", genderStrings, GraphType.STACKED_BARS); 
		
		//
		return result;
	}
	
	private ReportChart getLiftChart() throws PuckException{
		ReportChart result;
		
		result = StatisticsReporter.createMapChart(pairProbas, type+" "+eventTypeName+" (Lift)", genderStrings, GraphType.STACKED_BARS); 
		
		//
		return result;
	}
	
	public ReportChart getRamificationChart() throws PuckException{
		ReportChart result;
		
		result = StatisticsReporter.<String>createRamificationChart(depthPartitions, totalSums(), eventTypeName); 
		
		//
		return result;
	}
	
	
	public List<ReportChart> getCharts() throws PuckException {
		List<ReportChart> result;
		
		result = new ArrayList<ReportChart>();
		
		if (ungendered) {
			genderStrings = new String[]{"","","ALL"};
		} else  {
			genderStrings = new String[]{"MALE","FEMALE"};
		}
		
		result.add(getSingleProbaChart());
		result.add(getPairProbaChart());
		result.add(getConfidenceChart());
		result.add(getLiftChart());
		
		if (depthPartitions!=null){
			result.add(getRamificationChart());
		}
		
		//
		return result;
	}
	
	public ReportTable getTable (String title){
		ReportTable result;
		
		int rowCount = pairProbas.size()+1;
		int columnCount = 3*6;

		//
		result = new ReportTable(rowCount,columnCount);
		
		//
		result.setTitle(title);
		
		String[] headers = new String[]{"Pair Proba","First Proba","Second Proba","Confidence","Lift"};
		String[] genders = new String[]{"Male","Female","All"};
		
		int columnIndex = 1;
		for (String gender : genders){
			for (String header : headers){
				result.set(0,columnIndex,header+" ("+gender+")");
				columnIndex++;
			}
		}
		
		int rowIndex = 1;
		for (Value value : pairProbas.keySet()){
			
			String[] pairLabel = value.stringArrayValue();

			columnIndex = 0;
			result.set(rowIndex, columnIndex, value);
			columnIndex++;
			for (int i=0;i<3;i++){
				result.set(rowIndex, columnIndex, getPairProba(pairLabel,i));
				columnIndex++;
				result.set(rowIndex, columnIndex, getSingleProba(pairLabel[0],i));
				columnIndex++;
				result.set(rowIndex, columnIndex, getSingleProba(pairLabel[1],i));
				columnIndex++;
				result.set(rowIndex, columnIndex, getConfidence(pairLabel,i));
				columnIndex++;
				result.set(rowIndex, columnIndex, getLift(pairLabel,i));
				columnIndex++;
			}
			
			rowIndex++;
		}

		//
		return result;
		
	}
	
	private int[][] totalSums(){
		int[][] result;
		
		result = new int[maxDepth][3];
		
		for (S sequence : singles.keySet()){
			List<String> list = singles.get(sequence);
			for (int i=0;i<list.size();i++){
				result[i][gender(sequence)]++;
				result[i][2]++;
			}
		}
		//
		return result;
	}
	
	
/*	private double getDensity(Gender gender){
		double result;
		
		result = GraphUtils.density(getSequenceNetworks()[gender.toInt()]);
		
		//
		return result;
	}*/
	
	public GraphProfile<Cluster<String>> getProfile(Gender gender){
		return sequenceNetworkProfiles[gender.toInt()];
	}
	
	


	
	
	
	
	

	
	
}
