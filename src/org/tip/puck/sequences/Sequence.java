package org.tip.puck.sequences;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.tip.puck.PuckException;
import org.tip.puck.net.Attributable;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Populatable;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.util.Numberable;
import org.tip.puck.util.NumberablesHashMap;
import org.tip.puck.util.Value;

public class Sequence<E> implements Comparable<Sequence<E>>, Numberable, Sequenceable<E>, Populatable {
	
	int id;
	Map<Ordinal,E> stations;
	String idLabel;
	private Map<E,String> stationTypes;
	
//	SequenceType type;
	
	public Sequence (){
		this.stations = new TreeMap<Ordinal,E>(); 
	}
	
	public Sequence(String idLabel, int id){
		
		this.stations = new TreeMap<Ordinal,E>(); 
		this.idLabel = idLabel;
		this.id = id;
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String idLabel() {
		return idLabel;
	}
	
	public String getLabel(){
		return idLabel+"_"+id;
	}

/*	public void setIdLabel(String idLabel) {
		this.idLabel = idLabel;
	}*/
	
	public E getStation (Ordinal time){
		E result;
		
		if (time!=null){
			result = stations.get(time);
		} else {
			result = null;
		}
		return result;
	}


	@Override
	public String hashKey() {
		return id + "";
	}

	@Override
	public int compareTo(Sequence<E> sequence) {
		return Integer.valueOf(this.id).compareTo(Integer.valueOf(sequence.id));
	}
	
	public Ordinal getFirstTime (){
		Ordinal result;
		
		result = ((TreeMap<Ordinal,E>)stations).firstKey();
		
		//
		return result;
	}
	
	public Ordinal getFirstTimeNonNull(){
		Ordinal result;

		result = null;
		
		for (Ordinal time : getTimes()){
			if (stations.get(time)!=null){
				result = time;
				break;
			}
		}
			
		//
		return result;
	}

	
	public Ordinal getFirstTime(E station){
		Ordinal result;

		result = null;
		
		if (station != null){
			
			for (Ordinal time : getTimes()){
				if (station.equals(stations.get(time))){
					result = time;
					break;
				}
			}
		}
			
		//
		return result;
	}
	
	public Ordinal getNextTime (Ordinal key){
		Ordinal result;
		
		if (key!=null){
			result = ((TreeMap<Ordinal,E>)stations).higherKey(key);
		} else {
			result = null;
		}
		//
		return result;
	}
	
	public Ordinal getNextFreeTime (Ordinal key, Set<Ordinal> filter){
		Ordinal result;
		
		if (key!=null){
			result = ((TreeMap<Ordinal,E>)stations).higherKey(key);
			if (result !=null && filter.contains(result)){
				result = getNextFreeTime(result,filter);
			}
		} else {
			result = null;
		}
		//
		return result;
	}
	
	public List<String> getStationTypesAsSortedList(){
		List<String> result;
		
		result = new ArrayList<String>();
		for (String type : getStationTypes().values()){
			if (!result.contains(type)){
				result.add(type);
			}
		}
		Collections.sort(result);
		
		//
		return result;
	}
	
	public String getStationTypesAsString (){
		String result;
		
		result ="";
		boolean first = true;
		List<String> list = getStationTypesAsSortedList();
		
		if (getStationTypes() != null){
			for (String type : list){
				if (first){
					result += type;
					first = false;
				} else {
					result += ";"+type;
				}
			}
		}
		
		//
		return result;
	}
	
	public int getNrStations(){
		return stations.size();
	}
	
	public int getNrStationTypes(){
		int result;
		
		if (getStationTypes() != null){
			result = getStationTypesAsSortedList().size();
		} else {
			result = 0;
		}
		//
		return result;
	}
	
	public String getStationType (E station){
		String result;
		
		result = getStationTypes().get(station);

		//
		return result;
		
	}
	
/*	public Partition<E> getStationTypes (){
		Partition<E> result;
		
		result = new Partition<E>();
		
		for (E station : stations.values()){
			result.put(station, new Value(getStationType(station)));
		}
		
		//
		return result;
	}*/
	
	public Map<Ordinal, E> getStations() {
		return stations;
	}
	
	public void put(Ordinal ordinal, E station){
		stations.put(ordinal, station);
	}
	
	public List<E> toList(){
		List<E> result;
		
		//
		result = new ArrayList<E>(stations.values());
		
		//
		return result;
	}
	
	public String toValueString(){
		String result;
		
		result = "";
		
		for (E value : toList()){
			if (value!=null){
				result += value +";";
			} else {
				result += ";;";
			}
		}
		//
		return result;
	}
	
	
	public List<Ordinal> getTimes(){
		List<Ordinal> result;
		
		result = new ArrayList<Ordinal>(stations.keySet());
		Collections.sort(result);
		
		//
		return result;
	}
	
	public E firstStation(){
		E result;
		
		result = getStation(getTimes().get(0));
		
		//
		return result;
	}
	
	public E getPreviousStation(Ordinal time){
		E result;
		
		int idx = getTimes().indexOf(time);
		
		if (idx==0){
			result = null;
		} else {
			result = getStation(getTimes().get(idx-1));
		}
		//
		return result;
	}
	
	public List<Integer> getYears(){
		List<Integer> result;
		
		result = new ArrayList<Integer>();
		
		for (Ordinal time : getTimes()){
			if (result.contains(time.getYear())){
				result.add(time.getYear());
			} else {
				System.err.println("Warning: multiple occurence of year "+time.getYear());
			}
		}
		//
		return result;
	}
	
	public String toString(){
		
		return getLabel();
	}

	public Map<E,String> getStationTypes() {
		return stationTypes;
	}

	public void setStationTypes(Map<E,String> stationTypes) {
		this.stationTypes = stationTypes;
	}
	
	public Individuals getIndividuals() throws PuckException{
		Individuals result;
		
		result = new Individuals();
		
		for (E station : stations.values()){
			
			if (station instanceof Populatable) {
				
				result.add(((Populatable)station).getIndividuals());
			}
		}
		
		//
		return result;
	}
	
	public Individuals getAllIndividuals(List<Ordinal> times) throws PuckException{
		Individuals result;
		
		result = new Individuals();
		
		for (Ordinal time : times){
			
			E station = stations.get(time);
			
			if (station instanceof Populatable) {
				
				result.add(((Populatable)station).getIndividuals());
			}
		}
		
		//
		return result;
	}
	
	public <V extends Numberable> List<String> idValues(){
		List<String> result;
		
		result = new ArrayList<String>();
		
		for (E station : stations.values()){
			
			if (station instanceof NumberablesHashMap<?>){
				
				for (V stationItem : (NumberablesHashMap<V>)station){
			
					if (stationItem instanceof Attributable){
						
						String idValue = ((Attributable)stationItem).getAttributeValue(idLabel);
						
						if (!result.contains(idValue)){
							result.add(idValue);
						}
					} 
				}
			}
		}
		Collections.sort(result);
		
		//
		return result;
	}
	
	public <V extends Numberable> Map<String,Individuals> membersByRelationId () throws PuckException{
		Map<String,Individuals> result;
		
		result = new TreeMap<String,Individuals>();
		
		for (E station : stations.values()) {
			
			if (station instanceof NumberablesHashMap<?>){
				
				for (V stationItem : (NumberablesHashMap<V>)station){
			
					if (stationItem instanceof Populatable & stationItem instanceof Attributable){
						
						String idValue = ((Attributable)stationItem).getAttributeValue(idLabel);
						
						if (idValue != null){
							Individuals members = result.get(idValue);
							if (members == null){
								members = new Individuals();
								result.put(idValue, members);
							}
							members.add(((Populatable)stationItem).getIndividuals());
						}
					}
				}
			}
		}
		
		//
		return result;
	}
	
	// Unchecked casts.. Check also for ambiguous local unit labels...
	/**
	 * @deprecated
	 * @return
	 * @throws PuckException 
	 */
/*	public <S extends Sequenceable<V>,V extends Numberable> Sequences<S,V> toSequencesByIdValue (){
		Sequences<S,V> result;
		
		result = new Sequences<S,V>();
		
		for (Ordinal time : getTimes()){
			
			E station = stations.get(time);

			if (station instanceof NumberablesHashMap<?>){
				
				for (V stationItem : (NumberablesHashMap<V>)station){
					
					String idValue = ((Attributable)stationItem).getAttributeValue(idLabel);
					
					if (idValue != null){
						Integer id = Integer.parseInt(idValue);
						S sequence = result.getById(id);
						if (sequence==null){
							sequence = new Sequence<V>(idLabel,id);
							// Temporary method, since census methods are gender sensitive (separate index for genders necessary)
//							groupSequence.setEgo(new Individual(id,id+"",Gender.UNKNOWN));
							result.put(sequence);
						}
						sequence.put(time, stationItem);
					}
				}
			} else {
				System.err.println("Invalid local unit parameter "+idValue);
//				throw PuckExceptions.INVALID_PARAMETER.create("Invalid local unit label.");
			}
		}
		
		//
		return result;
	}
	
	// Unchecked casts
	public <V extends Numberable> EgoSequences toSequencesByEgo () throws PuckException{
		EgoSequences result;
		
		result = new EgoSequences();
		
		for (Ordinal time : getTimes()){
			
			E station = stations.get(time);

			if (station instanceof NumberablesHashMap<?>){
				
				for (V stationItem : (NumberablesHashMap<V>)station){
					
					for (Individual ego: ((Populatable)stationItem).getIndividuals()){
						EgoSequence indiSequence = result.getById(ego.getId());
						if (indiSequence==null){
							indiSequence = new EgoSequence(ego);
							result.put(indiSequence);
						}
						indiSequence.put(time, (Relation)stationItem);
					}
				}
			}
		}
		
		//
		return result;
	}*/
	

	public Individuals getIndividuals(Segmentation segmentation) throws PuckException{
		Individuals result;
		
		result = new Individuals();
		
		for (Individual individual : getIndividuals()){
			if (segmentation.getCurrentIndividuals().contains(individual)){
				result.add(individual);
			}
		}

		//
		return result;
	}
	
	@Override
	public Sequence<E> clone (){
		Sequence<E> result;
		
		result = new Sequence<E>(idLabel,id);
		
		for (Ordinal key : stations.keySet()){
			result.put(key, getStation(key));
		}
		
		//
		return result;
	}
	
	public Sequence<E> clone(int id){
		Sequence<E> result;
		
		result = clone();
		result.setId(id);
		
		//
		return result;
	}

	

	

}
