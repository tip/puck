package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.graphs.random.RandomGraphReporter;
import org.tip.puck.matrix.MatrixStatistics;
import org.tip.puck.report.Report;
import org.tip.puckgui.GroupNetGUI;
import org.tip.puckgui.PuckGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class RandomPermutationInputWindow extends JFrame {

	private static final long serialVersionUID = -6911460223180678404L;
	private static final Logger logger = LoggerFactory.getLogger(RandomPermutationInputWindow.class);

	private JFrame thisJFrame;
	private JPanel contentPane;
	private JSpinner spinnerRuns;
	private JCheckBox chckbxExtract;

	/**
	 * Similar with VirtualFielworkInputWindow.
	 */
	public RandomPermutationInputWindow(final GroupNetGUI groupNetGUI) {

		// /////////////////////////////////
		setIconImage(Toolkit.getDefaultToolkit().getImage(RandomPermutationInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		setTitle("Random Permutation Input Window");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 365, 175);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});

		JButton btnRestoreDefaults = new JButton("Restore defaults");
		btnRestoreDefaults.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Restore Defaults.
				setCriteria(new RandomPermutationCriteria());
			}
		});
		buttonPanel.add(btnRestoreDefaults);
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		getRootPane().setDefaultButton(btnLaunch);
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					//
					RandomPermutationCriteria criteria = getCriteria();

					//
					PuckGUI.instance().getPreferences().setRandomPermutationCriteria(criteria);

					//
					MatrixStatistics sourceStats = new MatrixStatistics(groupNetGUI.getGroupNet());
					MatrixStatistics randStats = RandomGraphReporter.createRandomGraphStatisticsByRandomPermutation(groupNetGUI.getGroupNet(),
							criteria.getNumberOfRuns());

					// Build report.
					Report report = RandomGraphReporter.reportRandomAllianceNetworkByRandomPermutation(criteria.getNumberOfRuns(), randStats, sourceStats);

					// Manage window.
					if (criteria.isExtractRepresentative()) {
						// Create new window and fill it with a report tab.
						if (StringUtils.isBlank(randStats.getGraph().getLabel())) {
							randStats.getGraph().setLabel("Random Group Network");
						}
						GroupNetGUI newGUI = PuckGUI.instance().createGroupNetGUI(randStats.getGraph());
						newGUI.addReportTab(report);

					} else {
						groupNetGUI.addReportTab(report);
					}

					dispose();
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(RandomPermutationInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);

		JPanel panel = new JPanel();
		this.contentPane.add(panel, BorderLayout.NORTH);
		panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblNrRuns = new JLabel("Number of runs:");
		lblNrRuns.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblNrRuns, "2, 4, right, default");

		this.spinnerRuns = new JSpinner();
		this.spinnerRuns.setModel(new SpinnerNumberModel(new Integer(100), new Integer(0), null, new Integer(100)));
		panel.add(this.spinnerRuns, "4, 4");

		this.chckbxExtract = new JCheckBox("Extract a representative network");
		panel.add(this.chckbxExtract, "2, 6, 3, 1");

		// //////////////////
		setCriteria(PuckGUI.instance().getPreferences().getRandomPermutationCriteria());
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public RandomPermutationCriteria getCriteria() throws PuckException {
		RandomPermutationCriteria result;

		//
		result = new RandomPermutationCriteria();

		//
		result.setNumberOfRuns((Integer) this.spinnerRuns.getValue());
		result.setExtractRepresentative(this.chckbxExtract.isSelected());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public void setCriteria(final RandomPermutationCriteria source) {
		//
		if (source != null) {
			//
			this.spinnerRuns.setValue(source.getNumberOfRuns());
			this.chckbxExtract.setSelected(source.isExtractRepresentative());
		}
	}
}
