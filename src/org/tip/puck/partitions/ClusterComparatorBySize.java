package org.tip.puck.partitions;

import java.util.Comparator;

import org.tip.puck.util.MathUtils;

/**
 * 
 * @author TIP
 * 
 */
public class ClusterComparatorBySize implements Comparator<Cluster<?>> {
	/**
	 * 
	 */
	@Override
	public int compare(final Cluster<?> alpha, final Cluster<?> bravo) {
		int result;

		//
		Integer size1;
		if (alpha == null) {
			size1 = null;
		} else {
			size1 = alpha.size();
		}

		//
		Integer size2;
		if (bravo == null) {
			size2 = null;
		} else {
			size2 = bravo.size();
		}

		//

		result = MathUtils.compare(size1, size2);

		//
		return result;
	}
}
