package org.tip.puckgui.models;

import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;

import org.tip.puck.report.ReportAttributes;

/**
 * 
 * @author TIP
 */
public class ReportItemsModel extends AbstractTableModel {

	private static final long serialVersionUID = 9193331439308501818L;

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.tip.puckgui.messages");

	protected ReportAttributes source;

	/**
	 * 
	 */
	public ReportItemsModel(final ReportAttributes source) {
		super();
		this.source = source;
	}

	/**
	 * 
	 */
	@Override
	public Class getColumnClass(final int columnIndex) {
		Class result;

		switch (columnIndex) {
			case 0:
				result = String.class;
			break;
			case 1:
				result = String.class;
			break;
			default:
				result = String.class;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getColumnCount() {
		int result;

		result = 2;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String getColumnName(final int columnIndex) {
		String result;

		switch (columnIndex) {
			case 0:
				// result = BUNDLE.getString("MainWindow.individual.id");
				result = "Label";
			break;
			case 1:
				result = "Value";
			break;
			default:
				result = "";
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getRowCount() {
		int result;

		if (this.source == null) {
			result = 0;
		} else {
			result = this.source.size();
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Object getValueAt(final int rowIndex, final int columnIndex) {
		Object result;

		if (this.source == null) {
			result = null;
		} else {
			switch (columnIndex) {
				case 0:
					result = this.source.get(rowIndex).label();
				break;
				case 1:
					result = this.source.get(rowIndex).value();
				break;
				default:
					result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public boolean isCellEditable(final int row, final int col) {
		boolean result;

		result = false;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void setSource(final ReportAttributes source) {
		this.source = source;
		fireTableStructureChanged();
	}

	/**
	 * 
	 * @return
	 */
	public ReportAttributes source() {
		ReportAttributes result;

		result = this.source;

		//
		return result;
	}
}
