package org.tip.puck.net.relations;

import fr.devinsy.util.StringList;

public class Test {
	
	public static StringList kinTerms (String name){

		StringList result;
		
		result = null;

		if (name.equals("English")){
			result = english();
		} else if (name.equals("Deutsch")){
			result = german();
		} else if (name.equals("Watsi")){
			result = watchi();
		}
		//
		return result;
		
	}
	
	public static StringList watchi (){
		StringList result;
		
		result = new StringList();
		
		result.add("TÔ\tFATHER");
		result.add("NÔ\tMOTHER");
		result.add("FOFO\tSIBLING\t\t\t\tMALE");
		result.add("DADA\tSIBLING\t\t\t\tFEMALE");
		result.add("FOFO\t\t\tATAGAN\tVI\tMALE");
		result.add("FOFO\t\t\tNYINE\tVI\tMALE");
		result.add("FOFO\t\t\tNYAGAN\tVI\tMALE");
		result.add("FOFO\t\t\tTASI\tVI\tMALE");
		result.add("DADA\t\t\tATAGAN\t\tFEMALE");
		result.add("DADA\t\t\tNYINE\tVI\tFEMALE");
		result.add("DADA\t\t\tNYAGAN\tVI\tFEMALE");
		result.add("DADA\t\t\tTASI\tVI\tFEMALE");
		result.add("VI\t\tTÔ");
		result.add("VI\t\tNÔ");
		result.add("VI\t\tATAGAN");
		result.add("VI\t\tNAGAN");
		result.add("TÔGBUI\t\t\tTÔ\tTÔ");
		result.add("TÔGBUI\t\t\tNÔ\tTÔ");
		result.add("MAMA\t\t\tTÔ\tNÔ");
		result.add("MAMA\t\t\tNÔ\tNÔ");
		result.add("TÔGBUIYÔVI\t\tTÔGBUI");
		result.add("MAMAYÔVI\t\tMAMA");
		result.add("ATAGAN\t\t\tTÔ\tFOFO");
		result.add("NYINE\t\t\tNÔ\tFOFO");
		result.add("NAGAN\t\t\tNÔ\tDADA");
		result.add("TASI\t\t\tTÔ\tDADA");
		result.add("NYINEYÔVI\t\tNYINE");
		result.add("TASIYÔVI\t\tTASI");
		
		//
		return result;
		
	}


	public static StringList english (){
		StringList result;
		
		result = new StringList();
		
		result.add("FATHER\tFATHER");
		result.add("MOTHER\tMOTHER");
		result.add("BROTHER\tSIBLING\t\t\t\tMALE");
		result.add("SISTER\tSIBLING\t\t\t\tFEMALE");
		result.add("SON\t\tFATHER\t\t\tMALE");
		result.add("SON\t\tMOTHER\t\t\tMALE");
		result.add("DAUGHTER\t\tFATHER\t\t\tFEMALE");
		result.add("DAUGHTER\t\tMOTHER\t\t\tFEMALE");
		result.add("GRANDFATHER\t\t\tFATHER\tFATHER");
		result.add("GRANDFATHER\t\t\tMOTHER\tFATHER");
		result.add("GRANDMOTHER\t\t\tFATHER\tMOTHER");
		result.add("GRANDMOTHER\t\t\tMOTHER\tMOTHER");
		result.add("GRANDSON\t\tGRANDFATHER\t\t\tMALE");
		result.add("GRANDSON\t\tGRANDMOTHER\t\t\tMALE");
		result.add("GRANDDAUGHTER\t\tGRANDFATHER\t\t\tFEMALE");
		result.add("GRANDDAUGHTER\t\tGRANDMOTHER\t\t\tFEMALE");
		result.add("UNCLE\t\t\tFATHER\tBROTHER");
		result.add("UNCLE\t\t\tMOTHER\tBROTHER");
		result.add("AUNT\t\t\tFATHER\tSISTER");
		result.add("AUNT\t\t\tMOTHER\tSISTER");
		result.add("NEPHEW\t\tUNCLE\t\t\tMALE");
		result.add("NEPHEW\t\tAUNT\t\t\tMALE");
		result.add("NIECE\t\tUNCLE\t\t\tFEMALE");
		result.add("NIECE\t\tAUNT\t\t\tFEMALE");

		
		//
		return result;
		
	}
	
	public static StringList german (){
		StringList result;
		
		result = new StringList();
		
		result.add("VATER\tFATHER");
		result.add("MUTTER\tMOTHER");
		result.add("BRUDER\tSIBLING\t\t\t\tMALE");
		result.add("SCHWESTER\tSIBLING\t\t\t\tFEMALE");
		result.add("SOHN\t\tVATER\t\t\tMALE");
		result.add("SOHN\t\tMUTTER\t\t\tMALE");
		result.add("TOCHTER\t\tVATER\t\t\tFEMALE");
		result.add("TOCHTER\t\tMUTTER\t\t\tFEMALE");
		result.add("NICHTE\t\t\tBRUDER\tTOCHTER");
		result.add("NICHTE\t\t\tSCHWESTER\tTOCHTER");
		
		
		//
		return result;
		
	}
	
	
}
