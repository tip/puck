package org.tip.puck.net.workers;

import java.util.ArrayList;
import java.util.Collections;

import org.tip.puck.net.workers.AttributeValueDescriptorComparator.Sorting;

/**
 * 
 * 
 * @author TIP
 */
public class AttributeValueDescriptorList extends ArrayList<AttributeValueDescriptor> {

	private static final long serialVersionUID = 4435746128087868670L;

	/**
	 * 
	 * @param scope
	 * @param label
	 */
	public AttributeValueDescriptorList() {
		super();
	}

	public AttributeValueDescriptorList(final AttributeValueDescriptors source) {
		super(source.size());

		for (AttributeValueDescriptor descriptor : source) {
			this.add(descriptor);
		}
	}

	/**
	 * 
	 */
	public AttributeValueDescriptorList reverse() {

		Collections.reverse(this);

		//
		return this;
	}

	/**
	 * 
	 */
	public AttributeValueDescriptorList sortByCount() {

		Collections.sort(this, new AttributeValueDescriptorComparator(Sorting.COUNT));

		//
		return this;
	}

	/**
	 * 
	 */
	public AttributeValueDescriptorList sortByValue() {

		Collections.sort(this, new AttributeValueDescriptorComparator(Sorting.VALUE));

		//
		return this;
	}

	/**
	 * 
	 * @return
	 */
	public long total() {
		long result;

		result = 0;
		for (AttributeValueDescriptor valueDescriptor : this) {
			result += valueDescriptor.getCount();
		}

		//
		return result;
	}
}
