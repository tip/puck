package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellEditor;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.workers.AttributeWorker;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.util.NumberablesHashMap.IdStrategy;
import org.tip.puck.util.ToolBox;
import org.tip.puckgui.InputSettings.CheckLevel;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.models.FamiliesCellRenderer;
import org.tip.puckgui.models.FamiliesModel;
import org.tip.puckgui.models.FamilyChildrenModel;
import org.tip.puckgui.util.AutoComboBox;
import org.tip.puckgui.util.ComboBoxIds;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.StringSet;

/**
 * 
 * @author TIP
 */
public class FamiliesPanel extends JPanel {

	private static final long serialVersionUID = -4201523850285782993L;

	private static final Logger logger = LoggerFactory.getLogger(FamiliesPanel.class);

	/**
	 * This variable is used to unmanage lost focus on edition mode.
	 */
	private boolean doLostFocus = true;

	int reportCounter = 0;
	private JPanel thisJPanel;
	private NetGUI netGUI;
	private StringSet attributeTemplates;
	private static ImageIcon mediumFemaleIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/female-32x32.png"));
	private static ImageIcon mediumMaleIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/male-32x32.png"));
	private static ImageIcon mediumUnknowIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/unknown-32x32.png"));
	private static ImageIcon largeUnknowIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/unknown-64x64.png"));
	private static ImageIcon largeUnmarriedIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/unmarried-x45.png"));
	private static ImageIcon largeMarriedIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/married-x45.png"));
	private static ImageIcon largeDivorcedIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/divorced-x45.png"));
	private JTextField txtfldSearchFamily;
	private JList familyList;
	private JTable tableFamilyChildren;
	private JLabel lblFamilyId;
	private JLabel lblFamilyStatus;
	private JLabel lblFamilyFatherId;
	private JLabel lblFamilyFatherName;
	private JLabel lblFamilyMotherId;
	private JLabel lblFamilyMotherName;
	private JLabel lblFamilyChildren;
	private JLabel lblPosition;
	private AttributesPanel attributesPanel;
	private AutoComboBox cmbbxFamilyParent1;
	private AutoComboBox cmbbxFamilyParent2;
	private AutoComboBox cmbbxFamilyChildrenIds;
	private AutoComboBox cmbbxIndividualChildrenOrders;
	private JButton btnAddChild;
	private JTextField txtFldFamilyId;

	/**
	 * Initialize the contents of the frame.
	 */
	public FamiliesPanel(final NetGUI guiManager) {
		//
		this.thisJPanel = this;
		this.netGUI = guiManager;
		this.attributeTemplates = new StringSet();
		updateAttributeTemplates();

		//
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		JSplitPane familiesSplit = new JSplitPane();
		add(familiesSplit);
		familiesSplit.setAlignmentY(Component.CENTER_ALIGNMENT);
		familiesSplit.setAlignmentX(Component.CENTER_ALIGNMENT);

		JPanel familiesPanel = new JPanel();
		familiesPanel.setSize(new Dimension(400, 0));
		familiesPanel.setMinimumSize(new Dimension(200, 10));
		familiesSplit.setLeftComponent(familiesPanel);
		familiesPanel.setLayout(new BoxLayout(familiesPanel, BoxLayout.Y_AXIS));

		JScrollPane familiesScrollPane = new JScrollPane();
		familiesScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		familiesPanel.add(familiesScrollPane);

		this.familyList = new JList();
		this.familyList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(final ListSelectionEvent event) {
				//
				if (!event.getValueIsAdjusting()) {
					// Select family.
					logger.debug("value changed = " + event.getValueIsAdjusting() + " " + event.getFirstIndex() + " " + event.getLastIndex() + " "
							+ FamiliesPanel.this.familyList.getSelectedIndex());
					if (FamiliesPanel.this.familyList.getSelectedIndex() != -1) {
						Family family = (Family) ((JList) event.getSource()).getModel().getElementAt(FamiliesPanel.this.familyList.getSelectedIndex());
						updateFamilyIdentity(family);
					}
				}
			}
		});
		this.familyList.setDoubleBuffered(true);
		this.familyList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.familyList.setCellRenderer(new FamiliesCellRenderer());
		this.familyList.setModel(new FamiliesModel(null));
		familiesScrollPane.setViewportView(this.familyList);

		JPanel familyPanel = new JPanel();
		familyPanel.setAlignmentY(Component.TOP_ALIGNMENT);
		familyPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		familiesSplit.setRightComponent(familyPanel);
		familyPanel.setLayout(new BoxLayout(familyPanel, BoxLayout.Y_AXIS));

		JPanel panelFamilyIdentity = new JPanel();
		familyPanel.add(panelFamilyIdentity);
		panelFamilyIdentity.setLayout(new BoxLayout(panelFamilyIdentity, BoxLayout.X_AXIS));

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		panelFamilyIdentity.add(horizontalStrut_2);

		Component horizontalGlue_6 = Box.createHorizontalGlue();
		panelFamilyIdentity.add(horizontalGlue_6);

		JPanel panel_1 = new JPanel();
		panelFamilyIdentity.add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));

		Component verticalStrut_1 = Box.createVerticalStrut(10);
		verticalStrut_1.setMaximumSize(new Dimension(20, 10));
		panel_1.add(verticalStrut_1);

		this.lblFamilyId = new JLabel("555");
		this.lblFamilyId.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				//
				if (event.getButton() == MouseEvent.BUTTON1) {
					//
					setFamilyIdEditorOn();
				}
			}
		});
		panel_1.add(this.lblFamilyId);
		this.lblFamilyId.setAlignmentX(Component.RIGHT_ALIGNMENT);

		this.txtFldFamilyId = new JTextField();
		this.txtFldFamilyId.setFocusTraversalKeysEnabled(false);
		this.txtFldFamilyId.setMaximumSize(new Dimension(200, 2147483647));
		this.txtFldFamilyId.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent event) {
				// FAMILY ID KEYPRESSED.

				try {
					if (event.getKeyCode() == KeyEvent.VK_ESCAPE) {
						logger.debug("escape pressed");
						FamiliesPanel.this.txtFldFamilyId.transferFocus();
					} else if (event.getKeyCode() == KeyEvent.VK_ENTER) {
						logger.debug("enter pressed");
						save();
						FamiliesPanel.this.txtFldFamilyId.transferFocus();
					} else if (event.getKeyCode() == KeyEvent.VK_TAB) {
						if (event.isShiftDown()) {
							logger.debug("shift-tab pressed");
							save();
							FamiliesPanel.this.txtFldFamilyId.transferFocus();
						} else {
							logger.debug("tab pressed");
							save();
							FamiliesPanel.this.txtFldFamilyId.transferFocus();
							setParent1EditorOn();
						}
					}
				} catch (PuckException exception) {
					// Save failed.
					FamiliesPanel.this.txtFldFamilyId.requestFocus();
				}
			}

			public void save() throws PuckException {
				// Save new id.
				String input = FamiliesPanel.this.txtFldFamilyId.getText();
				logger.debug("[lbl,cmb]=[" + FamiliesPanel.this.lblFamilyId.getText() + "," + input + "]");

				if ((StringUtils.isNotBlank(input))
						&& (!StringUtils.equals(FamiliesPanel.this.lblFamilyId.getText(), FamiliesPanel.this.txtFldFamilyId.getText()))) {
					input = input.trim();
					if (NumberUtils.isDigits(input)) {
						//
						int targetId = Integer.parseInt(input);

						Family family = FamiliesPanel.this.netGUI.getNet().families().getById(targetId);
						if (family == null) {
							//
							Family currentFamily = getSelectedFamily();
							FamiliesPanel.this.netGUI.getNet().changeId(currentFamily, targetId);
							FamiliesPanel.this.netGUI.setChanged(true);

							//
							FamiliesPanel.this.netGUI.updateAll();

							//
							select(currentFamily);

						} else {
							FamiliesPanel.this.doLostFocus = false;
							JOptionPane.showMessageDialog(FamiliesPanel.this.thisJPanel, "ID already in use.", "Invalid id", JOptionPane.ERROR_MESSAGE);
							FamiliesPanel.this.doLostFocus = true;
							throw PuckExceptions.INVALID_PARAMETER.create();
						}
					} else {
						FamiliesPanel.this.doLostFocus = false;
						JOptionPane.showMessageDialog(FamiliesPanel.this.thisJPanel, "Numeric value required.", "Invalid id", JOptionPane.ERROR_MESSAGE);
						FamiliesPanel.this.doLostFocus = true;
						throw PuckExceptions.INVALID_PARAMETER.create();
					}
				}
			}
		});
		this.txtFldFamilyId.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(final FocusEvent e) {
				logger.debug("FOCUS LOST FAMILY ID");

				//
				if (FamiliesPanel.this.doLostFocus) {
					FamiliesPanel.this.lblFamilyId.setVisible(true);
					FamiliesPanel.this.txtFldFamilyId.setVisible(false);
				}
			}
		});
		this.txtFldFamilyId.setFocusTraversalKeysEnabled(false);
		panel_1.add(this.txtFldFamilyId);
		this.txtFldFamilyId.setColumns(10);

		Component verticalStrut = Box.createVerticalStrut(10);
		verticalStrut.setMaximumSize(new Dimension(20, 10));
		panel_1.add(verticalStrut);

		this.lblPosition = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("FamiliesPanel.lblNewLabel.text")); //$NON-NLS-1$ //$NON-NLS-2$
		this.lblPosition.setAlignmentX(Component.RIGHT_ALIGNMENT);
		panel_1.add(this.lblPosition);

		Component horizontalStrut_5 = Box.createHorizontalStrut(5);
		panelFamilyIdentity.add(horizontalStrut_5);

		this.lblFamilyStatus = new JLabel("");
		this.lblFamilyStatus.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Click union status.
				if (event.getButton() == MouseEvent.BUTTON1) {
					//
					FamiliesPanel.this.lblFamilyStatus.requestFocus();
					Family currentFamily = getSelectedFamily();
					if (currentFamily != null) {
						switch (currentFamily.getUnionStatus()) {
							case UNMARRIED:
								currentFamily.setMarried();
							break;
							case MARRIED:
								currentFamily.setDivorced();
							break;
							case DIVORCED:
								currentFamily.setUnmarried();
							break;
						}

						//
						updateFamilyIdentity(currentFamily);

						//
						FamiliesPanel.this.netGUI.setChanged(true);
					}
				}
			}
		});
		this.lblFamilyStatus.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.lblFamilyStatus.setIcon(largeUnmarriedIcon);
		this.lblFamilyStatus.setFont(new Font("Dialog", Font.PLAIN, 24));
		panelFamilyIdentity.add(this.lblFamilyStatus);

		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		panelFamilyIdentity.add(horizontalStrut_4);

		JPanel panel_5 = new JPanel();
		panel_5.setMaximumSize(new Dimension(32767, 200));
		panelFamilyIdentity.add(panel_5);
		panel_5.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.DEFAULT_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] {
				FormFactory.MIN_ROWSPEC, FormFactory.MIN_ROWSPEC, FormFactory.MIN_ROWSPEC, FormFactory.MIN_ROWSPEC, }));

		this.cmbbxFamilyParent1 = new AutoComboBox(null);
		this.cmbbxFamilyParent1.setStrict(false);
		this.cmbbxFamilyParent1.setVisible(false);
		this.cmbbxFamilyParent1.setEditable(true);
		this.cmbbxFamilyParent1.getEditor().getEditorComponent().setFocusTraversalKeysEnabled(false);

		// Custom manually.
		this.cmbbxFamilyParent1.getEditor().getEditorComponent().addFocusListener(new FocusListener() {
			@Override
			public void focusGained(final FocusEvent arg0) {
				logger.debug("FOCUS GAINED PARENT 1 ID");
			}

			@Override
			public void focusLost(final FocusEvent arg0) {
				logger.debug("FOCUS LOST PARENT 1 ID");

				if (FamiliesPanel.this.doLostFocus) {
					//
					FamiliesPanel.this.cmbbxFamilyParent1.setVisible(false);
					FamiliesPanel.this.lblFamilyFatherId.setVisible(true);
					FamiliesPanel.this.lblFamilyFatherName.setVisible(true);

					// Clean.
					FamiliesPanel.this.cmbbxFamilyParent1.setDataList(new ArrayList<String>());
				}
			}
		});

		this.cmbbxFamilyParent1.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent event) {

				try {
					if (event.getKeyCode() == KeyEvent.VK_ESCAPE) {
						logger.debug("escape pressed");
						FamiliesPanel.this.cmbbxFamilyParent1.getEditor().getEditorComponent().transferFocus();
					} else if (event.getKeyCode() == KeyEvent.VK_ENTER) {
						logger.debug("enter pressed");
						save();
						FamiliesPanel.this.cmbbxFamilyParent1.getEditor().getEditorComponent().transferFocus();
					} else if (event.getKeyCode() == KeyEvent.VK_TAB) {
						if (event.isShiftDown()) {
							logger.debug("shift-tab pressed");
							save();
							FamiliesPanel.this.cmbbxFamilyParent1.getEditor().getEditorComponent().transferFocus();
							setFamilyIdEditorOn();
						} else {
							logger.debug("tab pressed");
							save();
							FamiliesPanel.this.cmbbxFamilyParent1.getEditor().getEditorComponent().transferFocus();
							setParent2EditorOn();
						}
					}
				} catch (PuckException exception) {
					// In case of failed save.
					logger.debug("SAVE FAILED");
					FamiliesPanel.this.cmbbxFamilyParent1.getEditor().getEditorComponent().requestFocus();
				}
			}

			public void save() throws PuckException {
				//
				String newFatherIdInput = (String) FamiliesPanel.this.cmbbxFamilyParent1.getEditor().getItem();
				logger.debug("[lbl,cmb]=[" + FamiliesPanel.this.lblFamilyFatherId.getText() + "," + newFatherIdInput + "]");

				//
				Individual newFather = ComboBoxIds.getOrCreateIndividualFromInput(FamiliesPanel.this.netGUI.getIndividualIdStrategy(),
						FamiliesPanel.this.netGUI.getNet(), newFatherIdInput, Gender.MALE);

				//
				Family currentFamily = getIdentityFamily();

				if (newFather != currentFamily.getFather()) {
					// Check undefinedRoles.
					String errorMessage = IndividualsPanel.controlPartners(CheckLevel.ERROR, newFather, currentFamily.getMother(), currentFamily.getChildren()
							.toArray());
					if (errorMessage != null) {
						FamiliesPanel.this.doLostFocus = false;
						JOptionPane.showMessageDialog(FamiliesPanel.this.thisJPanel, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
						FamiliesPanel.this.doLostFocus = true;
						throw PuckExceptions.INVALID_PARAMETER.create();
					} else {
						//
						Family previousFamily = FamiliesPanel.this.netGUI.getNet().families().getBySpouses(newFather, currentFamily.getFather());
						if ((newFather != null) && (currentFamily.getMother() != null) && (previousFamily != null)
								&& (previousFamily.getId() != currentFamily.getId())) {
							FamiliesPanel.this.doLostFocus = false;
							JOptionPane.showMessageDialog(FamiliesPanel.this.thisJPanel, "This family already exists (" + previousFamily.getId() + ").",
									"Error", JOptionPane.ERROR_MESSAGE);
							FamiliesPanel.this.doLostFocus = true;
							throw PuckExceptions.INVALID_PARAMETER.create();
						} else {
							//
							NetUtils.setKinFather(currentFamily, newFather);
							if (currentFamily.numberOfParents() == 2) {
								currentFamily.setUnionStatus(FamiliesPanel.this.netGUI.getDefaultUnionStatus());
							}
							FamiliesPanel.this.netGUI.setChanged(true);

							// Refresh.
							try {
								FamiliesPanel.this.netGUI.getSegmentation().refresh();
							} catch (PuckException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							FamiliesPanel.this.netGUI.updateAll();

							// Check warning.
							String warningMessage = IndividualsPanel.controlPartners(CheckLevel.WARNING, newFather, currentFamily.getMother(), currentFamily
									.getChildren().toArray());
							if (warningMessage != null) {
								JOptionPane.showMessageDialog(FamiliesPanel.this.thisJPanel, warningMessage, "Warning", JOptionPane.WARNING_MESSAGE);
							}
						}
					}
				}
			}
		});

		panel_5.add(this.cmbbxFamilyParent1, "1, 1, 2, 1, fill, default");

		this.lblFamilyFatherId = new JLabel("55");
		this.lblFamilyFatherId.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Family parent 1 id clicked.
				logger.debug("parent 1 id clicked");
				if (event.getButton() == MouseEvent.BUTTON1) {
					//
					setParent1EditorOn();
				}
			}
		});
		this.lblFamilyFatherId.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_5.add(this.lblFamilyFatherId, "1, 2, right, center");

		this.lblFamilyFatherName = new JLabel("anonymous");
		this.lblFamilyFatherName.setHorizontalAlignment(SwingConstants.LEFT);
		this.lblFamilyFatherName.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Click on family father.
				try {
					if (event.getClickCount() == 2) {
						//
						int selectedIndex = FamiliesPanel.this.familyList.getSelectedIndex();
						if (selectedIndex != -1) {
							Family selected = (Family) ((FamiliesModel) FamiliesPanel.this.familyList.getModel()).getElementAt(selectedIndex);
							if ((selected != null) && (selected.getFather() != null)) {
								FamiliesPanel.this.netGUI.changeSegmentationToCluster(selected.getFather());
							}
						}
					}
				} catch (final PuckException exception) {
					exception.printStackTrace();
				}
			}
		});
		this.lblFamilyFatherName.setIcon(new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/unknown-32x32.png")));
		panel_5.add(this.lblFamilyFatherName, "2, 2, left, center");

		JPopupMenu popupMenu = new JPopupMenu();
		addPopup(this.lblFamilyFatherName, popupMenu);

		JMenuItem mntmBrowse = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("FamiliesPanel.mntmBrowse.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmBrowse.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Browse parent1.
				try {
					//
					int selectedIndex = FamiliesPanel.this.familyList.getSelectedIndex();
					if (selectedIndex != -1) {
						Family selected = (Family) ((FamiliesModel) FamiliesPanel.this.familyList.getModel()).getElementAt(selectedIndex);
						if ((selected != null) && (selected.getFather() != null)) {
							FamiliesPanel.this.netGUI.changeSegmentationToCluster(selected.getFather());
						}
					}
				} catch (final PuckException exception) {
					//
					exception.printStackTrace();
				}
			}
		});
		popupMenu.add(mntmBrowse);

		JMenuItem mntmSwapParents = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("FamiliesPanel.mntmSwapParents.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmSwapParents.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Swap Individual parents.
				Family currentFamily = getIdentityFamily();
				if ((currentFamily != null) && ((currentFamily.getFather() != null) || (currentFamily.getMother() != null))) {
					NetUtils.swapParents(currentFamily);
					FamiliesPanel.this.netGUI.setChanged(true);
					FamiliesPanel.this.netGUI.updateAll();
					FamiliesPanel.this.netGUI.selectFamiliesTab(currentFamily);
				}
			}
		});

		JMenuItem mntmEdit = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("FamiliesPanel.mntmEdit.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmEdit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Edit parent 1 id.
				setParent1EditorOn();
			}
		});
		popupMenu.add(mntmEdit);
		popupMenu.add(mntmSwapParents);

		JSeparator separator = new JSeparator();
		popupMenu.add(separator);

		JMenuItem mntmDelete = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("FamiliesPanel.mntmDelete.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmDelete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Delete parent 1.
				Family currentFamily = getIdentityFamily();
				if (currentFamily.getFather() != null) {
					//
					NetUtils.removeFather(currentFamily);

					try {
						FamiliesPanel.this.netGUI.getSegmentation().refresh();
					} catch (PuckException exception) {
						// TODO Auto-generated catch block
						exception.printStackTrace();
					}
					FamiliesPanel.this.netGUI.updateAll();
					FamiliesPanel.this.netGUI.setChanged(true);
				}
			}
		});
		popupMenu.add(mntmDelete);

		this.lblFamilyMotherId = new JLabel("5555");
		this.lblFamilyMotherId.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Family parent 2 id clicked.
				logger.debug("parent 2 id clicked");
				if (event.getButton() == MouseEvent.BUTTON1) {
					//
					setParent2EditorOn();
				}
			}
		});
		this.lblFamilyMotherId.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_5.add(this.lblFamilyMotherId, "1, 3, right, center");

		this.lblFamilyMotherName = new JLabel("anonymous");
		this.lblFamilyMotherName.setHorizontalAlignment(SwingConstants.LEFT);
		this.lblFamilyMotherName.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Click on family mother.
				try {
					if (event.getClickCount() == 2) {
						int selectedIndex = FamiliesPanel.this.familyList.getSelectedIndex();
						if (selectedIndex != -1) {
							Family selected = (Family) ((FamiliesModel) FamiliesPanel.this.familyList.getModel()).getElementAt(selectedIndex);
							if ((selected != null) && (selected.getMother() != null)) {
								FamiliesPanel.this.netGUI.changeSegmentationToCluster(selected.getMother());
							}
						}
					}
				} catch (final PuckException exception) {
					exception.printStackTrace();
				}
			}
		});
		this.lblFamilyMotherName.setIcon(new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/unknown-32x32.png")));
		panel_5.add(this.lblFamilyMotherName, "2, 3, left, center");

		JPopupMenu popupMenu_1 = new JPopupMenu();
		addPopup(this.lblFamilyMotherName, popupMenu_1);

		JMenuItem mntmDelete_1 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("FamiliesPanel.mntmDelete_1.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmDelete_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Delete parent 2.
				Family currentFamily = getIdentityFamily();
				if (currentFamily.getMother() != null) {
					//
					NetUtils.removeMother(currentFamily);

					try {
						FamiliesPanel.this.netGUI.getSegmentation().refresh();
					} catch (PuckException exception) {
						// TODO Auto-generated catch block
						exception.printStackTrace();
					}
					FamiliesPanel.this.netGUI.updateAll();
					FamiliesPanel.this.netGUI.setChanged(true);
				}
			}
		});

		JMenuItem mntmSwapParents_1 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("FamiliesPanel.mntmSwapParents_1.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmSwapParents_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Swap Individual parents.
				Family currentFamily = getIdentityFamily();
				if ((currentFamily != null) && ((currentFamily.getFather() != null) || (currentFamily.getMother() != null))) {
					NetUtils.swapParents(currentFamily);
					FamiliesPanel.this.netGUI.setChanged(true);
					FamiliesPanel.this.netGUI.updateAll();
					FamiliesPanel.this.netGUI.selectFamiliesTab(currentFamily);
				}
			}
		});

		JMenuItem mntmBrowse_1 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("FamiliesPanel.mntmBrowse_1.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmBrowse_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Browse family mother.
				try {
					int selectedIndex = FamiliesPanel.this.familyList.getSelectedIndex();
					if (selectedIndex != -1) {
						Family selected = (Family) ((FamiliesModel) FamiliesPanel.this.familyList.getModel()).getElementAt(selectedIndex);
						if ((selected != null) && (selected.getMother() != null)) {
							FamiliesPanel.this.netGUI.changeSegmentationToCluster(selected.getMother());
						}
					}
				} catch (final PuckException exception) {
					//
					exception.printStackTrace();
				}
			}
		});
		popupMenu_1.add(mntmBrowse_1);

		JMenuItem mntmEdit_1 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("FamiliesPanel.mntmEdit_1.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmEdit_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Edit parent 2 id.
				setParent2EditorOn();
			}
		});
		popupMenu_1.add(mntmEdit_1);
		popupMenu_1.add(mntmSwapParents_1);

		JSeparator separator_1 = new JSeparator();
		popupMenu_1.add(separator_1);
		popupMenu_1.add(mntmDelete_1);

		this.cmbbxFamilyParent2 = new AutoComboBox(null);
		this.cmbbxFamilyParent2.setStrict(false);
		this.cmbbxFamilyParent2.setEditable(true);
		this.cmbbxFamilyParent2.setVisible(false);
		this.cmbbxFamilyParent2.getEditor().getEditorComponent().setFocusTraversalKeysEnabled(false);

		// Custom manually.
		this.cmbbxFamilyParent2.getEditor().getEditorComponent().addFocusListener(new FocusListener() {
			@Override
			public void focusGained(final FocusEvent arg0) {
				logger.debug("FOCUS GAINED PARENT 2 ID");
			}

			@Override
			public void focusLost(final FocusEvent arg0) {
				logger.debug("FOCUS LOST PARENT 2 ID");

				if (FamiliesPanel.this.doLostFocus) {
					//
					FamiliesPanel.this.cmbbxFamilyParent2.setVisible(false);
					FamiliesPanel.this.lblFamilyMotherId.setVisible(true);
					FamiliesPanel.this.lblFamilyMotherName.setVisible(true);

					// Clean.
					FamiliesPanel.this.cmbbxFamilyParent2.setDataList(new ArrayList<String>());
				}
			}
		});

		this.cmbbxFamilyParent2.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent event) {
				try {
					//
					if (event.getKeyCode() == KeyEvent.VK_ESCAPE) {
						logger.debug("escape pressed");
						FamiliesPanel.this.cmbbxFamilyParent2.getEditor().getEditorComponent().transferFocus();
					} else if (event.getKeyCode() == KeyEvent.VK_ENTER) {
						logger.debug("enter pressed");
						save();
						FamiliesPanel.this.cmbbxFamilyParent2.getEditor().getEditorComponent().transferFocus();
					} else if (event.getKeyCode() == KeyEvent.VK_TAB) {
						if (event.isShiftDown()) {
							logger.debug("shift-tab pressed");
							save();
							FamiliesPanel.this.cmbbxFamilyParent2.getEditor().getEditorComponent().transferFocus();
							setParent1EditorOn();
						} else {
							logger.debug("tab pressed");
							save();
							FamiliesPanel.this.cmbbxFamilyParent2.getEditor().getEditorComponent().transferFocus();
						}
					}
				} catch (PuckException exception) {
					// In case of failed save.
					logger.debug("SAVE FAILED");
					FamiliesPanel.this.cmbbxFamilyParent2.getEditor().getEditorComponent().requestFocus();
				}
			}

			public void save() throws PuckException {
				String newMotherIdInput = (String) FamiliesPanel.this.cmbbxFamilyParent2.getEditor().getItem();
				logger.debug("[lbl,cmb]=[" + FamiliesPanel.this.lblFamilyMotherId.getText() + "," + newMotherIdInput + "]");

				//
				Family currentFamily = getIdentityFamily();

				//
				Individual newMother = ComboBoxIds.getOrCreateIndividualFromInput(FamiliesPanel.this.netGUI.getIndividualIdStrategy(),
						FamiliesPanel.this.netGUI.getNet(), newMotherIdInput, Gender.FEMALE);

				if (newMother != currentFamily.getMother()) {

					// Check undefinedRoles.
					String errorMessage = IndividualsPanel.controlPartners(CheckLevel.ERROR, currentFamily.getFather(), newMother, currentFamily.getChildren()
							.toArray());
					if (errorMessage != null) {
						FamiliesPanel.this.doLostFocus = false;
						JOptionPane.showMessageDialog(FamiliesPanel.this.thisJPanel, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
						FamiliesPanel.this.doLostFocus = true;
						throw PuckExceptions.INVALID_PARAMETER.create();
					} else {
						//
						Family previousFamily = FamiliesPanel.this.netGUI.getNet().families().getBySpouses(newMother, currentFamily.getFather());
						if ((newMother != null) && (currentFamily.getFather() != null) && (previousFamily != null)
								&& (previousFamily.getId() != currentFamily.getId())) {
							FamiliesPanel.this.doLostFocus = false;
							JOptionPane.showMessageDialog(FamiliesPanel.this.thisJPanel, "This family already exists (" + previousFamily.getId() + ").",
									"Error", JOptionPane.ERROR_MESSAGE);
							FamiliesPanel.this.doLostFocus = true;
							throw PuckExceptions.INVALID_PARAMETER.create();
						} else {
							//
							NetUtils.setKinMother(currentFamily, newMother);
							if (currentFamily.numberOfParents() == 2) {
								currentFamily.setUnionStatus(FamiliesPanel.this.netGUI.getDefaultUnionStatus());
							}

							// Refresh.
							try {
								FamiliesPanel.this.netGUI.getSegmentation().refresh();
							} catch (PuckException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							FamiliesPanel.this.netGUI.updateAll();
							FamiliesPanel.this.netGUI.setChanged(true);

							// Check warning.
							String warningMessage = IndividualsPanel.controlPartners(CheckLevel.WARNING, currentFamily.getFather(), newMother, currentFamily
									.getChildren().toArray());
							if (warningMessage != null) {
								JOptionPane.showMessageDialog(FamiliesPanel.this.thisJPanel, warningMessage, "Warning", JOptionPane.WARNING_MESSAGE);
							}
						}
					}
				}
			}
		});

		panel_5.add(this.cmbbxFamilyParent2, "1, 4, 2, 1, fill, default");

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		panelFamilyIdentity.add(horizontalStrut_3);

		Component verticalStrut_7 = Box.createVerticalStrut(10);
		familyPanel.add(verticalStrut_7);

		JPanel panel = new JPanel();
		familyPanel.add(panel);
		panel.setLayout(new BorderLayout(0, 0));

		JSplitPane splitPane = new JSplitPane();
		panel.add(splitPane, BorderLayout.CENTER);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);

		JPanel panelFamilyChildren = new JPanel();
		panelFamilyChildren.setMinimumSize(new Dimension(10, 200));
		splitPane.setLeftComponent(panelFamilyChildren);
		panelFamilyChildren.setLayout(new BoxLayout(panelFamilyChildren, BoxLayout.Y_AXIS));

		JPanel panel_2 = new JPanel();
		panel_2.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelFamilyChildren.add(panel_2);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.X_AXIS));

		this.lblFamilyChildren = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.lblChildren.text"));
		panel_2.add(this.lblFamilyChildren);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		panel_2.add(horizontalStrut);

		this.btnAddChild = new JButton(" + ");
		this.btnAddChild.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Add Child (menu item).
				logger.debug("Add Child");
				addChild();
			}
		});
		this.btnAddChild.setBorder(new EmptyBorder(0, 0, 0, 0));
		panel_2.add(this.btnAddChild);

		Component horizontalStrut_1 = Box.createHorizontalStrut(5);
		panel_2.add(horizontalStrut_1);

		JLabel lblctrlk = new JLabel("(Ctrl-K)");
		panel_2.add(lblctrlk);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelFamilyChildren.add(scrollPane);

		this.tableFamilyChildren = new JTable();
		this.tableFamilyChildren.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Family children clicked.
				try {
					if (event.getClickCount() == 2) {
						int selectedIndex = FamiliesPanel.this.tableFamilyChildren.getSelectedRow();
						int selectedColumn = FamiliesPanel.this.tableFamilyChildren.getSelectedColumn();
						logger.debug("[index= " + selectedIndex + ", col]=[" + selectedColumn + "]");
						if (selectedIndex != -1) {
							if (selectedColumn > 0) {
								// tableFamilyChildren.editCellAt(selectedIndex,
								// 0);
								// //
								// tableFamilyChildren.getEditorComponent().requestFocus();
								// } else {
								Individual selected = ((FamilyChildrenModel) FamiliesPanel.this.tableFamilyChildren.getModel()).children().get(selectedIndex);
								if (selected != null) {
									FamiliesPanel.this.netGUI.changeSegmentationToCluster(selected);
								}
							}
						}
					}
				} catch (final PuckException exception) {
					exception.printStackTrace();
				}
			}
		});
		scrollPane.setViewportView(this.tableFamilyChildren);
		this.tableFamilyChildren.setBorder(new LineBorder(new Color(169, 169, 169)));
		this.tableFamilyChildren.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		this.tableFamilyChildren.getTableHeader().setReorderingAllowed(false);
		this.tableFamilyChildren.setModel(new FamilyChildrenModel(this.netGUI, null));

		JPopupMenu popupMenu_2 = new JPopupMenu();

		JMenuItem mntmDelete_2 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("FamiliesPanel.mntmDelete_2.text_1")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmDelete_2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Delete family children.
				logger.debug("Delete " + ArrayUtils.toString(FamiliesPanel.this.tableFamilyChildren.getSelectedRows()));

				int[] selectedRowIds = FamiliesPanel.this.tableFamilyChildren.getSelectedRows();
				if (selectedRowIds.length != 0) {
					ArrayUtils.reverse(selectedRowIds);
					for (int rowIndex : selectedRowIds) {
						int childId = (Integer) FamiliesPanel.this.tableFamilyChildren.getModel().getValueAt(rowIndex, 0);
						FamiliesPanel.this.netGUI.getNet().removeChild(FamiliesPanel.this.netGUI.getNet().individuals().getById(childId));
					}

					//
					FamiliesPanel.this.netGUI.setChanged(true);
					FamiliesPanel.this.netGUI.updateAll();
				}
			}
		});

		JMenuItem mntmBrowse_2 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("FamiliesPanel.mntmBrowse_2.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmBrowse_2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Browse Family child.
				try {
					int selectedIndex = FamiliesPanel.this.tableFamilyChildren.getSelectedRow();
					int selectedColumn = FamiliesPanel.this.tableFamilyChildren.getSelectedColumn();
					logger.debug("[index= " + selectedIndex + ", col]=[" + selectedColumn + "]");
					if (selectedIndex != -1) {
						if (selectedColumn > 0) {
							Individual selected = ((FamilyChildrenModel) FamiliesPanel.this.tableFamilyChildren.getModel()).children().get(selectedIndex);
							if (selected != null) {
								FamiliesPanel.this.netGUI.changeSegmentationToCluster(selected);
							}
						}
					}
				} catch (final PuckException exception) {
					exception.printStackTrace();
				}
			}
		});
		popupMenu_2.add(mntmBrowse_2);

		JSeparator separator_2 = new JSeparator();
		popupMenu_2.add(separator_2);
		popupMenu_2.add(mntmDelete_2);
		addPopup(this.tableFamilyChildren, popupMenu_2);

		this.attributesPanel = new AttributesPanel(this.netGUI, null, this.attributeTemplates);
		splitPane.setRightComponent(this.attributesPanel);

		//
		this.cmbbxFamilyChildrenIds = new AutoComboBox(null);
		this.cmbbxFamilyChildrenIds.getEditor().getEditorComponent().addFocusListener(new FocusListener() {
			@Override
			public void focusGained(final FocusEvent arg0) {
			}

			@Override
			public void focusLost(final FocusEvent arg0) {
				logger.debug("FOCUS LOST FAMILY CHILDREN TABLE " + FamiliesPanel.this.tableFamilyChildren.isEditing());
				if (FamiliesPanel.this.tableFamilyChildren.isEditing()) {
					FamiliesPanel.this.tableFamilyChildren.getColumnModel().getColumn(0).getCellEditor().cancelCellEditing();
				}

				if (((FamilyChildrenModel) FamiliesPanel.this.tableFamilyChildren.getModel()).isNewEditionOn()) {
					((FamilyChildrenModel) FamiliesPanel.this.tableFamilyChildren.getModel()).escapeNewEdition();
				}
			}
		});
		this.cmbbxFamilyChildrenIds.setEditable(true);
		this.cmbbxFamilyChildrenIds.setStrict(false);
		this.cmbbxFamilyChildrenIds.setMaximumRowCount(7);
		TableCellEditor editor = new DefaultCellEditor(this.cmbbxFamilyChildrenIds);
		this.tableFamilyChildren.getColumnModel().getColumn(0).setCellEditor(editor);
		this.tableFamilyChildren.getColumnModel().getColumn(0).setMinWidth(10);
		this.tableFamilyChildren.getColumnModel().getColumn(0).setPreferredWidth(90);
		this.tableFamilyChildren.getColumnModel().getColumn(0).setMaxWidth(110);

		this.tableFamilyChildren.getColumnModel().getColumn(1).setMinWidth(10);
		this.tableFamilyChildren.getColumnModel().getColumn(1).setMaxWidth(30);
		this.tableFamilyChildren.getColumnModel().getColumn(1).setResizable(false);

		List<Integer> orderList = new ArrayList<Integer>(99);
		for (int order = 1; order < 100; order++) {
			orderList.add(order);
		}
		this.cmbbxIndividualChildrenOrders = new AutoComboBox(orderList);
		this.cmbbxIndividualChildrenOrders.getEditor().getEditorComponent().addFocusListener(new FocusListener() {
			@Override
			public void focusGained(final FocusEvent arg0) {
			}

			@Override
			public void focusLost(final FocusEvent arg0) {
				logger.debug("FOCUS LOST CHILDREN TABLE(2) " + FamiliesPanel.this.tableFamilyChildren.isEditing());
				if (FamiliesPanel.this.tableFamilyChildren.isEditing()) {
					FamiliesPanel.this.tableFamilyChildren.getColumnModel().getColumn(3).getCellEditor().cancelCellEditing();
				}

				if (((FamilyChildrenModel) FamiliesPanel.this.tableFamilyChildren.getModel()).isNewEditionOn()) {
					((FamilyChildrenModel) FamiliesPanel.this.tableFamilyChildren.getModel()).escapeNewEdition();
				}
			}
		});
		this.cmbbxIndividualChildrenOrders.setEditable(true);
		this.cmbbxIndividualChildrenOrders.setStrict(false);
		this.cmbbxIndividualChildrenOrders.setMaximumRowCount(10);
		TableCellEditor orderEditor = new DefaultCellEditor(this.cmbbxIndividualChildrenOrders);
		this.tableFamilyChildren.getColumnModel().getColumn(3).setCellEditor(orderEditor);
		this.tableFamilyChildren.getColumnModel().getColumn(3).setPreferredWidth(50);
		this.tableFamilyChildren.getColumnModel().getColumn(3).setMaxWidth(50);
		this.tableFamilyChildren.getColumnModel().getColumn(3).setResizable(false);

		this.tableFamilyChildren.getColumnModel().getColumn(4).setMaxWidth(125);
		this.tableFamilyChildren.getColumnModel().getColumn(4).setResizable(false);

		this.tableFamilyChildren.getColumnModel().getColumn(5).setMaxWidth(125);
		this.tableFamilyChildren.getColumnModel().getColumn(5).setResizable(false);

		JPanel familiesButtonsPanel = new JPanel();
		add(familiesButtonsPanel);
		familiesButtonsPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		familiesButtonsPanel.setLayout(new BoxLayout(familiesButtonsPanel, BoxLayout.X_AXIS));

		JButton btnRemoveFamily = new JButton("");
		btnRemoveFamily.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Remove family.
				Family target = getSelectedFamily();
				if (target != null) {
					//
					int response = JOptionPane.showConfirmDialog(FamiliesPanel.this.thisJPanel, "Do you really want to delete family nr. " + target.getId()
							+ "?", "Family delete confirm", JOptionPane.YES_NO_OPTION);

					if (response == JOptionPane.YES_OPTION) {
						//
						int selectedIndex = FamiliesPanel.this.familyList.getSelectedIndex();
						FamiliesPanel.this.netGUI.getNet().remove(target);
						update();
						if (FamiliesPanel.this.netGUI.getNet().families().size() != 0) {
							if (selectedIndex == FamiliesPanel.this.netGUI.getNet().families().size()) {
								selectedIndex -= 1;
							}
							FamiliesPanel.this.familyList.setSelectedIndex(selectedIndex);
						}
						FamiliesPanel.this.netGUI.setChanged(true);
					}
				}
			}
		});
		familiesButtonsPanel.add(btnRemoveFamily);
		btnRemoveFamily.setToolTipText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnRemoveFamily.toolTipText")); //$NON-NLS-1$ //$NON-NLS-2$
		btnRemoveFamily.setIcon(new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/remove.png")));

		JButton btnAddFamily = new JButton("");
		btnAddFamily.addActionListener(new ActionListener() {
			// Add Family button clicked.
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Add family.
				logger.debug("Add family");
				addFamily();
			}
		});
		btnAddFamily.setToolTipText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnAddFamily.toolTipText")); //$NON-NLS-1$ //$NON-NLS-2$
		btnAddFamily.setIcon(new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/add.png")));
		familiesButtonsPanel.add(btnAddFamily);

		Component horizontalGlue = Box.createHorizontalGlue();
		familiesButtonsPanel.add(horizontalGlue);

		JButton btnFamiliesPrevious = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnPrevious.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnFamiliesPrevious.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Previous family.
				int selectedIndex = FamiliesPanel.this.familyList.getSelectedIndex();
				if (selectedIndex == -1) {
					if (FamiliesPanel.this.netGUI.getNet().families().size() != 0) {
						selectedIndex = FamiliesPanel.this.netGUI.getNet().families().size() - 1;
						FamiliesPanel.this.familyList.setSelectedIndex(selectedIndex);
						FamiliesPanel.this.familyList.ensureIndexIsVisible(selectedIndex);
					}
				} else if (selectedIndex > 0) {
					selectedIndex -= 1;
					FamiliesPanel.this.familyList.setSelectedIndex(selectedIndex);
					FamiliesPanel.this.familyList.ensureIndexIsVisible(selectedIndex);
				}
			}
		});
		familiesButtonsPanel.add(btnFamiliesPrevious);

		JButton btnFamiliesNext = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnNext.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnFamiliesNext.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Next family.
				int selectedIndex = FamiliesPanel.this.familyList.getSelectedIndex();
				if (selectedIndex == -1) {
					if (FamiliesPanel.this.netGUI.getNet().families().size() != 0) {
						selectedIndex = 0;
						FamiliesPanel.this.familyList.setSelectedIndex(selectedIndex);
						FamiliesPanel.this.familyList.ensureIndexIsVisible(selectedIndex);
					}
				} else if (selectedIndex < FamiliesPanel.this.netGUI.getNet().families().size() - 1) {
					selectedIndex += 1;
					FamiliesPanel.this.familyList.setSelectedIndex(selectedIndex);
					FamiliesPanel.this.familyList.ensureIndexIsVisible(selectedIndex);
				}
			}
		});
		familiesButtonsPanel.add(btnFamiliesNext);

		Component horizontalGlue_4 = Box.createHorizontalGlue();
		familiesButtonsPanel.add(horizontalGlue_4);

		JButton btnRta = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("FamiliesPanel.btnRta.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnRta.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// RTA : Reset Templated Attributes.
				updateAttributeTemplates();
			}
		});
		btnRta.setToolTipText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("FamiliesPanel.btnRta.toolTipText")); //$NON-NLS-1$ //$NON-NLS-2$
		familiesButtonsPanel.add(btnRta);

		Component horizontalGlue_1 = Box.createHorizontalGlue();
		familiesButtonsPanel.add(horizontalGlue_1);

		JLabel lblFamiliesSearch = new JLabel(" ");
		lblFamiliesSearch.setIcon(new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/find.png")));
		lblFamiliesSearch.setToolTipText("Search: ");
		lblFamiliesSearch.setMinimumSize(new Dimension(300, 15));
		familiesButtonsPanel.add(lblFamiliesSearch);

		this.txtfldSearchFamily = new JTextField();
		this.txtfldSearchFamily.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Search family.
				String pattern = FamiliesPanel.this.txtfldSearchFamily.getText();
				logger.debug("Search family=[" + pattern + "]");
				if (StringUtils.isNotBlank(pattern)) {
					int index = ((FamiliesModel) FamiliesPanel.this.familyList.getModel()).nextSearchedFamilyIndex(pattern);
					if (index != -1) {
						FamiliesPanel.this.familyList.setSelectedIndex(index);
						FamiliesPanel.this.familyList.ensureIndexIsVisible(index);
					}
				}
			}
		});
		this.txtfldSearchFamily.setText("");
		this.txtfldSearchFamily.setMaximumSize(new Dimension(500, 50));
		this.txtfldSearchFamily.setColumns(15);
		familiesButtonsPanel.add(this.txtfldSearchFamily);
	}

	/**
	 * 
	 */
	public void addChild() {
		// Add child.
		if (((FamilyChildrenModel) this.tableFamilyChildren.getModel()).getSource() != null) {
			if (!((FamilyChildrenModel) this.tableFamilyChildren.getModel()).isNewEditionOn()) {
				//
				((FamilyChildrenModel) this.tableFamilyChildren.getModel()).setNewItem();

				//
				this.tableFamilyChildren.scrollRectToVisible(this.tableFamilyChildren.getCellRect(this.tableFamilyChildren.getModel().getRowCount() - 1, 0,
						true));
				this.tableFamilyChildren.setRowSelectionInterval(this.tableFamilyChildren.getModel().getRowCount() - 1, this.tableFamilyChildren.getModel()
						.getRowCount() - 1);
				this.tableFamilyChildren.editCellAt(this.tableFamilyChildren.getModel().getRowCount() - 1, 0);
				this.tableFamilyChildren.getEditorComponent().requestFocus();
			}
		}
	}

	/**
	 * 
	 */
	public void addFamily() {
		try {
			Family newFamily = this.netGUI.getNet().createFamily(IdStrategy.APPEND, null, null);

			this.netGUI.setChanged(true);
			this.netGUI.updateAll();
			this.netGUI.selectFamiliesTab(newFamily);

			setParent1EditorOn();

		} catch (final Exception exception) {
			// Show trace.
			exception.printStackTrace();

			//
			String title = "Error computerum est";
			String message = "Error occured during working: " + exception.getMessage();

			//
			JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * 
	 * @return
	 */
	public Family getIdentityFamily() {
		Family result;

		if (NumberUtils.isDigits(this.lblFamilyId.getText())) {
			result = this.netGUI.getNet().families().getById(Integer.parseInt(this.lblFamilyId.getText()));
		} else {
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individual getIdentityParent1() {
		Individual result;

		if (NumberUtils.isDigits(this.lblFamilyFatherId.getText())) {
			result = this.netGUI.getNet().individuals().getById(Integer.parseInt(this.lblFamilyFatherId.getText()));
		} else {
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individual getIdentityParent2() {
		Individual result;

		if (NumberUtils.isDigits(this.lblFamilyMotherId.getText())) {
			result = this.netGUI.getNet().individuals().getById(Integer.parseInt(this.lblFamilyMotherId.getText()));
		} else {
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Family getSelectedFamily() {
		Family result;

		if (this.familyList.getSelectedIndex() == -1) {
			result = null;
		} else {
			result = (Family) ((FamiliesModel) this.familyList.getModel()).getElementAt(this.familyList.getSelectedIndex());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param family
	 */
	public void select(final Family family) {
		int familyIndex = ((FamiliesModel) this.familyList.getModel()).indexOf(family);
		if ((familyIndex >= 0) && (familyIndex < ((FamiliesModel) this.familyList.getModel()).getSize())) {
			this.familyList.setSelectedIndex(familyIndex);
			this.familyList.ensureIndexIsVisible(familyIndex);
		} else if (((FamiliesModel) this.familyList.getModel()).getSize() != 0) {
			this.familyList.setSelectedIndex(0);
			this.familyList.ensureIndexIsVisible(0);
		} else {
			updateFamilyIdentity(null);
		}
	}

	/**
	 * 
	 * @param target
	 */
	public void selectByIndex(final int familyIndex) {

		if ((familyIndex >= 0) && (familyIndex < ((FamiliesModel) this.familyList.getModel()).getSize())) {
			this.familyList.setSelectedIndex(familyIndex);
			this.familyList.ensureIndexIsVisible(familyIndex);
		} else if (((FamiliesModel) this.familyList.getModel()).getSize() != 0) {
			this.familyList.setSelectedIndex(0);
			this.familyList.ensureIndexIsVisible(0);
		} else {
			updateFamilyIdentity(null);
		}
	}

	/**
	 * 
	 */
	private void setFamilyIdEditorOn() {
		//
		logger.debug("Family id clicked");

		this.txtFldFamilyId.setText(this.lblFamilyId.getText());

		//
		this.lblFamilyId.setVisible(false);
		this.txtFldFamilyId.setVisible(true);

		//
		this.txtFldFamilyId.requestFocus();
	}

	/**
	 * 
	 */
	public void setFocusOnFind() {
		this.txtfldSearchFamily.requestFocus();
	}

	/**
	 * 
	 */
	private void setParent1EditorOn() {
		// Family parent 1 id clicked.
		Individual currentParent = getIdentityParent1();

		// Populate combobox.
		List<Individual> individuals = this.netGUI.getNet().individuals().toSortedList();
		List<String> items = new ArrayList<String>(individuals.size());
		for (Individual individual : individuals) {
			items.add(individual.getId() + " " + individual.getName());
		}
		this.cmbbxFamilyParent1.setDataList(items);

		// Set combobox selected item.
		if (currentParent == null) {
			this.cmbbxFamilyParent1.setSelectedIndex(-1);
		} else {
			int currentItemIndex = ToolBox.indexOf(currentParent.getId() + " " + currentParent.getName(), items);
			this.cmbbxFamilyParent1.setSelectedIndex(currentItemIndex);
		}

		//
		this.lblFamilyFatherId.setVisible(false);
		this.lblFamilyFatherName.setVisible(false);

		//
		this.cmbbxFamilyParent1.setVisible(true);

		//
		this.cmbbxFamilyParent1.requestFocus();
	}

	/**
	 * 
	 */
	private void setParent2EditorOn() {
		// Family parent 2 id clicked.
		Individual currentParent2 = getIdentityParent2();

		//
		List<Individual> individuals = this.netGUI.getNet().individuals().toSortedList();
		List<String> items = new ArrayList<String>(individuals.size());
		for (Individual individual : individuals) {
			items.add(individual.getId() + " " + individual.getName());
		}
		this.cmbbxFamilyParent2.setDataList(items);

		// Set combobox selected item.
		if (currentParent2 == null) {
			this.cmbbxFamilyParent2.setSelectedIndex(-1);
		} else {
			int currentItemIndex = ToolBox.indexOf(currentParent2.getId() + " " + currentParent2.getName(), items);
			this.cmbbxFamilyParent2.setSelectedIndex(currentItemIndex);
		}

		//
		this.lblFamilyMotherId.setVisible(false);
		this.lblFamilyMotherName.setVisible(false);

		//
		this.cmbbxFamilyParent2.setVisible(true);

		//
		this.cmbbxFamilyParent2.requestFocus();
	}

	/**
	 * 
	 */
	public void update() {
		//
		int selectedFamily = this.familyList.getSelectedIndex();

		//
		((FamiliesModel) this.familyList.getModel()).setSource(this.netGUI.getCurrentFamilies());

		//
		selectByIndex(selectedFamily);
	}

	/**
	 * 
	 */
	public void updateAttributeTemplates() {
		//
		this.attributeTemplates.clear();
		if ((this.netGUI != null) && (this.netGUI.getNet() != null)) {
			//
			List<String> templates = AttributeWorker.getExogenousAttributeDescriptors(this.netGUI.getNet().families(), null).labels();

			//
			this.attributeTemplates.addAll(templates);
		}
	}

	/**
	 * 
	 */
	public void updateFamilyIdentity(final Family source) {
		if (source == null) {
			logger.debug("updateFamilyIdentity null");
		} else {
			logger.debug("updateFamilyIdentity " + source.getId());
		}

		if (source == null) {
			//
			this.lblFamilyId.setText("---");
			this.txtFldFamilyId.setVisible(false);
			this.lblFamilyStatus.setIcon(largeUnknowIcon);
			this.lblPosition.setText("(--/--)");

			//
			this.lblFamilyFatherId.setText("---");
			this.lblFamilyFatherName.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.individual.unknown"));
			this.lblFamilyFatherName.setIcon(mediumUnknowIcon);

			//
			this.lblFamilyMotherId.setText("---");
			this.lblFamilyMotherName.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.individual.unknown"));
			this.lblFamilyMotherName.setIcon(mediumUnknowIcon);

			//
			this.lblFamilyChildren.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.lblChildren.text"));
			((FamilyChildrenModel) this.tableFamilyChildren.getModel()).setSource(null);

			//
			this.cmbbxFamilyChildrenIds.setDataList(new ArrayList<String>(0));

			//
			this.attributesPanel.setSource(null);

			//
			this.btnAddChild.setEnabled(false);
		} else {
			//
			this.lblFamilyId.setText(String.valueOf(source.getId()));
			this.txtFldFamilyId.setVisible(false);
			switch (source.getUnionStatus()) {
				case UNMARRIED:
					this.lblFamilyStatus.setIcon(largeUnmarriedIcon);
				break;
				case MARRIED:
					this.lblFamilyStatus.setIcon(largeMarriedIcon);
				break;
				case DIVORCED:
					this.lblFamilyStatus.setIcon(largeDivorcedIcon);
				break;
				default:
					this.lblFamilyStatus.setIcon(largeUnknowIcon);
			}
			this.lblPosition.setText("(" + (this.familyList.getSelectedIndex() + 1) + "/" + this.familyList.getModel().getSize() + ")");

			//
			if (source.getFather() == null) {
				//
				this.lblFamilyFatherId.setText("---");
				this.lblFamilyFatherName.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.individual.unknown"));
				this.lblFamilyFatherName.setIcon(mediumUnknowIcon);

			} else {
				//
				this.lblFamilyFatherId.setText(String.valueOf(source.getFather().getId()));
				this.lblFamilyFatherName.setText(source.getFather().getName());
				switch (source.getFather().getGender()) {
					case FEMALE:
						this.lblFamilyFatherName.setIcon(mediumFemaleIcon);
					break;
					case MALE:
						this.lblFamilyFatherName.setIcon(mediumMaleIcon);
					break;
					case UNKNOWN:
						this.lblFamilyFatherName.setIcon(mediumUnknowIcon);
					break;
				}
			}

			//
			if (source.getMother() == null) {
				//
				this.lblFamilyMotherId.setText("---");
				this.lblFamilyMotherName.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.individual.unknown"));
				this.lblFamilyMotherName.setIcon(mediumUnknowIcon);

			} else {
				//
				this.lblFamilyMotherId.setText(String.valueOf(source.getMother().getId()));
				this.lblFamilyMotherName.setText(source.getMother().getName());
				switch (source.getMother().getGender()) {
					case FEMALE:
						this.lblFamilyMotherName.setIcon(mediumFemaleIcon);
					break;
					case MALE:
						this.lblFamilyMotherName.setIcon(mediumMaleIcon);
					break;
					case UNKNOWN:
						this.lblFamilyMotherName.setIcon(mediumUnknowIcon);
					break;
				}
			}

			//
			((FamilyChildrenModel) this.tableFamilyChildren.getModel()).setSource(source);
			this.lblFamilyChildren.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.lblChildren.text") + " ("
					+ ((FamilyChildrenModel) this.tableFamilyChildren.getModel()).children().size() + ")");

			//
			this.cmbbxFamilyChildrenIds.setDataList(ComboBoxIds.instance().items());

			//
			this.attributesPanel.setSource(source.attributes());

			//
			int selectedIndex = this.familyList.getSelectedIndex();
			if (selectedIndex != -1) {
				Family selected = (Family) ((FamiliesModel) this.familyList.getModel()).getElementAt(selectedIndex);
				if (source != selected) {
					int sourceIndex = ((FamiliesModel) this.familyList.getModel()).indexOf(source);

					if (sourceIndex != -1) {
						this.familyList.setSelectedIndex(sourceIndex);
						this.familyList.ensureIndexIsVisible(sourceIndex);
					}
				}
			}

			//
			this.btnAddChild.setEnabled(true);
		}
	}

	private static void addPopup(final Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			@Override
			public void mouseReleased(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(final MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
