package org.tip.puckgui.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JMenuItem;

import org.tip.puck.PuckManager;
import org.tip.puckgui.NetGUI;

/**
 * 
 * @author TIP
 */
public class OpenRecentMenuItem extends JMenuItem {
	private static final long serialVersionUID = -6988304006803940278L;

	/**
	 * 
	 * @param window
	 * @param netGUI
	 * @param file
	 */
	public OpenRecentMenuItem(final JFrame window, final NetGUI netGUI, final File file) {
		super();
		setText(file.getName());
		if (!file.exists()) {
			setEnabled(false);
		}

		this.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				//
				MainWindow.openSelectedFile(netGUI, window, file, PuckManager.DEFAULT_CHARSET_NAME);
			}
		});
	}
}
