package org.tip.puck.net;

import java.util.Comparator;

import org.tip.puck.util.MathUtils;

/**
 * 
 * @author TIP
 * 
 */
public class FamilyComparator implements Comparator<Family> {

	public enum Sorting {
		ID,
		HUSBAND,
		HUSBAND_ORDER,
		WIFE,
		WIFE_ORDER
	}

	private Sorting sorting;

	/**
	 * 
	 * @param sorting
	 */
	public FamilyComparator(final Sorting sorting) {
		this.sorting = sorting;
	}

	/**
	 * 
	 */
	@Override
	public int compare(final Family alpha, final Family bravo) {
		int result;

		result = compare(alpha, bravo, this.sorting);

		//
		return result;
	}

	/**
	 * 
	 */
	public static int compare(final Family alpha, final Family bravo, final Sorting sorting) {
		int result;

		//
		switch (sorting) {
			case ID:
			default:
				//
				result = MathUtils.compare(getId(alpha), getId(bravo));
			break;

			case HUSBAND:
				//
				result = IndividualComparator.compare(getHusband(alpha), getHusband(bravo), IndividualComparator.Sorting.ID);

				if (result == 0) {
					result = IndividualComparator.compare(getWife(alpha), getWife(bravo), IndividualComparator.Sorting.ID);
				}
			break;

			case HUSBAND_ORDER:
				//
				result = MathUtils.compareOrder(getHusbandOrder(alpha), getHusbandOrder(bravo));
			break;

			case WIFE:
				//
				result = IndividualComparator.compare(getWife(alpha), getWife(bravo), IndividualComparator.Sorting.ID);

				if (result == 0) {
					result = IndividualComparator.compare(getHusband(alpha), getHusband(bravo), IndividualComparator.Sorting.ID);
				}
			break;

			case WIFE_ORDER:
				//
				result = MathUtils.compareOrder(getWifeOrder(alpha), getWifeOrder(bravo));
			break;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Individual getHusband(final Family source) {
		Individual result;

		//
		if (source == null) {
			//
			result = null;
		} else {
			//
			result = source.getHusband();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Integer getHusbandOrder(final Family source) {
		Integer result;

		//
		if (source == null) {
			//
			result = null;
		} else {
			//
			result = source.getHusbandOrder();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Integer getId(final Family source) {
		Integer result;

		//
		if (source == null) {
			//
			result = null;
		} else {
			//
			result = source.getId();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Individual getWife(final Family source) {
		Individual result;

		//
		if (source == null) {
			//
			result = null;
		} else {
			//
			result = source.getWife();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Integer getWifeOrder(final Family source) {
		Integer result;

		//
		if (source == null) {
			//
			result = null;
		} else {
			//
			result = source.getWifeOrder();
		}

		//
		return result;
	}
}
