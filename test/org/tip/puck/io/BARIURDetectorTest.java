package org.tip.puck.io;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckManager;
import org.tip.puck.io.BARIURDetector.Format;
import org.tip.puck.io.bar.BARFile;
import org.tip.puck.io.iur.IURFile;
import org.tip.puck.io.txt.TXTFileTest;
import org.tip.puck.net.Net;

/**
 * 
 * @author TIP
 */
public class BARIURDetectorTest {
	private static final Logger logger = LoggerFactory.getLogger(TXTFileTest.class);

	public static final String SIMPLE_FILE = "test/Resources/Chimane6.ods";
	public static final String COMPLEX_FILE = "test/Resources/Ebrei_08.ods";

	/**
     */
	@Test
	public void testBARIURDetector() throws Exception {

		logger.debug("========================== testLoad01");

		//
		File[] source = new File("/home/cpm/C/Puck/Corpus/BARIUR/Test/").listFiles();
		Arrays.sort(source);

		System.out.println("ID\tName                                       \tisBAR\tisIUR\tdetect      \tBARFile\tIURFile\tPuckM\tInd.\tUnions\tRela.");

		int index = 0;
		for (File file : source) {
			//
			if (StringUtils.endsWithAny(file.getName(), ".txt", ".xls", ".ods")) {
				// if ((file.getName().contains("Wilcan")) &&
				// (StringUtils.endsWithAny(file.getName(), ".txt", ".xls",
				// ".ods"))) {
				// if (file.getName().endsWith("Copper1922-BAR.xls")) {
				//
				index += 1;

				//
				if (index > 95) {
					break;
				}

				//
				System.out.print(index + "/" + (source.length / 2) + "\t");

				//
				System.out.print(file.getName());
				System.out.print(StringUtils.repeat(' ', 40 - file.getName().length()));
				// System.out.print("\t");

				//
				System.out.print("\t");
				System.out.print(BARFile.isBAR(file, PuckManager.DEFAULT_CHARSET_NAME));

				//
				System.out.print("\t");
				System.out.print(IURFile.isIUR(file, PuckManager.DEFAULT_CHARSET_NAME));

				//
				System.out.print("\t");
				BARIURDetector.Format format = BARIURDetector.detectFormat(file);
				System.out.print(format.toString() + StringUtils.repeat(' ', 15 - format.toString().length()));

				//
				System.out.print("\t");
				String result;
				try {
					BARFile.load(file, PuckManager.DEFAULT_CHARSET_NAME);
					result = "OK";
				} catch (Exception exception) {
					result = "EXC";
				}
				System.out.print(result);

				//
				System.out.print("\t");
				try {
					IURFile.load(file, PuckManager.DEFAULT_CHARSET_NAME);
					result = "OK";
				} catch (Exception exception) {
					result = "EXC";
				}
				System.out.print(result);

				//
				System.out.print("\t");
				String individualCount;
				String unionCount;
				String relationCount;
				try {
					Net net = PuckManager.loadNet(file);
					individualCount = String.valueOf(net.individuals().size());
					unionCount = String.valueOf(net.families().size());
					relationCount = String.valueOf(net.relations().size());
					result = "OK";
				} catch (Exception exception) {
					result = "EXC";
					individualCount = "n/a";
					unionCount = "n/a";
					relationCount = "n/a";
				}
				System.out.print(result + "\t" + individualCount + "\t" + unionCount + "\t" + relationCount);

				//
				System.out.println("");
			}
		}

		System.out.println("OVER");
	}

	/**
     */
	@Test
	public void testBARIURDetector00() throws Exception {

		logger.debug("========================== testBARIURDetector00");

		//
		{
			File file = new File("toto.BAR.txt");
			Assertions.assertThat(BARFile.isBAR(file)).isEqualTo(true);
			Assertions.assertThat(IURFile.isIUR(file)).isEqualTo(false);
			Assertions.assertThat(BARIURDetector.detectFormat(file)).isEqualTo(Format.BAR);
		}

		//
		{
			File file = new File("toto.Bar.txt");
			Assertions.assertThat(BARFile.isBAR(file)).isEqualTo(true);
			Assertions.assertThat(IURFile.isIUR(file)).isEqualTo(false);
			Assertions.assertThat(BARIURDetector.detectFormat(file)).isEqualTo(Format.BAR);
		}

		//
		{
			File file = new File("toto.bar.txt");
			Assertions.assertThat(BARFile.isBAR(file)).isEqualTo(true);
			Assertions.assertThat(IURFile.isIUR(file)).isEqualTo(false);
			Assertions.assertThat(BARIURDetector.detectFormat(file)).isEqualTo(Format.BAR);
		}

		//
		{
			File file = new File("toto.IUR.txt");
			Assertions.assertThat(BARFile.isBAR(file)).isEqualTo(false);
			Assertions.assertThat(IURFile.isIUR(file)).isEqualTo(true);
			Assertions.assertThat(BARIURDetector.detectFormat(file)).isEqualTo(Format.IUR);
		}

		//
		{
			File file = new File("toto.Iur.txt");
			Assertions.assertThat(BARFile.isBAR(file)).isEqualTo(false);
			Assertions.assertThat(IURFile.isIUR(file)).isEqualTo(true);
			Assertions.assertThat(BARIURDetector.detectFormat(file)).isEqualTo(Format.IUR);
		}

		//
		{
			File file = new File("toto.iur.txt");
			Assertions.assertThat(BARFile.isBAR(file)).isEqualTo(false);
			Assertions.assertThat(IURFile.isIUR(file)).isEqualTo(true);
			Assertions.assertThat(BARIURDetector.detectFormat(file)).isEqualTo(Format.IUR);
		}

		//
		{
			File file = new File("iur.txt");
			Assertions.assertThat(BARFile.isBAR(file)).isEqualTo(false);
			Assertions.assertThat(IURFile.isIUR(file)).isEqualTo(false);
			Assertions.assertThat(BARIURDetector.detectFormat(file)).isEqualTo(Format.UNKNOWN);
		}
	}
}
