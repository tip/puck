package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.census.workers.ChainValuator.ChainProperty;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.relations.Actors;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.RelationModels;
import org.tip.puck.net.workers.AttributeDescriptor.Scope;
import org.tip.puck.net.workers.AttributeDescriptors;
import org.tip.puck.net.workers.AttributeWorker;
import org.tip.puck.net.workers.ExpansionMode;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.workers.SequenceCriteria;
import org.tip.puck.sequences.workers.SequenceCriteria.EgoNetworksOperation;
import org.tip.puck.sequences.workers.SequenceCriteria.ParcoursNetworksOperation;
import org.tip.puck.sequences.workers.SequenceCriteria.SequenceGeneralStatistics;
import org.tip.puck.sequences.workers.SequenceCriteria.SequenceReportType;
import org.tip.puck.sequences.workers.SequenceCriteria.SliceGeneralStatistics;
import org.tip.puck.sequences.workers.SequenceCriteria.SliceReportType;
import org.tip.puck.sequences.workers.SequenceCriteria.TrajectoriesOperation;
import org.tip.puck.sequences.workers.SequenceCriteria.ValueSequenceLabel;
import org.tip.puckgui.PuckGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.StringList;
import fr.devinsy.util.StringSet;

/**
 * 
 * @author TIP
 */
public class SequenceAnalysisInputDialog extends JDialog {

	private static final long serialVersionUID = -7326870100552727961L;

	private static final Logger logger = LoggerFactory.getLogger(SequenceAnalysisInputDialog.class);

	private RelationModels relationModels;
	private AttributeDescriptors attributeDescriptors;
	private Segmentation segmentation;
	private SequenceCriteria dialogCriteria;

	private final JPanel contentPanel = new JPanel();
	private JComboBox cmbxRelationModel;
	private JComboBox cmbxEgoRole;
	private JComboBox cmbxDateLabel;
	private JSpinner spnrMaxAge;
	private JButton okButton;
	private JPanel alterRolesPanel;
	private JTextField txtfldDates;
	private JTextField txtfldMinimalPlaceNames;
	private JComboBox cmbxStartDateLabel;
	private JComboBox cmbxEndDateLabel;
	private JComboBox cmbxPlaceLabel;
	private JComboBox cmbxLocalUnitLabel;
	private JSpinner spnrMinAge;
	private JComboBox cmbxLevel;
	private JComboBox cmbxGeography;
	private JComboBox cmbxExpansionMode;
	private JComboBox cmbxFiliationType;
	private JComboBox cmbxStartPlaceLabel;
	private JComboBox cmbxEndPlaceLabel;
	private JComboBox cmbxDefaultReferentRoleName;
	private JPanel sequenceReportTypePanel;
	private JPanel sliceReportTypePanel;
	private JPanel circuitCensusCriteriaPanel;
	private JPanel alterRelationModelPanel;
	private JTextField txtfldPattern;
	private JComboBox cmbxChainClassification;
	private JTextField txtfldAlterFilterAttributeValue;
	private JTextField txtfldAlterFilterAttributeLabel;
	private JComboBox cmbxAlterFilterRole;
	private JComboBox cmbxIndividualClassificationType;
	private JPanel mainRelationClassificationTypePanel;
	private JPanel relationClassificationTypePanel;
	private JPanel trajectoriesClassificationTypePanel;
	private JPanel repotOptionsPanel;
	private JPanel sequenceGeneralStatisticsPanel;
	private JLabel lblMode;
	private JPanel sliceGeneralStatisticsPanel;
	private JPanel egoNetworksOperationPanel;
	private JScrollPane alterRolesScrollPane;
	private JPanel trajectoriesOperationsPanel;
	private JCheckBox chckbxTrajectoriesExportSequenceType;
	private JCheckBox chckbxTrajectoriesExportEvenType;
	private JCheckBox chckbxTrajectoriesListTrees;
	private JCheckBox chckbxTrajectoriesGeneral;
	private JCheckBox chckbxPNSGeneral;
	private JCheckBox chckbxPNSCentrality;
	private JCheckBox chckbxPNSCohesion;
	private JCheckBox chckbxPNSRelations;
	private JCheckBox chckbxPNSExportParcours;
	private JCheckBox chckbxPNSExportExtendedParcours;
	private JCheckBox chckbxPNSExportMultipleParcours;
	private JCheckBox chckbxTrajetoriesExportParcoursNetworks;
	private JCheckBox chckbxTrajectoriesExportSimilaryTrees;
	private JCheckBox chckbxTrajectoriesDraw;

	/**
	 * Create the dialog.
	 */
	public SequenceAnalysisInputDialog(final Segmentation segmentation, final RelationModels relationModels, final AttributeDescriptors attributeDescriptors) {
		super();

		this.segmentation = segmentation;
		this.relationModels = relationModels;
		this.attributeDescriptors = attributeDescriptors;

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Sequence Analysis Input Dialog");
		setIconImage(Toolkit.getDefaultToolkit().getImage(SequenceAnalysisInputDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			/**
			 * 
			 */
			@Override
			public void windowClosing(final WindowEvent event) {
				// Closing window.
				// Cancel button.
				SequenceAnalysisInputDialog.this.dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 1150, 785);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(new ColumnSpec[] { ColumnSpec.decode("max(175dlu;default)"), FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(100dlu;default)"), FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, }));
		{
		}
		{
			JPanel panel = new JPanel();
			this.contentPanel.add(panel, "1, 2, fill, fill");
			panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
					ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, }));
			{
				JLabel lblRelationModel = new JLabel("Relation Model:");
				panel.add(lblRelationModel, "2, 1");
			}
			this.cmbxRelationModel = new JComboBox(relationModels.nameList().sort().toArray());
			panel.add(this.cmbxRelationModel, "4, 1");
			this.cmbxRelationModel.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(final ItemEvent event) {
					// Update label list.
					//
					if (event.getStateChange() == ItemEvent.SELECTED) {
						//
						updateRelationModel();
					}
				}
			});
		}
		{
			this.lblMode = new JLabel("XXXX");
			this.contentPanel.add(this.lblMode, "3, 2, center, default");
		}
		{
			JPanel constructionOptionsPanel = new JPanel();
			constructionOptionsPanel.setBorder(new TitledBorder(null, "Constructions Options", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			this.contentPanel.add(constructionOptionsPanel, "1, 4, fill, fill");
			constructionOptionsPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
					FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
					FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("max(50dlu;default)"),
					FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC, }));
			{
				JLabel lblDateLabel = new JLabel("Date Label:");
				constructionOptionsPanel.add(lblDateLabel, "2, 2, right, default");
			}
			{
				this.cmbxDateLabel = new JComboBox();
				constructionOptionsPanel.add(this.cmbxDateLabel, "4, 2");
			}
			{
				JLabel lblStartDateLabel = new JLabel("Start Date Label:");
				constructionOptionsPanel.add(lblStartDateLabel, "2, 4, right, default");
			}
			{
				this.cmbxStartDateLabel = new JComboBox();
				constructionOptionsPanel.add(this.cmbxStartDateLabel, "4, 4");
			}
			{
				JLabel lblEndDateLabel = new JLabel("End Date Label:");
				constructionOptionsPanel.add(lblEndDateLabel, "2, 6, right, default");
			}
			{
				this.cmbxEndDateLabel = new JComboBox();
				constructionOptionsPanel.add(this.cmbxEndDateLabel, "4, 6");
			}
			{
				JLabel lblPlaceLabel = new JLabel("Place Label:");
				constructionOptionsPanel.add(lblPlaceLabel, "2, 8, right, default");
			}
			{
				this.cmbxPlaceLabel = new JComboBox();
				constructionOptionsPanel.add(this.cmbxPlaceLabel, "4, 8");
			}
			{
				JLabel lblStartPlaceLabel = new JLabel("Start Place Label:");
				constructionOptionsPanel.add(lblStartPlaceLabel, "2, 10, right, default");
			}
			{
				this.cmbxStartPlaceLabel = new JComboBox();
				constructionOptionsPanel.add(this.cmbxStartPlaceLabel, "4, 10, fill, default");
			}
			{
				JLabel lblEndPlaceLabel = new JLabel("End Place Label:");
				constructionOptionsPanel.add(lblEndPlaceLabel, "2, 12, right, default");
			}
			{
				this.cmbxEndPlaceLabel = new JComboBox();
				constructionOptionsPanel.add(this.cmbxEndPlaceLabel, "4, 12, fill, default");
			}
			{
				JLabel lblLocalUnitLabel = new JLabel("Local Unit Label:");
				constructionOptionsPanel.add(lblLocalUnitLabel, "2, 14, right, default");
			}
			{
				this.cmbxLocalUnitLabel = new JComboBox();
				this.cmbxLocalUnitLabel.addItemListener(new ItemListener() {
					/**
					 * 
					 */
					@Override
					public void itemStateChanged(final ItemEvent event) {
						//
						if (event.getStateChange() == ItemEvent.SELECTED) {
							//
							updateSequenceSliceMode();
						}
					}
				});
				constructionOptionsPanel.add(this.cmbxLocalUnitLabel, "4, 14");
			}
			{
				JLabel lblEgoRole = new JLabel("Ego Role:");
				constructionOptionsPanel.add(lblEgoRole, "2, 16, right, default");
			}
			{
				this.cmbxEgoRole = new JComboBox();
				constructionOptionsPanel.add(this.cmbxEgoRole, "4, 16");
			}
			{
				JLabel lblDefaultReferentRole = new JLabel("<html><div style=\"text-align: right;\">Default Referent<br/>Role Name</div></html>");
				constructionOptionsPanel.add(lblDefaultReferentRole, "2, 18, right, default");
			}
			{
				this.cmbxDefaultReferentRoleName = new JComboBox();
				constructionOptionsPanel.add(this.cmbxDefaultReferentRoleName, "4, 18");
			}
			{
				JLabel lblAlterRoles = new JLabel("Alter Roles:");
				constructionOptionsPanel.add(lblAlterRoles, "2, 20, right, top");
			}
			{
				this.alterRolesScrollPane = new JScrollPane();
				this.alterRolesScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
				constructionOptionsPanel.add(this.alterRolesScrollPane, "4, 20, fill, fill");
				{
					this.alterRolesPanel = new JPanel();
					this.alterRolesScrollPane.setViewportView(this.alterRolesPanel);
					this.alterRolesPanel.setLayout(new BoxLayout(this.alterRolesPanel, BoxLayout.Y_AXIS));
				}
			}
			{
				JLabel lblDates = new JLabel("Dates:");
				constructionOptionsPanel.add(lblDates, "2, 22, right, default");
			}
			{
				this.txtfldDates = new JTextField();
				this.txtfldDates.addFocusListener(new FocusAdapter() {
					/**
					 * 
					 */
					@Override
					public void focusLost(final FocusEvent event) {
						//
						logger.debug("Focus lost on dates.");
						updateSequenceSliceMode();
					}
				});
				constructionOptionsPanel.add(this.txtfldDates, "4, 22");
				this.txtfldDates.setColumns(10);
			}
			{
				JLabel lblGeography = new JLabel("Geography:");
				constructionOptionsPanel.add(lblGeography, "2, 24, right, default");
			}
/*			{
				this.cmbxGeography = new JComboBox();
				this.cmbxGeography.setModel(new DefaultComboBoxModel(new String[] { "Togo" }));
				constructionOptionsPanel.add(this.cmbxGeography, "4, 24");
			}*/
			{
				JLabel lblLevel = new JLabel("Level:");
				constructionOptionsPanel.add(lblLevel, "2, 26, right, default");
			}
			{
				this.cmbxLevel = new JComboBox();
				this.cmbxLevel.setModel(new DefaultComboBoxModel(GeoLevel.values()));
				constructionOptionsPanel.add(this.cmbxLevel, "4, 26");
			}
			{
				JLabel lblMinimalPlaceNames = new JLabel("Minimal Place Names:");
				constructionOptionsPanel.add(lblMinimalPlaceNames, "2, 28, right, default");
			}
			{
				this.txtfldMinimalPlaceNames = new JTextField();
				constructionOptionsPanel.add(this.txtfldMinimalPlaceNames, "4, 28");
				this.txtfldMinimalPlaceNames.setColumns(10);
			}
			{
				JLabel lblMinAge = new JLabel("Min Age (>=):");
				constructionOptionsPanel.add(lblMinAge, "2, 30, right, default");
			}
			{
				this.spnrMinAge = new JSpinner();
				constructionOptionsPanel.add(this.spnrMinAge, "4, 30");
				this.spnrMinAge.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
				((JSpinner.DefaultEditor) this.spnrMinAge.getEditor()).getTextField().setHorizontalAlignment(JTextField.RIGHT);
			}
			{
				JLabel lblMaxAge = new JLabel("Max Age (<):");
				constructionOptionsPanel.add(lblMaxAge, "2, 32, right, default");
			}
			{
				this.spnrMaxAge = new JSpinner();
				constructionOptionsPanel.add(this.spnrMaxAge, "4, 32");
				this.spnrMaxAge.setModel(new SpinnerNumberModel(new Integer(1000), new Integer(0), null, new Integer(1)));
				((JSpinner.DefaultEditor) this.spnrMaxAge.getEditor()).getTextField().setHorizontalAlignment(JTextField.RIGHT);
			}
			{
				JLabel lblExpansionMode = new JLabel("Expansion Mode:");
				constructionOptionsPanel.add(lblExpansionMode, "2, 34, right, default");
			}
			{
				this.cmbxExpansionMode = new JComboBox();
				this.cmbxExpansionMode.setModel(new DefaultComboBoxModel(ExpansionMode.values()));
				constructionOptionsPanel.add(this.cmbxExpansionMode, "4, 34");
			}
			{
				JLabel lblFiliationType = new JLabel("Filiation Type:");
				constructionOptionsPanel.add(lblFiliationType, "2, 36, right, default");
			}
			{
				this.cmbxFiliationType = new JComboBox();
				this.cmbxFiliationType.setModel(new DefaultComboBoxModel(FiliationType.values()));
				constructionOptionsPanel.add(this.cmbxFiliationType, "4, 36");
			}
		}
		{
			JPanel column2Panel = new JPanel();
			this.contentPanel.add(column2Panel, "3, 4, fill, fill");
			column2Panel.setLayout(new FormLayout(new ColumnSpec[] { ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), }));
			{
				this.repotOptionsPanel = new JPanel();
				column2Panel.add(this.repotOptionsPanel, "1, 1");
				this.repotOptionsPanel.setBorder(new TitledBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "", TitledBorder.LEADING,
						TitledBorder.TOP, null, new Color(51, 51, 51)), "Report Options", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
				this.repotOptionsPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] { RowSpec.decode("default:grow"),
						FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), }));
				{
					JLabel lblSequenceReportType = new JLabel("<html><div style=\"text-align: right;\">Sequence<br/>Report Type:</div></html>");
					this.repotOptionsPanel.add(lblSequenceReportType, "2, 1, right, top");
				}
				{
					this.sequenceReportTypePanel = new JPanel();
					this.repotOptionsPanel.add(this.sequenceReportTypePanel, "4, 1, fill, fill");
					this.sequenceReportTypePanel.setLayout(new BoxLayout(this.sequenceReportTypePanel, BoxLayout.Y_AXIS));

					for (SequenceReportType type : SequenceReportType.values()) {
						//
						JCheckBox chckbx = new JCheckBox(translate(type));
						this.sequenceReportTypePanel.add(chckbx);
					}
				}
				{
					JLabel lblSliceReportType = new JLabel("<html><div style=\"text-align: right\">Slice Report<br/>Type:</div></html>");
					this.repotOptionsPanel.add(lblSliceReportType, "2, 3, right, top");
				}
				{
					this.sliceReportTypePanel = new JPanel();
					this.repotOptionsPanel.add(this.sliceReportTypePanel, "4, 3, fill, fill");
					this.sliceReportTypePanel.setLayout(new BoxLayout(this.sliceReportTypePanel, BoxLayout.Y_AXIS));
					for (SliceReportType type : SliceReportType.values()) {
						//
						JCheckBox chckbx = new JCheckBox(translate(type));
						this.sliceReportTypePanel.add(chckbx);
					}
				}
			}
			{
				this.circuitCensusCriteriaPanel = new JPanel();
				this.circuitCensusCriteriaPanel.setBorder(new TitledBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "", TitledBorder.LEADING,
						TitledBorder.TOP, null, new Color(51, 51, 51)), "Circuit Census Criteria", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51,
						51, 51)));
				column2Panel.add(this.circuitCensusCriteriaPanel, "1, 3, fill, fill");
				this.circuitCensusCriteriaPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), },
						new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
								FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("min(40dlu;default)"), FormFactory.RELATED_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC, }));
				{
					JLabel lblCensysType = new JLabel("Pattern:");
					this.circuitCensusCriteriaPanel.add(lblCensysType, "4, 1, right, default");
				}
				{
					this.txtfldPattern = new JTextField();
					this.circuitCensusCriteriaPanel.add(this.txtfldPattern, "6, 1, fill, default");
					this.txtfldPattern.setColumns(10);
				}
				{
					JLabel lblChaineClassification = new JLabel("Chain Classification:");
					this.circuitCensusCriteriaPanel.add(lblChaineClassification, "4, 3, right, default");
				}
				{
					this.cmbxChainClassification = new JComboBox(new DefaultComboBoxModel(ChainProperty.values()));
					this.circuitCensusCriteriaPanel.add(this.cmbxChainClassification, "6, 3, fill, default");
				}
				{
					JLabel lblRelationModels = new JLabel("Alter Relation Models:");
					this.circuitCensusCriteriaPanel.add(lblRelationModels, "4, 5, right, top");
				}
				{
					JScrollPane scrollPane = new JScrollPane();
					scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
					this.circuitCensusCriteriaPanel.add(scrollPane, "6, 5, fill, fill");
					{
						this.alterRelationModelPanel = new JPanel();
						scrollPane.setViewportView(this.alterRelationModelPanel);
						this.alterRelationModelPanel.setLayout(new BoxLayout(this.alterRelationModelPanel, BoxLayout.Y_AXIS));
					}
				}
				{
					JLabel lblAlterfilterrolename = new JLabel("Alter Filter Role:");
					this.circuitCensusCriteriaPanel.add(lblAlterfilterrolename, "4, 7, right, default");
				}
				{
					this.cmbxAlterFilterRole = new JComboBox();
					this.circuitCensusCriteriaPanel.add(this.cmbxAlterFilterRole, "6, 7, fill, default");
				}
				{
					JLabel lblAlterFilterAttribute = new JLabel("<html>Alter Filter<br/>Attribute Label:</html>");
					this.circuitCensusCriteriaPanel.add(lblAlterFilterAttribute, "4, 9, right, default");
				}
				{
					this.txtfldAlterFilterAttributeLabel = new JTextField();
					this.txtfldAlterFilterAttributeLabel.setColumns(10);
					this.circuitCensusCriteriaPanel.add(this.txtfldAlterFilterAttributeLabel, "6, 9, fill, default");
				}
				{
					JLabel lblAlterFilterAttribute_1 = new JLabel("<html>Alter Filter<br/>Attribute Value:</html>");
					this.circuitCensusCriteriaPanel.add(lblAlterFilterAttribute_1, "4, 11, right, default");
				}
				{
					this.txtfldAlterFilterAttributeValue = new JTextField();
					this.txtfldAlterFilterAttributeValue.setColumns(10);
					this.circuitCensusCriteriaPanel.add(this.txtfldAlterFilterAttributeValue, "6, 11, fill, default");
				}
			}

			JPanel partitionCriteriaPanel = new JPanel();
			column2Panel.add(partitionCriteriaPanel, "1, 5");
			partitionCriteriaPanel.setBorder(new TitledBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "", TitledBorder.LEADING,
					TitledBorder.TOP, null, new Color(51, 51, 51)), "Partition Criteria", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
			partitionCriteriaPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
					FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] { FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("min(60dlu;default)"), }));
			{
				JLabel lblIndividualClassificationType = new JLabel("<html><div style=\"text-align: right;\">Individual<br/>Classification Type:</div></html>");
				partitionCriteriaPanel.add(lblIndividualClassificationType, "2, 1, right, default");
			}
			{
				StringList labels = IndividualValuator.getEndogenousAttributeLabels();
				labels.addAll(attributeDescriptors.findByScope(Scope.INDIVIDUALS).labels());
				labels.sort();
				this.cmbxIndividualClassificationType = new JComboBox(new DefaultComboBoxModel(labels.toArray()));
				partitionCriteriaPanel.add(this.cmbxIndividualClassificationType, "4, 1, fill, default");
			}
			{
				JLabel lblRelationClassificationType = new JLabel("<html><div style=\"text-align: right;\">Relation<br/>Classification Type:</div<</html>");
				partitionCriteriaPanel.add(lblRelationClassificationType, "2, 3, right, top");
			}
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			partitionCriteriaPanel.add(scrollPane, "4, 3, fill, fill");
			this.relationClassificationTypePanel = new JPanel();
			scrollPane.setViewportView(this.relationClassificationTypePanel);
			this.relationClassificationTypePanel.setLayout(new BoxLayout(this.relationClassificationTypePanel, BoxLayout.Y_AXIS));
			for (ValueSequenceLabel type : ValueSequenceLabel.values()) {
				//
				JCheckBox chckbx = new JCheckBox(type.toString());
				this.relationClassificationTypePanel.add(chckbx);
			}
			{
				JPanel generalStatisticsPanel = new JPanel();
				column2Panel.add(generalStatisticsPanel, "1, 7");
				generalStatisticsPanel.setBorder(new TitledBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "", TitledBorder.LEADING,
						TitledBorder.TOP, null, new Color(51, 51, 51)), "General Statistics", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51,
						51)));
				generalStatisticsPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), },
						new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
								FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
								RowSpec.decode("4dlu:grow"), }));
				{
					JLabel lblSequenceCensusOperation = new JLabel("Sequence Census Operation:");
					generalStatisticsPanel.add(lblSequenceCensusOperation, "2, 1");
				}
				{
					this.sequenceGeneralStatisticsPanel = new JPanel();
					generalStatisticsPanel.add(this.sequenceGeneralStatisticsPanel, "2, 3, fill, fill");
					this.sequenceGeneralStatisticsPanel.setLayout(new BoxLayout(this.sequenceGeneralStatisticsPanel, BoxLayout.X_AXIS));
					for (SequenceGeneralStatistics type : SequenceGeneralStatistics.values()) {
						//
						JCheckBox chckbx = new JCheckBox(type.name());
						this.sequenceGeneralStatisticsPanel.add(chckbx);
					}
				}
				{
					JLabel lblSliceCensusOperation = new JLabel("Slice Census Operation:");
					generalStatisticsPanel.add(lblSliceCensusOperation, "2, 5");
				}
				{
					this.sliceGeneralStatisticsPanel = new JPanel();
					generalStatisticsPanel.add(this.sliceGeneralStatisticsPanel, "2, 7, fill, fill");
					this.sliceGeneralStatisticsPanel.setLayout(new BoxLayout(this.sliceGeneralStatisticsPanel, BoxLayout.X_AXIS));
					for (SliceGeneralStatistics type : SliceGeneralStatistics.values()) {
						//
						JCheckBox chckbx = new JCheckBox(type.name());
						this.sliceGeneralStatisticsPanel.add(chckbx);
					}
				}
			}
		}
		{
			JPanel column3Panel = new JPanel();
			this.contentPanel.add(column3Panel, "5, 4, fill, fill");
			column3Panel.setLayout(new FormLayout(new ColumnSpec[] { ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
					RowSpec.decode("4dlu:grow"), }));
			{
				JPanel egoNetworkstatisticsPanel = new JPanel();
				column3Panel.add(egoNetworkstatisticsPanel, "1, 1");
				egoNetworkstatisticsPanel.setBorder(new TitledBorder(new TitledBorder(new LineBorder(new Color(0, 0, 255)), "", TitledBorder.LEADING,
						TitledBorder.TOP, null, new Color(51, 51, 51)), "Ego Networks", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
				egoNetworkstatisticsPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { RowSpec
						.decode("default:grow"), }));
				{
					JLabel lblCensusOperationLabels_1 = new JLabel("Census Operation:");
					egoNetworkstatisticsPanel.add(lblCensusOperationLabels_1, "2, 1, right, top");
				}
				{
					this.egoNetworksOperationPanel = new JPanel();
					egoNetworkstatisticsPanel.add(this.egoNetworksOperationPanel, "4, 1, fill, fill");
					this.egoNetworksOperationPanel.setLayout(new BoxLayout(this.egoNetworksOperationPanel, BoxLayout.Y_AXIS));
					for (EgoNetworksOperation type : EgoNetworksOperation.values()) {
						//
						JCheckBox chckbx = new JCheckBox(type.name());
						this.egoNetworksOperationPanel.add(chckbx);
					}
				}
			}
			{
				JPanel parcoursNetworkStatisticsPanel = new JPanel();
				parcoursNetworkStatisticsPanel.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 255)), "Sequence Network Statistics",
						TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
				column3Panel.add(parcoursNetworkStatisticsPanel, "1, 3, fill, fill");
				parcoursNetworkStatisticsPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
						FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] { RowSpec.decode("min(60dlu;default):grow"),
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));
				{
					JLabel lblMainRelationClassification = new JLabel(
							"<html><div style=\"text-align: right\">Main Relation<br/>Classification Types:</div></html>");
					parcoursNetworkStatisticsPanel.add(lblMainRelationClassification, "2, 1, right, top");
				}
				{
					JScrollPane scrollPane = new JScrollPane();
					scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
					parcoursNetworkStatisticsPanel.add(scrollPane, "4, 1, fill, fill");
					{
						this.mainRelationClassificationTypePanel = new JPanel();
						scrollPane.setViewportView(this.mainRelationClassificationTypePanel);
						this.mainRelationClassificationTypePanel.setLayout(new BoxLayout(this.mainRelationClassificationTypePanel, BoxLayout.Y_AXIS));
						for (ValueSequenceLabel type : ValueSequenceLabel.values()) {
							//
							JCheckBox chckbx = new JCheckBox(type.toString());
							this.mainRelationClassificationTypePanel.add(chckbx);
						}
					}
				}
				{
					JPanel panel = new JPanel();
					parcoursNetworkStatisticsPanel.add(panel, "2, 3, fill, fill");
					panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
					{
						JLabel label = new JLabel("Census Operation:");
						panel.add(label);
					}
					{
						this.chckbxPNSGeneral = new JCheckBox("General");
						panel.add(this.chckbxPNSGeneral);
					}
					{
						this.chckbxPNSCentrality = new JCheckBox("Centrality");
						panel.add(this.chckbxPNSCentrality);
					}
					{
						this.chckbxPNSCohesion = new JCheckBox("Cohesion");
						panel.add(this.chckbxPNSCohesion);
					}
					{
						this.chckbxPNSRelations = new JCheckBox("Relations");
						panel.add(this.chckbxPNSRelations);
					}
				}
				{
					JPanel panel = new JPanel();
					parcoursNetworkStatisticsPanel.add(panel, "4, 3, fill, fill");
					panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
					{
						this.chckbxPNSExportParcours = new JCheckBox("Export Sequences");
						panel.add(this.chckbxPNSExportParcours);
					}
					{
						this.chckbxPNSExportExtendedParcours = new JCheckBox("Export Extended Sequences");
						panel.add(this.chckbxPNSExportExtendedParcours);
					}
					{
						this.chckbxPNSExportMultipleParcours = new JCheckBox("Export Multiple Sequences");
						panel.add(this.chckbxPNSExportMultipleParcours);
					}
				}
			}
			{
				JPanel typeNetworkStatisticsPanel = new JPanel();
				typeNetworkStatisticsPanel.setBorder(new TitledBorder(new TitledBorder(new LineBorder(new Color(0, 0, 255)), "", TitledBorder.LEADING,
						TitledBorder.TOP, null, new Color(51, 51, 51)), "Sequence Network Statistics", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51,
						51, 51)));
				column3Panel.add(typeNetworkStatisticsPanel, "1, 5, fill, fill");
				typeNetworkStatisticsPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] { RowSpec.decode("min(60dlu;default):grow"),
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));
				{
					JLabel lblMainRelationClassification_1 = new JLabel(
							"<html><div style=\"text-align: right;\">Main Relation<br/>Classification Types:</div></html>");
					typeNetworkStatisticsPanel.add(lblMainRelationClassification_1, "2, 1, right, top");
				}
				{
					JScrollPane scrollPane = new JScrollPane();
					typeNetworkStatisticsPanel.add(scrollPane, "4, 1, fill, fill");
					{
						this.trajectoriesClassificationTypePanel = new JPanel();
						scrollPane.setViewportView(this.trajectoriesClassificationTypePanel);
						this.trajectoriesClassificationTypePanel.setLayout(new BoxLayout(this.trajectoriesClassificationTypePanel, BoxLayout.Y_AXIS));
						this.mainRelationClassificationTypePanel.setLayout(new BoxLayout(this.mainRelationClassificationTypePanel, BoxLayout.Y_AXIS));
						for (ValueSequenceLabel type : ValueSequenceLabel.values()) {
							//
							JCheckBox chckbx = new JCheckBox(type.toString());
							this.trajectoriesClassificationTypePanel.add(chckbx);
						}

					}
				}
				{
					JLabel lblCensusOperations = new JLabel("Census Operations:");
					typeNetworkStatisticsPanel.add(lblCensusOperations, "2, 3, right, top");
				}
				{
					this.trajectoriesOperationsPanel = new JPanel();
					typeNetworkStatisticsPanel.add(this.trajectoriesOperationsPanel, "2, 5, 3, 1, right, fill");
					this.trajectoriesOperationsPanel.setLayout(new BoxLayout(this.trajectoriesOperationsPanel, BoxLayout.Y_AXIS));
					{
						this.chckbxTrajectoriesGeneral = new JCheckBox("General");
						this.trajectoriesOperationsPanel.add(this.chckbxTrajectoriesGeneral);
					}
					{
						this.chckbxTrajectoriesListTrees = new JCheckBox("List Trees");
						this.trajectoriesOperationsPanel.add(this.chckbxTrajectoriesListTrees);
					}
					{
						this.chckbxTrajectoriesDraw = new JCheckBox("Draw");
						this.trajectoriesOperationsPanel.add(this.chckbxTrajectoriesDraw);
					}
					{
						this.chckbxTrajetoriesExportParcoursNetworks = new JCheckBox("Export Sequence Networks");
						trajectoriesOperationsPanel.add(chckbxTrajetoriesExportParcoursNetworks);
					}
					{
						this.chckbxTrajectoriesExportEvenType = new JCheckBox("Export Aggregate Sequence Networks");
						this.trajectoriesOperationsPanel.add(this.chckbxTrajectoriesExportEvenType);
					}
					{
						this.chckbxTrajectoriesExportSequenceType = new JCheckBox("Export Aggregate Cumulative Sequence Networks");
						this.trajectoriesOperationsPanel.add(this.chckbxTrajectoriesExportSequenceType);
					}
					{
						this.chckbxTrajectoriesExportSimilaryTrees = new JCheckBox("Export Sequence Network Similarity Trees");
						trajectoriesOperationsPanel.add(chckbxTrajectoriesExportSimilaryTrees);
					}
				}
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					/**
					 * 
					 */
					@Override
					public void actionPerformed(final ActionEvent event) {
						// Cancel button.
						SequenceAnalysisInputDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				this.okButton = new JButton("Launch");
				this.okButton.addActionListener(new ActionListener() {
					/**
					 * 
					 */
					@Override
					public void actionPerformed(final ActionEvent event) {
						//
						SequenceCriteria criteria = getCriteria();

						PuckGUI.instance().getPreferences().setSpaceTimeAnalysisCriteria(criteria);

						if (criteria.getRelationModelName() == null) {
							//
							String title = "Invalid input";
							String message = "A relation model is required to launch.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

						} else if (criteria.getEgoRoleName() == null) {
							//
							String title = "Invalid input";
							String message = "An ego role is required to launch.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

						} else if (criteria.getDateLabel() == null) {
							//
							String title = "Invalid input";
							String message = "A valid date label is required to launch.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						} else {
							//
							SequenceAnalysisInputDialog.this.dialogCriteria = criteria;
							SequenceAnalysisInputDialog.this.dialogCriteria.setCensusParameters();

							//
							setVisible(false);
						}
					}
				});
				{
					Component horizontalStrut = Box.createHorizontalStrut(20);
					buttonPane.add(horizontalStrut);
				}
				{
					JButton btnDefault = new JButton("Reset");
					btnDefault.setToolTipText("Reset Default Values");
					btnDefault.addActionListener(new ActionListener() {
						/**
						 * 
						 */
						@Override
						public void actionPerformed(final ActionEvent event) {
							// Set default values.
							SequenceCriteria criteria = new SequenceCriteria();

							setCriteria(criteria);

						}
					});
					buttonPane.add(btnDefault);
				}
				{
					Component horizontalStrut = Box.createHorizontalStrut(20);
					buttonPane.add(horizontalStrut);
				}
				this.okButton.setActionCommand("OK");
				buttonPane.add(this.okButton);
				getRootPane().setDefaultButton(this.okButton);
			}
		}
		for (RelationModel model : relationModels) {
			//
			JCheckBox checkBox = new JCheckBox(model.getName());
			checkBox.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(final ItemEvent event) {
					//
					logger.debug("checkbox changed.");
					updateAlterRelationModel();
				}
			});

			this.alterRelationModelPanel.add(checkBox);
		}

		// ////////////////////////////////////
		updateAlterRelationModel();

		setCriteria(PuckGUI.instance().getPreferences().getSpaceTimeAnalysisCriteria());
	}

	/**
	 * 
	 * @return
	 */
	private StringList getAlterRelationModelNames() {
		StringList result;

		result = new StringList();

		for (Component component : this.alterRelationModelPanel.getComponents()) {
			//
			JCheckBox checkbox = (JCheckBox) component;

			if (checkbox.isSelected()) {
				//
				result.add(checkbox.getText());
			}
		}

		logger.debug("alterRelationModel checked={}", result.toStringWithCommas());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public SequenceCriteria getCriteria() {
		SequenceCriteria result;

		result = new SequenceCriteria();

		logger.debug("========== getCriteria");

		// Column 1
		// ========
		//
		result.setRelationModelName((String) this.cmbxRelationModel.getSelectedItem());
		logger.debug("RelationModelName=" + result.getEgoRoleName());

		//
		result.setEgoRoleName((String) this.cmbxEgoRole.getSelectedItem());
		logger.debug("EgoRoleName=" + result.getEgoRoleName());

		//
		result.setDateLabel((String) this.cmbxDateLabel.getSelectedItem());
		logger.debug("DateLabel=" + result.getDateLabel());

		//
		result.setStartDateLabel((String) this.cmbxStartDateLabel.getSelectedItem());
		logger.debug("StartDateLabel=" + result.getStartDateLabel());

		//
		result.setEndDateLabel((String) this.cmbxEndDateLabel.getSelectedItem());
		logger.debug("EndDateLabel=" + result.getEndDateLabel());

		//
		result.setLocalUnitLabel((String) this.cmbxLocalUnitLabel.getSelectedItem());
		logger.debug("LocalUnitLabel=" + result.getLocalUnitLabel());

		//
		result.setEgoRoleName((String) this.cmbxEgoRole.getSelectedItem());
		logger.debug("EgoRoleName=" + result.getEgoRoleName());

		result.setDefaultReferentRoleName((String) this.cmbxDefaultReferentRoleName.getSelectedItem());
		logger.debug("DefaultReferentRoleName=" + result.getDefaultReferentRoleName());

		//
		result.getRoleNames().clear();
		for (Component component : this.alterRolesPanel.getComponents()) {
			//
			if (((JCheckBox) component).isSelected()) {
				//
				result.getRoleNames().add(((JCheckBox) component).getText());
			}
		}
		logger.debug("roleNames=" + result.getRoleNames().toStringWithCommas());

		//
		if (this.txtfldDates.getText() == null) {
			//
			result.setDates(new Integer[0]);
		} else {
			//
			StringList values = new StringList();
			String[] tokens = this.txtfldDates.getText().split("[, ]");
			for (String token : tokens) {
				if (NumberUtils.isDigits(token)) {
					values.add(token);
				}
			}
			Integer[] target = new Integer[values.size()];
			for (int index = 0; index < values.size(); index++) {
				target[index] = new Integer(values.get(index));
			}
			result.setDates(target);
		}

		//
		result.setLevel((GeoLevel) this.cmbxLevel.getSelectedItem());

		//
		if (StringUtils.isNotBlank(this.txtfldMinimalPlaceNames.getText())) {
			//
			result.getMinimalPlaceNames().addAll(new StringList(this.txtfldMinimalPlaceNames.getText().split("[ ,]")));
		}
		logger.debug("minimalPlaceNames=" + result.getMinimalPlaceNames().toStringWithCommas());

		//
		result.setMinAge((Integer) this.spnrMinAge.getValue());
		result.setMaxAge((Integer) this.spnrMaxAge.getValue());

		//
		result.setExpansionMode((ExpansionMode) this.cmbxExpansionMode.getSelectedItem());
		result.setFiliationType((FiliationType) this.cmbxFiliationType.getSelectedItem());

		// Column 2
		// =================================
		//
		for (int index = 0; index < this.sequenceReportTypePanel.getComponentCount(); index++) {
			//
			JCheckBox checkbox = (JCheckBox) this.sequenceReportTypePanel.getComponent(index);

			if (checkbox.isSelected()) {
				//
				result.getSequenceReportTypes().add(SequenceReportType.values()[index]);
			}
		}
		{
			StringList buffer = new StringList();
			for (SequenceReportType type : result.getSequenceReportTypes()) {
				buffer.append(type.name());
			}
			logger.debug("SequenceReportTypes=" + buffer.toStringWithCommas());
		}

		//
		for (int index = 0; index < this.sliceReportTypePanel.getComponentCount(); index++) {
			//
			JCheckBox checkbox = (JCheckBox) this.sliceReportTypePanel.getComponent(index);

			if (checkbox.isSelected()) {
				//
				result.getSliceReportTypes().add(SliceReportType.values()[index]);
			}
		}
		{
			StringList buffer = new StringList();
			for (SliceReportType type : result.getSliceReportTypes()) {
				buffer.append(type.name());
			}
			logger.debug("SliceReportTypes=" + buffer.toStringWithCommas());
		}

		//
		result.setPattern(this.txtfldPattern.getText());
		result.setChainClassification(((ChainProperty) this.cmbxChainClassification.getSelectedItem()).name());
		result.setRelationModelNames(getAlterRelationModelNames());
		result.setAlterFilterRoleName((String) this.cmbxAlterFilterRole.getSelectedItem());
		result.setAlterAttributeLabel(this.txtfldAlterFilterAttributeLabel.getText());
		result.setAlterAttributeValue(this.txtfldAlterFilterAttributeValue.getText());

		logger.debug("[relationModelName={}][chaineClassification={}][alterRelationModelNames={}]", result.getRelationModelName(),
				result.getChainClassification(), new StringList(result.getRelationModelNames()).toStringWithCommas());

		//
		result.setPartitionLabel((String) this.cmbxIndividualClassificationType.getSelectedItem());
		logger.debug("individualClassificationType=" + result.getIndividualPartitionLabel());

		//
		result.getValueSequenceLabels().clear();
		for (int index = 0; index < this.relationClassificationTypePanel.getComponentCount(); index++) {
			//
			JCheckBox checkbox = (JCheckBox) this.relationClassificationTypePanel.getComponent(index);

			if (checkbox.isSelected()) {
				//
				result.getValueSequenceLabels().add(ValueSequenceLabel.values()[index]);
			}
		}
		logger.debug("getRelationClassificationTypes.size=" + result.getValueSequenceLabels().size());

		//
		result.getSequenceGeneralStatistics().clear();
		for (Component component : this.sequenceGeneralStatisticsPanel.getComponents()) {
			//
			JCheckBox checkbox = (JCheckBox) component;

			if (checkbox.isSelected()) {
				//
				result.getSequenceGeneralStatistics().add(SequenceGeneralStatistics.valueOf(checkbox.getText()));
			}
		}
		{
			StringList buffer = new StringList();
			for (SequenceGeneralStatistics type : result.getSequenceGeneralStatistics()) {
				buffer.append(type.name());
			}
			logger.debug("SequenceGeneralStatistics=" + buffer.toStringWithCommas());
		}

		//
		result.getSliceGeneralStatistics().clear();
		for (Component component : this.sliceGeneralStatisticsPanel.getComponents()) {
			//
			JCheckBox checkbox = (JCheckBox) component;

			if (checkbox.isSelected()) {
				//
				result.getSliceGeneralStatistics().add(SliceGeneralStatistics.valueOf(checkbox.getText()));
			}
		}
		{
			StringList buffer = new StringList();
			for (SliceGeneralStatistics type : result.getSliceGeneralStatistics()) {
				buffer.append(type.name());
			}
			logger.debug("SliceGeneralStatistics=" + buffer.toStringWithCommas());
		}

		// Column 3
		// =================================
		//
		result.getEgoNetworksOperations().clear();
		for (Component component : this.egoNetworksOperationPanel.getComponents()) {
			//
			JCheckBox checkbox = (JCheckBox) component;

			if (checkbox.isSelected()) {
				//
				result.getEgoNetworksOperations().add(EgoNetworksOperation.valueOf(checkbox.getText()));
			}
		}
		{
			StringList buffer = new StringList();
			for (EgoNetworksOperation type : result.getEgoNetworksOperations()) {
				buffer.append(type.name());
			}
			logger.debug("EgoNetworksOperations=" + buffer.toStringWithCommas());
		}

		//
		result.getMainValueSequenceLabels().clear();
		for (int index = 0; index < this.mainRelationClassificationTypePanel.getComponentCount(); index++) {
			//
			JCheckBox checkbox = (JCheckBox) this.mainRelationClassificationTypePanel.getComponent(index);

			if (checkbox.isSelected()) {
				//
				result.getMainValueSequenceLabels().add(ValueSequenceLabel.values()[index]);
			}
		}
		logger.debug("getMainRelationClassificationTypes.size=" + result.getMainValueSequenceLabels().size());

		//
		result.getParcoursNetworksOperations().clear();
		if (this.chckbxPNSGeneral.isSelected()) {
			//
			result.getParcoursNetworksOperations().add(ParcoursNetworksOperation.GENERAL);

		}
		if (this.chckbxPNSCentrality.isSelected()) {
			//
			result.getParcoursNetworksOperations().add(ParcoursNetworksOperation.CENTRALITY);

		}
		if (this.chckbxPNSCohesion.isSelected()) {
			//
			result.getParcoursNetworksOperations().add(ParcoursNetworksOperation.COHESION);

		}
		if (this.chckbxPNSRelations.isSelected()) {
			//
			result.getParcoursNetworksOperations().add(ParcoursNetworksOperation.RELATIONS);

		}
		if (this.chckbxPNSExportParcours.isSelected()) {
			//
			result.getParcoursNetworksOperations().add(ParcoursNetworksOperation.EXPORT_PARCOURS);

		}
		if (this.chckbxPNSExportExtendedParcours.isSelected()) {
			//
			result.getParcoursNetworksOperations().add(ParcoursNetworksOperation.EXPORT_EXTENDED_PARCOURS);

		}
		if (this.chckbxPNSExportMultipleParcours.isSelected()) {
			//
			result.getParcoursNetworksOperations().add(ParcoursNetworksOperation.EXPORT_MULTIPLE_PARCOURS);

		}
		logger.debug("getParcoursNetworksOperations.size=" + result.getParcoursNetworksOperations().size());

		//
		result.getTrajectoriesRelationClassificationTypes().clear();
		for (int index = 0; index < this.trajectoriesClassificationTypePanel.getComponentCount(); index++) {
			//
			JCheckBox checkbox = (JCheckBox) this.trajectoriesClassificationTypePanel.getComponent(index);

			if (checkbox.isSelected()) {
				//
				result.getTrajectoriesRelationClassificationTypes().add(ValueSequenceLabel.values()[index]);
			}
		}
		logger.debug("getTrajectoriesRelationClassificationTypes.size=" + result.getTrajectoriesRelationClassificationTypes().size());

		//
		result.getTrajectoriesOperations().clear();
		if (this.chckbxTrajectoriesGeneral.isSelected()) {
			//
			result.getTrajectoriesOperations().add(TrajectoriesOperation.GENERAL);

		}
		if (this.chckbxTrajectoriesListTrees.isSelected()) {
			//
			result.getTrajectoriesOperations().add(TrajectoriesOperation.LIST_TREES);

		}
		if (this.chckbxTrajectoriesExportEvenType.isSelected()) {
			//
			result.getTrajectoriesOperations().add(TrajectoriesOperation.EXPORT_AGGREGATE_SEQUENCE_NETWORK);

		}
		if (this.chckbxTrajectoriesExportSequenceType.isSelected()) {
			//
			result.getTrajectoriesOperations().add(TrajectoriesOperation.EXPORT_SEQUENCE_TYPE_NETWORK);
		}

		if (this.chckbxTrajectoriesDraw.isSelected()) {
			//
			result.getTrajectoriesOperations().add(TrajectoriesOperation.DRAW);
		}
		
		if (this.chckbxTrajetoriesExportParcoursNetworks.isSelected()) {
			//
			result.getTrajectoriesOperations().add(TrajectoriesOperation.EXPORT_SEQUENCE_NETWORKS);

		}
		if (this.chckbxTrajectoriesExportSimilaryTrees.isSelected()) {
			//
			result.getTrajectoriesOperations().add(TrajectoriesOperation.EXPORT_SIMILARY_TREES);
		}

		logger.debug("getTrajectoriesOperations.size=" + result.getTrajectoriesOperations().size());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public SequenceCriteria getDialogCriteria() {
		SequenceCriteria result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public boolean isSequenceAnalysis() {
		boolean result;

		if ((StringUtils.isBlank(this.txtfldDates.getText())) && (StringUtils.isBlank((String) this.cmbxLocalUnitLabel.getSelectedItem()))) {
			//
			result = true;
		} else {
			//
			result = false;
		}

		logger.debug("isSequence=" + result);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final SequenceCriteria source) {
		//
		if (source != null) {
			//
			if (this.relationModels.isEmpty()) {
				//
				this.cmbxRelationModel.setEnabled(false);
				this.okButton.setEnabled(false);

			} else {
				//
				this.cmbxRelationModel.setEnabled(true);
				this.okButton.setEnabled(true);

				//
				logger.debug("==>" + source.getRelationModelName());
				this.cmbxRelationModel.setSelectedItem(source.getRelationModelName());
				if (this.cmbxRelationModel.getSelectedIndex() == -1) {
					this.cmbxRelationModel.setSelectedIndex(0);
				}
				updateRelationModel();

				if (StringUtils.isNotBlank(source.getPlaceLabel())) {
					//
					this.cmbxPlaceLabel.setSelectedItem(source.getPlaceLabel());
				}

				if (StringUtils.isNotBlank(source.getLocalUnitLabel())) {
					//
					this.cmbxLocalUnitLabel.setSelectedItem(source.getLocalUnitLabel());
				}

				if (StringUtils.isNotBlank(source.getEgoRoleName())) {
					//
					this.cmbxEgoRole.setSelectedItem(source.getEgoRoleName());
				}

				if (StringUtils.isNotBlank(source.getDefaultReferentRoleName())) {
					//
					this.cmbxDefaultReferentRoleName.setSelectedItem(source.getDefaultReferentRoleName());
				}

				//
				for (Component component : this.alterRolesPanel.getComponents()) {
					//
					JCheckBox checkbox = (JCheckBox) component;

					if ((source.getRoleNames().contains(checkbox.getText())) || (source.getRoleNames().contains("ALL"))) {
						//
						checkbox.setSelected(true);
					} else {
						//
						checkbox.setSelected(false);
					}
				}

				//
				if (source.getLevel() != null) {
					//
					this.cmbxLevel.setSelectedItem(source.getLevel());
				}

				//
				if (source.getDates() != null) {
					StringList dates = new StringList();
					for (Integer date : source.getDates()) {
						dates.add("" + date);
					}

					this.txtfldDates.setText(dates.toStringWithCommas());
				} else {
					//
					this.txtfldDates.setText("");
				}

				//
				if (source.getMinimalPlaceNames() != null) {
					//
					this.txtfldMinimalPlaceNames.setText(source.getMinimalPlaceNames().toStringWithCommas());
				}

				//
				this.spnrMinAge.setValue(source.getMinAge());
				this.spnrMaxAge.setValue(source.getMaxAge());

				//
				if (source.getExpansionMode() == null) {
					//
					this.cmbxExpansionMode.setSelectedItem(ExpansionMode.RELATED);
				} else {
					//
					this.cmbxExpansionMode.setSelectedItem(source.getExpansionMode());
				}

				//
				if (source.getFiliationType() == null) {
					//
					this.cmbxFiliationType.setSelectedItem(FiliationType.COGNATIC);
				} else {
					//
					this.cmbxFiliationType.setSelectedItem(source.getFiliationType());
				}

				// Column 2
				// =================================
				for (Component component : this.sequenceReportTypePanel.getComponents()) {
					//
					JCheckBox checkbox = (JCheckBox) component;

					if (source.getSequenceReportTypes().contains(translateToSequenceReportType(checkbox.getText()))) {
						//
						checkbox.setSelected(true);
					} else {
						//
						checkbox.setSelected(false);
					}
				}
				for (Component component : this.sliceReportTypePanel.getComponents()) {
					//
					JCheckBox checkbox = (JCheckBox) component;

					if (source.getSliceReportTypes().contains(translateToSliceReportType(checkbox.getText()))) {
						//
						checkbox.setSelected(true);
					} else {
						//
						checkbox.setSelected(false);
					}
				}

				this.txtfldPattern.setText(source.getPattern());
				this.cmbxChainClassification.setSelectedItem(ChainProperty.valueOf(source.getChainClassification()));

				for (Component component : this.alterRelationModelPanel.getComponents()) {
					//
					JCheckBox checkbox = (JCheckBox) component;

					if (source.getRelationModelNames().contains(checkbox.getText())) {
						//
						checkbox.setSelected(true);
					} else {
						//
						checkbox.setSelected(false);
					}
				}

				this.txtfldAlterFilterAttributeLabel.setText(source.getAlterFilterAttributeLabel());
				this.txtfldAlterFilterAttributeValue.setText(source.getAlterFilterAttributeValue());
				this.cmbxAlterFilterRole.setSelectedItem(source.getAlterFilterRoleName());

				//
				if (StringUtils.isNotBlank(source.getIndividualPartitionLabel())) {
					//
					this.cmbxIndividualClassificationType.setSelectedItem(source.getIndividualPartitionLabel());
				}

				//
				for (Component component : this.relationClassificationTypePanel.getComponents()) {
					//
					JCheckBox checkbox = (JCheckBox) component;
					try {
						if (source.getValueSequenceLabels().contains(ValueSequenceLabel.valueOf(checkbox.getText()))) {
							//
							checkbox.setSelected(true);
						} else {
							//
							checkbox.setSelected(false);
						}
					} catch (Exception exception) {
						exception.printStackTrace();
						logger.debug("Unknown label");
					}
				}

				//
				for (Component component : this.sequenceGeneralStatisticsPanel.getComponents()) {
					//
					JCheckBox checkbox = (JCheckBox) component;

					if (source.getSequenceGeneralStatistics().contains(SequenceGeneralStatistics.valueOf(checkbox.getText()))) {
						//
						checkbox.setSelected(true);
					} else {
						//
						checkbox.setSelected(false);
					}
				}

				for (Component component : this.sliceGeneralStatisticsPanel.getComponents()) {
					//
					JCheckBox checkbox = (JCheckBox) component;

					if (source.getSliceGeneralStatistics().contains(SliceGeneralStatistics.valueOf(checkbox.getText()))) {
						//
						checkbox.setSelected(true);
					} else {
						//
						checkbox.setSelected(false);
					}
				}

				// Column 3
				// =================================
				//
				for (Component component : this.egoNetworksOperationPanel.getComponents()) {
					//
					JCheckBox checkbox = (JCheckBox) component;

					if (source.getEgoNetworksOperations().contains(EgoNetworksOperation.valueOf(checkbox.getText()))) {
						//
						checkbox.setSelected(true);
					} else {
						//
						checkbox.setSelected(false);
					}
				}

				//
				for (Component component : this.mainRelationClassificationTypePanel.getComponents()) {
					//
					JCheckBox checkbox = (JCheckBox) component;
					try {
						if (source.getMainValueSequenceLabels().contains(ValueSequenceLabel.valueOf(checkbox.getText()))) {
							//
							checkbox.setSelected(true);
						} else {
							//
							checkbox.setSelected(false);
						}
					} catch (Exception exception) {
						exception.printStackTrace();
						logger.debug("Unknown label");
					}
				}

				//
				this.chckbxPNSCentrality.setSelected(false);
				this.chckbxPNSCohesion.setSelected(false);
				this.chckbxPNSExportExtendedParcours.setSelected(false);
				this.chckbxPNSExportMultipleParcours.setSelected(false);
				this.chckbxPNSExportParcours.setSelected(false);
				this.chckbxTrajetoriesExportParcoursNetworks.setSelected(false);
				this.chckbxTrajectoriesExportSimilaryTrees.setSelected(false);
				this.chckbxPNSGeneral.setSelected(false);
				this.chckbxPNSRelations.setSelected(false);
				for (ParcoursNetworksOperation operation : source.getParcoursNetworksOperations()) {
					switch (operation) {
						case CENTRALITY:
							this.chckbxPNSCentrality.setSelected(true);
						break;

						case COHESION:
							this.chckbxPNSCohesion.setSelected(true);
						break;

						case EXPORT_EXTENDED_PARCOURS:
							this.chckbxPNSExportExtendedParcours.setSelected(true);
						break;

						case EXPORT_MULTIPLE_PARCOURS:
							this.chckbxPNSExportMultipleParcours.setSelected(true);
						break;

						case EXPORT_PARCOURS:
							this.chckbxPNSExportParcours.setSelected(true);
						break;

						case GENERAL:
							this.chckbxPNSGeneral.setSelected(true);
						break;

						case RELATIONS:
							this.chckbxPNSRelations.setSelected(true);
						break;

						default:
					}
				}

				//
				for (Component component : this.trajectoriesClassificationTypePanel.getComponents()) {
					//
					JCheckBox checkbox = (JCheckBox) component;
					try {
						if (source.getTrajectoriesRelationClassificationTypes().contains(ValueSequenceLabel.valueOf(checkbox.getText()))) {
							//
							checkbox.setSelected(true);
						} else {
							//
							checkbox.setSelected(false);
						}
					} catch (Exception exception) {
						exception.printStackTrace();
						logger.debug("Unknown label");
					}
				}

				//
				this.chckbxTrajectoriesExportEvenType.setSelected(false);
				this.chckbxTrajectoriesExportSequenceType.setSelected(false);
				this.chckbxTrajectoriesGeneral.setSelected(false);
				this.chckbxTrajectoriesListTrees.setSelected(false);
				for (TrajectoriesOperation operation : source.getTrajectoriesOperations()) {
					switch (operation) {
						case EXPORT_AGGREGATE_SEQUENCE_NETWORK:
							this.chckbxTrajectoriesExportEvenType.setSelected(true);
						break;

						case EXPORT_SEQUENCE_TYPE_NETWORK:
							this.chckbxTrajectoriesExportSequenceType.setSelected(true);
						break;

						case GENERAL:
							this.chckbxTrajectoriesGeneral.setSelected(true);
						break;

						case LIST_TREES:
							this.chckbxTrajectoriesListTrees.setSelected(true);
						break;

						case DRAW:
							this.chckbxTrajectoriesDraw.setSelected(true);
						break;
						
						case EXPORT_SEQUENCE_NETWORKS:
							this.chckbxTrajetoriesExportParcoursNetworks.setSelected(true);
						break;

						case EXPORT_SIMILARY_TREES:
							this.chckbxTrajectoriesExportSimilaryTrees.setSelected(true);
						break;

						default:
					}
				}

			}
		}

		//
		updateSequenceSliceMode();
	}

	/**
	 * 
	 * @param source
	 * @param value
	 */
	private void setEnabledAll(final JPanel source, final boolean value) {
		//
		for (Component component : source.getComponents()) {
			//
			component.setEnabled(value);
		}
	}

	/**
	 * 
	 * @param currentRelationModelIndex
	 */
	private void updateAlterRelationModel() {
		//
		StringList currentRelationModelNames = getAlterRelationModelNames();

		StringSet roleNames = new StringSet();

		for (String relationModelName : currentRelationModelNames) {
			//
			roleNames.addAll(this.relationModels.getByName(relationModelName).roles().toNameList());
		}

		logger.debug("roleNames={}", roleNames.toStringList().toStringWithCommas());

		//
		StringList target = new StringList();
		target.add("ALL");
		target.addAll(roleNames.toStringList().sort());
		this.cmbxAlterFilterRole.setModel(new DefaultComboBoxModel(target.toArray()));
	}

	/**
	 * 
	 * @param currentRelationModelIndex
	 */
	private void updateRelationModel() {
		//
		RelationModel currentRelationModel = this.relationModels.getByName((String) this.cmbxRelationModel.getSelectedItem());

		StringList attributeLabels;
		StringList currentRoles;
		StringList optionalAttributeLabels;
		StringList actorAttributeLabels;
		if (currentRelationModel == null) {
			//
			attributeLabels = new StringList();
			optionalAttributeLabels = new StringList().append("");
			currentRoles = new StringList();
			actorAttributeLabels = new StringList();

		} else {
			//
			currentRoles = currentRelationModel.roles().toNameList().sort();
			attributeLabels = this.attributeDescriptors.findByRelationModelName(currentRelationModel.getName()).labels().sort();
			optionalAttributeLabels = new StringList().append("").append(attributeLabels);

			Actors actors = this.segmentation.getCurrentRelations().getActors();
			AttributeDescriptors actorAttributeDescriptors = AttributeWorker.getExogenousAttributeDescriptors(actors);
			actorAttributeLabels = actorAttributeDescriptors.labels().sort();
		}

		//
		if (attributeLabels.isEmpty()) {
			//
			this.cmbxDateLabel.setEnabled(false);
			this.cmbxPlaceLabel.setEnabled(false);
			this.cmbxStartPlaceLabel.setEnabled(false);
			this.cmbxEndPlaceLabel.setEnabled(false);
			this.cmbxLocalUnitLabel.setEnabled(false);

		} else {
			//
			this.cmbxDateLabel.setEnabled(true);
			this.cmbxPlaceLabel.setEnabled(true);
			this.cmbxStartPlaceLabel.setEnabled(true);
			this.cmbxEndPlaceLabel.setEnabled(true);
			this.cmbxLocalUnitLabel.setEnabled(true);

			//
			this.cmbxDateLabel.setModel(new DefaultComboBoxModel(attributeLabels.toArray()));
			this.cmbxDateLabel.setSelectedItem("DATE");
			this.cmbxPlaceLabel.setModel(new DefaultComboBoxModel(attributeLabels.toArray()));
			this.cmbxPlaceLabel.setSelectedItem("PLACE");
			this.cmbxStartPlaceLabel.setModel(new DefaultComboBoxModel(attributeLabels.toArray()));
			this.cmbxStartPlaceLabel.setSelectedItem("START_PLACE");
			this.cmbxEndPlaceLabel.setModel(new DefaultComboBoxModel(attributeLabels.toArray()));
			this.cmbxEndPlaceLabel.setSelectedItem("END_PLACE");
			this.cmbxLocalUnitLabel.setModel(new DefaultComboBoxModel(optionalAttributeLabels.toArray()));
		}

		//
		if (actorAttributeLabels.isEmpty()) {
			//
			this.cmbxStartDateLabel.setEnabled(false);
			this.cmbxEndDateLabel.setEnabled(false);

		} else {
			//
			this.cmbxStartDateLabel.setEnabled(true);
			this.cmbxEndDateLabel.setEnabled(true);

			//
			this.cmbxStartDateLabel.setModel(new DefaultComboBoxModel(actorAttributeLabels.toArray()));
			this.cmbxStartDateLabel.setSelectedItem("START_DATE");
			this.cmbxEndDateLabel.setModel(new DefaultComboBoxModel(actorAttributeLabels.toArray()));
			this.cmbxEndDateLabel.setSelectedItem("END_DATE");
		}

		//
		if (currentRoles.isEmpty()) {
			//
			this.cmbxEgoRole.setEnabled(false);
			this.cmbxDefaultReferentRoleName.setEnabled(false);
			this.alterRolesPanel.removeAll();
			this.alterRolesPanel.repaint();

		} else {
			//
			this.cmbxEgoRole.setEnabled(true);
			this.cmbxDefaultReferentRoleName.setEnabled(true);

			//
			this.cmbxEgoRole.setModel(new DefaultComboBoxModel(currentRoles.toArray()));
			this.cmbxDefaultReferentRoleName.setModel(new DefaultComboBoxModel(currentRoles.toArray()));

			this.alterRolesPanel.removeAll();
			for (String roleName : currentRoles) {
				//
				JCheckBox checkbox = new JCheckBox(roleName);
				checkbox.setSelected(true);
				this.alterRolesPanel.add(checkbox);
			}
		}
	}

	/**
	 * 
	 */
	private void updateSequenceSliceMode() {
		if (isSequenceAnalysis()) {
			//
			this.lblMode.setText("SEQUENCES MODE ");
			setEnabledAll(this.sliceReportTypePanel, false);
			setEnabledAll(this.sequenceReportTypePanel, true);
			setEnabledAll(this.sequenceGeneralStatisticsPanel, true);
			setEnabledAll(this.sliceGeneralStatisticsPanel, false);
		} else {
			//
			this.lblMode.setText("SLICES MODE ");
			setEnabledAll(this.sliceReportTypePanel, true);
			setEnabledAll(this.sequenceReportTypePanel, false);
			setEnabledAll(this.sequenceGeneralStatisticsPanel, false);
			setEnabledAll(this.sliceGeneralStatisticsPanel, true);
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		/* SequenceCriteria criteria = */showDialog(null, null, null);
	}

	/**
	 * Launch the application.
	 */
	public static SequenceCriteria showDialog(final Segmentation segmentation, final RelationModels relationModels,
			final AttributeDescriptors attributeDescriptors) {
		SequenceCriteria result;

		//
		SequenceAnalysisInputDialog dialog = new SequenceAnalysisInputDialog(segmentation, relationModels, attributeDescriptors);
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}

	/**
	 * 
	 * @param type
	 * @return
	 */
	private static String translate(final SequenceReportType type) {
		String result;

		if (type == null) {
			//
			result = null;

		} else {
			switch (type) {
				case ITINERARIES_SURVEY:
					result = "Itineraries";
				break;
				case ITINERARIES_DETAILS:
					result = "Itineraries (with context)";
				break;
				case BIOGRAPHIES:
					result = "Biography";
				break;
				case EXTENDED_BIOGRAPHIES:
					result = "Extended Biographies";
				break;
				case ACTOR_EVENT_TABLES:
					result = "Actor-Event Tables";
				break;
				case INTERACTION_TABLES:
					result = "Interacton Tables";
				break;
				default:
					result = type.name();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param type
	 * @return
	 */
	private static String translate(final SliceReportType type) {
		String result;

		if (type == null) {
			//
			result = null;

		} else {
			switch (type) {
				case MEMBERSHIP:
					result = "Membership";
					break;
				case TURNOVER:
					result = "Turnover";
					break;
				default:
					result = type.name();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param type
	 * @return
	 */
	private static SequenceReportType translateToSequenceReportType(final String type) {
		SequenceReportType result;

		if (type == null) {
			//
			result = null;

		} else if (type.equals("Itineraries")) {
			//
			result = SequenceReportType.ITINERARIES_SURVEY;

		} else if (type.equals("Itineraries (with context)")) {
			//
			result = SequenceReportType.ITINERARIES_DETAILS;

		} else if (type.equals("Biography")) {
			//
			result = SequenceReportType.BIOGRAPHIES;

		} else if (type.equals("Extended Biographies")) {
			//
			result = SequenceReportType.EXTENDED_BIOGRAPHIES;

		} else if (type.equals("Actor-Event Tables")) {
			//
			result = SequenceReportType.ACTOR_EVENT_TABLES;

		} else if (type.equals("Interacton Tables")) {
			//
			result = SequenceReportType.INTERACTION_TABLES;
		} else {
			//
			try {
				result = SequenceReportType.valueOf(type);
			} catch (Exception exception) {
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param type
	 * @return
	 */
	private static SliceReportType translateToSliceReportType(final String type) {
		SliceReportType result;

		if (type == null) {
			//
			result = null;

		} else if (type.equals("Membership")) {
			//
			result = SliceReportType.MEMBERSHIP;
			
		} else if (type.equals("Turnover")) {
			//
			result = SliceReportType.TURNOVER;
			
		} else {
			//
			try {
				result = SliceReportType.valueOf(type);
			} catch (Exception exception) {
				result = null;
			}
		}

		//
		return result;
	}
}
