package org.tip.puckgui.views;

import java.awt.Component;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author cpm
 */
public class ConfirmQuitDialog {

	private static final Logger logger = LoggerFactory.getLogger(ConfirmQuitDialog.class);

	/**
	 * 
	 * @param parent
	 * @return
	 */
	public static boolean showDialog(final Component parent) {
		boolean result;

		//
		String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.quitConfirm.existingChange.title");
		String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.quitConfirm.existingChange.text");
		Object optionPaneIsYesLast = UIManager.get("OptionPane.isYesLast");
		UIManager.put("OptionPane.isYesLast", false);
		String buttons[] = { "Quit without Saving", "Cancel" };
		int response = JOptionPane.showOptionDialog(null, message, title, JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, buttons,
				buttons[1]);
		UIManager.put("OptionPane.isYesLast", optionPaneIsYesLast);

		if (response == 1) {
			//
			logger.debug("Cancel quit");
			result = false;

		} else {
			//
			logger.debug("Quit");
			result = true;
		}

		//
		return result;
	}
}