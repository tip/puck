package org.tip.puck.io.permutation;

import java.util.HashMap;

/**
 * This class represents a permutation table.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class PermutationTable {

	private HashMap<Integer, Integer> sourceToTarget;
	private HashMap<Integer, Integer> targetToSource;

	/**
	 * 
	 */
	public PermutationTable() {
		this.sourceToTarget = new HashMap<Integer, Integer>();
		this.targetToSource = new HashMap<Integer, Integer>();
	}

	/**
	 * 
	 * @param source
	 * @param target
	 */
	public void add(final int source, final int target) {
		this.sourceToTarget.put(source, target);
		this.targetToSource.put(target, source);
	}

	/**
	 * 
	 */
	public Integer getSource(final int target) {
		Integer result;

		result = this.targetToSource.get(target);

		//
		return result;
	}

	/**
	 * 
	 */
	public Integer getTarget(final int source) {
		Integer result;

		result = this.sourceToTarget.get(source);

		//
		return result;
	}
}
