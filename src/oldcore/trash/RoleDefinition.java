package oldcore.trash;

import org.tip.puck.net.Gender;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.relations.Roles;
import org.tip.puck.util.Numberable;

/**
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class RoleDefinition implements Numberable {

	public enum AlterAge {
		ELDER,
		YOUNGER;

		public AlterAge invert() {
			AlterAge result;

			if (this == ELDER) {
				result = YOUNGER;
			} else if (this == YOUNGER) {
				result = ELDER;
			} else {
				result = null;
			}
			//
			return result;
		}
		

	}

	public enum Primary {
		PARENT,
		SIBLING,
		SPOUSE;
	}

	private int id;
	private Role role;
	private Primary primary;
	private Role inversion;
	private Roles composition;
	private AlterAge alterAge;
	private Gender alterGender;
	private Gender egoGender;
	private String other;

	
	public RoleDefinition(int id){
		this.id = id;
		this.alterGender = Gender.UNKNOWN;
		this.egoGender = Gender.UNKNOWN;
	}
	
	
	/**
	 * 
	 * @param id
	 * @param role
	 * @param primary
	 * @param inversion
	 * @param composition
	 * @param alterGender
	 * @param alterAge
	 * @param egoGender
	 */
	public RoleDefinition(final int id, final Role role, final Primary primary, final Role inversion, final Roles composition, final Gender alterGender,
			final AlterAge alterAge, final Gender egoGender) {

		this.id = id;
		this.role = role;
		this.primary = primary;
		this.inversion = inversion;
		if (composition != null && composition.size() > 0) {
			this.composition = composition;
		}
		this.alterGender = alterGender;
		this.alterAge = alterAge;
		this.egoGender = egoGender;
		
		if (this.composition != null && this.composition.size()==2 && this.composition.get(1) == null) {
			System.err.println("Composition error: " + id + " " + role + " " + this.composition.get(0));
		}

	}

	public AlterAge alterAge() {
		return this.alterAge;
	}

	public String alterAgeAsString() {
		String result;

		if (this.alterAge == null) {
			result = "";
		} else {
			result = this.alterAge.toString();
		}
		//
		return result;
	}

	public Gender alterGender() {
		return this.alterGender;
	}

	public String alterGenderAsString() {
		String result;

		if (this.alterGender == null) {
			result = "";
		} else {
			result = this.alterGender.toString();
		}
		//
		return result;
	}

	public String alterGenderAsSymbol() {
		String result;

		if (this.alterGender == null || this.alterGender == Gender.UNKNOWN) {
			result = "";
		} else {
			result = this.alterGender.toSymbol() + "";
		}
		//
		return result;
	}
	
	@Override
	public RoleDefinition clone() {
		RoleDefinition result;
		
		Roles compositionClone = null;
		if (composition!=null){
			compositionClone = composition.clone();
		}
		
		result = new RoleDefinition(this.id, this.role, this.primary, this.inversion, compositionClone, this.alterGender, this.alterAge, this.egoGender);

		//
		return result;
	}

	public Roles composition() {
		return this.composition;
	}

	private String compositionAsString() {
		String result;

		if (RoleDefinitions.isNullOrHasNullFactor(composition)) {
			result = "";
		} else {
			result = composition.get(0).getName() + " ° " + composition.get(1).getName();
		}
		//
		return result;
	}

	public Gender egoGender() {
		return this.egoGender;
	}

	String egoGenderAsSpeakerString() {
		String result;

		if (this.egoGender == null) {
			result = "";
		} else {
			result = this.egoGender.toSpeakerString();
		}
		//
		return result;
	}

	public String egoGenderAsString() {
		String result;

		if (this.egoGender == null) {
			result = "";
		} else {
			result = this.egoGender.toString();
		}
		//
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		boolean result;

		RoleDefinition other = (RoleDefinition) obj;

		result = (this.role.equals(other.role()) && this.alterGender == other.alterGender && this.egoGender == other.egoGender
				&& this.alterAge == other.alterAge && this.primary == other.primary && (this.inversion == null || this.inversion.equals(other.inversion)) && (this.composition == null || this.composition
				.equals(other.composition)));

		//
		return result;
	}

	@Override
	public int getId() {
		return this.id;
	}

	public String getInversePrimaryAsString(final RoleDefinitions roleDefinitions) {
		String result;

		result = "";

		for (RoleDefinition roleDefinition : roleDefinitions) {
			if (this.inversion.equals(roleDefinition.role()) && roleDefinition.primary() == Primary.PARENT) {
				if (this.alterGender.isUnknown()) {
					result = "Ch";
				} else if (this.alterGender.isMale()) {
					result = "S";
				} else if (this.alterGender.isFemale()) {
					result = "D";
				}
			}
		}

		//
		return result;
	}

	public String getPrimaryAsString() {
		String result;

		result = "";

		if (this.primary != null) {
			switch (this.primary) {
				case PARENT:
					if (this.alterGender.isUnknown()) {
						result = "Pa";
					} else if (this.alterGender.isMale()) {
						result = "F";
					} else if (this.alterGender.isFemale()) {
						result = "M";
					}
				break;
				case SIBLING:
					if (this.alterGender.isUnknown()) {
						result = "Sb";
					} else if (this.alterGender.isMale()) {
						result = "B";
					} else if (this.alterGender.isFemale()) {
						result = "Z";
					}
					if (this.alterAge == AlterAge.ELDER) {
						result = "e" + result;
					} else if (this.alterAge == AlterAge.YOUNGER) {
						result = "y" + result;
					}
				break;
				case SPOUSE:
					if (this.alterGender.isUnknown()) {
						result = "Sp";
					} else if (this.alterGender.isMale()) {
						result = "H";
					} else if (this.alterGender.isFemale()) {
						result = "W";
					}
				break;
			}
		}
		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String hashKey() {
		String result;

		result = "" + this.id;

		//
		return result;
	}

	public Role inversion() {
		return this.inversion;
	}

	private String inversionAsString() {
		String result;

		if (this.inversion == null) {
			result = "";
		} else {
			result = "-" + this.inversion.getName();
		}
		//
		return result;
	}

	public Primary primary() {
		return this.primary;
	}

	public String primaryAsString() {
		String result;

		if (this.primary == null) {
			result = "";
		} else {
			result = this.primary.toString();
		}
		//
		return result;

	}

	public Role role() {
		return this.role;
	}
	
	

	public void setRole(Role role) {
		this.role = role;
	}


	public void setAlterAge(final AlterAge alterAge) {
		this.alterAge = alterAge;
	}

	public void setAlterGender(final Gender alterGender) {
		this.alterGender = alterGender;
	}

	public void setEgoGender(final Gender egoGender) {
		this.egoGender = egoGender;
	}

	public void setPrimary(Primary primary) {
		this.primary = primary;
	}
	
	public void setInversion(Role inversion) {
		this.inversion = inversion;
	}
	
	public void setComposition() {
		this.composition = new Roles();
	}

	

	public String other() {
		return other;
	}


	public void setOther(String other) {
		this.other = other;
	}


	@Override
	public void setId(final int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		String result;
		
		String alterGenderSymbol = "";
		if (!alterGender.isUnknown()){
			alterGenderSymbol = " " + alterGender.toSymbol();
		}

		result = role.getName() + "\t" + alterAgeAsString() + alterGenderSymbol + " " + primaryAsString() + " " + inversionAsString() + " "
				+ compositionAsString() + " " + egoGenderAsSpeakerString();

		if (other!=null){
			result += "\t"+other;
		}
		
		//
		return result;
	}

}
