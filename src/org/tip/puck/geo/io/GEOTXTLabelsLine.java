package org.tip.puck.geo.io;

import fr.devinsy.util.StringList;

/**
 * This class represents a line of labels from a TXT file.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class GEOTXTLabelsLine extends StringList {

	private static final long serialVersionUID = -8225234746002957857L;

	/**
	 * 
	 */
	public GEOTXTLabelsLine() {
		super();
	}
}
