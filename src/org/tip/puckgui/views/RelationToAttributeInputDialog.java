package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.workers.AttributeDescriptor;
import org.tip.puck.net.workers.AttributeDescriptors;
import org.tip.puck.net.workers.AttributeToRelationCriteria;
import org.tip.puck.net.workers.AttributeWorker;
import org.tip.puck.net.workers.RelationToAttributeCriteria;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class RelationToAttributeInputDialog extends JDialog {

	private static final long serialVersionUID = 5776095801370887222L;
	private final JPanel contentPanel = new JPanel();
	private RelationToAttributeCriteria dialogCriteria;
	private static RelationToAttributeCriteria lastCriteria = new RelationToAttributeCriteria();
	private JComboBox cmbbxTarget;
	private JComboBox cmbbxAttributeLabel;
	private JComboBox cmbbxDateLabel;
	private JCheckBox chckbxIgnoreCase;

	/**
	 * Create the dialog.
	 */
	public RelationToAttributeInputDialog(final List<String> relationModelNames, final AttributeDescriptors attributeDescriptors) {
		super();

		//
		List<String> targetLabels = new ArrayList<String>();
		if (relationModelNames != null) {
			for (String name : relationModelNames) {
				targetLabels.add(name);
			}
		}

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Relation To Attribute Input");
		setIconImage(Toolkit.getDefaultToolkit().getImage(RelationToAttributeInputDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				RelationToAttributeInputDialog.this.dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 420, 235);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, }));
		{
			JLabel lblTarget = new JLabel("Target:");
			this.contentPanel.add(lblTarget, "2, 2, right, default");
		}
		{
			this.cmbbxTarget = new JComboBox(targetLabels.toArray());
			this.cmbbxTarget.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(final ItemEvent event) {
					// Update label list.
					//
					if (event.getStateChange() == ItemEvent.SELECTED) {
						//
						List<String> labels = attributeDescriptors.findByRelationModelName((String) RelationToAttributeInputDialog.this.cmbbxTarget.getSelectedItem())
										.labels();

						//
						Collections.sort(labels);

						//
						RelationToAttributeInputDialog.this.cmbbxAttributeLabel.setModel(new DefaultComboBoxModel(labels.toArray()));
						RelationToAttributeInputDialog.this.cmbbxAttributeLabel.setSelectedIndex(-1);
						//
						RelationToAttributeInputDialog.this.cmbbxDateLabel.setModel(new DefaultComboBoxModel(labels.toArray()));
						RelationToAttributeInputDialog.this.cmbbxDateLabel.setSelectedIndex(-1);
					}
				}
			});
			this.contentPanel.add(this.cmbbxTarget, "4, 2, fill, default");
		}
		{
			JLabel lblLabel = new JLabel("Attribute Label:");
			this.contentPanel.add(lblLabel, "2, 4, right, default");
		}
		{
			this.cmbbxAttributeLabel = new JComboBox(attributeDescriptors.labelsSorted().toArray());
			this.cmbbxAttributeLabel.setEditable(true);
			this.contentPanel.add(this.cmbbxAttributeLabel, "4, 4, fill, default");
		}
		{
			JLabel lblNewLabel = new JLabel("Date Label:");
			this.contentPanel.add(lblNewLabel, "2, 6, right, default");
		}
		
		{
			this.cmbbxDateLabel = new JComboBox(attributeDescriptors.labelsSorted().toArray());
			this.cmbbxDateLabel.setEditable(true);
			this.contentPanel.add(this.cmbbxDateLabel, "4, 6, fill, default");
		}

		{
			JPanel panel = new JPanel();
			panel.setBorder(null);
			this.contentPanel.add(panel, "4, 8, fill, center");
			panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.LABEL_COMPONENT_GAP_COLSPEC, ColumnSpec.decode("21px"),
					FormFactory.LABEL_COMPONENT_GAP_COLSPEC, ColumnSpec.decode("83px:grow"), }, new RowSpec[] { FormFactory.LINE_GAP_ROWSPEC,
					RowSpec.decode("21px"), FormFactory.DEFAULT_ROWSPEC, }));
			{
				this.chckbxIgnoreCase = new JCheckBox("");
				panel.add(this.chckbxIgnoreCase, "2, 2, left, top");
			}
			{
				JLabel lblIgnoreCase = new JLabel("ignore case");
				panel.add(lblIgnoreCase, "4, 2, left, center");
			}
			{
				JCheckBox chckbxTrim = new JCheckBox("");
				panel.add(chckbxTrim, "2, 3");
				chckbxTrim.setEnabled(false);
				chckbxTrim.setSelected(true);
			}
			{
				JLabel lblTrimLabelAnd = new JLabel("trim label and new label");
				panel.add(lblTrimLabelAnd, "4, 3");
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						RelationToAttributeInputDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("Convert");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						RelationToAttributeCriteria criteria = getCriteria();
						if (StringUtils.isBlank(criteria.getRelationAttributeLabel())) {
							//
							String title = "Invalid input";
							String message = "Please, enter a label not empty.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						} else {
							//
							lastCriteria = criteria;
							RelationToAttributeInputDialog.this.dialogCriteria = criteria;

							//
							setVisible(false);
						}

					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}

		// ////////////////////////
		setCriteria(lastCriteria);
	}

	/**
	 * 
	 * @return
	 */
	public RelationToAttributeCriteria getCriteria() {
		RelationToAttributeCriteria result;

		result = new RelationToAttributeCriteria();

		//
		result.setOptionalRelationName((String) this.cmbbxTarget.getSelectedItem());

		//
		result.setRelationAttributeLabel((String) this.cmbbxAttributeLabel.getSelectedItem());

		//
		result.setDateLabel(((String) this.cmbbxDateLabel.getSelectedItem()));

		//
		if (this.chckbxIgnoreCase.isSelected()) {
			//
			result.setCaseOption(AttributeWorker.CaseOption.IGNORE_CASE);

		} else {
			//
			result.setCaseOption(AttributeWorker.CaseOption.CASE_SENSITIVE);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public RelationToAttributeCriteria getDialogCriteria() {
		RelationToAttributeCriteria result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final RelationToAttributeCriteria source) {
		//
		if (source != null) {

			this.cmbbxTarget.setSelectedItem(source.getOptionalRelationName());

			//
			this.cmbbxAttributeLabel.setSelectedItem(source.getRelationAttributeLabel());

			//
			this.cmbbxDateLabel.setSelectedItem(source.getDateLabel());

			//
			if (source.isCaseSensitive()) {
				//
				this.chckbxIgnoreCase.setSelected(false);

			} else {
				//
				this.chckbxIgnoreCase.setSelected(true);
			}
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		/* AttributeFilter criteria = */showDialog(null, null);
	}

	/**
	 * Launch the application.
	 */
	public static RelationToAttributeCriteria showDialog(final List<String> relationModelNames, final AttributeDescriptors attributeDescriptors) {
		RelationToAttributeCriteria result;

		//
		RelationToAttributeInputDialog dialog = new RelationToAttributeInputDialog(relationModelNames, attributeDescriptors);
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
