package oldcore.trash;

public class RelationMaker {

	
/*	public static void applyModel1 (Net net, Segmentation segmentation, RelationModel model, int maxDistance){
		
		for (Individual ego : segmentation.getCurrentIndividuals()){
			
			Relation relation = net.createRelation(ego.getId(), "Ego = "+ego.getName()+" "+ego.getId(), model);
			setPrimaryTerms (ego,relation,model.roleDefinitions());
		}
		
		int previousActorCount = 0;
		int actorCount = net.relations().getByModel(model).actorCount();
		int distance = 1;
		
		System.out.println("distance "+distance+" relations added: "+actorCount);

		while (actorCount > previousActorCount && distance<maxDistance){

			for (Individual ego : segmentation.getCurrentIndividuals()){
				
				Relation relation = ego.relations().getByName(ego.getId()+" "+model.getName()).getFirst();
				if (relation!=null){
					setInverseTerms (ego,relation,model.roleDefinitions(), net);
				}
			}
			
			for (Individual ego : segmentation.getCurrentIndividuals()){
				
				Relation relation = ego.relations().getByName(ego.getId()+" "+model.getName()).getFirst();
				if (relation!=null){
					setCompositeTerms (ego,relation,model.roleDefinitions(), net);
				}
			}
			
			distance++;
			previousActorCount = actorCount;
			actorCount = net.relations().getByModel(model).actorCount();
			
			System.out.println("distance "+distance+" relations added: "+(actorCount - previousActorCount));
		}
	}
	
	private static void setPrimaryTerms (Individual ego, Relation relation, RoleDefinitions roleDefinitions){
		
		for (RoleDefinition roleDefinition : roleDefinitions){
			
			Role role = roleDefinition.role();
			Primary primary = roleDefinition.primary();
			
			if (primary==null || !genderMatch(roleDefinition.egoGender(),ego.getGender())){
				continue;
			}
			
			switch (primary){
			case PARENT:
				if (roleDefinition.alterGender()==Gender.MALE && ego.getFather()!=null){
					relation.addActor(ego.getFather(),role);
				} else if (roleDefinition.alterGender()==Gender.FEMALE && ego.getMother()!=null){
					relation.addActor(ego.getMother(),role);
				}
				break;
			case SIBLING:
				for (Individual sibling : ego.siblings()){
					if (genderMatch(roleDefinition.alterGender(),sibling.getGender())){
						relation.addActor(sibling, role);
					}
				}
				break;
			case SPOUSE:
				for (Individual spouse : ego.getPartners()){
					if (genderMatch(roleDefinition.alterGender(),spouse.getGender())){
						relation.addActor(spouse, role);
					}
				}
				break;
			}
			
			if (relation.actors()!=null){
				ego.relations().add(relation);
			}
			
		}
	}
	
	private static void setInverseTerms (Individual ego, Relation relation, RoleDefinitions roleDefinitions, Net net){
		
		for (RoleDefinition roleDefinition : roleDefinitions){
			
			Role role = roleDefinition.role();
			Role inverseRole = roleDefinition.inversion();
			
			if (inverseRole!=null) {

				for (Actor alterActor : relation.actors().getByRole(inverseRole.getName())){
					Individual alter = alterActor.getIndividual();
					
					if (genderMatch(roleDefinition.alterGender(),ego.getGender())){
						
						Relation alterRelation = alter.relations().getByName(alter.getId()+" "+relation.getModel().getName()).getFirst();
						if (!alterRelation.hasActor(ego, role.getName())){
							alterRelation.addActor(ego, role);
						}
						
						
					}
				}
			}
		}
	}
	
	private static void setCompositeTerms (Individual ego, Relation relation, RoleDefinitions roleDefinitions, Net net){
		
		for (RoleDefinition roleDefinition : roleDefinitions){
			
			Role role = roleDefinition.role();
			
			if (roleDefinition.composition()!=null && roleDefinition.composition().size()==2){
				Role mediusRole = roleDefinition.composition().get(0);
				Role alterRole = roleDefinition.composition().get(1);
				
				for (Actor mediusActor : relation.actors().getByRole(mediusRole.getName())){
					Individual medius = mediusActor.getIndividual();

					Relation mediusRelation = medius.relations().getByName(medius.getId()+" "+relation.getModel().getName()).getFirst();

					for (Actor alterActor : mediusRelation.actors().getByRole(alterRole.getName())){
						Individual alter = alterActor.getIndividual();
						
						if (genderMatch(roleDefinition.alterGender(),alter.getGender())){
							if (!relation.hasActor(alter, role.getName())){
								relation.addActor(alter, role);
							}
						}
					}					
				}
			}
		}
	}
	
	private static boolean genderMatch (Gender alphaGender, Gender betaGender){
		return (alphaGender==null || alphaGender == betaGender);
	}*/
	

}
