package org.tip.puck.matrix;

import org.tip.puck.util.NumberedIntegers;

/**
 * 
 * @author TIP
 */
public class SparseMatrix {
	private Matrix matrix;

	private NumberedIntegers idToCol;
	private NumberedIntegers idToRow;
	private NumberedIntegers colToId;
	private NumberedIntegers rowToId;

	/**
	 * 
	 */
	public SparseMatrix(final int nodeCount) {
		//
		this.matrix = new Matrix(nodeCount);

		//
		this.idToCol = new NumberedIntegers(nodeCount);
		this.idToRow = new NumberedIntegers(nodeCount);
		this.colToId = new NumberedIntegers(nodeCount);
		this.rowToId = new NumberedIntegers(nodeCount);
	}

	/**
	 * 
	 */
	public SparseMatrix(final int sourceCount, final int targetCount) {
		//
		this.matrix = new Matrix(sourceCount, targetCount);

		//
		this.idToCol = new NumberedIntegers(targetCount);
		this.idToRow = new NumberedIntegers(sourceCount);
		this.colToId = new NumberedIntegers(targetCount);
		this.rowToId = new NumberedIntegers(sourceCount);
	}

	/**
	 * 
	 * @param sourceNodeLabel
	 * @param targetNodeLabel
	 */
	public void add(final int sourceId, final int targetId, final double value) {
		//
		Integer rowIndex = getRowIndex(sourceId);
		if (rowIndex == null) {
			rowIndex = this.idToRow.size();
			this.idToRow.put(sourceId, rowIndex);
			this.rowToId.put(rowIndex, sourceId);
		}

		//
		Integer columnIndex = getColumnIndex(targetId);
		if (columnIndex == null) {
			columnIndex = this.idToCol.size();
			this.idToCol.put(targetId, columnIndex);
			this.colToId.put(columnIndex, targetId);
		}

		//
		this.matrix.augment(rowIndex, columnIndex, (int) value);
	}

	/**
	 * 
	 * @param sourceId
	 */
	public void addSource(final int sourceId) {
		//
		Integer rowIndex = getRowIndex(sourceId);
		if (rowIndex == null) {
			rowIndex = this.idToRow.size();
			this.idToRow.put(sourceId, rowIndex);
			this.rowToId.put(rowIndex, sourceId);
		}
	}

	/**
	 * 
	 * @param targetId
	 */
	public void addTarget(final int targetId) {
		Integer columnIndex = getColumnIndex(targetId);
		if (columnIndex == null) {
			columnIndex = this.idToCol.size();
			this.idToCol.put(targetId, columnIndex);
			this.colToId.put(columnIndex, targetId);
		}
	}

	/**
	 * 
	 * @param sourceId
	 * @param targetId
	 * @return
	 */
	public double get(final int sourceId, final int targetId) {
		double result;

		//
		Integer rowIndex = getRowIndex(sourceId);

		//
		Integer columnIndex = getColumnIndex(targetId);

		//
		if ((rowIndex == null) || (columnIndex == null)) {
			result = 0;
		} else {
			result = this.matrix.get(getRowIndex(sourceId), getColumnIndex(targetId));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetId
	 * @return
	 */
	public Integer getColumnIndex(final int targetId) {
		Integer result;

		//
		result = this.idToCol.get(targetId);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Matrix getMatrix() {
		Matrix result;

		result = this.matrix;

		//
		return result;
	}

	/**
	 * 
	 * @param sourceId
	 * @return
	 */
	public Integer getRowIndex(final int sourceId) {
		Integer result;

		result = this.idToRow.get(sourceId);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int getSourceCount() {
		int result;

		//
		result = this.matrix.getRowDim();

		//
		return result;
	}

	/**
	 * 
	 * @param rowIndex
	 * @return
	 */
	public int getSourceId(final int rowIndex) {
		int result;

		//
		result = this.rowToId.get(rowIndex);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int getSum() {
		int result;

		//
		result = this.matrix.getSum();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int getTargetCount() {
		int result;

		//
		result = this.matrix.getColDim();

		//
		return result;
	}

	/**
	 * 
	 * @param columnIndex
	 * @return
	 */
	public int getTargetId(final int columnIndex) {
		int result;

		//
		result = this.colToId.get(columnIndex);

		//
		return result;
	}

	/**
	 * 
	 * @param sourceId
	 * @param targetId
	 */
	public void inc(final int sourceId, final int targetId) {
		add(sourceId, targetId, 1);
	}

	/**
	 * 
	 * @param sourceNodeLabel
	 * @param targetNodeLabel
	 * @param weight
	 */
	public void set(final int sourceId, final int targetId, final double weight) {
		//
		Integer rowIndex = getRowIndex(sourceId);
		if (rowIndex == null) {
			rowIndex = this.idToRow.size();
			this.idToRow.put(sourceId, rowIndex);
			this.rowToId.put(rowIndex, sourceId);
		}

		//
		Integer columnIndex = getColumnIndex(targetId);
		if (columnIndex == null) {
			columnIndex = this.idToCol.size();
			this.idToCol.put(targetId, columnIndex);
			this.colToId.put(columnIndex, targetId);
		}

		//
		this.matrix.set(rowIndex, columnIndex, (int) weight);
	}
}
