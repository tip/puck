package oldcore.trash;

import java.awt.Color;

import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.util.StringUtils;

/**
 * This static class contains a number of auxiliary methods for string and object transformation and inspection
 * @author Klaus Hamberger
 * @since Version 0.6
 *
 */
public abstract class Trafo {
	
	/**
	 * compares two objects
	 * @param o1 the first object
	 * @param o2 the second object
	 * @param numeric true if the objects are of a numeric type
	 * @return the comparison (-1, 0, 1)
	 * @see groups.KinGroup.KinComparator#compare(Vertex,Vertex)
	 */
/*	public static int compare(Object o1, Object o2, boolean numeric) {
		if (numeric) return Trafo.asInt(o1).compareTo(Trafo.asInt(o2));
		else return Trafo.asString(o1).compareTo(Trafo.asString(o2));
	}*/

    /**
	 * returns the integer representation of an object (and 0 if the object is null)
	 * <p> throws NumberFormatException (important for partitions.NodePartiton#numeric())
	 * @param obj the object
	 * @return the integer representation of the object
	 * @since last modified 10-05-31
	 */
/*	public static Integer asInt (Object obj) {
		if (StringUtils.isNull(obj)) return 0;
		if (obj instanceof String) {
			try {
				return Integer.parseInt((String)obj);
			} catch (NumberFormatException nfe) {
//				try {
					return new Double((String)obj).intValue();
//				} catch (NumberFormatException nfe2) {
//					return 0;
//				}
			}
		}
		if (obj instanceof Integer) return (Integer)obj;
		if (obj instanceof Double) return ((Double)obj).intValue();
		return 0;
	}*/
    
    /**
	 * returns the string representation of an object (and "" if the object is null)
	 * @param obj the object
	 * @return the string representation of the object
	 */
/*	public static String asString (Object clu){
		if (clu == null) return "";
		if (clu instanceof Integer) return ((Integer)clu).toString().trim();
		if (clu instanceof String) return ((String)clu).trim();
		if (clu instanceof Individual) return ((Individual)clu).getName();
		if (clu instanceof Double) return ((Double)clu).toString().trim();
		if (clu instanceof Boolean) return ((Boolean)clu).toString().trim();
		return "";
	}*/

	/**
	 * returns an array as a tabstop-separated string
	 * @param a the array
	 * @return the string
	 */
/*	public static String arrayAsString(double[] a){
		String s ="";
		for (double o : a){
			s=s+o+"\t";
		}
		return s;
	}*/
	
	
	/**
	 * a shortcut that returns a system-dependent file separator
	 * @return the system-dependent file separator
	 */
/*	public static String sep(){
		return System.getProperty("file.separator");
	}*/
	
	/**
	 * replaces blanks of a string by underscores
	 * <p> (some systems do not accept blanks in filenames)
	 * @param str the string to be transformed
	 * @return the string without blanks
	 * @see gui.screens.Screen#getNetName(boolean)
	 * @see gui.screens.StartScreen#open(String, int)
	 */
/*	public static String wb (String str){
		return str.replaceAll(" ","_");
	}*/
	
	/**
	 * gets a randomly generated color
	 * @return a random color
	 * @see gui.screens.DatasetScreen#DatasetScreen(AbstractIOScreen, Net, String)
	 */
/*	public static Color randomColor () {
		int[] c = new int[3];
		for (int i=0;i<3;i++) {c[i]=50+new Double(Math.floor(Math.random()*200)).intValue();}
		return (new Color(c[0],c[1],c[2])).brighter().brighter();       
	}*/
	
//Trash	

	/*	public static int toInt (String str){
	if (str==null) return 0;
	return Integer.parseInt(str);
	}

	public static Integer asInteger (Object obj){
		try {
			if (obj instanceof Double) return ((Double)obj).intValue();
			return (Integer)obj;
		} catch (ClassCastException cce) { 
			String s = (String)obj;
			try {
				return Integer.parseInt(s);
			} catch (NumberFormatException nfe) {
				return new Double(s).intValue();
			}
		}
	}

    public static boolean unequals (int s1, int s2) {
    	if (s1 == -1 || s1==s2) return false;
    return true;
}

 public static boolean belongsTo (String str, String[] labels) {
	 for (String lb : labels) {
		 if (lb.equals(str)) return true;
	 }
	 return false;
 }

public static String op (boolean b, String s) {
      if (b) return s;
      return "";
}

	public static String get (String str){
		if (str==null) return "";
		return str;
	}*/
	

}
