package org.tip.puckgui.views;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.report.ReportChart;
import org.tip.puck.report.ReportChartMaker;

/**
 * 
 * @author TIP
 */
public class ReportChartPanelMaker {

	private static final Logger logger = LoggerFactory.getLogger(ReportChartPanelMaker.class);

	/**
	 * 
	 * @param reportChart
	 * @return
	 */
	public static JPanel createPanel(final ReportChart reportChart) {
		JPanel result;

		switch (reportChart.getType()) {
			case BAR_BOARD:
			case SURFACE:
				result = new JPanel();
				result.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229), 1, true), reportChart.getTitle(), TitledBorder.LEADING,
						TitledBorder.TOP, null, new Color(51, 51, 51)));
				result.setLayout(new BoxLayout(result, BoxLayout.X_AXIS));
				JLabel jlabel = new JLabel(new ImageIcon(ReportChartMaker.createBufferedImage(reportChart, 800, 600)));
				jlabel.setBorder(null);
				result.add(jlabel);
			break;

			default:
				// Build dataset for JFreeChart.
				JFreeChart chart = ReportChartMaker.createJFreeChart(reportChart);
				result = new ChartPanel(chart);
		}

		// Build panel.
		result.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229), 1, true), reportChart.getTitle(), TitledBorder.LEADING, TitledBorder.TOP,
				null, new Color(51, 51, 51)));

		//
		return result;
	}

	/**
	 * 
	 * @param reportChart
	 * @return
	 */
	public static JPanel createPanel(final ReportChart reportChart, final int width, final int height) {
		JPanel result;

		result = createPanel(reportChart);
		result.setPreferredSize(new java.awt.Dimension(width, height));
		result.setMaximumSize(new java.awt.Dimension(width, height));

		//
		return result;
	}

	/**
	 * 
	 * @param reportChart
	 * @param width
	 * @param height
	 * @return
	 */
	public static JPanel createSimplePanel(final ReportChart reportChart, final int width, final int height) {
		JPanel result;

		// Build panel.
		result = new JPanel();
		result.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229), 1, true), reportChart.getTitle(), TitledBorder.LEADING, TitledBorder.TOP,
				null, new Color(51, 51, 51)));
		result.setLayout(new BoxLayout(result, BoxLayout.X_AXIS));

		JLabel jlabel = new JLabel(new ImageIcon(ReportChartMaker.createBufferedImage(reportChart, width, height)));
		jlabel.setBorder(null);
		result.add(jlabel);
		jlabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				JPanel chartPanel;
				chartPanel = createPanel(reportChart);
				ChartWindow window = new ChartWindow(chartPanel);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			};
		});

		//
		result.setBounds(0, 0, width, height);

		//
		return result;
	}
}
