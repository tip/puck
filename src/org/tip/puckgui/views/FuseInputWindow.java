package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckManager;
import org.tip.puck.net.Net;
import org.tip.puck.report.Report;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.PuckGUI;
import org.tip.puckgui.util.GenericFileFilter;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class FuseInputWindow extends JFrame {

	private static final long serialVersionUID = -559628922664111960L;
	private static final Logger logger = LoggerFactory.getLogger(FuseInputWindow.class);

	private JFrame thisJFrame;
	private NetGUI netGUI;
	private static File sourceFile;
	private static File concordanceFile;
	private JPanel contentPane;
	private JTextField txtfldSourceFile;
	private JTextField txtfldConcordanceFile;

	/**
	 * Create the frame.
	 */
	public FuseInputWindow(final NetGUI netGUI) {
		setAlwaysOnTop(true);
		setIconImage(Toolkit.getDefaultToolkit().getImage(FooReporterInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		this.netGUI = netGUI;
		setTitle("Fuse Inputs");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 547, 200);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		this.contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));

		JLabel lblNewLabel = new JLabel("File to be joined:");
		panel.add(lblNewLabel, "2, 4, right, default");

		this.txtfldSourceFile = new JTextField();
		this.txtfldSourceFile.setEditable(false);
		panel.add(this.txtfldSourceFile, "4, 4, fill, default");
		this.txtfldSourceFile.setColumns(10);

		JButton btnSelectSourceFile = new JButton("...");
		btnSelectSourceFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Select source file.
				File file = MainWindow.selectFile(FuseInputWindow.this.thisJFrame, null);
				if (file != null) {
					sourceFile = file;
					FuseInputWindow.this.txtfldSourceFile.setText(file.getAbsolutePath());
				}
			}
		});

		JButton buttonClearSourceFile = new JButton("");
		buttonClearSourceFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Clear source file.
				FuseInputWindow.this.txtfldSourceFile.setText("");
				sourceFile = null;
			}
		});
		buttonClearSourceFile.setIcon(new ImageIcon(FuseInputWindow.class.getResource("/org/tip/puckgui/images/edit-clear-16x16.png")));
		panel.add(buttonClearSourceFile, "6, 4");
		panel.add(btnSelectSourceFile, "8, 4");

		JLabel lblNewLabel_1 = new JLabel("Concordance table:");
		panel.add(lblNewLabel_1, "2, 8, right, default");

		this.txtfldConcordanceFile = new JTextField();
		this.txtfldConcordanceFile.setEditable(false);
		panel.add(this.txtfldConcordanceFile, "4, 8, fill, default");
		this.txtfldConcordanceFile.setColumns(10);

		JButton btnSelectConcordanceFile = new JButton("...");
		btnSelectConcordanceFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Select concordance file.
				File file = selectConcordanceFile(FuseInputWindow.this.thisJFrame, null);
				if (file != null) {
					concordanceFile = file;
					FuseInputWindow.this.txtfldConcordanceFile.setText(file.getAbsolutePath());
				}
			}
		});

		JButton buttonClearConcordanceFile = new JButton("");
		buttonClearConcordanceFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Clear concordance file.
				FuseInputWindow.this.txtfldConcordanceFile.setText("");
				concordanceFile = null;
			}
		});
		buttonClearConcordanceFile.setIcon(new ImageIcon(FuseInputWindow.class.getResource("/org/tip/puckgui/images/edit-clear-16x16.png")));
		panel.add(buttonClearConcordanceFile, "6, 8");
		panel.add(btnSelectConcordanceFile, "8, 8");

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					if ((sourceFile == null) || (!sourceFile.exists())) {
						//
						String title = "Bad Inputs";
						String message = "Please, enter valid data.";

						//
						JOptionPane.showMessageDialog(FuseInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
					} else {
						//
						if (sourceFile != null) {
							Report report = new Report("Merging report");
							Net fusedNet = PuckManager.fuseNet(netGUI.getNet(), sourceFile, concordanceFile, report);
							
							//
							NetGUI newNetGUI = PuckGUI.instance().createNetGUI(netGUI.getFile(), fusedNet);
							newNetGUI.setChanged(true, "-fused");
							if (report.outputs().isNotEmpty()){
								newNetGUI.addReportTab(report);
							}
							dispose();
						}
					}
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(FuseInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);

		// /////////////////////////
		if (sourceFile != null) {
			this.txtfldSourceFile.setText(sourceFile.getAbsolutePath());
		}

		if (concordanceFile != null) {
			this.txtfldConcordanceFile.setText(concordanceFile.getAbsolutePath());
		}
	}

	/**
	 * This classes is a static one to be called by OpenRecentMenuItem.java
	 * 
	 * 
	 * @param netGUI
	 * @param parentPanel
	 * @param folder
	 */
	public static File selectConcordanceFile(final JFrame parentPanel, final File folder) {
		File result;

		//
		JFileChooser chooser = new JFileChooser();
		if ((folder != null) && (folder.exists()) && (folder.isDirectory())) {
			chooser.setCurrentDirectory(folder);
		} else if (PuckGUI.instance().recentFolders().isEmpty()) {
			chooser.setCurrentDirectory(new java.io.File("."));
		} else {
			chooser.setCurrentDirectory(PuckGUI.instance().recentFolders().getMoreRecent());
		}

		chooser.setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.openFileChooser.text"));
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setAcceptAllFileFilterUsed(true);
		GenericFileFilter defaultFileFilter = new GenericFileFilter("Genealogic files (*.txt, *.xls, *.ods)", "ods", "txt", "xls");
		chooser.addChoosableFileFilter(defaultFileFilter);
		chooser.addChoosableFileFilter(new GenericFileFilter("OpenDocumentFormat Spreadsheet (*.ods)", "ods"));
		chooser.addChoosableFileFilter(new GenericFileFilter("Text (*.txt)", "txt"));
		chooser.addChoosableFileFilter(new GenericFileFilter("Microsoft Excel (*.xls)", "xls"));
		chooser.setFileFilter(defaultFileFilter);

		//
		if (chooser.showOpenDialog(parentPanel) == JFileChooser.APPROVE_OPTION) {
			result = chooser.getSelectedFile();
			logger.debug("Selection of " + result);
		} else {
			logger.debug("No Selection ");
			result = null;
		}

		//
		return result;
	}

}
