package org.tip.puck.report;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.tip.puck.util.Value;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class ReportAttributes extends ArrayList<ReportAttribute> {
	private static final long serialVersionUID = -714200972773172623L;

	/**
	 * 
	 */
	public ReportAttributes() {
		super();
	}

	/**
	 * 
	 * @param source
	 */
	public void add(final ReportAttributes source) {
		for (ReportAttribute item : source) {
			add(item.label(), item.value());
		}
	}

	/**
	 * 
	 * @param lalel
	 * @param value
	 */
	public void add(final String label, final boolean value) {
		this.add(new ReportAttribute(label, String.valueOf(value)));
	}

	/**
	 * 
	 * @param lalel
	 * @param value
	 */
	public void add(final String label, final Calendar source) {
		String value;
		if (source == null) {
			value = null;
		} else {
			value = (new SimpleDateFormat("dd/MM/yyyy")).format(source);
		}

		this.add(new ReportAttribute(label, value));
	}

	/**
	 * 
	 * @param lalel
	 * @param value
	 */
	public void add(final String label, final double value) {
		this.add(new ReportAttribute(label, String.valueOf(value)));
	}

	/**
	 * 
	 * @param lalel
	 * @param value
	 */
	public void add(final String label, final float value) {
		this.add(new ReportAttribute(label, String.valueOf(value)));
	}

	/**
	 * 
	 * @param lalel
	 * @param value
	 */
	public void add(final String label, final int value) {
		this.add(new ReportAttribute(label, String.valueOf(value)));
	}

	/**
	 * 
	 * @param lalel
	 * @param value
	 */
	public void add(final String label, final String value) {
		this.add(new ReportAttribute(label, value));
	}

	/**
	 * 
	 * @param lalel
	 * @param value
	 */
	public void add(final String label, final Value value) {
		this.add(new ReportAttribute(label, value.toString()));
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		StringList buffer = new StringList();
		for (ReportAttribute attribute : this) {
			buffer.appendln(String.format("%s\t%s", attribute.label(), attribute.value()));
		}

		result = buffer.toString();

		//
		return result;
	}
}
