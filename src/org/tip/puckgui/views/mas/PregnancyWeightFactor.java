package org.tip.puckgui.views.mas;

/**
 * 
 * @author TIP
 */
public class PregnancyWeightFactor implements WeightFactor {

	private final Type type = Type.PREGNANCY;
	private double first;

	/**
	 * 
	 */
	public PregnancyWeightFactor() {
		this.first = 0;
	}

	public double getFirst() {
		return first;
	}

	@Override
	public Type getType() {
		return type;
	}

	public void setFirst(final double first) {
		this.first = first;
	}
}
