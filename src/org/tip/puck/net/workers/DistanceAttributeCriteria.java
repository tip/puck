package org.tip.puck.net.workers;

import org.tip.puck.net.FiliationType;

/**
 * 
 * @author TIP
 */
public class DistanceAttributeCriteria {

	private FiliationType filiationType;
	private int maxDistance;

	/**
	 * 
	 */
	public DistanceAttributeCriteria() {

		this.filiationType = FiliationType.COGNATIC;
		this.maxDistance = 100;
	}

	public FiliationType getFiliationType() {
		return this.filiationType;
	}

	public int getMaxDistance() {
		return this.maxDistance;
	}

	public void setFiliationType(final FiliationType filiationType) {
		this.filiationType = filiationType;
	}

	public void setMaxDistance(final int maxDistance) {
		this.maxDistance = maxDistance;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public static boolean isNotValid(final DistanceAttributeCriteria criteria) {
		boolean result;

		result = !isValid(criteria);

		//
		return result;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public static boolean isValid(final DistanceAttributeCriteria criteria) {
		boolean result;

		if ((criteria == null) || (criteria.getFiliationType() == null) || (criteria.getMaxDistance() < 0)) {
			//
			result = false;

		} else {
			//
			result = true;
		}

		//
		return result;
	}

}
