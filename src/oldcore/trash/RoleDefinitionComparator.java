package oldcore.trash;

import java.util.Comparator;

import org.tip.puck.util.PuckUtils;

public class RoleDefinitionComparator implements Comparator<RoleDefinition>{

	public int compare(RoleDefinition alpha, RoleDefinition beta){
		int result;
		
		result = PuckUtils.compare(alpha.role().getName(), beta.role().getName());
		
		//
		return result;
	}
	
	
}
