package oldcore.trash;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;



import org.tip.puck.census.chains.Chain;
import org.tip.puck.census.chains.Notation;
import org.tip.puck.util.MathUtils;

public class ClusterNetwork<E extends Clusterable>  {

	protected Partition<E> partition;
	Partition<E> partition2;
	Matrix matrix;
	
	Map<Link<Cluster<E>>,List<String>> links;


	
/*	private Cluster<E> get(int i){
		return partition.get(i);
	}
	
	private int size(){
		return partition.size();
	}
	
	private int nrItems(){
		return partition.nrItems();
	}
	
	private boolean add(Cluster<E> cluster){
		return partition.add(cluster);
	}*/
	

	

	
	/**
	 * constructs a CLusterNetwork for a given number of clusters
	 * 
	 * @param size
	 */
/*	public ClusterNetwork(final int size) {
		matrix = new Matrix(size);
		for (int k = 0; k < size; k++) {
			Cluster<E> cluster = new Cluster<E>();
			cluster.setValueTo(k);
			add(cluster);
		}
	}*/



	
	/**
	 * gets the allies (wife givers or wife takers) of a given Cluster
	 * 
	 * @param e
	 *            the id of the ego node
	 * @param giversToEgo
	 *            true if wife givers are demanded
	 * @return the set of ally nodes
	 */
/*	protected Set<Cluster<E>> getAllies(final int e, final boolean giversToEgo) {
		if (giversToEgo) {
			return get(e).getGivers();
		} else {
			return get(e).getTakers();
		}
	}*/

	// Check
	/**
	 * gets the greatest possible value of a link between two vertices of a
	 * GroupNet
	 * 
	 * @param v
	 *            the ego vertex
	 * @param w
	 *            the alter vertex
	 * @return the greatest possible value of a link from ego to alter
	 * @see maps.OldGroupNet#getCensus()
	 */
/*	private int pValue(final Cluster<E> v, final Cluster<E> w) {
		return Math.min(v.getOutDegree(), w.getInDegree());
	}*/


	
	// cf. Puck headlines
/*	private String t(int key) {
		return "";
	}*/
	   
	   
	/**
	 * decomposes a circuit intersection network
	 * @return the decomposition report as a string list
	 * @see io.write.AbstractWriter#writeDecomposition(RingGroupMap)
	 * @see gui.screens.StatsScreen#show(String[])
	 */
/*	public double[][] decompose () {
		double[][] result;
		
		ArrayList<String> protocol = new ArrayList<String>();
		ArrayList<Double> tot = new ArrayList<Double>();
		ArrayList<Double> siz = new ArrayList<Double>();
		Map<Cluster<E>,Integer> originalSizes = new HashMap<Cluster<E>,Integer>(); 
		
		int k = 1;
		for (Cluster<E> cluster : partition) {
			originalSizes.put(cluster,cluster.size());
		}
		
		int originalNrItems = nrItems();
		int originalNrClusters = size();
		
		while (size()>0) {
			Cluster<E> maxCluster = partition.maxCluster();
			String s = t(24);
			if (maxCluster.size()>1) s = t(25);
			double survivingItems = MathUtils.percent(nrItems(),originalNrItems);
			double survivingClusters = MathUtils.percent(size(),originalNrClusters);
			int maxClusterOriginalSize = originalSizes.get(maxCluster);
			double survivalRate = MathUtils.percent(maxCluster.size(),maxClusterOriginalSize);
			
			tot.add(survivingItems);
			siz.add(survivingClusters);
			protocol.add(k+". "+maxCluster.signature(0)+"\t"+maxCluster.size()+" "+s+" "+t(26)+" "+maxClusterOriginalSize+" ("+survivalRate+"%)\t"+nrItems()+" "+t(25)+" "+t(10)+" "+size()+" "+t(27)+" ("+survivingItems+"% / "+survivingClusters+"%)\t"+reduce(maxCluster));
	        k = k+1;
		}
		
		// Report
		for (String str : protocol){
			System.out.println(str);
		}

		result = new double[2][tot.size()];
		for (int i=0;i<tot.size();i++){
			result[0][i] = tot.get(i);
			result[1][i] = siz.get(i);
		}
		
		//
		return result;
	}*/
	
	   /**
	    * Removes the chains of a given ChainGroup from all the ChainGroups of the RingGroupMap, and removes the ChainGroups which are empty, and returns the report on removed chains
	    * @param c the ChainGroup containing the chains to be removed
	    * @return the report of chain removal as a string
	    * @see RingGroupMap#decompose()
	    */
/*	   private String reduce (Cluster<E> c) {
		  Cluster<E> removed = new Cluster<E>();
	      String str = "";
	      for (Cluster<E> cl : partition) {
	    	 int s = cl.size();
	         cl.reduce(c);
	         if (cl.size()<s) removed.add(cl,s);
	      }
	      removed.sortByDecomposition();
	      for (Chain r : removed) {
	         str = str+", "+r.getDecomp()+"% "+r.signature1(2).substring(2);
	         if (r.getDecomp()==100.) remove(r.getCharacteristicVector());
	      }
	      remove(c.getModel().getVector());
	      if (str=="") return t(28);
	      return t(29)+" : "+str.substring(2);
	   }*/
	


	/**
	 * gets the value of a link between two vertices of the GroupNet
	 * 
	 * @param v
	 *            the ego vertex
	 * @param w
	 *            the alter vertex
	 * @return the value of the link from ego to alter
	 * @see maps.OldGroupNet#getCensus()
	 */
/*	private int value(final Cluster<E> v, final Cluster<E> w) {
		return v.with(w);
	}*/

	
	/**
	 * 
	 * @return
	 */
/*	public String getPartitionLabel()
	{
		String result;
		
		if (this.partition == null) {
			result= null;
		}
		else {
			result = this.partition.getLabel();
		}
				
		//
		return result;				
	}*/

}
