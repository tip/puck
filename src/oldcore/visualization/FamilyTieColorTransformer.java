package oldcore.visualization;

import java.awt.Color;

import org.gephi.graph.api.*;
import org.gephi.ranking.api.*;
import org.gephi.ranking.impl.*;

public class FamilyTieColorTransformer  extends AbstractColorTransformer<Edge> implements ObjectColorTransformer<Edge> {
	
	FamilyTieColorTransformer(EdgeRanking<Edge> er){
		setRanking(er);
	}
	
	public Object transform(Edge target, float normalizedValue) {
		int unNormalizedValue = (Integer) ranking.unNormalize(normalizedValue);
        Color color = null;
        switch(unNormalizedValue){
        case 1:
        	color = new Color(5, 5, 5, 255);//Color.darkGray;
        	break;
        case 2: 
        	color = new Color(255, 0, 0, 255); //Color.red;
        	break;
        case 3 :
        	color = new Color(192, 192, 192, 0); //Color.lightGray;
        }
        target.getEdgeData().setColor(color.getRed() / 255f, color.getGreen() / 255f, color.getBlue() / 255f);
        target.getEdgeData().setAlpha(color.getAlpha() / 255f);
        return color;
    }
	
}
