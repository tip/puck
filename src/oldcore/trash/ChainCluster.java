package oldcore.trash;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.tip.puck.census.chains.Chain;
import org.tip.puck.net.FiliationType;


/**
 * A class for groups of vertex chains
 * 
 * @author Klaus Hamberger
 * @since Version 0.7
 * @param <T>
 *            the type of chain
 */
@SuppressWarnings("serial")
public class ChainCluster extends Cluster<Chain> {

	/**
	 * the label of the group
	 */
	private String label;
	/**
	 * the representative element of the group
	 */
	private Chain model;
	/**
	 * the ID number of the group
	 */
	private int nr;
	/**
	 * the (invariant) number of pivots of the chains of the group
	 */
	private int nrPivots;
	/**
	 * the type of the chains of the group
	 */
	private int type;
	/**
	 * the value of the group
	 */
	private int value;



	/**
	 * the original size of the group
	 */
	private int orSize;



	/**
	 * constructs a RingGroup from a formula
	 * 
	 * @param crossSex
	 *            true if only cross-sex marriages are allowed
	 * @param str
	 *            the VertexChain formula (in positional notation)
	 * @param sib
	 *            the sibling mode
	 * @param line
	 *            the filter criterion for ascendant lines (0 agnatic, 1
	 *            uterine, 2 cognatic)
	 * @param restriction
	 *            the code for cluster anchorage (0 no limitation, 1 at least
	 *            one pivot in the cluster, 2 all pivots in the cluster, 3 the
	 *            last married pivot in the cluster (for periodization), 4 ego
	 *            in cluster)
	 * @see gui.components.Button#actionPerformed(java.awt.event.ActionEvent)
	 * @see gui.screens.Calculator#go()
	 */
/*	public ChainCluster(final boolean crossSex, final String str, final SiblingMode sib, final FiliationType line, final RestrictionType restriction,
			final SymmetryType sym) {
		this.line = line;
		this.restriction = restriction;
		this.sym = sym;
		final boolean cs = crossSex && (restriction != RestrictionType.EGOALTER);
		if (str == null) {
			return;
		}
		if (str.charAt(0) == '<') {
			for (String form : formulae(str.substring(1))) {
				addAll(new ChainCluster(cs, form, sib, line, restriction, sym));
			}
			setModel(get(0));
			return;
		}
		for (String s : str.split("\\s")) {
			// s = s.replaceAll("\\(X\\)", "\\(Y\\)");
			develop(cs, transform(s), 0, sib);
		}
		setModel(get(0));
	}*/

	/**
	 * constructs a RingGroup from a model Ring
	 * 
	 * @param r
	 *            the model Ring
	 * @param n
	 *            the ID number of the Group
	 * @see OldRing.Ring#getDoubles(boolean)
	 * @see OldRing.Ring#newGroup(int)
	 */
/*	public ChainCluster(final Chain r, final int n) {
		model = r.clone();
		nr = n;
		if (getID() >= 0) {
			return;
		}
		for (int i = 0; i < r.dim() * 2; i++) {
			addModel(r.transform(i));
		}
	}*/

	/**
	 * adds the model of another RingGroup to the Group
	 * 
	 * @param cl
	 *            the second RingGroup
	 * @param s
	 *            a reference number (used for the original size of the second
	 *            Group)
	 * @see maps.groupmaps.RingGroupMap#reduce(ChainGroup)
	 */
/*	public void add(final ChainCluster cl, final int s) {
		// cl.getModel().setDecomp(Mat.percent(s-cl.size(), s));
		add(cl.getModel());
	}*/

	/**
	 * adds a Ring if it is not isomorphic to another Ring in the Group
	 * 
	 * @param r
	 *            the Ring to be added
	 * @see groups.RingGroup#develop(boolean, String, int, int)
	 * @see groups.RingGroup#RingGroup(OldRing, int)
	 */
/*	private void addModel(final Chain r) {
		if (containsModel(r)) {
			return;
		}
		add(r);
	}*/

	/**
	 * gets the list of combinations of rings of the Rings of the Group with
	 * that of another Group
	 * 
	 * @param c
	 *            the second RingGroup
	 * @return the list of combined Rings
	 * @see OldRing.Ring#combine(OldRing)
	 * @see gui.components.Button#actionPerformed(java.awt.event.ActionEvent)
	 */
/*	public ArrayList<String> combine(final ChainCluster c) {
		ArrayList<String> list = new ArrayList<String>();
		String str = "";
		for (Chain s : c) {
			s = s.standard();
			str = str + "\t" + s.signature1(7);
		}
		list.add(str);
		for (Chain r : this) {
			str = r.signature1(7);
			for (Chain q : c) {
				Chain s = r.getModel().combine(q.getModel());
				try {
					str = str + "\t" + s.standard().signature1(7);
				} catch (NullPointerException e) {
					str = str + "\t";
				} catch (StringIndexOutOfBoundsException siob) {
					str = str + "\t";
				}
			}
			list.add(str);
		}
		return list;
	}*/

	/**
	 * compares the VertexChainGroup with another
	 * 
	 * @param e
	 *            the second VertexChainGroup
	 * @return the comparison (-1,0,1)
	 */
/*	@SuppressWarnings("unchecked")
	public int compareTo(final ChainCluster e) {
		return getModel().compareTo(e.getModel());
	}*/

	/**
	 * checks whether the Group contains a Ring of the same form as given Ring
	 * 
	 * @param r
	 *            the Ring to be checked
	 * @return true if the Group conains already a Ring of that form
	 */
/*	private boolean containsModel(final Chain r) {
		for (Chain s : this) {
			if (s.signature(Notation.POSITIONAL).equals(r.signature(Notation.POSITIONAL))) {
				return true;
			}
		}
		return false;
	}*/

	/**
	 * counts the couples involved in the Rings of the Group
	 * 
	 * @param couples
	 *            the list of couples
	 * @param reset
	 *            true if the list is emptied at the beginning of the counting
	 *            process
	 * @return the number of couples involed in the Rings of the Group
	 * @see maps.groupmaps.RingGroupMap#countCouples()
	 * @see io.write.TxtWriter#writeSurvey(RingGroupMap,CountMap,int)
	 */
/*	public int countCouples(List<Chain> couples, final boolean reset) {
		if (reset) {
			couples = new ArrayList<Chain>();
		}
		for (Chain r : this) {
			for (Chain c : r.getCouples()) {
				if (!(contains(couples, c))) {
					couples.add(c);
				}
			}
		}
		return couples.size();
	}*/



	/**
	 * gets the order of the Rings of the Group
	 * 
	 * @return the order of the Rings of the Group
	 * @see maps.groupmaps.RingGroupMap#countCircuitClusters(RingGroupMap)
	 */
/*	public int dim() {
		return getModel().dim();
	}*/

	/**
	 * checks whether the VertexChainGroup equals another Group
	 * 
	 * @param c
	 *            the VertexChainGroup to be compared
	 * @return true if the two VertexChainGroup are equal
	 */
	@SuppressWarnings("unchecked")
/*	public boolean equals(final ChainCluster e) {
		return getModel().equals(e.getModel());
	}*/





	/**
	 * gets the cluster to which the Chains of the Group belong
	 * 
	 * @param cmd
	 *            the partition command
	 * @return the cluster to which the Chains of the Group belong
	 */
/*	public Object getCluster(final String cmd) {
		return getModel().getHeading(cmd);
	}*/

	/**
	 * gets the ID of the VertexChainGroup
	 * 
	 * @return the ID of the VertexChainGroup
	 */
/*	public int getID() {
		return nr;
	}*/

	/**
	 * gets the label of the VertexChainGroup
	 * 
	 * @return the label of the VertexChainGroup
	 */
/*	public String getLabel() {
		return label;
	}*/

	/**
	 * lists the signatures of the Chains of the Group
	 * 
	 * @param schema
	 *            the underlying Chain formula
	 * @return the signature list of the Chains of the Group
	 * @see gui.components.Button#actionPerformed(java.awt.event.ActionEvent)
	 */
/*	public ArrayList<String> getList(final String schema) {
		ArrayList<String> report = new ArrayList<String>();
		// report.add(size()+" "+Puck.getHeadline(14,26)+" "+schema);
		report.add("");
		for (Chain r : this) {
			report.add(r.signature(Notation.NUMBERS));
		}
		return report;
	}*/





	/**
	 * the characteristic vector of the circuits of the group
	 */
	// private int[] vector;
	// int count;

	/**
	 * gets the model Chain of the VertexChainGroup
	 * 
	 * @return the model Chain
	 */
/*	public Chain getModel() {
		return model;
	}*/

	/**
	 * gets the number of Pivots of a VertexChain of the Group
	 * 
	 * @return the number of Pivots of a Chain
	 * @see io.write.TxtWriter#writeSurvey(RingGroupMap,CountMap,int)
	 */
/*	public int getNrPivots() {
		return nrPivots;
	}*/

	/**
	 * gets the original size of the Group
	 * 
	 * @see maps.groupmaps.RingGroupMap#decompose()
	 */
/*	public int getOriginalSize() {
		return orSize;
	}*/

	/**
	 * gets the type of the VertexChainGroup
	 * 
	 * @return the type of the VertexChainGroup
	 */
/*	public int getType() {
		return type;
	}*/

	/**
	 * gets the value of the VertexChainGroup
	 * 
	 * @return the value of the VertexChainGroup
	 */
/*	public int getValue() {
		return value;
	}*/

	/**
	 * gets the characteristic vector of the Chains of the Group
	 * 
	 * @return the characteristic vector
	 */
/*	public int[] getCharacteristicVector() {
		return getModel().getCharacteristicVector();
	}*/

	// for circuitIntersectionNetwork; directely integrated
	/**
	 * gets a Group of non-isomorphic nrPivots of a VertexChainGroup
	 * <p>
	 * e.g. the non-isomorphic chains connecting the same couple (used in the
	 * construction of circuit intersection networks)
	 * 
	 * @return the Group of non-isomorphic nrPivots
	 * @see maps.GroupNet#ClassNet(RingGroupMap, boolean)
	 */
	/*
	 * public ChainCluster getModels () { ChainCluster c = new ChainCluster();
	 * for (Chain r : this) { boolean isNew = true; for (Chain t : c) { if
	 * (r.getID()==t.getID()) isNew = false; } if (isNew) c.add(r); } return c;
	 * }
	 */

	/**
	 * checks whether the group contains only linear chains
	 * 
	 * @return true if the group contains only linear chains
	 */
/*	public boolean isLinear() {
		for (Chain r : this) {
			if (!r.linear()) {
				return false;
			}
		}
		return true;
	}*/

	/**
	 * gets the (previously counted) number of Chains in another RingGroupMap
	 * that are of the same type as the Chains of the Group
	 * <p>
	 * used for comparing the numbers of open and closed Chains
	 * 
	 * @param ties
	 *            the RingGroupMap to be compared
	 * @return the number of corresponding Chains
	 * @see io.write.TxtWriter#writeSurvey(RingGroupMap,CountMap,int)
	 */
/*	public int nrTies(final RingGroupMap ties) {
		Integer i = ties.getFrequency(getVector());
		if (i == null) {
			return 0;
		}
		return i;
	}*/

	/**
	 * sets the label of the ChainGroup
	 * 
	 * @param label
	 *            the label of the Group
	 */
/*	public void setLabel(final String label) {
		this.label = label;
	}*/

	/**
	 * sets the model element of the group
	 * 
	 * @param model
	 *            the model of the group
	 */
/*	public void setModel(final Chain model) {
		this.model = model;
	}*/

	/**
	 * sets the number of Pivots of a Chain of the Group
	 * 
	 * @param nrPivots
	 *            the number of Pivots of a Chain of the Group
	 */
/*	public void setNrPivots(final int i) {
		this.nrPivots = i;
	}*/

	/**
	 * sets the ID number of the Vertex Chain Group
	 * 
	 * @param i
	 *            the ID number of the Group
	 */
/*	public void setNumber(final int i) {
		nr = i;
	}*/

	/**
	 * sets the original size of the Group
	 * 
	 * @see maps.groupmaps.RingGroupMap#decompose()
	 */
/*	public void setOriginalSize() {
		orSize = size();
	}*/


	/**
	 * gets the standard signature of the VertexChainGroup
	 * 
	 * @param withEgo
	 *            true if the gender of Ego shall be marked
	 * @return the signature of the VertexChainGroup
	 */
/*	public String signature(final boolean withEgo) {
		if (label != null) {
			return label;
		}
		return getModel().signature(Notation.CLASSIC_GENDERED);
		// return model.signature(withEgo);} //.substring(2);}
	}*/

	// Check or move to RingGroup
	/**
	 * gets a signature of the VertexChainGroup
	 * 
	 * @param i
	 *            the signature key (0 individuals, 1 character, 2 standard
	 *            (here always replaced by 8), 3 numeric, 4 conjoints, 5
	 *            couples, 6 ego-alter, 7 standard neutralized without ego, 8
	 *            standard nonneutralized without ego, 9 pGraph; 10 standard +
	 *            character, 11 standard without ego + character, 12 character +
	 *            individuals)
	 * @return the signature of the VertexChainGroup
	 */
/*	public String signature(final Notation notation) {
		String result;
		
		if (getLabel() != null) {
			result = getLabel();
		} else {
			result = getModel().signature(notation);
		}
		//
		return result;
	}*/

	/**
	 * gets the weighted sum of the chains of the group
	 * 
	 * @param k
	 *            a weight parameter (0 every Chain has weight 1, 1 every Chain
	 *            is weighted by its Value, 2 every Chain is weighted by a its
	 *            Type index)
	 * @return the weighted sum of the chains of the group
	 */
/*	public int size(final int k) {
		if (k == 0) {
			return size();
		}
		int s = 0;
		for (Chain chain : this) {
			if (k == 1) {
				s = s + chain.getValue();
			} else if (k == 2) {
				s = s + chain.getType();
				// s=s+r.count;
			}
		}
		return s;
	}*/

	/**
	 * sorts the Chains of the Group in order of their decomposition
	 * 
	 * @see maps.groupmaps.RingGroupMap#reduce(ChainGroup)
	 */
/*	public void sortByDecomposition() {
		Collections.sort(this, new Comparator<Object>() {
			@Override
			public int compare(final Object a, final Object b) {
				Double d1 = ((Chain) a).getDecomp();
				Double d2 = ((Chain) b).getDecomp();
				return d2.compareTo(d1);
			}
		});
	}*/

	/**
	 * gets the size of the group as a percentage of its original size
	 * 
	 * @return the size of the group as a percentage of its original size
	 * @see maps.groupmaps.RingGroupMap#decompose()
	 */
/*	public double survivalRate() {
		return Mat.percent(size(), orSize);
	}*/

	/**
	 * checks the linear character of the Rings of the Group
	 * 
	 * @return true if the Rings of the Group are linear
	 * @see chains.Ring#linear()
	 * @see maps.groupmaps.RingGroupMap#nonlin()
	 */
	/*
	 * public boolean linear () { return getModel().linear(); }
	 */

	/**
	 * gets the (previously counted) number of Chains that are of the same type
	 * as the Chains of the Group
	 * <p>
	 * used for comparing the numbers of open and closed Chains
	 * 
	 * @param ties
	 *            the CountMap containing the Chain frequencies
	 * @return the number of corresponding Chains
	 * @see io.write.TxtWriter#writeSurvey(RingGroupMap,CountMap,int)
	 */
	/*
	 * public int nrTies (CountMap<int[]> ties) { Integer i =
	 * ties.get(getVector()); if (i==null) return 0; return i; }
	 */


	/**
	 * checks whether a given 2-field-integer array is contained in a list of
	 * such arrays
	 * 
	 * @param list
	 *            the list of arrays
	 * @param a
	 *            the array to be checked
	 * @return true if the array a is contained in the list
	 */
/*	public static boolean contains(final List<Chain> list, final Chain a) {
		for (Chain b : list) {
			if (a.equals(b)) {
				return true;
			}
		}
		return false;
	}*/



	/*
	 * public int siblings (String schema) { int a = 0; int b = 0; for (int
	 * i=0;i<schema.length();i++) { if ((schema.charAt(i)=='Y')) return 3; if
	 * ((schema.charAt(i)=='(')) { if ((schema.charAt(i+1)==')')) a = 1; else b
	 * = 2; } } return a+b; }
	 */






}
