package org.tip.puck.report;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * Note: label and value are not nullable.
 * 
 * @author TIP
 */
public class ReportAttribute {

	private String label;
	private String value;

	/**
	 * 
	 */
	public ReportAttribute() {
		this.label = "";
		this.value = "";
	}

	/**
	 * 
	 */
	public ReportAttribute(final String label, final String value) {
		setLabel(label);
		setValue(value);
	}

	public String label() {
		return label;
	}

	public void setLabel(final String label) {
		if (StringUtils.isBlank(label)) {
			this.label = "";
		} else {
			this.label = label;
		}
	}

	public void setValue(final String value) {
		if (StringUtils.isBlank(value)) {
			this.value = "";
		} else {
			this.value = value;
		}
	}

	public String value() {
		return value;
	}

}
