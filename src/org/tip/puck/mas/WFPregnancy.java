package org.tip.puck.mas;


/**
 * @author Telmo Menezes
 *
 */
public class WFPregnancy implements WeightFactor {

    double weight;
    
    public WFPregnancy(double weight) {
        this.weight = weight;
    }
    
    public String factorName(){
    	return "pregnancy";
    }
    
    @Override
    public double factor(Agent mother, Agent father) {
        if (mother.getTimeOfLastChildBirth() == mother.getMas().getCurCycle()) {
        	return weight;
        }
        	
        return 1.0;
    }
    
    @Override
    public boolean appliesToMarriage() {return true;}
    
    @Override
    public boolean appliesToDivorce() {return true;}
}
