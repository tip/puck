package org.tip.puck.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author TIP
 */
public class NumberedBooleans extends HashMap<Integer, Boolean> implements Iterable<Boolean> {

	private static final long serialVersionUID = -3094105450341284802L;

	/**
	 *
	 */
	public NumberedBooleans() {
		super();
	}

	/**
	 *
	 */
	public NumberedBooleans(final int size) {
		super(size);
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Boolean> iterator() {
		Iterator<Boolean> result;

		result = super.values().iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Boolean> toList() {
		List<Boolean> result;

		result = new ArrayList<Boolean>(values());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Boolean> toSortedList() {
		List<Boolean> result;

		result = toList();
		Collections.sort(result);

		//
		return result;
	}

}
