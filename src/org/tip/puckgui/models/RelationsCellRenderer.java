package org.tip.puckgui.models;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.tip.puck.net.relations.Relation;

/**
 * 
 * @author TIP
 */
public class RelationsCellRenderer extends JLabel implements ListCellRenderer {

	private static final long serialVersionUID = 4698690175540568585L;

	/**
	 * 
	 */
	public RelationsCellRenderer() {
		super();
		setOpaque(true);
	}

	/**
	 * 	
	 */
	@Override
	public Component getListCellRendererComponent(final JList list, final Object lineObject, final int index, final boolean isSelected,
			final boolean cellHasFocus) {

		//
		if (lineObject == null) {
			throw new NullPointerException("Invalid null parameter.");
		} else if (lineObject instanceof String) {
			setText((String) lineObject);
		} else {
			Relation relation = (Relation) lineObject;

			setText(String.format("(%d) %s", relation.getTypedId(), relation.getName()));
		}

		//
		Color background = null;
		Color foreground = null;
		if (isSelected) {
			background = list.getSelectionBackground();
			foreground = list.getSelectionForeground();
		} else {
			background = list.getBackground();
			foreground = list.getForeground();
		}
		setBackground(background);
		setForeground(foreground);

		//
		return this;
	}
}
