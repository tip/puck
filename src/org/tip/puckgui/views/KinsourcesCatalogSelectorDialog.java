package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.tip.puck.io.kinsources.Catalog;
import org.tip.puck.io.kinsources.CatalogItem;
import org.tip.puck.io.kinsources.KinsourcesFile;
import org.tip.puckgui.models.CatalogModel;

/**
 * 
 * @author TIP
 */
public class KinsourcesCatalogSelectorDialog extends JDialog {

	private static final long serialVersionUID = -4219386743033136300L;
	private final JPanel contentPanel = new JPanel();
	private static Catalog lastCatalog;
	private CatalogItem dialogCriteria;
	private static CatalogItem lastCriteria = null;
	private JTable tblItems;
	private JButton btnBrowseKinsources;
	private JButton btnBrowseDataset;

	/**
	 * Create the dialog.
	 */
	public KinsourcesCatalogSelectorDialog() {
		super();

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Kinsources Catalog Selector");
		setIconImage(Toolkit.getDefaultToolkit().getImage(KinsourcesCatalogSelectorDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				KinsourcesCatalogSelectorDialog.this.dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 482, 478);
		getContentPane().setLayout(new BorderLayout());
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "About Kinsources", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			getContentPane().add(panel, BorderLayout.NORTH);
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			{
				JTextPane txtpnIntroduction = new JTextPane();
				txtpnIntroduction.setEditable(false);
				txtpnIntroduction
						.setText("Kinsources is an open and interactive platform to archive, share, analyze and compare kinship data used in scientific research.\n\nThis selector allows you to open a dataset file directly from Kinsources.\n\nVisit the Kinsources website: https://www.kinsources.net/");
				panel.add(txtpnIntroduction);
			}
			{
				this.btnBrowseKinsources = new JButton("Browse Kinsources");
				this.btnBrowseKinsources.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Browse Kinsources.
						String KINSOURCES_URL = "https://www.kinsources.net/";
						try {
							if (Desktop.isDesktopSupported()) {
								//
								Desktop desktop = Desktop.getDesktop();
								if (desktop.isSupported(Desktop.Action.BROWSE)) {
									//
									java.net.URI uri = new java.net.URI(KINSOURCES_URL);
									desktop.browse(uri);

								} else if (KinsourcesCatalogSelectorDialog.this.btnBrowseKinsources != null) {
									//
									KinsourcesCatalogSelectorDialog.this.btnBrowseKinsources.setEnabled(false);
								}
							} else if (KinsourcesCatalogSelectorDialog.this.btnBrowseKinsources != null) {
								//
								KinsourcesCatalogSelectorDialog.this.btnBrowseKinsources.setEnabled(false);
							}
						} catch (java.io.IOException ioe) {
							ioe.printStackTrace();
							System.err.println("The system cannot find the URL specified: [" + KINSOURCES_URL + "]");
							JOptionPane.showMessageDialog(null, "Sorry, browser call failed.", "Error", JOptionPane.ERROR_MESSAGE);
						} catch (java.net.URISyntaxException use) {
							use.printStackTrace();
							System.out.println("Illegal character in path [" + KINSOURCES_URL + "]");
							JOptionPane.showMessageDialog(null, "Sorry, browser call failed.", "Error", JOptionPane.ERROR_MESSAGE);
						}
					}
				});
				this.btnBrowseKinsources.setHorizontalAlignment(SwingConstants.LEFT);
				panel.add(this.btnBrowseKinsources);
			}
		}
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new BoxLayout(this.contentPanel, BoxLayout.Y_AXIS));
		{
			JScrollPane scrollPane = new JScrollPane();
			this.contentPanel.add(scrollPane);
			scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			{
				this.tblItems = new JTable();
				this.tblItems.addKeyListener(new KeyAdapter() {
					@Override
					public void keyPressed(final KeyEvent event) {
						// Key pressed.
						if (event.getKeyCode() == KeyEvent.VK_ENTER) {
							//
							openDataset();
						}
					}
				});
				this.tblItems.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(final MouseEvent event) {
						// Mouse clicked on dataset.
						if (event.getClickCount() == 2) {
							//
							openDataset();
						}
					}
				});
				this.tblItems.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				scrollPane.setViewportView(this.tblItems);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						KinsourcesCatalogSelectorDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("Open");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						openDataset();
					}
				});
				{
					Component horizontalStrut = Box.createHorizontalStrut(20);
					buttonPane.add(horizontalStrut);
				}
				{
					JButton btnRefresh = new JButton("Refresh");
					buttonPane.add(btnRefresh);
					btnRefresh.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(final ActionEvent e) {
							// Refresh.
							refresh();
						}
					});
				}
				{
					Component horizontalStrut = Box.createHorizontalStrut(20);
					buttonPane.add(horizontalStrut);
				}
				{
					this.btnBrowseDataset = new JButton("Browse");
					this.btnBrowseDataset.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(final ActionEvent e) {
							// Browse dataset.
							CatalogItem criteria = getCriteria();
							try {
								if (criteria == null) {
									//
									String title = "Invalid input";
									String message = "Please, select a dataset.";

									//
									JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

								} else {
									//
									if (Desktop.isDesktopSupported()) {
										//
										Desktop desktop = Desktop.getDesktop();
										if (desktop.isSupported(Desktop.Action.BROWSE)) {
											//
											java.net.URI uri = new java.net.URI(criteria.getPermanentLink());
											desktop.browse(uri);

										} else if (KinsourcesCatalogSelectorDialog.this.btnBrowseDataset != null) {
											//
											KinsourcesCatalogSelectorDialog.this.btnBrowseKinsources.setEnabled(false);
										}
									} else if (KinsourcesCatalogSelectorDialog.this.btnBrowseDataset != null) {
										//
										KinsourcesCatalogSelectorDialog.this.btnBrowseDataset.setEnabled(false);
									}
								}
							} catch (java.io.IOException ioe) {
								ioe.printStackTrace();
								System.err.println("The system cannot find the URL specified: [" + criteria.getPermanentLink() + "]");
								JOptionPane.showMessageDialog(null, "Sorry, browser call failed.", "Error", JOptionPane.ERROR_MESSAGE);
							} catch (java.net.URISyntaxException use) {
								use.printStackTrace();
								System.out.println("Illegal character in path [" + criteria.getPermanentLink() + "]");
								JOptionPane.showMessageDialog(null, "Sorry, browser call failed.", "Error", JOptionPane.ERROR_MESSAGE);
							}
						}
					});
					this.btnBrowseDataset.setActionCommand("OK");
					buttonPane.add(this.btnBrowseDataset);
				}
				{
					Component horizontalStrut = Box.createHorizontalStrut(20);
					buttonPane.add(horizontalStrut);
				}
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}

		// ////////////////////////
		if (lastCatalog == null) {
			//
			refresh();

		} else {
			//
			this.tblItems.setModel(new CatalogModel(lastCatalog));
		}
		setCriteria(lastCriteria);
		this.tblItems.getColumnModel().getColumn(0).setMaxWidth(40);
	}

	/**
	 * 
	 * @return
	 */
	public CatalogItem getCriteria() {
		CatalogItem result;

		int currentIndex = this.tblItems.getSelectedRow();

		if (currentIndex == -1) {
			//
			result = null;
		} else {
			//
			result = ((CatalogModel) this.tblItems.getModel()).getSource().getByIndex(currentIndex);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public CatalogItem getDialogCriteria() {
		CatalogItem result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 */
	public void openDataset() {
		//
		CatalogItem criteria = getCriteria();
		if (criteria == null) {
			//
			String title = "Invalid input";
			String message = "Please, select a dataset.";

			//
			JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

		} else {
			//
			lastCriteria = criteria;
			KinsourcesCatalogSelectorDialog.this.dialogCriteria = criteria;

			//
			setVisible(false);
		}

	}

	/**
	 * 
	 */
	public void refresh() {
		//
		Catalog catalog = KinsourcesFile.loadCatalog();

		if (catalog == null) {
			// Make a second try.
			catalog = KinsourcesFile.loadCatalog();
		}

		if (catalog == null) {
			//
			String title = "Kinsources Catalog Error";
			String message = "<html><p>Sorry, the Kinsources Catalog is not reachable.</p><p>Please, check your Internet connection and/or try later.</p></html>";
			JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

			//
			catalog = new Catalog();
			lastCatalog = null;

		} else {
			//
			catalog = catalog.findWithFile().sortByName();
			lastCatalog = catalog;
		}

		//
		TableModel model = this.tblItems.getModel();
		if (model instanceof DefaultTableModel) {
			//
			this.tblItems.setModel(new CatalogModel(catalog));

		} else {
			//
			((CatalogModel) this.tblItems.getModel()).setSource(catalog);
		}
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final CatalogItem source) {
		//
		if (source != null) {
			// TODO
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		/* AttributeFilter criteria = */showDialog();
	}

	/**
	 * Launch the application.
	 */
	public static CatalogItem showDialog() {
		CatalogItem result;

		//
		KinsourcesCatalogSelectorDialog dialog = new KinsourcesCatalogSelectorDialog();
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
