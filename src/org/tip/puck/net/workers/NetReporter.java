package org.tip.puck.net.workers;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.graphs.Graph;
import org.tip.puck.mas.MAS;
import org.tip.puck.net.Family;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionCriteria.ValueFilter;
import org.tip.puck.partitions.PartitionMaker;
import org.tip.puck.report.Report;
import org.tip.puck.report.ReportChart;
import org.tip.puck.report.ReportRawData;
import org.tip.puck.report.ReportTable;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.statistics.StatisticsReporter;
import org.tip.puck.util.Chronometer;
import org.tip.puck.util.PuckUtils;
import org.tip.puck.util.ToolBox;
import org.tip.puck.util.Value;
import org.tip.puckgui.views.RandomCorpusCriteria;
import org.tip.puckgui.views.mas.CousinsWeightFactor;
import org.tip.puckgui.views.mas.Divorce2WeightFactor;
import org.tip.puckgui.views.mas.DivorceWeightFactor;
import org.tip.puckgui.views.mas.NormalAgeDifferenceWeightFactor;
import org.tip.puckgui.views.mas.NormalAgeWeightFactor;
import org.tip.puckgui.views.mas.PregnancyWeightFactor;
import org.tip.puckgui.views.mas.WeightFactor;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class NetReporter {

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportOreGraph(final Segmentation source, final File currentCorpus) throws PuckException {
		Report result;

		if ((source == null) || (currentCorpus == null)) {
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
		} else {
			result = new Report();

			Chronometer chrono = new Chronometer();

			// Compute.

			Graph<Individual> graph = NetUtils.createOreGraph(source);

			StringList partitionLabels = new StringList();

			partitionLabels.append("SEX");
			partitionLabels.append("BIRT_PLAC");

			// Build Pajek data.
			ReportRawData rawData = new ReportRawData("Export to Pajek", "Pajek", "paj", new File(currentCorpus.getParent() + File.separator
					+ ToolBox.clean(graph.getLabel()) + ".paj"));

			rawData.setData(PuckUtils.writePajekNetwork(graph, partitionLabels).toString());

			// Build report.

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportPGraph(final Segmentation source, final File currentCorpus) throws PuckException {
		Report result;

		if ((source == null) || (currentCorpus == null)) {
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
		} else {
			result = new Report();

			Chronometer chrono = new Chronometer();

			// Compute.

			Graph<Family> graph = NetUtils.createPGraph(source);

			StringList partitionLabels = new StringList();

			// Build Pajek data.
			ReportRawData rawData = new ReportRawData("Export to Pajek", "Pajek", "paj", new File(currentCorpus.getParent() + File.separator
					+ ToolBox.clean(graph.getLabel()) + ".paj"));

			rawData.setData(PuckUtils.writePajekNetwork(graph, partitionLabels).toString());

			// Build report.

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportRandomCorpus(final RandomCorpusCriteria criteria, final Net target) throws PuckException {
		Report result;

		if ((criteria == null) || (target == null)) {
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
		} else {
			//
			Chronometer chrono = new Chronometer();

			//
			result = new Report("Random Corpus");
			result.setOrigin("NetReporter.reportRandomCorpus()");
			result.setTarget(target.getLabel());

			//
			StringList buffer = new StringList();
			buffer.appendln();

			//
			result.inputs().add("Initial Population", criteria.getInitialPopulation());
			result.inputs().add("Years", criteria.getYear());
			result.inputs().add("Fertility rate", criteria.getFertilityRate());
			result.inputs().add("Marriage rate", criteria.getMarriageRate());
			result.inputs().add("Divorce rate", criteria.getDivorceRate());
			result.inputs().add("Max age", criteria.getMaxAge());
			result.inputs().add("Min age", criteria.getMinAge());
			result.inputs().add("Normal age difference (mean)", criteria.getMeanAgeDifference());
			result.inputs().add("Normal age difference (stdev)", criteria.getStdevAgeDifference());
			result.inputs().add("Cousin preferences (type)", criteria.getCousinPreferenceType().toString());
			result.inputs().add("Cousin preferences (weight)", criteria.getCousinPreferenceWeight());

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportRandomCorpusMAS(final RandomCorpusCriteria criteria, final MAS mas, final Net target) throws PuckException {
		Report result;

		if ((criteria == null) || (target == null)) {
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
		} else {
			//
			Chronometer chrono = new Chronometer();

			//
			result = new Report("Random Corpus MAS");
			result.setOrigin("NetReporter.reportRandomCorpusMAS()");
			result.setTarget(target.getLabel());

			//
			StringList buffer = new StringList();

			//
			buffer.appendln("Weight forces input:");
			for (WeightFactor factor : criteria.weightFactors()) {
				switch (factor.getType()) {
					case DIVORCE:
						DivorceWeightFactor divorceFactor = (DivorceWeightFactor) factor;
						buffer.append("DIVORCE\tprobability=").appendln(divorceFactor.getProbability());
					break;
					case DIVORCE2:
						Divorce2WeightFactor divorce2Factor = (Divorce2WeightFactor) factor;
						buffer.append("DIVORCE2\tfemale=").appendln(divorce2Factor.getFemaleProbability()).append("\tmale=")
								.appendln(divorce2Factor.getMaleProbability()).append("\tboth=").appendln(divorce2Factor.getBothProbability());
					break;
					case NORMAL_AGE:
						NormalAgeWeightFactor normalAgeFactor = (NormalAgeWeightFactor) factor;
						buffer.append("NORMAL_AGE\tgender=").append(normalAgeFactor.getGender().toString()).append("\tmean=").append(normalAgeFactor.getMean())
								.append("\tstdev=").appendln(normalAgeFactor.getStdev());
					break;
					case NORMAL_AGE_DIFFERENCE:
						NormalAgeDifferenceWeightFactor differenceFactor = (NormalAgeDifferenceWeightFactor) factor;
						buffer.append("NORMAL_AGE_DIFFERENCE\tmean=").append(differenceFactor.getMean()).append("\tstdev=")
								.appendln(differenceFactor.getStdev());
					break;
					case COUSINS:
						CousinsWeightFactor cousinsFactor = (CousinsWeightFactor) factor;
						buffer.append("COUSINS\talpha=").appendln(cousinsFactor.getFirst()).append("\tbravo=").appendln(cousinsFactor.getSecond())
								.append("\tcharlie=").appendln(cousinsFactor.getThird());
					break;
					case PREGNANCY:
						PregnancyWeightFactor pregnancyFactor = (PregnancyWeightFactor) factor;
						buffer.append("PREGNANCY\talpha=").appendln(pregnancyFactor.getFirst());
					break;
					default:
				}
			}
			buffer.appendln();

			//
			buffer.appendln("Illegal marriages input:");
			buffer.appendln(criteria.getIllegalMarriages());
			buffer.appendln();
			buffer.appendln("MAS configuration:");
			buffer.appendln(criteria.toMASConfig());
			result.setInputComment(buffer.toString());

			//
			result.inputs().add("Year", criteria.getYear());
			result.inputs().add("Initial Population", criteria.getInitialPopulation());
			result.inputs().add("Same marriage probability", criteria.getSameMarriageProbability());
			result.inputs().add("Fertility rate", criteria.getFertilityRate());
			result.inputs().add("Max age", criteria.getMaxAge());

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}

	/**
	 * Generates a report about Snowball Structure.
	 * 
	 * @param segmentation
	 * @param criteria
	 * @return
	 * @throws PuckException
	 */
	public static Report reportSteps(final Segmentation segmentation, final SnowballCriteria criteria) throws PuckException {
		Report result;

		//
		Chronometer chrono = new Chronometer();
		result = new Report("Snowball Structure");
		result.setOrigin("Net reporter");

		//
		result.inputs().add("Relation Model Name", criteria.getRelationModelName());
		result.inputs().add("Ego Role Name", criteria.getEgoRoleName());
		result.inputs().add("Alternative Ego Role Name", criteria.getAlternativeEgoRoleName());
		result.inputs().add("Seed Label", criteria.getSeedLabel());
		result.inputs().add("Seed Value", criteria.getSeedValue());
		result.inputs().add("Indirect Label", criteria.getIndirectLabel());
		result.inputs().add("Reach Label", criteria.getReachLabel());
		result.inputs().add("Expansion Mode", criteria.getExpansionMode().name());
		result.inputs().add("Max Step Count", criteria.getMaxNrSteps());
		result.inputs().add("Seed Reference Year", criteria.getSeedReferenceYear());

		//
		Report report1 = new Report("Steps");

		String relationModelName = criteria.getRelationModelName();
		String egoRoleName = criteria.getEgoRoleName();
		String alternativeEgoRoleName = criteria.getAlternativeEgoRoleName();
		String seedLabel = criteria.getSeedLabel();
		String seedValue = criteria.getSeedValue();
		String reachLabel = criteria.getReachLabel();
		String indrectLabel = criteria.getIndirectLabel();
		ExpansionMode expansionMode = criteria.getExpansionMode();
		int year = criteria.getSeedReferenceYear();
		int maxStep = criteria.getMaxNrSteps();

		Individuals seeds = IndividualValuator.extract(segmentation.getCurrentIndividuals(), seedLabel, null, null, new Value(seedValue));
		Individuals neighbors = NetUtils.expand(seeds, relationModelName, egoRoleName, alternativeEgoRoleName, expansionMode, FiliationType.COGNATIC, seedLabel, maxStep);
		Individuals livingNeighbors = new Individuals();
		Individuals reachedNeighbors = new Individuals();
		Individuals reachedAdultNeighbors = new Individuals();
		
		for (Individual neighbor : neighbors) {
			if (!IndividualValuator.lifeStatusAtYear(neighbor, year).equals("DEAD")) {
				livingNeighbors.put(neighbor);
			}
			if (neighbor.getAttributeValue(reachLabel) != null) {
				reachedNeighbors.put(neighbor);
				if (neighbor.getAttributeValue(indrectLabel)==null) {
					reachedAdultNeighbors.put(neighbor);
				}
			}
		}

		String label = "STEP_" + seedLabel.replaceAll(" ", "_") + "_" + expansionMode;

		Partition<Individual> neighborPartition = PartitionMaker.createRaw("Environment", neighbors, label, null);
		Partition<Individual> livingNeighborPartition = PartitionMaker.createRaw("Environment (Living)", livingNeighbors, label, null);
		Partition<Individual> reachedNeighborPartition = PartitionMaker.createRaw("Environment (Living)", reachedNeighbors, label, null);
		Partition<Individual> reachedAdultNeighborPartition = PartitionMaker.createRaw("Environment (Living)", reachedAdultNeighbors, label, null);

		PartitionCriteria stepCriteria = new PartitionCriteria(seedLabel);
		stepCriteria.setValueFilter(ValueFilter.NULL);
		PartitionCriteria splitCriteria = new PartitionCriteria(reachLabel);
		splitCriteria.setValueFilter(ValueFilter.NULL);

		List<ReportChart> charts = new ArrayList<ReportChart>();
		List<ReportTable> tables = new ArrayList<ReportTable>();

		splitCriteria.setLabel("GENDER");

		ReportChart chart1 = StatisticsReporter.createPartitionChart(neighborPartition, stepCriteria, splitCriteria, null);
		chart1.setTitle("Total");
		charts.add(chart1);
		tables.add(ReportTable.transpose(chart1.createReportTableWithSum()));

		ReportChart chart2 = StatisticsReporter.createPartitionChart(livingNeighborPartition, stepCriteria, splitCriteria, null);
		chart2.setTitle("Alive");
		charts.add(chart2);
		tables.add(ReportTable.transpose(chart2.createReportTableWithSum()));

		ReportChart chart3 = StatisticsReporter.createPartitionChart(reachedAdultNeighborPartition, stepCriteria, splitCriteria, null);
		chart3.setTitle("Interviewed");
		charts.add(chart3);
		tables.add(ReportTable.transpose(chart3.createReportTableWithSum()));

		ReportChart chart4 = StatisticsReporter.createPartitionChart(reachedNeighborPartition, stepCriteria, splitCriteria, null);
		chart4.setTitle("Reached");
		charts.add(chart4);
		tables.add(ReportTable.transpose(chart4.createReportTableWithSum()));

		// Manage the number of chart by line.
		for (int chartIndex = 0; chartIndex < charts.size(); chartIndex++) {
			report1.outputs().append(charts.get(chartIndex));
			if (chartIndex % 4 == 3) {
				report1.outputs().appendln();
			}
		}

		// Add chart tables.
		for (ReportTable table : tables) {
			report1.outputs().appendln(table.getTitle());
			report1.outputs().appendln(table);
		}

		result.outputs().append(report1);

		Report report2 = new Report("List");

		for (Cluster<Individual> cluster : neighborPartition.getClusters().toListSortedByValue()) {
			report2.outputs().appendln(neighborPartition.getLabel() + " " + cluster.getLabel());
			report2.outputs().appendln();
			List<Individual> items = cluster.getItems();
			Collections.sort(items);
			for (Individual neighbor : items) {
				report2.outputs().appendln(neighbor + "\t" + IndividualValuator.lifeStatusAtYear(neighbor, 2009) + "\t" + neighbor.getAttributeValue("INTERV"));
			}
			report2.outputs().appendln();
		}

		result.outputs().append(report2);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;

	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportTipGraph(final Segmentation source, final File currentCorpus) throws PuckException {
		Report result;

		if ((source == null) || (currentCorpus == null)) {
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
		} else {
			result = new Report();

			Chronometer chrono = new Chronometer();

			// Compute.

			Graph<Individual> graph = NetUtils.createTipGraph(source);

			StringList partitionLabels = new StringList();

			partitionLabels.append("SEX");
			partitionLabels.append("BIRT_PLAC");

			// Build Pajek data.
			ReportRawData rawData = new ReportRawData("Export to Pajek", "Pajek", "paj", new File(currentCorpus.getParent() + File.separator
					+ ToolBox.clean(graph.getLabel()) + ".paj"));

			rawData.setData(PuckUtils.writePajekNetwork(graph, partitionLabels).toString());

			// Build report.

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}

}
