package org.tip.puck.partitions;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author TIP
 */
public class PartitionCriteriaList extends ArrayList<PartitionCriteria> {

	private static final long serialVersionUID = 4111770254846818079L;

	/**
	 * 
	 */
	public PartitionCriteriaList() {
		super();
	}

	/**
	 * 
	 * @param capacity
	 */
	public PartitionCriteriaList(final int capacity) {
		super(capacity);
	}
	
	public PartitionCriteria getByLabel (String label){
		PartitionCriteria result;
		
		result = null;
		
		for (PartitionCriteria criteria : this){
			if (criteria.getLabel()!=null && criteria.getLabel().equals(label)){
				result = criteria;
				break;
			}
		}
		//
		return result;
	}
	
	public List<String> getLabels(){
		List<String> result;
		
		result = new ArrayList<String>();
		
		for (PartitionCriteria criteria : this){
			if (criteria.getLabel()!=null){
				result.add(criteria.getLabel());
			}
		}
		//
		return result;
	}
	

}
