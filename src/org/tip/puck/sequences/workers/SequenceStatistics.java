package org.tip.puck.sequences.workers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.geo.Geography;
import org.tip.puck.geo.graphs.GeoNetworkUtils;
import org.tip.puck.geo.workers.GeocodingWorker;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.GraphComparatorByArcCount;
import org.tip.puck.graphs.GraphMaker;
import org.tip.puck.graphs.GraphProfile;
import org.tip.puck.graphs.Link;
import org.tip.puck.graphs.Node;
import org.tip.puck.graphs.workers.GraphUtils;
import org.tip.puck.io.kinsources.CatalogItemComparator.Criteria;
import org.tip.puck.net.Attributable;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.RelationEnvironment;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.relations.workers.RelationValuator;
import org.tip.puck.net.relations.workers.RelationWorker;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionMaker;
import org.tip.puck.partitions.PartitionSequence;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.EgoSequence;
import org.tip.puck.sequences.EgoSequences;
import org.tip.puck.sequences.Ordinal;
import org.tip.puck.sequences.Sequenceable;
import org.tip.puck.sequences.Sequenceables;
import org.tip.puck.sequences.Sequences;
import org.tip.puck.sequences.ValueSequence;
import org.tip.puck.sequences.workers.SequenceCriteria.SliceGeneralStatistics;
import org.tip.puck.sequences.workers.SequenceCriteria.TrajectoriesOperation;
import org.tip.puck.sequences.workers.SequenceCriteria.ValueSequenceLabel;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.Numberable;
import org.tip.puck.util.NumberedValues;
import org.tip.puck.util.PuckUtils;
import org.tip.puck.util.ToolBox;
import org.tip.puck.util.Trafo;
import org.tip.puck.util.Value;

import fr.devinsy.util.StringList;

public class SequenceStatistics<S extends Sequenceable<E>,E extends Numberable>  {
	
	private Segmentation segmentation;  // Needed for all kin circuit and migration census
	private List<Ordinal> times;

	Sequenceables<S,E> sequences;
	
	// Central maps for value storage
	private Map<String,PartitionSequence<S>> partitionSequences;
	private Map<String,PartitionSequence<E>> aggregatePartitionSequences;
	private Map<String,PartitionSequence<E>> elementPartitionSequences;
	private Map<String,Sequences<ValueSequence,Value>> valueSequenceMaps;
	private Map<String,Partition<S>> sequencePartitions;
	
	// For network analysis
	
	private Map<String,Map<S,Partition<Node<Individual>>>> componentsMap;
	private Map<ValueSequenceLabel,Partition<Link<Individual>>> linkPartitions;
	private Map<ValueSequenceLabel,Map<Value,Double[]>> similaritiesMaps;
	private Map<String,Map<Integer,Graph<Cluster<Relation>>>> sequenceNetworksMap;
	private Map<String,Map<String,Map<String,Value>>> parcoursNetworkStatistics;
	private SequenceNetworkStatistics<S,E> relationConnectionMatrix;
	
	// For sequence network analysis
	private Map<String,SequenceNetworkStatistics<S,E>> eventSequenceMatrices;
	private Map<String,SequenceNetworkStatistics<S,E>> subSequenceMatrices;
	
	/**
	 * really needed? 
	 */
	private List<Graph> graphs;
	
	// Buffers for network exportation
	Map<String, StringList> pajekBuffers;

	private static final Logger logger = LoggerFactory.getLogger(SequenceStatistics.class);
	

	
//	S singleSequence;
//	Map<S,Map<Ordinal,Map<String,Value>>> values;
//	Map<Ordinal,Map<String,Double>> sums;

//	Map<Individual,Map<Ordinal,Map<String,Value>>> valuesByIndividuals;
//	Map<Ordinal,Map<String,Double>> sumsOverIndividuals;

//	String pattern;
//	String affiliationLabel;
	
/*	public SequenceStatistics(Segmentation segmentation, S singleSequence, SequenceCriteria criteria){
		
		this.times = Ordinal.getOrdinals(criteria.getDates());
		this.segmentation = segmentation;
		this.singleSequence = singleSequence;
		
	}*/

	
	public SequenceStatistics(Segmentation segmentation, Sequenceables<S,E> sequences, SequenceCriteria criteria) throws PuckException{
		
		this.times = Ordinal.getOrdinals(criteria.getDates());
		this.sequences = sequences;
		this.segmentation = segmentation;
		
		this.pajekBuffers = new HashMap<String, StringList>();
		this.graphs = new ArrayList<Graph>();
		
		Map<S,RelationEnvironment> egoEnvironments = new HashMap<S,RelationEnvironment>();
		for (S sequence : sequences){
			if (sequence instanceof EgoSequence){
				
				if ((criteria.getValueSequenceLabels().contains(ValueSequenceLabel.MIG) || criteria.getValueSequenceLabels().contains(ValueSequenceLabel.HOSTMIG) || criteria.getValueSequenceLabels().contains(ValueSequenceLabel.MIGRATIONTYPE)|| criteria.getValueSequenceLabels().contains(ValueSequenceLabel.CHILDMIGRATIONTYPE)) && (!criteria.getRoleNames().contains("MIG"))){
					criteria.getRoleNames().add("MIG");
				}
				if ((criteria.getValueSequenceLabels().contains(ValueSequenceLabel.HOST) || criteria.getValueSequenceLabels().contains(ValueSequenceLabel.HOSTMIG) || criteria.getValueSequenceLabels().contains(ValueSequenceLabel.MIGRATIONTYPE) || criteria.getValueSequenceLabels().contains(ValueSequenceLabel.CHILDMIGRATIONTYPE)) && (!criteria.getRoleNames().contains("HOST"))){
					criteria.getRoleNames().add("HOST");
				}
				String alterRoleName = "ALL";
				
				if (criteria.getSliceGeneralStatistics().contains(SliceGeneralStatistics.POSITIONS)){
					alterRoleName = "REFERENT";
				}

				RelationEnvironment egoEnvironment = new RelationEnvironment(((Sequenceable<Relation>)sequence).getStations().values(),((EgoSequence)sequence).getEgo(),criteria.getEgoRoleName(),criteria.getRoleNames(), criteria.getRelationModelNames(), criteria.getImpersonalAlterLabel());
				egoEnvironment.setAlterRelations(((Sequenceable<Relation>)sequence).getStations().values(),((EgoSequence)sequence).getEgo(),criteria.getEgoRoleName(),alterRoleName, criteria.getRelationModelNames(), criteria.getPattern(), criteria.getChainClassification());
				egoEnvironment.setThreshold(criteria.getThreshold());
				egoEnvironments.put(sequence, egoEnvironment);
			}
		}

//		logger.debug("Evaluate stations "+segmentation+" "+criteria.getValueSequenceCriteriaList().getLabels());
		
		// Evaluation of states and events
		if (times.size() == 0){
			// Variable intervals (typical for event sequences)
			putValueSequences(criteria,egoEnvironments);
		} else {
			// Fix intervals (typical for state sequences)
			putPartitionSequences(criteria,egoEnvironments);
		}
		
		// Initialize maps for network analysis
		
		// Initialize components
		this.componentsMap = new HashMap<String,Map<S,Partition<Node<Individual>>>>();
		for (String networkTitle : criteria.getNetworkTitles()){
			if (networkTitle.contains("Ego Network") || networkTitle.contains("Parcours Similarity Network")){
				componentsMap.put(networkTitle, new HashMap<S,Partition<Node<Individual>>>());
			}
		}
		
		// Initialzie link partitions and similarity maps
		for (String label : criteria.getSequenceValueCriteriaList().getLabels()){
			
			if (label.contains("SIMILARITY")){
				if (similaritiesMaps == null){
					similaritiesMaps = new HashMap<ValueSequenceLabel,Map<Value,Double[]>>();
					linkPartitions = new HashMap<ValueSequenceLabel,Partition<Link<Individual>>>();
				}
				ValueSequenceLabel relationClassificationType = ValueSequenceLabel.valueOf(label.substring(label.lastIndexOf("#")+1));
				similaritiesMaps.put(relationClassificationType, new HashMap<Value,Double[]>());
				linkPartitions.put(relationClassificationType, new Partition<Link<Individual>>());

	//			valuesMap.put(label, new NumberedValues());
			}
		}
		
		// Initialize sequence network maps
		
		sequenceNetworksMap = new HashMap<String,Map<Integer,Graph<Cluster<Relation>>>>();
		for (String networkTitle : criteria.getNetworkTitles()){
			if (networkTitle.contains("Sequence Network") && !networkTitle.contains("Aggregate") && !networkTitle.contains("Tree")){
				sequenceNetworksMap.put(networkTitle,new HashMap<Integer,Graph<Cluster<Relation>>>());
			}
		}

		// Create sequences and sequence networks
		putSequenceValues(segmentation, criteria,egoEnvironments);
		
		logger.debug("Initialize networks for "+(criteria.getNetworkTitles()));

		// Event and sequence type network analysis
//		makeSequenceNetworks(criteria);
		analyzeSequenceNetworks(criteria);
		getConnectedNetworkRelations(criteria);
		
		logger.debug("Statistics established for "+sequences);
		
		//		this.pattern = criteria.getPattern();
		
		// Initialize valuesBySequences
		

/*		this.values = new TreeMap<S,Map<Ordinal,Map<String,Value>>>();
		for (S sequence : sequences.toSortedList()){
			values.put(sequence, new TreeMap<Ordinal,Map<String,Value>>());
			for (Ordinal time : times){
				values.get(sequence).put(time, new TreeMap<String,Value>());
			}
		}
		
		this.sums = new TreeMap<Ordinal,Map<String,Double>>();
		for (Ordinal time : times){
			sums.put(time, new TreeMap<String,Double>());
		}*/
		

		
		// Initialize valuesByIndividuals
		

/*		this.valuesByIndividuals = new TreeMap<Individual,Map<Ordinal,Map<String,Value>>>();
		for (Individual member : sequences.getIndividuals(segmentation).toSortedList()){
			valuesByIndividuals.put(member, new TreeMap<Ordinal,Map<String,Value>>());
			for (Ordinal time : times){
				valuesByIndividuals.get(member).put(time, new HashMap<String,Value>());
			}
		}
		
		this.sumsOverIndividuals = new TreeMap<Ordinal,Map<String,Double>>();
		for (Ordinal time : times){
			sumsOverIndividuals.put(time, new TreeMap<String,Double>());
		}*/
		
	}
	
/*	public void putMemberValues(SequenceCriteria criteria){
		
		for (Individual member : sequences.getIndividuals(segmentation).toSortedList()){
			
			Map<Ordinal,Map<String,Value>> memberValues = valuesByIndividuals.get(member);
			EgoSequence egoSequence = SequenceMaker.createPersonalStateSequence(member, criteria);
			
			for (Ordinal time : times){
				
				Map<String,Value> valueMap = new HashMap<String,Value>();
				Value relationValue = IndividualValuator.get(member,egoSequence.getStation(time),null);

				// Update life Status
				
				String lifeStatus = IndividualValuator.lifeStatusAtYear(member, time.getYear());
				
				if (relationValue==null){
					if (time.getYear().toString().equals(member.getAttributeValue("BIRT_DATE"))){
						lifeStatus = "UNBORN";
					} else if (time.getYear().toString().equals(member.getAttributeValue("DEAT_DATE"))){
						lifeStatus = "DEAD";
					}
				}
				valueMap.put("LIFE_STATUS", new Value(lifeStatus));
				
				// Set values // Generalize for individual-, relation- and actorvaluators...
				
				for (String indicator : indicators){
					if (relationValue!=null){
						
						Relation relation = relationValue.relationValue();
						
						if (indicator.equals("PLACE")){
							
							String placeValue = relation.getAttributeValue(criteria.getLocalUnitLabel());
							if (placeValue==null){
								placeValue = relation.getAttributeValue(criteria.getPlaceLabel());
							}
							valueMap.put(indicator, Value.valueOf(placeValue));
							
						} else {
							
							Actor actor = relation.getActor(member, criteria.getEgoRoleName());
							if (indicator.equals("REFERENT")){
								valueMap.put(indicator, Value.valueOf(actor.getReferent()));
							} else if (indicator.equals("REFERENT_KIN")){
								valueMap.put(indicator, Value.valueOf(RelationWorker.getReferentRole(actor, pattern, affiliationLabel, relation)));
							} else if (indicator.equals("REFERENT_CHAIN")){
								valueMap.put(indicator, Value.valueOf(RelationWorker.getReferentChainGenderString(actor, affiliationLabel, relation)));
							} else if (indicator.equals("REFERENT_KIN_TYPE")){
								valueMap.put(indicator, Value.valueOf(RelationWorker.getReferentRoleShort(actor, pattern, affiliationLabel, relation)));
							} else if (indicator.equals("REFERENT_CHAIN_TYPE")){
								valueMap.put(indicator, Value.valueOf(RelationWorker.getReferentChainNumber(actor, relation)));
							}
						}
					}
				}
				memberValues.put(time, valueMap);
			}
		}
	}
	
	public Individuals getPopulation(){
		Individuals result;
		
		result = sequences.getIndividuals(segmentation);
		
		//
		return result;
	}*/
	
	
	
	Partition<S> getStation (Ordinal time, String indicator) {
		Partition<S>  result;
		
		if (partitionSequences.get(indicator) == null){
			
			result = null;
			
		} else {
			
			result = partitionSequences.get(indicator).getStation(time);
		}
		
		//
		return result;
	}
	
	Value getValue(S sequence, String indicator){
		Value result;
				
		result = sequencePartitions.get(indicator).getValue(sequence);
		
		//
		return result;
	}
	
	// Temporary method to assure compatibility with EgoSequenceStatistics
	NumberedValues getValues (String indicator) {
		NumberedValues result;
		
		result = new NumberedValues(sequencePartitions.get(indicator));
		
		//
		return result;
	}

	
	Value getValue(S sequence, Ordinal time, String indicator){
		Value result;
		
		result = null;
		
		if (indicator.contains("$")){
						
			Value mapValue = partitionSequences.get(indicator.split("\\$")[0]).getValue(time, sequence);
			
			if (mapValue != null){
				
				result = ((Map<String,Value>)mapValue.mapValue()).get(indicator.split("\\$")[1]);
				
			}
			
		} else {
			
			result = partitionSequences.get(indicator).getValue(time, sequence);

		}
		
/*		PartitionSequence<S> partitionSequence = partitionSequences.get(indicator);
		
//		if (item instanceof Sequenceable){
			
			result = partitionSequence.getValue(time, sequence);*/
			
/*		} else if (item instanceof Individual){
			
			EgoSequence sequenceItem = ((EgoSequences)sequences).getByEgo((Individual)item);
			
			if (sequenceItem != null){
				
				result = partitionSequence.getValue(time, (S)sequenceItem);
			}
		}*/
		//
		return result;
	}
	
/*	public <V> Value getValue1(V item, Ordinal time, String indicator){
		Value result;
		
		result = null;
		
		Map<Ordinal,Map<String,Value>> itemValues = null;
		
		if (item instanceof Sequenceable){
			itemValues = values.get((S)item);
		} else if (item instanceof Individual){
			EgoSequence sequenceItem = ((EgoSequences)sequences).getByEgo((Individual)item);
			if (sequenceItem != null){
				itemValues = values.get(sequenceItem);
			}
		}
		
		if (itemValues != null){
			
			Map<String,Value> stationValues = itemValues.get(time);
			
			if (stationValues != null){
				
				result = stationValues.get(indicator);
			}
		}
		
		//
		return result;
		
	}*/

	
/*	public Value getBySequence(S sequence, Ordinal time, String indicator){
		Value result;
		
		result = null;
		
		Map<Ordinal,Map<String,Value>> sequenceValues = valuesBySequences.get(sequence);
		
		if (sequenceValues != null){
			
			Map<String,Value> stationValues = sequenceValues.get(time);
			
			if (stationValues != null){
				
				result = stationValues.get(indicator);
			}
		}
		
		//
		return result;
		
	}
	
	public Value getByIndividual(Individual individual, Ordinal time, String indicator){
		Value result;
		
		result = null;
		
		Map<Ordinal,Map<String,Value>> individualValues = valuesByIndividuals.get(individual);
		
		if (individualValues != null){
			
			Map<String,Value> stationValues = individualValues.get(time);
			
			if (stationValues != null){
				
				result = stationValues.get(indicator);
			}
		}
		
		//
		return result;
	}
	
	public Double sumOverSequences (Ordinal time, String indicator) {
		Double result;
		
		if (partitionSequences.get(indicator) == null){
			
			result = null;
			
		} else {
			
			result = sumOverSequences(time, indicator)/new Double(partitionSequences.get(indicator).getStation(time).size());
		}

		
		//
		return result;
	}
	
	public Double sumOverIndividuals (Ordinal time, String indicator) {
		Double result;
		
		Double sum = sumsOverIndividuals.get(time).get(indicator);
		
		if (sum == null){
			result = 0.;
		} else {
			result = sum;
		}
		
		//
		return result;
	}
	
	public Double meanOverSequences (Ordinal time, String indicator){
		Double result;
		
		if (partitionSequences.get(indicator) == null){
			
			result = null;
			
		} else {
			
			result = sumOverSequences(time, indicator)/new Double(partitionSequences.get(indicator).getStation(time).size());
		}
		
		//
		return result;
	}
	
	public Double meanOverIndividuals (Individual individual, Ordinal time, String indicator){
		Double result;
		
		result = sumOverSequences(time, indicator)/new Double(valuesByIndividuals.size());

		//
		return result;
	}
		
	public void put(S sequence, Ordinal time, String indicator, Value value){
		
		Map<Ordinal,Map<String,Value>> sequenceValues = values.get(sequence);
		
		if (sequenceValues != null){
			
			Map<String,Value> stationValues = sequenceValues.get(time);
			
			if (stationValues != null){
				
				stationValues.put(indicator,value);
				
				if (value != null && value.isNumber()){
					
					sums.get(time).put(indicator,sumOverSequences(time,indicator) + new Value(value).doubleValue());
					
				}
				
			}
		}
	}*/
	
	private void putSequenceValues (S sequence, SequenceCriteria sequenceCriteria, Map<String,GraphProfile<?>> networkProfiles,RelationEnvironment egoEnvironment) throws PuckException{
		
//		SequenceValuator<S,E> census = new SequenceValuator<S,E>(sequence, spaceTimeCriteria);

/*		RelationEnvironment egoEnvironment = null;
		
		if (sequence instanceof EgoSequence){
			
			if ((sequenceCriteria.getValueSequenceLabels().contains(ValueSequenceLabel.MIG) || sequenceCriteria.getValueSequenceLabels().contains(ValueSequenceLabel.HOSTMIG) || sequenceCriteria.getValueSequenceLabels().contains(ValueSequenceLabel.MIGRATIONTYPE)|| sequenceCriteria.getValueSequenceLabels().contains(ValueSequenceLabel.CHILDMIGRATIONTYPE)) && (!sequenceCriteria.getRoleNames().contains("MIG"))){
				sequenceCriteria.getRoleNames().add("MIG");
			}
			if ((sequenceCriteria.getValueSequenceLabels().contains(ValueSequenceLabel.HOST) || sequenceCriteria.getValueSequenceLabels().contains(ValueSequenceLabel.HOSTMIG) || sequenceCriteria.getValueSequenceLabels().contains(ValueSequenceLabel.MIGRATIONTYPE) || sequenceCriteria.getValueSequenceLabels().contains(ValueSequenceLabel.CHILDMIGRATIONTYPE)) && (!sequenceCriteria.getRoleNames().contains("HOST"))){
				sequenceCriteria.getRoleNames().add("HOST");
			}

			egoEnvironment = new RelationEnvironment(((Sequenceable<Relation>)sequence).getStations().values(),((EgoSequence)sequence).getEgo(),sequenceCriteria.getEgoRoleName(),sequenceCriteria.getRoleNames(), sequenceCriteria.getRelationModelNames());
			egoEnvironment.setAlterRelations(((Sequenceable<Relation>)sequence).getStations().values(),((EgoSequence)sequence).getEgo(),sequenceCriteria.getEgoRoleName(),"ALL", sequenceCriteria.getRelationModelNames(), sequenceCriteria.getPattern(), sequenceCriteria.getChainClassification());
			egoEnvironment.setThreshold(sequenceCriteria.getThreshold());
		}*/
		
		GraphProfile<?> networkProfile = null;
		if (sequenceCriteria.getNetworkTitles().contains("Ego Network")){
			networkProfile = networkProfiles.get("Nonmediated Ego Network");
		} else {
			for (String networkTitle : sequenceCriteria.getNetworkTitles()){
				if (networkTitle.contains("Sequence Network")){
					networkProfile = networkProfiles.get(networkTitle);
					break;
				}
			}
		}

		for (PartitionCriteria sequenceValueCriteria : sequenceCriteria.getSequenceValueCriteriaList()){

			String label = sequenceValueCriteria.getLabel();

			String separator = "#";
/*			if (label.contains("ALTERS") || label.contains("RELATION")){
				separator = "_";
			}*/
			
			String[] labels = ToolBox.splitLastPart(label,separator);
			sequencePartitions.get(label).put(sequence, SequenceValuator.get(sequence,segmentation, labels[0],labels[1],sequenceCriteria,egoEnvironment, networkProfile, valueSequenceMaps, partitionSequences));		
		}
		//
//		logger.debug("Sequence values put for sequence "+sequence.getId());
	}
	
	private void putSequenceValues(Segmentation segmentation, SequenceCriteria sequenceCriteria,Map<S,RelationEnvironment> egoEnvironments) throws PuckException{
		
//		logger.debug("Evaluate sequences "+segmentation+" "+sequenceCriteria.getSequenceValueCriteriaList().getLabels());

		// Initialize sequence Partitions
		
		sequencePartitions = new TreeMap<String,Partition<S>>();
		for (PartitionCriteria sequenceValueCriteria : sequenceCriteria.getSequenceValueCriteriaList()){
						
			String label = sequenceValueCriteria.getLabel();
			Partition<S> partition = new Partition<S>(label);
			sequencePartitions.put(label, partition);
		}
		
		for (S sequence : sequences.toSortedList()){
				
			Map<String,GraphProfile<?>> networkProfiles = createSequenceNetworks(sequence, segmentation, sequenceCriteria);
			RelationEnvironment egoEnvironment = egoEnvironments.get(sequence);
			putSequenceValues(sequence,sequenceCriteria,networkProfiles,egoEnvironment);
			
		}
	}

	private void putValueSequences(SequenceCriteria sequenceCriteria, Map<S,RelationEnvironment> egoEnvironments) throws PuckException {
				
		valueSequenceMaps = new TreeMap<String,Sequences<ValueSequence,Value>>();
		
		for (PartitionCriteria valueSequenceCriteria : sequenceCriteria.getValueSequenceCriteriaList()){
			
			String label = valueSequenceCriteria.getLabel();
			
			if (valueSequenceMaps.get(label)==null){
				
				Sequences<ValueSequence,Value> valueSequences = new Sequences<ValueSequence,Value>();
				valueSequenceMaps.put(label, valueSequences);
				
				for (S sequence : sequences.toSortedList()){
					
					ValueSequence valueSequence = new ValueSequence(label, sequence.getId());
					RelationEnvironment egoEnvironment = egoEnvironments.get(sequence);
					
					SequenceValuator<S,E> sequenceValuator = new SequenceValuator<S,E>(sequence,sequenceCriteria);
					
					for (Ordinal time : sequence.getTimes()){
						
						// reconsider this constraint (meant to exclude births as life events)
						if (!RelationValuator.isBirth((Attributable)sequence.getStation(time)) || label.contains("PLACE")){

							valueSequence.put(time,sequenceValuator.getStationValue(sequence,time,segmentation,sequenceCriteria,valueSequenceCriteria,egoEnvironment));
						}
					}
					
					valueSequence.setProfile();
					valueSequences.put(valueSequence);
				}
			}
			//
			logger.debug("Value sequences put for "+label+" "+valueSequenceCriteria);
		}
	}
	
	private void putPartitionSequences(SequenceCriteria sequenceCriteria, Map<S,RelationEnvironment> egoEnvironments) throws PuckException{
		
		partitionSequences = new TreeMap<String,PartitionSequence<S>>();
		aggregatePartitionSequences = new TreeMap<String,PartitionSequence<E>>();
		elementPartitionSequences = new TreeMap<String,PartitionSequence<E>>();
		
		for (PartitionCriteria valueSequenceCriteria : sequenceCriteria.getValueSequenceCriteriaList()){
						
			String label = valueSequenceCriteria.getLabel();
			PartitionSequence<S> partitionSequence = new PartitionSequence<S>(label,times);
			partitionSequences.put(label, partitionSequence);
			
			for (S sequence : sequences.toSortedList()){
				
				SequenceValuator<S,E> sequenceValuator = new SequenceValuator<S,E>(sequence,sequenceCriteria);
				RelationEnvironment egoEnvironment = egoEnvironments.get(sequence);
				
				for (Ordinal time : times){
					
					E station = sequence.getStation(time);
					
					if (station!=null && (!(station instanceof Relation) || (segmentation.getCurrentRelations().contains((Relation)station)))){
												
						Value value = sequenceValuator.getStationValue(sequence,time,segmentation,sequenceCriteria,valueSequenceCriteria,egoEnvironment);
						if (value == null){
							value = new Value("NONE");
						}
						
						//
						partitionSequence.put(time,sequence, value);
					}
				}
			}
						
			// Revalue Partitions
			
/*			for (Ordinal time : times){

				if (partitionSequence.isMulti()){
					
					partitionSequence.put(time, PartitionMaker.create(partitionSequence.getStation(time), new PartitionCriteria(valueSequenceCriteria.getLabel())));
					
				} else {
					
					partitionSequence.put(time, PartitionMaker.create(partitionSequence.getStation(time), new PartitionCriteria(valueSequenceCriteria.getLabel())));
				}
			}*/
			
			// Treat Multipartitions and Listpartitions
			
			if (partitionSequence.isMulti()){
				
				aggregatePartitionSequences.put(label, (PartitionSequence<E>)partitionSequence.aggregate(new PartitionCriteria(valueSequenceCriteria.getLabel())));
			}

			if (partitionSequence.hasListValues()){
				
				elementPartitionSequences.put(label, (PartitionSequence<E>)partitionSequence.pseudo(new PartitionCriteria(valueSequenceCriteria.getLabel())));
			}
			//
			logger.debug("Partition sequences put for "+label+" "+valueSequenceCriteria);
		}
	}
	
	private Map<String,GraphProfile<?>> createSequenceNetworks (S sequence, Segmentation segmentation, SequenceCriteria criteria) throws PuckException{
		Map<String,GraphProfile<?>> result;
		// Create Networks
		
		result = null;
		
		if (!criteria.getNetworkTitles().isEmpty()){
			
			result = SequenceNetworkMaker.createNetworkProfiles(sequence, segmentation, criteria);
//			SequenceNetworkMaker census = new SequenceNetworkMaker((EgoSequence)sequence, criteria);

			// Create Link Partitions (for Similarity Analysis)
			
			for (String label : criteria.getSequenceValueCriteriaList().getLabels()){
				if (label.contains("SIMILARITY")){
					
					ValueSequenceLabel relationClassificationType = ValueSequenceLabel.valueOf(label.substring(label.lastIndexOf("_")+1));
					GraphProfile<Individual> parcoursNetworkProfile = (GraphProfile<Individual>)result.get("Parcours Intersection Network_"+relationClassificationType);
					
					Partition<Link<Individual>> linkPartition = parcoursNetworkProfile.getLinkPartition();
					if (linkPartition==null){
						linkPartition = GraphUtils.getLinkPartitionByKinship(parcoursNetworkProfile.getGraph());
						parcoursNetworkProfile.setLinkPartition(linkPartition);
					}
					linkPartitions.get(relationClassificationType).add(linkPartition);
				} 
			}
						
			for (String networkTitle : criteria.getNetworkTitles()){
							
				// Store components for ego and similarity networks

				if (networkTitle.contains("Ego Network")){
					componentsMap.get(networkTitle).put(sequence, ((GraphProfile<Individual>)result.get("Nonmediated Ego Network")).getNonEgoComponents());
//					components.put(sequence, census.getComponents("Nonmediated Ego Network"));
				} else if (networkTitle.contains("Parcours Similarity Network")){
					ValueSequenceLabel relationClassificationType = ValueSequenceLabel.valueOf(networkTitle.substring(networkTitle.lastIndexOf("#")+1));
					componentsMap.get(networkTitle).put(sequence, ((GraphProfile<Individual>)result.get("Parcours Similarity Network_"+relationClassificationType)).getNonEgoComponents());
//					components.put(sequence, census.getComponents("Parcours Similarity Network_"+relationClassificationType));
				}

				// Store sequence networks
				
				if (networkTitle.contains("Sequence Network") && !networkTitle.contains("Aggregate") && !networkTitle.contains("Tree")){
									
					GraphProfile<Cluster<Relation>> sequenceNetworkProfile = (GraphProfile<Cluster<Relation>>)result.get(networkTitle);
					
					if (sequenceNetworkProfile != null){
						sequenceNetworksMap.get(networkTitle).put(((EgoSequence)sequence).getEgo().getId(), sequenceNetworkProfile.getGraph());
//						sequenceNetworksMap.get(networkTitle.substring(networkTitle.lastIndexOf("_")+1)).put(((EgoSequence)sequence).getEgo().getId(), (Graph<Cluster<Relation>>)census.getNetwork(networkTitle));
					}
				}

				// Write networks
					
				if (!networkTitle.equals("Event Type Network") && !networkTitle.equals("Sequence Type Network")){
					
					GraphProfile<Cluster<Relation>> networkProfile = (GraphProfile<Cluster<Relation>>)result.get(networkTitle);
					if (networkProfile != null){
						Graph<?> network = networkProfile.getGraph();
						if (network.nodeCount()>0){
							if (!pajekBuffers.containsKey(networkTitle)){
								pajekBuffers.put(networkTitle, new StringList());
							}
							pajekBuffers.get(networkTitle).addAll(PuckUtils.writePajekNetwork(network,networkProfile.getPartitionLabels()));
//							pajekBuffers.get(networkTitle).addAll(PuckUtils.writePajekNetwork(network,census.getPartitionLabels(networkTitle)));
						}
					}
				}
			}
		}
		//
		return result;
	}

	
/*	public void putValues1(SequenceCriteria criteria){
		
		for (S sequence : sequences.toSortedList()){
			
//			Map<Ordinal,Map<String,Object>> sequenceValues = valuesBySequences.get(sequence);
			Individual ego = null;
			if (sequence instanceof EgoSequence){
				ego = ((EgoSequence)sequence).getEgo();
			}
			
			for (Ordinal time : times){
				
				E station = sequence.getStation(time);
				
				if (ego != null){
					put(sequence,time,"LIFE_STATUS", getLifeStatus(ego,time,station));
				}
				
				if (station!=null) {

					Map<String,Value> statistics = getStatistics(ego,station,criteria,indicators);
					
					for (String indicator : indicators){
						
						Value value = statistics.get(indicator);
						put(sequence, time, indicator, value);
					}
					
//					sequenceValues.get(time).putAll(statistics);
				}
			}
		}
	}*/

	
	/**
	 * @param station
	 * @param indicators
	 * @param pattern
	 * @return
	 */
/*	public Map<String,Value> getStatistics (final Individual ego, final E station, final SequenceCriteria criteria, final List<String> indicators){
		Map<String,Value> result;
				
		result = null;
		
		if (station instanceof Relation){
			result = RelationWorker.getStatistics((Relation)station, ego, criteria, indicators);
		}
		//
		return result;
	}*/
	
	

/*	public List<String> indicators() {
		List<String> result;
		
		result = new ArrayList<String>(partitionSequences.keySet());
		Collections.sort(result);
		
		return result;
	}*/
	
	String getTrend(S sequence, String indicator){
		String result;
		
		result = null;
		Value lastValue = null;
		
		for (Ordinal time : times){
			
			Value value = getValue(sequence,time,indicator);
//			Value value = getBySequence(sequence,time,indicator);

			if (lastValue != null && value != null){

				if (lastValue.isNotNumber() || value.isNotNumber()){
					
					break;
					
				} else {
											
					int comp = ((Comparable<Value>)value).compareTo(lastValue);
					String trend = null;
					
					if (comp < 0){
						trend = "DECLINING";
					} else if (comp > 0) {
						trend = "AUGMENTING";
					} else if (comp == 0){
						trend = "CONSTANT";
					}
					
					if (result == null || result.equals("CONSTANT")){
						result = trend;
					} else if (!trend.equals("CONSTANT") && !trend.equals(result)){
						result = "VARIABLE";
						break;
					} 
				}
			}

			lastValue = value;
				
		}
		
		//
		return result;
	}
	
	String getMeanTrend (String indicator){
		String result;
		
		result = "";
		
		Map<String,Double> trendCounts = new HashMap<String,Double>();
		
		for (S sequence : sequences){
			
			String trend = getTrend(sequence,indicator);
			Double count = trendCounts.get(trend);
			if (count == null){
				trendCounts.put(trend,1.);
			} else {
				trendCounts.put(trend,count+1.);
			}
		}
		
		for (String trend : trendCounts.keySet()){
			trendCounts.put(trend,MathUtils.percent(trendCounts.get(trend), new Double(sequences.size())));
		}
		
	    List<Entry<String,Double>> sortedEntries = new ArrayList<Entry<String,Double>>(trendCounts.entrySet());

	    Collections.sort(sortedEntries, 
	            new Comparator<Entry<String,Double>>() {
	                @Override
	                public int compare(Entry<String,Double> e1, Entry<String,Double> e2) {
	                    return e2.getValue().compareTo(e1.getValue());
	                }
	            }
	    );

		for (Entry<String,Double> entry: sortedEntries){
			result += entry.getKey()+" "+entry.getValue()+" ";
		}
		
		//
		return result;
	}

/*	public <V> Map<Value,Double> getMeanValueFrequencies(Map<Ordinal,Partition<V>> census, Organizable<V> sliceables){
		Map<Value,Double> result;
		
		result = new TreeMap<Value,Double>();

		for (V sliceable : sliceables){
			
			for (Ordinal time: times){
				
				Value value = census.get(time).getValue(sliceable);

				if (value!=null){
					Double count = result.get(value);
					if (count==null){
						count = 1.;
					} else {
						count += 1.;
					}
					result.put(value,count);
				}
			}
		}
		
		for (Value value : result.keySet()){
			if (result.get(value)==null){
				result.put(value,result.get(value)/new Double(times.size()));
			}
		}
		//
		return result;
	}*/
	
/*	public <V> Matrix getTransitionMatrix(Map<Ordinal,Partition<V>> census, Organizable<V> sliceables){
		Matrix result;
		
		Map<String,Map<String,Integer>> transitionMap = new TreeMap<String,Map<String,Integer>>();
		List<String> values = new ArrayList<String>();
		
		for (V sliceable : sliceables){
			
			for (int i=1;i<times.size();i++){
				
				Value object1 = census.get(times.get(i-1)).getValue(sliceable);
				Value object2 = census.get(times.get(i)).getValue(sliceable);
				
				if (object1!=null && object2!=null){

					String value1 = object1.toString();
					String value2 = object2.toString();
					
					if (!values.contains(value1)){
						values.add(value1);
					}
					if (!values.contains(value2)){
						values.add(value2);
					}
					
					Map<String,Integer> targetMap = transitionMap.get(value1);
					if (targetMap==null){
						targetMap = new TreeMap<String,Integer>();
						transitionMap.put(value1,targetMap);
					}

					Integer count = targetMap.get(value2);
					if (count==null){
						count = 1;
					} else {
						count += 1;
					}
					targetMap.put(value2,count);
				}
			}
		}
		
		Collections.sort(values);
		String[] labels = new String[values.size()];
		for (int i=0;i<values.size();i++){
			labels[i] = values.get(i)+"";
		}
		
		result = new Matrix(values.size(),values.size());
		result.setRowLabels(labels);
		result.setColLabels(labels);
		
		for (Object value1 : transitionMap.keySet()){
			
			Map<String,Integer> targetMap = transitionMap.get(value1);

			for (Object value2 : targetMap.keySet()){
				
				result.augment(values.indexOf(value1), values.indexOf(value2), targetMap.get(value2));

			}
		}
		
		//
		return result;
	}*/
	
	PartitionSequence<E> getAggregatePartitionSequence (String label){
		PartitionSequence<E> result;
		
		result = aggregatePartitionSequences.get(label);
		
		//
		return result;
	}
	
	PartitionSequence<E> getElementPartitionSequence (String label){
		PartitionSequence<E> result;
		
		result = elementPartitionSequences.get(label);
		
		//
		return result;
	}
	
	PartitionSequence<S> getPartitionSequence (String label){
		PartitionSequence<S> result;
		
		result = partitionSequences.get(label);
		
	/*	result = new PartitionSequence<Individual>(times);
		
/*		result = new TreeMap<Ordinal,Partition<Individual>>();
		for (Ordinal time : times){
			result.put(time, new Partition<Individual>());
		}*/
		
	/*	for (Individual member : getPopulation()){
			
			for (Ordinal time : times){
				
//				Value value = getValue(member, time, censusType);
//				Value value = getByIndividual(member, time, censusType);

//				if (value!=null){

					result.put(time, member, getValue(member, time, censusType));

//				}
			}
		}*/
		//
		return result;
	}
	
	Map<String,PartitionSequence<Individual>> getMigrations (final SequenceCriteria sequenceCriteria, final PartitionCriteria partitionCriteria){
		Map<String,PartitionSequence<Individual>> result;
		
		result = new TreeMap<String,PartitionSequence<Individual>>();
		
		List<Ordinal> changes = new ArrayList<Ordinal>();
		for (int i=0;i<times.size()-1;i++){
			changes.add(new Ordinal(times.get(i)+"-"+times.get(i+1),null,null));
		}
		
		result.put("MIGRATIONS", new PartitionSequence<Individual>("Migrations",changes));
		result.put("DESTINATIONS", new PartitionSequence<Individual>("Destination",changes));
		result.put("ORIGINS", new PartitionSequence<Individual>("Origins",changes));
		
		for (int i=0;i<times.size()-1;i++){
			
			Ordinal change = changes.get(i);
			Ordinal startTime = times.get(i);
			Ordinal endTime = times.get(i+1);

			Relations filteredStartSpace = new Relations((List<Relation>)sequences.getStations(startTime));
			Relations filteredEndSpace = new Relations((List<Relation>)sequences.getStations(endTime));

			Relations totalStartSpace = segmentation.getAllRelations().getByTime(sequenceCriteria.getDateLabel(), startTime.getYear());
			Relations totalEndSpace = segmentation.getAllRelations().getByTime(sequenceCriteria.getDateLabel(), endTime.getYear());

			Partition<Individual> migrations = new Partition<Individual>("Migrations "+startTime+"/"+endTime);
			result.get("MIGRATIONS").put(change, migrations);

			Partition<Individual> destinations = new Partition<Individual>("Destinations "+startTime+"/"+endTime);
			result.get("DESTINATIONS").put(change, destinations);
			
			Partition<Individual> origins = new Partition<Individual>("Origins "+startTime+"/"+endTime);
			result.get("ORIGINS").put(change, origins);
			
			// Forward 
			
			for (Individual individual : filteredStartSpace.getIndividuals().toSortedList()){
				
		    	Integer deathYear = IndividualValuator.getDeathYear(individual);
		    	
		    	if (deathYear!=null && deathYear < endTime.getYear()) {
		    		migrations.put(individual, new Value("DIED"));
		    	} else if (totalEndSpace.getByIndividual(individual).isEmpty()){
			    	if (deathYear!=null && deathYear.equals(endTime.getYear())){
			    		migrations.put(individual, new Value("DIED"));
			    	} else {
			    		migrations.put(individual, new Value("UNKNOWN DESTINATION"));
			    	}
				} else if (filteredEndSpace.getByIndividual(individual).isEmpty()){
					migrations.put(individual, new Value("LEFT"));
					for (Relation destination : totalEndSpace.getByIndividual(individual)){
						destinations.put(individual,RelationValuator.get(destination,partitionCriteria.getLabel(),partitionCriteria.getLabelParameter(), sequenceCriteria.getGeography()));
					}
				} else {
					Relation start = filteredStartSpace.getByIndividual(individual).getFirst();
					Relation end = filteredEndSpace.getByIndividual(individual).getFirst();
					String startUnit = null;
					if (start!=null) {
						startUnit = start.getAttributeValue(sequenceCriteria.getLocalUnitLabel());
					}
					if (startUnit==null){
						startUnit = start.getAttributeValue(sequenceCriteria.getPlaceLabel());
					}
					String endUnit = null;
					if (start!=null) {
						endUnit = end.getAttributeValue(sequenceCriteria.getLocalUnitLabel());
					}
					if (endUnit==null){
						endUnit = start.getAttributeValue(sequenceCriteria.getPlaceLabel());
					}
					if (!startUnit.equals(endUnit)){
						migrations.put(individual, new Value("INTERNAL CHANGE"));
					} else {
						migrations.put(individual, new Value("UNCHANGED"));
					}
				}
			}
			
			// Backward 
			
			for (Individual individual : filteredEndSpace.getIndividuals().toSortedList()){
				
				Integer birthYear = IndividualValuator.getBirthYear(individual);
				
				if (birthYear!=null && birthYear > startTime.getYear()){
					migrations.put(individual, new Value("NEWBORN"));
				} else if (totalStartSpace.getByIndividual(individual).isEmpty()){		
					if (birthYear!=null && birthYear.equals(startTime.getYear())){
						migrations.put(individual, new Value("NEWBORN"));
					} else {
						migrations.put(individual, new Value("UNKNOWN ORIGIN"));
					}
				} else if (filteredStartSpace.getByIndividual(individual).isEmpty()){
					migrations.put(individual, new Value("ENTERED"));
					for (Relation origin : totalStartSpace.getByIndividual(individual)){
						origins.put(individual,RelationValuator.get(origin,partitionCriteria.getLabel(),partitionCriteria.getLabelParameter(),sequenceCriteria.getGeography()));;
					}
				}
			}
		}
		//
		return result;
	}
	
/*	private static <E> Individuals getIndividuals(E station){
		Individuals result;
		
		if (station instanceof Populatable){
			result = ((Populatable)station).getIndividuals();
		} else {
			result = null;
		}
		//
		return result;
	}
	
	// Unchecked casts
	public Map<String,Map<Ordinal,Partition<Individual>>> getDynamicIndividualCensus1 (final SequenceCriteria spaceTimeCriteria, final PartitionCriteria partitionCriteria){
		Map<String,Map<Ordinal,Partition<Individual>>> result;
		
		result = new TreeMap<String,Map<Ordinal,Partition<Individual>>>();
		
		result.put("MIGRATIONS", new TreeMap<Ordinal,Partition<Individual>>());
		result.put("DESTINATIONS", new TreeMap<Ordinal,Partition<Individual>>());
		result.put("ORIGINS", new TreeMap<Ordinal,Partition<Individual>>());
		
		for (int i=0;i<times.size()-1;i++){
			
			Ordinal startTime = times.get(i);
			Ordinal endTime = times.get(i+1);

			Relations filteredStartSpace = (Relations)singleSequence.getStation(startTime);
			Relations filteredEndSpace = (Relations)singleSequence.getStation(endTime);

			Relations totalStartSpace = segmentation.getAllRelations().getByTime(spaceTimeCriteria.getDateLabel(), startTime.getYear());
			Relations totalEndSpace = segmentation.getAllRelations().getByTime(spaceTimeCriteria.getDateLabel(), endTime.getYear());

			Partition<Individual> migrations = new Partition<Individual>();
			migrations.setLabel("Migrations "+startTime+"/"+endTime);
			result.get("MIGRATIONS").put(endTime, migrations);

			Partition<Individual> destinations = new Partition<Individual>();
			destinations.setLabel("Destinations "+startTime+"/"+endTime);
			result.get("DESTINATIONS").put(startTime, destinations);
			
			Partition<Individual> origins = new Partition<Individual>();
			origins.setLabel("Origins "+startTime+"/"+endTime);
			result.get("ORIGINS").put(endTime, origins);


//			Individuals totalStartPopulation = totalStartSpace.getIndividuals();
//			Individuals filteredStartPopulation = filteredStartSpace.getIndividuals();
//			Individuals totalEndPopulation = totalEndSpace.getIndividuals();
//			Individuals filteredEndPopulation = filteredEndSpace.getIndividuals();
			
			// Forward 
			
			for (Individual individual : filteredStartSpace.getIndividuals().toSortedList()){
				if (IndividualValuator.lifeStatusAtYear(individual, endTime.getYear()).equals("DEAD")){
					migrations.put(individual, new Value("DIED"));
//				} else if (!totalEndPopulation.contains(individual)){
				} else if (totalEndSpace.getByIndividual(individual).isEmpty()){
					migrations.put(individual, new Value("UNKNOWN DESTINATION"));
//				} else if (!filteredEndPopulation.contains(individual)){
				} else if (filteredEndSpace.getByIndividual(individual).isEmpty()){
					migrations.put(individual, new Value("LEFT"));
					for (Relation destination : totalEndSpace.getByIndividual(individual)){
						destinations.put(individual,RelationValuator.get(destination,partitionCriteria.getLabel(),partitionCriteria.getLabelParameter()));
					}
				} else {
					Relation start = filteredStartSpace.getByIndividual(individual).getFirst();
					Relation end = filteredEndSpace.getByIndividual(individual).getFirst();
					String startUnit = start.getAttributeValue(singleSequence.idLabel());
					if (startUnit==null){
						startUnit = start.getAttributeValue(spaceTimeCriteria.getPlaceLabel());
					}
					String endUnit = end.getAttributeValue(singleSequence.idLabel());
					if (endUnit==null){
						endUnit = start.getAttributeValue(spaceTimeCriteria.getPlaceLabel());
					}
					if (!startUnit.equals(endUnit)){
						migrations.put(individual, new Value("INTERNAL CHANGE"));
					} else {
						migrations.put(individual, new Value("UNCHANGED"));
					}
				}
			}
			
			// Backward 
			
			for (Individual individual : filteredEndSpace.getIndividuals().toSortedList()){
				if (IndividualValuator.lifeStatusAtYear(individual, startTime.getYear()).equals("UNBORN")){
					migrations.put(individual, new Value("NEWBORN"));
//				} else if (!totalStartPopulation.contains(individual)){
				} else if (totalStartSpace.getByIndividual(individual).isEmpty()){
					migrations.put(individual, new Value("UNKNOWN ORIGIN"));
//				} else if (!filteredStartPopulation.contains(individual)){
				} else if (filteredStartSpace.getByIndividual(individual).isEmpty()){
					migrations.put(individual, new Value("ENTERED"));
					for (Relation origin : totalStartSpace.getByIndividual(individual)){
						origins.put(individual,RelationValuator.get(origin,partitionCriteria.getLabel(),partitionCriteria.getLabelParameter()));;
					}
				} else if (!filteredStartSpace.getByIndividual(individual).equals(filteredEndSpace.getByIndividual(individual))){
//						partition.put(individual, new Value("INTERNAL CHANGE2"));
				} else {
//						partition.put(individual, new Value("UNCHANGED2"));
				}
			}
		}
		//
		return result;
	}
	
	public PartitionSequence<Individual> getDatedIndividualCensus (final SequenceCriteria spaceTimeCriteria, final PartitionCriteria partitionCriteria) throws PuckException{
		PartitionSequence<Individual> result;
		
		result = new PartitionSequence<Individual>(partitionCriteria.getLabel(),times);
		
		for (Ordinal time : times){

			E relations = singleSequence.getStation(time);
			String label = spaceTimeCriteria.getRelationModelName()+" "+time;
			
			if (partitionCriteria.getLabel().equals("REFERENT")){
				
				partitionCriteria.setLabelParameter(spaceTimeCriteria.getRelationModelName()+" "+spaceTimeCriteria.getEgoRoleName()+" "+time);
				Partition<Individual> prePartition = PartitionMaker.create(label, getIndividuals(relations), (Relations)relations, partitionCriteria);
				
				for (Individual ego : prePartition.getItemsAsList()){
					Value alterId = prePartition.getValue(ego);
					if (alterId!=null){
						List<String> alterRoles = NetUtils.getAlterRoles(ego, segmentation.getAllIndividuals().getById(alterId.intValue()), ToolBox.stringsToInts(spaceTimeCriteria.getPattern()), spaceTimeCriteria.getRelationModelNames(), spaceTimeCriteria.getChainClassification(), null, null);
						Collections.sort(alterRoles);
						result.put(time,ego, new Value(alterRoles.toString()));
					}
				}
				
				
			} else {
				if (partitionCriteria.getLabel().equals("AGE") || partitionCriteria.getLabel().equals("MATRISTATUS")|| partitionCriteria.getLabel().equals("OCCUPATION")){
					partitionCriteria.setLabelParameter(time+"");
				} 
				Partition<Individual> partition = PartitionMaker.create(label, getIndividuals(relations), (Relations)relations, partitionCriteria);
				result.put(time, partition);
			}
			//
		}
		//
		return result;
	}
	
	public PartitionSequence<S>  getDatedSequenceCensus(String censusType, SequenceCriteria criteria){
		PartitionSequence<S> result;
		
		result = new TreeMap<Ordinal,Partition<S>>();
		for (Ordinal time : times){
			result.put(time, new Partition<S>());
		}
				
		for (S sequence : values.keySet()){
				
			Map<Ordinal,Map<String,Value>> sequenceValues = values.get(sequence);
				
			for (Ordinal time : times){
					
				E station = sequence.getStation(time);
				
				
				Map<String,Value> map = null;
				
				if (station instanceof Relation){
					map = RelationValuator.get((Relation)station, censusType, segmentation, criteria).mapValue();
				}
				
				if (map!=null){
//					RelationWorker.getReferentKinCensus(relation, pattern, affiliationLabel);
					sequenceValues.get(time).putAll(map);
						
					for (String indicator : sequenceValues.get(time).keySet()){
						if (!indicators.contains(indicator)){
							indicators.add(indicator);
						}
					}
						
					Partition<S> partition = result.get(time);
					Value value = map.get("Types");
					if (value!=null){
						partition.put(sequence, new Value(value));
					}
				}
			}
		}
		
		return result;
	}
	
	public <V> Partition<V> getSequenceCensus (Map<Ordinal,Partition<V>> datedPartition, Organizable<V> sliceables){
		Partition<V> result;
		
		result = new Partition<V>();
		
		for (V member : sliceables){
			
			Sequence<Value> sequence = new Sequence<Value>();
			
			for (Ordinal time: times){
				
				sequence.put(time, datedPartition.get(time).getValue(member));
			}
			result.put(member, new Value(sequence.toValueString()));
		}
		//
		return result;
	}*/
	
	private Map<E,E> adjacentStations (String direction){
		Map<E,E> result;
		
		result = new HashMap<E,E>();
		
		Ordinal former = null;
		
		for (Ordinal later : times){
			
			if (former!=null){
				
				Ordinal current = null;
				if (direction.equals("OUT")){
					current = former;
				} else if (direction.equals("IN")){
					current = later;
				}
				
				for (S sequence : sequences){
					
					E currentRelation = sequence.getStation(current);
					
					if (currentRelation != null){
						if (direction.equals("OUT")){
							result.put(currentRelation,  sequence.getStation(later));
						} else if (direction.equals("IN")){
							result.put(currentRelation, sequence.getStation(former));
						}
					}
				}
			}
			former = later;
		}
		
		//
		return result;
	}
	
	Partition<String> getFlows (String direction, SequenceCriteria sequenceCriteria){
		Partition<String> result;
		
		result = new Partition<String>();
		
		int[] maxDegrees = ToolBox.stringsToInts(sequenceCriteria.getPattern());
		
		Map<Relation,Relation> adjacentStations = (Map<Relation,Relation>)adjacentStations(direction);
		
		for (Relation currentRelation : adjacentStations.keySet()){
			
			Relation otherRelation = adjacentStations.get(currentRelation);

			for (Actor actor : currentRelation.getDifferentwActors(otherRelation)){
				
				Individual referent = actor.getReferent();
				String link = "UNKNOWN";
				if (referent!=null){
					link = 	NetUtils.getAlterRole(actor.getIndividual(), referent, maxDegrees, sequenceCriteria.getRelationModelNames(), sequenceCriteria.getChainClassification());
				}
				Individual otherReferent = null;
				Actor otherActor = RelationWorker.getClosestHomologue(currentRelation, actor, sequenceCriteria.getDateLabel(),direction);
				if (otherActor!=null){
					otherReferent = otherActor.getReferent();
				} else {
//					System.err.println("Missing homologue "+actor+" "+direction+" "+year);
				}
				
				String otherLink = "UNKNOWN";
				if (otherReferent!=null){
					otherLink = NetUtils.getAlterRole(actor.getIndividual(), otherReferent, maxDegrees, sequenceCriteria.getRelationModelNames(), sequenceCriteria.getChainClassification());
				} 
				
				String change = null;
				if (referent!=null && referent.equals(otherReferent)){
					change = "IDENTICAL";
				} else if (direction.equals("OUT")){
					change = link+">"+otherLink;
				} else if (direction.equals("IN")){
					change = otherLink+">"+link;
				}
				result.put(currentRelation+"\t"+currentRelation.getTime(sequenceCriteria.getDateLabel())+"\t"+direction+"\t"+actor.getIndividual()+"\t"+referent+"\t"+otherReferent, new Value(change));
			}
		}

		//
		return result;
	}

	
	Map<String,StringList> getPajekBuffers (){
		return pajekBuffers;
	}

	public Partition<S> getSequencePartition(String label) {
		Partition<S> result;
		
		if (sequencePartitions == null){
			result = null;
		} else {
			result = sequencePartitions.get(label);
		}
		
		//
		return result;
	}
	
	public Map<Value,Double[]> getMeanNrMoves (){
		Map<Value, Double[]> result;
		
		result = new TreeMap<Value, Double[]>();
		
		Map<GeoLevel, Integer[]> sequenceMap = new HashMap<GeoLevel, Integer[]>();
		Map<GeoLevel, Integer[]> eventMap = new HashMap<GeoLevel, Integer[]>();
		
		for (GeoLevel level : new GeoLevel[]{GeoLevel.TRANSNATIONAL,GeoLevel.TRANSREGIONAL,GeoLevel.REGIONAL,GeoLevel.LOCAL}){
			sequenceMap.put(level, new Integer[]{0,0,0});
			eventMap.put(level, new Integer[]{0,0,0});
			result.put(new Value(level), new Double[]{0.,0.,0.});
		}
		
		
		for (S sequence : sequences){
			int gender = ((EgoSequence)sequence).getEgo().getGender().toInt();
			Value value = getValue(sequence,"MEAN_NR_MOVES");
//			Value value = valuesMap.get("MEAN_NR_MOVES").get(sequence.getEgo().getId());
			if (value!=null){
				Map<GeoLevel,Integer> distanceProfile = value.mapValue();
				for (GeoLevel level : distanceProfile.keySet()){
					if (sequenceMap.get(level)!=null){
						sequenceMap.get(level)[gender] += 1;
						sequenceMap.get(level)[2] += 1;
						eventMap.get(level)[gender] += distanceProfile.get(level);
						sequenceMap.get(level)[2] += distanceProfile.get(level);
					}
				}
			}
		}
		
		for (Value value : result.keySet()){
			for (int i=0;i<3;i++){
				result.get(value)[i] = new Double(eventMap.get(value.enumValue())[i])/new Double(sequenceMap.get(value.enumValue())[i]);
			}
		}
		//
		return result;
	}
	
	Map<S, Partition<Node<Individual>>> getComponents(String networkTitle) {
		return componentsMap.get(networkTitle);
	}

	SequenceNetworkStatistics<S,E> getEventSequenceMatrix(String eventTypeName) {
		SequenceNetworkStatistics<S,E> result;
		
		if (eventSequenceMatrices == null || eventTypeName == null){
			result = null;
		} else {
			result = eventSequenceMatrices.get(eventTypeName);
		}
		
		return result;
	}
	
	SequenceNetworkStatistics<S,E> getSubSequenceMatrix(String eventTypeName) {
		SequenceNetworkStatistics<S,E> result;
		
		if (subSequenceMatrices == null || eventTypeName == null){
			result = null;
		} else {
			result = subSequenceMatrices.get(eventTypeName);
		}
		
		return result;
	}

	List<String> getSequenceNetworkLabels(){
		List<String> result;
		
		if (eventSequenceMatrices!=null){
			
			result = new ArrayList<String>(eventSequenceMatrices.keySet());
			
		} else {
			
			result = new ArrayList<String>();
		}
		//
		return result;
	}
	
	List<String> getSequenceTreeLabels(){
		List<String> result;
		
		if (subSequenceMatrices!=null){
			
			result = new ArrayList<String>(subSequenceMatrices.keySet());
			
		} else {
			
			result = new ArrayList<String>();
		}
		//
		return result;
	}
	
	private void makeSequenceNetwork (String networkTitle, ValueSequenceLabel relationClassificationType){
		
		String eventTypeName = relationClassificationType.toString();
		Map<S,List<String>> singles = new TreeMap<S,List<String>>();
		Map<S,List<String[]>> pairs  = new TreeMap<S,List<String[]>>();
		
		for (S sequence : sequences){
			
//			Individual ego = ((EgoSequence)sequence).getEgo();
			
			Value singlesValue = getValue(sequence,"PROFILE#"+eventTypeName);
//			Value singlesValue = valuesMap.get("PROFILE_"+eventTypeName).get(ego.getId());
			
			if (singlesValue != null){

				String valueString = singlesValue.stringValue();
				List<String> singlesList = null;
				
				if (networkTitle.contains("Event Type Network")){
					
					singlesList = Arrays.asList(valueString.split(";"));
					
				} else if (networkTitle.contains("Sequence Type Network")){
				
					singlesList = PuckUtils.cumulateList(Arrays.asList(valueString.split(";")));
				}
				
				List<String[]> pairsList = new ArrayList<String[]>();
				for (int i=1;i<singlesList.size();i++){
					pairsList.add(new String[]{singlesList.get(i-1),singlesList.get(i)});
				}
				
				//
				singles.put(sequence, singlesList);
				pairs.put(sequence, pairsList);
			}
		}
		
		if (networkTitle.contains("Event Type Network")){
			
			eventSequenceMatrices.put(eventTypeName, new SequenceNetworkStatistics<S,E>("Event Type Network",eventTypeName,singles,pairs));
			
		} else if (networkTitle.contains("Sequence Type Network")){
		
			subSequenceMatrices.put(eventTypeName, new SequenceNetworkStatistics<S,E>("Sequence Type Network",eventTypeName,singles,pairs));
		}
	}
	
	/**
	 * Partially obsolete: Event Type Networks are replaced by Aggregate Sequence Networks directly produced during sequence production
	 * Sequence Type Networks (Aggregate Cumulate Sequence Networks) have not yet been replaced by new method, so the old method is still kept but not used
	 * @param criteria
	 * @throws PuckException
	 */
	private void makeSequenceNetworks (SequenceCriteria criteria) throws PuckException{
		
		this.eventSequenceMatrices = new HashMap<String,SequenceNetworkStatistics<S,E>>();
		this.subSequenceMatrices = new HashMap<String,SequenceNetworkStatistics<S,E>>();

		// Create Sequence Networks

		for (ValueSequenceLabel relationClassificationType : criteria.getTrajectoriesRelationClassificationTypes()){

			if (criteria.getNetworkTitles().contains("Event Type Network#"+relationClassificationType)){

				makeSequenceNetwork("Event Type Network "+relationClassificationType, relationClassificationType);
				
				SequenceNetworkStatistics<S,E> matrix = eventSequenceMatrices.get(relationClassificationType.toString());

				Graph<Cluster<String>>[] eventTypeNetworks = matrix.getSequenceNetworks();
				
				List<String> partitionLabels = new ArrayList<String>();
				partitionLabels.add(relationClassificationType.toString());
				Map<String,Map<Value,Integer>> partitionNumbersMaps = GraphMaker.getPartitionNumbersMaps(partitionLabels, eventTypeNetworks[2]);
/*				for (String label : partitionLabels){
					if (partitionNumbersMaps.get(label)!=null){
						nrValues.get(relationClassificationType).put(label, partitionNumbersMaps.get(label).size());
					}
				}*/
				partitionLabels.add("SIZE");

				StringList pajekBuffer = new StringList();
				pajekBuffers.put("Event Type Network",pajekBuffer);

				for (Gender gender : Gender.values()){

					pajekBuffer.addAll(PuckUtils.writePajekNetwork(eventTypeNetworks[gender.toInt()],partitionLabels,partitionNumbersMaps));

				}
				
				if (criteria.getTrajectoriesOperations().contains(TrajectoriesOperation.DRAW)){

		            Graph<String> placeNameGraph = GeoNetworkUtils.createGeoNetwork2(eventTypeNetworks[2], criteria.getLevel());
		            graphs.add(placeNameGraph);
				}
			}
			
			if (criteria.getNetworkTitles().contains("Sequence Type Network#"+relationClassificationType)){
				
				makeSequenceNetwork ("Sequence Type Network "+relationClassificationType, relationClassificationType);
				SequenceNetworkStatistics<S,E> matrix = subSequenceMatrices.get(relationClassificationType.toString());

				Graph<Cluster<String>>[] sequenceTypeNetworks = matrix.getSequenceNetworks();
				matrix.getDepthPartitions();
						
				List<String> partitionLabels = new ArrayList<String>();
				partitionLabels.add(relationClassificationType.toString());
				Map<String,Map<Value,Integer>> partitionNumbersMaps = GraphMaker.getPartitionNumbersMaps(partitionLabels, sequenceTypeNetworks[2]);
/*				for (String label : partitionLabels){
					nrValues.get(relationClassificationType).put(label, partitionNumbersMaps.get(label).size());
				}*/
				partitionLabels.add("SIZE");
				partitionLabels.add("STEP");

				StringList pajekBuffer = new StringList();
				pajekBuffers.put("Sequence Type Network",pajekBuffer);

				for (Gender gender : Gender.values()){
					pajekBuffer.addAll(PuckUtils.writePajekNetwork(sequenceTypeNetworks[gender.toInt()],partitionLabels,partitionNumbersMaps));
				}
			}
//		}
		
		// Write Sequence Networks

/*		for (ValueSequenceLabel relationClassificationType : criteria.getTrajectoriesRelationClassificationTypes()){

			if (criteria.getNetworkTitles().contains("Event Type Network")){

				SequenceNetworkStatistics<S,E> matrix = eventSequenceMatrices.get(relationClassificationType.toString());

				Graph<Cluster<String>>[] eventTypeNetworks = matrix.getSequenceNetworks();
				
				List<String> partitionLabels = new ArrayList<String>();
				partitionLabels.add(relationClassificationType.toString());
				Map<String,Map<Value,Integer>> partitionNumbersMaps = GraphMaker.getPartitionNumbersMaps(partitionLabels, eventTypeNetworks[2]);
				for (String label : partitionLabels){
					if (partitionNumbersMaps.get(label)!=null){
						nrValues.get(relationClassificationType).put(label, partitionNumbersMaps.get(label).size());
					}
				}
				partitionLabels.add("SIZE");

				for (Gender gender : Gender.values()){
					
					pajekBuffers.get("Event Type Network").addAll(PuckUtils.writePajekNetwork(eventTypeNetworks[gender.toInt()],partitionLabels,partitionNumbersMaps));

				}
			}
				
			if (criteria.getNetworkTitles().contains("Sequence Type Network")){
				
				SequenceNetworkStatistics<S,E> matrix = subSequenceMatrices.get(relationClassificationType.toString());

				Graph<Cluster<String>>[] sequenceTypeNetworks = matrix.getSequenceNetworks();
				matrix.getDepthPartitions();
						
				List<String> partitionLabels = new ArrayList<String>();
				partitionLabels.add(relationClassificationType.toString());
				Map<String,Map<Value,Integer>> partitionNumbersMaps = GraphMaker.getPartitionNumbersMaps(partitionLabels, sequenceTypeNetworks[2]);
				for (String label : partitionLabels){
					nrValues.get(relationClassificationType).put(label, partitionNumbersMaps.get(label).size());
				}
				partitionLabels.add("SIZE");
				partitionLabels.add("STEP");

				for (Gender gender : Gender.values()){
					pajekBuffers.get("Sequence Type Network").addAll(PuckUtils.writePajekNetwork(sequenceTypeNetworks[gender.toInt()],partitionLabels,partitionNumbersMaps));
				}
			}*/
		}
//		logger.debug("Sequence networks created.");
	}
	
	private void createSimilarityTrees(String networkTitle) throws PuckException{
					
			Map<Integer,Graph<Cluster<Relation>>> parcoursNetworks = sequenceNetworksMap.get(networkTitle);
			
			List<Graph<String>> flatParcoursNetworksNoLoops = new ArrayList<Graph<String>>();
			
			for (Graph<Cluster<Relation>> parcoursNetwork : parcoursNetworks.values()){
				Graph<String> flatParcoursNetworkNoLoops = SequenceNetworkMaker.getFlatParcoursNetworkNoLoops(parcoursNetwork);
/*					Graph<String> flatParcoursNetworkNoLoops = new Graph<String>(parcoursNetwork.getLabel());
				for (Link<Cluster<Relation>> link : parcoursNetwork.getLinks()){
					if (!link.isLoop()){
						flatParcoursNetworkNoLoops.addArc(link.getSourceNode().getReferent().getLabel(),link.getTargetNode().getReferent().getLabel());
					}
				}*/
				flatParcoursNetworksNoLoops.add(flatParcoursNetworkNoLoops);
			}
			
			Collections.sort(flatParcoursNetworksNoLoops, new GraphComparatorByArcCount<String>());
			
			// Make phylogenetic tree
			Graph<Set<Graph<String>>> tree = GraphUtils.createPhylogeneticTree(flatParcoursNetworksNoLoops);
			tree.setLabel(networkTitle+"_Tree");
			for (Node<Set<Graph<String>>> node : tree.getNodes()){
				node.setLabel(node.getLabel().replaceAll(networkTitle+" ",""));
				if (node.getReferent().size()==1){
					node.setAttribute("TYPE", "1");
					String[] splitLabel = Trafo.noParentheses(node.getReferent().toString()).split(" ");
					Integer egoId = Integer.parseInt(splitLabel[splitLabel.length-1]);
					node.setAttribute("GENDER", ((EgoSequences)sequences).getByEgoId(egoId).getEgo().getGender().toString());
				} else {
					node.setAttribute("TYPE", "0");
				}
			}
			
			List<String> treePartitionLabels = new ArrayList<String>();
			treePartitionLabels.add("TYPE");
			treePartitionLabels.add("GENDER");
			
			// Write network similarity trees
			
			String bufferTitle = networkTitle.replaceAll("Network", "Network Similarity Tree");
			StringList pajekBuffer = pajekBuffers.get(bufferTitle);
			if (pajekBuffer == null){
				pajekBuffer = new StringList();
				pajekBuffers.put(bufferTitle,pajekBuffer);
			}
			pajekBuffer.addAll(PuckUtils.writePajekNetwork(tree,treePartitionLabels)); 		
	}
	
	private void createUnionGraphs (String networkTitle, String partitionLabel, List<String> nodeStatisticsLabels, Geography geography) throws PuckException{
		
		// Make union graphs
		
		Map<Integer,Graph<Cluster<Relation>>> parcoursNetworks = sequenceNetworksMap.get(networkTitle);

		this.parcoursNetworkStatistics = new TreeMap<String,Map<String,Map<String,Value>>>();

		List<Graph<Cluster<Relation>>> unions = new ArrayList<Graph<Cluster<Relation>>>();
		PartitionCriteria partitionCriteria = new PartitionCriteria(partitionLabel);
		Partition<Individual> individualPartition = PartitionMaker.create("", ((EgoSequences)sequences).getEgos(), partitionCriteria, geography);
		Partition<Graph<Cluster<Relation>>> graphPartition = new Partition<Graph<Cluster<Relation>>>();
		
		for (Individual ego : individualPartition.getItems()){
			graphPartition.put(parcoursNetworks.get(ego.getId()), individualPartition.getValue(ego));
		}
						
		for (Cluster<Graph<Cluster<Relation>>> graphCluster : graphPartition.getClusters()){
			Graph<Cluster<Relation>> union = GraphUtils.fuseGraphs(graphCluster.getItems());
			union.setLabel(networkTitle+"_"+graphCluster.getValue());
			unions.add(union);
			parcoursNetworkStatistics.put(graphCluster.getValue()+"",GraphUtils.getNodeStatisticsByLabel(union, nodeStatisticsLabels));
		}
		Graph<Cluster<Relation>> totalUnion = GraphUtils.fuseGraphs(new ArrayList<Graph<Cluster<Relation>>>(parcoursNetworks.values()));
		totalUnion.setLabel(networkTitle+"_Total");
		parcoursNetworkStatistics.put("Total",GraphUtils.getNodeStatisticsByLabel(totalUnion, nodeStatisticsLabels));
		unions.add(totalUnion);
		
		List<String> unionPartitionLabels = new ArrayList<String>();
		unionPartitionLabels.add("NUMBER");
		unionPartitionLabels.add("SIZE");
		unionPartitionLabels.add("BETWEENNESS");
		unionPartitionLabels.add("DEGREE");

		// Write fused parcours networks
		
		for (Graph<Cluster<Relation>> union : unions){
			String bufferTitle = networkTitle.replaceAll("Sequence Network", "Aggregate Sequence Network");
			StringList pajekBuffer = pajekBuffers.get(bufferTitle);
			if (pajekBuffer == null){
				pajekBuffer = new StringList();
				pajekBuffers.put(bufferTitle,pajekBuffer);
			}
			pajekBuffer.addAll(PuckUtils.writePajekNetwork(union,unionPartitionLabels)); 		
		}
	}
	
	private void analyzeSequenceNetworks (SequenceCriteria criteria) throws PuckException{
		
		// Create union graphs, similarity networks and phylogenetic trees, make parcours network statitics 
		
		for (String networkTitle : criteria.getNetworkTitles()){ 
						
			if (networkTitle.contains("Aggregate Sequence Network")){

				String sourceNetworkTitle = networkTitle.replaceAll("Aggregate ", "");
				createUnionGraphs (sourceNetworkTitle, criteria.getIndividualPartitionLabel(), criteria.getNodeStatisticsLabels(), segmentation.getGeography());
				
			} else if (networkTitle.contains("Sequence Network Similarity Tree")) {
	
				String sourceNetworkTitle = networkTitle.replaceAll(" Similarity Tree", "");
				createSimilarityTrees(sourceNetworkTitle);
			}
		}
	}
		
	private void getConnectedNetworkRelations(SequenceCriteria criteria) throws PuckException{
		
		if (criteria.getSequenceValueCriteriaList().getLabels().contains("CONNECTED_NETWORK_RELATIONS")){

			Map<Individual,List<String>> singles = new TreeMap<Individual,List<String>>();
			Map<Individual,List<String[]>> pairs  = new TreeMap<Individual,List<String[]>>();
			
			for (S sequence : sequences){
				
				Individual ego = ((EgoSequence)sequence).getEgo();
				Value singlesValue = getValue(sequence,"NETWORK_RELATIONS");
//				Value singlesValue = valuesMap.get("NETWORK_RELATIONS").get(ego.getId());
				
				if (singlesValue != null){
					singles.put(ego, (List<String>)singlesValue.listValue());
				}
								
				Value pairsValue = getValue(sequence,"CONNECTED_NETWORK_RELATIONS");
//				Value pairsValue = valuesMap.get("CONNECTED_NETWORK_RELATIONS").get(ego.getId());
				
				if (pairsValue != null){
					pairs.put(ego, (List<String[]>)pairsValue.listValue());
				}
			}
			
			this.relationConnectionMatrix = new SequenceNetworkStatistics("Component connections",null,singles,pairs);
		}
		
/*		}
		
		if (criteria.getNetworkTitles().contains("Sequence Type Network")){

			for (RelationClassificationType relationClassificationType : criteria.getTrajectoriesRelationClassificationTypes()){
				
				String eventTypeName = relationClassificationType.toString();
				Map<Individual,List<String>> singles = new TreeMap<Individual,List<String>>();
				Map<Individual,List<String[]>> pairs  = new TreeMap<Individual,List<String[]>>();
				
				for (S sequence : sequences){
					
//					Individual ego = ((EgoSequence)sequence).getEgo();
					
					Value singlesValue = getValue(sequence,"PROFILE#"+eventTypeName);
//					Value singlesValue = valuesMap.get("PROFILE_"+eventTypeName).get(ego.getId());
					
					if (singlesValue != null){

						String valueString = singlesValue.stringValue();
						List<String> singlesList = PuckUtils.cumulateList(Arrays.asList(valueString.split(";")));
						List<String[]> pairsList = new ArrayList<String[]>();
						for (int i=1;i<singlesList.size();i++){
							pairsList.add(new String[]{singlesList.get(i-1),singlesList.get(i)});
						}
						//
						singles.put(ego, singlesList);
						pairs.put(ego, pairsList);
					}
				}
				
				subSequenceMatrices.put(eventTypeName, new CorrelationMatrix("Sequence Type Network",eventTypeName,singles,pairs));
			}
		}*/
	}
	
	public ValueSequence getValueSequence(String label, S sequence) throws PuckException{
		ValueSequence result;
		
		if (times.size()==0){
			result = valueSequenceMaps.get(label).getById(sequence.getId());
		} else {
			result = partitionSequences.get(label).getValueSequence(sequence);
		}
		//
		return result;
		
		
	}
	
/*	public Graph<Cluster<String>> getSequenceNetwork (String title, ValueSequenceLabel relationClassificationType, Partition<String> partition){
		Graph<Cluster<String>> result;
		
		if (sequences == null) {
			throw new IllegalArgumentException("Null parameter detected.");
		} else {
			
			result = new Graph<Cluster<String>>(title+"_"+relationClassificationType);
			
			//
			for (Cluster<String> cluster : partition.getClusters().toListSortedByDescendingSize()) {
				if (!cluster.isNull()) {
					result.addNode(cluster);
				}
			}
			
			//
			for (S sequence : sequences){
				Cluster<String> previous = null;
				for (Relation event : ((EgoSequence)sequence).getStations().values()){
					Cluster<String> next = partition.getCluster(((EgoSequence)sequence).getEgo().getId()+" "+event.getTypedId());
					if (previous!=null){
						result.incArcWeight(previous, next);
					}
					previous = next;
				}
			}
			
			for (Node<Cluster<String>> node : result.getNodes()){
				Cluster<String> referent = node.getReferent();
				if (referent !=null){
					Value clusterValue = referent.getValue();
					if (clusterValue!=null){
						String value = clusterValue.toString();
						if (value.lastIndexOf("-")>-1){
							value = value.substring(value.lastIndexOf("-")+1);
						}
						node.setAttribute(relationClassificationType.toString(), value);
					}
				}
			}
		}
		
		//
		return result;
	}*/

	SequenceNetworkStatistics<S, E> getRelationConnectionMatrix() {
		return relationConnectionMatrix;
	}

	Map<Value, Double[]> getSimilaritiesMap(ValueSequenceLabel relationClassificationType) {
		return similaritiesMaps.get(relationClassificationType);
	}

	public Map<String, Map<String, Map<String, Value>>> getParcoursNetworkStatistics() {
		return parcoursNetworkStatistics;
	}

	public List<Graph> getGraphs() {
		return graphs;
	}
	
	

}
