package org.tip.puck.sequences.workers;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.census.workers.CensusCriteria;
import org.tip.puck.census.workers.CensusReporter;
import org.tip.puck.census.workers.RestrictionType;
import org.tip.puck.census.workers.SymmetryType;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.geo.Geography;
import org.tip.puck.geo.Place;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.GraphMaker;
import org.tip.puck.graphs.GraphProfile;
import org.tip.puck.graphs.Node;
import org.tip.puck.graphs.workers.GraphReporter;
import org.tip.puck.io.paj.PAJFile;
import org.tip.puck.matrix.Matrix;
import org.tip.puck.matrix.MatrixStatistics.Indicator;
import org.tip.puck.matrix.MatrixStatistics.Mode;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.IndividualComparator.Sorting;
import org.tip.puck.net.Individualizable;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.net.Populatable;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.RelationEnvironment;
import org.tip.puck.net.relations.workers.RelationValuator;
import org.tip.puck.net.relations.workers.RelationWorker;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.MultiPartition;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.Partition.ValueCode;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionCriteriaList;
import org.tip.puck.partitions.PartitionMaker;
import org.tip.puck.partitions.PartitionSequence;
import org.tip.puck.report.ChartReport;
import org.tip.puck.report.Report;
import org.tip.puck.report.ReportChart;
import org.tip.puck.report.ReportChart.GraphType;
import org.tip.puck.report.ReportList;
import org.tip.puck.report.ReportRawData;
import org.tip.puck.report.ReportTable;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.EgoSequence;
import org.tip.puck.sequences.EgoSequences;
import org.tip.puck.sequences.EventTriangle;
import org.tip.puck.sequences.Ordinal;
import org.tip.puck.sequences.Sequence;
import org.tip.puck.sequences.Sequenceable;
import org.tip.puck.sequences.Sequenceables;
import org.tip.puck.sequences.Sequences;
import org.tip.puck.sequences.ValueSequence;
import org.tip.puck.sequences.workers.SequenceCriteria.EgoNetworksOperation;
import org.tip.puck.sequences.workers.SequenceCriteria.SequenceReportType;
import org.tip.puck.sequences.workers.SequenceCriteria.SliceGeneralStatistics;
import org.tip.puck.sequences.workers.SequenceCriteria.SliceReportType;
import org.tip.puck.sequences.workers.SequenceCriteria.TrajectoriesOperation;
import org.tip.puck.sequences.workers.SequenceCriteria.ValueSequenceLabel;
import org.tip.puck.statistics.StatisticsReporter;
import org.tip.puck.util.Chronometer;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.Numberable;
import org.tip.puck.util.NumberedValues;
import org.tip.puck.util.PuckUtils;
import org.tip.puck.util.ToolBox;
import org.tip.puck.util.Value;

import fr.devinsy.util.StringList;

/**
 * 
 * @author Klaus Hamberger
 * 
 */
public class SequenceReporter {

	private static final Logger logger = LoggerFactory.getLogger(SequenceReporter.class);

	private static StringList getStories(final Relation event, final Individual ego, final String egoRoleName) {
		StringList result;

		result = new StringList();

		for (Attribute attribute : event.getActor(ego, egoRoleName).attributes()) {
			String story = "";
			if (attribute.getLabel().contains("NOTE")) {
				String[] label = attribute.getLabel().split("_");
				if (label.length > 1 && StringUtils.isNumeric(label[1])) {
					int id = Integer.parseInt(label[1]);
					Individual indi = event.getIndividuals().getById(id);
					story += indi.signature() + ": ";
				}
				story += attribute.getValue();
				result.appendln(story);
			}
		}

		//
		return result;
	}
	

	/**
	 * 
	 * @param segmentation
	 * @param criteria
	 * @param bundle
	 * @return
	 */
	public static Report reportDiscontinuousItineraries(final Segmentation segmentation, final SequenceCriteria criteria, final ResourceBundle bundle) {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		int errorCount = 0;
		StringList errors = new StringList();

		//
		result = new Report();
		result.setTitle("Discontinuous Itineraries");
		result.setOrigin("Control reporter");
		result.setTarget(segmentation.getLabel());

		for (EgoSequence itinerary : SequenceMaker.createPersonalSequences(segmentation, criteria).toSortedList()) {
			EgoSequences partials = SequenceWorker.split(itinerary);
			if (partials.size() > 1) {
				errorCount++;
				errors.appendln(itinerary.getEgo().signature());
				int j = 0;
				for (EgoSequence partial : partials) {
					if (partial.getStations().size() != 0 && j > 0) {
						errors.appendln(partial.getFirstTime() + "\t" + partial.getStations().get(partial.getFirstTime()));
					}
					j++;
				}
				errors.appendln();
			}
		}

		//
		errors.add(0, errorCount + " " + Report.translate(bundle, "Discontinuous Itineraries") + "\n");
		result.outputs().append(errors.toString());

		//
		result.setStatus(errorCount);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}
	
	/**
	 * 
	 * @param segmentation
	 * @param criteria
	 * @param bundle
	 * @return
	 */
	public static Report reportUnknownRelations(final Segmentation segmentation, final SequenceCriteria criteria, final ResourceBundle bundle) throws PuckException {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		int errorCount = 0;
		StringList errors = new StringList();

		//
		result = new Report();
		result.setTitle("Unknown Relations");
		result.setOrigin("Sequence reporter");
		result.setTarget(segmentation.getLabel());
		
		Map<Individual,Individuals> unknownRelations = new TreeMap<Individual,Individuals>();
				
		for (EgoSequence itinerary : SequenceMaker.createPersonalSequences(segmentation, criteria).toSortedList()) {
			
			RelationEnvironment egoEnvironment = new RelationEnvironment(((EgoSequence)itinerary).getStations().values(), ((EgoSequence)itinerary).getEgo(), criteria.getEgoRoleName(), criteria.getRoleNames(), criteria.getRelationModelNames(), criteria.getImpersonalAlterLabel());
			egoEnvironment.setAlterRelations(((Sequenceable<Relation>)itinerary).getStations().values(),((EgoSequence)itinerary).getEgo(),criteria.getEgoRoleName(),"ALL", criteria.getRelationModelNames(), criteria.getPattern(), criteria.getChainClassification());

/*			Set<Individual> alters = new HashSet<Individual>();
			for (Relation event : itinerary.getEvents().values()){
				for (Individual alter : event.getIndividuals()){
					alters.add(alter);
				}
			}
			Map<Individual,List<String>> relationsByAlter = NetUtils.getAlterRelations1(itinerary.getEgo(),alters,ToolBox.stringsToInts(criteria.getPattern()),criteria.getRelationModelNames(), criteria.getChainClassification(),null);*/
			
			
			for (Individual alter : egoEnvironment.getAllAlters()){
				if (egoEnvironment.getRelationsByAlter().get(alter).size()==0){
					Individuals unknowns = unknownRelations.get(itinerary.getEgo());
					if (unknowns==null){
						unknowns = new Individuals();
						unknownRelations.put(itinerary.getEgo(), unknowns);
					}
					unknowns.put(alter);
					errorCount++;
				}
			}
		}

		//
		errors.add(0, errorCount + " " + Report.translate(bundle, "Unknown Relations") + "\n");
		for (Individual ego : unknownRelations.keySet()){
			for (Individual alter: unknownRelations.get(ego).toSortedList()){
				errors.add(ego+"\t"+alter+"\n");
			}
		}

		result.outputs().append(errors.toString());

		//
		result.setStatus(errorCount);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}
	

	/**
	 * 
	 * @param segmentation
	 * @param criteria
	 * @param bundle
	 * @return
	 * @throws PuckException 
	 */
	public static Report reportDiscontinuousBiographies(final Net net, final Segmentation segmentation, final SequenceCriteria criteria, final ResourceBundle bundle) throws PuckException {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		int errorCount = 0;
		StringList errors = new StringList();

		//
		result = new Report();
		result.setTitle("Discontinuous Biographies");
		result.setOrigin("Sequence reporter");
		result.setTarget(segmentation.getLabel());

		for (EgoSequence itinerary : SequenceMaker.createBiographies(net, segmentation, criteria).toSortedList()) {
			EgoSequences partials = SequenceWorker.split(itinerary);
			if (partials.size() > 1) {
				errorCount++;
				errors.appendln(itinerary.getEgo().signature());
				int j = 0;
				for (EgoSequence partial : partials) {
					if (partial.getStations().size() != 0 && j > 0) {
						errors.appendln(partial.getFirstTime() + "\t" + partial.getStations().get(partial.getFirstTime()));
					}
					j++;
				}
				errors.appendln();
			}
		}

		//
		errors.add(0, errorCount + " " + Report.translate(bundle, "Discontinuous Biographies") + "\n");
		result.outputs().append(errors.toString());

		//
		result.setStatus(errorCount);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * 
	 * @param segmentation
	 * @param criteria
	 * @param bundle
	 * @return
	 */
	public static Report reportMissingTestimonies(final Segmentation segmentation, final MissingTestimoniesCriteria criteria, final ResourceBundle bundle) {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		int errorCount = 0;
		StringList errors = new StringList();

		//
		result = new Report();
		result.setTitle("Missing Testimonies");
		result.setOrigin("Sequence reporter");
		result.setTarget(segmentation.getLabel());

		for (Relation event : segmentation.getCurrentRelations().getByModelName(criteria.getRelationModelName())) {
			errors.appendln("*"+event.getModel()+" "+event.getName());
			for (Individual witness : event.getIndividuals(criteria.getEgoRoleName())) {
				if (segmentation.getCurrentIndividuals().contains(witness)) {
					Actor actor = event.getActor(witness, criteria.getEgoRoleName());
					if (actor.getAttributeValue("NOTE")==null) {
					
/*					for (Attribute attribute : actor.attributes()) {
						if (attribute.getLabel().contains("NOTE")) {
							String signature = witness.getId()+":";
							if (attribute.getValue().contains(signature)){
//							}
						}
					}
					if (missing) {*/
						errorCount++;
						errors.appendln(witness.signature() + "\t" + event.getTypedId() + "\t" + event.getName());
					}
				}
			}
		}

		//
		errors.add(0, errorCount + " " + Report.translate(bundle, "Missing Testimonies") + "\n");
		result.outputs().append(errors.toString());

		//
		result.setStatus(errorCount);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}
	
	private static <S extends Sequenceable<E>,E extends Numberable> void createReports(Report totalReport, final List<String> titles, final Map<String,Report> reports, final Segmentation segmentation, final SequenceStatistics<S,E> sequenceStatistics, final SequenceCriteria sequenceCriteria, final Map<String, StringList> pajekBuffers, int width) throws PuckException{
		
		// Flow and Migration reports are not yet logically integrated

		for (String title : titles){
			
			if (title.equals("SURVEY")){
				
				reports.put(title, reportSurvey("Survey",sequenceStatistics, sequenceCriteria));
				
			} else if (title.equals("DIAGRAMS")){
				
				reports.put(title, reportDiagramsSequenceValues("Diagrams", sequenceStatistics, sequenceCriteria, segmentation,3));
			
			} else if (title.equals("DIAGRAMS_DATED")){
				
				reports.put(title, reportDiagramsValueSequences("Diagrams",sequenceStatistics,sequenceCriteria,segmentation,3));
			
			} else if (title.equals("DETAILS")){
				
				reports.put(title, reportDetailsSequenceValues("Details", sequenceStatistics, sequenceCriteria,sequenceCriteria.getSequenceValueCriteriaList().getLabels()));
			
			} else if (title.equals("DETAILS_DATED")){
				
				reports.put(title, reportDetailsValueSequences("Details", sequenceStatistics, sequenceCriteria,sequenceCriteria.getValueSequenceCriteriaList().getLabels(), pajekBuffers));
			
				for (PartitionCriteria valueSequenceCriteria : sequenceCriteria.getValueSequenceCriteriaList()){
					
					String label = valueSequenceCriteria.getLabel();
					
					List<?> mapKeys = sequenceStatistics.getPartitionSequence(label).getMapKeys();
					
					if (mapKeys != null){
						
						List<String> fineLabels = new ArrayList<String>();
						
						for (Object key : mapKeys){
							fineLabels.add(label+"$"+key); 
						}
						
						reports.put("DETAILS_DATED_"+label, reportDetailsValueSequences("Details_"+label,sequenceStatistics,sequenceCriteria,fineLabels,pajekBuffers));
					}
				}

			} else if (title.equals("CENSUS")){
				
				reports.put(title, reportCensuses("Censuses", sequenceStatistics, sequenceCriteria));
			
			} else if (title.equals("MATRICES")){
				
				reports.put(title, reportMatrices("Matrices", sequenceStatistics, sequenceCriteria,pajekBuffers));
			
			} else if (title.equals("FLOWS")){
				
				reports.put(title, reportFlows("Flows",sequenceStatistics,sequenceCriteria));
				
			} else if (title.equals("TREES")){
				
				reports.put(title, reportSequenceTree("Ramification trees", sequenceStatistics));
				
			} else if (title.equals("COMPONENTS")){
				
				reports.put(title, reportComponents("Components",sequenceStatistics,sequenceCriteria));

			}
		}
		
		for (Report report : reports.values()){
			
			if (report instanceof ChartReport){
				
				((ChartReport)report).arrangeChartsAndTables(width);

			}
			
			if (report.hasOutput()) {
				
				totalReport.outputs().append(report);
			}
		}
		
	}

	
	/**
	 * 
	 * @param segmentation
	 * @param censusType
	 * @return
	 * @throws PuckException
	 */
/*	public static <S extends Sequenceable<E>,E extends Numberable> Report reportGeneralSequenceCensus (final String title, final Segmentation segmentation, final Sequenceables<S,E> sequences, final SequenceCriteria spaceTimeCriteria) throws PuckException{
		Report result;
			
		result = new Report(title+" "+spaceTimeCriteria.getRelationModelName());
		result.setOrigin("Sequence reporter");

		//
		Chronometer chrono = new Chronometer();
		
		StringList pajekBufferStations = new StringList();
		StringList pajekBufferTransitions = new StringList();
		
		Map<String,Report> reports = new HashMap<String,Report>();

		// Create Partition charts and tables
//		List<ReportChart> charts = new ArrayList<ReportChart>();
//		List<ReportTable> tables = new ArrayList<ReportTable>();


		// Proceed census
		SequenceStatistics<S,E> sequenceStatistics = new EgoSequenceStatistics<S,E>(segmentation, sequences, spaceTimeCriteria);
		
		// Set partition criteria (put outside method)
		
		// Create Reports

		String[] reportTitles = new String[]{"SURVEY","DIAGRAMS","DETAILS"};
		createReports(reportTitles,reports,segmentation,sequenceStatistics,spaceTimeCriteria,pajekBufferStations,pajekBufferTransitions);*/
		
/*		List<Report> reportSupplements = new ArrayList<Report>();

		
		for (PartitionCriteria valueSequenceCriteria : spaceTimeCriteria.getValueSequenceCriteriaList()){
			
			String label = valueSequenceCriteria.getLabel();
			ValueCode valueCode = valueSequenceCriteria.getValueCode();
			
			if (valueSequenceCriteria.isWithCensus()){
				
				reportCensus(reportCensus,sequenceStatistics.getPartitionSequence(label),label,valueCode, spaceTimeCriteria);
				
				if (sequenceStatistics.getAggregatePartitionSequence(label)!=null){

					reportCensus(reportCensus,sequenceStatistics.getAggregatePartitionSequence(label),label+"_TOTAL",null, spaceTimeCriteria);
				}
			}
			
			if (valueSequenceCriteria.isWithMatrix()){
				
				if (valueSequenceCriteria.isWithGraph()){
					
					reportMatrix(reportMatrices,sequenceStatistics.getPartitionSequence(label),label,valueCode, pajekBufferTransitions,spaceTimeCriteria);
					
				} else {
					
					reportMatrix(reportMatrices,sequenceStatistics.getPartitionSequence(label),label,valueCode, null,spaceTimeCriteria);
				}
			}
		}



		// Make overall report
		overallReport.outputs().appendln(
			"Measure\tAverage (Male)\tAverage Pos. (Male)\tMedian (Male)\tMaximum (Male)\tSum (Male)\tAverage (Female)\tAverage Pos. (Female)\tMedian (Female)\tMaximum (Female)\tSum (Female)\tAverage (All)\tAverage Pos. (All)\tMedian (All)\tMaximum (All)\tSum (All)");

		for (PartitionCriteria partitionCriteria : spaceTimeCriteria.getSequenceValueCriteriaList()){

			String label = partitionCriteria.getLabel();

			Partition<EgoSequence> partition = sequenceStatistics.getSequencePartition(label);
			
//			NumberedValues values = sequenceStatistics.getValues(label);
			
			if (partition.isNumeric()){

				NumberedValues[] genderedValues = PuckUtils.getGenderedNumberedValues(partition);

				overallReport.outputs().append(label + "\t");
				for (int gender = 0; gender < 3; gender++) {
					String sum = "";
					if (label.startsWith("NR")) {
						sum = new Double(genderedValues[gender].sum()).intValue() + "";
					}
					overallReport.outputs().append(
							MathUtils.round(genderedValues[gender].average(), 2) + "\t" + MathUtils.round(genderedValues[gender].averagePositives(), 2) + "\t"
									+ genderedValues[gender].median() + "\t" + genderedValues[gender].max() + "\t" + sum + "\t");
				}
				overallReport.outputs().appendln();

			}
		}
		overallReport.outputs().appendln();*/
		
		// Set partition criteria
//		for (PartitionCriteria partitionCriteria : spaceTimeCriteria.getSequenceValueCriteriaList()){

/*			PartitionCriteria partitionCriteria = new PartitionCriteria(label);
			// partitionCriteria.setValueFilter(ValueFilter.NULL);*/

			/*			if (label.contains("PROFILE")){
							partitionCriteria.setType(PartitionType.PARTIALIZATION);
						} */

/*			if (label.equals("NREVENTS") || label.contains("NRSTATIONS")) {
				partitionCriteria.setType(PartitionType.FREE_GROUPING);
				partitionCriteria.setIntervals(PartitionMaker.getIntervals("1 5 10 15 20 25"));
				// partitionCriteria.setCumulationType(CumulationType.DESCENDANT);
			} else if (label.contains("AGEFIRST")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(0.);
				partitionCriteria.setSize(5.);
			} else if (label.equals("ECCENTRICITY")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(-100.);
				partitionCriteria.setSize(20.);
			} else if (label.contains("COVERAGE") || label.contains("SAME") || label.contains("NORM") || label.contains("DENSITY")
					|| label.contains("BETWEENNESS") || label.contains("EFFICIENCY") || label.contains("CONCENTRATION")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(0.);
				partitionCriteria.setSize(20.);
			} else if (label.contains("MEAN") || label.contains("COVERAGE") || label.contains("PEREVENT") || label.contains("BETWEENNESS")
					|| label.contains("BROKERAGE") || label.contains("EFFICIENT_SIZE")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(0.);
				partitionCriteria.setSize(1.);
			} else {
				partitionCriteria.setType(PartitionType.RAW);
			}*/

/*			String label = partitionCriteria.getLabel();
			NumberedValues values = sequenceStatistics.getValues(label);

//			System.out.println(label+" "+values);
			
			ReportChart chart = null;

			if (!label.contains("ALTERS") && !label.contains("PROFILE")&& !label.equals("MEAN_NR_MOVES")) {
								
				Partition<Individual> partition = PartitionMaker.create(label, segmentation.getCurrentIndividuals(), values, partitionCriteria);

				PartitionCriteria splitCriteria = new PartitionCriteria(spaceTimeCriteria.getPartitionLabel());
				chart = StatisticsReporter.createPartitionChart(partition, partitionCriteria, splitCriteria);

				if (label.substring(0, 3).equals("AGE")) {

					partitionCriteria.setType(PartitionType.RAW);
					partitionCriteria.setSizeFilter(SizeFilter.HOLES);
					partitionCriteria.setValueFilter(ValueFilter.NULL);

					partition = PartitionMaker.create(label, segmentation.getCurrentIndividuals(), values, partitionCriteria);

					if (partition.maxValue() != null) {
						ReportChart survivalChart = StatisticsReporter.createSurvivalChart(partition, splitCriteria);
						charts.add(survivalChart);
					} else {
						System.err.println(label + " no max value");
					}
				}

			}
			
			if (label.equals("MEAN_NR_MOVES")){
				Map<Value,Double[]> map = sequenceStatistics.getMeanNrMoves();
				chart = StatisticsReporter.createMapChart(map, label, new String[]{"MALE","FEMALE"}, GraphType.STACKED_BARS);
			}

			if (chart != null) {
				charts.add(chart);
				ReportTable table = ReportTable.transpose(chart.createReportTableWithSum());
				tables.add(table);
				if (!label.contains("EVENTS_") && !label.contains("RELATIONS")) {
//					tables.add(ReportTable.normalize(table));
				}
			}
		}*/


		// Create detailed report
				

/*		reportDetails.outputs().appendln("Old report:");
		reportDetails.outputs().append("Nr\tEgo\tGender");
		for (String partitionLabel : spaceTimeCriteria.getCensusOperationLabels()) {
			reportDetails.outputs().append("\t" + partitionLabel);
		}
		reportDetails.outputs().appendln();

		for (Individual ego : segmentation.getCurrentIndividuals().toSortedList()) {

			if (sequenceStatistics.getValues("NREVENTS").get(ego.getId()) != null){
				reportDetails.outputs().append(ego.getId() + "\t" + ego + "\t" + ego.getGender());
				for (String label : spaceTimeCriteria.getCensusOperationLabels()) {
					reportDetails.outputs().append("\t" + sequenceStatistics.getValues(label).get(ego.getId()));
				}
				reportDetails.outputs().appendln();
			}
		}

		// Manage the number of chart by line.
		for (int chartIndex = 0; chartIndex < charts.size(); chartIndex++) {
			reportDiagrams.outputs().append(charts.get(chartIndex));
			if (chartIndex % 4 == 3) {
				reportDiagrams.outputs().appendln();
			}
		}

		// Add chart tables.
		for (ReportTable table : tables) {
			reportDiagrams.outputs().appendln(table.getTitle());
			reportDiagrams.outputs().appendln(table);
		}*/

		
		// Finalize reports
/*		result.outputs().append(reportSurvey);
		
		reportDiagrams.arrangeChartsAndTables(3);
		result.outputs().append(reportDiagrams);
		
		result.outputs().append(reportDetails);*/

		// addPajekData

/*		Map<String, StringList> pajekBuffers = sequenceStatistics.getPajekBuffers();

		for (String bufferTitle : pajekBuffers.keySet()) {

			StringList pajekBuffer = pajekBuffers.get(bufferTitle);
			if (pajekBuffer.length() != 0) {
				File targetFile = ToolBox.setExtension(ToolBox.addToName(new File(segmentation.getLabel()), "-" + bufferTitle), ".paj");
				ReportRawData rawData = new ReportRawData("Export " + bufferTitle + "s to Pajek", "Pajek", "paj", targetFile);
				rawData.setData(PAJFile.convertToMicrosoftEndOfLine(pajekBuffer.toString()));

				result.outputs().appendln();
				result.outputs().append(rawData);
			}
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;

	}*/
	

	/**
	 * 
	 * @param segmentation
	 * @param censusType
	 * @return
	 * @throws PuckException
	 */
/*	public static <S extends Sequenceable<E>,E extends Numberable> Report reportEgoNetworksCensus(final String title, final Segmentation segmentation, final Sequenceables<S,E> sequences, final SequenceCriteria spaceTimeCriteria) throws PuckException {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		result = new Report("Ego Networks");
		result.setOrigin("Sequence reporter");

		// Create Partition charts and tables
//		List<ReportChart> charts = new ArrayList<ReportChart>();
//		List<ReportTable> tables = new ArrayList<ReportTable>();

		// Get (coherent) itineraries
		EgoSequences sequences = new EgoSequences();
		for (Individual ego : segmentation.getCurrentIndividuals().toSortedList()) {
			sequences.addRenumbered(SequenceWorker.getCoherentItinerarySegment(ego, censusCriteria));
		}

		// Proceed census
		SequenceStatistics sequenceStatistics = new SequenceStatistics(segmentation, sequences, spaceTimeCriteria);

		
		// Create Reports
		Report overallReport = reportSurvey("Survey",sequenceStatistics, spaceTimeCriteria);
		ChartReport diagramReport = reportDiagramsSequenceValues("Diagrams", sequenceStatistics, spaceTimeCriteria, segmentation,3);
		Report detailReport = reportDetailsSequenceValues("Details", sequenceStatistics, spaceTimeCriteria,spaceTimeCriteria.getSequenceValueCriteriaList().getLabels());

		Report componentReport = null;

		if (spaceTimeCriteria.getEgoNetworksOperations().contains(EgoNetworksOperation.COHESION)){
			componentReport = reportComponents("Components",sequenceStatistics,spaceTimeCriteria);
		}
		

		// Make overall report and diagrams
		overallReport
				.outputs()
				.appendln(
						"Measure\tAverage (Male)\tAverage Pos. (Male)\tMedian (Male)\tMaximum (Male)\tSum (Male)\tAverage (Female)\tAverage Pos. (Female)\tMedian (Female)\tMaximum (Female)\tSum (Female)\tAverage (All)\tAverage Pos. (All)\tMedian (All)\tMaximum (All)\tSum (All)");

		// Set partition criteria
		for (String label : spaceTimeCriteria.getCensusOperationLabels()) {

			PartitionCriteria partitionCriteria = new PartitionCriteria(label);
			// partitionCriteria.setValueFilter(ValueFilter.NULL);

			/*			if (label.contains("PROFILE")){
							partitionCriteria.setType(PartitionType.PARTIALIZATION);
						} */

/*			if (label.equals("NREVENTS")) {
				partitionCriteria.setType(PartitionType.RAW);
				// partitionCriteria.setCumulationType(CumulationType.DESCENDANT);
			} else if (label.contains("AGEFIRST")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(0.);
				partitionCriteria.setSize(5.);
			} else if (label.equals("ECCENTRICITY")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(-100.);
				partitionCriteria.setSize(20.);
			} else if (label.contains("COVERAGE") || label.contains("SAME") || label.contains("NORM") || label.contains("DENSITY")
					|| label.contains("BETWEENNESS") || label.contains("EFFICIENCY") || label.contains("CONCENTRATION")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(0.);
				partitionCriteria.setSize(20.);
			} else if (label.contains("MEAN") || label.contains("COVERAGE") || label.contains("PEREVENT") || label.contains("BETWEENNESS")
					|| label.contains("BROKERAGE") || label.contains("EFFICIENT_SIZE")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(0.);
				partitionCriteria.setSize(1.);
			} else {
				partitionCriteria.setType(PartitionType.RAW);
			}

			ReportChart chart = null;

			if (!label.contains("ALTERS") && !label.contains("PROFILE")) {
				NumberedValues values = sequenceStatistics.getValues(label);

				Partition<Individual> partition = PartitionMaker.create(label, segmentation.getCurrentIndividuals(), values, partitionCriteria);

				PartitionCriteria splitCriteria = new PartitionCriteria(spaceTimeCriteria.getPartitionLabel());
				chart = StatisticsReporter.createPartitionChart(partition, partitionCriteria, splitCriteria);

				if (label.substring(0, 3).equals("AGE")) {

					partitionCriteria.setType(PartitionType.RAW);
					partitionCriteria.setSizeFilter(SizeFilter.HOLES);
					partitionCriteria.setValueFilter(ValueFilter.NULL);

					partition = PartitionMaker.create(label, segmentation.getCurrentIndividuals(), values, partitionCriteria);

					if (partition.maxValue() != null) {
						ReportChart survivalChart = StatisticsReporter.createSurvivalChart(partition, splitCriteria);
						charts.add(survivalChart);
					} else {
						System.err.println(label + " no max value");
					}
				}

				NumberedValues[] genderedValues = PuckUtils.getGenderedNumberedValues(values, segmentation.getCurrentIndividuals());

				overallReport.outputs().append(label + "\t");
				for (int gender = 0; gender < 3; gender++) {
					String sum = "";
					if (label.startsWith("NR")) {
						sum = new Double(genderedValues[gender].sum()).intValue() + "";
					}
					overallReport.outputs().append(
							MathUtils.round(genderedValues[gender].average(), 2) + "\t" + MathUtils.round(genderedValues[gender].averagePositives(), 2) + "\t"
									+ values.median() + "\t" + genderedValues[gender].max() + "\t" + sum + "\t");
				}
				overallReport.outputs().appendln();

			}

			if (chart != null) {
				charts.add(chart);
					ReportTable table = ReportTable.transpose(chart.createReportTableWithSum());
					tables.add(table);
					if (!label.contains("EVENTS_") && !label.contains("RELATIONS")) {
//						tables.add(ReportTable.normalize(table));
				}

			}

		}
		overallReport.outputs().appendln();

		// Create detailed report
		detailReport.outputs().append("Nr\tEgo\tGender");
		for (String partitionLabel : spaceTimeCriteria.getCensusOperationLabels()) {
				detailReport.outputs().append("\t" + partitionLabel);
		}
		
		Map<String,Map<Value, Double[]>> componentChartMaps = new TreeMap<String, Map<Value, Double[]>>();
		Map<String,Map<Value, Double[]>> componentChartSizeMaps = new TreeMap<String, Map<Value, Double[]>>();
		for (String networkTitle : spaceTimeCriteria.getNetworkTitles()){
			componentChartMaps.put(networkTitle,new TreeMap<Value, Double[]>());
			componentChartSizeMaps.put(networkTitle,new TreeMap<Value, Double[]>());
		}

//		detailReport.outputs().appendln();
		for (Individual ego : segmentation.getCurrentIndividuals().toSortedList()) {

			if (census.getValues("SIZE").get(ego.getId()) != null) {
				detailReport.outputs().append(ego.getId() + "\t" + ego + "\t" + ego.getGender());
				for (String label : spaceTimeCriteria.getCensusOperationLabels()) {
						detailReport.outputs().append("\t" + census.getValues(label).get(ego.getId()));
				}
				detailReport.outputs().appendln();
			}
			
			if (spaceTimeCriteria.getEgoNetworksOperations().contains(EgoNetworksOperation.COHESION)){

				for (String networkTitle : spaceTimeCriteria.getNetworkTitles()) {
					Map<Integer, Partition<Node<Individual>>> componentsMap = sequenceStatistics.getComponents(networkTitle);
					Map<Value,Double[]> componentChartMap = componentChartMaps.get(networkTitle);
					Map<Value,Double[]> componentChartSizeMap = componentChartSizeMaps.get(networkTitle);
					
					if (componentsMap != null) {
						Partition<Node<Individual>> components = componentsMap.get(ego.getId());

						componentReport.outputs().appendln("Components " + networkTitle);
						componentReport.outputs().appendln(ego + "\t" + components.size());
						int i = 1;
						for (Cluster<Node<Individual>> cluster : components.getClusters().toListSortedByValue()) {
							componentReport.outputs().appendln(
									"\t" + i + "\t" + cluster.getValue() + "\t(" + cluster.size() + ")\t" + cluster.getItemsAsString());
							i++;
						}
						componentReport.outputs().appendln();

						for (Value value : components.getValues()) {
							Double[] map = componentChartMap.get(value);
							Double[] map2 = componentChartSizeMap.get(value);
							if (map == null) {
								map = new Double[]{0.,0.,0.};
								componentChartMap.put(value, map);
								map2 = new Double[]{0.,0.,0.};
								componentChartSizeMap.put(value, map2);
							}
							map[ego.getGender().toInt()]+=1.;
							map[2]+=1.;
							map2[ego.getGender().toInt()]+=components.getCluster(value).size();
							map2[2]+=components.getCluster(value).size();
						}
					}
				}
			}
		}
		
		for (String networkTitle : spaceTimeCriteria.getNetworkTitles()) {
			
			Map<Value,Double[]> componentChartMap = componentChartMaps.get(networkTitle);
			Map<Value,Double[]> componentChartSizeMap = componentChartSizeMaps.get(networkTitle);
			for (Value value: componentChartMap.keySet()){
				Double[] sums = componentChartSizeMap.get(value);
				for (int i=0;i<3;i++){
					sums[i]=new Double(sums[i]/componentChartMap.get(value)[i]);
				}
			}

			ReportChart componentChart = StatisticsReporter.createMapChart(componentChartMap, "COMPONENTS",new String[]{"MALE","FEMALE"}, GraphType.STACKED_BARS);
			charts.add(componentChart);
			tables.add(ReportTable.transpose(componentChart.createReportTableWithSum()));
			
			ReportChart componentSizeChart = StatisticsReporter.createMapChart(componentChartSizeMap, "COMPONENTS",new String[]{"MALE","FEMALE"}, GraphType.STACKED_BARS);
			charts.add(componentSizeChart);
			tables.add(ReportTable.transpose(componentSizeChart.createReportTableWithSum()));

				if (census.getRelationConnectionMatrix() != null) {
				for (ReportChart chart : census.getRelationConnectionMatrix().getCharts()) {
					charts.add(chart);
				}
				tables.add(census.getRelationConnectionMatrix().getTable("Component Connections"));
			}
		}

		// Manage the number of chart by line.
		for (int chartIndex = 0; chartIndex < charts.size(); chartIndex++) {
			diagramReport.outputs().append(charts.get(chartIndex));
			if (chartIndex % 4 == 3) {
				diagramReport.outputs().appendln();
			}
		}

		// Add chart tables.
		for (ReportTable table : tables) {
			diagramReport.outputs().appendln(table.getTitle());
			diagramReport.outputs().appendln(table);
		}

		// Finalize reports
		result.outputs().append(overallReport);
		diagramReport.arrangeChartsAndTables(3);
		result.outputs().append(diagramReport);
		result.outputs().append(detailReport);
		
		if (componentReport!=null){
			result.outputs().append(componentReport);
		}

		// addPajekData
		
		if (spaceTimeCriteria.getEgoNetworksOperations().contains(EgoNetworksOperation.EXPORT_EGO_NETWORKS)){

			Map<String, StringList> pajekBuffers = sequenceStatistics.getPajekBuffers();

			for (String bufferTitle : pajekBuffers.keySet()) {

				StringList pajekBuffer = pajekBuffers.get(bufferTitle);
				if (pajekBuffer.length() != 0) {
					File targetFile = ToolBox.setExtension(ToolBox.addToName(new File(segmentation.getLabel()), "-" + bufferTitle), ".paj");
					ReportRawData rawData = new ReportRawData("Export " + title + "s to Pajek", "Pajek", "paj", targetFile);
					rawData.setData(PAJFile.convertToMicrosoftEndOfLine(pajekBuffer.toString()));

					result.outputs().appendln();
					result.outputs().append(rawData);
				}
			}
		
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;

	}*/
	
	
	private static <S> int gender(S sequence){
		int gender;
		
		gender = 0;
		
		if (sequence instanceof Individualizable){
			gender = ((Individualizable)sequence).getEgo().getGender().toInt();
		}
		//
		return gender;
	}
	private static <S extends Sequenceable<E>,E extends Numberable> void createComponentCharts (ChartReport chartReport, String networkTitle, SequenceStatistics<S,E> sequenceStatistics, SequenceCriteria sequenceCriteria) throws PuckException{
				
		Map<S, Partition<Node<Individual>>> componentsMap = sequenceStatistics.getComponents(networkTitle);

		if (componentsMap != null) {

			Map<Value,Double[]> componentChartMap = new TreeMap<Value, Double[]>(); // componentChartMaps.get(networkTitle);
			Map<Value,Double[]> componentChartSizeMap = new TreeMap<Value, Double[]>(); // componentChartSizeMaps.get(networkTitle);

			for (S sequence : componentsMap.keySet()) {

				Partition<Node<Individual>> components = componentsMap.get(sequence);

				for (Value value : components.getValues()) {
					Double[] map = componentChartMap.get(value);
					Double[] map2 = componentChartSizeMap.get(value);
					if (map == null) {
						map = new Double[]{0.,0.,0.};
						componentChartMap.put(value, map);
						map2 = new Double[]{0.,0.,0.};
						componentChartSizeMap.put(value, map2);
					}
					map[gender(sequence)]+=1.;
					map[2]+=1.;
					map2[gender(sequence)]+=components.getCluster(value).size();
					map2[2]+=components.getCluster(value).size();
				}
			}
			
			for (Value value: componentChartMap.keySet()){
				Double[] sums = componentChartSizeMap.get(value);
				for (int i=0;i<3;i++){
					sums[i]=new Double(sums[i]/componentChartMap.get(value)[i]);
				}
			}
			//
			ReportChart componentChart = StatisticsReporter.createMapChart(componentChartMap, "COMPONENTS",new String[]{"MALE","FEMALE"}, GraphType.STACKED_BARS);
			chartReport.addChartWithTables(componentChart,"Components "+networkTitle);
			
			ReportChart componentSizeChart = StatisticsReporter.createMapChart(componentChartSizeMap, "COMPONENTS",new String[]{"MALE","FEMALE"}, GraphType.STACKED_BARS);
			chartReport.addChartWithTables(componentSizeChart,"Components Size "+networkTitle);
			
			SequenceNetworkStatistics<S,E> componentConnections = sequenceStatistics.getRelationConnectionMatrix();
			
			if (componentConnections!=null){
				for (ReportChart chart :componentConnections.getCharts()){
					chartReport.addChart(chart);
				}
				chartReport.addTable(componentConnections.getTable("Component Connections "+networkTitle));
			}
		}
	}
	
	private static <S extends Sequenceable<E>,E extends Numberable> Report reportComponents (String title, SequenceStatistics<S,E> sequenceStatistics, SequenceCriteria sequenceCriteria){
		Report componentReport;
		
		componentReport = new Report(title);
		
/*		Map<String,Map<Value, Double[]>> componentChartMaps = new TreeMap<String, Map<Value, Double[]>>();
		Map<String,Map<Value, Double[]>> componentChartSizeMaps = new TreeMap<String, Map<Value, Double[]>>();
		
		for (String networkTitle : spaceTimeCriteria.getNetworkTitles()){
			componentChartMaps.put(networkTitle,new TreeMap<Value, Double[]>());
			componentChartSizeMaps.put(networkTitle,new TreeMap<Value, Double[]>());
		}*/
		
//		if (spaceTimeCriteria.getEgoNetworksOperations().contains(EgoNetworksOperation.COHESION)){

			for (String networkTitle : sequenceCriteria.getNetworkTitles()) {
				
				Map<S, Partition<Node<Individual>>> componentsMap = sequenceStatistics.getComponents(networkTitle);
//				Map<Value,Double[]> componentChartMap = new TreeMap<Value, Double[]>(); // componentChartMaps.get(networkTitle);
//				Map<Value,Double[]> componentChartSizeMap = new TreeMap<Value, Double[]>(); // componentChartSizeMaps.get(networkTitle);

				
//		detailReport.outputs().appendln();
				if (componentsMap != null) {
					
					for (S sequence : new ArrayList<S>(componentsMap.keySet())) {

/*			if (census.getValues("SIZE").get(ego.getId()) != null) {
				detailReport.outputs().append(ego.getId() + "\t" + ego + "\t" + ego.getGender());
				for (String label : spaceTimeCriteria.getCensusOperationLabels()) {
						detailReport.outputs().append("\t" + census.getValues(label).get(ego.getId()));
				}
				detailReport.outputs().appendln();
			}*/
						Partition<Node<Individual>> components = componentsMap.get(sequence);

						componentReport.outputs().appendln("Components " + networkTitle);
						componentReport.outputs().appendln(sequence + "\t" + components.size());
						int i = 1;
						for (Cluster<Node<Individual>> cluster : components.getClusters().toListSortedByValue()) {
							componentReport.outputs().appendln(
									"\t" + i + "\t" + cluster.getValue() + "\t(" + cluster.size() + ")\t" + cluster.getItemsAsString());
							i++;
						}
						componentReport.outputs().appendln();
					}
				}
			}		
						
/*						for (Value value : components.getValues()) {
							Double[] map = componentChartMap.get(value);
							Double[] map2 = componentChartSizeMap.get(value);
							if (map == null) {
								map = new Double[]{0.,0.,0.};
								componentChartMap.put(value, map);
								map2 = new Double[]{0.,0.,0.};
								componentChartSizeMap.put(value, map2);
							}
							map[ego.getGender().toInt()]+=1.;
							map[2]+=1.;
							map2[ego.getGender().toInt()]+=components.getCluster(value).size();
							map2[2]+=components.getCluster(value).size();
						}
					}
				}*/
//			}
//		}
		
//		for (String networkTitle : spaceTimeCriteria.getNetworkTitles()) {
			
//			Map<Value,Double[]> componentChartMap = componentChartMaps.get(networkTitle);
//			Map<Value,Double[]> componentChartSizeMap = componentChartSizeMaps.get(networkTitle);
/*			for (Value value: componentChartMap.keySet()){
				Double[] sums = componentChartSizeMap.get(value);
				for (int i=0;i<3;i++){
					sums[i]=new Double(sums[i]/componentChartMap.get(value)[i]);
				}
			}

			ReportChart componentChart = StatisticsReporter.createMapChart(componentChartMap, "COMPONENTS",new String[]{"MALE","FEMALE"}, GraphType.STACKED_BARS);
			charts.add(componentChart);
			tables.add(ReportTable.transpose(componentChart.createReportTableWithSum()));
			
			ReportChart componentSizeChart = StatisticsReporter.createMapChart(componentChartSizeMap, "COMPONENTS",new String[]{"MALE","FEMALE"}, GraphType.STACKED_BARS);
			charts.add(componentSizeChart);
			tables.add(ReportTable.transpose(componentSizeChart.createReportTableWithSum()));

				if (census.getRelationConnectionMatrix() != null) {
				for (ReportChart chart : census.getRelationConnectionMatrix().getCharts()) {
					charts.add(chart);
				}
				tables.add(census.getRelationConnectionMatrix().getTable("Component Connections"));
			}*/
//		}
		//
		return componentReport;
	}

	/**
	 * 
	 * @param segmentation
	 * @param censusType
	 * @return
	 * @throws PuckException
	 */
/*	public static <S extends Sequenceable<E>,E extends Numberable> Report reportParcoursNetworksCensus(final String reportTitle, final Segmentation segmentation, final Sequenceables<S,E> sequences, final SequenceCriteria spaceTimeCriteria) throws PuckException {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		result = new Report(reportTitle);
		result.setOrigin("Sequence reporter");

		
		// Create Partition charts and tables
		List<ReportChart> charts = new ArrayList<ReportChart>();
		List<ReportTable> tables = new ArrayList<ReportTable>();

		// Get (coherent) itineraries
		EgoSequences sequences = new EgoSequences();
		for (Individual ego : segmentation.getCurrentIndividuals().toSortedList()) {
			sequences.addRenumbered(SequenceWorker.getCoherentItinerarySegment(ego, censusCriteria));
		}

		// Proceed census
		SequenceStatistics<S,E> sequenceStatistics = new SequenceStatistics(segmentation, sequences, spaceTimeCriteria);

		
		// Create Reports
		Report overallReport = reportSurvey("Survey",sequenceStatistics,spaceTimeCriteria);
		Report diagramReport = reportDiagramsSequenceValues("Diagrams",sequenceStatistics,spaceTimeCriteria,segmentation,3);
		Report detailReport = reportDetailsSequenceValues("Details", sequenceStatistics, spaceTimeCriteria,spaceTimeCriteria.getSequenceValueCriteriaList().getLabels());
		Report componentReport = null;
		
		if (spaceTimeCriteria.getParcoursNetworksOperations().contains(ParcoursNetworksOperation.COHESION)){
			componentReport = new Report("Components");
		}

		// Make overall report and diagrams
		overallReport
				.outputs()
				.appendln(
						"Measure\tAverage (Male)\tAverage Pos. (Male)\tMedian (Male)\tMaximum (Male)\tSum (Male)\tAverage (Female)\tAverage Pos. (Female)\tMedian (Female)\tMaximum (Female)\tSum (Female)\tAverage (All)\tAverage Pos. (All)\tMedian (All)\tMaximum (All)\tSum (All)");

		// Set partition criteria
		for (String label : spaceTimeCriteria.getCensusOperationLabels()) {

			PartitionCriteria partitionCriteria = new PartitionCriteria(label);*/
			// partitionCriteria.setValueFilter(ValueFilter.NULL);

			/*			if (label.contains("PROFILE")){
							partitionCriteria.setType(PartitionType.PARTIALIZATION);
						} */

/*			if (label.equals("NREVENTS")) {
				partitionCriteria.setType(PartitionType.RAW);
				// partitionCriteria.setCumulationType(CumulationType.DESCENDANT);
			} else if (label.contains("AGEFIRST")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(0.);
				partitionCriteria.setSize(5.);
			} else if (label.equals("ECCENTRICITY")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(-100.);
				partitionCriteria.setSize(20.);
			} else if (label.contains("COVERAGE") || label.contains("SAME") || label.contains("NORM") || label.contains("DENSITY")
					|| label.contains("BETWEENNESS") || label.contains("EFFICIENCY") || label.contains("CONCENTRATION")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(0.);
				partitionCriteria.setSize(20.);
			} else if (label.contains("MEAN") || label.contains("COVERAGE") || label.contains("PEREVENT") || label.contains("BETWEENNESS")
					|| label.contains("BROKERAGE") || label.contains("EFFICIENT_SIZE")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(0.);
				partitionCriteria.setSize(1.);
			} else {
				partitionCriteria.setType(PartitionType.RAW);
			}

			ReportChart chart = null;

			if (label.contains("SIMILARITY")) {
				
				RelationClassificationType relationClassificationType = RelationClassificationType.valueOf(label.substring(label.lastIndexOf("_") + 1));
				Map<Value, Double[]> similaritiesMap = sequenceStatistics.getSimilaritiesMap(relationClassificationType);
				chart = StatisticsReporter.createMapChart(similaritiesMap, label, new String[] { "HH", "FH", "HF", "FF", "All" }, GraphType.LINES);

				for (Value key : similaritiesMap.keySet()) {
					overallReport.outputs().appendln(label + "_" + key + "\t" + MathUtils.percent(similaritiesMap.get(key)[4], 100));
				}

			} 
			
			else if (!label.contains("ALTERS") && !label.contains("PROFILE")) {
				NumberedValues values = census.getValues(label);

				Partition<Individual> partition = PartitionMaker.create(label, segmentation.getCurrentIndividuals(), values, partitionCriteria);

				PartitionCriteria splitCriteria = new PartitionCriteria(censusCriteria.getPartitionLabel());
				chart = StatisticsReporter.createPartitionChart(partition, partitionCriteria, splitCriteria);

				if (label.substring(0, 3).equals("AGE")) {

					partitionCriteria.setType(PartitionType.RAW);
					partitionCriteria.setSizeFilter(SizeFilter.HOLES);
					partitionCriteria.setValueFilter(ValueFilter.NULL);

					partition = PartitionMaker.create(label, segmentation.getCurrentIndividuals(), values, partitionCriteria);

					if (partition.maxValue() != null) {
						ReportChart survivalChart = StatisticsReporter.createSurvivalChart(partition, splitCriteria);
						charts.add(survivalChart);
					} else {
						System.err.println(label + " no max value");
					}
				}

				NumberedValues[] genderedValues = PuckUtils.getGenderedNumberedValues(values, segmentation.getCurrentIndividuals());

				overallReport.outputs().append(label + "\t");
				for (int gender = 0; gender < 3; gender++) {
					String sum = "";
					if (label.startsWith("NR")) {
						sum = new Double(genderedValues[gender].sum()).intValue() + "";
					}
					overallReport.outputs().append(
							MathUtils.round(genderedValues[gender].average(), 2) + "\t" + MathUtils.round(genderedValues[gender].averagePositives(), 2) + "\t"
									+ values.median() + "\t" + genderedValues[gender].max() + "\t" + sum + "\t");
				}
				overallReport.outputs().appendln();

			}

			if (chart != null) {
				charts.add(chart);
				if (label.equals("SIMILARITY")) {
					tables.add(ReportTable.transpose(chart.createReportTable()));
				} else {
					ReportTable table = ReportTable.transpose(chart.createReportTableWithSum());
					tables.add(table);
					if (!label.contains("EVENTS_") && !label.contains("RELATIONS")) {
						tables.add(ReportTable.normalize(table));
					}
				}

			}

		}
		overallReport.outputs().appendln();
		
		// Add parcours network statistics
		
		Map<String,Map<String,Map<String,Value>>> parcoursNetworkStatistics = sequenceStatistics.getParcoursNetworkStatistics();
		List<String> clusterValues = new ArrayList<String>(parcoursNetworkStatistics.keySet());
		
		if (parcoursNetworkStatistics!=null){
			overallReport.outputs().appendln("Parcours Network Statistics");

			String headLine = null;
			boolean first = true;
			
			for (String nodeLabel : parcoursNetworkStatistics.get("Total").keySet()){
				String line = nodeLabel;
				headLine = "Node";
				
				for (String genderLabel : clusterValues){
					Map<String,Value> statistics = parcoursNetworkStatistics.get(genderLabel).get(nodeLabel);
					for (String statisticsLabel : spaceTimeCriteria.getNodeStatisticsLabels()){
						if (first){
							headLine+="\t"+statisticsLabel+" ("+genderLabel+")";
						}
						if (statistics!=null){
							line+="\t"+statistics.get(statisticsLabel);
						} else {
							line+="\t";
						}
					}
				}
				if (first){
					overallReport.outputs().appendln(headLine);
					first = false;
				}
				overallReport.outputs().appendln(line);
			}
		}

		// Create detailed report
		detailReport.outputs().append("Nr\tEgo\tGender");
		
		for (String partitionLabel : spaceTimeCriteria.getCensusOperationLabels()) {
			if (partitionLabel.contains("SIMILARITY")) {
				RelationClassificationType relationClassificationType = RelationClassificationType.valueOf(partitionLabel.substring(partitionLabel
						.lastIndexOf("_") + 1));
				detailReport.outputs().append(
						"\tSIMILARITY_PARENT_" + relationClassificationType + "\tSIMILARITY_CHILD_" + relationClassificationType + "\tSIMILARITY_SIBLING_"
								+ relationClassificationType + "\tSIMILARITY_SPOUSE_" + relationClassificationType);
			} else {
				detailReport.outputs().append("\t" + partitionLabel);
			}
		}

		Map<String,Map<Value, Double[]>> componentChartMaps = new TreeMap<String, Map<Value, Double[]>>();
		for (String networkTitle : spaceTimeCriteria.getNetworkTitles()){
			componentChartMaps.put(networkTitle,new TreeMap<Value, Double[]>());
		}

		detailReport.outputs().appendln();
		for (Individual ego : segmentation.getCurrentIndividuals().toSortedList()) {

			if ((sequenceStatistics.getValues("NREVENTS").get(ego.getId()) != null)){
				detailReport.outputs().append(ego.getId() + "\t" + ego + "\t" + ego.getGender());
				for (String label : spaceTimeCriteria.getCensusOperationLabels()) {
					if (label.contains("SIMILARITY")) {
						Value value = sequenceStatistics.getValues(label).get(ego.getId());
						Map<Value, Double[]> indiSimilaritiesMap = value.mapValue();
						String[] keys = new String[] { "PARENT", "CHILD", "SIBLING", "SPOUSE" };
						for (String key : keys) {
							Double[] sim = indiSimilaritiesMap.get(new Value(key));
							if (sim != null) {
								detailReport.outputs().append("\t" + MathUtils.round(sim[4], 2));
							}
						}
					} else {
						detailReport.outputs().append("\t" + sequenceStatistics.getValues(label).get(ego.getId()));
					}
				}
				detailReport.outputs().appendln();
			}

			if (componentReport!=null){

				for (String networkTitle : spaceTimeCriteria.getNetworkTitles()) {
					Map<Integer, Partition<Node<Individual>>> componentsMap = sequenceStatistics.getComponents(networkTitle);
					Map<Value,Double[]> componentChartMap = componentChartMaps.get(networkTitle);
					if (componentsMap != null) {
						Partition<Node<Individual>> components = componentsMap.get(ego.getId());

						componentReport.outputs().appendln("Components " + networkTitle);
						componentReport.outputs().appendln(ego + "\t" + components.size());
						int i = 1;
						for (Cluster<Node<Individual>> cluster : components.getClusters().toListSortedByValue()) {
							componentReport.outputs().appendln(
									"\t" + i + "\t" + cluster.getValue() + "\t(" + cluster.size() + ")\t" + cluster.getItemsAsString());
							i++;
						}
						componentReport.outputs().appendln();

						for (Value value : components.getValues()) {
							Double[] map = componentChartMap.get(value);
							if (map == null) {
								map = new Double[]{0.,0.,0.};
								componentChartMap.put(value, map);
							}
							map[ego.getGender().toInt()]+=+ 1.;
							map[2]+=+ 1.;
						}
					}
				}
			}
		}

		for (String networkTitle : spaceTimeCriteria.getNetworkTitles()) {
			ReportChart componentChart = StatisticsReporter.createMapChart(componentChartMaps.get(networkTitle),"COMPONENTS", new String[]{"MALE","FEMALE"}, GraphType.STACKED_BARS);
			charts.add(componentChart);
			tables.add(ReportTable.transpose(componentChart.createReportTableWithSum()));

			if (sequenceStatistics.getRelationConnectionMatrix() != null) {
				for (ReportChart chart : sequenceStatistics.getRelationConnectionMatrix().getCharts()) {
					charts.add(chart);
				}
				tables.add(sequenceStatistics.getRelationConnectionMatrix().getTable("Component Connections"));
			}
		}
		

		// Manage the number of chart by line.
		for (int chartIndex = 0; chartIndex < charts.size(); chartIndex++) {
			diagramReport.outputs().append(charts.get(chartIndex));
			if (chartIndex % 4 == 3) {
				diagramReport.outputs().appendln();
			}
		}

		// Add chart tables.
		for (ReportTable table : tables) {
			diagramReport.outputs().appendln(table.getTitle());
			diagramReport.outputs().appendln(table);
		}

		// Finalize reports
		result.outputs().append(overallReport);
		result.outputs().append(diagramReport);
		result.outputs().append(detailReport);

			result.outputs().append(componentReport);

		// addPajekData
		
		Map<String, StringList> pajekBuffers = sequenceStatistics.getPajekBuffers();

		for (String title : pajekBuffers.keySet()) {

			StringList pajekBuffer = pajekBuffers.get(title);
			if (pajekBuffer.length() != 0) {
				File targetFile = ToolBox.setExtension(ToolBox.addToName(new File(segmentation.getLabel()), "-" + title), ".paj");
				ReportRawData rawData = new ReportRawData("Export " + title + "s to Pajek", "Pajek", "paj", targetFile);
				rawData.setData(PAJFile.convertToMicrosoftEndOfLine(pajekBuffer.toString()));

				result.outputs().appendln();
				result.outputs().append(rawData);
			}
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;

	}*/
	
	private static <S extends Sequenceable<E>,E extends Numberable> void getRData (Report result, SequenceStatistics<S,E> sequenceStatistics, Segmentation segmentation, SequenceCriteria sequenceCriteria) throws PuckException{
		
		Map<String, StringList> rStateBuffers = new TreeMap<String,StringList>();
		Map<String, StringList> rEventBuffers = new TreeMap<String,StringList>();
		Map<String, Integer> rSizes = new TreeMap<String,Integer>();
		
//		NumberedValues dateProfiles = sequenceStatistics.getValues("PROFILE#"+spaceTimeCriteria.getDateLabel());
//		NumberedValues ageProfiles = sequenceStatistics.getValues("PROFILE#AGE");
		
		for (String partitionLabel : sequenceCriteria.getSequenceValueLabels()) {
				
			if (partitionLabel.contains("PROFILE#")){
				
				String label = partitionLabel.substring(8);
				rStateBuffers.put(label,new StringList());
				rSizes.put(label,0);
				StringList rEventBuffer = new StringList();
				rEventBuffer.appendln("Id\tStep\tDate\tAge\t"+label);
				rEventBuffers.put(label,rEventBuffer);
			}
		}

		for (S sequence : sequenceStatistics.sequences.toSortedList()){

			if (sequenceStatistics.getValues("NREVENTS").get(sequence.getId()) != null) {
				
				ValueSequence ageSequence = sequenceStatistics.getValueSequence("AGE", sequence);
				ValueSequence dateSequence = sequenceStatistics.getValueSequence(sequenceCriteria.getDateLabel(), sequence);
				
//				List<String> dates = (List<String>)dateProfiles.get(sequence.getId()).listValue();
//				List<String> ages = (List<String>)ageProfiles.get(sequence.getId()).listValue();
				
				for (String label : rStateBuffers.keySet()){
				
//				for (String label : spaceTimeCriteria.getCensusOperationLabels()) {
//					Value value = sequenceStatistics.getValues(label).get(sequence.getId());
						
//					if (label.contains("PROFILE")){
					StringList rStateBuffer = rStateBuffers.get(label);
					StringList rEventBuffer = rEventBuffers.get(label);
					
					String genderString = "";
					if (sequence instanceof Individualizable){
						genderString = ((Individualizable)sequence).getEgo().getGender().toString();
					}
														
					String rStateLine = sequence.getId() + "\t" + sequence + "\t" + genderString;
//					List<String> rItems = (List<String>)value.listValue();
					
					ValueSequence valueSequence = sequenceStatistics.getValueSequence(label, sequence);

					if (valueSequence.getNrStations()>rSizes.get(label)){
						rSizes.put(label, valueSequence.getNrStations());
					}
					
					int step = 0;
					for (Ordinal time : sequence.getTimes()){
						Value rItem = valueSequence.getStation(time);
//					}
//						for (String rItem: rItems){
						rStateLine += "\t"+rItem;
						if (rItem!=null){
							rEventBuffer.appendln(sequence.getId()+"\t"+step+"\t"+dateSequence.getStation(time)+"\t"+ageSequence.getStation(time)+"\t"+rItem);
						}
						step++;
					}
					rStateBuffer.append(rStateLine);
				}
//				}
			}
		}
		
		// Add data
		
		for (String title : rStateBuffers.keySet()) {
			
			StringList rStateBuffer = rStateBuffers.get(title);
			StringList rStateBufferCompleted = new StringList();
			
			int rSize = rSizes.get(title);
			String rStateHeadLine = "Id\tName\tGender";
			for (int i=0;i<rSize;i++){
				rStateHeadLine+="\t"+title+i;
			}
			rStateBufferCompleted.appendln(rStateHeadLine);
			for (String rStateLine : rStateBuffer){
				String rStateLineCompleted = "";
				String[] rStateItems = rStateLine.split("\t");
				for (int i=0;i<3+rSize;i++){
					if (i<rStateItems.length){
						rStateLineCompleted+=rStateItems[i]+"\t";
					} else {
						rStateLineCompleted+="NA\t";
					}
				}
				rStateBufferCompleted.appendln(rStateLineCompleted);
			}
			
			if (rStateBuffer.length() != 0) {
				
				File targetFile = ToolBox.setExtension(ToolBox.addToName(new File(segmentation.getLabel()), "-States-" + title), ".txt");
				ReportRawData rawData = new ReportRawData("Export " + title + " State Sequences to RData", "Text", "txt", targetFile);
				rawData.setData(PAJFile.convertToMicrosoftEndOfLine(rStateBufferCompleted.toString()));

				result.outputs().appendln();
				result.outputs().append(rawData);
			}
			
			StringList rEventBuffer = rEventBuffers.get(title);
			if (rEventBuffer.length() != 0) {
				
				File targetFile = ToolBox.setExtension(ToolBox.addToName(new File(segmentation.getLabel()), "-Events-" + title), ".txt");
				ReportRawData rawData = new ReportRawData("Export " + title + " Event Sequences to RData", "Text", "txt", targetFile);
				rawData.setData(PAJFile.convertToMicrosoftEndOfLine(rEventBuffer.toString()));

				result.outputs().appendln();
				result.outputs().append(rawData);
			}
		}
	}

	/**
	 * 
	 * @param segmentation
	 * @param censusType
	 * @return
	 * @throws PuckException
	 */
/*	public static Report reportTrajectoriesCensus(final String reportTitle, final Segmentation segmentation, final EgoSequences sequences, final SequenceCriteria spaceTimeCriteria) throws PuckException {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		result = new Report(reportTitle);
		result.setOrigin("Sequence reporter");

		List<String> reportTitles = Arrays.asList(new String[]{"SURVEY","DIAGRAMS","DETAILS"});
		if (spaceTimeCriteria.getTrajectoriesOperations().contains(TrajectoriesOperation.LIST_TREES)){
			reportTitles.add("TREES");
			
		}
		


		// Create Partition charts and tables
		List<ReportChart> charts = new ArrayList<ReportChart>();
		List<ReportTable> tables = new ArrayList<ReportTable>();

		// Get (coherent) itineraries
		EgoSequences sequences = new EgoSequences();
		for (Individual ego : segmentation.getCurrentIndividuals().toSortedList()) {
			sequences.addRenumbered(SequenceWorker.getCoherentItinerarySegment(ego, censusCriteria));
		}
		

		// Proceed census
		EgoSequenceStatistics sequenceStatistics = new EgoSequenceStatistics(segmentation, sequences, spaceTimeCriteria);
		
		Map<String,Report> reports = new HashMap<String,Report>();
		Map<String, StringList> pajekBuffers = sequenceStatistics.getPajekBuffers();


		System.out.println("Census established");

		
		// Create Reports
		Report overallReport = reportSurvey("Survey",sequenceStatistics,spaceTimeCriteria);
		Report diagramReport = reportDiagramsSequenceValues("Diagrams",sequenceStatistics,spaceTimeCriteria,segmentation,3);
		Report detailReport = reportDetailsSequenceValues("Details", sequenceStatistics, spaceTimeCriteria,spaceTimeCriteria.getSequenceValueCriteriaList().getLabels());
		Report treeReport = null;
				
		if (spaceTimeCriteria.getTrajectoriesOperations().contains(TrajectoriesOperation.LIST_TREES)){
			treeReport = new Report("Trees");
			
		}
	
		createReports(result, reportTitles,reports,segmentation,sequenceStatistics,spaceTimeCriteria,pajekBuffers, 3);

		System.out.println("Reports established");
		
//		getRData(result,sequenceStatistics,segmentation,spaceTimeCriteria);

		
		// Make overall report and diagrams
		overallReport
				.outputs()
				.appendln(
						"Measure\tAverage (Male)\tAverage Pos. (Male)\tMedian (Male)\tMaximum (Male)\tSum (Male)\tAverage (Female)\tAverage Pos. (Female)\tMedian (Female)\tMaximum (Female)\tSum (Female)\tAverage (All)\tAverage Pos. (All)\tMedian (All)\tMaximum (All)\tSum (All)");

		// Set partition criteria
		for (String label : censusCriteria.getCensusOperationLabels()) {

			PartitionCriteria partitionCriteria = new PartitionCriteria(label);
			// partitionCriteria.setValueFilter(ValueFilter.NULL);

			/*			if (label.contains("PROFILE")){
							partitionCriteria.setType(PartitionType.PARTIALIZATION);
						} */

/*			if (label.equals("NREVENTS") || label.contains("NRSTATIONS")) {
				partitionCriteria.setType(PartitionType.RAW);
//				partitionCriteria.setType(PartitionType.FREE_GROUPING);
//				partitionCriteria.setIntervals(PartitionMaker.getIntervals("1 5 10 15 20 25"));
				// partitionCriteria.setCumulationType(CumulationType.DESCENDANT);
			} else if (label.contains("AGEFIRST")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(0.);
				partitionCriteria.setSize(5.);
			} else if (label.equals("ECCENTRICITY")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(-100.);
				partitionCriteria.setSize(20.);
			} else if (label.contains("COVERAGE") || label.contains("SAME") || label.contains("NORM") || label.contains("DENSITY")
					|| label.contains("BETWEENNESS") || label.contains("EFFICIENCY") || label.contains("CONCENTRATION")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(0.);
				partitionCriteria.setSize(20.);
			} else if (label.contains("MEAN") || label.contains("COVERAGE") || label.contains("PEREVENT") || label.contains("BETWEENNESS")
					|| label.contains("BROKERAGE") || label.contains("EFFICIENT_SIZE")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(0.);
				partitionCriteria.setSize(1.);
			} else {
				partitionCriteria.setType(PartitionType.RAW);
			}

			ReportChart chart = null;

			if (!label.contains("ALTERS") && !label.contains("PROFILE")) {
				NumberedValues values = census.getValues(label);

				Partition<Individual> partition = PartitionMaker.create(label, segmentation.getCurrentIndividuals(), values, partitionCriteria);

				PartitionCriteria splitCriteria = new PartitionCriteria(censusCriteria.getPartitionLabel());
				chart = StatisticsReporter.createPartitionChart(partition, partitionCriteria, splitCriteria);

				if (label.substring(0, 3).equals("AGE")) {

					partitionCriteria.setType(PartitionType.RAW);
					partitionCriteria.setSizeFilter(SizeFilter.HOLES);
					partitionCriteria.setValueFilter(ValueFilter.NULL);

					partition = PartitionMaker.create(label, segmentation.getCurrentIndividuals(), values, partitionCriteria);

					if (partition.maxValue() != null) {
						ReportChart survivalChart = StatisticsReporter.createSurvivalChart(partition, splitCriteria);
						charts.add(survivalChart);
					} else {
						System.err.println(label + " no max value");
					}
				}

				NumberedValues[] genderedValues = PuckUtils.getGenderedNumberedValues(values, segmentation.getCurrentIndividuals());

				overallReport.outputs().append(label + "\t");
				for (int gender = 0; gender < 3; gender++) {
					String sum = "";
					if (label.startsWith("NR")) {
						sum = new Double(genderedValues[gender].sum()).intValue() + "";
					}
					overallReport.outputs().append(
							MathUtils.round(genderedValues[gender].average(), 2) + "\t" + MathUtils.round(genderedValues[gender].averagePositives(), 2) + "\t"
									+ values.median() + "\t" + genderedValues[gender].max() + "\t" + sum + "\t");
				}
				overallReport.outputs().appendln();

			}

			if (chart != null) {
				charts.add(chart);
					ReportTable table = ReportTable.transpose(chart.createReportTableWithSum());
					tables.add(table);
					if (!label.contains("EVENTS_") && !label.contains("RELATIONS")) {
						tables.add(ReportTable.normalize(table));
					}

			}

		}
		overallReport.outputs().appendln();*/

		// Create detailed report
		
/*		Map<String, StringList> rStateBuffers = new TreeMap<String,StringList>();
		Map<String, StringList> rEventBuffers = new TreeMap<String,StringList>();
		Map<String, Integer> rSizes = new TreeMap<String,Integer>();
		
		NumberedValues dateProfiles = sequenceStatistics.getValues("PROFILE#"+spaceTimeCriteria.getDateLabel());
		NumberedValues ageProfiles = sequenceStatistics.getValues("PROFILE#AGE");
		
//		detailReport.outputs().append("Nr\tEgo\tGender");
		for (String partitionLabel : spaceTimeCriteria.getCensusOperationLabels()) {
//				detailReport.outputs().append("\t" + partitionLabel);
				
				if (partitionLabel.contains("PROFILE")){
					rStateBuffers.put(partitionLabel.substring(8),new StringList());
					rSizes.put(partitionLabel.substring(8),0);
					StringList rEventBuffer = new StringList();
					rEventBuffer.appendln("Id\tStep\tDate\tAge\t"+partitionLabel.substring(8));
					rEventBuffers.put(partitionLabel.substring(8),rEventBuffer);
				}
		}

//		detailReport.outputs().appendln();
		for (Individual ego : segmentation.getCurrentIndividuals().toSortedList()) {

			if (sequenceStatistics.getValues("NREVENTS").get(ego.getId()) != null) {
				
//				detailReport.outputs().append(ego.getId() + "\t" + ego + "\t" + ego.getGender());
				List<String> dates = (List<String>)dateProfiles.get(ego.getId()).listValue();
				List<String> ages = (List<String>)ageProfiles.get(ego.getId()).listValue();
				
				for (String label : spaceTimeCriteria.getCensusOperationLabels()) {
					Value value = sequenceStatistics.getValues(label).get(ego.getId());
//						detailReport.outputs().append("\t" + value);
						
						if (label.contains("PROFILE")){
							StringList rStateBuffer = rStateBuffers.get(label.substring(8));
							StringList rEventBuffer = rEventBuffers.get(label.substring(8));

							int rSize = rSizes.get(label.substring(8));
									
							String rStateLine = ego.getId() + "\t" + ego + "\t" + ego.getGender();
							List<String> rItems = (List<String>)value.listValue();
							
							if (rItems.size()>rSize){
								rSizes.put(label.substring(8), rItems.size());
							}
							int time = 0;
							for (String rItem: rItems){
								rStateLine += "\t"+rItem;
								rEventBuffer.appendln(ego.getId()+"\t"+time+"\t"+dates.get(time)+"\t"+ages.get(time)+"\t"+rItem);
								time++;
							}
							rStateBuffer.append(rStateLine);
						}
				}
//				detailReport.outputs().appendln();
			}*/
	

		// SequenceAnalysis

/*			for (RelationClassificationType relationClassificationType : spaceTimeCriteria.getMainRelationClassificationTypes()) {

				if (spaceTimeCriteria.getNetworkTitles().contains("Event Type Network")) {

					CorrelationMatrix eventSequenceMatrix = sequenceStatistics.getEventSequenceMatrix(relationClassificationType.toString());

					if (eventSequenceMatrix != null) {
						for (ReportChart chart : eventSequenceMatrix.getCharts()) {
							charts.add(chart);
						}
						tables.add(eventSequenceMatrix.getTable("Event Type Sequences"));
						
					}

					overallReport.outputs().appendln();
					overallReport.outputs().appendln("Sequence Network Statistics " + relationClassificationType);
					overallReport.outputs().appendln("\tDensity\tInertia\t(Divergence)\tConcentration\t(Divergence)\tSymmetry\t(Divergence)\tCentral nodes");

					for (Gender gender : Gender.values()) {
						GraphProfile<Cluster<String>> profile = eventSequenceMatrix.getProfile(gender);

						String centralReferents = "";
						for (Cluster<String> centralReferent : profile.getCentralReferents()) {
							centralReferents += centralReferent.getValue() + " ";
						}
						double maxBetweenness = profile.getMaxBetweenness();
						double density = profile.density();
						double endo = MathUtils.round(profile.getStatistics(Indicator.LOOPS, Mode.NORMALIZED), 2);
						double endoExp = MathUtils.round(profile.getStatistics(Indicator.LOOPS, Mode.DIVERGENCE_NORMALIZED), 2);
						double conc = MathUtils.round(profile.getStatistics(Indicator.CONCENTRATION, Mode.SIMPLE), 2);
						double concExp = MathUtils.round(profile.getStatistics(Indicator.CONCENTRATION, Mode.DIVERGENCE), 2);
						double sym = MathUtils.round(profile.getStatistics(Indicator.SYMMETRY, Mode.SIMPLE), 2);
						double symExp = MathUtils.round(profile.getStatistics(Indicator.SYMMETRY, Mode.DIVERGENCE), 2);

						overallReport.outputs().appendln(
								gender + "\t" + density + "\t" + endo + "\t" + endoExp + "\t" + conc + "\t" + concExp + "\t" + sym + "\t" + symExp + "\t"
										+ centralReferents + "(" + maxBetweenness + ") betweenness centrality");
					}
					overallReport.outputs().appendln();

				}

				if (spaceTimeCriteria.getNetworkTitles().contains("Sequence Type Network")) {

					CorrelationMatrix subSequenceMatrix = sequenceStatistics.getSubSequenceMatrix(relationClassificationType.toString());

					if (subSequenceMatrix != null) {
						charts.add(subSequenceMatrix.getRamificationChart());
					}
				}

				// reportSequencePartition(overallReport,"",census.getEventPartition(eventTypeName),census.getEventPairPartition(eventTypeName),census.nrSequences(),census.nrEvents());
				// reportSequenceTree(treeReport,"",census.getSequenceTypeNetwork(eventTypeName));

								int maxPositions = census.getNrValues(eventType,eventType.toString());
										
								ReportChart diversityChart = StatisticsReporter.createDiversityChart(census.getDepthPartition(eventType.toString()), maxPositions);
								charts.add(diversityChart);
			}
		}
		

		// Manage the number of chart by line.
		for (int chartIndex = 0; chartIndex < charts.size(); chartIndex++) {
			diagramReport.outputs().append(charts.get(chartIndex));
			if (chartIndex % 4 == 3) {
				diagramReport.outputs().appendln();
			}
		}

		// Add chart tables.
		for (ReportTable table : tables) {
			diagramReport.outputs().appendln(table.getTitle());
			diagramReport.outputs().appendln(table);
		}

		// Finalize reports
		result.outputs().append(overallReport);
		result.outputs().append(diagramReport);
		result.outputs().append(detailReport);
		if (treeReport!=null){
			result.outputs().append(treeReport);
		}

		// addPajekData

//		if (spaceTimeCriteria.getTrajectoriesOperations().contains(TrajectoriesOperation.EXPORT_AGGREGATE_SEQUENCE_NETWORK) || spaceTimeCriteria.getTrajectoriesOperations().contains(TrajectoriesOperation.EXPORT_SEQUENCE_TYPE_NETWORK)){
			

			for (String title : pajekBuffers.keySet()) {
				
				StringList pajekBuffer = pajekBuffers.get(title);
				if (pajekBuffer.length() != 0) {
										
					File targetFile = ToolBox.setExtension(ToolBox.addToName(new File(segmentation.getLabel()), "-" + title), ".paj");
					ReportRawData rawData = new ReportRawData("Export " + title + "s to Pajek", "Pajek", "paj", targetFile);
					rawData.setData(PAJFile.convertToMicrosoftEndOfLine(pajekBuffer.toString()));

					result.outputs().appendln();
					result.outputs().append(rawData);
				}
			}
//		}
		
		// addRData

		if (spaceTimeCriteria.getTrajectoriesOperations().contains(TrajectoriesOperation.GENERAL)){
			
			for (String title : rStateBuffers.keySet()) {
				
				StringList rStateBuffer = rStateBuffers.get(title);
				StringList rStateBufferCompleted = new StringList();
				
				int rSize = rSizes.get(title);
				String rStateHeadLine = "Id\tName\tGender";
				for (int i=0;i<rSize;i++){
					rStateHeadLine+="\t"+title+i;
				}
				rStateBufferCompleted.appendln(rStateHeadLine);
				for (String rStateLine : rStateBuffer){
					String rStateLineCompleted = "";
					String[] rStateItems = rStateLine.split("\t");
					for (int i=0;i<3+rSize;i++){
						if (i<rStateItems.length){
							rStateLineCompleted+=rStateItems[i]+"\t";
						} else {
							rStateLineCompleted+="NA\t";
						}
					}
					rStateBufferCompleted.appendln(rStateLineCompleted);
				}
				
				if (rStateBuffer.length() != 0) {
					
					File targetFile = ToolBox.setExtension(ToolBox.addToName(new File(segmentation.getLabel()), "-States-" + title), ".txt");
					ReportRawData rawData = new ReportRawData("Export " + title + "s to RData (States)", "Text", "txt", targetFile);
					rawData.setData(PAJFile.convertToMicrosoftEndOfLine(rStateBufferCompleted.toString()));

					result.outputs().appendln();
					result.outputs().append(rawData);
				}
				
				StringList rEventBuffer = rEventBuffers.get(title);
				if (rEventBuffer.length() != 0) {
					
					File targetFile = ToolBox.setExtension(ToolBox.addToName(new File(segmentation.getLabel()), "-Events-" + title), ".txt");
					ReportRawData rawData = new ReportRawData("Export " + title + "s to RData (Events)", "Text", "txt", targetFile);
					rawData.setData(PAJFile.convertToMicrosoftEndOfLine(rEventBuffer.toString()));

					result.outputs().appendln();
					result.outputs().append(rawData);
				}
			}
		}
		

		// draw graph
		
		
		
		
		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;

	}
	
	public static Graph<Place> getTrajectoryGraphOld (final Segmentation segmentation, final SequenceCriteria censusCriteria) throws PuckException {
		Graph<Place> result;
		
		result = null;

		// Create Partition charts and tables
		List<ReportChart> charts = new ArrayList<ReportChart>();
		List<ReportTable> tables = new ArrayList<ReportTable>();

		// Get (coherent) itineraries
		EgoSequences sequences = new EgoSequences();
		for (Individual ego : segmentation.getCurrentIndividuals().toSortedList()) {
			sequences.addRenumbered(SequenceWorker.getCoherentItinerarySegment(ego, censusCriteria));
		}

		// Proceed census
		SequenceStatistics census = new SequenceStatistics(segmentation, sequences, censusCriteria);


		if (censusCriteria.getTrajectoriesOperations().contains(TrajectoriesOperation.DRAW)){
			
			SequenceNetworkStatistics eventSequenceMatrix = census.getEventSequenceMatrix("PLACE");
			Graph<Cluster<String>> drawGraph = eventSequenceMatrix.getSequenceNetwork("PLACE", Gender.UNKNOWN);
			
			result = GeoNetworkUtils.createGeoNetwork(drawGraph, censusCriteria.getLevel());

		}
		//
		return result;


	}

    public static Graph<Place> getTrajectoryGraph2 (final Segmentation segmentation, final SequenceCriteria criteria) throws PuckException {
        Graph<Place> result;
        
        result = null;

        // Create Partition charts and tables
        List<ReportChart> charts = new ArrayList<ReportChart>();
        List<ReportTable> tables = new ArrayList<ReportTable>();

        // Get (coherent) itineraries
        EgoSequences sequences = new EgoSequences();
        for (Individual ego : segmentation.getCurrentIndividuals().toSortedList()) {
            sequences.addRenumbered(SequenceWorker.getCoherentItinerarySegment(ego, criteria));
        }

        // Proceed census
        SequenceStatistics census = new SequenceStatistics(segmentation, sequences, criteria);


        if (criteria.getTrajectoriesOperations().contains(TrajectoriesOperation.DRAW)){
            
            SequenceNetworkStatistics eventSequenceMatrix = census.getEventSequenceMatrix("PLACE");
            Graph<Cluster<String>> drawGraph = eventSequenceMatrix.getSequenceNetwork("PLACE", Gender.UNKNOWN);
            
            Graph<String> placeNameGraph = GeoNetworkUtils.createGeoNetwork2(drawGraph, criteria.getLevel());
            
            result = GeocodingWorker.geocodeGraph(segmentation.getGeography(), placeNameGraph);

        }
        //
        return result;


    }



	 
	public static Report reportSequenceCensus(final Segmentation segmentation, final CensusType censusType) throws PuckException {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		result = new Report(censusType.toString());
		result.setOrigin("Sequence reporter");

		// Create Reports
		Report overallReport = new Report("Survey");
		Report diagramReport = new Report("Diagrams");
		Report detailReport = new Report("Details");
		Report componentReport = new Report("Components");
		Report treeReport = new Report("Trees");

		// Create Partition charts and tables
		List<ReportChart> charts = new ArrayList<ReportChart>();
		List<ReportTable> tables = new ArrayList<ReportTable>();

		// Set census criteria
		SequenceCriteria censusCriteria = new SequenceCriteria(censusType);

		// Get (coherent) itineraries
		EgoSequences sequences = new EgoSequences();
		for (Individual ego : segmentation.getCurrentIndividuals().toSortedList()) {
			sequences.addRenumbered(SequenceWorker.getCoherentItinerarySegment(ego, censusCriteria));
		}

		// Proceed census
		EgoSequenceStatistics census = new EgoSequenceStatistics(segmentation, sequences, censusCriteria);

		// Make overall report and diagrams
		// Generalize for all types of partitions (not only male and female)
		overallReport
				.outputs()
				.appendln(
						"Measure\tAverage (Male)\tAverage Pos. (Male)\tMedian (Male)\tMaximum (Male)\tSum (Male)\tAverage (Female)\tAverage Pos. (Female)\tMedian (Female)\tMaximum (Female)\tSum (Female)\tAverage (All)\tAverage Pos. (All)\tMedian (All)\tMaximum (All)\tSum (All)");

		// Set partition criteria
		for (String label : censusCriteria.getCensusOperationLabels()) {

			PartitionCriteria partitionCriteria = new PartitionCriteria(label);
			// partitionCriteria.setValueFilter(ValueFilter.NULL);

			/*			if (label.contains("PROFILE")){
							partitionCriteria.setType(PartitionType.PARTIALIZATION);
						} */

/*			if (label.equals("NREVENTS")) {
				partitionCriteria.setType(PartitionType.RAW);
				// partitionCriteria.setCumulationType(CumulationType.DESCENDANT);
			} else if (label.contains("AGEFIRST")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(0.);
				partitionCriteria.setSize(5.);
			} else if (label.equals("ECCENTRICITY")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(-100.);
				partitionCriteria.setSize(20.);
			} else if (label.contains("COVERAGE") || label.contains("SAME") || label.contains("NORM") || label.contains("DENSITY")
					|| label.contains("BETWEENNESS") || label.contains("EFFICIENCY") || label.contains("CONCENTRATION")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(0.);
				partitionCriteria.setSize(20.);
			} else if (label.contains("MEAN") || label.contains("COVERAGE") || label.contains("PEREVENT") || label.contains("BETWEENNESS")
					|| label.contains("BROKERAGE") || label.contains("EFFICIENT_SIZE")) {
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(0.);
				partitionCriteria.setSize(1.);
			} else {
				partitionCriteria.setType(PartitionType.RAW);
			}

			ReportChart chart = null;

			if (label.contains("SIMILARITY")) {
				RelationClassificationType relationClassificationType = RelationClassificationType.valueOf(label.substring(label.lastIndexOf("_") + 1));
				Map<Value, Double[]> similaritiesMap = census.getSimilaritiesMap(relationClassificationType);
				chart = StatisticsReporter.createMapChart(similaritiesMap, label, new String[] { "HH", "FH", "HF", "FF", "All" }, GraphType.LINES);

				for (Value key : similaritiesMap.keySet()) {
					overallReport.outputs().appendln(label + "_" + key + "\t" + MathUtils.percent(similaritiesMap.get(key)[4], 100));
				}

			} else if (!label.contains("ALTERS") && !label.contains("PROFILE")) {
				NumberedValues values = census.getValues(label);

				Partition<Individual> partition = PartitionMaker.create(label, segmentation.getCurrentIndividuals(), values, partitionCriteria);

				PartitionCriteria splitCriteria = new PartitionCriteria(censusCriteria.getPartitionLabel());
				chart = StatisticsReporter.createPartitionChart(partition, partitionCriteria, splitCriteria);

				if (label.substring(0, 3).equals("AGE")) {

					partitionCriteria.setType(PartitionType.RAW);
					partitionCriteria.setSizeFilter(SizeFilter.HOLES);
					partitionCriteria.setValueFilter(ValueFilter.NULL);

					partition = PartitionMaker.create(label, segmentation.getCurrentIndividuals(), values, partitionCriteria);

					if (partition.maxValue() != null) {
						ReportChart survivalChart = StatisticsReporter.createSurvivalChart(partition, splitCriteria);
						charts.add(survivalChart);
					} else {
						System.err.println(label + " no max value");
					}
				}

				NumberedValues[] genderedValues = PuckUtils.getGenderedNumberedValues(values, segmentation.getCurrentIndividuals());

				overallReport.outputs().append(label + "\t");
				for (int gender = 0; gender < 3; gender++) {
					String sum = "";
					if (label.startsWith("NR")) {
						sum = new Double(genderedValues[gender].sum()).intValue() + "";
					}
					overallReport.outputs().append(
							MathUtils.round(genderedValues[gender].average(), 2) + "\t" + MathUtils.round(genderedValues[gender].averagePositives(), 2) + "\t"
									+ values.median() + "\t" + genderedValues[gender].max() + "\t" + sum + "\t");
				}
				overallReport.outputs().appendln();

			}

			if (chart != null) {
				charts.add(chart);
				if (label.equals("SIMILARITY")) {
					tables.add(ReportTable.transpose(chart.createReportTable()));
				} else {
					ReportTable table = ReportTable.transpose(chart.createReportTableWithSum());
					tables.add(table);
					if (!label.contains("EVENTS_") && !label.contains("RELATIONS")) {
						tables.add(ReportTable.normalize(table));
					}
				}

			}

		}
		overallReport.outputs().appendln();
		
		// Add parcours network statistics
		
		Map<String,Map<String,Map<String,Value>>> parcoursNetworkStatistics = census.getParcoursNetworkStatistics();
		
		if (parcoursNetworkStatistics!=null){
			overallReport.outputs().appendln("Parcours Network Statistics");
			
			for (String nodeLabel : parcoursNetworkStatistics.get("Total").keySet()){
				String line = nodeLabel;
				for (String statisticsLabel : parcoursNetworkStatistics.get("Total").get(nodeLabel).keySet()){
					for (String genderLabel : parcoursNetworkStatistics.keySet()){
						line+="\t"+parcoursNetworkStatistics.get(genderLabel).get(nodeLabel).get(statisticsLabel);
					}
				}
				overallReport.outputs().appendln(line);
			}
		}
		
		// Create detailed report
		detailReport.outputs().append("Nr\tEgo\tGender");
		for (String partitionLabel : censusCriteria.getCensusOperationLabels()) {
			if (partitionLabel.contains("SIMILARITY")) {
				RelationClassificationType relationClassificationType = RelationClassificationType.valueOf(partitionLabel.substring(partitionLabel
						.lastIndexOf("_") + 1));
				detailReport.outputs().append(
						"\tSIMILARITY_PARENT_" + relationClassificationType + "\tSIMILARITY_CHILD_" + relationClassificationType + "\tSIMILARITY_SIBLING_"
								+ relationClassificationType + "\tSIMILARITY_SPOUSE_" + relationClassificationType);
			} else {
				detailReport.outputs().append("\t" + partitionLabel);
			}
		}

		Map<Value, Double[]> componentChartMap = new TreeMap<Value, Double[]>();

		detailReport.outputs().appendln();
		for (Individual ego : segmentation.getCurrentIndividuals().toSortedList()) {

			if ((((censusType == CensusType.GENERAL) || (censusType == CensusType.PARCOURS) || (censusType == CensusType.SEQUENCENETWORKS)) && (census
					.getValues("NREVENTS").get(ego.getId()) != null))
					|| ((censusType == CensusType.EGONETWORKS) && (census.getValues("SIZE").get(ego.getId()) != null))) {
				detailReport.outputs().append(ego.getId() + "\t" + ego + "\t" + ego.getGender());
				for (String label : censusCriteria.getCensusOperationLabels()) {
					if (label.contains("SIMILARITY")) {
						Value value = census.getValues(label).get(ego.getId());
						Map<Value, Double[]> indiSimilaritiesMap = value.mapValue();
						String[] keys = new String[] { "PARENT", "CHILD", "SIBLING", "SPOUSE" };
						for (String key : keys) {
							Double[] sim = indiSimilaritiesMap.get(new Value(key));
							if (sim != null) {
								detailReport.outputs().append("\t" + MathUtils.round(sim[4], 2));
							}
						}
					} else {
						detailReport.outputs().append("\t" + census.getValues(label).get(ego.getId()));
					}
				}
				detailReport.outputs().appendln();
			}

			if ((censusType == CensusType.EGONETWORKS || censusType == CensusType.SEQUENCENETWORKS)) {

				for (String networkTitle : censusCriteria.getNetworkTitles()) {
					Map<Integer, Partition<Node<Individual>>> componentsMap = census.getComponents(networkTitle);
					if (componentsMap != null) {
						Partition<Node<Individual>> components = componentsMap.get(ego.getId());

						componentReport.outputs().appendln("Components " + networkTitle);
						componentReport.outputs().appendln(ego + "\t" + components.size());
						int i = 1;
						for (Cluster<Node<Individual>> cluster : components.getClusters().toListSortedByValue()) {
							componentReport.outputs().appendln(
									"\t" + i + "\t" + cluster.getValue() + "\t(" + cluster.size() + ")\t" + cluster.getItemsAsString());
							i++;
						}
						componentReport.outputs().appendln();

						for (Value value : components.getValues()) {
							Double[] map = componentChartMap.get(value);
							if (map == null) {
								map = new Double[]{0.,0.,0.};
								componentChartMap.put(value, map);
							}
							map[ego.getGender().toInt()]+=+ 1.;
							map[2]+=+ 1.;
						}
					}
				}
			}
		}

		if ((censusType == CensusType.EGONETWORKS || censusType == CensusType.SEQUENCENETWORKS)) {
			ReportChart componentChart = StatisticsReporter.createMapChart(componentChartMap,"COMPONENTS",new String[]{"MALE","FEMALE"}, GraphType.STACKED_BARS);
			charts.add(componentChart);
			tables.add(ReportTable.transpose(componentChart.createReportTableWithSum()));

			if (census.getRelationConnectionMatrix() != null) {
				for (ReportChart chart : census.getRelationConnectionMatrix().getCharts()) {
					charts.add(chart);
				}
				tables.add(census.getRelationConnectionMatrix().getTable("Component Connections"));
			}

		}

		// SequenceAnalysis

		if (censusType == CensusType.PARCOURS) {

			for (RelationClassificationType relationClassificationType : censusCriteria.getMainRelationClassificationTypes()) {

				if (censusCriteria.getNetworkTitles().contains("Event Type Network")) {

					CorrelationMatrix<S,E> eventSequenceMatrix = census.getEventSequenceMatrix(relationClassificationType.toString());

					if (eventSequenceMatrix != null) {
						for (ReportChart chart : eventSequenceMatrix.getCharts()) {
							charts.add(chart);
						}
						tables.add(eventSequenceMatrix.getTable("Event Type Sequences"));
					}

					overallReport.outputs().appendln();
					overallReport.outputs().appendln("Sequence Network Statistics " + relationClassificationType);
					overallReport.outputs().appendln("\tDensity\tInertia\t(Divergence)\tConcentration\t(Divergence)\tSymmetry\t(Divergence)\tCentral nodes");

					for (Gender gender : Gender.values()) {
						GraphProfile<Cluster<String>> profile = eventSequenceMatrix.getProfile(gender);

						String centralReferents = "";
						for (Cluster<String> centralReferent : profile.getCentralReferents()) {
							centralReferents += centralReferent.getValue() + " ";
						}
						double maxBetweenness = profile.getMaxBetweenness();
						double density = profile.density();
						double endo = MathUtils.round(profile.getStatistics(Indicator.LOOPS, Mode.NORMALIZED), 2);
						double endoExp = MathUtils.round(profile.getStatistics(Indicator.LOOPS, Mode.DIVERGENCE_NORMALIZED), 2);
						double conc = MathUtils.round(profile.getStatistics(Indicator.CONCENTRATION, Mode.SIMPLE), 2);
						double concExp = MathUtils.round(profile.getStatistics(Indicator.CONCENTRATION, Mode.DIVERGENCE), 2);
						double sym = MathUtils.round(profile.getStatistics(Indicator.SYMMETRY, Mode.SIMPLE), 2);
						double symExp = MathUtils.round(profile.getStatistics(Indicator.SYMMETRY, Mode.DIVERGENCE), 2);

						overallReport.outputs().appendln(
								gender + "\t" + density + "\t" + endo + "\t" + endoExp + "\t" + conc + "\t" + concExp + "\t" + sym + "\t" + symExp + "\t"
										+ centralReferents + "(" + maxBetweenness + ") betweenness centrality");
					}
					overallReport.outputs().appendln();

				}

				if (censusCriteria.getNetworkTitles().contains("Sequence Type Network")) {

					CorrelationMatrix subSequenceMatrix = census.getSubSequenceMatrix(relationClassificationType.toString());

					if (subSequenceMatrix != null) {
						charts.add(subSequenceMatrix.getRamificationChart());
					}
				}

				// reportSequencePartition(overallReport,"",census.getEventPartition(eventTypeName),census.getEventPairPartition(eventTypeName),census.nrSequences(),census.nrEvents());
				// reportSequenceTree(treeReport,"",census.getSequenceTypeNetwork(eventTypeName));

				/*				int maxPositions = census.getNrValues(eventType,eventType.toString());
										
								ReportChart diversityChart = StatisticsReporter.createDiversityChart(census.getDepthPartition(eventType.toString()), maxPositions);
								charts.add(diversityChart);*/
/*			}
		}

		// Manage the number of chart by line.
		for (int chartIndex = 0; chartIndex < charts.size(); chartIndex++) {
			diagramReport.outputs().append(charts.get(chartIndex));
			if (chartIndex % 4 == 3) {
				diagramReport.outputs().appendln();
			}
		}

		// Add chart tables.
		for (ReportTable table : tables) {
			diagramReport.outputs().appendln(table.getTitle());
			diagramReport.outputs().appendln(table);
		}

		// Finalize reports
		result.outputs().append(overallReport);
		result.outputs().append(diagramReport);
		result.outputs().append(detailReport);

		if (censusType == CensusType.EGONETWORKS || censusType == CensusType.SEQUENCENETWORKS) {
			result.outputs().append(componentReport);
		}
		if (censusType == CensusType.PARCOURS) {
			result.outputs().append(treeReport);
		}

		// addPajekData

		Map<String, StringList> pajekBuffers = census.getPajekBuffers();

		for (String title : pajekBuffers.keySet()) {

			StringList pajekBuffer = pajekBuffers.get(title);
			if (pajekBuffer.length() != 0) {
				File targetFile = ToolBox.setExtension(ToolBox.addToName(new File(segmentation.getLabel()), "-" + title), ".paj");
				ReportRawData rawData = new ReportRawData("Export " + title + "s to Pajek", "Pajek", "paj", targetFile);
				rawData.setData(PAJFile.convertToMicrosoftEndOfLine(pajekBuffer.toString()));

				result.outputs().appendln();
				result.outputs().append(rawData);
			}
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;

	}*/


	/**
	 * 
	 * @param net
	 * @param segmentation
	 * @param criteria
	 * @return
	 * @throws PuckException
	 */
	public static Report reportSequences(final Net net, final Segmentation segmentation, final SequenceCriteria criteria) throws PuckException {
		Report result;

//		String spouseFilterLabel = "INTERV";

		//
		Chronometer chrono = new Chronometer();

		result = new Report("Sequence Report");
		result.setOrigin("Sequence reporter");

		// Initialize reports
		
		Report surveyReport = null;
		Report detailedReport = null;
		Report actorEventTableReport = null;
		Report interactionTableReport = null;
		Report biographyReport = null;
		Report extendedBiographyReport = null;
		
		List<Report> reports = new ArrayList<Report>();

		if (criteria.getSequenceReportTypes().contains(SequenceReportType.ITINERARIES_SURVEY)){
			surveyReport = new Report("Survey");
			reports.add(surveyReport);
		}
		
		if (criteria.getSequenceReportTypes().contains(SequenceReportType.ITINERARIES_DETAILS)){
			detailedReport = new Report("Details");
			reports.add(detailedReport);
		}
		
		if (criteria.getSequenceReportTypes().contains(SequenceReportType.ACTOR_EVENT_TABLES)){
			actorEventTableReport = new Report("Actor-Event tables");
			reports.add(actorEventTableReport);
		}
		
		if (criteria.getSequenceReportTypes().contains(SequenceReportType.INTERACTION_TABLES)){
			interactionTableReport = new Report("Interaction tables");
			reports.add(interactionTableReport);
		}

		// Initialize geography
		
		Geography geography = segmentation.getGeography();
		
		// Create sequences

		EgoSequences sequences = new EgoSequences();

		for (Individual ego : segmentation.getCurrentIndividuals().toSortedList()) {

			if (surveyReport!=null){
				surveyReport.outputs().appendln(ego.signature());
				surveyReport.outputs().appendln();
			}
			
			if (detailedReport!=null){
				detailedReport.outputs().appendln(ego.signature());
				detailedReport.outputs().appendln();
			}

			EgoSequence sequence = SequenceMaker.createPersonalEventSequence(ego, segmentation, criteria);
			sequences.add(sequence);

			// Move to appropriate census!
/*			for (Individual spouse : ego.spouses()) {
				if (spouse.getAttributeValue(spouseFilterLabel) != null) {
					// surveyReport.outputs().appendln("Common events with spouse ("+spouse+"): "+SequenceWorker.getCommonEvents(ego,spouse,censusCriteria.getRelationModelName()).size());
				}
			}*/
			
			RelationEnvironment egoEnvironment = new RelationEnvironment(sequence.getStations().values(),sequence.getEgo(),criteria.getEgoRoleName(),criteria.getRoleNames(),criteria.getRelationModelNames(), criteria.getImpersonalAlterLabel());
			egoEnvironment.setAlterRelations(sequence.getStations().values(),sequence.getEgo(),criteria.getEgoRoleName(),"ALL", criteria.getRelationModelNames(), criteria.getPattern(), criteria.getChainClassification());

			// Add role tables

			if (actorEventTableReport!=null){
				actorEventTableReport.outputs().append(SequenceWorker.roleTable(sequence));
			}
			
			if (interactionTableReport!=null){
				interactionTableReport.outputs().append(SequenceWorker.interactionTable(sequence));
			}

			// Create coherent subsequences

			EgoSequences subSequences = SequenceWorker.split(sequence);

			for (EgoSequence subSequence : subSequences) {

				if (subSequences.size() > 1) {
					if (surveyReport!=null){
						surveyReport.outputs().appendln(subSequence.getId());
					}
					if (detailedReport!=null){
						detailedReport.outputs().appendln(subSequence.getId());
					}
				}
				
				String startPlaceLabel = criteria.getStartPlaceLabel();
				String endPlaceLabel = criteria.getEndPlaceLabel();
				Place hiddenStart = null;
									
				for (Ordinal key : subSequence.getStations().keySet()) {

					Relation event = subSequence.getStations().get(key);
					Place start = geography.getByHomonym(event.getAttributeValue(startPlaceLabel));
					Place end = geography.getByHomonym(event.getAttributeValue(endPlaceLabel));
					String actorString = event.getActorsAsString(egoEnvironment);
					StringList stories = getStories(event, ego, criteria.getEgoRoleName());
					List<String> impersonalRelation = RelationValuator.getImpersonalRelationTypes(event, ego, egoEnvironment.getEgoRoleName(), criteria.getImpersonalAlterLabel(), egoEnvironment.getRelationsByAlter());
					
					if (!impersonalRelation.contains("TRANSITION")){
												
						if (hiddenStart!=null){
							start = hiddenStart;
							hiddenStart = null;
						}
						
						Place ancestor = geography.getCommonAncestor(start, end);
						GeoLevel commonLevel = null;
						String commonPlaceName = null;
						
						if (ancestor != null) {
							commonLevel = ancestor.getGeoLevel();
							commonPlaceName = ancestor.getToponym();
						}

						String order = SequenceWorker.order(event, ego);

						if (surveyReport!=null){
							
							surveyReport.outputs().appendln(
									key + "\t" + order + "\t"+key.getYear()+"\t(" + subSequence.getEgoAge(key.getYear()) + ")\t" + event.getTypedId() + "\t"
											+ start + "\t" + end + "\t" + commonLevel + "\t("
											+ commonPlaceName + ")\t"+actorString);
						}
						
						if (detailedReport!=null){
							detailedReport.outputs().appendln(
									key + "\t" + order  + "\t"+key.getYear()+ "\t(" + subSequence.getEgoAge(key.getYear()) + ")\t" + event.getTypedId() + "\t"
											+ start + "\t" + end + "\t" + commonLevel + "\t("
											+ commonPlaceName + ")\t"+actorString);
							detailedReport.outputs().appendln();
							detailedReport.outputs().appendln(stories);
						}
						
					} else {
						
						hiddenStart = start;
						
						if (!event.actors().getByRole("HOST").isEmpty()){
							System.err.println("Transition with host "+ego+" "+event+" "+event.getIndividuals("HOST"));
						}
					}
					
				} 
				
				for (Report report : reports){
					report.outputs().appendln();
				}
			}
		}

		// Create sequences including life events

		if (criteria.getSequenceReportTypes().contains(SequenceReportType.BIOGRAPHIES)){
			
			biographyReport = new Report("Biographies");
			reports.add(biographyReport);

			for (EgoSequence biography : SequenceMaker.createBiographies(net, segmentation, criteria).toSortedList()) {

				Individual ego = segmentation.getCurrentIndividuals().getById(biography.getId());

				biographyReport.outputs().appendln(ego.signature());
				for (Ordinal key : biography.getStations().keySet()) {
					Relation event = biography.getStation(key);
					biographyReport.outputs().appendln(
							key + "\t" + event.getRoles(ego).toString() + " (" + biography.getEgoAge(key.getYear()) + ")\t" + event.getName() + "\t"
									+ RelationValuator.getEgoRolePartners(event,ego, criteria.getRelationModelName(), criteria.getEgoRoleName()));
				}
				biographyReport.outputs().appendln();
			}
		}
		
		// Create extended biographies

		if (criteria.getSequenceReportTypes().contains(SequenceReportType.EXTENDED_BIOGRAPHIES)){

			extendedBiographyReport = new Report("Extended biographies");
			reports.add(extendedBiographyReport);

			for (EgoSequence extendedBiography : SequenceMaker.createExtendedBiographies(net, segmentation, criteria).toSortedList()) {

				Individual ego = segmentation.getCurrentIndividuals().getById(extendedBiography.getId());

				extendedBiographyReport.outputs().appendln(ego.signature());
				for (Ordinal key : extendedBiography.getStations().keySet()) {
					Relation event = extendedBiography.getStation(key);
					extendedBiographyReport.outputs().appendln(
							key + "\t" + event.getRoles(ego).toString() + " (" + extendedBiography.getEgoAge(key.getYear()) + ")\t" + event.getName() + " "
									+ RelationValuator.getEgoRolePartners(event,ego, criteria.getRelationModelName(), criteria.getEgoRoleName()));
				}
				extendedBiographyReport.outputs().appendln();
			}
		}
		
		// Create Pajek File

		if (criteria.getSequenceReportTypes().contains(SequenceReportType.EXPORT_RELATION_GRAPH)){
			
			Graph<Individual> graph = NetUtils.createRelationGraph(segmentation, criteria.getRelationModelName());
			List<String> partitionLabels = new ArrayList<String>();

			StringList pajekBuffer = new StringList();
			pajekBuffer.addAll(PuckUtils.writePajekNetwork(graph, partitionLabels));
			pajekBuffer.appendln();


			if (pajekBuffer.length() != 0) {
				File targetFile = ToolBox.setExtension(ToolBox.addToName(new File(segmentation.getLabel()), "-Relation Network"), ".paj");
				ReportRawData rawData = new ReportRawData("Export Relation Network to Pajek", "Pajek", "paj", targetFile);
				rawData.setData(PAJFile.convertToMicrosoftEndOfLine(pajekBuffer.toString()));

				result.outputs().appendln();
				result.outputs().append(rawData);
			}
		}

		for (Report report : reports){
			result.outputs().append(report);
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * 
	 * @param report
	 * @param value
	 * @param sequenceTypeNetwork
	 */
	private static <S extends Sequenceable<E>, E extends Numberable> Report reportSequenceTree(final String title, final SequenceStatistics<S,E> sequenceStatistics) {
		Report report;
		
		report = new Report(title);
		
		for (String value : sequenceStatistics.getSequenceNetworkLabels()){

			report.outputs().appendln("Sequence Tree " + value);
			
			Graph<Cluster<String>> sequenceTypeNetwork = sequenceStatistics.getSubSequenceMatrix(value).getSequenceNetworks()[2];
			
			Node<Cluster<String>> start = sequenceTypeNetwork.getNode(1);
			Stack<Node<Cluster<String>>> stack = new Stack<Node<Cluster<String>>>();
			report.outputs().appendln(start.getReferent() + "\t" + start.getReferent().size());
			stack.push(start);

			while (!stack.isEmpty()) {
				Node<Cluster<String>> node = stack.pop();
				for (Node<Cluster<String>> next : node.getOutNodes().toListSortedByLabel()) {
					report.outputs().appendln(next.getReferent() + "\t" + next.getReferent().size());
					stack.push(next);
				}
			}
			report.outputs().appendln();
		}
		report.outputs().appendln();

		//
		return report;
	}


	/**
	 * 
	 * @param report
	 * @param segmentation
	 * @throws PuckException
	 */
	private static void reportTriangles(final Report report, final Segmentation segmentation) throws PuckException {

		Map<Integer, Partition<EventTriangle>> trianglesMap = new HashMap<Integer, Partition<EventTriangle>>();
		Partition<EventTriangle> allTriangles = new Partition<EventTriangle>();
		Partition<Individual> triangleTypes = new Partition<Individual>();

		// Get triangles

		for (Individual ego : segmentation.getCurrentIndividuals()) {
			Individuals individuals = ego.getRelated("Migevent");
			individuals.add(ego);
			Partition<EventTriangle> triangles = SequenceWorker.getTriangles(individuals, "Migevent");
			allTriangles.add(triangles);
			trianglesMap.put(ego.getId(), triangles);
			for (Cluster<EventTriangle> cluster : triangles.getClusters()) {
				triangleTypes.put(ego, cluster.getValue());
			}
		}

		// Report triangles
		Report trianglesReport = new Report("Triangles");
		trianglesReport.outputs().appendln("Type\tnrTriangles\tnrEgoNetworks");

		for (Cluster<EventTriangle> cluster : allTriangles.getClusters().toListSortedByValue()) {
			trianglesReport.outputs().appendln(cluster.getValue() + "\t" + cluster.size() + "\t" + triangleTypes.getCluster(cluster.getValue()).size());
		}
		trianglesReport.outputs().appendln();

		PartitionCriteria partitionCriteria = new PartitionCriteria("Triangles");
		ReportChart chart5 = StatisticsReporter.createPartitionChart(allTriangles, partitionCriteria, null, null);
		trianglesReport.outputs().appendln(chart5);

		for (Individual ego : segmentation.getCurrentIndividuals().toSortedList()) {
			Partition<EventTriangle> triangles = trianglesMap.get(ego.getId());
			trianglesReport.outputs().appendln(ego + "\t" + triangles.size() + " types");
			for (Cluster<EventTriangle> cluster : triangles.getClusters().toListSortedByValue()) {
				trianglesReport.outputs().appendln(cluster.getValue() + "\t" + cluster.size());
				for (EventTriangle triangle : cluster.getItems()) {
					trianglesReport.outputs().appendln(triangle.getEventPattern());
				}
				trianglesReport.outputs().appendln();
			}
			trianglesReport.outputs().appendln();
		}
	}

	/**
	 * 
	 * @param cluster
	 * @return
	 */
	private static int sequenceNumber(final Cluster<String> cluster) {
		int result;

		Set<String> set = new HashSet<String>();

		for (String string : cluster.getItems()) {
			set.add(string.split("\\s")[0]);
		}

		result = set.size();

		//
		return result;
	}


	public static <S extends Sequenceable<E>,E extends Populatable> Report reportMembers(Sequenceables<S,E> slices, SequenceCriteria criteria) throws PuckException{
		Report result;
		
//		Relations relations = slices.relations();
		
		String empty = "\t\t\t\t\t";
		
		result = new Report(criteria.getRelationModelName()+" Members");
				
		List<Ordinal> times = Ordinal.getOrdinals(criteria.getDates());
		
//		Map<String,Individuals> membersByRelationId = slices.membersByRelationId();

		if (slices.isPopulatable()){
			
			for (S sequence : slices.toSortedList()){

				StringList list = new StringList();

//				for (String idValue : slices.idValues()){
					list.appendln(sequence.getId());
					list.appendln();
					
					List<Individual> members = ((Populatable)sequence).getAllIndividuals(times).toSortedList(Sorting.BIRT_YEAR);
//					List<Individual> members = membersByRelationId.get(idValue).toSortedList(Sorting.BIRT_YEAR);

					list.append("Year\t");
					for (Ordinal time : times){
						list.append(time+"\tNote\tReferent\tRelation\tChain\t");
					}
					list.appendln();

					for (Individual member : members){
						
						StringList additionalList = new StringList();
						
						list.append(member.signature()+" ("+IndividualValuator.lifeStatusAtYear(member, criteria.getReferenceYear())+")\t");

						String emptyCumul = "---\t";
						
						for (Ordinal time : Ordinal.getOrdinals(criteria.getDates())){
							
							List<String> statusLines = SequenceWorker.getStatus(slices, member, time, criteria);
							
							if (!statusLines.isEmpty()){

								list.append(statusLines.get(0)+"\t");

								for (int i=1;i<statusLines.size();i++){

									additionalList.append(emptyCumul+statusLines.get(i));

								}
							
							} else {
								
								Integer deathYear = IndividualValuator.getDeathYear(member);
								Integer birthYear = IndividualValuator.getBirthYear(member);
								
								if (deathYear!=null && deathYear<=time.getYear()){
									list.append("+"+deathYear);
								} else if (birthYear!=null && birthYear>=time.getYear()){
									list.append("*"+birthYear);
								} else {
									list.append("?");
								}
								list.append(empty);
							}
							
							emptyCumul += empty;
						}

						list.appendln();
						
						for (String additionalLine : additionalList){
							
							list.appendln(additionalLine);
						}
					}
					//
					list.appendln();

/*					Partition<Individual> presencePartition = SequenceWorker.getPresencePartition(slices, members, criteria);
					
					list.appendln("Profile\tNumber\t%\tList");

					for (Cluster<Individual> cluster : presencePartition.getClusters().toListSortedByValue()){
						list.appendln(cluster.getValue()+"\t"+cluster.size()+"\t"+presencePartition.share(cluster)+"\t"+cluster.getItemsAsSortedString());
					}
					//
					list.appendln();
				
					//*/
					result.outputs().append(list);
					
/*					MultiPartition<Individual> chainMobilityPartition = SequenceWorker.getChainMobilityPartition(slices, members, criteria);
					
					ReportTable table = CensusReporter.getFrequencyTable(chainMobilityPartition);
					result.outputs().append(table);*/
					
				}
			
				logger.debug("Member report created for "+slices);
				
		} else {
			
			throw PuckExceptions.INVALID_PARAMETER.create(slices +"has no members.");

		}
		//
		return result;
	}
	
	public static <S extends Sequenceable<E>,E extends Populatable> Report reportTurnover (Sequenceables<S,E> slices, SequenceCriteria criteria) throws PuckException{
		Report result;
		
		result = new Report(criteria.getRelationModelName()+" Turnover");
				
		if (slices.isPopulatable()){
			
			StringList list = new StringList();

			List<Individual> members = ((Populatable)slices).getAllIndividuals(Ordinal.getOrdinals(criteria.getDates())).toSortedList(Sorting.BIRT_YEAR);
			
			//
			list.appendln("Turnover statistics");
			list.appendln();
			
			Partition<Individual> presencePartition = SequenceWorker.getPresencePartition(slices, members, criteria);
			
			int[][] turnoverStatistics = SequenceWorker.getTurnoverStatistics(presencePartition, criteria);

			int[] stay = turnoverStatistics[0];
			int[] in = turnoverStatistics[1];
			int[] out = turnoverStatistics[2];
			int[] all = turnoverStatistics[3];
			
			Integer[] dates = criteria.getDates();

			list.appendln("Dates\tFirst\tSecond\tAll\tStay\t%Turnover\tOut\t%Out\tIn\t%In");

			for (int time=0;time<dates.length;time++){
				
				if (time<dates.length-1){

					list.append(dates[time]+"-"+dates[time+1]+"\t");

				} else {
					
					list.append(dates[0]+"-"+dates[dates.length-1]+"\t");
					
				}
				
				list.append((stay[time]+out[time])+"\t"+(stay[time]+in[time])+"\t"+all[time]+"\t");
				list.append(stay[time]+"\t"+(100.0-MathUtils.percent(stay[time], all[time]))+"\t");
				list.append(out[time]+"\t"+MathUtils.percent(out[time], out[time]+stay[time])+"\t");
				list.append(in[time]+"\t"+MathUtils.percent(in[time], in[time]+stay[time])+"\t");
				list.appendln();
			}

			list.appendln();
			list.appendln("Detailed Pattern");
			list.appendln();
			list.appendln("Profile\tNumber\t%\tList");

			for (Cluster<Individual> cluster : presencePartition.getClusters().toListSortedByValue()){
				list.appendln(cluster.getValue()+"\t"+cluster.size()+"\t"+presencePartition.share(cluster)+"\t"+cluster.getItemsAsSortedString());
			}
			//
			list.appendln();

			//
			result.outputs().append(list);
			
			MultiPartition<Individual>[] chainMobilityPartitions = SequenceWorker.getChainMobilityPartition(slices, members, criteria);
			
			ReportTable positionTable = CensusReporter.getFrequencyTable(chainMobilityPartitions[0]);
			ReportTable movementTable = CensusReporter.getFrequencyTable(chainMobilityPartitions[1]);
			
			result.outputs().appendln("Migrations by referent chain");
			result.outputs().append(positionTable);
			result.outputs().appendln();
			
			result.outputs().appendln("Migrations by referent chain change");
			result.outputs().append(movementTable);
			result.outputs().appendln();
			
			logger.debug("Turnover report created for "+slices);
				
		} else {
			
			throw PuckExceptions.INVALID_PARAMETER.create(slices +"has no members.");

		}
		//
		return result;
	}


/*	public static Report reportIndividualDynamics(final Segmentation segmentation, final Sequences<Relation> sequences, final SequenceCriteria spaceTimeCriteria, final StatisticsCriteria statisticsCriteria, final PartitionCriteria placeCriteria) throws PuckException{
				Report result;
				
				if ((sequences == null) || (statisticsCriteria == null)) {
					throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
				} else {
					result = new Report("Population Dynamics "+spaceTimeCriteria.getRelationModelName());
					Chronometer chrono = new Chronometer();
		
					result.setOrigin("Space reporter");
					
					List<Ordinal> times = Ordinal.getOrdinals(spaceTimeCriteria.getDates());
					
					List<ReportChart> charts = new ArrayList<ReportChart>(20);
					List<ReportTable> tables = new ArrayList<ReportTable>(20);
					
					SequenceStatistics<Sequence<Relation>,Relation> sequenceStatistics = new SequenceStatistics<Sequence<Relation>,Relation>(segmentation, sequences, spaceTimeCriteria, statisticsCriteria.getPartitionCriteriaList());
					Map<String,PartitionSequence<Individual>> census = sequenceStatistics.getDynamicIndividualCensus(spaceTimeCriteria, placeCriteria);
					
					for (int i=0;i<times.size()-1;i++){
						
						Ordinal startTime = times.get(i);
						Ordinal endTime = times.get(i+1);
						
						PartitionCriteria partitionCriteria = new PartitionCriteria();
						partitionCriteria.setLabel(spaceTimeCriteria.getRelationModelName()+" "+startTime+"-"+endTime);
						
						Relations totalStartSpace = segmentation.getAllRelations().getByTime(spaceTimeCriteria.getDateLabel(), startTime.getYear());
						Relations totalEndSpace = segmentation.getAllRelations().getByTime(spaceTimeCriteria.getDateLabel(), endTime.getYear());
						
						Relations filteredStartSpace = slices.getStation(startTime);
						Relations filteredEndSpace = slices.getStation(endTime);
						
						Partition<Individual> partition = new Partition<Individual>();
						partition.setLabel(spaceTimeCriteria.getRelationModelName()+" "+startTime+"-"+endTime);
						
						PartitionCriteria partitionCriteria = new PartitionCriteria();
						partitionCriteria.setLabel(partition.getLabel());
						
						Individuals totalStartPopulation = totalStartSpace.getIndividuals();
						Individuals totalEndPopulation = totalEndSpace.getIndividuals();
						Individuals filteredStartPopulation = filteredStartSpace.getIndividuals();
						Individuals filteredEndPopulation = filteredEndSpace.getIndividuals();
						
						// Forward 
						
						Partition<Individual> destinations = new Partition<Individual>();
						destinations.setLabel("Destinations "+startTime+"/"+endTime);
						
						for (Individual individual : filteredStartSpace.getIndividuals().toSortedList()){
							if (IndividualValuator.lifeStatusAtYear(individual, endTime.getYear()).equals("DEAD")){
								partition.put(individual, new Value("DIED"));
							} else if (!totalEndPopulation.contains(individual)){
								partition.put(individual, new Value("UNKNOWN DESTINATION"));
							} else if (!filteredEndPopulation.contains(individual)){
								partition.put(individual, new Value("LEFT"));
								for (Relation destination : totalEndSpace.getByIndividual(individual)){
									destinations.put(individual,RelationValuator.get(destination,"PLACE",statisticsCriteria.getPlaceParameter()));
								}
							} else {
								Relation start = filteredStartSpace.getByIndividual(individual).getFirst();
								Relation end = filteredEndSpace.getByIndividual(individual).getFirst();
								String startUnit = start.getAttributeValue(slices.idLabel());
								if (startUnit==null){
									startUnit = start.getAttributeValue(spaceTimeCriteria.getPlaceLabel());
								}
								String endUnit = end.getAttributeValue(slices.idLabel());
								if (endUnit==null){
									endUnit = start.getAttributeValue(spaceTimeCriteria.getPlaceLabel());
								}
								if (!startUnit.equals(endUnit)){
									partition.put(individual, new Value("INTERNAL CHANGE"));
								} else {
									partition.put(individual, new Value("UNCHANGED"));
								}
							}
						}
						
						// Backward 
						
						Partition<Individual> origins = new Partition<Individual>();
						origins.setLabel("Origins "+startTime+"/"+endTime);
		
						for (Individual individual : filteredEndSpace.getIndividuals().toSortedList()){
							if (IndividualValuator.lifeStatusAtYear(individual, startTime.getYear()).equals("UNBORN")){
								partition.put(individual, new Value("NEWBORN"));
							} else if (!totalStartPopulation.contains(individual)){
								partition.put(individual, new Value("UNKNOWN ORIGIN"));
							} else if (!filteredStartPopulation.contains(individual)){
								partition.put(individual, new Value("ENTERED"));
								for (Relation origin : totalStartSpace.getByIndividual(individual)){
									origins.put(individual,RelationValuator.get(origin,"PLACE",statisticsCriteria.getPlaceParameter()));
								}
							} else if (!filteredStartSpace.getByIndividual(individual).equals(filteredEndSpace.getByIndividual(individual))){
		//						partition.put(individual, new Value("INTERNAL CHANGE2"));
							} else {
		//						partition.put(individual, new Value("UNCHANGED2"));
							}
						}*/
						
	/*					for (Cluster<Individual> cluster : destinations.getClusters()){
							System.out.println(cluster+"\t"+cluster.size()+"\t"+cluster.getItemsAsString());
						}
						
						ReportChart chartChanges = StatisticsReporter.createPartitionChart(census.get("MIGRATIONS").getStation(endTime), partitionCriteria, statisticsCriteria.getSplitCriteria());
						charts.add(chartChanges);
						
						ReportTable tableChanges = ReportTable.transpose(chartChanges.createReportTableWithSum());
						tables.add(tableChanges);
						
						ReportChart chartDestinations = StatisticsReporter.createPartitionChart(census.get("DESTINATIONS").getStation(startTime), partitionCriteria, statisticsCriteria.getSplitCriteria());
						charts.add(chartDestinations);
						
						ReportTable tableDestinations = ReportTable.transpose(chartDestinations.createReportTableWithSum());
						tables.add(tableDestinations);
						
						ReportChart chartOrigins = StatisticsReporter.createPartitionChart(census.get("ORIGINS").getStation(endTime), partitionCriteria, statisticsCriteria.getSplitCriteria());
						charts.add(chartOrigins);
						
						ReportTable tableOrigins = ReportTable.transpose(chartOrigins.createReportTableWithSum());
						tables.add(tableOrigins);
		
					}
					
					int nr = Math.min(4, 2*times.size());
					
					// Manage the number of chart by line.
					for (int chartIndex = 0; chartIndex < charts.size(); chartIndex++) {
						result.outputs().append(charts.get(chartIndex));
						if (chartIndex % nr == nr-1) {
							result.outputs().appendln();
						}
					}
		
					// Add chart tables.
					for (ReportTable table : tables) {
						result.outputs().appendln(table);
		
					}
					
					result.setTimeSpent(chrono.stop().interval());
				}
		
				//
				return result;
			}


	public static Report reportIndividualStatics(final Segmentation segmentation, final Sequence<Relations> slices, final SequenceCriteria spaceTimeCriteria, final StatisticsCriteria statisticsCriteria) throws PuckException{
			Report result;
			
			if (statisticsCriteria == null) {
				throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
			} else {
				result = new Report("Population Statics "+spaceTimeCriteria.getRelationModelName());
				Chronometer chrono = new Chronometer();
	
				result.setOrigin("Sequence reporter");
	//			result.setTarget(spaces.getLabel());
				
				Sequences<Relation> sequences = SequenceMaker.createRelationSequences(segmentation, spaceTimeCriteria);
				SequenceStatistics<Sequence<Relations>,Relations> sequenceStatistics = new SequenceStatistics(segmentation, sequences, spaceTimeCriteria, statisticsCriteria.getPartitionCriteriaList());

	
				// Compute charts and tables.
				for (PartitionCriteria partitionCriteria : statisticsCriteria.getPartitionCriteriaList()) {
	
					if (StringUtils.isEmpty(partitionCriteria.getLabel())) {
						continue;
					}
	
					List<ReportChart> charts = new ArrayList<ReportChart>(20);
					List<ReportTable> tables = new ArrayList<ReportTable>(20);
					

//					SequenceStatistics<Sequence<Relations>,Relations> sequenceStatistics = new SequenceStatistics<Sequence<Relations>,Relations>(segmentation, slices, spaceTimeCriteria);
					PartitionSequence<?> census = sequenceStatistics.getAggregatePartitionSequence(partitionCriteria.getLabel());
//					PartitionSequence<Sequence<Relations>> census = sequenceStatistics.getPartitionSequence(partitionCriteria.getLabel());
//					PartitionSequence<Individual> census = sequenceStatistics.getDatedIndividualCensus(spaceTimeCriteria, partitionCriteria);
										
					if (census != null){
					
					for (Ordinal time : slices.getTimes()){
						
						Partition<?> partition = census.getStation(time);
//						Partition<Individual> partition = census.getStation(time);
	
						/*
						String label = spaceTimeCriteria.getRelationModelName()+" "+time;
						Relations relations = slices.getStation(time);
						Partition<Individual> partition = new Partition<Individual>();
						
						if (partitionCriteria.getLabel().equals("REFERENT")){
	
							partitionCriteria.setLabelParameter(spaceTimeCriteria.getRelationModelName()+" "+spaceTimeCriteria.getEgoRoleName()+" "+time);
							Partition<Individual> prePartition = PartitionMaker.create(label, relations.getIndividuals(), relations, partitionCriteria);
							
							for (Individual ego : prePartition.getItemsAsList()){
								Value alterId = prePartition.getValue(ego);
								if (alterId!=null){
									List<String> alterRoles = NetUtils.getAlterRoles(ego, segmentation.getAllIndividuals().getById(alterId.intValue()), ToolBox.stringsToInts(spaceTimeCriteria.getPattern()), spaceTimeCriteria.getRelationModelNames(), spaceTimeCriteria.getChainClassification(), null, null);
									Collections.sort(alterRoles);
									partition.put(ego, new Value(alterRoles.toString()));
								}
							}
							
							
						} else {
							if (partitionCriteria.getLabel().equals("AGE") || partitionCriteria.getLabel().equals("MATRISTATUS")|| partitionCriteria.getLabel().equals("OCCUPATION")){
								partitionCriteria.setLabelParameter(time+"");
							} 
							partition = PartitionMaker.create(label, relations.getIndividuals(), relations, partitionCriteria);
						}
						//
	
						ReportChart chart = StatisticsReporter.createPartitionChart(partition, partitionCriteria, statisticsCriteria.getSplitCriteria());
						charts.add(chart);
						
						ReportTable table = ReportTable.transpose(chart.createReportTableWithSum());
						tables.add(table);
	
					}
					
					int nr = Math.min(4, slices.getNrStations());
					
					// Manage the number of chart by line.
					for (int chartIndex = 0; chartIndex < charts.size(); chartIndex++) {
						result.outputs().append(charts.get(chartIndex));
						if (chartIndex % nr == nr-1) {
							result.outputs().appendln();
						}
					}
	
					// Add chart tables.
					for (ReportTable table : tables) {
						result.outputs().appendln(table);
					}
					}
				}
	
				result.setTimeSpent(chrono.stop().interval());
			}
	
			//
			return result;
		}


	public static Report reportEgoNetworks (Segmentation segmentation, Sequence<Relations> slices, final StatisticsCriteria criteria, final SequenceCriteria spaceTimeCriteria) throws PuckException{
			Report result;
			
			if ((slices == null) || (criteria == null)) {
				throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
			} else {
				result = new Report("Ego Networks "+spaceTimeCriteria.getRelationModelName());
				Chronometer chrono = new Chronometer();
	
				result.setOrigin("Space reporter");
				
				SequenceStatistics census = new SequenceStatistics(segmentation, slices.toSequencesByEgo(), spaceTimeCriteria);
				Individuals members = slices.getIndividuals();
				
				// Create Reports
				Report overallReport = new Report("Survey");
				Report diagramReport = new Report("Diagrams");
				Report detailReport = new Report("Details");
				Report componentReport = new Report("Components");
						
				// Create Partition charts and tables
				List<ReportChart> charts = new ArrayList<ReportChart>();
				List<ReportTable> tables = new ArrayList<ReportTable>();
					
				// Make overall report and diagrams
				overallReport.outputs().appendln("Measure\tAverage (Male)\tAverage Pos. (Male)\tMedian (Male)\tMaximum (Male)\tSum (Male)\tAverage (Female)\tAverage Pos. (Female)\tMedian (Female)\tMaximum (Female)\tSum (Female)\tAverage (All)\tAverage Pos. (All)\tMedian (All)\tMaximum (All)\tSum (All)");
	
				// Set partition criteria 
				for (String label : spaceTimeCriteria.getCensusOperationLabels()){
					
					PartitionCriteria partitionCriteria = new PartitionCriteria(label);
					
					ReportChart chart = null;
					
					if (!label.contains("ALTERS") && !label.contains("PROFILE")){
						
						NumberedValues values = census.getValues(label);
						
						Partition<Relation> partition = PartitionMaker.create(label, slices.getStation(slices.getTimes().get(0)), values, partitionCriteria);
					
						PartitionCriteria splitCriteria = new PartitionCriteria(spaceTimeCriteria.getPartitionLabel());
						chart = StatisticsReporter.createPartitionChart(partition, partitionCriteria, null);
	
						overallReport.outputs().append(label+"\t");
						String sum = "";
						if (label.startsWith("NR")){
							sum = new Double(values.sum()).intValue()+"";
						}
						overallReport.outputs().append(MathUtils.round(values.average(),2)+"\t"+MathUtils.round(values.averagePositives(),2)+"\t"+values.median()+"\t"+values.max()+"\t"+sum+"\t");
						overallReport.outputs().appendln();
					}
				
					if (chart != null) {
						charts.add(chart);
						ReportTable table = ReportTable.transpose(chart.createReportTableWithSum());
						tables.add(table);
						if (!label.contains("EVENTS_") && !label.contains("RELATIONS")){
							tables.add(ReportTable.normalize(table));
						}
					}
				}
			
				overallReport.outputs().appendln();
			
				
			// Make overall report and diagrams
			overallReport.outputs().appendln("Measure\tAverage (Male)\tAverage Pos. (Male)\tMedian (Male)\tMaximum (Male)\tSum (Male)\tAverage (Female)\tAverage Pos. (Female)\tMedian (Female)\tMaximum (Female)\tSum (Female)\tAverage (All)\tAverage Pos. (All)\tMedian (All)\tMaximum (All)\tSum (All)");
	
			// Set partition criteria 
			for (String label : spaceTimeCriteria.getCensusOperationLabels()){
				
				PartitionCriteria partitionCriteria = new PartitionCriteria(label);
	//			partitionCriteria.setValueFilter(ValueFilter.NULL);
				
				if (label.contains("PROFILE")){
					partitionCriteria.setType(PartitionType.PARTIALIZATION);
				} 
				
				if (label.equals("NREVENTS")){
					partitionCriteria.setType(PartitionType.RAW);
	//				partitionCriteria.setCumulationType(CumulationType.DESCENDANT);
				} else if (label.contains("AGEFIRST")){
					partitionCriteria.setType(PartitionType.SIZED_GROUPING);
					partitionCriteria.setStart(0.);
					partitionCriteria.setSize(5.);
				} else if (label.equals("ECCENTRICITY")){
					partitionCriteria.setType(PartitionType.SIZED_GROUPING);
					partitionCriteria.setStart(-100.);
					partitionCriteria.setSize(20.);
				} else if (label.contains("COVERAGE") || label.contains("SAME")|| label.contains("NORM")|| label.contains("DENSITY")|| label.contains("BETWEENNESS") || label.contains("EFFICIENCY")|| label.contains("CONCENTRATION")){
					partitionCriteria.setType(PartitionType.SIZED_GROUPING);
					partitionCriteria.setStart(0.);
					partitionCriteria.setSize(20.);
				} else if (label.contains("MEAN") || label.contains("COVERAGE") || label.contains("PEREVENT") || label.contains("BETWEENNESS")|| label.contains("BROKERAGE")|| label.contains("EFFICIENT_SIZE")){
					partitionCriteria.setType(PartitionType.SIZED_GROUPING);
					partitionCriteria.setStart(0.);
					partitionCriteria.setSize(1.);
				} else {
					partitionCriteria.setType(PartitionType.RAW);
				}
				
				ReportChart chart = null;
				
				if (!label.contains("ALTERS") && !label.contains("PROFILE")){
					NumberedValues values = census.getValues(label);
					
					Partition<Individual> partition = PartitionMaker.create(label, members, values, partitionCriteria);
				
					PartitionCriteria splitCriteria = new PartitionCriteria(spaceTimeCriteria.getPartitionLabel());
					chart = StatisticsReporter.createPartitionChart(partition, partitionCriteria, splitCriteria);
	
					if (label.substring(0, 3).equals("AGE")){
						
						partitionCriteria.setType(PartitionType.RAW);
						partitionCriteria.setSizeFilter(SizeFilter.HOLES);
						partitionCriteria.setValueFilter(ValueFilter.NULL);
					
						partition = PartitionMaker.create(label, members, values, partitionCriteria);
						
						if (partition.maxValue()!=null){
							ReportChart survivalChart = StatisticsReporter.createSurvivalChart(partition, splitCriteria);
							charts.add(survivalChart);
						} else {
							System.err.println(label+" no max value");
						}
					}
					
					NumberedValues[] genderedValues = PuckUtils.getGenderedNumberedValues(values, members);
					
					overallReport.outputs().append(label+"\t");
					for (int gender=0;gender<3;gender++){
						String sum = "";
						if (label.startsWith("NR")){
							sum = new Double(genderedValues[gender].sum()).intValue()+"";
						}
						overallReport.outputs().append(MathUtils.round(genderedValues[gender].average(),2)+"\t"+MathUtils.round(genderedValues[gender].averagePositives(),2)+"\t"+values.median()+"\t"+genderedValues[gender].max()+"\t"+sum+"\t");
					}
					overallReport.outputs().appendln();
					
				}
			
				if (chart != null) {
					charts.add(chart);
						ReportTable table = ReportTable.transpose(chart.createReportTableWithSum());
						tables.add(table);
						if (!label.contains("EVENTS_") && !label.contains("RELATIONS")){
							tables.add(ReportTable.normalize(table));
						}
					
	
				}
			
			}
			overallReport.outputs().appendln();
			
			// Create detailed report
			detailReport.outputs().append("Nr\tEgo\tGender");
			for (String partitionLabel : spaceTimeCriteria.getCensusOperationLabels()){
					detailReport.outputs().append("\t"+partitionLabel);
			}
			
			Map<String,Map<String,Double>> componentChartMap = new TreeMap<String,Map<String,Double>>();
			
			detailReport.outputs().appendln();
			for (Individual ego : members.toSortedList()){
							
				if (census.getValues("SIZE").get(ego.getId())!=null) {
					detailReport.outputs().append(ego.getId()+"\t"+ego+"\t"+ego.getGender());
					for (String label : spaceTimeCriteria.getCensusOperationLabels()){
							detailReport.outputs().append("\t"+census.getValues(label).get(ego.getId()));
					}
					detailReport.outputs().appendln();
				}
								
					for (String networkTitle : spaceTimeCriteria.getNetworkTitles()){
						Map<Integer,Partition<Node<Individual>>> componentsMap = census.getComponents(networkTitle);
						if (componentsMap!=null){
							Partition<Node<Individual>> components = componentsMap.get(ego.getId());
							
							componentReport.outputs().appendln("Components "+networkTitle);
							componentReport.outputs().appendln(ego+"\t"+components.size());
							int i=1;
							for (Cluster<Node<Individual>> cluster : components.getClusters().toListSortedByValue()){
								componentReport.outputs().appendln("\t"+i+"\t"+cluster.getValue()+"\t("+cluster.size()+")\t"+cluster.getItemsAsString());
								i++;
							}
							componentReport.outputs().appendln();
							
							for (Value value : components.getValues()){
								String label = value.toString();
								Map<String,Double> map = componentChartMap.get(label);
								if (map==null){
									map = new TreeMap<String,Double>();
									for (Gender gender : Gender.values()){
										map.put(gender.toString(), 0.);
									}
									componentChartMap.put(label, map);
								}
								map.put(ego.getGender().toString(), map.get(ego.getGender().toString())+1);
							}
						}
					}
				}
			
			
				ReportChart componentChart = StatisticsReporter.createChart("COMPONENTS", componentChartMap);
				charts.add(componentChart);
				tables.add(ReportTable.transpose(componentChart.createReportTableWithSum()));
			
				if (census.getRelationConnectionMatrix()!=null){
					for (ReportChart chart : census.getRelationConnectionMatrix().getCharts()){
						charts.add(chart);
					}
					tables.add(census.getRelationConnectionMatrix().getTable("Component Connections"));
				}
			
			
			// Manage the number of chart by line.
			for (int chartIndex = 0; chartIndex < charts.size(); chartIndex++) {
				diagramReport.outputs().append(charts.get(chartIndex));
				if (chartIndex % 4 == 3) {
					diagramReport.outputs().appendln();
				}
			}
			
			// Add chart tables.
			for (ReportTable table : tables) {
				diagramReport.outputs().appendln(table.getTitle());
				diagramReport.outputs().appendln(table);
			}
			
			// Finalize reports
			result.outputs().append(overallReport);
			result.outputs().append(diagramReport);
			result.outputs().append(detailReport);
			
			result.outputs().append(componentReport);
			
			//addPajekData
			
			Map<String,StringList> pajekBuffers = census.getPajekBuffers();
			
			for (String title : pajekBuffers.keySet()){
				
				StringList pajekBuffer = pajekBuffers.get(title);
				if (pajekBuffer.length() != 0) {
					File targetFile = ToolBox.setExtension(ToolBox.addToName(new File(spaceTimeCriteria.getRelationModelName()), "-"+title), ".paj");
					ReportRawData rawData = new ReportRawData("Export "+title+"s to Pajek", "Pajek", "paj", targetFile);
					rawData.setData(PAJFile.convertToMicrosoftEndOfLine(pajekBuffer.toString()));
	
					result.outputs().appendln();
					result.outputs().append(rawData);
				}
			}
			
	
			//
			result.setTimeSpent(chrono.stop().interval());
			}
	
			//
			return result;
		}
	
	public static <S extends Sequenceable<E>,E extends Numberable> Report reportGeneralSequenceCensus(final Sequenceables<S,E> slices, final StatisticsCriteria criteria, final SequenceCriteria spaceTimeCriteria) throws PuckException{
		Report result;
		
		if ((slices == null) || (criteria == null)) {
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
		} else {
			result = new Report("Development Cycles "+spaceTimeCriteria.getRelationModelName());
			Chronometer chrono = new Chronometer();

			result.setOrigin("Space reporter");

			SequenceStatistics<S,E> census = new SequenceStatistics<S,E>(null, slices, spaceTimeCriteria);
//			EgoSequenceStatistics census = new EgoSequenceStatistics(new EgoSequences(slices), spaceTimeCriteria);
			
			CensusType censusType = spaceTimeCriteria.getCensusType();
			
			// Create Reports
			Report overallReport = new Report("Survey");
			Report diagramReport = new Report("Diagrams");
			Report detailReport = new Report("Details");
			
			Report componentReport = new Report("Components");
			Report treeReport = new Report("Trees");
					
			// Create Partition charts and tables
			List<ReportChart> charts = new ArrayList<ReportChart>();
			List<ReportTable> tables = new ArrayList<ReportTable>();
				
			// Make overall report and diagrams
			overallReport.outputs().appendln("Measure\tAverage (Male)\tAverage Pos. (Male)\tMedian (Male)\tMaximum (Male)\tSum (Male)\tAverage (Female)\tAverage Pos. (Female)\tMedian (Female)\tMaximum (Female)\tSum (Female)\tAverage (All)\tAverage Pos. (All)\tMedian (All)\tMaximum (All)\tSum (All)");

			// Set partition criteria 
			for (String label : spaceTimeCriteria.getCensusOperationLabels()){
				
				PartitionCriteria partitionCriteria = new PartitionCriteria(label);
				
				ReportChart chart = null;
				
				if (!label.contains("ALTERS") && !label.contains("PROFILE")){
					
					NumberedValues values = census.getValues(label);
					Partition<S> partition = PartitionMaker.create(label, slices, values, partitionCriteria);
					PartitionCriteria splitCriteria = new PartitionCriteria(spaceTimeCriteria.getPartitionLabel());
					
					chart = StatisticsReporter.createPartitionChart(partition, partitionCriteria, null);

					overallReport.outputs().append(label+"\t");
						String sum = "";
						if (label.startsWith("NR")){
							sum = new Double(values.sum()).intValue()+"";
						}
						overallReport.outputs().append(MathUtils.round(values.average(),2)+"\t"+MathUtils.round(values.averagePositives(),2)+"\t"+values.median()+"\t"+values.max()+"\t"+sum+"\t");
					overallReport.outputs().appendln();
					
				}
			
				if (chart != null) {
					charts.add(chart);
						ReportTable table = ReportTable.transpose(chart.createReportTableWithSum());
						tables.add(table);
						if (!label.contains("EVENTS_") && !label.contains("RELATIONS")){
							tables.add(ReportTable.normalize(table));
						}

				}
			
			}
			overallReport.outputs().appendln();
			
			// Create detailed report
			detailReport.outputs().append("Nr\tEgo\tGender");
			for (String partitionLabel : spaceTimeCriteria.getCensusOperationLabels()){
					detailReport.outputs().append("\t"+partitionLabel);
			}
			
			detailReport.outputs().appendln();
			for (S sequence: slices){
				
//					Individual ego = sequence.getEgo();
				
				if ((((censusType==CensusType.GENERAL) || (censusType==CensusType.PARCOURS)))) {
					detailReport.outputs().append(sequence.getId()+"\t"+sequence.getLabel());
					for (String label : spaceTimeCriteria.getCensusOperationLabels()){
						if (label.contains("SIMILARITY")){
							Value value = census.getValues(label).get(sequence.getId());
							Map<Value,Double[]> indiSimilaritiesMap = (Map<Value,Double[]>)value.mapValue();
							String[] keys = new String[]{"PARENT","CHILD","SIBLING","SPOUSE"};
							for (String key : keys){
								Double[] sim = indiSimilaritiesMap.get(new Value(key));
								if (sim!=null){
									detailReport.outputs().append("\t"+MathUtils.round(sim[4], 2));
								}
							}
						} else {
							detailReport.outputs().append("\t"+census.getValues(label).get(sequence.getId()));
						}
					}
					detailReport.outputs().appendln();
				}
				
			}
			
			
			// SequenceAnalysis
			
			if (censusType == CensusType.PARCOURS){
				
				for (RelationClassificationType relationClassificationType : spaceTimeCriteria.getMainRelationClassificationTypes()){
					
					if (spaceTimeCriteria.getNetworkTitles().contains("Event Type Network")){

						CorrelationMatrix eventSequenceMatrix = census.getEventSequenceMatrix(relationClassificationType.toString());
						
						if (eventSequenceMatrix!=null){
							eventSequenceMatrix.setUngendered(true);
							for (ReportChart chart : eventSequenceMatrix.getCharts()){
								charts.add(chart);
							}
							tables.add(eventSequenceMatrix.getTable("Event Type Sequences"));
						}
						
						overallReport.outputs().appendln();
						overallReport.outputs().appendln("Sequence Network Statistics "+relationClassificationType);
						overallReport.outputs().appendln("\tDensity\tInertia\t(Divergence)\tConcentration\t(Divergence)\tSymmetry\t(Divergence)\tCentral nodes");
						
						for (Gender gender : Gender.values()){
							GraphProfile<Cluster<String>> profile = eventSequenceMatrix.getProfile(gender);

							String centralReferents = "";
							for (Cluster<String> centralReferent : profile.getCentralReferents()){
								centralReferents+=centralReferent.getValue()+" ";
							}
							double maxBetweenness = profile.getMaxBetweenness();
							double density = profile.density();
							double endo = MathUtils.round(profile.getStatistics(Indicator.LOOPS, Mode.NORMALIZED),2);
							double endoExp = MathUtils.round(profile.getStatistics(Indicator.LOOPS, Mode.DIVERGENCE_NORMALIZED),2);
							double conc = MathUtils.round(profile.getStatistics(Indicator.CONCENTRATION, Mode.SIMPLE),2);
							double concExp = MathUtils.round(profile.getStatistics(Indicator.CONCENTRATION, Mode.DIVERGENCE),2);
							double sym = MathUtils.round(profile.getStatistics(Indicator.SYMMETRY, Mode.SIMPLE),2);
							double symExp = MathUtils.round(profile.getStatistics(Indicator.SYMMETRY, Mode.DIVERGENCE),2);

							overallReport.outputs().appendln(gender+"\t"+density+"\t"+endo+"\t"+endoExp+"\t"+conc+"\t"+concExp+"\t"+sym+"\t"+symExp+"\t"+centralReferents +"("+maxBetweenness+") betweenness centrality");
						}
						overallReport.outputs().appendln();

					}
					
					if (spaceTimeCriteria.getNetworkTitles().contains("Sequence Type Network")){

						CorrelationMatrix subSequenceMatrix = census.getSubSequenceMatrix(relationClassificationType.toString());
						
						if (subSequenceMatrix!=null){
							charts.add(subSequenceMatrix.getRamificationChart());
						}
					}
					
					
//					reportSequencePartition(overallReport,"",census.getEventPartition(eventTypeName),census.getEventPairPartition(eventTypeName),census.nrSequences(),census.nrEvents());
//					reportSequenceTree(treeReport,"",census.getSequenceTypeNetwork(eventTypeName));
					
	/*				int maxPositions = census.getNrValues(eventType,eventType.toString());
							
					ReportChart diversityChart = StatisticsReporter.createDiversityChart(census.getDepthPartition(eventType.toString()), maxPositions);
					charts.add(diversityChart);*/
/*				}
			}
			
			// Manage the number of chart by line.
			for (int chartIndex = 0; chartIndex < charts.size(); chartIndex++) {
				diagramReport.outputs().append(charts.get(chartIndex));
				if (chartIndex % 4 == 3) {
					diagramReport.outputs().appendln();
				}
			}
			
			// Add chart tables.
			for (ReportTable table : tables) {
				diagramReport.outputs().appendln(table.getTitle());
				diagramReport.outputs().appendln(table);
			}
			
			// Finalize reports
			result.outputs().append(overallReport);
			result.outputs().append(diagramReport);
			result.outputs().append(detailReport);
			
			if (censusType == CensusType.EGONETWORKS || censusType ==CensusType.SEQUENCENETWORKS){
				result.outputs().append(componentReport);
			}
			if (censusType == CensusType.PARCOURS) {
				result.outputs().append(treeReport);
			}
			
			//addPajekData
			
			Map<String,StringList> pajekBuffers = census.getPajekBuffers();
			
			for (String title : pajekBuffers.keySet()){
				
				StringList pajekBuffer = pajekBuffers.get(title);
				if (pajekBuffer.length() != 0) {
					File targetFile = ToolBox.setExtension(ToolBox.addToName(new File(spaceTimeCriteria.getRelationModelName()), "-"+title), ".paj");
					ReportRawData rawData = new ReportRawData("Export "+title+"s to Pajek", "Pajek", "paj", targetFile);
					rawData.setData(PAJFile.convertToMicrosoftEndOfLine(pajekBuffer.toString()));

					result.outputs().appendln();
					result.outputs().append(rawData);
				}
			}

						
			
			result.setTimeSpent(chrono.stop().interval());

		}
			

		
		//
		return result;
	}


	public static Report reportGeneralSequenceCensus(final Sequence<Relations> slices, final StatisticsCriteria criteria, final SequenceCriteria spaceTimeCriteria) throws PuckException{
				Report result;
				
				if ((slices == null) || (criteria == null)) {
					throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
				} else {
					result = new Report("Development Cycles "+spaceTimeCriteria.getRelationModelName());
					Chronometer chrono = new Chronometer();
		
					result.setOrigin("Space reporter");
					
					SequenceCriteria censusCriteria = new SequenceCriteria();
					
					censusCriteria.setCensusType(CensusType.PARCOURS);
					censusCriteria.addNetworkTitle("Event Type Network");
					censusCriteria.getMainRelationClassificationTypes().add(RelationClassificationType.TREES);
					censusCriteria.getEventTypes().add(RelationClassificationType.TREES);
					
					for (RelationClassificationType relationClassificationType : censusCriteria.getMainRelationClassificationTypes()){
						censusCriteria.getCensusOperationLabels().add("PROFILE_"+relationClassificationType);
						censusCriteria.getCensusOperationLabels().add("SUPPORT_"+relationClassificationType);
					}
		
					EgoSequenceStatistics census = new EgoSequenceStatistics(slices.toSequencesByIdValue(), spaceTimeCriteria);
					CensusType censusType = spaceTimeCriteria.getCensusType();
					
					// Create Reports
					Report overallReport = new Report("Survey");
					Report diagramReport = new Report("Diagrams");
					Report detailReport = new Report("Details");
					Report componentReport = new Report("Components");
					Report treeReport = new Report("Trees");
							
					// Create Partition charts and tables
					List<ReportChart> charts = new ArrayList<ReportChart>();
					List<ReportTable> tables = new ArrayList<ReportTable>();
						
					// Make overall report and diagrams
					overallReport.outputs().appendln("Measure\tAverage (Male)\tAverage Pos. (Male)\tMedian (Male)\tMaximum (Male)\tSum (Male)\tAverage (Female)\tAverage Pos. (Female)\tMedian (Female)\tMaximum (Female)\tSum (Female)\tAverage (All)\tAverage Pos. (All)\tMedian (All)\tMaximum (All)\tSum (All)");
		
					// Set partition criteria 
					for (String label : spaceTimeCriteria.getCensusOperationLabels()){
						
						PartitionCriteria partitionCriteria = new PartitionCriteria(label);
						
						ReportChart chart = null;
						
						if (!label.contains("ALTERS") && !label.contains("PROFILE")){
							NumberedValues values = census.getValues(label);
							
							Partition<Relation> partition = PartitionMaker.create(label, slices.getStation(slices.getFirstTime()), values, partitionCriteria);
						
							PartitionCriteria splitCriteria = new PartitionCriteria(spaceTimeCriteria.getPartitionLabel());
							chart = StatisticsReporter.createPartitionChart(partition, partitionCriteria, null);
		
							overallReport.outputs().append(label+"\t");
								String sum = "";
								if (label.startsWith("NR")){
									sum = new Double(values.sum()).intValue()+"";
								}
								overallReport.outputs().append(MathUtils.round(values.average(),2)+"\t"+MathUtils.round(values.averagePositives(),2)+"\t"+values.median()+"\t"+values.max()+"\t"+sum+"\t");
							overallReport.outputs().appendln();
							
						}
					
						if (chart != null) {
							charts.add(chart);
								ReportTable table = ReportTable.transpose(chart.createReportTableWithSum());
								tables.add(table);
								if (!label.contains("EVENTS_") && !label.contains("RELATIONS")){
									tables.add(ReportTable.normalize(table));
								}
		
						}
					
					}
					overallReport.outputs().appendln();
					
					// Create detailed report
					detailReport.outputs().append("Nr\tEgo\tGender");
					for (String partitionLabel : spaceTimeCriteria.getCensusOperationLabels()){
							detailReport.outputs().append("\t"+partitionLabel);
					}
					
					detailReport.outputs().appendln();
					for (Sequence<Numberable> sequence: slices.toSequencesByIdValue()){
						
	//					Individual ego = sequence.getEgo();
						
						if ((((censusType==CensusType.GENERAL) || (censusType==CensusType.PARCOURS)))) {
							detailReport.outputs().append(ego.getLongId()+"\t"+ego);
							for (String label : spaceTimeCriteria.getCensusOperationLabels()){
								if (label.contains("SIMILARITY")){
									Value value = census.getValues(label).get(ego.getLongId());
									Map<Value,Double[]> indiSimilaritiesMap = (Map<Value,Double[]>)value.mapValue();
									String[] keys = new String[]{"PARENT","CHILD","SIBLING","SPOUSE"};
									for (String key : keys){
										Double[] sim = indiSimilaritiesMap.get(new Value(key));
										if (sim!=null){
											detailReport.outputs().append("\t"+MathUtils.round(sim[4], 2));
										}
									}
								} else {
									detailReport.outputs().append("\t"+census.getValues(label).get(ego.getLongId()));
								}
							}
							detailReport.outputs().appendln();
						}
						
					}
					
					
					// SequenceAnalysis
					
					if (censusType == CensusType.PARCOURS){
						
						for (RelationClassificationType relationClassificationType : spaceTimeCriteria.getMainRelationClassificationTypes()){
							
							if (spaceTimeCriteria.getNetworkTitles().contains("Event Type Network")){
		
								CorrelationMatrix eventSequenceMatrix = census.getEventSequenceMatrix(relationClassificationType.toString());
								
								if (eventSequenceMatrix!=null){
									eventSequenceMatrix.setUngendered(true);
									for (ReportChart chart : eventSequenceMatrix.getCharts()){
										charts.add(chart);
									}
									tables.add(eventSequenceMatrix.getTable("Event Type Sequences"));
								}
								
								overallReport.outputs().appendln();
								overallReport.outputs().appendln("Sequence Network Statistics "+relationClassificationType);
								overallReport.outputs().appendln("\tDensity\tInertia\t(Divergence)\tConcentration\t(Divergence)\tSymmetry\t(Divergence)\tCentral nodes");
								
								for (Gender gender : Gender.values()){
									GraphProfile<Cluster<String>> profile = eventSequenceMatrix.getProfile(gender);
		
									String centralReferents = "";
									for (Cluster<String> centralReferent : profile.getCentralReferents()){
										centralReferents+=centralReferent.getValue()+" ";
									}
									double maxBetweenness = profile.getMaxBetweenness();
									double density = profile.density();
									double endo = MathUtils.round(profile.getStatistics(Indicator.LOOPS, Mode.NORMALIZED),2);
									double endoExp = MathUtils.round(profile.getStatistics(Indicator.LOOPS, Mode.DIVERGENCE_NORMALIZED),2);
									double conc = MathUtils.round(profile.getStatistics(Indicator.CONCENTRATION, Mode.SIMPLE),2);
									double concExp = MathUtils.round(profile.getStatistics(Indicator.CONCENTRATION, Mode.DIVERGENCE),2);
									double sym = MathUtils.round(profile.getStatistics(Indicator.SYMMETRY, Mode.SIMPLE),2);
									double symExp = MathUtils.round(profile.getStatistics(Indicator.SYMMETRY, Mode.DIVERGENCE),2);
		
									overallReport.outputs().appendln(gender+"\t"+density+"\t"+endo+"\t"+endoExp+"\t"+conc+"\t"+concExp+"\t"+sym+"\t"+symExp+"\t"+centralReferents +"("+maxBetweenness+") betweenness centrality");
								}
								overallReport.outputs().appendln();
		
							}
							
							if (spaceTimeCriteria.getNetworkTitles().contains("Sequence Type Network")){
		
								CorrelationMatrix subSequenceMatrix = census.getSubSequenceMatrix(relationClassificationType.toString());
								
								if (subSequenceMatrix!=null){
									charts.add(subSequenceMatrix.getRamificationChart());
								}
							}
							
							
		//					reportSequencePartition(overallReport,"",census.getEventPartition(eventTypeName),census.getEventPairPartition(eventTypeName),census.nrSequences(),census.nrEvents());
		//					reportSequenceTree(treeReport,"",census.getSequenceTypeNetwork(eventTypeName));
							
			/*				int maxPositions = census.getNrValues(eventType,eventType.toString());
									
							ReportChart diversityChart = StatisticsReporter.createDiversityChart(census.getDepthPartition(eventType.toString()), maxPositions);
							charts.add(diversityChart);*/
/*						}
					}
					
					// Manage the number of chart by line.
					for (int chartIndex = 0; chartIndex < charts.size(); chartIndex++) {
						diagramReport.outputs().append(charts.get(chartIndex));
						if (chartIndex % 4 == 3) {
							diagramReport.outputs().appendln();
						}
					}
					
					// Add chart tables.
					for (ReportTable table : tables) {
						diagramReport.outputs().appendln(table.getTitle());
						diagramReport.outputs().appendln(table);
					}
					
					// Finalize reports
					result.outputs().append(overallReport);
					result.outputs().append(diagramReport);
					result.outputs().append(detailReport);
					
					if (censusType == CensusType.EGONETWORKS || censusType ==CensusType.SEQUENCENETWORKS){
						result.outputs().append(componentReport);
					}
					if (censusType == CensusType.PARCOURS) {
						result.outputs().append(treeReport);
					}
					
					//addPajekData
					
					Map<String,StringList> pajekBuffers = census.getPajekBuffers();
					
					for (String title : pajekBuffers.keySet()){
						
						StringList pajekBuffer = pajekBuffers.get(title);
						if (pajekBuffer.length() != 0) {
							File targetFile = ToolBox.setExtension(ToolBox.addToName(new File(spaceTimeCriteria.getRelationModelName()), "-"+title), ".paj");
							ReportRawData rawData = new ReportRawData("Export "+title+"s to Pajek", "Pajek", "paj", targetFile);
							rawData.setData(PAJFile.convertToMicrosoftEndOfLine(pajekBuffer.toString()));
		
							result.outputs().appendln();
							result.outputs().append(rawData);
						}
					}
		
								
					
					result.setTimeSpent(chrono.stop().interval());
		
				}
					
		
				
				//
				return result;
			}*/
	
	private static <S extends Sequenceable<E>,E extends Numberable> void reportDiagramsValueSequences (final ChartReport report, final PartitionSequence<E> partitionSequence, final PartitionCriteria partitionCriteria, final PartitionCriteria splitCriteria, final String label, final Geography geography) throws PuckException {

		Sequence<ReportChart> chartSequence = new Sequence<ReportChart>();
		
		for (Ordinal time : partitionSequence.getTimes()){
			
			Partition<E> partition = partitionSequence.getStation(time);
			Partition<E> regroupedPartition = PartitionMaker.create(partition, partitionCriteria);

			ReportChart chart = StatisticsReporter.createPartitionChart(regroupedPartition, partitionCriteria, splitCriteria, geography);
			chartSequence.put(time,chart);
		}
		//
		report.addChartsWithTables(chartSequence, label);
	}
	
	private static <S extends Sequenceable<E>,E extends Numberable, V extends Numberable> ChartReport reportDiagramsValueSequences (final String title, final SequenceStatistics<S,E> sequenceStatistics, final SequenceCriteria sequenceCriteria, final Segmentation segmentation, final int width) throws PuckException {
		ChartReport result;
		
		result = new ChartReport(title);
		
		for (PartitionCriteria partitionCriteria : sequenceCriteria.getValueSequenceCriteriaList()){
			
			String label = partitionCriteria.getLabel();
			
			if (partitionCriteria.isWithDiagram()){
								
				PartitionSequence<V> partitionSequence = null;
				
				if (sequenceStatistics.getAggregatePartitionSequence(label)!=null){
					
					if (label.equals("AGE")) {
						partitionCriteria.setSizedGrouping(0., 20., null);
					}
					
					partitionSequence = (PartitionSequence<V>)sequenceStatistics.getAggregatePartitionSequence(label).unpack(partitionCriteria.getValueCode());
					label = label+" TOTAL";

				} else if (sequenceStatistics.getElementPartitionSequence(label)!=null){
						
					partitionSequence = (PartitionSequence<V>)sequenceStatistics.getElementPartitionSequence(label);
					label = label+" ELEMENTS";

				} else {

					partitionSequence = (PartitionSequence<V>)sequenceStatistics.getPartitionSequence(label).unpack(partitionCriteria.getValueCode());
				}
				
				PartitionCriteria splitCriteria = null;
				if (partitionSequence.hasIndividualizableItems()){
					splitCriteria = new PartitionCriteria("GENDER");
				}
				reportDiagramsValueSequences (result, partitionSequence, partitionCriteria, splitCriteria, label, segmentation.getGeography());
			}
			
			if (partitionCriteria.isWithFlow()){
				
				PartitionCriteria splitCriteria = new PartitionCriteria("GENDER");
				Map<String,PartitionSequence<Individual>> partitionSequences = sequenceStatistics.getMigrations(sequenceCriteria,partitionCriteria);

				for (String key : partitionSequences.keySet()){
					
					reportDiagramsValueSequences(result,partitionSequences.get(key),partitionCriteria.cloneChangeLabel(key+"_"+label), splitCriteria,key+"_"+label, sequenceCriteria.getGeography());
					
				}
			}
		}
		
		logger.debug("Diagrams (dated) created for "+sequenceStatistics);

		//
		return result;
		
	}
	
	private static <S extends Sequenceable<E>,E extends Numberable> ChartReport reportDiagramsSequenceValues (final String title, final SequenceStatistics<S,E> sequenceStatistics, final SequenceCriteria sequenceCriteria, final Segmentation segmentation, final int width) throws PuckException{
		ChartReport result;

		result = new ChartReport(title);
		
		for (PartitionCriteria partitionCriteria : sequenceCriteria.getSequenceValueCriteriaList()){

			String label = partitionCriteria.getLabel();

			if (partitionCriteria.isWithDiagram()){
								
				ReportChart chart = null;
				
				if (label.equals("MEAN_NR_MOVES")){
					Map<Value,Double[]> map = sequenceStatistics.getMeanNrMoves();
					chart = StatisticsReporter.createMapChart(map, label, new String[]{"MALE","FEMALE"}, GraphType.STACKED_BARS);
				
				} else {
					
					PartitionCriteria splitCriteria = new PartitionCriteria("GENDER");
					Partition<S> sequencePartition = sequenceStatistics.getSequencePartition(label);
					if (sequencePartition.hasListValues()){
						sequencePartition = PartitionMaker.createPseudoPartition(sequencePartition, partitionCriteria);
					}
					Partition<S> regroupedSequencePartition = PartitionMaker.create(sequencePartition, partitionCriteria);
					
					chart = StatisticsReporter.createPartitionChart(regroupedSequencePartition, partitionCriteria, splitCriteria, sequenceCriteria.getGeography());
									
					if (label.substring(0, 3).equals("AGE")){
						
						if (sequencePartition.maxValue()!=null){
							ReportChart survivalChart = StatisticsReporter.createSurvivalChart((Partition<Individualizable>)sequencePartition, splitCriteria, sequenceCriteria.getGeography());
							result.addChartWithTables(survivalChart,label+"_Survival");
						} else {
							System.err.println(label+" no max value");
						}
					}
				}
				//
				result.addChartWithTables(chart,label);
			}
			
			// Similarity chart
			
			if (label.contains("SIMILARITY")) {
				
				ValueSequenceLabel relationClassificationType = ValueSequenceLabel.valueOf(label.substring(label.lastIndexOf("_") + 1));
				Map<Value, Double[]> similaritiesMap = sequenceStatistics.getSimilaritiesMap(relationClassificationType);
				ReportChart chart = StatisticsReporter.createMapChart(similaritiesMap, label, new String[] { "HH", "FH", "HF", "FF", "All" }, GraphType.LINES);
				result.addChartWithTables(chart,label);
			} 
		}
		
		// Sequence network diagrams
		
		for (String label : sequenceStatistics.getSequenceNetworkLabels()){
			
			SequenceNetworkStatistics<S,E> eventSequenceMatrix = sequenceStatistics.getEventSequenceMatrix(label);

			if (eventSequenceMatrix != null) {
				for (ReportChart chart : eventSequenceMatrix.getCharts()) {//
					result.addChart(chart);
				}
				result.addTable(eventSequenceMatrix.getTable("Event Type Sequences"));

/*		        if (sequenceCriteria.getTrajectoriesOperations().contains(TrajectoriesOperation.DRAW)){
		            
		        	Graph<Cluster<String>> drawGraph = eventSequenceMatrix.getSequenceNetwork("PLACE", Gender.UNKNOWN);
		            Graph<String> placeNameGraph = GeoNetworkUtils.createGeoNetwork2(drawGraph, sequenceCriteria.getLevel());
		            GeocodingWorker.geocodeGraph(segmentation.getGeography(), placeNameGraph);
		        }*/
			}
		}

		for (String label : sequenceStatistics.getSequenceTreeLabels()){
			
			SequenceNetworkStatistics<S,E> subSequenceMatrix = sequenceStatistics.getSubSequenceMatrix(label);

			if (subSequenceMatrix != null) {
				result.addChart(subSequenceMatrix.getRamificationChart());
			}
			
		}
		
		// Component charts
		
		for (String networkTitle : sequenceCriteria.getNetworkTitles()) {
			
			createComponentCharts(result, networkTitle, sequenceStatistics, sequenceCriteria);
		}

		
		logger.debug("Diagrams created for "+sequenceStatistics);

		//
		return result;
	}
	
	

	
	private static <V extends Numberable> void reportCensus (final Report result, PartitionSequence<V> partitionSequence, final String indicator, final ValueCode valueCode, final SequenceCriteria criteria) throws PuckException{

		partitionSequence = partitionSequence.unpack(valueCode);
		
		Partition<V> valueSequencePartition = partitionSequence.getValueSequencePartition();
		PartitionCriteria splitPartitionCriteria = new PartitionCriteria(criteria.getIndividualPartitionLabel());
		Partition<V> allSplitPartition = PartitionMaker.create(partitionSequence, splitPartitionCriteria, criteria.getGeography());
		
		result.outputs().appendln(indicator+" Census");
		result.outputs().appendln();
		
		String dimensionHead = "";
		
		if (indicator.contains("TREES")){
			
			dimensionHead = "Depth\tWidth\t";
		}
		
		String headLine0 = "Position\t"+dimensionHead+"MeanSize\tMeanShare\t";
		
		for (Integer year : criteria.getDates()){
			
			headLine0 += year+" ALL\t%\tList\t";
			
			if (!allSplitPartition.getValues().isEmpty()){
				
				for (Value splitValue : allSplitPartition.getValuesAsSortedList()){
					
					if (splitValue!=null){
						
						headLine0 += year+" "+splitValue+"\t%\tList\t";
					}
				}
			}
		}
		//
		result.outputs().appendln(headLine0);

		for (Value value : partitionSequence.getValuesAsSortedList()){
			
			String valueLine ="";
			
			double meanClusterSize = 0.;
			double meanClusterShare = 0.;
			double times = partitionSequence.getTimes().size();
			
			for (Ordinal time : partitionSequence.getTimes()){
				
				int clusterSize = partitionSequence.getClusterSize(time, value);
				double clusterShare = partitionSequence.getClusterShare(time, value);
				
				meanClusterSize += clusterSize;
				meanClusterShare += clusterShare;
				
				valueLine += "\t"+clusterSize+"\t"+clusterShare+"\t"+partitionSequence.getClusterItemsAsString(time, value);
				Partition<V> splitPartition = PartitionMaker.create(value+"_"+criteria.getIndividualPartitionLabel(), partitionSequence.getCluster(time, value), splitPartitionCriteria, criteria.getGeography());
				
				for (Value splitValue : allSplitPartition.getValuesAsSortedList()){

					if (splitValue!=null){
						
						Cluster<V> splitCluster = null;
						Cluster<V> allSplitCluster = allSplitPartition.getCluster(splitValue);
						
						if (splitPartition != null){

							splitCluster = splitPartition.getCluster(splitValue);
						}
						
						if (splitCluster==null){

							valueLine += "\t0\t0.0\t";
							
						} else {

							valueLine += "\t"+splitCluster.size()+"\t"+MathUtils.percent(splitCluster.count(), allSplitCluster.count())+"\t"+splitCluster.getItemsAsString();
						}
					}
				}
			}
			
			meanClusterSize = MathUtils.round(meanClusterSize/times,2);
			meanClusterShare = MathUtils.round(meanClusterShare/times,2);
			
			String valueString = value.toString();
			
			if (indicator.contains("TREES")){
				
				if (valueString.charAt(0)=='['){
					
					valueString = valueString.substring(1, valueString.length()-1); 
				}
				
				int[] treeDimensions = RelationWorker.getTreeDimensions(valueString);
				
				valueString = valueString+"\t"+treeDimensions[0]+"\t"+treeDimensions[1];
			}

			valueLine =  valueString+"\t"+meanClusterSize+"\t"+meanClusterShare+valueLine;
			
			result.outputs().appendln(valueLine);
		}
		result.outputs().appendln();

		result.outputs().appendln(indicator+" Sequence Census");
		result.outputs().appendln();
		result.outputs().appendln("Sequence");
		
		int sequenceCount = valueSequencePartition.itemsCount();
		
		for (Cluster<V> cluster : valueSequencePartition.getClusters().toListSortedByValue()){
			result.outputs().appendln(cluster.getValue()+"\t"+cluster.size()+"\t"+MathUtils.percent(cluster.size(), sequenceCount));
		}
		result.outputs().appendln();
	}

	private static <S extends Sequenceable<E>,E> void writeMatrix (Report report, Matrix matrix, String matrixLabel, String graphLabel, StringList pajekBuffer, Map<Value,Double> meanValueFrequencies) throws PuckException {

		report.outputs().appendln(matrixLabel);
		report.outputs().appendln();
			
		String headLine = "\t";
		for (int col=0;col<matrix.getRowDim();col++){
			headLine += matrix.getColLabel(col)+"\t";
		}
		report.outputs().appendln(headLine);
			
		for (int row=0;row<matrix.getRowDim();row++){
			String rowLine = matrix.getRowLabel(row)+"\t";
			for (int col=0;col<matrix.getRowDim();col++){
				rowLine += matrix.getAsRowPercentage(row, col)+"\t";
			}
			report.outputs().appendln(rowLine);
		}
		report.outputs().appendln();
		
		// Transition graph
		
		if (pajekBuffer != null){
			
			Graph<Node<String>> transitionGraph = GraphMaker.createGraph(graphLabel, matrix, meanValueFrequencies, "MEAN");
			List<String> partitionLabels = new ArrayList<String>();
			partitionLabels.add("MEAN");

			// Avoid that zero node count cuts pajek file import
			if (transitionGraph.nodeCount()>0){
				pajekBuffer.addAll(PuckUtils.writePajekNetwork(transitionGraph, partitionLabels));
				pajekBuffer.appendln();
				report.outputs().append(GraphReporter.reportStrengthsByTags(transitionGraph));
				report.outputs().appendln();
			}
		}
	}

	
	private static <S extends Sequenceable<E>,E> void reportMatrix (Report report, PartitionSequence<S> partitionSequence, String indicator, final ValueCode valueCode, StringList pajekBuffer, final SequenceCriteria criteria) throws PuckException {
		
		// Optionalize
		partitionSequence = partitionSequence.unpack(valueCode);
		
		Matrix matrix = partitionSequence.getTransitionMatrix();
		Map<Value,Double> meanValueFrequencies = partitionSequence.getMeanValueFrequencies();

		String matrixLabel = "Transition Matrix "+indicator;
		String graphLabel = "Transition Graph "+criteria.getRelationModelName()+" "+indicator;
		
		writeMatrix(report, matrix, matrixLabel, graphLabel, pajekBuffer, meanValueFrequencies);
		
		Map<Value,PartitionSequence<S>> splitPartitionSequences = partitionSequence.split(new PartitionCriteria(criteria.getIndividualPartitionLabel()), criteria.getGeography());
		List<Value> splitValues = new ArrayList<Value>(splitPartitionSequences.keySet());
		Collections.sort(splitValues);
		
		for (Value splitValue : splitValues){

			PartitionSequence<S> splitPartitionSequence = splitPartitionSequences.get(splitValue);
			
			Matrix splitMatrix = splitPartitionSequence.getTransitionMatrix();
			Map<Value,Double> splitMeanValueFrequencies = splitPartitionSequence.getMeanValueFrequencies();

			String splitMatrixLabel = "Transition Matrix "+indicator+" "+splitValue;
			String splitGraphLabel = "Transition Graph "+criteria.getRelationModelName()+" "+indicator+" "+splitValue;
			
			writeMatrix(report, splitMatrix, splitMatrixLabel, splitGraphLabel, pajekBuffer, splitMeanValueFrequencies);
		}
		
/*		report.outputs().appendln("Transition Matrix "+indicator);
		report.outputs().appendln();
			
		String headLine = "\t";
		for (int col=0;col<matrix.getRowDim();col++){
			headLine += matrix.getColLabel(col)+"\t";
		}
		report.outputs().appendln(headLine);
			
		for (int row=0;row<matrix.getRowDim();row++){
			String rowLine = matrix.getRowLabel(row)+"\t";
			for (int col=0;col<matrix.getRowDim();col++){
				rowLine += matrix.getAsRowPercentage(row, col)+"\t";
			}
			report.outputs().appendln(rowLine);
		}
		report.outputs().appendln();
		
		// Transition graph
		
		if (pajekBuffer != null){
			
			Graph<Node<String>> transitionGraph = GraphMaker.createGraph("Transition Graph "+criteria.getRelationModelName()+" "+indicator, matrix, meanValueFrequencies, "MEAN");
			List<String> partitionLabels = new ArrayList<String>();
			partitionLabels.add("MEAN");
			pajekBuffer.addAll(PuckUtils.writePajekNetwork(transitionGraph, partitionLabels));
			pajekBuffer.appendln();
			
			report.outputs().append(GraphReporter.reportStrengthsByTags(transitionGraph));
			report.outputs().appendln();

		}*/
			
	}
	
	private static <S extends Sequenceable<E>,E extends Numberable> Report reportFlows(final String title, final SequenceStatistics<S,E> sequenceStatistics, final SequenceCriteria sequenceCriteria){
		Report result;
		
		result = new Report(title);
		
		Partition<String> flows = new Partition<String>();
		flows.add(sequenceStatistics.getFlows("IN",sequenceCriteria));
		flows.add(sequenceStatistics.getFlows("OUT",sequenceCriteria));

		for (Cluster<String> cluster : flows.getClusters().toListSortedByValue()){
			result.outputs().appendln(cluster.getValue());
			for (String item : cluster.getItems()){
				result.outputs().appendln("\t"+item);
			}
		}
		result.outputs().appendln();
		
		logger.debug("Flow report created for "+sequenceStatistics);

		//
		return result;
	}

	private static <S extends Sequenceable<E>, E extends Numberable> Report reportSurvey (final String title, final SequenceStatistics<S,E> sequenceStatistics, final SequenceCriteria sequenceCriteria)  {
		Report result;
		
		result = new Report(title);
		
		String headLine = "INDICATOR";
		List<String> genderLabels = Arrays.asList(new String[]{"MALE","FEMALE","ALL"});
		List<String> measureLabels = Arrays.asList(new String[]{"AVERAGE","AVERAGE_POSITIVE","MEDIAN","MAX","SUM"});
		
		for (String genderLabel : genderLabels){
			for (String measureLabel : measureLabels){
				headLine += "\t"+measureLabel+" ("+genderLabel+")";
			}
		}
		result.outputs().appendln(headLine);
		
		// "Measure\tAverage (Male)\tAverage Pos. (Male)\tMedian (Male)\tMaximum (Male)\tSum (Male)\tAverage (Female)\tAverage Pos. (Female)\tMedian (Female)\tMaximum (Female)\tSum (Female)\tAverage (All)\tAverage Pos. (All)\tMedian (All)\tMaximum (All)\tSum (All)");

		for (PartitionCriteria partitionCriteria : sequenceCriteria.getSequenceValueCriteriaList()){

			String label = partitionCriteria.getLabel();
			Partition<S> partition = sequenceStatistics.getSequencePartition(label);
			
			// Average numbers
			
			if (partition.isNumeric()){

				try {
					
					NumberedValues[] genderedValues = PuckUtils.getGenderedNumberedValues((Partition<Individualizable>)partition);
					String measureLine = label + "\t";
					
					for (int gender = 0; gender < 3; gender++) {
						
/*					String sum = "";
						if (label.startsWith("NR")) {
							sum = new Double(genderedValues[gender].sum()).intValue() + "";
						}*/
						
						for (String measureLabel : measureLabels){
							measureLine+=genderedValues[gender].getValueAsString(measureLabel)+ "\t";;
						}

						
/*					overallReport.outputs().append(
								MathUtils.round(genderedValues[gender].average(), 2) + "\t" + MathUtils.round(genderedValues[gender].averagePositives(), 2) + "\t"
										+ genderedValues[gender].median() + "\t" + genderedValues[gender].max() + "\t" + sum + "\t");*/
					}
					result.outputs().appendln(measureLine);
					
				} catch (ClassCastException e) {
					
					System.err.println(partition+" is not individualizable");
					continue;
				}
			}
			
			// Similarity report
			
			if (label.contains("SIMILARITY")) {
				
				ValueSequenceLabel relationClassificationType = ValueSequenceLabel.valueOf(label.substring(label.lastIndexOf("_") + 1));
				Map<Value, Double[]> similaritiesMap = sequenceStatistics.getSimilaritiesMap(relationClassificationType);

				for (Value key : similaritiesMap.keySet()) {
					result.outputs().appendln(label + "_" + key + "\t" + MathUtils.percent(similaritiesMap.get(key)[4], 100));
				}
			} 
		}
		result.outputs().appendln();
		
		logger.debug("Survey report created for "+sequenceStatistics);

		
		// Sequence network statistics
		
		for (String label : sequenceStatistics.getSequenceNetworkLabels()){
			
			SequenceNetworkStatistics<S,E> eventSequenceMatrix = sequenceStatistics.getEventSequenceMatrix(label);
			
			result.outputs().appendln();
			result.outputs().appendln("Sequence Network Statistics " + label);
			result.outputs().appendln("\tDensity\tInertia\t(Divergence)\tConcentration\t(Divergence)\tSymmetry\t(Divergence)\tCentral nodes");

			for (Gender gender : Gender.values()) {
				GraphProfile<Cluster<String>> profile = eventSequenceMatrix.getProfile(gender);

				String centralReferents = "";
				for (Cluster<String> centralReferent : profile.getCentralReferents()) {
					centralReferents += centralReferent.getValue() + " ";
				}
				double maxBetweenness = profile.getMaxBetweenness();
				double density = profile.density();
				double endo = MathUtils.round(profile.getStatistics(Indicator.LOOPS, Mode.NORMALIZED), 2);
				double endoExp = MathUtils.round(profile.getStatistics(Indicator.LOOPS, Mode.DIVERGENCE_NORMALIZED), 2);
				double conc = MathUtils.round(profile.getStatistics(Indicator.CONCENTRATION, Mode.SIMPLE), 2);
				double concExp = MathUtils.round(profile.getStatistics(Indicator.CONCENTRATION, Mode.DIVERGENCE), 2);
				double sym = MathUtils.round(profile.getStatistics(Indicator.SYMMETRY, Mode.SIMPLE), 2);
				double symExp = MathUtils.round(profile.getStatistics(Indicator.SYMMETRY, Mode.DIVERGENCE), 2);

				result.outputs().appendln(
						gender + "\t" + density + "\t" + endo + "\t" + endoExp + "\t" + conc + "\t" + concExp + "\t" + sym + "\t" + symExp + "\t"
								+ centralReferents + "(" + maxBetweenness + ") betweenness centrality");
			}
			result.outputs().appendln();
		}
		result.outputs().appendln();
		
		// Parcours network statistics
				
		Map<String,Map<String,Map<String,Value>>> parcoursNetworkStatistics = sequenceStatistics.getParcoursNetworkStatistics();
		
		if (parcoursNetworkStatistics!=null){
			result.outputs().appendln("Sequence Network Statistics");

			String pnHeadline = null;
			boolean first = true;
			
			List<String> clusterValues = new ArrayList<String>(parcoursNetworkStatistics.keySet());

			for (String nodeLabel : parcoursNetworkStatistics.get("Total").keySet()){
				String pnLine = nodeLabel;
				pnHeadline = "Node";
				
				for (String genderLabel : clusterValues){
					Map<String,Value> statistics = parcoursNetworkStatistics.get(genderLabel).get(nodeLabel);
					for (String statisticsLabel : sequenceCriteria.getNodeStatisticsLabels()){
						if (first){
							pnHeadline+="\t"+statisticsLabel+" ("+genderLabel+")";
						}
						if (statistics!=null){
							pnLine+="\t"+statistics.get(statisticsLabel);
						} else {
							pnLine+="\t";
						}
					}
				}
				if (first){
					result.outputs().appendln(pnHeadline);
					first = false;
				}
				result.outputs().appendln(pnLine);
			}
		}
		
		logger.debug("Sequence network statistis created for "+sequenceStatistics);

		//
		return result;
	}
	
	private static <S extends Sequenceable<E>,E extends Numberable> Report reportDetailsValueSequences (final String title, final SequenceStatistics<S,E> sequenceStatistics, final SequenceCriteria sequenceCriteria, final List<String> labels, final Map<String, StringList> pajekBuffers) throws PuckException {
		Report result;
		
		result = new Report(title);
		
		List<String> constantLabels = Arrays.asList(new String[]{"ID","NAME","GENDER"});	
		PartitionCriteriaList valueSequenceCriteriaList = sequenceCriteria.getValueSequenceCriteriaList();
		
		StringList pajekBuffer = pajekBuffers.get("Referent Networks");
		if (pajekBuffer == null){
			pajekBuffer = new StringList();
			pajekBuffers.put("Referent Networks",pajekBuffer);
		}
		
		String headLine = "";
		String dateLine = "";
		
		for (String constantLabel : constantLabels){
			
			headLine += constantLabel+"\t";
			dateLine += "\t";
		}

		for (String label : labels){
			
			headLine += label;

			for (Integer year : sequenceCriteria.getDates()){
				
				headLine += "\t";
				dateLine += year+"\t";
			}
						
			if (sequenceStatistics.getPartitionSequence(label)!=null && sequenceStatistics.getPartitionSequence(label).isNumeric()){
				headLine += "\t";
				dateLine += "TREND\t";
			}
		}
		
		result.outputs().appendln(headLine);
		result.outputs().appendln(dateLine);
				
		for (S sequence : sequenceStatistics.sequences.toSortedList()){
			
			String memberLine = "";
			
			for (String constantLabel : constantLabels){
				
				memberLine += SequenceValuator.get(sequence,constantLabel,sequenceCriteria)+"\t";
			}
			
			for (String label : labels){
				
				for (Integer year : sequenceCriteria.getDates()){
			
					Ordinal time = new Ordinal(year);
					Value value = sequenceStatistics.getValue(sequence, time, label);
					
					if (value == null){
						
						memberLine += "\t";
						
					} else if (value.isGraph()){
						
						memberLine += "\t";
						
						pajekBuffer.addAll(PuckUtils.writePajekNetwork((Graph)value.graphValue(), new ArrayList<String>()));
						pajekBuffer.appendln();

					} else {
						
						ValueCode valueCode = null;
						if (valueSequenceCriteriaList != null && valueSequenceCriteriaList.getByLabel(label)!= null){
							valueCode = valueSequenceCriteriaList.getByLabel(label).getValueCode();
						}
						
						memberLine += value.unpack(valueCode)+"\t";
					}
					
				}

				if (sequenceStatistics.getPartitionSequence(label)!=null && sequenceStatistics.getPartitionSequence(label).isNumeric()){
					memberLine += sequenceStatistics.getTrend(sequence, label)+"\t";
				}

			}

			result.outputs().appendln(memberLine);
		}
		
		String endLine = "TOTAL\t";
		
		for (String constantLabel : constantLabels){
			
			endLine += "\t";
		}
		
		for (String label : labels){
			
			for (Ordinal time : Ordinal.getOrdinals(sequenceCriteria.getDates())){
				
				Double mean = null;
				Partition<S> partition = sequenceStatistics.getStation(time, label);
				
				if (partition != null){
					
					mean = partition.valueMeanByItems();
				}
				
				if (mean == null) {
					
					endLine += "\t";
					
				} else {
					
					endLine += mean+"\t";
				}
			}
			
			endLine += sequenceStatistics.getMeanTrend(label)+"\t";
		}
		result.outputs().appendln(endLine);

		logger.debug("Details report (dated) created for "+sequenceStatistics);

		//
		return result;
	}
	
	private static <S extends Sequenceable<E>,E extends Numberable> Report reportCensuses (final String title, final SequenceStatistics<S,E> sequenceStatistics, final SequenceCriteria sequenceCriteria) throws PuckException {
		Report result;
		
		result = new Report(title);
		
		
		for (PartitionCriteria valueSequenceCriteria : sequenceCriteria.getValueSequenceCriteriaList()){
			
			String label = valueSequenceCriteria.getLabel();
			ValueCode valueCode = valueSequenceCriteria.getValueCode();
			
			if (valueSequenceCriteria.isWithCensus()){
				
				reportCensus(result,sequenceStatistics.getPartitionSequence(label),label,valueCode, sequenceCriteria);
				
				if (sequenceStatistics.getAggregatePartitionSequence(label)!=null){

					reportCensus(result,sequenceStatistics.getAggregatePartitionSequence(label),label+"_TOTAL",null, sequenceCriteria);
				}
				
				if (sequenceStatistics.getElementPartitionSequence(label)!=null){

					reportCensus(result,sequenceStatistics.getElementPartitionSequence(label),label+"_ELEMENTS",null, sequenceCriteria);
				}
			}
		}
		result.outputs().appendln();
		
		logger.debug("Census report created for "+sequenceStatistics);
		
		//
		return result;
	}
		
	private static <S extends Sequenceable<E>,E extends Numberable> Report reportMatrices (final String title, final SequenceStatistics<S,E> sequenceStatistics, final SequenceCriteria sequenceCriteria, final  Map<String, StringList> pajekBuffers) throws PuckException {
		Report result;
		
		result = new Report(title);
		
		StringList pajekBuffer = pajekBuffers.get("Transition Graphs");
		if (pajekBuffer == null){
			pajekBuffer = new StringList();
			pajekBuffers.put("Transition Graphs",pajekBuffer);
		}
		
		for (PartitionCriteria valueSequenceCriteria : sequenceCriteria.getValueSequenceCriteriaList()){
			
			String label = valueSequenceCriteria.getLabel();
			ValueCode valueCode = valueSequenceCriteria.getValueCode();
			
			if (valueSequenceCriteria.isWithMatrix()){
				
				if (valueSequenceCriteria.isWithGraph()){
					
					reportMatrix(result,sequenceStatistics.getPartitionSequence(label),label,valueCode, pajekBuffer,sequenceCriteria);
					
				} else {
					
					reportMatrix(result,sequenceStatistics.getPartitionSequence(label),label,valueCode, null,sequenceCriteria);
				}
			}
		}
		
		logger.debug("Graphs and matrices created for "+sequenceStatistics);
		
		//
		return result;
	}
		
	
	private static <S extends Sequenceable<E>,E extends Numberable> Report reportDetailsSequenceValues (final String title, final SequenceStatistics<S,E> sequenceStatistics, final SequenceCriteria sequenceCriteria, final List<String> labels) throws PuckException {
		Report result;
		
		result = new Report(title);
		
		List<String> constantLabels = Arrays.asList(new String[]{"ID","NAME","GENDER"});	
		PartitionCriteriaList sequenceValueCriteriaList = sequenceCriteria.getSequenceValueCriteriaList();
		
		String headLine = "";
		
		for (String constantLabel : constantLabels){
			
			headLine += constantLabel+"\t";
		}

		for (String label : labels){

			if (label.contains("SIMILARITY")) {
				
				String subLabel = label.substring(label.lastIndexOf("_") + 1);
				headLine += "\tSIMILARITY_PARENT_" + subLabel + "\tSIMILARITY_CHILD_" + subLabel + "\tSIMILARITY_SIBLING_"+ subLabel + "\tSIMILARITY_SPOUSE_" + subLabel;

			} else {

				headLine += label+"\t";
			}
		}
		//
		result.outputs().appendln(headLine);
				
		for (S sequence : sequenceStatistics.sequences.toSortedList()){
			
			String memberLine = "";
			
			for (String constantLabel : constantLabels){
				
				memberLine += SequenceValuator.get(sequence,constantLabel,sequenceCriteria)+"\t";
			}
			
			for (String label : labels){
				
				Value value = sequenceStatistics.getValue(sequence, label);
								
				if (value == null){
					
					memberLine += "\t";
					
				} else if (label.contains("SIMILARITY")) {
					
					Map<Value,Double[]> indiSimilaritiesMap = (Map<Value,Double[]>)value.mapValue();
					String[] keys = new String[]{"PARENT","CHILD","SIBLING","SPOUSE"};
					
					for (String key : keys){
						Double[] sim = indiSimilaritiesMap.get(new Value(key));
						if (sim!=null){
							memberLine+=MathUtils.round(sim[4], 2)+"\t";
						}
					}
//					System.out.println("SIMILARITY "+memberLine);
					
				} else {
					
					ValueCode valueCode = null;
					if (sequenceValueCriteriaList != null && sequenceValueCriteriaList.getByLabel(label)!= null){
						valueCode = sequenceValueCriteriaList.getByLabel(label).getValueCode();
					}
					
					memberLine += value.unpack(valueCode)+"\t";
				}
			}
			//
			result.outputs().appendln(memberLine);
		}
		result.outputs().appendln();
		
		logger.debug("Details report created for "+sequenceStatistics);

		//
		return result;
	}
		

	
	private static <S extends Sequenceable<E>,E extends Numberable> Report reportSequenceAnalysis (final String title, final Segmentation segmentation, final Sequenceables<S,E> sequences, final SequenceCriteria sequenceCriteria, List<Graph> graphs) throws PuckException{
		Report result;
		
		if ((sequences == null) || (segmentation == null) || (sequenceCriteria.getRelationModelName() == null)) {
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
		} else {
			
			result = new Report(title+" "+sequenceCriteria.getRelationModelName());

			Chronometer chrono = new Chronometer();
			
			result.setOrigin("Sequence reporter");
			
			SequenceStatistics<S,E> sequenceStatistics = null;
			List<String> reportTitles = new ArrayList<String>();
			
//			CensusType censusType = spaceTimeCriteria.getCensusType();
			
			if (ArrayUtils.isEmpty(sequenceCriteria.getDates())){
				
				// Flexible intervals (typical for event sequences)
				sequenceStatistics = new SequenceStatistics<S,E>(segmentation, sequences, sequenceCriteria);
				
				reportTitles.addAll(Arrays.asList(new String[]{"SURVEY","DIAGRAMS","DETAILS"}));

			} else {
				
				// Fixed intervals (typical for state sequences)
				sequenceStatistics = new SequenceStatistics<S,E>(segmentation, sequences, sequenceCriteria);
				reportTitles.addAll(Arrays.asList(new String[]{"CENSUS","DIAGRAMS_DATED","DETAILS_DATED","MATRICES","FLOWS"}));
								
//				SequenceStatistics<S,E> census = new SequenceStatistics<S,E>(null, sequences, spaceTimeCriteria);
			}
			
//			if (censusType==CensusType.EGONETWORKS || censusType == CensusType.SEQUENCENETWORKS){
				
			if (sequenceCriteria.getEgoNetworksOperations().contains(EgoNetworksOperation.COHESION) || sequenceCriteria.getParcoursNetworksOperations().contains(EgoNetworksOperation.COHESION)){
				reportTitles.add("COMPONENTS");
			}
			
//			if (censusType == CensusType.PARCOURS){
				
				if (sequenceCriteria.getTrajectoriesOperations().contains(TrajectoriesOperation.LIST_TREES)){
					reportTitles.add("TREES");
				}			
//			}
				
			Map<String, StringList> pajekBuffers = sequenceStatistics.getPajekBuffers();
			Map<String,Report> reports = new HashMap<String,Report>();

			createReports(result, reportTitles,reports,segmentation,sequenceStatistics,sequenceCriteria,pajekBuffers, 3);

			getRData(result,sequenceStatistics,segmentation,sequenceCriteria);
			
			graphs.addAll(sequenceStatistics.getGraphs());

			// SequenceAnalysis
			
/*			if (censusType == CensusType.PARCOURS){
				
				for (RelationClassificationType relationClassificationType : spaceTimeCriteria.getMainRelationClassificationTypes()){
					
					if (spaceTimeCriteria.getNetworkTitles().contains("Event Type Network")){

						CorrelationMatrix eventSequenceMatrix = sequenceStatistics.getEventSequenceMatrix(relationClassificationType.toString());
						
						if (eventSequenceMatrix!=null){
							eventSequenceMatrix.setUngendered(true);
							for (ReportChart chart : eventSequenceMatrix.getCharts()){
								diagramReport.addChart(chart);
							}
							diagramReport.addTable(eventSequenceMatrix.getTable("Event Type Sequences"));
						}
						
						overallReport.outputs().appendln();
						overallReport.outputs().appendln("Sequence Network Statistics "+relationClassificationType);
						overallReport.outputs().appendln("\tDensity\tInertia\t(Divergence)\tConcentration\t(Divergence)\tSymmetry\t(Divergence)\tCentral nodes");
						
						for (Gender gender : Gender.values()){
							GraphProfile<Cluster<String>> profile = eventSequenceMatrix.getProfile(gender);

							String centralReferents = "";
							for (Cluster<String> centralReferent : profile.getCentralReferents()){
								centralReferents+=centralReferent.getValue()+" ";
							}
							double maxBetweenness = profile.getMaxBetweenness();
							double density = profile.density();
							double endo = MathUtils.round(profile.getStatistics(Indicator.LOOPS, Mode.NORMALIZED),2);
							double endoExp = MathUtils.round(profile.getStatistics(Indicator.LOOPS, Mode.DIVERGENCE_NORMALIZED),2);
							double conc = MathUtils.round(profile.getStatistics(Indicator.CONCENTRATION, Mode.SIMPLE),2);
							double concExp = MathUtils.round(profile.getStatistics(Indicator.CONCENTRATION, Mode.DIVERGENCE),2);
							double sym = MathUtils.round(profile.getStatistics(Indicator.SYMMETRY, Mode.SIMPLE),2);
							double symExp = MathUtils.round(profile.getStatistics(Indicator.SYMMETRY, Mode.DIVERGENCE),2);

							overallReport.outputs().appendln(gender+"\t"+density+"\t"+endo+"\t"+endoExp+"\t"+conc+"\t"+concExp+"\t"+sym+"\t"+symExp+"\t"+centralReferents +"("+maxBetweenness+") betweenness centrality");
						}
						overallReport.outputs().appendln();

					}
					
					if (spaceTimeCriteria.getNetworkTitles().contains("Sequence Type Network")){

						CorrelationMatrix subSequenceMatrix = sequenceStatistics.getSubSequenceMatrix(relationClassificationType.toString());
						
						if (subSequenceMatrix!=null){
							diagramReport.addChart(subSequenceMatrix.getRamificationChart());
						}
					}
				}
			}			
			
			

			List<Report> reportSupplements = new ArrayList<Report>();
			
			for (PartitionCriteria valueSequenceCriteria : spaceTimeCriteria.getValueSequenceCriteriaList()){
				
				String label = valueSequenceCriteria.getLabel();
				
				List<?> mapKeys = sequenceStatistics.getPartitionSequence(label).getMapKeys();
				
				if (mapKeys != null){
					
					List<String> fineLabels = new ArrayList<String>();
					
					for (Object key : mapKeys){
						fineLabels.add(label+"$"+key); 
					}
					
					reports.put("DETAILS_"+label, reportDetailsValueSequences("Details_"+label,sequenceStatistics,spaceTimeCriteria,fineLabels,pajekBufferStations));
				}

				ValueCode valueCode = valueSequenceCriteria.getValueCode();
								
				if (label.contains("MIGRATION_")){
					
					PartitionCriteria partitionCriteria = spaceTimeCriteria.getValueSequenceCriteriaByLabel(label.substring(label.indexOf("_")+1));
					PartitionCriteria splitCriteria = new PartitionCriteria("GENDER");
					Map<String,PartitionSequence<Individual>> partitionSequences = sequenceStatistics.getMigrations(spaceTimeCriteria,partitionCriteria);

					for (String key : partitionSequences.keySet()){
						
						reportDiagramsValueSequences(reportDiagrams,partitionSequences.get(key),partitionCriteria, splitCriteria,label);
						
					}
					continue;
				}
				
				
				if (valueSequenceCriteria.isWithCensus()){
					
					reportCensus(reportCensus,sequenceStatistics.getPartitionSequence(label),label,valueCode, spaceTimeCriteria);
					
					if (sequenceStatistics.getAggregatePartitionSequence(label)!=null){

						reportCensus(reportCensus,sequenceStatistics.getAggregatePartitionSequence(label),label+"_TOTAL",null, spaceTimeCriteria);
					}
				}
				
				if (valueSequenceCriteria.isWithMatrix()){
					
					if (valueSequenceCriteria.isWithGraph()){
						
						reportMatrix(reportMatrices,sequenceStatistics.getPartitionSequence(label),label,valueCode, pajekBufferTransitions,spaceTimeCriteria);
						
					} else {
						
						reportMatrix(reportMatrices,sequenceStatistics.getPartitionSequence(label),label,valueCode, null,spaceTimeCriteria);
					}
				}
			}*/
			

			// Add Pajek data
			
			for (String bufferTitle : pajekBuffers.keySet()) {

				StringList pajekBuffer = pajekBuffers.get(bufferTitle);
				
				if (pajekBuffer.length() != 0) {
					File targetFile = ToolBox.setExtension(ToolBox.addToName(new File(segmentation.getLabel()), "-" + bufferTitle), ".paj");
					ReportRawData rawData = new ReportRawData("Export " + bufferTitle + "s to Pajek", "Pajek", "paj", targetFile);
					rawData.setData(PAJFile.convertToMicrosoftEndOfLine(pajekBuffer.toString()));

					result.outputs().appendln();
					result.outputs().append(rawData);
				}
			}
			
/*			if (pajekBufferStations.length() != 0) {
				File targetFile = ToolBox.setExtension(ToolBox.addToName(new File("Station graphs_"), spaceTimeCriteria.getLocalUnitLabel()), ".paj");
				ReportRawData rawData = new ReportRawData("Export Sponsor Networks to Pajek", "Pajek", "paj", targetFile);
				rawData.setData(PAJFile.convertToMicrosoftEndOfLine(pajekBufferStations.toString()));

				result.outputs().appendln();
				result.outputs().append(rawData);
			}

			if (pajekBufferTransitions.length() != 0) {
				File targetFile = ToolBox.setExtension(ToolBox.addToName(new File("Transition graphs_"), spaceTimeCriteria.getRelationModelName()), ".paj");
				ReportRawData rawData = new ReportRawData("Export Transition Graphs to Pajek", "Pajek", "paj", targetFile);
				rawData.setData(PAJFile.convertToMicrosoftEndOfLine(pajekBufferTransitions.toString()));

				result.outputs().appendln();
				result.outputs().append(rawData);
			}*/
			
/*			if (reportCensus.hasOutput()) result.outputs().append(reportCensus);
			if (reportDetails.hasOutput()) result.outputs().append(reportDetails);
			if (reportMatrices.hasOutput()) result.outputs().append(reportMatrices);
			
			if (reportDiagrams.hasOutput()) {
				reportDiagrams.arrangeChartsAndTables(3);
				result.outputs().append(reportDiagrams);
			}
			
			if (reportFlows.hasOutput()) result.outputs().append(reportFlows);

			for (Report reportSupplement : reportSupplements){
				result.outputs().append(reportSupplement);
			}*/

			//
			result.setTimeSpent(chrono.stop().interval());
		}
		//
		return result;
	}


	/**
		 * Builds many report from a large criteria object:
		 * 
		 * <ul>
		 * <li>Report - Sequence Report</li>
		 * <li>Report - Slice Report</li>
		 * <li>Analysis - Sequences - General Sequence Statistics</li>
		 * <li>Analysis - Sequences - Trajectories</li>
		 * <li>Analysis - Sequences - Ego-Networks</li>
		 * <li>Analysis - Sequences - Sequence Networks</li>
		 * <li>Analysis - Slices - General Slices Statistics</li>
		 * <li>Analysis - Slices - Trajectories</li>
		 * <li>Analysis - Slices - EgoNetworks</li>
		 * </ul>
		 * 
		 * @param criteria
		 * @return
		 * @throws PuckException
		 */
	/*	public static ReportList reportHugeAnalysis(final Net net, final Segmentation segmentation, final SequenceCriteria criteria, final ResourceBundle bundle)
				throws PuckException {
			ReportList result;
	
			// Optionalize
			criteria.setGroupAffiliationLabel("PATRIL");
			criteria.setRoleNames(Arrays.asList(new String[] { "REFERENT" }));
			
			result = new ReportList();
	
			if (criteria != null) {
				//
	//			StringList relationModelNames = net.relationModels().sortedNameList();
	//			AttributeDescriptors attributeDescriptors = AttributeWorker.getExogenousAttributeDescriptors(segmentation, null);
	
				
				// Set partition criteria
				
	//			setPartitionCriteria(criteria);
				
				//
				if (criteria.hasVariableIntervals()) {
	
	
					// Get (coherent) itineraries
					EgoSequences egoSequences = SequenceWorker.getCoherentItineraries(segmentation, criteria);
							new EgoSequences();
					for (Individual ego : segmentation.getCurrentIndividuals().toSortedList()) {
						egoSequences.addRenumbered(SequenceWorker.getCoherentItinerarySegment(ego, criteria));
					}
	
					//
					
					// Reports
					if (!criteria.getSequenceReportTypes().isEmpty()){
						
						Report sequenceReport = SequenceReporter.reportSequences(net, segmentation, criteria);
						result.add(sequenceReport);
	
					}
					
					// Sequence Statistics
					
					if (!criteria.getSequenceGeneralStatistics().isEmpty()){
						Report sequenceCensusReport = SequenceReporter.reportSequences("General",segmentation, egoSequences,criteria);
						result.add(sequenceCensusReport);
					}
					
					if (!criteria.getEgoNetworksOperations().isEmpty()){
						Report egoNetworkReport = SequenceReporter.reportSequences("Ego Networks",segmentation, egoSequences,criteria);
						result.add(egoNetworkReport);
					}
					
					if (!criteria.getParcoursNetworksOperations().isEmpty()){
						Report parcoursNetworkReport = SequenceReporter.reportSequences("Parcours Networks",segmentation, egoSequences,criteria);
						result.add(parcoursNetworkReport);
					}
					
					if (!criteria.getTrajectoriesOperations().isEmpty()){
						Report trajectoriesReport = SequenceReporter.reportSequences("Trajectories",segmentation, egoSequences, criteria);
						result.add(trajectoriesReport);
					}
				} else {
					//
	
					sequences = SequenceMaker.createRelationSequences(segmentation, criteria);
					EgoSequences egoSequences = sequences.getIndividualStateSequences(segmentation, criteria);
					
					if (!criteria.getSliceReportTypes().isEmpty()){
						Report sliceReport = SequenceReporter.reportMembers(sequences, criteria);
						result.add(sliceReport);
					}
					
					// Optionalize
	//				criteria.setEgoRoleName("RESIDENT");
					criteria.setGroupAffiliationLabel("PATRIL");
					criteria.setRoleNames(Arrays.asList(new String[] { "REFERENT" }));
	
					
					// Individuals - Personal properties
					
					if (criteria.getSliceGeneralStatistics().contains(SliceGeneralStatistics.POPULATION)){
						
						List<String> labels = Arrays.asList(new String[]{"NRINDIVIDUALS","GENDER_RATIO","GENDER","BIRT_PLACE","QUARTER","QUARTER_POPULATION","PATRIL","OCCUPATION","MATRISTATUS","AGE"});
						criteria.setValueSequenceCriteria(labels, labels, null, null, labels);
						
						criteria.getValueSequenceCriteriaByLabel("BIRT_PLACE").setLabelParameter("Bas-Mono");
						criteria.getValueSequenceCriteriaByLabel("AGE").setSizedGrouping(0., 20., null);
						criteria.getValueSequenceCriteriaByLabel("NRINDIVIDUALS").setSizedGrouping(0., 10., null);
						criteria.getValueSequenceCriteriaByLabel("GENDER_RATIO").setSizedGrouping(0., 0.1, null);
						
						Report sliceStaticsReport = SequenceReporter.reportSequences("Population",segmentation, sequences, criteria);
	//					Report sliceStaticsReport = SequenceReporter.reportIndividualStatics(segmentation, slices, criteria, statisticsCriteria);
						result.add(sliceStaticsReport);
					}
					
					// Individuals - Relational Positions
					
					if (criteria.getSliceGeneralStatistics().contains(SliceGeneralStatistics.POSITIONS)){
						
						List<String> labels = Arrays.asList(new String[]{"PLACE","REFERENT_CHAIN","REFERENT_CHAIN_TYPE","REFERENT_KIN","REFERENT_KIN_TYPE","REFERENT"});
						List<String> withMatrix = Arrays.asList(new String[]{"REFERENT_CHAIN","REFERENT_KIN"});
						criteria.setValueSequenceCriteria(labels, withMatrix, withMatrix, withMatrix, null);
	
						Report report2 = SequenceReporter.reportSequences("Referents",segmentation, egoSequences, criteria);
						result.add(report2);
					}
					
					// Groups - Quantitative
					
					if (criteria.getSliceGeneralStatistics().contains(SliceGeneralStatistics.METRICS)){
						
						List<String> labels = Arrays.asList(new String[]{"SIZE","MAXDEPTH","MEANDEPTH","MEANINDEGREE","DIAMETER","NRCOMPONENTS","MAXCOMPONENT","CONCENTRATION"});			
						criteria.setValueSequenceCriteria(labels, labels, null, null, null);
						
						Report report = SequenceReporter.reportSequences("Groups",segmentation, sequences, criteria);
	//					Report report = SequenceReporter.reportTreeStructure(segmentation, criteria, false);
						result.add(report);
					}
					
					// Groups - Qualitative
					
					if (criteria.getSliceGeneralStatistics().contains(SliceGeneralStatistics.MORPHOLOGY)){
						
						List<String> labels = Arrays.asList(new String[]{"TREES_BY_ID","TREES_BY_GENDER","TREES_BY_KIN","REFERENT_CHAIN","REFERENT_KIN","ALL_KIN"});
						List<String> withMatrix = Arrays.asList(new String[]{"REFERENT_CHAIN","REFERENT_KIN"});
						criteria.setValueSequenceCriteria(labels, labels, withMatrix, withMatrix, null);
						
						Report report = SequenceReporter.reportSequences("Group Composition",segmentation, sequences, criteria);
						result.add(report);
						
					}
	
					// Flows and Migrations
					
					if (criteria.getSliceGeneralStatistics().contains(SliceGeneralStatistics.DYNAMICS)){
						
						List<String> labels = Arrays.asList(new String[]{"PLACE"});
						criteria.setValueSequenceCriteria(labels, null, null, null, null);
						criteria.getValueSequenceCriteriaByLabel("PLACE").setLabelParameter("Bas-Mono");
						criteria.getValueSequenceCriteriaByLabel("PLACE").setWithFlow(true);
						
						Report sliceDynamicsReport = SequenceReporter.reportSequences("Dynamics",segmentation, sequences, criteria);
	//					Report sliceDynamicsReport = SequenceReporter.reportIndividualDynamics(segmentation, sequences, criteria, statisticsCriteria, placeCriteria);
						result.add(sliceDynamicsReport);
					}
					
	//				StatisticsCriteria statisticsCriteria = new StatisticsCriteria();
					
					if (!criteria.getTrajectoriesOperations().isEmpty()){
						Report trajectoriesReport = SequenceReporter.reportSequences("Group trajectories", segmentation, sequences, criteria);
						result.add(trajectoriesReport);
	
					}
	
					if (!criteria.getEgoNetworksOperations().isEmpty()){
						
	//					Sequence<Relations> slices = SequenceMaker.createRelationSetSequence(segmentation, criteria);
						
						Report egoNetworkReport = SequenceReporter.reportSequences("Ego Networks",segmentation, egoSequences,criteria);
	//					Report egoNetworkReport = SequenceReporter.reportEgoNetworks(segmentation, slices, statisticsCriteria, criteria);
						result.add(egoNetworkReport);
					}
					
					// Dated Circuit Censuses
					
					// Overall Circuits Censuses
					if (criteria.getSliceGeneralStatistics().contains(SliceGeneralStatistics.CIRCUITS)){
						for (Report report3 : CensusReporter.reportRelationCensus(segmentation, criteria)) {
							result.add(report3);
						}
					}
	
					// Differential Circuit Censuses
	//				if (criteria.getReferentNetworkOperations().contains(ReferentNetworkOperation.DIFFERENTIAL_CENSUS)){
						CensusCriteria censusCriteria = new CensusCriteria();
	
						censusCriteria.setPattern(criteria.getPattern());
						censusCriteria.setChainClassification(criteria.getChainClassification());
						censusCriteria.setRelationAttributeLabel(criteria.getLocalUnitLabel());
	
						Integer[] dates = criteria.getDates();
						if (dates == null) {
							Report report = CensusReporter.reportDifferentialCensus(segmentation, criteria, null,
									censusCriteria);
							result.add(report);
						} else {
							for (Integer date : dates) {
								Report report = CensusReporter.reportDifferentialCensus(segmentation, criteria, date,
										censusCriteria);
								result.add(report);
							}
						}
				}
			}
	
			//
			return result;
		}*/
		
		private static void reportDatedCircuitCensus(ReportList reportList, Segmentation segmentation, SequenceCriteria criteria) throws PuckException{
			
			// Dated Circuit Censuses
			
			// Overall Circuits Censuses
			
			if (criteria.getSliceGeneralStatistics().contains(SliceGeneralStatistics.CIRCUITS)){
				for (Report report3 : CensusReporter.reportRelationCensus(segmentation, criteria)) {
					reportList.add(report3);
				}
			}
	
			// Differential Circuit Censuses
			
			CensusCriteria censusCriteria = new CensusCriteria();
	
			censusCriteria.setPattern(criteria.getPattern());
			censusCriteria.setChainClassification(criteria.getChainClassification());
			censusCriteria.setRelationAttributeLabel(criteria.getLocalUnitLabel());
			
			censusCriteria.setRestrictionType(RestrictionType.ALL);
			censusCriteria.setSymmetryType(SymmetryType.INVERTIBLE);
			censusCriteria.setCrossSexChainsOnly(false);
			censusCriteria.setClosingRelation("TOTAL");

	
			Integer[] dates = criteria.getDates();
			if (dates == null) {
				
				Report report = CensusReporter.reportDifferentialCensus(segmentation, criteria, null,
						censusCriteria);
				reportList.add(report);
				
			} else {
				
				for (Integer date : dates) {
					
					Report report = CensusReporter.reportDifferentialCensus(segmentation, criteria, date,
							censusCriteria);
					reportList.add(report);
				}
			}
		}
		
	public static ReportList reportSequenceAnalysis(final Net net, final Segmentation segmentation, final SequenceCriteria criteria, final ResourceBundle bundle)
			throws PuckException {
		ReportList result;
	
		result = new ReportList();
	
		if (criteria != null) {
			
			// Set partition criteria
			
			criteria.setPartitionCriteria();
	
			// Initialize sequence report titles
			
			List<String> reportTitles = new ArrayList<String>();
			
			if (!criteria.getSequenceGeneralStatistics().isEmpty()){
				reportTitles.add("General");
			}
			if (!criteria.getEgoNetworksOperations().isEmpty()){
				reportTitles.add("Ego Networks");
			}
			if (!criteria.getParcoursNetworksOperations().isEmpty()){
				reportTitles.add("Parcours Networks");
			}
			if (!criteria.getTrajectoriesOperations().isEmpty()){
				reportTitles.add("Sequence Networks");
			}
			if (criteria.getSliceGeneralStatistics().contains(SliceGeneralStatistics.POPULATION)){
				reportTitles.add("Population");
			}
			if (criteria.getSliceGeneralStatistics().contains(SliceGeneralStatistics.POSITIONS)){
				reportTitles.add("Referents");
			}
			if (criteria.getSliceGeneralStatistics().contains(SliceGeneralStatistics.METRICS)){
				reportTitles.add("Groups");
			}
			if (criteria.getSliceGeneralStatistics().contains(SliceGeneralStatistics.MORPHOLOGY)){
				reportTitles.add("Group Composition");
			}
			if (criteria.getSliceGeneralStatistics().contains(SliceGeneralStatistics.DYNAMICS)){
				reportTitles.add("Dynamics");
			}
			
			// Create sequences
	
			Sequences<?,Relation> sequences = null;
			
			if (criteria.hasVariableIntervals()) {
				
				sequences = SequenceWorker.getCoherentItineraries(segmentation, criteria);
				
			} else {
				
				sequences = SequenceMaker.createRelationSequences(segmentation, criteria);
				
				if (criteria.getSliceGeneralStatistics().contains(SliceGeneralStatistics.POSITIONS) || (!criteria.getEgoNetworksOperations().isEmpty())){
					
					sequences = sequences.getIndividualStateSequences(segmentation, criteria);
				}
			}
			
			
			logger.debug("Sequences created");
			
			// Create analysis reports
			
			for (String reportTitle : reportTitles){
				result.add(reportSequenceAnalysis(reportTitle,segmentation, sequences,criteria,result.getGraphs()));
			}
			
			// Create sequence reports
			
			if (!criteria.getSequenceReportTypes().isEmpty()){
				result.add(reportSequences(net, segmentation, criteria));
			}
			
			// Create member reports
			
			if (criteria.getSliceReportTypes().contains(SliceReportType.MEMBERSHIP)){
				result.add(reportMembers(sequences, criteria));
			}
			
			// Create turnover reports
			
			if (criteria.getSliceReportTypes().contains(SliceReportType.TURNOVER)){
				result.add(reportTurnover(sequences, criteria));
			}
			
			// Create circuit reports
			
			if (criteria.getSliceGeneralStatistics().contains(SliceGeneralStatistics.CIRCUITS)){
				reportDatedCircuitCensus(result,segmentation,criteria);
			}
		}
		//
		return result;
	}
	
/*	private static String getSplitLabel(String partitionLabel, String defaultSplitLabel, Individuals individuals){
		String result;
		
		result = IndividualValuator.isIndividualAttributeLabel(partitionLabel,individuals) ? defaultSplitLabel : null; 
		
		//
		return result;
	}
	
	



	public static Report reportItineraries1 (final Segmentation segmentation, final SequenceCriteria criteria) throws PuckException{
			Report result;
			
			if ((segmentation == null) || (criteria.getRelationModelName() == null)) {
				throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
			} else {
				
				result = new Report("Itineraries "+criteria.getRelationModelName());
	
				Chronometer chrono = new Chronometer();
				
				result.setOrigin("Relation reporter");
				
				List<String> detailsIndicators = Arrays.asList(new String[]{"PLACE","REFERENT_CHAIN","REFERENT_CHAIN_TYPE","REFERENT_KIN","REFERENT_KIN_TYPE","REFERENT"});
				List<String> matrixIndicators = Arrays.asList(new String[]{"REFERENT_CHAIN","REFERENT_KIN"});
				
				Sequences<Relation> sequences = SequenceMaker.createRelationSequences(segmentation, criteria);
				EgoSequences egoSequences = sequences.getIndividualStateSequences(segmentation, criteria);
//				SequenceStatistics<Sequence<Relation>,Relation> sequenceStatistics = new SequenceStatistics<Sequence<Relation>,Relation>(segmentation, sequences, criteria, indicators);
				SequenceStatistics<EgoSequence,Relation> sequenceStatistics = new SequenceStatistics<EgoSequence,Relation>(segmentation, egoSequences, criteria, detailsIndicators);

	//			sequenceStatistics.putMemberValues(criteria);
	//			houseStatistics.getMemberValues(segmentation);
				
				StringList pajekBuffer = new StringList();
				
				Report reportCensus = reportCensuses("Census", sequenceStatistics, matrixIndicators, criteria);
				Report reportSequences = reportDetails("Details", sequenceStatistics, detailsIndicators, criteria);
				Report reportMatrices = reportMatrices("Matrices", sequenceStatistics, matrixIndicators, pajekBuffer, criteria);*/
				
/*				Individuals members = sequenceStatistics.getPopulation();
				
				// Sequence report
				
				String headLine1 = "Id\tNAME\tGENDER\t";
				String dateLine1 = "\t\t\t";
				for (String indicator : detailsIndicators){
					
					headLine1 += indicator;
	
					for (Integer year : criteria.getDates()){
						
						headLine1 += "\t";
						dateLine1 += year+"\t";
					}
	
	//				headLine1 += "\t";
	//				dateLine1 += "TREND\t";
				}
				
				reportSequences.outputs().appendln(headLine1);
				reportSequences.outputs().appendln(dateLine1);
				
				for (Individual member : members.toSortedList()){
					String memberLine = member.getId()+"\t"+member.getName()+"\t"+member.getGender()+"\t";
					
					for (String indicator : detailsIndicators){
						
						for (Integer year : criteria.getDates()){
		
							Ordinal time = new Ordinal(year);
							Object value = sequenceStatistics.getValue(member, time, indicator);
	//						Object value = houseStatistics.getByIndividual(member, time, indicator);
							
							memberLine += value+"\t";
						}
					}
					reportSequences.outputs().appendln(memberLine);
				}
				
				// Census and Matrix report
				
				StringList pajekBuffer = new StringList();
	
				for (String matrixIndicator : matrixIndicators){
					
					reportMatrices.outputs().appendln("Transition Matrix "+matrixIndicator);
					reportMatrices.outputs().appendln();*/
					
/*					reportCensus.outputs().appendln(matrixIndicator+" Position Census");
					reportCensus.outputs().appendln();
					
					String headLine0 = "Position\t";
					for (Integer year : criteria.getDates()){
						headLine0 += year+"\t";
					}
					reportCensus.outputs().appendln(headLine0);*/
						
	//				PartitionSequence<Individual> individualValueCensuses = sequenceStatistics.getDatedIndividualCensus(matrixIndicator);
					
	//				Map<Value,Double> meanValueFrequencies = individualValueCensuses.getMeanValueFrequencies();
//					Map<Value,Double> meanValueFrequencies = sequenceStatistics.getMeanValueFrequencies(individualValueCensuses, members);
	//				Map<String,Double> meanValueFrequencies = houseStatistics.getMeanIndividualValueFrequencies(segmentation, matrixIndicator, criteria.getDates());
	
	//				Partition<Individual> individualSequenceCensus = individualValueCensuses.getValueSequencePartition();
//					Partition<Individual> individualSequenceCensus = sequenceStatistics.getSequenceCensus(individualValueCensuses, members);
	//				Partition<Individual> individualSequenceCensus = houseStatistics.getIndividualSequenceCensus(segmentation, matrixIndicator);
	
/*					for (Value value : meanValueFrequencies.keySet()){
						String valueLine = value+"";
						for (Integer year : criteria.getDates()){
							int count = 0;
							Cluster<Individual> cluster = individualValueCensuses.get(new Ordinal(year)).getCluster(new Value(value));
							if (cluster!=null){
								count += cluster.size();
							}
							valueLine += "\t"+count;
						}
						reportCensus.outputs().appendln(valueLine);
					}
					reportCensus.outputs().appendln();
	
					reportCensus.outputs().appendln(matrixIndicator+" Sequence Census");
					reportCensus.outputs().appendln();
					reportCensus.outputs().appendln("Sequence");
					
					int sequenceCount = individualSequenceCensus.itemsCount();
					
					for (Cluster<Individual> cluster : individualSequenceCensus.getClusters().toListSortedByDescendingSize()){
						reportCensus.outputs().appendln(cluster.getValue()+"\t"+cluster.size()+"\t"+MathUtils.percent(cluster.size(), sequenceCount));
					}
					reportCensus.outputs().appendln();*/
					
/*					PartitionSequence<Individual> census = sequenceStatistics.getDatedIndividualCensus(matrixIndicator);
					Matrix matrix = census.getTransitionMatrix();
//					Matrix matrix = sequenceStatistics.getTransitionMatrix(census, members);
					
	//				Matrix matrix = houseStatistics.getIndividualTransitionMatrix(segmentation, matrixIndicator);
					
					String headLine = "\t";
					for (int col=0;col<matrix.getRowDim();col++){
						headLine += matrix.getColLabel(col)+"\t";
					}
					reportMatrices.outputs().appendln(headLine);
					
					for (int row=0;row<matrix.getRowDim();row++){
						String rowLine = matrix.getRowLabel(row)+"\t";
						for (int col=0;col<matrix.getRowDim();col++){
							rowLine += matrix.getAsRowPercentage(row, col)+"\t";
						}
						reportMatrices.outputs().appendln(rowLine);
					}
					reportMatrices.outputs().appendln();
					
					// Transition graph
					
					Graph<Node<String>> transitionGraph = GraphMaker.createGraph("Transition Graph "+criteria.getRelationModelName()+" "+matrixIndicator, matrix, meanValueFrequencies, "MEAN");*/
							
	/*				Graph<Node<String>> transitionGraph = new Graph<Node<String>>("Transition Graph "+criteria.getRelationModelName()+" "+matrixIndicator);
					
					for (int row=0;row<matrix.getRowDim();row++){
						Node<String> node = new Node<String>(row,matrix.getRowLabel(row));
						Double mean = meanValueFrequencies.get(new Value(matrix.getRowLabel(row)));
						if (mean==null){
							node.setAttribute("MEAN","0.");
						} else {
							node.setAttribute("MEAN", mean+"");
						}
						transitionGraph.addNode(row,node);
					}
					
					for (int row=0;row<matrix.getRowDim();row++){
						for (int col=0;col<matrix.getRowDim();col++){
							if (matrix.get(row, col) > 0){
								transitionGraph.addArcWeight(row, col, matrix.get(row,col));
							}
						}
					}*/
					
/*					List<String> partitionLabels = new ArrayList<String>();
					partitionLabels.add("MEAN");
					pajekBuffer.addAll(PuckUtils.writePajekNetwork(transitionGraph, partitionLabels));
					pajekBuffer.appendln();
	
				}*/
				
/*				if (pajekBuffer.length() != 0) {
					File targetFile = ToolBox.setExtension(ToolBox.addToName(new File("Transition graphs_"), criteria.getRelationModelName()), ".paj");
					ReportRawData rawData = new ReportRawData("Export Transition Graphs to Pajek", "Pajek", "paj", targetFile);
					rawData.setData(PAJFile.convertToMicrosoftEndOfLine(pajekBuffer.toString()));
	
					result.outputs().appendln();
					result.outputs().append(rawData);
				}
	
				result.outputs().append(reportCensus);
				result.outputs().append(reportSequences);
				result.outputs().append(reportMatrices);
			}
			//
			return result;
		}*/


/*	public static Report reportTreeStructure(final Segmentation segmentation, final SequenceCriteria criteria, boolean reducedTrees) throws PuckException{
			Report result;
			
			if ((segmentation == null) || (criteria.getRelationModelName() == null)) {
				throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
			} else {
				
				result = new Report("Cluster Structure "+criteria.getRelationModelName());
				
				Chronometer chrono = new Chronometer();
				
				result.setOrigin("Sequence reporter");
				
				List<String> indicators1 = Arrays.asList(new String[]{"GRAPH","SIZE","MAXDEPTH","MEANDEPTH","MEANINDEGREE","DIAMETER","NRCOMPONENTS","MAXCOMPONENT","CONCENTRATION"});			
				List<String> indicators2 = Arrays.asList(new String[]{"TREES_BY_ID","TREES_BY_GENDER","TREES_BY_KIN"});
				List<String> indicators3 = Arrays.asList(new String[]{"REFERENT_KIN"});
				List<String> indicators4 = Arrays.asList(new String[]{"ALL_KIN"});
				List<String> indicators5 = Arrays.asList(new String[]{"REFERENT_CHAIN"});
				
				List<String> matrixIndicators = Arrays.asList(new String[]{"CHAIN","KIN"});		
				
				CensusCriteria censusCriteria = new CensusCriteria();
				censusCriteria.setPattern(criteria.getPattern());
				censusCriteria.setChainClassification(criteria.getChainClassification());
				censusCriteria.setRelationAttributeLabel(criteria.getLocalUnitLabel());
				censusCriteria.setRestrictionType(RestrictionType.ALL);
				censusCriteria.setSymmetryType(SymmetryType.INVERTIBLE);
				censusCriteria.setClosingRelation("TOTAL");
	
				Sequences<Relation> sequences = SequenceMaker.createRelationSequences(segmentation, criteria);
			
				SequenceStatistics<Sequence<Relation>,Relation> houseStatistics1 = new SequenceStatistics<Sequence<Relation>,Relation>(segmentation, sequences, criteria, indicators1);
//				SequenceStatistics<Sequence<Relation>,Relation> houseStatistics2 = new SequenceStatistics<Sequence<Relation>,Relation>(segmentation, sequences, criteria, indicators2);
				SequenceStatistics<Sequence<Relation>,Relation> houseStatistics3 = new SequenceStatistics<Sequence<Relation>,Relation>(segmentation, sequences, criteria, indicators3);
				SequenceStatistics<Sequence<Relation>,Relation> houseStatistics4 = new SequenceStatistics<Sequence<Relation>,Relation>(segmentation, sequences, criteria, indicators4);
				SequenceStatistics<Sequence<Relation>,Relation> houseStatistics5 = new SequenceStatistics<Sequence<Relation>,Relation>(segmentation, sequences, criteria, indicators5);
				SequenceStatistics<Sequence<Relation>,Relation> houseStatistics6 = new SequenceStatistics<Sequence<Relation>,Relation>(segmentation, sequences, criteria, new ArrayList<String>());
				
//				houseStatistics1.putSequenceValues();
//				houseStatistics2.putSequenceValues();
		
				Map<String,PartitionSequence<Sequence<Relation>>>  referentKinCensus = houseStatistics3.getPartitionSequence("REFERENT_KIN").unpack();	
				Map<String,PartitionSequence<Sequence<Relation>>>  allKinCensus = houseStatistics4.getPartitionSequence("ALL_KIN").unpack();	
				Map<String,PartitionSequence<Sequence<Relation>>>  referentChainCensus = houseStatistics5.getPartitionSequence("REFERENT_CHAIN").unpack();	
	
	//			houseStatistics4.getAllKinCensus(segmentation, censusCriteria);	
	//			Map<Ordinal,Partition<Sequence<Relation>>>  referentChainCensus = houseStatistics5.getReferentChainCensus();	
	//			Map<Ordinal,Partition<Sequence<Relation>>>  referentKinCensus = houseStatistics3.getReferentKinCensus();	
	
				indicators3 = houseStatistics3.indicators();
				indicators4 = houseStatistics4.indicators();
				indicators5 = houseStatistics5.indicators();*/
				
//				StringList pajekBuffer2 = new StringList();
				
/*				Report reportOverview = new Report("Overview");
				Report reportMetrics = new Report("Metrics");
//				Report reportMorphology = new Report("Morphology");
				Report reportReferentKin = new Report("Referent kin census");
				Report reportKin = new Report("All kin census");
				Report reportReferentChain = new Report("Referent chain census");
				Report reportFlow = new Report("Flow census");

				Partition<String> inFlows = houseStatistics6.getFlows("IN",criteria.getDateLabel(),criteria.getPattern());
				Partition<String> outFlows = houseStatistics6.getFlows("OUT",criteria.getDateLabel(),criteria.getPattern());
				Partition<String> allFlows = new Partition<String>();
				allFlows.add(inFlows);
				allFlows.add(outFlows);

				for (Cluster<String> cluster : allFlows.getClusters().toListSortedByValue()){
					reportFlow.outputs().appendln(cluster.getValue());
					for (String item : cluster.getItems()){
						reportFlow.outputs().appendln("\t"+item);
					}
				}
				
				// ReferentChainCensus
				
				Set<Value> values = new HashSet<Value>();
				String timeLine = "Chain Types\t";
				String typeLine = "type\t";
				for (Ordinal time : referentChainCensus.getTimes()){
					timeLine += time+"\t\t";
					
					typeLine += "#Houses\tHouses\t";
					Partition<Sequence<Relation>> partition = referentChainCensus.getStation(time);
					values.addAll(partition.getValues());
				}
				reportOverview.outputs().appendln(timeLine);
				reportOverview.outputs().appendln(typeLine);
	
				List<Value> valueList = new ArrayList<Value>(values);
				Collections.sort(valueList);
				
				for (Value value : valueList){
					String valueLine = value+"";
					for (Ordinal time : referentChainCensus.getTimes()){
						Cluster<Sequence<Relation>> cluster = referentChainCensus.getStation(time).getCluster(value);
						if (cluster!=null){
							valueLine += "\t"+cluster.size()+"\t"+cluster.getItemsAsString();
						} else {
							valueLine += "\t0\t";
						}
					}
					reportOverview.outputs().appendln(valueLine);
				}
				reportOverview.outputs().appendln();
	
				// ReferentKinCensus
				
				Set<Value> values2 = new HashSet<Value>();
				String timeLine2 = "Kin Types\t";
				String typeLine2 = "type\t";
				for (Ordinal time : referentKinCensus.getTimes()){
					timeLine2 += time+"\t\t";
					typeLine2 += "#Houses\tHouses\t";
					Partition<Sequence<Relation>> partition = referentKinCensus.getStation(time);
					values2.addAll(partition.getValues());
				}
				reportOverview.outputs().appendln(timeLine2);
				reportOverview.outputs().appendln(typeLine2);
	
				List<Value> valueList2 = new ArrayList<Value>(values2);
				Collections.sort(valueList2);
				
				for (Value value : valueList2){
					String valueLine = value+"";
					for (Ordinal time : referentKinCensus.getTimes()){
						Cluster<Sequence<Relation>> cluster = referentKinCensus.getStation(time).getCluster(value);
						if (cluster!=null){
							valueLine += "\t"+cluster.size()+"\t"+cluster.getItemsAsString();
						} else {
							valueLine += "\t0\t";
						}
					}
					reportOverview.outputs().appendln(valueLine);
				}
				reportOverview.outputs().appendln();*/
	
				
/*				String headLine1 = "HOUSE\t";
				String dateLine1 = "\t";
				for (String indicator : indicators1){
					
					if (indicator.equals("GRAPH")){
						continue;
					}
					
					headLine1 += indicator;
	
					for (Integer year : criteria.getDates()){
						
						headLine1 += "\t";
						dateLine1 += year+"\t";
					}
	
					headLine1 += "\t";
					dateLine1 += "TREND\t";
				}
				
				reportMetrics.outputs().appendln(headLine1);
				reportMetrics.outputs().appendln(dateLine1);
				
				String headLine2 = "HOUSE\t";
				String dateLine2 = "\t";
				for (String indicator : indicators2){
					
					headLine2 += indicator;
	
					for (Integer year : criteria.getDates()){
						
						headLine2 += "\t";
						dateLine2 += year+"\t";
					}
	
	//				headLine1 += "\t";
	//				dateLine1 += "TREND\t";
				}
				
				reportMorphology.outputs().appendln(headLine2);
				reportMorphology.outputs().appendln(dateLine2);
	
				String headLine3 = "HOUSE\t";
				String dateLine3 = "\t";
				for (String indicator : indicators3){
					
					headLine3 += indicator;
	
					for (Integer year : criteria.getDates()){
						
						headLine3 += "\t";
						dateLine3 += year+"\t";
					}
	
					headLine3 += "\t";
					dateLine3 += "TREND\t";
				}
				
				reportReferentKin.outputs().appendln(headLine3);
				reportReferentKin.outputs().appendln(dateLine3);
	
				String headLine4 = "HOUSE\t";
				String dateLine4 = "\t";
				for (String indicator : indicators4){
					
					headLine4 += indicator;
	
					for (Integer year : criteria.getDates()){
						
						headLine4 += "\t";
						dateLine4 += year+"\t";
					}
	
					headLine4 += "\t";
					dateLine4 += "TREND\t";
				}
				
				reportKin.outputs().appendln(headLine4);
				reportKin.outputs().appendln(dateLine4);
	
				
				String headLine5 = "HOUSE\t";
				String dateLine5 = "\t";
				for (String indicator : indicators5){
					
					headLine5 += indicator;
	
					for (Integer year : criteria.getDates()){
						
						headLine5 += "\t";
						dateLine5 += year+"\t";
					}
	
					headLine5 += "\t";
					dateLine5 += "TREND\t";
				}
				
				reportReferentChain.outputs().appendln(headLine5);
				reportReferentChain.outputs().appendln(dateLine5);*/
	
				
/*				for (Sequence<Relation> house : sequences.toSortedList()){
					
					String houseLine1 = house+"\t";
					String houseLine2 = house+"\t";
					String houseLine3 = house+"\t";
					String houseLine4 = house+"\t";
					String houseLine5 = house+"\t";
	
					for (String indicator : indicators1){
						
						for (Integer year : criteria.getDates()){
		
							Ordinal time = new Ordinal(year);
							Value value = houseStatistics1.getValue(house, time, indicator);
	//						Value value = houseStatistics1.getBySequence(house, time, indicator);
							
							if (indicator.equals("GRAPH")) {
								
								if (value!=null) {
	
									List<String> partitionLabels = new ArrayList<String>();
									pajekBuffer2.addAll(PuckUtils.writePajekNetwork((Graph<Individual>)value.graphValue(), partitionLabels));
									pajekBuffer2.appendln();
	
								}
								
							} else if (value != null){
								
								houseLine1 += value+"\t";
								
							} else {
								
								houseLine1 += "\t";
	
							}
						}
						
						if (!indicator.equals("GRAPH")) {
							
							houseLine1 += houseStatistics1.getTrend(house, indicator)+"\t";
						
						}
					}
	
					for (String indicator : indicators2){
						
						for (Integer year : criteria.getDates()){
		
							Ordinal time = new Ordinal(year);
							Value value = houseStatistics2.getValue(house, time, indicator);
	//						Object value = houseStatistics2.getBySequence(house, time, indicator);
							
							if (value != null){
								
								houseLine2 += value+"\t";
								
							} else {
								
								houseLine2 += "\t";
	
							}
						}
					}*/
	
/*					for (String indicator : indicators3){
						
						for (Integer year : criteria.getDates()){
		
							Ordinal time = new Ordinal(year);
							Value value = houseStatistics3.getValue(house, time, indicator);
	//						Object value = houseStatistics3.getBySequence(house, time, indicator);
							
							if (value != null){
								
								houseLine3 += value+"\t";
								
							} else {
								
								houseLine3 += "\t";
	
							}
						}
						houseLine3 += houseStatistics3.getTrend(house, indicator)+"\t";
					}
	
					for (String indicator : indicators4){
						
						for (Integer year : criteria.getDates()){
		
							Ordinal time = new Ordinal(year);
							Value value = houseStatistics4.getValue(house, time, indicator);
	//						Value value = houseStatistics4.getBySequence(house, time, indicator);
							
	/*						if (value == null){
								value = 0.;
								houseStatistics4.put(house, time, indicator, value);
							} 
							houseLine4 += value+"\t";
						}
						houseLine4 += houseStatistics4.getTrend(house, indicator)+"\t";
					}
	
					for (String indicator : indicators5){
						
						for (Integer year : criteria.getDates()){
		
							Ordinal time = new Ordinal(year);
							Value value = houseStatistics5.getValue(house, time, indicator);
	//						Value value = houseStatistics5.getBySequence(house, time, indicator);
							
							if (value == null){
								value = 0.;
								houseStatistics5.put(house, time, indicator, value);
							} 
							houseLine5 += value+"\t";
						}
						
						houseLine5 += houseStatistics5.getTrend(house, indicator)+"\t";
					}
	
//					reportMetrics.outputs().appendln(houseLine1);
//					reportMorphology.outputs().appendln(houseLine2);
					reportReferentKin.outputs().appendln(houseLine3);
					reportKin.outputs().appendln(houseLine4);
					reportReferentChain.outputs().appendln(houseLine5);
				}
	
				
//				String endLine1 = "TOTAL\t";
				String endLine3 = "TOTAL\t";
				String endLine4 = "TOTAL\t";
				String endLine5 = "TOTAL\t";*/
				
/*				for (String indicator : indicators1){
	
					if (indicator.equals("GRAPH")) {
						continue;
					}
	
					for (Integer year : criteria.getDates()){
	
						Ordinal time = new Ordinal(year);
						Object value = houseStatistics1.meanOverSequences(time, indicator);
						
						if (value == null) {
							
							endLine1 += "\t";
							
						} else {
							
							endLine1 += value+"\t";
						}
					}
					
					endLine1 += houseStatistics1.getMeanTrend(indicator)+"\t";
				}
				reportMetrics.outputs().appendln(endLine1);
				
				for (String indicator : indicators3){
	
					for (Integer year : criteria.getDates()){
	
						Ordinal time = new Ordinal(year);
						Object value = houseStatistics3.meanOverSequences(time, indicator);
						
						if (value == null) {
							
							endLine3 += "\t";
							
						} else {
							
							endLine3 += value+"\t";
						}
					}
					
					endLine3 += houseStatistics3.getMeanTrend(indicator)+"\t";
				}
				reportReferentKin.outputs().appendln(endLine3);
	
				for (String indicator : indicators4){
	
					for (Integer year : criteria.getDates()){
	
						Ordinal time = new Ordinal(year);
						Object value = houseStatistics4.meanOverSequences(time, indicator);
						
						if (value == null) {
							
							endLine4 += "\t";
							
						} else {
							
							endLine4 += value+"\t";
						}
					}
					
					endLine4 += houseStatistics4.getMeanTrend(indicator)+"\t";
				}
				reportKin.outputs().appendln(endLine4);
	
				for (String indicator : indicators5){
	
					for (Integer year : criteria.getDates()){
	
						Ordinal time = new Ordinal(year);
						Object value = houseStatistics5.meanOverSequences(time, indicator);
						
						if (value == null) {
							
							endLine5 += "\t";
							
						} else {
							
							endLine5 += value+"\t";
						}
					}
					
					endLine5 += houseStatistics5.getMeanTrend(indicator)+"\t";
				}
				reportReferentChain.outputs().appendln(endLine5);*/
	
	
				// Matrix report
				
/*				StringList pajekBuffer1 = new StringList();
	
				for (String matrixIndicator : matrixIndicators){
					
					Report reportMatrices = new Report("Matrix_"+matrixIndicator);
					
					reportMatrices.outputs().appendln("Transition Matrix "+matrixIndicator);
					reportMatrices.outputs().appendln();
					
					SequenceStatistics<Sequence<Relation>,Relation> houseStatistics = null;
					PartitionSequence<Sequence<Relation>> sequenceCensus = null;
					
					if (matrixIndicator.equals("KIN")){
						houseStatistics = houseStatistics3;
						sequenceCensus = referentKinCensus;
					} else if (matrixIndicator.equals("CHAIN")){
						houseStatistics = houseStatistics5;
						sequenceCensus = referentChainCensus;
					}				
	
	//				Matrix matrix = houseStatistics.getHouseTransitionMatrix(census, criteria.getDates());
//					Matrix matrix = houseStatistics.getTransitionMatrix(sequenceCensus,sequences);
					Matrix matrix = sequenceCensus.getTransitionMatrix();
					Map<Value,Double> meanValueFrequencies = sequenceCensus.getMeanValueFrequencies();
//					Map<Value,Double> meanValueFrequencies = houseStatistics.getMeanValueFrequencies(sequenceCensus,sequences);
					
					String headLine = "\t";
					for (int col=0;col<matrix.getRowDim();col++){
						headLine += matrix.getColLabel(col)+"\t";
					}
					reportMatrices.outputs().appendln(headLine);
					
					for (int row=0;row<matrix.getRowDim();row++){
						String rowLine = matrix.getRowLabel(row)+"\t";
						for (int col=0;col<matrix.getRowDim();col++){
							rowLine += matrix.getAsRowPercentage(row, col)+"\t";
						}
						reportMatrices.outputs().appendln(rowLine);
					}
					reportMatrices.outputs().appendln();
					
					// Transition graph
					
					Graph<Node<String>> transitionGraph = new Graph<Node<String>>("Transition Graph "+criteria.getRelationModelName()+" "+matrixIndicator);
					
					for (int row=0;row<matrix.getRowDim();row++){
						Node<String> node = new Node<String>(row,matrix.getRowLabel(row));
						if (meanValueFrequencies.get(new Value(matrix.getRowLabel(row)))==null){
							node.setAttribute("MEAN","0.");
						} else {
							node.setAttribute("MEAN", meanValueFrequencies.get(new Value(matrix.getRowLabel(row)))+"");
						}
						transitionGraph.addNode(row,node);
					}
					
					for (int row=0;row<matrix.getRowDim();row++){
						for (int col=0;col<matrix.getRowDim();col++){
							if (matrix.get(row, col) > 0){
								Link<Node<String>> arc = transitionGraph.addArc(row, col, matrix.get(row,col));
								TransformationType transformationType = RelationWorker.getTransformationType(matrix.getRowLabel(row).split("\\s"), matrix.getColLabel(col).split("\\s"));
								arc.setTag(transformationType.ordinal()+" "+transformationType);
							}
						}
					}
					
					List<String> partitionLabels = new ArrayList<String>();
					partitionLabels.add("MEAN");
					pajekBuffer1.addAll(PuckUtils.writePajekNetwork(transitionGraph, partitionLabels));
					pajekBuffer1.appendln();
	
					result.outputs().append(reportMatrices);*/
					
					// Strength Census
					
//					result.outputs().append(GraphReporter.reportStrengthsByTags(transitionGraph));
/*				}
	
				if (pajekBuffer2.length() != 0) {
					File targetFile = ToolBox.setExtension(ToolBox.addToName(new File("Sponsor network "), criteria.getLocalUnitLabel()), ".paj");
					ReportRawData rawData = new ReportRawData("Export Sponsor Networks to Pajek", "Pajek", "paj", targetFile);
					rawData.setData(PAJFile.convertToMicrosoftEndOfLine(pajekBuffer2.toString()));
	
					result.outputs().appendln();
					result.outputs().append(rawData);
				}
				
				if (pajekBuffer1.length() != 0) {
					File targetFile = ToolBox.setExtension(ToolBox.addToName(new File("Transition graphs_"), criteria.getLocalUnitLabel()), ".paj");
					ReportRawData rawData = new ReportRawData("Export Transition Graphs to Pajek", "Pajek", "paj", targetFile);
					rawData.setData(PAJFile.convertToMicrosoftEndOfLine(pajekBuffer1.toString()));
	
					result.outputs().appendln();
					result.outputs().append(rawData);
				}
				
				
	
/*				result.outputs().append(reportOverview);
				result.outputs().append(reportMetrics);
				result.outputs().append(reportMorphology);
				result.outputs().append(reportReferentKin);
				result.outputs().append(reportKin);
				result.outputs().append(reportReferentChain);
				result.outputs().append(reportFlow);
				
				
	//			chainReport.outputs().appendln(relation+"\t"+relation.actors().size()+"\t"+RelationWorker.getLinkChainsAsString(relation, "GENDER", null)+"\t"+RelationWorker.getLinkChainsAsString(relation, "ID", null)+"\t"+RelationWorker.getLinkChainsAsString(relation, "KIN", criteria.getPattern()));
	//			result.outputs().append(chainReport);
	
				//
	//			result.setTimeSpent(chrono.stop().interval());
	//		}
		
			//
			return result;
		}*/

}
