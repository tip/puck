package org.tip.puck.graphs;

/**
 * 
 * @author TIP
 */
public enum GraphType {
	OreGraph,
	TipGraph,
	PGraph,
	BinaryRelationGraph,
	BimodalRelationGraph;
}
