package org.tip.puck.kinoath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.kinoath.Booking.Direction;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;

/**
 * 
 * @author TIP
 */
public class IndividualAroundWorker {

	private static final Logger logger = LoggerFactory.getLogger(IndividualAroundWorker.class);

	/**
	 * Returns individuals who are around of one specific individual, browsing
	 * using some criteria.
	 * 
	 * @param source
	 * @param criteria
	 * @return
	 */
	public static Individuals searchAround(final Individual source, final PruningCriteria criteria) {
		Individuals result;

		//
		result = new Individuals();

		//
		result.add(source);

		//
		Bookings bookings = new Bookings();
		bookings.booked().add(source);

		//
		if (criteria.getParentsDepth() > 0) {
			//
			for (Individual parent : source.getParents()) {
				//
				bookings.add(new Booking(parent, 1, Direction.UP));
			}
		}

		if (criteria.getChildrenDepth() > 0) {
			//
			for (Individual child : source.children()) {
				//
				bookings.add(new Booking(child, 1, Direction.DOWN));
			}
		}

		if (criteria.getPartnersDepth() > 0) {
			//
			for (Individual partner : source.getPartners()) {
				//
				bookings.add(new Booking(partner, 1, Direction.ASIDE));
			}
		}

		//
		while (!bookings.isEmpty()) {
			//
			Booking current = bookings.pop();

			if (!result.contains(current.getIndividual())) {
				//
				result.add(current.getIndividual());

				switch (current.getDirection()) {
					case UP:
						if (criteria.getParentsDepth() > current.getDepth()) {
							//
							for (Individual parent : current.getIndividual().getParents()) {
								//
								bookings.add(new Booking(parent, current.getDepth() + 1, Direction.UP));
							}

							if (criteria.isCollaterals()) {
								//
								if (criteria.getParentsDepth() > current.getDepth()) {
									//
									for (Individual child : current.getIndividual().children()) {
										//
										bookings.add(new Booking(child, current.getDepth() + 1, Direction.UP));
									}
								}

								if (criteria.getParentsDepth() > current.getDepth()) {
									//
									for (Individual partner : current.getIndividual().getPartners()) {
										//
										bookings.add(new Booking(partner, current.getDepth() + 1, Direction.UP));
									}
								}
							}
						}
					break;

					case DOWN:
						if (criteria.getChildrenDepth() > current.getDepth()) {
							//
							for (Individual child : current.getIndividual().children()) {
								//
								bookings.add(new Booking(child, current.getDepth() + 1, Direction.DOWN));
							}

							if (criteria.isAffines()) {
								//
								if (criteria.getChildrenDepth() > current.getDepth()) {
									//
									for (Individual parent : current.getIndividual().getParents()) {

										//
										bookings.add(new Booking(parent, current.getDepth() + 1, Direction.DOWN));
									}
								}

								if (criteria.getChildrenDepth() > current.getDepth()) {
									//
									for (Individual partner : current.getIndividual().getPartners()) {
										//
										bookings.add(new Booking(partner, current.getDepth() + 1, Direction.DOWN));
									}
								}
							}
						}
					break;

					case ASIDE:
						if (criteria.getPartnersDepth() > current.getDepth()) {
							//
							for (Individual partner : current.getIndividual().getPartners()) {
								//
								bookings.add(new Booking(partner, current.getDepth() + 1, Direction.ASIDE));
							}

							//
							if (criteria.getPartnersDepth() > current.getDepth()) {
								//
								for (Individual child : current.getIndividual().children()) {
									//
									bookings.add(new Booking(child, current.getDepth() + 1, Direction.ASIDE));
								}
							}

							//
							if (criteria.getPartnersDepth() > current.getDepth()) {
								//
								for (Individual parent : current.getIndividual().getParents()) {

									//
									bookings.add(new Booking(parent, current.getDepth() + 1, Direction.ASIDE));
								}
							}
						}
					break;
				}
			}
		}

		//
		return result;
	}
}
