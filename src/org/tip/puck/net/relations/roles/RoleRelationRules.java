package org.tip.puck.net.relations.roles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.tip.puck.net.relations.roles.RoleRelationMaker.RoleRelationRule;

/**
 * 
 * @author TIP
 */
public class RoleRelationRules extends ArrayList<RoleRelationRule> {

	private static final long serialVersionUID = -5567708086083074472L;

	/**
	 * 
	 */
	public RoleRelationRules() {
		super();
	}

	/**
	 * 
	 */
	public RoleRelationRules(final RoleRelationRules source) {
		super(source);
	}

	/**
	 * 
	 * @return
	 */
	public RoleRelationRule getFirst() {
		RoleRelationRule result;

		if (isEmpty()) {
			result = null;
		} else {
			result = get(0);
		}
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public RoleRelationRule getLast() {
		RoleRelationRule result;

		if (isEmpty()) {
			result = null;
		} else {
			result = get(size() - 1);
		}
		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public RoleRelationRule[] toArray() {
		RoleRelationRule[] result;

		result = new RoleRelationRule[this.size()];
		int itemCount = 0;
		for (RoleRelationRule item : this) {
			//
			result[itemCount] = item;
			itemCount += 1;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<RoleRelationRule> toList() {
		List<RoleRelationRule> result;

		result = new ArrayList<RoleRelationRule>(this.size());
		for (RoleRelationRule item : this) {
			result.add(item);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<RoleRelationRule> toSortedList() {
		List<RoleRelationRule> result;

		result = toList();
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<RoleRelationRule> toSortedListReverse() {
		List<RoleRelationRule> result;

		result = toList();
		Collections.sort(result, Collections.reverseOrder());

		//
		return result;
	}

}
