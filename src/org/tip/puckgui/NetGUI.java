package org.tip.puckgui;

import java.io.File;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckManager;
import org.tip.puck.io.iur.IURTXTFile;
import org.tip.puck.net.Families;
import org.tip.puck.net.Family;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.net.UnionStatus;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.report.Report;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.util.NumberablesHashMap.IdStrategy;
import org.tip.puck.util.ToolBox;
import org.tip.puckgui.views.MainWindow;
import org.tip.puckgui.views.RelationsPanel;

/**
 * 
 * NetGUI manages file and net received. It never create them itself.
 * 
 * @author TIP
 */
public class NetGUI implements WindowGUI {

	private static final Logger logger = LoggerFactory.getLogger(NetGUI.class);

	private int id;
	private File file;
	private Net net;
	private MainWindow window;
	private boolean changed;
	private Segmentation segmentation;

	/**
	 * Initializes a newly created NetGUI object so that it represents an empty
	 * net associated to a default given file.
	 * 
	 * @param folder
	 *            The associated file.
	 */
	public NetGUI(final int id) {
		//
		this.id = id;

		//
		this.file = new File(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("puckgui.defaultFileName"));
		this.net = new Net();
		this.net.setLabel(this.file.getName());
		this.changed = false;
		this.segmentation = new Segmentation(this.net);

		//
		this.window = new MainWindow(this);
	}

	/**
	 * Initializes a newly create NetGUI object so that it represents a net
	 * associated to a given file.
	 * 
	 * @param file
	 *            The associated file.
	 * @param net
	 *            The net.
	 */
	public NetGUI(final int id, final File file, final Net net) {
		//
		this.id = id;
		this.file = file;
		this.net = net;
		this.changed = false;
		this.segmentation = new Segmentation(this.net);

		//
		this.window = new MainWindow(this);

		//
		this.window.selectIndividualsTab(null);
	}

	/**
	 * Initializes a newly create NetGUI object so that it represents a net
	 * associated to a given file and a given segmentation.
	 * 
	 * Warning: the segmentation must use the same object lists (individuals,
	 * families, relations) than the net parameter.
	 * 
	 * @param file
	 *            The associated file.
	 * 
	 * @param net
	 *            The net.
	 * 
	 * @param segmentation
	 *            The segmentation.
	 */
	public NetGUI(final int id, final File file, final Net net, final Segmentation segmentation) {
		//
		this.id = id;
		this.file = file;
		this.net = net;
		this.changed = false;
		if (segmentation == null) {
			//
			this.segmentation = new Segmentation(this.net);

		} else {
			//
			this.segmentation = segmentation;
		}

		//
		this.window = new MainWindow(this);

		//
		this.window.selectIndividualsTab(null);
	}

	/**
	 * 
	 * @param report
	 */
	@Override
	public void addRawTab(final String tabTitle, final JPanel panel) {
		this.window.addRawTab(tabTitle, panel);
	}

	/**
	 * 
	 * @param report
	 */
	public void addRelationTab(final RelationModel relationModel) {
		this.window.addRelationTab(relationModel);
	}

	/**
	 * 
	 * @param report
	 */
	@Override
	public void addReportTab(final Report report) {
		this.window.addReportTab(report);
	}

	/**
	 * 
	 */
	@Override
	public void addReportTab(final String tabTitle, final JPanel panel) {
		this.window.addReportTab(tabTitle, panel);
	}

	/**
	 * 
	 * @param report
	 */
	public void addTab(final String tabTitle, final JPanel panel) {
		this.window.addTab(tabTitle, panel);
	}

	/**
	 * @throws PuckException
	 * 
	 */
	public void changeSegmentationToClear() throws PuckException {
		//
		if (!this.segmentation.isAtTheTop()) {
			//
			Individual selectedIndividual = selectedIndividual();

			//
			this.segmentation.clear();

			//
			if (selectedIndividual == null) {
				//
				changeSegmentationToCluster(0);

			} else {
				//
				changeSegmentationToCluster(selectedIndividual);
			}
		}
	}

	/**
	 * 
	 * @param clusterIndex
	 * @throws PuckException
	 */
	public void changeSegmentationToCluster(final Individual individual) throws PuckException {
		if (individual != null) {
			//
			this.segmentation.selectCluster(individual);

			//
			this.window.getSegmentationPanel().update();
			this.window.getIndividualsTab().update(individual);
			this.window.selectIndividualsTab();
			this.window.getFamiliesTab().update();
			for (RelationsPanel tab : this.window.getRelationTabs()) {
				tab.update();
			}
		}
	}

	/**
	 * 
	 * @param clusterIndex
	 * @throws PuckException
	 */
	public void changeSegmentationToCluster(final int clusterIndex) throws PuckException {
		//
		this.segmentation.selectCluster(clusterIndex);

		//
		this.window.getSegmentationPanel().update();
		this.window.getIndividualsTab().update();
		this.window.setIndividualsTab(null);
		this.window.getFamiliesTab().update();
		for (RelationsPanel tab : this.window.getRelationTabs()) {
			tab.update();
		}
	}

	/**
	 * @throws PuckException
	 * 
	 */
	public void changeSegmentationToDown() throws PuckException {
		if (!this.segmentation.isAtTheBottom()) {
			//
			changeSegmentationToPartition(this.segmentation.getCurrentSegmentIndex() + 1);
		}
	}

	/**
	 * 
	 * @param criteria
	 * @throws PuckException
	 */
	public void changeSegmentationToNew(final PartitionCriteria criteria) throws PuckException {
		//
		if (criteria != null) {
			//
			Individual selectedIndividual = selectedIndividual();

			//
			this.segmentation.addSegment(criteria);
			this.segmentation.selectCluster(selectedIndividual);

			//
			this.window.getSegmentationPanel().update();
			this.window.getIndividualsTab().update();
			this.window.getIndividualsTab().select(selectedIndividual);
			this.window.getFamiliesTab().update();
			for (RelationsPanel tab : this.window.getRelationTabs()) {
				tab.update();
			}
		}
	}

	/**
	 * 
	 * @param partitionIndex
	 * @throws PuckException
	 */
	public void changeSegmentationToPartition(final int partitionIndex) throws PuckException {

		if ((partitionIndex >= 0) && (partitionIndex < this.segmentation.size())) {
			//
			Individual selectedIndividual = selectedIndividual();
			Family selectedFamily = selectedFamily();

			//
			this.segmentation.selectPartition(partitionIndex);

			if (selectedIndividual != null) {
				this.segmentation.selectCluster(selectedIndividual);
			}

			//
			this.window.getSegmentationPanel().update();
			this.window.getIndividualsTab().update();
			this.window.getIndividualsTab().select(selectedIndividual);
			this.window.getFamiliesTab().update();
			this.window.getFamiliesTab().select(selectedFamily);
			for (RelationsPanel tab : this.window.getRelationTabs()) {
				tab.update();
			}
		}
	}

	/**
	 * Do an up and remove all segments below.
	 * 
	 * @throws PuckException
	 */
	public void changeSegmentationToPop() throws PuckException {
		//
		if (!this.segmentation.isAtTheTop()) {
			//
			changeSegmentationToUp();

			//
			this.segmentation.clearBelow();
			this.window.getSegmentationPanel().update();
		}
	}

	/**
	 * @throws PuckException
	 * 
	 */
	public void changeSegmentationToRoot() throws PuckException {
		//
		if (!this.segmentation.isAtTheTop()) {
			//
			changeSegmentationToPartition(0);
		}
	}

	/**
	 * @throws PuckException
	 * 
	 */
	public void changeSegmentationToUp() throws PuckException {
		//
		if (!this.segmentation.isAtTheTop()) {
			changeSegmentationToPartition(this.segmentation.getCurrentSegmentIndex() - 1);
		}
	}

	/**
	 * 
	 */
	@Override
	public void closeCurrentTab() {
		this.window.closeCurrentTab();
	}

	/**
	 * 
	 * @param relationModel
	 */
	public void closeRelationTab(final RelationModel relationModel) {
		this.window.closeCurrentTab();
		this.setChanged(true);
	}

	/**
	 * 
	 * @return
	 */
	public Families getCurrentFamilies() {
		Families result;

		result = this.segmentation.getCurrentFamilies();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individuals getCurrentIndividuals() {
		Individuals result;

		result = this.segmentation.getCurrentIndividuals();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Relations getCurrentRelations() {
		Relations result;

		result = this.segmentation.getCurrentRelations();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public UnionStatus getDefaultUnionStatus() {
		UnionStatus result;

		result = this.window.getIndividualsTab().getDefaultUnionStatus();

		//
		return result;
	}

	public File getFile() {
		return this.file;
	}

	@Override
	public int getId() {
		return this.id;
	}

	/**
	 * 
	 * @return
	 */
	public IdStrategy getIndividualIdStrategy() {
		IdStrategy result;

		result = this.window.getIndividualsTab().getIndividualIdStrategy();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public JFrame getJFrame() {
		JFrame result;

		result = this.window.getJFrame();

		//
		return result;
	}

	public Net getNet() {
		return this.net;
	}

	public Segmentation getSegmentation() {
		return this.segmentation;
	}

	/**
	 * 
	 * @return
	 */
	public Family getSelectedFamily() {
		Family result;

		result = this.window.getFamiliesTab().getSelectedFamily();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public String getTitle() {
		String result;

		result = getFile().getName() + "-" + getId() + " (Net)";

		//
		return result;
	}

	/**
	 * Tests if netGUI is able to be lost or not.
	 * 
	 * @return true if netGUI is blank, false otherwise.
	 */
	public boolean isBlank() {
		boolean result;

		if ((this.net.individuals().size() == 0) && (!this.file.getName().contains("."))) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	@Override
	public boolean isChanged() {
		return this.changed;
	}

	/**
	 * @throws PuckException
	 * 
	 */
	public void revert() throws PuckException {
		//
		int currentTabIndex = this.window.getCurrentTabIndex();

		//
		if (ToolBox.getExtension(getFile()) == null) {
			//
			setNet(new Net());
			setChanged(false);

		} else {
			//
			Net newNet = PuckManager.loadNet(getFile());
			setNet(newNet);
			setChanged(false);
		}

		//
		this.window.selectTab(currentTabIndex);
	}

	/**
	 * 
	 * @return
	 */
	public Family selectedFamily() {
		Family result;

		result = this.window.getFamiliesTab().getSelectedFamily();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individual selectedIndividual() {
		Individual result;

		result = this.window.getIndividualsTab().getSelectedIndividual();

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 */
	public void selectFamiliesTab(final Family family) {
		this.window.selectFamiliesTab(family);
	}

	/**
	 * 
	 * @param target
	 */
	public void selectIndividualsTab(final Individual individual) {
		this.window.selectIndividualsTab(individual);
	}

	/**
	 * 
	 * @param relation
	 */
	public void selectRelationTab(final Relation relation) {
		this.window.selectRelationTab(relation);
	}

	/**
	 * 
	 */
	public void setBlank() {
		//
		this.window.closeRelationTabs();

		//
		this.file = new File(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("puckgui.defaultFileName"));
		this.net = new Net();
		this.net.setLabel(this.file.getName());
		this.changed = false;
		this.segmentation = new Segmentation(this.net);

		//
		this.window.updateMenuItems();
		this.window.updateTitle();
		this.window.getSegmentationPanel().update();
		this.window.getCorpusTab().update();
		this.window.getIndividualsTab().update();
		this.window.getFamiliesTab().update();
		this.window.getIndividualsTab().select(null);
		this.window.getFamiliesTab().select(null);

		//
		this.window.closeVolatilTabs();
	}

	/**
	 * 
	 * @param changed
	 */
	@Override
	public void setChanged(final boolean changed) {
		//
		this.changed = changed;

		//
		this.window.updateMenuItems();
		this.window.updateTitle();
	}

	/**
	 * 
	 * @param changed
	 */
	public void setChanged(final boolean changed, final String additionalName) {
		//
		this.changed = changed;

		//
		if (ToolBox.getExtension(this.getFile()) != null) {
			this.file = ToolBox.addToName(this.getFile(), additionalName);
		}

		//
		this.window.updateMenuItems();
		this.window.updateTitle();
	}

	/**
	 * 
	 * @param file
	 */
	public void setFile(final File file) {
		this.file = file;
		this.net.setLabel(file.getName());

		//
		this.window.updateMenuItems();
		this.window.updateTitle();

		//
		this.window.getCorpusTab().update();
	}

	/**
	 * 
	 * @param net
	 */
	public void setNet(final Net net) {
		this.net = net;
		this.segmentation = new Segmentation(net);
		this.window.getSegmentationPanel().update();
		this.window.getCorpusTab().update();
		this.window.getIndividualsTab().update();
		this.window.selectIndividualsTab(null);
		this.window.getFamiliesTab().update();
		this.window.closeRelationTabs();
		for (RelationModel relationModel : net.relationModels()) {
			this.window.addRawTab(relationModel.getName(), new RelationsPanel(this, relationModel));
		}
		updateAttributeTemplates();
	}

	/**
	 * Place the window on top.
	 */
	@Override
	public void toFront() {
		this.window.toFront();
	}

	/**
	 * 
	 */
	public void updateAll() {
		//
		this.window.getSegmentationPanel().update();
		this.window.getCorpusTab().update();
		this.window.getIndividualsTab().update();
		// this.window.selectIndividualsTab(null);
		this.window.getFamiliesTab().update();
		if (this.net != null) {
			for (RelationModel relationModel : this.net.relationModels()) {
				this.window.getRelationTab(relationModel).update();
			}
		}
	}

	/**
	 * 
	 */
	public void updateAttributeTemplates() {
		//
		this.window.getIndividualsTab().updateAttributeTemplates();

		//
		// this.window.getFamilyTab().updateAttributeTemplates();

		//
		for (RelationModel relationModel : this.net.relationModels()) {
			//
			this.window.getRelationTab(relationModel).updateAttributeTemplates();
		}
	}

	/**
	 * 
	 * @param file
	 * @throws PuckException
	 */
	public void updateFile(final File file) throws PuckException {
		updateFile(file, IURTXTFile.DEFAULT_CHARSET_NAME);
	}

	/**
	 * 
	 * @param file
	 * @throws PuckException
	 */
	public void updateFile(final File file, final Net net) throws PuckException {
		//
		setChanged(false);
		setFile(file);
		setNet(net);
		this.window.selectIndividualsTab(null);
	}

	/**
	 * 
	 * @param file
	 * @throws PuckException
	 */
	public void updateFile(final File file, final String charsetName) throws PuckException {
		//
		Net newNet = PuckManager.loadNet(file, charsetName);

		setChanged(false);
		setFile(file);
		setNet(newNet);
		this.window.selectIndividualsTab(null);
	}

	/**
	 * 
	 */
	public void updateIndividualIdentity() {
		//
		this.window.getIndividualsTab().updateIndividualIdentity();
	}

	/**
	 * 
	 * 
	 * 
	 * @param locale
	 */
	@Override
	public void updateLocale(final Locale locale) {
		// this.window.updateLocale(locale);
		// TODO Implements way to not close current windows.
		int tabIndex = this.window.getCurrentTabIndex();
		this.window.dispose();

		//
		this.window = new MainWindow(this);
		if (tabIndex < 3) {
			this.window.selectTab(tabIndex);
		} else {
			this.window.selectIndividualsTab();
		}
	}

	/**
	 * 
	 */
	public void updateRelationIdentity(final RelationModel relationModel) {
		//
		this.window.getRelationTab(relationModel).updateRelationIdentity();
	}
}
