package org.tip.puck.partitions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.util.MathUtils;

/**
 * 
 * @author TIP
 */
public class Interval implements Comparable<Interval> {

	public enum EndpointStatus {
		INCLUDED,
		EXCLUDED
	}

	private static final Logger logger = LoggerFactory.getLogger(Interval.class);

	private double min;
	private boolean minIncluded;
	private double max;
	private boolean maxIncluded;

	/**
	 * 
	 * @param min
	 * @param max
	 */
	public Interval(final Double min, final EndpointStatus minStatus, final Double max, final EndpointStatus maxStatus) {

		this.minIncluded = (minStatus == EndpointStatus.INCLUDED);
		this.maxIncluded = (maxStatus == EndpointStatus.INCLUDED);

		if (min == null) {
			this.min = Double.MIN_VALUE;
		} else {
			this.min = min;
		}

		if (max == null) {
			this.max = Double.MIN_VALUE;
		} else {
			this.max = max;
		}
	}

	/**
	 * 
	 */
	@Override
	public int compareTo(final Interval source) {
		int result;

		if (source == null) {
			result = 1;
		} else if (this.min == source.getMin()) {
			if ((!this.minIncluded) && (source.isMinIncluded())) {
				result = +1;
			} else if ((this.minIncluded) && (!source.isMinIncluded())) {
				result = -1;
			} else {
				result = 0;
			}
		} else if (this.min < source.getMin()) {
			result = -1;
		} else {
			result = +1;
		}

		//
		return result;
	}

	public double getMax() {
		return this.max;
	}

	public double getMin() {
		return this.min;
	}

	public boolean isMaxIncluded() {
		return this.maxIncluded;
	}

	public boolean isMinIncluded() {
		return this.minIncluded;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public boolean matches(final double value) {
		boolean result;

		if ((value > this.min) && (value < this.max)) {
			result = true;
		} else if ((value == this.min) && (this.minIncluded)) {
			result = true;
		} else if ((value == this.max) && (this.maxIncluded)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public boolean matches(final Double value) {
		boolean result;

		if (value == null) {
			result = false;
		} else {
			result = matches(value.doubleValue());
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		char minBoard;
		if (this.minIncluded) {
			minBoard = '[';
		} else {
			minBoard = ']';
		}

		char maxBoard;
		if (this.maxIncluded) {
			maxBoard = ']';
		} else {
			maxBoard = '[';
		}

		//
		String targetMin = MathUtils.toString(this.min);

		//
		String targetMax = MathUtils.toString(this.max);

		result = String.format("%c%s,%s%c", minBoard, targetMin, targetMax, maxBoard);

		//
		return result;
	}
}
