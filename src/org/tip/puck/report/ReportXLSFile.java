package org.tip.puck.report;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;

/**
 * This class represents a report File writer.
 * 
 * @author TIP
 */
public class ReportXLSFile {

	private static final Logger logger = LoggerFactory.getLogger(ReportXLSFile.class);

	/**
	 * Saves a report in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File file, final Report source) throws PuckException {
		try {
			//
			WorkbookSettings settings = new WorkbookSettings();
			settings.setLocale(new Locale("fr", "FR"));
			WritableWorkbook workbook = Workbook.createWorkbook(file, settings);

			//
			XLSHelper out = new XLSHelper(workbook);
			writeGeneral(out, source);
			write(out, source);

			//
			workbook.write();
			workbook.close();

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} catch (WriteException exception) {
			throw PuckExceptions.IO_ERROR.create("Opening file [" + file + "]");
		} catch (IOException e) {
			throw PuckExceptions.IO_ERROR.create("Opening file [" + file + "]");
		}
	}

	/**
	 * Writes a report in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param net
	 *            Source.
	 * @throws IndexOutOfBoundsException
	 * @throws WriteException
	 * @throws RowsExceededException
	 */
	public static void write(final XLSHelper out, final Report source) throws RowsExceededException, WriteException, IndexOutOfBoundsException {
		//
		out.newPage(StringUtils.defaultString(source.title(), "notitle"));

		//
		for (Object output : source.outputs()) {
			if (output != null) {
				if (output instanceof ReportAttributes) {
					//
					ReportAttributes attributes = (ReportAttributes) output;

					//
					out.appendln("Attribute table");
					for (ReportAttribute item : attributes) {
						out.append(item.label()).appendln(item.value());
					}

					//
					out.appendln();

				} else if (output instanceof ReportChart) {
					//
					ReportChart chart = (ReportChart) output;

					//
					out.appendln("Chart: " + chart.getTitle());

					BufferedImage image = ReportChartMaker.createBufferedImage(chart, 640, 400);

					//
					out.appendln(image);

					//
					out.appendln();

				} else if (output instanceof ReportTable) {
					//
					ReportTable table = (ReportTable) output;

					//
					for (int rowIndex = 0; rowIndex < table.getRowCount(); rowIndex++) {
						for (int columnIndex = 0; columnIndex < table.getColumnCount(); columnIndex++) {
							out.append(table.getString(rowIndex, columnIndex));
						}
						out.appendln();
					}

					//
					out.appendln();

				} else if (output instanceof ReportRawData) {
					//
					ReportRawData rawData = (ReportRawData) output;

					//
					out.newPage("RAW DATA");
					out.appendln(rawData.getData());

					//
					out.appendln();

				} else if (output instanceof Report) {
					//
					Report report = (Report) output;

					//
					write(out, report);

					//
					out.appendln();

				} else {
					//
					out.appendln(output.toString());

					//
					out.appendln();
				}
			}
		}
	}

	/**
	 * Writes a report in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param net
	 *            Source.
	 * @throws IndexOutOfBoundsException
	 * @throws WriteException
	 * @throws RowsExceededException
	 */
	public static void writeGeneral(final XLSHelper out, final Report source) throws RowsExceededException, WriteException, IndexOutOfBoundsException {
		//
		out.newPage("General");

		//
		out.appendln("=== General information");
		out.appendln();

		out.append("Title:").appendln(source.title());
		out.append("Date:").appendln((new SimpleDateFormat("dd/MM/yyyy HH':'mm")).format(source.date().getTime()));
		out.append("Target:").appendln(source.target());
		out.append("Title:").appendln(source.timeSpent());
		out.appendln();

		//
		out.appendln("=== Inputs");
		out.append("Input comment:").appendln(source.inputComment());
		out.appendln("Input data:");
		for (ReportAttribute item : source.inputs()) {
			out.append(item.label()).appendln(item.value());
		}
		out.appendln();
	}
}
