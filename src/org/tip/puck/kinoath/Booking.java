package org.tip.puck.kinoath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.Individual;

/**
 * This class defines a booking. A booking is useful in browse algorithm to note
 * which individual has to be visited.
 * 
 * This class takes care about the direction of the browsing.
 * 
 * @see org.tip.puck.kinoath.IndividualAroundWorker
 * 
 * @author TIP
 */
public class Booking {

	public enum Direction {
		UP,
		DOWN,
		ASIDE
	}

	private Individual individual;
	private long depth;
	private Direction direction;

	private static final Logger logger = LoggerFactory.getLogger(Booking.class);

	/**
	 * 
	 * @param individual
	 * @param currentDepth
	 * @param direction
	 */
	public Booking(final Individual individual, final long currentDepth, final Direction direction) {
		this.individual = individual;
		this.depth = currentDepth;
		this.direction = direction;
	}

	/**
	 * 
	 * @return
	 */
	public long getDepth() {
		return this.depth;
	}

	/**
	 * 
	 * @return
	 */
	public Direction getDirection() {
		return this.direction;
	}

	/**
	 * 
	 * @return
	 */
	public Individual getIndividual() {
		return this.individual;
	}

	/**
	 * 
	 * @param depth
	 */
	public void setDepth(final long depth) {
		this.depth = depth;
	}

	/**
	 * 
	 * @param direction
	 */
	public void setDirection(final Direction direction) {
		this.direction = direction;
	}
}
