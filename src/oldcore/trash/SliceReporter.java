package oldcore.trash;




public class SliceReporter {

	
/*	public static Report reportEgoNetworks (RelationSetSequence slices, final StatisticsCriteria criteria) throws PuckException{
		Report result;
		
		if ((slices == null) || (criteria == null)) {
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
		} else {
			result = new Report("Ego Networks "+slices.relationModelName());
			Chronometer chrono = new Chronometer();

			result.setOrigin("Space reporter");
			
			SequenceCriteria censusCriteria = new SequenceCriteria(CensusType.EGONETWORKS);
			censusCriteria.setRelationModelName("RESIDENCE");
			censusCriteria.setEgoRoleName("RESIDENT");
			censusCriteria.setAlterFilterRoleName("ALL");


			SequencesCensus census = new SequencesCensus(slices.indiSequences(), censusCriteria);
			CensusType censusType = censusCriteria.getCensusType();
			Individuals members = slices.individuals();
			
			// Create Reports
			Report overallReport = new Report("Survey");
			Report diagramReport = new Report("Diagrams");
			Report detailReport = new Report("Details");
			Report componentReport = new Report("Components");
			Report treeReport = new Report("Trees");
					
			// Create Partition charts and tables
			List<ReportChart> charts = new ArrayList<ReportChart>();
			List<ReportTable> tables = new ArrayList<ReportTable>();
				
			// Make overall report and diagrams
			overallReport.outputs().appendln("Measure\tAverage (Male)\tAverage Pos. (Male)\tMedian (Male)\tMaximum (Male)\tSum (Male)\tAverage (Female)\tAverage Pos. (Female)\tMedian (Female)\tMaximum (Female)\tSum (Female)\tAverage (All)\tAverage Pos. (All)\tMedian (All)\tMaximum (All)\tSum (All)");

			// Set partition criteria 
			for (String label : censusCriteria.getCensusOperationLabels()){
				
				PartitionCriteria partitionCriteria = new PartitionCriteria(label);
				
				ReportChart chart = null;
				
				if (!label.contains("ALTERS") && !label.contains("PROFILE")){
					NumberedValues values = census.getValues(label);
					
					Partition<Relation> partition = PartitionMaker.create(label, slices.getStation(slices.getFirstTime()), values, partitionCriteria);
				
					PartitionCriteria splitCriteria = new PartitionCriteria(censusCriteria.getPartitionLabel());
					chart = StatisticsReporter.createPartitionChart(partition, partitionCriteria, null);

					overallReport.outputs().append(label+"\t");
					String sum = "";
					if (label.startsWith("NR")){
						sum = new Double(values.sum()).intValue()+"";
					}
					overallReport.outputs().append(MathUtils.round(values.average(),2)+"\t"+MathUtils.round(values.averagePositives(),2)+"\t"+values.median()+"\t"+values.max()+"\t"+sum+"\t");
					overallReport.outputs().appendln();
				}
			
				if (chart != null) {
					charts.add(chart);
					ReportTable table = ReportTable.transpose(chart.createReportTableWithSum());
					tables.add(table);
					if (!label.contains("EVENTS_") && !label.contains("RELATIONS")){
						tables.add(ReportTable.normalize(table));
					}
				}
			}
		
			overallReport.outputs().appendln();
		
			
		// Make overall report and diagrams
		overallReport.outputs().appendln("Measure\tAverage (Male)\tAverage Pos. (Male)\tMedian (Male)\tMaximum (Male)\tSum (Male)\tAverage (Female)\tAverage Pos. (Female)\tMedian (Female)\tMaximum (Female)\tSum (Female)\tAverage (All)\tAverage Pos. (All)\tMedian (All)\tMaximum (All)\tSum (All)");

		// Set partition criteria 
		for (String label : censusCriteria.getCensusOperationLabels()){
			
			PartitionCriteria partitionCriteria = new PartitionCriteria(label);
//			partitionCriteria.setValueFilter(ValueFilter.NULL);
			
/*			if (label.contains("PROFILE")){
				partitionCriteria.setType(PartitionType.PARTIALIZATION);
			} */
			
/*			if (label.equals("NREVENTS")){
				partitionCriteria.setType(PartitionType.RAW);
//				partitionCriteria.setCumulationType(CumulationType.DESCENDANT);
			} else if (label.contains("AGEFIRST")){
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(0.);
				partitionCriteria.setSize(5.);
			} else if (label.equals("ECCENTRICITY")){
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(-100.);
				partitionCriteria.setSize(20.);
			} else if (label.contains("COVERAGE") || label.contains("SAME")|| label.contains("NORM")|| label.contains("DENSITY")|| label.contains("BETWEENNESS") || label.contains("EFFICIENCY")|| label.contains("CONCENTRATION")){
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(0.);
				partitionCriteria.setSize(20.);
			} else if (label.contains("MEAN") || label.contains("COVERAGE") || label.contains("PEREVENT") || label.contains("BETWEENNESS")|| label.contains("BROKERAGE")|| label.contains("EFFICIENT_SIZE")){
				partitionCriteria.setType(PartitionType.SIZED_GROUPING);
				partitionCriteria.setStart(0.);
				partitionCriteria.setSize(1.);
			} else {
				partitionCriteria.setType(PartitionType.RAW);
			}
			
			ReportChart chart = null;
			
			if (label.contains("SIMILARITY")){
				RelationClassificationType relationClassificationType = RelationClassificationType.valueOf(label.substring(label.lastIndexOf("_")+1));
				Map<Value,Double[]> similaritiesMap = census.getSimilaritiesMap(relationClassificationType);
				chart = StatisticsReporter.createMapChart(similaritiesMap, label, new String[]{"HH", "FH", "HF", "FF","All"},GraphType.LINES);
				
				for (Value key : similaritiesMap.keySet()){
					overallReport.outputs().appendln(label+"_"+key+"\t"+MathUtils.percent(similaritiesMap.get(key)[4],100));
				}
				
				
			} else if (!label.contains("ALTERS") && !label.contains("PROFILE")){
				NumberedValues values = census.getValues(label);
				
				Partition<Individual> partition = PartitionMaker.create(label, members, values, partitionCriteria);
			
				PartitionCriteria splitCriteria = new PartitionCriteria(censusCriteria.getPartitionLabel());
				chart = StatisticsReporter.createPartitionChart(partition, partitionCriteria, splitCriteria);

				if (label.substring(0, 3).equals("AGE")){
					
					partitionCriteria.setType(PartitionType.RAW);
					partitionCriteria.setSizeFilter(SizeFilter.HOLES);
					partitionCriteria.setValueFilter(ValueFilter.NULL);
				
					partition = PartitionMaker.create(label, members, values, partitionCriteria);
					
					if (partition.maxValue()!=null){
						ReportChart survivalChart = StatisticsReporter.createSurvivalChart(partition, splitCriteria);
						charts.add(survivalChart);
					} else {
						System.err.println(label+" no max value");
					}
				}
				
				NumberedValues[] genderedValues = PuckUtils.getGenderedNumberedValues(values, members);
				
				overallReport.outputs().append(label+"\t");
				for (int gender=0;gender<3;gender++){
					String sum = "";
					if (label.startsWith("NR")){
						sum = new Double(genderedValues[gender].sum()).intValue()+"";
					}
					overallReport.outputs().append(MathUtils.round(genderedValues[gender].average(),2)+"\t"+MathUtils.round(genderedValues[gender].averagePositives(),2)+"\t"+values.median()+"\t"+genderedValues[gender].max()+"\t"+sum+"\t");
				}
				overallReport.outputs().appendln();
				
			}
		
			if (chart != null) {
				charts.add(chart);
				if (label.equals("SIMILARITY")){
					tables.add(ReportTable.transpose(chart.createReportTable()));
				} else {
					ReportTable table = ReportTable.transpose(chart.createReportTableWithSum());
					tables.add(table);
					if (!label.contains("EVENTS_") && !label.contains("RELATIONS")){
						tables.add(ReportTable.normalize(table));
					}
				}

			}
		
		}
		overallReport.outputs().appendln();
		
		// Create detailed report
		detailReport.outputs().append("Nr\tEgo\tGender");
		for (String partitionLabel : censusCriteria.getCensusOperationLabels()){
			if (partitionLabel.contains("SIMILARITY")){
				RelationClassificationType relationClassificationType = RelationClassificationType.valueOf(partitionLabel.substring(partitionLabel.lastIndexOf("_")+1));
				detailReport.outputs().append("\tSIMILARITY_PARENT_"+relationClassificationType+"\tSIMILARITY_CHILD_"+relationClassificationType+"\tSIMILARITY_SIBLING_"+relationClassificationType+"\tSIMILARITY_SPOUSE_"+relationClassificationType);
			} else {
				detailReport.outputs().append("\t"+partitionLabel);
			}
		}
		
		Map<String,Map<String,Double>> componentChartMap = new TreeMap<String,Map<String,Double>>();
		
		detailReport.outputs().appendln();
		for (Individual ego : members.toSortedList()){
			
			if ((((censusType==CensusType.GENERAL) || (censusType==CensusType.PARCOURS) || (censusType==CensusType.SEQUENCENETWORKS) ) && (census.getValues("NREVENTS").get(ego.getId())!=null)) || ((censusType == CensusType.EGONETWORKS) && (census.getValues("SIZE").get(ego.getId())!=null))) {
				detailReport.outputs().append(ego.getId()+"\t"+ego+"\t"+ego.getGender());
				for (String label : censusCriteria.getCensusOperationLabels()){
					if (label.contains("SIMILARITY")){
						Value value = census.getValues(label).get(ego.getId());
						Map<Value,Double[]> indiSimilaritiesMap = (Map<Value,Double[]>)value.mapValue();
						String[] keys = new String[]{"PARENT","CHILD","SIBLING","SPOUSE"};
						for (String key : keys){
							Double[] sim = indiSimilaritiesMap.get(new Value(key));
							if (sim!=null){
								detailReport.outputs().append("\t"+MathUtils.round(sim[4], 2));
							}
						}
					} else {
						detailReport.outputs().append("\t"+census.getValues(label).get(ego.getId()));
					}
				}
				detailReport.outputs().appendln();
			}
			
			if ((censusType==CensusType.EGONETWORKS || censusType ==CensusType.SEQUENCENETWORKS)){
				
				for (String networkTitle : censusCriteria.getNetworkTitles()){
					Map<Integer,Partition<Node<Individual>>> componentsMap = census.getComponents(networkTitle);
					if (componentsMap!=null){
						Partition<Node<Individual>> components = componentsMap.get(ego.getId());
						
						componentReport.outputs().appendln("Components "+networkTitle);
						componentReport.outputs().appendln(ego+"\t"+components.size());
						int i=1;
						for (Cluster<Node<Individual>> cluster : components.getClusters().toListSortedByValue()){
							componentReport.outputs().appendln("\t"+i+"\t"+cluster.getValue()+"\t("+cluster.size()+")\t"+cluster.getItemsAsString());
							i++;
						}
						componentReport.outputs().appendln();
						
						for (Value value : components.getValues()){
							String label = value.toString();
							Map<String,Double> map = componentChartMap.get(label);
							if (map==null){
								map = new TreeMap<String,Double>();
								for (Gender gender : Gender.values()){
									map.put(gender.toString(), 0.);
								}
								componentChartMap.put(label, map);
							}
							map.put(ego.getGender().toString(), map.get(ego.getGender().toString())+1);
						}
					}
				}
			}
		}
		
		if ((censusType==CensusType.EGONETWORKS || censusType ==CensusType.SEQUENCENETWORKS)){
			ReportChart componentChart = StatisticsReporter.createChart("COMPONENTS", componentChartMap);
			charts.add(componentChart);
			tables.add(ReportTable.transpose(componentChart.createReportTableWithSum()));
		
			if (census.getRelationConnectionMatrix()!=null){
				for (ReportChart chart : census.getRelationConnectionMatrix().getCharts()){
					charts.add(chart);
				}
				tables.add(census.getRelationConnectionMatrix().getTable("Component Connections"));
			}
		
		}

		
		// SequenceAnalysis
		
		if (censusType == CensusType.PARCOURS){
			
			for (RelationClassificationType relationClassificationType : censusCriteria.getMainRelationClassificationTypes()){
				
				if (censusCriteria.getNetworkTitles().contains("Event Type Network")){

					CorrelationMatrix eventSequenceMatrix = census.getEventSequenceMatrix(relationClassificationType.toString());
					
					if (eventSequenceMatrix!=null){
						for (ReportChart chart : eventSequenceMatrix.getCharts()){
							charts.add(chart);
						}
						tables.add(eventSequenceMatrix.getTable("Event Type Sequences"));
					}
					
					overallReport.outputs().appendln();
					overallReport.outputs().appendln("Sequence Network Statistics "+relationClassificationType);
					overallReport.outputs().appendln("\tDensity\tInertia\t(Divergence)\tConcentration\t(Divergence)\tSymmetry\t(Divergence)\tCentral nodes");
					
					for (Gender gender : Gender.values()){
						GraphProfile<Cluster<String>> profile = eventSequenceMatrix.getProfile(gender);

						String centralReferents = "";
						for (Cluster<String> centralReferent : profile.getCentralReferents()){
							centralReferents+=centralReferent.getValue()+" ";
						}
						double maxBetweenness = profile.getMaxBetweenness();
						double density = profile.density();
						double endo = MathUtils.round(profile.getStatistics(Indicator.LOOPS, Mode.NORMALIZED),2);
						double endoExp = MathUtils.round(profile.getStatistics(Indicator.LOOPS, Mode.DIVERGENCE_NORMALIZED),2);
						double conc = MathUtils.round(profile.getStatistics(Indicator.CONCENTRATION, Mode.SIMPLE),2);
						double concExp = MathUtils.round(profile.getStatistics(Indicator.CONCENTRATION, Mode.DIVERGENCE),2);
						double sym = MathUtils.round(profile.getStatistics(Indicator.SYMMETRY, Mode.SIMPLE),2);
						double symExp = MathUtils.round(profile.getStatistics(Indicator.SYMMETRY, Mode.DIVERGENCE),2);

						overallReport.outputs().appendln(gender+"\t"+density+"\t"+endo+"\t"+endoExp+"\t"+conc+"\t"+concExp+"\t"+sym+"\t"+symExp+"\t"+centralReferents +"("+maxBetweenness+") betweenness centrality");
					}
					overallReport.outputs().appendln();

				}
				
				if (censusCriteria.getNetworkTitles().contains("Sequence Type Network")){

					CorrelationMatrix subSequenceMatrix = census.getSubSequenceMatrix(relationClassificationType.toString());
					
					if (subSequenceMatrix!=null){
						charts.add(subSequenceMatrix.getRamificationChart());
					}
				}
			}
		}
		
		// Manage the number of chart by line.
		for (int chartIndex = 0; chartIndex < charts.size(); chartIndex++) {
			diagramReport.outputs().append(charts.get(chartIndex));
			if (chartIndex % 4 == 3) {
				diagramReport.outputs().appendln();
			}
		}
		
		// Add chart tables.
		for (ReportTable table : tables) {
			diagramReport.outputs().appendln(table.getTitle());
			diagramReport.outputs().appendln(table);
		}
		
		// Finalize reports
		result.outputs().append(overallReport);
		result.outputs().append(diagramReport);
		result.outputs().append(detailReport);
		
		if (censusType == CensusType.EGONETWORKS || censusType ==CensusType.SEQUENCENETWORKS){
			result.outputs().append(componentReport);
		}
		if (censusType == CensusType.PARCOURS) {
			result.outputs().append(treeReport);
		}
		
		//addPajekData
		
		Map<String,StringList> pajekBuffers = census.getPajekBuffers();
		
		for (String title : pajekBuffers.keySet()){
			
			StringList pajekBuffer = pajekBuffers.get(title);
			if (pajekBuffer.length() != 0) {
				File targetFile = ToolBox.setExtension(ToolBox.addToName(new File(slices.relationModelName()), "-"+title), ".paj");
				ReportRawData rawData = new ReportRawData("Export "+title+"s to Pajek", "Pajek", "paj", targetFile);
				rawData.setData(PAJFile.convertToMicrosoftEndOfLine(pajekBuffer.toString()));

				result.outputs().appendln();
				result.outputs().append(rawData);
			}
		}
		

		//
		result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}*/
	
	
/*	public static Report reportResidenceSequence(Segmentation segmentation, SequenceCriteria criteria){
		Report result;
		
		Integer[] dates = criteria.getDates();
		String dateLabel = criteria.getDateLabel();
		String idLabel = criteria.getConstantAttributeLabel();
		String modelName = criteria.getRelationModelName();
		String placeLabel = criteria.getPlaceLabel();
		String startDateLabel = criteria.getStartDateLabel();
		String endDateLabel = criteria.getEndDateLabel();
				
		result = new Report(modelName+" Sequence");
		
		Relations relations = segmentation.getAllRelations().getByModelName(modelName);
		
		Slices slices = SliceMaker.createSlices(segmentation, criteria);
		
		Map<Integer,Map<Individual,String>> relationsByIndividuals = new TreeMap<Integer,Map<Individual,String>>();
		Map<Integer,Individuals> relationsByAttributeValues = new TreeMap<Integer,Individuals>();
		
		for (Relation relation : relations){
			
			String dateAsString = relation.getAttributeValue(dateLabel);
			
			if (dateAsString!=null){
	
				Integer date = Integer.parseInt(dateAsString);
				if (relationsByIndividuals.get(date)==null){
					relationsByIndividuals.put(date, new TreeMap<Individual,String>());
				}
				
				for (Actor actor : relation.actors()){
					
					Individual member = actor.getIndividual();
					
					String status = "";
					
					if (relation.getAttributeValue(idLabel)!=null){
						status += relation.getAttributeValue(idLabel);
					} else if (relation.getAttributeValue(placeLabel)!=null){
						status += relation.getAttributeValue(placeLabel);
					}
					if (actor.getAttributeValue("MODE")!=null){
						status += " "+actor.getAttributeValue("MODE");
					}
	
					status+="\t";
	
					if (actor.getAttributeValue(startDateLabel)!=null){
						status += actor.getAttributeValue(startDateLabel);
					}
					if (actor.getAttributeValue(endDateLabel)!=null){
						status += "-"+actor.getAttributeValue(endDateLabel);
					}
					if (actor.getAttributeValue("NOTE")!=null){
						status += " ["+actor.getAttributeValue("NOTE")+"]";
					}
					
					relationsByIndividuals.get(date).put(member, status);
					
					String idValue = relation.getAttributeValue(idLabel);
					if (idValue!=null && Arrays.asList(dates).contains(date)){
						Integer houseId = Integer.parseInt(idValue);
						Individuals individuals = relationsByAttributeValues.get(houseId);
						if (individuals == null){
							individuals = new Individuals();
							relationsByAttributeValues.put(houseId, individuals);
						}
						individuals.put(member);
					}
				}
			}
		}
		
		StringList list = new StringList();
		
		for (Integer houseId : relationsByAttributeValues.keySet()){
			list.appendln(houseId);
			list.appendln();
			
			List<Individual> individuals = relationsByAttributeValues.get(houseId).toSortedList(Sorting.BIRT_YEAR);
			for (Individual individual : individuals){
				list.append(individual.signature()+" ("+IndividualValuator.lifeStatusAtYear(individual, 2015)+")\t");
				for (Integer date : dates){
					String value = relationsByIndividuals.get(date).get(individual);
					if (value!=null){
						list.append(value+"\t");
					} else {
						Integer deathYear = IndividualValuator.getDeathYear(individual);
						Integer birthYear = IndividualValuator.getBirthYear(individual);
						
						if (deathYear!=null && deathYear<=date){
							list.append("+"+deathYear+"\t\t");
						} else if (birthYear!=null && birthYear>=date){
							list.append("*"+birthYear+"\t\t");
						} else {
							list.append("?\t\t");
						}
					}
				}
				list.appendln();
				
			}
			list.appendln();
		}
		result.outputs().append(list);
		
		//
		return result;
		
	}*/

	
	
	
}
