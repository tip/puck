package org.tip.puck.visualization.style.implementations;

import org.gephi.data.attributes.api.AttributeModel;
import org.gephi.data.attributes.api.AttributeOrigin;
import org.gephi.data.attributes.api.AttributeTable;
import org.gephi.data.attributes.api.AttributeType;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.Node;
import org.gephi.layout.api.LayoutController;
import org.gephi.layout.plugin.force.StepDisplacement;
import org.gephi.layout.spi.Layout;
import org.gephi.preview.api.PreviewController;
import org.gephi.preview.api.PreviewModel;
import org.gephi.preview.api.PreviewProperty;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;
import org.tip.gephiplugins.genealogicalrenderers.DashedEdgeRenderer;
import org.tip.puck.graphs.Link;
import org.tip.puck.visualization.layouts.simpa.SimPaLayoutUpdated;
import org.tip.puck.visualization.layouts.sugiyama.SugiyamaLayout;
import org.tip.puck.visualization.style.Style;
import org.tip.puck.visualization.style.attributes.AttributeMap;
import org.tip.puck.visualization.style.attributes.GraphStyle;

/**
 * @date May 13, 2012
 *
 * @author Edoardo Savoia
 */
public class PGraph implements Style {

    public static final String EDGE_DASHED_PROP = DashedEdgeRenderer.BOUND_PROP_DEFAULT;
    /*
     * P-graph: non-valued graph with two types of arcs
     *
     * Solid arcs for male links,
     *
     * Broken arcs for female links,
     *
     * Layers according to generation
     */
    private final AttributeMap map = new AttributeMap() {

	@Override
	public void setAttributes(Node gephiNode, org.tip.puck.graphs.Node<?> graphNode) {
	}

	@Override
	public void setAttributes(Edge gephiEdge, Link<?> graphEdge) {
	    // set dashed (Female -> -1) / normal (Male -> 1) link
	    int weightAsInt = graphEdge.getWeightAsInt();
	    if (weightAsInt == -1) {
		// dashed link
		gephiEdge.getEdgeData().getAttributes().setValue(EDGE_DASHED_PROP.toLowerCase(), Boolean.TRUE);
	    }else {
	    	gephiEdge.getEdgeData().getAttributes().setValue(EDGE_DASHED_PROP.toLowerCase(), Boolean.FALSE);
	    }

	}

	@Override
	public void registerAttributes(AttributeModel attributeModel) {
	    AttributeTable table = attributeModel.getEdgeTable();

	    table.addColumn(EDGE_DASHED_PROP.toLowerCase(),
		    EDGE_DASHED_PROP,
		    AttributeType.BOOLEAN,
		    AttributeOrigin.DATA,
		    Boolean.FALSE);
	}
    };

    @Override
    public GraphStyle graphStyle() {
	return GraphStyle.directed;
    }

    @Override
    public AttributeMap styleMapping() {
	return map;
    }

    @Override
    public void applyStyle(Workspace workspace) {
	layout(workspace);
	PreviewController previewController = Lookup.getDefault().lookup(PreviewController.class);
	PreviewModel previewModel = previewController.getModel(workspace);

	previewModel.getProperties().putValue(PreviewProperty.SHOW_NODE_LABELS, Boolean.FALSE);
	previewModel.getProperties().putValue(PreviewProperty.DIRECTED, Boolean.TRUE);
	previewModel.getProperties().putValue(PreviewProperty.EDGE_CURVED, Boolean.FALSE);
	previewModel.getProperties().putValue(DashedEdgeRenderer.ENABLE_DASHED, Boolean.TRUE);

	previewController.refreshPreview(workspace);

    }

    
    private void layout(Workspace workspace) {
	// Layout
	GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getModel(workspace);
	LayoutController lookup = Lookup.getDefault().lookup(LayoutController.class);
	
	// SimPaLayoutUpdated buildLayout = new SimPaLayoutUpdated(null, new StepDisplacement(1f));
	Layout buildLayout = new SugiyamaLayout(null);
	buildLayout.setGraphModel(graphModel);
	buildLayout.resetPropertiesValues();


	buildLayout.initAlgo();

	buildLayout.goAlgo();

	buildLayout.endAlgo();
	
	/*
	//Run YifanHuLayout for 100 passes - The layout always takes the current visible view
	YifanHuLayout layout = new YifanHuLayout(null, new StepDisplacement(1f));
	layout.setGraphModel(graphModel);
	layout.resetPropertiesValues();
	layout.setOptimalDistance(200f);

	layout.initAlgo();
	for (int i = 0; i < 50 && layout.canAlgo(); i++) {
	    layout.goAlgo();
	}
	layout.endAlgo();
	*/
	
	/*
	ForceAtlas2 layout = new ForceAtlas2(null);
	layout.setGraphModel(graphModel);
	layout.resetPropertiesValues();

	layout.initAlgo();
	for (int i = 0; i < 50 && layout.canAlgo(); i++) {
	    layout.goAlgo();
	}
	layout.endAlgo();
	*/
    }
}
