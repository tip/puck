package oldcore.trash;

import java.util.ArrayList;
import java.util.Set;


import org.jfree.data.category.DefaultCategoryDataset;
import org.tip.puck.census.chains.Notation;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;

/**
 * This class defines the clusters of a Partition
 * @author Klaus Hamberger
 * @since Version 0.6
 * @param <E> the type of nodes that composes the Cluster
 */
@SuppressWarnings("serial")
public class Cluster< E extends Clusterable> extends ArrayList<E>  {
	
	private Object value;
	
	private int weight;
	private int netWeight; //rename value2? 
	
	/**
	 * the array of output, input and overall degrees
	 */
	private int[] degree;
	/**
	 * the map of (valued) links from the node to other nodes
	 * @since 11-05-22
	 */
	private CountMap<Cluster<E>> links;
	/**
	 * the map of (valued) links to the node from other nodes
	 */
	private CountMap<Cluster<E>> invLinks;
	
		
/*	public Object getVal() {
		return value;
	}*/

/*	public void setValueTo(Object value) {
		this.value = value;
	}*/

	/**
	 * the default constructor
	 */
	public Cluster () {}
	
/*	public Cluster(Object value){
		this.value = value;
	}*/
		
	/**
	 * constructs a cluster from another cluster and attaches a new heading to it
	 * @param v the cluster heading
	 * @param c the cluster to be copied
	 * @see partitions.Partition#putAll(Cluster, Object)
	 */
/*	public Cluster(Object v, Cluster<E> c){
		value = v;
		addAll(c);
	}*/
		
	/**
	 * constructs a cluster with a given heading and adds a first element
	 * @param value the cluster value
	 * @param item the first cluster element
	 * @see partitions.Partition#put(E,Object)
	 */
	public Cluster (Object value, E item){
		this.value = value;
		add(item);
	}
		
	/**
	 * compares two clusters according to their headings
	 * @param e the cluster to be compared
	 * @return the comparison index (1, 0 or -1)
	 */
/*	public int compareTo (Cluster<E> otherCluster){
			if (otherCluster.getVal()==null) return 1;
			if (getVal() instanceof Double) return (((Double)getVal()).compareTo((Double)otherCluster.getVal()));
			if (getVal() instanceof Integer) return (((Integer)getVal()).compareTo((Integer)otherCluster.getVal()));
			if (getVal() instanceof String) {
				if (((String)getVal()).matches("((-|\\+)?[0-9]+(\\.[0-9]+)?)+")){
					return new Integer((String)getVal()).compareTo(new Integer((String)otherCluster.getVal()));
				}
				return (((String)getVal()).compareTo((String)otherCluster.getVal()));
			}
			if (getVal() instanceof int[]) return new VectorComparator().compare((int[])getVal(),(int[])otherCluster.getVal(),true);
		return 0;
	}*/
	
/*	public boolean equals (Object otherCluster){
		return compareTo((Cluster<E>)otherCluster)==0;
	}*/
	
	
/*	public boolean contains (Object obj){
		for (E other : this){
			if (other.equals(obj)) return true;
		}
		return false;
	}*/
		
	/**
	 * counts the vertices in the cluster (or in the classes represented by the ClassVertices of the cluster)
	 * @return the number of vertices belonging to the cluster
	 */
/*	public int count(){
		int k=0;
		try{
			for (E e : this) {
				k = k+((GroupNode)e).getValue();
			}
			return k;
		} catch (ClassCastException cce){
			return size();
//		}
	}*/
		
	/**
	 * gets the ID of the ith cluster element
	 * @return the ID of the ith cluster element
	 */
/*	public int getID(int i){
		return ((Individual)get(i)).getId();
	}*/

	/**
	 * gets the mean level of vertices in the cluster
	 * @return the mean level of vertices
	 * @since 10-11-06
	 */
/*	public double getMeanLevel(){
		double g = 0;
		for (Object a : this){
			g = g + g.getLevel();
		}
		return g/size();
	}*/

/*	boolean hasHeading (){
		if (heading==null) return false;
		return true;
	}*/
		
	/**
	 * checks whether the cluster has a given heading
	 * @param v the cluster heading searched for 
	 * @return true if the cluster has this heading
	 * @see partitions.Partition#get(Object)
	 * @see OldNodePartition.NodePartition#getIndex(Individual)
	 */
/*	public boolean hasValueEqualTo (Object value){
		if (this.value==null) return false;
		if (this.value.equals(value)) return true;
		return false;
	}*/
		
		
	/**
	 * checks whether the cluster has null or zero heading
	 * @return true if the cluster has null or zero heading
	 * @deprecated see puck.Cluster.isNull
	 */
/*	public boolean isNullCluster (){
		return (getVal()==null || getVal().equals(0));
	}*/
	
	/**
	 * renumbers the nodes of the cluster in continuous order
	 */
/*	public void renumber(){
		int i=1;
		for (IVertex v: this){
			v.setCurrent(i);
			i++;
		}
	}*/
		
	//Generalize to heading + count for partition report? 
	/**
	 * returns a signature of the cluster
	 * @param i the signature key 
	 * <p> 0 simple heading, 1 heading and size
	 * @return the signature of the cluster
	 */
/*	public String signature(int i){
		if (i==1) return asString(getVal())+"\t"+size();
		return asString(getVal());
	}*/
		
	/**
	 * gets the size of a gendered subcluster (containing only vertices of given gender)
	 * @param g the gender to be filtered
	 * @return the size of the gendered subcluster
	 * @see OldNodePartition.NodePartition#affiliationStatistics(String, String)
	 * @see OldNodePartition.NodePartition#relationStatistics()
	 */
/*	public int size (Gender g){
		int k = 0;
		for (E e : this) {
			if (((Individual)e).getGender()==g) k++;
		}
		return k;
	}*/
		
	//Simplify
	//Check parameter cumul
	/**
	 * adds the cluster size and value to a dataset and returns the cumulated size of all clusters already counted
	 * @param dataset the dataset
	 * @param cumul a cumulation index (+1 ascending, -1 descending, 0 no cumulation)
	 * @param cum the cumulated size of clusters already counted
	 * @param tot the total size of all clusters of the partition
	 * @param normalize true if the cluster size shall be normalized (as a percentage of total Net size)
	 * @param net the underlying Net
	 * @param part the underlying Partition
	 * @return the cumulated size of clusters already counted (including this cluster)
	 * @see partitions.NodePartition#getDataset(String)
	 */
/*	public int toDataset (DefaultCategoryDataset dataset, int cumul, int cum, int tot, boolean normalize, NodeMap<Vertex> net, Partition<Node> part) {
		if (isNullCluster()) return 0;
		int count = count()+cum;
		int cnt = count;
		if (cumul==-1 && !isNullCluster()) cnt=tot-cum;
		if (normalize) cnt = 100*cnt/net.size();
		if (part==null) dataset.addValue(cnt,"",signature(0));
		else dataset.addValue(cnt,signature(0),part.getName());
		if (cumul!=0) cum=count;
		return cum;
	}*/
	
	/**
	 * gets the number of arcs to the vertex
	 * @return the indegree of the vertex (number of arcs to the vertex)
	 * @see maps.OldGroupNet#pValue
	 */
/*	public int getInDegree (){
		if (degree==null) return 0;
		return degree[1];
	}*/
	
	//auxiliary method for writing allies in both directions, too complicated
	/**
	 * 
	 * @param net
	 * @return
	 * @see io.write.AbstractWriter#writeAllies(GroupNet)
	 */
/*	public GroupNode getInverse (GroupNet net){
		return (GroupNode)net.get(getID());
	}*/
	
	/**
	 * gets the links of the vertex
	 * @return the list of links of the vertex
	 */
/*	public CountMap<Cluster<E>> getLinks() {
		return links;
	}*/

	/**
	 * gets the links to the vertex
	 * @return the list of links to the vertex
	 */
/*	public CountMap<Cluster<E>> getInvLinks() {
		return invLinks;
	}*/
	
	/**
	 * gets the mean generational level of vertices in the cluster
	 * @return the mean level of vertices
	 * @since 10-11-06
	 */
/*	public double getMeanLevel(Partition<E> generations){
		double g = 0;
		for (E a : this){
			g += (Integer)generations.getVal(a);
		}
		return g/size();
	}	*/
	
	/**
	 * gets the net value of the vertex
	 * <p> used for ring intersection networks
	 * @return the net value of the vertex
	 */
/*	public int getNetWeight() {
		return netWeight;
	}*/
	
	/**
	 * gets the number of arcs from the vertex
	 * @return the outdegree of the vertex (number of arcs from the vertex)
	 * @see maps.OldGroupNet#pValue
	 */
/*	public int getOutDegree (){
		if (degree==null) return 0;
		return degree[0];
	}*/
	
	/**
	 * returns the value, netvalue, or autovalue of the vertex according to the chosen key
	 * @param t the key (0 value, 1 netvalue, 2 autovalue)
	 * @return the value of the chosen key
	 */
/*	public int getWeight (int t){
		if (t==0) return weight;
		if (t==1) return netWeight;
		if (t==2) return getAutoWeight();
		return 0;
	}*/

	/**
	 * set the degrees of the vertex
	 * @param i the degree index (0 indegree, 1 outdegree)
	 * @param j the degree
	 * @see GroupNode#setDegrees()
	 */
/*	private void setDegree(int i, int j){
		if (degree==null) degree = new int[2];
		degree[i]=j;
	}*/

	/**
	 * set the degrees of the vertex
	 * @see maps.OldGroupNet#getDegree()
	 */
/*	public void setDegrees (){
		if (getLinks()==null) return;
		for (Cluster<E> v : getLinks().keySet()){
			int d = getLinks().get(v);
			setDegree(0,getOutDegree()+d);
			v.setDegree(1,v.getInDegree()+d);
		}
	}*/
	
	/**
	 * sets a link (with value 1) to another vertex
	 * @param v the alter vertex
	 * @see maps.OldGroupNet#ClassNet(order.Part, maps.Net)
	 * @see maps.OldGroupNet#ClassNet(maps.RingGroupMap, boolean)
	 */
/*	public void setLink (Cluster<E> v) {
		setLink(v,1);
	}*/
	
	
	/**
	 * sets a link (with value 1) from another vertex
	 * @param v the alter vertex
	 */
/*	public void setInvLink (Cluster<E> v) {
		setInvLink(v,1);
	}*/

	/**
	 * sets a link with given value to another vertex
	 * @param v the alter vertex
	 * @param k the value of the link
	 * @see maps.OldGroupNet#ClassNet(OldGroupNet)
	 * @see maps.OldGroupNet#ClassNet(maps.ListMap, int[])
	 */
/*	public void setLink (Cluster<E> v, int k) {
		if (getLinks()==null) setLinks(new CountMap<Cluster<E>>());
		getLinks().add(v,k);
	}*/

	/**
	 * sets a link with given value from another vertex
	 * @param v the alter vertex
	 * @param k the value of the invLink
	 */
/*	public void setInvLink (Cluster<E> v, int k) {
		if (getInvLinks()==null) setInvLinks(new CountMap<Cluster<E>>());
		getInvLinks().add(v,k);
	}*/
	
	/**
	 * sets the links of a vertex
	 * @param links the list of links of the vertex
	 * @see GroupNode#setLink(GroupNode, int)
	 */
/*	private void setLinks(CountMap<Cluster<E>> links) {
		this.links = links;
	}*/

	/**
	 * sets the inverse links of a vertex
	 * @param links the list of links to the vertex
	 * @see GroupNode#setLink(GroupNode, int)
	 */
/*	private void setInvLinks(CountMap<Cluster<E>> invLinks) {
		this.invLinks = invLinks;
	}*/
	
	/**
	 * gets the value of the link with a given vertex (or 0 if there is no such link)
	 * @param v the alter vertex
	 * @return the value of the link with a given vertex (or 0 if there is no such link)
	 * @see maps.OldGroupNet#value(GroupNode,GroupNode)
	 */
/*	public int with(Cluster<E> v){
		if (getLinks()==null) return 0;
		if (getLinks().get(v)==null) return 0;
		return getLinks().get(v);
	}*/
	
	/**
	 * augments the value by 1;
	 * @since 11-05-22
	 */
/*	public void augmentWeight(){
		 weight++;
	}*/
	
	/**
	 * gets the value of the link of the GroupNode with itself (degree of endogamy)
	 * @return the value of the link of the GroupNode with itself with itself
	 */
/*	public int getAutoWeight(){
		try {
			return links.get(this);
		} catch (NullPointerException npe){
			return 0;
		}
	}*/
	

	/**
	 * gets the set of vertices linked to the vertex
	 * @return the set of linked vertices
	 * @see maps.OldGroupNet#getCensus()
	 */
/*	public Set<Cluster<E>> getTakers(){
		if (getLinks()==null) return null;
		return getLinks().keySet();
	}*/

	/**
	 * gets the set of vertices linked to the vertex
	 * @return the set of linked vertices
	 * @see maps.OldGroupNet#getCensus()
	 */
/*	public Set<Cluster<E>> getGivers(){
		if (getInvLinks()==null) return null;
		return getInvLinks().keySet();
	}*/
	
	/**
	 * diminishes the value of the link to the alter node
	 * @param v the alter node
	 * @since 11-07-02
	 */
/*	public void diminishLink (Cluster<E> v){
		getLinks().diminish(v);
		v.getInvLinks().diminish(this);
	}	*/
	
	
	/**
	 * augments the netValue by 1 if a condition is fulfilled
	 * @param condition the required condition
	 * @see maps.OldGroupNet#ClassNet(maps.RingGroupMap, boolean)
	 */
/*	public void augment (boolean condition){
		if (condition) netWeight++;
	}	*/
	
	//Simplify
	//Check parameter cumul
	/**
	 * adds the cluster size and value to a dataset and returns the cumulated size of all clusters already counted
	 * @param dataset the dataset
	 * @param cumul a cumulation index (+1 ascending, -1 descending, 0 no cumulation)
	 * @param cum the cumulated size of clusters already counted
	 * @param tot the total size of all clusters of the partition
	 * @param normalize true if the cluster size shall be normalized (as a percentage of total Net size)
	 * @param net the underlying Net
	 * @param part the underlying Partition
	 * @return the cumulated size of clusters already counted (including this cluster)
	 * @see partitions.NodePartition#getDataset(String)
	 */
/*	public int toDataset (DefaultCategoryDataset dataset, int cumul, int cum, int tot, boolean normalize, Net net, Partition<E> part) {
		if (isNullCluster()) return 0;
		int count = count()+cum;
		int cnt = count;
		if (cumul==-1 && !isNullCluster()) cnt=tot-cum;
		if (normalize) cnt = 100*cnt/net.size();
		if (part==null) dataset.addValue(cnt,"",signature(Notation.NUMBERS));
		else dataset.addValue(cnt,signature(Notation.NUMBERS),part.getLabel());
		if (cumul!=0) cum=count;
		return cum;
	}	*/

}
