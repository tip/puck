package org.tip.puck.alliancenets;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.tip.puck.util.RandomGenerator;


/**
 * @author Telmo Menezes
 *
 */
public class AllianceNet implements Cloneable {
    private static int CURID = 0;

    private double minPRIn;
    private double minPROut;
    private double maxPRIn;
    private double maxPROut;

    protected Vector<Group> groups;
    protected Vector<Alliance> alliances;
    protected Map<Integer, Group> nodeMap;
    
    private int nodeCount;
    private int edgeCount;

    private boolean selfEdges;
    

    public AllianceNet() {
        nodeCount = 0;
        edgeCount = 0;
        groups = new Vector<Group>();
        alliances = new Vector<Alliance>();
        nodeMap = new HashMap<Integer, Group>();
        selfEdges = false;
    }
    
    public AllianceNet(boolean selfEdges) {
        this();
        this.selfEdges = selfEdges;
    }
    
    @Override
    public AllianceNet clone()
    {
        AllianceNet clonedNet = new AllianceNet();
        
        // clone all nodes
        for (Group group : groups) {
            clonedNet.addNode(group.clone());
        }
        
        // recreate all edges
        for (Alliance alliance : alliances) {
            Group orig = alliance.getOrigin();
            Group targ = alliance.getTarget();
            long timestamp = alliance.getTimestamp();
            double weight = alliance.getWeight();
            
            Group corig = clonedNet.getNodeById(orig.getId());
            Group ctarg = clonedNet.getNodeById(targ.getId());
            
            clonedNet.addEdge(corig, ctarg, weight, timestamp);
        }
        
        return clonedNet;
    }
    
    public AllianceNet cloneFlagged()
    {
        AllianceNet clonedNet = new AllianceNet();
        
        // clone nodes
        for (Group group : groups) {
            if (group.isFlag()) {
                clonedNet.addNode(group.clone());
            }
        }
        
        // recreate edges
        for (Alliance alliance : alliances) {
            Group orig = alliance.getOrigin();
            Group targ = alliance.getTarget();
            
            if (orig.isFlag() && targ.isFlag()) {
                long timestamp = alliance.getTimestamp();
                double weight = alliance.getWeight();
            
                Group corig = clonedNet.getNodeById(orig.getId());
                Group ctarg = clonedNet.getNodeById(targ.getId());
            
                clonedNet.addEdge(corig, ctarg, weight, timestamp);
            }
        }
        
        return clonedNet;
    }
    
    public static AllianceNet load(String filePath) {
        return (new AllianceNetFile()).load(filePath);
    }

    public void save(String filePath) {
        (new AllianceNetFile()).save(this, filePath);
    }
    
    public Group addNode(Group group) {
        nodeCount++;
        groups.add(group);
        nodeMap.put(group.getId(), group);
        return group;
    }
    
    public Group addNode() {
        Group group = new Group(CURID++);
        addNode(group);
        return group;
    }

    public Group addNodeWithId(int nid) {
        if (nid >= CURID) {
            CURID = nid + 1;
        }
        Group group = new Group(nid);
        addNode(group);
        return group;
    }
    
    public Group getNodeById(int id) {
        return nodeMap.get(id);
    }
    
    public boolean addEdge(Group origin, Group target, double weight, long timestamp) {
        if ((!selfEdges) && (origin == target)) {
            return false;
        }

        if (edgeExists(origin, target)) {
            return false;
        }

        Alliance alliance = new Alliance(origin, target, weight, timestamp);
        alliances.add(alliance);
        origin.addOutEdge(alliance);
        target.addInEdge(alliance);

        edgeCount++;
        
        return true;
    }
    
    public boolean addEdge(Group origin, Group target) {
        return addEdge(origin, target, 0.0, 0l);
    }
    
    public boolean addEdge(Group origin, Group target, double weight) {
        return addEdge(origin, target, weight, 0l);
    }
    
    public boolean addEdge(Group origin, Group target, long timestamp) {
        return addEdge(origin, target, 0.0, timestamp);
    }

    public boolean edgeExists(Group origin, Group target) {
        for (Alliance alliance : origin.getOutEdges()) {
            if (alliance.getTarget() == target) {
                return true;
            }
        }

        return false;
    }
    
    public Alliance getEdge(Group origin, Group target) {
        for (Alliance alliance : origin.getOutEdges()) {
            if (alliance.getTarget() == target) {
                return alliance;
            }
        }

        return null;
    }
    
    public Alliance getInverseEdge(Alliance alliance) {
        return getEdge(alliance.getTarget(), alliance.getOrigin());
    }
    
    public void removeEdge(Alliance alliance) {
        alliance.getOrigin().removeOutput(alliance);
        alliance.getTarget().removeInput(alliance);
        alliances.remove(alliance);
        edgeCount--;
    }
    
    public void removeNode(Group group) {
        List<Alliance> remEdges = new LinkedList<Alliance>(group.getInEdges());
        for (Alliance e : remEdges) {
            removeEdge(e);
        }
        remEdges = new LinkedList<Alliance>(group.getOutEdges());
        for (Alliance e : remEdges) {
            removeEdge(e);
        }
        
        groups.remove(group);
        nodeMap.remove(group.getId());
        nodeCount--;
    }
    
    public void removeNonFlaggedNodes() {
        List<Group> remNodes = new LinkedList<Group>();
        for (Group n : groups) {
            if (!n.isFlag()) {
                remNodes.add(n);
            }
        }
        for (Group n : remNodes) {
            removeNode(n);
        }
    }

    public Group getRandomNode() {
        int pos = RandomGenerator.instance().random.nextInt(nodeCount);
        return groups.get(pos);
    }

    public void computePageranks() {
        // TODO: config
        int maxIter = 10;
        double drag = 0.999;

        for (Group group : groups) {
            group.setPrInLast(1);
            group.setPrOutLast(1);
        }

        int i = 0;

        // double delta_pr_in = 999;
        // double delta_pr_out = 999;
        // double zero_test = 0.0001;

        // while (((delta_pr_in > zero_test) || (delta_pr_out > zero_test)) &&
        // (i < max_iter)) {
        while (i < maxIter) {
            double accPRIn = 0;
            double accPROut = 0;

            for (Group group : groups) {
                group.setPrIn(0);
                for (Alliance origin : group.getInEdges()) {
                    group.setPrIn(group.getPrIn()
                            + origin.getOrigin().getPrInLast()
                            / ((double) origin.getOrigin().getOutDegree()));
                }

                group.setPrIn(group.getPrIn() * drag);
                group.setPrIn(group.getPrIn() + (1.0 - drag)
                        / ((double) nodeCount));

                accPRIn += group.getPrIn();

                group.setPrOut(0);
                for (Alliance target : group.getOutEdges()) {
                    group.setPrOut(group.getPrOut()
                            + target.getTarget().getPrOutLast()
                            / ((double) target.getTarget().getInDegree()));
                }

                group.setPrOut(group.getPrOut() * drag);
                group.setPrOut(group.getPrOut() + (1.0 - drag)
                        / ((double) nodeCount));

                accPROut += group.getPrOut();
            }

            // delta_pr_in = 0;
            // delta_pr_out = 0;

            for (Group group : groups) {
                group.setPrIn(group.getPrIn() / accPRIn);
                group.setPrOut(group.getPrOut() / accPROut);
                // delta_pr_in += Math.abs(node.pr_in - node.pr_in_last);
                // delta_pr_out += Math.abs(node.pr_out - node.pr_out_last);
                group.setPrInLast(group.getPrIn());
                group.setPrOutLast(group.getPrOut());
            }

            i++;
        }

        // relative pr
        double basePR = 1.0 / ((double) nodeCount);
        for (Group group : groups) {
            group.setPrIn(group.getPrIn() / basePR);
            group.setPrOut(group.getPrOut() / basePR);
        }

        // use log scale
        for (Group group : groups) {
            group.setPrIn(Math.log(group.getPrIn()));
            group.setPrOut(Math.log(group.getPrOut()));
        }

        // compute min/max EVC in and out
        minPRIn = 0;
        minPROut = 0;
        maxPRIn = 0;
        maxPROut = 0;
        boolean first = true;
        for (Group group : groups) {
            if ((!(new Double(group.getPrIn())).isInfinite())
                    && (first || (group.getPrIn() < minPRIn))) {
                minPRIn = group.getPrIn();
            }
            if ((!(new Double(group.getPrOut())).isInfinite())
                    && (first || (group.getPrOut() < minPROut))) {
                minPROut = group.getPrOut();
            }
            if ((!(new Double(group.getPrIn())).isInfinite())
                    && (first || (group.getPrIn() > maxPRIn))) {
                maxPRIn = group.getPrIn();
            }
            if ((!(new Double(group.getPrOut())).isInfinite())
                    && (first || (group.getPrOut() > maxPROut))) {
                maxPROut = group.getPrOut();
            }

            first = false;
        }
    }

    public void writePageranks(String filePath) {
        try {
            FileWriter outFile = new FileWriter(filePath);
            PrintWriter out = new PrintWriter(outFile);

            out.println("id, pr_in, pr_out, in_degree, out_degree");

            for (Group group : groups) {
                out.println(String.format("%d,%.10f,%.10f,%d,%d\n",
                        group.getId(), group.getPrIn(), group.getPrOut(),
                        group.getInDegree(), group.getOutDegree()));
            }

            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printNetInfo() {
        System.out.println("node number: " + nodeCount);
        System.out.println("edge number: " + edgeCount);
        System.out.println(String.format("log(pr_in): [%f, %f]\n", minPRIn,
                maxPRIn));
        System.out.println(String.format("log(pr_out): [%f, %f]\n", minPROut,
                maxPROut));
    }

    public int[] inDegSeq() {
        int seq[] = new int[nodeCount];
        int i = 0;
        for (Group curnode : groups) {
            seq[i] = curnode.getInDegree();
            i++;
        }

        return seq;
    }

    public int[] outDegSeq() {
        int seq[] = new int[nodeCount];
        int i = 0;
        for (Group curnode : groups) {
            seq[i] = curnode.getOutDegree();
            i++;
        }

        return seq;
    }

    void genDegreeSeq(AllianceNet refNet) {
        int[] inDegSeq = refNet.inDegSeq();
        int[] outDegSeq = refNet.outDegSeq();

        int totalDegree = refNet.edgeCount;

        // create nodes
        Group[] newNodes = new Group[refNet.nodeCount];
        for (int i = 0; i < refNet.nodeCount; i++) {
            newNodes[i] = addNode();
        }

        // create edges
        int stable = 0;
        while (stable < 1000) {
            //System.out.println("totalDegree: " + totalDegree);
            int origPos = RandomGenerator.instance().random.nextInt(totalDegree);
            int targPos = RandomGenerator.instance().random.nextInt(totalDegree);

            int curpos = 0;
            int origIndex = -1;
            while (curpos <= origPos) {
                origIndex++;
                curpos += outDegSeq[origIndex];
            }

            curpos = 0;
            int targIndex = -1;
            while (curpos <= targPos) {
                targIndex++;
                curpos += inDegSeq[targIndex];
            }
            //System.out.println("" + inDegSeq[targIndex]);
                
            //System.out.println("orig: " + origIndex + "; targ: " + targIndex);
            
            if (addEdge(newNodes[origIndex], newNodes[targIndex], 0)) {
                outDegSeq[origIndex]--;
                inDegSeq[targIndex]--;
                totalDegree--;
                stable = 0;
            }
            stable++;
        }
    }

    double getMinPRIn() {
        return minPRIn;
    }

    double getMinPROut() {
        return minPROut;
    }

    double getMaxPRIn() {
        return maxPRIn;
    }

    double getMaxPROut() {
        return maxPROut;
    }
    
    void printPRInfo() {
        System.out.println("Input PR > min: " + getMinPRIn() + "; max: " + getMaxPRIn());
        System.out.println("Output PR > min: " + getMinPROut() + "; max: " + getMaxPROut());
    }

    public Vector<Group> getNodes() {
        return groups;
    }

    public int getNodeCount() {
        return nodeCount;
    }

    void setNodeCount(int nodeCount) {
        this.nodeCount = nodeCount;
    }

    public int getEdgeCount() {
        return edgeCount;
    }

    void setEdgeCount(int edgeCount) {
        this.edgeCount = edgeCount;
    }
    
    public Vector<Alliance> getEdges() {
        return alliances;
    }
    
    @Override
    public String toString() {
        String str = "node count: " + nodeCount + "\n";
        str += "edge count: " + edgeCount + "\n";
        return str;
    }
    
    public void printDegDistInfo() {
        int[] inDegSeq = inDegSeq();
        int[] outDegSeq = outDegSeq();
        
        int[] inDegrees = new int[10];
        int[] outDegrees = new int[10];
        
        for (int i = 0; i < 10; i++) {
            inDegrees[i] = 0;
            outDegrees[i] = 0;
        }
        
        int maxIn = 0;
        for (int i : inDegSeq) {
            if (i > maxIn) {
                maxIn = i;
            }
            if (i < 10) {
                inDegrees[i]++;
            }
        }
        
        int maxOut = 0;
        for (int i : outDegSeq) {
            if (i > maxOut) {
                maxOut = i;
            }
            if (i < 10) {
                outDegrees[i]++;
            }
        }
        
        System.out.println("max in: " + maxIn + "; max out: " + maxOut);
        System.out.println(">>> in degrees");
        for (int i = 0; i < 10; i++) {
            System.out.print("" + i + ": " + inDegrees[i] + " ");
        }
        System.out.println("\n>>> out degrees");
        for (int i = 0; i < 10; i++) {
            System.out.print("" + i + ": " + outDegrees[i] + " ");
        }
        System.out.println("\n");
    }
    
    public void clearFlags() {
        for (Group group : groups) {
            group.setFlag(false);
        }
    }
    
    public Group firstNode() {
        Group group = null;
        try {
            group = groups.firstElement();
        }
        catch (Exception e) {}
        return group;
    }
}