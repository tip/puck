package org.tip.puckgui.views.mas;

/**
 * 
 * @author TIP
 */
public class NormalAgeDifferenceWeightFactor implements WeightFactor {

	private final Type type = Type.NORMAL_AGE_DIFFERENCE;
	private double mean;
	private double stdev;

	/**
	 * 
	 */
	public NormalAgeDifferenceWeightFactor() {
		this.mean = 0;
		this.stdev = 5;
	}

	public double getMean() {
		return mean;
	}

	public double getStdev() {
		return stdev;
	}

	@Override
	public Type getType() {
		return type;
	}

	public void setMean(final double mean) {
		this.mean = mean;
	}

	public void setStdev(final double stdev) {
		this.stdev = stdev;
	}
}
