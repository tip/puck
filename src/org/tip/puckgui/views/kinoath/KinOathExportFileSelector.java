package org.tip.puckgui.views.kinoath;

import java.awt.Component;
import java.io.File;
import java.util.ResourceBundle;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.util.ToolBox;
import org.tip.puckgui.util.GenericFileFilter;

/**
 * This class is based on the dialog type so it must set at first.
 * 
 * http://stackoverflow.com/questions/596429/adjust-selected-file-to-filefilter-
 * in-a-jfilechooser
 * 
 * @author cpm
 * 
 */
public class KinOathExportFileSelector extends JFileChooser {

	private static final long serialVersionUID = -7715442735368529483L;

	private static final Logger logger = LoggerFactory.getLogger(KinOathExportFileSelector.class);

	private File currentFile = null;

	/**
	 * 
	 */
	public KinOathExportFileSelector(final File targetFile) {
		super();

		//
		setSelectedFile(targetFile);
		setDialogTitle("Export kin diagram");
		setFileSelectionMode(JFileChooser.FILES_ONLY);
		setAcceptAllFileFilterUsed(false);
		setApproveButtonText("Export");
		setDialogType(CUSTOM_DIALOG);

		//
		GenericFileFilter defaultFileFilter = new GenericFileFilter("Diagram files (*.pdf, *.png, *.svg)", "pdf", "png", "svg");
		addChoosableFileFilter(defaultFileFilter);
		addChoosableFileFilter(new GenericFileFilter("Portable Document Format (*.pdf)", "pdf"));
		addChoosableFileFilter(new GenericFileFilter("Portable Network Graphics (*.png)", "png"));
		addChoosableFileFilter(new GenericFileFilter("Scalable Vector Graphics (*.svg)", "svg"));
		setFileFilter(defaultFileFilter);
	}

	/**
	 * 
	 */
	@Override
	public void approveSelection() {
		//
		File file = getSelectedFile();

		//
		if (!StringUtils.endsWithAny(ToolBox.getExtension(file), "svg", "pdf", "png")) {
			//
			file = new File(file.getAbsolutePath() + ".svg");
			setSelectedFile(file);
		}

		//
		if (file.exists()) {
			//
			String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.title");
			String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.text");

			int response = JOptionPane.showConfirmDialog(this, message, title, JOptionPane.YES_NO_CANCEL_OPTION);

			if (response == JOptionPane.YES_OPTION) {
				//
				super.approveSelection();

			} else if (response == JOptionPane.NO_OPTION) {
				// Continue.

			} else {
				//
				super.cancelSelection();
			}
		} else {
			//
			super.approveSelection();
		}
	}

	/**
	 * 
	 */
	@Override
	public void setSelectedFile(final File file) {
		//
		super.setSelectedFile(file);

		System.out.println("==== SET SELECTED FILE=================");
		System.out.println("SELECED FILE " + file);

		//
		if (file != null) {
			this.currentFile = file;
		}
	}

	/**
	 * 
	 */
	@Override
	public void setVisible(final boolean visible) {
		super.setVisible(visible);

		if (!visible) {
			//
			resetChoosableFileFilters();
		}
	}

	/**
	 * This method is the main one of the selector.
	 * 
	 * @param parent
	 * @return
	 */
	public static File showSelectorDialog(final Component parent, final File targetFile) {
		File result;

		//
		KinOathExportFileSelector selector = new KinOathExportFileSelector(targetFile);

		if (selector.showDialog(parent, null) == JFileChooser.APPROVE_OPTION) {
			//
			logger.debug("getCurrentDirectory(): {}", selector.getCurrentDirectory());
			logger.debug("getSelectedFile() : {}", selector.getSelectedFile());
			result = selector.getSelectedFile();

		} else {
			//
			result = null;
		}

		//
		return result;
	}
}
