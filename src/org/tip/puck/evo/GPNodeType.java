package org.tip.puck.evo;


/**
 * Enum of the genetic program node type.
 * 
 * Function, variable or constant value.
 * 
 * @author Telmo Menezes (telmo@telmomenezes.com)
 */
public enum GPNodeType {
    FUN, VAR, VAL 
}
