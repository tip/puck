package org.tip.puck.io.kinsources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents a catalog from Kinsources.
 * 
 * @author TIP
 */
public class CatalogItem {

	private static final Logger logger = LoggerFactory.getLogger(CatalogItem.class);

	private long id;
	private String name;
	private String permanentLink;
	private String author;
	private String contributor;
	private String license;
	private Long individualCount;
	private Long familyCount;
	private Long relationCount;
	private String publicationDate;

	/**
	 * 
	 */
	public CatalogItem() {

	}

	public String getAuthor() {
		return this.author;
	}

	public String getContributor() {
		return this.contributor;
	}

	public Long getFamilyCount() {
		return this.familyCount;
	}

	public long getId() {
		return this.id;
	}

	public Long getIndividualCount() {
		return this.individualCount;
	}

	public String getLicense() {
		return this.license;
	}

	public String getName() {
		return this.name;
	}

	public String getPermanentLink() {
		return this.permanentLink;
	}

	public String getPublicationDate() {
		return this.publicationDate;
	}

	public Long getRelationCount() {
		return this.relationCount;
	}

	public void setAuthor(final String author) {
		this.author = author;
	}

	public void setContributor(final String contributor) {
		this.contributor = contributor;
	}

	public void setFamilyCount(final Long familyCount) {
		this.familyCount = familyCount;
	}

	public void setId(final long id) {
		this.id = id;
	}

	public void setIndividualCount(final Long individualCount) {
		this.individualCount = individualCount;
	}

	public void setLicense(final String license) {
		this.license = license;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setPermanentLink(final String permanentLink) {
		this.permanentLink = permanentLink;
	}

	public void setPublicationDate(final String publicationDate) {
		this.publicationDate = publicationDate;
	}

	public void setRelationCount(final Long relationCount) {
		this.relationCount = relationCount;
	}

}
