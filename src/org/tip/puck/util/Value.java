package org.tip.puck.util;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.census.chains.Chain;
import org.tip.puck.census.chains.ChainMaker;
import org.tip.puck.census.chains.Vector;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.Node;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.Role;
import org.tip.puck.partitions.Interval;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.Partition.ValueCode;

/**
 * 
 * @author TIP
 */
public class Value implements Comparable<Value> {

	private static final Logger logger = LoggerFactory.getLogger(Value.class);

	private Object value;

	
	public static <E> Value valueOf(final E value) {
		Value result;
		
		if (value == null){
			result = null;
		} else {
			result = new Value(value);
		}
		
		//
		return result;
	}
	
	
	/**
	 * 
	 * @param value
	 */
	public Value(final char value) {
		this.value = new Character(value);
	}

	/**
	 * 
	 * @param value
	 */
	public Value(final double value) {
		this.value = new Double(value);
	}
	
	public Value(final Chain value){
		if (value == null) {
			throw new ClassCastException("Value cannot be null.");
		} else {
			this.value = value;
		}
	}
	

	/**
	 * 
	 * @param value
	 */
	public <E extends Comparable<E>> Value(final E value) {
		if (value == null) {
			throw new ClassCastException("Value cannot be null.");
		} else {
			this.value = value;
		}
	}

	/**
	 * 
	 * @param value
	 */
	public Value(final int value) {
		this.value = new Integer(value);
	}

	/**
	 * 
	 * @param value
	 */
	public Value(final Object value) {
		if (value == null) {
			throw new ClassCastException("Value cannot be null.");
		} else {
			this.value = value;
		}
	}
	
	/**
	 * 
	 * @param value
	 */
	public Value(final List<?> value) {
		if (value == null) {
			throw new ClassCastException("Value cannot be null.");
		} else {
			this.value = value;
		}
	}


	public Value(final String value) {
		this.value = new String(value);
	}

	/**
	 * 
	 * @return
	 */
	public Chain chainValue() {
		Chain result;

		if (this.value instanceof Chain) {
			result = (Chain) this.value;
		} else {
			throw new ClassCastException("Value is not a chain.");
		}

		return result;
	}

	/**
	 * 
	 * @return
	 */
	public char charValue() {
		char result;
		if (this.value == null) {
			result = ' ';
		} else if (this.value instanceof Character) {
			result = ((Character) this.value).charValue();
		} else {
			throw new ClassCastException("Value is not a character.");
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Role roleValue() {
		Role result;
		if (this.value instanceof Role) {
			result = (Role)this.value;
		} else {
			throw new ClassCastException("Value is not a role.");
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int compareTo(final Value source) {
		int result;

		if (source == null) {
			result = 1;
		} else if (source == this){
			result = 0;
		} else if ((isNumber()) && (source.isNumber())) {
			result = Double.compare(doubleValue(), source.doubleValue());
		} else if ((isVector()) && (source.isVector())) {
			result = vectorValue().compareTo(source.vectorValue());
		} else if ((isChain()) && (source.isChain())) {
			result = chainValue().compareTo(source.chainValue());
		} else if ((isIndividual()) && (source.isIndividual())) {
			result = Integer.valueOf(individualValue().getId()).compareTo(source.individualValue().getId());
		} else if ((isGender()) && (source.isGender())) {
			result = genderValue().compareTo(source.genderValue());
		} else if ((isRole()) && (source.isRole())) {
			result = roleValue().compareTo(source.roleValue());
		} else if ((isEnum()) && (source.isEnum())) {
			result = enumValue().compareTo(source.enumValue());
		} else if ((this.value instanceof String[]) && (source.getObject() instanceof String[])) {
			result = Arrays.toString(stringArrayValue()).compareTo(Arrays.toString(source.stringArrayValue()));
		} else if ((this.value instanceof Interval) && (source.getObject() instanceof Interval)) {
			result = ((Interval) this.getObject()).compareTo((Interval) source.getObject());
		} else if ((this.value instanceof Node) && source.getObject() instanceof Node) {
			result = ((Node) this.getObject()).compareTo((Node) source.getObject());
		} else {
			result = stringValue().compareTo(source.stringValue());
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int dimension() {
		int result;

		if (this.value instanceof Vector) {
			result = ((Vector) this.value).order();
		} else {
			result = 1;
		}

		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double doubleValue() {
		double result;

		if (this.value == null) {
			throw new ClassCastException("Null is not a number.");
		} else if (this.value instanceof Integer) {
			result = ((Integer) this.value).doubleValue();
		} else if (this.value instanceof Double) {
			result = ((Double) this.value).doubleValue();
		} else {
			//
			String stringValue = this.value.toString();

			//
			if (NumberUtils.isNumber(stringValue)) {
				result = Double.parseDouble(stringValue);
			} else {
				throw new ClassCastException("Value is not a number.");
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Enum enumValue() {
		Enum result;

		if (this.value instanceof Enum) {
			result = (Enum) this.value;
		} else {
			throw new ClassCastException("Value is not an Enum.");
		}

		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public List listValue() {
		List result;

		if (this.value instanceof List) {
			result = (List) this.value;
		} else {
			throw new ClassCastException("Value is not a List.");
		}

		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public Graph graphValue() {
		Graph result;

		if (this.value instanceof Graph) {
			result = (Graph) this.value;
		} else {
			throw new ClassCastException("Value is not a Graph.");
		}

		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public Map mapValue() {
		Map result;

		if (this.value instanceof Map) {
			result = (Map) this.value;
		} else {
			throw new ClassCastException("Value is not a Map.");
		}

		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public <E> Partition<E> partitionValue() {
		Partition<E> result;

		if (this.value instanceof Partition) {
			result = (Partition<E>) this.value;
		} else {
			throw new ClassCastException("Value is not a Partition.");
		}

		return result;
	}
	
	/**
	 * 
	 */
	@Override
	public boolean equals(final Object object) {
		boolean result;

		if ((object == null) || (!(object instanceof Value))) {
			result = false;
		} else if (((Value) object).getObject() instanceof String[]) {
			result = Arrays.toString((String[]) getObject()).equals(Arrays.toString((String[]) ((Value) object).getObject()));
		} else {
			result = stringValue().equals(((Value) object).stringValue());
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Gender genderValue() {
		Gender result;

		if (this.value instanceof Gender) {
			result = (Gender) this.value;
		} else {
			throw new ClassCastException("Value is not a gender.");
		}

		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Object getObject() {
		Object result;

		result = this.value;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int hashCode() {
		int result;

		if (getObject() instanceof String[]) {
			result = Arrays.toString((String[]) getObject()).hashCode();
		} else {
			result = stringValue().hashCode();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individual individualValue() {
		Individual result;

		if (this.value instanceof Individual) {
			result = (Individual) this.value;
		} else {
			throw new ClassCastException("Value is not an individual.");
		}

		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public Relation relationValue() {
		Relation result;

		if (this.value instanceof Relation) {
			result = (Relation) this.value;
		} else {
			throw new ClassCastException("Value is not a relation.");
		}

		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int intValue() {
		int result;

		if (this.value == null) {
			throw new ClassCastException("Null is not a number.");
		} else if (this.value instanceof Integer) {
			result = ((Integer) this.value).intValue();
		} else if (this.value instanceof Double) {
			result = ((Double) this.value).intValue();
		} else {
			String stringValue = this.value.toString();

			if (NumberUtils.isNumber(stringValue)) {
				result = Integer.parseInt(stringValue);
			} else {
				throw new ClassCastException("Value is not a number.");
			}
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public String[] stringArrayValue() {
		String[] result;

		if (this.value == null) {
			throw new ClassCastException("Null is not a string array.");
		} else if (this.value instanceof String[]) {
			result = (String[]) this.value;
		} else {
			throw new ClassCastException("Value is not a string array.");
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public Double[] doubleArrayValue() {
		Double[] result;

		if (this.value == null) {
			throw new ClassCastException("Null is not a double array.");
		} else if (this.value instanceof Double[]) {
			result = (Double[]) this.value;
		} else {
			throw new ClassCastException("Value is not a double array.");
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isMap() {
		boolean result;

		if (this.value == null) {
			result = false;
		} else if (this.value instanceof Map) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public boolean isPartition() {
		boolean result;

		if (this.value == null) {
			result = false;
		} else if (this.value instanceof Partition) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public boolean isGraph() {
		boolean result;

		if (this.value == null) {
			result = false;
		} else if (this.value instanceof Graph) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isChain() {
		boolean result;

		if (this.value == null) {
			result = false;
		} else if (this.value instanceof Chain) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	public boolean isEnum() {
		boolean result;

		if (this.value == null) {
			result = false;
		} else if ((this.value instanceof Enum)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isGender() {
		boolean result;

		if (this.value == null) {
			result = false;
		} else if (this.value instanceof Gender) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isRole() {
		boolean result;

		if (this.value == null) {
			result = false;
		} else if (this.value instanceof Role) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isIndividual() {
		boolean result;

		if (this.value == null) {
			result = false;
		} else if (this.value instanceof Individual) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotNumber() {
		boolean result;

		result = !isNumber();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotZero() {
		boolean result;

		if (this.value == null) {
			result = true;
		} else if (isNotNumber()) {
			result = true;
		} else if (doubleValue() == 0) {
			result = false;
		} else {
			result = true;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNumber() {
		boolean result;

		if (this.value == null) {
			result = false;
		} else if ((this.value instanceof Integer) || (this.value instanceof Double) || (NumberUtils.isNumber(this.value.toString()))) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isPositiveNumber() {
		boolean result;

		if ((this.isNumber()) && (this.doubleValue() > 0)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isString() {
		boolean result;

		if (this.value instanceof String) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isList() {
		boolean result;

		if (this.value instanceof List<?>) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isVector() {
		boolean result;

		if (this.value == null) {
			result = false;
		} else if (this.value instanceof Vector) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isZero() {
		boolean result;

		if (this.value == null) {
			result = false;
		} else if (isNotNumber()) {
			result = false;
		} else if (doubleValue() == 0) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public String stringValue() {
		String result;

		if (this.value == null) {
			result = "null";
		} else if (this.value instanceof String[]){
			result = Arrays.toString((String[])this.value);
		} else if (this.value instanceof List && listValue()!=null && listValue().size()>0 && listValue().get(0) instanceof String[]){
			result = "[";
			for (String[] item : (List<String[]>)listValue()){
				result+= Arrays.toString(item);
			}
			result+="]";
		} else {
			result = this.value.toString();
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		result = this.stringValue();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Vector vectorValue() {
		Vector result;

		if (this.value instanceof Vector) {
			result = (Vector) this.value;
		} else if (this.value instanceof String) {
			Chain chain = ChainMaker.fromString((String) this.value);
			if (chain != null) {
				result = chain.getCharacteristicVector();
			} else {
				throw new ClassCastException("Value is not a vector string.");
			}
		} else {
			throw new ClassCastException("Value is not a vector.");
		}

		return result;
	}

	/**
	 * 
	 * @param value1
	 * @param value2
	 * @return
	 */
	public static int compare(final Value value1, final Value value2) {
		int result;

		if ((value1 == null) && (value2 == null)) {
			result = 0;
		} else if (value1 == null) {
			result = -1;
		} else if (value2 == null) {
			result = +1;
		} else {
			result = value1.compareTo(value2);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isNumber(final Value value) {
		boolean result;

		if (value == null) {
			result = false;
		} else {
			result = value.isNumber();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static String stringValue(final Value value) {
		String result;

		if (value == null) {
			result = null;
		} else {
			result = value.stringValue();
		}

		//
		return result;
	}
	
	public Value unpack(ValueCode mode){
		Value result;
		
		if (mode == null){
			
			result = this;
			
		} else if (isPartition()){
								
			result = new Value(partitionValue().valueCode(mode));
					
		} else if (isMap()){
					
			result = new Value(ToolBox.toKeySetString(mapValue()));
					
		} else {
					
			result = this;
			
		}
		
		//
		return result;
	}

}
