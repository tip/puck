package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.graphs.random.DistributionType;
import org.tip.puck.graphs.random.RandomGraphReporter;
import org.tip.puck.matrix.MatrixStatistics;
import org.tip.puck.partitions.graphs.RandomAllianceNetworkByRandomDistributionCriteria;
import org.tip.puck.report.Report;
import org.tip.puckgui.GroupNetGUI;
import org.tip.puckgui.PuckGUI;
import org.tip.puckgui.WindowGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class RandomAllianceNetworkByRandomDistributionInputWindow extends JFrame {

	private static final long serialVersionUID = 3617593185000792806L;
	private static final Logger logger = LoggerFactory.getLogger(RandomAllianceNetworkByRandomDistributionInputWindow.class);

	private JFrame thisJFrame;
	private static RandomAllianceNetworkByRandomDistributionCriteria lastCriteria;
	private JPanel contentPane;
	private JButton btnRestoreDefaults;
	private JSpinner spnrNumberOfNodes;
	private JSpinner spnrNumberOfArcs;
	private JSpinner spnrPowerFactor;
	private JSpinner spnrNumberOfRuns;
	private JCheckBox chckbxExtractARepresentative;
	private JRadioButton rdbtnFree;
	private JRadioButton rdbtnBernoulli;
	private JRadioButton rdbtnPowerNonStandard;
	private JRadioButton rdbtnPowerStandard;
	private final ButtonGroup buttonGroupDistributionType = new ButtonGroup();

	/**
	 * Create the frame.
	 */
	public RandomAllianceNetworkByRandomDistributionInputWindow(final WindowGUI gui) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(FooReporterInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		setTitle("Random Alliance Network (by random distribution)");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 389, 350);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});

		this.btnRestoreDefaults = new JButton("Restore Defaults");
		this.btnRestoreDefaults.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Restore Defaults.
				setCriteria(new RandomAllianceNetworkByRandomDistributionCriteria());
			}
		});
		buttonPanel.add(this.btnRestoreDefaults);
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		getRootPane().setDefaultButton(btnLaunch);
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					//
					RandomAllianceNetworkByRandomDistributionCriteria criteria = getCriteria();

					//
					//
					PuckGUI.instance().getPreferences().setRandomDistributionCriteria(criteria);

					// Compute statistics (based on
					// RandomGraphReporter.reportRandomGraph(criteria,
					// runs);
					MatrixStatistics matrixStatistics = RandomGraphReporter.createRandomGraphStatisticsByRandomDistribution(criteria);

					// Build report.
					Report report = RandomGraphReporter.reportRandomAllianceNetworkByRandomDistribution(criteria, matrixStatistics);

					// Manage window.
					if (criteria.isExtractRepresentative()) {
						// Create new window and fill it with a report tab.
						if (StringUtils.isBlank(matrixStatistics.getGraph().getLabel())) {
							matrixStatistics.getGraph().setLabel("Random Group Network");
						}
						GroupNetGUI newGUI = PuckGUI.instance().createGroupNetGUI(matrixStatistics.getGraph());
						newGUI.addReportTab(report);

					} else {
						gui.addReportTab(report);
					}

					//
					dispose();

				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(RandomAllianceNetworkByRandomDistributionInputWindow.this.thisJFrame, message, title,
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);

		JPanel panel_parameters = new JPanel();
		this.contentPane.add(panel_parameters, BorderLayout.CENTER);
		panel_parameters.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("100dlu:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblNumberOfNodes = new JLabel("Number of nodes:");
		panel_parameters.add(lblNumberOfNodes, "2, 2, right, default");

		this.spnrNumberOfNodes = new JSpinner();
		this.spnrNumberOfNodes.setModel(new SpinnerNumberModel(new Integer(100), new Integer(1), null, new Integer(1)));
		panel_parameters.add(this.spnrNumberOfNodes, "4, 2");

		JLabel lblNumberOfArcs = new JLabel("Number of arcs:");
		panel_parameters.add(lblNumberOfArcs, "2, 4, right, default");

		this.spnrNumberOfArcs = new JSpinner();
		this.spnrNumberOfArcs.setModel(new SpinnerNumberModel(new Integer(1000), new Integer(1), null, new Integer(1)));
		panel_parameters.add(this.spnrNumberOfArcs, "4, 4");

		JLabel lblDistributionType = new JLabel("Distribution type:");
		panel_parameters.add(lblDistributionType, "2, 6, right, default");

		JPanel panel = new JPanel();
		panel_parameters.add(panel, "4, 6, fill, fill");
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		this.rdbtnFree = new JRadioButton("Free");
		this.buttonGroupDistributionType.add(this.rdbtnFree);
		panel.add(this.rdbtnFree);

		this.rdbtnBernoulli = new JRadioButton("Bernoulli");
		this.buttonGroupDistributionType.add(this.rdbtnBernoulli);
		panel.add(this.rdbtnBernoulli);

		this.rdbtnPowerNonStandard = new JRadioButton("Power non standard");
		this.buttonGroupDistributionType.add(this.rdbtnPowerNonStandard);
		panel.add(this.rdbtnPowerNonStandard);

		this.rdbtnPowerStandard = new JRadioButton("Power standard");
		this.buttonGroupDistributionType.add(this.rdbtnPowerStandard);
		panel.add(this.rdbtnPowerStandard);

		JLabel lblPowerFactor = new JLabel("Power factor:");
		panel_parameters.add(lblPowerFactor, "2, 8, right, default");

		this.spnrPowerFactor = new JSpinner();
		this.spnrPowerFactor.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		panel_parameters.add(this.spnrPowerFactor, "4, 8");

		JLabel lblNumberOfRuns = new JLabel("Number of runs:");
		panel_parameters.add(lblNumberOfRuns, "2, 10, right, default");

		this.spnrNumberOfRuns = new JSpinner();
		this.spnrNumberOfRuns.setModel(new SpinnerNumberModel(new Integer(100), new Integer(1), null, new Integer(1)));
		panel_parameters.add(this.spnrNumberOfRuns, "4, 10");

		this.chckbxExtractARepresentative = new JCheckBox("Extract a representative network");
		panel_parameters.add(this.chckbxExtractARepresentative, "2, 12, 3, 1");

		// //////////////////
		setCriteria(PuckGUI.instance().getPreferences().getRandomDistributionCriteria());
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public RandomAllianceNetworkByRandomDistributionCriteria getCriteria() throws PuckException {
		RandomAllianceNetworkByRandomDistributionCriteria result;

		//
		result = new RandomAllianceNetworkByRandomDistributionCriteria();

		//
		result.setNumberOfNodes((Integer) this.spnrNumberOfNodes.getValue());
		result.setNumberOfArcs((Integer) this.spnrNumberOfArcs.getValue());

		//
		DistributionType type;
		if (this.rdbtnFree.isSelected()) {
			type = DistributionType.FREE;
		} else if (this.rdbtnBernoulli.isSelected()) {
			type = DistributionType.BERNOULLI;
		} else if (this.rdbtnPowerNonStandard.isSelected()) {
			type = DistributionType.PARETO;
		} else if (this.rdbtnPowerStandard.isSelected()) {
			type = DistributionType.POWER;
		} else {
			throw PuckExceptions.INVALID_PARAMETER.create("Bad value");
		}
		result.setDistributionType(type);

		//
		result.setNumberOfRuns((Integer) this.spnrNumberOfRuns.getValue());
		result.setPowerFactor((Integer) this.spnrPowerFactor.getValue());
		result.setExtractRepresentative(this.chckbxExtractARepresentative.isSelected());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public void setCriteria(final RandomAllianceNetworkByRandomDistributionCriteria source) {
		//
		if (source != null) {
			//
			this.spnrNumberOfNodes.setValue(source.getNumberOfNodes());
			this.spnrNumberOfArcs.setValue(source.getNumberOfArcs());

			//
			this.rdbtnFree.setSelected(false);
			this.rdbtnBernoulli.setSelected(false);
			this.rdbtnPowerNonStandard.setSelected(false);
			this.rdbtnPowerStandard.setSelected(false);
			switch (source.getDistributionType()) {
				case FREE:
					this.rdbtnFree.setSelected(true);
				break;
				case BERNOULLI:
					this.rdbtnBernoulli.setSelected(true);
				break;
				case PARETO:
					this.rdbtnPowerNonStandard.setSelected(true);
				break;
				case POWER:
					this.rdbtnPowerStandard.setSelected(true);
				break;

			}

			//
			this.spnrPowerFactor.setValue(source.getPowerFactor());
			this.spnrNumberOfRuns.setValue(source.getNumberOfRuns());
			this.chckbxExtractARepresentative.setSelected(source.isExtractRepresentative());
		}
	}
}
