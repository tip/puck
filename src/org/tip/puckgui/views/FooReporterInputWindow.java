package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToggleButton;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.report.Report;
import org.tip.puck.workers.FooReporter;
import org.tip.puckgui.NetGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class FooReporterInputWindow extends JFrame {

	private static final long serialVersionUID = 898087774983200607L;
	private static final Logger logger = LoggerFactory.getLogger(FooReporterInputWindow.class);

	private JFrame thisJFrame;
	private NetGUI netGUI;
	private JPanel contentPane;
	private JTextField textField;
	private JCheckBox chckbxBravovalue;
	private JToggleButton tglbtnCharlievalue;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Create the frame.
	 */
	public FooReporterInputWindow(final NetGUI netGUI) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(FooReporterInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		this.netGUI = netGUI;
		setTitle("FooWorker Inputs");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 399, 381);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel titlePanel = new JPanel();
		this.contentPane.add(titlePanel, BorderLayout.NORTH);
		titlePanel.setLayout(new BoxLayout(titlePanel, BoxLayout.Y_AXIS));

		JLabel lblFooWorker = new JLabel("FOO WORKER");
		titlePanel.add(lblFooWorker);

		JTextPane txtpnWelcome = new JTextPane();
		txtpnWelcome.setText("Welcome to FooWorker input window.\nPlease enter your data and press 'Launch'.");
		titlePanel.add(txtpnWelcome);

		Component verticalStrut = Box.createVerticalStrut(10);
		titlePanel.add(verticalStrut);

		JPanel inputPanel = new JPanel();
		this.contentPane.add(inputPanel, BorderLayout.CENTER);
		inputPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblAlpha = new JLabel("Alpha:");
		inputPanel.add(lblAlpha, "2, 2, left, default");

		this.textField = new JTextField();
		inputPanel.add(this.textField, "4, 2, fill, default");
		this.textField.setColumns(10);

		JLabel lblBravo = new JLabel("Bravo:");
		inputPanel.add(lblBravo, "2, 4");

		this.chckbxBravovalue = new JCheckBox("");
		inputPanel.add(this.chckbxBravovalue, "4, 4");

		JLabel lblCharlie = new JLabel("Charlie:");
		inputPanel.add(lblCharlie, "2, 6");

		this.tglbtnCharlievalue = new JToggleButton("CharlieValue");
		inputPanel.add(this.tglbtnCharlievalue, "4, 6");

		JLabel lblDelta = new JLabel("Delta:");
		inputPanel.add(lblDelta, "2, 8");

		JRadioButton rdbtnDeltavalue = new JRadioButton("DeltaValue1");
		this.buttonGroup.add(rdbtnDeltavalue);
		inputPanel.add(rdbtnDeltavalue, "4, 8");

		JRadioButton rdbtnDeltavalue_1 = new JRadioButton("DeltaValue2");
		this.buttonGroup.add(rdbtnDeltavalue_1);
		inputPanel.add(rdbtnDeltavalue_1, "4, 10");

		JLabel lblEcho = new JLabel("Echo %:");
		inputPanel.add(lblEcho, "2, 12");

		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(90, 0, 100, 1));
		inputPanel.add(spinner, "4, 12");

		JLabel lblFox = new JLabel("Fox:");
		inputPanel.add(lblFox, "2, 14, left, default");

		JSlider slider = new JSlider();
		inputPanel.add(slider, "4, 14");

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {

					Report report = FooReporter.work(netGUI.getNet(), "alpha", "bravo");

					netGUI.addReportTab(report);

					dispose();

				} catch (final PuckException exception) {
					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(FooReporterInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);
	}
}
