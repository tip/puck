package org.tip.puck.sequences.workers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tip.puck.PuckException;
import org.tip.puck.geo.Geography;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.GraphComparatorByArcCount;
import org.tip.puck.graphs.GraphProfile;
import org.tip.puck.graphs.Link;
import org.tip.puck.graphs.Node;
import org.tip.puck.graphs.workers.GraphUtils;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individualizable;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.RelationEnvironment;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.relations.workers.RelationValuator;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Partition;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.EgoSequence;
import org.tip.puck.sequences.Sequenceable;
import org.tip.puck.sequences.workers.SequenceCriteria.ValueSequenceLabel;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.Numberable;
import org.tip.puck.util.PuckUtils;
import org.tip.puck.util.ToolBox;
import org.tip.puck.util.Value;

/**
 * 
 * @author Klaus Hamberger
 *
 */
public class SequenceNetworkMaker  {
	
//	private Individual ego;
//	private List<Relation> events;
	
//	private Map<String,List<String>> partitionLabels;

	
//	private String egoRoleName;
//	private int[] maxDepths;
//	private List<String> placeNames;
//	private List<String> relationModelNames; 
//	private List<String> roleNames; 
//	private List<RelationClassificationType> types; 
//	private String alterRoleName; // for ego networks (temp)
//	private List<RelationClassificationType>  mainEventTypes; // for parcours morphology and parcours intersection norks
//	private String relationModelName;
//	private Geography geography;
//	private String chainClassification;
	
//	private String alterAttributeLabel; // for parcours intersection norks
//	private String alterAttributeValue; // for parcours intersection norks
	
//	private int length;
//	private int threshold; // child age <= threshold
	
//	private Map<RelationClassificationType,Map<Relation,String>> eventEvaluations;
	
	
//	private Map<String, GraphProfile<Cluster<Relation>>> eventTypeNetworkProfiles;
//	private Map<String, GraphProfile<Numberable>> eventNetworkProfiles;
	
/*	private Map<RelationClassificationType,Graph<Set<Individual>>> parcoursSimilarityTrees;
	private Map<RelationClassificationType,ValueSequence> sequenceProfiles;
	private String pattern;*/
	
//	private String dateLabel;
	

	public static <S extends Sequenceable<E>,E> HashMap<String,GraphProfile<?>> createNetworkProfiles (S sequence, Segmentation segmentation, SequenceCriteria criteria) throws PuckException{
		HashMap<String,GraphProfile<?>> result;
		
/*		this.events = new ArrayList<Relation>();
		for (Relation event : source.getStations().values()){
			events.add(event);
		}	*/	

//		this.ego = source.getEgo();
		
//		this.length = SequenceValuator.get(source, "NREVENTS").intValue();
		
/*		this.length = 0;
		for (Relation event : source.getStations().values()){
			if (!isBirth(event) && !isDeath(event)){
				length++;
			}
		}*/

/*		this.egoRoleName = criteria.getEgoRoleName();
		this.pattern = criteria.getPattern();
		this.maxDepths = ToolBox.stringsToInts(criteria.getPattern());
		this.level = criteria.getLevel();
		this.partitionLabels = criteria.getPartitionLabels();
		this.relationModelName = criteria.getRelationModelName();
		this.relationModelNames = criteria.getRelationModelNames(); 
//		this.alterRoleName = criteria.getAlterFilterRoleName();
		this.geography = criteria.getGeography();
//		this.roleNames = criteria.getRoleNames();
		this.types = criteria.getTypes();
		this.chainClassification = criteria.getChainClassification();
		this.alterAttributeLabel = criteria.getAlterFilterAttributeLabel();
		this.alterAttributeValue = criteria.getAlterFilterAttributeValue();
		this.mainEventTypes = criteria.getMainRelationClassificationTypes();
//		this.placeNames = criteria.getMinimalPlaceNames();
		this.dateLabel = criteria.getDateLabel();*/

//		this.threshold = criteria.getThreshold();
//		this.threshold = 20; // INTEGRER DANS MASQUE!
				
//		this.sequenceProfiles = new HashMap<RelationClassificationType,ValueSequence>();
//		this.eventNetworkProfiles = new HashMap<String,GraphProfile<Numberable>>();
//		this.eventTypeNetworkProfiles = new HashMap<String,GraphProfile<Cluster<Relation>>>();
		
//		setAlters();
//		setAlterRelations("ALL");
		
//		evaluateEvents();

		result = new HashMap<String,GraphProfile<?>>();

		RelationEnvironment egoEnvironment = null;
		
		if (sequence instanceof Individualizable){
			egoEnvironment = new RelationEnvironment(((Sequenceable<Relation>)sequence).getStations().values(),((Individualizable)sequence).getEgo(),criteria.getEgoRoleName(),criteria.getRoleNames(),criteria.getRelationModelNames(), criteria.getImpersonalAlterLabel());
			egoEnvironment.setAlterRelations(((Sequenceable<Relation>)sequence).getStations().values(),((EgoSequence)sequence).getEgo(),criteria.getEgoRoleName(),"ALL", criteria.getRelationModelNames(), criteria.getPattern(), criteria.getChainClassification());
		}

		for (String networkTitle : criteria.getNetworkTitles()){
			
			ValueSequenceLabel type = null;
			String subLabel = ToolBox.splitLastPart(networkTitle,"#")[1];
			if (subLabel!=null){
				 type = ValueSequenceLabel.valueOf(subLabel);
			}
			
//			if (networkTitle.contains("Parcours Network") || networkTitle.contains("Parcours Similarity Network")){
				
//				for (ValueSequenceLabel type : criteria.getValueSequenceLabels()){
					
				if (type!=null && type.equals(ValueSequenceLabel.TREES)){
					for (E station : sequence.getStations().values()){
						((Relation)station).updateReferents(criteria.getDefaultReferentRoleName());
					}
				}

//				Graph<?> network = createNetwork (networkTitle, source, egoEnvironment, criteria.getRelationModelName(), type, null, criteria.getPattern(), criteria.getChainClassification(), criteria.getAlterFilterAttributeLabel(), criteria.getAlterFilterAttributeValue());
//				createNetworkProfile(result,network,networkTitle,egoEnvironment, criteria.getPartitionLabels().get(networkTitle));

//			}
				
//			} else {
				
			Graph<?> network = createNetwork (networkTitle, segmentation, (Sequenceable<Relation>)sequence, egoEnvironment, criteria.getRelationModelName(), type, criteria.getValueSequenceLabels(), criteria.getPattern(), criteria.getChainClassification(), criteria.getAlterFilterAttributeLabel(), criteria.getAlterFilterAttributeValue(), criteria.getImpersonalAlterLabel());
			createNetworkProfile(result,network,networkTitle,egoEnvironment, criteria.getPartitionLabels().get(networkTitle));
			
//			}
						
/*			if (networkTitle.contains("Ego Network")){
				setEgoNetworks(egoEnvironment, criteria.getRelationModelNames(), criteria);
			} else if (networkTitle.contains("Parcours Network")){
				setParcoursNetworks(egoEnvironment, criteria.getRelationModelName(), criteria.getRelationModelNames(), criteria.getTypes(), criteria.getMainRelationClassificationTypes(), criteria.getDefaultReferentRoleName());
			} else if (networkTitle.contains("Parcours Intersection Network")){
				setParcoursIntersectionNetworks(egoEnvironment,criteria.getRelationModelName(), criteria.getRelationModelNames(),criteria.getAlterFilterAttributeLabel(), criteria.getAlterFilterAttributeValue());
			} else if (networkTitle.contains("Parcours Similarity Network")){
				setParcoursSimilarityNetworks(egoEnvironment, criteria.getRelationModelName(), criteria.getRelationModelNames(), criteria.getPattern(), criteria.getMainRelationClassificationTypes(), criteria.getAlterFilterAttributeLabel(), criteria.getAlterFilterAttributeValue());
			}*/
		}
		//
		return result;
	}	
	
	private static <S extends Sequenceable<Relation>> Graph<?> createNetwork (String title, Segmentation segmentation, S sequence, RelationEnvironment egoEnvironment, String relationModelName, ValueSequenceLabel type, List<ValueSequenceLabel> types, String pattern, String chainClassification, String alterAttributeLabel, String alterAttributeValue, String impersonalAlterLabel) throws PuckException{
		Graph<?> result;
		
		if (title.equals("Ego Network")){
			
			result = getEgoNetwork(relationModelName, egoEnvironment, pattern, chainClassification);

		} else if (title.equals("Nonmediated Ego Network")){
			
			result = getNonMediatedEgoNetwork(relationModelName, egoEnvironment, pattern, chainClassification);

		} else if (title.equals("Sequence Network#"+type)){
			
			result = getSequenceNetwork(sequence, egoEnvironment, type, impersonalAlterLabel);

		} else if (title.equals("Parcours")){
			
			result = getParcoursGraph(sequence, egoEnvironment.getEgo());
			
		} else if (title.equals("Extended Parcours")){
			
			result = getExtendedParcoursGraph(sequence, egoEnvironment, types, segmentation.getGeography());
					
		} else if (title.equals("Multiple Parcours")){
			
			result = getMultipleParcoursGraph(sequence, segmentation, relationModelName, egoEnvironment);
			
		} else if (title.equals("Parcours Intersection Network")){

			result = getParcoursIntersectionNetwork(relationModelName, egoEnvironment, alterAttributeLabel, alterAttributeValue);

		} else if (title.equals("Parcours Similarity Network")){

			result = getParcoursSimilarityNetwork(type, segmentation, egoEnvironment, relationModelName, pattern, alterAttributeLabel, alterAttributeValue, impersonalAlterLabel);

		} else {
			
			result = null;
		}
		//
		return result;
	}
	
	private static <S extends Sequenceable<Relation>> void createNetworkProfile (HashMap<String,GraphProfile<?>> networkProfiles, Graph<?> network, String title, RelationEnvironment egoEnvironment, List<String> partitionLabels) throws PuckException{
		
		// Create network profile
		
		GraphProfile<?> networkProfile = null;
				
		if (title.contains("Ego Network")){
			
			networkProfile = new GraphProfile<Individual>((Graph<Individual>)network,egoEnvironment.getEgo(),partitionLabels);
			
			if (title.equals("Ego Network")){
				
				networkProfile.setSpecificDensities();
			}
			
			
		} else if (network!=null) {
			
			networkProfile = new GraphProfile(network,partitionLabels);
		}

		// Set component types
		if (partitionLabels.contains("COMPONENT")){
			
			Partition<Node<Individual>> nonEgoComponents = ((GraphProfile<Individual>)networkProfile).getNonEgoComponents();

			setComponentTypes(title, egoEnvironment, nonEgoComponents);
		}
		
		// Put network profile
		
		if (networkProfile != null){
			networkProfiles.put(title, networkProfile);
		}
	}
	
/*	private void setEgoNetworks(RelationEnvironment egoEnvironment, List<String> relationModelNames, SequenceCriteria criteria){

//		networkProfiles.put("Ego Network", new GraphProfile<Individual>(getEgoNetwork(criteria, egoEnvironment.getAltersByRole("SELECTED"),egoEnvironment.getRelationsByAlter()),ego));
//		networkProfiles.put("Nonmediated Ego Network", new GraphProfile<Individual>(getNonMediatedEgoNetwork(criteria, egoEnvironment.getAltersByRole("SELECTED"), egoEnvironment.getAltersByRole("ALL"),egoEnvironment.getRelationsByAlter()),ego));
		
//		networkProfiles.get("Ego Network").setSpecificDensities();
//		setComponentTypes("Nonmediated Ego Network", egoEnvironment.getRelationsByAlter(), relationModelNames);
	}*/
	
/*	private void setParcoursNetworks(RelationEnvironment egoEnvironment, String relationModelName, List<String> relationModelNames, List<RelationClassificationType> types, List<RelationClassificationType> mainEventTypes, String defaultReferentRoleName) throws PuckException{
		
		if (mainEventTypes.contains(RelationClassificationType.TREES)){
			for (Relation event : events){
				event.updateReferents(defaultReferentRoleName);
			}
		}*/

		/*		for (RelationClassificationType relationClassificationType : mainEventTypes){

//			networkProfiles.put("Parcours Network_"+relationClassificationType, new GraphProfile<Cluster<Relation>>(getParcoursNetwork(relationClassificationType)));
//			eventTypeNetworkProfiles.put("Parcours Network_"+relationClassificationType, new GraphProfile<Cluster<Relation>>(getParcoursNetwork(relationClassificationType)));
			
			if (partitionLabels.get("Parcours Network_"+relationClassificationType).contains("COMPONENT")){
				setComponentTypes("Parcours Network_"+relationClassificationType, egoEnvironment.getRelationsByAlter(), relationModelNames);
			}
		}*/
		
//		networkProfiles.put("Parcours", new GraphProfile<Relation>(getParcoursGraph()));
//		networkProfiles.put("Extended Parcours", new GraphProfile<Numberable>(getExtendedParcoursGraph(egoEnvironment, types)));
//		networkProfiles.put("Multiple Parcours", new GraphProfile<Relation>(getMultipleParcoursGraph(relationModelName, relationModelNames, egoEnvironment.getAltersByRole("SELECTED"))));
		
/*		eventNetworkProfiles.put("Parcours", new GraphProfile<Numberable>(getParcoursGraph()));
		eventNetworkProfiles.put("Extended Parcours", new GraphProfile<Numberable>(getExtendedParcoursGraph(egoRoleName, roleNames, altersByRoles, rolesByAlter, relationsByAlter)));
		eventNetworkProfiles.put("Multiple Parcours", new GraphProfile<Numberable>(getMultipleParcoursGraph(relationModelName, relationModelNames, altersByRoles.get("SELECTED"))));*/
//	}
	
/*	private void setParcoursIntersectionNetworks(RelationEnvironment egoEnvironment, String relationModelName, List<String> relationModelNames, String alterAttributeLabel, String alterAttributeValue){
		
//		networkProfiles.put("Parcours Intersection Network", new GraphProfile<Individual>(getParcoursIntersectionNetwork(relationModelName, egoEnvironment.getEgoRoleName(), egoEnvironment.getRoleNames(), egoEnvironment.getAltersByRole("SELECTED"),alterAttributeLabel, alterAttributeValue),ego));
			
		if (partitionLabels.get("Parcours Intersection Network").contains("COMPONENT")){
			setComponentTypes("Parcours Intersection Network", egoEnvironment.getRelationsByAlter(), relationModelNames);
		}
	}*/
	
	/*	private void setParcoursSimilarityNetworks(RelationEnvironment egoEnvironment, String relationModelName, List<String> relationModelNames, String pattern, List<RelationClassificationType> mainEventTypes, String alterAttributeLabel, String alterAttributeValue) throws PuckException{
		
//		parcoursSimilarityTrees = new HashMap<RelationClassificationType,Graph<Set<Individual>>>();
		
		for (RelationClassificationType relationClassificationType : mainEventTypes){

//			networkProfiles.put("Parcours Similarity Network_"+relationClassificationType, new GraphProfile<Individual>(getParcoursSimilarityNetwork(relationClassificationType, egoEnvironment.getEgoRoleName(), relationModelName, relationModelNames, egoEnvironment.getRoleNames(), pattern, egoEnvironment.getAltersByRole("SELECTED"), alterAttributeLabel, alterAttributeValue),ego));
			
			if (partitionLabels.get("Parcours Similarity Network_"+relationClassificationType).contains("COMPONENT")){
				setComponentTypes("Parcours Similarity Network_"+relationClassificationType, egoEnvironment.getRelationsByAlter(), relationModelNames);
			}
		}
		
	}*/
	
	
	private static String getComponentType(Cluster<Node<Individual>> component, RelationEnvironment egoEnvironment){
		String result;
		
		result = null;
		
		List<String> relations = new ArrayList<String>();
		int maxDegree = 0;
		
		for (Node<Individual> node : component.getItems()){
			int degree = node.getDegree();
			if (degree>maxDegree){
				relations = new ArrayList<String>();
			}
			if (degree>=maxDegree){
				Individual referent = node.getReferent();
				relations.addAll(egoEnvironment.getRelationsByAlter().get(referent));
				maxDegree = degree;
			}
		}
		if (PuckUtils.containsStrings(relations, "FATHER;PATERNAL_HOME") && PuckUtils.containsStrings(relations,"MOTHER")){
			result = "PARENT";
		} else if (PuckUtils.containsStrings(relations,"FATHER;PATERNAL_HOME")){
			result = "FATHER";
		} else if (PuckUtils.containsStrings(relations,"MOTHER;MATERNAL_HOME")){
			result = "MOTHER";
		} else if (PuckUtils.containsStrings(relations,"SPOUSE;MARITAL_HOME")){
			result = "SPOUSE";
		} else if (PuckUtils.containsStrings(relations,"RELATIVE;RELATIVE_AGNATIC;RELATIVE_UTERINE;RELATIVE_COGNATIC")){
			result = "RELATIVE";
		} else if (PuckUtils.containsStrings(relations, "AFFINE")){
			result = "AFFINE";
		} else if (PuckUtils.containsStrings(relations, "EMPLOYER")){
			result = "EMPLOYER";
		} else if (PuckUtils.containsStrings(relations, "MASTER")){
			result = "MASTER";
		}
		if (result == null) {
			List<String> list = new ArrayList<String>();
			for (String relationModelName : egoEnvironment.getRelationModelNames()){
				Relations egoRelations = egoEnvironment.getEgo().relations().getByModelName(relationModelName);
				if (egoRelations.size()>0){
					RelationModel model = egoRelations.getFirst().getModel();
					for (Role role : model.roles()){
						if (relations.contains(role.getName()) && !list.contains(role.getName())){
							list.add(role.getName());
						}
					}
				}
			}
			Collections.sort(list);
			if (list.size()>0){
				result = "";
				for (String item : list){
					if (result.length()>0){
						result+="-";
					}
					result+=item;
				}
			}
		}
		if (result == null) {
			if (relations.size()>0){
				result = "OTHER";
			} else {
				result = "UNKNOWN";
			}
		}
		//
		return result;
	}
	

	
/*	private void makeAlterRelationPartition (Graph<Individual> network){
		
		for (Node<Individual> node : network.getNodes()){
			List<String> relations = relationsByAlter.get(node.getReferent());
			if (node.getReferent()==ego){
				node.setAttribute("EGO-RELATION", "EGO");
			} else if (relations != null){
				node.setAttribute("EGO-RELATION", relations.toString());
			}
		}
	}*/

	private static <E> void makeAlterRelationPartition (Graph<E> network, Individual ego, Map<Individual,List<String>> relationsByAlter){
		
		for (Node<E> node : network.getNodes()){
			E referent = node.getReferent();
			Individual alter = null;
			if (referent instanceof Individual){
				alter = (Individual)referent;
			} else if (referent instanceof Set<?>){
				if (((Set<Individual>) referent).size()==1){
					for (Individual element : (Set<Individual>)referent){
						alter = element;
					}
				}
			} 
			
			if (alter!=null){
				List<String> relations = relationsByAlter.get(alter);
				if (alter==ego){
					node.setAttribute("EGO-RELATION", "EGO");
				} else if (relations != null){
					node.setAttribute("EGO-RELATION", relations.toString());
				}
			}
		}
	}
	
/*	void writePajekNetwork (StringList pajekBuffer, String title) throws PuckException{
		
		Graph<?> network = (Graph<?>)getNetwork(title);
		if (network.nodeCount()>0){
			pajekBuffer.addAll(PuckUtils.writePajekNetwork(network,networkProfiles.get(title).getPartitionLabels()));
		}*/


/*		if (title.contains("Parcours Similarity Tree") || title.contains("Parcours Network Fused")){
/*			RelationClassificationType relationClassificationType = RelationClassificationType.valueOf(title.substring(title.lastIndexOf("_")+1));
			Graph<Set<Individual>> network = parcoursSimilarityTrees.get(relationClassificationType);
			makeAlterRelationPartition(network);
			GraphUtils.getDepthPartition(network);
			GraphUtils.addNodeLabelsFromPartition(network, "EGO-RELATION");
			pajekBuffer.addAll(PuckUtils.writePajekNetwork(network,partitionLabels.get(title)));*/
/*		} else if (title.contains("Parcours Network") && !title.contains("Fused")){
			Graph<Cluster<Relation>> network = (Graph<Cluster<Relation>>)networkProfiles.get(title).getGraph();
//			Graph<Cluster<Relation>> network = eventTypeNetworkProfiles.get(title).getGraph();
			if (network.nodeCount()>0){
				pajekBuffer.addAll(PuckUtils.writePajekNetwork(network,partitionLabels.get(title)));
			}
		} else if (title.contains("Network")){
			Graph<Individual> network = (Graph<Individual>)networkProfiles.get(title).getGraph();
//			makeAlterRelationPartition(network, relationsByAlter);
//			GraphUtils.addNodeLabelsFromPartition(network, "EGO-RELATION");
			pajekBuffer.addAll(PuckUtils.writePajekNetwork(network,partitionLabels.get(title)));
		} else if (title.contains("Parcours")){
			Graph<Relation> network = (Graph<Relation>)networkProfiles.get(title).getGraph();
//			Graph<Relation> network = eventNetworkProfiles.get(title).getGraph();
			if (network.nodeCount()>0){
				pajekBuffer.addAll(PuckUtils.writePajekNetwork(network,partitionLabels.get(title)));
			}
		}
	}*/



	
/*	private Relation getPreviousEvent(Relation event){
		Relation result;
		
		int index = events.indexOf(event)-1;
		
		if (index>-1){
			result = events.get(index);
		} else {
			result = null;
		}
		//
		return result;
	}*/
	

	

	


	
/*	public void putEvents (Partition<String> eventPartition, EventType eventType){
		
		for (Relation event : events){
			String type = getEventType(eventType,event);
			eventPartition.put(ego.getId()+" "+event.getTypedId(), Value.valueOf(type));
		}
	}
	
	public void putEventPairs (Partition<String> eventPairPartition, EventType eventType){
		
		String previousType = null;
		for (Relation event : events){
			if (previousType == null){
				previousType = getEventType(eventType,event);
			} else {
				String nextType = getEventType(eventType,event);
				eventPairPartition.put(ego.getId()+" "+event.getTypedId(), new Value(new String[]{previousType,nextType}));
				previousType = nextType;
			}
		}
	}

	
	public void putSubSequences (Partition<String> sequencePartition, EventType eventType){
		
		String type = null;
		for (Relation event :events){
			if (type == null){
				type = getEventType(eventType,event);
			} else {
				type += "-"+getEventType(eventType,event);
			}
			if (type!=null){
				sequencePartition.put(ego.getId()+" "+event.getTypedId(), new Value(type));
			}
		}
	}*/
	
/*	public Relation getEvent(int i){
		Relation result;
		
		if (events!=null && events.size()>i){
			result = events.get(i);
		} else {
			result = null;
		}
		//
		return result;
	}
	

	public String getEventType (EventType type, Relation event){
		String result;
		
		if (type==null || event == null){
			result = null;
		} else {
			result = eventTypes.get(type).get(event);
		}
		return result;
	}
	
	public Set<String> getEventTypes (EventType type){
		Set<String> result;
		
		result = new HashSet<String>();
		for (Relation event : events){
			result.add(getEventType(type,event));
		}
		
		//
		return result;
	}*/
	

	// Attention: replace PLACE by ENDPLACE
/*	private void evaluateEvents (){
		
		eventEvaluations = new HashMap<RelationClassificationType,Map<Relation,String>>();
		
		for (RelationClassificationType type : types){
			
			eventEvaluations.put(type, new HashMap<Relation,String>());
			
/*			if (altersByRoles==null && (type==RelationClassificationType.HOST || type==RelationClassificationType.MIG || type==RelationClassificationType.HOSTMIG || type==RelationClassificationType.MIGRATIONTYPE)){
//				setAlters();
//				setAlterRelations("ALL");
			} else	if (type==RelationClassificationType.MOVEMENT){
				setPlaceList();
			} */
			
/*			int i = 0;
			for (Relation event : events){
				
//				String eventType = null;
				Value eventType = RelationValuator.get(event, type.toString());

/*				switch (type){
					case TYPEDID:
						eventType = event.getTypedId()+"";
						break;
					case HOST: 
						eventType = getHostType(event);
						break;
					case MIG:					
						eventType = getRelationTypesAsShortCutString(event,"MIG");
						break;
					case HOSTMIG:
						eventType = getRelationTypesAsShortCutString(event,"HOST")+":"+getRelationTypesAsShortCutString(event,"MIG");
						break;
					case MIGRATIONTYPE:
						eventType = getMigrationType(event);
						break;
					case CHILDMIGRATIONTYPE:
						eventType = getChildMigrationType(event);
						break;
					case TREES:
						eventType = RelationWorker.getLinkTrees(event, true, "GENDER", pattern);
						break;
					case DISTANCE: 
						
						if (RelationValuator.isBirth(event)){
							eventType = "BIRTH";
						} else if (RelationValuator.isDeath(event)){
							eventType = "DEATH";
						} else {
							GeoLevel distance = getDistance(event);
							if (distance != null){
								eventType = distance.dynamic().toString();
							}
						}
						break;
						
					case PLACE: 
						
						if (RelationValuator.isDeath(event)){
							eventType = "DEATH";
						} else {
							Place place = geography.get(event.getAttributeValue("END_PLACE"),level);
							if (place!=null){
								eventType = place.getToponym();
							} 
						}

						break;
						
					case REGION: 
						Place region = geography.getSup(event.getAttributeValue("END_PLACE"),placeNames);
						if (region!=null){
							eventType = region.getName();
						}
						break;
						
					case MOVEMENT:
						eventType = SequenceWorker.getMovement(placeList, i);	
						break;
						
					case TURNOVER:
						eventType = MathUtils.round(getTurnover(event), 2)+"";
						break;
						
					case DATE:
						eventType = event.getAttributeValue(dateLabel);
						break;
						
					case AGE:
						eventType = getAge(event)+"";
						break;
						
					default:
						eventType = null;
					}
					
				if (eventType==null){
					System.err.println("Event type "+type+" is null for event "+event);
					eventType = new Value("UNDEFINED");
				}
					
				eventEvaluations.get(type).put(event, eventType.toString());
				i++;
			}
		}
	}
	
	private GeoLevel getDistance (Relation event){
		GeoLevel result;
		
		Geography geography = Geography.getInstance();
		
		result = null;
		
		if (!RelationValuator.isBirth(event) && !RelationValuator.isDeath(event)){
			Place start = geography.getByHomonym(event.getAttributeValue("START_PLACE"));
			Place end = geography.getByHomonym(event.getAttributeValue("END_PLACE"));
			if (start != null && end != null){
				Place commonAncestor = geography.getCommonAncestor(start, end);
				if (commonAncestor!=null){
					result = commonAncestor.getLevel();
				}
			}
		}
		
		//
		return result;
	}*/
	


	
	private static <S extends Sequenceable<Relation>> Graph<Numberable> getExtendedParcoursGraph (S sequence, RelationEnvironment egoEnvironment, List<ValueSequenceLabel> types, Geography geography) {
		Graph<Numberable> result;
		
		Individual ego = egoEnvironment.getEgo();
		
		result = new Graph<Numberable>();
		result.setLabel("Extended Parcours "+ego);
		
		Relation lastEvent = null;
		for (Relation event : sequence.getStations().values()){
			if (lastEvent != null){
				Link<Numberable> eventLink = result.addArc(lastEvent, event,2);
				eventLink.setTag(ego.getId()+"");
				for (String label : egoEnvironment.getRoleNames()){
					for (Individual alter : event.getIndividuals(label)){
						if (label!=egoEnvironment.getEgoRoleName() || alter!=ego){
							Link<Numberable> alterLink = result.addArc(event, alter, 1);
							alterLink.setTag(label);
						}
					}
				}
			}
			lastEvent = event;
		}
		
		if (sequence.getStations().size()==1){
			result.addNode(lastEvent);
		}
		
		for (String label : egoEnvironment.getRoleNames()){
			Set<Individual> cluster = egoEnvironment.getAltersByRole(label);
			if (cluster!=null){
				for (Individual alter : cluster){
					for (Relation event : alter.relations().getByModel(lastEvent.getModel())){
						if (!sequence.getStations().containsValue(event)){
							if(!RelationValuator.isBirth(event) && !RelationValuator.isDeath(event) && RelationValuator.getYear(event)!=null && RelationValuator.getYear(lastEvent)!=null && RelationValuator.getYear(event)<=RelationValuator.getYear(lastEvent)){
								for (Individual tertius : event.getIndividuals()){
									if (tertius.getId()> alter.getId() && result.getNode(tertius)!=null){
										Link link1 = result.addArc(event, tertius, 1);
										link1.setTag(event.getRoleNamesAsString(tertius));
										Link link2 = result.addArc(event, alter, 1);
										link2.setTag(event.getRoleNamesAsString(alter));
									}
								}
							}
						}
					}
				}
			}
		}
		
		
		types.add(ValueSequenceLabel.DATE);
		types.add(ValueSequenceLabel.AGE);
		

		int order = 1;
		for (Object obj : result.getNodes()){
			Node node = (Node)obj;
			
			if (node.getReferent() instanceof Individual){
				
				node.setAttribute("TYPE", "INDIVIDUAL");

				for (ValueSequenceLabel type : types){
					
					List<String> list = null;
					if (type==ValueSequenceLabel.HOST || type==ValueSequenceLabel.MIG || type==ValueSequenceLabel.HOSTMIG){
						list = egoEnvironment.getRelationsByAlter((Individual)node.getReferent());
					} else {
						list = egoEnvironment.getRolessByAlter((Individual)node.getReferent());
					}
					if (list!=null){
						node.setAttribute(type.toString(), list.toString());
					}
				}
				
				node.setAttribute("ORDER", "0");
				
			} else if (node.getReferent() instanceof Relation){
				
				node.setAttribute("TYPE", "EVENT");
				
				for (ValueSequenceLabel type : types){
					
					Value value = RelationValuator.get((Relation)node.getReferent(), type.toString(), geography);
//					String value = eventEvaluations.get(type).get((Relation)node.getReferent());
					if (value!=null){
						node.setAttribute(type.toString(), value.toString());
					}
					
				}
				
				node.setAttribute("ORDER", order+"");
				order++;
			}
		}
		
		//
		return result;
	}
	
	
/*	private void setSpecificDensities (String networkTitle){
		
		Graph<Individual> network = networkProfiles.get(networkTitle).getGraph();
		
		specificDensities = new HashMap<String,Double>();
		specificDensities.put("PARENT-CHILD", 0.);
		specificDensities.put("SPOUSE", 0.);
		specificDensities.put("SIBLING", 0.);
		specificDensities.put("RELATIVE", 0.);
		specificDensities.put("AFFINE", 0.);
		specificDensities.put("EMPLOYMENT", 0.);
		specificDensities.put("RENT", 0.);
		
		for (Link<Individual> link : network.getLinks()){
			String tag = link.getTag();
			tag = tag.substring(tag.indexOf("'")+1,tag.lastIndexOf("'"));
			String key = null;
			if (tag.equals("FATHER") || tag.equals("MOTHER") || tag.equals("CHILD")){
				key = "PARENT-CHILD";
			} else if (tag.equals("SPOUSE") || tag.equals("SIBLING") || tag.equals("AFFINE")){
				key = tag;
			} else if (tag.contains("RELATIVE")){
				key = "RELATIVE";
			} else if (tag.contains("EMPLOY")){
				key = "EMPLOYMENT";
			} else if (tag.equals("LANDLORD") || tag.equals("LODGER")){
				key = "RENT";
			}
			
			if (key!=null){
				specificDensities.put(key, specificDensities.get(key)+1.);
			}
		}
		
		for (String key : specificDensities.keySet()){
			specificDensities.put(key, MathUtils.percent(specificDensities.get(key),network.lineCount()));
		}
		
	}*/

	
	private static void setComponentTypes(String networkTitle, RelationEnvironment egoEnvironment, Partition<Node<Individual>> nonEgoComponents){
				
		if (nonEgoComponents.size()>0){
			for (Cluster<Node<Individual>> component : nonEgoComponents.getClusters().toList()){
				String type = getComponentType(component, egoEnvironment);
				Value value = new Value(type);
				if (nonEgoComponents.getValues().contains(value)){
					int i = 1;
					while (nonEgoComponents.getValues().contains(value)){
						i++;
						value = new Value(type+" "+i);
					}
				}
				nonEgoComponents.changeClusterValue(component, value);
			}
		}
	}

	
	private static Graph<Individual> getEgoNetwork (String relationModelName, RelationEnvironment egoEnvironment, String pattern, String chainClassification){
		Graph<Individual> result;

		//
		result = new Graph<Individual>("Ego network " + egoEnvironment.getEgo()+" "+ relationModelName+" "+egoEnvironment.getEgoRoleName()+"-"+getSelectedRolesAsString(egoEnvironment.getRoleNames()));

		Map<String,Integer> tagMap = new HashMap<String,Integer>();

		result.addNode(egoEnvironment.getEgo());
		//
		for (Individual alter : egoEnvironment.getAltersByRole("SELECTED")) {
			result.addNode(alter);
		}
		
		List<Individual> referents = result.getReferents();
		
		//
		for (int i=0;i<referents.size();i++){
			Individual ego = referents.get(i);
			for (int j=0;j<i;j++){
				Individual alter = referents.get(j);
				
				for (String tag: NetUtils.getAlterRoles(ego, alter, ToolBox.stringsToInts(pattern), egoEnvironment.getRelationModelNames(), chainClassification)){
					Link<Individual> link = result.addArc(ego, alter,1);
					Integer tagNumber = tagMap.get(tag);
					if (tagNumber == null){
						tagNumber = tagMap.size()+1;
						tagMap.put(tag, tagNumber);
					}
					link.setTag(":"+tagNumber+" '"+tag+"'");
				}
			}
		}
		
		//
		NetUtils.setGenderShapes(result);
		makeAlterRelationPartition(result, egoEnvironment.getEgo(), egoEnvironment.getRelationsByAlter());
		GraphUtils.addNodeLabelsFromPartition(result, "EGO-RELATION");
		
		//
		return result;
	}
	
	private static <S extends Sequenceable<Relation>> Graph<Relation> getParcoursGraph (S sequence, Individual ego){
		Graph<Relation> result;
		
		result = new Graph<Relation>();
		result.setLabel("Parcours "+ego);
		
		Relation previousEvent = null;
		for (Relation event : sequence.getStations().values()){
			if (previousEvent != null){
				Link<Relation> link = result.addArc(previousEvent, event,1);
				link.setTag(ego.getId()+"");
			}
			previousEvent = event;
		}
		
		if (sequence.getStations().size()==1){
			result.addNode(previousEvent);
		}
		
		//
		return result;
	}
	
	private static <S extends Sequenceable<Relation>> Graph<Cluster<Relation>> getSequenceNetwork (S  sequence, RelationEnvironment egoEnvironment, ValueSequenceLabel relationClassificationType, String impersonalAlterLabel) throws PuckException{
		Graph<Cluster<Relation>> result;

		Individual ego = egoEnvironment.getEgo();
		
		result = new Graph<Cluster<Relation>>();
		result.setLabel("Sequence Network_"+relationClassificationType+" "+ego);
		
		Partition<Relation> partition = new Partition<Relation>();
//		Map<Relation,String> valueMap = eventEvaluations.get(relationClassificationType);

		Map<String,Individuals> hostsMap = new HashMap<String,Individuals>();
		hostsMap.put("SPOUSE", new Individuals());
		
		for (Relation event : sequence.getStations().values()){
//			String eventType = valueMap.get(event);
			
			String eventType = "";
			Value value = RelationValuator.get((Relation)event, ego, relationClassificationType.toString(), egoEnvironment, impersonalAlterLabel);
			if (value!=null){
				eventType = value.toString();
			}

			Individuals hosts = hostsMap.get(eventType);
			if (hosts!=null){
				for (Individual host : event.getIndividuals("HOST")){
					if (eventType.equals("SPOUSE") && !host.spouses().contains(ego)){
						// May be possible if hosted by spouse + spouse's landlords
						System.err.println("Non-spouse as spouse for "+ego+": "+host+" "+event);
						continue;
					}
					hosts.put(host);
				}
				if (hosts.size()>1){
					eventType += "_"+hosts.size();
				}
			} else {
				if (eventType.contains("via ")){
					Individuals viaHosts = hostsMap.get(eventType.replace("via ", ""));
					if (viaHosts!=null && viaHosts.size()>1){
						eventType += viaHosts.size();
					}
				}
			}
			partition.put(event, new Value(eventType));
		}
		
		Relation previousEvent = null;
		
		int t=1;
		for (Relation event : sequence.getStations().values()){
			if (previousEvent != null){
				
				List<String> impersonalRelation = RelationValuator.getImpersonalRelationTypes(previousEvent, ego, egoEnvironment.getEgoRoleName(), impersonalAlterLabel, egoEnvironment.getRelationsByAlter());
				
				if (impersonalRelation.contains("TRANSITION")){
					if (partition.getCluster(previousEvent).getValue().toString().equals("TRANSITION")){
						partition.getCluster(event).put(previousEvent);
						result.addNode(partition.getCluster(event));
						result.transferLinks(result.getNode(partition.getCluster(previousEvent)), result.getNode(partition.getCluster(event)));
					} else {
						System.err.println("Ambiguous transition "+ego+" "+t+" "+previousEvent+" "+previousEvent.getIndividuals("HOST")+" *"+partition.getCluster(previousEvent).getValue()+"*");
					}
				} else {
					Link<Cluster<Relation>> link = result.getArc(partition.getCluster(previousEvent), partition.getCluster(event));
					if (link==null){
						link = result.addArc(partition.getCluster(previousEvent), partition.getCluster(event),t);
						link.setTag(ego.getId()+" "+t);
					} else {
						String tag = link.getTag();
						double weight = link.getWeight();
						link.setTag(tag+" "+t);
						link.setWeight(weight*0.1+t);
					}
				}
			}
			previousEvent = event;
			t++;
		}
		
		if (sequence.getStations().size()==1){
			result.addNode(partition.getCluster(previousEvent));
		}
				
		for (Node<Cluster<Relation>> node : result.getNodes().toList()){
			if (node.getReferent().getValue().toString().equals("TRANSITION")){
				result.removeNode(node);
				break;
			}
		}
		
		//
		result.renumberNodes();
		
		if (result.getNodesByLabel("PARENTS").size()==0){
			System.err.println("No parents: "+result);
		}
		
		//
		return result;
	}

	
	private static <S extends Sequenceable<Relation>> Graph<Relation> getMultipleParcoursGraph(S sequence, Segmentation segmentation, String relationModelName, RelationEnvironment egoEnvironment) throws PuckException{
		Graph<Relation> result;
				
		List<Graph<Relation>> graphs = new ArrayList<Graph<Relation>>();
		
		SequenceCriteria criteria = new SequenceCriteria();
		criteria.setMaxAge(1000);
		criteria.setRelationModelNames(egoEnvironment.getRelationModelNames());
		criteria.setRelationModelName(relationModelName);
		
		for (Individual alter : egoEnvironment.getAltersByRole("SELECTED")) {
			EgoSequence fullBiography = SequenceMaker.createPersonalEventSequence(alter, segmentation, criteria);
			EgoSequence sortedBiography = SequenceWorker.split(fullBiography).getById(1);
			
//			SequenceNetworkMaker census = new SequenceNetworkMaker(sortedBiography,criteria);
			if (sortedBiography.getNrStations()>0){
				Graph<Relation> alterGraph = SequenceNetworkMaker.getParcoursGraph(sequence, egoEnvironment.getEgo());
				graphs.add(alterGraph);
			}
		}
		result = GraphUtils.fuse(graphs);
		result.setLabel("Multiple parcours "+egoEnvironment.getEgo());
		//
		return result;
	}
	
	public static Graph<String> getFlatParcoursNetworkNoLoops(Graph<Cluster<Relation>> parcoursNetwork){
		Graph<String>  result;
		result = new Graph<String>(parcoursNetwork.getLabel());
		for (Link<Cluster<Relation>> link : parcoursNetwork.getLinks()){
			if (!link.isLoop()){
				result.addArc(link.getSourceNode().getReferent().getLabel(),link.getTargetNode().getReferent().getLabel());
			}
		}
		//
		return result;
	}
	
	private static Relations getEvents (Individual indi, String relationModelName){
		Relations result;
		
		result = new Relations();
		
		for (Relation event : indi.relations().getByModelName(relationModelName)){
			if (!RelationValuator.isBirth(event)){
				result.add(event);
			}
		}
		//
		return result;
	}

	private static <S extends Sequenceable<Relation>> Graph<Individual> getParcoursSimilarityNetwork (ValueSequenceLabel relationClassificationType, Segmentation segmentation, RelationEnvironment egoEnvironment, String relationModelName, String pattern, String alterAttributeLabel, String alterAttributeValue, String impersonalAlterLabel) throws PuckException{
		Graph<Individual> result;

		Individual ego = egoEnvironment.getEgo();
		//
		result = new Graph<Individual>("Parcours Similarity Network_"+relationClassificationType +" "+ ego+" "+ relationModelName+" "+egoEnvironment.getEgoRoleName()+"-"+getSelectedRolesAsString(egoEnvironment.getRoleNames()));
		
		result.addNode(ego);
		//
		for (Individual alter : egoEnvironment.getAltersByRole("SELECTED")) {
			if (alter.hasAttributeValue(alterAttributeLabel,alterAttributeValue)){
				result.addNode(alter);
			}
		}
		List<Graph<String>> flatParcoursNetworksNoLoops = new ArrayList<Graph<String>>();
		
		Map<Graph<String>,Individual> index = new HashMap<Graph<String>,Individual>();
		Set<Value> clusterValues = new HashSet<Value>();
		
		for (Node<Individual> node : result.getNodes().toListSortedById()){

			Individual referent = node.getReferent();
			SequenceCriteria criteria = new SequenceCriteria();
			criteria.getValueSequenceLabels().add(relationClassificationType);
			criteria.setEgoRoleName(egoEnvironment.getEgoRoleName());
			criteria.setRoleNames(egoEnvironment.getRoleNames());
			criteria.setRelationModelName(relationModelName);
			criteria.setRelationModelNames(egoEnvironment.getRelationModelNames());
			criteria.setPattern(pattern);
			criteria.setImpersonalAlterLabel(impersonalAlterLabel);
			
			EgoSequence sequence = SequenceMaker.createPersonalEventSequence(referent, segmentation, criteria);
//			SequenceNetworkMaker census = new SequenceNetworkMaker(sequence,criteria);

			Graph<Cluster<Relation>> parcoursNetwork = SequenceNetworkMaker.getSequenceNetwork(sequence, egoEnvironment, relationClassificationType, impersonalAlterLabel);
			node.setAttribute("NRTRANSITIONS", parcoursNetwork.arcCount()+"");
			Graph<String> flatParcoursNetworkNoLoops = SequenceNetworkMaker.getFlatParcoursNetworkNoLoops(parcoursNetwork);
			flatParcoursNetworksNoLoops.add(flatParcoursNetworkNoLoops);
			
			index.put(flatParcoursNetworkNoLoops, referent);
			for (Node<Cluster<Relation>> clusterNode : parcoursNetwork.getNodes()){
				clusterValues.add(clusterNode.getReferent().getValue());
			}
		}

		Collections.sort(flatParcoursNetworksNoLoops, new GraphComparatorByArcCount<String>());
		Graph<Graph<String>> distanceGraph = GraphUtils.createDistanceGraph(flatParcoursNetworksNoLoops);
		
		for (Link<Graph<String>> link : distanceGraph.getEdges()){
			Link<Individual> edge = result.addEdge(index.get(link.getSourceNode().getReferent()), index.get(link.getTargetNode().getReferent()));
			List<Graph<String>> pair = new ArrayList<Graph<String>>();
			pair.add(link.getSourceNode().getReferent());
			pair.add(link.getTargetNode().getReferent());
			double maxArcs = new Double(GraphUtils.fuse(pair).arcCount());
			double weight = (maxArcs-link.getWeight())/maxArcs;
			// As Pajek does not allow decimal values:
			edge.setWeight(100*weight);
			edge.setTag(weight+"");
		}
		
		//
		NetUtils.setGenderShapes(result);
		
		//
		return result;
	}
	
	
	private static Graph<Individual> getParcoursIntersectionNetwork (String relationModelName, RelationEnvironment egoEnvironment, String alterAttributeLabel, String alterAttributeValue){
		Graph<Individual> result;
		
		Individual ego = egoEnvironment.getEgo();

		//
		result = new Graph<Individual>("Parcours Intersection Network "+ ego+" "+ relationModelName+" "+egoEnvironment.getEgoRoleName()+"-"+getSelectedRolesAsString(egoEnvironment.getRoleNames()));

		Map<Individual,Set<Relation>> eventMap = new HashMap<Individual,Set<Relation>>();

		result.addNode(ego);
		result.getNode(ego).setAttribute("NREVENTS", getEvents(ego, relationModelName).size()+"");
		Set<Relation> egoEvents = new HashSet<Relation>();
		eventMap.put(ego, egoEvents);
		for (Relation event : getEvents(ego, relationModelName)){
			egoEvents.add(event);
		}
		
		//
		for (Individual alter : egoEnvironment.getAltersByRole("SELECTED")) {
			if (alter.hasAttributeValue(alterAttributeLabel,alterAttributeValue)){
				result.addNode(alter);
				result.getNode(alter).setAttribute("NREVENTS", getEvents(alter,relationModelName).size()+"");
				Set<Relation> alterEvents = new HashSet<Relation>();
				eventMap.put(alter, alterEvents);
				for (Relation event : getEvents(alter, relationModelName)){
					alterEvents.add(event);
				}
			}
		}
		
		List<Individual> referents = result.getReferents();
		
		//
		for (int i=0;i<referents.size();i++){
			Individual first = referents.get(i);
			for (int j=0;j<i;j++){
				Individual second = referents.get(j);
				
				double[] shares = MathUtils.intersectionRates(eventMap.get(first), eventMap.get(second));
				
				if (shares[0]>0){
					Link<Individual> outArc = result.addArc(first, second,shares[0]);
				}
				if (shares[1]>0){
					Link<Individual> inArc = result.addArc(second, first,shares[1]);
				}
			}
		}
		
		//
		NetUtils.setGenderShapes(result);
		
		//
		return result;
	}
	
/*	public Partition<Link<Individual>> getParcoursLinkPartition(RelationClassificationType relationClassificationType){
		Partition<Link<Individual>> result;
		
		GraphProfile<Individual> parcoursNetworkProfile = (GraphProfile<Individual>)networkProfiles.get("Parcours Intersection Network_"+relationClassificationType);
		result = parcoursNetworkProfile.getLinkPartition();
		if (result==null){
			result = GraphUtils.getLinkPartitionByKinship(parcoursNetworkProfile.getGraph());
			parcoursNetworkProfile.setLinkPartition(result);
		}
		//
		return result;
	}
	
	private Map<Value, Double[]> getParcoursSimilarities(RelationClassificationType relationClassificationType) {
		Map<Value, Double[]> result;
		
		GraphProfile<Individual> parcoursNetworkProfile = (GraphProfile<Individual>)networkProfiles.get("Parcours Intersection Network_"+relationClassificationType);
		if (parcoursNetworkProfile.getLinkPartition()==null){
			parcoursNetworkProfile.setLinkPartition(GraphUtils.getLinkPartitionByKinship(parcoursNetworkProfile.getGraph()));
		}
		result = parcoursNetworkProfile.aggregateWeights();
		
		//
		return result;
	}*/
	
	private static String getSelectedRolesAsString (List<String> roleNames){
		String result;
		
		result = "";
		for (String roleName : roleNames){
			if (result.length()>0){
				result+=" ";
			}
			result+=roleName;
		}
		//
		return result;
	}
	
	private static Graph<Individual> getNonMediatedEgoNetwork (String relationModelName, RelationEnvironment egoEnvironment, String pattern, String chainClassification){
		Graph<Individual> result;

		//
		result = new Graph<Individual>("Nonmediated Ego Network " + egoEnvironment.getEgo()+" "+ relationModelName+" "+egoEnvironment.getEgoRoleName()+"-"+getSelectedRolesAsString(egoEnvironment.getRoleNames()));

		Map<String,Integer> tagMap = new HashMap<String,Integer>();
		
		Set<Individual> selectedAlters = egoEnvironment.getAltersByRole("SELECTED");
		Set<Individual> allAlters = egoEnvironment.getAltersByRole("ALL");

		result.addNode(egoEnvironment.getEgo());
		//
		for (Individual alter : selectedAlters) {
			result.addNode(alter);
		}
		Set<Individual> others = new HashSet<Individual>();
		for (Individual medius : allAlters){
			if (!selectedAlters.contains(medius)){
				others.add(medius);
			}
		}
		
		List<Individual> referents = result.getReferents();
		List<Individual> excludedIntermediaries = new ArrayList<Individual>();
		excludedIntermediaries.addAll(referents);
		Graph<Individual> barredEdges = new Graph<Individual>();
		
		int [] maxDepths = ToolBox.stringsToInts(pattern);
		
		// get direct links
		for (int i=0;i<referents.size();i++){
			Individual ego = referents.get(i);
			for (int j=0;j<i;j++){
				Individual alter = referents.get(j);
				
				List<String> alterRoles = NetUtils.getAlterRoles(ego, alter, maxDepths, egoEnvironment.getRelationModelNames(), chainClassification, excludedIntermediaries, barredEdges);
				
				for (String tag: alterRoles){
					Link<Individual> link = result.addEdge(ego, alter,1);
					Integer tagNumber = tagMap.get(tag);
					if (tagNumber == null){
						tagNumber = tagMap.size()+1;
						tagMap.put(tag, tagNumber);
					}
					link.setTag(":"+tagNumber+" '"+tag+"'");
				}
			}
		}
		
		// get indirect links mediated by nodes outside the network
		for (int i=0;i<referents.size();i++){
			Individual ego = referents.get(i);
			for (int j=0;j<i;j++){
				Individual alter = referents.get(j);
				if (result.getEdge(ego, alter)==null && barredEdges.getEdge(ego, alter)==null){
					List<String> alterRoles = new ArrayList<String>();
					for (Individual medius : others){
						if (medius!=ego){
							List<String> firstRoles = NetUtils.getAlterRoles(ego, medius, maxDepths, egoEnvironment.getRelationModelNames(), chainClassification, excludedIntermediaries, barredEdges);
							if (firstRoles.size()>0){
								List<String> secondRoles = NetUtils.getAlterRoles(medius, alter, maxDepths, egoEnvironment.getRelationModelNames(), chainClassification, excludedIntermediaries, barredEdges);
								for (String firstRole : firstRoles){
									for (String secondRole : secondRoles){
										if (!(firstRole.equals("CHILD") && (secondRole.contains("RELATIVE")))){
											String role = firstRole+"S_"+secondRole;
											String reducedRole = NetUtils.reduced(role);
											while (!reducedRole.equals(role)){
												role = reducedRole;
												reducedRole = NetUtils.reduced(role);
											}
											alterRoles.add(reducedRole);
										}
									}
								}
							}
						}
					}
					for (String tag: alterRoles){
						Link<Individual> link = result.addEdge(ego, alter,1);
						Integer tagNumber = tagMap.get(tag);
						if (tagNumber == null){
							tagNumber = tagMap.size()+1;
							tagMap.put(tag, tagNumber);
						}
						link.setTag(":"+tagNumber+" '"+tag+"'");
					}
				}
			}
		}
		
		//
		NetUtils.setGenderShapes(result);
		makeAlterRelationPartition(result, egoEnvironment.getEgo(), egoEnvironment.getRelationsByAlter());
		GraphUtils.addNodeLabelsFromPartition(result, "EGO-RELATION");
		
		//
		return result;
	}

/*	Partition<Node<Individual>> getComponents(String networkTitle) {
		return ((GraphProfile<Individual>)networkProfiles.get(networkTitle)).getNonEgoComponents();
	}

	private ValueSequence getProfile(RelationClassificationType relationClassificationType){
		ValueSequence result;
		
		result = sequenceProfiles.get(relationClassificationType);
		if (result==null){
			result = new ValueSequence(events, eventEvaluations, relationClassificationType);
			sequenceProfiles.put(relationClassificationType, result);
		}
		
		//
		return result;
	}
	
	public Graph<Cluster<Relation>> getEventTypeNetwork (String title){
		Graph<Cluster<Relation>> result;
		
		if (eventTypeNetworkProfiles.get(title)==null){
			result = null;
		} else {
			
			result = eventTypeNetworkProfiles.get(title).getGraph();

		}
		//
		return result;
	}
	
	public Graph<?> getNetwork (String title) {
		Graph<?> result;
		
		if (networkProfiles.get(title)==null){
			result = null;
		} else {
			
			result = networkProfiles.get(title).getGraph();

		}
		//
		return result;
	}

	public List<String> getPartitionLabels (String title) {
		List<String> result;
		
		if (networkProfiles.get(title)==null){
			result = null;
		} else {
			
			result = networkProfiles.get(title).getPartitionLabels();

		}
		//
		return result;
	}*/

	/**
		 * replace by SequenceCensus.setAlterRelations
		 * @param maxDegree
		 * @param maxOrder
		 * @param relationModelNames
		 */
	/*	public void setAlterRelations (int[] maxDegree, List<String> relationModelNames){
			alterRelations = NetUtils.getAlterRelations(ego,alters(),maxDegree,relationModelNames);
		}*/
		
		
	/*	public List<Individual> alters(String alterRoleName){
			List<Individual>  result;
			
			Cluster<Individual> cluster = alters.getCluster(new Value(alterRoleName));
			if (cluster!=null){
				result = cluster.getItems();
			} else {
				result = new ArrayList<Individual>();
			}
			
			
			//
			return result;
		}*/
	
	/*	public List<Individual> alters(){
			List<Individual>  result;
			
			result = alters.getItemsAsList();
			
			//
			return result;
		}*/
	
		
	/*	public Relation getStation (Ordinal order){
			Relation result;
			
			if (order!=null){
				result = stations.get(order);
			} else {
				result = null;
			}
			return result;
		}*/
		

	
	
	

}
