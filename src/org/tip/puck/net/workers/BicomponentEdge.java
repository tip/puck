package org.tip.puck.net.workers;

import java.util.Arrays;

import org.tip.puck.net.Individual;
import org.tip.puck.net.KinType;

/**
 * @author Klaus Hamberger
 * @since Version 0.6
 * @see Net#getBicomponent(Stack<Edge>, Edge, boolean)
 */
public class BicomponentEdge {

	private Individual source;
	private Individual target;
	private KinType kinType;
	private int back;
		
	public BicomponentEdge (Individual source, Individual target, KinType kinType, int back){
	
		this.source = source;
		this.target = target;
		this.kinType = kinType;
		this.back = back;
		
	}

	/**
	 * checks whether two edges are equal
	 * @param o the edge to be compared
	 * @return true if the edges are equal
	 */
	@Override
	public boolean equals (Object o){
		return Arrays.equals(getKey(),((BicomponentEdge)o).getKey());
	}
	
	/**
	 * returns the alter Individual
	 * @return the alter Individual
	 */
	public Individual getTarget(){
		return target;
	}
	
	/**
	 * returns the ego Individual
	 * @return the ego Individual
	 */
	public Individual getSource(){
		return source;
	}
	
	/**
	 * returns the ID numbers of ego and alter
	 * @return an array consisting of the ID numbers of ego and alter
	 */
	private int[] getKey (){
		int[] key = {source.getId(),target.getId()};
		return key;
	}

	
	/**
	 * gets a new hashCode
	 * @return the new hashCode
	 */
	@Override
	public int hashCode (){
		return Arrays.hashCode(getKey());
	}
	
	/**
	 * inverts the arc (permutes ego and alter and changes the line value accordingly)
	 * @return the edge in inverse representation
	 */
	private BicomponentEdge inverse (){
		BicomponentEdge result;
		
		result = new BicomponentEdge(target, source, invType(), back);
		
		//
		return result;
	}

	/**
	 * inverts the arc if alter coincides with a given Individual
	 * @param ego the Individual to be checked
	 * @return the inverse arc if v is equal to alter, the same arc else  
	 */
	public BicomponentEdge adjusted (Individual ego){
		BicomponentEdge result;
		
		if (ego==source) {
			result = this;
		} else if (ego==target) {
			result = inverse();
		} else {
			result = null;
		}
		//
		return result;
	}

	/**
	 * returns the (gendered) kin type of the inverse edge
	 * @return the (gendered) kin type of the inverse edge
	 */
	private KinType invType (){
		KinType result;
		
		switch (kinType){
			case SPOUSE: {
				result = KinType.SPOUSE;
				break;
			}
			case CHILD: {
				result = KinType.PARENT;
				break; //ego.getGender().toInt();
			}
			case PARENT: {
				result = KinType.CHILD;
				break;
			}
			default: {
				result = null;
			}
		}
		//
		return result;
	}

	public KinType getKinType() {
		return kinType;
	}

	public int getBack() {
		return back;
	}
	
	public String toString(){
		return source+" "+target+" "+kinType+" "+back;
	}
	
	

	
}
