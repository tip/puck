package org.tip.puck.sequences.workers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.census.workers.CensusCriteria;
import org.tip.puck.census.workers.RestrictionType;
import org.tip.puck.census.workers.SymmetryType;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.geo.Geography;
import org.tip.puck.geo.Place;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.GraphProfile;
import org.tip.puck.graphs.Node;
import org.tip.puck.graphs.workers.GraphUtils;
import org.tip.puck.net.Attributable;
import org.tip.puck.net.Individual;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.RelationEnvironment;
import org.tip.puck.net.relations.workers.RelationValuator;
import org.tip.puck.net.relations.workers.RelationWorker;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionSequence;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.EgoSequence;
import org.tip.puck.sequences.Ordinal;
import org.tip.puck.sequences.Sequence;
import org.tip.puck.sequences.Sequenceable;
import org.tip.puck.sequences.Sequences;
import org.tip.puck.sequences.ValueSequence;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.ToolBox;
import org.tip.puck.util.Value;

import com.mysql.jdbc.StringUtils;

public class SequenceValuator<S extends Sequenceable<E>,E> {
	
	public enum EndogenousLabel {
		ID,
		NAME,
		PROFILE,
		LENGTH,
		NREVENTS,
		SUPPORT,
		NRSTATIONS,
		CENTERS,
		NRCENTERS,
		CENTERSNOSTART,
		NRCENTERSNOSTART,
		NRINTERNALMOVES,
		NREXTERNALMOVES,
		NRDIRECTRETURNS,
		NRDIRECTRETURNS_NORM,
		NRCYCLES,
		NRCYCLES_NORM,
		FIRSTTIME,
		MOVEMENTS
	}
	
	private S sequence;
	

	private List<Place> placeList;

	
	public SequenceValuator(S source, SequenceCriteria criteria) throws PuckException{
		
		this.sequence = source;
		
//		} else	if (label.equals("MOVEMENT")){
		
		setPlaceList(criteria.getLevel(), criteria.getGeography());
//		} 

	}
	
	Value getStationValue(S sequence, Ordinal time, Segmentation segmentation, SequenceCriteria sequenceCriteria, PartitionCriteria valueSequenceCriteria, RelationEnvironment egoEnvironment) throws PuckException{
		Value result;
		
		result = null;
		
		String label = valueSequenceCriteria.getLabel();
		Object labelParameter = valueSequenceCriteria.getLabelParameter();
		
		Individual ego = null;
		if (sequence instanceof EgoSequence){
			ego = ((EgoSequence)sequence).getEgo();
		}

		E station = sequence.getStation(time);
				
		if (label.equals("LIFE_STATUS")){
			
			result = getLifeStatus(ego,time,station);
			
		} else if (label.equals("ALL_KIN")){
			
			CensusCriteria censusCriteria = new CensusCriteria();
			censusCriteria.setPattern(sequenceCriteria.getPattern());
			censusCriteria.setChainClassification(sequenceCriteria.getChainClassification());
			censusCriteria.setRelationAttributeLabel(sequenceCriteria.getLocalUnitLabel());
			censusCriteria.setRestrictionType(RestrictionType.ALL);
			censusCriteria.setSymmetryType(SymmetryType.INVERTIBLE);
			censusCriteria.setClosingRelation("TOTAL");
			
			result = new Value(RelationWorker.getAllKinCensus(segmentation, (Relation)station, censusCriteria));
			
		} else {
						
			if (IndividualValuator.isTimeDependent(label)){
				labelParameter = time+"";
			} else if (station!=null && label.contains("_POPULATION")){
				labelParameter = ((Relation)station).getId();
			} else if (label.equals("TREES")){
				labelParameter = sequenceCriteria.getPattern();
			} else if (label.equals("DATE")){
				label = sequenceCriteria.getDateLabel();
			} else if (label.equals("TURNOVER")){
				labelParameter = sequence.getPreviousStation(time);
			} else if (label.contains("PLACE")){
				String[] labels = ToolBox.splitLastPart(label, "_");
				label = labels[0];
				labelParameter = labels[1];
			} else if (label.equals("HOST") || label.contains("MIG")){
				labelParameter = egoEnvironment;
				if (labelParameter==null){
/*				try {
					labelParameter = new RelationEnvironment(((Sequenceable<Relation>)sequence).getStations().values(),((EgoSequence)sequence).getEgo(),sequenceCriteria.getEgoRoleName(),sequenceCriteria.getRoleNames(), sequenceCriteria.getRelationModelNames());
					((RelationEnvironment)labelParameter).setAlterRelations(((Sequenceable<Relation>)sequence).getStations().values(),((EgoSequence)sequence).getEgo(),sequenceCriteria.getEgoRoleName(),"ALL", sequenceCriteria.getRelationModelNames(), sequenceCriteria.getPattern(), sequenceCriteria.getChainClassification());
					((RelationEnvironment)labelParameter).setThreshold(sequenceCriteria.getThreshold());
				} catch (ClassCastException e) {*/
					throw PuckExceptions.INVALID_PARAMETER.create(sequence+" is not a Relation sequence");
				}
			}
			//
			result = getStationValue(ego,station,segmentation,sequenceCriteria,label, labelParameter);
		}
		//
		return result;
	}
	

	
	/**
	 * @param station
	 * @param indicators
	 * @param pattern
	 * @return
	 * @throws PuckException 
	 */
	public Value getStationValue (final Individual ego, final E station, final Segmentation segmentation, final SequenceCriteria criteria, String label, Object labelParameter) throws PuckException{
		Value result;
						
		result = null;
				
		if (station instanceof Relation && segmentation.getCurrentRelations().contains((Relation)station)){

			result = RelationValuator.get((Relation)station, segmentation, ego, label, labelParameter, criteria);
		}
		//
		return result;
	}

	
	/**
	 * 
	 * @param individual
	 * @param label
	 * 
	 * @return
	 * @throws PuckException 
	 */
	public static <S extends Sequenceable<E>,E> Value get(final S source, final Segmentation segmentation, final String label, final String parameter, final SequenceCriteria criteria, final RelationEnvironment egoEnvironment, final GraphProfile networkProfile, Map<String,Sequences<ValueSequence,Value>> valueSequenceMaps, Map<String,PartitionSequence<S>> partitionSequences) throws PuckException {
		Value result;
		
		if (parameter!=null && valueSequenceMaps!=null && valueSequenceMaps.containsKey(parameter)){

			result = get((ValueSequence)valueSequenceMaps.get(parameter).getById(source.getId()),label, criteria);

		} else if (parameter!=null && partitionSequences!=null && partitionSequences.containsKey(parameter)){

			result = get(((PartitionSequence<S>)partitionSequences.get(parameter)).getValueSequence(source),label, criteria);

		} else if (egoEnvironment != null){
						
			result = get(source,segmentation,label,parameter, criteria, egoEnvironment, networkProfile);

		} else {
			
			result = get(source,label,parameter, criteria);
		}
		
		//
		return result;
	}
	
	
	public static <S extends Sequenceable<E>,E> Value get(final S source, final String label, final SequenceCriteria criteria) {
		return get(source,label,null,criteria);
	}


	/**
	 * 
	 * @param individual
	 * @param label
	 * 
	 * @return
	 */
	public static <S extends Sequenceable<E>,E> Value get(final S source, final String label, final String parameter, final SequenceCriteria criteria) {
		Value result;
				
		result = null;
		
//		List<E> events = new ArrayList<E>(source.getStations().values());

//		int length = getNrEvents(source);
				
//		if (length != 0){
			
			//
			EndogenousLabel endogenousLabel;
			try {
				endogenousLabel = EndogenousLabel.valueOf(label.replace(" ", "_"));
			} catch (IllegalArgumentException exception) {
				endogenousLabel = null;
			}

			if (endogenousLabel == null) {
				
				if (label.equals("MAX_DISTANCE")){
					
					GeoLevel maxDistance = getMaxDistance(source, criteria.getGeography());
					if (maxDistance != null){
						result = new Value(maxDistance);
					}
					
				} else if (label.equals("MEAN_NR_MOVES")){
					
					Map<GeoLevel, Integer> distanceProfile = getDistanceProfile(source, criteria.getGeography());
					if (distanceProfile != null){
						result = new Value(distanceProfile);
					}
					
				} else if (label.equals("EVENTS")){

					result = get(source,"SUPPORT",parameter, criteria);
					
/*					String typeName = label.substring(label.indexOf("_")+1);

					List<String> types = new ArrayList<String>();
					for (E event : source.getStations().values()){
						String type = eventEvaluations.get(RelationClassificationType.valueOf(typeName)).get(event);
						if (!types.contains(type)){
							types.add(type);
						}
					}
					result = new Value(types);	*/		
					
				} else if (source instanceof EgoSequence){

					result = Value.valueOf(IndividualValuator.get(((EgoSequence)source).getEgo(),label,criteria.getGeography()));
				}
					
				
			} else {
					
				switch (endogenousLabel) {
				case ID:
					result = new Value(source.getId());
					break;
				case NAME:
					result = new Value(source.getLabel());
					break;
				case PROFILE:
					result = Value.valueOf(((Sequence)source).toValueString());
					break;
				case LENGTH:
					result = new Value(source.getStations().size());
					break;
				case NREVENTS:
					result = new Value(getNrEvents(source));
					break;
				case SUPPORT:
					result = Value.valueOf(((ValueSequence)source).getSupportAsList());
					break;
				case NRSTATIONS:
					result = Value.valueOf(((ValueSequence)source).getNrStations());
					break;
				case CENTERS:
					result = Value.valueOf(((ValueSequence)source).getCentersAsList());
					break;
				case NRCENTERS:
					result = Value.valueOf(((ValueSequence)source).getNrCenters());
					break;
				case CENTERSNOSTART:
					result = Value.valueOf(((ValueSequence)source).getCentersWithoutStartAsList());
					break;
				case NRCENTERSNOSTART:
					result = Value.valueOf(((ValueSequence)source).getNrCentersWithoutStart());
					break;
				case NRINTERNALMOVES:
					result = Value.valueOf(((ValueSequence)source).getNrLoops());
					break;
				case NREXTERNALMOVES:
					Value internalMoves = get(source,"NRINTERNALMOVES",criteria);
					result = new Value(getNrEvents(source) - 1); 
					if (internalMoves != null){
						result = new Value(result.intValue() - internalMoves.intValue());
					}
					break;
				case NRDIRECTRETURNS:
					result = Value.valueOf(((ValueSequence)source).getNrDirectReturns());
					break;
				case NRDIRECTRETURNS_NORM:
					result = Value.valueOf(MathUtils.percent(get(source,"NRDIRECTRETURNS",criteria).intValue(), getNrEvents(source)-1));
					break;
				case NRCYCLES:
					result = Value.valueOf(((ValueSequence)source).getNrCycles());
					break;
				case NRCYCLES_NORM:
					result = Value.valueOf(MathUtils.percent(get(source,"NRCYCLES",criteria).intValue(), getNrEvents(source)-2));
					break;
				case FIRSTTIME:
					result = Value.valueOf(((ValueSequence)source).getFirstTime());
					break;
				case MOVEMENTS:
					result = Value.valueOf(((ValueSequence)source).getMovements());
					break;
				default:
					result = null;
				}
			}
//		}
		//
		return result;
	}

	
	/**
	 * 
	 * @param individual
	 * @param label
	 * 
	 * @return
	 * @throws PuckException 
	 */
	public static <S extends Sequenceable<E>,E> Value get(final S source, final Segmentation segmentation, final String label, final String parameter, final SequenceCriteria criteria, RelationEnvironment egoEnvironment, GraphProfile networkProfile) throws PuckException {
		Value result;
				
		//
		EndogenousLabel endogenousLabel;
		try {
			endogenousLabel = EndogenousLabel.valueOf(label.replace(" ", "_"));
		} catch (IllegalArgumentException exception) {
			endogenousLabel = null;
		}

		if (endogenousLabel == null) {
			
/*			GraphProfile networkProfile = null;
			
			if (isRelationClassificationType(parameter)){
				if (eventTypeNetworkProfiles!=null){
					networkProfile = eventTypeNetworkProfiles.get("Parcours Network_"+parameter);
				}
				if (networkProfile == null && networkProfiles!=null){
					networkProfile = networkProfiles.get("Parcours Intersection Network_"+parameter);
				}
			} else if (networkProfiles!=null){
				networkProfile = networkProfiles.get("Nonmediated Ego Network");
			}*/
			
/*			int length = getNrEvents(source);
			
			if (length == 0){
				
				result = null;
				
				
			} else */
				
				if (label.contains("AGEFIRST")){
				
				String[] subLabels = label.split("_");
				Ordinal time = null;
				Integer age = null;
				Object stationParameter = null;
				
				if (subLabels.length==1){
					time = source.getFirstTime();
				} else {
					if (subLabels[1].equals("CHILDMIGRATIONTYPE")){
						
						stationParameter = egoEnvironment;	
					}
					ValueSequence valueSequence = new ValueSequence((EgoSequence)source, segmentation, subLabels[1],stationParameter, criteria);
					if (subLabels.length==2){
						time = valueSequence.getFirstTimeNonNull();
					} else {
						time = valueSequence.getFirstTime(new Value(subLabels[2]));
					}
				}
				
				if (time!=null){
					age = IndividualValuator.ageAtYear(((EgoSequence)source).getEgo(), time.getYear());
				}
				result = Value.valueOf(age);

			} else if (label.equals("NRALTERSPEREVENT")){
				
				result = new Value(MathUtils.percent(egoEnvironment.getAltersByRole(parameter).size(), 100*getNrEvents(source)));
				
			} else if (label.equals("MEAN_COVERAGE")){
				
				if (getNrEvents(source)==0){
					result = null;
				} else {
					result = Value.valueOf(egoEnvironment.getMeanCoverage());
				}
				
			} else if (label.equals("MAX_COVERAGE")){
				
				if (getNrEvents(source)==0){
					result = null;
				} else {
					result = Value.valueOf(egoEnvironment.getMaxCoverage());
				}
				
			} else if (label.equals("MAIN_ALTERS")){
				
				result = Value.valueOf(egoEnvironment.getMaxCoverageAlters());
				
			} else if (label.equals("MAIN_RELATIONS")){
				
				result = Value.valueOf(egoEnvironment.getMaxCoverageRelations());
				
			} else if (label.contains("NRALTERS")){
				
				result = Value.valueOf(egoEnvironment.getAltersByRole(parameter).size());
				
			} else if (label.contains("SAMESEXALTERS")){

				int sameSexAlters = 0;
				for (Individual alter : egoEnvironment.getAltersByRole(parameter)){
					if (alter.getGender()==((EgoSequence)source).getEgo().getGender()){
						sameSexAlters++;
					}
				}
				
				int nrAlters = egoEnvironment.getAltersByRole(parameter).size();
				
				result = Value.valueOf(MathUtils.percentNotInfinite(sameSexAlters, nrAlters));
				
			} else if (label.contains("SAMEPLACEALTERS")){

				// Attention optionalize geolevel parameter
				GeoLevel level = GeoLevel.valueOf("TOWN");
				Geography geography = criteria.getGeography();
				
				int samePlaceAlters = 0;
				Place egoBirthPlace = geography.getPlace(((EgoSequence)source).getEgo().getAttributeValue("BIRT_PLAC"),level);
				
				for (Individual alter : egoEnvironment.getAltersByRole(parameter)){
					Place alterBirthPlace = geography.getPlace(alter.getAttributeValue("BIRT_PLAC"),level);
					if (egoBirthPlace != null && egoBirthPlace.equals(alterBirthPlace)){
						samePlaceAlters++;
					}
				}
				
				int nrAlters = egoEnvironment.getAltersByRole(parameter).size();
				
				result = Value.valueOf(MathUtils.percentNotInfinite(samePlaceAlters, nrAlters));
				
			} else if (label.contains("MEAN_BETWEENNESS")){
				
				result = Value.valueOf(networkProfile.getMeanBetweenness());
				
			} else if (label.contains("MAX_BETWEENNESS")){

				result = Value.valueOf(networkProfile.getMaxNonEgoBetweenness());
				
			} else if (label.contains("CENTRAL_ALTERS")){
				
				if (networkProfile.nodeCount()<3){
					result = null;
				} else {
					result = Value.valueOf(networkProfile.getCentralAlters());
				}

				
			} else if (label.contains("CENTRAL_RELATIONS")){
				
				if (networkProfile.nodeCount()<3){
					result = null;
				} else {
					List<String> relations = new ArrayList<String>();
					for (Individual alter : ((GraphProfile<Individual>)networkProfile).getCentralAlters()){
						for (String relation : egoEnvironment.getRelationsByAlter(alter)){
							if (!relations.contains(relation)){
								relations.add(relation);
							}
						}
					}
					result = Value.valueOf(relations);
				}
				
			} else if (label.contains("CONNECTED_NETWORK_RELATIONS")){
				
				result = Value.valueOf(getConnectedRelations(networkProfile.getNonEgoComponents(),egoEnvironment.getRelationsByAlter()));

			} else if (label.contains("NETWORK_RELATIONS")){
				
				result = Value.valueOf(getNetworkRelations(networkProfile.getGraphWithoutEgo(),egoEnvironment.getRelationsByAlter()));

			} else if (label.contains("RELATIONS")){
				
				List<String> relations = egoEnvironment.getRelationsByRole(parameter);
								
				if (parameter.equals("HOST")){
					relations.addAll(egoEnvironment.getImpersonalRelations());
				}
				
				relations = RelationValuator.reduceRelations(relations, parameter);
						
				result = Value.valueOf(relations);

	/*		} else if (label.equals("NREVENTS")){
				
				result = Value.valueOf(length);*/
				
			} else if (label.contains("EGO-BETWEENNESS")){
				
				result = Value.valueOf(networkProfile.getEgoBetweenness());
				
			} else if (label.contains("SIZE")){
				
				result = Value.valueOf(networkProfile.nodeCount());
				
			} else if (label.contains("TIES")){

				result = Value.valueOf(networkProfile.nonNullLineCount());
				
			} else if (label.contains("DENSITY")){

				result = Value.valueOf(networkProfile.density());
				
			} else if (label.contains("DENSITY_NOLOOPS")){

				result = Value.valueOf(networkProfile.densityWithoutLoops());
				
			} else if (label.contains("SDENSITY")){
				
				Double density = networkProfile.getSpecificDensity(parameter);
				
				if (density==0.){
					result = null;
				} else {
					result = Value.valueOf(density);
				}
				
			} else if (label.contains("MEANDEGREE")){

				result = Value.valueOf(networkProfile.meanDegree());
							
			} else if (label.contains("MEANDEGREE_NOLOOPS")){

				result = Value.valueOf(networkProfile.meanDegreeWithoutLoops());
				
			} else if (label.contains("MEANDEGREE_NORM")){

				result = Value.valueOf(networkProfile.meanDegreeNormalized());
				
			} else if (label.contains("MEANDEGREE_NOLOOPS_NORM")){

				result = Value.valueOf(networkProfile.meanDegreeWithoutLoopsNormalized());
							
			} else if (label.contains("NRCOMPONENTS")){

				result = Value.valueOf(networkProfile.getNonEgoComponents().size());
							
			} else if (label.contains("NRISOLATES")){

				result = Value.valueOf(networkProfile.getNonEgoComponents().nrSingletons());
				
			} else if (label.contains("MAXCOMPONENT")){

				result = Value.valueOf(networkProfile.getNonEgoComponents().maxClusterSize());
							
			} else if (label.contains("NRCOMPONENTS_NORM")){

				if (networkProfile.getNonEgoComponents().size()==0){
					result = null;
				} else {
					result = Value.valueOf(networkProfile.getNonEgoComponents().meanShare());
				}
				
			} else if (label.contains("MAXCOMPONENT_NORM")){

				if (networkProfile.getNonEgoComponents().size()==0){
					result = null;
				} else {
					result = Value.valueOf(networkProfile.getNonEgoComponents().maxShare());
				}
							
			} else if (label.contains("NRISOLATES_NORM")){

				if (networkProfile.getNonEgoComponents().size()==0){
					result = null;
				} else {
					result = Value.valueOf(networkProfile.getNonEgoComponents().singletonShare());
				}
				
			} else if (label.contains("BROKERAGE")){

				result = Value.valueOf(networkProfile.brokerage());
				
			} else if (label.contains("EFFICIENT_SIZE")){

				result = Value.valueOf(networkProfile.efficientSize());
				
			} else if (label.contains("EFFICIENCY")){

				result = Value.valueOf(networkProfile.efficiency());
				
			} else if (label.contains("SIMILARITY")){
				
				// Check: only if network = "Parcours Intersection Network_"+relationClassificationType;
				if (networkProfile.getLinkPartition()==null){
					networkProfile.setLinkPartition(GraphUtils.getLinkPartitionByKinship(networkProfile.getGraph()));
				}
				result = Value.valueOf(networkProfile.aggregateWeights());
				
			} else if (label.contains("ECCENTRICITY")){

				result = Value.valueOf(networkProfile.getEccentricity());

			} else if (label.contains("CONCENTRATION")) {

				result = Value.valueOf(MathUtils.herfindahl(networkProfile.getNonEgoComponents().clusterSizes()));
				
			} else {
				
				result = get(source,label,parameter,criteria);
			}
			
		} else {
			
			result = get(source,label,parameter,criteria);
		}
		//
		return result;
	}
	
	private static <S extends Sequenceable<E>,E> int getNrEvents(S source){
		int nrEvents;
		
		nrEvents = 0;
		for (E event : source.getStations().values()){
			if (event instanceof Attributable){
				if (!RelationValuator.isBirth((Attributable)event) && !RelationValuator.isDeath((Attributable)event)){
					nrEvents++;
				}
			} else {
				nrEvents++;
			}
		}
		//
		return nrEvents;
		
	}
	
	static <E> Value getLifeStatus(Individual ego, Ordinal time, E station){
		Value result;
		
		String lifeStatus = IndividualValuator.lifeStatusAtYear(ego, time.getYear());
		
		if (station==null){
			if (time.getYear().toString().equals(ego.getAttributeValue("BIRT_DATE"))){
				lifeStatus = "UNBORN";
			} else if (time.getYear().toString().equals(ego.getAttributeValue("DEAT_DATE"))){
				lifeStatus = "DEAD";
			}
		}
		result = Value.valueOf(lifeStatus);
		//
		return result;
	}
	
	

	
	private void setPlaceList(GeoLevel level, Geography geography) throws PuckException{
		
		String placeLabel = "END_PLACE";
		
		//
		placeList = new ArrayList<Place>();
		
		for (E station : sequence.getStations().values()){
			
			if (!(station instanceof Attributable)){

				throw PuckExceptions.INVALID_PARAMETER.create(station +" is not attributable.");
			
			} else {
				
				Place place = geography.getPlace(((Attributable)station).getAttributeValue(placeLabel),level);

				if (place==null){
					place = new Place(level,"UNKNOWN",null);
				}
				placeList.add(place);
			}
		}
	}

/*	private static boolean isRelationClassificationType(String parameter){
		boolean result;
		
		result = false;
		
		if (parameter!=null){
			try {
				if (SequenceCriteria.RelationClassificationType.valueOf(parameter)!=null){
					result = true;
				}
			} catch (IllegalArgumentException iae) {
			}
		}
		
		//
		return result;
	}*/

	
	
	
	private static <S extends Sequenceable<E>,E> GeoLevel getMaxDistance(S source, Geography geography){
		GeoLevel result;
		
		result = null;
		
		for (E event : source.getStations().values()){
			
			if (!RelationValuator.isBirth(event) && !RelationValuator.isDeath(event)){
				
				String distance = RelationValuator.get((Relation)event,"DISTANCE",geography).stringValue();
				if (!StringUtils.isNullOrEmpty(distance) && !distance.equals("UNDEFINED")){
					if (result == null || GeoLevel.valueOf(distance).compareTo(result)<0){
						result = GeoLevel.valueOf(distance);
					}
				}
			}
		}
				
		//
		return result;
	}
	
	private static <S extends Sequenceable<E>,E> Map<GeoLevel, Integer> getDistanceProfile (S source, Geography geography){
		Map<GeoLevel, Integer> result;
		
		result = new HashMap<GeoLevel, Integer>();
		
		for (E event : source.getStations().values()){
			GeoLevel distance = RelationValuator.getDistance(geography,(Relation)event, "START_PLACE", "END_PLACE");
			if (distance!=null){
				distance = distance.dynamic();
				if (result.containsKey(distance)){
					result.put(distance, result.get(distance)+1);
				} else {
					result.put(distance, 1);
				}
			}
		}
		//
		return result;
	}
	
	
	private static List<String[]> getConnectedRelations(Partition<Node<Individual>> partition, Map<Individual,List<String>> relationsByAlter){
		List<String[]> result;
		
		result = new ArrayList<String[]>();
		
		for (Cluster<Node<Individual>> cluster : partition.getClusters()){
			List<String> relations = new ArrayList<String>();
			for (Node<Individual> node : cluster.getItems()){
				Individual referent = node.getReferent();
				for (String relation : relationsByAlter.get(referent)){
					if (!relations.contains(relation)){
						relations.add(relation);
					}
				}
			}
			Collections.sort(relations);
			for (String firstRelation : relations){
				for (String secondRelation : relations){
					if (!firstRelation.equals(secondRelation)){
						String[] pair = new String[]{firstRelation,secondRelation};
						if (!result.contains(pair)){
							result.add(pair);
						}
					}
				}
			}
		}
		
		//
		return result;
	}
	
	
	
	private static List<String> getNetworkRelations(Graph<Individual> graph, Map<Individual,List<String>> relationsByAlter){
		List<String> result;
		
		result = new ArrayList<String>();
		for (Node<Individual> node : graph.getNodes().toListSortedById()){
			Individual referent = node.getReferent();
			for (String relation : relationsByAlter.get(referent)){
				if (!result.contains(relation)){
					result.add(relation);
				}
			}
		}
		Collections.sort(result);
		
		//
		return result;

	}
	
	

}
