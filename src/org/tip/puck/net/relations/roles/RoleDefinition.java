package org.tip.puck.net.relations.roles;

import org.tip.puck.net.AlterAge;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.relations.roles.RoleActorPair.Adjustable;

public class RoleDefinition implements Comparable<RoleDefinition> {
	
	Role self;
	RoleActor actor;
	String letters;
	
	public RoleDefinition (Role self, RoleActor actor){
		
		this.self = self;
		this.actor = actor;
	}
	
	public RoleDefinition (String selfName, String alterName, String roleName, Gender egoGender, Gender alterGender, AlterAge alterAge, String egoName){
		
		this.self = new Role(selfName);
		this.actor = new RoleActor(new Role(alterName),new MetaRole(roleName,egoGender,alterGender,alterAge),egoName);
	}
	
	public RoleDefinition clone(){
		return new RoleDefinition(self,actor.clone());
	}
	
	public String toString(){
		
		String name = self.getName();
		if (self.hasSelfName()){
			name = "";
		}
				
		return 	actor.getIndividual()+"\t"+name+"\t"+actor.getRole().getName()+"\t"+actor.getRole().getEgoGender().toStringNoUnknown()+"\t"+actor.getRole().getAlterGender().toStringNoUnknown()+"\t"+actor.getRole().getAlterAge().toStringNoUnknown();

	}
	
	public Role getAlterTerm(){
		return actor.getIndividual();
	}
	
	public Role getSelfTerm(){
		return self;
	}
	
	public RoleActor getAlter(){
		return actor;
	}
	
	public RoleActor getSelf(){
		RoleActor result;
		
		result = new RoleActor(getSelfTerm(), new MetaRole(MetaRole.invertName(getRoleName()),getEgoGender(),getEgoGender(),getAlterAge().invert()),actor.selfName);

		//
		return result;
	}
	
	public MetaRole getRole(){
		return actor.getRole();
	}

	
	public String getRoleName(){
		return actor.getRole().getName();
	}
	
	public Gender getAlterGender(){
		return actor.getRole().getAlterGender();
	}
	
	public AlterAge getAlterAge(){
		return actor.getRole().getAlterAge();
	}
	
	public Gender getEgoGender(){
		return actor.getRole().getEgoGender();
	}
	
	public int compareTo(RoleDefinition other){
		int result;
		
		RoleActor self = getSelf();
		RoleActor otherSelf = other.getSelf();
		
		if (self.hasSelfName() && !otherSelf.hasSelfName()){
			
			result = -1;
			
		} else if (!self.hasSelfName() && otherSelf.hasSelfName()){
			
			result = 1;
			
		} else {
		
			result = new RoleActorComparator().compare(getSelf(),other.getSelf());
			
			if (result == 0){
				result = getAlter().compareTo(other.getAlter());
			}
		}
		//
		return result;
	}
	
	public int compareTo(RoleDefinition other, RoleRelations relations){
		int result;
		
		if (other==null){
			
			result = -1;
			
		} else {
			
			result = new RoleActorComparator(relations).compare(this.getAlter(),other.getAlter());
			
			if (result == 0){
				
				result = this.getSelfTerm().compareTo(other.getSelfTerm());
			}
		}
		
		//
		return result;
	}
	
	public boolean equals (Object obj){
		boolean result;
		
		if (obj==null){
			
			result = false;
			
		} else {
			
			RoleDefinition other = (RoleDefinition)obj;
			result = self.equals(other.self) && actor.equals(other.actor);
		}
		//
		return result;
	}
	
	public RoleActor getSelfActor (){
		RoleActor result;
		
		result = new RoleActor(self,new MetaRole(null,actor.getEgoGender(),null,AlterAge.UNKNOWN),actor.selfName);
		
		//
		return result;
	}
	
	public RoleActorPair getPair (RoleRelations relations, Adjustable adjustable){
		RoleActorPair result;
		
		result = new RoleActorPair(relations,getSelfActor(),actor,adjustable);
		
		//
		return result;
	}
	
	public RoleDefinition standard(RoleRelations relations){
		return getPair(relations,Adjustable.NONE).getStandardRoleDefinition(relations);
	}

	public RoleDefinition inverse (RoleRelations relations){
		RoleDefinition result;
				
		result = getPair(relations,Adjustable.NONE).reciprocal(relations).getRoleDefinition();
		//
		return result;
	}

	public String getLetters() {
		return letters;
	}

	public void setLetters(String letters) {
		this.letters = letters;
	}
	
	public boolean specifies (RoleDefinition definition){
		boolean result;
		
		result = this.getSelfTerm().equals(definition.getSelfTerm()) && this.getAlter().specifies(definition.getAlter());
		
		//
		return result;
	}
	
	public RoleDefinition cloneWithEgoGender(Gender egoGender){
		RoleDefinition result;
		
		result = clone();
		result.getAlter().setEgoGender(egoGender);
		
		//
		return result;
	}
	
	public RoleDefinition cloneWithAlterGender(Gender alterGender){
		RoleDefinition result;
		
		result = clone();
		result.getAlter().setAlterGender(alterGender);
		
		//
		return result;
	}
	
	public RoleDefinition cloneWithAlterAge(AlterAge alterAge){
		RoleDefinition result;
		
		result = clone();
		result.getAlter().setAlterAge(alterAge);
		
		//
		return result;
	}
	
	public boolean isEgoGenderComplement(RoleDefinition definition){
		boolean result;
		
		result = definition.cloneWithEgoGender(this.getEgoGender()).equals(this);
		
		//
		return result;
	}
	
	public boolean isAlterGenderComplement(RoleDefinition definition){
		boolean result;
		
		result = definition.cloneWithAlterGender(this.getAlterGender()).equals(this);
		
		//
		return result;
	}
	
	public boolean isAlterAgeComplement(RoleDefinition definition){
		boolean result;
		
		result = definition.cloneWithAlterAge(this.getAlterAge()).equals(this);
		
		//
		return result;
	}
	
	
	
	
}
