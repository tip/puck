package org.tip.puck.visualization.exporter.implementations;

/**
 * @date May 12, 2012
 * @author Edoardo Savoia
 */

import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.gephi.preview.api.*;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;
import org.tip.puck.visualization.exporter.Exporter;
import processing.core.PGraphicsJava2D;

public class ExporterJPG implements Exporter {

    private int width = 1024;
    private int height = 1024;


    @Override
    public void exportToFile(File file, Workspace workspace) {


	PreviewController controller = Lookup.getDefault().lookup(PreviewController.class);
        controller.getModel(workspace).getProperties().putValue(PreviewProperty.VISIBILITY_RATIO, 1.0);
        controller.refreshPreview(workspace);

        PreviewProperties props = controller.getModel(workspace).getProperties();
        props.putValue("width", width);
        props.putValue("height", height);

        ProcessingTarget target = (ProcessingTarget) controller.getRenderTarget(RenderTarget.PROCESSING_TARGET, workspace);

        try {
            target.refresh();

            PGraphicsJava2D pg2 = (PGraphicsJava2D) target.getGraphics();
            BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            img.setRGB(0, 0, width, height, pg2.pixels, 0, width);
            ImageIO.write(img, "jpg", file);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }


    }
}
