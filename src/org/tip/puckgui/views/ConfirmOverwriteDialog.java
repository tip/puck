package org.tip.puckgui.views;

import java.awt.Component;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author cpm
 */
public class ConfirmOverwriteDialog {

	private static final Logger logger = LoggerFactory.getLogger(ConfirmOverwriteDialog.class);

	/**
	 * 
	 * @param parent
	 * @return
	 */
	public static boolean showDialog(final Component parent) {
		boolean result;

		//
		String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.title");
		String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.text");
		String buttons[] = { "Cancel", "Overwrite" };
		int response = JOptionPane.showOptionDialog(null, message, title, JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, buttons,
				buttons[0]);

		if (response == 0) {
			//
			logger.debug("Cancel overwrite");
			result = false;

		} else {
			//
			logger.debug("Overwrite");
			result = true;
		}

		//
		return result;
	}
}