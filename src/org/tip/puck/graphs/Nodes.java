package org.tip.puck.graphs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author TIP
 */
public class Nodes<E> implements Iterable<Node<E>> {

	private HashMap<Integer, Node<E>> idToNode;
	private HashMap<E, Node<E>> referentToNode;

	/**
	 *
	 */
	public Nodes() {
		this.idToNode = new HashMap<Integer, Node<E>>();
		this.referentToNode = new HashMap<E, Node<E>>();
	}

	/**
	 *
	 */
	public Nodes(final int initialCapacity) {
		this.idToNode = new HashMap<Integer, Node<E>>(initialCapacity);
		this.referentToNode = new HashMap<E, Node<E>>(initialCapacity);
	}

	/**
	 * 
	 * @param idLabel
	 * @return
	 */
	public Node<E> add(final E referent) {
		Node<E> result;

		//
		result = new Node<E>(size() + 1, referent);
		this.idToNode.put(result.getId(), result);
		this.referentToNode.put(result.getReferent(), result);

		//
		return result;
	}

	/**
	 * 
	 * @param idLabel
	 * @return
	 */
	public Node<E> add(final int id, final E referent) {
		Node<E> result;

		//
		result = new Node<E>(id, referent);
		this.idToNode.put(result.getId(), result);
		this.referentToNode.put(result.getReferent(), result);

		//
		return result;
	}

	/**
	 * 
	 * @param idLabel
	 * @return
	 */
	public void add(final Node<E> node) {
		//
		this.idToNode.put(node.getId(), node);
		this.referentToNode.put(node.getReferent(), node);
	}

	/**
	 * 
	 * @param nodes
	 */
	public void addAll(final Nodes<E> nodes) {
		//
		for (Node<E> node : nodes) {
			this.add(node);
		}
	}

	/**
	 * 
	 */
	public void clear() {
		this.referentToNode.clear();
		this.idToNode.clear();
	}

	/**
	 * 
	 * @param node
	 * @return
	 */
	public boolean contains(final Node<E> node) {
		boolean result;

		if (this.idToNode.get(node.getId()) == null) {
			result = false;
		} else {
			result = true;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param idLabel
	 * @return
	 */
	public Node<E> get(final E referent) {
		Node<E> result;

		result = this.referentToNode.get(referent);

		//
		return result;
	}

	/**
	 * 
	 * @param idLabel
	 * @return
	 */
	public Node<E> get(final int id) {
		Node<E> result;

		result = this.idToNode.get(id);

		//
		return result;
	}

	/**
	 * gets direct neighbors (without ego)
	 * 
	 * @return
	 */
	public Nodes<E> getDirectNeighbors() {
		Nodes<E> result;

		//
		result = new Nodes<E>();

		for (Node<E> node : this) {
			result.addAll(node.getDirectNeighbors());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param tag
	 * @return
	 */
	public Nodes<E> getNodesByTag(final String tag) {
		Nodes<E> result;

		//
		result = new Nodes<E>();

		//
		for (Node<E> node : this) {
			if (StringUtils.equals(node.getTag(), tag)) {
				result.add(node);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param label
	 * @return
	 */
	public Nodes<E> getNodesByLabel(final String label) {
		Nodes<E> result;

		//
		result = new Nodes<E>();

		//
		for (Node<E> node : this) {
			if (StringUtils.equals(node.getLabel(), label)) {
				result.add(node);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Node<E>> iterator() {
		Iterator<Node<E>> result;

		result = this.idToNode.values().iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size() {
		int result;

		result = this.idToNode.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Node<E>> toList() {
		List<Node<E>> result;

		result = new ArrayList<Node<E>>(this.idToNode.values());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Node<E>> toListSortedById() {
		List<Node<E>> result;

		result = toList();
		Collections.sort(result, new NodeComparatorById<E>());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Node<E>> toListSortedByLabel() {
		List<Node<E>> result;

		result = toList();
		Collections.sort(result, new NodeComparatorByLabel<E>());

		//
		return result;
	}
	
	public boolean removeNode(Node<E> node){
		boolean result;
		
		if (node==null){
			result = false;
		} else {
			this.idToNode.remove(node.getId());
			this.referentToNode.remove(node.getReferent());
			result = true;
		}
		
		//
		return result;
	}
	
	public String toString(){
		return this.toList().toString();
	}

}
