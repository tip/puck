package org.tip.puck.kinoath;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.relations.Relation;

/**
 * 
 * @author TIP
 */
public class ComponentWorker {

	private static final Logger logger = LoggerFactory.getLogger(ComponentWorker.class);

	/**
	 * Returns a group of individual who are connected with one in a relation
	 * network (kin included).
	 * 
	 * @param source
	 */
	public static IndividualGroup searchComponent(final Individual source) {
		IndividualGroup result;

		//
		result = new IndividualGroup("Component");
		List<Individual> booked = new ArrayList<Individual>();

		//
		booked.add(source);

		while (!booked.isEmpty()) {
			//
			Individual current = booked.remove(booked.size() - 1);

			if (!result.contains(current)) {
				//
				result.add(current);

				//
				for (Individual parent : current.getParents()) {
					//
					if (!result.contains(parent)) {
						//
						booked.add(parent);
					}
				}

				//
				for (Individual partner : current.getPartners()) {
					//
					if (!result.contains(partner)) {
						//
						booked.add(partner);
					}
				}

				//
				for (Individual child : current.children()) {
					//
					if (!result.contains(child)) {
						//
						booked.add(child);
					}
				}

				//
				for (Relation relation : current.relations()) {
					//
					for (Individual relationship : relation.getIndividuals()) {
						//
						if (!result.contains(relationship)) {
							//
							booked.add(relationship);
						}
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static IndividualGroups searchComponents(final Individuals source) {
		IndividualGroups result;

		//
		result = new IndividualGroups();

		//
		if (source != null) {
			//
			Individuals booked = new Individuals(source.size());

			//
			for (Individual current : source) {
				//
				if (!booked.contains(current)) {
					//
					IndividualGroup component = searchComponent(current);

					//
					for (Individual individual : component) {
						//
						booked.add(individual);
					}

					//
					component.setId(result.size() + 1);
					result.add(component);
				}
			}
		}

		//
		return result;
	}

	/**
	 * Returns a group of individual who are connected with one in a kin
	 * network.
	 * 
	 * @param source
	 */
	public static IndividualGroup searchKinComponent(final Individual source) {
		IndividualGroup result;

		//
		result = new IndividualGroup("Component");
		List<Individual> booked = new ArrayList<Individual>();

		//
		booked.add(source);

		while (!booked.isEmpty()) {
			//
			Individual current = booked.remove(booked.size() - 1);

			if (!result.contains(current)) {
				//
				result.add(current);

				//
				for (Individual parent : current.getParents()) {
					//
					if (!result.contains(parent)) {
						//
						booked.add(parent);
					}
				}

				//
				for (Individual partner : current.getPartners()) {
					//
					if (!result.contains(partner)) {
						//
						booked.add(partner);
					}
				}

				//
				for (Individual child : current.children()) {
					//
					if (!result.contains(child)) {
						//
						booked.add(child);
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static IndividualGroups searchKinComponents(final Individuals source) {
		IndividualGroups result;

		//
		result = new IndividualGroups();

		//
		if (source != null) {
			//
			Individuals booked = new Individuals(source.size());

			//
			for (Individual current : source) {
				//
				if (!booked.contains(current)) {
					//
					IndividualGroup component = searchKinComponent(current);

					//
					for (Individual individual : component) {
						//
						booked.add(individual);
					}

					//
					component.setId(result.size() + 1);
					result.add(component);
				}
			}
		}

		//
		return result;
	}

}
