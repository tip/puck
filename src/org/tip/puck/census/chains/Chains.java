package org.tip.puck.census.chains;

import java.util.List;

import org.tip.puck.util.NumberablesHashMap;

public class Chains extends NumberablesHashMap<Chain> {
	
	/**
	 * 
	 */
	public Chains() {
		super();
	}

	/**
	 * 
	 */
	public Chains(final int capacity) {
		super(capacity);
	}

	/**
	 * 
	 * @param source
	 */
	public Chains(final List<Chain> source) {
		super();
		
		for (Chain chain : source){
			chain.setId(size());
			put(chain);
		}
	}





}
