package org.tip.puckgui.views.geographyEditor;

import java.awt.Component;
import java.io.File;
import java.util.ResourceBundle;

import javax.swing.JFileChooser;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.util.ToolBox;
import org.tip.puckgui.util.GenericFileFilter;
import org.tip.puckgui.views.ConfirmOverwriteDialog;

/**
 * This class is based on the dialog type so it must set at first.
 * 
 * http://stackoverflow.com/questions/596429/adjust-selected-file-to-filefilter-
 * in-a-jfilechooser
 * 
 * @author cpm
 * 
 */
public class GeographyExportFileSelector extends JFileChooser {

	private static final long serialVersionUID = 6340589919009662245L;
	private static final Logger logger = LoggerFactory.getLogger(GeographyExportFileSelector.class);

	/**
	 * 
	 */
	public GeographyExportFileSelector(final File targetFile) {
		super();

		//
		setSelectedFile(targetFile);
		setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.saveAsFileChooser.text"));
		setFileSelectionMode(JFileChooser.FILES_ONLY);
		setAcceptAllFileFilterUsed(false);
		setApproveButtonText("Save");
		setDialogType(CUSTOM_DIALOG);

		//
		GenericFileFilter defaultFileFilter = new GenericFileFilter("Geographic files (*.csv, *.ods, *.txt, *.xls)", "csv", "ods", "txt", "xls");
		addChoosableFileFilter(defaultFileFilter);
		addChoosableFileFilter(new GenericFileFilter("Geographic OpenDocumentFormat Spreadsheet (*.ods)", "ods"));
		addChoosableFileFilter(new GenericFileFilter("Geographic Text (*.txt, *.csv)", "txt", ".csv"));
		addChoosableFileFilter(new GenericFileFilter("Geographic Microsoft Excel (*.xls)", "xls"));
		setFileFilter(defaultFileFilter);
	}

	/**
	 * 
	 */
	@Override
	public void approveSelection() {
		//
		File targetFile = getSelectedFile();

		logger.debug("getCurrentDirectory(): {}", getCurrentDirectory());
		logger.debug("selectedFile={}", targetFile);

		if (StringUtils.isBlank(ToolBox.getExtension(targetFile))) {
			targetFile = new File(targetFile.getAbsolutePath() + ".txt");
		}

		//
		if (!StringUtils.endsWithAny(ToolBox.getExtension(targetFile), "csv", "ods", "txt", "xls")) {
			//
			targetFile = new File(targetFile.getAbsolutePath() + ".geo.csv");
			setSelectedFile(targetFile);
		}

		//
		if (targetFile.exists()) {
			//
			if (ConfirmOverwriteDialog.showDialog(null)) {
				//
				logger.debug("Overwrite");
				super.approveSelection();

			} else {
				//
				logger.debug("Cancel overwrite");
			}
		} else {
			//
			super.approveSelection();
		}
	}

	/**
	 * 
	 */
	@Override
	public void cancelSelection() {
		//
		logger.debug("Cancel selection.");
		super.cancelSelection();
	}

	/**
	 * 
	 */
	@Override
	public void setSelectedFile(final File file) {
		//
		super.setSelectedFile(file);
	}

	/**
	 * 
	 */
	@Override
	public void setVisible(final boolean visible) {
		//
		super.setVisible(visible);

		if (!visible) {
			//
			resetChoosableFileFilters();
		}
	}

	/**
	 * This method is the main one of the selector.
	 * 
	 * @param parent
	 * @return
	 */
	public static File showSelectorDialog(final Component parent, final File targetFile) {
		File result;

		//
		GeographyExportFileSelector selector = new GeographyExportFileSelector(targetFile);

		//
		if (selector.showDialog(parent, null) == JFileChooser.APPROVE_OPTION) {
			//
			logger.debug("getCurrentDirectory(): {}", selector.getCurrentDirectory());
			logger.debug("getSelectedFile() : {}", selector.getSelectedFile());
			result = selector.getSelectedFile();

		} else {
			//
			result = null;
		}

		//
		return result;
	}
}
