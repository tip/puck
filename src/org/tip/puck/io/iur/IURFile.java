package org.tip.puck.io.iur;

import java.io.File;
import java.security.InvalidParameterException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckManager;
import org.tip.puck.net.Net;

/**
 * This class represents a TXT File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class IURFile {

	private static final Logger logger = LoggerFactory.getLogger(IURFile.class);

	/**
	 * This method checks if a stream is a BAR one.
	 * 
	 * @param in
	 *            stream to check.
	 * 
	 * @return true if the stream is BAR one.
	 */
	public static boolean isIUR(final File source) {
		boolean result;

		result = isIUR(source, PuckManager.DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * This method checks if a file is a IUR one.
	 * 
	 * @param file
	 *            file to check.
	 * 
	 * @return true if the file is IUR one.
	 */
	public static boolean isIUR(final File source, final String charsetName) {
		boolean result;

		//
		result = IURDetector.isIUR(source, charsetName);

		//
		return result;
	}

	/**
	 * Loads a IUR file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File source, final String charsetName) throws PuckException {
		Net result;

		//
		if (source == null) {
			//
			throw new InvalidParameterException("Null parameter");
		} else {
			//
			String fileName = source.getName().toLowerCase();

			//
			if (fileName.endsWith(".ods")) {
				//
				result = IURODSFile.load(source);

			} else if (fileName.endsWith(".txt")) {
				//
				result = IURTXTFile.load(source, charsetName);

			} else if (fileName.endsWith(".xls")) {
				//
				result = IURXLSFile.load(source);

			} else {
				//
				throw new InvalidParameterException("Unknown extension.");
			}
		}

		//
		return result;
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File target, final Net source) throws PuckException {
		//
		if (source == null) {
			//
			throw new InvalidParameterException("Null parameter");
		} else {
			//
			String fileName = target.getName().toLowerCase();

			//
			if (fileName.endsWith(".ods")) {
				//
				IURODSFile.save(target, source);

			} else if (fileName.endsWith(".txt")) {
				//
				IURTXTFile.save(target, source);

			} else if (fileName.endsWith(".xls")) {
				//
				IURXLSFile.save(target, source);

			} else {
				//
				throw new InvalidParameterException("Unknown extension.");
			}
		}
	}
}
