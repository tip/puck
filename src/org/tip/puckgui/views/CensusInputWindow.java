package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.census.workers.CensusCriteria;
import org.tip.puck.census.workers.CensusReporter;
import org.tip.puck.census.workers.CensusUtils;
import org.tip.puck.census.workers.ChainValuator;
import org.tip.puck.census.workers.ChainValuator.ChainProperty;
import org.tip.puck.census.workers.CircuitFinder;
import org.tip.puck.census.workers.CircuitType;
import org.tip.puck.census.workers.RestrictionType;
import org.tip.puck.census.workers.SiblingMode;
import org.tip.puck.census.workers.SymmetryType;
import org.tip.puck.graphs.onemode.OMGraph.GraphMode;
import org.tip.puck.graphs.onemode.ShuffleCriteria;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.workers.AttributeDescriptor;
import org.tip.puck.net.workers.AttributeDescriptors;
import org.tip.puck.net.workers.AttributeWorker;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.report.Report;
import org.tip.puck.util.PuckUtils;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.PuckGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class CensusInputWindow extends JFrame {

	private static final long serialVersionUID = -8391646456003342197L;
	private static final Logger logger = LoggerFactory.getLogger(CensusInputWindow.class);

	private JFrame thisJFrame;
	private NetGUI netGUI;
	private JPanel contentPane;
	private final ButtonGroup buttonGroupSymmetry = new ButtonGroup();
	private final ButtonGroup buttonGroupFiliation = new ButtonGroup();
	private final ButtonGroup buttonGroupSib = new ButtonGroup();
	private final ButtonGroup buttonGroupCircuit = new ButtonGroup();
	private JTextField txtfldPattern;
	private JTextField txtfldFilter;
	private JCheckBox chckbxCrossSex;
	private JCheckBox chckbxMarriedOnly;
	private final ButtonGroup buttonGroupRestriction = new ButtonGroup();
	private JRadioButton rdbtnCircuitTypeCircuit;
	private JRadioButton rdbtnCircuitTypeRing;
	private JRadioButton rdbtnCircuitTypeMinor;
	private JRadioButton rdbtnCircuitTypeMinimal;
	private JRadioButton rdbtnFiliationAgnatic;
	private JRadioButton rdbtnFiliationUterine;
	private JRadioButton rdbtnFiliationCognatic;
	private JRadioButton rdbtnFiliationBilateral;
	private JRadioButton rdbtnRestrictionNone;
	private JRadioButton rdbtnRestrictionSome;
	private JRadioButton rdbtnRestrictionAll;
	private JRadioButton rdbtnRestrictionLastMarried;
	private JRadioButton rdbtnSibNone;
	private JRadioButton rdbtnSibFull;
	private JRadioButton rdbtnSibAll;
	private JRadioButton rdbtnSymmetryInvariable;
	private JRadioButton rdbtnSymmetryInvertible;
	private JRadioButton rdbtnSymmetryPermutable;
	private JLabel lblClosingRelationType;
	private JLabel lblAscendingRelationType;
	private JButton btnRestoreDefaults;
	private JComboBox cmbbxClosingRelationType;
	private JComboBox cmbbxAscendingRelationType;
	private JLabel lblChainClassification;
	private JComboBox cmbbxClassification;
	private JComboBox cmbbxClassificatoryLinking;
	private JPanel panel;
	private JLabel lblLabel;
	private JLabel lblReport;
	private JLabel lblDiagram;
	private JComboBox cmbbxLabel1;
	private JCheckBox chckbxReport1;
	private JCheckBox chckbxDiagram1;
	private JComboBox cmbbxLabel2;
	private JComboBox cmbbxLabel3;
	private JComboBox cmbbxLabel4;
	private JComboBox cmbbxLabel5;
	private JCheckBox chckbxReport2;
	private JCheckBox chckbxDiagram2;
	private JCheckBox chckbxReport3;
	private JCheckBox chckbxReport4;
	private JCheckBox chckbxReport5;
	private JCheckBox chckbxDiagram3;
	private JCheckBox chckbxDiagram4;
	private JCheckBox chckbxDiagram5;
	private JComboBox cmbbxLabel6;
	private JComboBox cmbbxLabel7;
	private JComboBox cmbbxLabel8;
	private JComboBox cmbbxLabel9;
	private JComboBox cmbbxLabel10;
	private JCheckBox chckbxReport6;
	private JCheckBox chckbxReport7;
	private JCheckBox chckbxReport8;
	private JCheckBox chckbxReport9;
	private JCheckBox chckbxReport10;
	private JCheckBox chckbxDiagram6;
	private JCheckBox chckbxDiagram7;
	private JCheckBox chckbxDiagram8;
	private JCheckBox chckbxDiagram9;
	private JCheckBox chckbxDiagram10;
	private static String[] endogenousLabels;
	private JCheckBox chckbxMarkIndividuals;
	private JPanel panel_2;
	private JCheckBox chckbxCircuitIntersection;
	private JCheckBox chckbxCircuitInduced;
	private JCheckBox chckbxCircuitInducedFrame;
	private JCheckBox chckbxCircuitNetworks;
	private JCheckBox chckbxOpenChainFrequencies;
	private JPanel panel_3;
	private JLabel lblEgoRole;
	private JLabel lblAlterRole;
	private JPanel panel_4;
	private JLabel lblAscendingEgoRole;
	private JLabel lblAscendingAlterRole;
	private JComboBox cmbbxClosingRelationEgoRole;
	private JComboBox cmbbxClosingRelationAlterRole;
	private JComboBox cmbbxAscendingRelationEgoRole;
	private JComboBox cmbbxAscendingRelationAlterRole;
	private JButton btnLaunchWithReshuffling;
	private JCheckBox chckbxCircuitsAsRelations;
	private JCheckBox chckbxListOutofcircuitPairs;
	private JCheckBox chckbxListAllPerspectives;
	private JScrollPane scrollPane;
	private JPanel pajekPartitionLabelsPanel;
	private Component horizontalStrut;
	private JPanel panel_5;
	private JScrollPane scrollPane_1;
	private JComboBox cmbbxDifferentialCensusPartition;
	private JLabel lblDifferentialCensusPartition;

	/**
	 * Create the frame.
	 */
	public CensusInputWindow(final NetGUI netGUI) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(FooReporterInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		// Compute the endogenous labels list.
		if (endogenousLabels == null) {
			endogenousLabels = ChainValuator.getEndogenousLabels();
		}

		this.thisJFrame = this;
		this.netGUI = netGUI;
		setTitle("Census Reporter Inputs");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 980, 600);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panelInputs = new JPanel();
		this.contentPane.add(panelInputs, BorderLayout.CENTER);
		panelInputs.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));

		JLabel lblPattern = new JLabel("Pattern:");
		panelInputs.add(lblPattern, "2, 2, right, default");

		this.txtfldPattern = new JTextField();
		this.txtfldPattern.setColumns(10);
		panelInputs.add(this.txtfldPattern, "4, 2, 3, 1, fill, default");

		this.panel_5 = new JPanel();
		this.panel_5.setBorder(new TitledBorder(null, "Details and Diagrams", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInputs.add(this.panel_5, "10, 2, 1, 7, fill, fill");
		this.panel_5.setLayout(new BorderLayout(0, 0));

		this.scrollPane_1 = new JScrollPane();
		this.scrollPane_1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		this.scrollPane_1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		this.panel_5.add(this.scrollPane_1);

		this.panel = new JPanel();
		this.scrollPane_1.setViewportView(this.panel);
		this.panel.setBorder(null);
		this.panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, }));

		this.lblLabel = new JLabel("Label");
		this.panel.add(this.lblLabel, "2, 2, left, default");

		this.lblReport = new JLabel("Report");
		this.panel.add(this.lblReport, "4, 2, left, default");

		this.lblDiagram = new JLabel("Diagram");
		this.panel.add(this.lblDiagram, "6, 2, left, default");

		this.cmbbxLabel1 = new JComboBox(endogenousLabels);
		this.panel.add(this.cmbbxLabel1, "2, 4, fill, default");

		this.chckbxReport1 = new JCheckBox("");
		this.panel.add(this.chckbxReport1, "4, 4, center, default");

		this.chckbxDiagram1 = new JCheckBox("");
		this.panel.add(this.chckbxDiagram1, "6, 4, center, default");

		this.cmbbxLabel6 = new JComboBox(endogenousLabels);
		this.panel.add(this.cmbbxLabel6, "2, 14, fill, default");

		this.chckbxReport6 = new JCheckBox("");
		this.panel.add(this.chckbxReport6, "4, 14, center, default");

		this.chckbxDiagram6 = new JCheckBox("");
		this.panel.add(this.chckbxDiagram6, "6, 14, center, default");

		this.cmbbxLabel2 = new JComboBox(endogenousLabels);
		this.panel.add(this.cmbbxLabel2, "2, 6, fill, default");

		this.chckbxReport2 = new JCheckBox("");
		this.panel.add(this.chckbxReport2, "4, 6, center, default");

		this.chckbxDiagram2 = new JCheckBox("");
		this.panel.add(this.chckbxDiagram2, "6, 6, center, default");

		this.cmbbxLabel7 = new JComboBox(endogenousLabels);
		this.panel.add(this.cmbbxLabel7, "2, 16, fill, default");

		this.chckbxReport7 = new JCheckBox("");
		this.panel.add(this.chckbxReport7, "4, 16, center, default");

		this.chckbxDiagram7 = new JCheckBox("");
		this.panel.add(this.chckbxDiagram7, "6, 16, center, default");

		this.cmbbxLabel3 = new JComboBox(endogenousLabels);
		this.panel.add(this.cmbbxLabel3, "2, 8, fill, default");

		this.chckbxReport3 = new JCheckBox("");
		this.panel.add(this.chckbxReport3, "4, 8, center, default");

		this.chckbxDiagram3 = new JCheckBox("");
		this.panel.add(this.chckbxDiagram3, "6, 8, center, default");

		this.cmbbxLabel8 = new JComboBox(endogenousLabels);
		this.panel.add(this.cmbbxLabel8, "2, 18, fill, default");

		this.chckbxReport8 = new JCheckBox("");
		this.panel.add(this.chckbxReport8, "4, 18, center, default");

		this.chckbxDiagram8 = new JCheckBox("");
		this.panel.add(this.chckbxDiagram8, "6, 18, center, default");

		this.cmbbxLabel4 = new JComboBox(endogenousLabels);
		this.panel.add(this.cmbbxLabel4, "2, 10, fill, default");

		this.chckbxReport4 = new JCheckBox("");
		this.panel.add(this.chckbxReport4, "4, 10, center, default");

		this.chckbxDiagram4 = new JCheckBox("");
		this.panel.add(this.chckbxDiagram4, "6, 10, center, default");

		this.cmbbxLabel9 = new JComboBox(endogenousLabels);
		this.panel.add(this.cmbbxLabel9, "2, 20, fill, default");

		this.chckbxReport9 = new JCheckBox("");
		this.panel.add(this.chckbxReport9, "4, 20, center, default");

		this.chckbxDiagram9 = new JCheckBox("");
		this.panel.add(this.chckbxDiagram9, "6, 20, center, default");

		this.cmbbxLabel5 = new JComboBox(endogenousLabels);
		this.panel.add(this.cmbbxLabel5, "2, 12, fill, default");

		this.chckbxReport5 = new JCheckBox("");
		this.panel.add(this.chckbxReport5, "4, 12, center, default");

		this.chckbxDiagram5 = new JCheckBox("");
		this.panel.add(this.chckbxDiagram5, "6, 12, center, default");

		this.cmbbxLabel10 = new JComboBox(endogenousLabels);
		this.panel.add(this.cmbbxLabel10, "2, 20, fill, default");

		this.chckbxReport10 = new JCheckBox("");
		this.panel.add(this.chckbxReport10, "4, 20, center, default");

		this.chckbxDiagram10 = new JCheckBox("");
		this.panel.add(this.chckbxDiagram10, "6, 20, center, default");

		JLabel lblex = new JLabel("(ex. \"3 0 1\" ou \"HF(F)F\")");
		lblex.setHorizontalAlignment(SwingConstants.TRAILING);
		panelInputs.add(lblex, "4, 4, left, default");

		this.panel_3 = new JPanel();
		this.panel_3.setBorder(new TitledBorder(null, "Closing Relation", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInputs.add(this.panel_3, "6, 6, right, fill");
		this.panel_3.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		this.panel_4 = new JPanel();
		this.panel_4.setBorder(new TitledBorder(null, "Ascending Relation", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInputs.add(this.panel_4, "2, 6, 3, 1, left, fill");
		this.panel_4.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		this.lblClosingRelationType = new JLabel("Relation Type:");
		this.panel_3.add(this.lblClosingRelationType, "2, 1");

		this.lblAscendingRelationType = new JLabel("Relation Type:");
		this.panel_4.add(this.lblAscendingRelationType, "2, 1");

		this.cmbbxClosingRelationType = new JComboBox();
		this.cmbbxClosingRelationType.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Select Closing Relation Type.
				String selectedType = (String) CensusInputWindow.this.cmbbxClosingRelationType.getSelectedItem();

				if ((PuckUtils.equalsAny(selectedType, "SPOUSE", "PARTN", "OPEN", "LINEAR", "COSPOUSE","TOTAL")) || (selectedType.startsWith("@"))) {
					//
					CensusInputWindow.this.cmbbxClosingRelationEgoRole.setEnabled(false);
					CensusInputWindow.this.cmbbxClosingRelationAlterRole.setEnabled(false);

				} else {
					//
					CensusInputWindow.this.cmbbxClosingRelationEgoRole.setEnabled(true);
					CensusInputWindow.this.cmbbxClosingRelationAlterRole.setEnabled(true);

					//
					List<String> roleNames = new ArrayList<String>();
					roleNames.add("");
					roleNames.add("ALL");
					roleNames.addAll(netGUI.getNet().relationModels().getByName(selectedType).roles().toNameList());
					Object[] roleNamesData = roleNames.toArray();
					CensusInputWindow.this.cmbbxClosingRelationEgoRole.setModel(new DefaultComboBoxModel(roleNamesData));
					CensusInputWindow.this.cmbbxClosingRelationAlterRole.setModel(new DefaultComboBoxModel(roleNamesData));
				}
			}
		});
		this.panel_3.add(this.cmbbxClosingRelationType, "4, 1");
		this.cmbbxClosingRelationType.setModel(new DefaultComboBoxModel(new String[] { "SPOUSE", "PARTN", "OPEN", "LINEAR", "COSPOUSE", "TOTAL" }));

		this.cmbbxAscendingRelationType = new JComboBox();
		this.cmbbxAscendingRelationType.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Select Ascending Relation Type.
				String selectedType = (String) CensusInputWindow.this.cmbbxAscendingRelationType.getSelectedItem();
				if ((StringUtils.equals(selectedType, "PARENT"))) {
					//
					CensusInputWindow.this.cmbbxAscendingRelationEgoRole.setEnabled(false);
					CensusInputWindow.this.cmbbxAscendingRelationAlterRole.setEnabled(false);
				} else {
					//
					CensusInputWindow.this.cmbbxAscendingRelationEgoRole.setEnabled(true);
					CensusInputWindow.this.cmbbxAscendingRelationAlterRole.setEnabled(true);

					//
					List<String> roleNames = new ArrayList<String>();
					roleNames.add("");
					roleNames.add("ALL");
					roleNames.addAll(netGUI.getNet().relationModels().getByName(selectedType).roles().toNameList());
					Object[] roleNamesData = roleNames.toArray();
					CensusInputWindow.this.cmbbxAscendingRelationEgoRole.setModel(new DefaultComboBoxModel(roleNamesData));
					CensusInputWindow.this.cmbbxAscendingRelationAlterRole.setModel(new DefaultComboBoxModel(roleNamesData));
				}
			}
		});
		this.panel_4.add(this.cmbbxAscendingRelationType, "4, 1");
		this.cmbbxAscendingRelationType.setModel(new DefaultComboBoxModel(new String[] { "PARENT" }));

		this.lblEgoRole = new JLabel("Ego Role:");
		this.panel_3.add(this.lblEgoRole, "2, 3, right, default");

		this.cmbbxClosingRelationEgoRole = new JComboBox();
		this.panel_3.add(this.cmbbxClosingRelationEgoRole, "4, 3, fill, default");

		this.lblAlterRole = new JLabel("Alter Role:");
		this.panel_3.add(this.lblAlterRole, "2, 5, right, default");

		this.cmbbxClosingRelationAlterRole = new JComboBox();
		this.panel_3.add(this.cmbbxClosingRelationAlterRole, "4, 5, fill, default");

		this.lblAscendingEgoRole = new JLabel("Ego Role:");
		this.panel_4.add(this.lblAscendingEgoRole, "2, 3, right, default");

		this.cmbbxAscendingRelationEgoRole = new JComboBox();
		this.panel_4.add(this.cmbbxAscendingRelationEgoRole, "4, 3, fill, default");

		this.lblAscendingAlterRole = new JLabel("Alter Role:");
		this.panel_4.add(this.lblAscendingAlterRole, "2, 5, right, default");

		this.cmbbxAscendingRelationAlterRole = new JComboBox();
		this.panel_4.add(this.cmbbxAscendingRelationAlterRole, "4, 5, fill, default");

		JLabel lblFilter = new JLabel("Filter:");
		panelInputs.add(lblFilter, "2, 8, 3, 1, right, default");

		this.txtfldFilter = new JTextField();
		this.txtfldFilter.setColumns(10);
		panelInputs.add(this.txtfldFilter, "6, 8, fill, default");
		
		lblDifferentialCensusPartition = new JLabel("Differential census partition:");
		panelInputs.add(lblDifferentialCensusPartition, "4, 10, right, default");
		
		this.cmbbxDifferentialCensusPartition = new JComboBox();
		this.cmbbxDifferentialCensusPartition.setModel(new DefaultComboBoxModel(new String[] { "" }));
		panelInputs.add(this.cmbbxDifferentialCensusPartition, "6, 10, fill, default");

		JLabel lblClassificatoryLinking = new JLabel("Classificatory linking:");
		panelInputs.add(lblClassificatoryLinking, "2, 12, 3, 1, right, default");

		this.cmbbxClassificatoryLinking = new JComboBox();
		this.cmbbxClassificatoryLinking.setModel(new DefaultComboBoxModel(new String[] { "" }));
		panelInputs.add(this.cmbbxClassificatoryLinking, "6, 12, fill, default");

		this.lblChainClassification = new JLabel("Chain classification:");
		panelInputs.add(this.lblChainClassification, "2, 14, 3, 1, right, default");

		this.cmbbxClassification = new JComboBox();
		this.cmbbxClassification.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				if ((StringUtils.equals((String) CensusInputWindow.this.cmbbxClassification.getSelectedItem(), "SIMPLE"))
						|| (StringUtils.equals((String) CensusInputWindow.this.cmbbxClassification.getSelectedItem(), "CLASSIC"))
						|| (StringUtils.equals((String) CensusInputWindow.this.cmbbxClassification.getSelectedItem(), "CLASSIC_GENDERED"))
						|| (StringUtils.equals((String) CensusInputWindow.this.cmbbxClassification.getSelectedItem(), "POSITIONAL"))) {
					CensusInputWindow.this.cmbbxLabel1.setEnabled(true);
					CensusInputWindow.this.cmbbxLabel2.setEnabled(true);
					CensusInputWindow.this.cmbbxLabel3.setEnabled(true);
					CensusInputWindow.this.cmbbxLabel4.setEnabled(true);
					CensusInputWindow.this.cmbbxLabel5.setEnabled(true);
					CensusInputWindow.this.cmbbxLabel6.setEnabled(true);
					CensusInputWindow.this.cmbbxLabel7.setEnabled(true);
					CensusInputWindow.this.cmbbxLabel8.setEnabled(true);
					CensusInputWindow.this.cmbbxLabel9.setEnabled(true);
					CensusInputWindow.this.cmbbxLabel10.setEnabled(true);

					CensusInputWindow.this.chckbxReport1.setEnabled(true);
					CensusInputWindow.this.chckbxReport2.setEnabled(true);
					CensusInputWindow.this.chckbxReport3.setEnabled(true);
					CensusInputWindow.this.chckbxReport4.setEnabled(true);
					CensusInputWindow.this.chckbxReport5.setEnabled(true);
					CensusInputWindow.this.chckbxReport6.setEnabled(true);
					CensusInputWindow.this.chckbxReport7.setEnabled(true);
					CensusInputWindow.this.chckbxReport8.setEnabled(true);
					CensusInputWindow.this.chckbxReport9.setEnabled(true);
					CensusInputWindow.this.chckbxReport10.setEnabled(true);

					CensusInputWindow.this.chckbxDiagram1.setEnabled(true);
					CensusInputWindow.this.chckbxDiagram2.setEnabled(true);
					CensusInputWindow.this.chckbxDiagram3.setEnabled(true);
					CensusInputWindow.this.chckbxDiagram4.setEnabled(true);
					CensusInputWindow.this.chckbxDiagram5.setEnabled(true);
					CensusInputWindow.this.chckbxDiagram6.setEnabled(true);
					CensusInputWindow.this.chckbxDiagram7.setEnabled(true);
					CensusInputWindow.this.chckbxDiagram8.setEnabled(true);
					CensusInputWindow.this.chckbxDiagram9.setEnabled(true);
					CensusInputWindow.this.chckbxDiagram10.setEnabled(true);
				} else {
					CensusInputWindow.this.cmbbxLabel1.setEnabled(false);
					CensusInputWindow.this.cmbbxLabel2.setEnabled(false);
					CensusInputWindow.this.cmbbxLabel3.setEnabled(false);
					CensusInputWindow.this.cmbbxLabel4.setEnabled(false);
					CensusInputWindow.this.cmbbxLabel5.setEnabled(false);
					CensusInputWindow.this.cmbbxLabel6.setEnabled(false);
					CensusInputWindow.this.cmbbxLabel7.setEnabled(false);
					CensusInputWindow.this.cmbbxLabel8.setEnabled(false);
					CensusInputWindow.this.cmbbxLabel9.setEnabled(false);
					CensusInputWindow.this.cmbbxLabel10.setEnabled(false);

					CensusInputWindow.this.chckbxReport1.setEnabled(false);
					CensusInputWindow.this.chckbxReport2.setEnabled(false);
					CensusInputWindow.this.chckbxReport3.setEnabled(false);
					CensusInputWindow.this.chckbxReport4.setEnabled(false);
					CensusInputWindow.this.chckbxReport5.setEnabled(false);
					CensusInputWindow.this.chckbxReport6.setEnabled(false);
					CensusInputWindow.this.chckbxReport7.setEnabled(false);
					CensusInputWindow.this.chckbxReport8.setEnabled(false);
					CensusInputWindow.this.chckbxReport9.setEnabled(false);
					CensusInputWindow.this.chckbxReport10.setEnabled(false);

					CensusInputWindow.this.chckbxDiagram1.setEnabled(false);
					CensusInputWindow.this.chckbxDiagram2.setEnabled(false);
					CensusInputWindow.this.chckbxDiagram3.setEnabled(false);
					CensusInputWindow.this.chckbxDiagram4.setEnabled(false);
					CensusInputWindow.this.chckbxDiagram5.setEnabled(false);
					CensusInputWindow.this.chckbxDiagram6.setEnabled(false);
					CensusInputWindow.this.chckbxDiagram7.setEnabled(false);
					CensusInputWindow.this.chckbxDiagram8.setEnabled(false);
					CensusInputWindow.this.chckbxDiagram9.setEnabled(false);
					CensusInputWindow.this.chckbxDiagram10.setEnabled(false);
				}
			}
		});
		this.cmbbxClassification.setModel(new DefaultComboBoxModel(ChainProperty.values()));
		panelInputs.add(this.cmbbxClassification, "6, 14, fill, default");

		this.chckbxMarriedOnly = new JCheckBox("Couples only");
		panelInputs.add(this.chckbxMarriedOnly, "2, 16, 3, 1");

		this.chckbxCrossSex = new JCheckBox("Cross-sex chains only");
		this.chckbxCrossSex.setSelected(true);
		panelInputs.add(this.chckbxCrossSex, "6, 16, 3, 1");

		this.chckbxMarkIndividuals = new JCheckBox("Mark Individuals");
		panelInputs.add(this.chckbxMarkIndividuals, "2, 18, 3, 1, default, top");

		this.panel_2 = new JPanel();
		this.panel_2.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Export networks in Pajek format:", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(51, 51, 51)));
		panelInputs.add(this.panel_2, "10, 12, 1, 11, fill, fill");
		this.panel_2.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, }));

		this.scrollPane = new JScrollPane();
		this.scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		this.scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		this.panel_2.add(this.scrollPane, "6, 2, 1, 9, fill, fill");

		this.pajekPartitionLabelsPanel = new JPanel();
		this.scrollPane.setViewportView(this.pajekPartitionLabelsPanel);
		this.pajekPartitionLabelsPanel.setLayout(new BoxLayout(this.pajekPartitionLabelsPanel, BoxLayout.Y_AXIS));

		this.chckbxCircuitIntersection = new JCheckBox("Circuit intersection network");
		this.panel_2.add(this.chckbxCircuitIntersection, "2, 4");

		this.chckbxCircuitNetworks = new JCheckBox("Circuit as networks");
		this.panel_2.add(this.chckbxCircuitNetworks, "2, 2");

		this.chckbxCircuitInduced = new JCheckBox("Circuit induced network");
		this.panel_2.add(this.chckbxCircuitInduced, "2, 6");

		this.chckbxCircuitInducedFrame = new JCheckBox("Circuit induced frame network");
		this.panel_2.add(this.chckbxCircuitInducedFrame, "2, 8");

		this.horizontalStrut = Box.createHorizontalStrut(20);
		this.panel_2.add(this.horizontalStrut, "4, 10");

		this.chckbxOpenChainFrequencies = new JCheckBox("Open Chains Frequencies");
		panelInputs.add(this.chckbxOpenChainFrequencies, "6, 18, 3, 1");

		this.chckbxCircuitsAsRelations = new JCheckBox("Circuits as Relations");
		panelInputs.add(this.chckbxCircuitsAsRelations, "2, 20, 3, 1");

		this.chckbxListOutofcircuitPairs = new JCheckBox("List out-of-circuit pairs");
		panelInputs.add(this.chckbxListOutofcircuitPairs, "6, 20, 3, 1");

		this.chckbxListAllPerspectives = new JCheckBox("List all perspectives");
		panelInputs.add(this.chckbxListAllPerspectives, "6, 22, 3, 1");

		JPanel panel_1 = new JPanel();
		panelInputs.add(panel_1, "2, 24, 9, 1, fill, fill");
		panel_1.setLayout(new GridLayout(0, 5, 0, 0));

		JPanel panelCircuitType = new JPanel();
		panel_1.add(panelCircuitType);
		panelCircuitType.setBorder(new TitledBorder(null, "Circuit Type", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
		panelCircuitType.setLayout(new BoxLayout(panelCircuitType, BoxLayout.Y_AXIS));

		this.rdbtnCircuitTypeCircuit = new JRadioButton("Circuit");
		panelCircuitType.add(this.rdbtnCircuitTypeCircuit);
		this.rdbtnCircuitTypeCircuit.setSelected(true);
		this.buttonGroupCircuit.add(this.rdbtnCircuitTypeCircuit);

		this.rdbtnCircuitTypeRing = new JRadioButton("Ring");
		panelCircuitType.add(this.rdbtnCircuitTypeRing);
		this.buttonGroupCircuit.add(this.rdbtnCircuitTypeRing);

		this.rdbtnCircuitTypeMinor = new JRadioButton("Minor");
		panelCircuitType.add(this.rdbtnCircuitTypeMinor);
		this.buttonGroupCircuit.add(this.rdbtnCircuitTypeMinor);

		this.rdbtnCircuitTypeMinimal = new JRadioButton("Minimal");
		panelCircuitType.add(this.rdbtnCircuitTypeMinimal);
		this.buttonGroupCircuit.add(this.rdbtnCircuitTypeMinimal);

		JPanel panelFiliationType = new JPanel();
		panel_1.add(panelFiliationType);
		panelFiliationType.setBorder(new TitledBorder(null, "Filiation Type", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelFiliationType.setLayout(new BoxLayout(panelFiliationType, BoxLayout.Y_AXIS));

		this.rdbtnFiliationAgnatic = new JRadioButton("Agnatic");
		this.buttonGroupFiliation.add(this.rdbtnFiliationAgnatic);
		panelFiliationType.add(this.rdbtnFiliationAgnatic);

		this.rdbtnFiliationUterine = new JRadioButton("Uterine");
		this.buttonGroupFiliation.add(this.rdbtnFiliationUterine);
		panelFiliationType.add(this.rdbtnFiliationUterine);

		this.rdbtnFiliationCognatic = new JRadioButton("Cognatic");
		this.buttonGroupFiliation.add(this.rdbtnFiliationCognatic);
		this.rdbtnFiliationCognatic.setSelected(true);
		panelFiliationType.add(this.rdbtnFiliationCognatic);

		this.rdbtnFiliationBilateral = new JRadioButton("Bilateral");
		this.buttonGroupFiliation.add(this.rdbtnFiliationBilateral);
		panelFiliationType.add(this.rdbtnFiliationBilateral);

		JPanel panelRestrictionType = new JPanel();
		panel_1.add(panelRestrictionType);
		panelRestrictionType.setBorder(new TitledBorder(null, "Restriction Type", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelRestrictionType.setLayout(new BoxLayout(panelRestrictionType, BoxLayout.Y_AXIS));

		this.rdbtnRestrictionNone = new JRadioButton("None");
		this.buttonGroupRestriction.add(this.rdbtnRestrictionNone);
		this.rdbtnRestrictionNone.setSelected(true);
		panelRestrictionType.add(this.rdbtnRestrictionNone);

		this.rdbtnRestrictionSome = new JRadioButton("Some");
		this.buttonGroupRestriction.add(this.rdbtnRestrictionSome);
		panelRestrictionType.add(this.rdbtnRestrictionSome);

		this.rdbtnRestrictionAll = new JRadioButton("All");
		this.buttonGroupRestriction.add(this.rdbtnRestrictionAll);
		panelRestrictionType.add(this.rdbtnRestrictionAll);

		this.rdbtnRestrictionLastMarried = new JRadioButton("Last Married");
		this.buttonGroupRestriction.add(this.rdbtnRestrictionLastMarried);
		panelRestrictionType.add(this.rdbtnRestrictionLastMarried);

		JPanel panelSiblingMode = new JPanel();
		panel_1.add(panelSiblingMode);
		panelSiblingMode.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Sibling Types", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelSiblingMode.setLayout(new BoxLayout(panelSiblingMode, BoxLayout.Y_AXIS));

		this.rdbtnSibNone = new JRadioButton("2 (None)");
		this.rdbtnSibNone.setToolTipText("No siblings assimilated.");
		panelSiblingMode.add(this.rdbtnSibNone);
		this.buttonGroupSib.add(this.rdbtnSibNone);

		this.rdbtnSibFull = new JRadioButton("3 (Full)");
		this.rdbtnSibFull.setToolTipText("Full siblings assimilated.");
		panelSiblingMode.add(this.rdbtnSibFull);
		this.rdbtnSibFull.setSelected(true);
		this.buttonGroupSib.add(this.rdbtnSibFull);

		this.rdbtnSibAll = new JRadioButton("1 (All)");
		this.rdbtnSibAll.setToolTipText("All siblings assimilated.");
		panelSiblingMode.add(this.rdbtnSibAll);
		this.buttonGroupSib.add(this.rdbtnSibAll);

		JPanel panelSymmetryType = new JPanel();
		panel_1.add(panelSymmetryType);
		panelSymmetryType.setBorder(new TitledBorder(null, "Symmetry Type", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelSymmetryType.setLayout(new BoxLayout(panelSymmetryType, BoxLayout.Y_AXIS));

		this.rdbtnSymmetryInvariable = new JRadioButton("Invariable");
		this.rdbtnSymmetryInvariable.setToolTipText("Not permutable.");
		panelSymmetryType.add(this.rdbtnSymmetryInvariable);
		this.buttonGroupSymmetry.add(this.rdbtnSymmetryInvariable);

		this.rdbtnSymmetryInvertible = new JRadioButton("Invertible");
		this.rdbtnSymmetryInvertible.setToolTipText("Ego-alter reflection possible.");
		panelSymmetryType.add(this.rdbtnSymmetryInvertible);
		this.buttonGroupSymmetry.add(this.rdbtnSymmetryInvertible);

		this.rdbtnSymmetryPermutable = new JRadioButton("Fully Permutable");
		panelSymmetryType.add(this.rdbtnSymmetryPermutable);
		this.rdbtnSymmetryPermutable.setSelected(true);
		this.buttonGroupSymmetry.add(this.rdbtnSymmetryPermutable);

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});

		this.btnRestoreDefaults = new JButton("Restore Defaults");
		this.btnRestoreDefaults.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Restore Defaults.
				CensusCriteria criteria = new CensusCriteria();
				setCriteria(criteria);
			}
		});
		buttonPanel.add(this.btnRestoreDefaults);
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		getRootPane().setDefaultButton(btnLaunch);
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					//
					CensusCriteria criteria = getCriteria();

					if (StringUtils.isBlank(criteria.getPattern())) {
						//
						String title = "Invalid input";
						String message = "Pattern field can not be empty.";

						//
						JOptionPane.showMessageDialog(CensusInputWindow.this.thisJFrame, message, title, JOptionPane.WARNING_MESSAGE);

					} else if (!StringUtils.isBlank(criteria.getIndividualPartitionLabel())){
						
						//
						PuckGUI.instance().getPreferences().setDifferentialCensusCriteria(criteria);

						//
						Report report = CensusReporter.reportDifferentialCensus(netGUI.getSegmentation(), null, null, criteria);
						// Report report =
						// CensusReporter.reportFindCircuit(netGUI.getNet(),
						// criteria);

						netGUI.addReportTab(report);

						dispose();

						
					} else {
						//
						PuckGUI.instance().getPreferences().setCensusCriteria(criteria);

						if (criteria.getClosingRelationEgoRole().equals("ALL") || criteria.getClosingRelationAlterRole().equals("ALL")) {

							List<CensusCriteria> criteriaList = new ArrayList<CensusCriteria>();

							for (int i = 2; i < CensusInputWindow.this.cmbbxClosingRelationEgoRole.getItemCount(); i++) {
								if (criteria.getClosingRelationEgoRole().equals("ALL")
										|| criteria.getClosingRelationEgoRole().equals(CensusInputWindow.this.cmbbxClosingRelationEgoRole.getItemAt(i))) {
									int k = 2;
									if (criteria.getClosingRelationEgoRole().equals("ALL")) {
										k = i;
									}
									for (int j = k; j < CensusInputWindow.this.cmbbxClosingRelationEgoRole.getItemCount(); j++) {
										if (criteria.getClosingRelationAlterRole().equals("ALL")
												|| criteria.getClosingRelationAlterRole().equals(
														CensusInputWindow.this.cmbbxClosingRelationAlterRole.getItemAt(j))) {
											CensusCriteria partialCriteria = getCriteria();
											partialCriteria.setClosingRelationEgoRole((String) CensusInputWindow.this.cmbbxClosingRelationEgoRole.getItemAt(i));
											partialCriteria.setClosingRelationAlterRole((String) CensusInputWindow.this.cmbbxClosingRelationAlterRole
													.getItemAt(j));
											criteriaList.add(partialCriteria);
										}
									}
								}
							}

							for (Report report : CensusReporter.reportMultipleCensus(netGUI.getSegmentation(), criteriaList)) {
								netGUI.addReportTab(report);
							}

						} else {

							CircuitFinder finder = new CircuitFinder(netGUI.getSegmentation(), criteria);
							Report report = CensusReporter.reportFindCircuit(finder, netGUI.getSegmentation(), criteria, netGUI.getFile());

							if (criteria.isCircuitInducedNetwork()) {
								Net newNet = CensusUtils.createCircuitInducedNet(finder);
								NetGUI newNetGui = PuckGUI.instance().createNetGUI(netGUI.getFile(), newNet);
								newNetGui.setChanged(true, "-circuits-" + finder.getPattern());
							}

							netGUI.addReportTab(report);

							if (criteria.isCircuitsAsRelations()) {

								//
								String name = "Circuits_" + finder.getPattern();
								String label = name;
								int i = 0;
								while (netGUI.getNet().relationModels().getByName(label) != null) {
									i++;
									label = name + " " + i;
								}
								RelationModel model = netGUI.getNet().createRelationModel(label);
								NetUtils.createRelationsFromCircuits(netGUI.getNet(), finder, model);

								//
								netGUI.setChanged(true);

								//
								netGUI.addRelationTab(model);

								// Refresh.
								netGUI.updateAll();

							}

						}

						//
						dispose();
					}

				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(CensusInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);

		this.btnLaunchWithReshuffling = new JButton("Launch with Reshuffling");
		this.btnLaunchWithReshuffling.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Launch.
				try {
					//
					CensusCriteria criteria = getCriteria();

					if (StringUtils.isBlank(criteria.getPattern())) {
						//
						String title = "Invalid input";
						String message = "Pattern field can not be empty.";

						//
						JOptionPane.showMessageDialog(CensusInputWindow.this.thisJFrame, message, title, JOptionPane.WARNING_MESSAGE);

					} else {
						//
						ShuffleCriteria shuffleCriteria = new ShuffleCriteria();

						shuffleCriteria.setSwitchesPerIteration(2);
						shuffleCriteria.setMaxGenerationalDistance(2);
						shuffleCriteria.setMinShufflePercentage(50);
						shuffleCriteria.setMinStableIterations(100);
						shuffleCriteria.setMode(GraphMode.OREGRAPH);
						int runs = 10;

						//
						PuckGUI.instance().getPreferences().setCensusCriteria(criteria);

						Report report2 = CensusReporter.reportFindCircuitsReshuffled(netGUI.getNet(), shuffleCriteria, runs, criteria);
						netGUI.addReportTab(report2);

						dispose();
					}
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(CensusInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(this.btnLaunchWithReshuffling);

		// /////////////////
		setCriteria(PuckGUI.instance().getPreferences().getCensusCriteria());
	}

	/**
	 * 
	 * @return
	 */
	public CensusCriteria getCriteria() {
		CensusCriteria result;

		//
		result = new CensusCriteria();

		//
		result.setPattern(this.txtfldPattern.getText());
		result.setFilter(this.txtfldFilter.getText());
		result.setClassificatoryLinking((String) this.cmbbxClassificatoryLinking.getSelectedItem());
		result.setClosingRelation((String) this.cmbbxClosingRelationType.getSelectedItem());
		result.setClosingRelationEgoRole((String) this.cmbbxClosingRelationEgoRole.getSelectedItem());
		result.setClosingRelationAlterRole((String) this.cmbbxClosingRelationAlterRole.getSelectedItem());
		result.setAscendingRelation((String) this.cmbbxAscendingRelationType.getSelectedItem());
		result.setAscendingRelationEgoRole((String) this.cmbbxAscendingRelationEgoRole.getSelectedItem());
		result.setAscendingRelationAlterRole((String) this.cmbbxAscendingRelationAlterRole.getSelectedItem());
		result.setChainClassification((String) this.cmbbxClassification.getSelectedItem());
		result.setCrossSexChainsOnly(this.chckbxCrossSex.isSelected());
		result.setMarriedOnly(this.chckbxMarriedOnly.isSelected());
		result.setMarkIndividuals(this.chckbxMarkIndividuals.isSelected());
		result.setOpenChainFrequencies(this.chckbxOpenChainFrequencies.isSelected());
		result.setCircuitsAsRelations(this.chckbxCircuitsAsRelations.isSelected());
		result.setWithOutOfCircuitCouples(this.chckbxListOutofcircuitPairs.isSelected());
		result.setWithAllPerspectives(this.chckbxListAllPerspectives.isSelected());

		//
		CircuitType circuitType;
		if (this.rdbtnCircuitTypeCircuit.isSelected()) {
			circuitType = CircuitType.CIRCUIT;
		} else if (this.rdbtnCircuitTypeMinimal.isSelected()) {
			circuitType = CircuitType.MINIMAL;
		} else if (this.rdbtnCircuitTypeMinor.isSelected()) {
			circuitType = CircuitType.MINOR;
		} else if (this.rdbtnCircuitTypeRing.isSelected()) {
			circuitType = CircuitType.RING;
		} else {
			circuitType = null;
		}
		result.setCircuitType(circuitType);

		//
		FiliationType filiationType;
		if (this.rdbtnFiliationAgnatic.isSelected()) {
			filiationType = FiliationType.AGNATIC;
		} else if (this.rdbtnFiliationBilateral.isSelected()) {
			filiationType = FiliationType.BILINEAR;
		} else if (this.rdbtnFiliationCognatic.isSelected()) {
			filiationType = FiliationType.COGNATIC;
		} else if (this.rdbtnFiliationUterine.isSelected()) {
			filiationType = FiliationType.UTERINE;
		} else {
			filiationType = null;
		}
		result.setFiliationType(filiationType);

		//
		RestrictionType restrictionType;
		if (this.rdbtnRestrictionAll.isSelected()) {
			restrictionType = RestrictionType.ALL;
		} else if (this.rdbtnRestrictionLastMarried.isSelected()) {
			restrictionType = RestrictionType.LASTMARRIED;
		} else if (this.rdbtnRestrictionNone.isSelected()) {
			restrictionType = RestrictionType.NONE;
		} else if (this.rdbtnRestrictionSome.isSelected()) {
			restrictionType = RestrictionType.SOME;
		} else {
			restrictionType = null;
		}
		result.setRestrictionType(restrictionType);

		//
		SiblingMode siblingMode;
		if (this.rdbtnSibAll.isSelected()) {
			siblingMode = SiblingMode.ALL;
		} else if (this.rdbtnSibFull.isSelected()) {
			siblingMode = SiblingMode.FULL;
		} else if (this.rdbtnSibNone.isSelected()) {
			siblingMode = SiblingMode.NONE;
		} else {
			siblingMode = null;
		}
		// Temporary adjustement (until sibling modes for multiple parents are
		// defined)
		if (!result.getAscendingRelation().equals("PARENT")) {
			siblingMode = SiblingMode.ALL;
		}
		//
		result.setSiblingMode(siblingMode);

		//
		SymmetryType symmetryType;
		if (this.rdbtnSymmetryInvariable.isSelected()) {
			symmetryType = SymmetryType.INVARIABLE;
		} else if (this.rdbtnSymmetryInvertible.isSelected()) {
			symmetryType = SymmetryType.INVERTIBLE;
		} else if (this.rdbtnSymmetryPermutable.isSelected()) {
			symmetryType = SymmetryType.PERMUTABLE;
		} else {
			symmetryType = null;
		}
		result.setSymmetryType(symmetryType);
		
		result.setIndividualPartitionLabel((String) this.cmbbxDifferentialCensusPartition.getSelectedItem());
/*		if (!StringUtils.isBlank(result.getIndividualPartitionLabel())){
			result.setClosingRelation("TOTAL");
		}*/


		//
		result.getCensusDetails().clear();
		result.getCensusDetails().add((String) this.cmbbxLabel1.getSelectedItem(), this.chckbxReport1.isSelected(), this.chckbxDiagram1.isSelected());
		result.getCensusDetails().add((String) this.cmbbxLabel2.getSelectedItem(), this.chckbxReport2.isSelected(), this.chckbxDiagram2.isSelected());
		result.getCensusDetails().add((String) this.cmbbxLabel3.getSelectedItem(), this.chckbxReport3.isSelected(), this.chckbxDiagram3.isSelected());
		result.getCensusDetails().add((String) this.cmbbxLabel4.getSelectedItem(), this.chckbxReport4.isSelected(), this.chckbxDiagram4.isSelected());
		result.getCensusDetails().add((String) this.cmbbxLabel5.getSelectedItem(), this.chckbxReport5.isSelected(), this.chckbxDiagram5.isSelected());
		result.getCensusDetails().add((String) this.cmbbxLabel6.getSelectedItem(), this.chckbxReport6.isSelected(), this.chckbxDiagram6.isSelected());
		result.getCensusDetails().add((String) this.cmbbxLabel7.getSelectedItem(), this.chckbxReport7.isSelected(), this.chckbxDiagram7.isSelected());
		result.getCensusDetails().add((String) this.cmbbxLabel8.getSelectedItem(), this.chckbxReport8.isSelected(), this.chckbxDiagram8.isSelected());
		result.getCensusDetails().add((String) this.cmbbxLabel9.getSelectedItem(), this.chckbxReport9.isSelected(), this.chckbxDiagram9.isSelected());
		result.getCensusDetails().add((String) this.cmbbxLabel10.getSelectedItem(), this.chckbxReport10.isSelected(), this.chckbxDiagram10.isSelected());

		//
		result.setCircuitInducedNetwork(this.chckbxCircuitInduced.isSelected());
		result.setCircuitInduceFrameNetwork(this.chckbxCircuitInducedFrame.isSelected());
		result.setCircuitIntersectionNetwork(this.chckbxCircuitIntersection.isSelected());
		result.setCircuitNetworks(this.chckbxCircuitNetworks.isSelected());

		//
		result.getPartitionLabels().clear();
		for (Component component : this.pajekPartitionLabelsPanel.getComponents()) {
			//
			if (((JCheckBox) component).isSelected()) {
				//
				result.getPartitionLabels().add(((JCheckBox) component).getText());
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public void setCriteria(final CensusCriteria source) {
		//
		if (source != null) {
			//
			this.txtfldPattern.setText(source.getPattern());
			this.txtfldFilter.setText(source.getFilter());
			
			AttributeDescriptors attributeDescriptors = AttributeWorker.getExogenousAttributeDescriptors(this.netGUI.getNet(),this.netGUI.getSegmentation(), null);
			List<String> individualAttributeLabels = attributeDescriptors.findByScope(AttributeDescriptor.Scope.INDIVIDUALS).labels();
			Collections.sort(individualAttributeLabels);
			individualAttributeLabels.add(0, "");
			
			//
/*			List<String> classificatoryLinkingLabels = new ArrayList<String>(20);
			classificatoryLinkingLabels.add("");
			classificatoryLinkingLabels.addAll(IndividualValuator.getAttributeLabels(this.netGUI.getCurrentIndividuals()));*/

			this.cmbbxClassificatoryLinking.setModel(new DefaultComboBoxModel(individualAttributeLabels.toArray()));

			if (StringUtils.isBlank(source.getClassificatoryLinking())) {
				//
				this.cmbbxClassificatoryLinking.setSelectedIndex(0);

			} else {
				//
				int classificatoryLinkingLabelIndex = individualAttributeLabels.indexOf(source.getClassificatoryLinking());
				if (classificatoryLinkingLabelIndex == -1) {
					this.cmbbxClassificatoryLinking.setSelectedIndex(0);
				} else {
					this.cmbbxClassificatoryLinking.setSelectedIndex(classificatoryLinkingLabelIndex);
				}
			}

			this.cmbbxDifferentialCensusPartition.setModel(new DefaultComboBoxModel(individualAttributeLabels.toArray()));

			if (StringUtils.isBlank(source.getIndividualPartitionLabel())) {
				//
				this.cmbbxDifferentialCensusPartition.setSelectedIndex(0);

			} else {
				//
				int differentialCensusPartitionLabelIndex = individualAttributeLabels.indexOf(source.getIndividualPartitionLabel());
				if (differentialCensusPartitionLabelIndex == -1) {
					this.cmbbxDifferentialCensusPartition.setSelectedIndex(0);
				} else {
					this.cmbbxDifferentialCensusPartition.setSelectedIndex(differentialCensusPartitionLabelIndex);
				}
			}

			//
			List<String> closingRelationLabels = new ArrayList<String>();
			closingRelationLabels.add("SPOUSE");
			closingRelationLabels.add("PARTN");
			closingRelationLabels.add("OPEN");
			closingRelationLabels.add("LINEAR");
			closingRelationLabels.add("COSPOUSE");
			closingRelationLabels.add("TOTAL");
			closingRelationLabels.addAll(this.netGUI.getNet().relationModels().sortedNameList());
			for (String label : IndividualValuator.getExogenousAttributeLabels(this.netGUI.getCurrentIndividuals())) {
				closingRelationLabels.add("@" + label);
			}
			this.cmbbxClosingRelationType.setModel(new DefaultComboBoxModel(closingRelationLabels.toArray()));
			if (StringUtils.isBlank(source.getClosingRelation())) {
				//
				this.cmbbxClosingRelationType.setSelectedIndex(0);

			} else if (closingRelationLabels.contains(source.getClosingRelation())) {
				//
				this.cmbbxClosingRelationType.setSelectedIndex(closingRelationLabels.indexOf(source.getClosingRelation()));
				this.cmbbxClosingRelationEgoRole.setSelectedItem(source.getClosingRelationEgoRole());
				this.cmbbxClosingRelationAlterRole.setSelectedItem(source.getClosingRelationAlterRole());

			} else {
				//
				this.cmbbxClosingRelationType.setSelectedIndex(0);
			}

			//
			List<String> ascendingRelationLabels = new ArrayList<String>();
			ascendingRelationLabels.add("PARENT");
			ascendingRelationLabels.addAll(this.netGUI.getNet().relationModels().sortedNameList());
			this.cmbbxAscendingRelationType.setModel(new DefaultComboBoxModel(ascendingRelationLabels.toArray()));
			if (StringUtils.isBlank(source.getAscendingRelation())) {
				//
				this.cmbbxAscendingRelationType.setSelectedIndex(0);

			} else if (ascendingRelationLabels.contains(source.getAscendingRelation())) {
				//
				this.cmbbxAscendingRelationType.setSelectedIndex(ascendingRelationLabels.indexOf(source.getAscendingRelation()));
				this.cmbbxAscendingRelationEgoRole.setSelectedItem(source.getAscendingRelationEgoRole());
				this.cmbbxAscendingRelationAlterRole.setSelectedItem(source.getAscendingRelationAlterRole());

			} else {
				//
				this.cmbbxAscendingRelationType.setSelectedIndex(0);
			}

			//
			this.cmbbxClassification.setModel(new DefaultComboBoxModel(endogenousLabels));

			int chainClassificationLabelIndex;
			if (StringUtils.isBlank(source.getChainClassification())) {
				chainClassificationLabelIndex = ArrayUtils.indexOf(endogenousLabels, "SIMPLE");
			} else {
				chainClassificationLabelIndex = ArrayUtils.indexOf(endogenousLabels, source.getChainClassification());
				if (chainClassificationLabelIndex == -1) {
					chainClassificationLabelIndex = ArrayUtils.indexOf(endogenousLabels, "SIMPLE");
				}
			}
			this.cmbbxClassification.setSelectedIndex(chainClassificationLabelIndex);

			//
			this.chckbxCrossSex.setSelected(source.isCrossSexChainsOnly());
			this.chckbxMarriedOnly.setSelected(source.isCouplesOnly());
			this.chckbxMarkIndividuals.setSelected(source.isMarkIndividuals());
			this.chckbxOpenChainFrequencies.setSelected(source.isOpenChainFrequencies());
			this.chckbxCircuitsAsRelations.setSelected(source.isCircuitsAsRelations());
			this.chckbxListOutofcircuitPairs.setSelected(source.isWithOutOfCircuitCouples());
			this.chckbxListAllPerspectives.setSelected(source.isWithAllPerspectives());

			//
			this.rdbtnCircuitTypeCircuit.setSelected(false);
			this.rdbtnCircuitTypeMinimal.setSelected(false);
			this.rdbtnCircuitTypeMinor.setSelected(false);
			this.rdbtnCircuitTypeRing.setSelected(false);
			switch (source.getCircuitType()) {
				case CIRCUIT:
					this.rdbtnCircuitTypeCircuit.setSelected(true);
				break;
				case MINIMAL:
					this.rdbtnCircuitTypeMinimal.setSelected(true);
				break;
				case MINOR:
					this.rdbtnCircuitTypeMinor.setSelected(true);
				break;
				case RING:
					this.rdbtnCircuitTypeRing.setSelected(true);
				break;
			}

			//
			this.rdbtnFiliationAgnatic.setSelected(false);
			this.rdbtnFiliationBilateral.setSelected(false);
			this.rdbtnFiliationCognatic.setSelected(false);
			this.rdbtnFiliationUterine.setSelected(false);
			switch (source.getFiliationType()) {
				case AGNATIC:
					this.rdbtnFiliationAgnatic.setSelected(true);
				break;
				case BILINEAR:
					this.rdbtnFiliationBilateral.setSelected(true);
				break;
				case COGNATIC:
					this.rdbtnFiliationCognatic.setSelected(true);
				break;
				case UTERINE:
					this.rdbtnFiliationUterine.setSelected(true);
				break;
			}

			//
			this.rdbtnRestrictionAll.setSelected(false);
			this.rdbtnRestrictionLastMarried.setSelected(false);
			this.rdbtnRestrictionNone.setSelected(false);
			this.rdbtnRestrictionSome.setSelected(false);
			switch (source.getRestrictionType()) {
				case ALL:
					this.rdbtnRestrictionAll.setSelected(true);
				break;
				case LASTMARRIED:
					this.rdbtnRestrictionLastMarried.setSelected(true);
				break;
				case NONE:
					this.rdbtnRestrictionNone.setSelected(true);
				break;
				case SOME:
					this.rdbtnRestrictionSome.setSelected(true);
				break;
			}

			//
			this.rdbtnSibAll.setSelected(false);
			this.rdbtnSibFull.setSelected(false);
			this.rdbtnSibNone.setSelected(false);
			switch (source.getSiblingMode()) {
				case ALL:
					this.rdbtnSibAll.setSelected(true);
				break;
				case FULL:
					this.rdbtnSibFull.setSelected(true);
				break;
				case NONE:
					this.rdbtnSibNone.setSelected(true);
				break;
			}

			//
			this.rdbtnSymmetryInvariable.setSelected(false);
			this.rdbtnSymmetryInvertible.setSelected(false);
			this.rdbtnSymmetryPermutable.setSelected(false);
			switch (source.getSymmetryType()) {
				case INVARIABLE:
					this.rdbtnSymmetryInvariable.setSelected(true);
				break;
				case INVERTIBLE:
					this.rdbtnSymmetryInvertible.setSelected(true);
				break;
				case PERMUTABLE:
					this.rdbtnSymmetryPermutable.setSelected(true);
				break;
			}

			//
			this.cmbbxLabel1.setSelectedIndex(ArrayUtils.indexOf(endogenousLabels, source.getCensusDetails().get(0).getLabel()));
			this.cmbbxLabel2.setSelectedIndex(ArrayUtils.indexOf(endogenousLabels, source.getCensusDetails().get(1).getLabel()));
			this.cmbbxLabel3.setSelectedIndex(ArrayUtils.indexOf(endogenousLabels, source.getCensusDetails().get(2).getLabel()));
			this.cmbbxLabel4.setSelectedIndex(ArrayUtils.indexOf(endogenousLabels, source.getCensusDetails().get(3).getLabel()));
			this.cmbbxLabel5.setSelectedIndex(ArrayUtils.indexOf(endogenousLabels, source.getCensusDetails().get(4).getLabel()));
			this.cmbbxLabel6.setSelectedIndex(ArrayUtils.indexOf(endogenousLabels, source.getCensusDetails().get(5).getLabel()));
			this.cmbbxLabel7.setSelectedIndex(ArrayUtils.indexOf(endogenousLabels, source.getCensusDetails().get(6).getLabel()));
			this.cmbbxLabel8.setSelectedIndex(ArrayUtils.indexOf(endogenousLabels, source.getCensusDetails().get(7).getLabel()));
			this.cmbbxLabel9.setSelectedIndex(ArrayUtils.indexOf(endogenousLabels, source.getCensusDetails().get(8).getLabel()));
			this.cmbbxLabel10.setSelectedIndex(ArrayUtils.indexOf(endogenousLabels, source.getCensusDetails().get(9).getLabel()));

			this.chckbxReport1.setSelected(source.getCensusDetails().get(0).isReport());
			this.chckbxReport2.setSelected(source.getCensusDetails().get(1).isReport());
			this.chckbxReport3.setSelected(source.getCensusDetails().get(2).isReport());
			this.chckbxReport4.setSelected(source.getCensusDetails().get(3).isReport());
			this.chckbxReport5.setSelected(source.getCensusDetails().get(4).isReport());
			this.chckbxReport6.setSelected(source.getCensusDetails().get(5).isReport());
			this.chckbxReport7.setSelected(source.getCensusDetails().get(6).isReport());
			this.chckbxReport8.setSelected(source.getCensusDetails().get(7).isReport());
			this.chckbxReport9.setSelected(source.getCensusDetails().get(8).isReport());
			this.chckbxReport10.setSelected(source.getCensusDetails().get(9).isReport());

			this.chckbxDiagram1.setSelected(source.getCensusDetails().get(0).isDiagram());
			this.chckbxDiagram2.setSelected(source.getCensusDetails().get(1).isDiagram());
			this.chckbxDiagram3.setSelected(source.getCensusDetails().get(2).isDiagram());
			this.chckbxDiagram4.setSelected(source.getCensusDetails().get(3).isDiagram());
			this.chckbxDiagram5.setSelected(source.getCensusDetails().get(4).isDiagram());
			this.chckbxDiagram6.setSelected(source.getCensusDetails().get(5).isDiagram());
			this.chckbxDiagram7.setSelected(source.getCensusDetails().get(6).isDiagram());
			this.chckbxDiagram8.setSelected(source.getCensusDetails().get(7).isDiagram());
			this.chckbxDiagram9.setSelected(source.getCensusDetails().get(8).isDiagram());
			this.chckbxDiagram10.setSelected(source.getCensusDetails().get(9).isDiagram());

			//
			this.chckbxCircuitInduced.setSelected(source.isCircuitInducedNetwork());
			this.chckbxCircuitInducedFrame.setSelected(source.isCircuitInducedFrameNetwork());
			this.chckbxCircuitIntersection.setSelected(source.isCircuitIntersectionNetwork());
			this.chckbxCircuitNetworks.setSelected(source.isCircuitNetworks());

			//
			this.pajekPartitionLabelsPanel.removeAll();
			for (String label : IndividualValuator.getAttributeLabels(this.netGUI.getCurrentIndividuals()).sort()) {
				//
				JCheckBox checkbox = new JCheckBox(label);
				if (source.getPartitionLabels().contains(label)) {
					//
					checkbox.setSelected(true);
				}
				this.pajekPartitionLabelsPanel.add(checkbox);
			}
		}
	}
}
