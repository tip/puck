package org.tip.puck.graphs;

import java.util.ArrayList;
import java.util.List;

import org.tip.puck.PuckException;

/**
 * 
 * @author TIP
 */
public class Link<E> {
	public enum LinkType {
		ARC,
		EDGE;

		public boolean isArc() {
			return (this == ARC);
		}

		public boolean isEdge() {
			return (this == EDGE);
		}
	};

	private LinkType type;
	private Node<E> sourceNode;
	private Node<E> targetNode;
	private double weight;
	private String tag;
	private String colour;
	private E referent;

	/**
	 * 
	 * @param target
	 * @return
	 * @throws PuckException
	 */
	public Link(final Node<E> source, final Node<E> target, final LinkType type) {
		if ((source == null) || (target == null) || (type == null)) {
			throw new NullPointerException("Null parameter detected.");
		} else {
			this.type = type;
			this.sourceNode = source;
			this.targetNode = target;
			this.weight = 0;
		}
	}

	/**
	 * 
	 * @param target
	 * @return
	 * @throws PuckException
	 */
	public Link(final Node<E> source, final Node<E> target, final LinkType type, final double weight) {
		if ((source == null) || (target == null) || (type == null)) {
			throw new NullPointerException("Null parameter detected.");
		} else {
			this.sourceNode = source;
			this.targetNode = target;
			this.weight = weight;
			this.type = type;
		}
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public double addWeight(final double value) {
		double result;

		this.weight += value;

		result = this.weight;

		//
		return result;
	}

	/**
	 * 
	 * @param node
	 * @return
	 * @throws PuckException
	 */
	public Node<E> getOtherNode(final Node<E> node) {
		Node<E> result;

		if (node == null) {
			result = null;
		} else if (node == this.sourceNode) {
			result = this.targetNode;
		} else if (node == this.targetNode) {
			result = this.sourceNode;
		} else {
			result = null;
		}

		//
		return result;
	}

	public Node<E> getSourceNode() {
		return sourceNode;
	}

	public String getTag() {
		return tag;
	}
	
	public List<String> getTags(){
		List<String> result;
		
		result = new ArrayList<String>();
		
		if (tag!=null){
			for (String tagPart : tag.split("\\;")){
				result.add(tagPart);
			}
		}
		//
		return result;
	}

	public Node<E> getTargetNode() {
		return targetNode;
	}
	
	public int getSourceId(){
		int result;
		if (sourceNode != null){
			result = sourceNode.getId();
		} else {
			result = 0;
		}
		//
		return result;
	}

	public int getTargetId(){
		int result;
		if (targetNode != null){
			result = targetNode.getId();
		} else {
			result = 0;
		}
		//
		return result;
	}

	public LinkType getType() {
		return type;
	}

	public double getWeight() {
		return weight;
	}

	public int getWeightAsInt() {
		return new Double(weight).intValue();
	}

	/**
	 * 
	 * @return
	 */
	public void incWeight() {
		this.weight += 1;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isArc() {
		boolean result;

		result = this.type.isArc();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEdge() {
		boolean result;

		result = this.type.isEdge();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isLoop() {
		boolean result;

		if (this.sourceNode == this.targetNode) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param sourceNode
	 * @throws PuckException
	 */
	public void setSourceNode(final Node<E> sourceNode) {
		if (sourceNode == null) {
			throw new NullPointerException("Null parameter detected.");
		} else {
			this.sourceNode = sourceNode;
		}
	}

	public void setTag(final String tag) {
		this.tag = tag;
	}
	
	public void appendTag(final String tag) {
		
		if (tag==null){
			this.tag = tag;
		} else if (!getTags().contains(tag)){
			this.tag += ";"+tag;
		}
	}


	/**
	 * 
	 * @param targetNode
	 * @throws PuckException
	 */
	public void setTargetNode(final Node<E> targetNode) {
		if (sourceNode == null) {
			throw new NullPointerException("Null parameter detected.");
		} else {
			this.targetNode = targetNode;
		}
	}

	/**
	 * 
	 * @param type
	 * @throws PuckException
	 */
	public void setType(final LinkType type) {
		if (type == null) {
			throw new NullPointerException("Null parameter detected.");
		} else {
			this.type = type;
		}
	}

	/**
	 * 
	 * @param weight
	 * @return
	 */
	public void setWeight(final double weight) {
		this.weight = weight;
	}

	public E getReferent() {
		return referent;
	}

	public void setReferent(E referent) {
		this.referent = referent;
	}
	
	public String toString(){
		String result;
		
		result = sourceNode+" > "+targetNode+" " + weight;
		if (tag!=null){
			result +=" "+tag;
		}
		//
		return result;
	}
	
	public E getSourceReferent(){
		E result;
		
		if (sourceNode==null){
			result = null;
		} else {
			result = sourceNode.getReferent();
		}
		//
		return result;
	}	
	
	public E getTargetReferent(){
		E result;
		
		if (targetNode==null){
			result = null;
		} else {
			result = targetNode.getReferent();
		}
		//
		return result;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}
	
	

}
