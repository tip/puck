package oldcore.trash;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;


import org.tip.puck.generators.PGraph;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Attributes;
import org.tip.puck.net.Individual;
import org.tip.puck.net.KinType;
import org.tip.puck.net.Net;
import org.tip.puck.net.workers.BicomponentEdge;



public class TransformWorker {







	   /**
	    * extracts the subnetwork of vertices with positive cluster values
	    * @param cmd the partition command
	    * @return the subnetwork of vertices with positive cluster values
	    */
/*	   public Net extract (Net net, String cmd){
		   String[] str = cmd.split("=");
		   Net result = new Net();
		   result.setName(net.getName());
		   result.setOrigin(net);
		   int deg = net.splitLabel(str);
		   for (Individual v : net.individuals()){
			   if (Trafo.isNull(cmd)) addIndividual(result,v);
			   Object clu = null;
			   if (deg>0) clu = v.getCluster(str[0],null,deg);
			   else clu = v.getCluster(str[0]);
			   if (str.length==1 && clu!=null && !clu.equals(0)) addIndividual(result,v);
			   if (str.length==2 && clu!=null && clu.toString().equals(str[1].trim())) addIndividual(result,v);
		   }
		   result.renumber();
		   return result;
	   }*/
	

	



	   /**
	    * fuses the Net with another Net read from a file
	    * @param netfile the file from which the new Net is read
	    * @param permfile the file containing the correspondance table (which identifies the ID numbers of vertices present in both Nets) 
	    * @throws IOException
	    * @see gui.screens.FuseScreen#go()
	    */
/*	   public void fuse (File netfile, File permfile) throws IOException {
			fuse(new Net(netfile,0),new SimpleReader(permfile).getPermutation());
	   }*/
	   
	   /**
	    * fuses the Net with another Net
	    * @param net2 the second Net
	    * @param index the correspondance table (which identifies the ID numbers of vertices present in both Nets) 
	    * @see OldNet.CopyOfNet#fuse(File, File)
	    */
/*	   private void fuse (Net net1, Net net2, TreeMap<Integer,Integer> index){
		   
		   for (Individual v : net2.individuals()) {
				Integer newId = index.get(v.getId());
				if (newId == null) {
					newId = v.getId() + net2.individuals().lastKey();
				}
				v.setId(newId);
		   }
		   update(net1, net2,false);
	   }
	   

	   
	   /**
	    * gets a generation-invariant permutation of husband vertices
	    * @return a generation-invariant permutation of husband vertices
	    */
/*	   public HashMap<Individual,Individual> getHusbandPermutation(Partition<Individual> gen){
		   HashMap<Individual,Individual> perm = new HashMap<Individual,Individual>();
		   for (Cluster<Individual> clu : gen){
			   Iterator<Individual> iter = clu.iterator();
			   while (iter.hasNext()) {
				   Individual v = (Individual)iter.next();
				   if (v.isFemale() || v.isSingle()) iter.remove();
			   }
		   }
		   for (Cluster<Individual> clu : gen){
			   int[] a = Mat.randomPermutation(clu.size());
			   int j = 0;
			   for (Individual v : clu){
				   perm.put(v, clu.get(a[j]));
				   j++;
			   }
		   }
		   return perm;
	   }
	   

	   
	   /**
	    * gets the Dual of the Net (the Net in P-Graph format)
	    * @return the dual of the Net
	    * @see gui.screens.ExportScreen#go()
	    */
/*	   public PGraph getPGraph (Net net) {
		   PGraph result;
		   
		   result = new PGraph(net); 
		   
		   return result;
	   }
	   

	   
	   /**
	    * resets the spouses or parents of vertices according to a chosen relational attribute
	    * @param net2 the underlying Net (which is transformed)
	    * @param label the label of the relational attribute
	    * @param t the kinship relation parameter (0 spouses, 1 parents)
	    * @see SubNet#SubNet(OldNet, int)
	    */
/*	   static void setKin (Net net1, Net net2, String label, KinType kinType){
		   for (Individual v : net1.individuals()) {
			   TreeMap<Integer,String> map = (TreeMap<Integer,String>)net2.get(v.getId()).getCluster(label,null,0,true);
			   if (map==null) continue;
			   int j = 0;
			   for (int i : map.keySet()){
				   if (kinType==KinType.PARENT) j = net1.get(i).getGender().toInt();
				   addRelation(v,net1.get(i),kinType,j,false);
			   }
		   }
	   }*/
	   
	   /**
	    * updates the Net according to another Net
	    * @param net2 the model Net
		* @param erase true if old data shall be erased if inconsistent with the new data
		* @see OldNet#fuse(OldNet, TreeMap)
		* @see gui.screens.UpdateScreen#go()
	    */
/*		public void update (Net net1, Net net2, boolean erase) {
			
			//transfer Metadata
			if (erase) {
				net1.setInfo(net2.getInfo());
			} else {
				net1.getInfo().addAll(net2.getInfo());
			}
			
			//transfer Individuals
			for (Individual secondIndi : net2.individuals()) {
				Individual firstIndi = net1.get(secondIndi.getId());
				if (firstIndi==null) {
					addIndividual(net1,secondIndi);
				} else {
					update(firstIndi,secondIndi,erase);
				}
			}
		}*/
		
		/**
		 * update the vertex on to the model of another vertex
		 * @param secondIndi the model vertex
		 * @param erase true if old vertex data shall be erased if inconsistent with the new data
		 * @see maps.Net#update(Net, boolean)
		 * @see maps.Net#eliminateDoubles()
		 */
/*		public static void update (Individual firstIndi, Individual secondIndi, boolean erase) {
			if (erase) {
				
				//updateName
				String secondName = secondIndi.getName();
				if (secondName.charAt(0)!='#' && secondName.indexOf("*")==-1){
					firstIndi.setName(secondName);
				}
				
				//updateGender
				firstIndi.setGender(secondIndi.getGender());
				
			} else {
				updateAttributes(firstIndi, secondIndi);
				updateKin(firstIndi, secondIndi,erase); 
			}
		}*/		
		

		

		/**
	    * updates the attributes of the vertex on the model of the attributes of another vertex
	    * @param secondIndi the model vertex
	    * @see OldIndividual#update(OldIndividual, boolean)
	    */
/*		private static void updateAttributes (Individual firstIndi, Individual secondIndi) {
			if (secondIndi.attributes() == null) return;
			if (firstIndi.attributes() == null) {
				firstIndi.attributes = new Attributes();
			}
			for (Attribute a : firstIndi.attributes()) {
				for (Attribute b : secondIndi.attributes()) {
					if (a.equals(b)) a.update(b);
				} 
			}
			for (Attribute b : secondIndi.attributes()) {
				b.updateLinkedAttribute(firstIndi.getId(),(Net)home);
				if (!(firstIndi.attributes().contains(b))) firstIndi.attributes().add(b);
			}
		}	*/	
		
		
		/**
		 * transfers relatives from a vertex in another network
		 * <p> used for subnet construction
		 * @param secondIndi the Vertex whose relatives are to be copied
		 * @param net2 the double's home network
		 * @param k the kintype not to be copied (-1 children, 0 spouses, 1 parents)
		 * @see maps.Subnet#SubNet(Net,int)
		 */
/*		public static void copyKin(Individual firstIndi, Individual secondIndi, Net net2, KinType notCopied){
			for (int i=-1;i<2;i++){
				KinType kinType = KinType.valueOf(i);
				if (kinType==notCopied || secondIndi.getKin(i)==null) continue;
				int position=-1;
				for (Individual w : secondIndi.getKin(i)){
					position++;
					if (w!=null) {
						Individual u = net2.get(w.getId());
						if (u!=null) addRelation(firstIndi,u,kinType,position,false);
					}
				}
			}
		}*/
		
		//check and harmonize with copyKin
		/**
		 * copies relatives from one vertex to another, erasing or not the original relatives
		 * <p> used for vertex update
		 * @param secondIndi the vertex whose relatives are copied
		 * @param erase true if original relatives are to be erased
		 * @see OldIndividual#update(OldIndividual,boolean)
		 * @since lastmodified 11-05-29
		 */
/*		private static void updateKin(Individual firstIndi, Individual secondIndi, boolean erase){
			for (int i=-1;i<2;i++){
				KinType kinType = KinType.valueOf(i);
				if (secondIndi.getKin(i)==null) continue;
				int position=-1;
				for (Individual w : secondIndi.getKin(i)){
					position++;
					if (kinType==KinType.PARENT && !erase && firstIndi.getParent(position)!=null) continue;
					if (w==null) continue; 
					addRelation(firstIndi,w,kinType,position,true);
					removeRelative(w,secondIndi,kinType,false);
				}
			}
		}	*/	
		
		

		

		   /**
		    * copies the vertices of another Net and their relatives
		    * <p> used for subnet construction
		    * @param net2 the Net from which the relatives are copied
		    * @param k the kintype not to be copied (-1 children, 0 spouses, 1 parents)
		    * @see OldNet.CopyOfNet#Net(OldNet)
		    * @see trash.SubNet#SubNet(OldNet, int)
		    */
/*		   static void copy (Net net1, Net net2, KinType notCopied) {
			   for (Individual indi : net2.individuals()) {
					addIndividual(net1, indi.clone());
			   }
			   copyKin (net1, net2, notCopied);
		   }*/
		   
/*		   private static void addIndividual (Net net, Individual indi){
			   // fill appropriate code
		   }*/
		   
		   /**
		    * transfers relatives from the vertices' doubles in another Net to the vertices of the Net
		    * <p> used for subnet construction
		    * @param net2 the Net from which the relatives are copied
		    * @see maps.Subnet#SubNet(OldNet,int)
		    */
/*		   private static void copyKin (Net net1, Net net2){
			   copyKin (net1, net2,null);
		   }*/

		   /**
		    * transfers relatives from the vertices' doubles in another Net to the vertices of the Net
		    * <p> used for subnet construction
		    * @param net2 the Net from which the relatives are copied
		    * @param k the kintype not to be copied (-1 children, 0 spouses, 1 parents)
		    * @see OldNet.CopyOfNet#copyKin(OldNet)
		    * @see OldNet.CopyOfNet#copy(OldNet, int)
		    */
/*		   private static void copyKin (Net net1, Net net2, KinType notCopied){
			   for (Individual firstIndi: net1.individuals()){
				   Individual secondIndi = net2.get(firstIndi.getId());
				   copyKin(firstIndi, secondIndi, net1, notCopied);
			   }
		   }*/
		   

		   

		   
		   //2
		   // Migrated to CutTailsWorker#copyCuttingTails(Net)
/*		   public static Net cutTails(Net net){
			   Net result;
			   
			   result = new Net();
			   Map<Individual,Integer> colors = getTreeColors(net,false); 

			   for (Individual v : net.individuals()){
				   if (colors.get(v)>-1) {
					   addIndividual(result,v.cloneWithAttributes()); 
				   } 
			   }
			   copyKin(result,net);
			   
			   //
			   return result;
		   }*/
		   

		   //3
		   // Migrated to NetUtils#copyWithoutStructuralChildren(Net)
/*		   public static Net eliminateStructuralChildren(Net net){
			   Net result;
			   
			   result = new Net();
			   
			   for (Individual v : net.individuals()){
				   if (!v.isSingle() || !v.isSterile()) {
					   addIndividual(result,v.cloneWithAttributes()); 
				   } 
			   }
			   copyKin(result,net);
//			   if (condition<7) getColorClusters(((String[])Puck.getParam("Subnet Labels"))[condition],1);
			   
			   //
			   return result;
		   }*/
		   
		   //4
		   // Migrateted to NetUtils.expand
/*		   public static Net expand(Net net, Cluster<Individual> cluster, int t){
			   Net result;
			   
			   result = new Net();
			   
			   Cluster<Individual> expandedCluster = new Cluster<Individual>();
			   for (Individual v: cluster){
				   expand(expandedCluster,v,t);
			   }
			   for (Individual v : expandedCluster){
				   addIndividual(result,v.cloneWithAttributes()); 
			   }
			   copyKin(result,net);
			   
			   //
			   return result;
		   }*/
		   
			/**
			 * marks all unmarked kin (of type t) of a vertex
			 * <p> used for expanding a vertex partition upwards
			 * @param t the kin type
			 * @see Net#transform(int)
			 */
/*			private static void expand (Cluster<Individual> expandedCluster, Individual ego, int t){
				
				if (!expandedCluster.contains(ego.getId())) {
					expandedCluster.add(ego);
					for (Individual alter: ego.getKin(t)){
						if (alter!=null) expand(expandedCluster, alter, t);
					}
				}
			}*/
		   

		   //6
		   // Migrated to NetUtils#copyWithoutStructuralChildren(Net)
/*		   public static Net eliminateVirtualIndividuals(Net net){
			   Net result;
			   
			   result = new Net();
			   //Map<Individual,Integer> colors = getTreeColors(net,false); 

			   for (Individual v : net.individuals()){
				   if (v.getName()!=null && v.getName().length()!=0 && v.getName().charAt(0)!='#') {
					   addIndividual(result,v.cloneWithAttributes()); 
				   } 
			   }
			   copyKin(result,net);
			   
			   //
			   return result;
		   }*/
		   
		   
		   //7
		   // Migrated to NetUtils#copyWithoutStructuralChildren(Net)
		   /**
		    * eliminates the vertices marked as double entries (i.e., vertices having a number instead of a name)
		    * <p> the "name" of a double is the ID number of the original
		    */
/*		   public static Net eliminateDoubles(Net net){
			   Net result;
			   
			   result = new Net();
			   
			   for (Individual doublon : net.individuals()){
				   try {
					   Individual original = net.get(Integer.parseInt(doublon.getFirstName()));
					   update(original,doublon,false);
					   original.setAttribute("DOUBLE",doublon.getId()+"");
					   addIndividual(result,original);
				   } catch (NumberFormatException nfe) {}
			   }
			   
			   //
			   return result;
		   }
		   
		   //8+9 redefineSpouses / redefineParents
		   public static Net redefineRelations(Net net, String cmd, KinType kinType){ //
			   Net result;
			   
			   result = new Net();
				   copy(result,net,kinType);
				   setKin(result,net,cmd,kinType);

			   
			   //
			   return result;
		   }
		   
		   //11
			//corresponds to method "extract" called by
			//writePopStatistics, writeSpecialReport...
		   //Migrated to NetUtils.extractByClusterValue
		   public static Net positiveClusterValues(Net net, String cmd){
			   Net result;
			   
			   result = new Net();
			   Partition<Individual> p = new Partition<Individual>(net,cmd);
			   for (Individual v : p.getValues().keySet()){
				   Object value = p.getVal(v);
				   if (value!=null && !value.equals(0)){ //   also account for binarization: clu.toString().equals(cmd.split("=")[1].trim())
					   addIndividual(result,v.cloneWithAttributes()); 
				   }
			   }
			   copyKin(result,net);
			   
			   //
			   return result;
		   }*/
		   
		   //12
/*		   public static Net extractLargeClusters(Net net, String cmd, double minPercent){  // 0.005
			   Net result;
			   
			   result = new Net();
			   Cluster<Individual> largeClusters = new Cluster<Individual>();
			   Partition<Individual> p = new Partition<Individual>(net,cmd);
			   int minSize = (int)(minPercent*net.size());
			   
			   for (Cluster<Individual> clu : p){
				   if (clu.size()>=minSize) {
					   for (Individual v : clu){
						   addIndividual(result,v.cloneWithAttributes()); 
					   }
				   }
			   }
			   copyKin(result,net);
			   
			   //
			   return result;
		   }*/
		   

		   /**
		    * sets the colors of the verticeds of the Net according to their belonging to a tree
		    * @param matrim true if structural children shall be excluded
		    * @see OldNet.CopyOfNet#setColors(int)
		    */
   		   // Migrated to CutTailsWorker#copyCuttingTails(Net)
/*		   private static Map<Individual,Integer> getTreeColors (Net net, boolean matrim){
			   Map<Individual,Integer> result;
			   
			   result = new HashMap<Individual,Integer>();
			   for (Individual v : net.individuals()){
				   if (matrim) {
					   getTreeColor(result, v, null,-1);
				   } else {
					   getTreeColor(result, v, null);
				   }
			   }
			   //
			   return result;
		   }*/
		   
		   
		   //from Individual
			//Verify index meaning and check!
			/**
			 * sets the color of the vertex according to its belonging to a tree
			 * @param root the root of the potential tree
			 * @return the tree color of the vertex (0 not yet marked, 1 in the course of being marked, 2 not a tree vertex, -1 a tree vertex)
			 */
   		   // Migrated to CutTailsWorker#copyCuttingTails(Net)
/*			public static int getTreeColor (Map<Individual,Integer> colors, Individual ego, Individual root) {
				
				if (colors.get(ego)!=0) return colors.get(ego);
				colors.put(ego,1);
				
				int k = 0;
				
				//
				if (root!=null) k = 1;
				
				for (int i=-1;i<2;i++){
					if (ego.getKin(i)==null) continue;
					for (Individual v : ego.getKin(i)){
						if (v==null) continue;
						if (v==root) continue;
						if (getTreeColor(colors,v,ego)==-1) continue;
						k++;
					}
				}
				if (k>1) colors.put(ego, 2);
				else colors.put(ego, -1);
				return colors.get(ego);
			}*/
			
			
			   
			/**
			 * sets the color of the vertex according to its belonging to a tree, excluding structural children
			 * @param root the root of the potential tree
			 * @param j the direction (ungendered kin tie index) of the search (1 ascending, 0 horizontal, -1 descending)
			 * @return the tree color of the vertex (0 not yet marked, 1 in the course of being marked, 2 not a tree vertex, -1 a tree vertex)
			 */
   		   // Migrated to CutTailsWorker#copyCuttingTails(Net)
/*			public static int getTreeColor (Map<Individual,Integer> colors, Individual ego, Individual root, int j) {
				
				if (colors.get(ego)!=0) return colors.get(ego);
				colors.put(ego,1);

				//
				if (ego.isSterile() && ego.isSingle()) {
					colors.put(ego,-1);
					return colors.get(ego);
				}
				
				int k = 0;

				//
				int h=0;
				
				for (int i=-1;i<2;i++){
					if (ego.getKin(i)==null) continue;
					for (Individual v : ego.getKin(i)){
						if (v==null) continue;
						
						if (getTreeColor(colors, v, ego, i)!=-1){
							k++;
							if (i!=-1) h++;
						}
						if (k>1 && h>0) {
							colors.put(ego, 2);
							return colors.get(ego);
						}
					}
				}
				colors.put(ego,-1);
		  	   	return colors.get(ego);
		   }*/
		   
		   

		   /**
		    * assigns an attribute to the vertices having a given color and erases other colors
		    * @param s the attribute label
		    * @param i the color chosen as a filter criterion
		    */
/*		   static void getColorClusters (Map<Individual,Integer> colors, String label, int i) {
			   for (Individual v : colors.keySet()){
				   int color = colors.get(v);
				   if (color!=i) color = 0;
				   v.setAttribute(label,""+color);
			   }
		   }*/






		   //parametrize the label choice (ev. via static function for attribute choices)
		   /**
		    * transforms a chosen partition into a relation
		    * @see gui.screens.OptionMask#transform(int)
		    */
/*		   public void relationalize(){
			   for (String lb : Puck.getLabels(4)){
				   getPartition(lb).relationalize();
			   }
		   }*/
		   
			
			/**
			 * adds a relative. the operation can be made reciprocal (so that ego is simultaneously added as a relative to alter) 
			 * @param alter the vertex to be added
			 * @param reciprocal the reciprocal character of the operation
			 */
/*			public static void addRelation (Individual ego, Individual alter, KinType kinType, int position, boolean reciprocal){
				if (ego!=null || alter!=null){
					// fill by appropriate code: add alter to ego's relatives of given type
					if (reciprocal) addRelation(alter,ego, kinType, position, false);
				}
			}
			
			public static void removeRelative (Individual ego, Individual alter, KinType kinType, boolean reciprocal){
				if (ego!=null || alter!=null){
					// fill by appropriate code: remove alter from ego's relatives of given type
					if (reciprocal) removeRelative(alter,ego, kinType, false);
				}
			}*/

			
			/**
			 * adds marriage links between coparents
			 * @since 11-06-21
			 */
			// Migrated to NetUtils.marryCoparents
/*			public void addCoparentsAsSpouses(){
				for (Individual v : individuals){
					if (v.getGender().isMale() && !v.isSterile()) {
						int position = -1;
						for (Individual ch : v.getChildren()){
							position++;
							addRelation(v,ch.getParent(v.getGender().invert().toInt()),KinType.SPOUSE,position,true);
						}
					}
				}
				System.out.println("coParents added");
			}*/



}
