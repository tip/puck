package org.tip.puck.alliancenets;


/**
 * @author Telmo Menezes
 *
 */
public class Alliance {
    private Group origin;
    private Group target;
    private long timestamp;
    private double weight;
    
    public Alliance(Group origin, Group target, double weight, long timestamp) {
        this.origin = origin;
        this.target = target;
        this.weight = weight;
        this.timestamp = timestamp;
    }
    
    public Alliance(Group origin, Group target, long timestamp) {
        this(origin, target, 0.0, timestamp);
    }
    
    public Alliance(Group origin, Group target, double weight) {
        this(origin, target, weight, 0);
    }
    
    public Group getOrigin() {
        return origin;
    }

    public Group getTarget() {
        return target;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}