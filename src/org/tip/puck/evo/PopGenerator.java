package org.tip.puck.evo;


import java.util.Vector;


/**
 * Population generator interface.
 * 
 * Population generators are used by EvoGen
 * objects to select individuals in a population to create the next generation.
 * 
 * @author Telmo Menezes (telmo@telmomenezes.com)
 */
public interface PopGenerator {
    public int popSize();
	public Vector<Generator> newGeneration(EvoGen evo);
	public String infoString();
}