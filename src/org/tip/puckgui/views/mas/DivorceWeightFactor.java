package org.tip.puckgui.views.mas;

/**
 * 
 * @author TIP
 */
public class DivorceWeightFactor implements WeightFactor {

	private final Type type = Type.DIVORCE;
	private double probability;

	/**
	 * 
	 */
	public DivorceWeightFactor() {
		this.probability = 0.01;
	}

	public double getProbability() {
		return probability;
	}

	@Override
	public Type getType() {
		return type;
	}

	public void setProbability(final double probability) {
		this.probability = probability;
	}
}
