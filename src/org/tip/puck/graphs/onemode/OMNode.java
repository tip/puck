/**
 * 
 */
package org.tip.puck.graphs.onemode;

import java.util.HashMap;
import java.util.Map;

import org.tip.puck.graphs.onemode.OMGraph.GraphMode;
import org.tip.puck.net.Family;
import org.tip.puck.net.Individual;
import org.tip.puck.util.Numberable;

/**
 * @author Telmo Menezes
 *
 */
public class OMNode {
	
	private OMNode femaleLink;
	private OMNode maleLink;
	private Numberable referent;
	private boolean virtual;
	private boolean marriedParents;
	
	public boolean hasMarriedParents() {
		return marriedParents;
	}

	public void setMarriedParents(boolean marriedParents) {
		this.marriedParents = marriedParents;
	}

	public boolean isVirtual() {
		return virtual;
	}

	public void setVirtual(boolean virtual) {
		this.virtual = virtual;
	}

	private Map<OMNode, Integer> descendants;
	private Map<OMNode, Integer> ascendants;
	private Map<OMNode, Integer> originalDescendants;
	private Map<OMNode, Integer> originalAscendants;
	private Map<OMNode, Integer> originalRelatives;

	private int outDegree;
	private int outDegreeF;
	private int outDegreeM;
	
	public OMNode(Numberable referent, 
			OMNode wifeLink, OMNode husbandLink) {
//		this.wife = wife;
//		this.husband = husband;
		this.referent = referent;
		this.femaleLink = wifeLink;
		this.maleLink = husbandLink;
		
		descendants = new HashMap<OMNode, Integer>();
		ascendants = new HashMap<OMNode, Integer>();
		outDegree = 0;
	}
	
	public Family getFamily() {
		Family result = null;
		if (referent instanceof Family){
			result = (Family)referent;
		}
		//
		return result;
	}
	
	public Individual getIndividual() {
		Individual result = null;
		if (referent instanceof Individual){
			result = (Individual)referent;
		}
		//
		return result;
	}

	public OMNode(Numberable referent){
		this(referent, null, null);
	}
	
	public OMNode cloneWithLinks() {
		return new OMNode(referent,femaleLink, maleLink);
	}
	
/*	public OMNode(OMNode m) {
		this(m.wife, m.husband, m.wifeLink, m.husbandLink);
	}*/
	
	public void addDescendant(OMNode origin, int distance) {
		// Recursive step
		if (femaleLink != null) {
			femaleLink.addDescendant(origin, distance + 1);
		}
		if (maleLink != null) {
			maleLink.addDescendant(origin, distance + 1);
		}
		
		// A marriage is not its own descendant
		if (this == origin) {
			return;
		}
		
		// Register descendants and update distances
		if (descendants.containsKey(origin)) {
			Integer curDistance = descendants.get(origin);
			if (curDistance > distance) {
				descendants.put(origin, distance);
			}
		}
		else {
			descendants.put(origin, distance);
		}
		
		// Register ascendants and update distances
		if (origin.ascendants.containsKey(this)) {
			Integer curDistance = origin.ascendants.get(this);
			if (-distance > curDistance) {
				origin.ascendants.put(this, -distance);
			}
		}
		else {
			origin.ascendants.put(this, -distance);
		}
		
	}
	
	public void clearDescendants() {
		descendants.clear();
	}
	
	public void storeOriginalDescendants() {
		originalDescendants = new HashMap<OMNode, Integer>(descendants);
		originalAscendants = new HashMap<OMNode, Integer>(ascendants);
	}
	
	public void storeOriginalRelatives() {
		originalRelatives = new HashMap<OMNode, Integer>();
		for (OMNode ascendant : originalAscendants.keySet()){
			int upDistance = originalAscendants.get(ascendant);
			Integer curDistance = originalRelatives.get(ascendant);
			if (curDistance==null || Math.abs(curDistance) > Math.abs(upDistance)) {
				originalRelatives.put(ascendant, upDistance);
			}
			for (OMNode consanguine : ascendant.originalDescendants.keySet()){
				int distance = upDistance + ascendant.originalDescendants.get(consanguine);
				curDistance = originalRelatives.get(consanguine);
				if (curDistance==null || Math.abs(curDistance) > Math.abs(distance)) {
					originalRelatives.put(consanguine, distance);
				}
			}
		}
		for (OMNode descendant : originalDescendants.keySet()){
			int downDistance = originalDescendants.get(descendant);
			if (descendant.originalAscendants!=null){
				Integer curDistance = originalRelatives.get(descendant);
				if (curDistance==null || Math.abs(curDistance) > Math.abs(downDistance)) {
					originalRelatives.put(descendant, downDistance);
				}
				for (OMNode affine : descendant.originalAscendants.keySet()){
					int distance = downDistance + descendant.originalAscendants.get(affine);
					curDistance = originalRelatives.get(affine);
					if (curDistance==null || Math.abs(curDistance) > Math.abs(distance)) {
						originalRelatives.put(affine, distance);
					}
				}
			}
		}
	}

	
	public int generationalDistance(OMNode m) {
		if (this == m) {
			return 0;
		}
		else if (descendants.containsKey(m)){
			return descendants.get(m);
		}
		else {
			return -1;
		}
	}
	
	public int originalGenerationalDistanceNew(OMNode m) {
		if (this == m) {
			return 0;
		}
		else if (originalRelatives.containsKey(m)){
			return Math.abs(originalRelatives.get(m));
		}
		else {
			return -1;
		}
	}
	

	
	public int originalGenerationalDistance(OMNode m) {
		if (this == m) {
			return 0;
		}
		else if (originalDescendants.containsKey(m)){
			return originalDescendants.get(m);
		}
		else {
			return -1;
		}
	}
	
	public boolean hasDescendant(OMNode m) {
		return descendants.containsKey(m);
	}
	
	public double distance(OMNode m) {
		double dist = 0;
		if (femaleLink != m.femaleLink) {
			dist += 0.5;
		}
		if (maleLink != m.maleLink) {
			dist += 0.5;
		}
		
		return dist;
	}
	
	public void incOutDegree() {
		outDegree += 1;
	}
	
	public int numberOfDescendants() {
		return descendants.size();
	}
	
	public String hashKey() {
		return referent.hashKey();
/*		String wifeStr = "?";
		String husbandStr = "?";
		if (wife != null) {
			wifeStr = "" + wife.getId();
		}
		if (husband != null) {
			husbandStr = "" + husband.getId();
		}
		return wifeStr + " [+] " + husbandStr; */
	}
	
	@Override
	public String toString() {
		return hashKey();
/*		String wifeStr = "?";
		String husbandStr = "?";
		if (wife != null) {
			wifeStr = "" + wife.getId();
		}
		if (husband != null) {
			husbandStr = "" + husband.getId();
		}
		return wifeStr + " [+] " + husbandStr;*/
	}
	
	public Individual getWife() {
		Individual result = null;
		if (referent != null && referent instanceof Family) {
			result = ((Family)referent).getWife();
		}
		//
		return result;
	}
	
	public void setWife(Individual wife) {
		if (referent instanceof Family){
			((Family)referent).setWife(wife);
		} else {
			referent = wife;
		}
//		this.wife = wife;
	}
	
	public Individual getHusband() {
		Individual result = null;
		if (referent != null && referent instanceof Family) {
			result = ((Family)referent).getHusband();
		}
		//
		return result;
	}
	
	public void setHusband(Individual husband) {
		if (referent instanceof Family){
			((Family)referent).setHusband(husband);
		} else {
			referent = husband;
		}
//		this.husband = husband;
	}
	
	public void setReferent (Numberable referent){
		this.referent = referent;
	}

	public OMNode getFemaleLink() {
		return femaleLink;
	}

	public void setFemaleLink(OMNode wifeLink) {
		this.femaleLink = wifeLink;
	}

	public OMNode getMaleLink() {
		return maleLink;
	}

	public void setMaleLink(OMNode husbandLink) {
		this.maleLink = husbandLink;
	}

	public Map<OMNode, Integer> getDescendants() {
		return descendants;
	}

	public int getOutDegree() {
		return outDegree;
	}

	public int getOutDegreeF() {
		return outDegreeF;
	}

	public void incOutDegreeF() {
		outDegreeF++;
	}

	public int getOutDegreeM() {
		return outDegreeM;
	}

	public void incOutDegreeM() {
		outDegreeM++;
	}
}
