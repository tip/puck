package org.tip.puckgui.views;

import java.awt.Component;
import java.io.File;
import java.util.ResourceBundle;

import javax.swing.JFileChooser;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.util.ToolBox;
import org.tip.puckgui.util.GenericFileFilter;

/**
 * This class is based on the dialog type so it must set at first.
 * 
 * http://stackoverflow.com/questions/596429/adjust-selected-file-to-filefilter-
 * in-a-jfilechooser
 * 
 * @author cpm
 * 
 */
public class CorpusSaveFileSelector extends JFileChooser {

	private static final long serialVersionUID = 5926488520862787717L;
	
	private static final Logger logger = LoggerFactory.getLogger(CorpusSaveFileSelector.class);

	/**
	 * 
	 */
	public CorpusSaveFileSelector(final File targetFile) {
		super();

		//
		setSelectedFile(targetFile);
		setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.saveAsFileChooser.text"));
		setFileSelectionMode(JFileChooser.FILES_ONLY);
		setAcceptAllFileFilterUsed(true);
		setApproveButtonText("Save");
		setDialogType(CUSTOM_DIALOG);

		//
		GenericFileFilter defaultFileFilter = new GenericFileFilter("Genealogic files (*.puc, *.iur.ods, *.iur.txt, *.iur.xls, *.ged, *.pl, *.tip, *.xml)",
				"ged", "iur.ods", "pl", "puc", "tip", "iur.txt", "iur.xls", "xml");
		addChoosableFileFilter(defaultFileFilter);
		addChoosableFileFilter(new GenericFileFilter("PUCK (*.puc)", "puc"));
		addChoosableFileFilter(new GenericFileFilter("IUR (*.iur.ods,*.iur.txt,*.iur.xls)", "iur.ods", "iur.txt", "iur.xls"));
		addChoosableFileFilter(new GenericFileFilter("IUR OpenDocumentFormat Spreadsheet (*.iur.ods)", "iur.ods"));
		addChoosableFileFilter(new GenericFileFilter("IUR Text (*.iur.txt)", "iur.txt"));
		addChoosableFileFilter(new GenericFileFilter("IUR Microsoft Excel (*.iur.xls)", "iur.xls"));
		addChoosableFileFilter(new GenericFileFilter("GEDCOM (*.ged)", "ged"));
		addChoosableFileFilter(new GenericFileFilter("Prolog (*.pl)", "pl"));
		addChoosableFileFilter(new GenericFileFilter("TIP (*.tip)", "tip"));
		addChoosableFileFilter(new GenericFileFilter("Extensible Markup Language (*.xml)", "xml"));
		setFileFilter(defaultFileFilter);
	}

	/**
	 * 
	 */
	@Override
	public void approveSelection() {
		//
		File targetFile = getSelectedFile();

		logger.debug("getCurrentDirectory(): {}", getCurrentDirectory());
		logger.debug("selectedFile={}", targetFile);

		if (StringUtils.isBlank(ToolBox.getExtension(targetFile))) {
			targetFile = new File(targetFile.getAbsolutePath() + ".txt");
		}

		//
		if (!StringUtils.endsWithAny(ToolBox.getExtension(targetFile), "ged", "ods", "pl", "puc", "tip", "txt", "xls", "xml")) {
			//
			targetFile = new File(targetFile.getAbsolutePath() + ".txt");
			setSelectedFile(targetFile);
		}

		//
		if (targetFile.exists()) {
			//
			if (ConfirmOverwriteDialog.showDialog(null)) {
				//
				logger.debug("Overwrite");
				super.approveSelection();

			} else {
				//
				logger.debug("Cancel overwrite");
			}
		} else {
			//
			super.approveSelection();
		}
	}

	/**
	 * 
	 */
	@Override
	public void cancelSelection() {
		//
		logger.debug("Cancel selection.");
		super.cancelSelection();
	}

	/**
	 * 
	 */
	@Override
	public void setSelectedFile(final File file) {
		//
		super.setSelectedFile(file);
	}

	/**
	 * 
	 */
	@Override
	public void setVisible(final boolean visible) {
		//
		super.setVisible(visible);

		if (!visible) {
			//
			resetChoosableFileFilters();
		}
	}

	/**
	 * This method is the main one of the selector.
	 * 
	 * @param parent
	 * @return
	 */
	public static File showSelectorDialog(final Component parent, final File targetFile) {
		File result;

		//
		CorpusSaveFileSelector selector = new CorpusSaveFileSelector(targetFile);

		//
		if (selector.showDialog(parent, null) == JFileChooser.APPROVE_OPTION) {
			//
			logger.debug("getCurrentDirectory(): {}", selector.getCurrentDirectory());
			logger.debug("getSelectedFile() : {}", selector.getSelectedFile());
			result = selector.getSelectedFile();

		} else {
			//
			result = null;
		}

		//
		return result;
	}
}
