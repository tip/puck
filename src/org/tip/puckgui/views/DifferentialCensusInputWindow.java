package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.census.workers.CensusCriteria;
import org.tip.puck.census.workers.CensusReporter;
import org.tip.puck.census.workers.ChainValuator.ChainProperty;
import org.tip.puck.census.workers.CircuitType;
import org.tip.puck.census.workers.RestrictionType;
import org.tip.puck.census.workers.SiblingMode;
import org.tip.puck.census.workers.SymmetryType;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.report.Report;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.PuckGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class DifferentialCensusInputWindow extends JFrame {
	
	private static final long serialVersionUID = 179593669410505055L;
	private static final Logger logger = LoggerFactory.getLogger(DifferentialCensusInputWindow.class);

	private JFrame thisJFrame;
	private NetGUI netGUI;
	private JPanel contentPane;
	private final ButtonGroup buttonGroupSymmetry = new ButtonGroup();
	private final ButtonGroup buttonGroupFiliation = new ButtonGroup();
	private final ButtonGroup buttonGroupSib = new ButtonGroup();
	private final ButtonGroup buttonGroupCircuit = new ButtonGroup();
	private JTextField txtfldPattern;
	private JTextField txtfldFilter;
	private JCheckBox chckbxCrossSex;
	private JCheckBox chckbxMarriedOnly;
	private final ButtonGroup buttonGroupRestriction = new ButtonGroup();
	private JRadioButton rdbtnCircuitTypeCircuit;
	private JRadioButton rdbtnCircuitTypeRing;
	private JRadioButton rdbtnCircuitTypeMinor;
	private JRadioButton rdbtnCircuitTypeMinimal;
	private JRadioButton rdbtnFiliationAgnatic;
	private JRadioButton rdbtnFiliationUterine;
	private JRadioButton rdbtnFiliationCognatic;
	private JRadioButton rdbtnFiliationBilateral;
	private JRadioButton rdbtnFiliationIdentity;
	private JRadioButton rdbtnSibNone;
	private JRadioButton rdbtnSibFull;
	private JRadioButton rdbtnSibAll;
	private JLabel lblRelationType;
	private JButton btnRestoreDefaults;
	private JComboBox cmbbxIndividualPartition;
	private JLabel lblChainClassification;
	private JComboBox cmbbxClassification;
	private JComboBox cmbbxClassificatoryLinking;

	/**
	 * Create the frame.
	 */
	public DifferentialCensusInputWindow(final NetGUI netGUI) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(FooReporterInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		this.netGUI = netGUI;
		setTitle("Differential Census Reporter Inputs");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 518, 512);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panelInputs = new JPanel();
		this.contentPane.add(panelInputs, BorderLayout.CENTER);
		panelInputs.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, }));

		JLabel lblPattern = new JLabel("Pattern:");
		panelInputs.add(lblPattern, "4, 4, right, default");

		this.txtfldPattern = new JTextField();
		this.txtfldPattern.setColumns(10);
		panelInputs.add(this.txtfldPattern, "6, 4, fill, default");

		JLabel lblex = new JLabel("(ex. \"3 2 4\" ou \"HF(F)F\")");
		panelInputs.add(lblex, "6, 6");

		JLabel lblFilter = new JLabel("Filter:");
		panelInputs.add(lblFilter, "4, 8, right, default");

		this.txtfldFilter = new JTextField();
		this.txtfldFilter.setColumns(10);
		panelInputs.add(this.txtfldFilter, "6, 8, fill, default");

		JLabel lblClassificatoryLinking = new JLabel("Classificatory linking:");
		panelInputs.add(lblClassificatoryLinking, "4, 10, right, default");

		this.cmbbxClassificatoryLinking = new JComboBox();
		this.cmbbxClassificatoryLinking.setModel(new DefaultComboBoxModel(new String[] { "" }));
		panelInputs.add(this.cmbbxClassificatoryLinking, "6, 10, fill, default");

		this.lblRelationType = new JLabel("Individual partition:");
		panelInputs.add(this.lblRelationType, "4, 12, right, default");

		this.cmbbxIndividualPartition = new JComboBox();
		this.cmbbxIndividualPartition.setModel(new DefaultComboBoxModel(new String[] { "" }));
		panelInputs.add(this.cmbbxIndividualPartition, "6, 12, fill, default");

		this.lblChainClassification = new JLabel("Chain classification:");
		panelInputs.add(this.lblChainClassification, "4, 14, right, default");

		this.cmbbxClassification = new JComboBox();
		this.cmbbxClassification.setModel(new DefaultComboBoxModel(ChainProperty.values()));
		panelInputs.add(this.cmbbxClassification, "6, 14, fill, default");

		this.chckbxCrossSex = new JCheckBox("Cross-sex chains only");
		this.chckbxCrossSex.setSelected(true);
		panelInputs.add(this.chckbxCrossSex, "6, 16");

		this.chckbxMarriedOnly = new JCheckBox("Married only");
		this.chckbxMarriedOnly.setSelected(true);
		panelInputs.add(this.chckbxMarriedOnly, "6, 18");

		JPanel panel_1 = new JPanel();
		panelInputs.add(panel_1, "4, 20, 3, 1, fill, fill");
		panel_1.setLayout(new GridLayout(0, 3, 0, 0));

		JPanel panelCircuitType = new JPanel();
		panel_1.add(panelCircuitType);
		panelCircuitType.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Circuit Type", TitledBorder.LEADING, TitledBorder.TOP, null,
				new Color(51, 51, 51)));
		panelCircuitType.setLayout(new BoxLayout(panelCircuitType, BoxLayout.Y_AXIS));

		this.rdbtnCircuitTypeCircuit = new JRadioButton("Circuit");
		panelCircuitType.add(this.rdbtnCircuitTypeCircuit);
		this.rdbtnCircuitTypeCircuit.setSelected(true);
		this.buttonGroupCircuit.add(this.rdbtnCircuitTypeCircuit);

		this.rdbtnCircuitTypeRing = new JRadioButton("Ring");
		panelCircuitType.add(this.rdbtnCircuitTypeRing);
		this.buttonGroupCircuit.add(this.rdbtnCircuitTypeRing);

		this.rdbtnCircuitTypeMinor = new JRadioButton("Minor");
		panelCircuitType.add(this.rdbtnCircuitTypeMinor);
		this.buttonGroupCircuit.add(this.rdbtnCircuitTypeMinor);

		this.rdbtnCircuitTypeMinimal = new JRadioButton("Minimal");
		panelCircuitType.add(this.rdbtnCircuitTypeMinimal);
		this.buttonGroupCircuit.add(this.rdbtnCircuitTypeMinimal);

		JPanel panelFiliationType = new JPanel();
		panel_1.add(panelFiliationType);
		panelFiliationType.setBorder(new TitledBorder(null, "Filiation Type", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelFiliationType.setLayout(new BoxLayout(panelFiliationType, BoxLayout.Y_AXIS));

		this.rdbtnFiliationAgnatic = new JRadioButton("Agnatic");
		this.buttonGroupFiliation.add(this.rdbtnFiliationAgnatic);
		panelFiliationType.add(this.rdbtnFiliationAgnatic);

		this.rdbtnFiliationUterine = new JRadioButton("Uterine");
		this.buttonGroupFiliation.add(this.rdbtnFiliationUterine);
		panelFiliationType.add(this.rdbtnFiliationUterine);

		this.rdbtnFiliationCognatic = new JRadioButton("Cognatic");
		this.buttonGroupFiliation.add(this.rdbtnFiliationCognatic);
		this.rdbtnFiliationCognatic.setSelected(true);
		panelFiliationType.add(this.rdbtnFiliationCognatic);

		this.rdbtnFiliationBilateral = new JRadioButton("Bilateral");
		this.buttonGroupFiliation.add(this.rdbtnFiliationBilateral);
		panelFiliationType.add(this.rdbtnFiliationBilateral);

		this.rdbtnFiliationIdentity = new JRadioButton("Identity");
		this.buttonGroupFiliation.add(this.rdbtnFiliationIdentity);
		panelFiliationType.add(this.rdbtnFiliationIdentity);

		JPanel panelSiblingMode = new JPanel();
		panel_1.add(panelSiblingMode);
		panelSiblingMode.setBorder(new TitledBorder(null, "Sibling Mode", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelSiblingMode.setLayout(new BoxLayout(panelSiblingMode, BoxLayout.Y_AXIS));

		this.rdbtnSibNone = new JRadioButton("None");
		this.rdbtnSibNone.setToolTipText("No siblings assimilated.");
		panelSiblingMode.add(this.rdbtnSibNone);
		this.buttonGroupSib.add(this.rdbtnSibNone);

		this.rdbtnSibFull = new JRadioButton("Full");
		this.rdbtnSibFull.setToolTipText("Full siblings assimilated.");
		panelSiblingMode.add(this.rdbtnSibFull);
		this.rdbtnSibFull.setSelected(true);
		this.buttonGroupSib.add(this.rdbtnSibFull);

		this.rdbtnSibAll = new JRadioButton("All");
		this.rdbtnSibAll.setToolTipText("All siblings assimilated.");
		panelSiblingMode.add(this.rdbtnSibAll);
		this.buttonGroupSib.add(this.rdbtnSibAll);

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});

		this.btnRestoreDefaults = new JButton("Restore Defaults");
		this.btnRestoreDefaults.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Restore Defaults.
				setCriteria(new CensusCriteria());
			}
		});
		buttonPanel.add(this.btnRestoreDefaults);
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					//
					CensusCriteria criteria = getCriteria();

					//
					PuckGUI.instance().getPreferences().setDifferentialCensusCriteria(criteria);

					//
					Report report = CensusReporter.reportDifferentialCensus(netGUI.getSegmentation(), null, null, criteria);
					// Report report =
					// CensusReporter.reportFindCircuit(netGUI.getNet(),
					// criteria);

					netGUI.addReportTab(report);

					dispose();

				} catch (final PuckException exception) {
					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(DifferentialCensusInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);

		// /////////////////
		setCriteria(PuckGUI.instance().getPreferences().getDifferentialCensusCriteria());
	}

	/**
	 * 
	 * @return
	 */
	public CensusCriteria getCriteria() {
		CensusCriteria result;

		//
		result = new CensusCriteria();

		//
		result.setClosingRelation("TOTAL");

		//
		result.setPattern(this.txtfldPattern.getText());
		result.setFilter(this.txtfldFilter.getText());
		result.setClassificatoryLinking((String) this.cmbbxClassificatoryLinking.getSelectedItem());
		result.setIndividualPartitionLabel((String) this.cmbbxIndividualPartition.getSelectedItem());
		result.setChainClassification((String) this.cmbbxClassification.getSelectedItem());
		result.setCrossSexChainsOnly(this.chckbxCrossSex.isSelected());
		result.setMarriedOnly(this.chckbxMarriedOnly.isSelected());

		//
		CircuitType circuitType;
		if (this.rdbtnCircuitTypeCircuit.isSelected()) {
			circuitType = CircuitType.CIRCUIT;
		} else if (this.rdbtnCircuitTypeMinimal.isSelected()) {
			circuitType = CircuitType.MINIMAL;
		} else if (this.rdbtnCircuitTypeMinor.isSelected()) {
			circuitType = CircuitType.MINOR;
		} else if (this.rdbtnCircuitTypeRing.isSelected()) {
			circuitType = CircuitType.RING;
		} else {
			circuitType = null;
		}
		result.setCircuitType(circuitType);

		//
		FiliationType filiationType;
		if (this.rdbtnFiliationAgnatic.isSelected()) {
			filiationType = FiliationType.AGNATIC;
		} else if (this.rdbtnFiliationBilateral.isSelected()) {
			filiationType = FiliationType.BILINEAR;
		} else if (this.rdbtnFiliationCognatic.isSelected()) {
			filiationType = FiliationType.COGNATIC;
		} else if (this.rdbtnFiliationIdentity.isSelected()) {
			filiationType = FiliationType.IDENTITY;
		} else if (this.rdbtnFiliationUterine.isSelected()) {
			filiationType = FiliationType.UTERINE;
		} else {
			filiationType = null;
		}
		result.setFiliationType(filiationType);

		//
		result.setRestrictionType(RestrictionType.ALL);

		//
		SiblingMode siblingMode;
		if (this.rdbtnSibAll.isSelected()) {
			siblingMode = SiblingMode.ALL;
		} else if (this.rdbtnSibFull.isSelected()) {
			siblingMode = SiblingMode.FULL;
		} else if (this.rdbtnSibNone.isSelected()) {
			siblingMode = SiblingMode.NONE;
		} else {
			siblingMode = null;
		}
		result.setSiblingMode(siblingMode);

		//
		result.setSymmetryType(SymmetryType.INVERTIBLE);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public void setCriteria(final CensusCriteria source) {
		//
		if (source != null) {
			//
			this.txtfldPattern.setText(source.getPattern());
			this.txtfldFilter.setText(source.getFilter());

			//
			List<String> exogenousLabels = new ArrayList<String>(20);
			exogenousLabels.add("");
			exogenousLabels.addAll(IndividualValuator.getExogenousAttributeLabels(this.netGUI.getCurrentIndividuals()));

			this.cmbbxClassificatoryLinking.setModel(new DefaultComboBoxModel(exogenousLabels.toArray()));

			if (StringUtils.isBlank(source.getClassificatoryLinking())) {
				this.cmbbxClassificatoryLinking.setSelectedIndex(0);
			} else {
				int classificatoryLinkingLabelIndex = exogenousLabels.indexOf(source.getClassificatoryLinking());
				if (classificatoryLinkingLabelIndex == -1) {
					this.cmbbxClassificatoryLinking.setSelectedIndex(0);
				} else {
					this.cmbbxClassificatoryLinking.setSelectedIndex(classificatoryLinkingLabelIndex);
				}
			}

			//
			List<String> attributesLabels = new ArrayList<String>(20);
			attributesLabels.addAll(exogenousLabels);
			attributesLabels.addAll(IndividualValuator.getEndogenousAttributeLabels());
			Collections.sort(attributesLabels);

			this.cmbbxIndividualPartition.setModel(new DefaultComboBoxModel(attributesLabels.toArray()));

			if (StringUtils.isBlank(source.getIndividualPartitionLabel())) {
				this.cmbbxIndividualPartition.setSelectedIndex(0);
			} else if (exogenousLabels.contains(source.getIndividualPartitionLabel())) {
				this.cmbbxIndividualPartition.setSelectedIndex(exogenousLabels.indexOf(source.getIndividualPartitionLabel()));
			} else {
				exogenousLabels.add(0, source.getIndividualPartitionLabel());
				this.cmbbxIndividualPartition.setSelectedIndex(0);
			}

			//
			List<String> chainClassificationLabels = new ArrayList<String>();
			for (ChainProperty label : ChainProperty.values()) {
				chainClassificationLabels.add(label.toString());
			}
			Collections.sort(chainClassificationLabels);
			this.cmbbxClassification.setModel(new DefaultComboBoxModel(chainClassificationLabels.toArray()));

			int chainClassificationLabelIndex;
			if (StringUtils.isBlank(source.getChainClassification())) {
				chainClassificationLabelIndex = chainClassificationLabels.indexOf("SIMPLE");
			} else {
				chainClassificationLabelIndex = chainClassificationLabels.indexOf(source.getChainClassification());
				if (chainClassificationLabelIndex == -1) {
					chainClassificationLabelIndex = chainClassificationLabels.indexOf("SIMPLE");
				}
			}
			this.cmbbxClassification.setSelectedIndex(chainClassificationLabelIndex);

			//
			this.chckbxCrossSex.setSelected(source.isCrossSexChainsOnly());
			this.chckbxMarriedOnly.setSelected(source.isCouplesOnly());

			//
			this.rdbtnCircuitTypeCircuit.setSelected(false);
			this.rdbtnCircuitTypeMinimal.setSelected(false);
			this.rdbtnCircuitTypeMinor.setSelected(false);
			this.rdbtnCircuitTypeRing.setSelected(false);
			switch (source.getCircuitType()) {
				case CIRCUIT:
					this.rdbtnCircuitTypeCircuit.setSelected(true);
				break;
				case MINIMAL:
					this.rdbtnCircuitTypeMinimal.setSelected(true);
				break;
				case MINOR:
					this.rdbtnCircuitTypeMinor.setSelected(true);
				break;
				case RING:
					this.rdbtnCircuitTypeRing.setSelected(true);
				break;
			}

			//
			this.rdbtnFiliationAgnatic.setSelected(false);
			this.rdbtnFiliationBilateral.setSelected(false);
			this.rdbtnFiliationCognatic.setSelected(false);
			this.rdbtnFiliationIdentity.setSelected(false);
			this.rdbtnFiliationUterine.setSelected(false);
			switch (source.getFiliationType()) {
				case AGNATIC:
					this.rdbtnFiliationAgnatic.setSelected(true);
				break;
				case BILINEAR:
					this.rdbtnFiliationBilateral.setSelected(true);
				break;
				case COGNATIC:
					this.rdbtnFiliationCognatic.setSelected(true);
				break;
				case IDENTITY:
					this.rdbtnFiliationIdentity.setSelected(true);
				break;
				case UTERINE:
					this.rdbtnFiliationUterine.setSelected(true);
				break;
			}

			//
			this.rdbtnSibAll.setSelected(false);
			this.rdbtnSibFull.setSelected(false);
			this.rdbtnSibNone.setSelected(false);
			switch (source.getSiblingMode()) {
				case ALL:
					this.rdbtnSibAll.setSelected(true);
				break;
				case FULL:
					this.rdbtnSibFull.setSelected(true);
				break;
				case NONE:
					this.rdbtnSibNone.setSelected(true);
				break;
			}
		}
	}
}
