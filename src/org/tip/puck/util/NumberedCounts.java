package org.tip.puck.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author TIP
 */
public class NumberedCounts extends HashMap<Integer, Count> implements Iterable<Count> {

	private static final long serialVersionUID = -7177175882820921690L;

	/**
	 *
	 */
	public NumberedCounts() {
		super();
	}

	/**
	 *
	 */
	public NumberedCounts(final int initialCapacity) {
		super(initialCapacity);
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Count> iterator() {
		Iterator<Count> result;

		result = super.values().iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Count> toList() {
		List<Count> result;

		result = new ArrayList<Count>(values());

		//
		return result;
	}
}
