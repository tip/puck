package org.tip.puck.visualization.style.implementations;

import org.gephi.data.attributes.api.*;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.Node;
import org.gephi.layout.plugin.forceAtlas2.ForceAtlas2;
import org.gephi.preview.api.PreviewController;
import org.gephi.preview.api.PreviewModel;
import org.gephi.preview.api.PreviewProperty;
import org.gephi.project.api.Workspace;
import org.gephi.ranking.api.Ranking;
import org.gephi.ranking.api.RankingController;
import org.gephi.ranking.api.RankingModel;
import org.gephi.ranking.api.Transformer;
import org.gephi.ranking.plugin.transformer.AbstractSizeTransformer;
import org.openide.util.Lookup;
import org.tip.puck.graphs.Link;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.visualization.style.Style;
import org.tip.puck.visualization.style.attributes.AttributeMap;
import org.tip.puck.visualization.style.attributes.GraphStyle;

/**
 * @date May 13, 2012
 *
 * @author Edoardo Savoia
 */
public class CircuitIntersectionNetwork implements Style {

    public static final String NODE_POPULATION_PROP = "population";
    public static final String DEFAULT_LABEL = "Default label";
    public static final String EDGE_WEIGHT_PROP = "weight";
    public static final int NODE_MINSIZE = 10;
    public static final int NODE_MAXSIZE = 50;
    public static final int EDGE_MINSIZE = 1;
    public static final int EDGE_MAXSIZE = 10;

    /*
     * Alliance network and flow network:
     *
     * valued oriented graphs
     *
     * Vertex size according to population size,
     *
     * Arc widths according to weight value
     *
     */
    
    private final AttributeMap map = new AttributeMap() {

	@Override
	public void setAttributes(Node gephiNode, org.tip.puck.graphs.Node<?> graphNode) {
	    Cluster<?> referent = (Cluster<?>) graphNode.getReferent();
	    int size = referent.size();
	    gephiNode.getNodeData().setSize(10.0f);
	    gephiNode.getNodeData().getAttributes().setValue(NODE_POPULATION_PROP,size*1.0f);
	    String label = referent.getLabel();
	    if (label != null) {
		gephiNode.getNodeData().setLabel(label);
	    } else {
		gephiNode.getNodeData().setLabel(DEFAULT_LABEL);
	    }
    	}

	@Override
	public void setAttributes(Edge gephiEdge, Link<?> graphEdge) {
	    double weight = graphEdge.getWeight();
	    gephiEdge.setWeight((float)weight);
	}

	@Override
	public void registerAttributes(AttributeModel attributeModel) {
	    AttributeTable table = attributeModel.getEdgeTable();

	    table = attributeModel.getNodeTable();
	    table.addColumn(NODE_POPULATION_PROP.toLowerCase(),
		    NODE_POPULATION_PROP,
		    AttributeType.FLOAT,
		    AttributeOrigin.DATA,
		    1.0f);
	}
    };

    @Override
    public GraphStyle graphStyle() {
	return GraphStyle.undirected;
    }

    @Override
    public AttributeMap styleMapping() {
	return map;
    }

    private void transformGraph(Workspace workspace){
	//Rank node size by "population" attribute
	AttributeModel attributeModel = Lookup.getDefault().lookup(AttributeController.class).getModel(workspace);
	AttributeColumn populationColumn = attributeModel.getNodeTable().getColumn(NODE_POPULATION_PROP);

	RankingController rankingController = Lookup.getDefault().lookup(RankingController.class);
	RankingModel rankingModel = rankingController.getModel(workspace);
	Ranking populationRanking = rankingModel.getRanking(Ranking.NODE_ELEMENT, populationColumn.getId());
	AbstractSizeTransformer sizeTransformer = (AbstractSizeTransformer) rankingModel.getTransformer(Ranking.NODE_ELEMENT, Transformer.RENDERABLE_SIZE);

	sizeTransformer.setMinSize(NODE_MINSIZE);
	sizeTransformer.setMaxSize(NODE_MAXSIZE);

	rankingController.transform(populationRanking, sizeTransformer);

	//Rank edge size by "arc" attribute
	AttributeColumn arcColumn = attributeModel.getEdgeTable().getColumn(EDGE_WEIGHT_PROP);
	Ranking arcRanking = rankingModel.getRanking(Ranking.EDGE_ELEMENT, arcColumn.getId());

	// workaround
	// possible bug in RenderableSizeTransformerBuilder:
	// isTrasformableForElement() -> return true for NODE but not for EDGES
	AbstractSizeTransformer sizeETransformer = (AbstractSizeTransformer) rankingModel.getTransformer(Ranking.NODE_ELEMENT, Transformer.RENDERABLE_SIZE);
	sizeETransformer.setMinSize(EDGE_MINSIZE);
	sizeETransformer.setMaxSize(EDGE_MAXSIZE);
	rankingController.transform(arcRanking, sizeETransformer);

    }

    @Override
    public void applyStyle(Workspace workspace) {
	transformGraph(workspace);

	PreviewController previewController = Lookup.getDefault().lookup(PreviewController.class);
	PreviewModel previewModel = previewController.getModel(workspace);

	previewModel.getProperties().putValue(PreviewProperty.SHOW_NODE_LABELS, Boolean.TRUE);
	previewModel.getProperties().putValue(PreviewProperty.DIRECTED, Boolean.FALSE);
	previewModel.getProperties().putValue(PreviewProperty.EDGE_CURVED, Boolean.FALSE);
	previewController.refreshPreview(workspace);

	layout(workspace);
	
	
    }

    private void layout(Workspace workspace) {
	GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getModel(workspace);

	// ForceAtlas2 Layout
	ForceAtlas2 layout = new ForceAtlas2(null);
	layout.setGraphModel(graphModel);
	layout.resetPropertiesValues();

	layout.initAlgo();
	for (int i = 0; i < 50 && layout.canAlgo(); i++) {
	    layout.goAlgo();
	}
	layout.endAlgo();

	/*
	// Random Layout
	RandomLayout lay = new RandomLayout(null, 1000f);
	lay.setGraphModel(graphModel);
	lay.initAlgo();
	lay.goAlgo();
	lay.endAlgo();
	*/

	/*
	// Run YifanHuLayout for 100 passes - The layout always takes the current visible view
	YifanHuLayout layout = new YifanHuLayout(null, new StepDisplacement(1f));
	layout.setGraphModel(graphModel);
	layout.resetPropertiesValues();
	layout.setOptimalDistance(200f);

	layout.initAlgo();
	for (int i = 0; i < 50 && layout.canAlgo(); i++) {
	    layout.goAlgo();
	}
	layout.endAlgo();
	*/
	
    }
}
