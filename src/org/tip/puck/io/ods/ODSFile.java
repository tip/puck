package org.tip.puck.io.ods;

import java.io.BufferedReader;
import java.io.File;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.io.BARIURDetector;
import org.tip.puck.io.bar.BARODSFile;
import org.tip.puck.io.iur.IURODSFile;
import org.tip.puck.net.Net;
import org.tip.puck.util.PuckUtils;

import fr.devinsy.util.StringList;

/**
 * This class represents a ODS File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class ODSFile {

	private static final Logger logger = LoggerFactory.getLogger(ODSFile.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	public static final int MAX_LINE_SIZE = 2048;

	/**
	 * Loads a ODS file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file) throws PuckException {
		Net result;

		BARIURDetector.Format format = BARIURDetector.detectFormat(file);

		//
		switch (format) {
			case BAR:
				result = BARODSFile.load(file);
			break;

			case IUR:
				result = IURODSFile.load(file);
			break;

			case CONFLICTING:
				throw PuckExceptions.FORMAT_CONFLICT.create("BAR/IUR conflict.");

			case UNKNOWN:
				throw PuckExceptions.BAD_FILE_FORMAT.create("Unknown format.");

			default:
				result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @return
	 */
	public static List<StringList> readLineBlocks(final File file) {
		List<StringList> result;

		BufferedReader in = null;
		try {
			//
			in = new ODSBufferedReader(file);
			result = PuckUtils.readLineBlocks(in);

		} catch (Exception exception) {
			//
			exception.printStackTrace();
			result = null;

		} finally {
			//
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @return
	 */
	public static StringList readLines(final File file) {
		StringList result;

		BufferedReader in = null;
		try {
			//
			in = new ODSBufferedReader(file);
			result = PuckUtils.readLines(in);

		} catch (Exception exception) {
			//
			exception.printStackTrace();
			result = null;

		} finally {
			//
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File file, final Net source) throws PuckException {

		IURODSFile.save(file, source);
	}

}
