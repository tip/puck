package org.tip.puck.net.relations.roles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.GraphMaker;
import org.tip.puck.graphs.Link.LinkType;
import org.tip.puck.graphs.Node;
import org.tip.puck.graphs.Nodes;
import org.tip.puck.graphs.workers.GraphUtils;
import org.tip.puck.graphs.workers.GraphValuator;
import org.tip.puck.net.AlterAge;
import org.tip.puck.net.Gender;
import org.tip.puck.net.KinType;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.relations.Roles;
import org.tip.puck.net.relations.roles.RoleActorPair.Adjustable;
import org.tip.puck.net.relations.roles.RoleRelationMaker.RoleRelationRule;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Partition;
import org.tip.puck.util.Value;

import fr.devinsy.util.StringList;

public class RoleRelationWorker {
	
	public enum CousinClassification {
		
		BIFURCATE_MERGING,
		BIFURCATE_COLLATERAL,
		GENERATIONAL,
		LINEAL,
		UNDEFINED;
		
	}

	private RoleRelations relations;
	private Partition<RoleActors> chainsPartition;
	private int maxIterations;
	private Roles generators;
	private Roles terms;
	private Partition<Role> termsByEgoGender;
	private Partition<Role> termsByGender;
	private Map<Role,List<Integer>> generations;
	private Map<Integer,Integer[]> termCountsByGenderationAndGender;
	private String title;
	
	private Map<Role,Roles> reciprocalRoles;
	private Roles autoReciprocalRoles;
	private Map<KinType,Roles> recursiveRoles;
	private Map<Role,StringList> positions;
	private Map<Role,Map<Role,Roles>> compositions;
	private Map<Role,Partition<Role>> basicCompositions;
	private Map<Role,RoleDefinitions> roleDefininitionsByRole;
	private Map<MetaRole,List<List<Role>>>[] collateralTerms;
	private Map<MetaRole,CousinClassification>[] collateralClassifications;
	private CousinClassification[] collateralClassification;
	private List<RoleRelationRule> rules;
	
	private Graph<Role>[] roleRelationGraphs;
	private Map<Value,Graph<Role>>[] roleRelationSubGraphs;
	
	public RoleRelationWorker(final RelationModel model, final int maxIterations){
		
		this.relations = model.getRoleRelations();
		this.terms = relations.getRoles();
		this.title = model.getName();
		this.maxIterations = maxIterations;
		this.chainsPartition = RoleRelationWorker.expand(relations,relations.getGenderConfigs(), maxIterations);
		this.generators = relations.getGenerators();
		this.rules = model.getRules();
	}

	private static Partition<RoleActors> expand (RoleRelations relations, Map<Role,Roles> genderConfigs, int maxIterations){
		Partition<RoleActors> result;
		
		result = new Partition<RoleActors>();
		
		for (Gender egoGender : Gender.values()){
			RoleActor self = relations.getSelf(egoGender);
			RoleRelationWorker.expand(result, relations,genderConfigs,self,new RoleActors(),0,maxIterations);
		}
		//
		return result;
	}

	private static void expand (Partition<RoleActors> positions, RoleRelations relations, Map<Role,Roles> genderConfigs, RoleActor self, RoleActors selfChain, int iterations, int maxIterations){
		
		for (KinType kinType : KinType.basicTypesWithSiblings()){
		
			for (RoleActor alter : relations.getAlters(self, kinType, null)){
				
				if (RoleRelationMaker.concatenable(self,alter)){
				
					RoleActors alterChain = selfChain.clone();
					
					if (alter.getEgoGender()!=self.getEgoGender()){
						
						RoleActor adjustedSelf = relations.getSelf(alter.getEgoGender());
						
						for (RoleActor actor : alterChain){
							
							RoleActorPair pair = new RoleActorPair(relations,adjustedSelf,actor,Adjustable.NONE);
							
							if (pair.isValid()){
								
								RoleActor adjustedActor = pair.getAlter();
								actor.setRole(adjustedActor.getRole()); // Seems to be unused code, check! 
								adjustedSelf = adjustedActor;
	
							} else {
								
								adjustedSelf = null;
								break;
							}
						}
						if (adjustedSelf == null){
							continue;
						}
					}
	
					if (!alter.hasSelfName()){
	
						alterChain.add(alter);
	
						if (positions.put(alterChain, new Value(alter.getIndividual())) && iterations<maxIterations){

							expand(positions,relations,genderConfigs,alter,alterChain,iterations+1,maxIterations);
						}
					}
				}
			}
		}
	}
	
	Map<Role,StringList> getPositions(){
		
		if (positions == null){
			
			positions = new TreeMap<Role,StringList>();
			
			for (Cluster<RoleActors> chainsCluster : chainsPartition.getClusters().toListSortedByValue()){

				StringList chainStrings = new StringList();
				positions.put(chainsCluster.getValue().roleValue(), chainStrings);

				List<RoleActors> chains = chainsCluster.getItems();
				Collections.sort(chains,new RoleActorsComparator());
				
				for (RoleActors chain : chains){
					
					String chainString = chain.toLetters();
					if (!chainStrings.contains(chainString)){
						chainStrings.add(chainString);
					}
				}
			}
		}
		//
		return positions;
	}

	Map<Role,Roles> getReciprocalRoles(){

		if (reciprocalRoles == null){
			
			reciprocalRoles = new TreeMap<Role,Roles>();
			autoReciprocalRoles = new Roles();
			
			for (Cluster<RoleActors> cluster : chainsPartition.getClusters().toListSortedByValue()){
				
				Role role = cluster.getValue().roleValue();
				Roles roles = new Roles();
				reciprocalRoles.put(role, roles);
						
				boolean cross = RoleActor.isUniqueCross(relations.getGenderConfigs().get(role));
				
				for (Value value : getReciprocals(relations,role,chainsPartition).getValues()){
					
					Role reciprocalRole = value.roleValue();
					roles.add(reciprocalRole);

					if (reciprocalRole.equals(role) && !cross){
						autoReciprocalRoles.add(role);
					}
				}
			}
		}
		//
		return reciprocalRoles;
	}
	
	Map<Role,Partition<Role>> getBasicCompositions (){
		
		if (basicCompositions == null){
			
			basicCompositions = new TreeMap<Role,Partition<Role>>();
			
			for (RoleRelation relation : relations.toSortedList()){
				
				RoleActor self = relation.getSelf();
				Role selfTerm = self.getIndividual();
				
				Partition<Role> partition = basicCompositions.get(selfTerm);
				if (partition==null){
					partition = new Partition<Role>();
					basicCompositions.put(selfTerm, partition);
				}
				
				for (RoleActor actor : relation.getActors()){
					
					if (!actor.isSelf()){

						for (Role metaRole : generators){

							if (((MetaRole)metaRole).specifies(actor.getRole())){
								
								if (!actor.hasSelfName() || (((MetaRole)metaRole).getEgoGender()).matchs(((MetaRole)metaRole).getAlterGender())){
									
									partition.put(actor.getIndividual(), new Value(metaRole));
								}
							}
						}
					}
				}
			}
		}
		
		//
		return basicCompositions;
	}
	
	public Roles getAutoReciprocalRoles() {
		
		if (autoReciprocalRoles == null){
			getReciprocalRoles();
		}
		//
		return autoReciprocalRoles;
	}
	
	
	private static Partition<RoleActors> getReciprocals(final RoleRelations relations, final Role alpha, final Partition<RoleActors> chainsMap){
		Partition<RoleActors> result;
		
		result = new Partition<RoleActors>();
		
		for (RoleActors chain : chainsMap.getCluster(new Value(alpha)).getItems()){
			
			RoleActors inverseChain = new RoleActors();

			for (int i=chain.size()-1;i>-1;i--){
				RoleActor alter = chain.get(i);
				RoleActor self = relations.getSelf(chain.getFirst().getEgoGender());
				if (i>0){
					self = chain.get(i-1);
				}
				inverseChain.add(self.asReciprocalOf(alter,relations));
			}
			

			List<RoleActors> startChains = new ArrayList<RoleActors>();
			RoleActors root = new RoleActors();
			RoleActor reciprocalSelf = relations.getSelf(chain.getLast().getAlterGender());
			root.add(reciprocalSelf);
			startChains.add(root);

			for (RoleActors endChain : relations.adjustChain(inverseChain, startChains)){
												
				result.put(endChain, new Value(endChain.getLast().getIndividual()));
			}
		}
		//
		return result;
	}
	
	private static Partition<RoleActors> getCompositions(final RoleRelations relations, final Role alpha, final Role beta, final Partition<RoleActors> chainsMap){
		Partition<RoleActors> result;
		
		result = new Partition<RoleActors>();
		
		for (RoleActors alphaActors : chainsMap.getCluster(new Value(alpha)).getItems()){
			
			for (RoleActors betaActors : chainsMap.getCluster(new Value(beta)).getItems()){
												
				if (alphaActors.getLast().getAlterGender().matchs(betaActors.getLast().getEgoGender()) && !alphaActors.getLast().hasSelfName() && !betaActors.getLast().hasSelfName()){

					List<RoleActors> startChains = new ArrayList<RoleActors>();
					startChains.add(alphaActors.clone());
					
					for (RoleActors chain : relations.adjustChain(betaActors, startChains)){
						
						result.put(chain, new Value(chain.getLast().getIndividual()));
					}
				}
			}
		}	
		//
		return result;
	}
	
	Map<Role,RoleDefinitions> getRoleDefininitionsByRole(){
		
		if (roleDefininitionsByRole == null){
			
			roleDefininitionsByRole = new TreeMap<Role,RoleDefinitions>();
			
			for (RoleDefinition definition : relations.getDefinitions()){
				
				RoleDefinitions definitions = roleDefininitionsByRole.get(definition.getAlterTerm());
				if (definitions==null){
					definitions = new RoleDefinitions();
					roleDefininitionsByRole.put(definition.getAlterTerm(),definitions);
				}
				definitions.add(definition);
			}
		}
		//
		return roleDefininitionsByRole;
	}
	
	
	Map<Role,Map<Role,Roles>> getCompositions() {
		
		if (compositions == null){
			
			compositions = new TreeMap<Role,Map<Role,Roles>>();
		}

		for (Cluster<RoleActors> alphaCluster : chainsPartition.getClusters().toListSortedByValue()){
			
			Role alpha = alphaCluster.getValue().roleValue();
			Map<Role,Roles> map = new TreeMap<Role,Roles>();
			compositions.put(alpha, map);
			
			for (Cluster<RoleActors> betaCluster : chainsPartition.getClusters().toListSortedByValue()){
				
				Role beta = betaCluster.getValue().roleValue();
				Roles roles = new Roles();
				map.put(beta, roles);

				for (Value gammaValue : getCompositions(relations,alpha,beta,chainsPartition).getValues()){
					
					roles.add(gammaValue.roleValue());
				}
			}
		}
						
		//
		return compositions;
	}
	
	public CousinClassification[] getCollateralClassification(){
		
		if (collateralClassification==null){
			getCollateralTerms();
		}
		//
		return collateralClassification;
	}
	
	Map<MetaRole,CousinClassification>[] getCollateralClassifications(){
		
		if (collateralClassifications==null){
			getCollateralTerms();
		}
		//
		return collateralClassifications;
	}
	
	Map<MetaRole,List<List<Role>>>[] getCollateralTerms (){
		
		if (collateralTerms == null) {
			
			collateralTerms = new TreeMap[2];
			collateralClassifications = new TreeMap[2];
			collateralClassification = new CousinClassification[2];
			
			for (int i=0;i<2;i++){
				collateralTerms[i] = new TreeMap<MetaRole,List<List<Role>>>();
				collateralClassifications[i] = new TreeMap<MetaRole,CousinClassification>();
			}

			RoleActors parents = new RoleActors();
			RoleActors parallelAscendants = new RoleActors();
			RoleActors crossAscendants = new RoleActors();

			RoleActors siblings = new RoleActors();
			RoleActors parallelCousins = new RoleActors();
			RoleActors crossCousins = new RoleActors();
			
			for (Gender egoGender : Gender.values()){
				RoleActor self = relations.getSelf(egoGender);
				
				siblings.addAll(relations.getAlters(self, KinType.SIBLING, null));

				for (RoleActor parent : relations.getAlters(self, KinType.PARENT, null)){
					parents.add(parent);
					for (RoleActor parentSibling : relations.getAlters(parent, KinType.SIBLING, null)){
						RoleActors cousins = relations.getAlters(parentSibling, KinType.CHILD,null);
						if (parent.getAlterGender()==parentSibling.getAlterGender()){
							parallelAscendants.add(parentSibling);
							for (RoleActor parallelCousin : cousins){
								if (!parallelCousin.hasSelfName()){
									parallelCousins.add(parallelCousin);
								}
							}
						} else if (parent.getAlterGender()==parentSibling.getAlterGender().invert()){
							crossAscendants.add(parentSibling);
							crossCousins.addAll(cousins);
						}
					}
				}
			}
			
			boolean noEgoGenderDistinction = !relations.hasEgoGenderDistinction();
			boolean noAlterGenderDistinction = !relations.hasAlterGenderDistinction();
			boolean noAlterAgeDistinction = !relations.hasAlterAgeDistinction();
			
			for (Gender egoGender : Gender.values(noEgoGenderDistinction)){
				for (Gender alterGender : Gender.values(noAlterGenderDistinction)){
					for (AlterAge alterAge : AlterAge.values(noAlterAgeDistinction)){
						
						MetaRole config = new MetaRole(null,egoGender,alterGender,alterAge);

						List<List<Role>>[] thisTerms = new List[2];
						
						for (int i=0;i<2;i++){
							thisTerms[i] = new ArrayList<List<Role>>();
						}
						
						thisTerms[0].add(parents.getByImpliedAbsoluteRole(config).getIndividuals().toSortedList());
						thisTerms[0].add(parallelAscendants.getByImpliedAbsoluteRole(config).getIndividuals().toSortedList());
						thisTerms[0].add(crossAscendants.getByImpliedAbsoluteRole(config).getIndividuals().toSortedList());

						thisTerms[1].add(siblings.getByImpliedAbsoluteRole(config).getIndividuals().toSortedList());
						thisTerms[1].add(parallelCousins.getByImpliedAbsoluteRole(config).getIndividuals().toSortedList());
						thisTerms[1].add(crossCousins.getByImpliedAbsoluteRole(config).getIndividuals().toSortedList());
						
/*						Boolean merging = null;
						Boolean lineal = null;
						
						if (!siblingTerms.isEmpty() && siblingTerms.equals(parallelCousinTerms)){
							merging = true;
						} else if (!siblingTerms.isEmpty() || !parallelCousinTerms.isEmpty()){
							merging = false;
						} 
						
						if (!crossCousinTerms.isEmpty() && crossCousinTerms.equals(parallelCousinTerms)){
							lineal = true;
						} else if (!crossCousinTerms.isEmpty() || !parallelCousinTerms.isEmpty()){
							lineal = false;
						}
						
						CousinClassification type = null;
						
						if (lineal!=null && lineal){
							if (merging!=null && merging){
								type = CousinClassification.GENERATIONAL;
							} else {
								type = CousinClassification.LINEAL;
							}
						} else {
							if (merging!=null && merging){
								type = CousinClassification.BIFURCATE_MERGING;
							} else {
								type = CousinClassification.BIFURCATE_COLLATERAL;
							}
						}*/
						
						CousinClassification[] thisCousinClassification = new CousinClassification[2];
						for (int i=0;i<2;i++){
							
							thisCousinClassification[i] = getTermClassification(thisTerms[i]);
							if (thisCousinClassification[i]!=null){
								
								collateralClassifications[i].put(config, thisCousinClassification[i]);
								collateralTerms[i].put(config,thisTerms[i]);
								
								if (collateralClassification[i]==null || collateralClassification[i].equals(thisCousinClassification[i])){
									collateralClassification[i] = thisCousinClassification[i];
								} else {
									collateralClassification[i] = CousinClassification.UNDEFINED;
								}
							}
						}
					}
				}
			}
		}

		//
		return collateralTerms;
	}
	
	public String getRecursiveRolePattern(){
		String result;
		
		result = "";
		
		for (KinType kinType : getRecursiveRoles().keySet()){
			if (kinType!=KinType.UNKNOWN && !getRecursiveRoles().get(kinType).isEmpty()){
				String sep = ";";
				if (result.equals("")){
					sep = "";
				}
				result += sep+getRecursiveRoles().get(kinType).size()+" "+kinType;
			}
		}
		
		result = getRecursiveRoles().get(KinType.UNKNOWN).size()+" ("+result+")";
		
		//
		return result;
	}
	
	Map<KinType,Roles> getRecursiveRoles (){
		
		if (recursiveRoles == null){
			
			recursiveRoles = new TreeMap<KinType,Roles>();
			for (KinType kinType : KinType.basicTypesWithSiblings()){
				recursiveRoles.put(kinType, new Roles());
			}
			recursiveRoles.put(KinType.UNKNOWN, new Roles());
			
			for (RoleRelation relation : relations){
				
				Role self = relation.getSelfTerm();
				if (!self.hasName(relations.getSelfName())){
					
					for (RoleActor actor : relation.getActors()){
						if (!actor.isSelf() && self.equals(actor.getIndividual())){
							recursiveRoles.get(actor.getRole().getKinType()).addNew(self);
							recursiveRoles.get(KinType.UNKNOWN).addNew(self);
						}
					}
				}
			}
		}
		
		//
		return recursiveRoles;
	}
	
	private static CousinClassification getTermClassification (List<List<Role>> terms){
		CousinClassification result;
		
		result = null;
		
		List<Role> linealTerms = terms.get(0);
		List<Role>  parallelTerms = terms.get(1);
		List<Role>  crossTerms = terms.get(2);
		
		if (!linealTerms.isEmpty() || !parallelTerms.isEmpty() || !crossTerms.isEmpty()){
			
			Boolean merging = null;
			Boolean lineal = null;
			
			if (!linealTerms.isEmpty() && linealTerms.equals(parallelTerms)){
				merging = true;
			} else if (!linealTerms.isEmpty() || !parallelTerms.isEmpty()){
				merging = false;
			} 
			
			if (!crossTerms.isEmpty() && crossTerms.equals(parallelTerms)){
				lineal = true;
			} else if (!crossTerms.isEmpty() || !parallelTerms.isEmpty()){
				lineal = false;
			}
			
			if (lineal!=null && lineal){
				if (merging!=null && merging){
					result = CousinClassification.GENERATIONAL;
				} else {
					result = CousinClassification.LINEAL;
				}
			} else {
				if (merging!=null && merging){
					result = CousinClassification.BIFURCATE_MERGING;
				} else {
					result = CousinClassification.BIFURCATE_COLLATERAL;
				}
			}
		}

		//
		return result;
	}
	
	private static void countGenerations(final Map<Role,List<Integer>> counts, final RoleRelations relations, final RoleActor self, final int selfCount, final int iterations, final int maxIterations){
		
		for (KinType kinType : KinType.basicTypesWithSiblings()){
			
			int alterCount = selfCount + kinType.genDistance();
						
			for (RoleActor alter : relations.getAlters(self, kinType, null)){
				
				Role role = alter.getIndividual();
				List<Integer> count = counts.get(role);
				if (count==null){
					count = new ArrayList<Integer>();
					counts.put(role, count);
				}
				if (!count.contains(alterCount)){
					count.add(alterCount);
				}
				if (iterations<maxIterations){
					countGenerations(counts,relations,alter,alterCount,iterations+1,maxIterations);
				}
			}
		}

	}
	
	Map<Role,List<Integer>> getGenerations (){

		if (generations == null){
			
			generations = new TreeMap<Role,List<Integer>>();
			
			for (Gender egoGender : Gender.values()){
				countGenerations(generations,relations,relations.getSelf(egoGender),0,0,maxIterations);
			}
			
			for (List<Integer> list : generations.values()){
				Collections.sort(list);
			}
		}
		//
		return generations;
	}
	
	List<Integer> getGenerationLevels(){
		List<Integer> result;
		
		result = new ArrayList<Integer>(getTermCountsByGenerationAndGender().keySet());
		Collections.sort(result,Collections.reverseOrder());
		
		//
		return result;
	}
	
	public String[] getGenerationPatterns(){
		String[] result;
		
		result = new String[]{"","","",""};
		
		for (Integer genLevel : getGenerationLevels()){
			for (int i=0;i<4;i++){
				if (genLevel==0){
					result[i]+="| ";
				}
				result[i]+= getTermCountsByGenerationAndGender().get(genLevel)[i]+" ";
				if (genLevel==0){
					result[i]+="| ";
				}
			}
		}
		//
		return result;
	}
	
	Map<Integer,Integer[]> getTermCountsByGenerationAndGender(){
		
		if (termCountsByGenderationAndGender == null){
			
			termCountsByGenderationAndGender = new TreeMap<Integer,Integer[]>();

			for (Role role : getTerms()){
				
				Integer gender = getGender(role).toInt();
				for (Integer generation : getGenerations(role)){
					
					Integer[] byGender = termCountsByGenderationAndGender.get(generation);
					if (byGender==null){
						byGender = new Integer[]{0,0,0,0};
						termCountsByGenderationAndGender.put(generation, byGender);
					}
					byGender[gender]++;
					byGender[3]++;
				}
			}
		}
		//
		return termCountsByGenderationAndGender;
	}
	
	
	Gender getEgoGender(Role role){
		Gender result;
		
		result = null;
		
		if (relations.getGenderConfigs().get(role)!=null){
			for (Role metaRole : relations.getGenderConfigs().get(role)){
				
				Gender gender = ((MetaRole)metaRole).getEgoGender();
				if (result==null){
					result = gender;
					break;
				} else if (result!=gender){
					result = Gender.UNKNOWN;
				}
			}
		}
		
		//
		return result;
	}
	
	Gender getGender(Role role){
		Gender result;
		
		result = null;
		
		if (relations.getGenderConfigs().get(role)!=null){
			for (Role metaRole : relations.getGenderConfigs().get(role)){
				
				Gender gender = ((MetaRole)metaRole).getAlterGender();
				if (result==null){
					result = gender;
					break;
				} else if (result!=gender){
					result = Gender.UNKNOWN;
				}
			}
		}

		//
		return result;
	}
	
	public List<Integer> getGenerations(Role role){
		List<Integer> result;
		
		result = getGenerations().get(role);
		
		if (result==null){
			result = new ArrayList<Integer>();
		}
		
		//
		return result;
	}
	
	void setGenderPartitions(){

		termsByEgoGender = new Partition<Role>();
		termsByGender = new Partition<Role>();
		
		for (Role role : getTerms()){

			termsByEgoGender.put(role,new Value(getEgoGender(role)));
			termsByGender.put(role,new Value(getGender(role)));
		}
	}
	
	public Roles getTermsByGender(Gender gender){
		Roles result;
		
		result = getTermsByExclusiveGender(gender);
		result.addAll(getTermsByExclusiveGender(Gender.UNKNOWN));
		//
		return result;
	}
	
    public Roles getTermsByExclusiveGender(Gender gender){
		Roles result;
		
		result = new Roles();
		
		if (termsByGender==null){
			setGenderPartitions();
		}
		if (termsByGender.getCluster(new Value(gender))!=null){
			result.addAll(termsByGender.getCluster(new Value(gender)).getItems());
		}
		
		//
		return result;
	}
	
	public Roles getTermsByEgoGender(Gender gender){
		Roles result;
		
		result = getTermsByExclusiveEgoGender(gender);
		result.addAll(getTermsByExclusiveEgoGender(Gender.UNKNOWN));
		//
		return result;
	}
	
	public Roles getTermsByExclusiveEgoGender(Gender gender){
		Roles result;
		
		result = new Roles();
		
		if (termsByEgoGender==null){
			setGenderPartitions();
		}
		if (termsByEgoGender.getCluster(new Value(gender))!=null){
			result.addAll(termsByEgoGender.getCluster(new Value(gender)).getItems());
		}
		
		//
		return result;
	}
	
	
	public RoleRelations getRelations() {
		return relations;
	}

	Partition<RoleActors> getChainsPartition() {
		return chainsPartition;
	}

	public int getMaxIterations() {
		return maxIterations;
	}
	
	public Roles getTerms(){
		return terms;
	}
	
	Roles getGenerators() {
		return generators;
	}

	public String getTitle() {
		return title;
	}
	
/*	private RoleActor getSelf(Role role, Gender egoGender, Map<Role,MetaRole> genderConfigs){
		RoleActor actor;
		
		actor = new RoleActor(role,new MetaRole("SELF",egoGender,getGender(role),AlterAge.UNKNOWN),relations.getSelfName());
		
		if (actor.hasSelfName()){
			
			actor.setAlterGender(egoGender);
			
		} else if (actor.getAlterGender().isUnknown() && genderConfigs.get(role)!=null && genderConfigs.get(role).isCross()){
			
			actor.getRole().setCross(true);
		}
		
		//
		return actor;
	}*/
	
	public Map<RoleActor,RoleActors> getRoleRelationMap(Gender egoGender) {
		Map<RoleActor,RoleActors> result;

		result = new TreeMap<RoleActor,RoleActors>();
		
		if (egoGender==null){
			
			for (Gender gender : Gender.valuesNotUnknown()){
				
				Map<RoleActor,RoleActors> map = getRoleRelationMap(gender);
								
				for (RoleActor actor : map.keySet()){

					RoleActor newActor = actor.clone();

					if (newActor.hasSelfName()){
						
/*						if (gender.isUnknown()) {
							continue;
						}*/
						
						newActor.setIndividual(new Role(actor.getName()+" "+gender));
					}

					RoleActors actors = map.get(actor);
					RoleActors newActors = new RoleActors();

					if (actors!=null){
						
						for (RoleActor alterActor : actors.toList()){
							
							RoleActor newAlterActor = alterActor.clone();
							
							if (newAlterActor.hasSelfName()){
								
								newAlterActor.setIndividual(new Role(alterActor.getName()+" "+gender));
							}
							
							newActors.add(newAlterActor);
						}
					}
					
					result.put(newActor, newActors);
				}
			}
						
		} else {
			
			for (RoleRelation relation : relations.toSortedList()){
				
				RoleActor self = relation.getSelf();

				if (egoGender.specifies(self.getEgoGender())){

					RoleActor genderedSelf = self.withEgoGender(egoGender);

					if (!self.hasSelfName()){
						
						genderedSelf.setAlterGender(RoleActor.getUniqueAlterGender(relations.getGenderConfigs().get(self.getIndividual()), null, egoGender));
					
					}
						
					RoleActors products = result.get(genderedSelf);
					if (products == null){
						products = new RoleActors();
						result.put(genderedSelf, products);
					}
					
					for (RoleActor actor : relation.getActors().toListSortedByRoles()){
						
						RoleActor genderedActor = actor.withEgoGender(egoGender);
						
						if (genderedActor.hasSelfName() && egoGender.isUnknown()){
							
							continue;
						}

						if (!genderedActor.isSelf() && genderedActor.hasCorrectGender(genderedSelf, relations)){
							
							products.addNew(genderedActor);
						}
					}
					
				}
			}
			
/*			for (RoleActor elementaryActor : relations.getElementaryAlters()){
				
				if (!isRepresented(elementaryActor,result.keySet())){
					
					result.put(elementaryActor.asSelf(relations).withEgoGender(egoGender), new RoleActors());
				}
			}*/
	
		}
		

		
		// Remove empty terms except generators
/*		for (RoleActor self : new ArrayList<RoleActor>(result.keySet())){
			
			if (result.get(self).isEmpty() && !relations.hasElementaryName(self)){
				
				result.remove(self);
			}
		}*/
		//
		return result;
	}
	
	private static boolean isRepresented (RoleActor actor, Collection<RoleActor> actors){
		boolean result;
		
		result = false;
		
		for (RoleActor other : actors){
			if (other.hasName(actor.getName())){
				result = true;
				break;
			}
		}
		//
		return result;
	}
	
	private void setRoleRelationGraphNodes(Graph<Role> graph, Map<RoleActor,RoleActors> roleRelationMap) {
		
		for (RoleActor self : new ArrayList<RoleActor>(roleRelationMap.keySet())) {
//			System.out.println("->"+self);
			Node<Role> node = graph.addNode(self.getIndividual());
			
			if (node.getAttributeValue("GENDER")!=null){
				String value = node.getAttributeValue("GENDER");
				
				if (!value.equals(self.getAlterGender().toString())){
					
					if (value.contains("CROSS")){
						
						if (self.getRole().isCross()){
							
							if (!value.contains(self.getAlterGender().toString())){
								node.setAttribute("GENDER", "CROSS-UNKNOWN");
							}
							
						} else {
							
							if (value.contains(self.getAlterGender().toString())){
								
								node.setAttribute("GENDER", self.getAlterGender().toString());
								
							} else {
								
								node.setAttribute("GENDER", "UNKNOWN");
							}
						}
						
					} else if (!value.equals(self.getAlterGender().toString())){
						
						if (value.equals("UNKNOWN")){
							
							node.setAttribute("GENDER", self.getAlterGender().toString());
							
						} else {
							
							node.setAttribute("GENDER", "X");
						}
						
					}
				}
				
//				System.out.println("Overwrite "+value+" "+self+" -> "+node.getAttributeValue("GENDER"));
				
			} else if (self.getRole().isCross()){

				node.setAttribute("GENDER", "CROSS-"+self.getAlterGender().toString());
//				System.out.println("Cross "+self+" -> "+node.getAttributeValue("GENDER"));

			} else {

				node.setAttribute("GENDER", self.getAlterGender().toString());
//				System.out.println("Gendered "+self+" -> "+node.getAttributeValue("GENDER"));
			}
		}
		
		for (Node<Role> node : graph.getNodes()){
			if (node.getAttributeValue("GENDER")!=null && node.getAttributeValue("GENDER").equals("X")){
				node.setAttribute("GENDER", "UNKNOWN");
			}
		}
	}
	
	public List<Graph<Role>> getRoleRelationGraphsGendered(Graph<Role> source){
		List<Graph<Role>> result;
		
		result = new ArrayList<Graph<Role>>();
		
		Nodes<Role> maleNodes = new Nodes<Role>();
		Nodes<Role> femaleNodes = new Nodes<Role>();
		Nodes<Role> maleNodesCross = new Nodes<Role>();
		Nodes<Role> femaleNodesCross = new Nodes<Role>();
		
		List<Nodes<Role>> nodesList = new ArrayList<Nodes<Role>>();
		nodesList.add(maleNodes);
		nodesList.add(femaleNodes);
		nodesList.add(maleNodesCross);
		nodesList.add(femaleNodesCross);
		
		String[] labels = new String[]{"MALE","FEMALE","CROSS-MALE","CROSS-FEMALE"};
		
		for (Node<Role> node : source.getNodes()){
			
			String gender = node.getAttributeValue("GENDER");
			
			if (gender.equals("MALE") || gender.equals("UNKNOWN")) {
				
				maleNodes.add(node);
			} 
			
			if (gender.equals("FEMALE") || gender.equals("UNKNOWN")) {
				
				femaleNodes.add(node);
			} 
			
			if (gender.equals("CROSS-MALE") || gender.equals("CROSS-UNKNOWN")) {
				
				maleNodesCross.add(node);
			} 
			
			if (gender.equals("CROSS-FEMALE") || gender.equals("CROSS-UNKNOWN")) {
			
				femaleNodesCross.add(node);
			}
		}
		
		int idx = 0;
		
		for (Nodes<Role> nodes : nodesList){
			
			result.add(GraphMaker.extractSubgraph(source, nodes.toListSortedById(), labels[idx]));
			idx++;
			
		}
		
		//
		return result;
	}
	
	public Graph<Role> getRoleRelationGraphInternal(LinkType type) {
		Graph<Role> result;
			
		result = new Graph<Role>();
		result.setLabel(getTitle()+" Same-Sex");
		
		Map<RoleActor,RoleActors> map = getRoleRelationMap(null);
		setRoleRelationGraphNodes(result,map);
		
		for (RoleActor self : new ArrayList<RoleActor>(map.keySet())) {
			
			RoleActors alters = map.get(self);
			
			if (alters!=null){

				for (RoleActor alter : alters.toListSortedByRoles()){
					
					if (self.getAlterGender().matchs(alter.getAlterGender()) && self.getEgoGender().matchs(alter.getEgoGender()) && self.getAlterGender().matchs(self.getEgoGender()) && alter.getAlterGender().matchs(alter.getEgoGender())){

						if (type==null || alter.getRole().getLinkType()==type){
							
							if (!alter.isParent()){
								
								result.addLink(self.getIndividual(), alter.getIndividual(), alter.getRole().getLinkType(), 1.,alter.getRole().getName());
							
							} else {
								
								result.addLink(alter.getIndividual(), self.getIndividual(), alter.getRole().getLinkType(), 1.,alter.getRole().invertName());

							}
						}
					}
				}				
			}
		}
		
		//
		result.renumberNodesByLabel();

		//
		return result;
	}

	public Graph<Role> getRoleRelationGraph(Gender egoGender) {
		Graph<Role> result;
			
		result = new Graph<Role>();
		if (egoGender!=null){
			
			result.setLabel(getTitle()+" "+egoGender.toSpeakerString());
		
		} else {
		
			result.setLabel(getTitle()+" Total");
		}
		
		Map<RoleActor,RoleActors> map = getRoleRelationMap(egoGender);
		setRoleRelationGraphNodes(result,map);

		for (RoleActor self : new ArrayList<RoleActor>(map.keySet())) {
			
			RoleActors alters = map.get(self);
			
			if (alters!=null){

				for (RoleActor alter : alters.toListSortedByRoles()){
					
					if (!alter.isParent()){
						
						result.addLink(self.getIndividual(), alter.getIndividual(), alter.getRole().getLinkType(), 1.,alter.getRole().getName());
					
					} else {
						
						result.addLink(alter.getIndividual(), self.getIndividual(), alter.getRole().getLinkType(), 1.,alter.getRole().invertName());

					}
				}

			}
						
		}
		
		//
		result.renumberNodesByLabel();
		
		//
		return result;
	}
	
/*	public Graph<Role> getRoleRelationGraph1(Gender egoGender) {
		Graph<Role> roleRelationGraph;
			
		roleRelationGraph = new Graph<Role>();
		roleRelationGraph.setLabel(getTitle()+" "+egoGender.toSpeakerString());
		
		Map<Role,MetaRole> genderConfigs = RoleRelationMaker.getUniqueGenderConfigs(relations);
		
		Node<Role> ego = roleRelationGraph.addNode(new Role(relations.getSelfName()));
		ego.setAttribute("GENDER", egoGender.toString());
	
		for (Role role : getTerms()) {
			Gender gender = getGender(role);
			Node<Role> node = roleRelationGraph.addNode(role);
			if (gender.isUnknown() && genderConfigs.get(role)!=null && genderConfigs.get(role).isCross()){
				node.setAttribute("GENDER", "CROSS");
			} else {
				node.setAttribute("GENDER", gender.toString());
			}
		}
		
		for (RoleRelation relation : relations.toSortedList()){
			
			Role self = relation.getSelfTerm();
			
			for (RoleActor actor : relation.getActors().toListSortedByRoles()){
				
				Role alter = actor.getIndividual();
				MetaRole metaRole = actor.getRole();
				
				if (egoGender.specifies(metaRole.getEgoGender()) && !metaRole.isSelf() && !metaRole.isParent()){
					
					roleRelationGraph.addLink(self, alter, metaRole.getLinkType(), 1.,metaRole.getName());
				}
			}
		}
		//
		return roleRelationGraph;
	}*/
	
	public Graph<Role>[] getRoleRelationGraphs() {
		
		if (roleRelationGraphs==null){
			
			roleRelationGraphs = new Graph[2];
			for (int i=0;i<2;i++){
				roleRelationGraphs[i] = getRoleRelationGraph(Gender.valueOf(i));
			}
		}
		//
		return roleRelationGraphs;
	}

	
	public Map<Value,Graph<Role>>[] getRoleRelationSubGraphs() {
		
		if (roleRelationSubGraphs==null){
			
			roleRelationSubGraphs = new TreeMap[2];
			for (int i=0;i<2;i++){
				roleRelationSubGraphs[i] = GraphMaker.createSubgraphsByLineTags(getRoleRelationGraph(Gender.valueOf(i)));
				roleRelationSubGraphs[i].put(new Value(KinType.UNKNOWN), getRoleRelationGraphs()[i]);
			}
		}
		//
		return roleRelationSubGraphs;
	}

	
	private Graph<Role> getGraph (KinType kinType, Gender egoGender){
		Graph<Role> result;
		
		if (kinType == KinType.UNKNOWN){
			
			result = getRoleRelationGraphs()[egoGender.toInt()];
			
		} else {
			
			result = getRoleRelationSubGraphs()[egoGender.toInt()].get(new Value(kinType.toString()));
		}

		//
		return result;
	}
	
	private Value getGraphStatistics (KinType kinType, Gender egoGender, String indicator){
		Value result;
		
		result = GraphValuator.get(getGraph(kinType,egoGender), indicator);
		
		//
		return result;
	}
	
	public Value getGraphStatistics (Gender egoGender, String indicator){
		Value result;
		
		result = GraphValuator.get(getRoleRelationGraphs()[egoGender.toInt()], indicator);
		
		//
		return result;
	}
	
	public StringList getNetworkAnalysis (){
		StringList result;
		
		result = new StringList();
		
/*		List<String> indicators = Arrays.asList(new String[]{"SIZE","NRCOMPONENTS","CONCENTRATION","DENSITY","MEANDEGREE","MEANCLUSTERINGCOEFF"});
		
		String headLine = "Network\tEgo Gender";
		for (String indicator : indicators){
			headLine += "\t"+indicator;
		}
		result.appendln(headLine);
		
		List<KinType> kinTypes = Arrays.asList(new KinType[]{KinType.UNKNOWN,KinType.CHILD,KinType.SIBLING,KinType.SPOUSE});
		
		for (KinType kinType : kinTypes){
			
			String line = kinType+"";
			if (kinType == KinType.UNKNOWN){
				line = "TOTAL";
			}
			
			for (Gender egoGender : Gender.valuesNotUnknown()){
				
				line +="\t"+egoGender;
				
				for (String indicator : indicators){
					
					line += "\t"+getGraphStatistics(kinType,egoGender,indicator);
				}
				//
				result.appendln(line);
				line = "";
			}
		}*/
		
		List<Graph<Role>> genderedGraphs = getRoleRelationGraphsGendered(getRoleRelationGraphInternal(LinkType.ARC));
		
		List<String> indicators2 = Arrays.asList(new String[]{"SIZE","NODES","NRLINES","NRCOMPONENTS","CONCENTRATION","DENSITY","MEANDEGREE","MAXDEPTH","MAXDEPTH_NORM","MEANDEPTH","MEANDEPTH_NORM","CYCLERANK","NRCYCLIC","NRCYCLIC_NORM","CYCLIC","NRLOOPS","LOOPS","NRTOPS","TOPS","NRBOTTOMS","BOTTOMS","NRISOLATES","ISOLATES","NRDOWNFORKS","DOWNFORKS","NRUPFORKS","UPFORKS"});
		
		Map<String,Map<String,String>> qualiMaps = new HashMap<String,Map<String,String>>();
		Map<String,Map<String,Value>> quantiMaps = new HashMap<String,Map<String,Value>>();
		
		String line = "";
				
		for (String indicator1 : indicators2){
			
			line += "\t"+indicator1;
		}
		//
		result.appendln(line);
		result.appendln();
		
		for (Graph<Role> genderedGraph : genderedGraphs){
			
			line = genderedGraph.getLabel();

			Map<String,String> qualiMap = new TreeMap<String,String>();
			Map<String,Value> quantiMap = new TreeMap<String,Value>();

			if (!genderedGraph.getLabel().contains("CROSS")){
				qualiMaps.put(genderedGraph.getLabel(), qualiMap);
				quantiMaps.put(genderedGraph.getLabel(), quantiMap);
			}
			
			for (String indicator1 : indicators2){
				
				Value value = GraphValuator.get(genderedGraph,indicator1);
				quantiMap.put(indicator1, value);
				
				line += "\t"+value;
				
				if (indicator1.equals("CONCENTRATION")){
					
					if (value.doubleValue()==100.) {
						qualiMap.put(indicator1, "COHESIVE");
					} else {
						qualiMap.put(indicator1, "DISPERSED");
					}
					
				} else if (indicator1.equals("NRLOOPS")){

					if (value.intValue()>0) {
						qualiMap.put(indicator1, "LOOPS");
					} else {
						qualiMap.put(indicator1, "NOLOOPS");
					}
					
				} else if (indicator1.equals("NRDOWNFORKS")){

					if (value.intValue()>1) {
						qualiMap.put(indicator1, "MULTI-COLLATERAL");
					} else if (value.intValue()==1) {
						qualiMap.put(indicator1, "UNI-COLLATERAL");
					} else {
						qualiMap.put(indicator1, "NON-COLLATERAL");
					}
					
				} else if (indicator1.equals("NRUPFORKS")){

					if (value.intValue()>1) {
						qualiMap.put(indicator1, "MULTI-AFFINAL");
					} else if (value.intValue()==1) {
						qualiMap.put(indicator1, "UNI-AFFINAL");
					} else {
						qualiMap.put(indicator1, "NON-AFFINAL");
					}
					
				} else if (indicator1.equals("NRTOPS")){

					if (value.intValue()>1) {
						qualiMap.put(indicator1, "MULTI-APICAL");
					} else if (value.intValue()==1) {
						qualiMap.put(indicator1, "UNI-APICAL");
					} else {
						qualiMap.put(indicator1, "CIRCULAR");
					}
					
				} else if (indicator1.equals("NRBOTTOMS")){

					if (value.intValue()>1) {
						qualiMap.put(indicator1, "MULTI-BASAL");
					} else if (value.intValue()==1) {
						qualiMap.put(indicator1, "UNI-BASAL");
					} else {
						qualiMap.put(indicator1, "CIRCULAR");
					}
					
				} else if (indicator1.equals("NRCYCLIC_NORM")){

					if (value.doubleValue()==100.) {
						qualiMap.put(indicator1, "CYCLIC");
					} else if (value.intValue()==0.) {
						qualiMap.put(indicator1, "ACYCLIC");
					} else {
						qualiMap.put(indicator1, "NON-ACYCLIC");
					}
					
				} else if (indicator1.equals("MEANDEGREE_NORM")){

					if (value.doubleValue()==100.) {
						qualiMap.put(indicator1, "VERTICAL");
					} else if (value.intValue()==0.) {
						qualiMap.put(indicator1, "HORIZONTAL");
					} else if (value.intValue()>50.) {
						qualiMap.put(indicator1, "VERTICAL BIAS");
					} else {
						qualiMap.put(indicator1, "HORIZONTAL BIAS");
					}
					
				}
			}
			//
			result.appendln(line);
			
			if (!genderedGraph.getLabel().contains("CROSS")){
				
				int i=1;
				for (Graph<Role> component : GraphUtils.componentGraphs(genderedGraph)){
					
					line = genderedGraph.getLabel()+"_"+i;
					
					for (String indicator : indicators2){
						
						if (indicator.equals("NRCOMPONENTS") || indicator.equals("CONCENTRATION")){
							line += "\t";
						} else {
							Value value = GraphValuator.get(component,indicator); 
							line += "\t"+value;

							if ((indicator.equals("NRTOPS") || indicator.equals("NRBOTTOMS")) && value.intValue()==0 && GraphValuator.get(component,"NRISOLATES").intValue()!=GraphValuator.get(component,"SIZE").intValue()){
								String oldValue = qualiMap.get(indicator);
								if (oldValue!=null && !oldValue.contains("CIRCULAR")){
									qualiMap.put(indicator, oldValue+"-CIRCULAR");
								}
							}
							
						}
					}
					//
					result.appendln(line);
					i++;
				}
				//
				result.appendln("");
			}
		}
		result.appendln();
		
		result.appendln("Summary");
		result.appendln();

		for (String indicator : qualiMaps.get("MALE").keySet()){
			String maleLine = indicator+"\t"+qualiMaps.get("MALE").get(indicator);
			String femaleLine = indicator+"\t"+qualiMaps.get("FEMALE").get(indicator);
			if (!maleLine.equals(femaleLine)){
				result.appendln(maleLine += " / "+qualiMaps.get("FEMALE").get(indicator));
			} else {
				result.appendln(maleLine);
			}
		}
		
		String symmetry = "SYMMETRIC";
		
		for (String indicator : quantiMaps.get("MALE").keySet()){
			if (!quantiMaps.get("MALE").get(indicator).equals(quantiMaps.get("FEMALE").get(indicator)) && !quantiMaps.get("MALE").get(indicator).isList()){
				symmetry = "ASYMMETRIC";
				break;
			}
		}
		result.append("SYMMETRY\t"+symmetry);
		
		
		//
		return result;
	}

	public List<RoleRelationRule> getRules() {
		return rules;
	}
	
	
	public Roles getEmicGenerator (RoleDefinition definition){
		Roles result;
		
		RoleActor actor = definition.getAlter();
		
		result = relations.getEmicRoles(actor.getRole(), actor.getEgoGender(), actor.getAlterGender(), actor.getAlterAge());
		
		//
		return result;
	}
		
	
}
