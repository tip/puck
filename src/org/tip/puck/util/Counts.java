package org.tip.puck.util;

import java.util.ArrayList;

/**
 * 
 * @author TIP
 */
public class Counts extends ArrayList<Count> {

	private static final long serialVersionUID = 9219190603510343073L;

	/**
	 * 
	 */
	public Counts() {
		super();
	}

	/**
	 * 
	 * @param capacity
	 */
	public Counts(final int capacity) {
		super(capacity);

		// Fill array.
		while (capacity >= this.size()) {
			this.add(new Count());
		}
	}

	/**
	 * 
	 * @return
	 */
	public double average() {
		double result;

		//
		double sum = 0;
		int count = 0;
		for (Count value : this) {
			sum += value.get();
			count += 1;
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum * 1.0 / count;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Count averageCount() {
		Count result;

		//
		result = new Count();
		int count = 0;
		for (Count value : this) {
			result.add(value.get());
			result.addMin(value.getMin());
			result.addMax(value.getMax());
			count += 1;
		}

		//
		if (count != 0) {
			result.set(result.get() * 1.0 / count);
			result.setMin(result.getMin() * 1.0 / count);
			result.setMax(result.getMax() * 1.0 / count);
		}

		//
		return result;
	}

	/**
	 * @return
	 * 
	 */
	@Override
	public Count get(final int index) {
		Count result;

		// Fill array.
		while (index >= this.size()) {
			this.add(new Count());
		}

		//
		result = super.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double max() {
		double result;

		if (this.size() == 0) {
			result = 0;
		} else {
			result = Double.MIN_VALUE;
			for (Count value : this) {
				if (value.get() > result) {
					result = value.get();
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double maxAverage() {
		double result;

		//
		double sum = 0;
		int count = 0;
		for (Count value : this) {
			sum += value.getMax();
			count += 1;
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum * 1.0 / count;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Count maxCount() {
		Count result;

		result = new Count(Double.MIN_VALUE, Double.MIN_VALUE, Double.MIN_VALUE);
		for (Count value : this) {
			if (value.get() > result.get()) {
				result.set(value.get());
			}
			if (value.getMin() > result.getMin()) {
				result.setMin(value.get());
			}
			if (value.getMax() > result.getMax()) {
				result.setMax(value.getMax());
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double maxMax() {
		double result;

		if (this.size() == 0) {
			result = 0;
		} else {
			result = Double.MIN_VALUE;
			for (Count value : this) {
				if (value.getMax() > result) {
					result = value.getMax();
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double min() {
		double result;

		if (this.size() == 0) {
			result = 0;
		} else {
			result = Double.MAX_VALUE;
			for (Count value : this) {
				if (value.get() < result) {
					result = value.get();
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double minAverage() {
		double result;

		//
		double sum = 0;
		int count = 0;
		for (Count value : this) {
			sum += value.getMin();
			count += 1;
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum * 1.0 / count;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Count minCount() {
		Count result;

		result = new Count(Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);
		for (Count value : this) {
			if (value.get() < result.get()) {
				result.set(value.get());
			}
			if (value.getMin() < result.getMin()) {
				result.setMin(value.getMin());
			}
			if (value.getMax() < result.getMax()) {
				result.setMax(value.getMax());
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double minMin() {
		double result;

		if (this.size() == 0) {
			result = 0;
		} else {
			result = Double.MAX_VALUE;
			for (Count value : this) {
				if (value.getMin() < result) {
					result = value.getMin();
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double sum() {
		double result;

		//
		result = 0;
		for (Count value : this) {
			result += value.get();
		}

		//
		return result;
	}

}
