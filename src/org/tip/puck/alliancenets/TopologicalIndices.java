package org.tip.puck.alliancenets;


/**
 * @author Telmo Menezes
 *
 */
public class TopologicalIndices {
    private double[][] matrix;
    private double[][] matrixExo;
    private int m;
    private double n;
    private double nExo;
    
    // topological indices
    private double endogamousPercentage;
    private double networkConcentration;
    private double endogamicNetworkConcentration;
    private double networkSymmetry;
   
    private double parallels;
    private double crosses;
   
    private double loopTri;
    private double tranTri;
    
    public TopologicalIndices(AllianceNet allianceNet) {
        m = allianceNet.getNodeCount();
        n = 0;
        nExo = 0;
            
        matrix = new double[m][m];
        matrixExo = new double[m][m];
        
        // extract matrix from net
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                Alliance alliance = allianceNet.getEdge(allianceNet.getNodes().get(i), allianceNet.getNodes().get(j));
                double x = 0;
                if (alliance != null) {
                    x = alliance.getWeight();
                    n += x;
                    if (i != j) {
                        nExo += x;
                    }
                }
                matrix[i][j] = x;
                if (i != j) {
                    matrixExo[i][j] = x;
                }
            }
        }
        
        // normalize matrices
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                matrix[i][j] /= n;
                matrixExo[i][j] /= nExo;
            }
        }
        
        // calc topological indices
        calcEndogamousPercentage();
        calcNetworkConcentration();
        calcEndogamicNetworkConcentration();
        calcNetworkSymmetry();
        
        calcParallelsAndCrosses(allianceNet);
        
        // triads
        calcTriadicCounts();
        
        // release matrix
        matrix = null;
    }
    
    private void calcEndogamousPercentage() {
        endogamousPercentage = 0;
        for (int i = 0; i < m; i++) {
            endogamousPercentage += matrix[i][i];
        }
    }
    
    private void calcNetworkConcentration() {
        networkConcentration = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                networkConcentration += matrix[i][j] * matrix[i][j];
            }
        }
    }
    
    private void calcEndogamicNetworkConcentration() {
        endogamicNetworkConcentration = 0;
        
        if (endogamousPercentage == 0.0)
            return;
        
        double endogamicN = endogamousPercentage;
        for (int i = 0; i < m; i++) {
            double x = matrix[i][i] / endogamicN;
            endogamicNetworkConcentration += x * x;
        }
    }
    
    private void calcNetworkSymmetry() {
        networkSymmetry = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                networkSymmetry += matrix[i][j] * matrix[j][i];
            }
        }
        networkSymmetry /= networkConcentration;
    }
    
    private void calcTriadicCounts() {
        double[][] matrix2 = new double[m][m];
        
        // x ^ 2
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                matrix2[i][j] = 0;
                for (int k = 0; k < m; k++) {
                    matrix2[i][j] += matrixExo[i][k] * matrixExo[k][j];
                }
            }
        }
        
        // loop triads
        loopTri = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                loopTri += matrixExo[j][i] * matrix2[i][j];
            }
        }
        loopTri *= nExo * nExo * nExo;
        loopTri /= 3.0;
        
        // transitive triadas
        tranTri = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                tranTri += matrixExo[i][j] * matrix2[i][j];
            }
        }
        tranTri *= nExo * nExo * nExo;
        tranTri /= 3.0;
    }
    
    private void calcParallelsAndCrosses(AllianceNet allianceNet) {
        parallels = 0.0;
        crosses = 0.0;
        
        for (Alliance alliance : allianceNet.getEdges()) {
            if (alliance.getWeight() > 1.0) {
                parallels += alliance.getWeight();
            }
            if (allianceNet.edgeExists(alliance.getTarget(), alliance.getOrigin())) {
                double w1 = alliance.getWeight();
                double w2 = allianceNet.getInverseEdge(alliance).getWeight();
                if (w1 > w2) {
                    crosses += w2;
                }
                else {
                    crosses += w1;
                }
            }
        }
    }
    
    public double distance(TopologicalIndices ti) {
        double small1 = 0.0001;
        double small2 = 1.0;
        
        double r1 = 0;
        double r2 = 0;
        double r3 = 0;
        double r4 = 0;
        double r5 = 0;
        double r6 = 0;
        double r7 = 0;
        double r8 = 0;
        
        if ((endogamousPercentage > 0) && (ti.getEndogamousPercentage() > 0)) {
        	r1 = Math.abs(Math.log(endogamousPercentage / ti.getEndogamousPercentage()));
        }
        else {
            r1 = Math.abs(Math.log((endogamousPercentage + small1) / (ti.getEndogamousPercentage() + small1)));
        }
        if ((networkConcentration > 0) && (ti.getNetworkConcentration() > 0)) {
            r2 = Math.abs(Math.log(networkConcentration / ti.getNetworkConcentration()));
        }
        else {
            r1 = Math.abs(Math.log((networkConcentration + small1) / (ti.getNetworkConcentration() + small1)));
        }
        if ((endogamicNetworkConcentration > 0) && (ti.getEndogamicNetworkConcentration() > 0)) {
            r3 = Math.abs(Math.log(endogamicNetworkConcentration / ti.getEndogamicNetworkConcentration()));
        }
        else {
            r1 = Math.abs(Math.log((endogamicNetworkConcentration + small1) / (ti.getEndogamicNetworkConcentration() + small1)));
        }
        if ((networkSymmetry > 0) && (ti.getNetworkSymmetry() > 0)) {
            r4 = Math.abs(Math.log(networkSymmetry / ti.getNetworkSymmetry()));
        }
        else {
            r1 = Math.abs(Math.log((networkSymmetry + small1) / (ti.getNetworkSymmetry() + small1)));
        }
        if ((parallels > 0) && (ti.getParallels() > 0)) {
            r5 = Math.abs(Math.log(parallels / ti.getParallels()));
        }
        else {
            r1 = Math.abs(Math.log((parallels + small2) / (ti.getParallels() + small2)));
        }
        if ((crosses > 0) && (ti.getCrosses() > 0)) {
            r6 = Math.abs(Math.log(crosses / ti.getCrosses()));
        }
        else {
            r1 = Math.abs(Math.log((crosses + small2) / (ti.getCrosses() + small2)));
        }
        if ((loopTri > 0) && (ti.getLoopTri() > 0)) {
            r7 = Math.abs(Math.log(loopTri / ti.getLoopTri()));
        }
        else {
            r1 = Math.abs(Math.log((loopTri + small2) / (ti.getLoopTri() + small2)));
        }
        if ((tranTri > 0) && (ti.getTranTri() > 0)) {
            r8 = Math.abs(Math.log(tranTri / ti.getTranTri()));
        }
        else {
            r1 = Math.abs(Math.log((tranTri + small2) / (ti.getTranTri() + small2)));
        }
        
        return (r1 + r2 + r3 + r4 + r5 + r6 + r7 + r8) / 8.0;
    }

    public double getEndogamousPercentage() {
        return endogamousPercentage;
    }

    public double getNetworkConcentration() {
        return networkConcentration;
    }

    public double getEndogamicNetworkConcentration() {
        return endogamicNetworkConcentration;
    }

    public double getNetworkSymmetry() {
        return networkSymmetry;
    }

    @Override
    public String toString() {
        return "TopologicalIndices [m=" + m + ", n=" + n
                + ", endogamousPercentage=" + endogamousPercentage
                + ", networkConcentration=" + networkConcentration
                + ", endogamicNetworkConcentration=" + endogamicNetworkConcentration
                + ", networkSymmetry=" + networkSymmetry
                + ", parallels=" + parallels
                + ", crosses=" + crosses
                + ", loopTri=" + loopTri
                + ", tranTri=" + tranTri
                + "]";
    }
    
    public double getN() {
        return n;
    }
    
    public double getLoopTri() {
        return loopTri;
    }

    public double getParallels() {
        return parallels;
    }

    public double getCrosses() {
        return crosses;
    }

    public double getTranTri() {
        return tranTri;
    }
    
    public static void main(String[] args) {
        AllianceNetFile mf = new AllianceNetFile();
        AllianceNet allianceNet = mf.load("datAllLinks/Ancien_Regime_DYNASTIES.dat");
        TopologicalIndices ti = new TopologicalIndices(allianceNet);
        System.out.println(ti);
    }
}