/**
 * Copyright 2012 Christian P. MOMON (christian.momon@devinsy.fr).
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Adaptations by TIP.
 * 
 */
package org.tip.puckgui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;

/**
 * This class represents a File reader and writer for recent files management.
 * 
 * Note: in file, first entry is the older.
 * 
 * @author Christian P. MOMON
 * @author TIP
 */
public class RecentFilesFile {

	private static final Logger logger = LoggerFactory.getLogger(RecentFilesFile.class);

	/**
	 * Loads.
	 * 
	 * @param file
	 *            file to load.
	 * 
	 * @return the loaded data.
	 * 
	 * @throws PuckException
	 */
	public static RecentFiles load(final File file) throws PuckException {
		RecentFiles result;

		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
			result = read(in);

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException exception) {
				logger.warn("Not managed error.");
				exception.printStackTrace();
			}
		}

		//
		return result;
	}

	/**
	 * Reads.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return
	 * 
	 * @throws PuckException
	 */
	public static RecentFiles read(final BufferedReader in) throws PuckException {
		RecentFiles result;

		try {
			//
			result = new RecentFiles();

			//
			boolean ended = false;
			while (!ended) {
				//
				String line = in.readLine();

				if (line == null) {
					ended = true;
				} else {
					result.update(new File(line));
				}
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading line.");
		}

		//
		return result;
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File file, final RecentFiles source) throws PuckException {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));

			write(out, source);

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param net
	 *            Source.
	 */
	public static void write(final PrintWriter out, final RecentFiles source) {

		for (File file : source.toReverseArray()) {
			out.println(file.getAbsolutePath());
		}
	}
}
