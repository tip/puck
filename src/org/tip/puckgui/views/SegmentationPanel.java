package org.tip.puckgui.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.tip.puck.PuckException;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.util.Labels;
import org.tip.puckgui.NetGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class SegmentationPanel extends JPanel {
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.tip.puckgui.messages"); //$NON-NLS-1$
	private static final long serialVersionUID = -1123800488357812371L;
	private NetGUI netGUI;
	private JComboBox comboBoxPartition;
	private JComboBox comboBoxCluster;
	private boolean isComboboxNeutralized;

	/**
	 * Create the panel.
	 */
	public SegmentationPanel(final NetGUI netGUI) {
		//
		this.netGUI = netGUI;

		// Set the default combobox index cause unwanted select event. To
		// neutralize it, a toggle variable.
		this.isComboboxNeutralized = false;

		// //////////////
		setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(140dlu;default)"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(125dlu;default)"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, }));

		JLabel label = new JLabel("Partition:");
		add(label, "2, 2, right, default");

		this.comboBoxPartition = new JComboBox();
		this.comboBoxPartition.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Partition selection.
				try {
					if (!SegmentationPanel.this.isComboboxNeutralized) {
						//
						netGUI.changeSegmentationToPartition(SegmentationPanel.this.comboBoxPartition.getSelectedIndex());
					}
				} catch (final PuckException exception) {
					exception.printStackTrace();
					// TODO
				}
			}
		});
		this.comboBoxPartition.setMaximumRowCount(15);
		this.comboBoxPartition.setModel(new DefaultComboBoxModel(new String[] { "ALL" }));
		add(this.comboBoxPartition, "4, 2, fill, default");

		JLabel lblCluster = new JLabel("Cluster:");
		add(lblCluster, "6, 2, right, default");

		this.comboBoxCluster = new JComboBox();
		this.comboBoxCluster.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Cluster selection.
				try {
					if (!SegmentationPanel.this.isComboboxNeutralized) {
						netGUI.changeSegmentationToCluster(SegmentationPanel.this.comboBoxCluster.getSelectedIndex());
					}
				} catch (final PuckException exception) {
					exception.printStackTrace();
					// TODO
				}
			}
		});
		this.comboBoxCluster.setMaximumRowCount(15);
		this.comboBoxCluster.setModel(new DefaultComboBoxModel(new String[] { "ALL" }));
		add(this.comboBoxCluster, "8, 2, fill, default");

		JButton btnClear = new JButton("");
		btnClear.setToolTipText(BUNDLE.getString("SegmentationPanel.btnClear.toolTipText")); //$NON-NLS-1$
		btnClear.setIcon(new ImageIcon(SegmentationPanel.class.getResource("/org/tip/puckgui/images/edit-clear-16x16.png")));
		btnClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Button Clear.
				try {
					netGUI.changeSegmentationToClear();
				} catch (final PuckException exception) {
					exception.printStackTrace();
					// TODO
				}
			}
		});
		add(btnClear, "10, 2");

		JButton btnAdd = new JButton("");
		btnAdd.setToolTipText(BUNDLE.getString("SegmentationPanel.btnAdd.toolTipText")); //$NON-NLS-1$
		btnAdd.setIcon(new ImageIcon(SegmentationPanel.class.getResource("/org/tip/puckgui/images/add-16x16.png")));
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Button Add.
				try {
					List<Labels> modelsLabels = Segmentation.buildModelLabels(netGUI.getCurrentIndividuals(), netGUI.getCurrentFamilies(), netGUI.getNet()
							.relationModels(), netGUI.getNet().relations());

					PartitionCriteria criteria = PartitionCriteriaDialog.showDialog(modelsLabels);
					if (criteria != null) {
						netGUI.changeSegmentationToNew(criteria);
					}
				} catch (final Exception exception) {
					//
					exception.printStackTrace();

					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JButton btnDown = new JButton("");
		btnDown.setToolTipText(BUNDLE.getString("SegmentationPanel.btnDown.toolTipText")); //$NON-NLS-1$
		btnDown.setIcon(new ImageIcon(SegmentationPanel.class.getResource("/org/tip/puckgui/images/down-16x16.png")));
		btnDown.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Button Up.
				try {
					netGUI.changeSegmentationToDown();
				} catch (final PuckException exception) {
					exception.printStackTrace();
					// TODO
				}
			}
		});

		JButton btnUp = new JButton("");
		btnUp.setToolTipText(BUNDLE.getString("SegmentationPanel.btnUp.toolTipText")); //$NON-NLS-1$
		btnUp.setIcon(new ImageIcon(SegmentationPanel.class.getResource("/org/tip/puckgui/images/up-16x16.png")));
		btnUp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Button Up.
				try {
					netGUI.changeSegmentationToUp();
				} catch (final PuckException exception) {
					exception.printStackTrace();
					// TODO
				}
			}
		});

		JButton btnRemove = new JButton("");
		btnRemove.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Remove current segment.
				try {
					netGUI.changeSegmentationToPop();
				} catch (final PuckException exception) {
					exception.printStackTrace();
					// TODO
				}
			}
		});
		btnRemove.setIcon(new ImageIcon(SegmentationPanel.class.getResource("/org/tip/puckgui/images/remove-16x16.png")));
		btnRemove.setToolTipText(BUNDLE.getString("SegmentationPanel.btnRemove.toolTipText")); //$NON-NLS-1$
		add(btnRemove, "12, 2");
		add(btnUp, "14, 2");
		add(btnDown, "16, 2");
		add(btnAdd, "18, 2");

		//
		update();
	}

	/**
	 * 
	 */
	public void update() {
		if (this.netGUI != null) {
			//
			Segmentation segmentation = this.netGUI.getSegmentation();

			//
			this.comboBoxPartition.setModel(new DefaultComboBoxModel(segmentation.getSegmentLabels().toArray()));
			this.isComboboxNeutralized = true;
			this.comboBoxPartition.setSelectedIndex(segmentation.getCurrentSegmentIndex());
			this.isComboboxNeutralized = false;

			//
			this.comboBoxCluster.setModel(new DefaultComboBoxModel(segmentation.getCurrentClusterLabels().toArray()));
			this.isComboboxNeutralized = true;
			this.comboBoxCluster.setSelectedIndex(segmentation.getCurrentClusterIndex());
			this.isComboboxNeutralized = false;
		}
	}
}
