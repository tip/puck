package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.relations.Roles;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.models.RolesModel;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class RelationModelInputWindow extends JFrame {
	
	private static final long serialVersionUID = 491254829638915415L;
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.tip.puckgui.messages"); //$NON-NLS-1$
	private static final Logger logger = LoggerFactory.getLogger(RelationModelInputWindow.class);

	private JFrame thisJFrame;
	private RelationModel relationModel;
	private JPanel contentPane;
	private JLabel lblName;
	private JTextField txtfldName;
	private JPanel panel;
	private JScrollPane scrollPane;
	private JTable roleTable;
	private JPopupMenu popupMenu;
	private JMenuItem mntmAddRole;
	private JMenuItem mntmRemoveRole;
	private Component verticalStrut;

	/**
	 * Create the frame.
	 */
	public RelationModelInputWindow(final NetGUI netGUI, final RelationModel source) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(FooReporterInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		this.relationModel = source;
		setTitle(BUNDLE.getString("RelationModelInputWindow.title")); //$NON-NLS-1$
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 395, 271);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		this.panel = new JPanel();
		this.contentPane.add(this.panel, BorderLayout.CENTER);
		this.panel.setLayout(new BoxLayout(this.panel, BoxLayout.Y_AXIS));

		JPanel inputPanel = new JPanel();
		this.panel.add(inputPanel);
		inputPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));

		this.lblName = new JLabel(BUNDLE.getString("RelationModelInputWindow.lblName.text")); //$NON-NLS-1$
		inputPanel.add(this.lblName, "2, 2, right, default");

		this.txtfldName = new JTextField();
		this.txtfldName.setText("");
		inputPanel.add(this.txtfldName, "4, 2, fill, default");
		this.txtfldName.setColumns(10);

		this.verticalStrut = Box.createVerticalStrut(20);
		this.panel.add(this.verticalStrut);

		this.scrollPane = new JScrollPane();
		this.panel.add(this.scrollPane);

		this.roleTable = new JTable();
		this.scrollPane.setViewportView(this.roleTable);
		this.roleTable.setModel(new RolesModel(null));

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton(BUNDLE.getString("RelationModelInputWindow.btnCancel.text")); //$NON-NLS-1$
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton(BUNDLE.getString("RelationModelInputWindow.btnSave.text")); //$NON-NLS-1$
		getRootPane().setDefaultButton(btnLaunch);
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Save.
				try {
					if (StringUtils.isBlank(RelationModelInputWindow.this.txtfldName.getText())) {
						JOptionPane
								.showMessageDialog(RelationModelInputWindow.this.thisJFrame, "Please, enter a name.", "Bad Value", JOptionPane.ERROR_MESSAGE);
					} else {
						// Manage the creation case.
						if (RelationModelInputWindow.this.relationModel == null) {
							RelationModelInputWindow.this.relationModel = netGUI.getNet().createRelationModel(
									RelationModelInputWindow.this.txtfldName.getText());

							//
							netGUI.addRelationTab(RelationModelInputWindow.this.relationModel);
						}

						//
						RelationModelInputWindow.this.relationModel.setName(RelationModelInputWindow.this.txtfldName.getText());

						//
						Role[] sourceRoles = RelationModelInputWindow.this.relationModel.roles().toArray();
						Roles targetRoles = ((RolesModel) RelationModelInputWindow.this.roleTable.getModel()).getTarget();
						for (int roleIndex = 0; roleIndex < targetRoles.size(); roleIndex++) {
							//
							Role targetRole = targetRoles.get(roleIndex);

							//
							if (roleIndex < sourceRoles.length) {
								Role sourceRole = sourceRoles[roleIndex];

								if (StringUtils.isBlank(targetRole.getName())) {
									// Remove an existing role.
									netGUI.getNet().removeRelationRole(RelationModelInputWindow.this.relationModel, sourceRole);
								} else {
									// Update an existing role.
									sourceRole.setName(targetRole.getName());
									sourceRole.setDefaultCardinality(targetRole.getDefaultCardinality());
								}
							} else if ((StringUtils.isNotBlank(targetRole.getName()))
									&& (!RelationModelInputWindow.this.relationModel.roles().exists(targetRole.getName()))) {
								// Create a role.
								RelationModelInputWindow.this.relationModel.roles().add(targetRole);
							}
						}

						//
						netGUI.setChanged(true);

						//
						netGUI.updateAll();

						//
						dispose();
					}
				} catch (final Exception exception) {
					//
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(RelationModelInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);

		//
		setSource(this.relationModel);
	}

	/**
	 * 
	 * @param source
	 */
	private void setSource(final RelationModel source) {
		if (source != null) {
			this.txtfldName.setText(source.getName());
			this.roleTable.setModel(new RolesModel(source.roles()));
		}
	}

	/**
	 * 
	 * @param component
	 * @param popup
	 */
	private static void addPopup(final Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			@Override
			public void mouseReleased(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(final MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
