/**
 * 
 */
package org.tip.puck.generators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.statistics.StatisticsWorker;
import org.tip.puck.util.RandomGenerator;

/**
 * @author Telmo Menezes
 * @author Klaus Hamberger
 * 
 */
public class PGraph1 {
	private Map<String, PGraphNode> marriages;
	private Map<Individual, Set<PGraphNode>> marriagesByIndividual;
	private Vector<PGraphLink> linksVector;

	private int totalLinks;
	
	private SiblingMarriageDistribution smDistrib;
	
	private int getProbableNumberOfSpouses(Gender gender, int k){
		return smDistrib.howManySpouses(gender, k);
	}
	
	public Net toNet (){
		Net result;
		
		Map<PGraphNode,Family> index = new HashMap<PGraphNode,Family>();
		
		result = new Net();
		
		//extract Individuals
		for (Individual individual: marriagesByIndividual.keySet()){
			result.individuals().add(individual.clone());
		}
		
		//extract PersonalFamilies
		int id = 1;
		for (PGraphNode marriage: marriages.values()){
			Individual husband = null;
			if (marriage.getHusband()!=null){
				husband = result.get(marriage.getHusband().getId());
			}
			Individual wife = null;
			if (marriage.getWife()!=null){
				wife = result.get(marriage.getWife().getId());
			}
			Family family = new Family(id,husband,wife);
			if (husband!=null) {
				husband.addPersonalFamily(family);
			}
			if (wife!=null) {
				wife.addPersonalFamily(family);
			}
			if (husband!=null && wife !=null){
				family.setMarried(true);
			}
			result.families().add(family);
			index.put(marriage, family);
			id++;
		}
		
		//extract OriginFamilies
		for (PGraphNode marriage: marriages.values()){
			Individual husband = null;
			if (marriage.getHusband()!=null){
				husband = result.get(marriage.getHusband().getId());
			}
			Individual wife = null;
			if (marriage.getWife()!=null){
				wife = result.get(marriage.getWife().getId());
			}
			Family husbandsFamily = index.get(marriage.getHusbandLink());
			Family wifesFamily = index.get(marriage.getWifeLink());
			if (husband!=null && husbandsFamily!=null) {
				husband.setOriginFamily(husbandsFamily);
				husbandsFamily.getChildren().add(husband);
			}
			if (wife!=null && wifesFamily!=null) {
				wife.setOriginFamily(wifesFamily);
				wifesFamily.getChildren().add(wife);
			}
		}
		
		//fuse same-sex Siblings
		for (Individual individual : result.individuals()){
			int additionalSpouses = getProbableNumberOfSpouses(individual.getGender(), monogamousSameSexSiblings(individual).size());
			while (additionalSpouses > 1 && additionalSpouses > monogamousSameSexSiblings(individual).size()) {
				Individual sibling = randomDraw(monogamousSameSexSiblings(individual));
				Family additionalFamily = sibling.getPersonalFamilies().toList().get(0);
				if (individual.isMale()){
					additionalFamily.setHusband(individual);
				} else if (individual.isFemale()){
					additionalFamily.setWife(individual);
				}
				individual.addPersonalFamily(additionalFamily);
				result.individuals().removeById(sibling.getId());
			}
		}
		
		//
		return result;
	}
	
	private static Individual randomDraw (Individuals individuals){
		int randomIndex = (int)(Math.random()*individuals.size());
		return individuals.toList().get(randomIndex);
	}
	
	public static Individuals monogamousSameSexSiblings(Individual individual) {
		Individuals result;
		
		result = new Individuals();
		Family family = individual.getOriginFamily();
		if (family != null) {
		    for (Individual sibling : family.getChildren()){
		        if (sibling!=individual && sibling.getGender()==individual.getGender() && sibling.spouses().size()==1){
		            result.add(sibling);
		        }
		    }
		}
		//
		return result;
	}

	public PGraph1(final Net net) {

		smDistrib = new SiblingMarriageDistribution(net);
		totalLinks = 0;
		marriages = new HashMap<String, PGraphNode>();
		marriagesByIndividual = new HashMap<Individual, Set<PGraphNode>>();

		// Extract marriages from families
		
		for (Family f : net.families()) {
			Individual wife = null;
			Individual husband = null;
			Individual p1 = f.getHusband();
			Individual p2 = f.getWife();
			if (p1 != null) {
				if (p1.getGender().isFemale()) {
					wife = p1;
				} else if (p1.getGender().isMale()) {
					husband = p1;
				}
			}
			if (p2 != null) {
				if (p2.getGender().isFemale()) {
					wife = p2;
				} else if (p2.getGender().isMale()) {
					husband = p2;
				}
			}
			PGraphNode newMarriage = new PGraphNode(wife, husband); // wife,husband? 
			String key = newMarriage.hashKey();
			if (!marriages.containsKey(key)) {
				marriages.put(key, newMarriage);
				if (wife != null) {
					if (!marriagesByIndividual.containsKey(wife)) {
						marriagesByIndividual.put(wife, new HashSet<PGraphNode>());
					}
					marriagesByIndividual.get(wife).add(newMarriage);
				}
				if (husband != null) {
					if (!marriagesByIndividual.containsKey(husband)) {
						marriagesByIndividual.put(husband, new HashSet<PGraphNode>());
					}
					marriagesByIndividual.get(husband).add(newMarriage);
				}
			}
		}
		System.out.println(marriages.size()+" "+StatisticsWorker.numberOfMarriages(net.families()));

		// Extract marriage links from families
		for (Family f : net.families()) {
			Individual wife = null;
			Individual husband = null;
			Individual p1 = f.getHusband();
			Individual p2 = f.getWife();
			if (p1 != null) {
				if (p1.isFemale()) {
					wife = p1;
					totalLinks++;
				} else if (p1.isMale()) {
					husband = p1;
					totalLinks++;
				}
			}
			if (p2 != null) {
				if (p2.isFemale()) {
					wife = p2;
					totalLinks++;
				} else if (p2.isMale()) {
					husband = p2;
					totalLinks++;
				}
			}

			PGraphNode parents = marriages.get((new PGraphNode(wife, husband)).hashKey());
			for (Individual child : f.getChildren()) {
				Set<PGraphNode> childMarriages = marriagesByIndividual.get(child);
				if (childMarriages != null) {
					for (PGraphNode m : childMarriages) {
						if (child.isFemale()) {
							m.setWifeLink(parents);
						} else if (child.isMale()) {
							m.setHusbandLink(parents);
						}
					}
				}
			}
		}

		// create links vector
		linksVector = new Vector<PGraphLink>();
		for (Map.Entry<String, PGraphNode> entry : marriages.entrySet()) {
			PGraphNode marriage = entry.getValue();
			PGraphNode wifeLink = marriage.getWifeLink();
			PGraphNode husbandLink = marriage.getHusbandLink();
			if (wifeLink != null) {
				PGraphLink link = new PGraphLink(wifeLink, marriage, true);
				linksVector.add(link);
			}
			if (husbandLink != null) {
				PGraphLink link = new PGraphLink(husbandLink, marriage, false);
				linksVector.add(link);
			}
		}

		// compute out degrees and ancestors
		computeOutDegrees();
		computeAncestors();
	}

	public PGraph1(final PGraph1 pg) {
	    smDistrib = pg.smDistrib;
		totalLinks = pg.totalLinks;

		// copy marriages
		marriages = new HashMap<String, PGraphNode>();
		marriagesByIndividual = new HashMap<Individual, Set<PGraphNode>>();
		for (Map.Entry<String, PGraphNode> entry : pg.marriages.entrySet()) {
			String key = entry.getKey();
			PGraphNode marriage = new PGraphNode(entry.getValue());
			marriages.put(key, marriage);
			Individual wife = marriage.getWife();
			Individual husband = marriage.getHusband();
			if (wife != null) {
				if (!marriagesByIndividual.containsKey(wife)) {
					marriagesByIndividual.put(wife, new HashSet<PGraphNode>());
				}
				marriagesByIndividual.get(wife).add(marriage);
			}
			if (husband != null) {
				if (!marriagesByIndividual.containsKey(husband)) {
					marriagesByIndividual.put(husband, new HashSet<PGraphNode>());
				}
				marriagesByIndividual.get(husband).add(marriage);
			}
		}

		// create links vector
		linksVector = new Vector<PGraphLink>();
		for (Map.Entry<String, PGraphNode> entry : marriages.entrySet()) {
			PGraphNode marriage = entry.getValue();
			PGraphNode wifeLink = marriage.getWifeLink();
			PGraphNode husbandLink = marriage.getHusbandLink();
			if (wifeLink != null) {
				PGraphLink link = new PGraphLink(wifeLink, marriage, true);
				linksVector.add(link);
			}
			if (husbandLink != null) {
				PGraphLink link = new PGraphLink(husbandLink, marriage, false);
				linksVector.add(link);
			}
		}

		// compute out degrees and ancestors
		computeOutDegrees();
		computeAncestors();
	}

	private boolean addLink(final PGraphLink link, final int maxGenDist) {
		PGraphNode orig = link.getOrigin();
		PGraphNode targ = link.getTarget();

		// marriages cannot link to themselves
		if (orig == targ) {
			// System.out.println("#1: self-link");
			return false;
		}

		// is origin a descendant of target?
		if (targ.hasDescendant(orig)) {
			// System.out.println("#2: orig is descendant of target");
			return false;
		}

		// is original generational distance within limit?
		int dist = orig.originalGenerationalDistance(targ);
		if ((dist < 0) || (dist > maxGenDist)) {
			//System.out.println("#3: originl generational distance not within limit " + dist);
			return false;
		}

		// set link on target
		if (link.isWifeLink()) {
			link.getTarget().setWifeLink(link.getOrigin());
		} else {
			link.getTarget().setHusbandLink(link.getOrigin());
		}

		// recompute ancestors
		clearAncestors();
		computeAncestors();

		return true;
	}

	public float averageNumberOfDescendants() {
		float avg = 0;

		for (Map.Entry<String, PGraphNode> entry : marriages.entrySet()) {
			PGraphNode marriage = entry.getValue();
			avg += marriage.numberOfDescendants();
		}

		avg /= numberOfMarriages();

		return avg;
	}
	
	public float maxNumberOfDescendants() {
		float max = 0;
		
		for (Map.Entry<String, PGraphNode> entry : marriages.entrySet()) {
			PGraphNode marriage = entry.getValue();
			float descs = marriage.numberOfDescendants();
			if (descs > max) {
				max = descs;
			}
		}

		return max;
	}

	private void clearAncestors() {
		for (Map.Entry<String, PGraphNode> entry : marriages.entrySet()) {
			PGraphNode marriage = entry.getValue();
			marriage.clearDescendants();
		}
	}

	private void computeAncestors() {
		for (Map.Entry<String, PGraphNode> entry : marriages.entrySet()) {
			PGraphNode marriage = entry.getValue();
			marriage.addDescendant(marriage, 0);
		}
	}

	private void computeOutDegrees() {
		for (Map.Entry<String, PGraphNode> entry : marriages.entrySet()) {
			PGraphNode marriage = entry.getValue();
			PGraphNode wifeLink = marriage.getWifeLink();
			PGraphNode husbandLink = marriage.getHusbandLink();
			if (wifeLink != null) {
				wifeLink.incOutDegree();
				wifeLink.incOutDegreeF();
			}
			if (husbandLink != null) {
				husbandLink.incOutDegree();
				husbandLink.incOutDegreeM();
			}
		}
	}

	public Map<String, PGraphNode> getMarriages() {
		return marriages;
	}

	public Map<Individual, Set<PGraphNode>> getMarriagesByIndividual() {
		return marriagesByIndividual;
	}

	public int getTotalLinks() {
		return totalLinks;
	}

	public int numberOfMarriages() {
		return marriages.size();
	}

	private void removeLink(final PGraphLink link) {
		if (link.isWifeLink()) {
			link.getTarget().setWifeLink(null);
		} else {
			link.getTarget().setWifeLink(null);
		}
	}

	public void storeOriginalDescendants() {
		for (Map.Entry<String, PGraphNode> entry : marriages.entrySet()) {
			PGraphNode marriage = entry.getValue();
			marriage.storeOriginalDescendants();
		}
	}

	public boolean switchLinks(final int k, final int maxGenDist) {
		// find list of links to shuffle, no repetitions, same gender target
		Set<PGraphLink> linkSet = new HashSet<PGraphLink>();
		PGraphLink link = linksVector.get(RandomGenerator.instance().random.nextInt(linksVector.size()));
		linkSet.add(link);
		boolean gender = link.isWifeLink();
		while (linkSet.size() < k) {
			link = linksVector.get(RandomGenerator.instance().random.nextInt(linksVector.size()));
			if (link.isWifeLink() == gender) {
				linkSet.add(link);
			}
		}
		List<PGraphLink> links = new ArrayList<PGraphLink>(linkSet);

		// create a perfect shuffle of the previous list
		List<PGraphLink> links2 = new ArrayList<PGraphLink>(links);
		boolean perfectShuffle = false;
		while (!perfectShuffle) {
			perfectShuffle = true;
			for (int i = 0; i < k; i++) {
				if (links.get(i) == links2.get(i)) {
					perfectShuffle = false;
				}
			}
			Collections.shuffle(links2);
		}

		// create new links list
		List<PGraphLink> newLinks = new ArrayList<PGraphLink>();
		for (int i = 0; i < k; i++) {
			PGraphLink newLink = new PGraphLink(links.get(i).getOrigin(), links2.get(i).getTarget(), links.get(i).isWifeLink());
			newLinks.add(newLink);
		}

		// remove old links
		for (PGraphLink oldLink : links) {
			removeLink(oldLink);
		}

		// recompute ancestors
		clearAncestors();
		computeAncestors();

		// try to add new links
		boolean fail = false;
		for (PGraphLink newLink : newLinks) {
			if (!addLink(newLink, maxGenDist)) {
				fail = true;
				break;
			}
		}

		// revert changes in case of failure
		if (fail) {
			for (PGraphLink newLink : newLinks) {
				removeLink(newLink);
			}
			for (PGraphLink oldLink : links) {
				addLink(oldLink, maxGenDist);
			}
		}
		else {
			// update link objects
			for (int i = 0; i < k; i++) {
				links.get(i).setTarget(links2.get(i).getTarget());
				links.get(i).setShuffled(true);
			}
		}

		return !fail;
	}
	
	public double distance(PGraph1 pg) {
		double dist = 0;
		double count = 0;
		for (Map.Entry<String, PGraphNode> entry : marriages.entrySet()) {
			String key = entry.getKey();
			PGraphNode m1 = entry.getValue();
			PGraphNode m2 = pg.getMarriages().get(key);
			dist += m1.distance(m2);
			count += 1.0;
		}
		
		return dist / count;
	}
	
	public double percentageShuffledLinks() {
		double shuffled = 0;
		double total = 0;
		for (PGraphLink link : linksVector) {
			if (link.isShuffled()) {
				shuffled += 1.0;
			}
			total += 1.0;
		}
		
		return (shuffled / total) * 100.0;
	}

	@Override
	public String toString() {
		String str = "Number of marriages: " + numberOfMarriages() + "\n";
		str += "Number of links: " + getTotalLinks() + "\n";
		str += "Average number of descendants: " + averageNumberOfDescendants() + "\n";
		str += "Max number of descendants: " + maxNumberOfDescendants() + "\n";
		return str;
	}
}
