/**
 * 
 */
package org.tip.puck.generators;

/**
 * @author Telmo Menezes
 *
 */
public class PGraphLink {
	private PGraphNode origin;
	private PGraphNode target;
	private boolean wifeLink;
	private boolean shuffled;
	
	public PGraphLink(PGraphNode origin, PGraphNode target, boolean wifeLink) {
		this.origin = origin;
		this.target = target;
		this.wifeLink = wifeLink;
		this.shuffled = false;
	}

	public PGraphNode getOrigin() {
		return origin;
	}

	public void setOrigin(PGraphNode origin) {
		this.origin = origin;
	}

	public PGraphNode getTarget() {
		return target;
	}

	public void setTarget(PGraphNode target) {
		this.target = target;
	}

	public boolean isWifeLink() {
		return wifeLink;
	}

	public void setWifeLink(boolean wifeLink) {
		this.wifeLink = wifeLink;
	}

	public boolean isShuffled() {
		return shuffled;
	}

	public void setShuffled(boolean shuffled) {
		this.shuffled = shuffled;
	}
}
