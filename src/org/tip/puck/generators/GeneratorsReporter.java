package org.tip.puck.generators;

import org.tip.puck.net.Net;
import org.tip.puck.report.Report;
import org.tip.puck.util.Chronometer;

/**
 * This class represents a reporter for generators.
 * 
 * @author TIP
 */
public class GeneratorsReporter {

	/**
	 * Generates a report for shuffle P-Graph.
	 * 
	 * @param net
	 *            source to analyze.
	 * @param edgePermutations
	 *            Number of edge permutations per step.
	 * @param maximumGenerationalDistance
	 *            Maximum generational distance.
	 * @param minimumShufflePercentage
	 *            Minimum shuffle percentage (stop condition).
	 * @param minimumStableIterations
	 *            Minimum stable iterations (stop condition).
	 * 
	 * @return a report.
	 */
	public static Report reportShufflePGraph(final PGraph pgraph, final Net sourceNet, final int edgePermutations, final int maxGenerationalDistance,
			final int minShufflePercentage, final int minStableIterations) {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Suffle P-Graph.");
		result.setOrigin("Generators reporter");
		result.setTarget(sourceNet.getLabel());

		//
		result.inputs().add("Edge permutations per step", edgePermutations);
		result.inputs().add("Maximum generational distance", maxGenerationalDistance);
		result.inputs().add("Minimum shuffle percentage (stop condition)", minShufflePercentage);
		result.inputs().add("Minimum stable iterations (stop condition)", minStableIterations);

		//
		result.outputs().append(pgraph.toString());

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}
}
