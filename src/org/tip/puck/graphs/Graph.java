package org.tip.puck.graphs;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.tip.puck.graphs.Link.LinkType;
import org.tip.puck.matrix.Matrix;
import org.tip.puck.matrix.SparseMatrix;
import org.tip.puck.net.Attributes;
import org.tip.puck.net.relations.Role;

/**
 * 
 * @author TIP
 */
public class Graph<E> {
	private String label;
	private Nodes<E> nodes;
	private Attributes attributes;
	
	public enum GraphMode {
		
		DIRECTED,
		UNDIRECTED;
	}

	/**
	 * 
	 */
	public Graph() {
		//
		this.nodes = new Nodes<E>();
		this.attributes = new Attributes();
	}

	/**
	 * 
	 * @param initialNodeCount
	 */
	public Graph(final int initialNodeCount) {
		//
		this.nodes = new Nodes<E>(initialNodeCount);

		//
		for (int nodeIndex = 1; nodeIndex <= initialNodeCount; nodeIndex++) {
			addNode(nodeIndex, null);
		}

		//
		this.attributes = new Attributes();
	}

	/**
	 * 
	 * @param initialNodeCount
	 * @param initialCapacity
	 */
	public Graph(final int initialNodeCount, final int initialCapacity) {
		//
		this.nodes = new Nodes<E>(initialCapacity);

		//
		for (int nodeIndex = 1; nodeIndex <= initialNodeCount; nodeIndex++) {
			addNode(nodeIndex, null);
		}

		//
		this.attributes = new Attributes();
	}

	/**
	 * 
	 */
	public Graph(final String label) {
		this.setLabel(label);
		this.nodes = new Nodes<E>();
		this.attributes = new Attributes();
	}

	/**
	 * 
	 * @param target
	 */
	public Link<E> addArc(final E sourceReferent, final E targetReferent) {
		Link<E> result;

		//
		result = addArc(sourceReferent, targetReferent, 0);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param target
	 */
	public Link<E> addArc(final E source, final E target, final double weight) {
		Link<E> result;

		//
		Node<E> sourceNode = this.nodes.get(source);
		if (sourceNode == null) {
			sourceNode = this.nodes.add(source);
		}

		//
		Node<E> targetNode = this.nodes.get(target);
		if (targetNode == null) {
			targetNode = this.nodes.add(target);
		}

		//
		result = addArc(sourceNode, targetNode, weight);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 */
	public Link<E> addArc(final int sourceId, final int targetId) {
		Link<E> result;

		result = addArc(sourceId, targetId, 0);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 */
	public Link<E> addArc(final int sourceId, final int targetId, final double weight) {
		Link<E> result;

		//
		Node<E> sourceNode = this.nodes.get(sourceId);
		if (sourceNode == null) {
			sourceNode = addNode(sourceId, null);
		}

		//
		Node<E> targetNode = this.nodes.get(targetId);
		if (targetNode == null) {
			targetNode = addNode(targetId, null);
		}

		//
		result = addArc(sourceNode, targetNode, weight);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 */
	public Link<E> addArc(final Node<E> sourceNode, final Node<E> targetNode) {
		Link<E> result;

		result = sourceNode.addArcWith(targetNode);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 */
	public Link<E> addArc(final Node<E> sourceNode, final Node<E> targetNode, final double weight) {
		Link<E> result;

		result = sourceNode.addArcWith(targetNode, weight);

		//
		return result;
	}

	/**
	 * 
	 * @param matrix
	 */
	public void addArcs(final Matrix matrix) {
		for (int i = 0; i < matrix.getRowDim(); i++) {
			for (int j = 0; j < matrix.getColDim(); i++) {
				incArcWeight(i, j);
			}
		}
	}

	/**
	 * 
	 * @param source
	 * @param target
	 */
	public double addArcWeight(final E source, final E target, final double weight) {
		double result;

		//
		Node<E> sourceNode = this.nodes.get(source);
		if (sourceNode == null) {
			sourceNode = this.nodes.add(source);
		}

		//
		Node<E> targetNode = this.nodes.get(target);
		if (targetNode == null) {
			targetNode = this.nodes.add(target);
		}

		//
		result = addArcWeight(sourceNode, targetNode, weight);

		//
		return result;
	}

	/**
	 * 
	 * @param sourceId
	 * @param targetId
	 */
	public double addArcWeight(final int sourceId, final int targetId, final double value) {
		double result;

		//
		Node<E> sourceNode = this.nodes.get(sourceId);

		//
		Node<E> targetNode = this.nodes.get(targetId);

		//
		result = addArcWeight(sourceNode, targetNode, value);

		//
		return result;
	}

	/**
	 * 
	 * @param sourceNodeLabel
	 * @param targetNodeLabel
	 */
	public double addArcWeight(final Node<E> sourceNode, final Node<E> targetNode, final double value) {
		double result;

		//
		result = sourceNode.addArcWeight(targetNode, value);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 */
	public Link<E> addEdge(final E sourceReferent, final E targetReferent) {
		Link<E> result;

		//
		result = addEdge(sourceReferent, targetReferent, 0);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param target
	 */
	public Link<E> addEdge(final E source, final E target, final double weight) {
		Link<E> result;

		//
		Node<E> sourceNode = this.nodes.get(source);
		if (sourceNode == null) {
			sourceNode = this.nodes.add(source);
		}

		//
		Node<E> targetNode = this.nodes.get(target);
		if (targetNode == null) {
			targetNode = this.nodes.add(target);
		}

		//
		result = addEdge(sourceNode, targetNode, weight);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 */
	public Link<E> addEdge(final int sourceId, final int targetId) {
		Link<E> result;

		result = addEdge(sourceId, targetId, 0);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 */
	public Link<E> addEdge(final int sourceId, final int targetId, final double weight) {
		Link<E> result;

		//
		Node<E> sourceNode = this.nodes.get(sourceId);
		if (sourceNode == null) {
			sourceNode = addNode(sourceId, null);
		}

		//
		Node<E> targetNode = this.nodes.get(targetId);
		if (targetNode == null) {
			targetNode = addNode(targetId, null);
		}

		//
		result = addEdge(sourceNode, targetNode, weight);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 */
	public Link<E> addEdge(final Node<E> node, final Node<E> otherNode) {
		Link<E> result;

		result = addEdge(node, otherNode, 0);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 */
	public Link<E> addEdge(final Node<E> node, final Node<E> otherNode, final double weight) {
		Link<E> result;

		result = node.addEdgeWith(otherNode, weight);

		//
		return result;
	}
	
	
	
	
	public Link<E> addLink (E ego, E alter, LinkType linkType, double weight){
		Link<E> result;

		result = null;
		
		if (linkType == LinkType.ARC){
			result = this.addArc(ego, alter, weight);
		} else if (linkType == LinkType.EDGE){
			result = this.addEdge(ego, alter, weight);
		}
		//
		return result;
	}

	public Link<E> addLink (E ego, E alter, LinkType linkType, double weight, String tag){
		Link<E> result;
		
		result = null;
		
		if (linkType == LinkType.ARC){
			result = this.getArc(ego, alter);
		} else if (linkType == LinkType.EDGE){
			result = this.getEdge(ego, alter);
		}
		
		if (result==null){
			
			result = addLink(ego,alter,linkType,weight);
			result.setTag(tag);
			
		} else {
			
			result.appendTag(tag);
		}
		//
		return result;
	}
	
	/**
	 * 
	 * @param source
	 * @param target
	 */
	public double addEdgeWeight(final E source, final E target, final double weight) {
		double result;

		//
		Node<E> sourceNode = this.nodes.get(source);
		if (sourceNode == null) {
			sourceNode = this.nodes.add(source);
		}

		//
		Node<E> targetNode = this.nodes.get(target);
		if (targetNode == null) {
			targetNode = this.nodes.add(target);
		}

		//
		result = addEdgeWeight(sourceNode, targetNode, weight);

		//
		return result;
	}

	/**
	 * 
	 * @param sourceId
	 * @param targetId
	 */
	public double addEdgeWeight(final int sourceId, final int targetId, final double value) {
		double result;

		//
		Node<E> sourceNode = this.nodes.get(sourceId);
		if (sourceNode == null) {
			sourceNode = addNode(sourceId, null);
		}

		//
		Node<E> targetNode = this.nodes.get(targetId);
		if (targetNode == null) {
			targetNode = addNode(targetId, null);
		}

		//
		result = addEdgeWeight(sourceNode, targetNode, value);

		//
		return result;
	}

	/**
	 * 
	 * @param sourceNodeLabel
	 * @param targetNodeLabel
	 */
	public double addEdgeWeight(final Node<E> sourceNode, final Node<E> targetNode, final double value) {
		double result;

		//
		result = sourceNode.addEdgeWeight(targetNode, value);

		//
		return result;
	}

	/**
	 * 
	 * @param referent
	 * @return
	 */
	public Node<E> addNode(final E referent) {
		Node<E> result;

		if (referent == null) {
			throw new NullPointerException("Node cannot have a null referent.");
		} else {
			result = this.nodes.get(referent);
			if (result == null) {
				result = this.nodes.add(referent);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param referent
	 * @return
	 */
	public Node<E> addNode(final int id, final E referent) {
		Node<E> result;

		result = this.nodes.get(id);
		if (result == null) {
			result = this.nodes.add(id, referent);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param nodes
	 */
	public void addNodesWithId(final Nodes<E> nodes) {

		for (Node<E> node : nodes) {
			addNode(node.getId(), node.getReferent());
		}

	}

	/**
	 * 
	 * @param nodes
	 */
	public void addNodes(final Nodes<E> nodes) {

		for (Node<E> node : nodes) {
			addNode(node.getReferent());
		}

	}

	/**
	 * 
	 * @param links
	 */
	public void addLinks(final Links<E> links) {

		for (Link<E> link : links) {
			addLink(link.getSourceNode().getReferent(),link.getTargetNode().getReferent(),link.getType(),link.getWeight(),link.getTag());
		}
	}
	
	
	/**
	 * 
	 * @param links
	 */
/*	public void addLinks(final Links<E> links) {

		for (Link<E> link : links) {
			if (link.isArc()){
				Link<E> arc = addArc(link.getSourceNode().getReferent(),link.getTargetNode().getReferent(),link.getWeight());
				arc.setTag(link.getTag());
			} else if (link.isEdge()){
				Link<E> edge = addEdge(link.getSourceNode().getReferent(),link.getTargetNode().getReferent(),link.getWeight());
				edge.setTag(link.getTag());
			}
		}
	}*/
	
	/**
	 * WARNING: setArcs and not appendArcs (possibility of overwriting arcs in case of multiple links)
	 * @param links
	 */
	public <T> void addLinksByIds(final Links<T> links) {

		for (Link<T> link : links) {
			if (link.isArc()){
				Link<E> arc = addArc(link.getSourceId(),link.getTargetId(),link.getWeight());
				arc.setTag(link.getTag());
			} else if (link.isEdge()){
				Link<E> edge = addEdge(link.getSourceId(),link.getTargetId(),link.getWeight());
				edge.setTag(link.getTag());
			}
		}
	}


	/**
	 * 
	 * @return
	 */
	public int arcCount() {
		return getArcs().size();
	}

	/**
	 * 
	 * @return
	 */
	public Attributes attributes() {
		Attributes result;

		result = this.attributes;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int edgeCount() {
		return getEdges().size();
	}

	/**
	 * 
	 * @param source
	 * @param target
	 */
	public Link<E> getArc(final E source, final E target) {
		Link<E> result;

		//
		Node<E> sourceNode = this.nodes.get(source);
		if (sourceNode == null) {
			sourceNode = this.nodes.add(source);
		}

		//
		Node<E> targetNode = this.nodes.get(target);
		if (targetNode == null) {
			targetNode = this.nodes.add(target);
		}

		//
		result = getArc(sourceNode, targetNode);

		//
		return result;
	}

	/**
	 * 
	 * @param node
	 * @param node
	 * @return
	 */
	public Link<E> getArc(final Node<E> node, final Node<E> otherNode) {
		Link<E> result;

		result = node.getArcWith(otherNode);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Links<E> getArcs() {
		Links<E> result;

		result = new Links<E>();

		for (Node<E> node : getNodes()) {
			result.addAll(node.getOutArcs().getLinks());
		}
		//
		return result;
	}

	/**
	 * 
	 * @param node
	 * @param otherNode
	 * @return
	 */
	public double getArcWeight(final int nodeId, final int otherNodeId) {
		double result;

		//
		Node<E> node = this.nodes.get(nodeId);

		//
		Node<E> otherNode = this.nodes.get(otherNodeId);

		//
		result = getArcWeight(node, otherNode);

		//
		return result;
	}

	/**
	 * 
	 * @param node
	 * @param otherNode
	 * @return
	 */
	public double getArcWeight(final Node<E> node, final Node<E> otherNode) {
		double result;

		Link<E> link = getArc(node, otherNode);
		if (link == null) {
			result = 0;
		} else {
			result = link.getWeight();
		}

		//
		return result;
	}

	public double getArcWeightSum() {
		double result = 0.;
		for (Node<E> node : getNodes()) {
			for (Link<E> link : node.getOutArcs().getLinks()) {
				result += link.getWeight();
			}
		}
		//
		return result;
	}


	/**
	 * 
	 * @param id
	 * @return
	 */
	public int getDegree(final int id) {
		return getNode(id).getDegree();
	}

	/**
	 * 
	 * @param source
	 * @param target
	 */
	public Link<E> getEdge(final E source, final E target) {
		Link<E> result;

		//
		Node<E> sourceNode = this.nodes.get(source);
		if (sourceNode == null) {
			sourceNode = this.nodes.add(source);
		}

		//
		Node<E> targetNode = this.nodes.get(target);
		if (targetNode == null) {
			targetNode = this.nodes.add(target);
		}

		//
		result = getEdge(sourceNode, targetNode);

		//
		return result;
	}

	/**
	 * 
	 * @param node
	 * @param node
	 * @return
	 */
	public Link<E> getEdge(final Node<E> node, final Node<E> otherNode) {
		Link<E> result;

		result = node.getEdgeWith(otherNode);

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public int getEdgeDegree(final int id) {
		int result;

		result = getNode(id).getEdgeDegree();

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public double getEdgeForce(final int id) {
		double result;

		result = getNode(id).getEdgeForce();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Links<E> getEdges() {
		Links<E> result;

		result = new Links<E>();

		for (Node<E> node : getNodes()) {
			result.addAll(node.getInferiorEdges().getLinks());
		}
		//
		return result;
	}

	/**
	 * 
	 * @param node
	 * @param otherNode
	 * @return
	 */
	public double getEdgeWeight(final int nodeId, final int otherNodeId) {
		double result;

		//
		Node<E> node = this.nodes.get(nodeId);

		//
		Node<E> otherNode = this.nodes.get(otherNodeId);

		//
		result = getEdgeWeight(node, otherNode);

		//
		return result;
	}

	/**
	 * 
	 * @param node
	 * @param otherNode
	 * @return
	 */
	public double getEdgeWeight(final Node<E> node, final Node<E> otherNode) {
		double result;

		Link<E> link = getEdge(node, otherNode);
		if (link == null) {
			result = 0;
		} else {
			result = link.getWeight();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public double getForce(final int id) {
		return getNode(id).getForce();
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public int getInDegree(final int id) {
		int result;

		result = getNode(id).getInDegree();

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public double getInForce(final int id) {
		double result;

		result = 0.;
		for (Link<E> arc : getNode(id).getInArcs()) {
			result = result += arc.getWeight();
		}
		//
		return result;
	}

	public Map<Integer, Double> getInForces() {
		Map<Integer, Double> result;

		result = new TreeMap<Integer, Double>();

		for (Node<E> node : nodes) {
			result.put(node.getId(), node.getInForce());
		}
		//
		return result;

	}

	public String getLabel() {
		return label;
	}

	/**
	 * 
	 * @return
	 */
	public Links<E> getLinks() {
		Links<E> result;

		result = new Links<E>();

		for (Node<E> node : getNodes()) {
			result.addAll(node.getInLinks());
		}
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public SparseMatrix getMatrix() {
		SparseMatrix result;

		//
		Nodes<E> sourceNodes = this.getSourceNodes();
		Nodes<E> targetNodes = this.getTargetNodes();

		//
		result = new SparseMatrix(sourceNodes.size(), targetNodes.size());

		// Sort target nodes by declaring them.
		for (Node<E> node : targetNodes.toListSortedByLabel()) {
			result.addTarget(node.getId());
		}

		//
		for (Node<E> node : sourceNodes.toListSortedByLabel()) {
			for (Link<E> link : node.getOutArcs()) {
				// Call add (not set) because matrix.set does not calculate
				// sums.
				result.add(link.getSourceNode().getId(), link.getTargetNode().getId(), link.getWeight());
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param referent
	 * @return
	 */
	public Node<E> getNode(final E referent) {
		Node<E> result;

		if (referent == null) {
			result = null;
		} else {
			result = this.nodes.get(referent);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param referent
	 * @return
	 */
	public Node<E> getNode(final int id) {
		Node<E> result;

		result = this.nodes.get(id);

		//
		return result;
	}

	/**
	 * 
	 * @param referent
	 * @return
	 */
	public int getNodeId(final E referent) {
		int result;

		if (referent == null) {
			result = Node.NO_ID;
		} else {
			Node<E> node = this.nodes.get(referent);
			if (node == null) {
				result = Node.NO_ID;
			} else {
				result = node.getId();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Nodes<E> getNodes() {
		return nodes;
	}

	/**
	 * 
	 * @param tag
	 * @return
	 */
	public Nodes<E> getNodesByTag(final String tag) {
		Nodes<E> result;

		//
		result = this.nodes.getNodesByTag(tag);

		//
		return result;
	}
	
	/**
	 * 
	 * @param label
	 * @return
	 */
	public Nodes<E> getNodesByLabel(final String label) {
		Nodes<E> result;

		//
		result = this.nodes.getNodesByLabel(label);

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public int getOutDegree(final int nodeId) {
		int result;

		result = getNode(nodeId).getOutDegree();

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public double getOutForce(final int nodeId) {
		double result;

		result = 0.;
		for (Link<E> arc : getNode(nodeId).getOutArcs()) {
			result = result += arc.getWeight();
		}
		//
		return result;
	}

	public Map<Integer, Double> getOutForces() {
		Map<Integer, Double> result;

		result = new TreeMap<Integer, Double>();

		for (Node<E> node : nodes) {
			result.put(node.getId(), node.getOutForce());
		}
		//
		return result;

	}

	/**
	 * 
	 * @return
	 */
	public List<E> getReferents() {
		List<E> result;

		result = new ArrayList<E>(this.nodeCount());
		for (Node<E> node : this.nodes) {
			result.add(node.getReferent());
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Nodes<E> getSourceNodes() {
		Nodes<E> result;

		result = new Nodes<E>();

		for (Node<E> node : this.nodes) {
			if (node.getOutArcs().size() > 0) {
				result.add(node);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public SparseMatrix getSquareMatrix() {
		SparseMatrix result;

		//
		result = new SparseMatrix(this.nodes.size(), this.nodes.size());

		// Sort target nodes by declaring them.
		for (Node<E> node : this.nodes.toListSortedByLabel()) {
			result.addSource(node.getId());
			result.addTarget(node.getId());
		}

		//
		for (Node<E> node : this.nodes) {
			for (Link<E> link : node.getOutArcs()) {
				// Call add (not set) because matrix.set does not calculate
				// sums.
				result.add(link.getSourceNode().getId(), link.getTargetNode().getId(), link.getWeight());
			}
			for (Link<E> link : node.getEdges()) {
				// Call add (not set) because matrix.set does not calculate
				// sums.
				if (link.getSourceNode()==node){
					result.add(link.getSourceNode().getId(), link.getTargetNode().getId(), link.getWeight());
					if (link.getSourceNode()!=link.getTargetNode()){
						result.add(link.getTargetNode().getId(), link.getSourceNode().getId(), link.getWeight());
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Nodes<E> getTargetNodes() {
		Nodes<E> result;

		result = new Nodes<E>();

		for (Node<E> node : this.nodes) {
			if (node.getInArcs().size() > 0) {
				result.add(node);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param target
	 */
	public double incArcWeight(final E source, final E target) {
		double result;

		//
		Node<E> sourceNode = this.nodes.get(source);
		if (sourceNode == null) {
			sourceNode = this.nodes.add(source);
		}

		//
		Node<E> targetNode = this.nodes.get(target);
		if (targetNode == null) {
			targetNode = this.nodes.add(target);
		}

		//
		result = incArcWeight(sourceNode, targetNode);

		//
		return result;
	}

	/**
	 * 
	 * @param sourceId
	 * @param targetId
	 */
	public double incArcWeight(final int sourceId, final int targetId) {
		double result;

		//
		Node<E> sourceNode = this.nodes.get(sourceId);
		if (sourceNode == null) {
			sourceNode = addNode(sourceId, null);
		}

		//
		Node<E> targetNode = this.nodes.get(targetId);
		if (targetNode == null) {
			targetNode = addNode(targetId, null);
		}

		//
		result = incArcWeight(sourceNode, targetNode);

		//
		return result;
	}

	/**
	 * 
	 * @param sourceNodeLabel
	 * @param targetNodeLabel
	 */
	public double incArcWeight(final Node<E> sourceNode, final Node<E> targetNode) {
		double result;

		//
		result = sourceNode.incArcWeight(targetNode);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param target
	 */
	public double incEdgeWeight(final E source, final E target) {
		double result;

		//
		Node<E> sourceNode = this.nodes.get(source);
		if (sourceNode == null) {
			sourceNode = this.nodes.add(source);
		}

		//
		Node<E> targetNode = this.nodes.get(target);
		if (targetNode == null) {
			targetNode = this.nodes.add(target);
		}

		//
		result = incEdgeWeight(sourceNode, targetNode);

		//
		return result;
	}

	/**
	 * 
	 * @param sourceId
	 * @param targetId
	 */
	public double incEdgeWeight(final int sourceId, final int targetId) {
		double result;

		//
		Node<E> sourceNode = this.nodes.get(sourceId);
		if (sourceNode == null) {
			sourceNode = addNode(sourceId, null);
		}

		//
		Node<E> targetNode = this.nodes.get(targetId);
		if (targetNode == null) {
			targetNode = addNode(targetId, null);
		}

		//
		result = incEdgeWeight(sourceNode, targetNode);

		//
		return result;
	}

	/**
	 * 
	 * @param sourceNodeLabel
	 * @param targetNodeLabel
	 */
	public double incEdgeWeight(final Node<E> sourceNode, final Node<E> targetNode) {
		double result;

		//
		result = sourceNode.incEdgeWeight(targetNode);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int lineCount() {
		int result;

		result = this.getLinks().size();

		//
		return result;
	}
	
	public int lineCountWithoutLoops() {
		int result;
		
		result = 0;
		for (Link<E> link : this.getLinks()){
			if (!link.isLoop()){
				result++;
			}
		}
		//
		return result;
	}
	
	public int tieCount() {
		int result;
		
		result = 0;
		int nrLoops = 0;
		
		for (Node<E> node : getNodes()){
			result += node.tieCount();
			nrLoops += node.getLoopDegree();
		}
		//
		result = (result + nrLoops)/2;
		//
		return result;
	}
	
	public int tieCountWithoutLoops() {
		int result;
		
		result = 0;
		
		for (Node<E> node : getNodes()){
			result += node.tieCount();
		}
		//
		result = result/2;
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int nodeCount() {
		int result;

		result = this.nodes.size();

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param target
	 */
	public void setArcWeight(final E source, final E target, final double weight) {
		//
		Node<E> sourceNode = this.nodes.get(source);
		if (sourceNode == null) {
			sourceNode = this.nodes.add(source);
		}

		//
		Node<E> targetNode = this.nodes.get(target);
		if (targetNode == null) {
			targetNode = this.nodes.add(target);
		}

		//
		setArcWeight(sourceNode, targetNode, weight);
	}

	/**
	 * 
	 * @param sourceId
	 * @param targetId
	 */
	public void setArcWeight(final int sourceId, final int targetId, final double value) {
		//
		Node<E> sourceNode = this.nodes.get(sourceId);
		if (sourceNode == null) {
			sourceNode = addNode(sourceId, null);
		}

		//
		Node<E> targetNode = this.nodes.get(targetId);
		if (targetNode == null) {
			targetNode = addNode(targetId, null);
		}

		//
		setArcWeight(sourceNode, targetNode, value);
	}

	/**
	 * 
	 * @param sourceNodeLabel
	 * @param targetNodeLabel
	 */
	public void setArcWeight(final Node<E> sourceNode, final Node<E> targetNode, final double value) {
		//
		sourceNode.setArcWeight(targetNode, value);
	}

	/**
	 * 
	 * @param source
	 * @param target
	 */
	public void setEdgeWeight(final E source, final E target, final double weight) {
		//
		Node<E> sourceNode = this.nodes.get(source);
		if (sourceNode == null) {
			sourceNode = this.nodes.add(source);
		}

		//
		Node<E> targetNode = this.nodes.get(target);
		if (targetNode == null) {
			targetNode = this.nodes.add(target);
		}

		//
		setEdgeWeight(sourceNode, targetNode, weight);
	}

	/**
	 * 
	 * @param sourceId
	 * @param targetId
	 */
	public void setEdgeWeight(final int sourceId, final int targetId, final double value) {
		//
		Node<E> sourceNode = this.nodes.get(sourceId);
		if (sourceNode == null) {
			sourceNode = addNode(sourceId, null);
		}

		//
		Node<E> targetNode = this.nodes.get(targetId);
		if (targetNode == null) {
			targetNode = addNode(targetId, null);
		}

		//
		setEdgeWeight(sourceNode, targetNode, value);
	}

	/**
	 * 
	 * @param sourceNodeLabel
	 * @param targetNodeLabel
	 */
	public void setEdgeWeight(final Node<E> sourceNode, final Node<E> targetNode, final double value) {
		//
		sourceNode.setEdgeWeight(targetNode, value);
	}

	public void setLabel(final String label) {
		this.label = label;
	}
	
	public void removeNode(final Node<E> node){
		
		for (Node<E> otherNode : nodes){
			otherNode.removeLinksToNode(node);
		}
		nodes.removeNode(node);
		
	}
	
	public void removeIsolates (){
		
		for (Node<E> node : nodes.toList()){
			if (node.getDegree()==0){
				nodes.removeNode(node);
			}
		}
	}
	

	/**
	 * transfers all links from old to new node, and eliminates links between them
	 * @param oldNode
	 * @param newNode
	 */
	public void transferLinks(final Node<E> oldNode, final Node<E> newNode){
		
		if (!nodes.contains(newNode)){
			nodes.add(newNode);
		}
		
		for (Link<E> link: oldNode.getLinks()){
			Node<E> sourceNode = link.getSourceNode();
			Node<E> targetNode = link.getTargetNode();
			
			if (sourceNode==oldNode){
				targetNode.removeLinksToNode(oldNode);
				if (targetNode!=newNode){
					link.setSourceNode(newNode);
					if (link.isArc()){
						newNode.getOutArcs().add(link);
						targetNode.getInArcs().add(link);
					} else {
						newNode.getEdges().add(link);
						targetNode.getEdges().add(link);
					}
				}
			} else if (link.getTargetNode()==oldNode){
				sourceNode.removeLinksToNode(oldNode);
				if (sourceNode!=newNode){
					link.setTargetNode(newNode);
					if (link.isArc()){
						newNode.getInArcs().add(link);
						sourceNode.getOutArcs().add(link);
					} else {
						newNode.getEdges().add(link);
						sourceNode.getEdges().add(link);
					}
				}
			}
		}
	}
	
	public String toString(){
		return label;
	}
	
	/**
	 * renumbers nodes continuously
	 */
	public void renumberNodes(){
		
		int id = 1;
		for (Node<E> node : nodes.toListSortedById()){
			node.setId(id);
			id++;
		}
	}

	
	/**
	 * renumbers nodes continuously
	 */
	public void renumberNodesByLabel(){
		
		int id = 1;
		for (Node<E> node : nodes.toListSortedByLabel()){
			node.setId(id);
			id++;
		}
	}

}
