package org.tip.puck.visualization.style.attributes;

import org.gephi.data.attributes.api.AttributeModel;
import org.gephi.project.api.Workspace;
import org.tip.puck.graphs.Link;
import org.tip.puck.graphs.Node;

/**
 * Attribute Converter interface.
 * Maps Graph<E>'s node and edge attributes to Gephi's.
 * @author Edoardo Savoia
 */
public interface AttributeMap {

    /*
     * Initialization method. This method is necessary to initalize Gephi's attributes
     */
    public void registerAttributes(AttributeModel attributeModel);

    public void setAttributes(org.gephi.graph.api.Node gephiNode, Node<?> graphNode);

    public void setAttributes(org.gephi.graph.api.Edge gephiEdge, Link<?> graphEdge);
}
