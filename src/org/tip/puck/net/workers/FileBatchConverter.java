package org.tip.puck.net.workers;

import java.io.File;
import java.io.FileFilter;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckManager;
import org.tip.puck.io.iur.IURTXTSplittedFile;
import org.tip.puck.net.Net;
import org.tip.puck.net.workers.FileBatchConverterCriteria.Mode;
import org.tip.puck.net.workers.FileBatchConverterCriteria.TargetFormat;
import org.tip.puck.report.Report;
import org.tip.puck.util.Chronometer;

/**
 * 
 * @author TIP
 */
public class FileBatchConverter {

	private static final Logger logger = LoggerFactory.getLogger(FileBatchConverter.class);
	private static final int PADDING = 40;

	/**
	 * 
	 * @param source
	 * @throws PuckException
	 */
	public static Report convert(final FileBatchConverterCriteria criteria) {
		Report result;

		//
		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("File Batch Converter Report");
		result.setOrigin("File Batch Converter");

		result.inputs().add("Source Directory", criteria.getSourceDirectory().getAbsolutePath());
		result.inputs().add("Source charset", criteria.getCharsetName());
		result.inputs().add("Target Directory", criteria.getTargetDirectory().getAbsolutePath());
		result.inputs().add("TargetFormat", criteria.getTargetFormat().toString());
		result.inputs().add("Mode", criteria.getMode().toString());

		//
		File[] files = criteria.getSourceDirectory().listFiles((FileFilter) FileFilterUtils.fileFileFilter());

		int count = 0;
		int convertCount = 0;
		int skipCount = 0;
		int overwriteCount = 0;
		int errorCount = 0;
		for (File sourceFile : files) {

			count += 1;

			File targetFile = new File(criteria.getTargetDirectory(), FilenameUtils.removeExtension(sourceFile.getName())
					+ criteria.getTargetFormat().getExtension());

			if (criteria.getMode() == Mode.SKIP) {

				if (targetFile.exists()) {

					result.outputs().appendln(sourceFile.getName() + " → " + targetFile.getName() + "\tSKIPPED");

					skipCount += 1;
				} else {

					try {
						Net net = PuckManager.loadNet(sourceFile, criteria.getCharsetName());
						if (criteria.getTargetFormat() == TargetFormat.IURTXTS) {
							IURTXTSplittedFile.export(criteria.getTargetDirectory(), net);
						} else {
							PuckManager.saveNet(targetFile, net);
						}

						convertCount += 1;

						result.outputs().appendln(sourceFile.getName() + " → " + targetFile.getName() + "\tCONVERTED");

					} catch (PuckException exception) {

						result.outputs().appendln(sourceFile.getName() + " → " + targetFile.getName() + "\tERROR");
						logger.error("Error detected for file [" + sourceFile.getAbsolutePath() + "]", exception);
						errorCount += 1;
					}
				}

			} else {

				if (targetFile.isFile()) {

					try {
						Net net = PuckManager.loadNet(sourceFile, criteria.getCharsetName());
						if (criteria.getTargetFormat() == TargetFormat.IURTXTS) {
							IURTXTSplittedFile.export(criteria.getTargetDirectory(), net);
						} else {
							PuckManager.saveNet(targetFile, net);
						}

						result.outputs().appendln(sourceFile.getName() + " → " + targetFile.getName() + "\tOVERWRITED");

						overwriteCount += 1;
						convertCount += 1;

					} catch (PuckException exception) {

						result.outputs().appendln(sourceFile.getName() + " → " + targetFile.getName() + "\tERROR");
						logger.error("Error detected for file [" + sourceFile.getAbsolutePath() + "]", exception);
						errorCount += 1;
					}

				} else {

					try {
						Net net = PuckManager.loadNet(sourceFile, criteria.getCharsetName());
						if (criteria.getTargetFormat() == TargetFormat.IURTXTS) {
							IURTXTSplittedFile.export(criteria.getTargetDirectory(), net);
						} else {
							PuckManager.saveNet(targetFile, net);
						}

						result.outputs().appendln(sourceFile.getName() + " → " + targetFile.getName() + "\tCONVERTED");

						convertCount += 1;

					} catch (PuckException exception) {

						result.outputs().appendln(sourceFile.getName() + " → " + targetFile.getName() + "\tERROR");
						logger.error("Error detected for file [" + sourceFile.getAbsolutePath() + "]", exception);
						errorCount += 1;
					}

				}
			}
		}

		result.outputs().appendln();
		result.outputs().appendln();
		result.outputs().appendln("Source\t" + count);
		result.outputs().appendln("Converted\t" + convertCount);
		result.outputs().appendln("Skip\t" + skipCount);
		result.outputs().appendln("Overwrite\t" + overwriteCount);
		result.outputs().appendln("Error\t" + errorCount);

		//
		result.setStatus(count);
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}
}
