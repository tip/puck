package org.tip.puck.io.tip;

import org.tip.puck.net.Gender;

/**
 * This class represents a line of individual from a TXT file.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class TIPLine {
	/**
	 */
	public enum LineType {
		IDENTITY,
		KINSHIP,
		PROPERTY,
		NET_INFO
	};

	/**
	 */
	public enum Relation {
		FATHER,
		MOTHER,
		SPOUSE;
	}

	private LineType lineType;
	private int individualId;
	private Gender gender;
	private String name;
	private int alterId;
	private Relation relation;
	private String propertyLabel;
	private String propertyValue;
	private String propertyPlace;
	private String propertyDate;

	/**
	 * 
	 * @param individualId
	 * @param gender
	 * @param name
	 */
	public TIPLine(final int individualId, final Gender gender, final String name) {
		this.lineType = LineType.IDENTITY;
		this.individualId = individualId;
		this.gender = gender;
		this.name = name;
	}

	/**
	 * 
	 * @param individualId
	 * @param alterId
	 * @param relation
	 */
	public TIPLine(final int individualId, final int alterId, final Relation relation) {
		this.lineType = LineType.KINSHIP;
		this.individualId = individualId;
		this.alterId = alterId;
		this.relation = relation;
	}

	/**
	 * 
	 * @param individualId
	 * @param label
	 * @param value
	 * @param place
	 * @param date
	 * @param alterId
	 */
	public TIPLine(final int individualId, final String label, final String value, final String date, final String place, final int alterId) {
		this.lineType = LineType.PROPERTY;
		this.individualId = individualId;
		this.propertyLabel = label;
		this.propertyValue = value;
		this.propertyPlace = place;
		this.propertyDate = date;
		this.alterId = alterId;
	}

	/**
	 * 
	 * @param label
	 * @param value
	 */
	public TIPLine(final String label, final String value) {
		this.lineType = LineType.NET_INFO;
		this.propertyLabel = label;
		this.propertyValue = value;
	}

	public int alterId() {
		return this.alterId;
	}

	public Gender gender() {
		return this.gender;
	}

	public int individualId() {
		return this.individualId;
	}

	public LineType lineType() {
		return this.lineType;
	}

	public String name() {
		return this.name;
	}

	public String propertyDate() {
		return this.propertyDate;
	}

	public String propertyLabel() {
		return this.propertyLabel;
	}

	public String propertyPlace() {
		return this.propertyPlace;
	}

	public String propertyValue() {
		return this.propertyValue;
	}

	public Relation relation() {
		return this.relation;
	}

	public void setAlterId(final int alterId) {
		this.alterId = alterId;
	}

	public void setGender(final Gender gender) {
		this.gender = gender;
	}

	public void setIndividualId(final int individualId) {
		this.individualId = individualId;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setPropertyDate(final String propertyDate) {
		this.propertyDate = propertyDate;
	}

	public void setPropertyLabel(final String propertyLabel) {
		this.propertyLabel = propertyLabel;
	}

	public void setPropertyPlace(final String propertyPlace) {
		this.propertyPlace = propertyPlace;
	}

	public void setPropertyValue(final String propertyValue) {
		this.propertyValue = propertyValue;
	}

	public void setRelation(final Relation relation) {
		this.relation = relation;
	}
}
