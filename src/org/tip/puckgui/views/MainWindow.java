package org.tip.puckgui.views;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.swing.FlatDB4GeoNamesRequestPanel;
import org.tip.flatdb4geonames.swing.downloaddatabase.DownloadDatabaseDialog;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.PuckManager;
import org.tip.puck.census.workers.CensusReporter;
import org.tip.puck.census.workers.KinshipChainsCriteria;
import org.tip.puck.geo.Geography;
import org.tip.puck.geo.Place;
import org.tip.puck.geo.workers.GeocodingWorker;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.workers.GraphReporter;
import org.tip.puck.io.bar.BARFile;
import org.tip.puck.io.dat.DAT1File;
import org.tip.puck.io.deluz.DELUZFile;
import org.tip.puck.io.ged.IdShrinker;
import org.tip.puck.io.iur.IURFile;
import org.tip.puck.io.iur.IURTXTSplittedFile;
import org.tip.puck.io.kinsources.CatalogItem;
import org.tip.puck.io.kinsources.KinsourcesFile;
import org.tip.puck.io.paj.PAJFile;
import org.tip.puck.io.selz.SELZFile;
import org.tip.puck.kinoath.ComponentWorker;
import org.tip.puck.kinoath.IndividualGroups;
import org.tip.puck.kinoath.SegmentationWorker;
import org.tip.puck.net.Family;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Gender.GenderCode;
import org.tip.puck.net.Individual;
import org.tip.puck.net.IndividualComparator.Sorting;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.net.random.RandomNetReporter;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.RelationModels;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.relations.roles.RoleActorPair;
import org.tip.puck.net.relations.roles.RoleRelationMaker;
import org.tip.puck.net.relations.roles.RoleRelationMaker.RoleRelationRule;
import org.tip.puck.net.relations.roles.RoleRelationRules;
import org.tip.puck.net.relations.roles.RoleRelationsTXTFile;
import org.tip.puck.net.relations.roles.RoleRelationsXLSFile;
import org.tip.puck.net.relations.roles.RoleRelationsXMLFile;
import org.tip.puck.net.workers.AttributeDescriptors;
import org.tip.puck.net.workers.AttributeFilter;
import org.tip.puck.net.workers.AttributeRenameCriteria;
import org.tip.puck.net.workers.AttributeReplaceValueCriteria;
import org.tip.puck.net.workers.AttributeSetValueCriteria;
import org.tip.puck.net.workers.AttributeToRelationCriteria;
import org.tip.puck.net.workers.AttributeValueDescriptors;
import org.tip.puck.net.workers.AttributeWorker;
import org.tip.puck.net.workers.BicomponentWorker;
import org.tip.puck.net.workers.ControlReporter;
import org.tip.puck.net.workers.DistanceAttributeCriteria;
import org.tip.puck.net.workers.ExpandCriteria;
import org.tip.puck.net.workers.ExpansionMode;
import org.tip.puck.net.workers.FileBatchConverter;
import org.tip.puck.net.workers.FileBatchConverterCriteria;
import org.tip.puck.net.workers.GeneaQuiltConvert;
import org.tip.puck.net.workers.GeoPlaceNetCriteria;
import org.tip.puck.net.workers.HairCutWorker;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.net.workers.NetReporter;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.net.workers.RelationToAttributeCriteria;
import org.tip.puck.net.workers.SnowballCriteria;
import org.tip.puck.net.workers.TransmitAttributeValueCriteria;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.report.Report;
import org.tip.puck.report.ReportList;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.workers.MissingTestimoniesCriteria;
import org.tip.puck.sequences.workers.SequenceCriteria;
import org.tip.puck.sequences.workers.SequenceCriteria.TrajectoriesOperation;
import org.tip.puck.sequences.workers.SequenceReporter;
import org.tip.puck.sequences.workers.UnknownPlacesCriteria;
import org.tip.puck.statistics.StatisticsReporter;
import org.tip.puck.statistics.StatisticsWorker;
import org.tip.puck.util.Chronometer;
import org.tip.puck.util.Labels;
import org.tip.puck.util.ToolBox;
import org.tip.puck.visualization.VisualizationController;
import org.tip.puck.visualization.style.implementations.OreGraph;
import org.tip.puck.visualization.style.implementations.PGraph;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.PuckGUI;
import org.tip.puckgui.WindowGUI;
import org.tip.puckgui.util.ButtonTabComponent;
import org.tip.puckgui.util.GenericFileFilter;
import org.tip.puckgui.views.filebatchconverter.FileBatchConverterDialog;
import org.tip.puckgui.views.geneaquilt.IndividualGroupEventQuiltsPanel;
import org.tip.puckgui.views.geneaquilt.IndividualGroupQuiltsPanel;
import org.tip.puckgui.views.geo.MapPanel;
import org.tip.puckgui.views.geographyEditor.GeographyEditorPanel;
import org.tip.puckgui.views.iurtxts.ExportIURTXTSFileSelector;
import org.tip.puckgui.views.iurtxts.ImportIURTXTSFileSelector;
import org.tip.puckgui.views.kinoath.GlobalDiagramPanel;
import org.tip.puckgui.views.kinoath.IndividualDiagramsPanel;
import org.tip.puckgui.views.kinoath.IndividualGroupDiagramsPanel;
import org.tip.puckgui.views.visualization.VisualizationPanel;

import fr.devinsy.util.StringList;
import fr.devinsy.util.StringSet;
import fr.devinsy.util.xml.XMLBadFormatException;
import fr.inria.aviz.geneaquilt.gui.quiltview.GeneaQuiltPanel;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.VertexComparator;

/**
 * 
 * @author TIP
 */
public class MainWindow {

	private static final Logger logger = LoggerFactory.getLogger(MainWindow.class);

	private int reportCounter = 0;
	private JFrame frmPuck;
	private NetGUI netGUI;
	private JMenuItem mntmSave;
	private JMenuItem mntmRevert;
	private JMenu mntmRecent;
	private JTabbedPane tabbedPaneCorpus;
	private CorpusPanel corpusTab;
	private FamiliesPanel familiesTab;
	private IndividualsPanel individualsTab;
	private SegmentationPanel segmentationPanel;

	/**
	 * Create the application.
	 */
	public MainWindow(final NetGUI guiManager) {
		//
		this.netGUI = guiManager;
		initialize();
		this.frmPuck.setVisible(true);

		//
		updateMenuItems();
		updateTitle();
		getCorpusTab().update();
		getIndividualsTab().update();
		getFamiliesTab().update();
		getIndividualsTab().select(null);
		getFamiliesTab().select(null);
		for (RelationModel relationModel : this.netGUI.getNet().relationModels()) {
			//
			addRawTab(relationModel.getName(), new RelationsPanel(this.netGUI, relationModel));
		}
	}

	/**
	 * 
	 * @param report
	 */
	public void addRawTab(final String title, final JPanel panel) {
		//
		this.tabbedPaneCorpus.addTab(title, null, panel, null);
		this.tabbedPaneCorpus.setSelectedComponent(panel);
	}

	/**
	 * 
	 * @param report
	 */
	public void addRelationTab(final RelationModel relationModel) {
		//
		if (relationModel != null) {
			//
			JPanel relationPanel = new RelationsPanel(this.netGUI, relationModel);

			//
			addRawTab(relationModel.getName(), relationPanel);
		}
	}

	/**
	 * 
	 * @param report
	 */
	public void addReportTab(final Report report) {
		//
		JPanel reportPanel;
		if (report.containsSubReport()) {
			//
			reportPanel = new ReportsPanel(this.netGUI, report);

		} else {
			//
			reportPanel = new ReportPanel(this.netGUI, report);
		}

		addReportTab(report.title(), reportPanel);
	}

	/**
	 * 
	 * @param report
	 */
	public void addReportTab(final String title, final JPanel reportPanel) {
		//
		this.reportCounter += 1;

		addTab(title + " (" + this.reportCounter + ")", reportPanel);
	}

	/**
	 * 
	 * @param report
	 */
	public void addTab(final String title, final JPanel panel) {
		//
		this.tabbedPaneCorpus.addTab(title, null, panel, null);
		this.tabbedPaneCorpus.setSelectedComponent(panel);
		this.tabbedPaneCorpus.setTabComponentAt(this.tabbedPaneCorpus.getSelectedIndex(), new ButtonTabComponent(this.tabbedPaneCorpus));
	}

	/**
	 * 
	 */
	public void closeCurrentTab() {
		//
		this.tabbedPaneCorpus.remove(this.tabbedPaneCorpus.getSelectedIndex());
	}

	/**
	 * 
	 */
	public void closeRelationTabs() {

		for (RelationsPanel tab : getRelationTabs()) {
			//
			this.tabbedPaneCorpus.remove(tab);
		}
	}

	/**
	 * 
	 */
	public void closeVolatilTabs() {
		//
		for (int tabIndex = this.tabbedPaneCorpus.getTabCount() - 1; tabIndex >= 3; tabIndex--) {
			//
			this.tabbedPaneCorpus.remove(tabIndex);
		}
	}

	/**
	 * 
	 */
	public void dispose() {
		//
		this.frmPuck.dispose();
	}

	/**
	 * 
	 * @return
	 */
	public CorpusPanel getCorpusTab() {
		return this.corpusTab;
	}

	/**
	 * 
	 * @return
	 */
	public int getCurrentTabIndex() {
		int result;

		result = this.tabbedPaneCorpus.getSelectedIndex();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public FamiliesPanel getFamiliesTab() {
		return this.familiesTab;
	}

	/**
	 * 
	 * @return
	 */
	public IndividualsPanel getIndividualsTab() {
		return this.individualsTab;
	}

	/**
	 * 
	 * @return
	 */
	public JFrame getJFrame() {
		JFrame result;

		result = this.frmPuck;

		//
		return result;
	}

	/**
	 * 
	 * @param relationModel
	 * @return
	 */
	public RelationsPanel getRelationTab(final RelationModel relationModel) {
		RelationsPanel result;

		int tabIndex = this.getRelationTabIndex(relationModel);
		if (tabIndex == -1) {
			result = null;
		} else {
			result = (RelationsPanel) this.tabbedPaneCorpus.getComponent(tabIndex);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param relationModel
	 * @return
	 */
	public int getRelationTabIndex(final RelationModel relationModel) {
		int result;

		result = -1;
		boolean ended = false;
		int tabIndex = 0;
		while (!ended) {
			if (tabIndex < this.tabbedPaneCorpus.getComponentCount()) {
				Component tab = this.tabbedPaneCorpus.getComponent(tabIndex);
				if ((tab.getClass() == RelationsPanel.class) && (((RelationsPanel) tab).getRelationModel() == relationModel)) {
					ended = true;
					result = tabIndex;
				} else {
					tabIndex += 1;
				}
			} else {
				ended = true;
				result = -1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<RelationsPanel> getRelationTabs() {
		List<RelationsPanel> result;

		result = new ArrayList<RelationsPanel>();

		for (Component tab : this.tabbedPaneCorpus.getComponents()) {
			if (tab.getClass() == RelationsPanel.class) {
				result.add((RelationsPanel) tab);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public SegmentationPanel getSegmentationPanel() {
		return this.segmentationPanel;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.frmPuck = new JFrame();
		this.frmPuck.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent event) {
				logger.debug("Closing event.");
				performCloseWindow();
			}
		});
		this.frmPuck.setIconImage(Toolkit.getDefaultToolkit().getImage(MainWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		this.frmPuck.setTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.frmPuck.title")); //$NON-NLS-1$ //$NON-NLS-2$
		this.frmPuck.setBounds(50, 0, 1020, getDesktopHeight());

		this.frmPuck.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		JMenuBar menuBar = new JMenuBar();
		this.frmPuck.setJMenuBar(menuBar);

		JMenu mnFile = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnFile.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnFile.setMnemonic('F');
		menuBar.add(mnFile);

		JMenu mnNew = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnNew.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnFile.add(mnNew);

		JMenuItem mntmBlankCorpus = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmBlankCorpus.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmBlankCorpus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// New Blank Corpus.
				PuckGUI.instance().createNetGUI();
			}
		});
		mnNew.add(mntmBlankCorpus);

		JMenu mnRandomnetwork = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnRandomnetwork.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnNew.add(mnRandomnetwork);

		JMenuItem mntmClassic = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmClassic.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmClassic.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// New Random Corpus.
				RandomCorpusInputWindow window = new RandomCorpusInputWindow(MainWindow.this.netGUI);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});
		mnRandomnetwork.add(mntmClassic);

		JMenuItem mntmTelmo = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmTelmo.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmTelmo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// New Random Corpus.
				RandomCorpusMASInputWindow window = new RandomCorpusMASInputWindow(MainWindow.this.netGUI);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});
		mnRandomnetwork.add(mntmTelmo);

		JMenuItem mntmVariationsbc = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmVariationsbc.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmVariationsbc.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				try {
					Report report = RandomNetReporter.ReportMASVariation();
					MainWindow.this.netGUI.addReportTab(report);
				} catch (PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnRandomnetwork.add(mntmVariationsbc);

		JMenuItem mntmEmptyAllianceNetwork = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmEmptyAllianceNetwork.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmEmptyAllianceNetwork.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
		mntmEmptyAllianceNetwork.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// New Alliance Network.
				try {
					//
					PuckGUI.instance().createGroupNetGUI(
							new Graph<Cluster<Individual>>(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("puckgui.defaultFileName")));

				} catch (PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnNew.add(mntmEmptyAllianceNetwork);

		JMenuItem mntmOpen = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmOpen.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmOpen.setToolTipText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmOpen.toolTipText")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		mntmOpen.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Open…
				//
				File file = selectFile(MainWindow.this.frmPuck, MainWindow.this.netGUI.getFile().getParentFile());

				//
				openSelectedFile(MainWindow.this.netGUI, MainWindow.this.frmPuck, file, PuckManager.DEFAULT_CHARSET_NAME);
			}
		});
		mnFile.add(mntmOpen);

		this.mntmRecent = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnNewMenu.text"));
		this.mntmRecent.addMenuListener(new MenuListener() {
			@Override
			public void menuCanceled(final MenuEvent event) {
			}

			@Override
			public void menuDeselected(final MenuEvent event) {
				// OpenRecent menu cleaning.
				JMenu openRecentMenu = (JMenu) event.getSource();
				openRecentMenu.removeAll();
			}

			@Override
			public void menuSelected(final MenuEvent event) {
				// OpenRecent menu updating.
				JMenu openRecentMenu = (JMenu) event.getSource();
				for (File file : PuckGUI.instance().recentFiles()) {
					openRecentMenu.add(new OpenRecentMenuItem(MainWindow.this.frmPuck, MainWindow.this.netGUI, file));
				}
			}
		});
		mnFile.add(this.mntmRecent);

		JMenu mnOpenRecentFolder = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnOpenFolder.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnOpenRecentFolder.addMenuListener(new MenuListener() {
			@Override
			public void menuCanceled(final MenuEvent event) {
			}

			@Override
			public void menuDeselected(final MenuEvent event) {
				// OpenRecentFolder menu cleaning.
				JMenu openRecentFolderMenu = (JMenu) event.getSource();
				openRecentFolderMenu.removeAll();
			}

			@Override
			public void menuSelected(final MenuEvent event) {
				// OpenRecentFodler menu updating.
				JMenu openRecentFolderMenu = (JMenu) event.getSource();
				for (File folder : PuckGUI.instance().recentFolders()) {
					openRecentFolderMenu.add(new OpenRecentFolderMenuItem(MainWindow.this.frmPuck, MainWindow.this.netGUI, folder));
				}
			}
		});
		mnFile.add(mnOpenRecentFolder);

		this.mntmRevert = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmRevert.text"));
		this.mntmRevert.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
		this.mntmRevert.setToolTipText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmRevert.toolTipText")); //$NON-NLS-1$ //$NON-NLS-2$
		this.mntmRevert.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Revert.
				try {
					if (MainWindow.this.netGUI.isChanged()) {
						//
						int response = JOptionPane.showConfirmDialog(MainWindow.this.frmPuck, "All changes will be lost. Confirm revert of this corpus?",
								"Revert confirm", JOptionPane.YES_NO_OPTION);

						if (response == JOptionPane.YES_OPTION) {
							//
							MainWindow.this.netGUI.revert();
						}
					} else {
						//
						MainWindow.this.netGUI.revert();
					}

				} catch (final PuckException exception) {
					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case FILE_NOT_FOUND:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.fileNotFound");
						break;
						case BAD_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.badFileFormat");
						break;
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.unsupportedFileFormat");
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.notAFile");
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.unsupportedEncoding");
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.default");
					}

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JMenuItem mntmOpenEncoding = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmOpenEncoding.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmOpenEncoding.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Open Encoding.
				//
				String charsetName = OpenEncodingWindow.showDialog();
				logger.debug("charset=[" + charsetName + "]");

				//
				if (charsetName != null) {
					//
					File file = selectFile(MainWindow.this.frmPuck, MainWindow.this.netGUI.getFile().getParentFile());

					//
					openSelectedFile(MainWindow.this.netGUI, MainWindow.this.frmPuck, file, charsetName);
				}
			}
		});
		mnFile.add(mntmOpenEncoding);

		JMenuItem mntmOpenFromKinsources = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmOpenFromKinsources.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmOpenFromKinsources.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Open from Kinsources.
				try {
					//
					CatalogItem criteria = KinsourcesCatalogSelectorDialog.showDialog();

					//
					if (criteria != null) {
						//
						logger.info("Download Kinsources dataset file [{}][{}]", criteria.getId(), criteria.getName());

						Net newNet = KinsourcesFile.load(criteria.getId());

						File newFile = new File(newNet.getLabel());

						//
						if (MainWindow.this.netGUI.isBlank()) {
							//
							MainWindow.this.netGUI.setFile(newFile);
							MainWindow.this.netGUI.setNet(newNet);

						} else {
							//
							PuckGUI.instance().createNetGUI(newFile, newNet);
						}
					}

				} catch (final PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnFile.add(mntmOpenFromKinsources);
		mnFile.add(this.mntmRevert);

		JSeparator separator_4 = new JSeparator();
		mnFile.add(separator_4);

		this.mntmSave = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmSave.text"));
		this.mntmSave.setToolTipText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmSave.toolTipText")); //$NON-NLS-1$ //$NON-NLS-2$
		this.mntmSave.setEnabled(false);
		this.mntmSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		this.mntmSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Save net.
				try {
					File targetFile = MainWindow.this.netGUI.getFile();

					if (ToolBox.getExtension(targetFile) == null) {
						//
						targetFile = CorpusSaveFileSelector.showSelectorDialog(MainWindow.this.frmPuck, targetFile);
					}

					if (targetFile != null) {
						//
						PuckManager.saveNet(targetFile, MainWindow.this.netGUI.getNet());

						//
						MainWindow.this.netGUI.setFile(targetFile);

						//
						MainWindow.this.netGUI.setChanged(false);

						//
						PuckGUI.instance().recentFiles().updateFile(targetFile);
						PuckGUI.instance().recentFolders().updateFolder(targetFile.getParentFile());
					}
				} catch (final PuckException exception) {
					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedFileFormat");
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.notAFile");
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedEncoding");
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.default") + "\n"
									+ exception.getMessage();
					}

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JMenuItem mntmFuse = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmFuse.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmFuse.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Fuse.
				FuseInputWindow window = new FuseInputWindow(MainWindow.this.netGUI);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});
		mnFile.add(mntmFuse);

		JMenuItem mntmUpdateOverwriting = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmUpdateOverwriting.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmUpdateOverwriting.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Update Overwriting.
				try {
					File file = selectFile(MainWindow.this.frmPuck, null);

					//
					if (file != null) {
						//
						Report report = new Report("Update report");
						Net updatedNet = PuckManager.updateNetOverwriting(MainWindow.this.netGUI.getNet(), file, report);

						//
						NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), updatedNet);
						newNetGui.setChanged(true, "-overwritten");
						if (report.outputs().isNotEmpty()) {
							newNetGui.addReportTab(report);
						}
					}
				} catch (final PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnFile.add(mntmUpdateOverwriting);

		JMenuItem mntmUpdateAppending = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmUpdateAppending.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmUpdateAppending.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Update Appending.
				try {
					File file = selectFile(MainWindow.this.frmPuck, null);

					//
					if (file != null) {
						//
						Report report = new Report("Update report");
						Net updatedNet = PuckManager.updateNetAppending(MainWindow.this.netGUI.getNet(), file, report);

						//
						NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), updatedNet);
						newNetGui.setChanged(true, "-appended");
						if (report.outputs().isNotEmpty()) {
							newNetGui.addReportTab(report);
						}
					}
				} catch (final PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnFile.add(mntmUpdateAppending);

		JSeparator separator_5 = new JSeparator();
		mnFile.add(separator_5);

		mnFile.add(this.mntmSave);

		JMenuItem mntmSaveAs = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmSaveAs.text"));
		mntmSaveAs.setToolTipText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmSaveAs.toolTipText")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmSaveAs.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Save As.
				performSaveAs(MainWindow.this.frmPuck, MainWindow.this.netGUI);
			}
		});

		mnFile.add(mntmSaveAs);

		JMenuItem mntmSaveACopy = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmSaveACopy.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmSaveACopy.setToolTipText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmSaveACopy.toolTipText")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmSaveACopy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Save a copy.
				try {
					//
					File targetFile = CorpusSaveFileSelector.showSelectorDialog(MainWindow.this.frmPuck, MainWindow.this.netGUI.getFile());

					if (targetFile != null) {
						//
						PuckManager.saveNet(targetFile, MainWindow.this.netGUI.getNet());

						//
						PuckGUI.instance().recentFiles().updateFile(targetFile);
						PuckGUI.instance().recentFolders().updateFolder(targetFile.getParentFile());
					}
				} catch (final PuckException exception) {
					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedFileFormat");
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.notAFile");
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedEncoding");
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.default") + "\n"
									+ exception.getMessage();
					}

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnFile.add(mntmSaveACopy);

		JMenuItem mntmClose = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmClose.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmClose.setToolTipText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmClose.toolTipText")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK));
		mntmClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Perform Close Corpus.
				performCloseCorpus();
			}
		});

		JSeparator separator = new JSeparator();
		mnFile.add(separator);

		JMenuItem mntmExportToPajek = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmExportToPajek.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmExportToPajek.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Export to Pajek.
				// Linking Chains
				try {
					//
					PajekExportCriteria criteria = PajekExportInputDialog.showDialog(MainWindow.this.netGUI.getFile(),
							IndividualValuator.getAttributeLabels(MainWindow.this.netGUI.getNet().individuals()));

					//
					if (criteria != null) {
						PAJFile.exportToPajek(MainWindow.this.netGUI.getNet(), criteria.getTargetFileName(), criteria.getGraphType(),
								criteria.getPartitionLabelsNotBlank());
					}
				} catch (final PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JMenuItem mntmExportBAR = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmExportOds.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmExportBAR.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Export BAR.
				performBARSave();
			}
		});

		JMenuItem mntmImportDat = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmImportDat.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmImportDat.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Import DAT.
				try {
					//
					File file = selectDATFileToLoad(MainWindow.this.frmPuck, MainWindow.this.netGUI.getFile());

					//
					if (file != null) {
						//
						Net net = DAT1File.load(file);

						//
						if (MainWindow.this.netGUI.isBlank()) {
							MainWindow.this.netGUI.setFile(file);
							MainWindow.this.netGUI.setNet(net);
						} else {
							PuckGUI.instance().createNetGUI(file, net);
						}
					}
				} catch (final PuckException exception) {
					exception.printStackTrace();

					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case FILE_NOT_FOUND:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.fileNotFound");
						break;
						case BAD_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.badFileFormat");
						break;
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.unsupportedFileFormat");
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.notAFile");
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.unsupportedEncoding");
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.default");
					}

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnFile.add(mntmImportDat);

		JMenuItem mntmImportSelz = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmImportOds.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmImportSelz.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Import Selz.
				try {
					//
					// File file = selectFile(frmPuck, netGUI.getFile());
					File file = selectSelzFileToLoad(MainWindow.this.frmPuck, MainWindow.this.netGUI.getFile());

					//
					if (file != null) {
						//
						Net net = SELZFile.load(file, PuckManager.DEFAULT_CHARSET_NAME);

						//
						if (MainWindow.this.netGUI.isBlank()) {
							MainWindow.this.netGUI.setFile(file);
							MainWindow.this.netGUI.setNet(net);
						} else {
							PuckGUI.instance().createNetGUI(file, net);

						}
					}
				} catch (final PuckException exception) {
					exception.printStackTrace();

					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case FILE_NOT_FOUND:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.fileNotFound");
						break;
						case BAD_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.badFileFormat");
						break;
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.unsupportedFileFormat");
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.notAFile");
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.unsupportedEncoding");
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.default");
					}

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnFile.add(mntmImportSelz);

		JMenuItem mntmImportDeluz = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmImportDeluz.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmImportDeluz.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {

				// Import Deluz.
				try {
					//
					// File file = selectFile(frmPuck, netGUI.getFile());
					File file = selectFolder(MainWindow.this.frmPuck, MainWindow.this.netGUI.getFile().getParentFile());
					String relationModelName = "SEGMENT";
					String defaultRoleName = "MEMBER";
					Report report = new Report("Import");

					//
					if (file != null) {
						//
						Net net = DELUZFile.load(file, relationModelName, defaultRoleName, report);

						//
						if (MainWindow.this.netGUI.isBlank()) {
							MainWindow.this.netGUI.setFile(file);
							MainWindow.this.netGUI.setNet(net);
						} else {
							PuckGUI.instance().createNetGUI(file, net);

						}
						MainWindow.this.netGUI.addReportTab(report);

					}
				} catch (final PuckException exception) {
					exception.printStackTrace();

					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case FILE_NOT_FOUND:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.fileNotFound");
						break;
						case BAD_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.badFileFormat");
						break;
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.unsupportedFileFormat");
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.notAFile");
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.unsupportedEncoding");
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.default");
					}

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}

		});
		mnFile.add(mntmImportDeluz);

		JMenuItem mntmImportIurtxts = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmImportIurtxts.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmImportIurtxts.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Import IURTXTS
				try {
					File file = ImportIURTXTSFileSelector.showSelectorDialog(MainWindow.this.frmPuck, null);

					//
					if (file != null) {
						Net net = IURTXTSplittedFile.doImport(file);

						if (net != null) {
							//
							if (MainWindow.this.netGUI.isBlank()) {
								MainWindow.this.netGUI.setFile(file);
								MainWindow.this.netGUI.setNet(net);
							} else {
								PuckGUI.instance().createNetGUI(file, net);

							}
						}
					}
				} catch (final PuckException exception) {
					exception.printStackTrace();

					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case FILE_NOT_FOUND:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.fileNotFound");
						break;
						case BAD_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.badFileFormat");
						break;
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.unsupportedFileFormat");
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.notAFile");
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.unsupportedEncoding");
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.default");
					}

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnFile.add(mntmImportIurtxts);
		mnFile.add(mntmExportBAR);
		mnFile.add(mntmExportToPajek);

		JMenuItem mntmExportToIurs = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmExportToIurs.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmExportToIurs.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Export IURS.
				try {
					//
					File targetFile = ExportIURTXTSFileSelector.showSelectorDialog(MainWindow.this.frmPuck, MainWindow.this.netGUI.getFile());

					if (targetFile != null) {
						//
						IURTXTSplittedFile.export(targetFile, MainWindow.this.netGUI.getNet());
					}
				} catch (final PuckException exception) {
					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedFileFormat");
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.notAFile");
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedEncoding");
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.default");
					}

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}

			}
		});
		mnFile.add(mntmExportToIurs);

		JSeparator separator_1 = new JSeparator();
		mnFile.add(separator_1);
		mnFile.add(mntmClose);

		JMenuItem mntmQuit = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmQuit.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmQuit.setToolTipText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmQuit.toolTipText")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmQuit.setMnemonic('Q');
		mntmQuit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				logger.debug("Quit action.");

				performQuit();
			}
		});

		JSeparator separator_7 = new JSeparator();
		mnFile.add(separator_7);
		mnFile.add(mntmQuit);

		JMenu mnEdit = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnNewMenu.text_1"));
		mnEdit.setMnemonic('E');
		menuBar.add(mnEdit);

		JMenuItem mntmCut = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmCut.text"));
		mntmCut.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_CUT, 0));
		mntmCut.setEnabled(false);
		mnEdit.add(mntmCut);

		JMenuItem mntmCopy = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmCopy.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmCopy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_COPY, 0));
		mntmCopy.setEnabled(false);
		mnEdit.add(mntmCopy);

		JMenuItem mntmPaste = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmPaste.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmPaste.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_PASTE, 0));
		mntmPaste.setEnabled(false);
		mnEdit.add(mntmPaste);

		JMenuItem mntmDelete = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmDelete.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmDelete.setEnabled(false);
		mnEdit.add(mntmDelete);

		JSeparator separator_2 = new JSeparator();
		mnEdit.add(separator_2);

		JMenuItem mntmPreferences = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmPreferences.text"));
		mntmPreferences.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Preferences.
				PreferencesWindow preferences = new PreferencesWindow();
				preferences.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				preferences.setVisible(true);
			}
		});

		JMenuItem mntmAddIndividual = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAddIndividual.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmAddIndividual.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_MASK));
		mntmAddIndividual.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Add Individual.
				MainWindow.this.individualsTab.addIndividual();
			}
		});
		mnEdit.add(mntmAddIndividual);

		JMenuItem mntmFind = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmFind.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmFind.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Find.
				switch (MainWindow.this.tabbedPaneCorpus.getSelectedIndex()) {
					case 1:
						MainWindow.this.individualsTab.setFocusOnFind();
					break;
					case 2:
						MainWindow.this.familiesTab.setFocusOnFind();
					break;
					default:
				}
			}
		});

		JMenuItem mntmAddPartner = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAddPartner.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmAddPartner.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK));
		mntmAddPartner.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Add partner (menu item).
				switch (MainWindow.this.tabbedPaneCorpus.getSelectedIndex()) {
					case 1:
						MainWindow.this.individualsTab.addPartner();
					break;
					default:
				}
			}
		});

		JMenuItem mntmAddOriginFamily = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAddOriginFamily.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmAddOriginFamily.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Add Original Family.
				switch (MainWindow.this.tabbedPaneCorpus.getSelectedIndex()) {
					case 1:
						MainWindow.this.individualsTab.addOriginFamily();
					break;
					default:
				}
			}
		});
		mntmAddOriginFamily.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, InputEvent.CTRL_MASK));
		mnEdit.add(mntmAddOriginFamily);
		mnEdit.add(mntmAddPartner);

		JMenuItem mntmAddChild = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAddChild.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmAddChild.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Add child (menu item).
				switch (MainWindow.this.tabbedPaneCorpus.getSelectedIndex()) {
					case 1:
						MainWindow.this.individualsTab.addChild();
					break;
					case 2:
						MainWindow.this.familiesTab.addChild();
					break;
					default:
				}
			}
		});
		mntmAddChild.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_MASK));
		mnEdit.add(mntmAddChild);

		JMenuItem mntmAddFamily = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAddFamily.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmAddFamily.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Add family.
				MainWindow.this.familiesTab.addFamily();
			}
		});
		mntmAddFamily.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		mnEdit.add(mntmAddFamily);

		JMenuItem mntmAddRelationModel = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmNewMenuItem.text_1")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmAddRelationModel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Add Relation Model.
				RelationModelInputWindow window = new RelationModelInputWindow(MainWindow.this.netGUI, null);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});
		mnEdit.add(mntmAddRelationModel);

		JMenuItem mntmEditGeography = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmEditGeography.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmEditGeography.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_MASK));
		mntmEditGeography.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Edit geography.

				//
				// for (Component tab : this.tabbedPaneCorpus.getComponents()) {
				// if (tab.getClass() == RelationsPanel.class) {
				// result.add((RelationsPanel) tab);
				// }
				// }

				if (!isGeographyEditorTab()) {
					GeographyEditorPanel panel = new GeographyEditorPanel(MainWindow.this.netGUI);
					addTab("Geography editor", panel);
				}
			}
		});
		mnEdit.add(mntmEditGeography);

		JMenuItem menuItem_8 = new JMenuItem("Convert Geography");
		mnEdit.add(menuItem_8);
		mntmFind.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_MASK));
		mnEdit.add(mntmFind);

		JSeparator separator_9 = new JSeparator();
		mnEdit.add(separator_9);
		mnEdit.add(mntmPreferences);

		JMenu mnCorpus = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnCorpus.text")); //$NON-NLS-1$ //$NON-NLS-2$
		menuBar.add(mnCorpus);

		JMenuItem mntmListById = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmListById.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmListById.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				List<Sorting> sorting = new ArrayList<Sorting>();
				sorting.add(Sorting.ID);
				Report report = StatisticsReporter.reportIndividuals(MainWindow.this.netGUI.getSegmentation(), sorting, null);
				MainWindow.this.netGUI.addReportTab(report);
			}
		});
		mnCorpus.add(mntmListById);

		JMenuItem mntmListByFirst = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmListByFirst.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmListByFirst.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				List<Sorting> sorting = new ArrayList<Sorting>();
				sorting.add(Sorting.FIRSTN);
				sorting.add(Sorting.LASTN);
				sorting.add(Sorting.ID);
				Report report = StatisticsReporter.reportIndividuals(MainWindow.this.netGUI.getSegmentation(), sorting, null);
				MainWindow.this.netGUI.addReportTab(report);
			}
		});
		mnCorpus.add(mntmListByFirst);

		JMenuItem mntmListByLast = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmListByLast.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmListByLast.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				List<Sorting> sorting = new ArrayList<Sorting>();
				sorting.add(Sorting.LASTN);
				sorting.add(Sorting.FIRSTN);
				sorting.add(Sorting.ID);
				Report report = StatisticsReporter.reportIndividuals(MainWindow.this.netGUI.getSegmentation(), sorting, null);
				MainWindow.this.netGUI.addReportTab(report);
			}
		});
		mnCorpus.add(mntmListByLast);

		JMenuItem mntmListByClusters = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmListByClusters.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmListByClusters.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				ListByClusterInputWindow window = new ListByClusterInputWindow(MainWindow.this.netGUI);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});

		JMenuItem mntmListByLast_1 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmListByLast_1.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmListByLast_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				List<String> details = new ArrayList<String>();
				details.add("BAP_DATE");
				details.add("PARENTS");
				details.add("PARTNERS");
				details.add("MARR_DATE");
				details.add("CHILDREN");
				details.add("FIRST_CHILD_BAP_DATE");
				details.add("FIRST_CHILD_FIRST_MARR_DATE");
				List<Sorting> sorting = new ArrayList<Sorting>();
				sorting.add(Sorting.LASTN);
				sorting.add(Sorting.FIRSTN);
				sorting.add(Sorting.ID);
				Report report = StatisticsReporter.reportIndividuals(MainWindow.this.netGUI.getSegmentation(), sorting, details);
				MainWindow.this.netGUI.addReportTab(report);
			}
		});
		mnCorpus.add(mntmListByLast_1);
		mnCorpus.add(mntmListByClusters);

		JMenuItem mntmListRelatives = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmListRelatives.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmListRelatives.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				ListRelativesByClusterInputWindow window = new ListRelativesByClusterInputWindow(MainWindow.this.netGUI);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});
		mnCorpus.add(mntmListRelatives);

		JMenuItem mntmExportFamilyIds = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmExportFamilyIds.text"));
		mntmExportFamilyIds.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Export family Ids by Husband.
				Report report = StatisticsReporter.reportFamiliesByHusband(MainWindow.this.netGUI.getSegmentation());
				MainWindow.this.netGUI.addReportTab(report);
			}
		});

		JMenuItem mntmListMissingRelatives = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmListMissingRelatives.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmListMissingRelatives.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				ListMissingRelativesByClusterInputWindow window = new ListMissingRelativesByClusterInputWindow(MainWindow.this.netGUI);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});
		mnCorpus.add(mntmListMissingRelatives);
		mnCorpus.add(mntmExportFamilyIds);

		JMenuItem mntmListFamiliesBy = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmListFamiliesBy.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmListFamiliesBy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Export family Ids by Wife.
				Report report = StatisticsReporter.reportFamiliesByWife(MainWindow.this.netGUI.getSegmentation());
				MainWindow.this.netGUI.addReportTab(report);
			}
		});
		mnCorpus.add(mntmListFamiliesBy);

		JMenuItem mntmHomonyms = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmHomonyms.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmHomonyms.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// List homonyms.
				HomonymReportInputWindow window = new HomonymReportInputWindow(MainWindow.this.netGUI);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});

		JSeparator separator_6 = new JSeparator();
		mnCorpus.add(separator_6);
		mnCorpus.add(mntmHomonyms);

		JMenuItem mntmHomonymsunions = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmHomonymsunions.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmHomonymsunions.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// List union homonyms.
				Report report = StatisticsReporter.reportUnionHomonyms(MainWindow.this.netGUI.getNet(), 2);
				MainWindow.this.netGUI.addReportTab(report);
			}
		});
		mnCorpus.add(mntmHomonymsunions);

		JMenuItem mntmNameSeries = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmNameSeries.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmNameSeries.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				// List agnatic name series.
				Report report = StatisticsReporter.reportNameSeries(MainWindow.this.netGUI.getNet());
				MainWindow.this.netGUI.addReportTab(report);
			}
		});
		mnCorpus.add(mntmNameSeries);

		JMenu mnControls = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnControls.text"));
		mnCorpus.add(mnControls);

		JMenuItem mntmControl = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmControl.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnControls.add(mntmControl);

		JSeparator separator_8 = new JSeparator();
		mnControls.add(separator_8);

		JMenuItem mntmControlSameSex = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmControlSameSex.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnControls.add(mntmControlSameSex);

		JMenuItem mntmControlFemaleFathers = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmControlFemaleFathers.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnControls.add(mntmControlFemaleFathers);

		JMenuItem mntmMultipleFathersOr = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmMultipleFathersOr.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmMultipleFathersOr.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Control Female Fathers or Male Mothers.
				Report report = ControlReporter.reportMultipleFathersOrMothers(MainWindow.this.netGUI.getSegmentation(),
						ResourceBundle.getBundle("org.tip.puckgui.messages"));

				if (report.status() == 0) {
					//
					String title = "Control special features report";
					String message = "No special features found.";

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);
				} else {
					//
					MainWindow.this.netGUI.addReportTab(report);
				}
			}
		});
		mnControls.add(mntmMultipleFathersOr);

		JMenuItem mntmControlCyclicDescent = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmControlCyclicDescent.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnControls.add(mntmControlCyclicDescent);

		JMenuItem mntmControlUnknownSex = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmControlUnknownSex.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnControls.add(mntmControlUnknownSex);

		JMenuItem mntmUnknownSexParents = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmUnknownSexParents.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmUnknownSexParents.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Control Unknown Sex Parents or Spouses.
				Report report = ControlReporter.reportUnknownSexParentsSpouses(MainWindow.this.netGUI.getSegmentation(),
						ResourceBundle.getBundle("org.tip.puckgui.messages"));

				if (report.status() == 0) {
					//
					String title = "Control special features report";
					String message = "No special features found.";

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);
				} else {
					MainWindow.this.netGUI.addReportTab(report);
				}
			}
		});
		mnControls.add(mntmUnknownSexParents);

		JMenuItem mntmControlNamelessPersons = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmControlNamelessPersons.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnControls.add(mntmControlNamelessPersons);

		JMenuItem mntmControlParentchildMarriages = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmControlParentchildMarriages.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnControls.add(mntmControlParentchildMarriages);

		JMenuItem mntmAutomarriage = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAutomarriage.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmAutomarriage.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Control Auto-marriages.
				Report report = ControlReporter.reportAutoMarriages(MainWindow.this.netGUI.getSegmentation(),
						ResourceBundle.getBundle("org.tip.puckgui.messages"));

				if (report.status() == 0) {
					//
					String title = "Control special features report";
					String message = "No special features found.";

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);
				} else {
					//
					MainWindow.this.netGUI.addReportTab(report);
				}
			}
		});
		mnControls.add(mntmAutomarriage);

		JMenuItem mntmInconsistentDates = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmInconsistentDates.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmInconsistentDates.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Control Inconsistent Dates.
				Report report = ControlReporter.reportInconsistentDates(MainWindow.this.netGUI.getSegmentation(),
						ResourceBundle.getBundle("org.tip.puckgui.messages"));

				if (report.status() == 0) {
					//
					String title = "Control special features report";
					String message = "No special features found.";

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);
				} else {
					MainWindow.this.netGUI.addReportTab(report);
				}
			}
		});
		mnControls.add(mntmInconsistentDates);

		JMenuItem mntmMissingDatesCompact = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmMissingDates.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmMissingDatesCompact.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Control Missing Dates in Compact Format.
				Report report = ControlReporter.reportMissingDatesCompact(MainWindow.this.netGUI.getSegmentation(),
						ResourceBundle.getBundle("org.tip.puckgui.messages"));

				if (report.status() == 0) {
					//
					String title = "Control special features report";
					String message = "No special features found.";

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);
				} else {
					MainWindow.this.netGUI.addReportTab(report);
				}
			}
		});

		JMenuItem mntmMissingDates = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmMissingDates_1.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmMissingDates.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Control Missing Dates.
				Report report = ControlReporter.reportMissingDates(MainWindow.this.netGUI.getSegmentation(),
						ResourceBundle.getBundle("org.tip.puckgui.messages"));

				if (report.status() == 0) {
					//
					String title = "Control special features report";
					String message = "No special features found.";

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);
				} else {
					MainWindow.this.netGUI.addReportTab(report);
				}
			}
		});
		mnControls.add(mntmMissingDates);
		mnControls.add(mntmMissingDatesCompact);
		
		JSeparator separator_27 = new JSeparator();
		mnControls.add(separator_27);
		
		JMenuItem menuItem_1 = new JMenuItem("Unknown Places");
		menuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Report report = ControlReporter.reportUnknownPlaces(netGUI.getSegmentation(), netGUI.getNet().getGeography(), ResourceBundle.getBundle("org.tip.puckgui.messages"));

				if (report.status() == 0) {
					//
					String title = "Control special features report";
					String message = "No special features found.";

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);
				} else {
					//
					netGUI.addReportTab(report);
				}

			}
		});
		mnControls.add(menuItem_1);
		
		JMenuItem menuItem_2 = new JMenuItem("Unknown Places\u2026");
		menuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {

					//
					UnknownPlacesCriteria criteria = UnknownPlacesInputDialog.showDialog(MainWindow.this.netGUI.getNet().relationModels().nameList());

					//
					if (criteria != null) {
						//
						logger.debug("criteria=" + criteria.toString());

						//
						Report report = ControlReporter.reportUnknownPlaces(MainWindow.this.netGUI.getSegmentation(), netGUI.getNet().getGeography(), criteria, ResourceBundle.getBundle("org.tip.puckgui.messages"));

						//
						MainWindow.this.netGUI.addReportTab(report);
					}
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnControls.add(menuItem_2);

		mntmControlParentchildMarriages.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Control Parent-Child marriages.
				Report report = ControlReporter.reportParentChildMarriages(MainWindow.this.netGUI.getSegmentation(),
						ResourceBundle.getBundle("org.tip.puckgui.messages"));

				if (report.status() == 0) {
					//
					String title = "Control special features report";
					String message = "No special features found.";

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);
				} else {
					//
					MainWindow.this.netGUI.addReportTab(report);
				}
			}
		});
		mntmControlNamelessPersons.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Control Nameless persons.
				Report report = ControlReporter.reportNamelessPersons(MainWindow.this.netGUI.getSegmentation(),
						ResourceBundle.getBundle("org.tip.puckgui.messages"));

				if (report.status() == 0) {
					//
					String title = "Control special features report";
					String message = "No special features found.";

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);
				} else {
					//
					MainWindow.this.netGUI.addReportTab(report);
				}
			}
		});
		mntmControlUnknownSex.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Control Unknown Sex Persons.
				Report report = ControlReporter.reportUnknownSexPersons(MainWindow.this.netGUI.getSegmentation(),
						ResourceBundle.getBundle("org.tip.puckgui.messages"));

				if (report.status() == 0) {
					//
					String title = "Control special features report";
					String message = "No special features found.";

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);
				} else {
					MainWindow.this.netGUI.addReportTab(report);
				}
			}
		});
		mntmControlCyclicDescent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Control Cyclic Descent cases.
				Report report = ControlReporter.reportCyclicDescentCases(MainWindow.this.netGUI.getSegmentation(),
						ResourceBundle.getBundle("org.tip.puckgui.messages"));

				if (report.status() == 0) {
					//
					String title = "Control special features report";
					String message = "No special features found.";

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);
				} else {
					//
					MainWindow.this.netGUI.addReportTab(report);
				}
			}
		});
		mntmControlFemaleFathers.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Control Female Fathers or Male Mothers.
				Report report = ControlReporter.reportFemaleFathersOrMaleMothers(MainWindow.this.netGUI.getSegmentation(),
						ResourceBundle.getBundle("org.tip.puckgui.messages"));

				if (report.status() == 0) {
					//
					String title = "Control special features report";
					String message = "No special features found.";

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);
				} else {
					//
					MainWindow.this.netGUI.addReportTab(report);
				}
			}
		});
		mntmControlSameSex.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Control Same-Sex persons.
				Report report = ControlReporter.reportSameSexSpouses(MainWindow.this.netGUI.getSegmentation(),
						ResourceBundle.getBundle("org.tip.puckgui.messages"));

				if (report.status() == 0) {
					//
					String title = "Control special features report";
					String message = "No special features found.";

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);
				} else {
					//
					MainWindow.this.netGUI.addReportTab(report);
				}
			}
		});
		mntmControl.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Control special features.
				ControlInputWindow window = new ControlInputWindow(MainWindow.this.netGUI);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});

		JMenuItem mntmAttributesStatistics = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAttributesStatistics.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmAttributesStatistics.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Attribute statistics.
				//
				Report report;
				if (MainWindow.this.netGUI.getSegmentation().isAtTheTop()) {
					report = StatisticsReporter.reportAttributeStatistics(MainWindow.this.netGUI.getNet());
				} else {
					report = StatisticsReporter.reportAttributeStatistics(MainWindow.this.netGUI.getSegmentation());
				}

				//
				MainWindow.this.netGUI.addReportTab(report);
			}
		});

		JMenuItem mntmTemp = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmTemp.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmTemp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				Report report = ControlReporter.reportIncoherentKinAttributes(MainWindow.this.netGUI.getSegmentation());
				MainWindow.this.netGUI.addReportTab(report);
			}
		});
		mnCorpus.add(mntmTemp);

		JSeparator separator_24 = new JSeparator();
		mnCorpus.add(separator_24);
		mnCorpus.add(mntmAttributesStatistics);

		JMenuItem mntmSnowballStructure = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmSnowballStructure.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmSnowballStructure.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				// Snowball structure.
				try {
					
					RelationModels relationModels = MainWindow.this.netGUI.getNet().relationModels();
					
					AttributeDescriptors attributeDescriptors = AttributeWorker.getExogenousAttributeDescriptors(
							MainWindow.this.netGUI.getCurrentIndividuals(), null);

					//
					SnowballCriteria criteria = SnowballStructureInputDialog.showDialog(relationModels,attributeDescriptors);

					//
					if (criteria != null) {
						//
						logger.debug("criteria=" + criteria.toString());

						//
						Report report = NetReporter.reportSteps(MainWindow.this.netGUI.getSegmentation(), criteria);

						//
						MainWindow.this.netGUI.addReportTab(report);
					}
				} catch (final PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JMenuItem mntmAttributeValueStatistics = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAttributeValueStatistics.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmAttributeValueStatistics.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				// Attribute value statistics.
				//
				Report report;
				if (MainWindow.this.netGUI.getSegmentation().isAtTheTop()) {
					report = StatisticsReporter.reportAttributeAndValueStatistics(MainWindow.this.netGUI.getSegmentation());
				} else {
					report = StatisticsReporter.reportAttributeAndValueStatistics(MainWindow.this.netGUI.getSegmentation());
				}

				//
				MainWindow.this.netGUI.addReportTab(report);
			}
		});
		mnCorpus.add(mntmAttributeValueStatistics);

		JMenuItem mntmGeographyStatistics = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmGeographyStatistics.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmGeographyStatistics.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				// Geography Statistics.
				//
				Report report = StatisticsReporter.reportGeographyStatistics(MainWindow.this.netGUI.getNet());

				//
				MainWindow.this.netGUI.addReportTab(report);
			}
		});
		
		JMenuItem mntmAttributeValueStatistics_1 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAttributeValueStatistics_1.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmAttributeValueStatistics_1.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent event) {
				AttributeValueStatisticsByClusterInputWindow window = new AttributeValueStatisticsByClusterInputWindow(MainWindow.this.netGUI);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});
		mnCorpus.add(mntmAttributeValueStatistics_1);
		mnCorpus.add(mntmGeographyStatistics);

		JSeparator separator_25 = new JSeparator();
		mnCorpus.add(separator_25);
		mnCorpus.add(mntmSnowballStructure);

		JMenu mnTransform = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnTransform.text")); //$NON-NLS-1$ //$NON-NLS-2$
		menuBar.add(mnTransform);

		JMenuItem mntmToModel = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmToModel.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmToModel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {

				Report report = new Report("Import log");
				RelationModel model;
				try {
					model = RoleRelationMaker.create(MainWindow.this.netGUI.getNet(), report);
					MainWindow.this.netGUI.addRelationTab(model);
					if (report.outputs().isNotEmpty()) {
						MainWindow.this.netGUI.addReportTab(report);
					}

					//
					MainWindow.this.netGUI.setChanged(true);

					//
					MainWindow.this.netGUI.updateAll();

				} catch (PuckException exception) {
					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}

			}
		});
		mnTransform.add(mntmToModel);

		JSeparator separator_20 = new JSeparator();
		mnTransform.add(separator_20);

		JMenuItem mntmDuplicate = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmDuplicate.text"));
		mnTransform.add(mntmDuplicate);
		mntmDuplicate.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK));
		mntmDuplicate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Duplicate Corpus.
				try {
					//
					PuckGUI.instance().duplicate(MainWindow.this.netGUI);

				} catch (final PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JSeparator separator_26 = new JSeparator();
		mnTransform.add(separator_26);

		JMenu mnChangeNames = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnChangeNames.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnTransform.add(mnChangeNames);

		JMenuItem mntmAnonymizeByFirst = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAnonymizeByFirst.text_1")); //$NON-NLS-1$ //$NON-NLS-2$
		mnChangeNames.add(mntmAnonymizeByFirst);

		JMenuItem mntmAnonymizeByLast = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAnonymizeByLast.text_1")); //$NON-NLS-1$ //$NON-NLS-2$
		mnChangeNames.add(mntmAnonymizeByLast);

		JMenuItem mntmAnonymizeByGender = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAnonymizeByGender.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnChangeNames.add(mntmAnonymizeByGender);
		
		JMenuItem mntmAnonymizeByGender_1 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAnonymizeByGender_1.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmAnonymizeByGender_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Copy and anonymize BGI.
				//
				Net newNet = new Net(MainWindow.this.netGUI.getNet());
				Individuals currentIndividuals = newNet.individuals().getByIds(MainWindow.this.netGUI.getCurrentIndividuals().getIds());

				//
				AttributeWorker.anonymizeByGenderAndId(currentIndividuals, GenderCode.MF);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, "-anonymized");
			}
		});
		mnChangeNames.add(mntmAnonymizeByGender_1);

		JMenuItem mntmNumberNames_1 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmNumberNames_1.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnChangeNames.add(mntmNumberNames_1);
		mntmNumberNames_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Copy and number names.
				//
				Net targetNet = MainWindow.this.netGUI.getNet();
				Individuals currentIndividuals = targetNet.individuals().getByIds(MainWindow.this.netGUI.getCurrentIndividuals().getIds());

				//
				NetUtils.numberNames(currentIndividuals);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), targetNet);
				newNetGui.setChanged(true, "-names_numbered");
			}
		});
		mntmAnonymizeByGender.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Copy and anonymize BGI.
				//
				Net newNet = new Net(MainWindow.this.netGUI.getNet());
				Individuals currentIndividuals = newNet.individuals().getByIds(MainWindow.this.netGUI.getCurrentIndividuals().getIds());

				//
				AttributeWorker.anonymizeByGenderAndId(currentIndividuals, GenderCode.HF);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, "-anonymized");
			}
		});
		mntmAnonymizeByLast.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Copy and anonymize BFN.
				//
				Net newNet = new Net(MainWindow.this.netGUI.getNet());
				Individuals currentIndividuals = newNet.individuals().getByIds(MainWindow.this.netGUI.getCurrentIndividuals().getIds());

				//
				AttributeWorker.anonymizeByLastName(currentIndividuals);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, "-anonymized");
			}
		});
		mntmAnonymizeByFirst.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Copy and anonymize BFN.
				//
				Net targetNet = new Net(MainWindow.this.netGUI.getNet());
				Individuals currentIndividuals = targetNet.individuals().getByIds(MainWindow.this.netGUI.getCurrentIndividuals().getIds());

				//
				AttributeWorker.anonymizeByFirstName(currentIndividuals);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), targetNet);
				newNetGui.setChanged(true, "-anonymized");
			}
		});

		JMenuItem mntmNamesToAttributes = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmNamesToAttributes.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmNamesToAttributes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Names to attributes.
				//
				Net targetNet = MainWindow.this.netGUI.getNet();
				Individuals currentIndividuals = targetNet.individuals().getByIds(MainWindow.this.netGUI.getCurrentIndividuals().getIds());

				//
				NetUtils.namesToAttributes(currentIndividuals);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), targetNet);
				newNetGui.setChanged(true, "-names_as_attributes");
			}
		});
		mnTransform.add(mntmNamesToAttributes);
		
		JMenuItem mntmIdToAttributes = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmIdToAttributes.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmIdToAttributes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					// ID to attributes.
					//
					Net targetNet = MainWindow.this.netGUI.getNet();
					Individuals currentIndividuals = targetNet.individuals().getByIds(MainWindow.this.netGUI.getCurrentIndividuals().getIds());

					//
					NetUtils.idToAttributes(currentIndividuals);

					//
					NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), targetNet);
					newNetGui.setChanged(true, "-id_as_attributes");
				}
		});
		mnTransform.add(mntmIdToAttributes);

		JSeparator separator_18 = new JSeparator();
		mnTransform.add(separator_18);

		JMenu mnChangeAttributes = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnChangeAttributes.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnTransform.add(mnChangeAttributes);

		JMenuItem mntmRenameExogenousAttribute = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmRenameExogenousAttribute.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnChangeAttributes.add(mntmRenameExogenousAttribute);

		JMenuItem mntmSetExogenousAttribute = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmSetExogenousAttribute.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnChangeAttributes.add(mntmSetExogenousAttribute);

		JMenuItem mntmFilterAttribute = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmFilterAttribute.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnChangeAttributes.add(mntmFilterAttribute);

		JMenuItem mntmSetAttributeValue = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmSetAttributeValue.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnChangeAttributes.add(mntmSetAttributeValue);

		JMenuItem mntmReplaceAttributeValue = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmReplaceAttributeValue.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnChangeAttributes.add(mntmReplaceAttributeValue);

		JMenuItem mntmValuateExogeneousAttribute = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmValuateExogeneousAttribute.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnChangeAttributes.add(mntmValuateExogeneousAttribute);

		JMenuItem mntmRemoveAllAttributes = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmRemoveAllAttributes.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnChangeAttributes.add(mntmRemoveAllAttributes);
		mntmRemoveAllAttributes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Remove All Attributes.
				try {
					//
					AttributeRemoveAllInputDialog.Criteria criteria = AttributeRemoveAllInputDialog.showDialog();

					//
					if (criteria != null) {
						//
						Net newNet = new Net(MainWindow.this.netGUI.getNet());

						//
						Segmentation newSegmentation = new Segmentation(newNet, MainWindow.this.netGUI.getSegmentation());

						//
						long count;
						if (criteria == AttributeRemoveAllInputDialog.Criteria.CORPUS_INCLUDED) {
							//
							count = AttributeWorker.removeAllAttributes(newNet);

						} else {
							//
							count = AttributeWorker.removeAllAttributesExceptCorpus(newNet);
						}

						//
						String title = "Attribute Remove Count";
						String message = "Number of removed attributes: " + count;
						JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

						//
						if (count > 0) {
							//
							newSegmentation.refresh();
							NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet, newSegmentation);
							newNetGui.setChanged(true, "-attributeRemoved");
						}
					}
				} catch (final PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}

			}
		});
		mntmValuateExogeneousAttribute.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Valuate Exogeneous Attribute.
				//
				ValuateExogenousAttributeCriteria criteria = ValuateExogenousAttributeDialog.showDialog();

				//
				if (criteria != null) {
					//
					Net newNet = new Net(MainWindow.this.netGUI.getNet());

					//
					int count = AttributeWorker.valuateExogenousAttribute(newNet.individuals(), criteria.getSourceLabel(), criteria.getTargetLabel(),
							criteria.getValuePrefix(), newNet.getGeography());

					//
					String title = "Valuate Exogenous Attribute Count";
					String message = "Number of attribute touched: " + count;
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

					//
					if (count > 0) {
						NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
						newNetGui.setChanged(true, "-attributeValuated");
					}
				}
			}
		});
		mntmReplaceAttributeValue.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Replace attribute value.
				try {
					//
					List<String> relationModelNames = MainWindow.this.netGUI.getNet().relationModels().sortedNameList();
					AttributeDescriptors attributeDescriptors = AttributeWorker.getExogenousAttributeDescriptors(MainWindow.this.netGUI.getNet(),
							MainWindow.this.netGUI.getSegmentation(), null);
					AttributeReplaceValueCriteria criteria = AttributeReplaceValueInputDialog.showDialog(relationModelNames, attributeDescriptors);

					//
					if (criteria != null) {
						//
						Net newNet = new Net(MainWindow.this.netGUI.getNet());

						//
						Segmentation newSegmentation = new Segmentation(newNet, MainWindow.this.netGUI.getSegmentation());

						//
						long count = AttributeWorker.replaceAttributeValue(newNet, newSegmentation, criteria);

						//
						String title = "Attribute Replace Count";
						String message = "Number of attribute touched: " + count;
						JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

						//
						if (count > 0) {
							//
							newSegmentation.refresh();
							NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet, newSegmentation);
							newNetGui.setChanged(true, "-attributeReplace");
						}
					}
				} catch (final PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mntmSetAttributeValue.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Set attribute value.
				try {
					//
					List<String> relationModelNames = MainWindow.this.netGUI.getNet().relationModels().sortedNameList();
					AttributeDescriptors attributeDescriptors = AttributeWorker.getExogenousAttributeDescriptors(MainWindow.this.netGUI.getNet(),
							MainWindow.this.netGUI.getSegmentation(), null);
					AttributeSetValueCriteria criteria = AttributeSetValueInputDialog.showDialog(relationModelNames, attributeDescriptors);

					//
					if (criteria != null) {
						//
						Net newNet = new Net(MainWindow.this.netGUI.getNet());

						//
						Segmentation newSegmentation = new Segmentation(newNet, MainWindow.this.netGUI.getSegmentation());

						//
						long count = AttributeWorker.setAttributeValue(newNet, newSegmentation, criteria);

						//
						String title = "Attribute Set Count";
						String message = "Number of attribute touched: " + count;
						JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

						//
						if (count > 0) {
							//
							newSegmentation.refresh();
							NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet, newSegmentation);
							newNetGui.setChanged(true, "-attributeSet");
						}
					}
				} catch (final PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mntmFilterAttribute.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Filter attribute.
				try {
					//
					List<String> relationModelNames = MainWindow.this.netGUI.getNet().relationModels().sortedNameList();
					AttributeDescriptors attributeDescriptors = AttributeWorker.getExogenousAttributeDescriptors(MainWindow.this.netGUI.getNet(),
							MainWindow.this.netGUI.getSegmentation(), null);
					AttributeFilter filter = AttributeFilterInputDialog.showDialog(relationModelNames, attributeDescriptors);

					//
					if (filter != null) {
						//
						Net newNet = new Net(MainWindow.this.netGUI.getNet());

						//
						Segmentation newSegmentation = new Segmentation(newNet, MainWindow.this.netGUI.getSegmentation());

						//
						long count = AttributeWorker.filter(newNet, newSegmentation, filter);

						//
						String title = "Filter Attribute Count";
						String message = "Number of attribute touched: " + count;
						JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

						//
						if (count > 0) {
							NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet, newSegmentation);
							newNetGui.setChanged(true, "-attributeFilter");

						}
					}
				} catch (final PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		mntmRenameExogenousAttribute.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Rename attribute.
				try {
					//
					List<String> relationModelNames = MainWindow.this.netGUI.getNet().relationModels().sortedNameList();
					AttributeDescriptors attributeDescriptors = AttributeWorker.getExogenousAttributeDescriptors(MainWindow.this.netGUI.getNet(),
							MainWindow.this.netGUI.getSegmentation(), null);
					AttributeRenameCriteria criteria = AttributeRenameInputDialog.showDialog(relationModelNames, attributeDescriptors);

					//
					if (criteria != null) {
						//
						Net newNet = new Net(MainWindow.this.netGUI.getNet());

						//
						Segmentation newSegmentation = new Segmentation(newNet, MainWindow.this.netGUI.getSegmentation());

						//
						long count = AttributeWorker.renameAttribute(newNet, newSegmentation, criteria);

						//
						String title = "Attribute Rename Count";
						String message = "Number of attribute touched: " + count;
						JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

						//
						if (count > 0) {
							//
							newSegmentation.refresh();
							NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet, newSegmentation);
							newNetGui.setChanged(true, "-attributeRename");
						}
					}
				} catch (final PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}

			}
		});
		
		mntmSetExogenousAttribute.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Set attribute.
				try {
					//
/*					List<String> relationModelNames = MainWindow.this.netGUI.getNet().relationModels().sortedNameList();
					AttributeDescriptors attributeDescriptors = AttributeWorker.getExogenousAttributeDescriptors(MainWindow.this.netGUI.getNet(),
							MainWindow.this.netGUI.getSegmentation(), null);
					AttributeRenameCriteria xcriteria = AttributeRenameInputDialog.showDialog(relationModelNames, attributeDescriptors);*/
					List<Labels> modelsLabels = Segmentation.buildModelLabels(netGUI.getCurrentIndividuals(), netGUI.getCurrentFamilies(), netGUI.getNet()
							.relationModels(), netGUI.getNet().relations());
					PartitionCriteria criteria = PartitionCriteriaDialog.showDialog(modelsLabels);
					
					//
					if (criteria != null) {
						//
						Net newNet = new Net(MainWindow.this.netGUI.getNet());

						//
						Segmentation newSegmentation = new Segmentation(newNet, MainWindow.this.netGUI.getSegmentation());

						//
						long count = AttributeWorker.setEndogenousAttribute(newNet,newSegmentation,criteria);

						//
						String title = "Attribute Set Count";
						String message = "Number of attributes set: " + count;
						JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

						//
						if (count > 0) {
							//
							newSegmentation.refresh();
							NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet, newSegmentation);
							newNetGui.setChanged(true, "-attributeSet "+criteria.getLabel());
						}
					}
				} catch (final PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}

			}
		});


		JMenuItem mntmTransmitAttributeValues = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmTransmitAttributeValues.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmTransmitAttributeValues.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				// Transmit attribute values.
				try {
					//
					List<String> labels = IndividualValuator.getAttributeLabels(MainWindow.this.netGUI.getSegmentation().getCurrentIndividuals());

					//
					TransmitAttributeValueCriteria criteria = TransmitAttributeValueInputDialog.showDialog(labels);

					//
					if (criteria != null) {
						//
						Net newNet = new Net(MainWindow.this.netGUI.getNet());

						//
						Segmentation newSegmentation = new Segmentation(newNet, MainWindow.this.netGUI.getSegmentation());

						//
						int count = NetUtils.transmitAttributeValue(newSegmentation.getCurrentIndividuals(), criteria, newSegmentation.getGeography());

						//
						String title = "Transmit Attribute Value Count";
						String message = "Number of attribute transmitted: " + count;
						JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

						//
						if (count > 0) {
							newSegmentation.refresh();
							NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet, newSegmentation);
							newNetGui.setChanged(true, "-transmittedAttributeValue");
						}
					}

				} catch (final PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnTransform.add(mntmTransmitAttributeValues);

		JMenuItem mntmAttributeToRelation = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAttributeToRelation.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmAttributeToRelation.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Transform attribute to relation.
				try {
					//
					List<String> relationModelNames = MainWindow.this.netGUI.getNet().relationModels().sortedNameList();
					AttributeDescriptors attributeDescriptors = AttributeWorker.getExogenousAttributeDescriptors(MainWindow.this.netGUI.getNet(),
							MainWindow.this.netGUI.getSegmentation(), null);
					AttributeToRelationCriteria criteria = AttributeToRelationInputDialog.showDialog(relationModelNames, attributeDescriptors);

					//
					if (criteria != null) {
						//
						Net newNet = new Net(MainWindow.this.netGUI.getNet());

						//
						Segmentation newSegmentation = new Segmentation(newNet, MainWindow.this.netGUI.getSegmentation());

						//
						long count = NetUtils.transformAttributeToRelation(newNet, newSegmentation, criteria);

						//
						String title = "Attribute to Relation Count";
						String message = "Number of attributes transformed to relations: " + count;
						JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

						//
						if (count > 0) {
							//
							newSegmentation.refresh();
							NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet, newSegmentation);
							newNetGui.setChanged(true, "-attributeToRelation");
						}
					}
				} catch (final PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}

			}
		});

		JMenuItem mntmParentAttributes = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmParentAttributes.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmParentAttributes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Transmit attribute values.
				try {
					//
					List<String> labels = IndividualValuator.getAttributeLabels(MainWindow.this.netGUI.getSegmentation().getCurrentIndividuals());

					//
					TransmitAttributeValueCriteria criteria = TransmitAttributeValueInputDialog.showDialog(labels);

					//
					if (criteria != null) {
						//
						Net newNet = new Net(MainWindow.this.netGUI.getNet());

						//
						Segmentation newSegmentation = new Segmentation(newNet, MainWindow.this.netGUI.getSegmentation());

						//
						int count = NetUtils.getParentAttributeValues(newSegmentation.getCurrentIndividuals(), criteria);

						//
						String title = "Transmit Attribute Value Count";
						String message = "Number of attribute transmitted: " + count;
						JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

						//
						if (count > 0) {
							newSegmentation.refresh();
							NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet, newSegmentation);
							newNetGui.setChanged(true, "-transmittedAttributeValue");
						}
					}

				} catch (final PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnTransform.add(mntmParentAttributes);

		JMenuItem mntmIndividualizeFamilyAttributes = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmIndividualizeFamilyAttributes.text_1")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmIndividualizeFamilyAttributes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				//
				Net newNet = new Net(MainWindow.this.netGUI.getNet());

				//
				NetUtils.individualizeFamilyAttributes(newNet);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, "-individualized");
			}
		});
		mnTransform.add(mntmIndividualizeFamilyAttributes);
		mnTransform.add(mntmAttributeToRelation);

		JSeparator separator_19 = new JSeparator();
		mnTransform.add(separator_19);

		JMenuItem mntmMarryCoparents = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmMarryCoparents.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmMarryCoparents.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Copy without marked double individuals.
				//
				Net newNet = new Net(MainWindow.this.netGUI.getNet());
				Report report = new Report("Unmarried parents report");

				//
				int count = NetUtils.marryCoparents(newNet, report);

				//
				String title = "Marry co-parents Result";
				String message = "Number of unmarried co-parents married: " + count;
				JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, "-married");
				if (report.outputs().isNotEmpty()) {
					newNetGui.addReportTab(report);
				}
			}
		});
		mnTransform.add(mntmMarryCoparents);

		JMenuItem mntmAdjustParentGender = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAdjustParentGender.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmAdjustParentGender.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				// Adjust parent Gender.
				//
				Net newNet = new Net(MainWindow.this.netGUI.getNet());
				Report report = new Report("Gender adjustment report");

				//
				int count = NetUtils.adjustParentGender(newNet, report);

				//
				String title = "Adjust parent gender Result";
				String message = "Number of gender-adjusted families: " + count;
				JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, "-adjusted");
				if (report.outputs().isNotEmpty()) {
					newNetGui.addReportTab(report);
				}
			}
		});
		mnTransform.add(mntmAdjustParentGender);

		JMenuItem mntmRenumber = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmRenumber.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnTransform.add(mntmRenumber);
		mntmRenumber.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Renumberate individual ids.
				//
				NetUtils.renumberIndividuals(MainWindow.this.netGUI.getNet());

				//
				MainWindow.this.netGUI.setChanged(true, "renumbered");

				// Refresh.
				MainWindow.this.netGUI.updateAll();
			}
		});
		mntmRenumber.setEnabled(true);

		JMenuItem mntmRenumberIdsFrom = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmRenumberIdsFrom.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmRenumberIdsFrom.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Renumber from ID.
				//
				int count = NetUtils.renumberFromAttribute(MainWindow.this.netGUI.getNet(), "ID");

				//
				String title = "Renumber ids from ID Result";
				String message = "Number of individuals renumbered: " + count;
				JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

				//
				if (count > 0) {
					//
					MainWindow.this.netGUI.setChanged(true, "renumbered");

					// Refresh.
					MainWindow.this.netGUI.updateAll();
				}
			}
		});

		JMenuItem mntmRenumberFamilyIds = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmRenumberFamilyIds.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmRenumberFamilyIds.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Re-number family ids.
				//
				NetUtils.renumberFamilies(MainWindow.this.netGUI.getNet());

				//
				MainWindow.this.netGUI.setChanged(true, "renumbered");

				// Refresh.
				MainWindow.this.netGUI.updateAll();
			}
		});
		mntmRenumberFamilyIds.setEnabled(true);
		mnTransform.add(mntmRenumberFamilyIds);
		mnTransform.add(mntmRenumberIdsFrom);

		JMenuItem mntmFamiliesAsRelations = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAddEditableFamilies.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmFamiliesAsRelations.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				try {
					//
					RelationModel model = NetUtils.createRelationsFromFamilies(MainWindow.this.netGUI.getNet());

					//
					MainWindow.this.netGUI.setChanged(true);

					//
					MainWindow.this.netGUI.addRelationTab(model);

					// Refresh.
					MainWindow.this.netGUI.updateAll();

				} catch (final PuckException exception) {

					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JMenuItem mntmRenumberIdsFrom_1 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmRenumberIdsFrom_1.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmRenumberIdsFrom_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Renumber from REFN.
				//
				int count = NetUtils.renumberFromAttribute(MainWindow.this.netGUI.getNet(), "REFN");

				//
				String title = "Renumber ids from REFN Result";
				String message = "Number of individuals renumbered: " + count;
				JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

				//
				if (count > 0) {
					//
					MainWindow.this.netGUI.setChanged(true, "re-numbered");

					// Refresh.
					MainWindow.this.netGUI.updateAll();
				}
			}
		});
		mnTransform.add(mntmRenumberIdsFrom_1);

		JSeparator separator_21 = new JSeparator();
		mnTransform.add(separator_21);
		mnTransform.add(mntmFamiliesAsRelations);

		JMenuItem mntmEnlargeFromAttributes = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmEnlargeFromAttributes.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmEnlargeFromAttributes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Interface is missing - labels taken for Xuetas dataset

				try {
					Map<String, String> husbandLabels = new HashMap<String, String>();
					husbandLabels.put("HUSB_prepar", "prepar");
					husbandLabels.put("HUSB_nompar", "nompar");
					husbandLabels.put("HUSB_premar", "premar");
					husbandLabels.put("HUSB_nommar", "nommar");
					Map<String, String> wifeLabels = new HashMap<String, String>();
					wifeLabels.put("WIFE_prepar", "prepar");
					wifeLabels.put("WIFE_nompar", "nompar");
					wifeLabels.put("WIFE_premar", "premar");
					wifeLabels.put("WIFE_nommar", "nommar");

					NetUtils.enlargeNetFromAttributes(MainWindow.this.netGUI.getNet(), "prepar", "nompar", "premar", "nommar", "HUSB_prenom", "HUSB_nomnom",
							"WIFE_prenom", "WIFE_nomnom", husbandLabels, wifeLabels, "avipap", "avipan", "aviapp", "aviapn", "avimap", "aviman", "aviamp",
							"aviamn");

					MainWindow.this.netGUI.setChanged(true);
					// Refresh.
					MainWindow.this.netGUI.updateAll();

				} catch (PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JMenuItem mntmAddLifeEvents = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAddLifeEvents.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmAddLifeEvents.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				//
				try {
					//
					RelationModel model = NetUtils.createLifeEvents(MainWindow.this.netGUI.getNet());

					//
					MainWindow.this.netGUI.setChanged(true);

					//
					MainWindow.this.netGUI.addRelationTab(model);

					// Refresh.
					MainWindow.this.netGUI.updateAll();

				} catch (final PuckException exception) {

					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JMenuItem mntmFamiliesFromRelations = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmFamiliesFromRelations.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmFamiliesFromRelations.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				//

				Map<String, String> labels = new HashMap<String, String>();

				// Entry mask needed
				/*				String relationLabel = "HOUSEHOLD";
								labels.put("husband", "chef");
								labels.put("wife", "femme");
								labels.put("son", "fils");
								labels.put("daughter", "fille");
								labels.put("otherSon", "fils d'autre part");
								labels.put("otherDaughter", "fille d'autre part");
								labels.put("otherWife", "femme d'autre part");
								labels.put("naturalSon", "fils naturel");
								labels.put("naturalDaughter", "fille naturelle");
								labels.put("brother", "fr�re");
								labels.put("sister", "s�ur");
								labels.put("husbandMother", "m�re");*/

				String relationLabel = "NOTE_";
				labels.put("husband", "Father");
				labels.put("wife", "Mother");
				labels.put("son", "Children");
				labels.put("daughter", "Children");
				String alternativeLabel = "Version_Alternative";

				//
				Report report = new Report("Update report");
				Net updatedNet = NetUtils.createFamiliesFromRelations(MainWindow.this.netGUI.getNet(), relationLabel, alternativeLabel, labels, report);
				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), updatedNet);
				newNetGui.setChanged(true, "-families from relations");

				if (report.outputs().isNotEmpty()) {
					newNetGui.addReportTab(report);
				}

				//
				/*				MainWindow.this.netGUI.setChanged(true);

								// Refresh.
								MainWindow.this.netGUI.updateAll();*/
			}
		});
		mnTransform.add(mntmFamiliesFromRelations);
		mnTransform.add(mntmAddLifeEvents);

		JMenuItem mntmRelationsFromAttributes = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmRelationsFromAttributes.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmRelationsFromAttributes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				try {
					//
					List<String> relationModelNames = MainWindow.this.netGUI.getNet().relationModels().sortedNameList();
					AttributeDescriptors attributeDescriptors = AttributeWorker.getExogenousAttributeDescriptors(MainWindow.this.netGUI.getNet(),
							MainWindow.this.netGUI.getSegmentation(), null);
					AttributeToRelationCriteria criteria = AttributeToRelationInputDialog.showDialog(relationModelNames, attributeDescriptors);
					
					//
					if (criteria != null) {
						//
						Net newNet = new Net(MainWindow.this.netGUI.getNet());

						//
						Segmentation newSegmentation = new Segmentation(newNet, MainWindow.this.netGUI.getSegmentation());

						//
						long count = NetUtils.createRelationsFromAttributes(newNet, newSegmentation, criteria);

						//
						String title = "Attribute to Relation Count";
						String message = "Number of attributes transformed to relations: " + count;
						JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

						//
						if (count > 0) {
							//
							newSegmentation.refresh();
							NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet, newSegmentation);
							newNetGui.setChanged(true, "-attributeToRelation");
						}
					}
				} catch (final PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}

			}
		});

		mnTransform.add(mntmRelationsFromAttributes);
		mnTransform.add(mntmEnlargeFromAttributes);

		JMenuItem mntmUniqueRelationsTo = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmUniqueRelationsTo.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmUniqueRelationsTo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				//
				try {

					Report report = new Report("Relations to attributes");
					NetUtils.transformRelationsToAttributes(MainWindow.this.netGUI.getNet(), report);
					//
					MainWindow.this.netGUI.setChanged(true);

					//
					MainWindow.this.netGUI.addReportTab(report);

					// Refresh.
					MainWindow.this.netGUI.updateAll();

				} catch (final Exception exception) {

					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JMenuItem mntmRelativeNamesFrom = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmRelativeNamesFrom.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmRelativeNamesFrom.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				//
				try {

					Net newNet = new Net(MainWindow.this.netGUI.getNet());

					Report report = new Report("Names from attributes");
					NetUtils.relativeNamesFromAttributes(newNet, report);
					//
					//
					NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
					newNetGui.setChanged(true, "-renamed_relatives");

					//
					newNetGui.addReportTab(report);

					// Refresh.
					newNetGui.updateAll();

				} catch (final Exception exception) {

					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnTransform.add(mntmRelativeNamesFrom);
		mnTransform.add(mntmUniqueRelationsTo);

		JMenuItem mntmRelationRolesTo = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmRelationRolesTo.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmRelationRolesTo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				//
				try {

					NetUtils.transformRelationRolesToAttributes(MainWindow.this.netGUI.getNet());
					//
					MainWindow.this.netGUI.setChanged(true);

					// Refresh.
					MainWindow.this.netGUI.updateAll();

				} catch (final Exception exception) {

					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		JMenuItem mntmRelationAttributesTo = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmRelationAttributesTo.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmRelationAttributesTo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//
				try {
					
					List<String> relationModelNames = MainWindow.this.netGUI.getNet().relationModels().sortedNameList();
					AttributeDescriptors attributeDescriptors = AttributeWorker.getExogenousAttributeDescriptors(MainWindow.this.netGUI.getNet(),
							MainWindow.this.netGUI.getSegmentation(), null);
					RelationToAttributeCriteria criteria = RelationToAttributeInputDialog.showDialog(relationModelNames, attributeDescriptors);

					//
					if (criteria != null) {
						//
						Net newNet = new Net(MainWindow.this.netGUI.getNet());

						//
						Segmentation newSegmentation = new Segmentation(newNet, MainWindow.this.netGUI.getSegmentation());

						//
						long count = NetUtils.transformRelationAttributesToAttributes(newNet, criteria.getOptionalRelationName(), criteria.getRelationAttributeLabel(), criteria.getDateLabel());


						//
						String title = "Relation to Attribute";
						String message = "Number of relation attributes transformed to individual attributes: " + count;
						JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

						//
						if (count > 0) {
							//
							newSegmentation.refresh();
							NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet, newSegmentation);
							newNetGui.setChanged(true, "-relationToAttribute");
						}
					}


				} catch (final Exception exception) {

					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnTransform.add(mntmRelationAttributesTo);
		mnTransform.add(mntmRelationRolesTo);

		JSeparator separator_10 = new JSeparator();
		mnTransform.add(separator_10);

		JMenuItem mntmTransitiveRelations = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmTransitiveRelations.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmTransitiveRelations.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				for (RelationModel model : new ArrayList<RelationModel>(MainWindow.this.netGUI.getNet().relationModels())) {
					try {
						//
						MainWindow.this.netGUI.setChanged(true);
						//
						MainWindow.this.netGUI.addRelationTab(NetUtils.createTransitiveRelations(MainWindow.this.netGUI.getNet(), model));
						// Refresh.
						MainWindow.this.netGUI.updateAll();

					} catch (PuckException exception) {
						// Show trace.
						exception.printStackTrace();

						//
						String title = "Error computerum est";
						String message = "Error occured during working: " + exception.getMessage();

						//
						JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		mnTransform.add(mntmTransitiveRelations);

		JSeparator separator_11 = new JSeparator();
		mnTransform.add(separator_11);

		JMenu mnReduce = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnReduce.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnTransform.add(mnReduce);

		JMenuItem mntmCuttingTails = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmCopyCuttingTails.text_1")); //$NON-NLS-1$ //$NON-NLS-2$
		mnReduce.add(mntmCuttingTails);

		JMenuItem mntmCopyWithoutMarked = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmCopyWithoutMarked.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnReduce.add(mntmCopyWithoutMarked);

		JMenuItem mntmCopyWithoutStruct = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmCopyWithoutStruct.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnReduce.add(mntmCopyWithoutStruct);

		JMenuItem mntmEliminateSingles = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmEliminateSingles.text"));
		mnReduce.add(mntmEliminateSingles);

		JMenuItem mntmCopyWithoutVirtuals = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmCopyWithoutVirtuals.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnReduce.add(mntmCopyWithoutVirtuals);
		mntmCopyWithoutVirtuals.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Copy without unknown parents.
				//
				Net newNet = new Net(MainWindow.this.netGUI.getNet());
				Individuals currentIndividuals = newNet.individuals().getByIds(MainWindow.this.netGUI.getCurrentIndividuals().getIds());

				//
				Report report = new Report("Virtuals report");
				int count = NetUtils.eliminateVirtualParents(newNet, currentIndividuals, report);

				//
				String title = "Eliminate Virtuals Individuals Result";
				String message = "Number of individuals removed: " + count;
				JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, "-wo_virtuals");
				if (report.outputs().isNotEmpty()) {
					newNetGui.addReportTab(report);
				}

			}
		});
		mntmEliminateSingles.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Eliminate unmarried.
				//
				Chronometer chrono = new Chronometer();

				//
				Net newNet = new Net(MainWindow.this.netGUI.getNet());
				Individuals currentIndividuals = newNet.individuals().getByIds(MainWindow.this.netGUI.getCurrentIndividuals().getIds());

				//
				Report report = new Report("Singles report");
				int previousIndividualCount = newNet.individuals().size();
				int count = NetUtils.eliminateSingles(newNet, currentIndividuals, report);

				//
				String title = "Eliminate Unmarried Result";
				String message = String.format("Eliminated singles: %d\nPrevious count: %d\nUpdated count: %d\nTime spent: %d ms", count,
						previousIndividualCount, MainWindow.this.netGUI.getNet().individuals().size(), chrono.stop().interval());
				JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, "-no-singles");
				if (report.outputs().isNotEmpty()) {
					newNetGui.addReportTab(report);
				}

			}
		});
		mntmCopyWithoutStruct.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Eliminate Structural children.
				//
				Net newNet = new Net(MainWindow.this.netGUI.getNet());
				Individuals currentIndividuals = newNet.individuals().getByIds(MainWindow.this.netGUI.getCurrentIndividuals().getIds());

				//
				Report report = new Report("Herod report");
				int count = NetUtils.eliminateStructuralChildren(newNet, currentIndividuals, report);

				//
				String title = "Eliminate Structural Children Result";
				String message = "Number of individuals removed: " + count;
				JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

				//
				NetGUI newNetGUI = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGUI.setChanged(true, "-adults");
				if (report.outputs().isNotEmpty()) {
					newNetGUI.addReportTab(report);
				}

			}
		});
		mntmCopyWithoutMarked.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Copy without marked double individuals.
				//
				Net newNet = new Net(MainWindow.this.netGUI.getNet());
				Individuals currentIndividuals = newNet.individuals().getByIds(MainWindow.this.netGUI.getCurrentIndividuals().getIds());
				Relations currentRelations = newNet.relations().getByIds(MainWindow.this.netGUI.getCurrentRelations().getIds());
				Report report = new Report("Consolidation report");

				//
				NetUtils.eliminateDoubleIndividuals(newNet, currentIndividuals, report);
				report.outputs().appendln();
				NetUtils.eliminateDoubleRelations(newNet, currentRelations, report);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, "-univocal");
				if (report.outputs().isNotEmpty()) {
					newNetGui.addReportTab(report);
				}
			}
		});
		mntmCuttingTails.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Copy cutting tails.
				Net newNet = new Net(MainWindow.this.netGUI.getNet());
				Report report = new Report("Shave report");

				int count = HairCutWorker.shave(newNet, report);

				//
				String title = "Haircut Result";
				String message = "Number of individuals eliminated: " + count;
				JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, "-shaved");
				if (report.outputs().isNotEmpty()) {
					newNetGui.addReportTab(report);
				}

			}
		});

		JMenu mnExtract = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnExtract.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnTransform.add(mnExtract);

		JMenuItem mntmCurrentCluster = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmCurrentCluster.text_1")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmCurrentCluster.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Extract current cluster.
				//
				Net newNet = NetUtils.extractCurrentCluster(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation());

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, " " + MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getShortShortLabel().replace("*", "out"));
			}
		});
		mntmCurrentCluster.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK));
		mnExtract.add(mntmCurrentCluster);

		JMenuItem mntmExtractCore = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmExtractCore.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnExtract.add(mntmExtractCore);

		JMenuItem mntmKernel = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmKernel.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnExtract.add(mntmKernel);

		JMenuItem mntmMaximalBicomponent = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmMaximalBicomponent.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnExtract.add(mntmMaximalBicomponent);
		mntmMaximalBicomponent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Extract Maximal Bicomponent.
				Net target = BicomponentWorker.getMaximalBicomponent(MainWindow.this.netGUI.getNet());

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), target);
				newNetGui.setChanged(true, "-maxBicomponent");
			}
		});

		JMenuItem mntmMaxComponent = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmMaxComponent.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmMaxComponent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				// Extract Maximal Component
				Net target = NetUtils.extractMaxComponent(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation());

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), target);
				newNetGui.setChanged(true, "-maxComponent");
			}
		});
		mnExtract.add(mntmMaxComponent);

		JMenuItem mntmExtractLargeClusters = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmExtractLargeClusters.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnExtract.add(mntmExtractLargeClusters);

		JMenuItem mntmExtractPositiveCluster = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmExtractPositiveCluster.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnExtract.add(mntmExtractPositiveCluster);
		mntmExtractPositiveCluster.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Extract positive cluster values.
				ClusterValueExtractionInputWindow window = new ClusterValueExtractionInputWindow(MainWindow.this.netGUI);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});
		mntmExtractLargeClusters.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Extract large clusters.
				ClusterSizeExtractionInputWindow window = new ClusterSizeExtractionInputWindow(MainWindow.this.netGUI);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});
		mntmKernel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Extract Kernel.
				Net target = BicomponentWorker.getKernel(MainWindow.this.netGUI.getNet());

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), target);
				newNetGui.setChanged(true, "-kernel");
			}
		});
		mntmExtractCore.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Extract Core
				Net target = BicomponentWorker.getCore(MainWindow.this.netGUI.getNet());

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), target);
				newNetGui.setChanged(true, "-core");
			}
		});

		JSeparator separator_17 = new JSeparator();
		mnTransform.add(separator_17);

		JMenuItem mntmShufflePgraph = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmShufflePgraph.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmShufflePgraph.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Shuffle P-Graph.
				//
				ReshufflingNetworkInputWindow window = new ReshufflingNetworkInputWindow(MainWindow.this.netGUI);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});

		JMenu mnNewMenu = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnNewMenu.text_2")); //$NON-NLS-1$ //$NON-NLS-2$
		mnTransform.add(mnNewMenu);

		JMenuItem mntmUniversal = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmUniversal.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmUniversal.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Universal kinship cluster expansion
				//
				Net newNet = NetUtils.expand(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation().getCurrentIndividuals(),
						MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getLabel(), ExpansionMode.KIN, FiliationType.COGNATIC, 0);
				// Net newNet =
				// NetUtils.expandByKin(MainWindow.this.netGUI.getNet(),
				// MainWindow.this.netGUI.getSegmentation().getCurrentSegment(),
				// null);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, " " + MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getShortShortLabel().replace("*", "out")
						+ "_expanded_ALL_KIN");
			}
		});

		JMenuItem mntmAllRelated = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAllRelated.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmAllRelated.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Universal relation cluster expansion
				//
				Net newNet = NetUtils.expand(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation().getCurrentIndividuals(),
						MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getLabel(), ExpansionMode.RELATED, null, 0);
				// Net newNet =
				// NetUtils.expandByRelations(MainWindow.this.netGUI.getNet(),
				// MainWindow.this.netGUI.getSegmentation().getCurrentSegment());

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, " " + MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getShortShortLabel().replace("*", "out")
						+ "_expanded_RELATIONS");
			}
		});

		JMenuItem mntmUniversal_1 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmUniversal_1.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmUniversal_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Universal cluster expansion
				//
				Net newNet = NetUtils.expand(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation().getCurrentIndividuals(),
						MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getLabel(), ExpansionMode.ALL, FiliationType.COGNATIC, 0);
				// Net newNet =
				// NetUtils.expand(MainWindow.this.netGUI.getNet(),
				// MainWindow.this.netGUI.getSegmentation().getCurrentSegment());

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, " " + MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getShortShortLabel().replace("*", "out")
						+ "_expanded_ALL");
			}
		});

		JMenuItem mntmSpecialFeature = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmSpecialFeature.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmSpecialFeature.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Expand.
				//
				ExpandCriteria criteria = ExpandInputDialog.showDialog();

				//
				if (criteria != null) {
					//
					Net newNet = NetUtils.expand(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation().getCurrentIndividuals(),
							MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getLabel(), criteria.getExpansionMode(), criteria.getFiliationType(),
							criteria.getMaxStep());

					//
					NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
					newNetGui.setChanged(true, " " + MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getShortShortLabel().replace("*", "out")
							+ "_" + criteria.getSubTitle());
				}
			}
		});
		mnNewMenu.add(mntmSpecialFeature);
		mnNewMenu.add(mntmUniversal_1);
		mnNewMenu.add(mntmAllRelated);
		mnNewMenu.add(mntmUniversal);

		JMenuItem mntmBottomup = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmBottomup.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmBottomup.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Ascending cluster expansion
				//
				Net newNet = NetUtils.expand(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation().getCurrentIndividuals(),
						MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getLabel(), ExpansionMode.PARENT, FiliationType.COGNATIC, 0);
				// Net newNet =
				// NetUtils.expandByKin(MainWindow.this.netGUI.getNet(),
				// MainWindow.this.netGUI.getSegmentation().getCurrentSegment(),KinType.PARENT);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, " " + MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getShortShortLabel().replace("*", "out")
						+ "_expanded_UP");
			}
		});
		mnNewMenu.add(mntmBottomup);

		JMenuItem mntmTopdown = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmTopdown.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmTopdown.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Descending cluster expansion
				//
				Net newNet = NetUtils.expand(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation().getCurrentIndividuals(),
						MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getLabel(), ExpansionMode.CHILD, FiliationType.COGNATIC, 0);
				// Net newNet =
				// NetUtils.expandByKin(MainWindow.this.netGUI.getNet(),
				// MainWindow.this.netGUI.getSegmentation().getCurrentSegment(),KinType.CHILD);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, " " + MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getShortShortLabel().replace("*", "out")
						+ "_expanded_DOWN");
			}
		});

		JMenuItem mntmAscendingagnatic = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAscendingagnatic.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmAscendingagnatic.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Ascending (agnatic) cluster expansion
				//
				Net newNet = NetUtils.expand(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation().getCurrentIndividuals(),
						MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getLabel(), ExpansionMode.PARENT, FiliationType.AGNATIC, 0);
				// Net newNet =
				// NetUtils.expandByKin(MainWindow.this.netGUI.getNet(),
				// MainWindow.this.netGUI.getSegmentation().getCurrentSegment(),KinType.PARENT);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, " " + MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getShortShortLabel().replace("*", "out")
						+ "_expanded_UP_A");
			}
		});
		mnNewMenu.add(mntmAscendingagnatic);

		JMenuItem mntmAscendinguterine = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAscendinguterine.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmAscendinguterine.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Ascending (uterine) cluster expansion
				//
				Net newNet = NetUtils.expand(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation().getCurrentIndividuals(),
						MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getLabel(), ExpansionMode.PARENT, FiliationType.UTERINE, 0);
				// Net newNet =
				// NetUtils.expandByKin(MainWindow.this.netGUI.getNet(),
				// MainWindow.this.netGUI.getSegmentation().getCurrentSegment(),KinType.PARENT);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, " " + MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getShortShortLabel().replace("*", "out")
						+ "_expanded_UP");
			}
		});
		mnNewMenu.add(mntmAscendinguterine);
		mnNewMenu.add(mntmTopdown);

		JMenuItem mntmConjugal = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmConjugal.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmConjugal.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Horizontal cluster expansion
				//
				Net newNet = NetUtils.expand(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation().getCurrentIndividuals(),
						MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getLabel(), ExpansionMode.SPOUSE, FiliationType.COGNATIC, 0);
				// Net newNet =
				// NetUtils.expandByKin(MainWindow.this.netGUI.getNet(),
				// MainWindow.this.netGUI.getSegmentation().getCurrentSegment(),KinType.SPOUSE);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, " " + MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getShortShortLabel().replace("*", "out")
						+ "_expanded_MARR");
			}
		});

		JMenuItem mntmDescendingagnatic = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmDescendingagnatic.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmDescendingagnatic.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Descending (agnatic) cluster expansion
				//
				Net newNet = NetUtils.expand(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation().getCurrentIndividuals(),
						MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getLabel(), ExpansionMode.CHILD, FiliationType.AGNATIC, 0);
				// Net newNet =
				// NetUtils.expandByKin(MainWindow.this.netGUI.getNet(),
				// MainWindow.this.netGUI.getSegmentation().getCurrentSegment(),KinType.CHILD);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, " " + MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getShortShortLabel().replace("*", "out")
						+ "_expanded_DOWN_A");
			}
		});
		mnNewMenu.add(mntmDescendingagnatic);

		JMenuItem mntmDescendinguterine = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmDescendinguterine.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmDescendinguterine.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Descending (uterine) cluster expansion
				//
				Net newNet = NetUtils.expand(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation().getCurrentIndividuals(),
						MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getLabel(), ExpansionMode.CHILD, FiliationType.UTERINE, 0);

				//
				NetGUI newNetGui = PuckGUI.instance().createNetGUI(MainWindow.this.netGUI.getFile(), newNet);
				newNetGui.setChanged(true, " " + MainWindow.this.netGUI.getSegmentation().getCurrentSegment().getShortShortLabel().replace("*", "out")
						+ "_expanded_DOWN_U");
			}
		});
		mnNewMenu.add(mntmDescendinguterine);
		mnNewMenu.add(mntmConjugal);

		JSeparator separator_15 = new JSeparator();
		mnTransform.add(separator_15);

		JMenu mnShrink = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnShrink.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnTransform.add(mnShrink);

		JMenuItem mntmInterMarriageReport = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmIntermarriageReport.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnShrink.add(mntmInterMarriageReport);

		JMenuItem mntmFlowReport = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmFlowReport_1.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnShrink.add(mntmFlowReport);
		mntmFlowReport.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Flow report 2.
				FlowNetworkInputWindow window = new FlowNetworkInputWindow(MainWindow.this.netGUI);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});
		mntmInterMarriageReport.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// InterMarriage Report2
				// Reduce Report*.
				AllianceNetworkInputWindow window = new AllianceNetworkInputWindow(MainWindow.this.netGUI);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});

		JSeparator separator_16 = new JSeparator();
		mnTransform.add(separator_16);
		mnTransform.add(mntmShufflePgraph);

		JMenuItem mntmVirtualFieldwork = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmVirtualFieldwork.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmVirtualFieldwork.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				FieldworkInputWindow window = new FieldworkInputWindow(MainWindow.this.netGUI);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});
		mnTransform.add(mntmVirtualFieldwork);

		JMenuItem mntmVirtualFieldworkVariations = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmVirtualFieldworkVariations.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmVirtualFieldworkVariations.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				try {
					Report report = RandomNetReporter.reportVirtualFieldworkVariations(MainWindow.this.netGUI.getSegmentation());
					MainWindow.this.netGUI.addReportTab(report);
				} catch (PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnTransform.add(mntmVirtualFieldworkVariations);

		JMenu mnCensus = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnCensus.text")); //$NON-NLS-1$ //$NON-NLS-2$
		menuBar.add(mnCensus);

		JMenuItem mntmPedigree = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmPedigree.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmPedigree.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Pedigree.
				//
				Individual ego = MainWindow.this.netGUI.selectedIndividual();
				if (ego == null) {
					//
					String title = "Information";
					String message = "Please, select an individual on the list.";

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				} else {
					String parameter = JOptionPane.showInputDialog(null, "Maximal generational depth: ", "Pedigree Report", JOptionPane.QUESTION_MESSAGE);

					logger.debug("parameter=[" + parameter + "]");
					if (parameter != null) {
						int maxDepth = Integer.parseInt(parameter);

						Report report = CensusReporter.reportPedigree(MainWindow.this.netGUI.getNet().getLabel(), ego, maxDepth);

						MainWindow.this.netGUI.addReportTab(report);
					}
				}
			}
		});
		mnCensus.add(mntmPedigree);

		JMenuItem mntmRelatives = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmRelatives.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmRelatives.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Relatives
				//
				Individual ego = MainWindow.this.netGUI.selectedIndividual();
				if (ego == null) {
					//
					String title = "Information";
					String message = "Please, select an individual on the list.";

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				} else {
					String parameter = JOptionPane.showInputDialog(null, "Enter kinship string (in positional notation): ", "Chains Report",
							JOptionPane.QUESTION_MESSAGE);
					logger.debug("parameter=[" + parameter + "]");

					Report report = CensusReporter.reportRelatives(MainWindow.this.netGUI.getNet().getLabel(), ego, parameter);

					MainWindow.this.netGUI.addReportTab(report);
				}
			}
		});

		JMenuItem mntmProgeniture = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmProgeniture.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmProgeniture.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Progeniture.
				//
				Individual ego = MainWindow.this.netGUI.selectedIndividual();
				if (ego == null) {
					//
					String title = "Information";
					String message = "Please, select an individual on the list.";

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				} else {
					String parameter = JOptionPane.showInputDialog(null, "Maximal generational depth: ", "Progeniture Report", JOptionPane.QUESTION_MESSAGE);

					logger.debug("parameter=[" + parameter + "]");
					if (parameter != null) {
						int maxDepth = Integer.parseInt(parameter);

						Report report = CensusReporter.reportProgeniture(MainWindow.this.netGUI.getNet().getLabel(), ego, maxDepth);

						MainWindow.this.netGUI.addReportTab(report);
					}
				}
			}
		});
		mnCensus.add(mntmProgeniture);
		mnCensus.add(mntmRelatives);

		JMenuItem mntmKinshipChains = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmKinshipChains.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmKinshipChains.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Linking Chains
				Individual ego = MainWindow.this.netGUI.selectedIndividual();
				if (ego == null) {
					//
					String title = "Information";
					String message = "Please, select an individual on the list.";

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				} else {
					//
					KinshipChainsCriteria criteria = KinshipChainsInputDialog.showDialog();

					//
					if (criteria != null) {
						Individual alter = MainWindow.this.netGUI.getNet().get(criteria.getAlterId());

						Report report = CensusReporter.reportKinshipChains(MainWindow.this.netGUI.getNet().getLabel(), ego, alter, criteria.getMaximalDepth(),
								criteria.getMaximalOrder(), criteria.getChainClassification());

						MainWindow.this.netGUI.addReportTab(report);
					}
				}
			}
		});
		mnCensus.add(mntmKinshipChains);

		JMenuItem mntmFindCircuit2 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmFindCircuit2.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmFindCircuit2.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_J, InputEvent.CTRL_MASK));
		mntmFindCircuit2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Find Circuit Report*.
				CensusInputWindow window = new CensusInputWindow(MainWindow.this.netGUI);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});

		JMenuItem mntmDistances = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmDistances.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmDistances.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Generate distance attribute.
				//
				DistanceAttributeCriteria criteria = DistanceAttributeInputDialog.showDialog();

				//
				if (criteria != null) {
					//
					Individual ego = MainWindow.this.netGUI.selectedIndividual();

					if (ego == null) {
						//
						throw new IllegalArgumentException("Please, select an individual in tab.");

					} else {
						//
						Report report = new Report("Distances " + criteria.getFiliationType() + " " + ego);
						StatisticsWorker.getDistances(ego, criteria.getMaxDistance(), criteria.getFiliationType(), report);

						//
						MainWindow.this.netGUI.updateAll();
						MainWindow.this.netGUI.addReportTab(report);

					}
				}
			}
		});
		mnCensus.add(mntmDistances);

		JSeparator separator_13 = new JSeparator();
		mnCensus.add(separator_13);

		JMenuItem mntmBasicInformation = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmBasicInformation.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnCensus.add(mntmBasicInformation);
		mntmBasicInformation.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_MASK));
		mntmBasicInformation.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Statistics.
				try {
					Chronometer chrono = new Chronometer();
					Report report = StatisticsReporter.reportBasicInformation(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation());
					chrono.stop();
					logger.info(String.format("Time spent: %d ms", chrono.interval()));
					MainWindow.this.netGUI.addReportTab(report);
				} catch (final PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JMenuItem mntmBasicGraphs = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmBasicGraphs.text"));
		mnCensus.add(mntmBasicGraphs);
		mntmBasicGraphs.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Statistics.
				StatisticsInputWindow window = new StatisticsInputWindow(MainWindow.this.netGUI);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});
		mntmBasicGraphs.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, InputEvent.CTRL_MASK));

		JMenuItem mntmPartitionStatistics = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmPartitionStatistics.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmPartitionStatistics.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Partition Statistics.
				PartitionStatisticsInputWindow window = new PartitionStatisticsInputWindow(MainWindow.this.netGUI);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});
		mnCensus.add(mntmPartitionStatistics);

		JSeparator separator_14 = new JSeparator();
		mnCensus.add(separator_14);
		mnCensus.add(mntmFindCircuit2);

		JSeparator separator_12 = new JSeparator();
		mnCensus.add(separator_12);

		JMenuItem mntmSynopsis = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmSynopsis.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmSynopsis.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {

				File file = selectFolder(MainWindow.this.frmPuck, MainWindow.this.netGUI.getFile().getParentFile());

				//
				if (file != null) {
					//
					/*
					 * String pathname =
					 * "C:\\Documents and Settings\\Klaus Hamberger\\Mes Documents\\Testcorpus"
					 * ;
					 */
					String directory = file.getAbsolutePath();
					Report report = StatisticsReporter.reportSynopsis(directory);
					MainWindow.this.netGUI.addReportTab(report);
				}

			}
		});
		mnCensus.add(mntmSynopsis);

		JMenuItem mntmQuickReview = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmQuickReview.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmQuickReview.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_MASK));
		mntmQuickReview.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				try {
					Chronometer chrono = new Chronometer();
					Report report = StatisticsReporter.reportQuickReview(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation(),
							MainWindow.this.netGUI.getFile());
					chrono.stop();
					logger.info(String.format("Time spent: %d ms", chrono.interval()));
					MainWindow.this.netGUI.addReportTab(report);
				} catch (final PuckException exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnCensus.add(mntmQuickReview);

		JMenu mnRelations = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnRelations.text")); //$NON-NLS-1$ //$NON-NLS-2$
		menuBar.add(mnRelations);

		JMenuItem mntmFromGenealogy = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmFromGenealogy.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmFromGenealogy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {

				int maxIterations = 2;

				// Open genealogy
				File file = selectFile(MainWindow.this.frmPuck, MainWindow.this.netGUI.getFile().getParentFile());

				//
				openSelectedFile(MainWindow.this.netGUI, MainWindow.this.frmPuck, file, PuckManager.DEFAULT_CHARSET_NAME);

				// Create model from genealogy
				Report report = new Report("Import log");
				RelationModel model;
				try {
					model = RoleRelationMaker.create(MainWindow.this.netGUI.getNet(), report);
					if (report.outputs().isNotEmpty()) {
						MainWindow.this.netGUI.addReportTab(report);
					}

					// Apply model to genealogy
					RoleRelationMaker.applyModel(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation(), model, maxIterations);

					MainWindow.this.netGUI.addRelationTab(model);

					//
					MainWindow.this.netGUI.setChanged(true);

					//
					MainWindow.this.netGUI.updateAll();

				} catch (PuckException exception) {
					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}

			}
		});

		JMenuItem mntmImportModelgenealogical = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmImportModelgenealogical.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmImportModelgenealogical.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				try {
					//
					int maxIterations = PuckGUI.instance().getPreferences().getTerminologyMaxIterations();
					String selfName = PuckGUI.instance().getPreferences().getTerminologySelfName();

					File file = selectRelationModelFileToLoad(MainWindow.this.frmPuck, MainWindow.this.netGUI.getFile());

					//
					if (file != null) {
						//

						String fileName = file.getName();

						StringList list = null;

						if (fileName.endsWith(".txt")) {
							//
							list = RoleRelationsTXTFile.load(file, RoleRelationsTXTFile.DEFAULT_CHARSET_NAME);

						} else if (fileName.endsWith(".xls")) {
							//
							list = RoleRelationsXLSFile.load(file);
						}

						Report report = new Report("Import log");
						List<RoleRelationRule> rules = RoleRelationRuleInputDialog.showDialog();

						RelationModel model = RoleRelationMaker.createEtic(file.getName(), selfName, list, rules, report);

						if (MainWindow.this.netGUI.isBlank()) {
							Net net = RoleRelationMaker.createNet(model, maxIterations);
							MainWindow.this.netGUI.setNet(net);
							//
							MainWindow.this.netGUI.setChanged(true);
							MainWindow.this.netGUI.updateAll();
						}

						RoleRelationMaker.applyModel(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation(), model, 2 * maxIterations);

						MainWindow.this.netGUI.addRelationTab(model);
						if (report.outputs().isNotEmpty()) {
							MainWindow.this.netGUI.addReportTab(report);
						}

						//
						MainWindow.this.netGUI.setChanged(true);

						//
						MainWindow.this.netGUI.updateAll();

					}
				} catch (final PuckException exception) {
					exception.printStackTrace();

					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.title");

					String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.default");

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JMenuItem mntmImportModelnew = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmImportModelnew.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmImportModelnew.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				try {
					//
					File file = selectRelationModelFileToLoad(MainWindow.this.frmPuck, MainWindow.this.netGUI.getFile());

					int maxIterations = PuckGUI.instance().getPreferences().getTerminologyMaxIterations();
					String selfName = PuckGUI.instance().getPreferences().getTerminologySelfName();

					//
					if (file != null) {
						//

						String fileName = file.getName();
						StringList list = null;

						if (fileName.endsWith(".txt")) {
							//
							list = RoleRelationsTXTFile.load(file, RoleRelationsTXTFile.DEFAULT_CHARSET_NAME);

						} else if (fileName.endsWith(".xls")) {
							//
							list = RoleRelationsXLSFile.load(file);
						}

						Report report = new Report("Import log");
						List<RoleRelationRule> rules = RoleRelationRuleInputDialog.showDialog();

						RelationModel model = RoleRelationMaker.createStandard(file.getName(), selfName, list, rules, report);

						if (MainWindow.this.netGUI.isBlank()) {
							Net net = RoleRelationMaker.createNet(model, maxIterations);
							MainWindow.this.netGUI.setNet(net);
							//
							MainWindow.this.netGUI.setChanged(true);
							MainWindow.this.netGUI.updateAll();
						}

						RoleRelationMaker.applyModel(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation(), model, maxIterations);

						MainWindow.this.netGUI.addRelationTab(model);
						if (report.outputs().isNotEmpty()) {
							MainWindow.this.netGUI.addReportTab(report);
						}

						//
						MainWindow.this.netGUI.setChanged(true);

						//
						MainWindow.this.netGUI.updateAll();

					}
				} catch (final PuckException exception) {
					exception.printStackTrace();

					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.title");

					String message = exception.getMessage();
					if (message == null) {
						message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.default");
					}

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JMenuItem mntmImport = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmImport.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmImport.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Import TERMs file.
				try {
					//
					File file = ImportRelationModelFileSelector.showSelectorDialog(MainWindow.this.frmPuck, null);

					//
					if (file != null) {
						//
						Report report = new Report("Import log");

						//
						RelationModel model = PuckManager.loadRelationModel(file, report, null);

						//
						int maxIterations = PuckGUI.instance().getPreferences().getTerminologyMaxIterations();

						//
						if (MainWindow.this.netGUI.isBlank()) {

							Net net = RoleRelationMaker.createNet(model, maxIterations);
							MainWindow.this.netGUI.setNet(net);

							MainWindow.this.netGUI.setChanged(true);
							MainWindow.this.netGUI.updateAll();
						}

						//
						RoleRelationMaker.applyModel(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation(), model, maxIterations);

						MainWindow.this.netGUI.addRelationTab(model);
						if (report.outputs().isNotEmpty()) {
							MainWindow.this.netGUI.addReportTab(report);
						}

						//
						MainWindow.this.netGUI.setChanged(true);

						//
						MainWindow.this.netGUI.updateAll();

					}
				} catch (final PuckException exception) {
					exception.printStackTrace();

					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.title");

					String message = exception.getMessage();
					if (message == null) {
						message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.default");
					}

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnRelations.add(mntmImport);

		JMenuItem mntmImportSet = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmImportSet.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmImportSet.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Import TERMS and parameterize…
				try {
					//
					File file = ImportParamaterizedRelationModelFileSelector.showSelectorDialog(MainWindow.this.frmPuck, null);

					//
					if (file != null) {

						//
						RoleRelationRules rules = RoleRelationRuleInputDialog.showDialog();

						//
						Report report = new Report("Import log");

						//
						RelationModel model = PuckManager.loadRelationModel(file, report, rules);

						//
						int maxIterations = PuckGUI.instance().getPreferences().getTerminologyMaxIterations();

						//
						if (MainWindow.this.netGUI.isBlank()) {

							Net net = RoleRelationMaker.createNet(model, maxIterations);
							MainWindow.this.netGUI.setNet(net);

							MainWindow.this.netGUI.setChanged(true);
							MainWindow.this.netGUI.updateAll();
						}

						//
						RoleRelationMaker.applyModel(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation(), model, maxIterations);

						MainWindow.this.netGUI.addRelationTab(model);
						if (report.outputs().isNotEmpty()) {
							MainWindow.this.netGUI.addReportTab(report);
						}

						//
						MainWindow.this.netGUI.setChanged(true);

						//
						MainWindow.this.netGUI.updateAll();

					}
				} catch (final PuckException exception) {
					exception.printStackTrace();

					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.title");

					String message = exception.getMessage();
					if (message == null) {
						message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.default");
					}

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnRelations.add(mntmImportSet);

		JSeparator separator_23 = new JSeparator();
		mnRelations.add(separator_23);
		mnRelations.add(mntmImportModelnew);

		JMenuItem mntmImportModel = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmImportModel.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmImportModel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				try {
					//
					File file = selectRelationModelFileToLoad(MainWindow.this.frmPuck, MainWindow.this.netGUI.getFile());

					int maxIterations = PuckGUI.instance().getPreferences().getTerminologyMaxIterations();
					String selfName = PuckGUI.instance().getPreferences().getTerminologySelfName();

					//
					if (file != null) {
						//
						String fileName = file.getName();
						StringList list = null;

						if (fileName.endsWith(".txt")) {
							//
							list = RoleRelationsTXTFile.load(file, RoleRelationsTXTFile.DEFAULT_CHARSET_NAME);

						} else if (fileName.endsWith(".xls")) {
							//
							list = RoleRelationsXLSFile.load(file);
						}

						List<RoleRelationRule> rules = RoleRelationRuleInputDialog.showDialog();

						Report report = new Report("Import log");
						RelationModel model = RoleRelationMaker.createEmic(file.getName(), selfName, list, rules, report);

						if (MainWindow.this.netGUI.isBlank()) {
							Net net = RoleRelationMaker.createNet(model, maxIterations);
							MainWindow.this.netGUI.setNet(net);
							//
							MainWindow.this.netGUI.setChanged(true);
							MainWindow.this.netGUI.updateAll();
						}

						RoleRelationMaker.applyModel(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation(), model, maxIterations);

						MainWindow.this.netGUI.addRelationTab(model);
						if (report.outputs().isNotEmpty()) {
							MainWindow.this.netGUI.addReportTab(report);
						}

						//
						MainWindow.this.netGUI.setChanged(true);

						//
						MainWindow.this.netGUI.updateAll();

					}
				} catch (final PuckException exception) {
					exception.printStackTrace();

					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.title");

					String message = exception.getMessage();
					if (message == null) {
						message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.default");
					}

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnRelations.add(mntmImportModel);
		mnRelations.add(mntmImportModelgenealogical);
		mnRelations.add(mntmFromGenealogy);

		JMenuItem mntmImportFromKaes = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmImportFromKaes.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmImportFromKaes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					//
					File file = selectRelationModelFileToLoad(MainWindow.this.frmPuck, MainWindow.this.netGUI.getFile());

					int maxIterations = 2;
					//
					if (file != null) {
						//

						List<RoleActorPair> list = new ArrayList<RoleActorPair>();
						String selfName = RoleRelationsXMLFile.load(file, RoleRelationsTXTFile.DEFAULT_CHARSET_NAME, list);

						Report report = new Report("Import log");

						RelationModel model = RoleRelationMaker.createFromMap(file.getName(), list, report);

						if (MainWindow.this.netGUI.isBlank()) {
							Net net = RoleRelationMaker.createNet(model, maxIterations);
							MainWindow.this.netGUI.setNet(net);
							//
							MainWindow.this.netGUI.setChanged(true);
							MainWindow.this.netGUI.updateAll();
						}

						RoleRelationMaker.applyModel(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation(), model, maxIterations);

						MainWindow.this.netGUI.addRelationTab(model);
						if (report.outputs().isNotEmpty()) {
							MainWindow.this.netGUI.addReportTab(report);
						}

						//
						MainWindow.this.netGUI.setChanged(true);

						//
						MainWindow.this.netGUI.updateAll();

					}
				} catch (final PuckException exception) {
					exception.printStackTrace();

					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.title");

					String message = exception.getMessage();
					if (message == null) {
						message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.default");
					}

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				} catch (XMLBadFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		mnRelations.add(mntmImportFromKaes);

		JMenu mnSequences = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnSequences.text"));
		menuBar.add(mnSequences);

		JSeparator separator_22 = new JSeparator();
		mnSequences.add(separator_22);

		JMenuItem mntmSpaceTimeAnalysis = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmSpaceTimeAnalysis.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmSpaceTimeAnalysis.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Sequence Analysis.
				try {
					AttributeDescriptors attributeDescriptors = AttributeWorker.getExogenousAttributeDescriptors(MainWindow.this.netGUI.getSegmentation(), null);

					SequenceCriteria criteria = SequenceAnalysisInputDialog.showDialog(MainWindow.this.netGUI.getSegmentation(), MainWindow.this.netGUI
							.getNet().relationModels(), attributeDescriptors);

					if (criteria != null) {
						
						criteria.setGeography(MainWindow.this.netGUI.getSegmentation().getGeography());
						// Optionalize via input dialog
						criteria.setThreshold(20);
						criteria.setGroupAffiliationLabel("PATRIL_ID");
						criteria.setImpersonalAlterLabel("NOHOST");
						criteria.setReferentRelationLabel("REFREL");

						if (criteria != null) {

							criteria.setReferenceYear(Calendar.getInstance().get(Calendar.YEAR));
							//
							ReportList reports = SequenceReporter.reportSequenceAnalysis(MainWindow.this.netGUI.getNet(),
									MainWindow.this.netGUI.getSegmentation(), criteria, ResourceBundle.getBundle("org.tip.puckgui.messages"));

							for (Report report : reports) {
								//
								MainWindow.this.netGUI.addReportTab(report);
							}

							if (criteria.getTrajectoriesOperations().contains(TrajectoriesOperation.DRAW)) {

								Geography geography = MainWindow.this.netGUI.getNet().getGeography();

								for (Graph placeNameGraph : reports.getGraphs()) {

									Graph<Place> geocodedGraph = GeocodingWorker.geocodeGraph(geography, placeNameGraph);

									String label = String.format("%s-%s", MainWindow.this.netGUI.getNet().getLabel(), "SpaceTime Cartography");
									logger.debug("geocoded graph label=[{}]", label);
									geocodedGraph.setLabel(label);

									MapPanel geographyMap = new MapPanel(geocodedGraph);
									addTab("SpaceTime Cartography", geographyMap);
/*									ConfigGeocodingDialog dialog = new ConfigGeocodingDialog(MainWindow.this.netGUI, placeNameGraph);
									dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
									dialog.setVisible(true);*/
								}								
							}
						}
					}
				} catch (final PuckException exception) {
					//
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		mnSequences.add(mntmSpaceTimeAnalysis);

		JMenu menu = new JMenu("Transform");
		mnSequences.add(menu);

		JMenuItem menuItem = new JMenuItem("Add ChildRoles");
		menu.add(menuItem);

		JMenu menu_1 = new JMenu("Controls");
		mnSequences.add(menu_1);

		JMenuItem menuItem_5 = new JMenuItem("Discontinuous Itineraries");
		menuItem_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					AttributeDescriptors attributeDescriptors = AttributeWorker.getExogenousAttributeDescriptors(
							MainWindow.this.netGUI.getCurrentRelations(), null);
					SequenceCriteria criteria = DiscontinuousItinerariesInputDialog.showDialog(MainWindow.this.netGUI.getNet().relationModels(), attributeDescriptors);
					
					if (criteria != null) {
						
						criteria.setGeography(MainWindow.this.netGUI.getSegmentation().getGeography());
						Report report = SequenceReporter.reportDiscontinuousItineraries(MainWindow.this.netGUI.getSegmentation(), criteria, ResourceBundle.getBundle("org.tip.puckgui.messages"));
						MainWindow.this.netGUI.addReportTab(report);
						
					}
				} catch (Exception exception) {
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		menu_1.add(menuItem_5);

		JMenuItem menuItem_6 = new JMenuItem("Discontinous Biographies");
		menuItem_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					AttributeDescriptors attributeDescriptors = AttributeWorker.getExogenousAttributeDescriptors(
							MainWindow.this.netGUI.getCurrentRelations(), null);
					SequenceCriteria criteria = DiscontinuousItinerariesInputDialog.showDialog(MainWindow.this.netGUI.getNet().relationModels(), attributeDescriptors);
					
					if (criteria != null) {
						
						criteria.setGeography(MainWindow.this.netGUI.getSegmentation().getGeography());
						Report report = SequenceReporter.reportDiscontinuousBiographies(MainWindow.this.netGUI.getNet(), MainWindow.this.netGUI.getSegmentation(), criteria, ResourceBundle.getBundle("org.tip.puckgui.messages"));
						MainWindow.this.netGUI.addReportTab(report);
						
					}
				} catch (Exception exception) {
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		menu_1.add(menuItem_6);

		JMenuItem menuItem_7 = new JMenuItem("Missing Witnesses");
		menuItem_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					MissingTestimoniesCriteria criteria = MissingTestimoniesInputDialog.showDialog(MainWindow.this.netGUI.getNet().relationModels());
					
					if (criteria != null) {
						
						Report report = SequenceReporter.reportMissingTestimonies(MainWindow.this.netGUI.getSegmentation(), criteria, ResourceBundle.getBundle("org.tip.puckgui.messages"));
						MainWindow.this.netGUI.addReportTab(report);
						
					}
				} catch (Exception exception) {
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		menu_1.add(menuItem_7);
		
		JMenuItem menuItem_4 = new JMenuItem("Unknown Relations");
		menuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					AttributeDescriptors attributeDescriptors = AttributeWorker.getExogenousAttributeDescriptors(
							MainWindow.this.netGUI.getCurrentRelations(), null);
					SequenceCriteria criteria = DiscontinuousItinerariesInputDialog.showDialog(MainWindow.this.netGUI.getNet().relationModels(), attributeDescriptors);
					
					if (criteria != null) {
						
						Report report = SequenceReporter.reportUnknownRelations(MainWindow.this.netGUI.getSegmentation(), criteria, ResourceBundle.getBundle("org.tip.puckgui.messages"));
						MainWindow.this.netGUI.addReportTab(report);
						
					}
				} catch (Exception exception) {
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		menu_1.add(menuItem_4);

		JMenu mnDraw = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnDraw.text")); //$NON-NLS-1$ //$NON-NLS-2$
		menuBar.add(mnDraw);

		JMenuItem mntmOregraph = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmOregraph.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmOregraph.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Ore-Graph visualization.
				//
				Graph<Individual> graph = NetUtils.createOreGraph(MainWindow.this.netGUI.getSegmentation());

				//
				VisualizationController vizController = VisualizationController.getSharedInstance();
				VisualizationPanel buildView = vizController.buildView(graph, OreGraph.class);

				addTab("Ore-Graph Draw", buildView);
			}
		});
		mnDraw.add(mntmOregraph);

		JMenuItem mntmPgraph = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmPgraph.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmPgraph.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// P-Graph visualization.
				//
				Graph<Individual> graph = NetUtils.createOreGraph(MainWindow.this.netGUI.getSegmentation());

				//
				VisualizationController vizController = VisualizationController.getSharedInstance();
				VisualizationPanel buildView = vizController.buildView(graph, PGraph.class);

				addTab("P-Graph Draw", buildView);
			}
		});
		mnDraw.add(mntmPgraph);

		JMenuItem mntmFullDiagram = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmFullDiagram.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmFullDiagram.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Draw full diagram.
				Individuals currentIndividuals = MainWindow.this.netGUI.getCurrentIndividuals();
				JPanel diagramPanel = new GlobalDiagramPanel(MainWindow.this.netGUI, currentIndividuals);
				addTab("Full diagram", diagramPanel);
			}
		});

		JMenuItem mntmKinoath = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmKinoath.text"));
		mntmKinoath.setEnabled(false);
		mnDraw.add(mntmKinoath);
		mnDraw.add(mntmFullDiagram);

		JMenuItem mntmComponentDiagrams = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmComponentDiagram.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmComponentDiagrams.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Component Diagram.
				Individuals currentIndividuals = MainWindow.this.netGUI.getCurrentIndividuals();
				IndividualGroups groups = ComponentWorker.searchKinComponents(currentIndividuals);
				JPanel diagramPanel = new IndividualGroupDiagramsPanel(MainWindow.this.netGUI, groups);
				addReportTab("Component diagrams", diagramPanel);
			}
		});

		JMenuItem mntmIndividualDiagrams = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmIndividualDiagram.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmIndividualDiagrams.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Draw individual diagram.
				JPanel diagramPanel = new IndividualDiagramsPanel(MainWindow.this.netGUI);
				((IndividualDiagramsPanel) diagramPanel).update(getIndividualsTab().getSelectedIndividual());
				addReportTab("Ind. diagrams", diagramPanel);
			}
		});
		mnDraw.add(mntmIndividualDiagrams);

		JMenuItem mntmSegmentDiagrams = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmPartitionDiagrams.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmSegmentDiagrams.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Segment diagram.
				IndividualGroups groups = SegmentationWorker.convert(MainWindow.this.netGUI.getSegmentation().getCurrentSegment());
				JPanel diagramPanel = new IndividualGroupDiagramsPanel(MainWindow.this.netGUI, groups);
				addReportTab("Segment diagrams", diagramPanel);
			}
		});
		mnDraw.add(mntmSegmentDiagrams);
		mnDraw.add(mntmComponentDiagrams);

		JMenuItem mntmGeneaquilt = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmGeneaquilt.text"));
		mntmGeneaquilt.setEnabled(false);
		mnDraw.add(mntmGeneaquilt);

		JMenuItem mntmGeneaQuiltViewFull = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmQuiltDiagram.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmGeneaQuiltViewFull.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				Network network = GeneaQuiltConvert.convertToGeneaQuilt(MainWindow.this.netGUI.getNet());
				logger.debug("convert done.");
				JPanel geneaQuiltPanel = new GeneaQuiltPanel(MainWindow.this.tabbedPaneCorpus, network);
				logger.debug("panel done.");
				addReportTab("Full quilt", geneaQuiltPanel);
			}
		});
		mnDraw.add(mntmGeneaQuiltViewFull);

		JMenuItem mntmGeneaQuiltSegment = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmSegmentQuilt.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmGeneaQuiltSegment.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				Network network = GeneaQuiltConvert.convertToGeneaQuilt(MainWindow.this.netGUI.getCurrentIndividuals(),
						MainWindow.this.netGUI.getCurrentFamilies());
				logger.debug("convert done.");
				JPanel geneaQuiltPanel = new GeneaQuiltPanel(MainWindow.this.tabbedPaneCorpus, network);
				logger.debug("panel done.");
				addReportTab("Segment quilt", geneaQuiltPanel);
			}
		});
		mnDraw.add(mntmGeneaQuiltSegment);

		JMenuItem mntmGeneaQuiltSegments = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmSegmentQuilts.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmGeneaQuiltSegments.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				IndividualGroups groups = SegmentationWorker.convert(MainWindow.this.netGUI.getSegmentation().getCurrentSegment());
				JPanel diagramPanel = new IndividualGroupQuiltsPanel(MainWindow.this.netGUI, groups);
				addReportTab("Segment quilts", diagramPanel);
			}
		});
		mnDraw.add(mntmGeneaQuiltSegments);

		JMenuItem mntmGeneaQuiltComponents = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmComponentQuilts.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmGeneaQuiltComponents.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				Individuals currentIndividuals = MainWindow.this.netGUI.getCurrentIndividuals();
				IndividualGroups groups = ComponentWorker.searchKinComponents(currentIndividuals);
				JPanel quiltPanel = new IndividualGroupQuiltsPanel(MainWindow.this.netGUI, groups);
				addReportTab("Component quilts", quiltPanel);
			}
		});
		mnDraw.add(mntmGeneaQuiltComponents);

		JMenuItem mntmEventquiltViews = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmEventquiltViews.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmEventquiltViews.setEnabled(false);
		mnDraw.add(mntmEventquiltViews);

		JMenuItem mntmEventQuiltFull = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmFullQuilt.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmEventQuiltFull.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				Network network = GeneaQuiltConvert.convertToEventQuilt(MainWindow.this.netGUI.getNet());
				logger.debug("convert done.");
				JPanel geneaQuiltPanel = new GeneaQuiltPanel(MainWindow.this.tabbedPaneCorpus, network, VertexComparator.Sorting.DATERANGE_START);
				logger.debug("panel done.");
				addReportTab("Full event quilt", geneaQuiltPanel);
			}
		});
		mnDraw.add(mntmEventQuiltFull);

		JMenuItem mntmEventQuiltComponents = new JMenuItem("Component quilts");
		mntmEventQuiltComponents.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				Individuals currentIndividuals = MainWindow.this.netGUI.getCurrentIndividuals();
				IndividualGroups groups = ComponentWorker.searchComponents(currentIndividuals);
				JPanel quiltPanel = new IndividualGroupEventQuiltsPanel(MainWindow.this.netGUI, groups);
				addReportTab("Component event quilts", quiltPanel);
			}
		});

		JMenuItem mntmEvenQuiltSegment = new JMenuItem("Segment quilt");
		mntmEvenQuiltSegment.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				Network network = GeneaQuiltConvert.convertToEventQuilt(MainWindow.this.netGUI.getCurrentIndividuals(),
						MainWindow.this.netGUI.getCurrentFamilies(), MainWindow.this.netGUI.getCurrentRelations());
				logger.debug("convert done.");
				JPanel geneaQuiltPanel = new GeneaQuiltPanel(MainWindow.this.tabbedPaneCorpus, network, VertexComparator.Sorting.DATERANGE_START);
				logger.debug("panel done.");
				addReportTab("Segment event quilt", geneaQuiltPanel);

			}
		});
		mnDraw.add(mntmEvenQuiltSegment);

		JMenuItem mntmEventQuiltSegments = new JMenuItem("Segment quilts");
		mntmEventQuiltSegments.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				IndividualGroups groups = SegmentationWorker.convert(MainWindow.this.netGUI.getSegmentation().getCurrentSegment());
				JPanel diagramPanel = new IndividualGroupEventQuiltsPanel(MainWindow.this.netGUI, groups);
				addReportTab("Segment event quilts", diagramPanel);
			}
		});
		mnDraw.add(mntmEventQuiltSegments);
		mnDraw.add(mntmEventQuiltComponents);

		JMenuItem mntmGeoViews = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmGeoViews.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmGeoViews.setEnabled(false);
		mnDraw.add(mntmGeoViews);

		JMenuItem mntmNetPlaces = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmNetPlaces.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmNetPlaces.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				// Geo views : Net Places.
				//
				GeoPlaceNetCriteria criteria = GeoPlaceNetDialog.showDialog(MainWindow.this.netGUI.getNet().relationModels().nameList());

				if (criteria != null) {
					//
					AttributeValueDescriptors valueDescriptors = AttributeWorker.getExogenousAttributeValueDescriptors(MainWindow.this.netGUI.getNet(),
							criteria.getScopes(), "^.*_PLAC.*$");

					double weightStep;
					if (criteria.isWeight()) {
						weightStep = 1;
					} else {
						weightStep = 0;
					}

					Graph<Place> geocodedGraph = GeocodingWorker.geocodeAttributeValueDescriptors(MainWindow.this.netGUI.getNet().getGeography(),
							valueDescriptors, weightStep);

					String label = String.format("%s-%s", MainWindow.this.netGUI.getNet().getLabel(), "Net Places");
					logger.debug("geocoded graph label=[{}]", label);
					geocodedGraph.setLabel(label);

					MapPanel cartographyMap = new MapPanel(geocodedGraph);

					addTab("NetPlaces Cartography", cartographyMap);

					//
					String title = "Information";
					StringList buffer = new StringList();
					buffer.append("Toponym count   =").appendln(valueDescriptors.size());
					buffer.append("Graph node count=").appendln(geocodedGraph.nodeCount());
					String message = buffer.toString();
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});

		JMenuItem mntmGeographyPlaces = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmGeographyPlaces.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmGeographyPlaces.addActionListener(new ActionListener() {
			/**
					 * 
					 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				// Geography Places.
				//
				Geography geography = MainWindow.this.netGUI.getNet().getGeography();

				if ((geography == null) || (geography.isEmpty())) {
					//
					String title = "Warning message";
					String message = "Geography missing.";

					//
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
				} else {
					//
					StringSet toponyms = geography.getMainToponyms();

					Graph<Place> geocodedGraph = GeocodingWorker.geocodeToponyms(MainWindow.this.netGUI.getNet().getGeography(), toponyms);

					String label = String.format("%s-%s", MainWindow.this.netGUI.getNet().getLabel(), "Geography Places");
					logger.debug("geocoded graph label=[{}]", label);
					geocodedGraph.setLabel(label);

					MapPanel cartographyMap = new MapPanel(geocodedGraph);

					addTab("Geography Cartography", cartographyMap);

					//
					String title = "Information";
					StringList buffer = new StringList();
					buffer.append("Toponym count   =").appendln(toponyms.size());
					buffer.append("Graph node count=").appendln(geocodedGraph.nodeCount());
					String message = buffer.toString();
					JOptionPane.showMessageDialog(MainWindow.this.frmPuck, message, title, JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		mnDraw.add(mntmGeographyPlaces);
		mnDraw.add(mntmNetPlaces);

		JMenu mnTools = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnTools.text")); //$NON-NLS-1$ //$NON-NLS-2$
		menuBar.add(mnTools);

		JMenuItem mntmCalculator = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmCalculator.text"));
		mntmCalculator.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Call calculator.
				CalculatorWindow window = new CalculatorWindow(MainWindow.this.netGUI);
				window.getJFrame().setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.getJFrame().setVisible(true);
			}
		});
		mnTools.add(mntmCalculator);

		JMenuItem mntmFileBatchConverter = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmFileBatchConverter.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmFileBatchConverter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// File Batch Converter.
				FileBatchConverterCriteria criteria = FileBatchConverterDialog.showDialog();
				if (criteria != null) {
					Report report = FileBatchConverter.convert(criteria);

					if (report.outputs().isNotEmpty()) {
						addReportTab(report);
					}
				}
			}
		});
		mnTools.add(mntmFileBatchConverter);

		JMenuItem mntmFlatdbgeonames = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmFlatdbgeonames.text"));
		mntmFlatdbgeonames.setHorizontalAlignment(SwingConstants.CENTER);
		mntmFlatdbgeonames.setEnabled(false);
		mnTools.add(mntmFlatdbgeonames);

		JMenuItem mntmDownloadFlatdbgeonamesDatabase = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmDownloadFlatdbgeonamesDatabase.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmDownloadFlatdbgeonamesDatabase.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// FlatDB4GeoNames : Download database.
				//
				DownloadDatabaseDialog.showDialog();
			}
		});
		mnTools.add(mntmDownloadFlatdbgeonamesDatabase);

		JMenuItem mntmRequest = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmRequest.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmRequest.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// FlatDB4GeoNames : request.
				//
				addTab("FlatDB4GeoNames", new FlatDB4GeoNamesRequestPanel());
			}
		});
		mnTools.add(mntmRequest);

		JMenu mnWindows = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnWindows.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnWindows.addMenuListener(new MenuListener() {
			@Override
			public void menuCanceled(final MenuEvent event) {
			}

			@Override
			public void menuDeselected(final MenuEvent event) {
				// Windows menu cleaning.
				JMenu windowsMenu = (JMenu) event.getSource();
				windowsMenu.removeAll();
			}

			@Override
			public void menuSelected(final MenuEvent event) {
				// Windows menu updating.
				JMenu windowsMenu = (JMenu) event.getSource();
				for (WindowGUI windowGUI : PuckGUI.instance().windowGUIs()) {
					windowsMenu.add(new WindowMenuItem(windowGUI));
				}
			}
		});

		menuBar.add(mnWindows);

		JMenu mnHelp = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnHelp.text"));
		mnHelp.setMnemonic('H');
		menuBar.add(mnHelp);

		JMenuItem mntmOfficialWebSite = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmOfficialWebSite.text"));
		mntmOfficialWebSite.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Puck Help.
				callPuckHelp((JMenuItem) event.getSource());
			}
		});
		mnHelp.add(mntmOfficialWebSite);

		JMenuItem mntmAboutPuck = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmAboutPuck.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmAboutPuck.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// About Puck.
				AboutPopup.showDialog();
			}
		});

		JSeparator separator_3 = new JSeparator();
		mnHelp.add(separator_3);
		mnHelp.add(mntmAboutPuck);

		JMenu mnFoo = new JMenu(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mnFoo.text"));
		mnFoo.setVisible(false);
		mnFoo.setEnabled(false);
		menuBar.add(mnFoo);

		JMenuItem mntmFooworker = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmFooworker.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnFoo.add(mntmFooworker);

		JMenuItem mntmTestDev = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmTestDev.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mnFoo.add(mntmTestDev);
		mntmTestDev.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Test dev.
				// Launch.
				try {
					GraphReporter.test();
				} catch (PuckException exception) {
					// TODO Auto-generated catch block
					exception.printStackTrace();
				}
			}
		});
		mntmTestDev.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_MASK));

		JMenuItem mntmVisualizationTest = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmVisualizationTdev.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmVisualizationTest.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Visualization test.
				try {
					org.tip.puckgui.views.visualization.Test.test(MainWindow.this.netGUI);
				} catch (MalformedURLException exception) {
					// TODO Auto-generated catch block
					exception.printStackTrace();
				} catch (PuckException exception) {
					// TODO Auto-generated catch block
					exception.printStackTrace();
				} catch (URISyntaxException exception) {
					// TODO Auto-generated catch block
					exception.printStackTrace();
				}
			}
		});
		mntmVisualizationTest.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		mnFoo.add(mntmVisualizationTest);
		mntmFooworker.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Preferences.
				FooReporterInputWindow window = new FooReporterInputWindow(MainWindow.this.netGUI);
				window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				window.setVisible(true);
			}
		});
		this.frmPuck.getContentPane().setLayout(new BoxLayout(this.frmPuck.getContentPane(), BoxLayout.Y_AXIS));

		this.segmentationPanel = new SegmentationPanel(this.netGUI);
		this.frmPuck.getContentPane().add(this.segmentationPanel);

		this.tabbedPaneCorpus = new JTabbedPane(JTabbedPane.TOP);
		this.frmPuck.getContentPane().add(this.tabbedPaneCorpus);

		this.corpusTab = new CorpusPanel(this.netGUI);
		this.tabbedPaneCorpus.addTab(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.corpusTab.title"), null, this.corpusTab, null); //$NON-NLS-1$ //$NON-NLS-2$

		this.individualsTab = new IndividualsPanel(this.netGUI);
		this.tabbedPaneCorpus.addTab(
				ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.individualsPanel.title"), null, this.individualsTab, null); //$NON-NLS-1$ //$NON-NLS-2$

		this.familiesTab = new FamiliesPanel(this.netGUI);
		this.tabbedPaneCorpus.addTab(
				ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.familiesPanel.title"), null, this.familiesTab, null); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * 
	 * @return
	 */
	public boolean isGeographyEditorTab() {
		boolean result;

		boolean ended = false;
		int tabIndex = 0;
		result = false;
		while (!ended) {
			if (tabIndex < this.tabbedPaneCorpus.getComponentCount()) {
				Component tab = this.tabbedPaneCorpus.getComponent(tabIndex);

				if (tab.getClass() == GeographyEditorPanel.class) {
					result = true;
					ended = true;
				} else {
					tabIndex += 1;
				}
			} else {
				result = false;
				ended = true;
			}
		}

		//
		return result;
	}

	/**
	 * Performs a save as BAR action.
	 * 
	 * @param displayFileChooser
	 *            Flag to display a file chooser.
	 * 
	 * @param persistFileName
	 *            Flag to persist the file name in the current netGUI.
	 */
	protected void performBARSave() {
		// Save As.
		try {
			//
			File targetFile = ExportBARFileSelector.showSelectorDialog(this.frmPuck, MainWindow.this.netGUI.getFile());

			if (targetFile != null) {
				//
				BARFile.save(targetFile, this.netGUI.getNet());
			}
		} catch (final PuckException exception) {
			//
			String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.title");

			//
			String message;
			switch (PuckExceptions.valueOf(exception.getCode())) {
				case UNSUPPORTED_FILE_FORMAT:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedFileFormat");
				break;
				case NOT_A_FILE:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.notAFile");
				break;
				case UNSUPPORTED_ENCODING:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedEncoding");
				break;
				default:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.default");
			}

			//
			JOptionPane.showMessageDialog(this.frmPuck, message, title, JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * 
	 */
	private void performCloseCorpus() {
		//
		if ((!this.netGUI.isChanged()) || ((this.netGUI.isChanged()) && (ConfirmMainWindowCloseDialog.showDialog(this.frmPuck, this.netGUI)))) {
			//
			if (PuckGUI.instance().windowGUIs().size() == 1) {
				//
				if (!this.netGUI.isBlank()) {
					this.netGUI.setBlank();
				}
			} else {
				//
				PuckGUI.instance().close(this.netGUI);
				this.frmPuck.dispose();
			}
		}
	}

	/**
	 * 
	 */
	private void performCloseWindow() {
		//
		if ((!this.netGUI.isChanged()) || ((this.netGUI.isChanged()) && (ConfirmMainWindowCloseDialog.showDialog(this.frmPuck, this.netGUI)))) {
			//
			if (PuckGUI.instance().windowGUIs().size() == 1) {
				//
				if (this.netGUI.isBlank()) {
					//
					PuckGUI.instance().exit();
				} else {
					//
					this.netGUI.setBlank();
				}
			} else {
				//
				PuckGUI.instance().close(this.netGUI);
				this.frmPuck.dispose();
			}
		}
	}

	/**
	 * 
	 */
	private void performQuit() {
		//
		if ((!PuckGUI.instance().existsUnsavedChanges()) || ((PuckGUI.instance().existsUnsavedChanges()) && (ConfirmQuitDialog.showDialog(this.frmPuck)))) {
			//
			PuckGUI.instance().exit();
		}
	}

	/**
	 * 
	 * @param family
	 */
	public void selectFamiliesTab(final Family family) {
		this.tabbedPaneCorpus.setSelectedIndex(2);
		this.familiesTab.select(family);
	}

	/**
	 * 
	 */
	public void selectIndividualsTab() {
		this.tabbedPaneCorpus.setSelectedIndex(1);
	}

	/**
	 * 
	 */
	public void selectIndividualsTab(final Individual individual) {
		this.tabbedPaneCorpus.setSelectedIndex(1);
		this.individualsTab.select(individual);
	}

	/**
	 * 
	 * @param relation
	 */
	public void selectRelationTab(final Relation relation) {
		int tabIndex = getRelationTabIndex(relation.getModel());
		if (tabIndex != -1) {
			this.tabbedPaneCorpus.setSelectedIndex(tabIndex);
			RelationsPanel tab = (RelationsPanel) this.tabbedPaneCorpus.getComponent(tabIndex);
			tab.select(relation);
		}
	}

	/**
	 * 
	 * @param tabIndex
	 */
	public void selectTab(final int tabIndex) {
		this.tabbedPaneCorpus.setSelectedIndex(tabIndex);
	}

	/**
	 * 
	 */
	public void setIndividualsTab(final Individual individual) {
		this.individualsTab.select(individual);
	}

	public void toFront() {
		this.frmPuck.toFront();
	}

	/**
	 * 
	 * @param locale
	 */
	public void updateLocale(final Locale locale) {
		this.frmPuck.setLocale(locale);
		// initialize();
	}

	/**
	 * 
	 */
	public void updateMenuItems() {
		//
		if (this.netGUI.isChanged()) {
			//
			this.mntmSave.setEnabled(true);

			//
			if (this.netGUI.getFile() == null) {
				//
				this.mntmRevert.setEnabled(false);

			} else {
				//
				this.mntmRevert.setEnabled(this.netGUI.getFile().exists());
			}
			this.mntmRevert.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmRevert.text"));

		} else {
			//
			this.mntmSave.setEnabled(false);

			if (this.netGUI.getFile() == null) {
				//
				this.mntmRevert.setEnabled(false);
				this.mntmRevert.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmRevert.text"));

			} else {
				//
				this.mntmRevert.setEnabled(this.netGUI.getFile().exists());
				this.mntmRevert.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.mntmReload.text"));
			}
		}
	}

	/**
	 * 
	 */
	public void updateTitle() {
		String changeToken;
		if (this.netGUI.isChanged()) {
			changeToken = "*";

			// Set MAC OS X feature.
			this.frmPuck.getRootPane().putClientProperty("Window.documentModified", Boolean.TRUE);
		} else {
			changeToken = "";

			// Set MAC OS X feature.
			this.frmPuck.getRootPane().putClientProperty("Window.documentModified", Boolean.FALSE);
		}

		this.frmPuck.setTitle(changeToken + this.netGUI.getFile().getName() + "-" + this.netGUI.getId() + " - Puck");
	}

	/**
	 * 
	 */
	public static void callPuckHelp(final JMenuItem source) {
		String HELP_URL = "http://www.kintip.net/puck-aide-mainmenu-24";
		try {
			if (Desktop.isDesktopSupported()) {
				Desktop desktop = Desktop.getDesktop();
				if (desktop.isSupported(Desktop.Action.BROWSE)) {
					//
					java.net.URI uri = new java.net.URI(HELP_URL);
					desktop.browse(uri);
				} else if (source != null) {
					source.setEnabled(false);
				}
			} else if (source != null) {
				source.setEnabled(false);
			}
		} catch (java.io.IOException ioe) {
			ioe.printStackTrace();
			logger.error("The system cannot find the URL specified: [" + HELP_URL + "]");
			JOptionPane.showMessageDialog(null, "Sorry, browser call failed.", "Error", JOptionPane.ERROR_MESSAGE);
		} catch (java.net.URISyntaxException use) {
			use.printStackTrace();
			logger.error("Illegal character in path [" + HELP_URL + "]");
			JOptionPane.showMessageDialog(null, "Sorry, browser call failed.", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * 
	 */
	public static int getDesktopHeight() {
		int result;

		Toolkit toolkit = Toolkit.getDefaultToolkit();

		// Get default screen size.
		Dimension dim = toolkit.getScreenSize();

		// Get default screen configuration.
		GraphicsConfiguration gconf = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();

		// Get screen insets.
		Insets insets = toolkit.getScreenInsets(gconf);

		// Compute the desktop height.
		result = (int) (dim.getHeight() - insets.bottom - insets.top);

		//
		return result;
	}

	/**
	 * 
	 * @param netGUI
	 * @param parentPanel
	 * @param file
	 * @param charsetName
	 */
	public static void openConflictingFile(final NetGUI netGUI, final JFrame parentPanel, final File file, final String charsetName) {
		//
		try {
			// Request arbitration.
			Object[] options = { "IUR", "BAR", "Cancel" }; // Order is reversed
															// in dialog.
			int choice = JOptionPane.showOptionDialog(parentPanel, "Puck can not determine the file format. Please, speficy it.", "BAR or IUR?",
					JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, null);

			//
			if (choice != 2) {
				//
				Net net;
				if (choice == 1) {
					net = BARFile.load(file, charsetName);
				} else {
					net = IURFile.load(file, charsetName);
				}

				//
				PuckGUI.instance().recentFiles().updateFile(file);
				PuckGUI.instance().recentFolders().updateFolder(file.getParentFile());

				//
				if (netGUI.isBlank()) {
					//
					netGUI.updateFile(file, net);
				} else {
					//
					PuckGUI.instance().createNetGUI(file, net);
				}
			}
		} catch (final PuckException exception) {
			//
			exception.printStackTrace();

			//
			String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.title");

			//
			String message;
			switch (PuckExceptions.valueOf(exception.getCode())) {
				case FILE_NOT_FOUND:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.fileNotFound");
					JOptionPane.showMessageDialog(parentPanel, message, title, JOptionPane.ERROR_MESSAGE);
				break;
				case BAD_FILE_FORMAT:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.badFileFormat");
					JOptionPane.showMessageDialog(parentPanel, message, title, JOptionPane.ERROR_MESSAGE);
				break;
				case UNSUPPORTED_FILE_FORMAT:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.unsupportedFileFormat");
					JOptionPane.showMessageDialog(parentPanel, message, title, JOptionPane.ERROR_MESSAGE);
				break;
				case NOT_A_FILE:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.notAFile");
					JOptionPane.showMessageDialog(parentPanel, message, title, JOptionPane.ERROR_MESSAGE);
				break;
				case UNSUPPORTED_ENCODING:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.unsupportedEncoding");
					JOptionPane.showMessageDialog(parentPanel, message, title, JOptionPane.ERROR_MESSAGE);
				break;
				default:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.default");
					JOptionPane.showMessageDialog(parentPanel, message, title, JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * This classes is a static one to be called by OpenRecentMenuItem.java
	 * 
	 * 
	 * @param netGUI
	 * @param parentPanel
	 * @param folder
	 */
	public static void openSelectedFile(final NetGUI netGUI, final JFrame parentPanel, final File file, final String charsetName) {
		//
		try {
			//
			if (file != null) {
				//
				if (netGUI.isBlank()) {
					//
					netGUI.updateFile(file, charsetName);
				} else {
					//
					PuckGUI.instance().createNetGUI(file, charsetName);
				}

				//
				PuckGUI.instance().recentFiles().updateFile(file);
				PuckGUI.instance().recentFolders().updateFolder(file.getParentFile());
			}
		} catch (final PuckException exception) {
			//
			exception.printStackTrace();

			//
			String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.title");

			//
			String message;
			switch (PuckExceptions.valueOf(exception.getCode())) {
				case FILE_NOT_FOUND:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.fileNotFound");
					JOptionPane.showMessageDialog(parentPanel, message, title, JOptionPane.ERROR_MESSAGE);
				break;
				case BAD_FILE_FORMAT:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.badFileFormat");
					if (StringUtils.isNotBlank(exception.getMessage())) {
						message += "\n" + exception.getMessage();
					}
					JOptionPane.showMessageDialog(parentPanel, message, title, JOptionPane.ERROR_MESSAGE);
				break;
				case UNSUPPORTED_FILE_FORMAT:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.unsupportedFileFormat");
					JOptionPane.showMessageDialog(parentPanel, message, title, JOptionPane.ERROR_MESSAGE);
				break;
				case NOT_A_FILE:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.notAFile");
					JOptionPane.showMessageDialog(parentPanel, message, title, JOptionPane.ERROR_MESSAGE);
				break;
				case UNSUPPORTED_ENCODING:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.unsupportedEncoding");
					JOptionPane.showMessageDialog(parentPanel, message, title, JOptionPane.ERROR_MESSAGE);
				break;
				case FORMAT_CONFLICT:
					openConflictingFile(netGUI, parentPanel, file, charsetName);
				break;
				default:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.default");
					JOptionPane.showMessageDialog(parentPanel, message, title, JOptionPane.ERROR_MESSAGE);
			}

		} catch (final Exception exception) {
			//
			exception.printStackTrace();

			// Try to load GEDCOM file with long IDs.
			try {
				if (IdShrinker.containsLongId(file)) {
					JOptionPane.showMessageDialog(parentPanel,
							"GEDCOM file with long ids detected.\nWill try to load it with shrinked ids.\nWarning: new file will be created.",
							"GEDCOM Long IDs detected.", JOptionPane.WARNING_MESSAGE);
					File fixedfile = IdShrinker.shrink(file);

					//
					if (netGUI.isBlank()) {
						netGUI.updateFile(fixedfile, charsetName);
					} else {
						PuckGUI.instance().createNetGUI(fixedfile, charsetName);
					}

				} else {
					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.title");

					String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.default");

					//
					JOptionPane.showMessageDialog(parentPanel, message, title, JOptionPane.ERROR_MESSAGE);
				}
			} catch (IOException subException) {
				subException.printStackTrace();

				//
				String subtitle = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.title");
				String submessage = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.default");

				//
				JOptionPane.showMessageDialog(parentPanel, submessage, subtitle, JOptionPane.ERROR_MESSAGE);
			} catch (PuckException subException) {
				subException.printStackTrace();

				//
				String subtitle = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.title");
				String submessage = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.openFileError.default");

				//
				JOptionPane.showMessageDialog(parentPanel, submessage, subtitle, JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * 
	 * @param parent
	 * @param netgui
	 * @return
	 */
	public static boolean performSaveAs(final Component parent, final NetGUI netGUI) {
		boolean result;

		// Save As.
		try {
			//
			File targetFile = CorpusSaveFileSelector.showSelectorDialog(parent, netGUI.getFile());

			if (targetFile == null) {
				//
				result = false;

			} else {
				//
				PuckManager.saveNet(targetFile, netGUI.getNet());

				//
				netGUI.setFile(targetFile);

				//
				netGUI.setChanged(false);

				//
				PuckGUI.instance().recentFiles().updateFile(targetFile);
				PuckGUI.instance().recentFolders().updateFolder(targetFile.getParentFile());

				//
				result = true;
			}
		} catch (final PuckException exception) {
			//
			String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.title");

			//
			String message;
			switch (PuckExceptions.valueOf(exception.getCode())) {
				case UNSUPPORTED_FILE_FORMAT:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedFileFormat");
				break;
				case NOT_A_FILE:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.notAFile");
				break;
				case UNSUPPORTED_ENCODING:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedEncoding");
				break;
				default:
					message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.default") + "\n" + exception.getMessage();
			}

			//
			JOptionPane.showMessageDialog(parent, message, title, JOptionPane.ERROR_MESSAGE);

			//
			result = false;

		} catch (final Exception exception) {
			// Show trace.
			exception.printStackTrace();

			//
			String title = "Error computerum est";
			String message = "Error occured during working: " + exception.getMessage();

			//
			JOptionPane.showMessageDialog(parent, message, title, JOptionPane.ERROR_MESSAGE);

			//
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * 
	 * @param netGUI
	 * @param parentPanel
	 * @param folder
	 */
	public static File selectDATFileToLoad(final Component parentPanel, final File targetFile) {
		File result;

		//
		JFileChooser chooser = new JFileChooser();

		if ((targetFile != null) && (StringUtils.isNotBlank(targetFile.getAbsolutePath()))) {
			chooser.setSelectedFile(ToolBox.setExtension(targetFile, ".dat"));
		}
		chooser.setDialogTitle("Import DAT");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setAcceptAllFileFilterUsed(false);
		GenericFileFilter defaultFileFilter = new GenericFileFilter("DAT (*.dat)", "dat");
		chooser.addChoosableFileFilter(defaultFileFilter);
		chooser.setFileFilter(defaultFileFilter);

		//
		if (chooser.showOpenDialog(parentPanel) == JFileChooser.APPROVE_OPTION) {
			logger.debug("getCurrentDirectory(): " + chooser.getCurrentDirectory());
			logger.debug("getSelectedFile() : " + chooser.getSelectedFile());
			result = chooser.getSelectedFile();
		} else {
			result = null;
		}

		//
		return result;
	}

	/**
	 * This classes is a static one to be called by OpenRecentMenuItem.java
	 * 
	 * 
	 * @param netGUI
	 * @param parentPanel
	 * @param folder
	 */
	public static File selectFile(final JFrame parentPanel, final File folder) {
		File result;

		//
		JFileChooser chooser = new JFileChooser();
		if ((folder != null) && (folder.exists()) && (folder.isDirectory())) {
			chooser.setCurrentDirectory(folder);
		} else if (PuckGUI.instance().recentFolders().isEmpty()) {
			chooser.setCurrentDirectory(new java.io.File("."));
		} else {
			chooser.setCurrentDirectory(PuckGUI.instance().recentFolders().getMoreRecent());
		}

		chooser.setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.openFileChooser.text"));
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setAcceptAllFileFilterUsed(true);
		GenericFileFilter defaultFileFilter = new GenericFileFilter("Genealogic files (*.ged, *.ods, *.paj, *.pl, *.puc, *.tip, *.txt, *.xls, *.xml)", "ged",
				"ods", "paj", "pl", "puc", "tip", "txt", "xls", "xml");
		chooser.addChoosableFileFilter(defaultFileFilter);
		chooser.addChoosableFileFilter(new GenericFileFilter("GEDCOM (*.ged)", "ged"));
		chooser.addChoosableFileFilter(new GenericFileFilter("OpenDocumentFormat Spreadsheet (*.ods)", "ods"));
		chooser.addChoosableFileFilter(new GenericFileFilter("Pajek Network (*.paj)", "paj"));
		chooser.addChoosableFileFilter(new GenericFileFilter("Prolog (*.pl)", "pl"));
		chooser.addChoosableFileFilter(new GenericFileFilter("PUCK (*.puc)", "puc"));
		chooser.addChoosableFileFilter(new GenericFileFilter("TIP (*.tip)", "tip"));
		chooser.addChoosableFileFilter(new GenericFileFilter("Text BAR/IUR (*.txt)", "txt"));
		chooser.addChoosableFileFilter(new GenericFileFilter("Microsoft Excel (*.xls)", "xls"));
		chooser.addChoosableFileFilter(new GenericFileFilter("Extensible Markup Language (*.xml)", "xml"));
		chooser.setFileFilter(defaultFileFilter);

		//
		if (chooser.showOpenDialog(parentPanel) == JFileChooser.APPROVE_OPTION) {
			logger.debug("getCurrentDirectory(): " + chooser.getCurrentDirectory());
			logger.debug("getSelectedFile() : " + chooser.getSelectedFile());
			result = chooser.getSelectedFile();
		} else {
			logger.debug("No Selection ");
			result = null;
		}

		//
		return result;
	}

	/**
	 * This classes is a static one to be called by OpenRecentMenuItem.java
	 * 
	 * 
	 * @param netGUI
	 * @param parentPanel
	 * @param folder
	 */
	public static File selectFolder(final JFrame parentPanel, final File folder) {
		File result;

		//
		JFileChooser chooser = new JFileChooser();
		if ((folder != null) && (folder.exists()) && (folder.isDirectory())) {
			chooser.setCurrentDirectory(folder);
		} else if (PuckGUI.instance().recentFolders().isEmpty()) {
			chooser.setCurrentDirectory(new java.io.File("."));
		} else {
			chooser.setCurrentDirectory(PuckGUI.instance().recentFolders().getMoreRecent());
		}

		chooser.setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.openFileChooser.text"));
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setAcceptAllFileFilterUsed(true);

		//
		if (chooser.showOpenDialog(parentPanel) == JFileChooser.APPROVE_OPTION) {
			logger.debug("getCurrentDirectory(): " + chooser.getCurrentDirectory());
			logger.debug("getSelectedFile() : " + chooser.getSelectedFile());
			result = chooser.getSelectedFile();
		} else {
			logger.debug("No Selection ");
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * 
	 * @param netGUI
	 * @param parentPanel
	 * @param folder
	 */
	public static File selectRelationModelFileToLoad(final Component parentPanel, final File targetFile) {
		File result;

		//
		JFileChooser chooser = new JFileChooser();

		if ((targetFile != null) && (StringUtils.isNotBlank(targetFile.getAbsolutePath()))) {
			chooser.setSelectedFile(ToolBox.setExtension(targetFile, ".txt"));
		}
		chooser.setDialogTitle("Import Relation Model");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setAcceptAllFileFilterUsed(false);
		GenericFileFilter defaultFileFilter = new GenericFileFilter("Selz files (*.ods, *.txt, *.xls)", "ods", "txt", "xls");
		chooser.addChoosableFileFilter(defaultFileFilter);
		chooser.addChoosableFileFilter(new GenericFileFilter("OpenDocumentFormat Spreadsheet (*.ods)", "ods"));
		chooser.addChoosableFileFilter(new GenericFileFilter("Text (*.txt)", "txt"));
		chooser.addChoosableFileFilter(new GenericFileFilter("Microsoft Excel (*.xls)", "xls"));
		chooser.addChoosableFileFilter(new GenericFileFilter("XML file (*.xml)", "xml"));
		chooser.setFileFilter(defaultFileFilter);

		//
		if (chooser.showOpenDialog(parentPanel) == JFileChooser.APPROVE_OPTION) {
			logger.debug("getCurrentDirectory(): " + chooser.getCurrentDirectory());
			logger.debug("getSelectedFile() : " + chooser.getSelectedFile());
			result = chooser.getSelectedFile();
		} else {
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * 
	 * @param netGUI
	 * @param parentPanel
	 * @param folder
	 */
	public static File selectSelzFileToLoad(final Component parentPanel, final File targetFile) {
		File result;

		//
		JFileChooser chooser = new JFileChooser();

		if ((targetFile != null) && (StringUtils.isNotBlank(targetFile.getAbsolutePath()))) {
			chooser.setSelectedFile(ToolBox.setExtension(targetFile, ".txt"));
		}
		chooser.setDialogTitle("Import Selz");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setAcceptAllFileFilterUsed(false);
		GenericFileFilter defaultFileFilter = new GenericFileFilter("Selz files (*.ods, *.txt, *.xls)", "ods", "txt", "xls");
		chooser.addChoosableFileFilter(defaultFileFilter);
		chooser.addChoosableFileFilter(new GenericFileFilter("OpenDocumentFormat Spreadsheet (*.ods)", "ods"));
		chooser.addChoosableFileFilter(new GenericFileFilter("Text (*.txt)", "txt"));
		chooser.addChoosableFileFilter(new GenericFileFilter("Microsoft Excel (*.xls)", "xls"));
		chooser.setFileFilter(defaultFileFilter);

		//
		if (chooser.showOpenDialog(parentPanel) == JFileChooser.APPROVE_OPTION) {
			logger.debug("getCurrentDirectory(): {}", chooser.getCurrentDirectory());
			logger.debug("getSelectedFile(): {}", chooser.getSelectedFile());
			result = chooser.getSelectedFile();
		} else {
			result = null;
		}

		//
		return result;
	}

}
