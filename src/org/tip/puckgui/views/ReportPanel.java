package org.tip.puckgui.views;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.PuckManager;
import org.tip.puck.report.Report;
import org.tip.puck.report.ReportAttributes;
import org.tip.puck.report.ReportChart;
import org.tip.puck.report.ReportRawData;
import org.tip.puck.report.ReportTable;
import org.tip.puck.util.Chronometer;
import org.tip.puckgui.WindowGUI;
import org.tip.puckgui.models.ReportItemsModel;
import org.tip.puckgui.models.ReportTableModel;
import org.tip.puckgui.util.GenericFileFilter;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

public class ReportPanel extends JPanel {

	private static final long serialVersionUID = -792034151386088280L;

	private static final Logger logger = LoggerFactory.getLogger(ReportPanel.class);

	private File file;
	private JTable tableInputs;
	private Report report;
	private JLabel lblTitleValue;
	private JLabel lblOriginValue;
	private JLabel lblDateValue;
	private JLabel lblTargetValue;
	private JTextPane txtpnInputcomment;

	/**
	 * Create the panel.
	 */
	public ReportPanel(final WindowGUI parent, final Report report) {

		this.report = report;
		
		// Initialize specific variables.
		this.file = new File("report-" + new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime()) + ".txt");
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		JScrollPane scrollPaneContent = new JScrollPane();
		add(scrollPaneContent);

		JPanel panel_2 = new JPanel();
		scrollPaneContent.setViewportView(panel_2);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));

		JPanel panelPanelGeneral = new JPanel();
		panelPanelGeneral.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelPanelGeneral.setMaximumSize(new Dimension(32767, 175));
		panel_2.add(panelPanelGeneral);
		panelPanelGeneral.setLayout(new BoxLayout(panelPanelGeneral, BoxLayout.Y_AXIS));

		JPanel panelGeneral = new JPanel();
		panelGeneral.setPreferredSize(new Dimension(10, 150));
		panelPanelGeneral.add(panelGeneral);
		panelGeneral.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229), 1, true), "General", TitledBorder.LEADING, TitledBorder.TOP, null,
				new Color(51, 51, 51)));
		panelGeneral.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelGeneral.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblTitle = new JLabel("Title:");
		panelGeneral.add(lblTitle, "2, 2");

		this.lblTitleValue = new JLabel(this.report.title());
		panelGeneral.add(this.lblTitleValue, "4, 2");

		JLabel lblOrigin = new JLabel("Origin:");
		panelGeneral.add(lblOrigin, "2, 4");

		this.lblOriginValue = new JLabel(this.report.origin());
		panelGeneral.add(this.lblOriginValue, "4, 4");

		JLabel lblDate = new JLabel("Date:");
		panelGeneral.add(lblDate, "2, 6");

		this.lblDateValue = new JLabel((new SimpleDateFormat("dd/MM/yyyy HH':'mm")).format(this.report.date().getTime()));
		panelGeneral.add(this.lblDateValue, "4, 6");

		JLabel lblTarget = new JLabel("Target:");
		panelGeneral.add(lblTarget, "2, 8");

		this.lblTargetValue = new JLabel(this.report.target());
		panelGeneral.add(this.lblTargetValue, "4, 8");

		JLabel lblTimeSpent = new JLabel("Time spent:");
		panelGeneral.add(lblTimeSpent, "2, 10");

		JLabel lblTimeSpentValue = new JLabel(Chronometer.toHumanString(this.report.timeSpent()));
		panelGeneral.add(lblTimeSpentValue, "4, 10");

		Component verticalStrut = Box.createVerticalStrut(10);
		panelPanelGeneral.add(verticalStrut);

		JSeparator separator = new JSeparator();
		panelPanelGeneral.add(separator);

		Component verticalStrut_5 = Box.createVerticalStrut(10);
		panelPanelGeneral.add(verticalStrut_5);

		JPanel panelInputs = new JPanel();
		panelInputs.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_2.add(panelInputs);
		panelInputs.setBorder(new TitledBorder(null, "Inputs", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInputs.setLayout(new BoxLayout(panelInputs, BoxLayout.Y_AXIS));

		this.txtpnInputcomment = new JTextPane();
		panelInputs.add(this.txtpnInputcomment);
		this.txtpnInputcomment.setText(this.report.inputComment());
		this.txtpnInputcomment.setToolTipText("Input comment");
		this.txtpnInputcomment.setAlignmentX(Component.LEFT_ALIGNMENT);

		Component verticalStrut_1 = Box.createVerticalStrut(10);
		panelInputs.add(verticalStrut_1);

		JScrollPane scrollPaneTableInputs = new JScrollPane();
		scrollPaneTableInputs.setMaximumSize(new Dimension(600, 32767));
		scrollPaneTableInputs.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		scrollPaneTableInputs.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		panelInputs.add(scrollPaneTableInputs);
		scrollPaneTableInputs.setAlignmentX(Component.LEFT_ALIGNMENT);

		this.tableInputs = new JTable();
		this.tableInputs.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		scrollPaneTableInputs.setViewportView(this.tableInputs);
		this.tableInputs.setModel(new ReportItemsModel(null));
		((ReportItemsModel) this.tableInputs.getModel()).setSource(report.inputs());

		Component verticalStrut_2 = Box.createVerticalStrut(10);
		panelInputs.add(verticalStrut_2);

		JPanel panelOutputs = new JPanel();
		panelOutputs.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_2.add(panelOutputs);
		panelOutputs.setBorder(new TitledBorder(null, "Outputs", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelOutputs.setLayout(new BoxLayout(panelOutputs, BoxLayout.Y_AXIS));

		Component verticalStrut_6 = Box.createVerticalStrut(10);
		panelOutputs.add(verticalStrut_6);

		// ///////////////////////////////////////////
		if (report != null) {
			
			for (int outputIndex = 0; outputIndex < report.outputs().getCount(); outputIndex++) {
				Object output = report.outputs().get(outputIndex);
				if (output != null) {
					if (output instanceof ReportAttributes) {
						//
						JScrollPane scrollPaneTableOutputs = new JScrollPane();
						scrollPaneTableOutputs.setMaximumSize(new Dimension(600, 32767));
						scrollPaneTableOutputs.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
						panelOutputs.add(scrollPaneTableOutputs);
						scrollPaneTableOutputs.setAlignmentX(Component.LEFT_ALIGNMENT);

						JTable tableOutputs = new JTable();
						tableOutputs.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
						scrollPaneTableOutputs.setViewportView(tableOutputs);
						tableOutputs.setModel(new ReportItemsModel((ReportAttributes) output));

					} else if (output instanceof ReportChart) {
						// Calculate chart size.
						int screenWidth = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getWidth();
						int screenHeight = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight();

						// Build chart panel.
						JPanel chartPanel = ReportChartPanelMaker.createSimplePanel((ReportChart) output, screenWidth / 5, screenHeight / 5);
						chartPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

						// Add chart panel in report panel.
						if ((outputIndex != 0) && (report.outputs().get(outputIndex - 1) instanceof ReportChart)) {
							// Add a graph in an existing list.
							JPanel chartsPanel = (JPanel) panelOutputs.getComponent(panelOutputs.getComponentCount() - 1);
							chartsPanel.add(chartPanel);

						} else if ((outputIndex + 1 < report.outputs().getCount()) && (report.outputs().get(outputIndex + 1) instanceof ReportChart)) {
							JPanel graphsPanel = new JPanel();
							graphsPanel.setLayout(new BoxLayout(graphsPanel, BoxLayout.X_AXIS));
							graphsPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
							graphsPanel.add(chartPanel);

							panelOutputs.add(graphsPanel);

						} else {
							// Add a single graph panel.
							panelOutputs.add(chartPanel);
						}
					} else if (output instanceof ReportTable) {
						
						ReportTable table = (ReportTable) output;
						JTable tableOutputs = new JTable(new ReportTableModel(table));
						tableOutputs.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
						// scrollPaneTableOutputs.setViewportView(tableOutputs);

						// tableOutputs.setModel(new ReportTableModel(table));
						tableOutputs.setAlignmentX(Component.LEFT_ALIGNMENT);

						Dimension size = tableOutputs.getMaximumSize();
						tableOutputs.setMaximumSize(new Dimension(tableOutputs.getColumnCount() * 150, (int) size.getHeight()));
						
						if ((SystemUtils.IS_OS_WINDOWS) && ((int)size.getHeight() > 500)) {
							JTextPane txtpnOutputcomment = new JTextPane();
							panelOutputs.add(txtpnOutputcomment);
							txtpnOutputcomment.setText("\n…\nOutput truncated (only under Microsoft Windows). To get the full output, please click on the save button.");
						} else {
							panelOutputs.add(tableOutputs);
						}

					} else if (output instanceof ReportRawData) {
						ReportRawData rawData = (ReportRawData) output;
						JButton btnSaveRawData = new SaveReportRawDataButton(parent.getJFrame(), rawData);
						panelOutputs.add(btnSaveRawData);
					} else if (output instanceof Report) {
						// Do nothing.
					} else {
						JTextPane txtpnOutputcomment = new JTextPane();
						panelOutputs.add(txtpnOutputcomment);

						// Added tips for memory leak under Microsoft Windows
						// Swing when JTextPane contains huge text.
						String text = output.toString();
						if ((SystemUtils.IS_OS_WINDOWS) && (text.length() > 50000)) {
							txtpnOutputcomment.setText(text.substring(0, 50000)
									+ "\n…\nOutput truncated (only under Microsoft Windows). To get the full output, please click on the save button.");
						} else {
							txtpnOutputcomment.setText(text);
						}
						txtpnOutputcomment.setAlignmentX(Component.LEFT_ALIGNMENT);
						txtpnOutputcomment.setEditable(false);
					}
				}
			}
		}

		Component verticalStrut_3 = Box.createVerticalStrut(10);
		panelOutputs.add(verticalStrut_3);

		Component verticalGlue = Box.createVerticalGlue();
		panelOutputs.add(verticalGlue);

		Component verticalStrut_4 = Box.createVerticalStrut(5);
		add(verticalStrut_4);

		JPanel panel = new JPanel();
		add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				parent.closeCurrentTab();
			}
		});
		panel.add(btnClose);

		Component horizontalGlue = Box.createHorizontalGlue();
		panel.add(horizontalGlue);

		JButton btnSaveButton = new JButton("Save");
		btnSaveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Save.
				try {
					File targetFile = new File("report-" + report.target() + "-"
							+ (new SimpleDateFormat("yyyyMMddhhmmss").format(Calendar.getInstance().getTime())) + ".txt");

					boolean ended = false;
					while (!ended) {

						// Manage possibility to select a target file.
						JFileChooser chooser = new JFileChooser();
						chooser.setSelectedFile(targetFile);

						chooser.setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.saveAsFileChooser.text"));
						chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
						chooser.setAcceptAllFileFilterUsed(false);
						GenericFileFilter defaultFileFilter = new GenericFileFilter("Report Formats (*.txt, *.xls)", "txt", "xls");
						chooser.addChoosableFileFilter(defaultFileFilter);
						chooser.addChoosableFileFilter(new GenericFileFilter("Text (*.txt)", "txt"));
						chooser.addChoosableFileFilter(new GenericFileFilter("Microsoft Excel (*.xls)", "xls"));
						chooser.setFileFilter(defaultFileFilter);

						//
						if (chooser.showSaveDialog(parent.getJFrame()) == JFileChooser.APPROVE_OPTION) {
							System.out.println("getCurrentDirectory(): " + chooser.getCurrentDirectory());
							System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
							targetFile = chooser.getSelectedFile();
						} else {
							// Nothing to do.
							System.out.println("No Selection ");
							ended = true;
						}

						//
						if (!ended) {
							boolean doSave;
							if (targetFile.exists()) {
								// Manage confirmation dialog.
								String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.title");
								String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.text");

								int response = JOptionPane.showConfirmDialog(parent.getJFrame(), message, title, JOptionPane.YES_NO_CANCEL_OPTION);

								if (response == JOptionPane.YES_OPTION) {
									doSave = true;
									ended = true;
								} else if (response == JOptionPane.NO_OPTION) {
									doSave = false;
									ended = false;
								} else {
									doSave = false;
									ended = true;
								}
							} else {
								doSave = true;
								ended = true;
							}

							if (doSave) {
								PuckManager.saveReport(targetFile, report);
							}
						}
					}
				} catch (final PuckException exception) {
					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedFileFormat");
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.notAFile");
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedEncoding");
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.default");
					}

					//
					JOptionPane.showMessageDialog(parent.getJFrame(), message, title, JOptionPane.ERROR_MESSAGE);
				}
			}

		});
		panel.add(btnSaveButton);

		//
		if (!this.report.hasInput()) {
			panelInputs.setVisible(false);
		} else {
			if (StringUtils.isBlank(this.report.inputComment())) {
				this.txtpnInputcomment.setVisible(false);
			}
			if (this.report.inputs().size() == 0) {
				scrollPaneTableInputs.setVisible(false);
			}
		}

		//
		scrollPaneContent.getVerticalScrollBar().setValue(1);
		scrollPaneContent.getVerticalScrollBar().setUnitIncrement(20);
		scrollPaneContent.getHorizontalScrollBar().setUnitIncrement(20);
	}
}
