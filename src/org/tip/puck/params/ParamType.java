package org.tip.puck.params;

/**
 * @author Telmo Menezes
 *
 */
public enum ParamType {
    FIXED, VARIABLE
}
