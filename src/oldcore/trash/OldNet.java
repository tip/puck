package oldcore.trash;
import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import puck.Puck;


/**
 * This class defines the standard version of a genealogical corpus 
 * @author Klaus Hamberger
 * @since Version 1.0
 */
@SuppressWarnings("serial")
public class OldNet extends AbstractNet<Individual>{//TreeMap<Integer, Vertex> {






   
   /**
 * gets the label of the relation type for a given integer code k
 * <p> 0 marriage, 1 parent-child, 2 and above consanguinity of degree k-1 
 * @param k the relation type parameter
 * @return the label of the relation type
 * @see OldNet#density(ArrayList, int)
 */
/*private static String relationType (int k){
	String relTypes[] = (String[])Puck.getParam("Arc Types");
	if (k<2) return relTypes[k];
	k = k-1;
	return relTypes[2]+" "+stat(5)+" "+k;
}*/
   
   //Check need for getClusters
   //Generalize for vertex.getName? 
   //Move to util.Trafo
   /**
    * splits a string in two at the first blank
    * @param str the string to be split in two
    * @return the array of the two string parts
    * @see OldNet#getClusters(String)
    */
/*   private static String[] splitOnce (String str){
	   int i = str.indexOf(" ");
	   if (i==-1) return new String[]{str,null};
	   return new String[]{str.substring(0,i),str.substring(i+1)};
   }*/
   




    

	

   
  
   

   


   
   /**
    * gets the vertex having the same Id number as the ith vertex of a ring
    * @param r the ring
    * @param i the index of the vertex in the ring
    * @return the corresponding vertex of the Net
    * @see pas.Net#Net(Ring)
    */
/*   private Individual get (Ring r, int i){
		return get(r.getID(i));
   }*/
   
   /**
    * gets the vertex having the same Id number as a given vertex (the vertice's double) 
    * @param v the model vertex
    * @return the corresponding vertex in the Net
    */
/*   public Individual get (Individual v){
	   if (v==null) return null;
	   return get(v.getID());
   }*/
   
   /**
    * gets the matrix of marriage alliances between partition clusters (rows = wife giver clusters, columns = wife taker clusters)
    * @param cmd the partition command
    * @param split true if the matrix shall be split into generational submatrices 
    * @return the alliance matrix
    * @since modif 10-08-10, 10-08-15
    */
/*   public Matrix getAllianceMatrix(String cmd, boolean split){
	   if (split) return new SplitMatrix(this,cmd);
	   return new Matrix(this, cmd);
   }*/
   

   //not functional...
   //rewrite as a static function, parametrize
   //generalize for other net types
   /**
    * translates arc type indices into arc labels, depending on the net type
    * @param i the arc type index
    * @return the arc label
    * @see 
    */
/*   private String getArcLabel(int i) {
      if (getType() == 0) {
    	  if (i == 1) return "H.F";
    	  if (i == 2) return "(F)F";
    	  if (i == 3) return "(F)H";
    	  if (i == 4) return "(H)F";
    	  if (i == 5) return "(H)H";
      }
      return ""; // erg�nzen...
   }*/
   

   
   /**
    * effectuates a census of the Net according to the options specified in the census screen
    * @return the RingGroupMap containing the census
    * @see gui.screens.CensusScreen#report()
    * @see gui.screens.StatsScreen#show(String[])
    */
/*   public RingGroupMap getCensus () {
	   begin();
	   int t = 3;
	   if (RingGroupMap.optionMatrimonialCensus()) t=7;
	   RingGroupMap rn = new RingGroupMap (this,t);
	   if (rn.getClusterLabel()==null) {
		   end("Census finished");
		   rn.setTime(getTime());
		   return rn;
	   }
	   rn = new RingGroupMap(rn,2);
	   end("Census finished");
	   rn.setTime(getTime());
	   return rn;
   }*/
   

   

   

   


   


   
   /**
    * produces a virtual vertex for a relative not represented in the original corpus and adds the kinship relation to the protocol of missing vertices (if the corresponding option has been chosen)
    * @param v the ego vertex
    * @param a the Id number of alter
	* @param i the gendered kin tie index (ch -1, f 0, m 1, sp 2)
    * @return the virtual alter vertex
    * @see maps.Net@setLink(int,int,int)
    */
/*   private Individual getLinked(Individual v, int a, int i) {
	   if (a==0) return null;
	   if (get(a) == null) {
		   setVertex(a,"#",v.alterGender(i));
		   if (!Puck.startOption(7)) return get(a); 
		   openProtocol();
		   getProtocol().add(get(a).getName(-1)+"\t"+get(a).getKinTerm(i)+" "+headline(6)+" "+v.getName(1));
	   }
	   return get(a);
   }*/






	/**
	 * gets the theoretical statistics of parallel and cross relinkings
	 * @param cmd the partition command
	 * @param split true if the matrix shall be split into generational submatrices 
	 * @return the string list for the report
	 * @since 10-07-04, modif. 10-08-07
	 */
/*	public List<String> reportRelinkings (String cmd, int runs){
//		Partition<Node> p = getPartition(cmd);
	//	((NodePartition)p).essai();
		String [] h = Puck.getHeadlines(18);
		List<String> stats = new ArrayList<String>();
		stats.add(h[0]+" "+cmd);
		String s = "\tsim ("+runs+")\tperm("+runs+")\tth�or.\t";
		stats.add(h[1]+s+h[2]+s+h[3]+s);
		stats.add(getName()+" ncg.\t"+getAllianceMatrix(cmd,false).reportRelinkingStatistics(runs));
		stats.add(getName()+" gc.\t"+getAllianceMatrix(cmd,true).reportRelinkingStatistics(runs));
		return stats;
	}*/





   //Simplify?
   /**
    * renumbers the vertices of the Net by assigning them their current indices as ID numbers 
    * <p> used to render ID numbers continuous, as required for exportation in Pajek format
    * @see io.read.PajReader#PajReader(OldNet, File, int)
    */
/*   public void invRenumber (){
	   ArrayList<Individual> list = new ArrayList<Individual>(values());
	   empty();
	   for (Individual v : list){
		   v.setNumber(v.getCurrent());
		   put(v);
	   }
   }*/



   
   /**
    * puts a new vertex without name, gender and properties at the end of the network and returns its ID number
    * @return the ID number of the new vertex
    * @see gui.screens.DataScreen#turn(int)
    */
/*   public int newKey() {
	   if (this instanceof PartNet) return -1;
	   setVertex(lastKey()+1,"",2);
	   return lastKey();
   }*/
   

   /**
    * gets the ID numbner of the next vertex in the Net
    * @param i the ID number of the current vertex
    * @return the ID number of the next vertex
    * @see gui.screens.DataScreen#turn(int)
    * @see gui.components.TipTableModel#setValueAt(Object, int, int)
    */
/*   public int nextKey (int i) {
      if (i==lastKey()) return i;
      while (get(i+1)==null){
    	  i++;
      }
      return i+1;
   }*/
   
   /**
    * gets the vertex with the ID number following that of the current vertex, and adds a new vertex (without name, gender and properties) with this ID number, if there is no such vertex 
    * @param i the ID number of the current vertex
    * @return the vertex (existing or created) with the following ID number
    * @see gui.screens.DataScreen#turn(int)
    */
/*   public int nextOrNewKey (int i){
	   if (get(i+1)==null) setVertex(i+1,"",2);
	   return i+1;
   }*/
   

   




   

   
/*   public void update (Net n) {
      update (n,true);
   }*/


   
/*   public int nrTies (CountMap<int[]> ties) {
		  if (ties==null) return -1;
		  return ties.sum();
	   }*/

   //generalize for higher class?
   /**
    * gets the key preceding a given key in the map
    * @param i the given key
    * @return the key preceding the given key
    * @see OldNet#getPrevious(Individual)
    * @see gui.screens.DataScreen#turn(int)
    */
/*   public int previousKey (int i) {
      if (i==firstKey()) return i;
      while (get(i-1)==null){
    	  i--;
      }
      return i-1;
   }*/
  
   /*
   public NodeMap getOrigin (){
	   if (this instanceof PartNet) return ((PartNet)this).getOrigin(); 
	   return this;
   }*/
   
/*   public Net getHome (int i) {
      if (get(i) != null) return this;
      return origin.getHome(i);
   }*/

   /**
    * puts a new vertex of unspecified gender and name with a given ID number
    * @param i the ID number of the new vertex
    * @see OldNet#Net(String)
    * @see gui.screens.AbstractIOScreen#init(AbstractScreen,OldNet,String,Color)
    * @see io.read.AbstractReader#put(int)
    */
/*   public void put (int i){
	   put(new Individual(i,"#",2));
   }*/
   


   
	/**
	 * renumbers the elements according to their position in another Net
	 * @param net the model net
	 * @see maps.groupmaps.RingGroupMap#setSuperNet()
	 */
/*	public void renumber (NodeMap<Individual> net){
		int i=1;
		for (Individual v: net.values()){
			v.setCurrent(i);
			if (get(v)!=null) get(v).setCurrent(i);
			i++;
		}
	}*/
	   




   



   

   
   // Simplify! getLinked for e!
   /**
    * sets a (reciprocal) kinship link of between two vertices of the Net
	* @param e the ID number of ego
	* @param a the ID number of alter
	* @param i the gendered kin tie index (ch -1, f 0, m 1, sp 2)
	* @see io.read.AbstractReader#setLink(int,int,int)
	* @see io.read.TipReader#updateLink(String[])
    */
/*   public void setLink (int e, int a, int i) {
      try {
    	  get(e).setLink(getLinked(get(e),a,i),i,true);
      	} catch (NullPointerException npe) {}                          
   }*/


   
   /**
    * puts a new vertex
    * @param nr the ID number of the vertex
    * @param lb the label of the vertex
    * @param gd the gender of the vertex
    * @see OldNet#getLinked(Individual, int, int)
    * @see OldNet#newKey()
    * @see OldNet#nextOrNewKey(int)
    */
/*   private void setVertex (int nr, String lb, int gd) {
      Individual v = new Individual(nr,lb,gd);
      put(v.getID(),v);
      v.setCurrent(nodeCount());
   }*/
   

	  
   /**
    * gets the signature of the Net
    * @return the signature of the Net
    * @see gui.screens.AbstractIOScreen#IOMask(AbstractIOScreen)
    * @see gui.screens.AbstractIOScreen#IOMask(AbstractIOScreen, OldNet, String, Color)
    */
/*   public String signature (){
	   return getName()+" ("+nodeCount()+")";
   }*/

   /**
    * gets the signature of the ith page of the Net
    * @param i the page number
    * @return the signature of the ith page of the Net
    * @see gui.screens.DataScreen#update()
    */
/*   public String signature (int i){
	   return getName()+" ("+getPage(i)+"/"+nodeCount()+")  "+get(i).signature(3);
   }*/


   



   



//Check attribute types
//Render static? (reduce getNet() to parameter net)
/**
 * gets the list of attribute labels
 * @param t the attribute type (0 properties, 1 events, 2 relations, 3 notes)
 * @param augment true if also default attributes are to be considered (over and above attributes loaded in the Net)
 * @param unfold true if sublabels are to be considered
 * @return the list of attribute labels
 * @see io.write.AbstractWriter#labels(NodeMap,int)
 * @see gui.components.Table#getList(int,int,int)
 * @see gui.components.ComboBox#setModel()
 * @see gui.components.LabelChooser#setBox(int)
 */
/*public TipList getLabels (int t, boolean augment, boolean unfold){
	TipList list = new TipList();
	for (Individual v : this.values()) {
		for (Attribute a : v.getAttributes(t)) {
			list.append(a.getLabel());
		}
	}
	if (!augment) {
		if (!unfold) return list.sorted();  
    	return list.unfold().sorted();
    }
	
	for (int i=0;i<4;i++){
		if ((i!=t) && (t!=3)) continue;
		for (String str : (String[])Puck.getParam("Cluster Labels",i)) {
			list.append(str);
		}
	}
	if (!unfold) return list.sorted();  
	return list.unfold().sorted();
}*/





	
}
