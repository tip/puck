/**
 * Copyright 2012 Christian P. MOMON (christian.momon@devinsy.fr).
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Adaptations by TIP.
 * 
 */
package org.tip.puckgui;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.tip.puck.util.ToolBox;

/**
 * 
 * @author Christian P. MOMON
 * @author TIP
 * 
 */
public class RecentFiles implements Iterable<File> {

	private static final int MAX_SIZE = 15;
	private List<File> datas;

	/**
	 * 
	 */
	public RecentFiles() {
		this.datas = new ArrayList<File>(MAX_SIZE + 1);
	}

	/**
	 * 
	 * @return
	 */
	public File getMoreRecent() {
		File result;

		if (this.datas.isEmpty()) {
			result = null;
		} else {
			result = this.datas.get(0);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @return
	 */
	public int indexOf(final File file) {
		int result;

		boolean ended = false;
		result = -1;
		int count = 0;
		while (!ended) {
			if (count < this.datas.size()) {
				if (this.datas.get(count).getAbsolutePath().equals(file.getAbsolutePath())) {
					ended = true;
					result = count;
				} else {
					count += 1;
				}
			} else {
				ended = true;
				result = -1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		boolean result;

		result = this.datas.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty() {
		boolean result;

		result = !this.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public Iterator<File> iterator() {
		Iterator<File> result;

		result = this.datas.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size() {
		int result;

		result = this.datas.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public File[] toArray() {
		File[] result;

		result = new File[this.datas.size()];
		this.datas.toArray(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public File[] toReverseArray() {
		File[] result;

		result = toArray();
		ArrayUtils.reverse(result);

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 */
	public void update(final File file) {

		if (file != null) {
			// Remove file if it exists.
			int index = indexOf(file);
			if (index != -1) {
				this.datas.remove(index);
			}

			// Add the file on top.
			this.datas.add(0, file);

			// Remove overflow.
			if (this.datas.size() > MAX_SIZE) {
				this.datas.remove(this.datas.size() - 1);
			}
		}
	}

	/**
	 * 
	 * @param file
	 */
	public void updateFile(final File file) {
		if (ToolBox.getExtension(file) != null) {
			update(file);
		}
	}

	/**
	 * 
	 * @param file
	 */
	public void updateFolder(final File folder) {

		if ((folder != null) && (folder.isDirectory())) {
			update(folder);
		}
	}
}
