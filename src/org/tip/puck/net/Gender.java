package org.tip.puck.net;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Note: to avoid bad comparison, a hard parameter is setted for each enum
 * entry.
 * 
 * @author TIP
 */
public enum Gender {

	MALE(0),
	FEMALE(1),
	UNKNOWN(2);

	private static final char MALE_CHAR = 0x2642;
	private static final char FEMALE_CHAR = 0x2640;
	private static final char NEUTRAL_CHAR = 0x26B2;
	private int code;
	
	public enum GenderCode {
		
		HF,
		MF,
		ASCENDANT,
		DESCENDANT,
		SPOUSE,
	}

	/**
	 * 
	 * @param code
	 * @param message
	 */
	private Gender(final int code) {
		this.code = code;
	}

	/**
	 * 
	 * @param gender
	 * @return
	 */
	public boolean canMarry(final Gender gender) {
		boolean result;

		result = (!(this == gender) || (this == UNKNOWN));

		//
		return result;
	}
	
	public static Gender valueOfSymbol(final char symbol){
		Gender result;
		
		result = UNKNOWN;
		
		switch (symbol) {
		case FEMALE_CHAR:
			result = FEMALE;
		break;
		case MALE_CHAR:
			result = MALE;
		break;
		case NEUTRAL_CHAR:
			result = UNKNOWN;
		break;
		}
		
		//
		return result;

	}

	/**
	 * 
	 * @return
	 */
	public Gender invert() {
		Gender result;

		if (this == MALE) {
			result = FEMALE;
		} else if (this == FEMALE) {
			result = MALE;
		} else {
			result = this;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isFemale() {
		boolean result;

		result = this == FEMALE;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isMale() {
		boolean result;

		result = this == MALE;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isUnknown() {
		boolean result;

		result = this == UNKNOWN;

		//
		return result;
	}

	/**
	 * 
	 * @param gender
	 * @return
	 */
	public boolean matchs(final Gender gender) {
		boolean result;

		if ((gender == null) || (this == UNKNOWN) || (gender == UNKNOWN) || (this == gender)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param gender
	 * @return
	 */
	public boolean specifies(final Gender gender) {
		boolean result;

		if ((gender == UNKNOWN) || (this == gender)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public char toAscendantChar() {
		char result;

		switch (this) {
			case FEMALE:
				result = 'M';
			break;
			case MALE:
				result = 'F';
			break;
			default:
				result = 'P';
		}
		//
		return result;
	}
	
	public char toChar(GenderCode code){
		char result;
		
		switch (code){
		case HF:
			result = toChar();
			break;
		case MF:
			result = toGedChar();
			break;
		case ASCENDANT:
			result = toAscendantChar();
			break;
		case DESCENDANT:
			result = toDescendantChar();
			break;
		case SPOUSE:
			result = toSpouseChar();
			break;
		default:
			result = toChar();
		}
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public char toChar() {
		char result;

		switch (this) {
			case FEMALE:
				result = 'F';
			break;
			case MALE:
				result = 'H';
			break;
			default:
				result = 'X';
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public char toGedChar() {
		char result;

		switch (this) {
			case FEMALE:
				result = 'F';
			break;
			case MALE:
				result = 'M';
			break;
			default:
				result = 'X';
		}

		//
		return result;
	}


	/**
	 * 
	 * @return
	 */
	public char toDescendantChar() {
		char result;

		switch (this) {
			case FEMALE:
				result = 'D';
			break;
			case MALE:
				result = 'S';
			break;
			default:
				result = 'C';
		}
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public char toSpouseChar() {
		char result;

		switch (this) {
			case FEMALE:
				result = 'W';
			break;
			case MALE:
				result = 'H';
			break;
			default:
				result = 'X';
		}
		//
		return result;
	}

	/**
	 * TIP format compatibility.
	 * 
	 * @return
	 */
	public int toInt() {
		int result;

		result = this.code;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public String toShapeString() {
		String result;
		switch (this) {
			case FEMALE:
				result = "ellipse";
			break;
			case MALE:
				result = "triangle";
			break;
			default:
				result = "diamond";
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public char toSymbol() {
		char result;

		switch (this) {
			case FEMALE:
				result = FEMALE_CHAR;
			break;
			case MALE:
				result = MALE_CHAR;
			break;
			default:
				result = NEUTRAL_CHAR;
		}

		//
		return result;

	}
	
	public String toSpeakerString(){
		String result;
		switch (this) {
			case FEMALE:
				result = "f.s.";
			break;
			case MALE:
				result = "m.s.";
			break;
			default:
				result = "";
		}

		//
		return result;
		
	}

	/**
	 * 
	 * @param alpha
	 * @param bravo
	 * @return
	 */
	public static int compare(final Gender alpha, final Gender bravo) {
		int result;

		if ((alpha == null) && (bravo == null)) {
			//
			result = 0;

		} else if (alpha == null) {
			//
			result = -1;

		} else if (bravo == null) {
			//
			result = +1;

		} else {
			//
			result = alpha.compareTo(bravo);
		}

		//
		return result;
	}

	/**
	 * This method build a list of the gender values sorted for display in
	 * charts.
	 * 
	 * @return a list of the gender values.
	 */
	public static List<Gender> getChartValueList() {
		List<Gender> result;

		result = Arrays.asList(Gender.getChartValues());

		//
		return result;
	}

	/**
	 * This method build an array of the gender values sorted for display in
	 * charts.
	 * 
	 * @return an array of the gender values.
	 */
	public static Gender[] getChartValues() {
		Gender[] result;

		result = new Gender[] { Gender.FEMALE, Gender.MALE, Gender.UNKNOWN };

		//
		return result;
	}
	
	public static char inverseGenderSymbol (final char c){
		char result;
		
		result = ' ';
		
		switch (c){
		case FEMALE_CHAR:
			result = MALE_CHAR;
			break;
		case MALE_CHAR:
			result = FEMALE_CHAR;
			break;
		case NEUTRAL_CHAR:
			result = NEUTRAL_CHAR;
			break;
		}
		//
		return result;
	}

	/**
	 * 
	 * @param c
	 * @return
	 */
	public static boolean isGenderSymbol(final char c) {
		boolean result;

		result = (c == FEMALE_CHAR || c == MALE_CHAR || c == NEUTRAL_CHAR);
		//
		return result;
	}
	
	public static boolean matchesChar(final char letter, final char symbol){
		boolean result;
		
		result = ((symbol == Gender.MALE_CHAR && (letter == 'F' || letter == 'S' || letter == 'H' || letter == 'B'))
			  || (symbol == Gender.FEMALE_CHAR && (letter == 'M' || letter == 'D' || letter == 'W' || letter == 'Z')));
	
			 //
			  return result;
	}
	
	public static boolean sameSexChar(final char firstLetter, final char secondLetter){
		boolean result;
		
		result = ((secondLetter == 'H' && (firstLetter == 'F' || firstLetter == 'S' || firstLetter == 'H' || firstLetter == 'B'))
			  || (secondLetter == 'W' && (firstLetter == 'M' || firstLetter == 'D' || firstLetter == 'W' || firstLetter == 'Z')));
	
			 //
			  return result;
	}



	/**
	 * 
	 * TIP format compatible (0->M, 1->F). - Deactivated for in some txt
	 * datasets 0 = unknown.
	 * 
	 * 
	 * @param value
	 * 
	 * @return
	 */
	public static Gender valueOf(final char value) {
		Gender result;

		switch (value) {
			case '0':
			case 'm':
			case 'M':
			case 'h':
			case 'H':
				result = MALE;
			break;

			case '1':
			case 'f':
			case 'F':
			case 'w':
			case 'W':
				result = FEMALE;
			break;
			default:
				result = UNKNOWN;
		}

		//
		return result;
	}

	/**
	 * K
	 * 
	 * @param value
	 * 
	 * @return
	 */
	public static Gender valueOf(final int value) {
		Gender result;

		switch (value) {
			case 0:
				result = Gender.MALE;
			break;
			case 1:
				result = Gender.FEMALE;
			break;
			default:
				result = Gender.UNKNOWN;
		}

		//
		return result;
	}
	
	public static List<Gender> valuesNotUnknown(){
		List<Gender> result;
		
		result = new ArrayList<Gender>();
		
		for (Gender gender : values()){
			if (!gender.isUnknown()){
				result.add(gender);
			}
		}
		//
		return result;
	}
	
	public String toStringNoUnknown(){
		String result;
		
		if (isUnknown()){
			result = "";
		} else {
			result = toString();
		}
		//
		return result;
	}
	
	public int compareToUnknownFirst(Gender other){
		int result;
		
		if (isUnknown() && !other.isUnknown()){
			result = -1;
		} else if (!isUnknown() && other.isUnknown()){
			result = +1;
		} else {
			result = compareTo(other);
		}
		//
		return result;
	}
	
	public static List<Gender> values(boolean unknownOnly){
		List<Gender> result;
		
		if (unknownOnly){
			result = new ArrayList<Gender>();
			result.add(Gender.UNKNOWN);
		} else {
			result = valuesNotUnknown();
		}
		//
		return result;
	}

}
