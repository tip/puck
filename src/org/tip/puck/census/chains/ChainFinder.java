package org.tip.puck.census.chains;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import oldcore.trash.OldIndividual;

import org.tip.puck.census.workers.ChainValuator;
import org.tip.puck.census.workers.SiblingMode;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.KinType;
import org.tip.puck.net.IndividualComparator.Sorting;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Partition;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.util.Value;

import fr.devinsy.util.StringList;

public class ChainFinder {
	

	
	/**
	 * gets the distance of the shortest consanguineous chain between the vertex and an alter vertex, and sets it as vertex color
	 * @param v the alter vertex
	 * @param g the filter criterion for ascendant lines (0 agnatic, 1 uterine, 2 cognatic)
	 * @return the distance of the shortest consanguineous chain between the vertex and an alter vertex
	 */
	private static int getDistance (Individual ego, Individual alter, FiliationType g){
		int h = getLinearDistance(ego, alter,g);
		if (h>-1) return h;
		Map<Individual, Integer> distances = new HashMap<Individual, Integer>();
		getDistance(ego, alter, 0, g, distances);
		return distances.get(alter);
	}
	/**
	 * gets the distance of the consanguineous chain between the vertex (or its spouse) and a second vertex
	 * @param v the vertex to be checked 
	 * @param g the filter criterion for ascendant lines (0 agnatic, 1 uterine, 2 cognatic) 
	 * @param cons true if the consanguinous chain starts from ego, false if it starts from a spouse of ego
	 * @return the distance of the chain between v and ego (or its spouse), or -1 if there is no such chain
	 * @see OldIndividual#getLinks(OldIndividual, boolean)
	 */
	private static int getDistance (Individual ego, Individual alter, FiliationType g, boolean cons){
		if (cons) return getDistance(ego, alter, g);
		return getAffinalDistance(ego, alter, g);
	}
	
	/**
	 * gets the distance of the shortest consanguineous chain between the vertex and an alter vertex
	 * @param d the length of the kinship chain already traversed
	 * @param v the alter vertex
	 * @param g the filter criterion for ascendant lines (0 agnatic, 1 uterine, 2 cognatic)
	 * @see OldIndividual#getDistance(OldIndividual, int) 
	 */
	private static void getDistance (Individual ego, Individual alter, int d, FiliationType g, Map<Individual, Integer> distances) {
		int h = getLinearDistance(ego,alter,g);
		if (h>-1) {
			if (distances.get(alter)<h+d) return;
			distances.put(alter, h+d);
			return;
		}
		for (Individual parent : ego.getParents()){
			if (parent.hasLinkingGender(g)){
				getDistance (parent, alter, d+1, g, distances);
			}
		}
		
/*		for (int i=0;i<2;i++){
			if (Mat.noMatch(i,g)) continue;
			Individual parent = ego.getParent(i);
			if (parent!=null) getDistance (parent, alter, d+1, g, distances);
		}*/
	}
	
	/**
	 * gets the linear distance to an alter vertex 
	 * @param v the alter vertex
	 * @param g the filter criterion for ascendant lines (0 agnatic, 1 uterine, 2 cognatic)
	 * @see OldIndividual#getDistance(OldIndividual, int)
	 */
	private static int getLinearDistance (Individual ego, Individual alter, FiliationType g){
		int result;
		
		Map<Individual,Integer> distances = new HashMap<Individual,Integer>();
		getLinearDistance(ego, alter, 0,g, distances);
		Integer d = distances.get(alter);
		if (d==null) {
			result = -1;
		} else {
			result = d;
		}
		//
		return result;
	}
	
	/**
	 * gets the linear distance to an alter vertex 
	 * @param d the length of the kinship chain already traversed
	 * @param alter the alter vertex
	 * @param g the filter criterion for ascendant lines (0 agnatic, 1 uterine, 2 cognatic)
	 * @see OldIndividual#getLinearDistance(OldIndividual, int)
	 */
	private static void getLinearDistance (Individual ego, Individual alter, int d, FiliationType g, Map<Individual,Integer> distances) {
		if (distances.get(alter)==d) return;
		if (ego.equals(alter)) {
			distances.put(alter, d);
			return;
		}
		for (Individual parent: ego.getParents()){
			if (parent.hasLinkingGender(g)){
				getLinearDistance (parent, alter, d+1, g, distances);
			}
		}
/*		for (int i=0;i<2;i++){
			if (Mat.noMatch(i,g)) continue;
			Individual parent = getParent(i);
			if (parent!=null) {
				getLinearDistance (parent, alter, d+1, g, distances);
			}
		}*/
	}
	
	/**
	 * gets the lowest odd integer of an array
	 * @param a the integer array
	 * @return the lowest odd number
	 * @see OldIndividual#getLink(OldIndividual)
	 */
	private static int minOdd (int[] a){
		int min = a[1];
		for (int i=1;i<a.length;i++){
			if (i%2==0) continue;
			int b = a[i];
			if (b>0 && (min==0 || b<min)) min=b;
		}
		return min;
	}
	
	/**
	    * finds and lists all chains (within given bounds) for a list of vertices read from a text file
	    * @param file the import file
	    * @param k the maximal german degree
	    * @param w the maximal number of marriages
	    * @throws IOException
	    * @since 11-03-26
	    */
/*	   public static List<String> reportChains (File file, Net net, int k, int w, String chainClassification) throws IOException{
	   	BufferedReader reader = new BufferedReader(new FileReader(file));
	   	String line;
	   	ArrayList<String> list = new ArrayList<String>();
	   	try {
	   		line= reader.readLine();
	   		while ( line != null ) {
	   			if (line.length()>0) {
	   				String[] s = line.split("\t");
	   				Individual a = net.get(new Integer(s[0]));
	   				Individual b = net.get(new Integer(s[1]));
	   				String head = s[0]+"\t"+a.getName()+"\t"+s[1]+"\t"+b.getName();
	   				boolean nolink=true;
//	   				ChainFinder finder = new ChainFinder(a,b);
	   				for (Chain chain : findChains(a, b, k, w, chainClassification)){
	   					list.add(head+"\t"+chain.signature(Notation.NUMBERS)+"\t"+chain.signature(Notation.POSITIONAL));
	   					nolink=false;
	   				}
	   				if (nolink) list.add(head+"\tno kinship chain found");
	   			}
	        	line = reader.readLine();
	   		}
	   		} finally {
	   			if (reader!=null) reader.close();
	   		}
	   		return list;
	   	}*/
	      
	// should be defined as a Report 
	   /**
	    * finds and lists the shortest chains for a list of vertices read from a text file
	    * @param file the import file
	    * @throws IOException
	    * @since 11-04-21
	    */
/*	   public static List<String> reportShortestChains (File file, Net net, int k, int w) throws IOException{
	   	BufferedReader reader = new BufferedReader(new FileReader(file));
	   	String line;
	   	ArrayList<String> list = new ArrayList<String>();
	   	try {
	   		line= reader.readLine();
	   		while ( line != null ) {
	   			if (line.length()>0) {
	   				String[] s = line.split("\t");
	   				Individual a = net.get(new Integer(s[0]));
	   				Individual b = net.get(new Integer(s[1]));
	   				String head = s[0]+"\t"+a.getName()+"\t"+s[1]+"\t"+b.getName();
	   				Chain r = findShortestChain(a, b, k,w);
	   				if (r!=null) {
	   					list.add(head+"\t"+r.signature());
	   				} else {
	   					list.add(head+"\tno kinship chain found");
	   				}
	   			}
	        	line = reader.readLine();
	   		}
	   		} finally {
	   			if (reader!=null) reader.close();
	   		}
	   		return list;
	   	}*/

	
	/**
	 * checks the presence of a direct or indirect conjugal link with another vertex
	 * @param v the vertex to be compared
	 * @return 1 if v is a spouse, 2 if v is a co-spouse, 0 else
	 * @see OldIndividual#relationStatistics(List)
	 */
	private static int conjugalLink (Individual ego, Individual alter){
		if (ego.isSingle()) return 0;
		for (Individual w : ego.spouses()){
			if (w.equals(alter)) return 1; 
			if (w.spouses()==null) return 0;
			if (w.spouses().contains(alter)) return 2;
		}
		return 0;
	}
	
	/**
	 * expands a chain within given bounds and adds a copy to the chain list if the chain reaches a given target
	 * @param target the target vertex
	 * @param maxDepth the maximal depth of the chains
	 * @param maxOrder the maximal width (order) of the chains
	 * @return the list of kinship chains between ego and alter
	 */
	public static void expand (Partition<Chain> chains, Chain chain, Individual target, int maxDepth, int maxOrder, Set<Individual> visited){

		Individual last = chain.getLast();
		if (last.equals(target)) {
			Chain circuit = chain.clone();
			circuit.getSubchains();
			circuit = circuit.neutralize(SiblingMode.FULL);
			chains.put(circuit,ChainValuator.get(circuit,chains.getLabel())); 
		}
		
		visited.add(last);
		for (int t=-1;t<2;t++){
			if ((t!=0 || chain.dim()<=maxOrder) && (t==0 || chain.depth()<=maxDepth) && !(chain.lastDir()==-1 && t==1)) {
				Individuals kin = last.getKin(t);
				if (kin!=null){
					for (Individual v: kin){
						if (v!=null && !visited.contains(v)){
							chain.add(v,t);
							if (chain.dim()<=maxOrder && chain.depth() <= maxDepth){
								expand(chains, chain,target,maxDepth,maxOrder, visited); 
							}
							chain.removeLast();
						}
					}
				}
			}
		}
		visited.remove(last);
	}
	
	/**
	 * expands a chain within given bounds and adds a copy to the chain list if the chain reaches a given target
	 * @param target the target vertex
	 * @param maxDepth the maximal depth of the chains
	 * @param maxOrder the maximal width (order) of the chains
	 * @return the list of kinship chains between ego and alter
	 */
	public static void expandShortest (Partition<Chain> chains, Chain chain, Individual target, int maxDepth, int maxOrder, Set<Individual> visited){

		Chain referenceChain = null;
		for (Chain oldChain : chains.getItems()){
			if (oldChain.getLast().equals(target)){
				referenceChain = oldChain;
			}
		}
		
		if (referenceChain==null || referenceChain.dim()>chain.dim() || (referenceChain.dim()==chain.dim() && referenceChain.depth()>=chain.depth())){
			Individual last = chain.getLast();
			if (last.equals(target)) {
				Chain circuit = chain.clone().neutralize(SiblingMode.ALL);
				circuit.getSubchains();
				if (referenceChain !=null){
					chains.removeItem(referenceChain);
				}
				chains.put(circuit,ChainValuator.get(circuit,chains.getLabel()));
			}
			
			visited.add(last);
			for (int t=-1;t<2;t++){
				if ((t!=0 || chain.dim()<=maxOrder) && (t==0 || chain.depth()<=maxDepth) && !(chain.lastDir()==-1 && t==1)) {
					Individuals kin = last.getKin(t);
					if (kin!=null){
						for (Individual v: kin){
							if (v!=null && !visited.contains(v)){
								chain.add(v,t);
								if (chain.dim()<=maxOrder && chain.depth() <= maxDepth){
									expandShortest(chains, chain,target,maxDepth,maxOrder, visited); 
								}
								chain.removeLast();
							}
						}
					}
				}
			}
			visited.remove(last);
		}
	}
	
/*	public static List<String> findReciprocalTerms (Segmentation segmentation, String term){
		List<String> result;
		
		Set<String> set = new HashSet<String>();
		
		Individual maleEgo = null;
		Individual femaleEgo = null;
		
		for (Individual individual : segmentation.getCurrentIndividuals()){
			if (individual.getName().equals("[Ego]")){
				if (individual.isMale()){
					maleEgo = individual;
				} else if (individual.isFemale()){
					femaleEgo = individual;
				}
			}
		}
		
		Partition<Chain> chains = findChainsForTerms(segmentation,term,"SIMPLE");
		
		for (Chain chain : chains.getItems()){
			Chain inverseChain = chain.reflect();
			Individual ego = null;
			if (inverseChain.getFirst().isMale()){
				ego = maleEgo;
			} else if(inverseChain.getFirst().isFemale()){
				ego = femaleEgo;
			}
			Partition<Chain> relatives = ChainFinder.getKin(ego, inverseChain.signature(Notation.POSITIONAL));
			for (Chain relative : relatives.getItems()){
				set.add(relative.getLast().getName());
			}
			
		}
		
		result = new ArrayList<String>(set);
		Collections.sort(result);
		
		//
		return result;
	}

	
	public static Partition<Chain> findChainsForTerms (Segmentation segmentation, String kinTerm, String chainClassification){
		Partition<Chain> result	= new Partition<Chain>();
		result.setLabel(chainClassification);
		
		Individual maleEgo = null;
		Individual femaleEgo = null;
		List<Individual> alters = new ArrayList<Individual>();
		
		for (Individual individual : segmentation.getCurrentIndividuals()){
			if (individual.getName().equals("[Ego]")){
				if (individual.isMale()){
					maleEgo = individual;
				} else if (individual.isFemale()){
					femaleEgo = individual;
				}
			} else if (individual.getName().equals(kinTerm) && !individual.getName().equals("[Ego]")) {
				alters.add(individual);
			}
		}
		
		for (Individual alter : alters){
			Set<Individual> visited = new HashSet<Individual>();
			visited.add(maleEgo);
			expandShortest(result, new Chain(maleEgo),alter,1000,100,visited);
			visited = new HashSet<Individual>();
			visited.add(femaleEgo);
			expandShortest(result, new Chain(femaleEgo),alter,1000,100,visited);
		}
		
		//
		return result;
	}*/	
	
	public static Partition<Chain> findChains (Individual ego, Individual alter, int maxDepth, int maxOrder, String chainClassification){
		Partition<Chain> result = new Partition<Chain>();
		result.setLabel(chainClassification);
		Set<Individual> visited = new HashSet<Individual>();
		visited.add(ego);
		expand(result, new Chain(ego),alter,maxDepth,maxOrder,visited);
		return result;
	}	
	
	/**
	 * finds the shortest chain between two vertices, using a bfs algorithm 
	 * @param aim the target vertex
	 * @param maxDegree the maximal degree
	 * @param maxOrder the maximal order
	 * @return the shortest chain
	 */
	public static Chain findShortestChain (Individual ego, Individual alter, int[] maxDegrees) {
		Chain result;
		
		result = null;
		
		Queue<Individual> queue=new LinkedList<Individual>();
	    Set<Individual> visited = new HashSet<Individual>();
	    Map<Individual,Chain> map = new HashMap<Individual,Chain>();
	    
		queue.add(ego);
		visited.add(ego);
	    map.put(ego,new Chain(ego));
	    
	    int maxOrder = maxDegrees.length;
		
		while(!queue.isEmpty()){
			Individual last = queue.remove();
			Chain chain = map.get(last);
			for (KinType kinType : KinType.basicTypes()){
				if ((chain.size()==1) || (chain.dir(chain.size()-1)!=-1 || kinType!=KinType.PARENT)) {
					Individuals kin = last.getKin(kinType);
					if (kin!=null){
						for (Individual v : kin){
							if (v!=null && !visited.contains(v)) {
								Chain newChain = chain.clone();
								newChain.add(v,kinType.toInt());
								if (newChain.dim()<=maxOrder && newChain.depth()<=maxDegrees[newChain.dim()-1]) {
									if (v==alter) {
										result = newChain.neutralize(SiblingMode.FULL);
										return result;
									}
									map.put(v,newChain);
									queue.add(v);
									visited.add(v);
								}
							}
						}
					}
				}
			}
		}
		
		return result;
	}
	
	
	/**
	 * gets the distance of the consanguineous chain between a spouse vertex and a third vertex
	 * @param alter the third vertex to be checked 
	 * @param g the filter criterion for ascendant lines (0 agnatic, 1 uterine, 2 cognatic) 
	 * @return the distance of the consanguineous chain between v and a spouse vertex, or -1 if there is no such chain
	 * @see OldIndividual#getDistance(OldIndividual, int, boolean)
	 */
	private static int getAffinalDistance (Individual ego, Individual alter, FiliationType g){
		int i = -1;
		if (!ego.isSingle()) {
			for (Individual spouse : ego.spouses()){
				int d = getDistance(spouse,alter,g);
				if (d>0 && (i<0 || d<i)) i = d;
			}
		}
		if (!alter.isSingle()) {
			for (Individual spouse : alter.spouses()){
				int d = getDistance(spouse,ego,g);
				if (d>0 && (i<0 || d<i)) i = d;
			}
		}
		return i+1;
	}
	
	//Should be eliminated!
	/**
	 * gets the kinship link to an alter vertex coded as an integer pair
	 * @param v the alter vertex 
	 * @return the kinship link as an integer pair (the first number is the ungendered kinship tie index (-1 child, 0 spouse, 1 parent), the second the ordinal index of alter within the kin group)
	 * @see maps.Net#Net(maps.groupmaps.RingGroupMap)
	 */
	public static int[] getLink (Individual ego, Individual alter){
		for (int i=-1;i<2;i++){
			Individuals k = ego.getKin(i);
			if (k==null) continue;
			int j=0;
			for (Individual relative : k){
				if (relative==alter) {
					return new int[]{i,j};
				}
				j++;
			}
		}
		return null;
	}
	

	
	
	// should be defined as a Report 
	
	   /**
	 * gets the profile of kinship ties and distances to an alter vertex
	 * @param v the alter vertex
	 * @return the profile of kinship tie as an integer array
	 * @see OldIndividual#relationStatistics(List)
	 */
/*	private static int[] getLinks(Individual ego, Individual alter){
		int md[] = new int[10];
		int a[] = getLinks(ego, alter, true);
		int b[] = getLinks(ego, alter, false);
		md[9] = 0;
		int minb = minOdd(b);
		int mina = minOdd(a);
		if (minb>0 && (mina==0 || minb<mina)) md[9]=11;
		for (int i=0;i<9;i++){
			if (md[9]==0) md[i]=a[i];
			else md[i]=b[i];
		}
		return md;
	}*/
	   
		/**
	 * gets the profile of kinship ties and distances to an alter vertex
	 * @param v the alter vertex
	 * @cons false if the distnaces are reckoned from ego's spouse, true if they are reckoned from ego
	 * @return the profile of kinship tie as an integer array
	 * @see OldIndividual#getLinks(OldIndividual)
	 */
/*	private static int[] getLinks (Individual ego, Individual alter, boolean cons){
		int bil = 0;
		int[] md = new int[9];
		for (int g=0;g<2;g++){
			int h = getDistance(ego, alter, g,cons);
			if (h<1) continue;
			bil++;
			md[2*g]++;
			md[2*g+1]=md[2*g+1]+h;
		}
		if (bil==2) md[8]++;
		if (bil>0) { //unil
			md[4]++;
			int m = Math.min(md[1],md[3]);
			if (m==0) m = md[1]+md[3];
			md[5]=md[5]+m;
			return md;
		}
		int h = getDistance(ego, alter, -1,cons);
		if (h<1) return md;
		md[4]++;
		md[5]=md[5]+h;
		md[6]++;
		md[7]=md[7]+h;
		return md;
	}*/
	   
		//Simplify and check
		   /**
		    * gets a special census for particular types of cousins
		    * @param k the census type<p>
		    * @param g the character of the sibling relation between parents (0=agnatic, 1=uterine, 2=indifferent)
		    * 0 bilateral cross cousins, 1 oblique cousins, 2 double cross cousins, 3 bilateral parallel cousins
		    */
/*	private void getParticularCrossCousins (Net net, FiliationType g, int k){
		ArrayList<Chain> list = new ArrayList<Chain>();
		for (Individual individual : net.individuals()){
			if (!individual.getGender().isMale()) continue;
			if (k == 0) individual.bilateralCousins(list,g,true);
			else if (k == 1) individual.ObliqueCousins(list, g);
			else if (k == 2) individual.doubleCrossCousins(list,g,1);
			else if (k == 3) individual.bilateralCousins(list,g,false);
		}*/

  
	 /**
	    * gets the siblings in the Net that are at the same time cousins
	    * @param list the list of sibling-cousins
	    * @param g the character of the sibling relation (0=agnatic, 1=uterine, 2=indifferent)
	    */
/*	   private void getSiblingCousins (Net net, FiliationType g){
	         ArrayList<Chain> list = new ArrayList<Chain>();
	         for (Individual v : net.individuals()){
	       	  if (!v.getGender().isMale()) continue;
	       	  v.siblingCousins(list,g);
	         }
	   }*/
	   
	   
	   /** gets the guignard-"spirals" of the Net (as a list of chains of unilinear groups)
	    * @param g the filter criterion for ascendant lines (0 agnatic, 1 uterine, 2 cognatic) 
	    * @return the list of spirals
	    */
/*	   private ArrayList<Chain> getSpirals (Net net, FiliationType g){

		   ArrayList<Chain> list = new ArrayList<Chain>();
		   for (Individual v : net.individuals()){
			   if (v.getGender().toInt()!=g.toInt()) continue;
			   if (v.spouses()==null) continue;
			   for (Individual w : v.getInlaws(g)){
				   for (Individual u : w.getInlaws(g)){
					   if (v.hasSameAncestor(u,g)) continue;
					   for (Individual x : u.obliques(g)){
						   if (v.spouses().contains(x)) {
							   Chain p = new Chain(v,x);
							   if (!list.contains(p)) {
								   list.add(p);
							   }
						   }
					   }
				   }
			   }
		   }
		   return list;
	   }*/
	   
	   /**
	 * gets the profile of kinship relations to the other vertices of a cluster
	 * @param clu the cluster of vertices
	 * @return the profile of kinship relations as an integer matrix
	 * @see trash.Part#relationStatistics()
	 */
/*	public static int[][] relationStatistics (Individual ego, Cluster<Individual> clu){
		int[][] md = new int[20][2]; //9
		for (Individual alter : clu.getItems()){
			if (alter.getId()<=ego.getId()) continue;
			int cl = conjugalLink(ego, alter);
			if (cl>0) { 
				md[10][2-cl]++; 
				continue;
			}
			int k = 0;
			if (ego.getGender()!=alter.getGender()) k = 1;
			int[] m = getLinks(ego, alter);
			for (int i=0;i<9;i++){
				md[i+m[9]][k]=md[i+m[9]][k]+m[i];
			}
		}
		if (!ego.isSterile()) {
			for (Individual alter : ego.children()){
				if (clu.getItems().contains(alter)) {
					int k = 0;
					if (ego.getGender()!=alter.getGender()) k = 1;
					md[9][k]++;
				}
			}
		}
		return md;
	}*/
	
   
	/**
	 * gets the pedigree of a vertex (recursive part)
	 * @param pedigree the preceding parts of the pedigree as a string list
	 * @param maxDepth the maximal depth of the pedigree
	 * @param nameString the preceding chain of ascendants' names
	 * @param kinString the preceding kinship chain in positional notation
	 * @param sep a separator
	 * @see OldIndividual#getPedigree(int)
	 */
	private static void getPedigree (StringList pedigree, Individual ego, int maxDepth, String nameString, String kinString, String sep) {
		if (kinString.length()<=maxDepth) {
		    pedigree.append(nameString+kinString+sep+ego.getName());
		    if (!ego.isOrphan()) {
			    for (Individual parent : ego.getParents().toSortedList(Sorting.GENDER)) {
			    	if (parent!=null){
				    	getPedigree(pedigree,parent,maxDepth,nameString+"  ",kinString+parent.getGender().toAscendantChar(),": ");
			    	}
			    }
		    }
		}
	}
	
	/**
	 * gets the progeniture of a vertex (recursive part)
	 * @param progeniture the preceding parts of the progeniture as a string list
	 * @param maxDepth the maximal depth of the progeniture
	 * @param nameString the progeniture chain of ascendants' names
	 * @param kinString the progeniture kinship chain in positional notation
	 * @param sep a separator
	 */
	private static void getProgeniture (StringList progeniture, Individual ego, int maxDepth, String nameString, String kinString, String sep) {
		if (kinString.length()<=maxDepth) {
		    progeniture.append(nameString+kinString+sep+ego.getName());
		    if (!ego.isSterile()) {
			    for (Individual child : ego.children().toSortedList(Sorting.GENDER)) {
			    	if (child!=null){
				    	getProgeniture(progeniture,child,maxDepth,nameString+"  ",kinString+child.getGender().toDescendantChar(),": ");
			    	}
			    }
		    }
		}
	}

	/**
	 * gets the pedigree of a vertex
	 * @param maxDepth the maximal depth of the pedigree
	 * @return the pedigree as a string list
	 */
	public static String getPedigree (Individual ego, int maxDepth) {
		String result;
		
		StringList list = new StringList();
		getPedigree(list,ego, maxDepth,"","","");
		
		result = list.toStringSeparatedBy("\n");
		//
		return result;
	}	
	
	/**
	 * gets the progeniture of a vertex
	 * @param maxDepth the maximal depth of the progeniture
	 * @return the progeniture as a string list
	 */
	public static String getProgeniture (Individual ego, int maxDepth) {
		String result;
		
		StringList list = new StringList();
		getProgeniture(list,ego, maxDepth,"","","");
		
		result = list.toStringSeparatedBy("\n");
		//
		return result;
	}	

	/**
	 * gets the relatives of the vertex as a string list
	 * @param kinString the code of the kinship relation in positional notation
	 * @return the string list of relatives
	 */
	public static Partition<Chain> getKin (Individual ego, String kinString) {
	
		Partition<Chain> result = new Partition<Chain>();      
		getKin(result, new Chain(ego),ChainMaker.fromString(kinString));
		
		//
		return result;
	}
	
	
	/**
	 * gets the relatives of the vertex as a string list (recursive part)
	 * @param relatives the existing part of the relatives list as a string list
	 * @param i the length of the preceding kinship chain
	 * @param chain the preceding kinship chain
	 * @param abstractChain the abstract kinship relation in ring format
	 */
	private static void getKin (Partition<Chain> relatives, Chain chain, Chain abstractChain) {
		
		int length = chain.length();
		Individual relative = chain.get(length);
		Gender targetGender = abstractChain.get(length).getGender();
		
		if (targetGender.isUnknown() || relative.getGender()==targetGender) {
			if (length == abstractChain.length()) {
/*				Cluster<Chain> clu = relatives.getCluster(new Value(relative));
				if (clu==null) {
					clu = new Cluster<Chain>(new Value(relative));
				}
				clu.put(chain.clone());*/
				relatives.put(chain.clone(), new Value(relative));
			} else {
				int dir = abstractChain.dir(length+1);
				Individuals kinGroup = relative.getKin(dir);
				if (kinGroup != null) {
					for (Individual v : kinGroup) {
						if (v!=null && !chain.contains(v)) {
							chain.add(v, dir);
							getKin(relatives,chain,abstractChain);
							chain.removeLast();
						}
					}
				}
			}
		}
	}	



}
