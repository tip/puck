/*
 * Copyright 2015 Klaus Hamberger, Christian Pierre Momon, Devinsy and UMR 7186
 * LESC.
 * 
 * Copyright 2010-2014 Jean-Daniel Fekete, Pierre Dragicevic and INRIA.
 * 
 * This file is part of GeneaQuilt. This software (GeneaQuilt) is a computer
 * program whose purpose is a new visualization technique for representing large
 * genealogies.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.tip.puckgui.views.geneaquilt;

import fr.devinsy.util.StringList;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class EventQuiltCriteria {

	private boolean showUndatedEvent;
	private boolean showUnlinkedIndividual;
	private Long componentSteps;
	private StringList models;

	/**
	 * 
	 */
	public EventQuiltCriteria() {
		this.showUndatedEvent = true;
		this.showUnlinkedIndividual = true;
		this.componentSteps = null;
		this.models = new StringList();
	}

	public Long getComponentSteps() {
		return this.componentSteps;
	}

	public StringList getModels() {
		return this.models;
	}

	public boolean isShowUndatedEvent() {
		return this.showUndatedEvent;
	}

	public boolean isShowUnlinkedIndividual() {
		return this.showUnlinkedIndividual;
	}

	public void setComponentSteps(final Long componentSteps) {
		this.componentSteps = componentSteps;
	}

	public void setShowUndatedEvent(final boolean showUndatedEvent) {
		this.showUndatedEvent = showUndatedEvent;
	}

	public void setShowUnlinkedIndividual(final boolean showUnlinkedIndividual) {
		this.showUnlinkedIndividual = showUnlinkedIndividual;
	}

}
