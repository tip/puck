package org.tip.puck.segmentation;

import java.util.ArrayList;
import java.util.List;

import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.geo.Geography;
import org.tip.puck.net.Families;
import org.tip.puck.net.Family;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionCriteria.FamilyScope;
import org.tip.puck.partitions.PartitionMaker;

/**
 * 
 * 
 * @author TIP
 */
public class RelationSegment implements Segment {
	private PartitionCriteria criteria;
	private Partition<Relation> partition;
	private Cluster<Relation> currentCluster;
	private int currentClusterIndex;
	private Individuals currentIndividuals;
	private Families currentFamilies;
	private Relations currentRelations;
	private Individuals outOfPartitionIndividuals;
	private Families outOfPartitionFamilies;
	private Relations outOfPartitionRelations;
	private Geography geography;

	/**
	 * 
	 * @param source
	 * @param criteria
	 * @throws PuckException
	 */
	public RelationSegment(final Segment source, final PartitionCriteria criteria) throws PuckException {

		this.criteria = criteria;
		refresh(source);

		if (this.currentClusterIndex != 0) {
			selectCluster(0);
		}
	}

	/**
	 * 
	 */
	@Override
	public int getClusterCount() {
		int result;

		result = this.partition.getClusters().size();

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getClusterLabels() {
		List<String> result;

		//
		result = new ArrayList<String>();

		//
		if (this.partition != null) {
			for (Cluster<Relation> cluster : this.partition.getClusters().toListSortedByValue()) {
				String label = cluster.getLabel();
				if (label == null) {
					label = "[null]";
				}
				result.add(String.format("%s (%d)", cluster.getLabel(), cluster.size()));
			}
		}

		//
		result.add("*");

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public PartitionCriteria getCriteria() {
		PartitionCriteria result;

		result = this.criteria;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Cluster<Relation> getCurrentCluster() {
		Cluster<Relation> result;

		result = this.currentCluster;

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getCurrentClusterIndex() {
		int result;

		result = this.currentClusterIndex;

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Families getCurrentFamilies() {
		Families result;

		result = this.currentFamilies;

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Individuals getCurrentIndividuals() {
		Individuals result;

		result = this.currentIndividuals;

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Relations getCurrentRelations() {
		Relations result;

		result = this.currentRelations;

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getCurrentSize() {
		int result;

		// Note: currentCluster is null on out selection.
		if (this.currentCluster == null) {
			result = this.outOfPartitionRelations.size();
		} else {
			result = this.currentCluster.size();
		}

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLabel() {
		String result;

		if (this.currentCluster == null) {
			result = this.partition.getCriteria().toShortString() + " : *";
		} else {
			result = this.partition.getCriteria().toShortString() + " : " + this.currentCluster.getLabel();
		}

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Families getOutOfCurrentClusterFamilies() {
		Families result;

		result = new Families();

		// Code is splitted/unshared to increase expressiveness.
		if (this.currentCluster == null) {
			for (Cluster<Relation> cluster : this.partition.getClusters()) {
				Individuals individuals = findIndividualsFromRelations(new Relations(cluster.getItems()), this.outOfPartitionIndividuals);
				Families families = findFamiliesFromIndividuals(individuals, this.partition.getCriteria().getFamilyScope(), this.outOfPartitionFamilies);
				for (Family family : families) {
					result.add(family);
				}
			}
		} else {
			for (Cluster<Relation> cluster : this.partition.getClusters()) {
				if (cluster != this.currentCluster) {
					Individuals individuals = findIndividualsFromRelations(new Relations(cluster.getItems()), this.outOfPartitionIndividuals);
					Families families = findFamiliesFromIndividuals(individuals, this.partition.getCriteria().getFamilyScope(), this.outOfPartitionFamilies);
					for (Family family : families) {
						if (!this.currentFamilies.contains(family)) {
							result.add(family);
						}
					}
				}
			}

			//
			result.add(this.outOfPartitionFamilies);
		}

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Individuals getOutOfCurrentClusterIndividuals() {
		Individuals result;

		//
		result = new Individuals();

		// Code is splitted/unshared to increase expressiveness.
		if (this.currentCluster == null) {
			for (Cluster<Relation> cluster : this.partition.getClusters()) {
				Individuals individuals = findIndividualsFromRelations(new Relations(cluster.getItems()), this.outOfPartitionIndividuals);
				for (Individual individual : individuals) {
					result.add(individual);
				}
			}
		} else {
			for (Cluster<Relation> cluster : this.partition.getClusters()) {
				if (cluster != this.currentCluster) {
					Individuals individuals = findIndividualsFromRelations(new Relations(cluster.getItems()), this.outOfPartitionIndividuals);
					for (Individual individual : individuals) {
						if (!this.currentIndividuals.contains(individual)) {
							result.add(individual);
						}
					}
				}
			}

			//
			result.add(this.outOfPartitionIndividuals);
		}

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Relations getOutOfCurrentClusterRelations() {
		Relations result;

		result = new Relations();

		// Code is splitted/unshared to increase expressiveness.
		if (this.partition == null) {
			for (Cluster<Relation> cluster : this.partition.getClusters()) {
				result.add(cluster.getItems());
			}
		} else {
			for (Cluster<Relation> cluster : this.partition.getClusters()) {
				if (cluster != this.currentCluster) {
					result.add(cluster.getItems());
				}
			}

			//
			result.add(this.outOfPartitionRelations);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public Partition<Relation> getPartition() {
		Partition<Relation> result;

		result = this.partition;

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getShortLabel() {
		String result;

		result = this.partition.getCriteria().toShortString();

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getShortShortLabel() {
		String result;

		if (this.currentCluster == null) {
			result = this.partition.getCriteria().getLabel() + " : *";
		} else {
			result = this.partition.getCriteria().getLabel() + " : " + this.currentCluster.getValue();
		}

		//
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void refresh(final Segment source) throws PuckException {
		//
		Cluster<Relation> previousCurrentCluster = this.currentCluster;

		this.geography = source.getGeography();

		//
		this.partition = PartitionMaker.create(this.criteria.toShortString(), source.getCurrentRelations(), this.criteria, this.geography);

		//
		this.outOfPartitionIndividuals = source.getOutOfCurrentClusterIndividuals();
		this.outOfPartitionFamilies = source.getOutOfCurrentClusterFamilies();
		this.outOfPartitionRelations = source.getOutOfCurrentClusterRelations();

		//
		if (previousCurrentCluster == null) {
			//
			selectOutOfPartitionCluster();

		} else {
			//
			this.currentCluster = this.partition.getClusters().get(previousCurrentCluster.getValue());

			if (this.currentCluster == null) {
				//
				selectCluster(0);
			}
		}
	}

	/**
	 * Nothing to do.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public void selectCluster(final Family family) throws PuckException {
	}

	/**
	 * Nothing to do.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public void selectCluster(final Individual individual) throws PuckException {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void selectCluster(final int index) throws PuckException {
		if (index < 0) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Index out of bounds.");

		} else if ((this.partition == null) && (index > 0)) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Index out of bounds.");

		} else if ((this.partition != null) && (index >= this.partition.getClusters().size() + 1)) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Index out of bounds.");

		} else {
			//
			if (index < this.partition.size()) {
				this.currentCluster = this.partition.getClusters().toListSortedByValue().get(index);
				this.currentClusterIndex = index;
				this.currentRelations = new Relations(this.currentCluster.getItems());
				this.currentIndividuals = findIndividualsFromRelations(this.currentRelations, this.outOfPartitionIndividuals);
				this.currentFamilies = findFamiliesFromIndividuals(this.currentIndividuals, this.partition.getCriteria().getFamilyScope(),
						this.outOfPartitionFamilies);
			} else {
				this.currentCluster = null;
				this.currentClusterIndex = this.partition.getClusters().size();
				this.currentRelations = this.outOfPartitionRelations;
				this.currentIndividuals = this.outOfPartitionIndividuals;
				this.currentFamilies = this.outOfPartitionFamilies;
			}
		}
	}

	/**
	 * Nothing to do.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public void selectCluster(final Relation relation) throws PuckException {
		//
		if (relation == null) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");

		} else {
			//
			this.currentCluster = this.partition.getCluster(relation);
			if (this.currentCluster == null) {
				selectOutOfPartitionCluster();
			} else {
				//
				List<Cluster<Relation>> clusters = this.partition.getClusters().toListSortedByValue();
				boolean ended = false;
				int clusterIndex = 0;
				while (!ended) {
					if (clusters.get(clusterIndex) == this.currentCluster) {
						ended = true;
					} else {
						clusterIndex += 1;
					}
				}

				//
				selectCluster(clusterIndex);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws PuckException
	 */
	@Override
	public void selectOutOfPartitionCluster() throws PuckException {
		selectCluster(this.partition.size());
	}

	/**
	 * 
	 * @param cluster
	 * @return
	 */
	private static Families findFamiliesFromIndividuals(final Individuals source, final FamilyScope familyScope, final Families outs) {
		Families result;

		//
		result = new Families();

		//
		for (Individual individual : source) {
			for (Family family : individual.getPersonalFamilies()) {
				if (!outs.contains(family)) {
					if (familyScope == FamilyScope.SOME) {
						result.add(family);
					} else {
						Individual spouse = family.getOtherParent(individual);
						if ((spouse != null) || (source.contains(spouse))) {
							result.add(family);
						}
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param cluster
	 * @return
	 */
	private static Individuals findIndividualsFromRelations(final Relations source, final Individuals outs) {
		Individuals result;

		//
		result = new Individuals();

		//
		for (Relation relation : source) {
			for (Actor actor : relation.actors()) {
				if (!outs.contains(actor.getIndividual())) {
					result.add(actor.getIndividual());
				}
			}
		}

		//
		return result;
	}

	public Geography getGeography() {
		return geography;
	}

	public void setGeography(Geography geography) {
		this.geography = geography;
	}
	
	
}
