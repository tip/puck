package org.tip.puck.net.workers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.geo.Geography;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Attributes;
import org.tip.puck.net.Families;
import org.tip.puck.net.Family;
import org.tip.puck.net.Gender.GenderCode;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Actors;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionMaker;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.util.NumberedValues;
import org.tip.puck.util.Value;

import fr.devinsy.util.StringSet;

/**
 * 
 * Methods added in this classes has to be static and autonomous.
 * 
 * @author TIP
 */
public class AttributeWorker {

	public enum CaseOption {
		CASE_SENSITIVE,
		IGNORE_CASE
	}

	public enum EmptyType {
		VOID,
		BLANK,
		FORCED_BLANK
	}

	public enum Scope {
		ALL,
		CORPUS,
		INDIVIDUALS,
		FAMILIES,
		RELATION,
		RELATIONS,
		ACTORS,
		NONE
	}

	public static final Pattern YEAR_PATTERN = Pattern.compile("(\\d\\d\\d\\d)");

	private static final Logger logger = LoggerFactory.getLogger(AttributeWorker.class);

	/**
	 * This method anonymizes individuals leaving only the first name.
	 * 
	 * @param source
	 *            Source to anonymize.
	 * 
	 * @return the number of field anonymized
	 */
	public static long anonymizeByFirstName(final Individuals source) {
		long result;

		//
		if (source == null) {
			//
			result = 0;

		} else {
			//
			for (Individual individual : source) {
				if (!NetUtils.notToAnonymize(individual)){
					individual.setName(StringUtils.defaultString(individual.getFirstName(), ""));
				}
			}

			//
			result = source.size();
		}

		//
		return result;
	}

	/**
	 * This method anonymizes a net leaving only the first name.
	 * 
	 * @param source
	 *            Source to anonymize.
	 * 
	 * @return the number of field anonymized
	 */
	public static long anonymizeByFirstName(final Net source) {
		long result;

		//
		if (source == null) {
			//
			result = 0;

		} else {
			//
			result = anonymizeByFirstName(source.individuals());
		}

		//
		return result;
	}

	/**
	 * This method anonymizes individuals replacing name by gender and id.
	 * 
	 * @param source
	 *            Source to anonymize.
	 * 
	 * @return the number of field anonymized
	 */
	public static long anonymizeByGenderAndId(final Individuals source, final GenderCode code) {
		long result;

		//
		if (source == null) {
			//
			result = 0;

		} else {
			//
			for (Individual individual : source) {
				if (!NetUtils.notToAnonymize(individual)){
					individual.setName(individual.getGender().toChar(code) + " " + individual.getId());
				}
			}

			//
			result = source.size();
		}

		//
		return result;
	}

	/**
	 * This method anonymizes a net replacing name by gender and id.
	 * 
	 * @param source
	 *            Source to anonymize.
	 * 
	 * @return the number of field anonymized
	 */
	public static long anonymizeByGenderAndId(final Net source, final GenderCode code) {
		long result;

		//
		if (source == null) {
			//
			result = 0;

		} else {
			//
			result = anonymizeByGenderAndId(source.individuals(), code);
		}

		//
		return result;
	}

	/**
	 * This method anonymizes individuals leaving only the last name.
	 * 
	 * @param source
	 *            Source to anonymize.
	 * 
	 * @return the number of field anonymized
	 */
	public static long anonymizeByLastName(final Individuals source) {
		long result;

		//
		if (source == null) {
			//
			result = 0;

		} else {
			//
			for (Individual individual : source) {
				if (!NetUtils.notToAnonymize(individual)){
					individual.setName(StringUtils.defaultString(individual.getLastName(), ""));
				}
			}

			//
			result = source.size();
		}

		//
		return result;
	}

	/**
	 * This method anonymizes a net leaving only the last name.
	 * 
	 * @param source
	 *            Source to anonymize.
	 * 
	 * @return the number of field anonymized
	 */
	public static long anonymizeByLastName(final Net source) {
		long result;

		//
		if (source == null) {
			//
			result = 0;

		} else {
			//
			result = anonymizeByLastName(source.individuals());
		}

		//
		return result;
	}

	/**
	 * This method anonymizes families by numbering each one.
	 * 
	 * @param source
	 *            Source to anonymize.
	 * 
	 * @label label of attribute to anonymize.
	 * 
	 * @return the number of field anonymized
	 */
	public static int anonymizeByNumbering(final Actors source, final String label) {
		int result;

		//
		result = 0;

		//
		if ((source != null) && (label != null)) {
			//
			Hashtable<String, String> pivot = new Hashtable<String, String>();

			//
			for (Actor actor : source) {
				//
				String oldValue = actor.attributes().getValue(label);
				if (oldValue != null) {
					//
					oldValue = oldValue.toLowerCase();

					//
					String newValue = pivot.get(oldValue);

					//
					if (newValue == null) {
						newValue = String.valueOf(pivot.size() + 1);
						pivot.put(oldValue, newValue);
					}

					//
					actor.attributes().put(label, newValue);

					//
					result += 1;
				}
			}
		}

		//
		return result;
	}

	/**
	 * This method anonymizes families by numbering each one.
	 * 
	 * @param source
	 *            Source to anonymize.
	 * 
	 * @label label of attribute to anonymize.
	 * 
	 * @return the number of field anonymized
	 */
	public static int anonymizeByNumbering(final Hashtable<String, String> pivot, final Actors source, final String labelPart) {
		int result;

		//
		result = 0;

		//
		if ((source != null) && (labelPart != null)) {
			//
			for (Actor actor : source) {
				
				for (Attribute attribute : actor.attributes()){
					
					String label = attribute.getLabel();
					
					if (label.contains(labelPart)){

						//
						String oldValue = actor.attributes().getValue(label);
						if (oldValue != null) {
							//
							oldValue = oldValue.toLowerCase();

							//
							String newValue = pivot.get(oldValue);

							//
							if (newValue == null) {
								newValue = String.valueOf(pivot.size() + 1);
								pivot.put(oldValue, newValue);
							}

							//
							actor.attributes().put(label, newValue);

							//
							result += 1;
						}
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * This method anonymizes families by numbering each one.
	 * 
	 * @param source
	 *            Source to anonymize.
	 * 
	 * @label label of attribute to anonymize.
	 * 
	 * @return the number of field anonymized
	 */
	public static int anonymizeByNumbering(final Families source, final String label) {
		int result;

		//
		result = 0;

		//
		if ((source != null) && (label != null)) {
			//
			Hashtable<String, String> pivot = new Hashtable<String, String>();

			//
			for (Family family : source) {
				//
				String oldValue = family.attributes().getValue(label);
				if (oldValue != null) {
					//
					oldValue = oldValue.toLowerCase();

					//
					String newValue = pivot.get(oldValue);

					//
					if (newValue == null) {
						newValue = String.valueOf(pivot.size() + 1);
						pivot.put(oldValue, newValue);
					}

					//
					family.attributes().put(label, newValue);

					//
					result += 1;
				}
			}
		}

		//
		return result;
	}

	
	/**
	 * This method anonymizes families by numbering each one.
	 * 
	 * @param source
	 *            Source to anonymize.
	 * 
	 * @label label of attribute to anonymize.
	 * 
	 * @return the number of field anonymized
	 */
	public static int anonymizeByNumbering(final Hashtable<String, String> pivot, final Families source, final String labelPart) {
		int result;

		//
		result = 0;

		//
		if ((source != null) && (labelPart != null)) {

			//
			for (Family family : source) {
				
				for (Attribute attribute : family.attributes()){
					
					String label = attribute.getLabel();
					
					if (label.contains(labelPart)){

						//
						String oldValue = family.attributes().getValue(label);
						if (oldValue != null) {
							//
							oldValue = oldValue.toLowerCase();

							//
							String newValue = pivot.get(oldValue);

							//
							if (newValue == null) {
								newValue = String.valueOf(pivot.size() + 1);
								pivot.put(oldValue, newValue);
							}

							//
							family.attributes().put(label, newValue);

							//
							result += 1;
						}
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * This method anonymizes families by numbering each one.
	 * 
	 * @param source
	 *            Source to anonymize.
	 * 
	 * @label Label of attribute to anonymize.
	 * 
	 * @return The number of field anonymized.
	 */
	public static int anonymizeByNumbering(final Hashtable<String, String> pivot, final Individuals source, final String labelPart) {
		int result;

		//
		result = 0;

		//
		if ((source != null) && (labelPart != null)) {
			//
			for (Individual individual : source) {
				
				for (Attribute attribute : individual.attributes()){
					
					String label = attribute.getLabel();
					
					if (label.contains(labelPart)){
						
						//
						String oldValue = individual.attributes().getValue(label);
						if (oldValue != null) {
							//
							oldValue = oldValue.toLowerCase();

							//
							String newValue = pivot.get(oldValue);

							//
							if (newValue == null) {
								newValue = String.valueOf(pivot.size() + 1);
								pivot.put(oldValue, newValue);
							}

							//
							individual.attributes().put(label, newValue);

							//
							result += 1;
						}
					}
				}
			}
		}

		//
		return result;
	}



	/**
	 * This method anonymizes families by numbering each one.
	 * 
	 * @param source
	 *            Source to anonymize.
	 * 
	 * @label Label of attribute to anonymize.
	 * 
	 * @return The number of field anonymized.
	 */
	public static int anonymizeByNumbering(final Individuals source, final String label) {
		int result;

		//
		result = 0;

		//
		if ((source != null) && (label != null)) {
			//
			Hashtable<String, String> pivot = new Hashtable<String, String>();

			//
			for (Individual individual : source) {
				//
				String oldValue = individual.attributes().getValue(label);
				if (oldValue != null) {
					//
					oldValue = oldValue.toLowerCase();

					//
					String newValue = pivot.get(oldValue);

					//
					if (newValue == null) {
						newValue = String.valueOf(pivot.size() + 1);
						pivot.put(oldValue, newValue);
					}

					//
					individual.attributes().put(label, newValue);

					//
					result += 1;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param filter
	 * @return The number of field anonymized.
	 */
	public static long anonymizeByNumbering(final Net net, final AttributeFilter filter) {
		long result;

		//
		result = 0;

		//
		if ((net != null) && (filter != null)) {
			//
			if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.CORPUS)) {
				//
				result += filter(net.attributes(), filter.getLabel(), filter.getMode());
			}

			//
			if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.INDIVIDUALS)) {
				//
				result += anonymizeByNumbering(net.individuals(), filter.getLabel());
			}

			//
			if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.FAMILIES)) {
				//
				result += anonymizeByNumbering(net.families(), filter.getLabel());
			}

			//
			if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.RELATIONS)) {
				//
				for (RelationModel model : net.relationModels()) {
					//
					Relations relations = net.relations().getByModel(model);
					result += anonymizeByNumbering(relations, filter.getLabel());
				}

			} else if (filter.getScope() == AttributeFilter.Scope.RELATION) {
				//
				Relations relations = net.relations().getByModelName(filter.getOptionalRelationName());
				result = anonymizeByNumbering(relations, filter.getLabel());
			}

			//
			if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.ACTORS)) {
				//
				result += anonymizeByNumbering(net.relations().getActors(), filter.getLabel());
			}
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @param net
	 * @param filter
	 * @return The number of field anonymized.
	 */
	public static long anonymizeByConsistentNumbering(final Net net, final Segmentation segmentation, final AttributeFilter filter) {
		long result;

		//
		result = 0;
		
		Hashtable<String, String> pivot = new Hashtable<String, String>();

		//
		if ((net != null) && (filter != null)) {
			//
			if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.CORPUS)) {
				//
				result += filter(net.attributes(), filter.getLabel(), filter.getMode());
			}

			//
			if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.INDIVIDUALS)) {
				//
				result += anonymizeByNumbering(pivot, segmentation.getCurrentIndividuals(), filter.getLabel());
			}

			//
			if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.FAMILIES)) {
				//
				result += anonymizeByNumbering(pivot, segmentation.getCurrentFamilies(), filter.getLabel());
			}

			//
			if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.RELATIONS)) {
				//
				for (RelationModel model : net.relationModels()) {
					//
					Relations relations = segmentation.getCurrentRelations().getByModel(model);
					result += anonymizeByNumbering(pivot, relations, filter.getLabel());
				}

			} else if (filter.getScope() == AttributeFilter.Scope.RELATION) {
				//
				Relations relations = segmentation.getCurrentRelations().getByModelName(filter.getOptionalRelationName());
				result = anonymizeByNumbering(pivot, relations, filter.getLabel());
			}

			//
			if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.ACTORS)) {
				//
				result += anonymizeByNumbering(pivot, segmentation.getCurrentRelations().getActors(), filter.getLabel());
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param filter
	 * @return The number of field anonymized.
	 */
	public static long anonymizeByNumbering(final Net net, final Segmentation segmentation, final AttributeFilter filter) {
		long result;

		//
		result = 0;

		//
		if ((net != null) && (filter != null)) {
			//
			if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.CORPUS)) {
				//
				result += filter(net.attributes(), filter.getLabel(), filter.getMode());
			}

			//
			if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.INDIVIDUALS)) {
				//
				result += anonymizeByNumbering(segmentation.getCurrentIndividuals(), filter.getLabel());
			}

			//
			if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.FAMILIES)) {
				//
				result += anonymizeByNumbering(segmentation.getCurrentFamilies(), filter.getLabel());
			}

			//
			if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.RELATIONS)) {
				//
				for (RelationModel model : net.relationModels()) {
					//
					Relations relations = segmentation.getCurrentRelations().getByModel(model);
					result += anonymizeByNumbering(relations, filter.getLabel());
				}

			} else if (filter.getScope() == AttributeFilter.Scope.RELATION) {
				//
				Relations relations = segmentation.getCurrentRelations().getByModelName(filter.getOptionalRelationName());
				result = anonymizeByNumbering(relations, filter.getLabel());
			}

			//
			if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.ACTORS)) {
				//
				result += anonymizeByNumbering(segmentation.getCurrentRelations().getActors(), filter.getLabel());
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param label
	 * @return The number of field anonymized.
	 */
	public static int anonymizeByNumbering(final Relations source, final String label) {
		int result;

		//
		result = 0;

		//
		if ((source != null) && (label != null)) {
			//
			Hashtable<String, String> pivot = new Hashtable<String, String>();

			//
			for (Relation relation : source) {
				//
				String oldValue = relation.attributes().getValue(label);
				if (oldValue != null) {
					//
					oldValue = oldValue.toLowerCase();

					//
					String newValue = pivot.get(oldValue);

					//
					if (newValue == null) {
						newValue = String.valueOf(pivot.size() + 1);
						pivot.put(oldValue, newValue);
					}

					//
					relation.attributes().put(label, newValue);

					//
					result += 1;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param label
	 * @return The number of field anonymized.
	 */
	public static int anonymizeByNumbering(final Hashtable<String,String> pivot, final Relations source, final String labelPart) {
		int result;

		//
		result = 0;

		//
		if ((source != null) && (labelPart != null)) {
			//
			for (Relation relation : source) {
				
				for (Attribute attribute : relation.attributes()){
					
					String label = attribute.getLabel();
					
					if (label.contains(labelPart)){
				
						//
						String oldValue = relation.attributes().getValue(label);
						if (oldValue != null) {
							//
							oldValue = oldValue.toLowerCase();

							//
							String newValue = pivot.get(oldValue);

							//
							if (newValue == null) {
								newValue = String.valueOf(pivot.size() + 1);
								pivot.put(oldValue, newValue);
							}

							//
							relation.attributes().put(label, newValue);

							//
							result += 1;
						}
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * 
	 * @return The number of field capitalized.
	 */
	public static long capitalizeValue(final Attribute target) {
		long result;

		if (target == null) {
			//
			result = 0;

		} else if (StringUtils.isEmpty(target.getValue())) {
			//
			result = 0;

		} else if (Character.isLowerCase(target.getValue().charAt(0))) {
			//
			target.setValue(StringUtils.capitalize(target.getValue()));
			result = 1;

		} else {
			//
			result = 0;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param attributeLabel
	 * 
	 * @return The number of attributes cleaned.
	 */
	public static int cleanAttribute(final Individuals source, final String attributeLabel) {
		int result;

		//
		result = 0;

		//
		for (Individual individual : source) {
			//
			Attribute attribute = individual.attributes().get(attributeLabel);

			//
			if ((attribute != null) && ((StringUtils.isBlank(attribute.getValue()) || (attribute.getValue().trim().equals("0"))))) {
				individual.attributes().remove(attributeLabel);
				logger.debug("remove ===>" + individual.getId() + " " + individual.attributes().get(attributeLabel));
				result += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param attributes
	 * @param label
	 * @param filter
	 * 
	 * @return The number of attributes filtered.
	 */
	public static long filter(final Attributes source, final String label, final AttributeFilter.Mode filter) {
		long result;

		if (source == null) {
			//
			result = 0;

		} else if (label == null) {
			//
			throw new IllegalArgumentException("label parameter is null");

		} else if (filter == null) {
			//
			throw new IllegalArgumentException("filter parameter is null");

		} else {
			//
			Attribute target = source.get(label);

			//
			switch (filter) {

				case ANONYMIZE_BY_NUMBERING:
					//
					String value = source.getValue(label);

					if (value == null) {
						result = 0;
					} else {
						source.put(label, "1");
						result = 1;
					}
				break;

				case CAPITALIZE_VALUE:
					result = capitalizeValue(target);
				break;

				case CLEAN: {
					//
					if ((target != null) && ((StringUtils.isBlank(target.getValue()) || (target.getValue().trim().equals("0"))))) {
						source.remove(label);
						result = 1;
					} else {
						result = 0;
					}
				}
				break;

				case CLEAN_BLANK: {
					//
					if ((target != null) && (StringUtils.isBlank(target.getValue()))) {
						source.remove(label);
						result = 1;
					} else {
						result = 0;
					}
				}
				break;

				case FORCE_TO_BLANK:
					source.put(label, "");
					result = 1;
				break;

				case LOWERCASE_VALUE:
					result = lowerCaseValue(target);
				break;

				case REDUCE_DATE:
					result = reduceDateValue(target);
				break;

				case REMOVE:
					if (target == null) {
						result = 0;
					} else {
						source.remove(label);
						result = 1;
					}
				break;

				case REPLACE_BY_BLANK:
					if (target == null) {
						result = 0;
					} else {
						target.setValue("");
						result = 1;
					}
				break;

				case TRIM_VALUE:
					result = trimValue(target);
				break;

				case UPPERCASE_VALUE:
					result = upperCaseValue(target);
				break;

				default:
					result = 0;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * 
	 * @param filter
	 * 
	 * @return The number of attributes filtered.
	 */
	public static long filter(final Net net, final AttributeFilter filter) {
		long result;

		if ((net == null) || (filter == null) || (filter.getMode() == AttributeFilter.Mode.NONE)) {
			//
			result = 0;

		} else if (filter.getMode() == AttributeFilter.Mode.ANONYMIZE_BY_NUMBERING) {
			//
			result = anonymizeByNumbering(net, filter);

		} else if ((filter.getScope() == AttributeFilter.Scope.INDIVIDUALS) && (filter.getMode() == AttributeFilter.Mode.ANONYMIZE_BY_FIRST_NAME)) {
			//
			result = AttributeWorker.anonymizeByFirstName(net);

		} else if ((filter.getScope() == AttributeFilter.Scope.INDIVIDUALS) && (filter.getMode() == AttributeFilter.Mode.ANONYMIZE_BY_LAST_NAME)) {
			//
			result = AttributeWorker.anonymizeByLastName(net);

		} else if ((filter.getScope() == AttributeFilter.Scope.INDIVIDUALS) && (filter.getMode() == AttributeFilter.Mode.ANONYMIZE_BY_GENDER_AND_ID_HF)) {
			//
			result = AttributeWorker.anonymizeByGenderAndId(net, GenderCode.HF);

		} else if ((filter.getScope() == AttributeFilter.Scope.INDIVIDUALS) && (filter.getMode() == AttributeFilter.Mode.ANONYMIZE_BY_GENDER_AND_ID_MF)) {
			//
			result = AttributeWorker.anonymizeByGenderAndId(net, GenderCode.MF);

		} else {
			//
			result = 0;

			//
			if ((net != null) && (filter != null)) {

				//
				if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.CORPUS)) {
					//
					result += filter(net.attributes(), filter.getLabel(), filter.getMode());
				}

				//
				if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.INDIVIDUALS)) {
					//
					for (Individual individual : net.individuals()) {
						result += filter(individual.attributes(), filter.getLabel(), filter.getMode());
					}
				}

				//
				if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.FAMILIES)) {
					//
					for (Family family : net.families()) {
						result += filter(family.attributes(), filter.getLabel(), filter.getMode());
					}
				}

				//
				if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.RELATIONS)) {
					//
					for (Relation relation : net.relations()) {
						result += filter(relation.attributes(), filter.getLabel(), filter.getMode());
					}
				}

				//
				if (filter.getScope() == AttributeFilter.Scope.RELATION) {
					//
					Relations relations = net.relations().getByModelName(filter.getOptionalRelationName());
					for (Relation relation : relations) {
						result += filter(relation.attributes(), filter.getLabel(), filter.getMode());
					}
				}

				//
				if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.ACTORS)) {
					//
					for (Actor actor : net.relations().getActors()) {
						result += filter(actor.attributes(), filter.getLabel(), filter.getMode());
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param source
	 * 
	 * @return The number of attributes filtered.
	 */
/*	public static long filter(final Net target, final AttributeFilters source) {
		long result;

		//
		result = 0;

		//
		if ((target != null) && (source != null)) {
			//
			for (AttributeFilter filter : source) {
				//
				result += filter(target, filter);
			}
		}

		//
		return result;
	}*/

	/**
	 * 
	 * @param net
	 * 
	 * @param filter
	 * 
	 * @return The number of attributes filtered.
	 */
	public static long filter(final Net net, final Segmentation segmentation, final AttributeFilter filter) {
		long result;

		if (segmentation == null) {
			//
			result = filter(net, filter);

		} else if ((net == null) || (filter == null) || (filter.getMode() == AttributeFilter.Mode.NONE)) {
			//
			result = 0;

		} else if (filter.getMode() == AttributeFilter.Mode.ANONYMIZE_BY_NUMBERING) {
			//
			result = anonymizeByNumbering(net, segmentation, filter);

		} else if (filter.getMode() == AttributeFilter.Mode.ANONYMIZE_BY_CONSISTENT_NUMBERING) {
			//
			result = anonymizeByConsistentNumbering(net, segmentation, filter);

		} else if ((filter.getScope() == AttributeFilter.Scope.INDIVIDUALS) && (filter.getMode() == AttributeFilter.Mode.ANONYMIZE_BY_FIRST_NAME)) {
			//
			result = AttributeWorker.anonymizeByFirstName(segmentation.getCurrentIndividuals());

		} else if ((filter.getScope() == AttributeFilter.Scope.INDIVIDUALS) && (filter.getMode() == AttributeFilter.Mode.ANONYMIZE_BY_LAST_NAME)) {
			//
			result = AttributeWorker.anonymizeByLastName(segmentation.getCurrentIndividuals());

		} else if ((filter.getScope() == AttributeFilter.Scope.INDIVIDUALS) && (filter.getMode() == AttributeFilter.Mode.ANONYMIZE_BY_GENDER_AND_ID_HF)) {
			//
			result = AttributeWorker.anonymizeByGenderAndId(segmentation.getCurrentIndividuals(), GenderCode.HF);

		} else if ((filter.getScope() == AttributeFilter.Scope.INDIVIDUALS) && (filter.getMode() == AttributeFilter.Mode.ANONYMIZE_BY_GENDER_AND_ID_MF)) {
			//
			result = AttributeWorker.anonymizeByGenderAndId(segmentation.getCurrentIndividuals(), GenderCode.MF);

		} else {
			//
			result = 0;

			//
			if ((net != null) && (filter != null)) {

				//
				if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.CORPUS)) {
					//
					result += filter(net.attributes(), filter.getLabel(), filter.getMode());
				}

				//
				if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.INDIVIDUALS)) {
					//
					for (Individual individual : segmentation.getCurrentIndividuals()) {
						//
						result += filter(individual.attributes(), filter.getLabel(), filter.getMode());
					}
				}

				//
				if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.FAMILIES)) {
					//
					for (Family family : segmentation.getCurrentFamilies()) {
						//
						result += filter(family.attributes(), filter.getLabel(), filter.getMode());
					}
				}

				//
				if ((filter.getScope() == AttributeFilter.Scope.ALL) || (filter.getScope() == AttributeFilter.Scope.RELATIONS)) {
					//
					for (Relation relation : segmentation.getCurrentRelations()) {
						//
						result += filter(relation.attributes(), filter.getLabel(), filter.getMode());
					}
				}

				//
				if (filter.getScope() == AttributeFilter.Scope.RELATION) {
					//
					Relations relations = segmentation.getCurrentRelations().getByModelName(filter.getOptionalRelationName());
					for (Relation relation : relations) {
						//
						result += filter(relation.attributes(), filter.getLabel(), filter.getMode());
					}
				}

				//
				if (filter.getScope() == AttributeFilter.Scope.ACTORS) {
					//
					Actors actors = segmentation.getCurrentRelations().getActors();
					for (Actor actor : actors) {
						//
						result += filter(actor.attributes(), filter.getLabel(), filter.getMode());
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param source
	 * 
	 * @return The number of attributes filtered.
	 */
/*	public static long filter(final Net target, final Segmentation segmentation, final AttributeFilters source) {
		long result;

		//
		result = 0;

		//
		if ((target != null) && (source != null)) {
			//
			for (AttributeFilter filter : source) {
				//
				result += filter(target, segmentation, filter);
			}
		}

		//
		return result;
	}*/

	/**
	 * 
	 * @param source
	 * @param limit
	 * @return
	 */
	public static AttributeDescriptors getExogenousAttributeDescriptors(final Actors source) {
		AttributeDescriptors result;

		//
		result = new AttributeDescriptors(20);

		//
		if (source != null) {
			//
			List<Attribute> attributes = new ArrayList<Attribute>(source.size() * 7);

			//
			for (Actor actor : source) {
				//
				attributes.addAll(actor.attributes().toList());
			}

			//
			AttributeDescriptors descriptors = getExogenousAttributeDescriptors(attributes, source.size(), AttributeDescriptor.Scope.ACTORS, null);

			//
			result.addAll(descriptors);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param limit
	 * @return
	 */
	public static AttributeDescriptors getExogenousAttributeDescriptors(final Families source, final Integer limit) {
		AttributeDescriptors result;

		//
		result = new AttributeDescriptors(20);

		//
		if (source != null) {
			//
			List<Attribute> attributes = new ArrayList<Attribute>(source.size() * 7);

			//
			int index = 0;
			Iterator<Family> iterator = source.iterator();
			while ((iterator.hasNext()) && ((limit == null) || (index < limit))) {
				//
				Family family = iterator.next();

				//
				attributes.addAll(family.attributes().toList());

				//
				index += 1;
			}

			//
			AttributeDescriptors descriptors = getExogenousAttributeDescriptors(attributes, source.size(), AttributeDescriptor.Scope.FAMILIES, null);

			//
			result.addAll(descriptors);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param limit
	 * @return
	 */
	public static AttributeDescriptors getExogenousAttributeDescriptors(final Individuals source, final Integer limit) {
		AttributeDescriptors result;

		//
		result = new AttributeDescriptors(20);

		//
		if (source != null) {
			//
			List<Attribute> attributes = new ArrayList<Attribute>(source.size() * 7);

			//
			int index = 0;
			Iterator<Individual> iterator = source.iterator();
			while ((iterator.hasNext()) && ((limit == null) || (index < limit))) {
				//
				Individual individual = iterator.next();

				//
				attributes.addAll(individual.attributes().toList());

				//
				index += 1;
			}

			//
			AttributeDescriptors descriptors = getExogenousAttributeDescriptors(attributes, source.size(), AttributeDescriptor.Scope.INDIVIDUALS, null);

			//
			result.addAll(descriptors);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param maxCount
	 * @param scope
	 * @param optionalRelationName
	 * @return
	 */
	public static AttributeDescriptors getExogenousAttributeDescriptors(final List<Attribute> source, final long maxCount,
			final AttributeDescriptor.Scope scope, final String optionalRelationName) {
		AttributeDescriptors result;

		//
		result = new AttributeDescriptors(20);

		//
		if (source != null) {
			//
			Hashtable<String, AttributeDescriptor> descriptors = new Hashtable<String, AttributeDescriptor>();

			//
			for (Attribute attribute : source) {
				//
				AttributeDescriptor descriptor = descriptors.get(attribute.getLabel());

				//
				if (descriptor == null) {
					//
					if (scope == AttributeDescriptor.Scope.RELATION) {
						//
						descriptor = new AttributeDescriptor(optionalRelationName, attribute.getLabel());

					} else {
						//
						descriptor = new AttributeDescriptor(scope, attribute.getLabel());
					}

					//
					descriptor.setMax(maxCount);
				}

				//
				descriptor.incCountOfSet();

				//
				if (StringUtils.isBlank(attribute.getValue())) {
					descriptor.incCountOfBlank();
				}

				//
				descriptors.put(attribute.getLabel(), descriptor);
			}

			//
			for (AttributeDescriptor descriptor : descriptors.values()) {

				result.add(descriptor);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param limit
	 * @return
	 */
	public static AttributeDescriptors getExogenousAttributeDescriptors(final Net source, final Integer limit) {
		AttributeDescriptors result;

		//
		result = new AttributeDescriptors(20);

		//
		if (source != null) {
			//
			{
				//
				HashSet<String> labels = new HashSet<String>();

				//
				for (Attribute attribute : source.attributes()) {
					labels.add(attribute.getLabel());
				}

				//
				for (String label : labels) {
					AttributeDescriptor descriptor = new AttributeDescriptor(AttributeDescriptor.Scope.CORPUS, label);
					descriptor.incCountOfSet();
					descriptor.setMax(1);
					result.add(descriptor);
				}
			}

			//
			result.addAll(getExogenousAttributeDescriptors(source.individuals(), limit));

			//
			result.addAll(getExogenousAttributeDescriptors(source.families(), limit));

			//
			result.addAll(getExogenousAttributeDescriptors(source.relations(), limit));

			//
			result.addAll(getExogenousAttributeDescriptors(source.relations().getActors()));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param source
	 * @param limit
	 * @return
	 */
	public static AttributeDescriptors getExogenousAttributeDescriptors(final Net net, final Segmentation source, final Integer limit) {
		AttributeDescriptors result;

		//
		result = new AttributeDescriptors(20);

		//
		if (source != null) {
			//
			result.addAll(getExogenousAttributeDescriptors(net.attributes().toList(), source.size(), AttributeDescriptor.Scope.CORPUS, null));

			//
			result.addAll(getExogenousAttributeDescriptors(source, limit));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param limit
	 * @return
	 */
	public static AttributeDescriptors getExogenousAttributeDescriptors(final Relations source, final Integer limit) {
		AttributeDescriptors result;

		//
		result = new AttributeDescriptors(20);

		//
		if (source != null) {
			//
			for (RelationModel model : source.getRelationModels()) {
				//
				Relations relations = source.getByModel(model);

				//
				result.addAll(getExogenousAttributeDescriptors(relations, model.getName(), limit));
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param relationName
	 * @param limit
	 * @return
	 */
	public static AttributeDescriptors getExogenousAttributeDescriptors(final Relations source, final String relationName, final Integer limit) {
		AttributeDescriptors result;

		//
		result = new AttributeDescriptors(20);

		//
		if (source != null) {
			//
			List<Attribute> attributes = new ArrayList<Attribute>(source.size() * 7);

			//
			int index = 0;
			Iterator<Relation> iterator = source.iterator();
			while ((iterator.hasNext()) && ((limit == null) || (index < limit))) {
				//
				Relation relation = iterator.next();

				//
				attributes.addAll(relation.attributes().toList());

				//
				index += 1;
			}

			//
			AttributeDescriptors descriptors = getExogenousAttributeDescriptors(attributes, source.size(), AttributeDescriptor.Scope.RELATION, relationName);

			//
			result.addAll(descriptors);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param limit
	 * @return
	 */
	public static AttributeDescriptors getExogenousAttributeDescriptors(final Segmentation source, final Integer limit) {
		AttributeDescriptors result;

		//
		result = new AttributeDescriptors(20);

		//
		if (source != null) {
			//
			result.addAll(getExogenousAttributeDescriptors(source.getCurrentIndividuals(), limit));

			//
			result.addAll(getExogenousAttributeDescriptors(source.getCurrentFamilies(), limit));

			//
			result.addAll(getExogenousAttributeDescriptors(source.getCurrentRelations(), limit));

			//
			result.addAll(getExogenousAttributeDescriptors(source.getCurrentRelations().getActors()));
		}

		//
		return result;
	}

	/**
	 * Not finished yet.
	 * 
	 * 
	 * @param source
	 * 
	 * @return
	 */
	public static AttributeDescriptors getExogenousAttributeDescriptorsAndValues(final Individuals source) {
		AttributeDescriptors result;

		//
		if (source == null) {
			//
			result = new AttributeDescriptors();

		} else {
			//
			Hashtable<String, AttributeDescriptor> descriptors = new Hashtable<String, AttributeDescriptor>();

			//
			for (Individual individual : source) {
				//
				for (Attribute attribute : individual.attributes()) {
					//
					AttributeDescriptor descriptor = descriptors.get(attribute.getLabel());

					//
					if (descriptor == null) {
						//
						descriptor = new AttributeDescriptor(AttributeDescriptor.Scope.INDIVIDUALS, attribute.getLabel());

						//
						descriptor.setMax(source.size());
					}

					//
					descriptor.incCountOfSet();

					//
					if (StringUtils.isBlank(attribute.getValue())) {
						descriptor.incCountOfBlank();
					}

					// TODO work about value statistics.

					//
					descriptors.put(attribute.getLabel(), descriptor);
				}
			}

			//
			result = new AttributeDescriptors(descriptors.size());
			result.addAll(descriptors.values());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param limit
	 * @return
	 */
	public static List<String> getExogenousAttributeLabels(final Net source, final Integer limit) {
		List<String> result;

		//
		result = new ArrayList<String>(20);

		//
		if (source != null) {

			//
			HashSet<String> buffer = new HashSet<String>();

			//
			{
				for (Attribute attribute : source.attributes()) {
					buffer.add(attribute.getLabel());
				}
			}

			//
			{
				int index = 0;
				Iterator<Individual> iterator = source.individuals().iterator();
				while ((iterator.hasNext()) && ((limit == null) || (index < limit))) {
					Individual individual = iterator.next();
					for (Attribute attribute : individual.attributes()) {
						buffer.add(attribute.getLabel());
					}
					index += 1;
				}
			}

			//
			{
				int index = 0;
				Iterator<Family> iterator = source.families().iterator();
				while ((iterator.hasNext()) && ((limit == null) || (index < limit))) {
					Family family = iterator.next();
					for (Attribute attribute : family.attributes()) {
						buffer.add(attribute.getLabel());
					}
					index += 1;
				}
			}

			//
			{
				int index = 0;
				Iterator<Relation> iterator = source.relations().iterator();
				while ((iterator.hasNext()) && ((limit == null) || (index < limit))) {
					Relation relation = iterator.next();
					for (Attribute attribute : relation.attributes()) {
						buffer.add(attribute.getLabel());
					}
					index += 1;
				}
			}

			//
			{
				int index = 0;
				Iterator<Actor> iterator = source.relations().getActors().iterator();
				while ((iterator.hasNext()) && ((limit == null) || (index < limit))) {
					Actor actor = iterator.next();
					for (Attribute attribute : actor.attributes()) {
						buffer.add(attribute.getLabel());
					}
					index += 1;
				}
			}

			//
			for (String string : buffer) {
				result.add(string);
			}

			//
			Collections.sort(result);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param limit
	 * @return
	 */
	public static AttributeValueDescriptors getExogenousAttributeValueDescriptors(final Actors source, final String pattern) {
		AttributeValueDescriptors result;

		//
		result = new AttributeValueDescriptors();

		//
		if (source != null) {
			//
			for (Actor actor : source) {
				//
				result.putAll(getExogenousAttributeValueDescriptors(actor.attributes(), pattern));
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param limit
	 * @return
	 */
	public static AttributeValueDescriptors getExogenousAttributeValueDescriptors(final Attributes source, final String pattern) {
		AttributeValueDescriptors result;

		//
		result = new AttributeValueDescriptors();

		//
		if (source != null) {
			//
			for (Attribute attribute : source) {

				if ((pattern == null) || (attribute.getLabel().matches(pattern))) {

					result.put(attribute.getValue());
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param limit
	 * @return
	 */
	public static AttributeValueDescriptors getExogenousAttributeValueDescriptors(final Families source, final String pattern) {
		AttributeValueDescriptors result;

		//
		result = new AttributeValueDescriptors();

		//
		if (source != null) {
			//
			for (Family family : source) {
				//
				result.putAll(getExogenousAttributeValueDescriptors(family.attributes(), pattern));
			}
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @param source
	 * @param limit
	 * @return
	 */
	public static AttributeValueDescriptors getAttributeValueDescriptors(final Partition<Individual> partition) {
		AttributeValueDescriptors result;

		//
		result = new AttributeValueDescriptors();

		//
		if (partition != null) {
			//
			for (Individual individual : partition.getItems()) {
				
				result.put(partition.getValue(individual).toString());
				
			}
		}

		//
		return result;
	}


	/**
	 * 
	 * @param source
	 * @param limit
	 * @return
	 */
	public static AttributeValueDescriptors getExogenousAttributeValueDescriptors(final Individuals source, final String pattern) {
		AttributeValueDescriptors result;

		//
		result = new AttributeValueDescriptors();

		//
		if (source != null) {
			//
			for (Individual individual : source) {
				//
				result.putAll(getExogenousAttributeValueDescriptors(individual.attributes(), pattern));
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param limit
	 * @return
	 */
	public static AttributeValueDescriptors getExogenousAttributeValueDescriptors(final Net source) {
		AttributeValueDescriptors result;

		result = getExogenousAttributeValueDescriptors(source, null);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param limit
	 * @return
	 */
	public static AttributeValueDescriptors getExogenousAttributeValueDescriptors(final Net source, final AttributeType scope, final String pattern) {
		AttributeValueDescriptors result;

		//
		result = new AttributeValueDescriptors(50);

		//
		if ((source != null) && (scope != null)) {
			//
			if (scope.isCorpus()) {
				result.putAll(getExogenousAttributeValueDescriptors(source.attributes(), pattern));
			}

			//
			if (scope.isIndividuals()) {
				result.putAll(getExogenousAttributeValueDescriptors(source.individuals(), pattern));
			}

			//
			if (scope.isFamilies()) {
				result.putAll(getExogenousAttributeValueDescriptors(source.families(), pattern));
			}

			//
			if (scope.isRelation()) {
				result.putAll(getExogenousAttributeValueDescriptors(source.relations().getByModelName(scope.getRelationModelName()), pattern));
			}

			//
			if (scope.isActors()) {
				result.putAll(getExogenousAttributeValueDescriptors(source.relations().getActors(), pattern));
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param scopes
	 * @param pattern
	 * @return
	 */
	public static AttributeValueDescriptors getExogenousAttributeValueDescriptors(final Net source, final AttributeTypes scopes, final String pattern) {
		AttributeValueDescriptors result;

		result = new AttributeValueDescriptors();

		for (AttributeType scope : scopes) {
			result.putAll(getExogenousAttributeValueDescriptors(source, scope, pattern));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param limit
	 * @return
	 */
	public static AttributeValueDescriptors getExogenousAttributeValueDescriptors(final Net source, final String pattern) {
		AttributeValueDescriptors result;

		//
		result = new AttributeValueDescriptors();

		//
		if (source != null) {
			//
			result.putAll(getExogenousAttributeValueDescriptors(source.attributes(), pattern));

			//
			result.putAll(getExogenousAttributeValueDescriptors(source.individuals(), pattern));

			//
			result.putAll(getExogenousAttributeValueDescriptors(source.families(), pattern));

			//
			result.putAll(getExogenousAttributeValueDescriptors(source.relations(), pattern));

			//
			result.putAll(getExogenousAttributeValueDescriptors(source.relations().getActors(), pattern));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param limit
	 * @return
	 */
	public static AttributeValueDescriptors getExogenousAttributeValueDescriptors(final Relations source, final String pattern) {
		AttributeValueDescriptors result;

		//
		result = new AttributeValueDescriptors();

		//
		if (source != null) {
			//
			for (Relation relation : source) {
				//
				result.putAll(getExogenousAttributeValueDescriptors(relation.attributes(), pattern));
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param limit
	 * @return
	 */
	public static AttributeValueDescriptors getExogenousAttributeValueDescriptors(final Segmentation source) {
		AttributeValueDescriptors result;

		//
		result = getExogenousAttributeValueDescriptors(source, null);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param limit
	 * @return
	 */
	public static AttributeValueDescriptors getExogenousAttributeValueDescriptors(final Segmentation source, final String pattern) {
		AttributeValueDescriptors result;

		//
		result = new AttributeValueDescriptors();

		//
		if (source != null) {
			//
			result.putAll(getExogenousAttributeValueDescriptors(source.getCurrentIndividuals(), pattern));

			//
			result.putAll(getExogenousAttributeValueDescriptors(source.getCurrentFamilies(), pattern));

			//
			result.putAll(getExogenousAttributeValueDescriptors(source.getCurrentRelations(), pattern));

			//
			result.putAll(getExogenousAttributeValueDescriptors(source.getCurrentRelations().getActors(), pattern));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param limit
	 * @return
	 */
	public static StringSet getExogenousAttributeValues(final Net source, final AttributeTypes types, final String pattern) {
		StringSet result;

		//
		result = new StringSet();

		//
		if (source != null) {
			//
			for (AttributeType type : types) {
				//
				if (AttributeType.isIndividuals(type)) {
					//
					for (Individual individual : source.individuals()) {
						//
						result.put(individual.attributes().searchByLabel(pattern).getValues());
					}
				} else if (AttributeType.isFamilies(type)) {
					//
					for (Family family : source.families()) {
						//
						result.put(family.attributes().searchByLabel(pattern).getValues());
					}
				} else if (AttributeType.isActors(type)) {
					//
					for (Actor actor : source.relations().getActors()) {
						//
						result.put(actor.attributes().searchByLabel(pattern).getValues());
					}
				} else {
					//
					for (Relation relation : source.relations().getByModelName(type.getRelationModelName())) {
						//
						result.put(relation.attributes().searchByLabel(pattern).getValues());
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @return
	 */
	public static long lowerCaseValue(final Attribute target) {
		long result;

		if (target == null) {
			//
			result = 0;

		} else if (StringUtils.isEmpty(target.getValue())) {
			//
			result = 0;

		} else {
			//
			target.setValue(StringUtils.lowerCase(target.getValue()));
			result = 1;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @return
	 */
	public static long reduceDateValue(final Attribute target) {
		long result;

		if (target == null) {
			//
			result = 0;

		} else if (StringUtils.isBlank(target.getValue())) {
			//
			result = 0;

		} else {
			//
			Matcher matcher = YEAR_PATTERN.matcher(target.getValue());
			if ((matcher.find()) && (matcher.groupCount() > 0)) {
				//
				target.setValue(matcher.group(1));
				result = 1;

			} else {
				//
				result = 0;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static long removeAllAttributes(final Net net) {
		long result;

		result = removeAllAttributes(net, Scope.ALL, null);

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param scope
	 * @param optionalRelationModelName
	 * @return
	 */
	public static long removeAllAttributes(final Net net, final Scope scope, final String optionalRelationModelName) {
		int result;

		if (net == null) {
			//
			throw new IllegalArgumentException("net parameter is null");

		} else if (scope == null) {
			//
			throw new IllegalArgumentException("target parameter is null");

		} else {
			//
			result = 0;

			//
			if ((scope == Scope.ALL) || (scope == Scope.CORPUS)) {
				result += net.attributes().size();
				net.attributes().clear();
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.INDIVIDUALS)) {
				for (Individual individual : net.individuals()) {
					result += individual.attributes().size();
					individual.attributes().clear();
				}
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.FAMILIES)) {
				for (Family family : net.families()) {
					result += family.attributes().size();
					family.attributes().clear();
				}
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.RELATIONS)) {
				for (Relation relation : net.relations()) {
					result += relation.attributes().size();
					relation.attributes().clear();
				}
			}

			//
			if (scope == Scope.RELATION) {
				Relations relations = net.relations().getByModelName(optionalRelationModelName);
				for (Relation relation : relations) {
					result += relation.attributes().size();
					relation.attributes().clear();
				}
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.ACTORS)) {
				for (Actor actor : net.relations().getActors()) {
					result += actor.attributes().size();
					actor.attributes().clear();
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static long removeAllAttributesExceptCorpus(final Net net) {
		long result;

		result = removeAllAttributes(net, Scope.INDIVIDUALS, null);
		result += removeAllAttributes(net, Scope.FAMILIES, null);
		result += removeAllAttributes(net, Scope.RELATIONS, null);
		result += removeAllAttributes(net, Scope.ACTORS, null);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param label
	 * @param type
	 * @return
	 */
	public static int removeAttribute(final Attributes source, final String label, final EmptyType type) {
		int result;

		if (label == null) {
			//
			throw new IllegalArgumentException("label parameter is null");

		} else if (type == null) {
			//
			throw new IllegalArgumentException("type parameter is null");

		} else if (source == null) {
			//
			result = 0;

		} else {
			//
			Attribute target = source.get(label);
			switch (type) {
				case VOID:
					if (target == null) {
						result = 0;
					} else {
						source.remove(label);
						result = 1;
					}
				break;

				case BLANK:
					if (target == null) {
						result = 0;
					} else {
						target.setValue("");
						result = 1;
					}
				break;

				case FORCED_BLANK:
					source.put(label, "");
					result = 1;
				break;

				default:
					result = 0;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param scope
	 * @param optionalRelationModelName
	 * @param label
	 * @param type
	 * @return
	 */
	public static int removeAttribute(final Net net, final Scope scope, final String optionalRelationModelName, final String label, final EmptyType type) {
		int result;

		logger.debug("removeAttribute [net={}][scope={}][optionalName={}][label={}][type={}]", net, scope, optionalRelationModelName, label, type);

		if (net == null) {
			//
			throw new IllegalArgumentException("net parameter is null");

		} else if (scope == null) {
			//
			throw new IllegalArgumentException("target parameter is null");

		} else if (label == null) {
			//
			throw new IllegalArgumentException("label parameter is null");

		} else if (type == null) {
			//
			throw new IllegalArgumentException("type parameter is null");

		} else {
			//
			result = 0;

			//
			if ((scope == Scope.ALL) || (scope == Scope.CORPUS)) {
				result += removeAttribute(net.attributes(), label, type);
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.INDIVIDUALS)) {
				for (Individual individual : net.individuals()) {
					result += removeAttribute(individual.attributes(), label, type);
				}
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.FAMILIES)) {
				for (Family family : net.families()) {
					result += removeAttribute(family.attributes(), label, type);
				}
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.RELATIONS)) {
				for (Relation relation : net.relations()) {
					result += removeAttribute(relation.attributes(), label, type);
				}
			}

			//
			if (scope == Scope.RELATION) {
				Relations relations = net.relations().getByModelName(optionalRelationModelName);
				for (Relation relation : relations) {
					result += removeAttribute(relation.attributes(), label, type);
				}
			}

			if ((scope == Scope.ALL) || (scope == Scope.ACTORS)) {
				for (Actor actor : net.relations().getActors()) {
					result += removeAttribute(actor.attributes(), label, type);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param label
	 * @param newLabel
	 * @param caseOption
	 * @return
	 */
	public static long renameAttribute(final Attributes source, final String label, final String newLabel, final CaseOption caseOption) {
		long result;

		if (source == null) {
			//
			result = 0;

		} else if (label == null) {
			//
			throw new IllegalArgumentException("label parameter is null");

		} else if (StringUtils.isBlank(newLabel)) {
			//
			throw new IllegalArgumentException("newLabel parameter is blank.");

		} else {
			//
			Attribute target;
			if ((caseOption == null) || (caseOption == CaseOption.CASE_SENSITIVE)) {
				//
				target = source.get(StringUtils.trim(label));

			} else {
				//
				target = source.getIgnoreCase(StringUtils.trim(label));
			}

			if (target == null) {
				//
				result = 0;

			} else {
				//
				source.remove(target.getLabel());
				target.setLabel(newLabel);
				source.put(target);
				result = 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param criteria
	 * @return
	 */
	public static long renameAttribute(final Net net, final AttributeRenameCriteria criteria) {
		long result;

		if ((net == null) || (AttributeRenameCriteria.isNotValid(criteria))) {
			//
			result = 0;

		} else {
			//
			result = renameAttribute(net, criteria.getScope(), criteria.getOptionalRelationName(), criteria.getLabel(), criteria.getNewLabel(),
					criteria.getCaseOption());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param scope
	 * @param optionalRelationModelName
	 * @param label
	 * @param newLabel
	 * @param caseOption
	 * @return
	 */
	public static long renameAttribute(final Net net, final Scope scope, final String optionalRelationModelName, final String label, final String newLabel,
			final CaseOption caseOption) {
		long result;

		logger.debug("renameAttribute [net={}][scope={}][optionalName={}][label={}][newLabel={}][caseOption={}]", net, scope, optionalRelationModelName, label,
				newLabel, caseOption);

		if (net == null) {
			//
			throw new IllegalArgumentException("net parameter is null");

		} else if (scope == null) {
			//
			throw new IllegalArgumentException("scope parameter is null");

		} else if (label == null) {
			//
			throw new IllegalArgumentException("label parameter is null");

		} else if (newLabel == null) {
			//
			throw new IllegalArgumentException("newLabel parameter is null");

		} else {
			//
			result = 0;

			//
			if ((scope == Scope.ALL) || (scope == Scope.CORPUS)) {
				//
				result += renameAttribute(net.attributes(), label, newLabel, caseOption);
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.INDIVIDUALS)) {
				//
				for (Individual individual : net.individuals()) {
					//
					result += renameAttribute(individual.attributes(), label, newLabel, caseOption);
				}
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.FAMILIES)) {
				//
				for (Family family : net.families()) {

					result += renameAttribute(family.attributes(), label, newLabel, caseOption);
				}
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.RELATIONS)) {
				//
				for (Relation relation : net.relations()) {

					result += renameAttribute(relation.attributes(), label, newLabel, caseOption);
				}

			} else if (scope == Scope.RELATION) {
				//
				Relations relations = net.relations().getByModelName(optionalRelationModelName);

				//
				for (Relation relation : relations) {

					result += renameAttribute(relation.attributes(), label, newLabel, caseOption);
				}
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.ACTORS)) {
				//
				for (Actor actor : net.relations().getActors()) {

					result += renameAttribute(actor.attributes(), label, newLabel, caseOption);
				}
			}
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @param net
	 * @param segmentation
	 * @param criteria
	 * @return
	 * @throws PuckException 
	 */
	public static long setEndogenousAttribute(final Net net, final Segmentation segmentation, final PartitionCriteria criteria) throws PuckException {
		long result;

		result = 0;
		
		Partition<Individual> partition = PartitionMaker.create(net,criteria); 
		
		for (Individual individual : segmentation.getCurrentIndividuals()) {
			
			Value value = partition.getValue(individual);
			
			if (value != null) {
				
				individual.setAttribute(criteria.getLabel(),value.toString());
				result++;

			}
		}
		
		//
		return result;
	}


	/**
	 * 
	 * @param net
	 * @param segmentation
	 * @param criteria
	 * @return
	 */
	public static long renameAttribute(final Net net, final Segmentation segmentation, final AttributeRenameCriteria criteria) {
		long result;

		if ((net == null) || (AttributeRenameCriteria.isNotValid(criteria))) {
			//
			result = 0;

		} else {
			//
			result = renameAttribute(net, segmentation, criteria.getScope(), criteria.getOptionalRelationName(), criteria.getLabel(), criteria.getNewLabel(),
					criteria.getCaseOption());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param segmentation
	 * @param scope
	 * @param optionalRelationModelName
	 * @param label
	 * @param newLabel
	 * @param caseOption
	 * @return
	 */
	public static long renameAttribute(final Net net, final Segmentation segmentation, final Scope scope, final String optionalRelationModelName,
			final String label, final String newLabel, final CaseOption caseOption) {
		long result;

		logger.debug("renameAttribute [net={}][scope={}][optionalName={}][label={}][newLabel={}][caseOption={}]", net, scope, optionalRelationModelName, label,
				newLabel, caseOption);

		if (net == null) {
			//
			throw new IllegalArgumentException("net parameter is null");

		} else if (scope == null) {
			//
			throw new IllegalArgumentException("scope parameter is null");

		} else if (label == null) {
			//
			throw new IllegalArgumentException("label parameter is null");

		} else if (newLabel == null) {
			//
			throw new IllegalArgumentException("newLabel parameter is null");

		} else if (segmentation == null) {
			//
			result = renameAttribute(net, scope, optionalRelationModelName, label, newLabel, caseOption);

		} else {
			//
			result = 0;

			//
			if ((scope == Scope.ALL) || (scope == Scope.CORPUS)) {
				//
				result += renameAttribute(net.attributes(), label, newLabel, caseOption);
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.INDIVIDUALS)) {
				//
				for (Individual individual : segmentation.getCurrentIndividuals()) {
					//
					result += renameAttribute(individual.attributes(), label, newLabel, caseOption);
				}
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.FAMILIES)) {
				//
				for (Family family : segmentation.getCurrentFamilies()) {

					result += renameAttribute(family.attributes(), label, newLabel, caseOption);
				}
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.RELATIONS)) {
				//
				for (Relation relation : segmentation.getCurrentRelations()) {

					result += renameAttribute(relation.attributes(), label, newLabel, caseOption);
				}

			} else if (scope == Scope.RELATION) {
				//
				Relations relations = segmentation.getCurrentRelations().getByModelName(optionalRelationModelName);

				//
				for (Relation relation : relations) {

					result += renameAttribute(relation.attributes(), label, newLabel, caseOption);
				}
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.ACTORS)) {
				//
				for (Actor actor : segmentation.getCurrentRelations().getActors()) {

					result += renameAttribute(actor.attributes(), label, newLabel, caseOption);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param label
	 * @param newLabel
	 * @param caseOption
	 * @return
	 */
	public static long replaceAttributeValue(final Attributes source, final String label, final String targetValue, final String newValue,
			final CaseOption caseOption) {
		long result;

		if (source == null) {
			//
			result = 0;

		} else if (label == null) {
			//
			throw new IllegalArgumentException("label parameter is null");

		} else {
			//
			Attribute target = source.get(StringUtils.trim(label));

			if (target == null) {
				//
				result = 0;

			} else {
				//
				if ((caseOption == null) || (caseOption == CaseOption.CASE_SENSITIVE)) {
					//
					if (StringUtils.equals(target.getValue(), targetValue)) {
						//
						target.setValue(newValue);
						result = 1;

					} else {
						//
						result = 0;

					}

				} else {
					//
					if (StringUtils.equalsIgnoreCase(target.getValue(), targetValue)) {
						//
						target.setValue(newValue);
						result = 1;

					} else {
						//
						result = 0;

					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param criteria
	 * @return
	 */
	public static long replaceAttributeValue(final Net net, final AttributeReplaceValueCriteria criteria) {
		long result;

		result = replaceAttributeValue(net, new Segmentation(net), criteria);

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param scope
	 * @param optionalRelationModelName
	 * @param label
	 * @param targetValue
	 * @param newValue
	 * @param caseOption
	 * @return
	 */
	public static long replaceAttributeValue(final Net net, final Scope scope, final String optionalRelationModelName, final String label,
			final String targetValue, final String newValue, final CaseOption caseOption) {
		long result;

		result = replaceAttributeValue(net, new Segmentation(net), scope, optionalRelationModelName, label, targetValue, newValue, caseOption);

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param criteria
	 * @return
	 */
	public static long replaceAttributeValue(final Net net, final Segmentation segmentation, final AttributeReplaceValueCriteria criteria) {
		long result;

		if ((net == null) || (AttributeReplaceValueCriteria.isNotValid(criteria))) {
			//
			result = 0;

		} else {
			//
			result = replaceAttributeValue(net, segmentation, criteria.getScope(), criteria.getOptionalRelationName(), criteria.getLabel(),
					criteria.getTargetValue(), criteria.getNewValue(), criteria.getCaseOption());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param segmentation
	 * @param scope
	 * @param optionalRelationModelName
	 * @param label
	 * @param targetValue
	 * @param newValue
	 * @param caseOption
	 * @return
	 */
	public static long replaceAttributeValue(final Net net, final Segmentation segmentation, final Scope scope, final String optionalRelationModelName,
			final String label, final String targetValue, final String newValue, final CaseOption caseOption) {
		long result;

		logger.debug("renameAttribute [net={}][scope={}][optionalName={}][label={}][value={}][newValue={}][caseOption={}]", net, scope,
				optionalRelationModelName, label, targetValue, newValue, caseOption);

		if (net == null) {
			//
			throw new IllegalArgumentException("net parameter is null");

		} else if (scope == null) {
			//
			throw new IllegalArgumentException("scope parameter is null");

		} else if (label == null) {
			//
			throw new IllegalArgumentException("label parameter is null");

		} else if (segmentation == null) {
			//
			result = replaceAttributeValue(net, scope, optionalRelationModelName, label, targetValue, newValue, caseOption);

		} else {
			//
			result = 0;

			//
			if ((scope == Scope.ALL) || (scope == Scope.CORPUS)) {
				//
				result += replaceAttributeValue(net.attributes(), label, targetValue, newValue, caseOption);
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.INDIVIDUALS)) {
				//
				for (Individual individual : segmentation.getCurrentIndividuals()) {
					//
					result += replaceAttributeValue(individual.attributes(), label, targetValue, newValue, caseOption);
				}
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.FAMILIES)) {
				//
				for (Family family : segmentation.getCurrentFamilies()) {

					result += replaceAttributeValue(family.attributes(), label, targetValue, newValue, caseOption);
				}
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.RELATIONS)) {
				//
				for (Relation relation : segmentation.getCurrentRelations()) {

					result += replaceAttributeValue(relation.attributes(), label, targetValue, newValue, caseOption);
				}

			} else if (scope == Scope.RELATION) {
				//
				Relations relations = segmentation.getCurrentRelations().getByModelName(optionalRelationModelName);

				//
				for (Relation relation : relations) {

					result += replaceAttributeValue(relation.attributes(), label, targetValue, newValue, caseOption);
				}
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.ACTORS)) {
				//
				for (Actor actor : segmentation.getCurrentRelations().getActors()) {

					result += replaceAttributeValue(actor.attributes(), label, targetValue, newValue, caseOption);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param label
	 * @param value
	 * @return
	 */
	public static long setAttributeValue(final Attributes source, final String label, final String value) {
		long result;

		if (source == null) {
			//
			result = 0;

		} else if (label == null) {
			//
			throw new IllegalArgumentException("label parameter is null");

		} else {
			//
			Attribute target = source.get(StringUtils.trim(label));

			if (target == null) {
				//
				source.add(new Attribute(label, value));
				result = 1;

			} else {
				//
				target.setValue(value);
				result = 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param criteria
	 * @return
	 */
	public static long setAttributeValue(final Net net, final AttributeSetValueCriteria criteria) {
		long result;

		result = setAttributeValue(net, new Segmentation(net), criteria);

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param scope
	 * @param optionalRelationModelName
	 * @param label
	 * @param targetValue
	 * @return
	 */
	public static long setAttributeValue(final Net net, final Scope scope, final String optionalRelationModelName, final String label, final String targetValue) {
		long result;

		result = setAttributeValue(net, new Segmentation(net), scope, optionalRelationModelName, label, targetValue);

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param segmentation
	 * @param criteria
	 * @return
	 */
	public static long setAttributeValue(final Net net, final Segmentation segmentation, final AttributeSetValueCriteria criteria) {
		long result;

		if ((net == null) || (AttributeSetValueCriteria.isNotValid(criteria))) {
			//
			result = 0;

		} else {
			//
			result = setAttributeValue(net, segmentation, criteria.getScope(), criteria.getOptionalRelationName(), criteria.getLabel(),
					criteria.getTargetValue());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param segmentation
	 * @param scope
	 * @param optionalRelationModelName
	 * @param label
	 * @param targetValue
	 * @return
	 */
	public static long setAttributeValue(final Net net, final Segmentation segmentation, final Scope scope, final String optionalRelationModelName,
			final String label, final String targetValue) {
		long result;

		logger.debug("setAttribute [net={}][scope={}][optionalName={}][label={}][value={}]", net, scope, optionalRelationModelName, label, targetValue);

		if (net == null) {
			//
			throw new IllegalArgumentException("net parameter is null");

		} else if (scope == null) {
			//
			throw new IllegalArgumentException("scope parameter is null");

		} else if (label == null) {
			//
			throw new IllegalArgumentException("label parameter is null");

		} else if (segmentation == null) {
			//
			result = setAttributeValue(net, scope, optionalRelationModelName, label, targetValue);

		} else {
			//
			result = 0;

			//
			if ((scope == Scope.ALL) || (scope == Scope.CORPUS)) {
				//
				result += setAttributeValue(net.attributes(), label, targetValue);
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.INDIVIDUALS)) {
				//
				for (Individual individual : segmentation.getCurrentIndividuals()) {
					//
					result += setAttributeValue(individual.attributes(), label, targetValue);
				}
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.FAMILIES)) {
				//
				for (Family family : segmentation.getCurrentFamilies()) {

					result += setAttributeValue(family.attributes(), label, targetValue);
				}
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.RELATIONS)) {
				//
				for (Relation relation : segmentation.getCurrentRelations()) {

					result += setAttributeValue(relation.attributes(), label, targetValue);
				}

			} else if (scope == Scope.RELATION) {
				//
				Relations relations = segmentation.getCurrentRelations().getByModelName(optionalRelationModelName);

				//
				for (Relation relation : relations) {

					result += setAttributeValue(relation.attributes(), label, targetValue);
				}
			}

			//
			if ((scope == Scope.ALL) || (scope == Scope.ACTORS)) {
				//
				for (Actor actor : segmentation.getCurrentRelations().getActors()) {

					result += setAttributeValue(actor.attributes(), label, targetValue);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @return
	 */
	public static long trimValue(final Attribute target) {
		long result;

		if (target == null) {
			//
			result = 0;

		} else if (StringUtils.isEmpty(target.getValue())) {
			//
			result = 0;

		} else {
			//
			target.setValue(StringUtils.trim(target.getValue()));
			result = 1;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @return
	 */
	public static long upperCaseValue(final Attribute target) {
		long result;

		if (target == null) {
			//
			result = 0;

		} else if (StringUtils.isEmpty(target.getValue())) {
			//
			result = 0;

		} else {
			//
			target.setValue(StringUtils.upperCase(target.getValue()));
			result = 1;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param sourceLabel
	 * @param targetLabel
	 * @param valuePrefix
	 * @return
	 */
	public static int valuateExogenousAttribute(final Individuals source, final String sourceLabel, final String targetLabel, final String valuePrefix, final Geography geography) {
		int result;

		logger.debug("valuateExogenousAttribute [source={}][sourceLabel={}][targetLabel={}][valuePrefix={}]", source, sourceLabel, targetLabel, valuePrefix);

		if (source == null) {
			//
			throw new IllegalArgumentException("source parameter is null");

		} else if (sourceLabel == null) {
			//
			throw new IllegalArgumentException("source label parameter is null");

		} else if (StringUtils.isBlank(targetLabel)) {
			//
			throw new IllegalArgumentException("target label parameter is null");

		} else {
			//
			result = 0;
			NumberedValues values = IndividualValuator.get(source, sourceLabel, geography);
			for (Individual individual : source) {
				//
				Value value = values.get(individual.getId());

				//
				String attributeValue;
				if (value == null) {
					attributeValue = "";
				} else {
					attributeValue = value.toString();
				}

				//
				String prefix;
				if (valuePrefix == null) {
					prefix = "";
				} else {
					prefix = valuePrefix;
				}

				//
				individual.attributes().put(targetLabel, prefix + attributeValue);
				result += 1;
			}
		}

		//
		return result;
	}
}
