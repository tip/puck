package org.tip.puckgui.views;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.AbstractListModel;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;

/**
 * 
 * @author TIP
 */
public class DoubleListSelector extends JPanel {

	private static final long serialVersionUID = -5038216362421472698L;

	/**
	 * Create the panel.
	 */
	public DoubleListSelector() {
		setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JPanel panelLeft = new JPanel();
		panelLeft.setMinimumSize(new Dimension(200, 100));
		add(panelLeft);
		panelLeft.setLayout(new BoxLayout(panelLeft, BoxLayout.Y_AXIS));

		JLabel lblSource = new JLabel("SOURCE");
		panelLeft.add(lblSource);

		JList list = new JList();
		list.setModel(new AbstractListModel() {
			String[] values = new String[] { "ALPHA", "BRAVO", "CHARLIE", "DELTA" };

			@Override
			public Object getElementAt(final int index) {
				return values[index];
			}

			@Override
			public int getSize() {
				return values.length;
			}
		});
		panelLeft.add(list);

		JPanel panelCenter = new JPanel();
		add(panelCenter);
		panelCenter.setLayout(new BoxLayout(panelCenter, BoxLayout.Y_AXIS));

		JButton btnNewButton = new JButton("New button");
		panelCenter.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("New button");
		panelCenter.add(btnNewButton_1);

		JPanel panelRight = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panelRight.getLayout();
		flowLayout_1.setAlignment(FlowLayout.RIGHT);
		add(panelRight);

		JLabel lblTarget = new JLabel("TARGET");
		panelRight.add(lblTarget);

	}

}
