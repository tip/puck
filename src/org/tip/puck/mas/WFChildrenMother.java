package org.tip.puck.mas;

public class WFChildrenMother implements WeightFactor {
    private double wgtChildren0;
    private double wgtChildren1;
    private double wgtChildren2;
    private double wgtChildren3;
    private double wgtChildren4;
    
    public WFChildrenMother(double wgtChildren0, double wgtChildren1, double wgtChildren2, double wgtChildren3, double wgtChildren4) {
        this.wgtChildren0 = wgtChildren0;
        this.wgtChildren1 = wgtChildren1;
        this.wgtChildren2 = wgtChildren2;
        this.wgtChildren3 = wgtChildren3;
        this.wgtChildren4 = wgtChildren4;
    }
    
    public String factorName(){
    	return "childrenmother";
    }
    
    @Override
    public double factor(Agent mother, Agent father) {
    	int count = mother.children().size();
    	
        switch(count) {
        case 0:
        	return wgtChildren0;
        case 1:
        	return wgtChildren1;
        case 2:
        	return wgtChildren2;
        case 3:
        	return wgtChildren3;
        default:
        	return wgtChildren4;
        }
    }
    
    @Override
    public boolean appliesToMarriage() {return true;}
    
    @Override
    public boolean appliesToDivorce() {return true;}
}