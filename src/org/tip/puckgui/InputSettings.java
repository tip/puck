package org.tip.puckgui;

/**
 * 
 * @author TIP
 * 
 */
public class InputSettings {

	public enum CheckLevel {
		NONE,
		WARNING,
		ERROR
	};

	private CheckLevel sameSexSpouses;
	private CheckLevel femaleFathersOrMaleMothers;
	private CheckLevel parentChildMarriages;

	/**
	 * 
	 */
	public InputSettings() {
		this.sameSexSpouses = CheckLevel.NONE;
		this.femaleFathersOrMaleMothers = CheckLevel.NONE;
		this.parentChildMarriages = CheckLevel.NONE;
	}

	public CheckLevel getFemaleFathersOrMaleMothers() {
		return femaleFathersOrMaleMothers;
	}

	public CheckLevel getParentChildMarriages() {
		return parentChildMarriages;
	}

	public CheckLevel getSameSexSpouses() {
		return sameSexSpouses;
	}

	public void setFemaleFathersOrMaleMothers(final CheckLevel femaleFathersOrMaleMothers) {
		this.femaleFathersOrMaleMothers = femaleFathersOrMaleMothers;
	}

	public void setParentChildMarriages(final CheckLevel parentChildMarriages) {
		this.parentChildMarriages = parentChildMarriages;
	}

	public void setSameSexSpouses(final CheckLevel sameSexSpouses) {
		this.sameSexSpouses = sameSexSpouses;
	}
}
