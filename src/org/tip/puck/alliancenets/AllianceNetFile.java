package org.tip.puck.alliancenets;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Vector;


/**
 * @author Telmo Menezes
 *
 */
public class AllianceNetFile {
    
    public AllianceNet load(String filePath) {
        AllianceNet allianceNet = new AllianceNet(true);
        Vector<Group> groups = new Vector<Group>();
        
        try {
            // add nodes
            BufferedReader in = new BufferedReader(new FileReader(filePath));
            String line = in.readLine();
            String[] tokens = line.split("\t");
            for (int j = 0; j < Array.getLength(tokens); j++) {
                Group group = allianceNet.addNode();
                groups.add(group);
            }
            in.close();
            
            // add edges
            in = new BufferedReader(new FileReader(filePath));
            int i = 0;
            while ((line = in.readLine()) != null) {
                tokens = line.split("\t");
                for (int j = 0; j < groups.size(); j++) {
                    double val = Double.parseDouble(tokens[j]);
                    if (val > 0) {
                        allianceNet.addEdge(groups.get(i), groups.get(j), val);
                    }
                }
                i++;
            }
            in.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return allianceNet;
    }

    public void save(AllianceNet allianceNet, String filePath) {
    	try{ 
            FileWriter fstream = new FileWriter(filePath);
            BufferedWriter out = new BufferedWriter(fstream);
            
            for (Alliance edge : allianceNet.getEdges()) {
                out.write("" + edge.getOrigin().getId() + '\t' + edge.getTarget().getId() + '\n');
            }
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        System.out.println("testing MatrixFile");
        AllianceNetFile mf = new AllianceNetFile();
        AllianceNet allianceNet = mf.load("datAllLinks/Chimane_AGNATES.dat");
        System.out.println(allianceNet);
    }
}