package org.tip.puck.partitions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tip.puck.util.MathUtils;
import org.tip.puck.util.Value;

public class MultiPartition<E> {
	
	Integer sum;
	Map<Value,Integer> rowSums;
	Map<Value,Integer> colSums;
	private String label;
	
	Map<Value,Partition<E>> rows;
	List<Value> colValues;
	
	public MultiPartition (){
		rows = new HashMap<Value,Partition<E>>();
		colValues = new ArrayList<Value>();
		
		sum = 0;
		rowSums = new HashMap<Value,Integer>();
		colSums = new HashMap<Value,Integer>();
		
	}
	
	public void put (Partition<E> partition, Value value1){
		rows.put(value1, partition);
		for (Cluster<E> cluster : partition.getClusters()){
			Value colValue = cluster.getValue();
			if (!colValues.contains(colValue)){
				colValues.add(cluster.getValue());
			}
		}
	}
	

	public List<Value> rowValues(){
		List<Value> result;
		
	    result = new ArrayList<Value>(rows.keySet());
//		Collections.sort(result);
//		Collections.sort(result, Collections.reverseOrder());
		
		//
		return result;
	}
	
	public Collection<Value> sortedRowValues(){
		List<Value> result;

		result = rowValues();
		Collections.sort(result);
		
		//
		return result;
		
	}
	
	public Collection<Value> rowValuesSortedBySize(){
		List<Value> result;

		result = rowValues();
		Collections.sort(result, Collections.reverseOrder(new PartitionComparator<E>(this)));
		
		//
		return result;
		
	}

	
	public Collection<Value> sortedColValues(){
		List<Value> result;

		result = (List<Value>)colValues();
		Collections.sort(result);
		
		//
		return result;
		
	}
	
	public Collection<Value> colValues(){
		return colValues;
	}
	
	public Cluster<E> getCluster (Value value1, Value value2){
		return rows.get(value1).getCluster(value2);
	}
	
	public Partition<E> getRow(Value value1){
		return rows.get(value1);
	}
	
	public int colSum (Value colValue){
		int result;
		
		Integer colSum = colSums.get(colValue);
		if (colSum == null){
			result = 0;
		} else {
			result = colSum;
		}
		//
		return result;
	}
	
	public int rowSum (Value rowValue){
		int result;
		
		Integer rowSum = rowSums.get(rowValue);
		if (rowSum == null){
			result = 0;
		} else {
			result = rowSum;
		}
		//
		return result;
	}
	
	public int colCount(){
		return colValues.size();
	}

	public int rowCount(){
		return rows.size();
	}

	public void count (){
		
		for (Value rowValue : rowValues()){
			
			Partition<E> partition = rows.get(rowValue);
			
			Integer rowSum = rowSums.get(rowValue);
			
			if (rowSum == null){

				rowSum = partition.clusteredItemsCount();
				rowSums.put(rowValue, rowSum);
			}
			//
			sum += rowSum;
			
			for (Cluster<E> cluster : partition.getClusters()){
				Value colValue = cluster.getValue();
				int colSum = colSum(colValue) + cluster.count();
				colSums.put(colValue, colSum);
			}
		}
	}
	
	public void putRowSum (Value rowValue, Integer rowSum){
		
		rowSums.put(rowValue, rowSum);
	}
	
	public int frequency (Value value1, Value value2){
		int result;
		
		Cluster<E> cluster = getCluster(value1,value2);
		
		if (cluster == null){
			result = 0;
		} else {
			result = cluster.count();
		}
		
		return result;
	}
	
	public double rowPercentage(Value value1, Value value2){
		return MathUtils.percent(frequency(value1, value2), rowSums.get(value1));
	}
	
	public double colPercentage(Value value1, Value value2){
		return MathUtils.percent(frequency(value1, value2), colSums.get(value2));
	}
		
	public int sum()
	{
		int result;
		
		if (this.sum == null)
		{
			result = 0;
		}
		else
		{
			result = this.sum;
		}

		//
		return result;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public Value getColValue (int i){
		return colValues.get(i);
	}
	
	public void put (Value value1, Value value2, E item){
		
		Partition<E> row = getRow(value1);
		
		if (row == null){
			
			row = new Partition<E>();
			put(row, value1);
		}
		
		row.put(item, value2);

		if (!colValues.contains(value2)){
			colValues.add(value2);
		}
	}
	
	public MultiPartition<Value> getIntervalPartition (){
		MultiPartition<Value> result;
		
		result = new MultiPartition<Value>();
		result.setLabel(label+"_INTERVALS");
		
		for (Value rowValue : sortedRowValues()) {

			for (Value colValue : sortedColValues()) {

				Integer ceilingPercentage = (int)Math.ceil(rowPercentage(rowValue, colValue)/10)*10;
				
				if (ceilingPercentage > 100) System.out.println(label+" "+rowValue+" "+colValue+" "+ceilingPercentage+" "+rowPercentage(rowValue, colValue)+" "+this.frequency(rowValue, colValue)+" "+this.rowSum(rowValue));
				
				result.put(colValue, new Value(ceilingPercentage), rowValue);
			}
		}
		
		//
		return result;
	}
	
	
	class PartitionComparator<E> implements Comparator<Value> {
		
		MultiPartition<E> partitions;
		
		public PartitionComparator (MultiPartition<E> partitions){
			this.partitions = partitions;
		}
		
		public int compare(Value first, Value second){
			return new Integer(partitions.getRow(first).itemsCount()).compareTo(partitions.getRow(second).itemsCount());
		}
	}
	

	



}
