package org.tip.puckgui.views.kinoath;

import javax.swing.AbstractListModel;

import org.tip.puck.kinoath.IndividualGroup;
import org.tip.puck.kinoath.IndividualGroupList;
import org.tip.puck.kinoath.IndividualGroups;

/**
 * 
 * @author TIP
 */
public class IndividualGroupsModel extends AbstractListModel {

	public enum Sorting {
		ID,
		SIZE,
		REVERSED_SIZE
	}

	private static final long serialVersionUID = 3976006165441114496L;

	private IndividualGroups source;
	private IndividualGroupList delegate;
	private Sorting sorting;

	/**
	 * 
	 */
	public IndividualGroupsModel(final IndividualGroups source) {
		//
		super();

		//
		this.source = source;
		if (source == null) {
			//
			this.delegate = new IndividualGroupList();

		} else {
			//
			this.delegate = this.source.toList().sortById();
		}
		this.sorting = Sorting.ID;
	}

	/**
	 * 
	 */
	@Override
	public Object getElementAt(final int index) {
		IndividualGroup result;

		result = this.delegate.get(index);

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getSize() {
		int result;

		result = this.delegate.size();

		//
		return result;
	}

	public Sorting getSorting() {
		return this.sorting;
	}

	public IndividualGroups getSource() {
		return this.source;
	}

	/**
	 * 
	 * @param group
	 * @return
	 */
	public int indexOf(final IndividualGroup group) {
		int result;

		if (this.source == null) {
			//
			result = -1;

		} else {
			boolean ended = false;
			result = -1;
			int index = 0;
			while (!ended) {
				//
				if (index < this.delegate.size()) {
					//
					if (this.delegate.get(index) == group) {
						//
						ended = true;
						result = index;

					} else {
						//
						index += 1;
					}
				} else {
					//
					ended = true;
					result = -1;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param sorting
	 */
	public void setSorting(final Sorting sorting) {
		//
		if (sorting != null) {
			//
			this.sorting = sorting;
			setSource(this.source);
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void setSource(final IndividualGroups source) {
		//
		fireIntervalRemoved(this, 0, this.delegate.size());
		switch (this.sorting) {
			case ID:
				this.delegate = this.source.toList().sortById();
			break;

			case SIZE:
				this.delegate = this.source.toList().sortBySize();
			break;

			case REVERSED_SIZE:
				this.delegate = this.source.toList().sortByReversedSize();
			break;
		}

		fireIntervalAdded(this, 0, this.delegate.size());
	}

	/**
	 * 
	 */
	public void touchSorting() {
		//
		switch (this.sorting) {
			case ID:
				setSorting(Sorting.REVERSED_SIZE);
			break;

			case SIZE:
				setSorting(Sorting.ID);
			break;

			case REVERSED_SIZE:
				setSorting(Sorting.SIZE);
			break;
		}
	}
}
