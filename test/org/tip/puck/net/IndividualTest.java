package org.tip.puck.net;

import org.fest.assertions.Assertions;
import org.junit.Test;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class IndividualTest {
	/**
     */
	@Test
	public void testHasAttributeValue00() {

		Individual ego = new Individual(1);
		Assertions.assertThat(ego.hasAttributeValue(null, null)).isFalse();
		Assertions.assertThat(ego.hasAttributeValue(null, "victor")).isFalse();
		Assertions.assertThat(ego.hasAttributeValue("lima", null)).isFalse();
		Assertions.assertThat(ego.hasAttributeValue("lima", "victor")).isFalse();

		ego.attributes().put("lima", "victor");
		Assertions.assertThat(ego.hasAttributeValue(null, null)).isFalse();
		Assertions.assertThat(ego.hasAttributeValue(null, "victor")).isFalse();
		Assertions.assertThat(ego.hasAttributeValue("lima", null)).isFalse();
		Assertions.assertThat(ego.hasAttributeValue("lima", "victor")).isTrue();
		Assertions.assertThat(ego.hasAttributeValue("lima", "victor-alpha")).isFalse();
		Assertions.assertThat(ego.hasAttributeValue("lima-alpha", "victor")).isFalse();
		Assertions.assertThat(ego.hasAttributeValue("lima-alpha", "victor-alpha")).isFalse();
	}
}
