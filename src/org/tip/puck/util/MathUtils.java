package org.tip.puck.util;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author TIP
 * 
 */
public class MathUtils {

	/**
	 * This method compares two nullable boolean values.
	 * 
	 * <pre>
	 * compare(null, null)   = 0
	 * compare(null, false)  > 0
	 * compare(null, true)   > 0
	 * compare(false, null)  < 0
	 * compare(true, null)   < 0
	 * compare(false, false) = 0
	 * compare(false, true)  < 0
	 * compare(true, false)  > 0
	 * compare(true, true)   = 0
	 * </pre>
	 * 
	 * @param alpha
	 *            one of the value.
	 * 
	 * @param bravo
	 *            the other value.
	 * 
	 * @return zero or a positive value or a negative value.
	 * 
	 */
	public static int compare(final Boolean alpha, final Boolean bravo) {
		int result;

		//
		if ((alpha == null) && (bravo == null)) {
			//
			result = 0;

		} else if (alpha == null) {
			//
			result = -1;

		} else if (bravo == null) {
			//
			result = +1;

		} else {
			//
			result = alpha.compareTo(bravo);
		}

		//
		return result;
	}

	/**
	 * This method compares two nullable double values.
	 * 
	 * <pre>
	 * compare(null, null)   = 0
	 * compare(null, bravo)  < 0
	 * compare(alpha, null)  > 0
	 * compare(alpha, bravo) = alpha.compareTo(bravo)
	 * </pre>
	 * 
	 * @param alpha
	 *            one of the value.
	 * 
	 * @param bravo
	 *            the other value.
	 * 
	 * @return zero or a positive value or a negative value.
	 * 
	 */
	public static int compare(final Double alpha, final Double bravo) {
		int result;

		//
		if ((alpha == null) && (bravo == null)) {
			//
			result = 0;

		} else if (alpha == null) {
			//
			result = -1;

		} else if (bravo == null) {
			//
			result = +1;

		} else {
			//
			result = alpha.compareTo(bravo);
		}

		//
		return result;
	}

	/**
	 * This method compares two nullable integer values.
	 * 
	 * <pre>
	 * compare(null, null)   = 0
	 * compare(null, bravo)  < 0
	 * compare(alpha, null)  > 0
	 * compare(alpha, bravo) = alpha.compareTo(bravo)
	 * </pre>
	 * 
	 * @param alpha
	 *            one of the value.
	 * 
	 * @param bravo
	 *            the other value.
	 * 
	 * @return zero or a positive value or a negative value.
	 * 
	 */
	public static int compare(final Integer alpha, final Integer bravo) {
		int result;

		//
		if ((alpha == null) && (bravo == null)) {
			//
			result = 0;

		} else if (alpha == null) {
			//
			result = -1;

		} else if (bravo == null) {
			//
			result = +1;

		} else {
			//
			result = alpha.compareTo(bravo);
		}

		//
		return result;
	}

	/**
	 * This method compares two nullable long values.
	 * 
	 * <pre>
	 * compare(null, null)   = 0
	 * compare(null, bravo)  < 0
	 * compare(alpha, null)  > 0
	 * compare(alpha, bravo) = alpha.compareTo(bravo)
	 * </pre>
	 * 
	 * @param alpha
	 *            one of the value.
	 * 
	 * @param bravo
	 *            the other value.
	 * 
	 * @return zero or a positive value or a negative value.
	 * 
	 */
	public static int compare(final Long alpha, final Long bravo) {
		int result;

		//
		if ((alpha == null) && (bravo == null)) {
			//
			result = 0;

		} else if (alpha == null) {
			//
			result = -1;

		} else if (bravo == null) {
			//
			result = +1;

		} else {
			//
			result = alpha.compareTo(bravo);
		}

		//
		return result;
	}

	/**
	 * This method compares two nullable integer values.
	 * 
	 * <pre>
	 * compare(null, null)   = 0
	 * compare(null, bravo)  > 0
	 * compare(alpha, null)  < 0
	 * compare(alpha, bravo) = alpha.compareTo(bravo)
	 * </pre>
	 * 
	 * @param alpha
	 *            one of the value.
	 * 
	 * @param bravo
	 *            the other value.
	 * 
	 * @return zero or a positive value or a negative value.
	 * 
	 */
	public static int compareOrder(final Integer alpha, final Integer bravo) {
		int result;

		//
		if ((alpha == null) && (bravo == null)) {
			//
			result = 0;

		} else if (alpha == null) {
			//
			result = +1;

		} else if (bravo == null) {
			//
			result = -1;

		} else {
			//
			result = alpha.compareTo(bravo);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isDecimalInteger(final double value) {
		boolean result;

		if (((int) value) == value) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isDecimalInteger(final Double value) {
		boolean result;

		if (value == null) {
			result = false;
		} else {
			result = isDecimalInteger(value.doubleValue());
		}

		//
		return result;
	}

	public static double meanValue(final Map<Integer, Integer> map) {
		double result;

		result = 0.;

		double totalNumber = 0.;
		double totalValue = 0.;

		for (int value : map.keySet()) {
			totalValue += value;
			totalNumber += map.get(value);
		}

		result = totalValue / totalNumber;
		//
		return result;
	}

	public static double medianValue(final Map<Integer, Integer> map) {
		double result;

		result = 0.;
		double totalNumber = 0.;

		for (int value : map.keySet()) {
			totalNumber += map.get(value);
		}

		double half = totalNumber / 2;
		double count = 0.;

		for (int value : map.keySet()) {
			count += map.get(value);
			if (count >= half) {
				result = value;
				break;
			}
		}
		//
		return result;
	}

	/**
	 * Calculates percentage.
	 * 
	 * @param a
	 *            The first double.
	 * @param b
	 *            The second double.
	 * @return Percentage.
	 */
	public static double percent(final double a, final double b) {
		double result;
		
		if (b == 0.){
			
			result = 0.;
			
		} else {
			
			result = Math.round(10000. * (a / b)) / 100.;

		}

		//
		return result;
	}

	/**
	 * Calculates a percentage with a given number of decimal positions.
	 * 
	 * @param a
	 *            The first double.
	 * @param b
	 *            The second double.
	 * @param dec
	 *            The number of decimal positions.
	 * 
	 * @return percentage.
	 */
	public static double percent(final double a, final double b, final int dec) {
		double result;

		if (b == 0.){
			result = 0.;
		} else {
			double k = Math.pow(10, dec);
			result = Math.round(100 * k * (a / b)) / k;
		}

		//
		return result;
	}
	
	public static Double percentNotInfinite(final int a, final int b){
		Double result;
		
		if (b==0){
			result = null;
		} else {
			result = percent(a, b);
		}
		//
		return result;

	}

	/**
	 * Calculates percentage.
	 * 
	 * @param a
	 *            the first integer
	 * @param b
	 *            the second integer
	 * 
	 * @return percentage.
	 */
	public static double percent(final int a, final int b) {
		double result;

		result = percent(new Double(a).doubleValue(), new Double(b).doubleValue());

		//
		return result;
	}
	
	public static double jacquard (final double a, final double b){
		double result;

		result = percent(a - b, a + b);

		//
		return result;
	}
	
	public static double herfindahl (final List<Double> list){
		return herfindahl(list,0.);
	}

	
	public static double herfindahl (final List<Double> list, double totalSum){
		double result;
		
		double sum = 0.;
		for (Double item : list){
			sum += item;
		}
		
		result = 0.;
		if (totalSum > 0.){
			result = Math.pow((totalSum - sum) / totalSum, 2);
			sum = totalSum;
		}
		
		for (Double item : list){
			result += Math.pow(item / sum, 2);
		}
		
		result = round(100*result,2);
		
		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @param decimalNumber
	 * @return
	 */
	public static double round(final double value, final int decimalNumber) {
		double result;

		double shifter = Math.pow(10, decimalNumber);
		result = ((int) (value * shifter)) / shifter;

		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @param decimalNumber
	 * @return
	 */
	public static double roundHundredth(final double value) {
		double result;

		result = Math.round(100. * value) / 100.;

		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @param factor
	 * @return
	 */
	public static int toIntAmplified(final double value, final int factor) {
		int result;

		result = new Double(factor * value).intValue();

		return result;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static String toString(final double value) {
		String result;

		if (MathUtils.isDecimalInteger(value)) {
			result = String.valueOf((int) value);
		} else {
			result = String.valueOf(value);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static String toString(final Double value) {
		String result;

		if (value == null) {
			result = "";
		} else {
			result = toString(value.doubleValue());
		}

		//
		return result;
	}
	
	public static <E> double[] intersectionRates (final Collection<E> firstSet, final Collection<E> secondSet){
		double[] result;
		
		result = new double[3];
		
		int firstSize = firstSet.size();
		int secondSize = secondSet.size();
		int shareSize = 0;
		
		for (E element : firstSet){
			if (secondSet.contains(element)){
				shareSize++;
			}
		}
		
		result[0] = percent(shareSize,firstSize);
		result[1] = percent(shareSize,secondSize);
		result[2] = percent(shareSize,firstSize+secondSize-shareSize); // distance de Jacquard
		
		//
		return result;
		
	}
	
	/**
	 * returns the square of an integer
	 * @param i the integer
	 * @return the square of the integer
	 */
	public static int pow2 (int i){
		return new Double(Math.pow(2,i)).intValue();
	}


}