package org.tip.puckgui.views.geographyEditor;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.FlatDB4GeoNamesException;
import org.tip.flatdb4geonames.util.Chronometer;
import org.tip.puck.geo.Places;
import org.tip.puckgui.util.ProgressStatus;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class AutoFillProgressDialog extends JDialog {

	private static final long serialVersionUID = -4694669492176890205L;

	private static Logger logger = LoggerFactory.getLogger(AutoFillProgressDialog.class);

	private final JPanel contentPanel = new JPanel();
	private SwingWorker<Boolean, Integer> progressWorker;
	private Timer progressUpdater;
	private JButton btnCancel;
	private JButton btnDone;
	private JProgressBar progressBar;
	private JLabel lblTimer;
	private JLabel lblCounter;
	private JLabel lblfilledPlaces;
	private JButton btnStart;

	/**
	 * Create the dialog.
	 */
	public AutoFillProgressDialog(final Places places) {
		addWindowListener(new WindowAdapter() {
			/**
			 * 
			 */
			@Override
			public void windowClosing(final WindowEvent event) {
				cancel();
			}
		});
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModal(true);
		setTitle("Download GeoNames dump files");
		setBounds(100, 100, 523, 293);
		setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new BoxLayout(this.contentPanel, BoxLayout.Y_AXIS));
		{
			JPanel introPanel = new JPanel();
			this.contentPanel.add(introPanel);
			{
				JLabel lblIntroduction = new JLabel(
						"<html>\n<p>This action will fill empty place automatically.</p>\n<p>For each empty place, if GeoNames returns single match or direct match<br/>\nthen coordinates and homonyms are setted to the place,<br/>\notherwise nothing is done.</p>\n</html>");
				introPanel.add(lblIntroduction);
			}
		}
		{
			Component verticalStrut = Box.createVerticalStrut(10);
			this.contentPanel.add(verticalStrut);
		}
		{
			JPanel inputPanel = new JPanel();
			this.contentPanel.add(inputPanel);
			inputPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
					FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
					FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));
			{
				this.progressBar = new JProgressBar();
				this.progressBar.setEnabled(false);
				inputPanel.add(this.progressBar, "4, 2");
			}
			{
				this.lblCounter = new JLabel("- / - (  0 %)");
				inputPanel.add(this.lblCounter, "4, 4, center, default");
			}
			{
				this.lblfilledPlaces = new JLabel("Filled places: -");
				inputPanel.add(this.lblfilledPlaces, "4, 6, center, default");
			}
			{
				this.lblTimer = new JLabel("00:00:00");
				inputPanel.add(this.lblTimer, "4, 9, center, default");
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				this.btnCancel = new JButton("Cancel");
				this.btnCancel.addActionListener(new ActionListener() {
					/**
					 * 
					 */
					@Override
					public void actionPerformed(final ActionEvent event) {
						// Cancel button.

						cancel();

						//
						dispose();
					}
				});
				this.btnCancel.setActionCommand("Cancel");
				buttonPane.add(this.btnCancel);
			}
			{
				Component horizontalStrut = Box.createHorizontalStrut(20);
				buttonPane.add(horizontalStrut);
			}
			{
				this.btnStart = new JButton("Start");
				this.btnStart.addActionListener(new ActionListener() {
					/**
					 * 
					 */
					@Override
					public void actionPerformed(final ActionEvent event) {

						AutoFillProgressDialog.this.btnStart.setEnabled(false);
						doWork(places);
					}
				});
				this.btnStart.setActionCommand("Start");
				buttonPane.add(this.btnStart);
			}
			{
				Component horizontalStrut = Box.createHorizontalStrut(20);
				buttonPane.add(horizontalStrut);
			}
			{
				this.btnDone = new JButton("Done");
				this.btnDone.addActionListener(new ActionListener() {
					/**
					 * 
					 */
					@Override
					public void actionPerformed(final ActionEvent event) {
						// Button done.
						dispose();
					}
				});
				this.btnDone.setEnabled(false);
				this.btnDone.setActionCommand("Done");
				buttonPane.add(this.btnDone);
			}
		}
	}

	/**
	 * 
	 */
	private void cancel() {
		//
		if (AutoFillProgressDialog.this.progressWorker != null) {
			AutoFillProgressDialog.this.progressWorker.cancel(true);
		}

		if (AutoFillProgressDialog.this.progressUpdater != null) {
			AutoFillProgressDialog.this.progressUpdater.stop();
		}
	}

	/**
	 * 
	 */
	private void doWork(final Places places) {

		final ProgressStatus fillStatus = new ProgressStatus();

		ActionListener taskPerformer = new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				refreshDisplay(fillStatus);
			}
		};
		this.progressUpdater = new Timer(1000, taskPerformer);
		this.progressUpdater.setRepeats(true);
		this.progressUpdater.start();

		//
		this.progressWorker = new SwingWorker<Boolean, Integer>() {
			/**
			 * 
			 */
			@Override
			protected Boolean doInBackground() {
				boolean result;

				try {

					int fillCount = GeographyEditorPanel.fillAutomatically(places, fillStatus);

				} catch (IOException exception) {
					//
					exception.printStackTrace();

					//
					String title = "Error.";
					String message = exception.getMessage();
					JOptionPane.showMessageDialog(AutoFillProgressDialog.this, message, title, JOptionPane.ERROR_MESSAGE);

				} catch (FlatDB4GeoNamesException exception) {
					//
					exception.printStackTrace();

					//
					String title = "FlatDB4GeoNames Error.";
					String message = exception.getMessage();
					JOptionPane.showMessageDialog(AutoFillProgressDialog.this, message, title, JOptionPane.ERROR_MESSAGE);

				} finally {

					AutoFillProgressDialog.this.progressUpdater.stop();
					refreshDisplay(fillStatus);
					AutoFillProgressDialog.this.btnCancel.setEnabled(false);
					AutoFillProgressDialog.this.btnDone.setEnabled(true);
					JRootPane rootPane = SwingUtilities.getRootPane(AutoFillProgressDialog.this.btnDone);
					rootPane.setDefaultButton(AutoFillProgressDialog.this.btnDone);
				}

				result = true;

				//
				return result;
			}
		};
		this.progressWorker.execute();
	}

	/**
	 * 
	 */
	private void refreshDisplay(final ProgressStatus source) {
		// Update.
		AutoFillProgressDialog.this.progressBar.setValue(source.getCurrentRate());

		String counterValue = String.format("%d / %d (%3d %%)", source.getCurrent(), source.getMax(), source.getCurrentRate());
		AutoFillProgressDialog.this.lblCounter.setText(counterValue);

		String filledCounter = String.format("Filled places: %d", source.getExtra1());
		AutoFillProgressDialog.this.lblfilledPlaces.setText(filledCounter);

		String timerValue = Chronometer.toTimer(source.getChrono().stop().interval());
		AutoFillProgressDialog.this.lblTimer.setText(timerValue);
	}

	/**
	 * This method shows the dialog in center of the screen.
	 */
	public static void showDialog() {
		showDialog(null, null);
	}

	/**
	 * This method shows the dialog.
	 */
	public static void showDialog(final Component parent, final Places places) {
		//
		AutoFillProgressDialog dialog = new AutoFillProgressDialog(places);
		dialog.pack();
		dialog.setLocationRelativeTo(parent);
		dialog.setVisible(true);
	}
}
