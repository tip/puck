package org.tip.puck.io.iur;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.geo.Geography;
import org.tip.puck.geo.io.GEOTXTFile;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Attributes;
import org.tip.puck.net.Families;
import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.net.UnionStatus;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Actors;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.RelationModels;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.workers.AttributeWorker;
import org.tip.puck.util.LogHelper;

import fr.devinsy.util.StringList;

/**
 * This class represents a TXT File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class IURTXTFile {
	private static final Logger logger = LoggerFactory.getLogger(IURTXTFile.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	public static final int MAX_LINE_SIZE = 4096;

	public static final String BIRTH_ORDER_LABEL = "ORD";
	public static final String HUSBAND_ORDER_LABEL = "HUSB_ORD";
	public static final String WIFE_ORDER_LABEL = "WIFE_ORD";
	public static final String ACTOR_ORDER_LABEL = "ACTOR_ORD";

	public static final String DEFAULT_ROLE_NAME = "MEMBER";

	/***
	 * temporary auxiliary to convert Caio dataset...21/12/2016
	 * 
	 * @param net
	 * @return
	 */
	private static Net forMargie(final Net net) {
		Net result;

		result = net;

		Map<Integer, Individual> oldIds = new TreeMap<Integer, Individual>();
		Map<Integer, Individual> newIds = new TreeMap<Integer, Individual>();

		for (Individual indi : net.individuals()) {

			Integer oldId = Integer.parseInt(indi.getAttributeValue("regno"));
			Integer newId = Integer.parseInt(indi.getAttributeValue("newno"));
			oldIds.put(oldId, indi);
			newIds.put(newId, indi);
		}

		for (Individual indi : net.individuals()) {

			Individual father = null;
			String fatherIdValue = indi.getAttributeValue("fnewno");
			if (fatherIdValue != null) {
				father = newIds.get(Integer.parseInt(fatherIdValue));
			} else {
				fatherIdValue = indi.getAttributeValue("fregno");
				if (fatherIdValue != null) {
					father = oldIds.get(Integer.parseInt(fatherIdValue));
				}
			}
			if (father != null) {
				indi.setAttribute("Father", father.getId() + "");
			}

			Individual mother = null;
			String motherIdValue = indi.getAttributeValue("mnewno");
			if (motherIdValue != null) {
				mother = newIds.get(Integer.parseInt(motherIdValue));
			} else {
				motherIdValue = indi.getAttributeValue("mregno");
				if (motherIdValue != null) {
					mother = oldIds.get(Integer.parseInt(motherIdValue));
				}
			}
			if (mother != null) {
				indi.setAttribute("Mother", mother.getId() + "");
			}

			Individual spouse = null;
			String spouseIdValue = indi.getAttributeValue("spnewno");
			if (spouseIdValue != null) {
				spouse = newIds.get(Integer.parseInt(spouseIdValue));
			} else {
				spouseIdValue = indi.getAttributeValue("spregno");
				if (spouseIdValue != null) {
					spouse = oldIds.get(Integer.parseInt(spouseIdValue));
				}
			}
			if (spouse != null) {
				indi.setAttribute("Spouse", spouse.getId() + "");
			}
		}

		//
		return result;
	}

	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file) throws PuckException {
		Net result;

		result = load(file, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file, final String charsetName) throws PuckException {
		Net result;

		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), charsetName));
			result = read(in);

			result.setLabel(file.getName());

		} catch (UnsupportedEncodingException exception) {
			//
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");

		} catch (FileNotFoundException exception) {
			//
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");

		} finally {
			//
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}

	/**
	 * Reads a net from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static Net read(final BufferedReader in) throws PuckException {
		Net result;

		//
		result = new Net();

		//
		readCorpusAttributes(result.attributes(), in);

		//
		readIndividuals(result, in);

		//
		readFamilies(result, in);

		//
		logger.debug("Read relations.");
		boolean ended = false;
		while (!ended) {
			if (!readRelations(result, in)) {
				ended = true;
			}
		}

		//
		logger.debug("Read geography2.");
		Geography geography = GEOTXTFile.readGeography(in);
		if (geography != null) {
			result.setGeography(geography);
			logger.debug("Geography read.");
		}

		//
		return result;
	}

	/**
	 * Reads a line of corpus attribute labels from a BufferedReader.
	 * 
	 * More over than readLabelsLine method, it detect the absence of the
	 * optional corpus attribute list.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of labels or null.
	 * 
	 * @throws PuckException
	 */
	public static IURTXTLabelsLine readCorpusAttributeLabelsLine(final BufferedReader in) throws PuckException {
		IURTXTLabelsLine result;

		try {
			//
			in.mark(MAX_LINE_SIZE);
			String line = readNotEmptyLine(in);
			if (line == null) {
				//
				result = null;

			} else if ((line.matches("^[Ii][Dd]\\t.*$")) || (line.matches("^\\d.*$"))) {
				//
				in.reset();
				result = null;

			} else {
				//
				String[] tokens = line.split("\\t");

				//
				result = new IURTXTLabelsLine();
				for (String token : tokens) {
					//
					result.add(token);
				}
			}
		} catch (final IOException exception) {
			//
			throw PuckExceptions.IO_ERROR.create(exception, "Reading labels line.");
		}

		//
		return result;
	}

	/**
	 * Reads a net from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static void readCorpusAttributes(final Attributes target, final BufferedReader in) throws PuckException {

		logger.debug("Read corpus attributes.");

		//
		IURTXTLabelsLine labelsLine = readCorpusAttributeLabelsLine(in);
		if (labelsLine != null) {
			IURTXTLabelsLine valuesLine = readLabelsLine(in);

			// Set other attributes.
			if (valuesLine != null) {
				for (int valueCount = 0; valueCount < valuesLine.size(); valueCount++) {
					//
					String label = labelsLine.get(valueCount);
					String value = valuesLine.get(valueCount);

					//
					if (StringUtils.isNotBlank(value)) {
						target.put(label, value);
					}
				}
			}
		}
	}

	/**
	 * Reads a net from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static void readFamilies(final Net target, final BufferedReader in) throws PuckException {

		logger.debug("Read families.");

		// Read families labels.
		IURTXTLabelsLine labelsLine = readLabelsLine(in);
		// logger.debug(toString(labelsLine));

		// Read families data.
		boolean ended = false;
		while (!ended) {
			//
			IURTXTFamilyLine source = readFamilyLine(in);
			// logger.debug(toString(source));

			if (source == null) {
				//
				ended = true;

			} else if (source.getId() != IURTXTFamilyLine.NOID) {

				//
				Individual father;
				if (source.getFatherId() == IURTXTFamilyLine.NOID) {
					//
					father = null;

				} else {
					//
					father = target.individuals().getById(source.getFatherId());
					if (father == null) {
						//
						father = target.createIndividual(source.getFatherId());
					}
				}

				//
				Individual mother;
				if (source.getMotherId() == IURTXTFamilyLine.NOID) {
					//
					mother = null;

				} else {
					//
					mother = target.individuals().getById(source.getMotherId());
					if (mother == null) {
						//
						mother = target.createIndividual(source.getMotherId());
					}
				}

				//
				UnionStatus status;
				switch (source.getStatus()) {
					case IURTXTFamilyLine.UNMARRIED:
						status = UnionStatus.UNMARRIED;
					break;
					case IURTXTFamilyLine.MARRIED:
						status = UnionStatus.MARRIED;
					break;
					case IURTXTFamilyLine.DIVORCED:
						status = UnionStatus.DIVORCED;
					break;
					default:
						throw PuckExceptions.BAD_FILE_FORMAT.create("Unknown union status [" + source.getStatus() + "]");
				}

				//
				Family family = target.createFamily(source.getId(), father, mother, status);

				// Set children.
				if (StringUtils.isNotBlank(source.getChildIds())) {
					//
					String[] ids = source.getChildIds().split("[ .,;]+");
					for (String childId : ids) {

						if (NumberUtils.isNumber(childId)) {
							//
							Individual child = target.individuals().getById(Integer.parseInt(childId));
							if (child == null) {
								//
								child = target.createIndividual(Integer.parseInt(childId));
							}

							if (child.getOriginFamily() == null) {
								//
								child.setOriginFamily(family);
								family.getChildren().add(child);
							} else {
								//
								throw PuckExceptions.BAD_FILE_FORMAT.create("Child [" + child.getId() + "] defined in two different families ["
										+ child.getOriginFamily().getId() + "][" + family.getId() + "]");
							}
						}
					}
				}

				// Set other attributes.
				{
					// logger.debug("source=" + LogHelper.toString(source));
					for (int valueCount = 0; valueCount < source.attributeValues().size(); valueCount++) {
						// logger.debug("valueCount=" + valueCount + " " +
						// source.attributeValues().size() + " " +
						// source.attributeValues().get(valueCount));

						//
						String label = labelsLine.get(valueCount + 5);
						String value = source.attributeValues().get(valueCount);

						// Set special attribute husband order.
						if (StringUtils.isNotBlank(value)) {
							//
							if (label.equals(HUSBAND_ORDER_LABEL) && StringUtils.isNumeric(value)) {
								//
								family.setHusbandOrder(new Integer(value));

							} else if (label.equals(WIFE_ORDER_LABEL) && StringUtils.isNumeric(value)) {
								//
								family.setWifeOrder(new Integer(value));

							} else {
								//
								family.attributes().put(label, value);
							}
						}
					}
				}

				// logger.debug("put : " + individual);
			}
		}

	}

	/**
	 * Reads a line of individual data from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of individual data or null.
	 * 
	 * @throws PuckException
	 */
	public static IURTXTFamilyLine readFamilyLine(final BufferedReader in) throws PuckException {
		IURTXTFamilyLine result;

		try {
			//
			in.mark(MAX_LINE_SIZE);
			String line = readNotEmptyLine(in);
			// logger.debug("line=[" + line + "]");

			if (line == null) {
				result = null;
			} else if (line.matches("^\\d.*$")) {

				//
				String[] tokens = line.split("\\t");

				//
				result = new IURTXTFamilyLine();

				//
				result.setId((Double.valueOf(tokens[0]).intValue()));

				//
				if (tokens.length > 1) {
					String statusToken = tokens[1].trim().toUpperCase();
					if (statusToken.length() == 0) {
						result.setStatus(IURTXTFamilyLine.UNMARRIED);
					} else if (statusToken.length() == 1) {
						result.setStatus(statusToken.charAt(0));
					} else {
						throw PuckExceptions.BAD_FILE_FORMAT.create("Bad union status [" + tokens[1] + "].");
					}
				}

				//
				if (tokens.length > 2) {
					if (StringUtils.isBlank(tokens[2])) {
						result.setFatherId(0);
					} else {
						//
						if (NumberUtils.isNumber(tokens[2])) {
							result.setFatherId((Double.valueOf(tokens[2]).intValue()));
						} else {
							throw PuckExceptions.BAD_FILE_FORMAT.create("Bad father id value [" + tokens[2] + "].");
						}
					}
				}

				//
				if (tokens.length > 3) {
					if (StringUtils.isBlank(tokens[3])) {
						result.setMotherId(0);
					} else {
						if (NumberUtils.isNumber(tokens[3])) {
							result.setMotherId((Double.valueOf(tokens[3]).intValue()));
						} else {
							throw PuckExceptions.BAD_FILE_FORMAT.create("Bad mother id value [" + tokens[3] + "].");
						}
					}
				}

				// Set children.
				if ((tokens.length > 4) && (StringUtils.isNotBlank(tokens[4]))) {
					result.setChildIds(tokens[4]);
				}

				// Set attributes.
				for (int tokenCount = 5; tokenCount < tokens.length; tokenCount++) {
					result.attributeValues().add(tokens[tokenCount]);
				}
			} else {
				in.reset();
				result = null;
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading family line.");
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @return
	 * @throws PuckException
	 * 
	 * @Deprecated
	 */
	/*	public static boolean readGeography(final Net target, final BufferedReader in) throws PuckException {
			boolean result;

			// Read relation model name.
			IURTXTLabelsLine labelsLine = readLabelsLine(in);
			// logger.debug("Labels=" + LogHelper.toString(labelsLine));
			if ((labelsLine == null) || (labelsLine.size() != 1) || (!labelsLine.get(0).equalsIgnoreCase("GEOGRAPHY"))) {
				//
				result = false;

			} else {
				//
				logger.debug("Geography loading...");
				Geography geography = new Geography();
				Place world = new Place(GeoLevel.INTERCONTINENTAL, "World");
				geography.put(world);

				target.setGeography(geography);

				//
				boolean ended = false;
				while (!ended) {
					//
					IURTXTGeographyLine source = readGeographyLine(in);
					// logger.debug("====>" + LogHelper.toString(source));
					if (source == null) {
						//
						ended = true;

					} else if ((source.id() != IURTXTGeographyLine.NOID) && (StringUtils.isNotBlank(source.getName()))) {
						//

						Coordinate coordinate = null;
						if (source.getLatitude() != null && source.getLongitude() != null) {
							coordinate = new Coordinate(Double.parseDouble(source.getLongitude()), Double.parseDouble(source.getLatitude()));
						}

						Place place = new Place(source.id(), source.getName(), "", "", "", source.getCountryCode(), coordinate);

						Place supPlace = geography.get(source.getCountryCode(), GeoLevel.COUNTRY);
						if (supPlace == null) {
							supPlace = new Place(GeoLevel.COUNTRY, source.getCountryCode());
							geography.put(supPlace);
							geography.put(source.getCountryCode(), supPlace);
						}
						place.setSup(supPlace);
						place.setLevel(GeoLevel.TOWN); // Temporary solution
						geography.put(place);
						geography.put(source.getName(), place);

						Place homonym = new Place(GeoLevel.HOMONYM, source.getName());
						homonym.setSup(place);
						geography.put(homonym);
						geography.put(source.getName(), homonym);
					}
				}

				//
				logger.debug("Geography loaded.");
				result = true;
			}
			//
			return result;
		}*/

	/**
	 * Reads a line of attributes from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of attributes or null if no more.
	 * 
	 * @throws PuckException
	 * 
	 * @Deprecated
	 */
	@Deprecated
	public static IURTXTGeographyLine readGeographyLine(final BufferedReader in) throws PuckException {
		IURTXTGeographyLine result;

		try {
			//
			in.mark(MAX_LINE_SIZE);
			String line = readNotEmptyLine(in);

			if (line == null) {
				result = null;
			} else if (line.matches("^\\d.*$")) {
				String[] tokens = line.split("\\t");

				//
				result = new IURTXTGeographyLine();

				//
				result.setId((Double.valueOf(tokens[0]).intValue()));

				//
				result.setName(tokens[1]);
				if (tokens.length > 2) {
					result.setCountryCode(tokens[2]);
				}
				if (tokens.length > 3) {
					result.setLongitude(tokens[3]);
					result.setLatitude(tokens[4]);
				}

			} else {
				in.reset();
				result = null;
			}
		} catch (final IOException exception) {
			//
			throw PuckExceptions.IO_ERROR.create(exception, "Reading individual line.");
		}

		//
		return result;
	}

	/**
	 * Reads a line of individual data from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of individual data or null.
	 * 
	 * @throws PuckException
	 */
	public static IURTXTIndividualLine readIndividualLine(final BufferedReader in) throws PuckException {
		IURTXTIndividualLine result;

		try {
			//
			in.mark(MAX_LINE_SIZE);
			String line = readNotEmptyLine(in);

			if (line == null) {
				result = null;
			} else if (line.matches("^\\d.*$")) {

				//
				String[] tokens = line.split("\\t");

				//
				result = new IURTXTIndividualLine();

				//
				if (NumberUtils.isNumber(tokens[0])) {
					result.setId((Double.valueOf(tokens[0]).intValue()));
				} else {
					throw PuckExceptions.BAD_FILE_FORMAT.create("Individual id is not number [" + line + "].");
				}

				//
				if (tokens.length > 1) {
					result.setName(tokens[1]);
				}

				//
				if (tokens.length > 2 && tokens[2].length() > 0) {
					result.setGender(tokens[2].charAt(0));
				} else {
					result.setGender('X');
				}

				//
				for (int tokenCount = 3; tokenCount < tokens.length; tokenCount++) {
					result.attributeValues().add(tokens[tokenCount]);
				}
			} else {
				in.reset();
				result = null;
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading individual line.");
		}

		//
		return result;
	}

	/**
	 * Reads a net from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static void readIndividuals(final Net target, final BufferedReader in) throws PuckException {

		logger.debug("Read individuals.");

		// Read individuals labels.
		IURTXTLabelsLine labelsLine = readLabelsLine(in);
		// logger.debug(toString(labelsLine));

		// Read individuals data.
		boolean ended = false;
		while (!ended) {
			//
			IURTXTIndividualLine source = readIndividualLine(in);
			// logger.debug(toString(source));

			// logger.debug("source = " + LogHelper.toString(source));

			if (source == null) {
				ended = true;
			} else if (source.getId() != IURTXTIndividualLine.NOID) {

				// Create a new individual.
				Individual individual;
				try {
					//
					individual = target.createIndividual(source.getId());

				} catch (final PuckException exception) {
					//
					throw PuckExceptions.BAD_FILE_FORMAT.create("Individual [" + source.getId() + "] define twice.");
				}

				//
				if (!StringUtils.isBlank(source.getName())) {
					//
					individual.setName(source.getName());
				}

				//
				if (Gender.valueOf(source.getGender()) != Gender.UNKNOWN) {
					//
					individual.setGender(Gender.valueOf(source.getGender()));
				}

				// Set other attributes.
				{
					// logger.debug("source=" + LogHelper.toString(source));
					for (int valueIndex = 0; valueIndex < source.attributeValues().size(); valueIndex++) {
						//
						String label = labelsLine.get(valueIndex + 3);
						String value = source.attributeValues().get(valueIndex);

						// Set special attribute birth order.
						if (StringUtils.isNotBlank(value)) {

							value = value.trim();

							//
							if (label.equals(BIRTH_ORDER_LABEL) && StringUtils.isNumeric(value)) {
								//
								individual.setBirthOrder(new Integer(value));

							} else {
								//
								individual.attributes().put(label, value);
							}
						}
					}
				}

				// logger.debug("put : " + LogHelper.toString(individual));
			}
		}
	}

	/**
	 * Reads a line of labels from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of labels or null.
	 * 
	 * @throws PuckException
	 */
	public static IURTXTLabelsLine readLabelsLine(final BufferedReader in) throws PuckException {
		IURTXTLabelsLine result;

		try {
			//
			in.mark(MAX_LINE_SIZE);
			String line = readNotEmptyLine(in);

			if (line == null) {
				result = null;
			} else if (line.matches("^\\d.*$")) {
				in.reset();
				result = null;
			} else {
				//
				String[] tokens = line.split("\\t");

				//
				result = new IURTXTLabelsLine();
				for (String token : tokens) {
					result.add(token);
				}
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading labels line.");
		}

		//
		return result;
	}

	/**
	 * Reads a not empty line from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a not empty line or null.
	 * 
	 * @throws PuckException
	 */
	public static String readNotEmptyLine(final BufferedReader in) throws PuckException {
		String result;

		try {
			boolean ended = false;
			result = null;
			while (!ended) {
				//
				String line = in.readLine();

				if (line == null) {
					ended = true;
					result = null;
				} else if (StringUtils.isNotBlank(line)) {
					ended = true;
					result = line;
				}
			}
		} catch (final IOException exception) {
			//
			throw PuckExceptions.IO_ERROR.create(exception, "Reading line.");
		}

		//
		return result;
	}

	/**
	 * Reads a line of attributes from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of attributes or null if no more.
	 * 
	 * @throws PuckException
	 */
	public static IURTXTRelationLine readRelationLine(final BufferedReader in) throws PuckException {
		IURTXTRelationLine result;

		try {
			//
			in.mark(MAX_LINE_SIZE);
			String line = readNotEmptyLine(in);

			if (line == null) {
				result = null;
			} else if (line.matches("^\\d.*$")) {
				String[] tokens = line.split("\\t");

				//
				result = new IURTXTRelationLine();

				//
				result.setId((Double.valueOf(tokens[0]).intValue()));

				//
				result.setName(tokens[1]);

				//
				for (int tokenCount = 2; tokenCount < tokens.length; tokenCount++) {
					result.values().add(tokens[tokenCount]);
				}
			} else {
				in.reset();
				result = null;
			}
		} catch (final IOException exception) {
			//
			throw PuckExceptions.IO_ERROR.create(exception, "Reading individual line.");
		}

		//
		return result;
	}

	/**
	 * Reads a net from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static boolean readRelations(final Net target, final BufferedReader in) throws PuckException {
		boolean result;

		try {
			// Mark stream position in case of bad block.
			in.mark(MAX_LINE_SIZE);

			// Read relation model name.
			IURTXTLabelsLine labelsLine = readLabelsLine(in);
			// logger.debug("Labels=" + LogHelper.toString(labelsLine));

			//
			if (labelsLine == null) {
				//
				result = false;

			} else if ((labelsLine.size() != 1) || (labelsLine.get(0).equalsIgnoreCase("GEOGRAPHY") || (labelsLine.get(0).equalsIgnoreCase("FAMILY")))) {
				//
				logger.debug(labelsLine + ": This block is not a relation. Rewind");
				result = false;
				in.reset();

			} else {
				//
				logger.debug("Relations loading...");
				String relationModelName = labelsLine.get(0);

				//
				RelationModel model = target.relationModels().getByName(relationModelName);
				if (model != null) {
					//
					throw PuckExceptions.BAD_FILE_FORMAT.create("Relation model [" + relationModelName + "] is defined twice.");

				} else {
					//
					model = new RelationModel(relationModelName);

					//
					target.relationModels().add(model);
					logger.debug("Relation model=" + model.getName());

					// Read relation roles and attributes labels.
					labelsLine = readLabelsLine(in);
					// logger.debug("Labels=" + LogHelper.toString(labelsLine));
					if ((labelsLine != null) && (labelsLine.size() > 1)) {
						//
						for (int labelIndex = 2; labelIndex < labelsLine.size(); labelIndex++) {
							//
							String label = labelsLine.get(labelIndex);

							if ((!label.startsWith("#")) && (!label.startsWith("$")) && (!label.startsWith(ACTOR_ORDER_LABEL))) {
								//
								if (model.roles().exists(label)) {

									logger.debug("Found a role defined more than once.");
									logger.debug("labelsLine=[{}]", labelsLine);
									throw PuckExceptions.BAD_FILE_FORMAT.create("Role [" + label + "] is defined more than once.");

								} else {
									model.roles().add(new Role(label, 0));
								}
							}
						}

						if ((labelsLine != null) && (labelsLine.size() > 2)) {
							// In a relation block, if several line have the
							// same
							// typeId, we must not create only one line.
							// To check that, we will manage a locale list of
							// relations.
							Map<Integer, Relation> relationTypedIdIndex = new HashMap<Integer, Relation>();

							//
							boolean ended = false;
							while (!ended) {
								//
								IURTXTRelationLine source = readRelationLine(in);
								// logger.debug("====>" +
								// LogHelper.toString(source));
								if (source == null) {
									//
									ended = true;

								} else if ((source.id() != IURTXTRelationLine.NOID) && (StringUtils.isNotBlank(source.getName()))) {
									//
									Relation relation = relationTypedIdIndex.get(source.id());
									if (relation == null) {
										// Note: the line id goes to the
										// typedId.
										relation = target.createRelation(source.id(), source.getName(), model);
										relationTypedIdIndex.put(source.id(), relation);
									}

									// logger.debug("source=" +
									// LogHelper.toString(source));
									Actors actors = new Actors();
									for (int attributeCount = 0; attributeCount < source.values().size(); attributeCount++) {
										//
										String label = labelsLine.get(attributeCount + 2);
										String value = source.values().get(attributeCount);

										//
										if (StringUtils.isNotBlank(value)) {
											//
											if (label.toUpperCase().equals("ID")) {
												// NOP.

											} else if ((label.toUpperCase().equals("NAME")) || (label.toUpperCase().equals("#NAME"))) {
												//
												relation.setName(value);

											} else if (label.startsWith("#")) {
												// Relation attribute case.
												relation.attributes().add(new Attribute(label.substring(1), value));

											} else if (label.startsWith(ACTOR_ORDER_LABEL)) {
												// Actor attribute case.
												if (NumberUtils.isDigits(value.trim())) {

													Integer order = Integer.valueOf(value);

													for (Actor actor : actors) {
														actor.setRelationOrder(order);
													}
												}

											} else if (label.startsWith("$")) {
												// Actor attribute case.
												String trunkLabel = label.substring(1);

												//
												if (actors.isEmpty()) {
													//
													logger.debug("line=[{}]", source.toString());
													throw PuckExceptions.BAD_FILE_FORMAT.create("actor missing in this line "+value);

												} else {
													//
													for (Actor actor : actors) {
														//
														String oldValue = actor.getAttributeValue(trunkLabel);
														if (oldValue==null){
															actor.setAttribute(trunkLabel, value);
														} else if (!oldValue.contains(value)) {
															actor.setAttribute(trunkLabel, oldValue+";"+value);
														}

														if (trunkLabel.equals("REF")) {

															Individual referent = target.individuals().getById(Integer.parseInt(value));
															/* if (referent == null) {
																	if (!relation.getModel().roles().exists(DEFAULT_ROLE_NAME)) {
																			model.roles().add(new Role(DEFAULT_ROLE_NAME, 0));
																	}

																	referent = target.createRelationActor(relation, Integer.parseInt(value), DEFAULT_ROLE_NAME)
																										.getIndividual();
																	System.out.println("created "+referent+" "+DEFAULT_ROLE_NAME);
															}*/
															actor.setReferent(referent);

															/*	} else if (trunkLabel.equals("INVREF")) {
																Actor dependent = relation.firstActorById(Integer.parseInt(value));
																if (dependent == null) {
																	if (!relation.getModel().roles().exists(DEFAULT_ROLE_NAME)) {
																		model.roles().add(new Role(DEFAULT_ROLE_NAME, 0));
																	}
																	dependent = target.createRelationActor(relation, Integer.parseInt(value), DEFAULT_ROLE_NAME);
																}
																dependent.setReferent(lastActor.getIndividual());
															*/
														}
													}
												}
											} else {
												//
												String[] ids = value.split("[ .,;]+");

												for (String id : ids) {
													
													if (target.individuals().getById(Integer.parseInt(id))==null){
														System.err.println("Missing actor "+id+" in "+relation);
													}
													
													//
													Actor actor = target.createRelationActor(relation, Integer.parseInt(id), label);
													actors.add(actor);
												}
											}
										}
									}
								}
							}

						}
					}
				}

				//
				logger.debug("Relations loaded.");
				result = true;
			}
		} catch (IOException exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.IO_ERROR.create(exception, "Error on readRelation.");
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File file, final Net source) throws PuckException {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));

			write(out, source);

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} finally {
			//
			IOUtils.closeQuietly(out);
		}
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static String toString(final IURTXTFamilyLine source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			result = String.format("[id=%d][status=%c][fatherId=%d][motherId=%d][childIds=%s][attributeValues=%s]", source.getId(), source.getStatus(),
					source.getFatherId(), source.getMotherId(), source.getChildIds(), LogHelper.toString(source.attributeValues()));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static String toString(final IURTXTIndividualLine source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			result = String.format("[id=%d][name=%s][gender=%c][attributeValues=%s]", source.getId(), source.getName(), source.getGender(),
					LogHelper.toString(source.attributeValues()));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final IURTXTLabelsLine source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			StringList buffer = new StringList();
			buffer.append("[");
			for (String label : source) {
				buffer.append(label);
				buffer.append(",");
			}
			buffer.removeLast();
			buffer.append("]");

			//
			result = buffer.toString();
		}

		//
		return result;
	}

	/**
	 * Writes a line of family data.
	 * 
	 * @param out
	 *            Target.
	 * @param source
	 *            Source.
	 */
	public static void write(final PrintWriter out, final IURTXTFamilyLine source) {

		//
		StringBuffer buffer = new StringBuffer(512);

		//
		buffer.append(String.format("%d\t%c\t%d\t%d", source.getId(), source.getStatus(), source.getFatherId(), source.getMotherId()));

		//
		buffer.append("\t");
		if (source.getChildIds() != null) {
			buffer.append(source.getChildIds());
		}

		//
		for (String value : source.attributeValues()) {
			buffer.append("\t");
			buffer.append(value);
		}

		//
		out.println(buffer.toString());
	}

	/**
	 * Writes a line of individual data.
	 * 
	 * @param out
	 *            Target.
	 * @param source
	 *            Source.
	 */
	public static void write(final PrintWriter out, final IURTXTIndividualLine source) {

		//
		StringBuffer buffer = new StringBuffer(512);

		//
		buffer.append(String.format("%d\t%s\t%c", source.getId(), source.getName(), source.getGender()));

		//
		for (String value : source.attributeValues()) {
			buffer.append("\t");
			buffer.append(value);
		}

		//
		out.println(buffer.toString());
	}

	/**
	 * Writes a line of labels.
	 * 
	 * @param out
	 *            Target.
	 * @param source
	 *            Source.
	 */
	public static void write(final PrintWriter out, final IURTXTLabelsLine source) {

		StringBuffer buffer = new StringBuffer(192);
		for (String value : source) {
			buffer.append(value);
			buffer.append("\t");
		}
		if (buffer.length() > 0) {
			buffer.deleteCharAt(buffer.length() - 1);
		}

		//
		out.println(buffer.toString());
	}

	/**
	 * Writes a line of family data.
	 * 
	 * @param out
	 *            Target.
	 * @param source
	 *            Source.
	 */
	public static void write(final PrintWriter out, final IURTXTRelationLine source) {

		StringBuffer buffer = new StringBuffer(512);

		buffer.append(String.format("%d\t%s", source.id(), source.getName()));

		for (String value : source.values()) {
			//
			buffer.append("\t");
			buffer.append(value);
		}
		out.println(buffer.toString());
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 * @throws PuckException
	 */
	public static void write(final PrintWriter out, final Net source) throws PuckException {
		//
		if (!source.attributes().isEmpty()) {
			//
			writeCorpusAttributes(out, source.attributes());
		}

		writeIndividuals(out, source.individuals());

		writeFamilies(out, source.families());

		logger.debug("Write families blocks.");
		writeRelations(out, source.relationModels(), source.relations());

		if (source.getGeography() != null) {
			logger.debug("Write geography block.");
			GEOTXTFile.writeGeography(out, source.getGeography());
		}
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 */
	public static void writeCorpusAttributes(final PrintWriter out, final Attributes source) {

		if ((source != null) && (!source.isEmpty())) {
			//
			logger.debug("Write corpus attributes block.");
			IURTXTLabelsLine labelsLine = new IURTXTLabelsLine();
			labelsLine.addAll(source.labels().sort());
			write(out, labelsLine);

			//
			IURTXTLabelsLine valuesLine = new IURTXTLabelsLine();

			// Build corpus attributes data line.
			logger.debug("Write corpus attributes data.");
			for (String label : labelsLine) {

				String value = source.getValue(label);
				if (value == null) {
					valuesLine.add("");
				} else {
					valuesLine.add(value.replace('\t', ' '));
				}
			}

			// Write corpus attributes data line.
			write(out, valuesLine);
		}

		// Write block separation.
		out.println();
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 * @throws PuckException
	 */
	public static void writeFamilies(final PrintWriter out, final Families source) throws PuckException {

		//
		logger.debug("Write families block.");
		IURTXTLabelsLine labelsLine = new IURTXTLabelsLine();
		// Never write "ID" cause a Microsoft Excel bug.
		labelsLine.add("Id");
		labelsLine.add("Status");
		labelsLine.add("FatherId");
		labelsLine.add("MotherId");
		labelsLine.add("Children");
		labelsLine.add(HUSBAND_ORDER_LABEL);
		labelsLine.add(WIFE_ORDER_LABEL);
		labelsLine.addAll(source.getAttributeLabels().sort());
		write(out, labelsLine);

		// Write families data.
		logger.debug("Write families data.");
		for (Family family : source.toSortedList()) {
			//
			IURTXTFamilyLine target = new IURTXTFamilyLine();

			//
			target.setId(family.getId());

			//
			char status;
			switch (family.getUnionStatus()) {
				case UNMARRIED:
					status = IURTXTFamilyLine.UNMARRIED;
				break;
				case MARRIED:
					status = IURTXTFamilyLine.MARRIED;
				break;
				case DIVORCED:
					status = IURTXTFamilyLine.DIVORCED;
				break;
				default:
					throw PuckExceptions.INVALID_PARAMETER.create("Unknown union status code  [" + family.getUnionStatus().toString() + "]");
			}
			target.setStatus(status);

			//
			if (family.getFather() == null) {
				//
				target.setFatherId(IURTXTFamilyLine.NOID);

			} else {
				//
				target.setFatherId(family.getFather().getId());
			}

			//
			if (family.getMother() == null) {
				//
				target.setMotherId(IURTXTFamilyLine.NOID);

			} else {
				//
				target.setMotherId(family.getMother().getId());
			}

			// Write children.
			StringList buffer = new StringList();
			for (Individual individual : family.getChildren()) {
				//
				buffer.append(individual.getId());
				buffer.append(';');
			}
			buffer.removeLast();
			target.setChildIds(buffer.toString());

			// Write husband order.
			String husbandOrderValue;
			if (family.getHusbandOrder() == null) {
				//
				husbandOrderValue = "";

			} else {
				//
				husbandOrderValue = String.valueOf(family.getHusbandOrder());
			}
			target.attributeValues().add(husbandOrderValue);

			// Write wife order.
			String wifeOrderValue;
			if (family.getWifeOrder() == null) {
				//
				wifeOrderValue = "";

			} else {
				//
				wifeOrderValue = String.valueOf(family.getWifeOrder());
			}
			target.attributeValues().add(wifeOrderValue);

			// Build family data line.
			if (!family.attributes().isEmpty()) {
				// Fill attributes.
				for (int labelCount = 7; labelCount < labelsLine.size(); labelCount++) {
					//
					String label = labelsLine.get(labelCount);
					String value = family.getAttributeValue(label);
					if (value == null) {
						//
						target.attributeValues().add("");

					} else {
						//
						target.attributeValues().add(value);
					}
				}
			}

			// Write family data line.
			write(out, target);
		}

		// Write block separation.
		out.println();
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 */
	public static void writeIndividuals(final PrintWriter out, final Individuals source) {

		//
		logger.debug("Write individuals block.");
		IURTXTLabelsLine labelsLine = new IURTXTLabelsLine();
		// Never write "ID" cause a Microsoft Excel bug.
		labelsLine.add("Id");
		labelsLine.add("Name");
		labelsLine.add("Gender");
		labelsLine.add(BIRTH_ORDER_LABEL);
		labelsLine.addAll(source.getAttributeLabels().sort());
		write(out, labelsLine);

		// Write individuals data.
		logger.debug("Write individuals data.");
		for (Individual individual : source.toSortedList()) {
			//
			IURTXTIndividualLine target = new IURTXTIndividualLine();

			target.setId(individual.getId());
			target.setName(individual.getName());
			target.setGender(individual.getGender().toChar());

			String birthOrderValue;
			if (individual.getBirthOrder() == null) {
				//
				birthOrderValue = "";

			} else {
				//
				birthOrderValue = String.valueOf(individual.getBirthOrder());
			}
			target.attributeValues().add(birthOrderValue);

			// Build individual data line.
			if (!individual.attributes().isEmpty()) {
				// Fill attributes.
				for (int labelIndex = 4; labelIndex < labelsLine.size(); labelIndex++) {
					//
					String label = labelsLine.get(labelIndex);
					String value = individual.getAttributeValue(label);
					if (value == null) {
						//
						target.attributeValues().add("");

					} else {
						//
						target.attributeValues().add(value.replace('\t', ' '));
					}
				}
			}

			// Write individual data line.
			write(out, target);
		}

		// Write block separation.
		out.println();
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 */
	public static void writeRelations(final PrintWriter out, final RelationModel relationModel, final Relations source) {

		//
		Relations relations = source.getByModel(relationModel);

		//
		out.println(relationModel.getName());

		//
		logger.debug("Find labels.");
		IURTXTLabelsLine labelsLine = new IURTXTLabelsLine();
		// Never write "ID" cause a Microsoft Excel bug.
		labelsLine.add("Id");
		labelsLine.add("Name");
		labelsLine.addAll(relationModel.roles().toNameList()); // Do not
																// sort!
		for (String attributeLabel : relations.getAttributeLabels().sort()) {
			labelsLine.add("#" + attributeLabel);
		}
		labelsLine.add(ACTOR_ORDER_LABEL);
		for (String attributeLabel : AttributeWorker.getExogenousAttributeDescriptors(relations.getActors()).labels()) {
			labelsLine.add("$" + attributeLabel);
		}

		//
		write(out, labelsLine);

		//
		if (!relations.isEmpty()) {

			// Build and write relation lines.
			for (Relation relation : relations.toSortedList()) {
				// Build and write a relation line for actor without
				// attribute.
				{
					IURTXTRelationLine target = new IURTXTRelationLine();

					//
					target.setId(relation.getTypedId());
					target.setName(relation.getName());

					// Cf. "Do not sort!"
					for (Role role : relationModel.roles()) {

						Actors actors = relation.actors().getByRole(role);

						StringList buffer = new StringList();
						for (Actor actor : actors.toSortedList()) {
							// Only write actor without attribute and
							// without order.

							if ((actor.attributes().isEmpty()) && (actor.getRelationOrder() == null)) {

								//
								buffer.append(actor.getId());
								buffer.append(';');
							}
						}
						buffer.removeLast();
						target.values().add(buffer.toString());
					}

					// Add relation attributes.
					if (!relation.attributes().isEmpty()) {
						// Fill attributes.
						for (int labelCount = 2 + relationModel.roles().size(); labelCount < labelsLine.size(); labelCount++) {

							String label = labelsLine.get(labelCount);

							if (label.startsWith("#")) {

								String value = relation.getAttributeValue(label.substring(1));

								if (value == null) {

									target.values().add("");

								} else {

									target.values().add(value.replace('\t', ' '));
								}
							} else {

								target.values().add("");
							}
						}
					}

					// logger.debug("Write attribute: " +
					// LogHelper.toString(target));
					if ((relation.actors().isEmpty()) || (!StringUtils.isBlank(target.values().toString()))) {
						write(out, target);
					}
				}

				// Build and write relation lines for actor with attributes.
				for (Actor actor : relation.actors().toSortedList()) {
					//
					if ((!actor.attributes().isEmpty()) || (actor.getRelationOrder() != null)) {
						//
						IURTXTRelationLine target = new IURTXTRelationLine();
						target.setId(relation.getTypedId());
						target.setName(relation.getName());

						// Cf. "Do not sort!"
						for (Role role : relationModel.roles()) {

							if (actor.getRole() == role) {

								target.values().add(String.valueOf(actor.getId()));

							} else {

								target.values().add("");
							}
						}

						// Fill attributes.
						for (int labelCount = 2 + relationModel.roles().size(); labelCount < labelsLine.size(); labelCount++) {

							String label = labelsLine.get(labelCount);

							if (label.startsWith("#")) {

								target.values().add("");

							} else if (label.startsWith("$")) {

								String value = actor.getAttributeValue(label.substring(1));
								if (value == null) {

									target.values().add("");

								} else {

									target.values().add(value.replace('\t', ' '));
								}
							} else if (label.equals(ACTOR_ORDER_LABEL)) {

								if (actor.getRelationOrder() == null) {

									target.values().add("");

								} else {

									target.values().add(String.valueOf(actor.getRelationOrder()));
								}
							} else {

								target.values().add("");
							}
						}

						//
						write(out, target);
					}
				}
			}
		}

		// Write block separation.
		out.println();
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 */
	public static void writeRelations(final PrintWriter out, final RelationModels models, final Relations source) {

		for (RelationModel relationModel : models) {

			writeRelations(out, relationModel, source);

		}
	}

}
