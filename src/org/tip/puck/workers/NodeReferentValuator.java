package org.tip.puck.workers;

import java.util.List;

import org.tip.puck.geo.Geography;
import org.tip.puck.graphs.Node;
import org.tip.puck.util.Valuator;
import org.tip.puck.util.Value;

/**
 * 
 * @author TIP
 */
public class NodeReferentValuator<E> extends Valuator<Node<E>> {
	/**
	 * 
	 * @param item
	 * @param label
	 * @return
	 */
	@Override
	public Value get(final Node<E> source, final String label, final Geography geography) {
		Value result;

		if ((source == null) || (source.getReferent() == null)) {
			result = null;
		} else {
			result = new MetaValuator<E>().get(source.getReferent(), label, geography);
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public List<String> getAttributeLabels() {
		// TODO Auto-generated method stub
		return null;
	}
}
