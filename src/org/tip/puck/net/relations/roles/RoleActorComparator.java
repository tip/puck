package org.tip.puck.net.relations.roles;

import java.util.Comparator;

import org.tip.puck.net.relations.Role;

public class RoleActorComparator implements Comparator<RoleActor> {

	RoleRelations relations;
	
    public int compare(RoleActor alpha, RoleActor beta) {
    	int result;
    	
    	result = 0;
    	
    	if (relations!=null) {
    		
    		Role alphaTerm = alpha.getIndividual();
    		Role betaTerm = beta.getIndividual();
    		
    		if (relations.getDistances().get(alphaTerm)!=null && relations.getDistances().get(betaTerm)==null){
    			result = -1;
    		} else if (relations.getDistances().get(alphaTerm)==null && relations.getDistances().get(betaTerm)!=null){
    			result = 1;
    		} else if (relations.getDistances().get(alphaTerm)!=null && relations.getDistances().get(betaTerm)!=null) {
        		result = relations.getDistances().get(alphaTerm) - relations.getDistances().get(betaTerm);
    		}
    	}
    	
    	if (result == 0){
    		
        	result = alpha.getRole().compareTo(beta.getRole());
        	
        	if (result == 0){
        		
        		result = alpha.getIndividual().compareTo(beta.getIndividual());
        	}
    	}

    	//
    	return result;
	}
    
    RoleActorComparator(){
    }
    
    
    RoleActorComparator(RoleRelations relations){
    	
    	this.relations = relations;
    }
	
}
