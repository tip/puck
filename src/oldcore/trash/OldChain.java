package oldcore.trash;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;



import puck.document.model.enums.Gender;

public class OldChain extends ArrayList<Link> implements Comparable<OldChain> {
	
	int last = -1;
	Set<Integer> elements = new HashSet<Integer>();
//	Gender egoGender;
	String[][] subChains;
	String signature;
	int order;
	int depth;
		

	public OldChain (){
		
	}
	
	public OldChain (int v){
		add(v,0);
	}
	
	public OldChain clone (){
		OldChain chain = new OldChain();
		chain.addAll(this);
		return chain;
	}
	
	//temporary placeholder, to change or delete
	public OldChain getModel(){
		return clone(); 
	}
	

	public boolean add(Link e){
		if (e.getEgo()!=last && last>-1){
			System.out.println("Incompatible links "+e.getEgo()+" "+last);
			return false;
		}
		last = e.getAlter();
		elements.add(last);
		return super.add(e);
	}
	
	public boolean addAll (OldChain c){
		if (size()==0 && c.size()==0) {
			last = c.last;
			elements.add(last);
		}
		for (Link e : c){
			add(e);
		}
		return true;
	}
	
	public int dim(){
		return order;
	}
	

	
	public boolean addInv (OldChain c){
		for (int i=c.size()-1;i>-1;i--){
			add(c.get(i).inv());
		}
		return true;
	}

	
	public OldChain inv(){

		OldChain c = new OldChain();
		c.addInv(this);
		int n = subChains[1].length;
		c. subChains = new String[2][n];
		for (int k=0;k<2;k++){
			for (int i=0;i<n;i++){
				c.subChains[k][i]=subChains[k][n-1-i];
			}
		}
		
		return c;
	}
	
	public int getVertex(int i){
		if (i<size()) return get(i).getEgo();
		else if (size()==0) return last;
		return get(size()-1).getAlter();
	}
	
	public int getLast(){
		return last;
	}
	
	public int getFirst(){
		if (size()==0) return last;
		return get(0).getEgo();
	}
	
	private Gender getLastGender(){
		return get(size()-1).alterGender;
	}
	
	private Gender getFirstGender(){
		return get(0).egoGender;
	}
	
/*	private static String[] fuseSubChains (Chain c1, Chain c2, boolean inv){
		if (c1.subChains == null || c2.subChains == null) return c2.subChains;
		String[] subChains = new String[c1.subChains.length+c2.subChains.length];
		int i = 0;
		for (String s : c1.subChains){
			subChains[i] = s;
			i++;
		}
		int j = subChains.length-1;
		for (String s : c2.subChains){
			if (inv) {
				subChains[j] = s;
				j--;
			} else {
				subChains[i] = s;
				i++;
			}
		}
		return subChains;
	}*/
	
	public static OldChain concatenate (OldChain left, OldChain right, boolean byMarriage){
		OldChain c = new OldChain();
		c.addAll(left);
		if (byMarriage) c.add(getLink(left,right));
		c.addAll(right);
		return c;
	}
	
	private boolean hasElementsInCommon (OldChain c){
		for (int i : c.elements){
			if (elements.contains(i)) return true;
		}
		return false;
	}
	
	public static OldChain concatenate(List<OldChain> chains, int dmax){
		if (chains.size()==1) return chains.get(0);
		OldChain c = new OldChain();
		for (int i=0;i<chains.size();i++){
			OldChain next = chains.get(i);
			if (next.depth>dmax) return null;
			if (c.hasElementsInCommon(next)) return null;
			if (i>0) c.add(getLink(c,next));
			c.addAll(next);
		}
		return c;
	}
	
	public static OldChain concatenateInv (OldChain left, OldChain right){
//		if (left.size()==0 && right.size()==0) return new Chain(left.last);  //line can be omitted
		OldChain c = new OldChain();
		c.addAll(left);
		c.addInv(right);
		c.depth = Math.max(left.size(), right.size());
		return c;
	}

	private static Link getLink(OldChain c1, OldChain c2){
		return new Link(c1.getLast(),c2.getFirst(),0);
	}
	
	
	
	public boolean add(int v, int dir){
		if (v==-1) return false;
		if (last>-1) add(new Link(last,v,dir));
		last = v;
		elements.add(v);
		return true;
	}
	
	public boolean removeLast (){
		Link lastlink = get(size()-1);
		elements.remove(last);
		last = lastlink.getEgo();
		return remove(lastlink);
	}
	
	public boolean contains(int v){
		return elements.contains(v);
	}
	
	private static String toString(Gender g){
		if (g == Gender.MALE) return "H";
		if (g == Gender.FEMALE) return "F";
		return "X";
	}
	
	private static String ego(Link e, int k){
		if (k==0) return e.ego+"";
		else return toString(e.egoGender);
	}

	private static String alter(Link e, int k){
		if (k==0) return e.alter+"";
		else return toString(e.alterGender);
	}
	
	public String signature(){
		if (signature!=null) return signature;
		return signature(0);
	}
	
	
	private int writeChain (String s[], Link e, int or, int k){
		if (or == e.dir){
			s[k] = s[k] + " "+alter(e,k); //filiation
			return or;
		} else if (or == -1) {
			s[k] = s[k] + "." + alter(e, k); //marriage
			return 1;
		} else {
			s[k] = s[k] + "." + ego(e, k); //apex
			return writeChain(s,e,-1,k); 
		}
	}
	
	public void setSubChains (){
				
		subChains = new String[2][];
		String[] s = new String[2];
		
		for (int k=0;k<2;k++){
			s[k] = ego(get(0),k);
			int or = 1;
			for (Link e : this){
				or = writeChain(s,e,or,k);
			}
			if (or==1) 	s[k] = s[k] + "." + alter(get(size()-1), k);
		
			
			String[] z = s[k].split("\\.");
			subChains[k] = new String[z.length];
			for (int i = 0;i<z.length;i++){
				if (i%2==1) {
					subChains[k][i]="";
					String[] y = z[i].split("\\s");
					for (int j = y.length-1; j>= 0; j--){
						subChains[k][i]+=y[j];
						if (j>0) subChains[k][i]+=" ";
					}
				}
				else subChains[k][i]=z[i];
			}
		}
		order = subChains[0].length/2;
		
	}

/*	public void setSubChains (){
		String[] s = profiles()[1].split("\\.");
		int n = s.length;
		String[] t = new String[n];
		for (int i = 0;i<n;i++){
			if (i%2==1) {
				t[i]="";
				for (int j = s[i].length()-1; j>= 0; j--){
		            t[i]+= s[i].charAt(j);
				}
			}
			else t[i]=s[i];
		}
		subChains = t;
	}*/

	public String signature (int k){
		return signature (k,false);
	}
	
	public String baseSignature(){
		String t = "";
		for (int i=0;i<size()+1;i++){
			t=t+getVertex(i)+" ";
		}
		return t;
	}
	
	public String alterSignature (){
		String s = "";
		for (Link e : this){
			s = s+e.alterLetter();
		}
		return s;
	}
	
	public String signature (int k, boolean neutral){
		String t = "";
		if (k==0) t = " ";
		String[] a = subChains[k];
		String s = "";
		for (int i=0;i<a.length;i++){
			String[] b = a[i].split("\\s");
			int n = b.length;
			for (int j=0;j<n;j++){
				int u = j;
				if (i%2==1) u = n-1-j;
				if (i%2==0 && j==n-1) {
					if (neutral){
						int m = a[i+1].split("\\s").length;
						if (n>1 && m>1) s = s+t+"()";
					}
					else s = s+t+"("+b[j]+")";
				}
				else if (j>0 || i%2==0)  s = s+t+b[u];
			}
			if (i%2==1 && i<a.length-1) s=s+t+".";
		}

		return s;
	}
	
	public String coupleSignature (oldcore.trash.Net net){
		boolean cons = false;
		String s = net.get(getFirst()).getName();
		int lastAlter = -1;
		for (int i=0;i<size();i++){
			Link e = get(i);
			if (e.dir!=0) continue;
			cons=true;
			if (e.getEgo()!=lastAlter) s = s+" - "+ net.get(e.getEgo()).getName();
			s=s+" = "+net.get(e.getAlter()).getName();
			lastAlter = e.getAlter();
		}
		if (cons) s = s+" - "+ net.get(getLast()).getName();
		else s = s+" = "+ net.get(getLast()).getName();
		return s;
	}
	
/*	public String signature (int k){
		int dir = 1;
		String t = "";
		if (k==0) t = " ";
		String str = "";
		String s = ego(get(0),k);
		for (Link e : this){
			if (e.dir!=dir) {
				if (k==2) s = s+"."+s;
				else s = "("+s+")";
			}
			if (e.dir==0) s = s + t + ".";
			str = str + t + s;
			dir = e.dir;
			s = alter(e,k);
		}
		if (dir!=-1) {
			if (k==2) s = s+"."+s;
			else s = "("+s+")";
		}
		str = str + t + s;
		if (k==0) signature = str;
		return str;

	}*/
	
	
	public String[] getSubChains(int k){
		if (subChains ==null) setSubChains();
		return subChains[k];
	}

	private static int compareChains (String s1, String s2) {
		if (s1.charAt(0)=='H' && s2.charAt(0)=='F') return 1;
		if (s1.charAt(0)=='F' && s2.charAt(0)=='H') return -1;
		if (s1.length()!=s2.length()) return new Integer(s1.length()).compareTo(s2.length());
		if (!s1.equals(s2)) return new String(s1).compareTo(s2);
		return 0;
	}

	private int getStandardEgoPosition (){
		
//		String standard = subChains[1][0];
		int max = 0;
		int w = 2*order;
	
		for (int i=1;i<w;i++){
//			if (test()) System.out.println("step "+i);
			for (int j=0;j<w-1;j++){
				int u = (i+j)%w;
				int v = (max+j)%w;
				int comp = compareChains(subChains[1][u],subChains[1][v]);
				if (comp!=0) {
/*					String sc = subChains[1][i];
					String standard = subChains[1][max];
					if (test()) System.out.println(i+" "+standard + " / " + sc+  "   comp "+ compareChains(sc,standard)+"  max "+max);*/
					if (comp>0) max = i;
//					standard = sc;
					break;
				}
			}
		}
//		if (test()) System.out.println("Pos "+max);
		return max;
	}
	
	public int getMarrPos (int i){
		int count=0;
		int k=0;
		for (Link e : this){
			if (e.dir==0) {
				count++;
				if (count==i) return k;
			}
			k++;
		}
		return k;
	}
	
	

	
	public OldChain transform(int i) {
		if (i==0) return this;
		if (i==2*order-1) return inv();
		OldChain c = new OldChain();
		if (i%2==0) {
			int m = getMarrPos(i/2);
			for (int j=m+1;j<size();j++){
				c.add(get(j));
			}
			c.add(new Link(getLast(),getFirst(),0,getLastGender(),getFirstGender()));
			for (int j=0;j<m;j++){
				c.add(get(j));
			}
		} else {
			int m = getMarrPos((i+1)/2);
			for (int j=m-1;j>-1;j--){
				c.add(get(j).inv());
			}
			c.add(new Link(getFirst(),getLast(),0,getFirstGender(),getLastGender()));
			for (int j=size()-1;j>m;j--){
				c.add(get(j).inv());
			}
		}
	    return c;                     
	}
	
	public OldChain standard (){
//		if (test()) System.out.println(baseSignature()+" "+Arrays.toString(subChains[1]));
			 
		
/*		if (test()) {
			for (int i=0;i<4;i++){
				System.out.println(transform(i).baseSignature());
			}
		}
		if (test()) {
			System.out.println("Pos "+getStandardEgoPosition());
//			Chain t = transform(getStandardEgoPosition());
//			System.out.println(t.baseSignature()+" "+Arrays.toString(t.subChains[1]));
		}*/
		
		return transform(getStandardEgoPosition());
	}
	
	public int compareTo(OldChain c){
		int k = new Integer(getVertex(0)).compareTo(c.getVertex(0));
		if (k!=0) return k;
		for (int i=0;i>size();i++){
			k = new Integer(get(i).getAlter()).compareTo(c.get(i).getAlter());
			if (k!=0) return k;
		}
		return 0;
		
/*		String[] s1 = subChains;
		String[] s2 = c.subChains;
		if (s1.length!=s2.length) return new Integer(s2.length).compareTo(s1.length);
		for (int i=0;i<s1.length;i++){
			if (s1[i].length()!=s2[i].length()) return new Integer(s1[i].length()).compareTo(s2[i].length());
		}
		for (int i=0;i<s1.length;i++){
			if (s1[i].equals(s2[i])) return new String(s1[i]).compareTo(s2[i]);
		}
		return 0;*/
	}
	
	public boolean equals (Object e){
		OldChain c = (OldChain)e;
		if (size()!=c.size()) return false;
		if (size()==0) return last == c.last;
		for (int i=0;i<size();i++){
			if (!get(i).equals(c.get(i))) return false;
		}
		return true;
	}
	
	public boolean neutrequals (OldChain c){
		return signature(0,true).equals((c).signature(0,true));
	}
	
	public boolean test(){
	    return getVertex(0)==334 && getVertex(1)==361 && getVertex(2)==305;
	}





}
