package org.tip.puck.io.gis;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.batik.svggen.SVGGeneratorContext;
import org.apache.batik.svggen.SVGGraphics2D;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.FileDataStoreFactorySpi;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.Transaction;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.renderer.lite.StreamingRenderer;
import org.opengis.feature.simple.SimpleFeatureType;
import org.tip.puck.PuckException;
import org.tip.puck.geo.Place;
import org.tip.puck.graphs.Graph;
import org.w3c.dom.Document;

public class SIGFile {

	public static <E> void exportToGIS(final Graph<Place> source, final String fileName) throws PuckException {

		//		PrintWriter out = null;
		//		try {
		//			//
		//			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(new File(fileName)), "UTF-8"));
		//
		//			//
		//			String gisString = PuckUtils.writeGISNetwork(source).toString();
		//
		//			//
		//			gisString =PAJFile.convertToMicrosoftEndOfLine(gisString);
		//
		//			//
		//			out.print(gisString);
		//
		//		} catch (UnsupportedEncodingException exception) {
		//			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + fileName + "]");
		//		} catch (FileNotFoundException exception) {
		//			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + fileName + "]");
		//		} finally {
		//			if (out != null) {
		//				out.close();
		//			}
		//		}
	}

	public static void exportToShapefile(List<Layer> layersList, File file) {

		Transaction t = null;

		try {

			for (Layer layer : layersList) {

				t = new DefaultTransaction("create");

				SimpleFeatureSource featureSource = (SimpleFeatureSource) layer.getFeatureSource();
				SimpleFeatureType ft = featureSource.getSchema();

				if( !featureSource.getFeatures().isEmpty() ){

					//Prepare date format for export filename geometry type
					
					String schemaName = file.toString() + "_" + ft.getTypeName();
					
					schemaName = schemaName.substring(schemaName.lastIndexOf("/")+1, schemaName.length());
					int loShp = schemaName.lastIndexOf(".shp");
//					schemaName = schemaName + schemaName.substring(schemaName.lastIndexOf(".shp"), 4);
					
					String name = file.toString();
					String dir = file.toString().substring(0, file.toString().lastIndexOf("/")+1);
					name = name.substring(name.lastIndexOf("/")+1, name.length());
					int li = name.lastIndexOf("_")+1;
					int lo = name.lastIndexOf(".shp");
					String saveEndName = name.substring(li, lo);
					name = name.substring(0, li) + ft.getTypeName() + "_" + saveEndName;
					name = name.replace(" ", "_");
					
					File shapeFile = new File(dir + name + ".shp");

					URL shapeURL = shapeFile.toURI().toURL();

//					Map<String, java.io.Serializable> creationParams = new HashMap<String, java.io.Serializable>();
					
					Map map = Collections.singletonMap( "url", shapeURL );
//					creationParams.put("url", shapeURL);

					FileDataStoreFactorySpi factory = FileDataStoreFinder.getDataStoreFactory("shp");
					DataStore dataStore = factory.createNewDataStore(map);

					dataStore.createSchema(ft);

					// The following workaround to write out the prj is no longer needed
					// ((ShapefileDataStore)dataStore).forceSchemaCRS(ft.getCoordinateReferenceSystem());

					SimpleFeatureStore featureStore = (SimpleFeatureStore) DataStoreFinder.getDataStore(map ).getFeatureSource(name);
					featureStore.setTransaction(t);
					
					SimpleFeatureCollection collection = featureSource.getFeatures(); // grab all features
					featureStore.addFeatures(collection);

					t.commit(); // write it out
					t.close();

				}
				
			}

		} catch (Exception eek) {
			eek.printStackTrace();
			try {
				t.rollback();
			} catch (IOException doubleEeek) {
				// rollback failed?
			}
		}
		
	}


	//Adapted from geotools tutorial
	public static void exportSVG(MapContent map, ReferencedEnvelope env, OutputStream out,
			Dimension canvasSize) throws IOException, ParserConfigurationException {
		if (canvasSize == null) {
			canvasSize = new Dimension(6400, 3600); // default of 300x300
		}
		Document document = null;

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();

		// Create an instance of org.w3c.dom.Document
		document = db.getDOMImplementation().createDocument(null, "svg", null);

		// Set up the map
		SVGGeneratorContext ctx1 = SVGGeneratorContext.createDefault((org.w3c.dom.Document) document);
		SVGGeneratorContext ctx = ctx1;
		ctx.setComment("Generated by GeoTools2 with Batik SVG Generator");

		SVGGraphics2D g2d = new SVGGraphics2D(ctx, true);

		g2d.setSVGCanvasSize(canvasSize);

		StreamingRenderer renderer = new StreamingRenderer();
		renderer.setMapContent(map);

		Rectangle outputArea = new Rectangle(g2d.getSVGCanvasSize());
		ReferencedEnvelope dataArea = map.getMaxBounds();

		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
		renderer.paint(g2d, outputArea, dataArea);
		OutputStreamWriter osw = null;
		try {
			osw = new OutputStreamWriter(out, "UTF-8");
			g2d.stream(osw);
		} finally {
			if (osw != null)
				osw.close();
		}

	}

}