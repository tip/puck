package org.tip.puck.statistics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author TIP
 */
public class NumberedFiliationCounts extends HashMap<Integer, FiliationCount> implements Iterable<FiliationCount> {

	private static final long serialVersionUID = -3309433041497195519L;

	/**
	 *
	 */
	public NumberedFiliationCounts() {
		super();
	}

	/**
	 *
	 */
	public NumberedFiliationCounts(final int initialCapacity) {
		super(initialCapacity);
	}

	/**
	 * 
	 * @return
	 */
	public double averageAgnatic() {
		double result;

		//
		int sum = 0;
		int count = 0;
		for (FiliationCount value : this) {
			sum += value.getAgnatic();
			count += 1;
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum * 1.0 / count;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double averageCognatic() {
		double result;

		//
		int sum = 0;
		int count = 0;
		for (FiliationCount value : this) {
			sum += value.getCognatic();
			count += 1;
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum * 1.0 / count;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double averagePositiveAgnatic() {
		double result;

		int sum = 0;
		int count = 0;
		for (FiliationCount value : this) {
			if (value.getAgnatic() != 0) {
				sum += value.getAgnatic();
				count += 1;
			}
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum * 1.0 / count;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double averagePositiveCognatic() {
		double result;

		int sum = 0;
		int count = 0;
		for (FiliationCount value : this) {
			if (value.getCognatic() != 0) {
				sum += value.getCognatic();
				count += 1;
			}
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum * 1.0 / count;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double averagePositiveUterine() {
		double result;

		int sum = 0;
		int count = 0;
		for (FiliationCount value : this) {
			if (value.getUterine() != 0) {
				sum += value.getUterine();
				count += 1;
			}
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum * 1.0 / count;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double averageUterine() {
		double result;

		//
		int sum = 0;
		int count = 0;
		for (FiliationCount value : this) {
			sum += value.getUterine();
			count += 1;
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum * 1.0 / count;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<FiliationCount> iterator() {
		Iterator<FiliationCount> result;

		result = super.values().iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double maxAgnatic() {
		double result;

		if (this.size() == 0) {
			result = 0;
		} else {
			result = Double.MIN_VALUE;
			for (FiliationCount value : this) {
				if (value.getAgnatic() > result) {
					result = value.getAgnatic();
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double maxCognatic() {
		double result;

		if (this.size() == 0) {
			result = 0;
		} else {
			result = Double.MIN_VALUE;
			for (FiliationCount value : this) {
				if (value.getCognatic() > result) {
					result = value.getCognatic();
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double maxUterine() {
		double result;

		if (this.size() == 0) {
			result = 0;
		} else {
			result = Double.MIN_VALUE;
			for (FiliationCount value : this) {
				if (value.getUterine() > result) {
					result = value.getUterine();
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double minAgnatic() {
		double result;

		if (this.size() == 0) {
			result = 0;
		} else {
			result = Double.MAX_VALUE;
			for (FiliationCount value : this) {
				if (value.getAgnatic() < result) {
					result = value.getAgnatic();
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double minCognatic() {
		double result;

		if (this.size() == 0) {
			result = 0;
		} else {
			result = Double.MAX_VALUE;
			for (FiliationCount value : this) {
				if (value.getCognatic() < result) {
					result = value.getCognatic();
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double minUterine() {
		double result;

		if (this.size() == 0) {
			result = 0;
		} else {
			result = Double.MAX_VALUE;
			for (FiliationCount value : this) {
				if (value.getUterine() < result) {
					result = value.getUterine();
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<FiliationCount> toList() {
		List<FiliationCount> result;

		result = new ArrayList<FiliationCount>(values());

		//
		return result;
	}
}
