package org.tip.puck.cli;

import java.io.File;

import org.apache.commons.cli.CommandLine;
import org.tip.puck.PuckException;
import org.tip.puck.io.tip.TIPFile;
import org.tip.puck.mas.MAS;
import org.tip.puck.mas.MASConfig;
import org.tip.puck.net.Net;
import org.tip.puck.net.random.RandomNetExplorer;
import org.tip.puck.net.workers.MemoryCriteria;
import org.tip.puck.report.Report;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.statistics.StatisticsReporter;



public class RunMAS extends Command {

    @Override
    public boolean run(CommandLine cline) {
        if(!cline.hasOption("sim")) {
            setErrorMessage("kinship simulation config file must be specified");
            return false;
        }
        if(!cline.hasOption("onet")) {
            setErrorMessage("output net file must be specified");
            return false;
        }
        if(!cline.hasOption("onet2")) {
            setErrorMessage("second output net file must be specified");
            return false;
        }
        
        String cfgFile = cline.getOptionValue("sim");
        String onet = cline.getOptionValue("onet");
        String onet2 = cline.getOptionValue("onet2");
        
        MASConfig config = new MASConfig();
        config.fromFile(cfgFile);
        MAS mas = config.getMas();
        
        System.out.println(mas);
        
        // run simulation
        mas.run();
        Net net = mas.toNet();
        try {
			TIPFile.save(new File(onet), net);
		}
        catch (PuckException e) {
			e.printStackTrace();
		}
        
        // print report
        try {
			Report basicInfo = StatisticsReporter.reportBasicInformation(net, new Segmentation(net));
			System.out.println(basicInfo);
		}
        catch (PuckException e1) {
			e1.printStackTrace();
		}
        
        // simulate field work
        MemoryCriteria criteria = config.getMemoryCriteria();
        Report report = new Report();
        try {
			Net net2 = new RandomNetExplorer(new Segmentation(net), criteria, report).createRandomNetByObserverSimulation();
			TIPFile.save(new File(onet2), net2);
		}
        catch (PuckException e) {
			e.printStackTrace();
		}
        
        //System.out.println(report);
        
        return true;
    }
}