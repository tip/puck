package org.tip.puck.io.paj;

import java.util.regex.Pattern;

/**
 * This class represents a line of individual from a PAJ file.
 * 
 * @author TIP
 */
public class PAJLine {

	public enum LineType {
		VOID,
		NETWORK_HEADER,
		VERTICES_HEADER,
		ARCS_HEADER,
		PARTITION_HEADER,
		VECTOR_HEADER,
		ARC_EDGE_DATA,
		VERTICE_LONG_DATA,
		VERTICE_SHORT_DATA,
		EDGE_HEADER
	}

	public enum Shape {
		TRIANGLE,
		SQUARE,
		ELLIPSE,
		CIRCLE,
		BOX,
		DIAMOND
	}

	public static final Pattern NETWORK_HEADER_PATTERN = Pattern.compile("^\\*[Nn]etwork (.+)$");
	public static final Pattern VERTICES_HEADER_PATTERN = Pattern.compile("^\\*[Vv]ertices (\\d+)$");
	public static final Pattern ARCS_HEADER_PATTERN = Pattern.compile("^\\*[Aa]rcs\\s*:+\\s*(\\d+)\\s['\"](.+)['\"]$");
	public static final Pattern PARTITION_HEADER_PATTERN = Pattern.compile("^\\*[Pp]artition (.+)$");
	public static final Pattern VECTOR_HEADER_PATTERN = Pattern.compile("^\\*[Vv]ector (.+)$");
	public static final Pattern ARC_EDGE_DATA_PATTERN = Pattern.compile("^\\s*(\\d+)\\s+(\\d+)\\s+(-?\\d+).*$");
	public static final Pattern VERTICE_LONG_DATA_PATTERN = Pattern
			.compile("^\\s*(\\d+) ['\"](.*)['\"](\\s+([\\d\\.]+)\\s+([\\d\\.]+)\\s+([\\d\\.]+)){0,1}\\s*(box|diamond|circle|ellipse|square|triangle){0,1}$");

	public static final Pattern VERTICE_SHORT_DATA_PATTERN = Pattern.compile("^\\s*(\\d+)$");
	public static final Pattern NAME_PATTERN = Pattern.compile("^(.*)\\s*(\\((\\d+)\\))\\s*$");

	private LineType type;
	private String label;
	private String name;
	private Integer cardinal;
	private Integer relationCode;
	private Shape shape;
	private Integer sourceId;
	private Integer targetId;
	private String value;
	private Integer weight;

	/**
	 * 
	 */
	public PAJLine() {
		this.type = LineType.VOID;
	}

	public Integer getCardinal() {
		return this.cardinal;
	}

	public String getLabel() {
		return this.label;
	}

	public String getName() {
		return this.name;
	}

	public Integer getRelationCode() {
		return this.relationCode;
	}

	public Shape getShape() {
		return this.shape;
	}

	public Integer getSourceId() {
		return this.sourceId;
	}

	public Integer getTargetId() {
		return this.targetId;
	}

	public LineType getType() {
		return this.type;
	}

	public String getValue() {
		return this.value;
	}

	public Integer getWeight() {
		return this.weight;
	}

	public void setCardinal(final Integer cardinal) {
		this.cardinal = cardinal;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setRelationCode(final Integer relationCode) {
		this.relationCode = relationCode;
	}

	public void setShape(final Shape shape) {
		this.shape = shape;
	}

	public void setSourceId(final Integer sourceId) {
		this.sourceId = sourceId;
	}

	public void setTargetId(final Integer targetId) {
		this.targetId = targetId;
	}

	public void setType(final LineType type) {
		this.type = type;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	public void setWeight(final Integer weight) {
		this.weight = weight;
	}

	/**
	 * 
	 */
	public static PAJLine createArcEdgeData(final int sourceId, final int targetId, final int weight) {
		PAJLine result;

		result = new PAJLine();

		result.setType(LineType.ARC_EDGE_DATA);
		result.setSourceId(sourceId);
		result.setTargetId(targetId);
		result.setWeight(weight);

		//
		return result;
	}

	/**
	 * 
	 */
	public static PAJLine createArcsHeader() {
		PAJLine result;

		result = new PAJLine();
		result.setType(LineType.ARCS_HEADER);
		result.setRelationCode(null);
		result.setLabel(null);

		//
		return result;
	}

	/**
	 * 
	 */
	public static PAJLine createArcsHeader(final int relationCode, final String label) {
		PAJLine result;

		result = new PAJLine();
		result.setType(LineType.ARCS_HEADER);
		result.setRelationCode(relationCode);
		result.setLabel(label);

		//
		return result;
	}

	/**
	 * 
	 */
	public static PAJLine createEdgeHeader() {
		PAJLine result;

		result = new PAJLine();
		result.setType(LineType.EDGE_HEADER);
		result.setRelationCode(null);

		//
		return result;
	}

	/**
	 * 
	 */
	public static PAJLine createEmptyLine() {
		PAJLine result;

		result = new PAJLine();
		result.setType(LineType.VOID);

		//
		return result;
	}

	/**
	 * 
	 */
	public static PAJLine createNetworkHeader(final String label) {
		PAJLine result;

		result = new PAJLine();

		result.setType(LineType.NETWORK_HEADER);
		result.setLabel(label);

		//
		return result;
	}

	/**
	 * 
	 */
	public static PAJLine createPartitionHeader(final String label) {
		PAJLine result;

		result = new PAJLine();
		result.setType(LineType.PARTITION_HEADER);
		result.setLabel(label);

		//
		return result;
	}

	/**
	 * 
	 */
	public static PAJLine createVectorHeader(final String label) {
		PAJLine result;

		result = new PAJLine();
		result.setType(LineType.VECTOR_HEADER);
		result.setLabel(label);

		//
		return result;
	}

	/**
	 * 
	 */
	public static PAJLine createVertice(final int sourceId, final Integer targetId, final String name, final Shape shape) {
		PAJLine result;

		result = new PAJLine();
		result.setType(LineType.VERTICE_LONG_DATA);
		result.setSourceId(sourceId);
		result.setTargetId(targetId);
		result.setName(name);
		result.setShape(shape);

		//
		return result;
	}

	/**
	 * 
	 */
	public static PAJLine createVertice(final String value) {
		PAJLine result;

		result = new PAJLine();
		result.setType(LineType.VERTICE_SHORT_DATA);
		result.setValue(value);

		//
		return result;
	}

	/**
	 * 
	 */
	public static PAJLine createVerticesHeader(final int cardinal) {
		PAJLine result;

		result = new PAJLine();
		result.setType(LineType.VERTICES_HEADER);
		result.setCardinal(cardinal);

		//
		return result;
	}

}
