package org.tip.puck.visualization.layouts.hierarchical.datastructs;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * @param <K>
 * @param <V>
 * @date Jul 19, 2012
 * @author Edoardo Savoia
 */
public class ManyValuesMap<K,V> implements Map<K,Set<V>> {
    private Map<K,Set<V>> map = new HashMap<K,Set<V>>();


    // MOD

    public static <K> Map<Integer,Set<K>> valuesCount(Map<K,? extends Set<?>> map){
	Map<Integer,Set<K>> wMap = new HashMap<Integer, Set<K>>();
	for (Map.Entry<K, ? extends Set<?>> entry : map.entrySet()) {
	    K k = entry.getKey();
	    int weight = entry.getValue().size();
	    Set<K> kSet = wMap.get(weight);
	    if (kSet == null) {
		wMap.put(weight, new HashSet<K>());
		kSet = wMap.get(weight);
	    }
	    kSet.add(k);
	}
	return wMap;
    }

    private Set<V> init(K key){
	Set<V> get = map.get(key);
	if (get == null) {
	    get = new HashSet<V>();
	    map.put(key, get);
	}
	return get;
    }

    public Map<K, Set<V>> toMap() {
	return Collections.unmodifiableMap(map);
    }

    public int valuesSize(){
	int total = 0;
	for (Entry<K, Set<V>> entry : this.entrySet()) {
	    Set<V> Set = entry.getValue();
	    if (Set != null) {
		total += Set.size();
	    }
	}
	return total;
    }

    public int valueSize(K key){
	return map.get(key).size();
    }

    /**
     * 
     * @param key
     * @param value
     * @return
     */
    public boolean add(K key, V value) {
	return init(key).add(value);
    }

    public boolean remove(K key, V value) {
	Set<V> set = map.get(key);
	if (set == null) {
	    return false;
	} else {
	    return set.remove(value);
	}
    }

    public boolean removeAll(K key, Collection<? extends V> values) {
	Set<V> set = map.get(key);
	if (set == null) {
	    return false;
	}
	boolean result = true;
	for (V v : values) {
	    result &= set.remove(v);
	}
	return result;
    }

    public boolean addAll(K key, Collection<? extends V> values) {
	Set<V> initSet = init(key);
	boolean result = true;
	for (V v : values) {
	    result &= initSet.add(v);
	}
	return result;
    }

    public Set<V> append(K key, Set<V> value) {
	Set<V> init = init(key);
	Set<V> set = new TreeSet<V>(init);
	init.addAll(value);
	return set;
    }

    public void appendAll(Map<? extends K, ? extends Set<V>> m) {
	Set<? extends K> keySet = m.keySet();
	for (K k : keySet) {
	    Set<V> set = m.get(k);
	    append(k, set);
	}
    }


    public boolean hasValue(K key,V value) {
	
	Set<V> elementSet = map.get(key);
	if (elementSet == null) {
	    return false;
	}
	return elementSet.contains(value);
    }

    @Override
    public boolean containsValue(Object value) {
	for (Entry<K, Set<V>> entry : this.entrySet()) {
	    Set<V> Set = entry.getValue();
	    if (Set.contains(value)) {
		 return true;
	    }
	}
	return false;
    }

    @Override
    public String toString() {
	String string = "";
	for (Entry<K, Set<V>> entry : map.entrySet()) {
	    K k = entry.getKey();
	    Set<V> Set = entry.getValue();
	    string = string + k + ":  "+ Set + ".\n";
	}
	return string.substring(0, string.length()-1);
    }


    // END MOD

    @Override
    public int size() {
	return map.size();
    }

    @Override
    public boolean isEmpty() {
	return map.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
	return map.containsKey(key);
    }

    @Override
    public Set<V> get(Object key) {
	return map.get(key);
    }

    @Override
    public Set<V> put(K key, Set<V> value) {
	return map.put(key, value);
    }

     @Override
    public void putAll(Map<? extends K, ? extends Set<V>> m) {
	map.putAll(m);
    }

    @Override
    public Set<V> remove(Object key) {
	return map.remove(key);
    }

    @Override
    public void clear() {
	map.clear();
    }

    @Override
    public Set<K> keySet() {
	return map.keySet();
    }

    @Override
    public Collection<Set<V>> values() {
	return map.values();
    }

    @Override
    public Set<Entry<K, Set<V>>> entrySet() {
	return map.entrySet();
    }


    public static void main(String[] args){
	ManyValuesMap<Integer, Integer> manyValuesMap = new ManyValuesMap<Integer, Integer>();
	for (int i = 0; i < 10; i++) {
	    for (int j = 0; j < 10; j++) {
		manyValuesMap.add(i, j*i);
	    }
	}
	System.out.println("Map: ");
	System.out.println(manyValuesMap.toString());
    }
}
