package org.tip.puck.visualization;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.gephi.preview.api.PreviewController;
import org.gephi.preview.api.ProcessingTarget;
import org.gephi.preview.api.RenderTarget;
import org.gephi.project.api.Project;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.tip.puck.graphs.Graph;
import org.tip.puck.visualization.exporter.Exporter;
import org.tip.puck.visualization.style.Style;
import org.tip.puck.visualization.style.attributes.AttributeMap;
import org.tip.puckgui.views.visualization.VisualizationPanel;
import processing.core.PApplet;

/**
 * Main Visualization class.
 * @author Edoardo Savoia
 */
public class VisualizationController {

    private final ProjectController projectController;
    private final List<Style> styles = new ArrayList<Style>();

    private VisualizationController() {
	projectController = Lookup.getDefault().lookup(ProjectController.class);
	projectController.newProject();
	Project currentProject = projectController.getCurrentProject();
    }

    /*
     * Singleton manager
     */
    public static VisualizationController getSharedInstance() {
	return VisualizationControllerHolder.INSTANCE;
    }

    /*
     * Singleton Instance Holder
     */
    private static class VisualizationControllerHolder {

	private static final VisualizationController INSTANCE = new VisualizationController();
    }

    /*
     * Workspace manager
     */
    protected Workspace createWorkspace() {
	Project currentProject = projectController.getCurrentProject();
	Workspace newWorkspace = projectController.newWorkspace(currentProject);
	return newWorkspace;
    }

    /*
     * This method should be called when a visulization panel is closed (to avoid leaks)
     */
    protected void deleteWorkspace(Workspace workspace) {
	projectController.deleteWorkspace(workspace);
    }


    /*
     * Style manager
     */
    private <D extends Style> D loadStyle(Class<D> styleClass) {
	D style = getStyle(styleClass);
	if (style != null) {
	    return style;
	}

	boolean added = false;
	D newInstance = null;
	try {
	    newInstance = styleClass.newInstance();
	    added = styles.add(newInstance);
	} catch (InstantiationException ex) {
	    added = false;
	    Exceptions.printStackTrace(ex);
	} catch (IllegalAccessException ex) {
	    added = false;
	    Exceptions.printStackTrace(ex);
	}

	// return added ? newInstance : null ;
	// TOBE SURE
	return getStyle(styleClass);
    }

    protected <D extends Style> D getStyle(Class<D> styleClass) {
	for (Style style : styles) {
	    if (style.getClass().equals(styleClass)) {
		return (D) style;
	    }
	}
	return null;
    }


    /*
     * Builder Manager
     */
    public Exporter getExporter(Class<? extends Exporter> exporterClass) {
	Exporter exporter = null;
	// default constructor
	try {
	    exporter = exporterClass.newInstance();
	} catch (InstantiationException ex) {
	    Logger.getLogger(VisualizationController.class.getName()).log(Level.SEVERE, null, ex);
	    exporter = null;
	} catch (IllegalAccessException ex) {
	    Logger.getLogger(VisualizationController.class.getName()).log(Level.SEVERE, null, ex);
	    exporter = null;
	}
	return exporter;
    }


    /*
     * Main method:
     * Returns visualization panel for the given graph and style
     */
    public <D extends Style> VisualizationPanel buildView(Graph<?> graph, Class<D> styleClass) {
	D style = this.<D>loadStyle(styleClass);

	if (style == null) {
	    System.out.println("style not loaded");
	    return null;
	}
	// create a new workspace
	Workspace newWorkspace = this.createWorkspace();
	projectController.openWorkspace(newWorkspace);
	
	// get style's AttributeMapping
	AttributeMap styleMapping = style.styleMapping();
	
	// get importer
	GraphImporter importer = new GraphImporter(newWorkspace, style.graphStyle());
	
	// import graph into gephi using style's attribute mapping
	try {
	    importer.importGraph(graph,styleMapping);
	} catch (Exception ex) {
	    Exceptions.printStackTrace(ex);
	}
	
	style.applyStyle(newWorkspace);
	
	PreviewController previewController = Lookup.getDefault().lookup(PreviewController.class);

	//New Processing target, get the PApplet
	ProcessingTarget target = (ProcessingTarget) previewController.getRenderTarget(RenderTarget.PROCESSING_TARGET, newWorkspace);
	PApplet applet = target.getApplet();
	applet.init();

	//Refresh the preview and reset the zoom
	previewController.render(target);
	target.refresh();
	target.resetZoom();
	VisualizationPanel visualizationPanel = new VisualizationPanel(newWorkspace, applet);
	return visualizationPanel;
    }
}
