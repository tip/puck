package org.tip.puck.statistics;

/**
 * 
 * @author TIP
 */
public class GenderedInt {

	private int value;
	private int menValue;
	private int womenValue;
	private int unknownValue;

	/**
	 * 
	 */
	public GenderedInt() {
	}

	/**
	 * 
	 */
	public GenderedInt(final int value, final int menValue, final int womenValue, final int unknownValue) {
		set(value, menValue, womenValue, unknownValue);
	}

	public int get() {
		return this.value;
	}

	public int getMenValue() {
		return this.menValue;
	}

	public int getUnknownValue() {
		return this.unknownValue;
	}

	public int getWomenValue() {
		return this.womenValue;
	}

	/**
	 * 
	 * @return
	 */
	public int inc() {
		int result;

		this.value += 1;
		result = this.value;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int incMen() {
		int result;

		this.menValue += 1;
		result = this.menValue;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int incUnknown() {
		int result;

		this.unknownValue += 1;
		result = this.unknownValue;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int incWomen() {
		int result;

		this.womenValue += 1;
		result = this.womenValue;

		//
		return result;
	}

	public void set(final int value) {
		this.value = value;
	}

	/**
	 * 
	 * @param value
	 * @param menValue
	 * @param womenValue
	 * @param unknownValue
	 * 
	 * @return
	 */
	public GenderedInt set(final int value, final int menValue, final int womenValue, final int unknownValue) {
		GenderedInt result;

		this.value = value;
		this.menValue = menValue;
		this.womenValue = womenValue;
		this.unknownValue = unknownValue;

		result = this;

		//
		return result;
	}

	public void setMenValue(final int value) {
		this.menValue = value;
	}

	public void setUnknownValue(final int unknownValue) {
		this.unknownValue = unknownValue;
	}

	public void setWomenValue(final int value) {
		this.womenValue = value;
	}

	/**
	 * 
	 * @return
	 */
	public int total() {
		int result;

		result = this.menValue + this.womenValue + this.unknownValue;

		//
		return result;
	}
}
