package org.tip.puck.visualization.layouts.hierarchical.datastructs.chain;

import java.util.Iterator;
import java.util.Set;
import org.gephi.graph.api.Node;
import org.tip.puck.visualization.layouts.GraphSource;

/**
 * @date Jun 23, 2012
 *
 * @author Edoardo Savoia
 */
public class ColoredChain {

    public static ColoredChain createDownChain(Node firstNode, GraphSource graphSource) {
	ColoredChain chain = new ColoredChain(firstNode);
	chain.populateDownChain(graphSource);
	return chain;
    }

    private final Node top;
    private Node bottom;
    private int height;

    private ColoredChain(final Node top ) {
	this.top = top;
	this.bottom = top;
	height = 1;
    }

    public Node[] getNodes(GraphSource graphSource) {
	Node[] list = new Node[height];
	list[0] = top;
	for (int i = 1; i < height; i++) {
	    Set<Node> children = graphSource.getChildren(list[i-1]);
	    Node next = children.iterator().next();
	    list[i] = next;
	}
	return list;
    }

    public Node getBottomNode() {
	return bottom;
    }

    public Node getTopNode() {
	return top;
    }

    public int getHeight() {
	return height;
    }

    @Override
    public int hashCode() {
	int hash = 7;
	hash = 61 * hash + (this.top != null ? this.top.hashCode() : 0);
	hash = 61 * hash + (this.bottom != null ? this.bottom.hashCode() : 0);
	hash = 61 * hash + this.height;
	return hash;
    }

    @Override
    public boolean equals(Object obj) {
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	final ColoredChain other = (ColoredChain) obj;
	if (this.top != other.top && (this.top == null || !this.top.equals(other.top))) {
	    return false;
	}
	if (this.bottom != other.bottom && (this.bottom == null || !this.bottom.equals(other.bottom))) {
	    return false;
	}
	if (this.height != other.height) {
	    return false;
	}
	return true;
    }

    /**
     * Populate the chain descending the graph from current bottom node.
     * It stops when:
     *	- there are no reachable nodes
     *	- there are many reachable nodes
     *	- there is one node shared with another node
     * It colors all the unreachable node:
     *	- RED: node shared with another node
     *	[- BLACK: node not shared]
     * 
     */
    private void populateDownChain(GraphSource graphSource) {
	boolean canGo = true;
	/*
	 * Black Color is Temporary
	if (Color.isColored(bottom, Color.Value.BLACK)) {
	    Color.clean(bottom);
	}
	*/
	while (canGo) {
	    Node last = bottom;
	
	    Set<Node> children = graphSource.getChildren(last);

	    if (children.isEmpty()) {
		canGo = false;
	    }
	    else if (children.size() == 1) {
		Iterator<Node> it = children.iterator();
		Node child = it.next();
		Node otherParent = graphSource.getOtherParent(child, last);
		if (otherParent == null) {
		    bottom = child;
		    ++height;
		}
		else {
		    canGo = false;
		    NodeLayoutData.color(child, NodeLayoutData.Color.RED);
		}
	    }
	    else {
		// color children
		for (Node child : children) {
		    Node otherParent = graphSource.getOtherParent(child, last);
		    if (otherParent != null) {
			NodeLayoutData.color(child, NodeLayoutData.Color.RED);
		    }
		    /* else {
			Color.color(child, Color.Value.BLACK);
		    } */
		}
		canGo = false;
	    }
	}
    }
}
