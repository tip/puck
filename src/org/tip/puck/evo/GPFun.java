package org.tip.puck.evo;


/**
 * Enum of the functions available to genetic programs.
 * 
 * @author Telmo Menezes (telmo@telmomenezes.com)
 */
public class GPFun {
	public final static int SUM = 0;
	public final static int SUB = 1;
	public final static int MUL = 2;
	public final static int DIV = 3;
	public final static int EQ = 4;
	public final static int GRT = 5;
	public final static int LRT = 6;
	public final static int ZER = 7;
	public final static int EXP = 8;
	public final static int LOG = 9;
	public final static int ABS = 10;
	public final static int MIN = 11;
	public final static int MAX = 12;
	public final static int ODD = 13;
}
