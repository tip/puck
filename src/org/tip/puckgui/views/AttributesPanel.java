package org.tip.puckgui.views;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Attributes;
import org.tip.puckgui.WindowGUI;
import org.tip.puckgui.models.AttributesModel;

import fr.devinsy.util.StringSet;

/**
 * 
 * @author TIP
 */
public class AttributesPanel extends JPanel {
	private static final Logger logger = LoggerFactory.getLogger(AttributesPanel.class);

	private static final long serialVersionUID = -7349931177487894947L;
	private static final ResourceBundle bundle = ResourceBundle.getBundle("org.tip.puckgui.messages");
	private JTable table;
	private JLabel lblTitle;
	private JButton btnAdd;
	private WindowGUI guim;
	private JButton btnAddTemplates;
	private JButton btnClearBlank;
	private JLabel lblDescription;

	/**
	 * Create the panel.
	 * 
	 * @wbp.parser.constructor
	 */
	public AttributesPanel(final WindowGUI guiManager, final Attributes source, final StringSet attributeTemplates) {
		this(guiManager, source, attributeTemplates, null);
	}

	/**
	 * Create the panel.
	 */
	public AttributesPanel(final WindowGUI guiManager, final Attributes source, final StringSet attributeTemplates, final String description) {

		this.guim = guiManager;

		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

		JPanel panel_1 = new JPanel();
		add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));

		Component verticalStrut_1 = Box.createVerticalStrut(5);
		panel_1.add(verticalStrut_1);

		JPanel panel = new JPanel();
		panel_1.add(panel);
		panel.setMaximumSize(new Dimension(32767, 25));
		panel.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

		this.lblTitle = new JLabel("Additional Data");
		panel.add(this.lblTitle);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		panel.add(horizontalStrut);

		this.btnAdd = new JButton(" + ");
		this.btnAdd.setToolTipText("Add a new attribute");
		this.btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				if (((AttributesModel) AttributesPanel.this.table.getModel()).getSource() != null) {
					// Add attribute.
					if (AttributesPanel.this.table.getModel().getRowCount() == ((AttributesModel) AttributesPanel.this.table.getModel()).getSource().size()) {
						((AttributesModel) AttributesPanel.this.table.getModel()).setNewItem();
					}
					AttributesPanel.this.table.scrollRectToVisible(AttributesPanel.this.table.getCellRect(
							AttributesPanel.this.table.getModel().getRowCount() - 1, 0, true));
					AttributesPanel.this.table.setRowSelectionInterval(AttributesPanel.this.table.getModel().getRowCount() - 1, AttributesPanel.this.table
							.getModel().getRowCount() - 1);
					AttributesPanel.this.table.setColumnSelectionInterval(0, 0);
					AttributesPanel.this.table.editCellAt(AttributesPanel.this.table.getModel().getRowCount() - 1, 0);
					AttributesPanel.this.table.getEditorComponent().requestFocus();
				}
			}
		});
		this.btnAdd.setBorder(new EmptyBorder(0, 0, 0, 0));
		panel.add(this.btnAdd);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		panel.add(horizontalStrut_1);

		this.btnAddTemplates = new JButton(" ++ ");
		this.btnAddTemplates.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// ++ : add attribute templates
				//
				if (attributeTemplates != null) {
					//
					Attributes attributes = ((AttributesModel) AttributesPanel.this.table.getModel()).getSource();
					for (String label : attributeTemplates) {
						//
						if (!attributes.containsKey(label)) {
							//
							attributes.add(new Attribute(label, ""));
						}
					}

					//
					setSource(((AttributesModel) AttributesPanel.this.table.getModel()).getSource());
				}
			}
		});
		this.btnAddTemplates.setToolTipText("Add templated attributes");
		this.btnAddTemplates.setBorder(new EmptyBorder(0, 0, 0, 0));
		panel.add(this.btnAddTemplates);

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		panel.add(horizontalStrut_2);

		this.btnClearBlank = new JButton("  c  ");
		this.btnClearBlank.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// c : Remove all blank attributes.
				((AttributesModel) AttributesPanel.this.table.getModel()).removeBlankAttributes();
			}
		});
		this.btnClearBlank.setToolTipText("Clear all blank attributes");
		this.btnClearBlank.setBorder(new EmptyBorder(0, 0, 0, 0));
		panel.add(this.btnClearBlank);

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		panel.add(horizontalStrut_3);

		JButton btnR = new JButton("  s  ");
		btnR.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// s : Set current attributes as template.
				//
				if (attributeTemplates != null) {
					//
					Attributes attributes = ((AttributesModel) AttributesPanel.this.table.getModel()).getSource();

					attributeTemplates.clear();
					for (Attribute attribute : attributes) {
						//
						if (StringUtils.isNotBlank(attribute.getLabel())) {
							//
							attributeTemplates.add(attribute.getLabel());
						}
					}
				}
			}
		});
		btnR.setToolTipText("Set current attributes as template");
		btnR.setBorder(new EmptyBorder(0, 0, 0, 0));
		panel.add(btnR);

		Component horizontalGlue = Box.createHorizontalGlue();
		panel.add(horizontalGlue);

		this.lblDescription = new JLabel("");
		panel.add(this.lblDescription);

		Component horizontalStrut_4 = Box.createHorizontalStrut(5);
		panel.add(horizontalStrut_4);

		Component verticalStrut = Box.createVerticalStrut(5);
		panel_1.add(verticalStrut);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_1.add(scrollPane);

		this.table = new JTable();
		this.table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		this.table.setCellSelectionEnabled(true);
		scrollPane.setViewportView(this.table);
		this.table.setModel(new AttributesModel(this.guim, null, attributeTemplates));

		JPopupMenu popupMenu = new JPopupMenu();

		JMenuItem mntmDelete = new JMenuItem("Delete");
		mntmDelete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Delete.
				logger.debug("Delete " + ArrayUtils.toString(AttributesPanel.this.table.getSelectedRows()));
				int[] selectedRowIds = AttributesPanel.this.table.getSelectedRows();
				if (selectedRowIds.length > 0) {
					ArrayUtils.reverse(selectedRowIds);
					for (int rowIndex : selectedRowIds) {
						String label = (String) AttributesPanel.this.table.getModel().getValueAt(rowIndex, 0);
						((AttributesModel) AttributesPanel.this.table.getModel()).getSource().remove(label);
					}
					setSource(((AttributesModel) AttributesPanel.this.table.getModel()).getSource());

					guiManager.setChanged(true);
				}
			}
		});
		popupMenu.add(mntmDelete);

		this.table.getModel().addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(final TableModelEvent event) {
				AttributesPanel.this.lblTitle.setText(bundle.getString("MainWindow.lblAdditionalData.text") + " ("
						+ AttributesPanel.this.table.getModel().getRowCount() + ")");
			}
		});

		addPopup(this.table, popupMenu);

		// //////////////////////////////
		if (StringUtils.isNotBlank(description)) {
			//
			this.lblDescription.setText(description);
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void setSource(final Attributes source) {
		if (source == null) {
			//
			this.lblTitle.setText(bundle.getString("MainWindow.lblAdditionalData.text"));
			((AttributesModel) this.table.getModel()).setSource(null);
			this.btnAdd.setEnabled(false);
			this.btnAddTemplates.setEnabled(false);
			this.btnClearBlank.setEnabled(false);
		} else {
			//
			if ((this.table.isEditing()) && (this.table.getCellEditor() != null)) {
				this.table.getCellEditor().stopCellEditing();
			}
			((AttributesModel) this.table.getModel()).setSource(source);
			this.lblTitle.setText(bundle.getString("MainWindow.lblAdditionalData.text") + " (" + source.size() + ")");
			this.btnAdd.setEnabled(true);
			this.btnAddTemplates.setEnabled(true);
			this.btnClearBlank.setEnabled(true);
		}
	}

	private static void addPopup(final Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			@Override
			public void mouseReleased(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(final MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
