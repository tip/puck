package org.tip.puckgui.views.geneaquilt;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.kinoath.IndividualGroup;
import org.tip.puck.kinoath.IndividualGroups;
import org.tip.puck.kinoath.KinOathDiagram;
import org.tip.puck.net.Families;
import org.tip.puck.net.Individual;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.workers.GeneaQuiltConvert;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.views.kinoath.IndividualGroupsCellRenderer;
import org.tip.puckgui.views.kinoath.IndividualGroupsModel;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.StringList;
import fr.devinsy.util.StringSet;
import fr.inria.aviz.geneaquilt.gui.quiltview.GeneaQuiltPanel;
import fr.inria.aviz.geneaquilt.model.Network;
import fr.inria.aviz.geneaquilt.model.VertexComparator;

/**
 * 
 * @author TIP
 */
public class IndividualGroupEventQuiltsPanel extends JPanel {

	private static final long serialVersionUID = 5801594480758319946L;

	private static final Logger logger = LoggerFactory.getLogger(IndividualGroupEventQuiltsPanel.class);

	private static EventQuiltCriteria eventQuiltCriteria = new EventQuiltCriteria();
	private JPanel thisJPanel;
	private NetGUI netGUI;
	JList individualGroupList;
	JScrollPane individualGroupsScrollPane;
	KinOathDiagram diagram;
	private GeneaQuiltPanel geneaQuiltPanel;
	private JSplitPane split;
	private JCheckBox chckbxUndatedEvents;
	private JPanel pnlModels;
	private JComboBox cmbxComponentSteps;
	private JCheckBox chckbxUnlinkedIndividuals;

	/**
	 * Initialize the contents of the frame.
	 */
	public IndividualGroupEventQuiltsPanel(final NetGUI guiManager, final IndividualGroups source) {

		//
		this.thisJPanel = this;
		this.netGUI = guiManager;

		//
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		this.split = new JSplitPane();
		add(this.split);
		this.split.setAlignmentX(Component.CENTER_ALIGNMENT);

		JPanel individualGroupsPanel = new JPanel();
		individualGroupsPanel.setMinimumSize(new Dimension(200, 10));
		this.split.setLeftComponent(individualGroupsPanel);
		individualGroupsPanel.setLayout(new BorderLayout(0, 0));

		this.individualGroupsScrollPane = new JScrollPane();
		this.individualGroupsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		individualGroupsPanel.add(this.individualGroupsScrollPane);

		this.individualGroupList = new JList();
		this.individualGroupList.setDoubleBuffered(true);
		this.individualGroupList.addListSelectionListener(new ListSelectionListener() {
			/**
			 * 
			 */
			@Override
			public void valueChanged(final ListSelectionEvent event) {
				logger.debug("event = " + event.getValueIsAdjusting() + " " + event.getFirstIndex() + " " + event.getLastIndex() + " "
						+ IndividualGroupEventQuiltsPanel.this.individualGroupList.getSelectedIndex());

				if (!event.getValueIsAdjusting()) {
					// Selected.
					if (IndividualGroupEventQuiltsPanel.this.individualGroupList.getSelectedIndex() != -1) {
						//
						IndividualGroup group = (IndividualGroup) ((JList) event.getSource()).getModel().getElementAt(
								IndividualGroupEventQuiltsPanel.this.individualGroupList.getSelectedIndex());

						updateModels();
						updateIndividualGroupDiagram(group);
					}
				}
			}
		});
		this.individualGroupList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		this.individualGroupList.setCellRenderer(new IndividualGroupsCellRenderer());
		this.individualGroupList.setModel(new IndividualGroupsModel(source));
		this.individualGroupsScrollPane.setViewportView(this.individualGroupList);

		JPanel panel = new JPanel();
		individualGroupsPanel.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BorderLayout(0, 0));

		JPanel buttonsPanel = new JPanel();
		panel.add(buttonsPanel, BorderLayout.NORTH);
		buttonsPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));

		JButton btnGroupsPrevious = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnPrevious.text"));
		btnGroupsPrevious.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Previous individual group.
				int selectedIndex = IndividualGroupEventQuiltsPanel.this.individualGroupList.getSelectedIndex();
				if (selectedIndex == -1) {
					int size = IndividualGroupEventQuiltsPanel.this.individualGroupList.getModel().getSize();
					if (size != 0) {
						selectedIndex = size - 1;
						IndividualGroupEventQuiltsPanel.this.individualGroupList.setSelectedIndex(selectedIndex);
						IndividualGroupEventQuiltsPanel.this.individualGroupList.ensureIndexIsVisible(selectedIndex);
					}
				} else if (selectedIndex > 0) {
					selectedIndex -= 1;
					IndividualGroupEventQuiltsPanel.this.individualGroupList.setSelectedIndex(selectedIndex);
					IndividualGroupEventQuiltsPanel.this.individualGroupList.ensureIndexIsVisible(selectedIndex);
				}
			}
		});

		JButton btnSort = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.btnSort.text"));
		buttonsPanel.add(btnSort);
		btnSort.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Sort individual group list.

				IndividualGroup selectedGroup = getSelectedGroup();
				((IndividualGroupsModel) IndividualGroupEventQuiltsPanel.this.individualGroupList.getModel()).touchSorting();
				if (selectedGroup != null) {
					//
					select(selectedGroup);
				}
			}
		});

		Component horizontalGlue = Box.createHorizontalGlue();
		buttonsPanel.add(horizontalGlue);
		buttonsPanel.add(btnGroupsPrevious);

		JButton btnGroupNext = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnNext.text"));
		btnGroupNext.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Next group.
				int selectedIndex = IndividualGroupEventQuiltsPanel.this.individualGroupList.getSelectedIndex();
				int size = IndividualGroupEventQuiltsPanel.this.individualGroupList.getModel().getSize();
				if (selectedIndex == -1) {
					if (size != 0) {
						selectedIndex = 0;
						IndividualGroupEventQuiltsPanel.this.individualGroupList.setSelectedIndex(selectedIndex);
						IndividualGroupEventQuiltsPanel.this.individualGroupList.ensureIndexIsVisible(selectedIndex);
					}
				} else if (selectedIndex < size - 1) {
					selectedIndex += 1;
					IndividualGroupEventQuiltsPanel.this.individualGroupList.setSelectedIndex(selectedIndex);
					IndividualGroupEventQuiltsPanel.this.individualGroupList.ensureIndexIsVisible(selectedIndex);
				}
			}
		});
		buttonsPanel.add(btnGroupNext);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		buttonsPanel.add(horizontalStrut);

		JPanel panel_13 = new JPanel();
		buttonsPanel.add(panel_13);
		panel_13.setLayout(new BoxLayout(panel_13, BoxLayout.X_AXIS));

		JPanel panelLeftControls = new JPanel();
		panelLeftControls.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "On selection", TitledBorder.LEADING, TitledBorder.TOP, null,
				new Color(51, 51, 51)));
		panel.add(panelLeftControls, BorderLayout.CENTER);
		panelLeftControls.setPreferredSize(new Dimension(10, 300));
		panelLeftControls.setMinimumSize(new Dimension(10, 400));
		panelLeftControls.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, }));

		JLabel lblComponentSteps = new JLabel(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("IndividualGroupEventQuiltsPanel.lblComponentSteps.text")); //$NON-NLS-1$ //$NON-NLS-2$
		lblComponentSteps.setToolTipText(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("IndividualGroupEventQuiltsPanel.lblComponentSteps.toolTipText")); //$NON-NLS-1$ //$NON-NLS-2$
		panelLeftControls.add(lblComponentSteps, "2, 2, right, default");

		this.cmbxComponentSteps = new JComboBox();
		this.cmbxComponentSteps.addItemListener(new ItemListener() {
			/**
																																																									 * 
																																																									 */
			@Override
			public void itemStateChanged(final ItemEvent event) {
				//
				refreshGroupDiagram();
			}
		});
		this.cmbxComponentSteps.setModel(new DefaultComboBoxModel(new String[] { "NONE", "1" }));
		panelLeftControls.add(this.cmbxComponentSteps, "4, 2, fill, default");

		this.chckbxUndatedEvents = new JCheckBox(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("IndividualGroupEventQuiltsPanel.chckbxWoDateEvents.text")); //$NON-NLS-1$ //$NON-NLS-2$
		this.chckbxUndatedEvents.addActionListener(new ActionListener() {
			/**
																																																																	 * 
																																																																	 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				refreshGroupDiagram();
			}
		});
		this.chckbxUndatedEvents.setSelected(true);
		panelLeftControls.add(this.chckbxUndatedEvents, "2, 4, 3, 1");

		this.chckbxUnlinkedIndividuals = new JCheckBox(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("IndividualGroupEventQuiltsPanel.chckbxUnlinkedIndividuals.text")); //$NON-NLS-1$ //$NON-NLS-2$
		this.chckbxUnlinkedIndividuals.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent event) {
				//
				refreshGroupDiagram();
			}
		});
		this.chckbxUnlinkedIndividuals.setSelected(true);
		panelLeftControls.add(this.chckbxUnlinkedIndividuals, "2, 6, 3, 1");

		JLabel lblRelations = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualGroupEventQuiltsPanel.lblRelations.text")); //$NON-NLS-1$ //$NON-NLS-2$
		panelLeftControls.add(lblRelations, "2, 8, 3, 1");

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		panelLeftControls.add(scrollPane, "2, 10, 3, 1, fill, fill");

		this.pnlModels = new JPanel();
		scrollPane.setViewportView(this.pnlModels);
		this.pnlModels.setLayout(new BoxLayout(this.pnlModels, BoxLayout.Y_AXIS));

		this.geneaQuiltPanel = new GeneaQuiltPanel(null, (Network) null);
		this.split.setRightComponent(this.geneaQuiltPanel);

		// ////////////////////
		setCriteria(eventQuiltCriteria);
		selectByIndex(1);
	}

	/**
	 * 
	 * @return
	 */
	private EventQuiltCriteria getCriteria() {
		EventQuiltCriteria result;

		result = new EventQuiltCriteria();

		//
		result.setShowUndatedEvent(this.chckbxUndatedEvents.isSelected());
		result.setShowUnlinkedIndividual(this.chckbxUnlinkedIndividuals.isSelected());

		//
		try {
			String value = (String) this.cmbxComponentSteps.getSelectedItem();
			Long number = Long.parseLong(value);
			result.setComponentSteps(number);
		} catch (NumberFormatException exception) {
			result.setComponentSteps(null);
		}

		//
		result.getModels().clear();
		for (Component component : this.pnlModels.getComponents()) {
			//
			if (((JCheckBox) component).isSelected()) {
				//
				result.getModels().add(((JCheckBox) component).getText());
			}
		}
		logger.debug("models=[{}]", result.getModels().toStringWithCommas());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public IndividualGroup getSelectedGroup() {
		IndividualGroup result;

		if (this.individualGroupList.getSelectedIndex() == -1) {
			result = null;
		} else {
			result = (IndividualGroup) ((IndividualGroupsModel) this.individualGroupList.getModel()).getElementAt(this.individualGroupList.getSelectedIndex());
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public void refreshGroupDiagram() {
		//
		refreshIndividualGroupDiagram(getSelectedGroup());
	}

	/**
	 * 
	 */
	public void refreshIndividualGroupDiagram(final IndividualGroup source) {
		//
		if (source == null) {
			//
			logger.debug("updateIndividualGroupDiagram(null)");

		} else {
			//
			logger.debug("updateIndividualGroupDiagram(size=" + source.size() + ")");

			EventQuiltCriteria criteria = getCriteria();

			logger.debug("Component step=[{}]", criteria.getComponentSteps());
			IndividualGroup target;
			if (criteria.getComponentSteps() == null) {
				//
				target = source;

			} else {

				target = new IndividualGroup(source);
				for (Individual individual : source) {
					//
					if (criteria.getModels().contains("Family (core)")) {
						//
						target.add(individual.getParents());
						target.add(individual.getPartners());
						target.add(individual.children());
					}

					//
					for (Relation relation : individual.relations()) {
						//
						if (criteria.getModels().contains(relation.getModel().getName())) {
							//
							target.add(relation.getIndividuals());
						}
					}
				}
			}

			//
			this.split.remove(this.geneaQuiltPanel);

			Families families = new Families();
			if (criteria.getModels().contains("Family (core)")) {
				for (Individual individual : source) {
					//
					families.add(individual.getOriginFamily());
					families.add(individual.getPersonalFamilies());
				}
			}

			Relations relations = new Relations();
			for (Individual individual : source) {
				//
				for (Relation relation : individual.relations()) {
					//
					if (criteria.getModels().contains(relation.getModel().getName())) {
						//
						relations.add(individual.relations());
					}
				}
			}

			Network network = GeneaQuiltConvert.convertToEventQuilt(target, families, relations, getCriteria());
			this.geneaQuiltPanel = new GeneaQuiltPanel(this, network, VertexComparator.Sorting.DATERANGE_START);

			this.split.setRightComponent(this.geneaQuiltPanel);
		}
	}

	/**
	 * 
	 * @param target
	 */
	public void select(final IndividualGroup group) {
		int groupIndex = ((IndividualGroupsModel) this.individualGroupList.getModel()).indexOf(group);

		if ((groupIndex >= 0) && (groupIndex < ((IndividualGroupsModel) this.individualGroupList.getModel()).getSize())) {
			//
			this.individualGroupList.setSelectedIndex(groupIndex);
			this.individualGroupList.ensureIndexIsVisible(groupIndex);

			//
			updateModels();

		} else if (((IndividualGroupsModel) this.individualGroupList.getModel()).getSize() != 0) {
			//
			this.individualGroupList.setSelectedIndex(0);
			this.individualGroupList.ensureIndexIsVisible(0);

		} else {
			//
			updateIndividualGroupDiagram(null);
		}
	}

	/**
	 * 
	 * @param target
	 */
	public void selectByIndex(final int groupIndex) {

		if ((groupIndex >= 0) && (groupIndex < ((IndividualGroupsModel) this.individualGroupList.getModel()).getSize())) {
			//
			this.individualGroupList.setSelectedIndex(groupIndex);
			this.individualGroupList.ensureIndexIsVisible(groupIndex);

		} else if (((IndividualGroupsModel) this.individualGroupList.getModel()).getSize() != 0) {
			//
			this.individualGroupList.setSelectedIndex(0);
			this.individualGroupList.ensureIndexIsVisible(0);

		} else {
			//
			updateIndividualGroupDiagram(null);
		}
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final EventQuiltCriteria source) {
		if (source != null) {
			//
			this.chckbxUndatedEvents.setSelected(source.isShowUndatedEvent());

			//
			this.chckbxUnlinkedIndividuals.setSelected(source.isShowUnlinkedIndividual());

			//
			if (source.getComponentSteps() == null) {
				//
				this.cmbxComponentSteps.setSelectedIndex(0);
			} else {
				//
				this.cmbxComponentSteps.setSelectedItem(String.valueOf(source.getComponentSteps()));
			}
		}
	}

	/**
	 * 
	 */
	public void updateGroupDiagram() {
		//
		updateIndividualGroupDiagram(getSelectedGroup());
	}

	/**
	 * 
	 */
	public void updateIndividualGroupDiagram(final IndividualGroup source) {
		//
		updateModels();
		refreshIndividualGroupDiagram(source);
	}

	/**
	 * 
	 */
	private void updateModels() {
		//
		this.pnlModels.removeAll();

		IndividualGroup group = getSelectedGroup();

		StringSet modelNames = new StringSet();
		for (Individual individual : group) {
			//
			for (Relation relation : individual.relations()) {
				//
				modelNames.put(relation.getModel().getName());
			}
		}

		StringList modelNameList = new StringList();
		modelNameList.append("Family (core)");
		modelNameList.append(modelNames.toStringList().sort());
		for (String modelName : modelNameList) {
			//
			JCheckBox checkbox = new JCheckBox(modelName);
			checkbox.setSelected(true);
			this.pnlModels.add(checkbox);
			checkbox.addActionListener(new ActionListener() {
				/**
				 * 
				 */
				@Override
				public void actionPerformed(final ActionEvent event) {
					//
					refreshGroupDiagram();
				}
			});
		}
	}
}
