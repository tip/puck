package oldcore.trash;

import java.util.Arrays;
import java.util.Comparator;


import org.tip.puck.net.Individual;


/**
 * This class defines the comparator for the keys used in maps
 * @author Klaus Hamberger
 * @since Version 0.5
 */
public class VectorComparator implements Comparator<Object> {
		

	// 0 alphanumeric, 1 vector;
	private int type;
	
	
	/**
	 * gets the length of a kinship chain with given characteristic number
	 * @param i the characteristic number of the chain
	 * @return the length of the chain
	 */
/*   private static int getLength (int i){
	   int a = Math.abs(i);
	   return new Double(Math.floor(Math.log(a+1)/Math.log(2))).intValue()-1;
   }*/
	
	/**
	 * gets the length of a kinship chain with given characteristic vector
	 * @param v the characteristic vector of the chain
	 * @return the length of the chain
	 */
/*  private static int getLength (int[] v){
	   int j = 0;
	   for (int i : v){
		   j = j + getLength(i);
	   }
	   return j + v.length/2 - 1;
   }*/
	

	
/*	public VectorComparator(){
		
	}*/
	
	/**
	 * reduces the characteristic vector to a vector of characteristic numbers of linear chains up to the apical ancestors and adds an array of apical ancestor gender numbers
	 * <p> kinship relations that differ only in the sex of apical ancestors thus have the same reduced vector
	 * @param vec  the characteristic vector
	 * @return a table consisting of (1) the reduced vector and (2) the array of apical gender numbers
	 * <p> 0 male, 1 female, 2 indeterminate (for full sibling relations)
	 */
/*	public static int[][] getReducedVector (int[] vec){
		int[][] c = new int[2][vec.length];
		for (int i=0;i<vec.length;i++){
			int a = Math.abs(vec[i]);
			int d = new Double(Math.floor(Math.log(a+1)/Math.log(2))).intValue();
			int k = Mat.pow2(d);
			int r = a-k+1;
			c[1][i] = new Long(Math.round(new Double(r)/new Double(k))).intValue();
			c[0][i] = a-(c[1][i]+1)*k/2;
			if (vec[i]<0) c[1][i]=-1;
		}
		return c;
	}*/
	
	/**
	 * compares two vectors of node chains according to a given comparison mode
	 * @param v1 the vector of the first chain
	 * @param v2 the vector of the second chain
	 * @param mode the mode of comparison<p>
	 * 0 width, 1 egoGender, 2 length, 3 slope, 4 branches
	 * @return the comparison result (-1, 0, 1)
	 * @see maps.VectorComparator#compare(Object, Object)
	 */
/*	private int compare (int[] v1, int[]v2, int mode){
		if (mode==0) return (Math.abs(v2[0]%2))-(Math.abs(v1[0]%2)); //chains with male ego first
		if (mode==1) return Mat.compareAbs(v1.length,v2.length);  //narrower chains first
		if (mode==2) return Mat.compareAbs(getLength(v1),getLength(v2)); //shorter chains first
		if (mode==3) {
			 for (int i=0;i<v1.length/2;i++) {
				 int a = getLength(v1[2*i]);//+length(v1[2*i+1]); //ascending branches first
				 int b = getLength(v2[2*i]);//+length(v2[2*i+1]); 
				 if (a!=b) return Mat.compareAbs(b,a);
			 }
			 return 0;
		}
		if (mode==4) {
			for (int i=0;i<v1.length;i++) {
		     	 int c = Mat.compareAbs(v1[i],v2[i]); 
		     	 if (c != 0) return c;
			}
		    return 0;
		}
		for (int i=0;i<v1.length;i++) {
	     	 int c = Mat.compare(v1[i],v2[i]); 
	     	 if (c != 0) return c;
		}
		if (mode==4){ 
//			if (Math.abs(v1[0])<3 || Math.abs(v1[v1.length-1])<3 || Math.abs(v2[0])<3 || Math.abs(v2[v2.length-1])<3) return 0;
			return Mat.compare(-Math.abs(Mat.compareAbs(v2[0]%2,v2[v2.length-1]%2)),-Math.abs(Mat.compareAbs(v1[0]%2,v1[v1.length-1]%2))); //Same-Sex before cross-sex relations
		}
		if (mode==5){
//			if (Math.abs(v1[0])<3 || Math.abs(v1[v1.length-1])<3 || Math.abs(v2[0])<3 || Math.abs(v2[v2.length-1])<3) {
//				return Math.abs(v2[v2.length-1]%2)-Math.abs(v1[v1.length-1]%2);
//			}
			return Math.abs(v2[0]%2)-Math.abs(v1[0]%2); // Male Ego First
		}
	
		int[][] red1 = getReducedVector(v1);
		int[][] red2 = getReducedVector(v2);
		
//		System.out.println(Arrays.toString(v1)+"  ... "+Arrays.toString(v2));

		for (int i=0;i<v1.length;i++) {
			int j = i+1-2*(i%2);
			if (red1[0][j]==0 || red2[0][j]==0){  //linear relations : apical gender considered
		     	 int c = Mat.compare(v1[i],v2[i]); 
		     	 if (c != 0) return c;
			} else {                              //nonlinear relations: apical gender is considered last
				int c = Mat.compare(red1[0][i],red2[0][i]); 
				if (c != 0) return c;
			}
		}
		for (int i=0;i<v1.length;i++) { //apical gender: full siblings first, then agnatic and uterine half siblings
			int c = Mat.compare(red1[1][i],red2[1][i]); 
			if (c != 0) return c;
		}
		return 0;
	}*/
	
	/**
	 * compares two objects
	 * @return the comparison result (-1, 0, 1)
	 * @since last modified 10-07-23
	 */
/*	public int compare (Object o1, Object o2){
		if (type==0){
			for (int i=0;i<3;i++){
				int c = compare(o1,o2,i);
				if (c!=0) return c;
			}
			return 0;
		}
		if (type==-1) return compare(((Individual)o1).getId(),((Individual)o2).getId(),0);
		if (type==-2) {
			int[] a = (int[])o1;
			int[] b = (int[])o2;
			for (int i=0;i<a.length;i++){
				if (a[i]>b[i]) return 1;
				if (b[i]>a[i]) return -1;
			}
			return 0;
		}
//		if (type==6) return compare((int[])o1,(int[])o2,false);
		return compare((int[])o1,(int[])o2,(type==7));
	}*/
	
	/**
	 * compares two ring vectors
	 * @param o1 the first vector
	 * @param o2 the second vector
	 * @param maleEgoFirst true if male ego is preferred
	 * @return the comparison index
	 * @since last modified 10/04/18
	 */
/*	public int compare(int[] o1, int[] o2, boolean maleEgoFirst){
		int k = 0;
		if (!maleEgoFirst) k=1;
		for (int i=k;i<7;i++){
			if (!maleEgoFirst && i==5) continue; //no male-ego-first preference if not demanded
			if (type==4 && i==4) continue; //no parallel-sex preference for open chain distribution chart
			int c = compare(o1,o2,i);
			if (c!=0) return c;
		}
		return 0;
	}*/
	


	//Check NumberFormatException and mode for Census with individual clustering!
	/**
	 * compares two alphanumeric keys according to a given comparison mode
	 * @param o1 the first key
	 * @param o2 the second key
	 * @param mode the comparison mode
	 * <p> 0 integer, 1 numbered string, 2 string
	 * @return the comparison result (-1, 0, 1)
	 * @see maps.VectorComparator#compare(Object, Object)
	 */
/*	private int compare (Object o1, Object o2, int mode){
		if (mode==0) {
			try {
				return Trafo.asInt(o1).compareTo(Trafo.asInt(o2));
//				return ((Integer)o1).compareTo((Integer)o2);
			} catch (ClassCastException cce) {
				return 0;
			} catch (NumberFormatException nfe1) {
				return Trafo.asString(o1).compareTo(Trafo.asString(o2));
			}
		} else if (mode==1) {
			try {
				return getFirstInt(o1).compareTo(getFirstInt(o2));
			} catch (NumberFormatException nfe) {
				return 0;
			}
		} else return Trafo.asString(o1).compareTo(Trafo.asString(o2));
	}*/
	
	/**
	 * returns the integer part of a numbered string
	 * @param o the numbered string
	 * @return the integer part as an integer
	 * @see maps.VectorComparator#compare(Object, Object, int)
	 */
/*	private Integer getFirstInt (Object o) {
		return Integer.parseInt(((Trafo.asString(o)).split(" "))[0]);
	}*/

}
