package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.RelationModels;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.relations.workers.AddChildRolesCriteria;
import org.tip.puck.net.workers.AttributeDescriptors;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class AddChildRolesInputDialog extends JDialog {

	private static final long serialVersionUID = -6186893836416427038L;

	private static final Logger logger = LoggerFactory.getLogger(AddChildRolesInputDialog.class);

	private RelationModels relationModels;
	private AttributeDescriptors attributeDescriptors;

	private final JPanel contentPanel = new JPanel();
	private AddChildRolesCriteria dialogCriteria;
	private static AddChildRolesCriteria lastCriteria = new AddChildRolesCriteria();
	private JComboBox cmbbxRelationModel;
	private JComboBox cmbbxEgoRole;
	private JComboBox cmbbxDateLabel;
	private JSpinner spnnrMaxAge;
	private JButton okButton;
	private JPanel alterRolesPanel;

	/**
	 * Create the dialog.
	 */
	public AddChildRolesInputDialog(final RelationModels relationModels, final AttributeDescriptors attributeDescriptors) {
		super();

		this.relationModels = relationModels;
		this.attributeDescriptors = attributeDescriptors;

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Add Child Roles Input Dialog");
		setIconImage(Toolkit.getDefaultToolkit().getImage(AddChildRolesInputDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				AddChildRolesInputDialog.this.dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 420, 376);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("max(100dlu;default):grow"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("max(100dlu;default):grow"),
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, }));
		{
			JLabel lblRelationModel = new JLabel("Relation Model:");
			this.contentPanel.add(lblRelationModel, "2, 2, right, default");
		}
		{
			this.cmbbxRelationModel = new JComboBox(relationModels.nameList().toArray());
			this.cmbbxRelationModel.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(final ItemEvent event) {
					// Update label list.
					//
					if (event.getStateChange() == ItemEvent.SELECTED) {
						//
						setRelationModel(AddChildRolesInputDialog.this.cmbbxRelationModel.getSelectedIndex(), null, null);
					}
				}
			});
			this.contentPanel.add(this.cmbbxRelationModel, "4, 2, fill, default");
		}
		{
			JLabel lblEgoRole = new JLabel("Ego Role:");
			this.contentPanel.add(lblEgoRole, "2, 4, right, default");
		}
		{
			this.cmbbxEgoRole = new JComboBox();
			this.contentPanel.add(this.cmbbxEgoRole, "4, 4, fill, default");
		}
		{
			JLabel lblAlterRoles = new JLabel("Alter Roles:");
			this.contentPanel.add(lblAlterRoles, "2, 6, right, top");
		}
		{
			JScrollPane alterRolesScrollPane = new JScrollPane();
			alterRolesScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			this.contentPanel.add(alterRolesScrollPane, "4, 6, fill, fill");
			{
				this.alterRolesPanel = new JPanel();
				alterRolesScrollPane.setViewportView(this.alterRolesPanel);
				this.alterRolesPanel.setLayout(new BoxLayout(this.alterRolesPanel, BoxLayout.Y_AXIS));
			}
		}
		{
			JLabel lblDateLabel = new JLabel("Date Label:");
			this.contentPanel.add(lblDateLabel, "2, 8, right, default");
		}
		{
			this.cmbbxDateLabel = new JComboBox();
			this.contentPanel.add(this.cmbbxDateLabel, "4, 8, fill, default");
		}
		{
			JLabel lblMaxAge = new JLabel("Max Age:");
			this.contentPanel.add(lblMaxAge, "2, 10, right, default");
		}
		{
			this.spnnrMaxAge = new JSpinner();
			this.spnnrMaxAge.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
			this.contentPanel.add(this.spnnrMaxAge, "4, 10");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						AddChildRolesInputDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				this.okButton = new JButton("Launch");
				this.okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						AddChildRolesCriteria criteria = getCriteria();

						if (criteria.getRelationModelName() == null) {
							//
							String title = "Invalid input";
							String message = "A relation model is required to launch.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

						} else if (criteria.getEgoRoleName() == null) {
							//
							String title = "Invalid input";
							String message = "An ego role is required to launch.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

						} else if (criteria.getAlterRoleNames().isEmpty()) {
							//
							String title = "Invalid input";
							String message = "Almost one alter role is required to launch.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

						} else if (criteria.getDateLabel() == null) {
							//
							String title = "Invalid input";
							String message = "A valid date label is required to launch.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						} else {
							//
							lastCriteria = criteria;
							AddChildRolesInputDialog.this.dialogCriteria = criteria;

							//
							setVisible(false);
						}
					}
				});
				this.okButton.setActionCommand("OK");
				buttonPane.add(this.okButton);
				getRootPane().setDefaultButton(this.okButton);
			}
		}

		// ////////////////////////
		setCriteria(lastCriteria);
	}

	/**
	 * 
	 * @return
	 */
	public AddChildRolesCriteria getCriteria() {
		AddChildRolesCriteria result;

		result = new AddChildRolesCriteria();

		//
		int currentRelationModelIndex = this.cmbbxRelationModel.getSelectedIndex();
		if (currentRelationModelIndex != -1) {
			//
			result.setRelationModelName(this.relationModels.get(currentRelationModelIndex).getName());
		}

		//
		int currentEgoRoleIndex = this.cmbbxEgoRole.getSelectedIndex();
		if (currentEgoRoleIndex != -1) {
			//
			result.setEgoRoleName(this.relationModels.get(currentRelationModelIndex).roles().get(currentEgoRoleIndex).getName());
		}
		logger.debug("EgoRoleName=" + result.getEgoRoleName());

		//
		result.getAlterRoleNames().clear();
		for (Component component : this.alterRolesPanel.getComponents()) {
			//
			if (((JCheckBox) component).isSelected()) {
				//
				result.getAlterRoleNames().add(((JCheckBox) component).getText());
			}
		}

		//
		result.setMaxAge((Integer) this.spnnrMaxAge.getValue());

		//
		int currentDateLabelIndex = this.cmbbxDateLabel.getSelectedIndex();
		if (currentDateLabelIndex != -1) {
			//
			result.setDateLabel(this.attributeDescriptors.findByRelationModelName(this.relationModels.get(currentRelationModelIndex).getName()).sort()
					.getByIndex(currentDateLabelIndex).getLabel());
		}

		logger.debug(result.toString());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public AddChildRolesCriteria getDialogCriteria() {
		AddChildRolesCriteria result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final AddChildRolesCriteria source) {
		//
		if (source != null) {
			//
			if (this.relationModels.isEmpty()) {
				//
				this.cmbbxRelationModel.setEnabled(false);
				this.cmbbxEgoRole.setEnabled(false);
				this.cmbbxDateLabel.setEnabled(false);

			} else {
				//
				this.cmbbxRelationModel.setEnabled(true);
				this.cmbbxEgoRole.setEnabled(true);
				this.cmbbxDateLabel.setEnabled(true);

				//
				RelationModel currentRelationModel = this.relationModels.getByName(source.getRelationModelName());
				if (currentRelationModel == null) {
					//
					currentRelationModel = this.relationModels.get(0);
				}

				//
				setRelationModel(this.relationModels.indexOf(currentRelationModel), source.getEgoRoleName(), source.getDateLabel());
			}
		}
	}

	/**
	 * 
	 * @param currentRelationModelIndex
	 */
	private void setRelationModel(final int currentRelationModelIndex, final String egoRoleName, final String dateLabel) {
		//
		RelationModel currentRelationModel = this.relationModels.get(currentRelationModelIndex);

		//
		if (currentRelationModel.roles().isEmpty()) {
			//
			this.cmbbxEgoRole.setEnabled(false);
			this.cmbbxDateLabel.setEnabled(false);

		} else {
			//
			this.cmbbxEgoRole.setModel(new DefaultComboBoxModel(currentRelationModel.roles().toNameList().toArray()));

			//
			Role currentEgoRole = currentRelationModel.roles().getByName(egoRoleName);
			if (currentEgoRole == null) {
				//
				currentEgoRole = currentRelationModel.roles().get(0);
			}

			this.cmbbxEgoRole.setSelectedIndex(currentRelationModel.roles().indexOf(currentEgoRole));

			//
			this.alterRolesPanel.removeAll();
			for (Role role : currentRelationModel.roles()) {
				//
				this.alterRolesPanel.add(new JCheckBox(role.getName()));
			}

			//
			StringList currentLabels = this.attributeDescriptors.findByRelationModelName(currentRelationModel.getName()).labels().sort();

			if (currentLabels.isEmpty()) {
				//
				this.cmbbxDateLabel.setEnabled(false);

			} else {
				//
				this.cmbbxDateLabel.setModel(new DefaultComboBoxModel(currentLabels.toArray()));

				//
				int currentDateLabelIndex = currentLabels.indexOf(dateLabel);
				if (currentDateLabelIndex == -1) {
					//
					currentDateLabelIndex = 0;
				}

				this.cmbbxDateLabel.setSelectedIndex(currentDateLabelIndex);
			}
		}

	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		/* AttributeFilter criteria = */showDialog(null, null);
	}

	/**
	 * Launch the application.
	 */
	public static AddChildRolesCriteria showDialog(final RelationModels relationModels, final AttributeDescriptors attributeDescriptors) {
		AddChildRolesCriteria result;

		//
		AddChildRolesInputDialog dialog = new AddChildRolesInputDialog(relationModels, attributeDescriptors);
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
