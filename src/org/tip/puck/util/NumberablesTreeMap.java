package org.tip.puck.util;

import java.util.Iterator;
import java.util.TreeMap;

/**
 * 
 * 
 * @author TIP
 */
public class NumberablesTreeMap<NumberableType extends Numberable> implements Numberables<NumberableType> {

	protected TreeMap<Integer, NumberableType> data;

	/**
	 * 
	 */
	public NumberablesTreeMap() {
		this.data = new TreeMap<Integer, NumberableType>();
	}

	/**
	 * 
	 */
	@Override
	public void clear() {
		this.data.clear();
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public boolean containsId(final int id) {
		boolean result;

		result = this.data.containsKey(id);

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public NumberableType getById(final int id) {
		NumberableType result;

		result = this.data.get(id);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public Integer[] getIds() {
		Integer[] result;

		result = new Integer[this.data.keySet().size()];
		this.data.keySet().toArray(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public boolean isEmpty() {
		boolean result;

		result = this.data.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<NumberableType> iterator() {
		Iterator<NumberableType> result;

		result = this.data.values().iterator();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public void put(final NumberableType indexedObject) {
		if (indexedObject != null) {
			this.data.put(indexedObject.getId(), indexedObject);
		}
	}

	/**
	 * 
	 * @param id
	 */
	@Override
	public void removeById(final int id) {
		this.data.remove(id);
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public int size() {
		int result;

		result = this.data.size();

		//
		return result;
	}
}
