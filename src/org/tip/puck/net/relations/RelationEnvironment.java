package org.tip.puck.net.relations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.tip.puck.net.Individual;
import org.tip.puck.net.KinType;
import org.tip.puck.net.relations.workers.RelationValuator;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.ToolBox;

import fr.devinsy.util.StringList;

public class RelationEnvironment {
	
	private Individual ego;
	private String egoRoleName;
	private List<String> roleNames;
	private List<String> relationModelNames;
	
	private Map<Individual,List<String>> relationsByAlter;
	private Map<String,Set<Individual>> altersByRoles;
//	private Map<String,List<String>> relationsByRoles;
	
	private Map<Individual,List<String>> rolesByAlter;
	private Map<Individual,List<Relation>> eventsByAlter;
	private Map<Integer,List<Individual>> coverage;
	
	private String impersonalAlterLabel;
	private List<String> impersonalRelations;
	
	private int threshold; // temporary solution to bridge call gaps, should not be in this class

	public RelationEnvironment(Collection<Relation> stations, Individual ego, String egoRoleName, List<String> roleNames, List<String> relationModelNames, String impersonalAlterLabel){
		
		this.ego = ego;
		this.egoRoleName = egoRoleName;
		this.roleNames = roleNames;
		this.relationModelNames = relationModelNames;
		this.impersonalAlterLabel = impersonalAlterLabel;
		
		altersByRoles = new HashMap<String,Set<Individual>>();
		altersByRoles.put("ALL", new HashSet<Individual>());
		altersByRoles.put("SELECTED", new HashSet<Individual>());
		for (String alterRoleName : roleNames){
			altersByRoles.put(alterRoleName, new HashSet<Individual>());
		}
		rolesByAlter = new HashMap<Individual, List<String>>();
		eventsByAlter = new HashMap<Individual, List<Relation>>();
		coverage = new TreeMap<Integer,List<Individual>>();
		
		//
		for (Relation station : stations){
			if (station.hasRole(ego, egoRoleName)){
				// Add parents as birth actors
				if (RelationValuator.isBirth(station)){
					for (Individual parent : ego.getParents()){
						if (!station.getIndividuals().contains(parent)){
							station.actors().add(new Actor(parent,new Role("OTHER")));
						}
					}
				}
				for (Actor actor : station.actors()){
					Individual alter = actor.getIndividual();
					String roleName = actor.getRole().getName();
					if (!roleName.equals(egoRoleName) || alter!=ego){
						altersByRoles.get("ALL").add(alter);
						if (!altersByRoles.containsKey(roleName)){
							altersByRoles.put(roleName, new HashSet<Individual>());
						}
						altersByRoles.get(actor.getRole().getName()).add(alter);
						if (roleNames.contains(roleName)){
							altersByRoles.get("SELECTED").add(alter);
						}
						List<String> roleNameList = rolesByAlter.get(alter);
						if (roleNameList == null){
							roleNameList = new ArrayList<String>();
							rolesByAlter.put(alter, roleNameList);
						}
						roleNameList.add(roleName);
						List<Relation> eventList = eventsByAlter.get(alter);
						if (eventList == null){
							eventList = new ArrayList<Relation>();
							eventsByAlter.put(alter, eventList);
						}
						if (!eventList.contains(station)){
							eventList.add(station);
						}
					}
				}
			}
		}
		
		for (Individual alter : eventsByAlter.keySet()){
			Integer cov = eventsByAlter.get(alter).size();
			List<Individual> list = coverage.get(cov);
			if (list == null){
				list = new ArrayList<Individual>();
				coverage.put(cov, list);
			}
			if (!list.contains(alter)){
				list.add(alter);
			}
		}
		
	}

	
	public void setAlterRelations (Collection<Relation> stations, Individual ego, String egoRoleName, String alterRoleName, List<String> relationModelNames, String pattern, String chainClassification){
		
		//
		relationsByAlter = NetUtils.getAlterRelations1(ego,altersByRoles.get(alterRoleName),ToolBox.stringsToInts(pattern),relationModelNames, chainClassification,null,null);
		
/*		relationsByRoles = new HashMap<String,List<String>>();
		for (String roleName : altersByRoles.keySet()){
			List<String> relations = new ArrayList<String>();
			relationsByRoles.put(roleName, relations);
			for (Individual alter : altersByRoles.get(roleName)){
				if (relationsByAlter.get(alter).size()==0){
					relations.add("UNKNOWN");
					System.err.println("Unknown relation of "+ego+" to "+roleName+" "+alter);
				} else {
					for (String relation : relationsByAlter.get(alter)){
						if (!relations.contains(relation)){
							relations.add(relation);
						}
					}
				}
			}
		}*/

		// Add impersonal relations
		impersonalRelations = new ArrayList<String>();
		for (Relation event : stations){
			for (String impersonalRelation : RelationValuator.getImpersonalRelationTypes(event, ego, egoRoleName, impersonalAlterLabel, relationsByAlter)){
				if (impersonalRelation!=null && !impersonalRelation.equals("TRANSITION") && !impersonalRelations.contains(impersonalRelation)){
					impersonalRelations.add(impersonalRelation);
				}
			}
		}

		// Add other kin types (for ego networks)
		for (KinType kinType : KinType.basicTypes()){
			for (Individual alter : ego.getKin(kinType)){
				if (relationsByAlter.get(alter)==null){
					List<String> list = new ArrayList<String>();
					list.add(kinType.toString());
					relationsByAlter.put(alter, list);
				}
			}
		}
	}
	
	public Set<Individual> getAltersByRole (String roleName){
		Set<Individual> result;
		
		result = altersByRoles.get(roleName);
		
		//
		return result;
		
		
	}
	
	public List<String> getRelationsByRole (String roleName){
		List<String> result;
		
//		result = relationsByRoles.get(roleName);
		
		result = new ArrayList<String>();
//		relationsByRoles.put(roleName, result);
		for (Individual alter : altersByRoles.get(roleName)){
			if (relationsByAlter.get(alter).size()==0){
				result.add("UNKNOWN");
				System.err.println("Unknown relation of "+ego+" to "+roleName+" "+alter);
			} else {
				for (String relation : relationsByAlter.get(alter)){
					if (!result.contains(relation)){
						result.add(relation);
					}
				}
			}
		}
		
		//
		return result;
		
		
	}
	
	public Integer getMaxCoverage (){
		Integer result;
		
		if (coverage.size()>0){
			result = ((TreeMap<Integer,List<Individual>>)coverage).lastKey();
		} else {
			result = 0;
		}
		//
		return result;
	}
	
	public List<Individual> getMaxCoverageAlters (){
		List<Individual> result;
		
		if (coverage.size()>1){
			result = coverage.get(getMaxCoverage());
		} else {
			result = new ArrayList<Individual>();
		}
		//
		return result; 
	}
	
	public List<String> getMaxCoverageRelations (){
		List<String> relations;
		
		relations = new ArrayList<String>();
		for (Individual alter : getMaxCoverageAlters()){
			for (String relation : relationsByAlter.get(alter)){
				if (!relations.contains(relation)){
					relations.add(relation);
				}
			}
		}
		//
		return relations;
	}
	
	public double getMeanCoverage (){
		double result;
		
		result = 0;
		
		if (coverage.size()>0){
			double sum = 0.;
			
			for (int i : coverage.keySet()){
				result += i;
				sum++;
			}
			
			result = MathUtils.percent(result, sum);
		}
		//
		return result;
	}


	public List<String> getImpersonalRelations() {
		return impersonalRelations;
	}
	
	public Set<Individual> getAllAlters(){
		return altersByRoles.get("ALL");
	}
	
	public Map<Individual, List<String>> getRelationsByAlter() {
		return relationsByAlter;
	}
	
	public List<String> getRelationsByAlter(Individual alter) {
		return relationsByAlter.get(alter);
	}

	public List<String> getRolessByAlter(Individual alter) {
		return rolesByAlter.get(alter);
	}


	public String getEgoRoleName() {
		return egoRoleName;
	}


	public List<String> getRoleNames() {
		return roleNames;
	}
	

	public Individual getEgo() {
		return ego;
	}


	public List<String> getRelationModelNames() {
		return relationModelNames;
	}


	public int getThreshold() {
		return threshold;
	}


	public void setThreshold(int threshold) {
		this.threshold = threshold;
	}


	public String getImpersonalAlterLabel() {
		return impersonalAlterLabel;
	}
	
	
	
	
	
	
	
}
