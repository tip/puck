package org.tip.puckgui.views;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.graphs.GraphType;

/**
 * 
 * @author TIP
 */
public class PajekExportCriteria {

	private GraphType graphType;
	private List<String> partitionLabels;
	private String targetFileName;

	/**
	 * 
	 */
	public PajekExportCriteria() {
		this.graphType = GraphType.OreGraph;
		this.partitionLabels = new ArrayList<String>();
		for (int index = 0; index < 5; index++) {
			this.partitionLabels.add("");
		}
		this.targetFileName = null;
	}

	public GraphType getGraphType() {
		return graphType;
	}

	public List<String> getPartitionLabels() {
		return partitionLabels;
	}

	public List<String> getPartitionLabelsNotBlank() {
		List<String> result;

		result = new ArrayList<String>();
		for (String label : partitionLabels) {
			if (StringUtils.isNotBlank(label)) {
				result.add(label);
			}
		}

		//
		return result;
	}

	public String getTargetFileName() {
		return targetFileName;
	}

	public void setGraphType(final GraphType graphType) {
		this.graphType = graphType;
	}

	public void setTargetFileName(final String fileName) {
		this.targetFileName = fileName;
	}

}
