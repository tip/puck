package org.tip.puck.partitions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.Numberable;
import org.tip.puck.util.Value;
import org.tip.puck.util.Values;

/**
 * 
 * @author TIP
 */
public class Partition<E> {
	
	public enum ValueCode {
		VALUES,
		ORDERED_VALUES;
	}

	private static final Logger logger = LoggerFactory.getLogger(Partition.class);

	private String label;
	private PartitionCriteria criteria;
	private Clusters<E> clusters;
	private HashMap<E, Value> itemToValueShortcuts;
	private HashSet<E> originalItems;
	private boolean pseudo = false;

	
	
	public boolean isPseudo() {
		return pseudo;
	}

	public void setPseudo(boolean pseudo) {
		this.pseudo = pseudo;
	}
	
	public int indexOf(Cluster<E> cluster){
		int result;
		
		result = 0;
		for (Cluster<E> thisCluster : clusters){
			if (thisCluster==cluster){
				break;
			}
			result++;
		}
		//
		return result;
	}

	/**
	 * 
	 */
	public Partition() {

		this.clusters = new Clusters<E>();
		this.itemToValueShortcuts = new HashMap<E, Value>();
	}

	/**
	 * 
	 */
	public Partition(final String label) {

		this.label = label;
		this.clusters = new Clusters<E>();
		this.itemToValueShortcuts = new HashMap<E, Value>();
	}

	/**
	 * 
	 * @param partition
	 */
	public void add(final Partition<E> partition) {

		for (Value value : partition.getValues()) {
			if (getCluster(value) == null) {
				putCluster(value);
			}
			for (E item : partition.getCluster(value).getItems()) {
				put(item, value);
			}
		}

		/*		for (E item : partition.getItems()){
					Value value = partition.itemToValueShortcuts.get(item);
					put(item,value);
				}*/
	}

	/**
	 * 
	 * @return
	 */
	public int clusteredItemsCount() {
		int result;

		result = 0;
		for (Cluster<E> cluster : getClusters()) {
			if (!cluster.isNull()) {
				result += cluster.count();
			}
		}
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Map<Integer, Integer> clusterSizeDistribution() {
		Map<Integer, Integer> result;

		result = new TreeMap<Integer, Integer>();

		for (Cluster<E> cluster : getClusters()) {
			if (cluster.getLabel() != null) {
				int size = cluster.size();
				Integer number = result.get(size);
				if (number == null) {
					result.put(size, 1);
				} else {
					result.put(size, number + 1);
				}
			}
		}
		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public boolean containsValue(final Value value) {
		boolean result;

		// TODO Improve algorithm.
		result = false;
		for (Cluster<E> cluster : getClusters()) {
			if (cluster.getValue().equals(value)) {
				result = true;
			}
		}

		return result;
	}

	/**
	 * Returns the cluster of an element.
	 * 
	 * @param item
	 * @return
	 */
	public Cluster<E> getCluster(final E item) {
		Cluster<E> result;

		if (this.itemToValueShortcuts.containsKey(item)) {
			Value value = getValue(item);
			result = getCluster(value);
		} else {
			result = null;
		}

		//
		return result;
	}

	/**
	 * Returns the cluster of a value.
	 * 
	 * @param value
	 * @return
	 */
	public Cluster<E> getCluster(final Value value) {
		Cluster<E> result;

		result = this.clusters.get(value);

		//
		return result;
	}

	public Clusters<E> getClusters() {
		return this.clusters;
	}

	public PartitionCriteria getCriteria() {
		return this.criteria;
	}

	/**
	 * 
	 * @return
	 */
	public String getCriteriaLabel() {
		String result;

		if (this.criteria == null) {
			result = null;
		} else {
			result = this.criteria.getLabel();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Set<E> getItems() {
		Set<E> result;

		result = this.itemToValueShortcuts.keySet();

		//
		return result;
	}


	/**
	 * 
	 * @return
	 */
	public List<E> getItemsAsList() {
		List<E> result;

		result = new ArrayList<E>(this.itemToValueShortcuts.keySet());

		//
		return result;
	}

	/**
	 * 
	 * @param comparator
	 * @return
	 */
	public List<E> getItemsAsSortedList(final Comparator<E> comparator) {
		List<E> result;

		result = new ArrayList<E>(this.itemToValueShortcuts.keySet());
		Collections.sort(result, comparator);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public String getLabel() {
		return this.label;
	}

	/**
	 * Returns the cluster value of a given item.
	 * 
	 * @param item
	 *            The vertex to be checked.
	 * 
	 * @return The cluster value of the vertex.
	 */
	public Value getValue(final E item) {
		Value result;

		result = this.itemToValueShortcuts.get(item);

		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public int getValueFrequency(final Value value) {
		int result;

		Cluster<E> cluster = this.clusters.get(value);
		if (cluster == null) {
			result = 0;
		} else {
			result = cluster.size();
		}

		//
		return result;
	}

	/**
	 * Returns the cluster value of a given item.
	 * 
	 * @param item
	 *            The vertex to be checked.
	 * 
	 * @return The cluster value of the vertex.
	 */
	public Value getValueNotNull(final E item) {
		Value result;

		result = getValue(item);

		if (result == null) {
			result = new Value(0);
		}

		//
		return result;
	}
	
	public Double valueSumByItems(){
		Double result;
		
		if (isNotNumeric()){
			
			result = null;
			
		} else {
			
			result = 0.;
			
			for (Cluster<E> cluster : clusters){
				
				result += cluster.size()*(cluster.getValue().doubleValue());
			}
		}
		//
		return result;
	}
	
	public Double valueMeanByItems(){
		Double result;
		
		if (isNotNumeric()){
			
			result = null;
			
		} else {
			
			result = valueSumByItems()/itemsCount();

		}
		//
		return result;
	}

	public Double valueSumByClusters(){
		Double result;
		
		if (isNotNumeric()){
			
			result = null;
			
		} else {
			
			result = 0.;
			
			for (Cluster<E> cluster : clusters){
				
				result += cluster.getValue().doubleValue();
			}
		}
		//
		return result;
	}
	
	public Double valueMeanByClusters(){
		Double result;
		
		if (isNotNumeric()){
			
			result = null;
			
		} else {
			
			result = valueSumByClusters()/size();

		}
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Collection<Value> getValues() {
		Collection<Value> result;

		result = this.getClusters().getValues();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Value> getValuesAsSortedList() {
		List<Value> result;

		result = new ArrayList<Value>(getValues());
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotNumeric() {
		boolean result;

		result = !isNumeric();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNumeric() {
		boolean result;

		if (this.label.equals("ID") || this.label.equals("ORDER")) {
			result = false;
		} else {
			result = this.clusters.isNumeric();
		}

		//
		return result;
	}


	/**
	 * 
	 * @return
	 */
	public int itemsCount() {
		int result;

		if (isPseudo()){
			
			result = getOriginalItems().size();
		
		} else {
			
			result = getItems().size();
			
		}

		//
		return result;
	}

	/**
	 * Gets the maximal Cluster.
	 * 
	 * @return the maximal Cluster
	 */
	public Cluster<E> maxCluster() {
		Cluster<E> result;

		result = null;
		for (Cluster<E> cluster : this.clusters) {
			if ((result == null) || (cluster.size() > result.size())) {
				result = cluster;
			}
		}
		

		


		//
		return result;
	}
	
	public int maxClusterSize(){
		int result;
		
		Cluster<E> maxCluster = maxCluster();
		if (maxCluster != null){
			result = maxCluster.size();
		} else {
			result = 0;
		}
		//
		return result;
		
	}
	
	public List<Double> clusterSizes(){
		List<Double> result;
		
		result = new ArrayList<Double>();
		
		for (Cluster<E> cluster : this.clusters){
			result.add(new Double(cluster.size()));
		}
		
		//
		return result;
	}
	
	public double concentration(){
		double result;
		
		result = MathUtils.herfindahl(clusterSizes());
		
		//
		return result;
	}

	public double maxShare() {
		
		return share(maxCluster());
	}

	/**
	 * Gets the maximal Cluster.
	 * 
	 * @return the maximal Cluster
	 */
	public Value maxValue() {
		Value result;

		result = null;
		for (Value value : this.itemToValueShortcuts.values()) {
			if (value != null && value.compareTo(result) > 0) {
				result = value;
			}
		}
		//
		return result;
	}
	
	public int nrSingletons (){
		int result;
		
		result = 0;
		for (Cluster<E> cluster : clusters){
			if (cluster.size()==1){
				result++;
			}
		}
		//
		return result;
	}
	
	public double singletonShare (){
		return MathUtils.percent(nrSingletons(), size());
	}

	/**
	 * 
	 * @return
	 */
	public double meanShare() {
		return MathUtils.percent(1, size());
	}

	/**
	 * 
	 * @param minSize
	 * @return
	 */
	public double meanShare(final int minSize) {
		double result;

		double clustersCount = 0.;

		for (Cluster<E> cluster : this.clusters) {
			if (cluster.size() >= minSize) {
				clustersCount += 1;
			}
		}

		result = MathUtils.percent(1, clustersCount);
		//
		return result;
	}

	/**
	 * 
	 * @param item
	 * @param partitionValue
	 */
	public boolean put(final E item, final Value partitionValue) {
		boolean result;
		
		//
		Cluster<E> cluster = this.clusters.get(partitionValue);
		if (cluster == null) {
			cluster = new Cluster<E>(partitionValue);
			this.clusters.put(cluster);
		}

		//
		result = cluster.put(item);

		//
		this.itemToValueShortcuts.put(item, partitionValue);
		//
		return result;
	}

	/**
	 * 
	 * @param items
	 * @param partitionValue
	 */
	public void putAll(final List<E> items, final Value partitionValue) {

		if (items != null) {
			//
			if (items.isEmpty()) {
				
				putCluster(partitionValue);
				
			} else {
				//
				for (E item : items) {
										
					put(item, partitionValue);
				}
			}
		}
	}
	
	public void putClusters(){
		putClusters(criteria.getValues());
	}
	
	
	public void putClusters(final Values values){
		if (values!=null){
			for (Value value : values){
				putCluster(value);
			}
		}
	}

	/**
	 * 
	 * @param value
	 */
	public void putCluster(final Value value) {
		Cluster<E> cluster = new Cluster<E>(value);
		this.clusters.put(cluster);
	}
	
	/**
	 * 
	 * @param cluster
	 */
	public Cluster<E> addCluster(final Cluster<E> cluster){
		Cluster<E> result;
		
		result = getCluster(cluster.getValue());
		if (result==null) {
			result = new Cluster<E>(cluster.getValue());
			this.clusters.put(result);
		}
		for (E item : cluster.getItems()){
			result.put(item);
		}
		//
		return result;
	}

	/**
	 * 
	 * @param cluster
	 * @return
	 */
	public Cluster<E> removeCluster(final Cluster<E> cluster) {
		Cluster<E> result;

		result = this.clusters.remove(cluster.getValue());
		for (E item : cluster.getItems()) {
			this.itemToValueShortcuts.remove(item);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param item
	 * @return
	 */
	public Cluster<E> removeCluster(final E item) {
		Cluster<E> result;

		result = removeCluster(getCluster(item));

		//
		return result;
	}

	/**
	 * 
	 * @param item
	 */
	public void removeItem(final E item) {

		getCluster(item).remove(item);
		this.itemToValueShortcuts.remove(item);

	}

	/**
	 * 
	 * @param criteria
	 */
	public void setCriteria(final PartitionCriteria criteria) {
		this.criteria = criteria;
	}

	/**
	 * 
	 * @param label
	 */
	public void setLabel(final String label) {
		this.label = label;
	}

	/**
	 * Gets the share of a cluster (the percentage of nodes contained in the
	 * cluster).
	 * 
	 * @param c
	 *            the cluster
	 */
	public double share(final Cluster<E> cluster) {
		double result;

		int itemsCount = itemsCount();

		if (itemsCount == 0) {
			result = 0;
		} else {
			result = MathUtils.percent(cluster.count(), itemsCount);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size() {
		int result;

		result = this.clusters.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int nonNullClusterCount() {
		int result;

		result = 0;
		for (Cluster<E> cluster : clusters){
			if (!cluster.isNull()){
				result++;
			}
		}
		//
		return result;
	}

	/**
	 * 
	 * @param item
	 * @param cluster1
	 * @param cluster2
	 */
	public void swap(final E item, final Cluster<E> cluster1, final Cluster<E> cluster2) {

		//
		if (item != null && cluster1 != null && cluster1.getItems().contains(item)) {
			cluster1.remove(item);
			put(item, cluster2.getValue());
		}
	}

	/**
	 * 
	 * @param item
	 * @param value1
	 * @param value2
	 */
	public void swap(final E item, final Value value1, final Value value2) {

		//
		Cluster<E> cluster = this.clusters.get(value2);
		if (item != null && cluster != null && cluster.getItems().contains(item)) {
			cluster.remove(item);
			put(item, value2);
		}
	}
	
	public void changeClusterValue(final Cluster<E> cluster, final Value value){
		
		//
		if (value != null && cluster != null) {
			for (Object item : cluster.getItems().toArray()){
				put((E)item, value);
			}
			clusters.remove(cluster.getValue());
//			itemToValueShortcuts.remove(cluster);
		}
	}
	
	public String toString(){
		String result;
		
		result = "";
		
		for (Cluster<E> cluster: this.clusters.toListSortedByValue()){
			if (result.length()>0){
				result+=", ";
			}
			result+=cluster.getValue()+" ["+cluster.getItemsAsString()+"]";
		}
		
		//
		return result;
	}
	
	/**
	 * weak check - only one layered value is needed, not all
	 * @return
	 */
	public boolean isMulti (){
		boolean result;
		
		result = false;
		
		for (Value value : getValues()){
			
			if (value!=null && value.isPartition()){
				
				result = true;
				break;
			}
		}
		
		//
		return result;
	}
	
	public <V extends Numberable> Partition<V> aggregate (PartitionCriteria criteria) throws PuckException {
		Partition<V> result;
		
		result = new Partition<V>();
		result.setLabel(label);
		
		for (Cluster<E> cluster : clusters){

			Value value = cluster.getValue();
			
			if (value.isPartition()){
				
				Partition<V> partition = (Partition<V>)value.partitionValue();
				
				for (Cluster<V> subCluster : partition.getClusters()){
					
					Value subValue = subCluster.getValue();
					
					if (subValue == null){
						subValue = new Value("NONE");
					}
					
					result.putAll(subCluster.getItems(), subValue);
				}

			} else if (value!=null && !value.equals(new Value("NONE"))){
				
				throw PuckExceptions.INVALID_PARAMETER.create((Exception)null, "value is not a partition: "+label+" "+cluster.getLabel()+" = "+value);
				
			}
		}
		
		result = PartitionMaker.create(result, criteria);
		
		//
		return result;
	}
	
	public String valueCode (ValueCode mode){
		String result;
		
		if (mode == ValueCode.VALUES) {
			
			result = "";
			for (Cluster<E> cluster : getClusters().toListSortedByValue()){
				
				result += cluster.getValue()+" ";
			}
			
		} else if (mode == ValueCode.ORDERED_VALUES){
			
			result = "";
			for (Cluster<E> cluster : getClusters().toListSortedByDescendingSize()){
				
				result += cluster.getValue()+" ";
			}
			
		} else {
			
			result = null;
		}
		
		//
		return result;
	}
	
	public <V> Partition<E> revalue (ValueCode mode) throws PuckException {
		Partition<E> result;
		
		result = new Partition<E>();
		
		for (Cluster<E> cluster : clusters){

			Value value = cluster.getValue();
			
			if (value.isPartition()){
				
				Partition<V> partition = (Partition<V>)value.partitionValue();
									
				result.putAll(cluster.getItems(), new Value(partition.valueCode(mode)));
				
			} else {
				
				throw PuckExceptions.INVALID_PARAMETER.create((Exception)null, "value is not a partition: "+value);
				
			}
		}
		//
		return result;

		
		
	}
	
	public boolean hasListValues(){
		boolean result;
		
		result = false;
		
		for (Value value : itemToValueShortcuts.values()){
			
			if (value == null){
				continue;
			} else if (value.isList()){
				result = true;
			}
			break;
		}
		//
		return result;
	}

	public void setOriginalItems(HashSet<E> originalItems) {
		this.originalItems = originalItems;
	}

	public HashSet<E> getOriginalItems() {
		return originalItems;
	}
	
	
	
	


	
	
	
	
}
