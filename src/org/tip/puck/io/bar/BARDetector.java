package org.tip.puck.io.bar;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckManager;
import org.tip.puck.io.bar.BARTXTFile.Format;
import org.tip.puck.io.bar.BARTXTFile.Status;
import org.tip.puck.io.ods.ODSBufferedReader;
import org.tip.puck.io.xls.XLSBufferedReader;

/**
 * This class represents a TXT File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class BARDetector {

	private static final Logger logger = LoggerFactory.getLogger(BARDetector.class);

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static boolean assertIsBlankOrID(final String source) {
		boolean result;

		if (StringUtils.isBlank(source) || NumberUtils.isDigits(source) || StringUtils.startsWith(source, "?") || StringUtils.endsWith(source, "?")
				|| StringUtils.equals(source, "-") || StringUtils.equals(source, "U")) {
			result = true;

		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param block
	 * @return
	 */
	public static boolean assertIsIndividualLine(final String source) {
		boolean result;

		if (StringUtils.isBlank(source)) {
			//
			result = false;

		} else {
			//
			String[] tokens = source.split("\\t");

			//
			if (tokens.length == 0) {
				//
				result = false;

			} else if ((tokens.length > 0) && (!NumberUtils.isNumber(tokens[0]))) {
				//
				result = false;

			} else {
				// From column index 3, tokens must be void, blank or ID.
				boolean ended = false;
				result = true;
				int index = 3;
				while (!ended) {
					if (index < tokens.length) {
						String token = tokens[index];
						if (!assertIsBlankOrID(token)) {
							ended = true;
							result = false;
						} else {
							index += 1;
						}
					} else {
						ended = true;
						result = true;
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * This method checks if a stream is a BAR one.
	 * 
	 * @param in
	 *            stream to check.
	 * 
	 * @return true if the stream is BAR one.
	 * @throws Exception
	 */
	public static boolean isBAR(final BufferedReader in) throws Exception {
		boolean result;

		//
		if (in == null) {
			//
			result = false;

		} else {
			//
			Format format = BARTXTFile.readLabelsLine(in, Status.OPTIONAL).getFormat();

			//
			if (format == BARTXTFile.Format.TWOMODE) {
				//
				logger.debug("TWOMODE detected => NOT BAR");
				result = true;

			} else {
				// Check individual lines.
				boolean ended = false;
				int individualLineCount = 0;
				result = false;
				while (!ended) {
					//
					in.mark(BARTXTFile.MAX_LINE_SIZE);
					String line = in.readLine();

					logger.debug("source = " + line);

					if (StringUtils.isBlank(line)) {
						//
						ended = true;

						//
						result = true;

					} else if (line.matches("^\\D.*$")) {
						//
						ended = true;

						//
						in.reset();
						result = true;

					} else {
						//
						if (assertIsIndividualLine(line)) {
							//
							individualLineCount += 1;

						} else {
							//
							ended = true;

							//
							logger.debug("Detected not individual line => NOT BAR");
							logger.debug("line=" + line);
							result = false;
						}
					}
				}

				//
				if (result) {
					if (individualLineCount < 2) {
						//
						logger.debug("Individual line count < 2 => NOT BAR");
						result = false;

					} else {
						//
						result = true;
					}
				}
			}

		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * This method checks if a file is a BAR one.
	 * 
	 * @param file
	 *            file to check.
	 * 
	 * @return true if the file is BAR one.
	 */
	public static boolean isBAR(final File source) {
		boolean result;

		result = isBAR(source, PuckManager.DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * This method checks if a file is a BAR one.
	 * 
	 * @param file
	 *            file to check.
	 * 
	 * @return true if the file is BAR one.
	 */
	public static boolean isBAR(final File source, final String charsetName) {
		boolean result;

		//
		if (source == null) {
			//
			result = false;

		} else if (source.getName().matches("^.*\\.[Bb][Aa][Rr]\\.(ods|ODS|txt|TXT|xls|XLS)$")) {
			//
			result = true;

		} else if (source.getName().matches("^.*\\.[Ii][Uu][Rr]\\.(ods|ODS|txt|TXT|xls|XLS)$")) {
			//
			result = false;

		} else {
			//
			BufferedReader in = null;
			try {
				//
				String fileName = source.getName().toLowerCase();

				//
				if (fileName.endsWith(".ods")) {
					//
					in = new ODSBufferedReader(source);

				} else if (fileName.endsWith(".txt")) {
					//
					in = new BufferedReader(new InputStreamReader(new FileInputStream(source), charsetName));

				} else if (fileName.endsWith(".xls")) {
					//
					in = new XLSBufferedReader(source);

				} else {
					//
					in = null;
				}

				//
				result = isBAR(in);

			} catch (Exception exception) {
				//
				exception.printStackTrace();
				result = false;

			} finally {
				//
				IOUtils.closeQuietly(in);
			}
		}

		//
		return result;
	}
}
