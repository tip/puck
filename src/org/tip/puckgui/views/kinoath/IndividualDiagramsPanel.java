package org.tip.puckgui.views.kinoath;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Hashtable;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.kinoath.IndividualAroundWorker;
import org.tip.puck.kinoath.KinOathDiagram;
import org.tip.puck.kinoath.PruningCriteria;
import org.tip.puck.kinoath.Puck2KinOath;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.models.IndividualsCellRenderer;
import org.tip.puckgui.models.IndividualsModel;
import org.tip.puckgui.views.MainWindow;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class IndividualDiagramsPanel extends JPanel {

	private static final long serialVersionUID = -6495462880561996773L;

	private static final Logger logger = LoggerFactory.getLogger(IndividualDiagramsPanel.class);

	int reportCounter = 0;
	private JPanel thisJPanel;
	private JTextField txtfldSearchIndividual;
	private NetGUI netGUI;
	JList individualList;
	JScrollPane individualsScrollPane;
	KinOathDiagram diagram;
	private KinOathDiagramPanel diagramScrollPanel;
	private JSlider parentsSlider;
	private JSlider partnersSlider;
	private JSlider childrenSlider;
	private JCheckBox chckbxAddCollaterals;
	private JCheckBox chckbxAddAffines;

	private static int SLIDER_MAX = 6;
	private JPanel pruningPanel;

	// setCriteria() method send slider event which need to be ignored.
	private boolean sliderActive = true;

	/**
	 * Initialize the contents of the frame.
	 */
	public IndividualDiagramsPanel(final NetGUI guiManager) {

		//
		this.thisJPanel = this;
		this.netGUI = guiManager;

		Hashtable<Integer, JComponent> sliderLabels = new Hashtable<Integer, JComponent>();
		sliderLabels.put(new Integer(0), new JLabel("+0"));
		sliderLabels.put(new Integer(1), new JLabel("+1"));
		sliderLabels.put(new Integer(2), new JLabel("+2"));
		sliderLabels.put(new Integer(3), new JLabel("+3"));
		sliderLabels.put(new Integer(4), new JLabel("+4"));
		sliderLabels.put(new Integer(5), new JLabel("+5"));
		sliderLabels.put(new Integer(6), new JLabel("+∞"));

		Object defaultPaintValue = UIManager.get("Slider.paintValue");
		UIManager.put("Slider.paintValue", false);
		//

		//
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		JSplitPane individualsSplit = new JSplitPane();
		add(individualsSplit);
		individualsSplit.setAlignmentX(Component.CENTER_ALIGNMENT);

		JPanel individualsPanel = new JPanel();
		individualsPanel.setMinimumSize(new Dimension(200, 10));
		individualsSplit.setLeftComponent(individualsPanel);
		individualsPanel.setLayout(new BoxLayout(individualsPanel, BoxLayout.Y_AXIS));

		this.individualsScrollPane = new JScrollPane();
		this.individualsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		individualsPanel.add(this.individualsScrollPane);

		this.individualList = new JList();
		this.individualList.setDoubleBuffered(true);
		this.individualList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(final ListSelectionEvent event) {
				logger.debug("event = " + event.getValueIsAdjusting() + " " + event.getFirstIndex() + " " + event.getLastIndex() + " "
						+ IndividualDiagramsPanel.this.individualList.getSelectedIndex());

				if (!event.getValueIsAdjusting()) {
					// Selected.
					if (IndividualDiagramsPanel.this.individualList.getSelectedIndex() != -1) {
						//
						Individual individual = (Individual) ((JList) event.getSource()).getModel().getElementAt(
								IndividualDiagramsPanel.this.individualList.getSelectedIndex());
						updateIndividualDiagram(individual);
					}
				}
			}
		});
		this.individualList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		this.individualList.setCellRenderer(new IndividualsCellRenderer());
		this.individualList.setModel(new IndividualsModel(null));
		this.individualsScrollPane.setViewportView(this.individualList);

		JPanel diagramZonePanel = new JPanel();
		individualsSplit.setRightComponent(diagramZonePanel);
		diagramZonePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		diagramZonePanel.setAlignmentY(Component.TOP_ALIGNMENT);
		diagramZonePanel.setLayout(new BorderLayout(0, 0));

		JPanel parametersPanel = new JPanel();
		diagramZonePanel.add(parametersPanel, BorderLayout.NORTH);
		parametersPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));

		this.pruningPanel = new JPanel();
		parametersPanel.add(this.pruningPanel, "2, 2");
		this.pruningPanel.setBorder(new TitledBorder(null, "Pruning", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.pruningPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));

		JLabel lblParentDepth = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualDiagramPanel.lblParentDepth.text"));
		this.pruningPanel.add(lblParentDepth, "2, 2, default, center");

		this.parentsSlider = new JSlider();
		this.parentsSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(final ChangeEvent event) {
				// State change.
				if ((event.getSource() instanceof JSlider) && (!((JSlider) event.getSource()).getValueIsAdjusting())) {
					//
					if (IndividualDiagramsPanel.this.sliderActive) {
						//
						updateIndividualDiagram();
					}
				}
			}
		});
		this.parentsSlider.setSnapToTicks(true);
		this.pruningPanel.add(this.parentsSlider, "4, 2");
		this.parentsSlider.setMajorTickSpacing(1);
		this.parentsSlider.setValue(1);
		this.parentsSlider.setMaximum(6);

		this.chckbxAddCollaterals = new JCheckBox(ResourceBundle.getBundle("org.tip.puckgui.messages").getString(
				"IndividualDiagramPanel.chckbxAddCollaterals.text"));
		this.chckbxAddCollaterals.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// State change.
				updateIndividualDiagram();
			}
		});
		this.pruningPanel.add(this.chckbxAddCollaterals, "6, 2");

		JLabel lblPartner = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualDiagramPanel.lblPartner.text"));
		this.pruningPanel.add(lblPartner, "2, 4, default, center");

		this.partnersSlider = new JSlider();
		this.partnersSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(final ChangeEvent event) {
				// State change.
				if ((event.getSource() instanceof JSlider) && (!((JSlider) event.getSource()).getValueIsAdjusting())) {
					//
					if (IndividualDiagramsPanel.this.sliderActive) {
						//
						updateIndividualDiagram();
					}
				}
			}
		});
		this.pruningPanel.add(this.partnersSlider, "4, 4");
		this.partnersSlider.setMajorTickSpacing(1);
		this.partnersSlider.setValue(3);
		this.partnersSlider.setMaximum(6);
		this.partnersSlider.setSnapToTicks(true);

		JLabel lblChildren = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualDiagramPanel.lblChildren.text"));
		this.pruningPanel.add(lblChildren, "2, 6, default, top");

		this.childrenSlider = new JSlider();
		this.childrenSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(final ChangeEvent event) {
				// State change.
				if ((event.getSource() instanceof JSlider) && (!((JSlider) event.getSource()).getValueIsAdjusting())) {
					//
					if (IndividualDiagramsPanel.this.sliderActive) {
						//
						updateIndividualDiagram();
					}
				}
			}
		});
		this.pruningPanel.add(this.childrenSlider, "4, 6");
		this.childrenSlider.setValue(2);
		this.childrenSlider.setMajorTickSpacing(1);
		this.childrenSlider.setMaximum(6);
		this.childrenSlider.setSnapToTicks(true);
		this.childrenSlider.setPaintLabels(true);
		this.childrenSlider.setPaintTicks(true);

		this.chckbxAddAffines = new JCheckBox(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualDiagramPanel.chckbxAddAffines.text"));
		this.chckbxAddAffines.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// State change.
				updateIndividualDiagram();
			}
		});
		this.pruningPanel.add(this.chckbxAddAffines, "6, 6, default, top");

		JPanel panel = new JPanel();
		parametersPanel.add(panel, "4, 2, fill, fill");
		panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, },
				new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JButton btnDirect = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualDiagramPanel.btnDirect.text"));
		btnDirect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Button Direct.
				setCriteria(PruningCriteria.DIRECT);
				updateIndividualDiagram();
			}
		});
		panel.add(btnDirect, "2, 2");

		JButton btnGrand = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualDiagramPanel.btnGrand.text"));
		btnGrand.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Button Grand.
				setCriteria(PruningCriteria.GRAND);
				updateIndividualDiagram();
			}
		});

		JButton btnExport = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualDiagramPanel.btnExport.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnExport.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Export.
				try {
					IndividualDiagramsPanel.this.diagramScrollPanel.export();

				} catch (IOException exception) {
					// TODO Auto-generated catch block
					exception.printStackTrace();
				} catch (PuckException exception) {
					// TODO Auto-generated catch block
					exception.printStackTrace();
				}
			}
		});
		panel.add(btnExport, "4, 2");
		panel.add(btnGrand, "2, 4");

		JButton btnGreat = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualDiagramPanel.btnGreat.text"));
		btnGreat.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Button Great.
				setCriteria(PruningCriteria.GREAT);
				updateIndividualDiagram();
			}
		});

		JButton btnCopy = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualDiagramPanel.btnCopy.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnCopy.setToolTipText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualDiagramPanel.btnCopy.toolTipText")); //$NON-NLS-1$ //$NON-NLS-2$
		btnCopy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Copy to clipboard.
				IndividualDiagramsPanel.this.diagramScrollPanel.copyToClipboard();
			}
		});
		panel.add(btnCopy, "4, 4");
		panel.add(btnGreat, "2, 6");

		JButton btnFull = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualDiagramPanel.btnFull.text"));
		btnFull.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Button FULL.
				setCriteria(PruningCriteria.FULL);
				updateIndividualDiagram();
			}
		});
		panel.add(btnFull, "2, 8");

		JButton btnResetZoom = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("GlobalDiagramPanel.btnResetZoom.text")); //$NON-NLS-1$ //$NON-NLS-2$
		panel.add(btnResetZoom, "4, 8");

		btnResetZoom.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				IndividualDiagramsPanel.this.diagramScrollPanel.resetZoom();
			}
		});
		this.childrenSlider.setLabelTable(sliderLabels);

		JPanel diagramPanel = new JPanel();
		diagramZonePanel.add(diagramPanel);

		JPanel bottomButtonsPanel = new JPanel();
		add(bottomButtonsPanel);
		bottomButtonsPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		bottomButtonsPanel.setLayout(new BoxLayout(bottomButtonsPanel, BoxLayout.X_AXIS));

		JButton btnIndividualsPrevious = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnPrevious.text"));
		btnIndividualsPrevious.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Previous individual.
				int selectedIndex = IndividualDiagramsPanel.this.individualList.getSelectedIndex();
				if (selectedIndex == -1) {
					int size = IndividualDiagramsPanel.this.individualList.getModel().getSize();
					if (size != 0) {
						selectedIndex = size - 1;
						IndividualDiagramsPanel.this.individualList.setSelectedIndex(selectedIndex);
						IndividualDiagramsPanel.this.individualList.ensureIndexIsVisible(selectedIndex);
					}
				} else if (selectedIndex > 0) {
					selectedIndex -= 1;
					IndividualDiagramsPanel.this.individualList.setSelectedIndex(selectedIndex);
					IndividualDiagramsPanel.this.individualList.ensureIndexIsVisible(selectedIndex);
				}
			}
		});

		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		bottomButtonsPanel.add(horizontalStrut_6);

		JButton btnClose = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualDiagramPanel.btnClose.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				IndividualDiagramsPanel.this.netGUI.closeCurrentTab();
			}
		});
		bottomButtonsPanel.add(btnClose);

		Component horizontalGlue_1 = Box.createHorizontalGlue();
		bottomButtonsPanel.add(horizontalGlue_1);

		JButton btnSort = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.btnSort.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnSort.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Sort individual list.

				Individual selectedIndividual = getSelectedIndividual();
				((IndividualsModel) IndividualDiagramsPanel.this.individualList.getModel()).touchSorting();
				if (selectedIndividual != null) {
					//
					select(selectedIndividual);
				}
			}
		});
		bottomButtonsPanel.add(btnSort);

		Component horizontalGlue_4 = Box.createHorizontalGlue();
		bottomButtonsPanel.add(horizontalGlue_4);
		bottomButtonsPanel.add(btnIndividualsPrevious);

		JButton btnIndividualsNext = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnNext.text"));
		btnIndividualsNext.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Next individual.
				int selectedIndex = IndividualDiagramsPanel.this.individualList.getSelectedIndex();
				int size = IndividualDiagramsPanel.this.individualList.getModel().getSize();
				if (selectedIndex == -1) {
					if (size != 0) {
						selectedIndex = 0;
						IndividualDiagramsPanel.this.individualList.setSelectedIndex(selectedIndex);
						IndividualDiagramsPanel.this.individualList.ensureIndexIsVisible(selectedIndex);
					}
				} else if (selectedIndex < size - 1) {
					selectedIndex += 1;
					IndividualDiagramsPanel.this.individualList.setSelectedIndex(selectedIndex);
					IndividualDiagramsPanel.this.individualList.ensureIndexIsVisible(selectedIndex);
				}
			}
		});
		bottomButtonsPanel.add(btnIndividualsNext);

		Component horizontalGlue = Box.createHorizontalGlue();
		bottomButtonsPanel.add(horizontalGlue);

		JPanel panel_13 = new JPanel();
		bottomButtonsPanel.add(panel_13);
		panel_13.setLayout(new BoxLayout(panel_13, BoxLayout.X_AXIS));

		JLabel lblIndividualsSearch = new JLabel(" ");
		lblIndividualsSearch.setToolTipText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.lblSearch.text")); //$NON-NLS-1$ //$NON-NLS-2$
		lblIndividualsSearch.setIcon(new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/find.png")));
		bottomButtonsPanel.add(lblIndividualsSearch);
		lblIndividualsSearch.setMinimumSize(new Dimension(300, 15));

		this.txtfldSearchIndividual = new JTextField();
		this.txtfldSearchIndividual.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Search individual.
				String pattern = IndividualDiagramsPanel.this.txtfldSearchIndividual.getText();
				logger.debug("Search individual=[" + pattern + "]");
				if (StringUtils.isNotBlank(pattern)) {
					int index = ((IndividualsModel) IndividualDiagramsPanel.this.individualList.getModel()).nextSearchedIndividualIndex(pattern);
					if (index != -1) {
						IndividualDiagramsPanel.this.individualList.setSelectedIndex(index);
						IndividualDiagramsPanel.this.individualList.ensureIndexIsVisible(index);
					}
				}
			}
		});
		bottomButtonsPanel.add(this.txtfldSearchIndividual);
		this.txtfldSearchIndividual.setMaximumSize(new Dimension(500, 50));
		this.txtfldSearchIndividual.setText("");
		this.txtfldSearchIndividual.setColumns(15);

		this.diagramScrollPanel = new KinOathDiagramPanel();

		diagramPanel.setLayout(new BorderLayout(0, 0));
		diagramPanel.add(this.diagramScrollPanel);

		JLabel lblHelp = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualDiagramPanel.lblHelp.text")); //$NON-NLS-1$ //$NON-NLS-2$
		diagramZonePanel.add(lblHelp, BorderLayout.SOUTH);

		// ///////////////////////////////
		// Restore default UI settings.
		UIManager.put("Slider.paintValue", defaultPaintValue);

		setCriteria(PruningCriteria.GRAND);
	}

	/**
	 * 
	 */
	public PruningCriteria getCriteria() {
		PruningCriteria result;

		result = new PruningCriteria();

		if (this.parentsSlider.getValue() == SLIDER_MAX) {
			//
			result.setParentsDepth(Integer.MAX_VALUE);

		} else {
			//
			result.setParentsDepth(this.parentsSlider.getValue());
		}

		if (this.partnersSlider.getValue() == SLIDER_MAX) {
			//
			result.setPartnersDepth(Integer.MAX_VALUE);

		} else {
			//
			result.setPartnersDepth(this.partnersSlider.getValue());
		}

		if (this.childrenSlider.getValue() == SLIDER_MAX) {
			//
			result.setChildrenDepth(Integer.MAX_VALUE);

		} else {
			//
			result.setChildrenDepth(this.childrenSlider.getValue());
		}

		result.setCollaterals(this.chckbxAddCollaterals.isSelected());
		result.setAffines(this.chckbxAddAffines.isSelected());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individual getSelectedIndividual() {
		Individual result;

		if (this.individualList.getSelectedIndex() == -1) {
			//
			result = null;

		} else {
			//
			result = (Individual) ((IndividualsModel) this.individualList.getModel()).getElementAt(this.individualList.getSelectedIndex());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 */
	public void select(final Individual individual) {
		int individualIndex = ((IndividualsModel) this.individualList.getModel()).indexOf(individual);

		if ((individualIndex >= 0) && (individualIndex < ((IndividualsModel) this.individualList.getModel()).getSize())) {
			this.individualList.setSelectedIndex(individualIndex);
			this.individualList.ensureIndexIsVisible(individualIndex);
		} else if (((IndividualsModel) this.individualList.getModel()).getSize() != 0) {
			this.individualList.setSelectedIndex(0);
			this.individualList.ensureIndexIsVisible(0);
		} else {
			updateIndividualDiagram(null);
		}
	}

	/**
	 * 
	 * @param target
	 */
	public void selectByIndex(final int individualIndex) {

		if ((individualIndex >= 0) && (individualIndex < ((IndividualsModel) this.individualList.getModel()).getSize())) {
			this.individualList.setSelectedIndex(individualIndex);
			this.individualList.ensureIndexIsVisible(individualIndex);
		} else if (((IndividualsModel) this.individualList.getModel()).getSize() != 0) {
			this.individualList.setSelectedIndex(0);
			this.individualList.ensureIndexIsVisible(0);
		} else {
			updateIndividualDiagram(null);
		}
	}

	/**
	 * 
	 */
	public void setCriteria(final PruningCriteria source) {
		//
		this.sliderActive = false;

		//
		this.parentsSlider.setValue(source.getParentsDepth());
		this.partnersSlider.setValue(source.getPartnersDepth());
		this.childrenSlider.setValue(source.getChildrenDepth());
		this.chckbxAddCollaterals.setSelected(source.isCollaterals());
		this.chckbxAddAffines.setSelected(source.isAffines());

		//
		this.sliderActive = true;
	}

	/**
	 * 
	 */
	public void setFocusOnFind() {
		this.txtfldSearchIndividual.requestFocus();
	}

	/**
	 * 
	 */
	public void update() {
		//
		int selectedIndividual = this.individualList.getSelectedIndex();

		//
		((IndividualsModel) this.individualList.getModel()).setSource(this.netGUI.getCurrentIndividuals());

		//
		selectByIndex(selectedIndividual);
	}

	/**
	 * 
	 */
	public void update(final Individual individual) {
		//
		((IndividualsModel) this.individualList.getModel()).setSource(this.netGUI.getCurrentIndividuals());

		//
		select(individual);
	}

	/**
	 * 
	 */
	public void updateIndividualDiagram() {
		//
		updateIndividualDiagram(getSelectedIndividual());
	}

	/**
	 * 
	 */
	public void updateIndividualDiagram(final Individual source) {
		//
		if (source == null) {
			//
			logger.debug("updateIndividualDiagram(null)");

		} else {
			//
			logger.debug("updateIndividualDiagram(" + source.getId() + ")");

			Individuals target = IndividualAroundWorker.searchAround(source, getCriteria());
			logger.debug("around count=" + target.size());

			//
			this.diagramScrollPanel.updateSource(Puck2KinOath.convert(target));
		}
	}
}
