package org.tip.puck.visualization.exporter.implementations;


import java.io.File;
import java.io.IOException;
import org.gephi.io.exporter.api.ExportController;
import org.gephi.io.exporter.spi.GraphExporter;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;
import org.tip.puck.visualization.exporter.Exporter;

/**
 * @date May 12, 2012
 * @author Edoardo Savoia
 */
public class ExporterByExtension implements Exporter {

    
    /*
     * FileType is get from filename extension, if absent use default (gexf)
     */
    @Override
    public void exportToFile(File file, Workspace workspace) {
	//Export full graph
	ExportController ec = Lookup.getDefault().lookup(ExportController.class);

	try {
	    ec.exportFile(file,workspace);
	} catch (Exception ex) {
	    // ex.printStackTrace();
	    // bad extension or IO
	    GraphExporter exporter = (GraphExporter) ec.getExporter("gexf");
	    exporter.setWorkspace(workspace);
	    try {
		System.out.println("second try");
		ec.exportFile(file, exporter);
	    } catch (IOException exc) {
		System.out.println("second catched");
		exc.printStackTrace();
	    }
	}
    }

}
