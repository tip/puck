package org.tip.puck.io.pl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.util.LogHelper;

/**
 * This class represents a TXT File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class PLFile {

	private static final Logger logger = LoggerFactory.getLogger(PLFile.class);
	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	// public static final Pattern PLLINE_PATTERN =
	// Pattern.compile("^(\\w+)\\(p(\\d+),\\s+(.+)\\)\\.$");
	public static final Pattern PLLINE_PATTERN = Pattern.compile("^(\\D+\\d*)\\(p(\\d+),\\s*((p(\\d+))|('(.*)'))\\)\\.$");

	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file) throws PuckException {
		Net result;

		result = load(file, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file, final String charsetname) throws PuckException {
		Net result;

		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), charsetname));
			result = read(in);
			logger.debug("Net=" + LogHelper.toString(result));
			result = NetUtils.buildCleanedNet(result);

			result.setLabel(file.getName());

		} catch (UnsupportedEncodingException exception) {
			//
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");

		} catch (FileNotFoundException exception) {
			//
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");

		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException exception) {
				logger.warn("Not managed error.");
				exception.printStackTrace();
			}
		}

		//
		return result;
	}

	/**
	 * Reads a net from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static Net read(final BufferedReader in) throws PuckException {
		Net result;

		result = new Net();

		//
		boolean ended = false;
		while (!ended) {

			//
			PLLine source = readPLLine(in);

			if (source == null) {
				ended = true;
			} else {
				//
				if ((source.label().equals("daughter")) || (source.label().equals("son"))) {
					// FIXME
					// PuckUtils.setFiliationRelation(result, source.value2(),
					// source.value1());
				} else if ((source.label().equals("daughter")) || (source.label().equals("son"))) {
					// FIXME
					// PuckUtils.setFiliationRelation(result, source.value2(),
					// source.value1());
				} else if ((source.label().equals("father")) || (source.label().equals("mother"))) {
					// Nothing to do.
				} else if ((source.label().equals("husband")) || (source.label().equals("wife"))) {
					NetUtils.setSpouseRelationAndFixRoles(result, source.value1(), source.value2());
				} else if (source.label().equals("gname")) {
					Individual individual = result.individuals().getById(source.value1());
					if (individual == null) {
						individual = new Individual(source.value1());
						result.individuals().add(individual);
					}
					individual.setName(source.value2ToString());
				} else if (source.label().equals("sex")) {
					Individual individual = result.individuals().getById(source.value1());
					if (individual == null) {
						individual = new Individual(source.value1());
						result.individuals().add(individual);
					}
					individual.setGender(Gender.valueOf(source.value2ToChar()));
				} else if (source.label().startsWith("info")) {
					if (source.label().equals("info1")) {
						NetUtils.setAttribute(result, source.value1(), "OCCU", source.value2ToString());
					} else if (source.label().equals("info2")) {
						NetUtils.setAttribute(result, source.value1(), "BIRT", source.value2ToString());
					} else if (source.label().equals("info3")) {
						NetUtils.setAttribute(result, source.value1(), "DEAT", source.value2ToString());
					} else {
						NetUtils.setAttribute(result, source.value1(), source.label(), source.value2ToString());
					}
				} else {
					throw PuckExceptions.BAD_FILE_FORMAT.create("Unknown label.");
				}
			}
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * Reads a line of attributes from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of attributes or null if no more.
	 * 
	 * @throws PuckException
	 */
	public static PLLine readPLLine(final BufferedReader in) throws PuckException {
		PLLine result;

		try {
			String line = in.readLine();

			if (line == null) {
				result = null;
			} else {

				Matcher matcher = PLLINE_PATTERN.matcher(line);

				if ((matcher.find()) && (matcher.groupCount() == 7)) {
					//
					String label = matcher.group(1);
					int value1 = Integer.parseInt(matcher.group(2).trim());
					String value2;
					if (matcher.group(5) == null) {
						value2 = matcher.group(7).trim();
					} else {
						value2 = matcher.group(5);
					}

					//
					result = new PLLine(label, value1, value2);
				} else {
					throw PuckExceptions.BAD_FILE_FORMAT.create("Bad attributes line format: [" + line + "].");
				}
			}
		} catch (IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception);
		}

		//
		return result;
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File file, final Net source) throws PuckException {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));

			write(out, source);

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param net
	 *            Source.
	 */
	public static void write(final PrintWriter out, final Net source) {

		// Write daughter relations.
		for (Individual individual : source.individuals()) {
			for (Individual child : individual.children()) {
				if (child.isFemale()) {
					write(out, new PLLine("daughter", child.getId(), individual.getId()));
				}
			}
		}

		// Write father relations.
		for (Individual individual : source.individuals()) {
			Individual parent = individual.getFather();
			if ((parent != null) && (!parent.isFemale())) {
				write(out, new PLLine("father", parent.getId(), individual.getId()));
			}
		}

		// Write name.
		for (Individual individual : source.individuals()) {
			write(out, new PLLine("gname", individual.getId(), individual.getName()));
		}

		// Write husband relations.
		for (Family family : source.families()) {
			Individual husband = family.getHusband();
			Individual wife = family.getWife();
			if ((husband != null) && (wife != null)) {
				write(out, new PLLine("husband", husband.getId(), wife.getId()));
			}
		}

		// Write infos.
		for (Individual individual : source.individuals()) {
			Attribute attribute = individual.attributes().get("OCCU");
			if (attribute != null) {
				write(out, new PLLine("info1", individual.getId(), attribute.getValue()));
			}
			attribute = individual.attributes().get("BIRT");
			if (attribute != null) {
				write(out, new PLLine("info2", individual.getId(), attribute.getValue()));
			}
			attribute = individual.attributes().get("DEAT");
			if (attribute != null) {
				write(out, new PLLine("info3", individual.getId(), attribute.getValue()));
			}
		}

		// Write mother relations.
		for (Individual individual : source.individuals()) {
			Individual parent = individual.getMother();
			if (parent != null) {
				write(out, new PLLine("mother", parent.getId(), individual.getId()));
			}
		}

		// Write sex.
		for (Individual individual : source.individuals()) {
			write(out, new PLLine("sex", individual.getId(), individual.getGender().toChar()));
		}

		// Write son relations.
		for (Individual individual : source.individuals()) {
			for (Individual child : individual.children()) {
				if (child.getGender() != Gender.FEMALE) {
					write(out, new PLLine("son", child.getId(), individual.getId()));
				}
			}
		}

		// Write wife relations.
		for (Family family : source.families()) {
			Individual husband = family.getHusband();
			Individual wife = family.getWife();
			if ((husband != null) && (wife != null)) {
				write(out, new PLLine("wife", wife.getId(), husband.getId()));
			}
		}

	}

	/**
	 * Writes a line of labels.
	 * 
	 * @param out
	 *            Target.
	 * @param source
	 *            Source.
	 */
	public static void write(final PrintWriter out, final PLLine source) {

		String line;
		if (source.isValue2Number()) {
			line = String.format("%s(p%d, p%d).", source.label(), source.value1(), source.value2());
		} else {
			line = String.format("%s(p%d, '%s').", source.label(), source.value1(), source.value2ToString());
		}

		out.println(line);
	}
}
