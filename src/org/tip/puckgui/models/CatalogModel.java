package org.tip.puckgui.models;

import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.io.kinsources.Catalog;

/**
 * 
 * @author TIP
 */
public class CatalogModel extends AbstractTableModel {

	private static final long serialVersionUID = -4889205936851096650L;
	private static final Logger logger = LoggerFactory.getLogger(CatalogModel.class);
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.tip.puckgui.messages");

	private Catalog source;

	/**
	 * 
	 */
	public CatalogModel(final Catalog source) {
		super();
		setSource(source);
	}

	/**
	 * 
	 */
	@Override
	public Class getColumnClass(final int columnIndex) {
		Class result;

		if (this.source == null) {
			//
			result = String.class;

		} else {
			//
			switch (columnIndex) {
				case 0:
					result = Long.class;
				break;
				case 1:
					result = String.class;
				break;
				case 2:
					result = String.class;
				break;
				default:
					result = String.class;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getColumnCount() {
		int result;

		result = 4;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String getColumnName(final int columnIndex) {
		String result;

		switch (columnIndex) {
			case 0:
				result = "ID";
			break;
			case 1:
				result = "Dataset Name";
			break;
			case 2:
				result = "Author";
			break;
			case 3:
				result = "License";
			break;
			default:
				result = "";
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getRowCount() {
		int result;

		result = this.source.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Catalog getSource() {
		Catalog result;

		result = this.source;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Object getValueAt(final int rowIndex, final int columnIndex) {
		Object result;

		if (this.source.getByIndex(rowIndex) == null) {
			//
			result = "";

		} else {
			//
			switch (columnIndex) {
				case 0:
					result = this.source.getByIndex(rowIndex).getId();
				break;
				case 1:
					result = this.source.getByIndex(rowIndex).getName();
				break;
				case 2:
					result = this.source.getByIndex(rowIndex).getAuthor();
				break;
				case 3:
					result = this.source.getByIndex(rowIndex).getLicense();
				break;
				default:
					result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public boolean isCellEditable(final int rowIndex, final int columnIndex) {
		boolean result;

		switch (columnIndex) {

			default:
				result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void setSource(final Catalog source) {
		//
		this.source = source;

		//
		fireTableDataChanged();
	}
}
