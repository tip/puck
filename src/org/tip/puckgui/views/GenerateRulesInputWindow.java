package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.alliancenets.AllianceNet;
import org.tip.puck.alliancenets.EvoAllianceGenNoCLI;
import org.tip.puck.evo.EvoGen;
import org.tip.puck.evo.EvoStrategy;
import org.tip.puck.partitions.graphs.ClusterNetworkReporter;
import org.tip.puck.partitions.graphs.ClusterNetworkUtils;
import org.tip.puck.report.Report;
import org.tip.puckgui.GroupNetGUI;
import org.tip.puckgui.PuckGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class GenerateRulesInputWindow extends JFrame {

	private static final long serialVersionUID = -49290702424155334L;
	private static final Logger logger = LoggerFactory.getLogger(GenerateRulesInputWindow.class);

	private JFrame thisJFrame;
	private JPanel contentPane;
	private JSpinner spinnerGenerationCount;

	/**
	 * Similar with VirtualFielworkInputWindow.
	 */
	public GenerateRulesInputWindow(final GroupNetGUI groupNetGUI) {

		// /////////////////////////////////
		setIconImage(Toolkit.getDefaultToolkit().getImage(GenerateRulesInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		setTitle("Generate Rules Input Window");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 365, 147);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});

		JButton btnRestoreDefaults = new JButton("Restore defaults");
		btnRestoreDefaults.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Restore Defaults.
				setCriteria(new GenerateRulesCriteria());
			}
		});
		buttonPanel.add(btnRestoreDefaults);
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		getRootPane().setDefaultButton(btnLaunch);
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					//
					GenerateRulesCriteria criteria = getCriteria();

					//
					PuckGUI.instance().getPreferences().setGenerateRulesCriteria(criteria);

					//
					AllianceNet allianceNet = ClusterNetworkUtils.graphToAllianceNet(groupNetGUI.getGroupNet());
					EvoAllianceGenNoCLI callbacks = new EvoAllianceGenNoCLI(allianceNet);
					EvoStrategy popGen = new EvoStrategy(1, 1, 1);
					// Tournament popGen = new Tournament(25, 2, 0.1, 0.7);
					EvoGen evo = new EvoGen(popGen, callbacks, criteria.getGenerationCount());
					evo.run();

					// Build report.
					Report report = ClusterNetworkReporter.reportGenerateRules(groupNetGUI.getGroupNet(), criteria, evo);

					//
					groupNetGUI.addReportTab(report);

					//
					dispose();

				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(GenerateRulesInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);

		JPanel panel = new JPanel();
		this.contentPane.add(panel, BorderLayout.NORTH);
		panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblNrGenerationCount = new JLabel("Generation count:");
		lblNrGenerationCount.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblNrGenerationCount, "2, 4, right, default");

		this.spinnerGenerationCount = new JSpinner();
		this.spinnerGenerationCount.setModel(new SpinnerNumberModel(new Integer(100), new Integer(0), null, new Integer(100)));
		panel.add(this.spinnerGenerationCount, "4, 4");

		// //////////////////
		setCriteria(PuckGUI.instance().getPreferences().getGenerateRulesCriteria());
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public GenerateRulesCriteria getCriteria() throws PuckException {
		GenerateRulesCriteria result;

		//
		result = new GenerateRulesCriteria();

		//
		result.setGenerationCount((Integer) this.spinnerGenerationCount.getValue());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public void setCriteria(final GenerateRulesCriteria source) {
		//
		if (source != null) {
			//
			this.spinnerGenerationCount.setValue(source.getGenerationCount());
		}
	}
}
