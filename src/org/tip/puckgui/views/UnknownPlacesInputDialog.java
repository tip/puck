package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.sequences.workers.UnknownPlacesCriteria;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class UnknownPlacesInputDialog extends JDialog {

	private static final long serialVersionUID = -5189682867843879482L;
	private static final Logger logger = LoggerFactory.getLogger(UnknownPlacesInputDialog.class);

	private final JPanel contentPanel = new JPanel();
	private UnknownPlacesCriteria dialogCriteria;
	private static UnknownPlacesCriteria lastCriteria = new UnknownPlacesCriteria();
	private JComboBox cmbbxTarget;

	/**
	 * Create the dialog.
	 */
	public UnknownPlacesInputDialog(final StringList relationModelNames) {
		super();

		//
		List<String> targetLabels = new ArrayList<String>();
		targetLabels.add("ALL");
		targetLabels.add("INDIVIDUALS");
		targetLabels.add("RELATIONS");
		if (relationModelNames != null) {
			//
			for (String name : relationModelNames) {
				//
				targetLabels.add(name);
			}
		}

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Unknown Places Input");
		setIconImage(Toolkit.getDefaultToolkit().getImage(UnknownPlacesInputDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				UnknownPlacesInputDialog.this.dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 420, 120);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, }));
		{
			JLabel lblTarget = new JLabel("Target:");
			this.contentPanel.add(lblTarget, "2, 2, right, default");
		}
		{
			this.cmbbxTarget = new JComboBox(targetLabels.toArray());
			this.contentPanel.add(this.cmbbxTarget, "4, 2, fill, default");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						UnknownPlacesInputDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("Launch");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						UnknownPlacesCriteria criteria = getCriteria();
						if (criteria == null) {
							//
							String title = "Invalid input";
							String message = "Invalid input.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

						} else {
							//
							lastCriteria = criteria;
							UnknownPlacesInputDialog.this.dialogCriteria = criteria;

							//
							setVisible(false);
						}

					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}

		// ////////////////////////
		setCriteria(lastCriteria);
	}

	/**
	 * 
	 * @return
	 */
	public UnknownPlacesCriteria getCriteria() {
		UnknownPlacesCriteria result;

		result = new UnknownPlacesCriteria();

		//
		switch (this.cmbbxTarget.getSelectedIndex()) {
			case 0:
				result.setIncludedIndividual(true);
				result.setIncludedAllRelations(true);
			break;

			case 1:
				result.setIncludedIndividual(true);
				result.setIncludedAllRelations(false);
			break;

			case 2:
				result.setIncludedIndividual(false);
				result.setIncludedAllRelations(true);
			break;

			default:
				result.setIncludedIndividual(false);
				result.setIncludedAllRelations(false);
				result.getRelationNames().add((String) this.cmbbxTarget.getSelectedItem());
			break;
		}

		logger.debug("result={}", result.toString());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public UnknownPlacesCriteria getDialogCriteria() {
		UnknownPlacesCriteria result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final UnknownPlacesCriteria source) {
		//
		if (source != null) {
			//
			if (source.isIncludedIndividual()) {
				//
				if (source.isIncludedAllRelations()) {
					//
					this.cmbbxTarget.setSelectedIndex(0);
				} else {
					//
					this.cmbbxTarget.setSelectedIndex(1);
				}
			} else {
				//
				if (source.getRelationNames().size() == 1) {
					//
					this.cmbbxTarget.setSelectedItem(source.getRelationNames().get(0));
				} else {
					//
					this.cmbbxTarget.setSelectedIndex(2);
				}
			}
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		/* UnknownPlacesCriteria criteria = */showDialog(null);
	}

	/**
	 * Launch the application.
	 */
	public static UnknownPlacesCriteria showDialog(final StringList relationModelNames) {
		UnknownPlacesCriteria result;

		//
		UnknownPlacesInputDialog dialog = new UnknownPlacesInputDialog(relationModelNames);
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
