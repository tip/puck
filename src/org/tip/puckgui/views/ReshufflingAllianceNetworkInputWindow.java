package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.graphs.random.RandomGraphReporter;
import org.tip.puck.graphs.workers.GraphReporter;
import org.tip.puck.matrix.MatrixStatistics;
import org.tip.puck.report.Report;
import org.tip.puckgui.GroupNetGUI;
import org.tip.puckgui.PuckGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class ReshufflingAllianceNetworkInputWindow extends JFrame {

	private static final long serialVersionUID = -8964016910920013460L;
	private static final Logger logger = LoggerFactory.getLogger(ReshufflingAllianceNetworkInputWindow.class);

	private JFrame thisJFrame;
	private JPanel contentPane;
	private JSpinner spinnerRuns;
	private JCheckBox chckbxExtract;

	/**
	 * Similar with VirtualFielworkInputWindow.
	 */
	public ReshufflingAllianceNetworkInputWindow(final GroupNetGUI groupNetGUI) {

		// /////////////////////////////////
		setIconImage(Toolkit.getDefaultToolkit().getImage(ReshufflingAllianceNetworkInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		setTitle("Random Reshuffling");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 365, 175);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});

		JButton btnRestoreDefaults = new JButton("Restore defaults");
		btnRestoreDefaults.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Restore Defaults.
				setCriteria(new ReshufflingCriteria());
			}
		});
		buttonPanel.add(btnRestoreDefaults);
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		getRootPane().setDefaultButton(btnLaunch);
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					//
					ReshufflingCriteria criteria = getCriteria();

					//
					PuckGUI.instance().getPreferences().setReshufflingCriteria(criteria);

					//
					MatrixStatistics matrixStatistics = RandomGraphReporter.createRandomGraphStatisticsByReshuffling(groupNetGUI.getGroupNet(),
							criteria.getNumberOfRuns());
					MatrixStatistics sourceStatistics = GraphReporter.getMatrixStatistics(groupNetGUI.getGroupNet());

					// Build report.
					Report report = RandomGraphReporter.reportRandomAllianceNetworkByReshuffling(criteria.getNumberOfRuns(), matrixStatistics, sourceStatistics);

					// Manage window.
					if (criteria.isExtractRepresentative()) {
						// Create new window and fill it with a report tab.
						if (StringUtils.isBlank(matrixStatistics.getGraph().getLabel())) {
							matrixStatistics.getGraph().setLabel("Random Group Network");
						}
						GroupNetGUI newGUI = PuckGUI.instance().createGroupNetGUI(matrixStatistics.getGraph());
						newGUI.addReportTab(report);

					} else {
						groupNetGUI.addReportTab(report);
					}

					dispose();
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(ReshufflingAllianceNetworkInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);

		JPanel panel = new JPanel();
		this.contentPane.add(panel, BorderLayout.NORTH);
		panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblNrRuns = new JLabel("Number of runs:");
		lblNrRuns.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblNrRuns, "2, 4, right, default");

		this.spinnerRuns = new JSpinner();
		this.spinnerRuns.setModel(new SpinnerNumberModel(new Integer(100), new Integer(0), null, new Integer(100)));
		panel.add(this.spinnerRuns, "4, 4");

		this.chckbxExtract = new JCheckBox("Extract a representative network");
		panel.add(this.chckbxExtract, "2, 6, 3, 1");

		// //////////////////
		setCriteria(PuckGUI.instance().getPreferences().getReshufflingCriteria());
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public ReshufflingCriteria getCriteria() throws PuckException {
		ReshufflingCriteria result;

		//
		result = new ReshufflingCriteria();

		//
		result.setNumberOfRuns((Integer) this.spinnerRuns.getValue());
		result.setExtractRepresentative(this.chckbxExtract.isSelected());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public void setCriteria(final ReshufflingCriteria source) {
		//
		if (source != null) {
			//
			this.spinnerRuns.setValue(source.getNumberOfRuns());
			this.chckbxExtract.setSelected(source.isExtractRepresentative());
		}
	}
}
