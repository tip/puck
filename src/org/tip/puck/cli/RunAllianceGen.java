package org.tip.puck.cli;

import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.tip.puck.alliancenets.AllianceGen;
import org.tip.puck.alliancenets.AllianceNet;
import org.tip.puck.alliancenets.TopologicalIndices;


public class RunAllianceGen extends Command {

    @Override
    public boolean run(CommandLine cline) {
        if(!cline.hasOption("inet")) {
            setErrorMessage("input network file must be specified");
            return false;
        }
        
        if(!cline.hasOption("prg")) {
            setErrorMessage("generator program file must be specified");
            return false;
        }
        
        if(!cline.hasOption("onet")) {
            setErrorMessage("output network file must be specified");
            return false;
        }
        
        String netfile = cline.getOptionValue("inet");
        String prgFile = cline.getOptionValue("prg");
        String onetFile = cline.getOptionValue("onet");
        
        System.out.println("target net: " + netfile);
        
        AllianceNet targNet = AllianceNet.load(netfile);
        targNet.save(onetFile);
        
        TopologicalIndices targIndices = new TopologicalIndices(targNet);
        AllianceGen gen = new AllianceGen(targNet.getNodeCount(), targNet.getEdgeCount(), targIndices);
        try {
            gen.loadProgs(prgFile);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        gen.run();
        AllianceNet simNet = gen.getNet();
        
        // write net
        simNet.save(onetFile);
        
        TopologicalIndices simIndices = new TopologicalIndices(simNet);
        
        double fit = simIndices.distance(targIndices);
        
        gen.printProgs(true);
        
        System.out.println("targ:");
        System.out.println(targIndices);
        System.out.println("sim:");
        System.out.println(simIndices);
        
        System.out.println("fitness: " + fit);
        
        return true;
    }
}