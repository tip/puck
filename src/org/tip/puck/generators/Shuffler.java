/**
 * 
 */
package org.tip.puck.generators;

import java.io.File;

import org.tip.puck.PuckException;
import org.tip.puck.census.workers.CensusCriteria;
import org.tip.puck.census.workers.CircuitFinder;
import org.tip.puck.io.bar.BARTXTFile;
import org.tip.puck.net.Net;
import org.tip.puck.segmentation.Segmentation;

/**
 * @author Telmo Menezes
 *
 */
public class Shuffler {
	
	public static Net shuffle (Net net, int k, int maxGenDist,
			double minShufflePer, int minStableIter){
		Net result;
		
		PGraph pgraph = new PGraph(net);
		shuffle(pgraph,k,maxGenDist,minShufflePer,minStableIter);
		result = pgraph.toNet();
		
		//
		return result;
	}
	
	public static CircuitFinder findCircuits (Net net, int k, int maxGenDist,
			double minShufflePer, int minStableIter, int runs, CensusCriteria criteria) throws PuckException{
		CircuitFinder result;
		
		result = new CircuitFinder(new Segmentation(net),criteria);
		
		result.initializeCounts();

		for (int run=0; run < runs; run++){
			Net shuffledNet = shuffle(net,k,maxGenDist,minShufflePer,minStableIter);
			CircuitFinder finder = new CircuitFinder(new Segmentation(shuffledNet),criteria);
			finder.findCircuits();
			result.incrementCounts(finder);
		}
		
		result.normalizeCounts(runs);
		
		//
		return result;
	}

	public static void shuffle(PGraph pgraph, int k, int maxGenDist,
			double minShufflePer, int minStableIter) {
		PGraph origPGraph = new PGraph(pgraph);
		pgraph.storeOriginalDescendants();
		int iteration = 0;
		int switches = 0;
		double maxDist = 0;
		int stableIterations = 0;
		
		boolean stop = false;
		while (!stop) {
			double shuffled = pgraph.percentageShuffledLinks();
			double dist = pgraph.distance(origPGraph);
			
			System.out.println("#iter " + iteration + "\t\tswaps: " + switches + "\t\tstable iter: " + stableIterations);
			System.out.println("  shuffled: " + shuffled + "%; dist: " + dist + "; max dist: " + maxDist);
			
			if (dist > maxDist) {
				maxDist = dist;
				stableIterations = 0;
			}
			else {
				stableIterations++;
			}
			if (pgraph.switchLinks(k, maxGenDist)) {
				switches++;
			}
			iteration++;
			
			if ((shuffled >= minShufflePer) && (stableIterations >= minStableIter)) {
				stop = true;
			}
		}
	}
	
	public static void main(String[] args) {
		final int k = 2;
		final int maxGenDist = 2;
		final double minShufflePer = 100;
		final int minStableIter = 1000;
		
		try {
			Net net = BARTXTFile.load(new File("Chimane6.txt"));
			PGraph pgraph = new PGraph(net);
			System.out.println(pgraph);
			System.out.println("\n");
	
			shuffle(pgraph, k, maxGenDist, minShufflePer, minStableIter);
			
			Net newNet = pgraph.toNet();
			System.out.println(newNet);
			
			System.out.println("\n");
			System.out.println(pgraph);
			System.out.println("done.");
		} catch (PuckException e) {
			e.printStackTrace();
		}

	}

}
