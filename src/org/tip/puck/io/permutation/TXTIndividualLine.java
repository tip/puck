package org.tip.puck.io.permutation;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a line of individual from a TXT file.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class TXTIndividualLine {
	public static final int NOID = 0;

	private int id;
	private String name;
	private char gender;
	private int fatherId;
	private int motherId;
	private List<Integer> spouseIds;

	/**
	 * 
	 */
	public TXTIndividualLine() {
		this.spouseIds = new ArrayList<Integer>();
		this.gender = 'X';
	}

	public int fatherId() {
		return this.fatherId;
	}

	public char gender() {
		return this.gender;
	}

	public int id() {
		return this.id;
	}

	public int motherId() {
		return this.motherId;
	}

	public String name() {
		return this.name;
	}

	public void setFatherId(final int fatherId) {
		this.fatherId = fatherId;
	}

	public void setGender(final char gender) {
		this.gender = gender;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public void setMotherId(final int motherId) {
		this.motherId = motherId;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public List<Integer> spouseIds() {
		return this.spouseIds;
	}

}
