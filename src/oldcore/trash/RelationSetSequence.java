package oldcore.trash;

import org.tip.puck.net.relations.Relations;
import org.tip.puck.sequences.Sequence;

public class RelationSetSequence extends Sequence<Relations> {

/*	private String idLabel;
	private String dateLabel;
	private String placeLabel;
	private String startDateLabel;
	private String endDateLabel;*/
	
//	Map<String,Individuals> membersByRelationId;
	
//	private String relationModelName;
//	private String egoRoleName;
//	private Relations relations;
//	private Relations allRelations;
//	private Individuals allIndividuals;
	
//	private EgoRelationSequences groupSequences;
//	private EgoRelationSequences indiSequences;
	
/*	private int[] maxDepths;
	private String chainClassification;
	private List<String> relationModelNames;*/

	public RelationSetSequence(String idLabel, int id){
		super(idLabel, id);
		
//		membersByRelationId = new TreeMap<String,Individuals>();
		
//		groupSequences = new EgoRelationSequences();
//		indiSequences = new EgoRelationSequences();

//		relationModelName = criteria.getRelationModelName();
//		egoRoleName = criteria.getEgoRoleName();
//		idLabel = criteria.getLocalUnitLabel();
/*		dateLabel = criteria.getDateLabel();
		placeLabel = criteria.getPlaceLabel();
		startDateLabel = criteria.getStartDateLabel();
		endDateLabel = criteria.getEndDateLabel();
		maxDepths = ToolBox.stringsToInts(criteria.getPattern());
		chainClassification = criteria.getChainClassification();
		relationModelNames = criteria.getRelationModelNames();*/

	}
	
/*	public RelationSet getByOrdinal(Ordinal ordinal){
		RelationSet result;
		
		result = null;
		if (ordinal !=null){
			result = getStation(ordinal);
//			result = getById(ordinal.getYear());
		}
		//
		return result;
		
	}

	public Map<String, Individuals> membersByRelationId() {
		return membersByRelationId;
	}
	
	public List<String> idValues(){
		List<String> result;
		
		result = new ArrayList<String>(membersByRelationId.keySet());
		Collections.sort(result);
		
		//
		return result;
	}
	
	public List<Integer> ids (){
		List<Integer> result;
		
		result = new ArrayList<Integer>(data.keySet());
		Collections.sort(result);
		
		//
		return result;
	}

	public String idLabel() {
		return idLabel;
	}

	public void setIdLabel(String idLabel) {
		this.idLabel = idLabel;
	}

	public String relationModelName() {
		return relationModelName;
	}

	public void setRelationModelName(String relationModelName) {
		this.relationModelName = relationModelName;
	}*/

/*	public Relations relations() {
		Relations result;
		
		result = new Relations();
		
		for (Relations station : stations.values()){
			result.put(station);
		}
		//
		return result;
//		return relations;
	}*/

/*	public void setRelations(Relations relations) {
		this.relations = relations;
	}

	public EgoRelationSequences toSequencesByIdValue() {
		return groupSequences;
	}

	public void setGroupSequences(EgoRelationSequences sequences) {
		this.groupSequences = sequences;
	}
	
	
	public EgoRelationSequences toSequencesByEgo() {
		return indiSequences;
	}

	public void setIndiSequences(EgoRelationSequences indiSequences) {
		this.indiSequences = indiSequences;
	}

	public Relations getStation(Ordinal time){
		Relations result;

		result = null;
		
		RelationSet station = getStation(time);
		
		if (station!=null){
			result = station.relations();
		}
		
		return result;
	}

	
	public Relations allRelations(Ordinal time){
		Relations result;
		
		result = null;
		
		RelationSet slice = getStation(time);
		
		if (slice!=null){
			result = slice.allRelations();
		}
		
		//
		return result;
		
	}

	public String dateLabel() {
		return dateLabel;
	}

	public void setDateLabel(String dateLabel) {
		this.dateLabel = dateLabel;
	}

	public String placeLabel() {
		return placeLabel;
	}

	public void setPlaceLabel(String placeLabel) {
		this.placeLabel = placeLabel;
	}

	public String startDateLabel() {
		return startDateLabel;
	}

	public void setStartDateLabel(String startDateLabel) {
		this.startDateLabel = startDateLabel;
	}

	public String endDateLabel() {
		return endDateLabel;
	}

	public void setEndDateLabel(String endDateLabel) {
		this.endDateLabel = endDateLabel;
	}
	
	public Ordinal time(Relation relation){
		Ordinal result;
				
		Value timeValue = RelationValuator.get(relation, dateLabel);
		if (timeValue!=null) {
			result = new Ordinal(timeValue.intValue());
		} else {
			result = null;
		}
		//
		return result;
	}*/
	
/*	public Individuals individuals(){
		Individuals result;
		
		result = new Individuals();
		
		for (RelationSet slice : stations.values()){
			result.add(slice.getIndividuals());
		}
		//
		return result;
	}

	public String getEgoRoleName() {
		return egoRoleName;
	}

	public Individuals getAllIndividuals() {
		return allIndividuals;
	}

	public void setAllIndividuals(Individuals individuals) {
		this.allIndividuals = individuals;
	}

	public int[] maxDepths() {
		return maxDepths;
	}

	public String getChainClassification() {
		return chainClassification;
	}

	public List<String> getRelationModelNames() {
		return relationModelNames;
	}

	public Relations allRelations() {
		return allRelations;
	}

	public void setAllRelations(Relations allRelations) {
		this.allRelations = allRelations;
	}*/
	
	

	
}
