package org.tip.puck.sequences;

public enum SequenceType {
	
	EVENTS,
	MOVES,
	STATES

}
