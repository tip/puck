package org.tip.puckgui.models;

import java.awt.Color;
import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.tip.puck.net.Individual;

/**
 * This class provides implementation for displaying decorated individual list.
 * 
 * @author Edoardo Savoia
 * @author TIP
 */
public class IndividualsCellRenderer extends JLabel implements ListCellRenderer {
	private static final long serialVersionUID = -1989484202174600878L;
	private static ImageIcon femaleIcon = new ImageIcon(IndividualsCellRenderer.class.getResource("/org/tip/puckgui/images/female-16x16.png"));
	private static ImageIcon maleIcon = new ImageIcon(IndividualsCellRenderer.class.getResource("/org/tip/puckgui/images/male-16x16.png"));
	private static ImageIcon unknowIcon = new ImageIcon(IndividualsCellRenderer.class.getResource("/org/tip/puckgui/images/unknown-16x16.png"));

	/**
	 * 
	 */
	public IndividualsCellRenderer() {
		super();
		setOpaque(true);
	}

	/**
	 * 	
	 */
	@Override
	public Component getListCellRendererComponent(final JList list, final Object lineObject, final int index, final boolean isSelected,
			final boolean cellHasFocus) {

		//
		if (lineObject == null) {
			//
			throw new NullPointerException("Invalid null parameter.");

		} else if (lineObject instanceof String) {
			//
			setText((String) lineObject);

		} else {
			//
			Individual individual = (Individual) lineObject;

			switch (individual.getGender()) {
				case FEMALE:
					setIcon(femaleIcon);
				break;
				case MALE:
					setIcon(maleIcon);
				break;
				case UNKNOWN:
					setIcon(unknowIcon);
				break;
			}

			String name;
			if (individual.getName() == null) {
				//
				name = "Unknown";

			} else {
				//
				name = individual.getName();
			}

			setText(String.format("(%d) %s", individual.getId(), name));
		}

		//
		Color background = null;
		Color foreground = null;
		if (isSelected) {
			//
			background = list.getSelectionBackground();
			foreground = list.getSelectionForeground();

		} else {
			//
			background = list.getBackground();
			foreground = list.getForeground();
		}
		setBackground(background);
		setForeground(foreground);

		//
		return this;
	}
}
