package org.tip.puck.report;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class Report {

	private String title;
	private Calendar date;
	private String target;
	private String origin;
	private long timeSpent;
	private int status;
	private String inputComment;
	private ReportAttributes inputs;
	private ReportOutputs outputs;

	/**
	 * 
	 */
	public Report() {
		this.date = Calendar.getInstance();
		this.inputs = new ReportAttributes();
		this.outputs = new ReportOutputs();
		this.timeSpent = 0;
	}

	/**
	 * 
	 */
	public Report(final String title) {
		this.title = title;
		this.date = Calendar.getInstance();
		this.inputs = new ReportAttributes();
		this.outputs = new ReportOutputs();
		this.timeSpent = 0;
	}

	/**
	 * 
	 * @return
	 */
	public boolean containsSubReport() {
		boolean result;

		result = this.outputs.containsSubReport();

		//
		return result;
	}

	public Calendar date() {
		return date;
	}

	/**
	 * 
	 * @return
	 */
	public boolean hasInput() {
		boolean result;

		if ((this.inputs().size() == 0) && (StringUtils.isBlank(this.inputComment))) {
			result = false;
		} else {
			result = true;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean hasOutput() {
		boolean result;

		result = !this.outputs.isEmpty();

		//
		return result;
	}

	public String inputComment() {
		return inputComment;
	}

	public ReportAttributes inputs() {
		return this.inputs;
	}

	public String origin() {
		return origin;
	}

	/**
	 * 
	 * @return
	 */
	public ReportOutputs outputs() {
		return outputs;
	}

	public void setDate(final Calendar date) {
		this.date = date;
	}

	public void setInputComment(final String inputComment) {
		this.inputComment = inputComment;
	}

	public void setOrigin(final String origin) {
		this.origin = origin;
	}

	public void setStatus(final int status) {
		this.status = status;
	}

	public void setTarget(final String target) {
		this.target = target;
	}

	public void setTimeSpent(final long timeSpent) {
		this.timeSpent = timeSpent;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public int status() {
		return status;
	}

	/**
	 * 
	 * @return
	 */
	public Reports subReports() {
		Reports result;

		result = new Reports();

		for (Object item : this.outputs()) {
			if (item instanceof Report) {
				result.add((Report) item);
			}
		}

		//
		return result;
	}

	public String target() {
		return target;
	}

	public long timeSpent() {
		return timeSpent;
	}

	public String title() {
		return title;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		result = toString(null);

		//
		return result;
	}

	/**
	 * 
	 */
	public String toString(final ResourceBundle bundle) {
		String result;

		StringList buffer = new StringList(1024);

		//
		buffer.append(translate(bundle, "     General information")).append("\n");
		buffer.append(translate(bundle, "Title:")).append(" ").append(this.title).append("\n");
		buffer.append(translate(bundle, "Origin:")).append(" ").append(this.origin).append("\n");
		buffer.append(translate(bundle, "Date:")).append(" ").append((new SimpleDateFormat("dd/MM/yyyy HH':'mm")).format(this.date.getTime())).append("\n");
		buffer.append(translate(bundle, "Target:")).append(" ").append(this.target).append("\n");
		buffer.append(translate(bundle, "Time spent:")).append(" ").append(this.timeSpent).append("\n");
		buffer.append("\n");

		//
		buffer.append(translate(bundle, "     Inputs")).append("\n");
		buffer.append(translate(bundle, "Input comment:")).append(" ").append(this.inputComment).append("\n");
		buffer.append(translate(bundle, "Input data:")).append("\n");
		for (ReportAttribute item : this.inputs) {
			buffer.append(translate(bundle, item.label())).append("\t").append(item.value()).append("\n");
		}
		buffer.append("\n");

		//
		buffer.append(translate(bundle, "     Outputs")).append("\n");

		for (Object output : this.outputs) {
			buffer.append(output.toString());
		}

		//
		result = buffer.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param bundle
	 * @param key
	 * @return
	 */
	public static String translate(final ResourceBundle bundle, final String key) {
		String result;

		if (key == null) {
			result = "";
		} else if (bundle == null) {
			result = key;
		} else if (key.contains(".")) {
			try {
				result = bundle.getString(key);
			} catch (MissingResourceException exception) {
				result = key;
			}
		} else {
			try {
				result = bundle.getString("report." + key);
			} catch (MissingResourceException exception) {
				result = key;
			}
		}

		//
		return result;
	}
}
