package org.tip.puck.graphs.workers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.GraphMaker;
import org.tip.puck.graphs.Node;
import org.tip.puck.net.Individual;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.relations.workers.RelationValuator.EndogenousLabel;
import org.tip.puck.partitions.Partition;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.Value;

public class GraphValuator {

	public enum EndogenousLabel {
		GRAPH,
		SIZE,
		MAXDEPTH,
		MEANDEPTH,
		MEANINDEGREE,
		DIAMETER,
		CONCENTRATION,
		NRCOMPONENTS,
		MAXCOMPONENT
	}
	
	public static <E> Value get(final Graph<E> graph, final String label) {
		Value result;
		
		result = null;
		
		if (graph!=null){
			
			if (label.equals("GRAPH")){
				
				result = Value.valueOf(graph);
				
			} else if (label.equals("NODES")){
				
				result = Value.valueOf(graph.getNodes().toListSortedByLabel());
				
			} else if (label.equals("SIZE")){
				
				result = Value.valueOf(graph.nodeCount());
				
			} else if (label.equals("NRLINES")){
				
				result = Value.valueOf(graph.lineCount());
				
			} else if (label.equals("MAXDEPTH")){
				
				result = Value.valueOf(GraphUtils.getMaxDepth(graph));
				
			} else if (label.equals("MEANDEPTH")){
				
				result = Value.valueOf(MathUtils.round(GraphUtils.getMeanDepth(graph),2));
				
			} else if (label.equals("MAXDEPTH_NORM")){
				
				result = Value.valueOf(MathUtils.round(GraphUtils.getMaxDepthNormalized(graph),2));
				
			} else if (label.equals("MEANDEPTH_NORM")){
				
				result = Value.valueOf(MathUtils.round(GraphUtils.getMeanDepthNormalized(graph),2));
				
			} else if (label.equals("MEANDEGREE")){
				
				result = Value.valueOf(MathUtils.round(GraphUtils.meanDegree(graph),2));
				
			} else if (label.equals("DENSITY")){
				
				result = Value.valueOf(MathUtils.round(GraphUtils.density(graph),2));
				
			} else if (label.equals("MEANINDEGREE")){
				
				result = Value.valueOf(MathUtils.round(GraphUtils.meanInDegree(graph),2));
				
			} else if (label.equals("DIAMETER")){
				
				result = Value.valueOf(GraphUtils.getTreeDiameter(graph));
				
			} else if (label.equals("MEANCLUSTERINGCOEFF")){
				
				result = Value.valueOf(GraphUtils.getMeanClusteringCoefficient(graph));
				
			} else if (label.equals("CYCLERANK")){
				
				result = Value.valueOf(GraphUtils.cyclomaticNumber(graph));
				
			} else if (label.equals("LOOPS")) {
				
				result = Value.valueOf(GraphUtils.getLoops(graph).toListSortedByLabel());
				
			} else if (label.equals("TOPS")) {
				
				result = Value.valueOf(GraphUtils.getTops(graph).toListSortedByLabel());
				
			} else if (label.equals("BOTTOMS")) {
				
				result = Value.valueOf(GraphUtils.getBottoms(graph).toListSortedByLabel());
				
			} else if (label.equals("ISOLATES")) {
				
				result = Value.valueOf(GraphUtils.getIsolates(graph).toListSortedByLabel());
				
			} else if (label.equals("UPFORKS")) {
				
				result = Value.valueOf(GraphUtils.getUpForks(graph).toListSortedByLabel());
				
			} else if (label.equals("DOWNFORKS")) {
				
				result = Value.valueOf(GraphUtils.getDownForks(graph).toListSortedByLabel());
				
			} else if (label.equals("NRLOOPS")) {
				
				result = Value.valueOf(GraphUtils.getLoops(graph).size());
				
			} else if (label.equals("NRTOPS")) {
				
				result = Value.valueOf(GraphUtils.getTops(graph).size());
				
			} else if (label.equals("NRBOTTOMS")) {
				
				result = Value.valueOf(GraphUtils.getBottoms(graph).size());
				
			} else if (label.equals("NRISOLATES")) {
				
				result = Value.valueOf(GraphUtils.getIsolates(graph).size());
				
			} else if (label.equals("NRUPFORKS")) {
				
				result = Value.valueOf(GraphUtils.getUpForks(graph).size());
				
			} else if (label.equals("NRDOWNFORKS")) {
				
				result = Value.valueOf(GraphUtils.getDownForks(graph).size());
				
			} else if (label.equals("CYCLIC")) {
				
				result = Value.valueOf(GraphMaker.getCyclicCore(graph).getNodes().toListSortedByLabel());
				
			} else if (label.equals("NRCYCLIC")) {
				
				result = Value.valueOf(GraphMaker.getCyclicCore(graph).nodeCount());
				
			} else if (label.equals("NRCYCLIC_NORM")) {
				
				result = Value.valueOf(MathUtils.percent(GraphMaker.getCyclicCore(graph).nodeCount(),graph.nodeCount()));
				
			} else if (label.contains("COMPONENT") || label.equals("CONCENTRATION")){
				
				Partition<Node<E>> components = GraphUtils.components(graph);
				
				if (label.equals("NRCOMPONENTS")){
					
					result = Value.valueOf(components.size());

				} else if (label.equals("MAXCOMPONENT")){
					
					result = Value.valueOf(components.maxClusterSize());

				} else if (label.equals("CONCENTRATION")){
					
					result = Value.valueOf(components.concentration());
					
				}
			}
		}
	
		//
		return result;
	}	
	
	/**
	 * @param individuals
	 * @return
	 */
	public static List<String> getAttributeLabels() {
		List<String> result;

		//
		result = new ArrayList<String>(20);

		//
		for (EndogenousLabel label : EndogenousLabel.values()) {
			result.add(label.toString());
		}

		//
		Collections.sort(result);

		//
		return result;
	}
}
