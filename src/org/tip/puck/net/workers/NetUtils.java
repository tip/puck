package org.tip.puck.net.workers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.census.chains.Chain;
import org.tip.puck.census.chains.ChainFinder;
import org.tip.puck.census.chains.Couple;
import org.tip.puck.census.chains.Notation;
import org.tip.puck.census.workers.ChainValuator;
import org.tip.puck.census.workers.CircuitFinder;
import org.tip.puck.geo.Geography;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.Link;
import org.tip.puck.graphs.Node;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Families;
import org.tip.puck.net.Family;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.KinType;
import org.tip.puck.net.Net;
import org.tip.puck.net.UnionStatus;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Actors;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.relations.Roles;
import org.tip.puck.net.workers.AttributeWorker.Scope;
import org.tip.puck.net.workers.UpdateWorker.UpdateMode;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionMaker;
import org.tip.puck.report.Report;
import org.tip.puck.segmentation.Segment;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.statistics.StatisticsWorker;
import org.tip.puck.util.Numberable;
import org.tip.puck.util.NumberablesHashMap.IdStrategy;
import org.tip.puck.util.PuckUtils;
import org.tip.puck.util.Value;

import fr.devinsy.util.StringList;

/**
 * 
 * Methods added in this classes has to be static and autonomous.
 * 
 * @author TIP
 */
public class NetUtils {

	private static final Logger logger = LoggerFactory.getLogger(NetUtils.class);

	/**
	 * This method anonymizes a net leaving only the first name.
	 * 
	 * @param net
	 *            Source to anonymize.
	 * 
	 * @return the number of field anonymized
	 */
	public static long anonymizeByFirstName(final Individuals source) {
		return AttributeWorker.anonymizeByFirstName(source);
	}

	/**
	 * This method anonymizes a net leaving only the first name.
	 * 
	 * @param net
	 *            Source to anonymize.
	 * 
	 * @return the number of field anonymized
	 */
	public static long anonymizeByFirstName(final Net source) {
		return AttributeWorker.anonymizeByFirstName(source);
	}

	/**
	 * Anonymizes a net replacing name by gender and id.
	 * 
	 * @param net
	 *            Source to anonymize.
	 * 
	 * @return the number of field anonymized
	 */
/*	public static long anonymizeByGenderAndId(final Individuals source) {
		return AttributeWorker.anonymizeByGenderAndId(source);
	}*/

	/**
	 * Anonymizes a net replacing name by gender and id.
	 * 
	 * @param net
	 *            Source to anonymize.
	 * 
	 * @return the number of field anonymized
	 */
/*	public static long anonymizeByGenderAndId(final Net source) {
		return AttributeWorker.anonymizeByGenderAndId(source);
	}*/

	/**
	 * Anonymizes a net leaving only the first name.
	 * 
	 * @param net
	 *            Source to anonymize.
	 * 
	 * @return the number of field anonymized
	 */
	public static long anonymizeByLastName(final Individuals source) {
		return AttributeWorker.anonymizeByLastName(source);
	}

	/**
	 * This method anonymizes a net leaving only the first name.
	 * 
	 * @param net
	 *            Source to anonymize.
	 * 
	 * @return the number of field anonymized
	 */
	public static long anonymizeByLastName(final Net source) {
		return AttributeWorker.anonymizeByLastName(source);
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Net buildCleanedNet(final Net source) throws PuckException {
		Net result;

		//
		result = new Net();

		//
		result.setLabel(source.getLabel());

		//
		for (Attribute attribute : source.attributes().toSortedList()) {
			result.attributes().put(attribute.getLabel(), attribute.getValue());
		}

		// Create individuals (with bad data).
		for (Individual sourceIndividual : source.individuals()) {

			/**
			 * Set individual data. Warning:
			 * <ul>
			 * <li>origin family is set with source data and it will be fixed
			 * later
			 * <li>personal families is not set and it will be fixed later
			 * <li>relations is not set and it will be done later.
			 * </ul>
			 */
			Individual targetIndividual = new Individual(sourceIndividual.getId());
			targetIndividual.setGender(sourceIndividual.getGender());
			targetIndividual.setName(sourceIndividual.getName());
			targetIndividual.setOriginFamily(sourceIndividual.getOriginFamily());
			targetIndividual.attributes().addAll(sourceIndividual.attributes());

			//
			result.individuals().add(targetIndividual);
		}

		// Create family and fix data.
		for (Family sourceFamily : source.families()) {
			
			//
			int fatherId;
			if (sourceFamily.getHusband() == null) {
				fatherId = 0;
			} else {
				fatherId = sourceFamily.getHusband().getId();
			}

			//
			int motherId;
			if (sourceFamily.getWife() == null) {
				motherId = 0;
			} else {
				motherId = sourceFamily.getWife().getId();
			}

			// Get parent family or create it.
			// Note: If one parent is unknown, we can't associate
			// the individual to an existent family, we have to
			// avoid german relation. So, if one parent is
			// unknown, we have to create a new family.
			Family targetFamily;
			if ((fatherId == 0) || (motherId == 0)) {
				targetFamily = null;
			} else {
				targetFamily = result.families().getBySpouses(fatherId, motherId);
			}

			if (targetFamily == null) {
				targetFamily = new Family(result.families().size() + 1);
				result.families().add(targetFamily);

				//
				if (sourceFamily.getHusband() != null) {
					targetFamily.setHusband(result.individuals().getById(sourceFamily.getHusband().getId()));
				}
				if (sourceFamily.getWife() != null) {
					targetFamily.setWife(result.individuals().getById(sourceFamily.getWife().getId()));
				}
				targetFamily.setUnionStatus(sourceFamily.getUnionStatus());

				for (Individual sourceChild : sourceFamily.getChildren()) {
					Individual targetChild = result.individuals().getById(sourceChild.getId());
					targetFamily.getChildren().add(targetChild);
					targetChild.setOriginFamily(targetFamily);
				}

				targetFamily.attributes().addAll(sourceFamily.attributes());

			} else {
				for (Individual sourceChild : sourceFamily.getChildren()) {
					if (targetFamily.getChildren().getById(sourceChild.getId()) == null) {
						Individual targetChild = result.individuals().getById(sourceChild.getId());
						targetFamily.getChildren().add(targetChild);
						targetChild.setOriginFamily(targetFamily);
					}
				}

				if (sourceFamily.hasMarried()) {
					targetFamily.setMarried(true);
				}

				targetFamily.attributes().addAll(sourceFamily.attributes());
			}

			//
			if (sourceFamily.getHusband() != null) {
				Individual targetParent = result.individuals().getById(sourceFamily.getHusband().getId());
				targetParent.getPersonalFamilies().add(targetFamily);
			}

			//
			if (sourceFamily.getWife() != null) {
				Individual targetParent = result.individuals().getById(sourceFamily.getWife().getId());
				targetParent.getPersonalFamilies().add(targetFamily);
			}
		}

		//
		for (RelationModel model : source.relationModels()) {
			result.relationModels().add(new RelationModel(model));
		}

		//
		for (Relation sourceRelation : source.relations()) {
			//
			Relation targetRelation = new Relation(sourceRelation.getId(), sourceRelation.getTypedId(), result.relationModels().getByName(
					sourceRelation.getModel().getName()), sourceRelation.getName());
			result.relations().add(targetRelation);

			//
			for (Actor sourceActor : sourceRelation.actors()) {
				result.createRelationActor(targetRelation, sourceActor.getId(), sourceActor.getRole().getName());
			}
		}

		//
		return result;
	}


/*	public static Net copy(final Net source) {
		Net result;

		result = new Net(source);

		//
		return result;
	}


	public static Net copyWithMarriedCoparents(final Net source, final Report report) {
		Net result;

		result = new Net(source);
		marryCoparents(result, report);

		//
		return result;
	}


	public static Net copyWithoutSingles(final Net source, final Report report) {
		Net result;

		result = new Net(source);
		eliminateSingles(result, report);

		//
		return result;
	}


	public static Net copyWithoutStructuralChildren(final Net source, Report report) {
		Net result;

		result = new Net(source);
		eliminateStructuralChildren(result, report);

		//
		return result;
	}


	public static Net copyWithoutVirtuals(final Net source, Report report) {
		Net result;

		result = new Net(source);
		eliminateVirtuals(result, report);

		//
		return result;
	}*/
	
	private static void actorsToChildren(Net net, Relation relation, String roleName, Individual husband, Individual wife, boolean married, Report report, StringList errorReport){
		
		Actors actors = relation.actors().getByRole(roleName);
		
		if (!actors.isEmpty()) {
			for (Actor childActor : actors.toSortedList()) {
				Individual child = childActor.getIndividual();
				Individual father = child.getFather();
				Individual mother = child.getMother();
				
				if ((father!=null && !father.equals(husband)) || (mother!=null && !mother.equals(wife))){
					errorReport.appendln("Contradictory parents for "+child+": "+husband+"+"+wife+" vs "+mother+"+"+father+" ("+relation+")");
				} else {
					Family family = createFamily(net,relation,husband,wife,married,report);
					if (family!=null && !family.getChildren().contains(child)){
						if (child.getOriginFamily()!=null){
							report.outputs().appendln("\tMoved child "+child+" from family "+child.getOriginFamily()+" to family "+family);
							child.getOriginFamily().getChildren().removeById(child.getId());
							if (child.getOriginFamily().attributes().isEmpty() && child.getOriginFamily().getChildren().isEmpty()){
								net.families().removeById(child.getOriginFamily().getId());
								report.outputs().appendln("Family "+child.getOriginFamily()+" merged with family "+family);
							}
						} else {
							report.outputs().appendln("\tAdded child "+child+" to family "+family);
						}
						child.setOriginFamily(family);
						family.getChildren().put(child);
						relation.removeActor(childActor);
					}
				}
			}
		}
	}
	
	private static Family createFamily(Net net, Relation relation, Individual husband, Individual wife, boolean married,Report report){
		Family result;
		
		result = null;
		
		if (husband != null || wife != null) {
			result = net.families().getBySpouses(husband, wife);
			if (result==null){
				if (husband!=null && wife==null && husband.getPersonalFamilies().size()==1 && husband.spouses().isEmpty()){
					result = husband.getPersonalFamilies().getFirst();
				} else if (wife!=null && husband==null && wife.getPersonalFamilies().size()==1 && wife.spouses().isEmpty()){
					result = wife.getPersonalFamilies().getFirst();
				} else {
					result = net.createFamily(husband, wife, new Individuals());
					if (married){
						result.setMarried();
					}
					report.outputs().appendln("Created family "+result+" from "+relation);
				}
			} 
			result.attributes().addAll(relation.attributes());
			String sourceRelation = relation.getModel()+" "+relation.getId();
			String sourceRelations = result.getAttributeValue("SOURCE");
			if (sourceRelations==null){
				result.setAttribute("SOURCE", sourceRelation);
			} else {
				result.setAttribute("SOURCE", sourceRelations+";"+sourceRelation);
			}
		}
		//
		return result;
	}

	/**
	 * Direct kinship relations for both sides
	 * Extended kinship relations still only for husband's side (for virilocal census data...), to be completed
	 * @param source
	 * @param relationLabel
	 * @param alternativeLabel
	 * @param labels
	 * @param report
	 * @return
	 */
	public static Net createFamiliesFromRelations(final Net source, final String relationLabel, final String alternativeLabel, final Map<String, String> labels, Report report) {
		Net result;

		result = new Net(source);
		result.setDefaultIdStrategy(IdStrategy.APPEND);

		String husbandLabel = labels.get("husband");
		String wifeLabel = labels.get("wife");
		String sonLabel = labels.get("son");
		String daughterLabel = labels.get("daughter");
		
		String otherWifeLabel = labels.get("otherWife");
		String otherHusbandLabel = labels.get("otherHusband");

		String husbandsOtherSonLabel = labels.get("husbandsOtherSon");
		String husbandsOtherDaughterLabel = labels.get("husbandsOtherDaughter");
		String husbandsNaturalSonLabel = labels.get("husbandsNaturalSon");
		String husbandsNaturalDaughterLabel = labels.get("husbandsNaturalDaughter");
		String husbandMotherLabel = labels.get("husbandMother");
		String husbandFatherLabel = labels.get("husbandFather");
		
		String wifesOtherSonLabel = labels.get("wifesOtherSon");
		String wifesOtherDaughterLabel = labels.get("wifesOtherDaughter");
		String wifesNaturalSonLabel = labels.get("wifesNaturalSon");
		String wifesNaturalDaughterLabel = labels.get("wifesNaturalDaughter");
		String wifeMotherLabel = labels.get("wifeMother");
		String wifeFatherLabel = labels.get("wifeFather");
		
		String brotherLabel = labels.get("brother");
		String sisterLabel = labels.get("sister");
		
		RelationModel alternativeModel = result.relationModels().getByName(alternativeLabel);
		if (alternativeModel==null){
			alternativeModel = new RelationModel(alternativeLabel);
		}
		
		StringList errors = new StringList();
		
		for (Relation relation : result.relations().getByPartialModelName(relationLabel)) {

			Individual husband = null;
			Individual wife = null;

			Actors husbandActors = relation.actors().getByRole(husbandLabel);
			if (!husbandActors.isEmpty()) {
				if (husbandActors.size() == 1) {
					husband = husbandActors.get(0).getIndividual();
					relation.removeActor(husbandActors.get(0));
				} else {
					errors.appendln("Multiple "+husbandLabel+"s for relation " + relation+": "+husbandActors.toSortedList());
				}
			}
			Actors wifeActors = relation.actors().getByRole(wifeLabel);
			if (!wifeActors.isEmpty()) {
				if (wifeActors.size() == 1) {
					wife = wifeActors.get(0).getIndividual();
					relation.removeActor(wifeActors.get(0));
				} else {
					errors.appendln("Multiple "+wifeLabel+"s for relation " + relation+": "+husbandActors.toSortedList());
				}
			}
			
			createFamily(result,relation,husband,wife,true,report);
			actorsToChildren(result, relation,sonLabel,husband, wife, true, report,errors);
			if (daughterLabel!=null && !daughterLabel.equals(sonLabel)){
				actorsToChildren(result, relation,daughterLabel,husband, wife, true, report,errors);
			}

			// Create family
/*			if (husband != null || wife != null || children.size() > 0) {
				Family family = result.families().getBySpouses(husband, wife);
				if (family==null){
					family = result.createFamily(husband, wife, children);
					family.setMarried();
				} else {
					for (Individual child : children){
						if (!family.getChildren().contains(child)){
							if (child.getOriginFamily()!=null){
								errors.outputs().appendln("Multiple origin families for child "+child);
							} else {
								child.setOriginFamily(family);
								family.getChildren().put(child);
							}
						}
					}
				}
				family.attributes().addAll(relation.attributes());
			}*/

			Actors otherWifeActors = relation.actors().getByRole(otherWifeLabel);
			if (!otherWifeActors.isEmpty()) {
				for (Actor otherWifeActor : otherWifeActors.toSortedList()) {
					Family family = result.createFamily(husband, otherWifeActor.getIndividual());
					family.setMarried();
					report.outputs().appendln("Created family "+family+" from "+relation);
					relation.removeActor(otherWifeActor);
				}
			}

			Actors otherHusbandActors = relation.actors().getByRole(otherHusbandLabel);
			if (!otherHusbandActors.isEmpty()) {
				for (Actor otherHusbandActor : otherHusbandActors.toSortedList()) {
					Family family = result.createFamily(otherHusbandActor.getIndividual(),wife);
					family.setMarried();
					report.outputs().appendln("Created family "+family+" from "+relation);
					relation.removeActor(otherHusbandActor);
				}
			}
			
			actorsToChildren(result,relation,husbandsOtherSonLabel,husband,null,true,report,errors);
			actorsToChildren(result,relation,husbandsOtherDaughterLabel,husband,null,true,report,errors);
			actorsToChildren(result,relation,husbandsNaturalSonLabel,husband,null,false,report,errors);
			actorsToChildren(result,relation,husbandsNaturalDaughterLabel,husband,null,false,report,errors);
			actorsToChildren(result,relation,wifesOtherSonLabel,null,husband,true,report,errors);
			actorsToChildren(result,relation,wifesOtherDaughterLabel,null,husband,true,report,errors);
			actorsToChildren(result,relation,wifesNaturalSonLabel,null,husband,false,report,errors);
			actorsToChildren(result,relation,wifesNaturalDaughterLabel,null,husband,false,report,errors);
		}


		for (Individual husband : result.individuals()) {
			for (Relation relation : husband.relations().getByPartialModelName(relationLabel)) {
				
				if (relation.getIndividuals(husbandLabel).contains(husband)) {
					
					if (relation.hasActors(brotherLabel, sisterLabel, husbandMotherLabel)) {
						Individual husbandFather = null;
						Individual husbandMother = null;
						Individuals husbandSiblings = new Individuals();
						husbandSiblings.add(husband);

						Actors husbandFatherActors = relation.actors().getByRole(husbandFatherLabel);
						if (!husbandFatherActors.isEmpty()) {
							if (husbandFatherActors.size() == 1) {
								husbandFather = husbandFatherActors.get(0).getIndividual();
								relation.removeActor(husbandFatherActors.get(0));
							} else {
								report.outputs().appendln("Multiple husbandFathers for role " + husbandFatherLabel);
							}
						}
						Actors husbandMotherActors = relation.actors().getByRole(husbandMotherLabel);
						if (!husbandMotherActors.isEmpty()) {
							if (husbandMotherActors.size() == 1) {
								husbandMother = husbandMotherActors.get(0).getIndividual();
								relation.removeActor(husbandMotherActors.get(0));
							} else {
								report.outputs().appendln("Multiple husbandMothers for role " + husbandMotherLabel);
							}
						}
						Actors brotherActors = relation.actors().getByRole(brotherLabel);
						if (!brotherActors.isEmpty()) {
							for (Actor childActor : brotherActors.toSortedList()) {
								husbandSiblings.add(childActor.getIndividual());
								relation.removeActor(childActor);
							}
						}

						Actors sisterActors = relation.actors().getByRole(sisterLabel);
						if (!sisterActors.isEmpty()) {
							for (Actor childActor : sisterActors.toSortedList()) {
								husbandSiblings.add(childActor.getIndividual());
								relation.removeActor(childActor);
							}
						}

						Family husbandOriginFamily = husband.getOriginFamily();
						if (husbandOriginFamily == null) {
							husbandOriginFamily = result.createFamily(husbandFather, husbandMother, husbandSiblings);
						}
						if (husbandOriginFamily.getFather() == null) {
							husbandOriginFamily.setFather(husbandFather);
						} else if (husbandOriginFamily.getFather().equals(husbandFather)) {
							report.outputs().appendln("Multiple husbandFathers for role " + husbandFatherLabel);
						}
						if (husbandOriginFamily.getMother() == null) {
							husbandOriginFamily.setMother(husbandMother);
						} else if (husbandOriginFamily.getMother().equals(husbandMother)) {
							report.outputs().appendln("Multiple husbandMothers for role " + husbandMotherLabel);
						}
						for (Individual husbandSibling : husbandSiblings) {
							husbandOriginFamily.getChildren().add(husbandSibling);
							husbandSibling.setOriginFamily(husbandOriginFamily);
						}
					}
				}
			}
		}
		
		for (Individual wife : result.individuals()) {
			for (Relation relation : wife.relations().getByPartialModelName(relationLabel)) {
				
				if (relation.getIndividuals(wifeLabel).contains(wife)) {
					
					if (relation.hasActors(brotherLabel, sisterLabel, wifeMotherLabel)) {
						Individual wifeFather = null;
						Individual wifeMother = null;
						Individuals wifeSiblings = new Individuals();
						wifeSiblings.add(wife);

						Actors wifeFatherActors = relation.actors().getByRole(wifeFatherLabel);
						if (!wifeFatherActors.isEmpty()) {
							if (wifeFatherActors.size() == 1) {
								wifeFather = wifeFatherActors.get(0).getIndividual();
								relation.removeActor(wifeFatherActors.get(0));
							} else {
								report.outputs().appendln("Multiple wifeFathers for role " + wifeFatherLabel);
							}
						}
						Actors wifeMotherActors = relation.actors().getByRole(wifeMotherLabel);
						if (!wifeMotherActors.isEmpty()) {
							if (wifeMotherActors.size() == 1) {
								wifeMother = wifeMotherActors.get(0).getIndividual();
								relation.removeActor(wifeMotherActors.get(0));
							} else {
								report.outputs().appendln("Multiple wifeMothers for role " + wifeMotherLabel);
							}
						}
						Actors brotherActors = relation.actors().getByRole(brotherLabel);
						if (!brotherActors.isEmpty()) {
							for (Actor childActor : brotherActors.toSortedList()) {
								wifeSiblings.add(childActor.getIndividual());
								relation.removeActor(childActor);
							}
						}

						Actors sisterActors = relation.actors().getByRole(sisterLabel);
						if (!sisterActors.isEmpty()) {
							for (Actor childActor : sisterActors.toSortedList()) {
								wifeSiblings.add(childActor.getIndividual());
								relation.removeActor(childActor);
							}
						}

						Family wifeOriginFamily = wife.getOriginFamily();
						if (wifeOriginFamily == null) {
							wifeOriginFamily = result.createFamily(wifeFather, wifeMother, wifeSiblings);
						}
						if (wifeOriginFamily.getFather() == null) {
							wifeOriginFamily.setFather(wifeFather);
						} else if (wifeOriginFamily.getFather().equals(wifeFather)) {
							report.outputs().appendln("Multiple wifeFathers for role " + wifeFatherLabel);
						}
						if (wifeOriginFamily.getMother() == null) {
							wifeOriginFamily.setMother(wifeMother);
						} else if (wifeOriginFamily.getMother().equals(wifeMother)) {
							report.outputs().appendln("Multiple wifeMothers for role " + wifeMotherLabel);
						}
						for (Individual wifeSibling : wifeSiblings) {
							wifeOriginFamily.getChildren().add(wifeSibling);
							wifeSibling.setOriginFamily(wifeOriginFamily);
						}
					}
				}
			}
		}
		
		if (!errors.isEmpty()){
			report.outputs().appendln();
			report.outputs().appendln(errors.size()+" errors found:");
			report.outputs().append(errors);
		}
		
		int alternativeId = result.relations().getByModel(alternativeModel).size()+1;
		
		for (Relation relation : result.relations().toSortedList()){
			if (StringUtils.containsIgnoreCase(relation.getModel().getName(), relationLabel)){
				if (!relation.actors().isEmpty()){
					Relation alternativeRelation = result.createRelation(relation.getName()+" ("+relation.getModel()+" "+relation.getId()+")", alternativeModel,relation.actors());
					alternativeRelation.setAttribute("SOURCE", relation+"");
					alternativeRelation.setTypedId(alternativeId);
					alternativeId++;
				}
				result.remove(relation);
			}
		}
		
		for (RelationModel model : new ArrayList<RelationModel>(source.relationModels())){
			if (StringUtils.containsIgnoreCase(model.getName(), relationLabel)){
				result.remove(model);
			}
		}
		//
		return result;

	}

	public static void createIndividualsFromFamilyAttributes(final Net net, final Family family, final String husbandFirstNameLabel,
			final String husbandLastNameLabel, final String wifeFirstNameLabel, final String wifeLastNameLabel, final Map<String, String> husbandLabels,
			final Map<String, String> wifeLabels) throws PuckException {

/*		String husbandFirstName = family.getAttributeValue(husbandFirstNameLabel);
		if (husbandFirstName == null) {
			husbandFirstName = "";
		}
		String husbandLastName = family.getAttributeValue(husbandLastNameLabel);
		if (husbandLastName == null) {
			husbandLastName = "";
		}
		String wifeFirstName = family.getAttributeValue(wifeFirstNameLabel);
		if (wifeFirstName == null) {
			wifeFirstName = "";
		}
		String wifeLastName = family.getAttributeValue(wifeLastNameLabel);
		if (wifeLastName == null) {
			wifeLastName = "";
		}

		Individual husband = null;
		Individual wife = null;

		if (husbandFirstName != null || husbandLastName != null) {
			husband = new Individual(net.individuals().nextFreeId(net.getDefaultIdStrategy()), husbandFirstName + " / " + husbandLastName, Gender.MALE);
			for (String famLabel : husbandLabels.keySet()) {
				String indLabel = husbandLabels.get(famLabel);
				String value = family.getAttributeValue(famLabel);
				if (value != null) {
					husband.setAttribute(indLabel, value);
				}
			}
			net.individuals().put(husband);
			family.setHusband(husband);
			husband.addPersonalFamily(family);
		}
		if (wifeFirstName != null || wifeLastName != null) {
			wife = new Individual(net.individuals().nextFreeId(net.getDefaultIdStrategy()), wifeFirstName + " / " + wifeLastName, Gender.FEMALE);
			for (String famLabel : wifeLabels.keySet()) {
				String indLabel = wifeLabels.get(famLabel);
				String value = family.getAttributeValue(famLabel);
				if (value != null) {
					wife.setAttribute(indLabel, value);
				}
			}
			net.individuals().put(wife);
			family.setWife(wife);
			wife.addPersonalFamily(family);
		}*/
		
		Individual husband = family.getHusband();
		Individual wife = family.getWife();
		
		if (husband!=null){
			
			String wife2FirstName = family.getAttributeValue("HUSB_esanpr");
			if (wife2FirstName==null){
				wife2FirstName = "";
			}
			String wife2LastName = family.getAttributeValue("HUSB_esanno");
			if (wife2LastName==null){
				wife2LastName = "";
			}
			String wife2LastName2 = family.getAttributeValue("HUSB_esanbi");
			if (wife2LastName2==null){
				wife2LastName2 = "";
			}

			if (StringUtils.isNotBlank(wife2FirstName+wife2LastName+wife2LastName2)){
				Individual wife2 = new Individual(net.individuals().nextFreeId(net.getDefaultIdStrategy()), wife2FirstName + " / " + wife2LastName + " / " + wife2LastName2, Gender.FEMALE);
				if (StringUtils.isNotBlank(wife2LastName2) || wife2LastName.contains(",")){
					wife2.setAttribute("CHECK", "Noms et Parents");
				}
				net.individuals().add(wife2);
				Family family2 = net.createFamily(net.families().nextFreeId(net.getDefaultIdStrategy()), husband, wife2,UnionStatus.MARRIED);
				if (husband.spouses().size()<3){
					family.setHusbandOrder(2);
					family2.setHusbandOrder(1);
				} else {
					System.err.println("Check spouse order for W of "+husband+": "+husband.spouses());
				}
			}
		}

		if (wife!=null){
			
			String husband2FirstName = family.getAttributeValue("WIFE_esanpr");
			if (husband2FirstName==null){
				husband2FirstName = "";
			}
			String husband2LastName = family.getAttributeValue("WIFE_esanno");
			if (husband2LastName==null){
				husband2LastName = "";
			}
			String husband2LastName2 = family.getAttributeValue("WIFE_esanbi");
			if (husband2LastName2==null){
				husband2LastName2 = "";
			}

			if (StringUtils.isNotBlank(husband2FirstName+husband2LastName+husband2LastName2)){
				Individual husband2 = new Individual(net.individuals().nextFreeId(net.getDefaultIdStrategy()), husband2FirstName + " / " + husband2LastName + " / " + husband2LastName2, Gender.MALE);
				if (StringUtils.isNotBlank(husband2LastName2) || husband2LastName.contains(",")){
					husband2.setAttribute("CHECK", "Noms et Parents");
				}
				net.individuals().add(husband2);
				Family family2 = net.createFamily(net.families().nextFreeId(net.getDefaultIdStrategy()), husband2,wife, UnionStatus.MARRIED);
				if (wife.spouses().size()<3){
					family.setWifeOrder(2);
					family2.setWifeOrder(1);
				} else {
					System.err.println("Check spouse order for H of "+wife+": "+wife.spouses());
				}
			}
		}

		
		
	}

	/**
	 * 
	 * 
	 * @param net
	 * @throws PuckException
	 */
	public static RelationModel createLifeEvents(final Net net) throws PuckException {
		RelationModel result;

		//
		RelationModel model = net.createRelationModel("BIO");

		//
		model.roles().add(new Role("EGO"));
		model.roles().add(new Role("SPOUSE"));
		model.roles().add(new Role("PARENT"));
		model.roles().add(new Role("CHILD"));

		//
		for (Individual individual : net.individuals()) {

			for (Attribute attribute : individual.attributes()) {
				if (attribute.getLabel().contains("DATE") && attribute.getLabel().indexOf("_") > -1 && !attribute.getValue().equals("0")) {
					String label = attribute.getLabel().substring(0, attribute.getLabel().indexOf("_"));
					Relation relation = net.createRelation(net.relations().getFirstFreeId(), label + " " + individual.toString(), model);
					//
					relation.setAttribute("DATE", attribute.getValue());
					String place = individual.getAttributeValue(label + "_PLAC");
					if (place != null) {
						relation.setAttribute("PLAC", place);
					}

					if (label.equals("BIRT")) {
						net.createRelationActor(relation, individual.getId(), "CHILD");
						if (individual.getFather() != null) {
							net.createRelationActor(relation, individual.getFather().getId(), "PARENT");
						}
						if (individual.getMother() != null) {
							net.createRelationActor(relation, individual.getMother().getId(), "PARENT");
						}
					} else {
						net.createRelationActor(relation, individual.getId(), "EGO");
					}
				}
			}
		}

		//
		for (Family family : net.families()) {

			for (Attribute attribute : family.attributes()) {
				if (attribute.getLabel().contains("DATE") && attribute.getLabel().indexOf("_") > -1 && !attribute.getValue().equals("0")) {
					String label = attribute.getLabel().substring(0, attribute.getLabel().indexOf("_"));
					Relation relation = net.createRelation(net.relations().getFirstFreeId(), label + " " + family.toString(), model);

					relation.setAttribute("DATE", attribute.getValue());
					String place = family.getAttributeValue(label + "_PLAC");
					if (place != null) {
						relation.setAttribute("PLAC", place);
					}

					//
					if (family.getHusband() != null) {
						net.createRelationActor(relation, family.getHusband().getId(), "SPOUSE");
					}
					if (family.getWife() != null) {
						net.createRelationActor(relation, family.getWife().getId(), "SPOUSE");
					}
				}
			}
		}

		//
		result = model;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Graph<Individual> createBinaryRelationGraph(final Segmentation source) {
		Graph<Individual> result;

		//
		result = new Graph<Individual>("MultiRelation graph " + source.getLabel());

		//
		for (Individual individual : source.getCurrentIndividuals().toSortedList()) {
			result.addNode(individual);
		}
		
		Map<RelationModel,Integer> edgeValues = new HashMap<RelationModel,Integer>();
		Map<RelationModel,Integer> arcValues = new HashMap<RelationModel,Integer>();
		
		for (Relation relation: source.getCurrentRelations()){

			Individuals individuals = relation.getIndividuals();

//			if (individuals.size()==2){
				
				RelationModel model = relation.getModel();
				
				Integer edgeValue = edgeValues.get(model);
				Integer arcValue = arcValues.get(model);
								
				if (edgeValue == null && arcValue == null){
					
					if (model.roles().size()==1){
						
						edgeValues.put(model, edgeValues.size()+1);
						edgeValue = edgeValues.get(model);
						
					} else if (model.roles().size()>1){
						
						arcValues.put(model, arcValues.size()+1);
						arcValue = arcValues.get(model);
					}
				}
								
				if (edgeValue != null){
					
					List<Individual> list = individuals.toList();
					
					for (int i=0;i<list.size();i++){

						for (int j=i+1;j<list.size();j++){
						
							result.addEdge(list.get(i), list.get(j), edgeValue);
							
						}
					}
				} 
				
				if (arcValue != null) {
					
					StringList roles = model.roles().toSortedNameList();
					
					List<Individual> sourceList = new ArrayList<Individual>();
					List<Individual> targetList = new ArrayList<Individual>();
					
					// Optionalize!
					
					for (int i=0;i<roles.size()-1;i++){
						sourceList.addAll(relation.getIndividuals(roles.get(i)).toList());
					}
					
					targetList.addAll(relation.getIndividuals(roles.getLast()).toList());

					for (int i=0;i<sourceList.size();i++){

						for (int j=0;j<targetList.size();j++){
						
							result.addArc(sourceList.get(i), targetList.get(j), arcValue);
							
						}
					}
				}
			}
//		}

		//
		NetUtils.setGenderShapes(result);

		//
		return result;
	}
	
	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Graph<Numberable> createBimodalRelationGraph(final Segmentation source) {
		Graph<Numberable> result;

		//
		result = new Graph<Numberable>("Bimodal Relation graph " + source.getLabel());

		//
		for (Individual individual : source.getCurrentIndividuals().toSortedList()) {

			result.addNode(individual);
			result.getNode(individual).setAttribute("Mode", "I");

		}
		
		for (Relation relation: source.getCurrentRelations()){
			
			result.addNode(relation);
			
			RelationModel model = relation.getModel();
			
			result.getNode(relation).setAttribute("Type", model.getName());
			result.getNode(relation).setAttribute("Mode", "II");

			for (Actor actor : relation.actors()){
				
				Individual individual = actor.getIndividual();
				int roleIndex = model.getRoleIndex(actor.getRole());
			
				result.addArc(relation, individual, roleIndex+1);
			}
		}

		//
		return result;
	}

	
	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Graph<Individual> createOreGraph(final Segmentation source) {
		Graph<Individual> result;

		//
		result = new Graph<Individual>("Ore graph " + source.getLabel());

		//
		for (Individual individual : source.getCurrentIndividuals().toSortedList()) {
			result.addNode(individual);
		}

		//
		for (Family family : source.getCurrentFamilies()) {
			Individual father = family.getHusband();
			Individual mother = family.getWife();
			if (father != null && mother != null && family.hasMarried()) {
				result.addEdge(father, mother, 1);
			}
			if (!family.isSterile()) {
				for (Individual child : family.getChildren()) {
					if (father != null) {
						result.addArc(father, child, 1);
					}
					if (mother != null) {
						result.addArc(mother, child, 1);
					}
				}
			}
		}

		//
		NetUtils.setGenderShapes(result);

		//
		return result;
	}

	public static void createOriginFamilyFromIndividualAttributes(final Net net, final Individual ego, final String fatherFirstNameLabel,
			final String fatherLastNameLabel, final String motherFirstNameLabel, final String motherLastNameLabel) {

		String fatherFirstName = ego.getAttributeValue(fatherFirstNameLabel);
		if (fatherFirstName == null) {
			fatherFirstName = "";
		}
		String fatherLastName = ego.getAttributeValue(fatherLastNameLabel);
		if (fatherLastName == null) {
			fatherLastName = "";
		}
		String motherFirstName = ego.getAttributeValue(motherFirstNameLabel);
		if (motherFirstName == null) {
			motherFirstName = "";
		}
		String motherLastName = ego.getAttributeValue(motherLastNameLabel);
		if (motherLastName == null) {
			motherLastName = "";
		}

		Individual father = null;
		Individual mother = null;

		if (!StringUtils.isBlank(fatherFirstName + fatherLastName)) {
			father = new Individual(net.individuals().nextFreeId(net.getDefaultIdStrategy()), fatherFirstName + " / " + fatherLastName, Gender.MALE);
			net.individuals().put(father);
		}
		if (!StringUtils.isBlank(motherFirstName + motherLastName)) {
			mother = new Individual(net.individuals().nextFreeId(net.getDefaultIdStrategy()), motherFirstName + " / " + motherLastName, Gender.FEMALE);
			net.individuals().put(mother);
		}

		if (father != null || mother != null) {
			net.createFamily(father, mother, ego);
		}
	}
	
	public static void createGrandParentsFromIndividualAttributes(final Net net, final Individual ego, final String FFFirstNameLabel,
			final String FFLastNameLabel, final String FMFirstNameLabel, final String FMLastNameLabel, final String MFFirstNameLabel,
			final String MFLastNameLabel, final String MMFirstNameLabel, final String MMLastNameLabel) {

		String FFFirstName = ego.getAttributeValue(FFFirstNameLabel);
		if (FFFirstName == null) {
			FFFirstName = "";
		}
		String FFLastName = ego.getAttributeValue(FFLastNameLabel);
		if (FFLastName == null) {
			FFLastName = "";
		}
		String FMFirstName = ego.getAttributeValue(FMFirstNameLabel);
		if (FMFirstName == null) {
			FMFirstName = "";
		}
		String FMLastName = ego.getAttributeValue(FMLastNameLabel);
		if (FMLastName == null) {
			FMLastName = "";
		}
		String MFFirstName = ego.getAttributeValue(MFFirstNameLabel);
		if (MFFirstName == null) {
			MFFirstName = "";
		}
		String MFLastName = ego.getAttributeValue(MFLastNameLabel);
		if (MFLastName == null) {
			MFLastName = "";
		}
		String MMFirstName = ego.getAttributeValue(MMFirstNameLabel);
		if (MMFirstName == null) {
			MMFirstName = "";
		}
		String MMLastName = ego.getAttributeValue(MMLastNameLabel);
		if (MMLastName == null) {
			MMLastName = "";
		}

		Individual FF = null;
		Individual FM = null;
		Individual MF = null;
		Individual MM = null;
		
		Individual orFF = null;
		Individual orFM = null;
		Individual orMF = null;
		Individual orMM = null;
		
		Individual father = ego.getFather();
		Individual mother = ego.getMother();
		
		if (father!=null){
			orFF = father.getFather();
			orFM = father.getMother();
		}
		
		if (mother!=null){
			orMF = mother.getFather();
			orMM = mother.getMother();
		}

		if (!StringUtils.isBlank(FFFirstName + FFLastName)) {
			if (orFF==null){
				FF = new Individual(net.individuals().nextFreeId(net.getDefaultIdStrategy()), FFFirstName + " / " + FFLastName, Gender.MALE);
				net.individuals().put(FF);
			} else if (!orFF.hasNameConsistentWith(FFFirstName, FFLastName)){
				System.err.println("Name inconsistency for FF of "+ego+": "+FFFirstName+" / "+FFLastName+" blocked by "+orFF.getName());
			}
		}
		
		if (!StringUtils.isBlank(FMFirstName + FMLastName)) {
			if (orFM==null){
				FM = new Individual(net.individuals().nextFreeId(net.getDefaultIdStrategy()), FMFirstName + " / " + FMLastName, Gender.FEMALE);
				net.individuals().put(FM);
			} else if (!orFM.hasNameConsistentWith(FMFirstName, FMLastName)){
				System.err.println("Name inconsistency for FM of "+ego+": "+FMFirstName+" / "+FMLastName+" blocked by "+orFM.getName());
			}
		}
		
		if (FF != null || FM != null) {
			if (father==null){
				father = new Individual(net.individuals().nextFreeId(net.getDefaultIdStrategy()), "[father of "+ego.getName()+"]", Gender.MALE);
				net.individuals().put(father);
				System.err.println("Missing father for "+ego);
			}
			net.createFamily(FF, FM, father);
		}

		if (!StringUtils.isBlank(MFFirstName + MFLastName)) {
			if (orMF==null){
				MF = new Individual(net.individuals().nextFreeId(net.getDefaultIdStrategy()), MFFirstName + " / " + MFLastName, Gender.MALE);
				net.individuals().put(MF);
			} else if (!orMF.hasNameConsistentWith(MFFirstName, MFLastName)){
				System.err.println("Name inconsistency for MF of "+ego+": "+MFFirstName+" / "+MFLastName+" blocked by "+orMF.getName());
			}
		}
		
		if (!StringUtils.isBlank(MMFirstName + MMLastName)) {
			if (orMM==null){
				MM = new Individual(net.individuals().nextFreeId(net.getDefaultIdStrategy()), MMFirstName + " / " + MMLastName, Gender.FEMALE);
				net.individuals().put(MM);
			} else if (!orMM.hasNameConsistentWith(MMFirstName, MMLastName)){
				System.err.println("Name inconsistency for MM of "+ego+": "+MMFirstName+" / "+MMLastName+" blocked by "+orMM.getName());
			}
		}
		
		if (MF != null || MM != null) {
			if (mother==null){
				mother = new Individual(net.individuals().nextFreeId(net.getDefaultIdStrategy()), "[mother of "+ego.getName()+"]", Gender.FEMALE);
				net.individuals().put(mother);
				System.err.println("Missing mother for "+ego);
			}
			net.createFamily(MF, MM, mother);
		}



	}



	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Graph<Family> createPGraph(final Segmentation source) throws PuckException {
		Graph<Family> result;
		//
		result = new Graph<Family>("Pgraph " + source.getLabel());

		// getAllFamilies would be better
		int familyCount = source.getCurrentFamilies().getLastId();

		for (Individual individual : source.getCurrentIndividuals()) {
			Family originFamily = individual.getOriginFamily();
			if (originFamily == null) {
				familyCount++;
				originFamily = new Family(familyCount);
				originFamily.getChildren().add(individual);
			}
			Families personalFamilies = individual.getPersonalFamilies();
			if (personalFamilies.size() == 0) {
				familyCount++;
				Family personalFamily = new Family(familyCount);
				if (individual.isMale()) {
					personalFamily.setHusband(individual);
				} else if (individual.isFemale()) {
					personalFamily.setWife(individual);
				}
			}
			for (Family personalFamily : personalFamilies) {
				int weight = 1 - 2 * (individual.getGender().toInt() % 2);

				result.addArc(originFamily, personalFamily, weight);
			}
		}

		//
		return result;

	}

	public static Graph<Individual> createRelationGraph(final Individual source, final String relationModelName, final String egoRoleName,
			final String alterRoleName) {
		Graph<Individual> result;

		//
		result = new Graph<Individual>("Ego network " + source + " " + relationModelName + " " + egoRoleName + "-" + alterRoleName);

		Map<String, Integer> tagMap = new HashMap<String, Integer>();

		result.addNode(source);
		//
		for (Relation relation : source.relations()) {
			if (relation.getRoleNames(source).contains(egoRoleName)) {
				Individuals alters;
				if (alterRoleName.equals("ALL")) {
					alters = relation.getIndividuals();
				} else {
					alters = relation.getIndividuals(alterRoleName);
				}
				for (Individual alter : alters) {
					result.addNode(alter);
				}
			}
		}

		//
		for (Individual ego : result.getReferents()) {
			for (Relation relation : ego.relations().getByModelName(relationModelName)) {
				for (Individual alter : relation.getIndividuals()) {
					if (result.getReferents().contains(alter)) {
						for (String egoRole : relation.getRoleNames(ego)) {
							for (String alterRole : relation.getRoleNames(alter)) {
								Link<Individual> link = null;
								if (egoRole.equals(alterRole)) {
									if (ego.getId() < alter.getId()) {
										link = result.addEdge(ego, alter, 1);
									}
								} else if (egoRole.compareTo(alterRole) < 0) {
									link = result.addArc(ego, alter, 1);
								}
								if (link != null) {
									String tag = egoRole + "-" + alterRole;
									Integer tagNumber = tagMap.get(tag);
									if (tagNumber == null) {
										tagNumber = tagMap.size() + 1;
										tagMap.put(tag, tagNumber);
									}
									link.setTag(":" + tagNumber + " '" + tag + "'");
								}
							}
						}
					}
				}
			}
		}

		//
		NetUtils.setGenderShapes(result);

		//
		return result;
	}

	public static Graph<Individual> createRelationGraph(final Segmentation source, final String relationName) {
		Graph<Individual> result;

		//
		result = new Graph<Individual>("Relation graph " + relationName + " " + source.getLabel());

		//
		for (Individual individual : source.getCurrentIndividuals().toSortedList()) {
			result.addNode(individual);
		}

		Map<String, Integer> tagMap = new HashMap<String, Integer>();

		//
		for (Relation relation : source.getCurrentRelations().getByModelName(relationName)) {
			for (Actor egoActor : relation.actors()) {
				String egoRole = egoActor.getRole().getName();
				Individual ego = egoActor.getIndividual();
				for (Actor alterActor : relation.actors()) {
					String alterRole = alterActor.getRole().getName();
					Individual alter = alterActor.getIndividual();
					Link<Individual> link = null;
					if (egoRole.equals(alterRole)) {
						if (ego.getId() < alter.getId()) {
							link = result.addEdge(ego, alter, 1);
						}
					} else if (egoRole.compareTo(alterRole) < 0) {
						link = result.addArc(ego, alter, 1);
					}
					if (link != null) {
						String tag = egoRole + "-" + alterRole;
						Integer tagNumber = tagMap.get(tag);
						if (tagNumber == null) {
							tagNumber = tagMap.size() + 1;
							tagMap.put(tag, tagNumber);
						}
						link.setTag(":" + tagNumber + " '" + tag + "'");
					}
				}
			}
		}

		//
		NetUtils.setGenderShapes(result);

		//
		return result;
	}
	

	public static long createRelationsFromAttributes(final Net net, final Segmentation segmentation, final AttributeToRelationCriteria criteria) throws PuckException {
		long result;
		
		result = 0;
		
		// Actually restricted to individuals, enlarge later...
		Scope scope = criteria.getScope();
		String relationLabel = criteria.getTargetRelationName();
		String attributeLabel = criteria.getLabel();
		String roleLabel = criteria.getRoleName();
		String eponymRoleLabel = criteria.getEponymRoleName();
		String dateSeparator = criteria.getDateSeparator();
		//

		RelationModel model = net.relationModels().getByName(relationLabel);
		if (model == null){
			model = net.createRelationModel(relationLabel);
		}
		
		int typedId = 0;
		
		if (scope == Scope.INDIVIDUALS){
			
			for (Individual individual : segmentation.getCurrentIndividuals()){

				String relationValue = null;
				roleLabel = criteria.getRoleName();

				for (Attribute attribute : individual.attributes()) {
					
					String dateValue = null;
					
					if (attribute.getLabel().equals(attributeLabel)) {

						relationValue = attribute.getValue();
						result++;

					} else {
						
						String[] attributeLabels = attribute.getLabel().split(dateSeparator);
						if (attributeLabels[0].equals(attributeLabel) && StringUtils.isNumeric(attributeLabels[1])){
							relationValue = attribute.getValue();
							dateValue = attributeLabels[1];
							result++;
						}
					}
					if (StringUtils.isEmpty(roleLabel) && attribute.getLabel().equals("ROLE")) {
						roleLabel = attribute.getValue();
					}
					
					if (relationValue!=null && !StringUtils.isEmpty(roleLabel)){
						
						net.createRelationRole(model, roleLabel);
						if (StringUtils.isNotBlank(eponymRoleLabel)){
							net.createRelationRole(model, eponymRoleLabel);
						}
						
						for (String typedIdString : relationValue.replaceAll(";", ",").split(",")){
							
							Relation relation = null;
							
							typedIdString = typedIdString.trim();
							
							if (StringUtils.isNumeric(typedIdString)){
								
								typedId = Integer.parseInt(typedIdString);
								relation = net.relations().getByTypedId(typedId, model);
								if (relation == null){
									relation = net.createRelation(typedId, relationLabel + " " + typedId, model);
								}
								
							} else {
								
								Relations relations = net.relations().getByName(typedIdString);
								
								if (!relations.isEmpty()){
									relation = relations.getFirst();
								} else {
									typedId++;
									relation = net.createRelation(typedId, typedIdString, model);
									if (StringUtils.isNotBlank(eponymRoleLabel)){
										net.createRelationActor(relation, typedId, eponymRoleLabel);
									}
								}
							}
							
							Actor actor = net.createRelationActor(relation, individual.getId(), roleLabel);
							if (actor!=null && dateValue!=null){
								String oldDateValue = actor.getAttributeValue("DATE");
								if (oldDateValue!=null){
									dateValue = oldDateValue+";"+dateValue;
								}
								actor.setAttribute("DATE", dateValue);
							}
						}
					}
				}
			}
			
		} else if (scope == Scope.RELATION){
						
			for (Relation sourceRelation : segmentation.getCurrentRelations().getByModelName(criteria.getSourceRelationName())){
				
				String relationValue = sourceRelation.getAttributeValue(criteria.getLabel());
				
				if (relationValue!=null){
					
					net.createRelationRole(model, roleLabel);
					if (StringUtils.isNotBlank(eponymRoleLabel)){
						net.createRelationRole(model, eponymRoleLabel);
					}
					
					for (String typedIdString : relationValue.replaceAll(";", ",").split(",")){
						
						Relation relation = null;
						
						typedIdString = typedIdString.trim();
												
						if (StringUtils.isNumeric(typedIdString)){
							
							typedId = Integer.parseInt(typedIdString);
							relation = net.relations().getByTypedId(typedId, model);
							if (relation == null){
								relation = net.createRelation(typedId, relationLabel + " " + typedId, model);
							}
							
						} else {
							
							Relations relations = net.relations().getByName(typedIdString);
							
							if (!relations.isEmpty()){
								relation = relations.getFirst();
							} else {
								typedId++;
								relation = net.createRelation(typedId, typedIdString, model);
								if (StringUtils.isNotBlank(eponymRoleLabel)){
									net.createRelationActor(relation, typedId, eponymRoleLabel);
								}
							}
						}
						
						for (Actor sourceActor : sourceRelation.actors().getByRole(criteria.getRoleName())){
							
							Actor actor = net.createRelationActor(relation, sourceActor.getIndividual().getId(), roleLabel);
							
							String dateValue = sourceRelation.getAttributeValue("DATE");
							if (actor!=null && dateValue!=null){
								String oldDateValue = actor.getAttributeValue("DATE");
								if (oldDateValue!=null){
									dateValue = oldDateValue+";"+dateValue;
								}
								actor.setAttribute("DATE", dateValue);
							}
							result++;
						}
					}
				}
			}
		}

		if (StringUtils.isNotBlank(eponymRoleLabel)){
			
			for (Relation relation : net.relations().getByModelName(relationLabel)){
				typedId = relation.getTypedId();
				Actor eponymActor = net.createRelationActor(relation, typedId, eponymRoleLabel);
				
				List<String> dateValues = new ArrayList<String>();
				for (Actor actor : relation.actors()){
					String dateValueString = actor.getAttributeValue("DATE");
					if (dateValueString!=null){
						for (String dateValue : dateValueString.split(";")){
							if (!dateValues.contains(dateValue)){
								dateValues.add(dateValue);
							}
						}
					}
				}
				Collections.sort(dateValues);
				String dateValueString = null;
				if (dateValues.size()>0){
					dateValueString = dateValues.get(0);
				} 
				for (int i=1;i<dateValues.size();i++){
					dateValueString+=";"+dateValues.get(i);
				}
				if (eponymActor!=null && dateValueString!=null){
					eponymActor.setAttribute("DATE", dateValueString);
				}
			}
		}


		//
		return result;

	}

	/**
	 * 
	 * 
	 * @param net
	 * @throws PuckException
	 */
	public static RelationModel createRelationsFromCircuits(final Net net, final CircuitFinder finder, final RelationModel model) throws PuckException {
		RelationModel result;

		//
		model.roles().add(new Role("PIVOT"));
		model.roles().add(new Role("INTERMEDIARY"));

		//
		int i = 1;
		for (Cluster<Chain> cluster : finder.getCircuits().getClusters().toListSortedByValue()) {
			for (Chain r : cluster.getItems()) {
				//
				Relation relation = net.createRelation(i, r.signature(Notation.NUMBERS), model);
				for (int j = 0; j < 2 * r.dim(); j++) {
					Individual pivot = r.getPivot(j);
					if (!relation.hasActor(pivot)) {
						net.createRelationActor(relation, pivot.getId(), "PIVOT");
					}
				}
				relation.attributes().add(new Attribute("TYPE", cluster.getValue().toString()));
				relation.attributes().add(new Attribute("CLASSIC", r.signature(Notation.CLASSIC_GENDERED)));

				for (Individual indi : r) {
					if (!relation.hasActor(indi)) {
						if (indi instanceof Couple) {
							if (((Couple) indi).getFirstId() > 0) {
								net.createRelationActor(relation, ((Couple) indi).getFirstId(), "INTERMEDIARY");
							}
							if (((Couple) indi).getSecondId() > 0) {
								net.createRelationActor(relation, ((Couple) indi).getSecondId(), "INTERMEDIARY");
							}
						} else {
							net.createRelationActor(relation, indi.getId(), "INTERMEDIARY");
						}
					}
				}
				i++;
			}
		}

		//
		result = model;

		//
		return result;
	}

	/**
	 * 
	 * 
	 * @param net
	 * @throws PuckException
	 */
	public static RelationModel createRelationsFromFamilies(final Net net) throws PuckException {
		RelationModel result;

		//
		RelationModel model = net.createRelationModel("FamiliesPlus");

		//
		model.roles().add(new Role("HUSBAND"));
		model.roles().add(new Role("WIFE"));
		model.roles().add(new Role("CHILD"));

		//
		for (Family family : net.families()) {

			//
			Relation relation = net.createRelation(family.getId(), family.hashKey(), model);

			//
			if (family.getHusband() != null) {
				Actor husband = net.createRelationActor(relation, family.getHusband().getId(), "HUSBAND");
				husband.setRelationOrder(family.getHusbandOrder());
			}

			if (family.getWife() != null) {
				Actor wife = net.createRelationActor(relation, family.getWife().getId(), "WIFE");
				wife.setRelationOrder(family.getWifeOrder());
			}

			for (Individual child : family.getChildren()) {
				Actor actor = net.createRelationActor(relation, child.getId(), "CHILD");
				actor.setRelationOrder(child.getBirthOrder());
			}

			//
			relation.attributes().addAll(family.attributes());
		}

		//
		result = model;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Graph<Individual> createTipGraph(final Segmentation source) throws PuckException {

		Graph<Individual> result;
		//
		result = new Graph<Individual>("Tip graph " + source.getLabel());

		for (Individual individual : source.getCurrentIndividuals().toSortedList()) {
			result.addNode(individual);
		}

		//
		for (Family family : source.getCurrentFamilies()) {
			//
			Individual father = family.getHusband();
			Individual mother = family.getWife();

			//
			if (father != null && mother != null && family.hasMarried()) {
				Link<Individual> link = result.addArc(mother, father, 1);
				link.setTag("1 'H.F'");
			}

			//
			if (!family.isSterile()) {
				for (Individual child : family.getChildren()) {
					if (father != null) {
						//
						int relationCode;
						String relationPattern;
						switch (child.getGender()) {
							case FEMALE:
								relationCode = 4;
								relationPattern = "'F(H)'";
							break;
							case MALE:
								relationCode = 5;
								relationPattern = "'H(H)'";
							break;
							default:
								relationCode = 7;
								relationPattern = "'X(H)'";
						}

						//
						Link<Individual> link = result.addArc(father, child, relationCode);
						link.setTag(relationCode + " " + relationPattern);
					}

					if (mother != null) {
						//
						int relationCode;
						String relationPattern;
						switch (child.getGender()) {
							case FEMALE:
								relationCode = 2;
								relationPattern = "'F(F)'";
							break;
							case MALE:
								relationCode = 3;
								relationPattern = "'M(F)'";
							break;
							default:
								relationCode = 6;
								relationPattern = "'X(F)'";
						}

						//
						Link<Individual> link = result.addArc(mother, child, relationCode);
						link.setTag(relationCode + " " + relationPattern);
					}
				}
			}
		}

		//
		NetUtils.setGenderShapes(result);

		//
		return result;
	}
	
	private static boolean isYes (String code){
		boolean result;
		
		result = code!=null && (code.equalsIgnoreCase("Yes") || code.equalsIgnoreCase("Y"));
		
		//
		return result;
	}
	
	public static boolean notToAnonymize (Individual individual){
		boolean result;
		
		result = NumberUtils.isNumber(individual.getFirstName()) || 
				(StringUtils.isBlank(individual.getName())) || 
				individual.getName().equals("?") || 
				(individual.getName().charAt(0) == '#')  ||
				isYes(individual.getAttributeValue("VISIBLE"));
		
		//
		return result;
	}

	/**
	 * Eliminates the individuals marked as double (i.e., individuals having a
	 * number instead of a name).
	 * <p>
	 * The "name" of a double is the ID number of the original.
	 * 
	 * @param net
	 *            Source.
	 * 
	 * @return The number of single individuals eliminated.
	 * @throws PuckException
	 */
	public static void eliminateDoubleIndividuals(final Net source, final Individuals sample, final Report report) {

		List<Individual> individuals = sample.toList();

		Map<Integer, Integer> originalIds = new HashMap<Integer, Integer>();
		List<Integer> inexistentOriginalIds = new ArrayList<Integer>();
		List<Integer> selfReferentialOriginals = new ArrayList<Integer>();
		List<Integer> unreplacedIds = new ArrayList<Integer>();
		StringList conflictingParents = new StringList();
		StringList individualReplacements = new StringList();
		StringList familyReplacements = new StringList();
		StringList conflictingUnionStatus = new StringList();
		
		Report attributeErrorReport = new Report();
		
		int indiCount = 0;
		int familyCount = 0;
		
		for (Individual sourceIndividual : individuals) {
			if (NumberUtils.isNumber(sourceIndividual.getFirstName())) {
				//
				int originalId = Integer.parseInt(sourceIndividual.getFirstName());

				if (originalId == sourceIndividual.getId()) {
					selfReferentialOriginals.add(originalId);
					continue;
				}

				Individual targetIndividual = source.individuals().getById(originalId);

				while (targetIndividual == null) {
					if (originalIds.get(originalId) == null) {
						inexistentOriginalIds.add(originalId);
						break;
					} else {
						originalId = originalIds.get(originalId);
						targetIndividual = source.individuals().getById(originalId);
					}
				}
				if (targetIndividual == null) {
					unreplacedIds.add(sourceIndividual.getId());
					continue;
				}

				originalIds.put(sourceIndividual.getId(), originalId);

				String track = targetIndividual.getAttributeValue("DOUBLE");
				if (track == null) {
					track = String.valueOf(sourceIndividual.getId());
				} else {
					track += ";" + String.valueOf(sourceIndividual.getId());
				}
				targetIndividual.setAttribute("DOUBLE", track);

				// Append attributes.
				UpdateWorker.update("I\t"+sourceIndividual.toString(), targetIndividual.attributes(), sourceIndividual.attributes(), attributeErrorReport, UpdateMode.APPEND);

				// Append parents.
				
				if ((targetIndividual.getFather() == null) && (sourceIndividual.getFather() != null)) {
					NetUtils.setFatherRelation(source, sourceIndividual.getFather().getId(), targetIndividual.getId());
				} else if (targetIndividual.getFather()!=null && sourceIndividual.getFather()!=null){
					if (targetIndividual.getFather()!=sourceIndividual.getFather() && (!NumberUtils.isNumber(sourceIndividual.getFather().getFirstName())|| Integer.parseInt(sourceIndividual.getFather().getFirstName())!=targetIndividual.getFather().getId()) && (!NumberUtils.isNumber(targetIndividual.getFather().getFirstName())|| Integer.parseInt(targetIndividual.getFather().getFirstName())!=sourceIndividual.getFather().getId())
							&& (!NumberUtils.isNumber(sourceIndividual.getFather().getFirstName())||!NumberUtils.isNumber(targetIndividual.getFather().getFirstName()) || Integer.parseInt(sourceIndividual.getFather().getFirstName())!=Integer.parseInt(targetIndividual.getFather().getFirstName()))) {
						conflictingParents.appendln("Father conflict for "+targetIndividual+":\t"+sourceIndividual.getFather()+" vs "+targetIndividual.getFather());
						targetIndividual.setAttribute("F_ALT", sourceIndividual.getFather()+"");
					}
				}
				if ((targetIndividual.getMother() == null) && (sourceIndividual.getMother() != null)) {
					NetUtils.setMotherRelation(source, sourceIndividual.getMother().getId(), targetIndividual.getId());
				} else if (targetIndividual.getMother()!=null && sourceIndividual.getMother()!=null){
					if (targetIndividual.getMother()!=sourceIndividual.getMother() && (!NumberUtils.isNumber(sourceIndividual.getMother().getFirstName())|| Integer.parseInt(sourceIndividual.getMother().getFirstName())!=targetIndividual.getMother().getId())&& (!NumberUtils.isNumber(targetIndividual.getMother().getFirstName())|| Integer.parseInt(targetIndividual.getMother().getFirstName())!=sourceIndividual.getMother().getId()) &&
						(!NumberUtils.isNumber(sourceIndividual.getMother().getFirstName())||!NumberUtils.isNumber(targetIndividual.getMother().getFirstName()) || Integer.parseInt(sourceIndividual.getMother().getFirstName())!=Integer.parseInt(targetIndividual.getMother().getFirstName()))) {
						conflictingParents.appendln("Mother conflict for "+targetIndividual+":\t"+sourceIndividual.getMother()+" vs "+targetIndividual.getMother());
						targetIndividual.setAttribute("M_ALT", sourceIndividual.getMother()+"");
					}
				}

				// Append or fuse personalFamilies
				for (Family sourceIndividualFamily : sourceIndividual.getPersonalFamilies().toSortedList()) {
					Family targetIndividualFamily = null;
					if (sourceIndividual == sourceIndividualFamily.getHusband()) {
						targetIndividualFamily = source.families().getBySpouses(targetIndividual, sourceIndividualFamily.getWife());
						sourceIndividualFamily.setHusband(targetIndividual);
					} else if (sourceIndividual == sourceIndividualFamily.getWife()) {
						targetIndividualFamily = source.families().getBySpouses(sourceIndividualFamily.getHusband(), targetIndividual);
						sourceIndividualFamily.setWife(targetIndividual);
					}
					targetIndividual.addPersonalFamily(sourceIndividualFamily);
						
					if (targetIndividualFamily != null) {
						
						Family sourceFamily = null;
						Family targetFamily = null;
						if (targetIndividualFamily.getId()>sourceIndividualFamily.getId()){
							targetFamily = sourceIndividualFamily;
							sourceFamily = targetIndividualFamily;
						} else {
							targetFamily = targetIndividualFamily;
							sourceFamily = sourceIndividualFamily;
						}
						
						targetFamily.getChildren().add(sourceFamily.getChildren());
						for (Individual child : sourceFamily.getChildren()) {
							child.setOriginFamily(targetFamily);
						}
						UpdateWorker.update("F\t"+targetFamily.toString(), targetFamily.attributes(), sourceFamily.attributes(), attributeErrorReport, UpdateMode.APPEND);
						if (targetFamily.getUnionStatus()!=sourceFamily.getUnionStatus()){
							if (targetFamily.getUnionStatus().isUnmarried()){
								conflictingUnionStatus.appendln(targetFamily.getId()+"\t"+sourceFamily.getId()+"\tchanged status\t"+targetFamily.getUnionStatus()+"\t->\t"+sourceFamily.getUnionStatus());
								targetFamily.setUnionStatus(sourceFamily.getUnionStatus());
							} else {
								conflictingUnionStatus.appendln(targetFamily.getId()+"\t"+sourceFamily.getId()+"\tnot changed status\t"+targetFamily.getUnionStatus()+"\t->|\t"+sourceFamily.getUnionStatus());
							}
						}
						sourceFamily.getHusband().getPersonalFamilies().removeById(sourceFamily.getId());
						sourceFamily.getWife().getPersonalFamilies().removeById(sourceFamily.getId());
						source.families().removeById(sourceFamily.getId());
						
						String famTrack = targetFamily.getAttributeValue("DOUBLE");
						if (famTrack == null) {
							famTrack = String.valueOf(sourceFamily.getId());
						} else {
							famTrack += ";" + String.valueOf(sourceFamily.getId());
						}
						targetFamily.setAttribute("DOUBLE", famTrack);

						familyCount += 1;
						familyReplacements.appendln(sourceFamily.getId()+" > "+targetFamily.getId());
					}
				}
				
				// Append relations
				
				for (Relation relation: sourceIndividual.relations().toSortedList()){
					for (Actor actor: relation.actors().getByIndividual(sourceIndividual)){
						actor.setIndividual(targetIndividual);
					}
					sourceIndividual.relations().remove(relation);
					targetIndividual.relations().add(relation);
				}

				source.remove(sourceIndividual);
				individualReplacements.appendln(sourceIndividual.getId()+" > "+targetIndividual);
				indiCount += 1;
			}
		}
		
		// Clean double families
		
		Families targetFamilies = new Families();
		for (Family sourceFamily : source.families().toSortedList()) {
			Family targetFamily = targetFamilies.getBySpouses(sourceFamily.getFather(), sourceFamily.getMother());
			if (targetFamily==null){
				targetFamilies.add(sourceFamily);
			} else {
				targetFamily.getChildren().add(sourceFamily.getChildren());
				for (Individual child : sourceFamily.getChildren()) {
					child.setOriginFamily(targetFamily);
				}
				UpdateWorker.update("F\t"+targetFamily.toString(), targetFamily.attributes(), sourceFamily.attributes(), attributeErrorReport, UpdateMode.APPEND);
				if (targetFamily.getUnionStatus()!=sourceFamily.getUnionStatus()){
					if (targetFamily.getUnionStatus().isUnmarried()){
						conflictingUnionStatus.appendln(targetFamily.getId()+"\t"+sourceFamily.getId()+"\tchanged status\t"+targetFamily.getUnionStatus()+"\t->\t"+sourceFamily.getUnionStatus());
						targetFamily.setUnionStatus(sourceFamily.getUnionStatus());
					} else {
						conflictingUnionStatus.appendln(targetFamily.getId()+" ("+sourceFamily.getId()+")\tnot changed status\t"+targetFamily.getUnionStatus()+"\t->|\t"+sourceFamily.getUnionStatus());
					}
				}
				sourceFamily.getHusband().getPersonalFamilies().removeById(sourceFamily.getId());
				sourceFamily.getWife().getPersonalFamilies().removeById(sourceFamily.getId());
				source.families().removeById(sourceFamily.getId());
				
				String famTrack = targetFamily.getAttributeValue("DOUBLE");
				if (famTrack == null) {
					famTrack = String.valueOf(sourceFamily.getId());
				} else {
					famTrack += ";" + String.valueOf(sourceFamily.getId());
				}
				targetFamily.setAttribute("DOUBLE", famTrack);

				familyCount += 1;
				familyReplacements.appendln(sourceFamily.getId()+" > "+targetFamily.getId());
			}
		}

		// Make error report
		
		StringList reportList = new StringList();
		
		if (inexistentOriginalIds.size()>0){
			reportList.append("Inexistent original individuals: ");
			for (int id : inexistentOriginalIds) {
				reportList.append(id + " ");
			}
			reportList.appendln();
		}

		if (selfReferentialOriginals.size()>0){
			reportList.append("Selfreferential original individuals: ");
			for (int id : selfReferentialOriginals) {
				reportList.append(id + " ");
			}
			reportList.appendln();
		}

		if (unreplacedIds.size()>0){
			reportList.append("Unreplaced individual ids: ");
			for (int id : unreplacedIds) {
				reportList.append(id + " ");
			}
			reportList.appendln();
		}

		reportList.appendln();
		if (conflictingParents.size()>0){
			reportList.appendln("Conflicting parents: ");
			reportList.append(conflictingParents);
			reportList.appendln();
		}
		report.outputs().append(reportList);

		if (attributeErrorReport.outputs().isNotEmpty()){
			report.outputs().appendln("Conflicting attributes: ");
			report.outputs().append(attributeErrorReport.outputs());
			report.outputs().appendln();
		}

		if (conflictingUnionStatus.size()>0){
			report.outputs().appendln("Conflicting union Status: ");
			report.outputs().append(conflictingUnionStatus);
			report.outputs().appendln();
		}

		if (individualReplacements.size()>0){
			report.outputs().appendln("Replaced individuals: "+indiCount);
			report.outputs().appendln(individualReplacements);
			report.outputs().appendln();
		}

		if (familyReplacements.size()>0){
			report.outputs().appendln("Replaced families: "+familyCount);
			report.outputs().appendln(familyReplacements);
			report.outputs().appendln();
		}

	}

	/**
	 * Eliminates the relations marked as double (i.e., relations having a
	 * number instead of a name).
	 * <p>
	 * The "name" of a double is the ID number of the original.
	 * 
	 * @param net
	 *            Source.
	 * 
	 * @return The number of single relations eliminated.
	 * @throws PuckException
	 */
	public static void eliminateDoubleRelations(final Net source, final Relations sample, final Report report) {

		List<Relation> relations = sample.toSortedList();

		Map<Integer, Integer> originalIds = new HashMap<Integer, Integer>();
		List<Integer> inexistentOriginals = new ArrayList<Integer>();
		List<Integer> selfReferentialOriginals = new ArrayList<Integer>();
		List<Integer> unreplacedIds = new ArrayList<Integer>();
		StringList replacements = new StringList();
		
		Report attributeErrorReport = new Report();
		
		int count = 0;
		for (Relation sourceRelation : relations) {

			if (NumberUtils.isNumber(sourceRelation.getName())) {
				//
				int originalTypedId = Integer.parseInt(sourceRelation.getName());
				RelationModel model = sourceRelation.getModel();

				if (originalTypedId == sourceRelation.getTypedId()) {
					selfReferentialOriginals.add(originalTypedId);
					continue;
				}

				Relation targetRelation = source.relations().getByTypedId(originalTypedId, model);

				while (targetRelation == null) {
					if (originalIds.get(originalTypedId) == null) {
						inexistentOriginals.add(originalTypedId);
						break;
					} else {
						originalTypedId = originalIds.get(originalTypedId);
						targetRelation = source.relations().getByTypedId(originalTypedId, model);
					}
				}
				
				if (targetRelation == null) {
					unreplacedIds.add(sourceRelation.getTypedId());
					continue;
				}

				originalIds.put(sourceRelation.getTypedId(), originalTypedId);

				String track = targetRelation.getAttributeValue("DOUBLE");
				if (track == null) {
					track = String.valueOf(sourceRelation.getTypedId());
				} else {
					track += ";" + String.valueOf(sourceRelation.getTypedId());
				}

				targetRelation.setAttribute("DOUBLE", track);

				// Append attributes.
				UpdateWorker.update(sourceRelation.toString(), targetRelation.attributes(), sourceRelation.attributes(), attributeErrorReport, UpdateMode.APPEND);

				// Append or fuse actors
				for (Actor sourceActor : sourceRelation.actors().toList()) {
					Individual individual = sourceActor.getIndividual();
					if (!targetRelation.actors().contains(sourceActor)) {
						targetRelation.actors().add(sourceActor);
						individual.relations().add(targetRelation);
					} else {
						Actor targetActor = targetRelation.getActor(sourceActor);
						UpdateWorker.update("A\t"+targetActor.toString(), targetActor.attributes(), sourceActor.attributes(), attributeErrorReport, UpdateMode.APPEND);
					}
					sourceRelation.removeActor(sourceActor);
//					individual.relations().remove(sourceRelation); // directly integrated into removeActor method
				}

				source.remove(sourceRelation);

				replacements.appendln(sourceRelation.getId()+" > "+targetRelation);
				count += 1;
			}
		}
		
		// Make error report
		StringList reportList = new StringList();
		
		reportList.appendln("Relations replaced: "+count);
		reportList.appendln();

		if (inexistentOriginals.size()>0){
			reportList.append("Inexistent original relations: ");
			for (int id : inexistentOriginals) {
				reportList.append(id + " ");
			}
			reportList.appendln();
		}

		if (selfReferentialOriginals.size()>0){
			reportList.append("Selfreferential original relations: ");
			for (int id : selfReferentialOriginals) {
				reportList.append(id + " ");
			}
			reportList.appendln();
		}

		if (unreplacedIds.size()>0){
			reportList.append("Unreplaced relation ids: ");
			for (int id : unreplacedIds) {
				reportList.append(id + " ");
			}
			reportList.appendln();
		}

		report.outputs().append(reportList);
		
		if (attributeErrorReport.outputs().isNotEmpty()){
			report.outputs().appendln("Conflicting attributes: ");
			report.outputs().append(attributeErrorReport.outputs());
			report.outputs().appendln();
		}

		if (replacements.size()>0){
			report.outputs().appendln("Replacements: ");
			report.outputs().appendln(replacements);
			report.outputs().appendln();
		}
		

		//
	}

	/**
	 * Eliminates single individual.
	 * 
	 * @param net
	 *            Source.
	 * 
	 * @return The number of single individuals eliminated.
	 */
	public static int eliminateSingles(final Net source, final Report report) {
		int result;

		result = eliminateSingles(source, source.individuals(), report);

		//
		return result;
	}

	/**
	 * Eliminates single individual.
	 * 
	 * @param net
	 *            Source.
	 * 
	 * @return The number of single individuals eliminated.
	 */
	public static int eliminateSingles(final Net source, final Individuals sample, final Report report) {
		int result;

		StringList list = new StringList();

		result = 0;
		for (Individual individual : sample.toSortedList()) {
			if (individual.isSingle()) {
				//
				source.remove(individual);
				list.appendln(individual.toString());

				//
				result += 1;
			}
		}
				
		report.outputs().appendln(result+" Individuals removed:");
		report.outputs().appendln();
		report.outputs().append(list);



		//
		return result;
	}

	/**
	 * @param net
	 *            Source.
	 * 
	 * @return The number of single individuals eliminated.
	 */
	public static int eliminateStructuralChildren(final Net source, final Report report) {
		int result;

		result = eliminateStructuralChildren(source, source.individuals(), report);

		//
		return result;
	}

	/**
	 * @param net
	 *            Source.
	 * 
	 * @return The number of single individuals eliminated.
	 */
	public static int eliminateStructuralChildren(final Net source, final Individuals sample, final Report report) {
		int result;

		StringList list = new StringList();
		result = 0;
		for (Individual individual : sample.toSortedList()) {
			if ((individual.isSingle()) && (individual.isSterile())) {
				source.remove(individual);
				list.appendln(individual.toString());
				result += 1;
			}
		}
		
		report.outputs().appendln(result+" Individuals removed:");
		report.outputs().appendln();
		report.outputs().append(list);

		//
		return result;
	}

	/**
	 * @param net
	 *            Source.
	 * 
	 * @return The number of single individuals eliminated.
	 */
	public static int eliminateVirtuals(final Net source, Report report) {
		int result;

		result = eliminateVirtualParents(source, source.individuals(), report);

		//
		return result;
	}

	/**
	 * @param net
	 *            Source.
	 * 
	 * @return The number of single individuals eliminated.
	 */
	public static int eliminateVirtualParents(final Net source, final Individuals sample, final Report report) {
		int result;

		StringList list = new StringList();

		result = 0;
		for (Individual individual : sample.toSortedList()) {
			if (((StringUtils.isBlank(individual.getName())) || individual.getName().equals("?") || (individual.getName().charAt(0) == '#')) && individual.isOrphan() && individual.attributes().isEmpty() && individual.relations().isEmpty() && individual.getPersonalFamilies().size()<2) {
				source.remove(individual);
				list.appendln(individual.toString()+"\tchildren: "+individual.children());

				result += 1;
			}
		}

		report.outputs().appendln(result+" Individuals removed: ");
		report.outputs().appendln();
		report.outputs().append(list);

		//
		return result;
	}

	public static void enlargeNetFromAttributes(final Net net, final String fatherFirstNameLabel, final String fatherLastNameLabel,
			final String motherFirstNameLabel, final String motherLastNameLabel, final String husbandFirstNameLabel, final String husbandLastNameLabel,
			final String wifeFirstNameLabel, final String wifeLastNameLabel, final Map<String, String> husbandLabels, final Map<String, String> wifeLabels, 
			final String FFFirstNameLabel,
			final String FFLastNameLabel, final String FMFirstNameLabel, final String FMLastNameLabel, final String MFFirstNameLabel,
			final String MFLastNameLabel, final String MMFirstNameLabel, final String MMLastNameLabel) throws PuckException {

		for (Family family : net.families().toSortedList()) {
			createIndividualsFromFamilyAttributes(net, family, husbandFirstNameLabel, husbandLastNameLabel, wifeFirstNameLabel, wifeLastNameLabel,
					husbandLabels, wifeLabels);
		}
		for (Individual ego : net.individuals().toSortedList()) {
			createOriginFamilyFromIndividualAttributes(net, ego, fatherFirstNameLabel, fatherLastNameLabel, motherFirstNameLabel, motherLastNameLabel);
		}
		
		for (Individual ego : net.individuals().toSortedList()) {
			createGrandParentsFromIndividualAttributes(net, ego, FFFirstNameLabel,	FFLastNameLabel, FMFirstNameLabel, FMLastNameLabel, MFFirstNameLabel,
					MFLastNameLabel, MMFirstNameLabel, MMLastNameLabel);
		}
		
		
	}
	
	public static Individuals neighbors(final Individual ego, final ExpansionMode expansionMode, final FiliationType filiationType){
		return neighbors(ego, null, null, null, expansionMode, filiationType);
	}

	
	public static Individuals neighbors(final Individual ego, final String relationModelName, final String egoRoleName, final String alternativeEgoRoleName, final ExpansionMode expansionMode, final FiliationType filiationType){
		Individuals result;
		
		result = new Individuals();

		switch (expansionMode) {
			case ALL:
				result = ego.getRelated(relationModelName, egoRoleName, alternativeEgoRoleName);
				result.add(ego.getKin());
				result.add(ego.getPartners());
			break;

			case CHILD:
				result = ego.getKin(KinType.CHILD, filiationType);
			break;

			case KIN:
				result = ego.getKin();
			break;

			case PARENT:
				result = ego.getKin(KinType.PARENT, filiationType);

			break;

			case RELATED:
				result = ego.getRelated(relationModelName, egoRoleName, alternativeEgoRoleName);
			break;

			case SPOUSE:
				result = ego.getKin(KinType.SPOUSE, filiationType);
			break;

			default:
				KinType kinType = KinType.valueOf(expansionMode.toString());
				result = ego.getKin(kinType, filiationType);
		}
		//
		return result;

	}
	
	public static Individuals expand(final Individuals seeds, final ExpansionMode expansionMode, final FiliationType filiationType, final String seedLabel,
			final int maxStep) {
		return expand(seeds, null, null, null, expansionMode, filiationType, seedLabel, maxStep);
	}


	/**
	 * 
	 * @param seeds
	 * @param expansionMode
	 * @param filiationType
	 * @param seedLabel
	 * @param maxNrSteps
	 * @return
	 */
	public static Individuals expand(final Individuals seeds, final String relationModelName, final String egoRoleName, final String alternativeEgoRoleName, final ExpansionMode expansionMode, final FiliationType filiationType, final String seedLabel,
			final int maxStep) {
		Individuals result;

		result = new Individuals();

		String label = "STEP_" + seedLabel.replaceAll(" ", "_") + "_" + expansionMode;
		if (filiationType != FiliationType.COGNATIC) {
			label += "_" + filiationType;
		}

		Queue<Individual> queue = new LinkedList<Individual>();
		for (Individual seed : seeds) {
			queue.add(seed);
			result.add(seed);
			seed.setAttribute(label, 0 + "");
		}

		while (!queue.isEmpty()) {

			Individual ego = queue.remove();
			int step = Integer.parseInt(ego.getAttributeValue(label)) + 1;

			if (maxStep <= 0 || step <= maxStep) {

				Individuals neighbors = neighbors(ego, relationModelName, egoRoleName, alternativeEgoRoleName, expansionMode, filiationType);

				for (Individual neighbor : neighbors) {
					//
					if (!result.contains(neighbor)) {
						//
						queue.add(neighbor);
						result.add(neighbor);
						neighbor.setAttribute(label, step + "");
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * This method expands a net.
	 * 
	 * @param source
	 *            The net to expand.
	 * @param seeds
	 * @param seedLabel
	 * @param expansionMode
	 *            How expand the net (ALL, RELATED, KIN, PARENT, CHILD, SPOUSE).
	 * @param filiationType
	 *            Expansion parameter (COGNATIC, AGNATIC, UTERINE).
	 * @param maxNrSteps
	 *            The limit of step expansion.
	 * @return A new net.
	 */
	public static Net expand(final Net source, final Individuals seeds, final String seedLabel, final ExpansionMode expansionMode,
			final FiliationType filiationType, final int maxStep) {
		Net result;

		Individuals individuals = expand(seeds, expansionMode, filiationType, seedLabel, maxStep);

		result = extract(source, individuals);

		String title = result.getLabel() + "_" + seedLabel + "_" + expansionMode;
		result.setLabel(title);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 *            Net to update.
	 * 
	 * @param sourceFamilies
	 *            Families of the current individuals segment.
	 * 
	 * @return A new Net based on the current families segment.
	 */
	public static Net extract(final Net source, final Families sourceFamilies) {
		Net result;

		//
		result = new Net();
		result.setLabel(source.getLabel());
		result.attributes().addAll(source.attributes());

		// Extract families.
		for (Family sourceFamily : sourceFamilies) {
			//
			Family targetFamily = new Family(sourceFamily.getId());
			targetFamily.setMarried(sourceFamily.hasMarried());
			targetFamily.setHusbandOrder(sourceFamily.getHusbandOrder());
			targetFamily.setWifeOrder(sourceFamily.getWifeOrder());
			targetFamily.attributes().addAll(sourceFamily.attributes());

			// targetFamily.setFather(sourceFamily.getFather());
			// targetFamily.setMother(sourceFamily.getMother());
			// targetFamily.getChildren().add(sourceFamily.getChildren());

			//
			result.families().add(targetFamily);
		}

		// Extract individuals.
		for (Individual sourceIndividual : source.individuals()) {
			//
			Individual targetIndividual = new Individual(sourceIndividual.getId());
			targetIndividual.setName(sourceIndividual.getName());
			targetIndividual.setGender(sourceIndividual.getGender());
			targetIndividual.setBirthOrder(sourceIndividual.getBirthOrder());
			targetIndividual.attributes().addAll(sourceIndividual.attributes());

			//
			if (sourceIndividual.getOriginFamily() != null) {
				//
				Family targetOriginFamily = result.families().getById(sourceIndividual.getOriginFamily().getId());

				//
				if (targetOriginFamily != null) {
					//
					targetIndividual.setOriginFamily(targetOriginFamily);

					targetOriginFamily.getChildren().add(targetIndividual);
				}
			}

			//
			for (Family sourcePersonalFamily : sourceIndividual.getPersonalFamilies()) {
				//
				Family targetPersonalFamily = result.families().getById(sourcePersonalFamily.getId());

				//
				if (targetPersonalFamily != null) {
					//
					targetIndividual.getPersonalFamilies().add(targetPersonalFamily);

					//
					if (sourcePersonalFamily.isFather(sourceIndividual)) {
						//
						targetPersonalFamily.setFather(targetIndividual);
					}

					// Do not add else there.

					//
					if (sourcePersonalFamily.isMother(sourceIndividual)) {
						//
						targetPersonalFamily.setMother(targetIndividual);
					}
				}
			}

			//
			if ((targetIndividual.getOriginFamily() != null) || (!targetIndividual.getPersonalFamilies().isEmpty())) {
				//
				result.individuals().add(targetIndividual);
			}
		}

		// Extract relation models.
		result.relationModels().addAll(source.relationModels());

		// Extract relations.
		for (Relation sourceRelation : source.relations()) {
			//
			RelationModel targetModel = result.relationModels().getByName(sourceRelation.getModel().getName());

			//
			Relation targetRelation = new Relation(sourceRelation.getId(), sourceRelation.getTypedId(), targetModel, sourceRelation.getName(), new Actor[0]);
			targetRelation.attributes().addAll(sourceRelation.attributes());

			//
			for (Actor sourceActor : sourceRelation.actors()) {
				//
				Individual targetIndividual = result.individuals().getById(sourceActor.getId());

				if (targetIndividual != null) {
					//
					Actor targetActor = new Actor(targetIndividual, targetModel.roles().getByName(sourceActor.getRole().getName()));
					targetActor.setRelationOrder(sourceActor.getRelationOrder());

					targetRelation.actors().add(targetActor);

					targetIndividual.relations().add(targetRelation);
				}
			}

			//
			if (!targetRelation.actors().isEmpty()) {
				//
				result.relations().add(targetRelation);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 *            Net to update.
	 * 
	 * @param sourceIndividuals
	 *            Individuals of the current individuals segment.
	 * 
	 * @return A new Net based on the current individuals segment.
	 */
	public static Net extract(final Net source, final Individuals sourceIndividuals) {
		Net result;

		//
		result = new Net();
		result.setLabel(source.getLabel());
		result.attributes().addAll(source.attributes());

		// Extract individuals.
		for (Individual sourceIndividual : sourceIndividuals) {
			//
			Individual targetIndividual = new Individual(sourceIndividual.getId());
			targetIndividual.setName(sourceIndividual.getName());
			targetIndividual.setGender(sourceIndividual.getGender());
			targetIndividual.setBirthOrder(sourceIndividual.getBirthOrder());
			targetIndividual.attributes().addAll(sourceIndividual.attributes());

			//
			result.individuals().add(targetIndividual);
		}

		// Extract families.
		for (Family sourceFamily : source.families()) {
			//
			Family targetFamily = new Family(sourceFamily.getId());
			targetFamily.setMarried(sourceFamily.hasMarried());
			targetFamily.setHusbandOrder(sourceFamily.getHusbandOrder());
			targetFamily.setWifeOrder(sourceFamily.getWifeOrder());
			targetFamily.attributes().addAll(sourceFamily.attributes());

			//
			Individual sourceFather = sourceFamily.getFather();
			if (sourceFather != null) {
				//
				Individual targetFather = result.individuals().getById(sourceFather.getId());

				if (targetFather != null) {
					//
					targetFamily.setFather(targetFather);
				}
			}

			//
			Individual sourceMother = sourceFamily.getMother();
			if (sourceMother != null) {
				//
				Individual targetMother = result.individuals().getById(sourceMother.getId());

				if (targetMother != null) {
					//
					targetFamily.setMother(targetMother);
				}
			}

			//
			for (Individual sourceChild : sourceFamily.getChildren()) {
				//
				Individual targetChild = result.individuals().getById(sourceChild.getId());
				if (targetChild != null) {
					//
					targetFamily.getChildren().add(targetChild);
				}
			}

			//
			// Add only families that imply relations
			if ((targetFamily.getFather() != null && targetFamily.getMother() != null) || (targetFamily.getFather() != null && !targetFamily.getChildren().isEmpty())|| (targetFamily.getMother() != null && !targetFamily.getChildren().isEmpty())) {
				//
				if (targetFamily.getFather()!=null){
					targetFamily.getFather().getPersonalFamilies().add(targetFamily);
				}
				if (targetFamily.getMother()!=null){
					targetFamily.getMother().getPersonalFamilies().add(targetFamily);
				}
				for (Individual targetChild : targetFamily.getChildren()){
					targetChild.setOriginFamily(targetFamily);
				}
				result.families().add(targetFamily);
			}
		}

		// Extract relation models.
		result.relationModels().addAll(source.relationModels());

		// Extract relations.
		for (Relation sourceRelation : source.relations()) {
			//
			RelationModel targetModel = result.relationModels().getByName(sourceRelation.getModel().getName());

			//
			Relation targetRelation = new Relation(sourceRelation.getId(), sourceRelation.getTypedId(), targetModel, sourceRelation.getName(), new Actor[0]);

			//
			targetRelation.attributes().addAll(sourceRelation.attributes());

			//
			for (Actor sourceActor : sourceRelation.actors()) {
				//
				Individual targetIndividual = result.individuals().getById(sourceActor.getId());

				if (targetIndividual != null) {
					//
					Actor targetActor = new Actor(targetIndividual, targetModel.roles().getByName(sourceActor.getRole().getName()));
					targetActor.setRelationOrder(sourceActor.getRelationOrder());
					targetActor.attributes().addAll(sourceActor.attributes());
					
					targetRelation.actors().add(targetActor);

					targetIndividual.relations().add(targetRelation);
				}
			}

			//
			if (!targetRelation.actors().isEmpty()) {
				//
				result.relations().add(targetRelation);
			}
		}
		//Add geography
		result.setGeography(source.getGeography());

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 *            Net to update.
	 * 
	 * @param sourceRelations
	 *            Relations of the current individuals segment.
	 * 
	 * @return A new Net based on the current relations segment.
	 */
	public static Net extract(final Net source, final Relations sourceRelations) {
		Net result;

		//
		result = new Net();
		result.setLabel(source.getLabel());
		result.attributes().addAll(source.attributes());

		// Extract relation models.
		result.relationModels().addAll(source.relationModels());

		// Extract Relations.
		for (Relation sourceRelation : sourceRelations) {
			//
			RelationModel targetModel = result.relationModels().getByName(sourceRelation.getModel().getName());

			//
			Relation targetRelation = new Relation(sourceRelation.getId(), sourceRelation.getTypedId(), targetModel, sourceRelation.getName(), new Actor[0]);
			targetRelation.attributes().addAll(sourceRelation.attributes());

			//
			result.relations().add(targetRelation);
		}

		// Extract individuals.
		for (Individual sourceIndividual : source.individuals()) {
			//
			Individual targetIndividual = new Individual(sourceIndividual.getId());
			targetIndividual.setName(sourceIndividual.getName());
			targetIndividual.setGender(sourceIndividual.getGender());
			targetIndividual.setBirthOrder(sourceIndividual.getBirthOrder());
			targetIndividual.attributes().addAll(sourceIndividual.attributes());

			for (Relation sourceRelation : sourceIndividual.relations()) {
				//
				RelationModel targetModel = result.relationModels().getByName(sourceRelation.getModel().getName());

				Relation targetRelation = result.relations().getById(sourceRelation.getId());

				if (targetRelation != null) {
					//
					targetIndividual.relations().add(targetRelation);

					for (Actor sourceActor : sourceRelation.actors()) {
						//
						if (sourceActor.getIndividual() == sourceIndividual) {
							//
							Actor targetActor = new Actor(targetIndividual, targetModel.roles().getByName(sourceActor.getRole().getName()));
							targetActor.setRelationOrder(sourceActor.getRelationOrder());

							targetRelation.actors().add(targetActor);
						}
					}
				}
			}

			//
			if (targetIndividual.relations().isEmpty()) {
				//
				result.individuals().add(targetIndividual);
			}
		}

		// Extract families.
		for (Family sourceFamily : source.families()) {
			//
			Family targetFamily = new Family(sourceFamily.getId());
			targetFamily.setMarried(sourceFamily.hasMarried());
			targetFamily.setHusbandOrder(sourceFamily.getHusbandOrder());
			targetFamily.setWifeOrder(sourceFamily.getWifeOrder());
			targetFamily.attributes().addAll(sourceFamily.attributes());

			//
			Individual sourceFather = sourceFamily.getFather();
			if (sourceFather != null) {
				//
				Individual targetFather = result.individuals().getById(sourceFather.getId());

				if (targetFather != null) {
					//
					targetFamily.setFather(targetFather);
					targetFather.getPersonalFamilies().add(targetFamily);
				}
			}

			//
			Individual sourceMother = sourceFamily.getMother();
			if (sourceMother != null) {
				//
				Individual targetMother = result.individuals().getById(sourceMother.getId());

				if (targetMother != null) {
					//
					targetFamily.setMother(targetMother);
					targetMother.getPersonalFamilies().add(targetFamily);
				}
			}

			//
			for (Individual sourceChild : sourceFamily.getChildren()) {
				//
				Individual targetChild = result.individuals().getById(sourceChild.getId());
				if (targetChild != null) {
					//
					targetFamily.getChildren().add(targetChild);

					targetChild.setOriginFamily(targetFamily);
				}
			}

			//
			if ((targetFamily.getFather() != null) || (targetFamily.getMother() != null) || (!targetFamily.getChildren().isEmpty())) {
				//
				result.families().add(targetFamily);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 *            Network from which to extract.
	 * @param segment
	 * @param segmentLabel
	 * @return
	 * @throws PuckException
	 */
/*	public static Net extract(final Net source, final Segment segment, final String segmentLabel) {
		Net result;

		if ((source == null) || (segment == null)) {
			//
			result = null;

		} else {
			//
			PartitionCriteria criteria = segment.getCriteria();

			//
			if (criteria.getRelationModelName().equals(PartitionCriteria.RelationModelCanonicalNames.INDIVIDUAL.toString())) {
				//
				Partition<Individual> partition = (Partition<Individual>) segment.getPartition();
				//
				Individuals individuals = new Individuals();
				for (Cluster<Individual> cluster : partition.getClusters()) {
					//
					if (!cluster.isNull()) {
						//
						individuals.add(cluster.getItems());
					}
				}

				//
				result = extract(source, individuals);

			} else if (criteria.getRelationModelName().equals(PartitionCriteria.RelationModelCanonicalNames.FAMILY.toString())) {
				//
				Partition<Family> partition = (Partition<Family>) segment.getPartition();
				//
				Families families = new Families();
				for (Cluster<Family> cluster : partition.getClusters()) {
					//
					if (!cluster.isNull()) {
						//
						families.add(cluster.getItems());
					}
				}

				//
				result = extract(source, families);

			} else {
				//
				Partition<Relation> partition = (Partition<Relation>) segment.getPartition();

				//
				Relations relations = new Relations();
				for (Cluster<Relation> cluster : partition.getClusters()) {
					//
					if (!cluster.isNull()) {
						//
						relations.add(cluster.getItems());
					}
				}

				//
				result = extract(source, relations);
			}

			//
			result.setLabel(segmentLabel);
			result.setGeography(source.getGeography());
		}

		//
		return result;
	}*/

	/**
	 * 
	 * @param source
	 *            Network from which to extract.
	 * @param segment
	 * @param segmentLabel
	 * @param minimalValue
	 * @return
	 * @throws PuckException
	 */
	public static Net extractByClusterSize(final Net source, final Segment segment, final String segmentLabel, final int minimalNumberOfMembers)
			throws PuckException {
		Net result;

		if ((source == null) || (segment == null)) {
			//
			result = null;

		} else {
			//
			PartitionCriteria criteria = segment.getCriteria();

			//
			if (criteria.getRelationModelName().equals(PartitionCriteria.RelationModelCanonicalNames.INDIVIDUAL.toString())) {
				//
				Partition<Individual> partition = (Partition<Individual>) segment.getPartition();
				//
				Individuals individuals = new Individuals();
				for (Cluster<Individual> cluster : partition.getClusters()) {
					//
					if ((!cluster.isNull()) && (cluster.count() >= minimalNumberOfMembers)) {
						//
						individuals.add(cluster.getItems());
					}
				}

				//
				result = extract(source, individuals);

			} else if (criteria.getRelationModelName().equals(PartitionCriteria.RelationModelCanonicalNames.FAMILY.toString())) {
				//
				Partition<Family> partition = (Partition<Family>) segment.getPartition();
				//
				Families families = new Families();
				for (Cluster<Family> cluster : partition.getClusters()) {
					//
					if ((!cluster.isNull()) && (cluster.count() >= minimalNumberOfMembers)) {
						//
						families.add(cluster.getItems());
					}
				}

				//
				result = extract(source, families);

			} else {
				//
				Partition<Relation> partition = (Partition<Relation>) segment.getPartition();

				//
				Relations relations = new Relations();
				for (Cluster<Relation> cluster : partition.getClusters()) {
					//
					if ((!cluster.isNull()) && (cluster.count() >= minimalNumberOfMembers)) {
						//
						relations.add(cluster.getItems());
					}
				}

				//
				result = extract(source, relations);
			}

			//
			result.setLabel(segmentLabel);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 *            Network from which to extract.
	 * @param label
	 * @param labelParameter
	 *            Parameter of the label (example: 3 for PEDG 3).
	 * @param minimalNumberOfMembers
	 * @return
	 * @throws PuckException
	 */
	public static Net extractByClusterSize(final Net source, final String label, final String labelParameter, final int minimalNumberOfMembers)
			throws PuckException {
		Net result;

		//
		Partition<Individual> partition = PartitionMaker.createRaw(source, label, labelParameter);

		//
		Individuals individuals = new Individuals();
		for (Cluster<Individual> cluster : partition.getClusters()) {
			if (!cluster.isNull() && cluster.count() >= minimalNumberOfMembers) {
				individuals.add(cluster.getItems());
			}
		}

		//
		result = extract(source, individuals);

		//
		String netLabel;
		if (StringUtils.isBlank(labelParameter)) {
			//
			netLabel = result.getLabel() + "_" + label + "_min_" + minimalNumberOfMembers;

		} else {
			//
			netLabel = result.getLabel() + "_" + label + " " + labelParameter + "_min_" + minimalNumberOfMembers;
		}
		result.setLabel(netLabel);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 *            Network from which to extract.
	 * @param segment
	 * @param segmentLabel
	 * @param minimalValue
	 * @return
	 * @throws PuckException
	 */
	public static Net extractByClusterValue(final Net source, final Segment segment, final String segmentLabel, final int minimalValue) throws PuckException {
		Net result;

		if ((source == null) || (segment == null)) {
			//
			result = null;

		} else {
			//
			PartitionCriteria criteria = segment.getCriteria();

			//
			if (criteria.getRelationModelName().equals(PartitionCriteria.RelationModelCanonicalNames.INDIVIDUAL.toString())) {
				//
				Partition<Individual> partition = (Partition<Individual>) segment.getPartition();
				//
				Individuals individuals = new Individuals();
				for (Cluster<Individual> cluster : partition.getClusters()) {
					//
					if ((!cluster.isNull()) && (cluster.getValue().doubleValue() >= minimalValue)) {
						//
						individuals.add(cluster.getItems());
					}
				}

				//
				result = extract(source, individuals);

			} else if (criteria.getRelationModelName().equals(PartitionCriteria.RelationModelCanonicalNames.FAMILY.toString())) {
				//
				Partition<Family> partition = (Partition<Family>) segment.getPartition();
				//
				Families families = new Families();
				for (Cluster<Family> cluster : partition.getClusters()) {
					//
					if ((!cluster.isNull()) && (cluster.getValue().doubleValue() >= minimalValue)) {
						//
						families.add(cluster.getItems());
					}
				}

				//
				result = null; // extract(source, families);

			} else {
				//
				Partition<Relation> partition = (Partition<Relation>) segment.getPartition();

				//
				Relations relations = new Relations();
				for (Cluster<Relation> cluster : partition.getClusters()) {
					//
					if ((!cluster.isNull()) && (cluster.getValue().doubleValue() >= minimalValue)) {
						//
						relations.add(cluster.getItems());
					}
				}

				//
				result = null; // extract(source, relations);
			}

			//
			result.setLabel(segmentLabel);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 *            Network from which to extract.
	 * @param label
	 * @param labelParameter
	 *            Parameter of the label (example: 3 for PEDG 3).
	 * @param minimalValue
	 * @return
	 * @throws PuckException
	 */
	public static Net extractByClusterValue(final Net source, final String label, final String labelParameter, final int minimalValue) throws PuckException {
		Net result;

		//
		Partition<Individual> partition = PartitionMaker.createRaw(source, label, labelParameter);

		//
		Individuals individuals = new Individuals();
		for (Cluster<Individual> cluster : partition.getClusters()) {
			if (!cluster.isNull() && cluster.getValue().doubleValue() >= minimalValue) {
				individuals.add(cluster.getItems());
			}
		}

		//
		result = extract(source, individuals);

		//
		String netLabel;
		if (StringUtils.isBlank(labelParameter)) {
			//
			netLabel = result.getLabel() + "_" + label + "_min_" + minimalValue;

		} else {
			//
			netLabel = result.getLabel() + "_" + label + " " + labelParameter + "_min_" + minimalValue;
		}
		result.setLabel(netLabel);

		//
		return result;
	}

	/*	private static void expand(final Individuals individuals, final Individual ego) {
			if (!individuals.contains(ego)) {
				individuals.add(ego);
				ego.setAttribute("EXPANDED", "true");

				Individuals related = ego.getRelated();
				related.add(ego.getKin());

				for (Individual alter : related) {
					expand(individuals, alter);
				}
			}
		}

		
		public static Net expand(final Net source, final Segment segment) {
			Net result;

			Individuals individuals = new Individuals();
			for (Individual individual : segment.getCurrentIndividuals()) {
				expand(individuals, individual);
			}

			result = extract(source, individuals);

			String label = result.getLabel() + "_" + segment.getLabel() + "_expanded_ALL";
			result.setLabel(label);

			//
			return result;
		}


		private static void expandByKin(final Individuals individuals, final Individual ego, final KinType kinType) {
			if (!individuals.contains(ego)) {
				individuals.add(ego);
				ego.setAttribute("EXPANDED_" + kinType, "true");

				Individuals kin;
				if (kinType == null) {
					kin = ego.getKin();
				} else {
					kin = ego.getKin(kinType);
				}

				for (Individual alter : kin) {
					expandByKin(individuals, alter, kinType);
				}
			}
		}

		public static Net expandByKin(final Net source, final Segment segment, final KinType direction) {
			Net result;

			Individuals individuals = new Individuals();
			for (Individual individual : segment.getCurrentIndividuals()) {
				expandByKin(individuals, individual, direction);
			}

			result = extract(source, individuals);

			String label = result.getLabel() + "_" + segment.getLabel() + "_expanded";
			if (direction != null) {
				label = label + "_" + direction;
			} else {
				label = label + "_ALL_KIN";
			}
			result.setLabel(label);

			//
			return result;
		}

		private static void expandByRelations(final Individuals individuals, final Individual ego) {
			if (!individuals.contains(ego)) {
				individuals.add(ego);
				ego.setAttribute("EXPANDED_RELATED", "true");

				Individuals related = ego.getRelated();

				for (Individual alter : related) {
					expandByRelations(individuals, alter);
				}
			}
		}

		public static Net expandByRelations(final Net source, final Segment segment) {
			Net result;

			Individuals individuals = new Individuals();
			for (Individual individual : segment.getCurrentIndividuals()) {
				expandByRelations(individuals, individual);
			}

			result = extract(source, individuals);

			String label = result.getLabel() + "_" + segment.getLabel() + "_expanded_RELATIONS";
			result.setLabel(label);

			//
			return result;
		}*/
	

	/**
	 * 
	 * @param source
	 *            Network from which to extract.
	 * @param segmentation
	 * @return
	 * @throws PuckException
	 */
	public static Net extractMaxComponent(final Net source, final Segmentation segmentation) {
		Net result;
		
		if ((source == null) || (segmentation == null)) {
			//
			result = null;

		} else {
			
			Cluster<Individual> maxComponent = StatisticsWorker.components(segmentation.getCurrentIndividuals()).maxCluster();
			result = extract(source, new Individuals(maxComponent.getItems()));
			
		}
		//
		return result;
	}



	/**
	 * 
	 * @param source
	 *            Network from which to extract.
	 * @param segmentation
	 * @return
	 * @throws PuckException
	 */
	public static Net extractCurrentCluster(final Net source, final Segmentation segmentation) {
		Net result;

		if ((source == null) || (segmentation == null)) {
			//
			result = null;

		} else if (segmentation.isAtTheTop()) {
			//
			result = new Net(source);

		} else {
			//
			PartitionCriteria criteria = segmentation.getCurrentSegment().getCriteria();

			//
			if (criteria.getRelationModelName().equals(PartitionCriteria.RelationModelCanonicalNames.INDIVIDUAL.toString())) {
				//
				result = extract(source, segmentation.getCurrentIndividuals());

			} else if (criteria.getRelationModelName().equals(PartitionCriteria.RelationModelCanonicalNames.FAMILY.toString())) {
				//
				result = extract(source, segmentation.getCurrentFamilies());

			} else {
				//
				result = extract(source, segmentation.getCurrentRelations());
			}

			//
			result.setLabel("Extact current cluster");
			result.setGeography(source.getGeography());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param parent1
	 * @param parent2
	 * @return
	 */
	public static Individual fixFatherByGender(final Individual ego, final Individual alter) {
		Individual result;

		if ((ego == null) && (alter == null)) {
			result = null;
		} else if (ego == null) {
			if (alter.isFemale()) {
				result = ego;
			} else {
				result = alter;
			}
		} else if (alter == null) {
			if (ego.isMale()) {
				result = ego;
			} else {
				result = alter;
			}
		} else {
			if (ego.isMale() || (alter.isFemale() && ego.isUnknown())) {
				result = ego;
			} else {
				result = alter;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param parent1
	 * @param parent2
	 * @return
	 */
	public static Individual fixMotherByGender(final Individual ego, final Individual alter) {
		Individual result;

		Individual newHusband = fixFatherByGender(ego, alter);

		if (newHusband == ego) {
			result = alter;
		} else {
			result = ego;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param family
	 */
	public static void fixSpouseRolesByGender(final Family family) {

		if (family != null) {
			//
			Individual newHusband = fixFatherByGender(family.getHusband(), family.getWife());

			if (newHusband != family.getHusband()) {
				swapParents(family);
			}
		}
	}

	/**
	 * 
	 * @param family
	 */
	public static void fixSpouseRolesByGender2(final Family family) {

		if (family != null) {
			Individual ego = family.getHusband();
			Individual alter = family.getWife();

			Individual husband;
			Individual wife;
			if (ego.isMale() || (alter.isFemale() && ego.isUnknown())) {
				husband = ego;
				wife = alter;
			} else {
				husband = alter;
				wife = ego;
			}

			family.setHusband(husband);
			family.setWife(wife);
		}
	}

	public static void fuseIndividuals(final Net source, final Individual sourceIndividual, final Individual targetIndividual) throws PuckException {

		String track = targetIndividual.getAttributeValue("DOUBLE");
		if (track == null) {
			track = String.valueOf(sourceIndividual.getId());
		} else {
			track += ";" + String.valueOf(sourceIndividual.getId());
		}

		targetIndividual.setAttribute("DOUBLE", track);

		// Append attributes.
		UpdateWorker.update(sourceIndividual.toString(), targetIndividual.attributes(), sourceIndividual.attributes(), new Report(), UpdateMode.APPEND);

		// Append parents.
		if ((targetIndividual.getFather() == null) && (sourceIndividual.getFather() != null)) {
			NetUtils.setFatherRelation(source, sourceIndividual.getFather().getId(), targetIndividual.getId());
		}
		if ((targetIndividual.getMother() == null) && (sourceIndividual.getMother() != null)) {
			NetUtils.setMotherRelation(source, sourceIndividual.getMother().getId(), targetIndividual.getId());
		}

		// Append or fuse personalFamilies
		for (Family sourceFamily : sourceIndividual.getPersonalFamilies()) {
			Family targetFamily;
			if (sourceIndividual == sourceFamily.getHusband()) {
				targetFamily = source.families().getBySpouses(targetIndividual, sourceFamily.getWife());
				if (targetFamily == null) {
					sourceFamily.setHusband(targetIndividual);
					targetIndividual.addPersonalFamily(sourceFamily);
				} else {
					targetFamily.getChildren().add(sourceFamily.getChildren());
					for (Individual child : sourceFamily.getChildren()) {
						child.setOriginFamily(targetFamily);
					}
					if (sourceFamily.getHusband() != null) {
						sourceFamily.getHusband().getPersonalFamilies().removeById(sourceFamily.getId());
					}
					if (sourceFamily.getWife() != null) {
						sourceFamily.getWife().getPersonalFamilies().removeById(sourceFamily.getId());
					}
					source.families().removeById(sourceFamily.getId());
				}
			} else if (sourceIndividual == sourceFamily.getWife()) {
				targetFamily = source.families().getBySpouses(sourceFamily.getHusband(), targetIndividual);
				if (targetFamily == null) {
					sourceFamily.setWife(targetIndividual);
					targetIndividual.addPersonalFamily(sourceFamily);
				} else {
					targetFamily.getChildren().add(sourceFamily.getChildren());
					for (Individual child : sourceFamily.getChildren()) {
						child.setOriginFamily(targetFamily);
					}
					if (sourceFamily.getHusband() != null) {
						sourceFamily.getHusband().getPersonalFamilies().removeById(sourceFamily.getId());
					}
					if (sourceFamily.getWife() != null) {
						sourceFamily.getWife().getPersonalFamilies().removeById(sourceFamily.getId());
					}
					source.families().removeById(sourceFamily.getId());
				}
			}
		}

		source.remove(sourceIndividual);
	}

/*	public static Map<Individual, String> getAlterRelations(final Individual ego, final List<Individual> alters, final int[] maxDegrees,
			final List<String> relationModelNames, final String chainClassification) {
		Map<Individual, String> result;

		result = new HashMap<Individual, String>();

		for (Individual alter : alters) {
			result.put(alter, getAlterRole(ego, alter, maxDegrees, relationModelNames, chainClassification));
		}
		//
		return result;
	}*/

	public static Map<Individual, List<String>> getAlterRelations1(final Individual ego, final Set<Individual> altersSet, final int[] maxDegrees,
			final List<String> relationModelNames, final String chainClassification, final List<Individual> excludedIntermediaries, Graph<Individual> barredEdges) {
		Map<Individual, List<String>> result;

		result = new HashMap<Individual, List<String>>();
		
		List<Individual> alters = new ArrayList<Individual>(altersSet);
		Collections.sort(alters);

		List<Individual> unrelatedAlters = new ArrayList<Individual>();
		List<Individual> relatedAlters = new ArrayList<Individual>();
		
		for (Individual alter : alters) {
			List<String> roles = getAlterRoles(ego, alter, maxDegrees, relationModelNames, chainClassification, excludedIntermediaries, barredEdges);
			result.put(alter, roles);
			if (roles.size()==0){
				unrelatedAlters.add(alter);
			} else {
				relatedAlters.add(alter);
			}
		}
		
		while (relatedAlters.size()> 0 && unrelatedAlters.size()>0){
			
			Map<Individual,List<String>> newresult = new HashMap<Individual, List<String>>();

			for (Individual alter : unrelatedAlters) {
				newresult.put(alter,new ArrayList<String>());
				for (Individual medius : relatedAlters){
					if (medius!=ego){
						List<String> firstRoles = result.get(medius);
						if (firstRoles.size()>0){
							List<String> secondRoles = getAlterRoles(medius, alter, maxDegrees, relationModelNames, chainClassification, excludedIntermediaries, barredEdges);
							for (String firstRole : firstRoles){
								for (String secondRole : secondRoles){
									if (!(firstRole.equals("CHILD") && (secondRole.contains("RELATIVE")))){
										String role = firstRole+"S_"+secondRole;
										String reducedRole = reduced(role);
										while (!reducedRole.equals(role)){
											role = reducedRole;
											reducedRole = reduced(role);
										}
										if (!newresult.get(alter).contains(reducedRole)){
											newresult.get(alter).add(reducedRole);
										}
									}
								}
							}
						}
					}
				}
			}
			relatedAlters = new ArrayList<Individual>();
			for (Individual alter : newresult.keySet()){
				if (newresult.get(alter).size()>0){
					result.put(alter, newresult.get(alter));
					unrelatedAlters.remove(alter);
					relatedAlters.add(alter);
				}
			}
		}
		
		for (Individual alter : result.keySet()){
			List<String> list = result.get(alter);
			if (PuckUtils.containsStrings(list, "RELATIVE;RELATIVE_AGNATIC;RELATIVE_UTERINE;RELATIVE_COGNATIC;RELATIVE_OR_AFFINE")){
				list.add("KIN");
			}
		}
		
		//
		return result;
	}
	
	public static String reduced(String relation){
		String result;
		
		result = relation;
		
			result = result.replaceAll("RELATIVE_RELATIVE", "RELATIVE");
		
			result = result.replaceAll("FATHERS_RELATIVE_AGNATIC","RELATIVE_AGNATIC");
			result = result.replaceAll("MOTHERS_RELATIVE_UTERINE","RELATIVE_UTERINE");
			
			result = result.replaceAll("RELATIVES_CHILD","RELATIVE");
			result = result.replaceAll("RELATIVE_COGNATICS_CHILD","RELATIVE_COGNATIC");
			result = result.replaceAll("RELATIVE_AGNATICS_CHILD","RELATIVE");
			result = result.replaceAll("RELATIVE_UTERINES_CHILD","RELATIVE");

			if (result.replaceAll("FATHER", "").replaceAll("MOTHER","").replaceAll("SIBLING", "").replaceAll("CHILD","").replaceAll("_AGNATIC", "").replaceAll("_UTERINE","").replaceAll("_COGNATIC", "").equals("S_RELATIVE")){
				result = "RELATIVE_OR_AFFINE";
			}

			if (result.replaceAll("FATHER", "").replaceAll("MOTHER","").replaceAll("SIBLING", "").replaceAll("CHILD","").replaceAll("_AGNATIC", "").replaceAll("_UTERINE","").replaceAll("_COGNATIC", "").equals("RELATIVES_")){
				result = "RELATIVE_OR_AFFINE";
			}

			if (result.replaceAll("_AGNATIC", "").replaceAll("_UTERINE","").replaceAll("_COGNATIC", "").equals("RELATIVES_RELATIVE")){
				result = "RELATIVE_OR_AFFINE";
			}
			
			if (result.replaceAll("_AGNATIC", "").replaceAll("_UTERINE","").replaceAll("_COGNATIC", "").equals("RELATIVES_OR_AFFINES_RELATIVE")){
				result = "RELATIVE_OR_AFFINE";
			}
			
			if (result.replaceAll("_AGNATIC", "").replaceAll("_UTERINE","").replaceAll("_COGNATIC", "").equals("AFFINES_RELATIVE")){
				result = "AFFINE";
			}
			if (result.replaceAll("_AGNATIC", "").replaceAll("_UTERINE","").replaceAll("_COGNATIC", "").equals("RELATIVES_AFFINE")){
				result = "AFFINE";
			}
			if (result.replaceAll("_AGNATIC", "").replaceAll("_UTERINE","").replaceAll("_COGNATIC", "").equals("SPOUSES_RELATIVE")){
				result = "AFFINE";
			}
			if (result.replaceAll("_AGNATIC", "").replaceAll("_UTERINE","").replaceAll("_COGNATIC", "").equals("RELATIVES_SPOUSE")){
				result = "AFFINE";
			}

			result = result.replaceAll("AFFINES_AFFINE","AFFINE");
			result = result.replaceAll("SPOUSES_SPOUSE","AFFINE");
			
			result = result.replaceAll("SPOUSES_AFFINE","AFFINE");
			result = result.replaceAll("FATHERS_AFFINE","AFFINE");
			result = result.replaceAll("MOTHERS_AFFINE","AFFINE");
			result = result.replaceAll("SIBLINGS_AFFINE","AFFINE");
			result = result.replaceAll("CHILDS_AFFINE","AFFINE");

			result = result.replaceAll("AFFINES_SPOUSE","AFFINE");
			result = result.replaceAll("AFFINES_FATHER","AFFINE");
			result = result.replaceAll("AFFINES_MOTHER","AFFINE");
			result = result.replaceAll("AFFINES_SIBLING","AFFINE");
			result = result.replaceAll("AFFINES_CHILD","AFFINE");
			
		//
		return result;
	}

	public static String getAlterRole(final Individual ego, final Individual alter, final int[] maxDegrees, final List<String> relationModelNames, final String chainClassification) {
		String result;

		result = "UNRELATED";

		// Check for relatives

		if (ego == alter) {
			result = "IDENTITY";
		} else if (ego.spouses().contains(alter)) {
			result = "SPOUSE";
		} else if (ego.getFather()!=null && ego.getFather().equals(alter)) {
			result = "FATHER";
		} else if (ego.getMother()!=null && ego.getMother().equals(alter)) {
			result = "MOTHER";
		} else if (ego.children().contains(alter)) {
			result = "CHILD";
		} else if (ego.siblings().contains(alter)) {
			result = "SIBLING";
		} else {
			Chain chain = ChainFinder.findShortestChain(ego, alter, maxDegrees);
			if (chain != null) {
				if (chain.dim() > 1) {
					result = "AFFINE";
				} else {
					String chainType = "";
					chain.getSubchains();
					Value chainValue = ChainValuator.get(chain, chainClassification);
					if (chainValue != null) {
						chainType = "_" + chainValue.toString();
					}
					result = "RELATIVE" + chainType;
				}
			} else if (relationModelNames!=null) {
				for (Relation relation : ego.relations()) {
					if (relationModelNames.contains(relation.getModel().getName())) {
						if (relation.getIndividuals().contains(alter)) {
							result = relation.getRoleNames(alter).get(0).toString();
							break;
						}
					}
				}

			}
		}
		
		//
		return result;
	}

	public static List<String> getAlterRoles(final Individual ego, final Individual alter, final int[] maxDegrees, final List<String> relationModelNames,
			final String chainClassification) {
		return getAlterRoles(ego, alter, maxDegrees, relationModelNames, chainClassification, null, null);
	}

	public static List<String> getAlterRoles(final Individual ego, final Individual alter, final int[] maxDegrees, final List<String> relationModelNames,
			final String chainClassification, final List<Individual> excludedIntermediaries, final Graph<Individual> barredEdges) {
		List<String> result;

		result = new ArrayList<String>();

		// Check for relatives

		if (ego == alter) {
			result.add("EGO");
		} else if (ego.getFather() != null && ego.getFather().equals(alter)) {
			result.add("FATHER");
		} else if (ego.getMother() != null && ego.getMother().equals(alter)) {
			result.add("MOTHER");
		} else if (ego.spouses().contains(alter)) {
			result.add("SPOUSE");
		} else if (ego.children().contains(alter)) {
			result.add("CHILD");
		} else {
			// Check for siblings
			if (ego.siblings().contains(alter)) {
				if (excludedIntermediaries == null) {
					result.add("SIBLING");
				} else {
					boolean noExcludedIntermediaries = true;
					for (Individual parent : ego.getParents()) {
						if (alter.getParents().contains(parent) && excludedIntermediaries.contains(parent)) {
							noExcludedIntermediaries = false;
							barredEdges.addEdge(ego, alter);
							if (!excludedIntermediaries.contains(alter)){
								excludedIntermediaries.add(alter);
							}
							break;
						}
					}
					if (noExcludedIntermediaries) {
						result.add("SIBLING");
					}
				}
			}
			// Check for other relatives
			if (!result.contains("SIBLING")) {

				Chain chain = ChainFinder.findShortestChain(ego, alter, maxDegrees);
				
				if (chain != null) {

					boolean noExcludedIntermediaries = true;
					if (excludedIntermediaries != null && chain.size() > 2) {
						for (Individual intermediary : excludedIntermediaries) {
							for (int i = 1; i < chain.size() - 1; i++) {
								if (chain.get(i) instanceof Couple) {
									for (Individual apex : ((Couple) (chain.get(i))).individuals()) {
										if (apex.equals(intermediary)) {
											noExcludedIntermediaries = false;
											barredEdges.addEdge(ego, alter);
											if (!excludedIntermediaries.contains(alter)){
												excludedIntermediaries.add(alter);
											}
											break;
										}
									}
								}
								if (chain.get(i).equals(intermediary)) {
									noExcludedIntermediaries = false;
									barredEdges.addEdge(ego, alter);
									if (!excludedIntermediaries.contains(alter)){
										excludedIntermediaries.add(alter);
									}
									break;
								}
							}
							if (!noExcludedIntermediaries) {
								break;
							}
						}
					}
					if (noExcludedIntermediaries) {
						if (chain.dim() > 1) {
							result.add("AFFINE");
						} else {
							String chainType = "";
							if (chainClassification != null) {
								chain.getSubchains();
								Value chainValue = ChainValuator.get(chain, chainClassification);
								if (chainValue != null) {
									chainType = "_" + chainValue.toString();
								}
							}
							result.add("RELATIVE" + chainType);
						}
					}
				}
			}
		}
		if (ego != alter) {
			for (Relation relation : ego.relations()) {
				if (relationModelNames.contains(relation.getModel().getName())) {
					if (relation.getIndividuals().contains(alter)) {
						for (String roleName : relation.getRoleNames(alter)) {
							if (!result.contains(roleName)) {
								result.add(roleName);
							}
						}
					}
				}
			}
		}
		
		//
		
		//
		return result;
	}

	/**
	 * 
	 * @param father
	 * @param mother
	 * @return
	 */
	public static boolean isBirthOrderUsed(final Individuals source) {
		boolean result;

		if (source == null) {
			//
			result = false;
		} else {
			result = true;
			boolean ended = false;
			Iterator<Individual> iterator = source.iterator();
			while (!ended) {
				if (iterator.hasNext()) {
					//
					Individual current = iterator.next();

					//
					if (current.getBirthOrder() != null) {
						ended = true;
						result = true;
					}
				} else {
					//
					ended = true;
					result = false;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param father
	 * @param mother
	 * @return
	 */
	public static boolean isFemaleFatherOrMaleMother(final Individual father, final Individual mother) {
		boolean result;

		if ((father == null) || (mother == null)) {
			result = false;
		} else if ((father.isFemale()) || (mother.isMale())) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param father
	 * @param mother
	 * @return
	 */
	public static boolean isParentChildMarriage(final Family family, final Individual children) {
		boolean result;

		if ((family == null) || (children == null)) {
			//
			result = false;
		} else {
			//
			Individuals partners = children.spouses();

			if ((partners.contains(family.getFather())) || (partners.contains(family.getMother()))) {
				//
				result = true;
			} else {
				//
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param father
	 * @param mother
	 * @return
	 */
	public static boolean isParentChildMarriage(final Individual parent, final Individual children) {
		boolean result;

		if ((parent == null) || (children == null)) {
			result = false;
		} else if (children.spouses().contains(parent)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param father
	 * @param mother
	 * @return
	 */
	public static boolean isParentChildMarriage(final Individual parent, final Individual... children) {
		boolean result;

		if (parent == null) {
			result = false;
		} else {

			boolean ended = false;
			result = false;
			int childIndex = 0;
			while (!ended) {
				if (childIndex < children.length) {
					if (isParentChildMarriage(parent, children[childIndex])) {
						ended = true;
						result = true;
					} else {
						childIndex += 1;
					}
				} else {
					ended = true;
					result = false;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param father
	 * @param mother
	 * @return
	 */
	public static boolean isParentChildMarriage(final Individual father, final Individual mother, final Individual... children) {
		boolean result;

		if ((isParentChildMarriage(father, children)) || (isParentChildMarriage(mother, children))) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param parent1
	 * @param parent2
	 * @return
	 */
	public static boolean isRolesFixedByGender(final Individual ego, final Individual alter) {
		boolean result;

		Individual newHusband = fixFatherByGender(ego, alter);

		if (newHusband == ego) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static boolean isSame(final Individual ego, final Individual alter) {
		boolean result;

		if ((ego == null) || (alter == null)) {
			result = false;
		} else if (ego == alter) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static boolean isSameSex(final Individual ego, final Individual alter) {
		boolean result;

		if ((ego == null) || (alter == null) || (ego.isUnknown()) || (alter.isUnknown())) {
			result = false;
		} else if (ego.getGender() == alter.getGender()) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}
	
	public static int adjustParentGender(final Net source, final Report report) {
		int result;

		//
		result = 0;
		StringList list = new StringList();

		for (Family family : source.families().toSortedList()) {
			Individual father = family.getFather();
			Individual mother = family.getMother();
			
			if (father!=null && mother!=null && father.isFemale() && mother.isMale()){
				family.setFather(mother);
				family.setMother(father);
				list.appendln(family.toNameString()+"\tswapped parents");
				result++;
			} else if (father!=null && mother!=null && father.isUnknown() && mother.isUnknown()){
				father.setGender(Gender.MALE);
				mother.setGender(Gender.FEMALE);
				list.appendln(family.toNameString()+"\tset father and mother gender to male and female");
				result++;
			} else if (father!=null && father.isFemale() && (mother==null || mother.isUnknown())){
				family.setFather(mother);
				family.setMother(father);
				list.appendln(family.toNameString()+"\tswapped parents");
				result++;
			} else if (mother!=null && mother.isMale() && (father==null || father.isUnknown()) ){
				family.setFather(mother);
				family.setMother(father);
				list.appendln(family.toNameString()+"\tswapped parents");
				result++;
			} else if (father!=null && father.isUnknown()){
				father.setGender(Gender.MALE);
				list.appendln(family.toNameString()+"\tset father gender to male");
				result++;
			} else if (mother!=null && mother.isUnknown()){
				mother.setGender(Gender.FEMALE);
				list.appendln(family.toNameString()+"\tset mother gender to female");
				result++;
			}
		}

		report.outputs().appendln(result+" adjusted parent gender:");
		report.outputs().appendln();
		report.outputs().append(list);

		//
		return result;

	}

	
	
	

	public static int marryCoparents(final Net source, final Report report) {
		int result;

		//
		result = 0;
		StringList list = new StringList();

		for (Family family : source.families().toSortedList()) {
			if (!family.isSterile() && !family.isSingleParent() && !family.hasMarried()) {
				family.setMarried();
				list.appendln(family.signatureTab());
				result++;
			}
		}
		

		report.outputs().appendln(result+" unmarried couples married:");
		report.outputs().appendln();
		report.outputs().append(list);

		//
		return result;

	}

	/**
	 * Adds the identity number between parentheses to the original name (e.g.
	 * "Pierre Dupont (5)").
	 * 
	 * @param net
	 *            Source to modify.
	 */
	public static void numberNames(final Individuals source) {
		if (source != null) {
			for (Individual individual : source) {
				individual.setName(individual.getTrimmedName() + " (" + individual.getId() + ")");
			}
		}
	}

	/**
	 * adds first and last name as attributes
	 * @param source
	 */
	public static void namesToAttributes (final Individuals source){
		if (source != null) {
			for (Individual individual : source) {
				individual.setAttribute("FIRSTN", individual.getFirstName());
				individual.setAttribute("LASTTN", individual.getLastName());
			}
		}
	}

	/**
	 * adds id as attributes
	 * @param source
	 */
	public static void idToAttributes (final Individuals source){
		if (source != null) {
			for (Individual individual : source) {
				individual.setAttribute("ORIG_ID", individual.getId()+"");
			}
		}
	}

	/**
	 * Adds the identity number between parentheses to the original name (e.g.
	 * "Pierre Dupont (5)").
	 * 
	 * @param net
	 *            Source to modify.
	 */
	public static void numberNames(final Net source) {
		if (source != null) {
			numberNames(source.individuals());
		}
	}

	/**
	 * 
	 * @param family
	 */
	public static void removeFather(final Family family) {
		if (family != null) {
			family.getFather().getPersonalFamilies().removeById(family.getId());
			family.setFather(null);
		}
	}

	/**
	 * 
	 * @param family
	 */
	public static void removeMother(final Family family) {
		if ((family != null) && (family.getMother() != null)) {
			family.getMother().getPersonalFamilies().removeById(family.getId());
			family.setMother(null);
		}
	}

	/**
	 * 
	 * @param family
	 */
	public static void removeSpouse(final Family family, final Individual spouse) {
		if ((family != null) && (spouse != null)) {
			if (family.getFather() == spouse) {
				removeFather(family);
			} else if (family.getMother() == spouse) {
				removeMother(family);
			}
		}
	}

	/**
	 * This method renumerates individuals of a net.
	 * 
	 * @param net
	 *            Source to renumberate.
	 */
	public static void renumberIndividuals(final Net source) {
		//
		if (source != null) {
			//
			List<Individual> individuals = source.individuals().toSortedList();

			//
			source.individuals().clear();

			//
			int index = 1;
			for (Individual individual : individuals) {
				//
				individual.setId(index);
				index += 1;
				source.individuals().add(individual);
			}
		}
	}
	
	/**
	 * This method renumerates families of a net.
	 * 
	 * @param net
	 *            Source to renumberate.
	 */
	public static void renumberFamilies(final Net source) {
		//
		if (source != null) {
			//
			List<Family> families = source.families().toSortedList();

			//
			source.families().clear();

			//
			int index = 1;
			for (Family family : families) {
				//
				family.setId(index);
				index += 1;
				source.families().add(family);
			}
		}
	}


	/**
	 * Re-numberate a net.
	 * 
	 * @param net
	 *            Source to renumberate.
	 */
	public static int renumberFromAttribute(final Net source, final String attributeLabel) {
		int result;

		//
		result = 0;

		//
		if ((source != null) && (attributeLabel != null)) {
			//
			Individuals candidates = source.individuals().searchByAttribute(attributeLabel, "^I?\\d+$");
			Individuals unchanged = source.individuals().getIndividuals().remove(candidates);

			logger.debug("candidates: " + candidates.size());
			logger.debug("unchanged:  " + unchanged.size());

			// Check new ids.
			// This step is required because id change touch individual. So, if
			// an eror comes after an individual touch, then the net is
			// partially touched.
			Set<Integer> newIds = new HashSet<Integer>(source.size());
			for (Individual individual : unchanged) {
				//
				newIds.add(individual.getId());
			}

			for (Individual individual : candidates) {
				//
				String value = individual.getAttributeValue(attributeLabel);
				int id;
				if (value.startsWith("I")) {
					//
					id = Integer.parseInt(value.substring(1));

				} else {
					//
					id = Integer.parseInt(value);
				}

				if (newIds.contains(id)) {
					//
					throw new IllegalArgumentException(String.format("value (%d) is already set.", id));

				} else {
					//
					newIds.add(id);
				}
			}

			//
			Individuals targets = new Individuals(source.individuals().size());
			targets.add(unchanged);

			//
			for (Individual individual : candidates) {
				//
				String value = individual.getAttributeValue(attributeLabel);
				int id;
				if (value.startsWith("I")) {
					//
					id = Integer.parseInt(value.substring(1));

				} else {
					//
					id = Integer.parseInt(value);
				}

				//
				individual.setAttribute(attributeLabel, String.valueOf(individual.getId()));
				individual.setId(id);
				targets.add(individual);

				result += 1;
			}

			//
			source.individuals().clear();
			source.individuals().add(targets);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param parentId
	 * @param childId
	 * @throws PuckException
	 */
	public static void setAttribute(final Net net, final int individualId, final String label, final String value) throws PuckException {
		if ((net != null) && (individualId != 0) && (StringUtils.isNotBlank(label)) && (StringUtils.isNotBlank(value))) {
			//
			Individual individual = net.individuals().getById(individualId);
			if (individual == null) {
				individual = new Individual(individualId);
				net.individuals().add(individual);
			}

			//
			individual.attributes().put(label, value);
		}
	}

	/**
	 * WARNING: the use of this method can created duplicated families. WARNING:
	 * use only if source does not contain family ids.
	 * 
	 * WARNING: Use of this method necessitates continuously numbered families!
	 * (Do NetUtils.renumberFamilies first!
	 * 
	 * 
	 * @param parentId
	 * @param childId
	 */
	public static void setFatherRelation(final Net net, final int fatherId, final int childId)  {
		if ((net != null) && (fatherId != 0) && (childId != 0)) {
			//
			Individual parent = net.individuals().getById(fatherId);
			if (parent == null) {
				parent = new Individual(fatherId);
				net.individuals().add(parent);
			}

			//
			Individual child = net.individuals().getById(childId);
			if (child == null) {
				child = new Individual(childId);
				net.individuals().add(child);
			}

			//
			Family childSourceFamily;
			if (child.getOriginFamily() == null) {
				childSourceFamily = new Family(net.families().getLastId() + 1);
				net.families().add(childSourceFamily);
				child.setOriginFamily(childSourceFamily);
				childSourceFamily.getChildren().add(child);
			} else {
				childSourceFamily = child.getOriginFamily();
				if (childSourceFamily.getFather() != null) {
					childSourceFamily.getFather().getPersonalFamilies().removeById(childSourceFamily.getId());
				}
			}
			childSourceFamily.setFather(parent);

			//
			Family parentPersonalFamily = parent.getPersonalFamilies().getById(childSourceFamily.getId());
			if (parentPersonalFamily == null) {
				parent.getPersonalFamilies().add(childSourceFamily);
			}
		}
	}

	/**
	 * 
	 * @param graph
	 */
	public static void setGenderShapes(final Graph<Individual> graph) {
		for (Node<Individual> node : graph.getNodes()) {
			node.setTag(node.getReferent().getGender().toShapeString());
		}
	}

	/**
	 * 
	 * This method needs not null parameters.
	 * 
	 * The case "ego equals alter" is managed normally to be detectable in
	 * controls.
	 * 
	 * In case of PARENT kintype, the gender will fix the role (father, mother).
	 * 
	 * @param ego
	 *            spouse or parent
	 * @param alter
	 *            spouse or child
	 * @param type
	 * 
	 * @throws PuckException
	 */
	public static void setKin(final Net net, final Individual ego, final Individual alter, final KinType type) throws PuckException {

		if ((ego == null) || (alter == null) || (type == null)) {
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter.");
		} else {
			switch (type) {
				case PARENT:
					setKinParent(net, ego, alter);
				break;

				case CHILD:
					setKinParent(net, alter, ego);
				break;

				case SPOUSE:
					setKinSpouse(net, ego, alter);
				break;
				
				case SIBLING:
					Relation relation = new Relation(net.relations().size()+1,net.relations().size()+1,net.relationModels().getByName("SIBLING"),ego.getName()+"+"+alter.getName(), new Actor(ego,new Role("sibling")),new Actor(alter,new Role("sibling")));
					net.relations().add(relation);
					ego.relations().add(relation);
					alter.relations().add(relation);
				break;
			}
		}
	}

	/**
	 * 
	 * @param net
	 * @param family
	 * @param newFather
	 */
	public static void setKinFather(final Family family, final Individual newFather) {
		if (family != null) {
			//
			if (family.getFather() != null) {
				removeFather(family);
			}

			//
			if (newFather != null) {
				if ((family.getMother() == null) || (newFather.getId() != family.getMother().getId())) {
					family.setFather(newFather);
					newFather.getPersonalFamilies().add(family);
				}
			}
		}
	}

	/**
	 * 
	 * @param net
	 * @param father
	 * @param child
	 */
	public static void setKinFather(final Net net, final Individual father, final Individual child) {

		setKinParent(net, father, KinType.FATHER, child);
	}

	/**
	 * 
	 * @param net
	 * @param family
	 * @param newFather
	 */
	public static void setKinMother(final Family family, final Individual newMother) {
		if (family != null) {
			//
			if (family.getMother() != null) {
				removeMother(family);
			}

			//
			if (newMother != null) {
				if ((family.getFather() == null) || (newMother.getId() != family.getFather().getId())) {
					family.setMother(newMother);
					newMother.getPersonalFamilies().add(family);
				}
			}
		}
	}

	/**
	 * 
	 * @param net
	 * @param mother
	 * @param child
	 */
	public static void setKinMother(final Net net, final Individual mother, final Individual child) {

		setKinParent(net, mother, KinType.MOTHER, child);
	}

	/**
	 * This method sets a parent kinship determining parent role by gender.
	 * 
	 * In case of another parent gender value than FATHER or MOTHER, this method
	 * do nothing.
	 * 
	 * 
	 * @param net
	 * @param ego
	 * @param adjustedAlter
	 */
	public static void setKinParent(final Net net, final Individual parent, final Individual child) {

		//
		switch (parent.getGender()) {
			case FEMALE:
				setKinParent(net, parent, KinType.MOTHER, child);
			break;

			case MALE:
				setKinParent(net, parent, KinType.FATHER, child);
			break;
		}
	}

	/**
	 * In case of another parentRole value than FATHER or MOTHER, this method do
	 * nothing.
	 * 
	 * Action is done with the point of child view.
	 * 
	 * @param net
	 * @param newParent
	 * @param parentRole
	 * @param child
	 */
	public static void setKinParent(final Net net, final Individual newParent, final KinType parentRole, final Individual child) {
		// Notation:
		// F = Father
		// M = Mother
		// f = family
		// C = Child
		// C1 = Child number 1
		// (A) = kinship objects
		// + A = new kin parameter
		// ' = new object
		if ((child != null) && (parentRole != null) && ((parentRole == KinType.FATHER) || (parentRole == KinType.MOTHER))) {

			if (parentRole == KinType.FATHER) {
				// Remove father.
				if ((child.getFather() != null) && (child.getFather() != newParent)) {
					//
					Family previousFamily = child.getOriginFamily();
					previousFamily.getChildren().removeById(child.getId());
					child.setOriginFamily(null);

					//
					if (previousFamily.getMother() != null) {
						net.createFamily((Individual) null, previousFamily.getMother(), child);
					}

					// Clean
					if ((previousFamily.isSterile()) && (previousFamily.isSingleParent()) && (previousFamily.attributes().size() == 0)) {
						net.remove(previousFamily);
					}
				}

				// Add father.
				if (newParent != null) {
					if (child.getOriginFamily() == null) {
						net.createFamily(newParent, null, child);
					} else if ((child.getFather() == null) && (child.getMother() == null)) {
						child.getOriginFamily().setFather(newParent);
					} else if ((child.getFather() == null) && (child.getMother() != null)) {
						Family newOriginFamily = net.families().getBySpouses(newParent, child.getOriginFamily().getMother());
						if (newOriginFamily == null) {
							child.getOriginFamily().setFather(newParent);
							child.getFather().getPersonalFamilies().add(child.getOriginFamily());
						} else {
							//
							Family previousFamily = child.getOriginFamily();
							previousFamily.getChildren().removeById(child.getId());
							if (previousFamily.isEmpty()) {
								net.remove(previousFamily);
							}

							//
							child.setOriginFamily(newOriginFamily);
							newOriginFamily.getChildren().add(child);

							// Clean
							if ((previousFamily.isSterile()) && (previousFamily.isSingleParent()) && (previousFamily.attributes().size() == 0)) {
								net.remove(previousFamily);
							}
						}
					}
				}
			} else if (parentRole == KinType.MOTHER) {
				// Remove Mother.
				if ((child.getMother() != null) && (child.getMother() != newParent)) {
					//
					Family previousFamily = child.getOriginFamily();
					previousFamily.getChildren().removeById(child.getId());
					child.setOriginFamily(null);

					//
					if (previousFamily.getFather() != null) {
						net.createFamily(previousFamily.getFather(), (Individual) null, child);
					}

					// Clean
					if ((previousFamily.isSterile()) && (previousFamily.isSingleParent()) && (previousFamily.attributes().size() == 0)) {
						net.remove(previousFamily);
					}
				}

				// Add mother.
				if (newParent != null) {
					if (child.getOriginFamily() == null) {
						net.createFamily((Individual) null, newParent, child);
					} else if ((child.getFather() == null) && (child.getMother() == null)) {
						child.getOriginFamily().setMother(newParent);
					} else if ((child.getFather() != null) && (child.getMother() == null)) {
						Family newOriginFamily = net.families().getBySpouses(child.getOriginFamily().getFather(), newParent);
						if (newOriginFamily == null) {
							child.getOriginFamily().setMother(newParent);
							child.getMother().getPersonalFamilies().add(child.getOriginFamily());
						} else {
							//
							Family previousFamily = child.getOriginFamily();
							previousFamily.getChildren().removeById(child.getId());
							if (previousFamily.isEmpty()) {
								net.remove(previousFamily);
							}

							//
							child.setOriginFamily(newOriginFamily);
							newOriginFamily.getChildren().add(child);

							// Clean.
							if ((previousFamily.isSterile()) && (previousFamily.isSingleParent()) && (previousFamily.attributes().size() == 0)) {
								net.remove(previousFamily);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * In case of another parentRole value than FATHER or MOTHER, this method do
	 * nothing.
	 * 
	 * Bad way because there is no matter about attributes.
	 * 
	 * @param net
	 * @param newParent
	 * @param parentRole
	 * @param child
	 */
	public static void setKinParent2(final Net net, final Individual newParent, final KinType parentRole, final Individual child) {
		// Notation:
		// F = Father
		// M = Mother
		// f = family
		// C = Child
		// C1 = Child number 1
		// (A) = kinship objects
		// + A = new kin parameter
		// ' = new object

		if ((parentRole != null) && ((parentRole == KinType.FATHER) || (parentRole == KinType.MOTHER))) {
			//
			if (child.isOrphan()) {
				// Rule: (null-C) + F-C -> F-f-C
				// Rule: (null-C) + M-C -> M-f-C

				if (child.getOriginFamily() == null) {
					if (parentRole == KinType.MOTHER) {
						child.getOriginFamily().setMother(newParent);
					} else {
						child.getOriginFamily().setFather(newParent);
					}
				} else {
					if (parentRole == KinType.MOTHER) {
						net.createFamily((Individual) null, newParent, child);
					} else {
						net.createFamily(newParent, null, child);
					}
				}

				//
				newParent.getPersonalFamilies().add(child.getOriginFamily());

			} else if ((child.isOrphanOfMother()) && (parentRole == KinType.FATHER)) {
				// Check if the newParent is already parent.
				if (child.getOriginFamily().getFather() != newParent) {

					//
					if (child.getOriginFamily().getChildren().size() == 1) {
						// Rule: (F-f-C) + F'-C -> (F,F'-f-C)

						//
						Individual previousFather = child.getOriginFamily().getFather();
						previousFather.getPersonalFamilies().removeById(child.getOriginFamily().getId());

						//
						child.getOriginFamily().setFather(newParent);
					} else {
						// Rule: (F-f-C1C2) + F'-C1 -> (F-f-C2,F'-f'-C1)

						//
						child.getOriginFamily().getChildren().removeById(child.getId());

						//
						net.createFamily(newParent, null, child);
					}
				}

			} else if ((child.isOrphanOfFather()) && (parentRole == KinType.MOTHER)) {
				// Check if the newParent is already parent.
				if (child.getOriginFamily().getMother() != newParent) {

					//
					if (child.getOriginFamily().getChildren().size() == 1) {
						// Rule: (M-f-C) + M'-C -> (M,M'-f-C)

						//
						child.getOriginFamily().getMother().getPersonalFamilies().removeById(child.getOriginFamily().getId());

						//
						child.getOriginFamily().setMother(newParent);
					} else {
						// Rule: (M-f-C1C2) + M'-C1 -> (M-f-C2,M'-f'-C1)

						//
						child.getOriginFamily().getChildren().removeById(child.getId());

						//
						net.createFamily(child.getFather(), newParent, child);
					}
				}

			} else if ((child.isOrphanOfMother()) && (parentRole == KinType.MOTHER)) {

				//
				Family newChildOriginFamily = net.families().getBySpouses(child.getOriginFamily().getFather(), newParent);

				if (newChildOriginFamily == null) {
					if (child.getOriginFamily().getChildren().size() == 1) {
						// Rule: (F-f-C) + M-C -> (FM-f-C)
						child.getOriginFamily().setMother(newParent);
						newParent.getPersonalFamilies().add(child.getOriginFamily());
					} else {
						// Rule: (F-f-C1C2) + M-C1 -> (F-f-C2, FM-f'-C)

						//
						child.getOriginFamily().getChildren().removeById(child.getId());

						//
						net.createFamily(child.getFather(), newParent, child);
					}
				} else {
					if (child.getOriginFamily().getChildren().size() == 1) {
						// Rule: (F-f1-C, FM-f2) + M-C -> (f1 removed, FM-f2-C)
						child.getFather().getPersonalFamilies().removeById(child.getOriginFamily().getId());
						child.getOriginFamily().getChildren().removeById(child.getId());
						net.families().removeById(child.getOriginFamily().getId());

						//
						child.setOriginFamily(newChildOriginFamily);
					} else {
						// Rule: (F-f1-C1C2, FM-f2) + M-C1 -> (F-f1-C2,
						// FM-f2-C1)
						child.getOriginFamily().getChildren().removeById(child.getId());
						child.setOriginFamily(newChildOriginFamily);
						child.getOriginFamily().getChildren().add(child);
					}
				}

			} else if ((child.isOrphanOfFather()) && (parentRole == KinType.FATHER)) {
				//
				Family newChildOriginFamily = net.families().getBySpouses(newParent, child.getOriginFamily().getMother());

				if (newChildOriginFamily == null) {
					if (child.getOriginFamily().getChildren().size() == 1) {
						// Rule: (M-f-C) + F-C -> (FM-f-C)
						child.getOriginFamily().setFather(newParent);
						newParent.getPersonalFamilies().add(child.getOriginFamily());
					} else {
						// Rule: (M-f-C1C2) + F-C1 -> (F-f-C2, FM-f'-C)

						//
						child.getOriginFamily().getChildren().removeById(child.getId());

						//
						net.createFamily(child.getFather(), newParent, child);
					}
				} else {
					if (child.getOriginFamily().getChildren().size() == 1) {
						// Rule: (M-f1-C, FM-f2) + F-C -> (f1 removed, FM-f2-C)
						child.getMother().getPersonalFamilies().removeById(child.getOriginFamily().getId());
						child.getOriginFamily().getChildren().removeById(child.getId());
						net.families().removeById(child.getOriginFamily().getId());

						//
						child.setOriginFamily(newChildOriginFamily);
					} else {
						// Rule: (M-f1-C1C2, FM-f2) + F-C1 -> (F-f1-C2,
						// FM-f2-C1)
						child.getOriginFamily().getChildren().removeById(child.getId());
						child.setOriginFamily(newChildOriginFamily);
						child.getOriginFamily().getChildren().add(child);
					}
				}

			} else {
				// Note: child is not orphan.

				if ((parentRole == KinType.FATHER) && (child.getFather() != newParent)) {
					//
					Family newChildOriginFamily = net.families().getBySpouses(newParent, child.getOriginFamily().getMother());

					if (newChildOriginFamily == null) {
						// Rule: (FM-f-C) + F'-C -> (FM-f, F'M-f'-C)

						//
						child.getOriginFamily().getChildren().removeById(child.getId());

						//
						net.createFamily(newParent, child.getMother(), child);
					} else {
						// Rule: (FM-f1-C, F'M-f2) + F'-C -> (FM-f1, F'M-f2-C)

						//
						child.getOriginFamily().getChildren().removeById(child.getId());
						child.setOriginFamily(newChildOriginFamily);
						child.getOriginFamily().getChildren().put(child);
					}
				} else if ((parentRole == KinType.MOTHER) && (child.getMother() != newParent)) {
					//
					Family newChildOriginFamily = net.families().getBySpouses(child.getOriginFamily().getFather(), newParent);

					if (newChildOriginFamily == null) {
						// Rule: (FM-f-C) + M'-C -> (FM-f, FM'-f'-C)

						//
						child.getOriginFamily().getChildren().removeById(child.getId());

						//
						net.createFamily(child.getFather(), newParent, child);
					} else {
						// Rule: (FM-f1-C, FM'-f2) + M'-C -> (FM-f1, FM'-f2-C)

						//
						child.getOriginFamily().getChildren().removeById(child.getId());
						child.setOriginFamily(newChildOriginFamily);
						child.getOriginFamily().getChildren().put(child);
					}
				}
			}
		}
	}

	/**
	 * WARNING: the use of this method can created duplicated families. WARNING:
	 * use only if source does not contain family ids or contains necessitates continuously numbered families!
	 * (Do NetUtils.renumberFamilies first!
	 * 
	 * @param net
	 * @param egoId
	 * @param alterId
	 * @param kinType
	 * @throws PuckException
	 */
	public static void setKinRelation(final Net net, final Individual ego, final Individual alter, final KinType kinType) throws PuckException {
		int egoId = ego.getId();
		int alterId = alter.getId();

		switch (kinType) {
			case PARENT:
				switch (alter.getGender()) {
					case MALE:
						setFatherRelation(net, alterId, egoId);
					break;
					case FEMALE:
						setMotherRelation(net, alterId, egoId);
					break;
				}
			break;
			case CHILD:
				switch (ego.getGender()) {
					case MALE:
						setFatherRelation(net, egoId, alterId);
					break;
					case FEMALE:
						setMotherRelation(net, egoId, alterId);
					break;
				}
			break;
			case SPOUSE:
				switch (ego.getGender()) {
					case MALE:
						setSpouseRelation(net, egoId, alterId);
					break;
					case FEMALE:
						setSpouseRelation(net, alterId, egoId);
					break;
				}
			break;
		}
	}

	/**
	 * This method sets a spouse kinship determining role by gender.
	 * 
	 * @param net
	 * @param ego
	 * @param alter
	 */
	public static void setKinSpouse(final Net net, final Individual ego, final Individual alter) {
		//
		Individual husband;
		Individual wife;
		if (ego.isMale() || (alter.isFemale() && ego.isUnknown())) {
			husband = ego;
			wife = alter;
		} else {
			wife = ego;
			husband = alter;
		}

		//
		setKinSpouseByRole(net, husband, wife);
	}

	/**
	 * This method sets a spouse kinship.
	 * 
	 * @param net
	 * @param ego
	 * @param adjustedAlter
	 */
	public static void setKinSpouseByRole(final Net net, final Individual husband, final Individual wife) {
		//
		Family family = net.families().getBySpouses(husband, wife);

		//
		if (family == null) {
			family = new Family(net.families().getFirstFreeId());
			net.families().add(family);

			//
			husband.getPersonalFamilies().add(family);
			wife.getPersonalFamilies().add(family);
		}

		//
		family.setHusband(husband);
		family.setWife(wife);

		//
		family.setMarried(true);
	}

	/**
	 * WARNING: the use of this method can created duplicated families. WARNING:
	 * use only if source does not contain family ids.
	 * 
	 * WARNING: Use of this method necessitates continuously numbered families!
	 * (Do NetUtils.renumberFamilies first!

	 * @param parentId
	 * @param childId
	 */
	public static void setMotherRelation(final Net net, final int motherId, final int childId)  {
		if ((net != null) && (motherId != 0) && (childId != 0)) {

			//
			Individual parent = net.individuals().getById(motherId);
			if (parent == null) {
				parent = new Individual(motherId);
				net.individuals().add(parent);
			}

			//
			Individual child = net.individuals().getById(childId);
			if (child == null) {
				child = new Individual(childId);
				net.individuals().add(child);
			}

			//
			Family childSourceFamily;
			if (child.getOriginFamily() == null) {
				childSourceFamily = new Family(net.families().getLastId() + 1);
				net.families().add(childSourceFamily);
				child.setOriginFamily(childSourceFamily);
				childSourceFamily.getChildren().add(child);
			} else {
				childSourceFamily = child.getOriginFamily();
				if (childSourceFamily.getMother() != null) {
					childSourceFamily.getMother().getPersonalFamilies().removeById(childSourceFamily.getId());
				}
			}
			childSourceFamily.setMother(parent);

			//
			Family parentPersonalFamily = parent.getPersonalFamilies().getById(childSourceFamily.getId());
			if (parentPersonalFamily == null) {
				parent.getPersonalFamilies().add(childSourceFamily);
			}
		}
	}

	/**
	 * WARNING: the parent role is determined by gender.
	 * 
	 * WARNING: the use of this method can created duplicated families.
	 * 
	 * WARNING: use only if source does not contain family ids.
	 * 
	 * @param parentId
	 * @param childId
	 * @throws PuckException
	 */
	public static void setParentRelation(final Net net, final int parentId, final int childId) throws PuckException {
		if ((net != null) && (parentId != 0) && (childId != 0)) {
			//
			Individual parent = net.individuals().getById(parentId);
			if (parent == null) {
				parent = new Individual(parentId);
				net.individuals().add(parent);
			}

			//
			Individual child = net.individuals().getById(childId);
			if (child == null) {
				child = new Individual(childId);
				net.individuals().add(child);
			}

			//
			Family childSourceFamily;
			if (child.getOriginFamily() == null) {
				childSourceFamily = new Family(net.families().getLastId() + 1);
				net.families().add(childSourceFamily);
				child.setOriginFamily(childSourceFamily);
				childSourceFamily.getChildren().add(child);
			} else {
				childSourceFamily = child.getOriginFamily();
			}

			//
			switch (parent.getGender()) {
				case FEMALE:
					childSourceFamily.setMother(parent);
				break;

				case MALE:
				case UNKNOWN:
					childSourceFamily.setFather(parent);
			}

			//
			Family parentPersonalFamily = parent.getPersonalFamilies().getById(childSourceFamily.getId());
			if (parentPersonalFamily == null) {
				parent.getPersonalFamilies().add(childSourceFamily);
			}
		}
	}

	/**
	 * WARNING: the use of this method can created duplicated families.
	 * 
	 * WARNING: Use of this method necessitates continuously numbered families!
	 * (Do NetUtils.renumberFamilies first!
	 * @param parentId
	 * @param childId
	 * @throws PuckException
	 */
	public static Family setSpouseRelation(final Net net, final int husbandId, final int wifeId) throws PuckException {
		Family result;

		if ((net == null) || (husbandId == 0) || (wifeId == 0)) {
			result = null;
		} else {
			//
			boolean isNewFamily = false;

			//
			Individual husband = net.individuals().getById(husbandId);
			if (husband == null) {
				husband = new Individual(husbandId);
				net.individuals().add(husband);
				isNewFamily = true;
			}

			//
			Individual wife = net.individuals().getById(wifeId);
			if (wife == null) {
				wife = new Individual(wifeId);
				net.individuals().add(wife);
				isNewFamily = true;
			}

			//
			Family family;
			if (isNewFamily) {
				family = null;
			} else {
				family = net.families().getBySpouses(husband, wife);
			}
			
			//
			if (family == null) {
				family = new Family(net.families().getLastId() + 1);
				net.families().add(family);
				family.setHusband(husband);
				family.setWife(wife);
				family.setMarried(true);
				husband.getPersonalFamilies().add(family);
				wife.getPersonalFamilies().add(family);
			}

			//
			result = family;
		}

		//
		return result;
	}

	/**
	 * WARNING: the use of this method can created duplicated families.
	 * 
	 * WARNING: Use of this method necessitates continuously numbered families!
	 * (Do NetUtils.renumberFamilies first!

	 * @param parentId
	 * @param childId
	 * @throws PuckException
	 */
	public static void setSpouseRelationAndFixRoles(final Net net, final int husbandId, final int wifeId) throws PuckException {
		//
		Family family = setSpouseRelation(net, husbandId, wifeId);
		if (family != null) {
			fixSpouseRolesByGender(family);
		}
	}

	/**
	 * 
	 * @param source
	 */
	public static void swapParents(final Family source) {
		if (source != null) {
			Individual pivot = source.getFather();
			source.setFather(source.getMother());
			source.setMother(pivot);
		}
	}
	
	public static int getParentAttributeValues(final Individuals individuals, final String label, final FiliationType filiationType, final Integer maxGenerations) {
		int result;

		result = 0;
		
		for (Individual individual : individuals){
			
			Individual parent = individual.getParent(filiationType);
			
			if (parent!=null){
				
				String value = parent.getAttributeValue(label);
				
				if (!StringUtils.isEmpty(value)){
					
					individual.setAttribute(label+"_"+parent.getGender().toAscendantChar(), value);
					result++;
					
				}
			}
			
		}
		

		//
		return result;
	}


	
	public static int getParentAttributeValues(final Individuals individuals, final TransmitAttributeValueCriteria criteria){
		int result;
		
		if (TransmitAttributeValueCriteria.isNotValid(criteria)) {
			//
			throw new IllegalArgumentException("Invalid criteria.");

		} else {
			//
			result = getParentAttributeValues(individuals, criteria.getAttributeLabel(), criteria.getFiliationType(), criteria.getMaxGenerations());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param seeds
	 * @param label
	 * @param filiationType
	 * @param maxGenerations
	 */
	public static int transmitAttributeValue(final Individuals seeds, final String label, final FiliationType filiationType, final Integer maxGenerations, final Geography geography) {
		int result;

		Individuals target = new Individuals();

		result = 0;

		Stack<Individual> stack = new Stack<Individual>();
		for (Individual seed : seeds) {
			Value seedValue = IndividualValuator.get(seed, label, geography);
			if (seedValue != null) {
				String value = seedValue.stringValue();
				stack.push(seed);
				target.add(seed);
				while (!stack.isEmpty()) {
					Individual ego = stack.pop();
					for (Individual neighbor : ego.getKin(KinType.CHILD, filiationType)) {
						if (!target.contains(neighbor)) {
							stack.push(neighbor);
							target.add(neighbor);
							neighbor.setAttribute(label, value);
							result += 1;
						}
					}
					// stack.pop();
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param seeds
	 * @param criteria
	 * @return
	 */
	public static int transmitAttributeValue(final Individuals seeds, final TransmitAttributeValueCriteria criteria, final Geography geography) {
		int result;

		if (TransmitAttributeValueCriteria.isNotValid(criteria)) {
			//
			throw new IllegalArgumentException("Invalid criteria.");

		} else {
			//
			result = transmitAttributeValue(seeds, criteria.getAttributeLabel(), criteria.getFiliationType(), criteria.getMaxGenerations(), geography);
		}

		//
		return result;
	}
	
	public static RelationModel createTransitiveRelations(final Net net, final RelationModel model) throws PuckException {
		RelationModel result;

		//
		result = net.createRelationModel(model.getName()+"+");
		for (Role role : model.roles()){
			result.roles().add(role);
		}
		net.relations().add(net.relations().getByModel(model).getTransitiveClosure(result));

		//
		return result;

	}
	
	/**
	 * to complete (for the moment only for Kanembu dataset)
	 * @param net
	 * @param report
	 */
	public static void relativeNamesFromAttributes (final Net net, Report report){
		
		StringList changes = new StringList();
		StringList conflicts = new StringList();
		StringList missings = new StringList();
		
		for (Individual indi : net.individuals()){
			
			Individual f = indi.getFather();
			Individual ff = null;
			Individual fff = null;
			
			if (f !=null){
				ff = f.getFather();
				if (ff!=null){
					fff = ff.getFather();
				}
			}
			
			String fname = indi.getAttributeValue("NAME_F");
			if (fname!=null){
				if (f!=null){
					if (StringUtils.isBlank(f.getName()) || f.getName().equals("?")){
						changes.appendln("Name of F of "+indi+"\t"+f+"\t"+f.getName()+"\tchanged to\t"+fname);
						f.setName(fname);
					} else if (!fname.equals(f.getName())){
						conflicts.appendln("Name conflict for F of "+indi+"\t"+f+"\t"+f.getName()+"\tvs\t"+fname);
					}
				} else {
					missings.appendln("Missing F of "+indi+"\t"+f+"\t\t\t"+fname);
				}
			}
			
			String ffname = indi.getAttributeValue("NAME_FF");
			if (ffname!=null){
				if (ff!=null){
					if (StringUtils.isBlank(ff.getName()) || ff.getName().equals("?")){
						changes.appendln("Name of FF of "+indi+"\t"+ff+"\t"+ff.getName()+"\tchanged to\t"+ffname);
						ff.setName(ffname);
					} else if (!ffname.equals(ff.getName())){
						conflicts.appendln("Name conflict for FF of "+indi+"\t"+ff+"\t"+ff.getName()+"\tvs\t"+ffname);
					}
				} else {
					missings.appendln("Missing FF of "+indi+"\t"+ff+"\t\t\t"+ffname);
				}
			}
			
			String fffname = indi.getAttributeValue("NAME_FFF");
			if (fffname!=null){
				if (fff!=null){
					if (StringUtils.isBlank(fff.getName()) || fff.getName().equals("?")){
						changes.appendln("Name of FFF of "+indi+"\t"+fff+"\t"+fff.getName()+"\tchanged to\t"+fffname);
						fff.setName(fffname);
					} else if (!fffname.equals(fff.getName())){
						conflicts.appendln("Name conflict for FFF of "+indi+"\t"+fff+"\t"+fff.getName()+"\tvs\t"+fffname);
					}
				} else {
					missings.appendln("Missing FFF of "+indi+"\t"+fff+"\t\t\t"+fffname);
				}
			}


		}
		
		report.outputs().appendln("Names changed:");
		report.outputs().append(changes);
		report.outputs().appendln();
		report.outputs().appendln("Name conflicts:");
		report.outputs().append(conflicts);
		report.outputs().appendln();
		report.outputs().appendln("Missing persons:");
		report.outputs().append(missings);
		report.outputs().appendln();
		
	}
	
	public static void transformRelationRolesToAttributes (final Net net){
		
		for (Relation relation : net.relations()){
			
			for (Actor actor : relation.actors()){
				
				actor.getIndividual().setAttribute(relation.getModel().getName()+"."+actor.getRole(), "Yes");
				
			}
			
			
		}
	}
		
	public static long transformRelationAttributesToAttributes (final Net net, final String relationModelName, String relationAttributeLabel, final String dateLabel){
		long result;
		
		result = 0;
		
		for (Individual individual : net.individuals()){
			
			for (Relation relation : individual.relations().getByModelName(relationModelName)){
				
				String attributeValue = relation.getAttributeValue(relationAttributeLabel);
				String dateValue = relation.getAttributeValue(dateLabel);
				String attributeLabel = relationAttributeLabel;
				
				if (attributeValue!=null){

					if (dateValue!=null){
						
						attributeLabel += "_"+dateValue;
					}
					
					if (individual.getAttributeValue(attributeLabel)!=null){
						
						attributeValue = individual.getAttributeValue(attributeLabel)+"; "+attributeValue;
					}
					
					individual.setAttribute(attributeLabel, attributeValue);
					result++;
				} 
			}
		}
		//
		return result;
	}

	
	public static void transformRelationsToAttributes (final Net net, Report report){
		
		int transformed = 0;
		
		for (RelationModel model : net.relationModels()){
			
			report.outputs().append(model.getName()+": ");
			
			if (model.roles().size()==1){
				
				Partition<Individual> values = new Partition<Individual>();
				
				for (Relation relation : net.relations().getByModel(model)){
					
					if (relation.attributes()==null || relation.attributes().isEmpty()){
						
						for (Individual individual : relation.getIndividuals()){
							
							if (values.getValue(individual)==null){
								
								values.put(individual, new Value(relation.getName()));
								
							} else {
								
								report.outputs().appendln("No unique relations");
								values = null;
								break;

							}
						}
						
						if (values == null){
							break;
						}
						
					} else {
						
						report.outputs().appendln("Relation attributes "+relation.attributes().labels().toStringWithCommas());
						values = null;
						break;
					}
				}
				
				if (values!=null){
					for (Individual individual : values.getItems()){
						individual.setAttribute(model.getName(), values.getValue(individual).toString());
					}
					report.outputs().appendln(values.nonNullClusterCount()+" distinct values attributed to "+values.itemsCount()+" individuals");
					transformed++;
				}
		
			} else {
				report.outputs().appendln("No single role");
			}
		}
		report.outputs().appendln();
		report.outputs().appendln(transformed+" relations transformed to attributes");
	}
	
	public static long transformAttributeToRelation(Net net, Segmentation segmentation, AttributeToRelationCriteria criteria){
		long result;
		
		result = 0;
		
		// Actually restricted to individuals, enlarge later...
		Scope scope = criteria.getScope();
		String label = criteria.getLabel();
		Role role = new Role(criteria.getRoleName());
		
		RelationModel model = new RelationModel(label);
		model.roles().add(role);
		net.relationModels().add(model);
		
		for (Individual individual : segmentation.getCurrentIndividuals()){
			for (Attribute attribute : individual.attributes()){
				if (attribute.getLabel().equals(label)){
					Relation valuedRelation = net.relations().getByModel(model).getByName(attribute.getValue()).getFirst();
					if (valuedRelation==null){
						valuedRelation = net.createRelation(attribute.getValue(), model, new Actor(individual,role));
					} else {
						valuedRelation.addActor(individual, role);
					}
					result++;
				}
			}
		}
		
		//
		return result;
	}
	
	public static void individualizeFamilyAttributes (Net net){
		
		for (Family family : net.families()){
			
			for (Attribute familyAttribute : family.attributes()){
				String label = familyAttribute.getLabel();
				String value = familyAttribute.getValue();
				
				for (Individual parent : family.getParents()){
					String indiLabel = label+"_"+parent.getGender().toSpouseChar();
					String indiValue = parent.getAttributeValue(indiLabel);
					if (indiValue==null){
						indiValue = value+" ("+family.getId()+")";
					} else {
						indiValue+="; "+value+" ("+family.getId()+")";
					}
					parent.setAttribute(indiLabel, indiValue);
				}
				for (Individual child : family.getChildren()){
					String indiLabel = label+"_C";
					String indiValue = child.getAttributeValue(indiLabel);
					if (indiValue==null){
						indiValue = value+" ("+family.getId()+")";
					} else {
						indiValue+="; "+value+" ("+family.getId()+")";
					}
					child.setAttribute(indiLabel, indiValue);
				}
			}
		}
	}

}
