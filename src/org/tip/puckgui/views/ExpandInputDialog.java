package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import org.tip.puck.net.FiliationType;
import org.tip.puck.net.workers.ExpandCriteria;
import org.tip.puck.net.workers.ExpansionMode;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class ExpandInputDialog extends JDialog {

	private static final long serialVersionUID = 2039420566887299841L;
	private final JPanel contentPanel = new JPanel();
	private ExpandCriteria dialogCriteria;
	private static ExpandCriteria lastCriteria = new ExpandCriteria();
	private JSpinner spnnrMaxStep;
	private JRadioButton rdbtnHorizontal;
	private JRadioButton rdbtnDescendingUterine;
	private JRadioButton rdbtnDescendingAgnatic;
	private JRadioButton rdbtnDescending;
	private JRadioButton rdbtnAscendingUterine;
	private JRadioButton rdbtnAscendingAgnatic;
	private JRadioButton rdbtnAscending;
	private JRadioButton rdbtnAllKin;
	private JRadioButton rdbtnAllRelated;
	private JRadioButton rdbtnUniversal;
	private final ButtonGroup buttonGroupExpansionMode = new ButtonGroup();

	/**
	 * Create the dialog.
	 */
	public ExpandInputDialog() {
		super();

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Expand Input");
		setIconImage(Toolkit.getDefaultToolkit().getImage(ExpandInputDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				ExpandInputDialog.this.dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 390, 380);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));
		{
			JLabel lblTarget = new JLabel("Expansion Mode:");
			this.contentPanel.add(lblTarget, "2, 2, right, default");
		}
		{
			JPanel panel = new JPanel();
			this.contentPanel.add(panel, "4, 2, fill, fill");
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			{
				this.rdbtnUniversal = new JRadioButton("Universal");
				this.buttonGroupExpansionMode.add(this.rdbtnUniversal);
				panel.add(this.rdbtnUniversal);
			}
			{
				this.rdbtnAllRelated = new JRadioButton("All Related");
				this.buttonGroupExpansionMode.add(this.rdbtnAllRelated);
				panel.add(this.rdbtnAllRelated);
			}
			{
				this.rdbtnAllKin = new JRadioButton("All Kin");
				this.buttonGroupExpansionMode.add(this.rdbtnAllKin);
				panel.add(this.rdbtnAllKin);
			}
			{
				this.rdbtnAscending = new JRadioButton("Ascending");
				this.buttonGroupExpansionMode.add(this.rdbtnAscending);
				panel.add(this.rdbtnAscending);
			}
			{
				this.rdbtnAscendingAgnatic = new JRadioButton("Ascending Agnatic");
				this.buttonGroupExpansionMode.add(this.rdbtnAscendingAgnatic);
				panel.add(this.rdbtnAscendingAgnatic);
			}
			{
				this.rdbtnAscendingUterine = new JRadioButton("Ascending Uterine");
				this.buttonGroupExpansionMode.add(this.rdbtnAscendingUterine);
				panel.add(this.rdbtnAscendingUterine);
			}
			{
				this.rdbtnDescending = new JRadioButton("Descending");
				this.buttonGroupExpansionMode.add(this.rdbtnDescending);
				panel.add(this.rdbtnDescending);
			}
			{
				this.rdbtnDescendingAgnatic = new JRadioButton("Descending Agnatic");
				this.buttonGroupExpansionMode.add(this.rdbtnDescendingAgnatic);
				panel.add(this.rdbtnDescendingAgnatic);
			}
			{
				this.rdbtnDescendingUterine = new JRadioButton("Descending Uterine");
				this.buttonGroupExpansionMode.add(this.rdbtnDescendingUterine);
				panel.add(this.rdbtnDescendingUterine);
			}
			{
				this.rdbtnHorizontal = new JRadioButton("Horizontal");
				this.buttonGroupExpansionMode.add(this.rdbtnHorizontal);
				panel.add(this.rdbtnHorizontal);
			}
		}
		{
			JLabel lblLabel = new JLabel("Max step:");
			this.contentPanel.add(lblLabel, "2, 4, right, default");
		}
		{
			this.spnnrMaxStep = new JSpinner();
			this.spnnrMaxStep.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
			this.contentPanel.add(this.spnnrMaxStep, "4, 4");
		}
		{
			JLabel lblNo = new JLabel("0 = no limit");
			this.contentPanel.add(lblNo, "4, 6, center, default");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						ExpandInputDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("Expand");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						ExpandCriteria criteria = getCriteria();

						//
						if (ExpandCriteria.isValid(criteria)) {
							//
							lastCriteria = criteria;
							ExpandInputDialog.this.dialogCriteria = criteria;

							//
							setVisible(false);

						} else {
							//
							String title = "Invalid input";
							String message = "Please, check your input.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						}

					}
				});
				{
					Component horizontalStrut = Box.createHorizontalStrut(20);
					buttonPane.add(horizontalStrut);
				}
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}

		// ////////////////////////
		setCriteria(lastCriteria);
	}

	/**
	 * 
	 * @return
	 */
	public ExpandCriteria getCriteria() {
		ExpandCriteria result;

		result = new ExpandCriteria();

		//
		if (this.rdbtnAllKin.isSelected()) {
			//
			result.setExpansionMode(ExpansionMode.KIN);
			result.setFiliationType(FiliationType.COGNATIC);
			result.setSubTitle("expanded_ALL_KIN");

		} else if (this.rdbtnAllRelated.isSelected()) {
			//
			result.setExpansionMode(ExpansionMode.RELATED);
			result.setFiliationType(null);
			result.setSubTitle("expanded_RELATIONS");

		} else if (this.rdbtnAscending.isSelected()) {
			//
			result.setExpansionMode(ExpansionMode.PARENT);
			result.setFiliationType(FiliationType.COGNATIC);
			result.setSubTitle("expanded_UP");

		} else if (this.rdbtnAscendingAgnatic.isSelected()) {
			//
			result.setExpansionMode(ExpansionMode.PARENT);
			result.setFiliationType(FiliationType.AGNATIC);
			result.setSubTitle("expanded_UP_A");

		} else if (this.rdbtnAscendingUterine.isSelected()) {
			//
			result.setExpansionMode(ExpansionMode.PARENT);
			result.setFiliationType(FiliationType.UTERINE);
			result.setSubTitle("expanded_UP_U");

		} else if (this.rdbtnDescending.isSelected()) {
			//
			result.setExpansionMode(ExpansionMode.CHILD);
			result.setFiliationType(FiliationType.COGNATIC);
			result.setSubTitle("");

		} else if (this.rdbtnDescendingAgnatic.isSelected()) {
			//
			result.setExpansionMode(ExpansionMode.CHILD);
			result.setFiliationType(FiliationType.AGNATIC);
			result.setSubTitle("expanded_DOWN_A");

		} else if (this.rdbtnDescendingUterine.isSelected()) {
			//
			result.setExpansionMode(ExpansionMode.CHILD);
			result.setFiliationType(FiliationType.UTERINE);
			result.setSubTitle("expanded_DOWN_U");

		} else if (this.rdbtnHorizontal.isSelected()) {
			//
			result.setExpansionMode(ExpansionMode.SPOUSE);
			result.setFiliationType(FiliationType.COGNATIC);
			result.setSubTitle("expanded_MARR");

		} else if (this.rdbtnUniversal.isSelected()) {
			//
			result.setExpansionMode(ExpansionMode.ALL);
			result.setFiliationType(FiliationType.COGNATIC);
			result.setSubTitle("expanded_ALL");
		}

		//
		result.setMaxStep((Integer) this.spnnrMaxStep.getValue());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public ExpandCriteria getDialogCriteria() {
		ExpandCriteria result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final ExpandCriteria source) {
		//
		if (source != null) {

			//
			this.rdbtnAllKin.setSelected(false);
			this.rdbtnAllRelated.setSelected(false);
			this.rdbtnAscending.setSelected(false);
			this.rdbtnAscendingAgnatic.setSelected(false);
			this.rdbtnAscendingUterine.setSelected(false);
			this.rdbtnDescending.setSelected(false);
			this.rdbtnDescendingAgnatic.setSelected(false);
			this.rdbtnDescendingUterine.setSelected(false);
			this.rdbtnHorizontal.setSelected(false);
			this.rdbtnUniversal.setSelected(false);

			//
			switch (source.getExpansionMode()) {
				case ALL:
					this.rdbtnUniversal.setSelected(true);
				break;

				case CHILD:
					if (source.getFiliationType() == null) {
						//
						this.rdbtnDescending.setSelected(true);

					} else {
						//
						switch (source.getFiliationType()) {
							case AGNATIC:
								this.rdbtnDescendingAgnatic.setSelected(true);
							break;

							case UTERINE:
								this.rdbtnDescendingUterine.setSelected(true);
							break;

							case COGNATIC:
							default:
								this.rdbtnDescending.setSelected(true);
						}
					}
				break;

				case KIN:
					this.rdbtnUniversal.setSelected(true);
				break;

				case PARENT:
					if (source.getFiliationType() == null) {
						//
						this.rdbtnAscending.setSelected(true);

					} else {
						//
						switch (source.getFiliationType()) {
							case AGNATIC:
								this.rdbtnAscendingAgnatic.setSelected(true);
							break;

							case UTERINE:
								this.rdbtnAscendingUterine.setSelected(true);
							break;

							case COGNATIC:
							default:
								this.rdbtnAscending.setSelected(true);
						}
					}
				break;

				case RELATED:
					this.rdbtnAllRelated.setSelected(true);
				break;

				case SPOUSE:
					this.rdbtnHorizontal.setSelected(true);
				break;

				default:
			}

			//
			this.spnnrMaxStep.setValue(source.getMaxStep());
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		/* ExpandCriteria criteria = */showDialog();
	}

	/**
	 * Launch the application.
	 */
	public static ExpandCriteria showDialog() {
		ExpandCriteria result;

		//
		ExpandInputDialog dialog = new ExpandInputDialog();
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
