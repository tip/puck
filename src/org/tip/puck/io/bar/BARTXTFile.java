package org.tip.puck.io.bar;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.geo.Geography;
import org.tip.puck.geo.io.GEOTXTFile;
import org.tip.puck.io.iur.IURTXTFile;
import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.net.UnionStatus;
import org.tip.puck.net.workers.NetUtils;

/**
 * This class represents a TXT File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class BARTXTFile {
	protected enum Format {
		ONEMODE,
		TWOMODE
	}

	protected enum Status {
		MANDATORY,
		OPTIONAL
	}

	private static final Logger logger = LoggerFactory.getLogger(BARTXTFile.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	public static final int MAX_LINE_SIZE = 2048;
	public static final String BIRTH_ORDER_LABEL = "ORD";
	public static final String HUSBAND_ORDER_LABEL = "HUSB_ORD";
	public static final String WIFE_ORDER_LABEL = "WIFE_ORD";
	public static final String DIVORCE_LABEL = "DIV";

	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file) throws PuckException {
		Net result;

		result = load(file, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file, final String charsetName) throws PuckException {
		Net result;

		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), charsetName));
			result = read(in);

			result.setLabel(file.getName());

		} catch (UnsupportedEncodingException exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");

		} catch (FileNotFoundException exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");

		} finally {
			//
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}

	/**
	 * Reads a net from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static Net read(final BufferedReader in) throws PuckException {
		Net result;

		result = new Net();

		//
		readIndividuals(result, in);

		//
		readFamilies(result, in);

		// Read family attributes.
		readFamilyAttributes(result, in);

		// Read relations.
		boolean relationEnded = false;
		while (!relationEnded) {
			if (!IURTXTFile.readRelations(result, in)) {
				relationEnded = true;
			}
		}

		// Read Geography.
		Geography geography = GEOTXTFile.readGeography(in);
		if (geography != null) {
			result.setGeography(geography);
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * Reads a line of attributes from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of attributes or null if no more.
	 * 
	 * @throws PuckException
	 */
	public static BARAttributesLine readAttributesLine(final BufferedReader in) throws PuckException {
		BARAttributesLine result;

		try {
			//
			in.mark(MAX_LINE_SIZE);
			String line = readNotEmptyLine(in);

			if (line == null) {
				result = null;
			} else if (line.matches("^\\d.*$")) {
				String[] tokens = line.split("\\t");

				result = new BARAttributesLine();

				result.setId((Double.valueOf(tokens[0]).intValue()));
				for (int tokenCount = 1; tokenCount < tokens.length; tokenCount++) {
					result.values().add(tokens[tokenCount].trim());
				}
			} else {
				in.reset();
				result = null;
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading individual line.");
		}

		//
		return result;
	}

	/**
	 * 
	 * @param result
	 * @param in
	 * @throws PuckException
	 */
	public static void readFamilies(final Net result, final BufferedReader in) throws PuckException {
		// Read second block labels.
		logger.debug("Read second block.");
		BARTXTLabelsLine labelsLine = readLabelsLine(in, Status.MANDATORY);
		// logger.debug("Labels=" + LogHelper.toString(labelsLine));

		// Read second block datas.
		if ((labelsLine != null) && (labelsLine.size() > 1)) {
			//
			boolean ended = false;
			while (!ended) {
				//
				BARAttributesLine source = readAttributesLine(in);
				// logger.debug("====>" + LogHelper.toString(source));
				if (source == null) {
					//
					ended = true;

				} else if (source.id() != 0) {
					// logger.debug("source=" + LogHelper.toString(source));
					for (int attributeIndex = 0; attributeIndex < source.values().size(); attributeIndex++) {
						//
						String label = labelsLine.get(attributeIndex + 1);
						String value = source.values().get(attributeIndex);

						//
						if (StringUtils.isNotBlank(value)) {
							//
							if (label.equals(BIRTH_ORDER_LABEL) && StringUtils.isNumeric(value)) {
								//
								result.individuals().getById(source.id()).setBirthOrder(new Double(value).intValue());

							} else if (label.equals(DIVORCE_LABEL)) {
								//
								String[] exSpouseIds = value.split(";");
								for (String exSpouseId : exSpouseIds) {
									if (StringUtils.isNumeric(exSpouseId)) {
										Family family = result.families().getBySpouses(source.id(), Integer.parseInt(exSpouseId));
										if (family != null) {
											family.setDivorced();
										} else {
											System.err.println("Divorced spouse missing for " + source.id() + ": " + exSpouseId);
											result.individuals().setAttribute(source.id(), "NOTE_DIV", "Divorced of " + value);
										}
									}
								}
							} else {
								//
								result.individuals().setAttribute(source.id(), label, value);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @return
	 * @throws PuckException
	 */
	public static boolean readFamilyAttributes(final Net target, final BufferedReader in) throws PuckException {
		boolean result;

		logger.debug("Read family attribute block.");

		// Mark stream position in case of bad block.
		try {
			in.mark(MAX_LINE_SIZE);

			// Read relation model name.
			BARTXTLabelsLine labelsLine = readLabelsLine(in, Status.MANDATORY);
			// logger.debug("Labels=" + LogHelper.toString(labelsLine));

			//
			if (labelsLine == null) {
				//
				result = false;

			} else if ((labelsLine.size() != 1) || (!labelsLine.get(0).equalsIgnoreCase("FAMILY"))) {
				//
				logger.debug(labelsLine.get(0) + ": This block is not a family attributes block. Rewind");
				result = false;

				in.reset();

			} else {
				//
				logger.debug("Read family attribute block.");
				labelsLine = readLabelsLine(in, Status.MANDATORY);

				//
				if ((labelsLine != null) && (labelsLine.size() > 1)) {
					//
					boolean ended = false;
					while (!ended) {
						//
						BARAttributesLine source = readAttributesLine(in);
						// logger.debug("====>" +
						// LogHelper.toString(source));
						if (source == null) {
							//
							ended = true;

						} else if (source.id() != 0) {

							int husbandId = Integer.parseInt(source.values().get(0));
							int wifeId = Integer.parseInt(source.values().get(1));
							Family family = target.families().getBySpouses(husbandId, wifeId);

							for (int attributeCount = 2; attributeCount < source.values().size(); attributeCount++) {
								//
								String label = labelsLine.get(attributeCount + 1);
								String value = source.values().get(attributeCount);
								if (StringUtils.isNotBlank(value)) {
									family.setAttribute(label, value);
									if (label.equals(HUSBAND_ORDER_LABEL) && StringUtils.isNumeric(value)) {
										family.setHusbandOrder(new Double(value).intValue());
									}
									if (label.equals(WIFE_ORDER_LABEL) && StringUtils.isNumeric(value)) {
										family.setWifeOrder(new Double(value).intValue());
									}
									if (label.equals("UNIONSTATUS")) {
										family.setUnionStatus(UnionStatus.valueOf(value));
									}
								}
							}
						}
					}
				}

				//
				logger.debug("Family Attributes loaded.");
				result = true;
			}

		} catch (IOException exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.IO_ERROR.create(exception, "Error on readFamilyAttributes.");
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * Reads a line of individual data from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of individual data or null.
	 * 
	 * @throws PuckException
	 */
	public static BARTXTIndividualLine readIndividualLine(final BufferedReader in) throws PuckException {
		BARTXTIndividualLine result;

		try {
			//
			in.mark(MAX_LINE_SIZE);
			String line = readNotEmptyLine(in);

			if (line == null) {
				result = null;
			} else if (line.matches("^\\d.*$")) {
				String[] tokens = line.split("\\t");

				result = new BARTXTIndividualLine(Format.ONEMODE);
				result.setId((Double.valueOf(tokens[0]).intValue()));
				if (tokens.length > 1) {
					result.setName(tokens[1]);
				}
				if (tokens.length > 2 && tokens[2].length() > 0) {
					result.setGender(tokens[2].charAt(0));
				} else {
					result.setGender('X');
				}
				if (tokens.length > 3 && NumberUtils.isNumber(tokens[3])) {
					result.setFatherId((Double.valueOf(tokens[3]).intValue()));
				} else {
					result.setFatherId(0);
				}

				if (tokens.length > 4 && NumberUtils.isNumber(tokens[4])) {
					result.setMotherId((Double.valueOf(tokens[4]).intValue()));
				} else {
					result.setMotherId(0);
				}
				for (int tokenCount = 5; tokenCount < tokens.length; tokenCount++) {
					if (NumberUtils.isNumber(tokens[tokenCount])) {
						result.spouseIds().add((Double.valueOf(tokens[tokenCount]).intValue()));
					}
				}
			} else {
				in.reset();
				result = null;
			}
		} catch (final IOException exception) {
			//
			throw PuckExceptions.IO_ERROR.create(exception, "Reading individual line.");
		}

		//
		return result;
	}

	/**
	 * Reads a line of individual data from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of individual data or null.
	 * 
	 * @throws PuckException
	 */
	public static BARTXTIndividualLine readIndividualLineTwoMode(final BufferedReader in) throws PuckException {
		BARTXTIndividualLine result;

		try {
			//
			in.mark(MAX_LINE_SIZE);
			String line = readNotEmptyLine(in);

			if (line == null) {
				result = null;
			} else if (line.matches("^\\d.*$")) {
				String[] tokens = line.split("\\t");

				result = new BARTXTIndividualLine(Format.TWOMODE);
				result.setId((Double.valueOf(tokens[0]).intValue()));
				if (tokens.length > 1) {
					result.setName(tokens[1]);
				}
				if (tokens.length > 2 && tokens[2].length() > 0) {
					result.setGender(tokens[2].charAt(0));
				} else {
					result.setGender('X');
				}
				if (tokens.length > 3 && NumberUtils.isNumber(tokens[3])) {
					result.setOriginFamilyId((Double.valueOf(tokens[3]).intValue()));
				} else {
					result.setOriginFamilyId(0);
				}
				for (int tokenCount = 4; tokenCount < tokens.length; tokenCount++) {
					if (NumberUtils.isNumber(tokens[tokenCount])) {
						result.personalFamilyIds().add((Double.valueOf(tokens[tokenCount]).intValue()));
					}
				}
			} else {
				in.reset();
				result = null;
			}
		} catch (final IOException exception) {
			//
			throw PuckExceptions.IO_ERROR.create(exception, "Reading individual line.");
		}

		//
		return result;
	}

	/**
	 * 
	 * @param result
	 * @param in
	 * @throws PuckException
	 */
	public static void readIndividuals(final Net result, final BufferedReader in) throws PuckException {
		// Read first block labels.
		Format format = readLabelsLine(in, Status.OPTIONAL).getFormat();

		// Read first block datas.
		logger.debug("Read first block.");
		boolean ended = false;
		while (!ended) {
			//
			BARTXTIndividualLine source;

			if (format == Format.TWOMODE) {
				source = readIndividualLineTwoMode(in);
			} else {
				source = readIndividualLine(in);
			}

			// logger.debug("source = " + LogHelper.toString(source));

			if (source == null) {
				ended = true;
			} else if (source.id() != 0) {

				// Get individual or create it.
				Individual individual = result.get(source.id());
				if (individual == null) {
					individual = new Individual(source.id());
					result.individuals().put(individual);
				}

				// Update specific attributes.
				if (!StringUtils.isBlank(source.name())) {
					individual.setName(source.name());
				}
				if (Gender.valueOf(source.gender()) != Gender.UNKNOWN) {
					individual.setGender(Gender.valueOf(source.gender()));
				}

				// Update source family.
				if (source.originFamilyId() != 0) {

					if (individual.getOriginFamily() == null) {

						Family parents = result.families().getById(source.originFamilyId());
						if (parents == null) {
							parents = new Family(source.originFamilyId());
							result.families().put(parents);
						}

						// Update current individual.
						parents.getChildren().put(individual);
						individual.setOriginFamily(parents);
					}

				} else if ((source.fatherId() != 0) || (source.motherId() != 0)) {
					if (individual.getOriginFamily() == null) {
						// Instantiate a flag to save family search.
						boolean isNewFamily = false;

						// Get father or create him.
						Individual father;
						if (source.fatherId() == 0) {
							father = null;
						} else {
							father = result.get(source.fatherId());
							if (father == null) {
								father = new Individual(source.fatherId());
								result.individuals().put(father);
								isNewFamily = true;
							}
						}

						// Get father or create her.
						Individual mother;
						if (source.motherId() == 0) {
							mother = null;
						} else {
							mother = result.get(source.motherId());
							if (mother == null) {
								mother = new Individual(source.motherId());
								result.individuals().put(mother);
								isNewFamily = true;
							}
						}

						// Get parent family or create it.
						// Note: If one parent is unknown, we can't associate
						// the individual to an existant family, we have to
						// avoid the german relation. So, if one parent is
						// unknown, we have to create a new family.
						Family parents;
						if ((isNewFamily) || (father == null) || (mother == null)) {
							parents = null;
						} else {
							parents = result.families().getBySpouses(father, mother);
							// Check roles.
							if (parents != null) {
								// Fix role issue (because missed information
								// in spouses creation).
								if ((parents.getHusband() == mother) || (parents.getWife() == father)) {
									parents.setHusband(father);
									parents.setWife(mother);
								}
							}
						}

						if (parents == null) {
							parents = new Family(result.families().size() + 1);
							result.families().put(parents);
							parents.setSpouses(father, mother);
							if (father != null) {
								father.getPersonalFamilies().add(parents);
							}
							if (mother != null) {
								mother.getPersonalFamilies().add(parents);
							}
						}

						// Update current individual.
						parents.getChildren().put(individual);
						individual.setOriginFamily(parents);
					}
				}

				// Update spouses.
				if (format == Format.TWOMODE) {
					for (int personalFamilyId : source.personalFamilyIds()) {
						if (personalFamilyId != 0) {

							// Get spouse family or create it.
							Family family = result.families().getById(personalFamilyId);

							if (family == null) {
								family = new Family(personalFamilyId);
								result.families().put(family);
							}

							// Set default roles
							if (individual.isMale()) {
								family.setHusband(individual);
							} else {
								family.setWife(individual);
							}
							//
							if (family.getHusband() != null && family.getWife() != null) {
								family.setMarried(true);
							}

							// Update current individual.
							individual.getPersonalFamilies().put(family);
						}
					}
				} else {
					for (int spouseId : source.spouseIds()) {
						if (spouseId != 0) {
							// Instantiate a flag to save family search.
							boolean isNewFamily = false;

							// Get spouse or create it.
							Individual spouse = result.individuals().getById(spouseId);
							if (spouse == null) {
								spouse = new Individual(spouseId);
								result.individuals().put(spouse);
								isNewFamily = true;
							}

							// Get spouse family or create it.
							Family family;
							if (isNewFamily) {
								family = null;
							} else {
								family = result.families().getBySpouses(individual, spouse);
							}

							if (family == null) {
								family = new Family(result.families().size() + 1);

								// Set default roles (because missed
								// information).
								if (individual.isMale()) {
									family.setSpouses(individual, spouse);
								} else {
									family.setSpouses(spouse, individual);
								}
								result.families().put(family);
							}

							//
							family.setMarried(true);

							// Update current individual.
							individual.getPersonalFamilies().put(family);
							spouse.getPersonalFamilies().put(family);
						}
					}
				}

				// logger.debug("put : " + individual);
			}
		}

	}

	/**
	 * Reads a line of labels from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of labels or null.
	 * 
	 * @throws PuckException
	 */
	public static BARTXTLabelsLine readLabelsLine(final BufferedReader in, final Status status) throws PuckException {
		BARTXTLabelsLine result;

		try {
			//
			in.mark(MAX_LINE_SIZE);
			String line = readNotEmptyLine(in);

			if (line == null) {
				result = null;
			} else if (line.matches("^\\d.*$")) {
				if (status == Status.MANDATORY) {
					throw PuckExceptions.BAD_FILE_FORMAT.create("Bad labels line format: [" + line + "].");
				} else {
					in.reset();
					result = null;
				}
			} else {
				String[] tokens = line.split("\\t");

				result = new BARTXTLabelsLine();

				if (tokens[0].equalsIgnoreCase("IDI")) {
					result.setFormat(Format.TWOMODE);
				} else {
					result.setFormat(Format.ONEMODE);
				}

				for (String token : tokens) {
					result.add(token);
				}
			}
		} catch (final IOException exception) {
			//
			throw PuckExceptions.IO_ERROR.create(exception, "Reading labels line.");
		}

		//
		return result;
	}

	/**
	 * Reads a not empty line from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a not empty line or null.
	 * 
	 * @throws PuckException
	 */
	public static String readNotEmptyLine(final BufferedReader in) throws PuckException {
		String result;

		try {
			boolean ended = false;
			result = null;
			while (!ended) {
				//
				String line = in.readLine();

				if (line == null) {
					ended = true;
					result = null;
				} else if (StringUtils.isNotBlank(line)) {
					ended = true;
					result = line;
				}
			}
		} catch (final IOException exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.IO_ERROR.create(exception, "Reading line.");
		}

		//
		return result;
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File file, final Net source) throws PuckException {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));

			write(out, source);

		} catch (UnsupportedEncodingException exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");

		} catch (FileNotFoundException exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");

		} finally {
			//
			IOUtils.closeQuietly(out);
		}
	}

	/**
	 * Write a line of attributes.
	 * 
	 * @param out
	 *            Target.
	 * @param source
	 *            Source.
	 */
	public static void write(final PrintWriter out, final BARAttributesLine source) {
		if (source != null) {
			StringBuffer buffer = new StringBuffer(192);
			buffer.append(source.id());
			for (String value : source.values()) {
				buffer.append("\t");
				buffer.append(value);
			}
			out.println(buffer.toString());
		}
	}

	/**
	 * Writes a line of individual data.
	 * 
	 * @param out
	 *            Target.
	 * @param source
	 *            Source.
	 */
	public static void write(final PrintWriter out, final BARTXTIndividualLine source) {

		StringBuffer buffer = new StringBuffer(512);
		buffer.append(String.format("%d\t%s\t%c\t%d\t%d", source.id(), source.name(), source.gender(), source.fatherId(), source.motherId()));
		for (int spouseId : source.spouseIds()) {
			buffer.append(String.format("\t%d", spouseId));
		}

		out.println(buffer.toString());
	}

	/**
	 * Writes a line of labels.
	 * 
	 * @param out
	 *            Target.
	 * @param source
	 *            Source.
	 */
	public static void write(final PrintWriter out, final BARTXTLabelsLine source) {

		StringBuffer buffer = new StringBuffer(192);
		for (String value : source) {
			if (buffer.length() != 0) {
				buffer.append("\t");
			}
			buffer.append(value);
		}
		out.println(buffer.toString());
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 */
	public static void write(final PrintWriter out, final Net source) {

		// Write first block labels.
		logger.debug("Write first block labels.");
		out.println("ID\tName\tGender\tFather\tMother\tSpouses");

		// Write first block data.
		logger.debug("Write first block data.");
		for (Individual individual : source.individuals().toSortedList()) {
			//
			BARTXTIndividualLine target = new BARTXTIndividualLine(Format.ONEMODE);

			target.setId(individual.getId());
			target.setName(individual.getName());
			target.setGender(individual.getGender().toChar());

			if (individual.getFather() == null) {
				//
				target.setFatherId(0);

			} else {
				//
				target.setFatherId(individual.getFather().getId());
			}

			if (individual.getMother() == null) {
				//
				target.setMotherId(0);

			} else {
				//
				target.setMotherId(individual.getMother().getId());
			}

			for (Family family : individual.getPersonalFamilies().toSortedList()) {
				//
				if (family.hasMarried()) {
					//
					Individual spouse = family.getOtherParent(individual);

					//
					if (spouse != null) {
						target.spouseIds().add(spouse.getId());
					}
				}
			}

			//
			write(out, target);
		}
		out.println();

		// Write second block labels.
		// ==========================

		// Find labels.
		logger.debug("Find labels.");
		BARTXTLabelsLine labelsLine = new BARTXTLabelsLine();
		// Never write "ID" cause a Microsoft Excel bug.
		labelsLine.add("Id");
		for (Individual individual : source.individuals()) {
			//
			for (String label : individual.attributes().labels().sort()) {
				//
				if (!labelsLine.contains(label)) {
					//
					labelsLine.add(label);
				}
			}
		}

		// Add the birth order attribute.
		if (!labelsLine.contains(BIRTH_ORDER_LABEL)) {
			//
			if (NetUtils.isBirthOrderUsed(source.individuals())) {
				//
				labelsLine.add(BIRTH_ORDER_LABEL);
			}
		}

		//
		if ((labelsLine.size() != 1) || (source.relationModels().size() != 0)) {
			//
			write(out, labelsLine);
		}

		if (labelsLine.size() != 1) {
			// Build and write attributes lines.
			for (Individual individual : source.individuals().toSortedList()) {
				// Build attributes line.
				BARAttributesLine target = new BARAttributesLine();
				target.setId(individual.getId());
				if ((!individual.attributes().isEmpty()) || (individual.getBirthOrder() != null)) {
					// Fill attributes.
					for (int labelIndex = 1; labelIndex < labelsLine.size(); labelIndex++) {
						//
						String label = labelsLine.get(labelIndex);

						//
						String value;
						if (StringUtils.equals(label, BIRTH_ORDER_LABEL)) {
							//
							if (individual.getBirthOrder() == null) {
								//
								value = null;

							} else {
								//
								value = String.valueOf(individual.getBirthOrder());
							}
						} else {
							//
							value = individual.getAttributeValue(label);
						}

						//
						if (value == null) {
							//
							target.values().add("");

						} else {
							//
							target.values().add(value.replace('\t', ' '));
						}
					}
				}

				// Write attributes lines.
				// logger.debug("Write attribute: " +
				// LogHelper.toString(target));
				write(out, target);
			}
		}

		//
		out.println();

		// Write third block (relations).
		// =============================
		logger.debug("Write relations.");
		IURTXTFile.writeRelations(out, source.relationModels(), source.relations());

		// Write fourth block (geography).
		// =============================
		logger.debug("Write geography.");
		if (source.getGeography() != null) {
			GEOTXTFile.writeGeography(out, source.getGeography());
		}
	}
}
