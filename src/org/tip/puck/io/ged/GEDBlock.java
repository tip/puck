package org.tip.puck.io.ged;

import java.util.ArrayList;

/**
 * This class represents a line of individual from a TXT file.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class GEDBlock extends ArrayList<GEDLine> {

	/**
	 * 
	 */
	public GEDBlock() {
		super();
	}
}
