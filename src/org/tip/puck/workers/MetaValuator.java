package org.tip.puck.workers;

import java.util.List;

import org.tip.puck.census.chains.Chain;
import org.tip.puck.census.workers.ChainValuator;
import org.tip.puck.geo.Geography;
import org.tip.puck.graphs.Node;
import org.tip.puck.graphs.workers.NodeValuator;
import org.tip.puck.net.Individual;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.workers.RelationValuator;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.workers.ClusterValuator;
import org.tip.puck.sequences.EgoSequence;
import org.tip.puck.util.Valuator;
import org.tip.puck.util.Value;

/**
 * 
 * @author TIP
 */
public class MetaValuator<E> extends Valuator<E> {
	
	
	/**
	 * 
	 * @param item
	 * @param label
	 * @return
	 */
	@Override
	public Value get(final E item, final String label, final Geography geography) {
		Value result;

		if (item instanceof Individual) {
			result = IndividualValuator.get((Individual) item, label, geography);
		} else if (item instanceof Chain) {
			result = ChainValuator.get((Chain) item, label);
		} else if (item instanceof Relation) {
			result = RelationValuator.get((Relation) item, label, geography);
		} else if (item instanceof Node<?>) {
			result = NodeValuator.get((Node<?>) item, label);
		} else if (item instanceof Cluster<?>) {
			result = ClusterValuator.get((Cluster<?>) item, label, geography);
		} else if (item instanceof EgoSequence){
			result = IndividualValuator.get(((EgoSequence) item).getEgo(), label, geography);
		} else {
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public List<String> getAttributeLabels() {
		// TODO Auto-generated method stub
		return null;
	}
}
