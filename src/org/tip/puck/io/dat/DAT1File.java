package org.tip.puck.io.dat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.net.Family;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.net.UnionStatus;

/**
 * 
 * @author TIP
 */
public class DAT1File {

	// private static final Logger logger =
	// LoggerFactory.getLogger(DAT1File.class);
	public static final String DEFAULT_CHARSET_NAME = "UTF-8";
	public static final int MAX_LINE_SIZE = 1024;;

	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file) throws PuckException {
		Net result;

		result = load(file, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file, final String charsetName) throws PuckException {
		Net result;

		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), charsetName));
			result = read(in);

			result.setLabel(file.getName());

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException exception) {
				// logger.warn("Not managed error.");
				exception.printStackTrace();
			}
		}

		//
		return result;
	}

	/**
	 * Reads a net from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static Net read(final BufferedReader in) throws PuckException {
		Net result;

		result = new Net();

		boolean ended = false;
		while (!ended) {
			//

			Individual source = readIndividualLine(in);
			if (source == null) {
				ended = true;
			} else {
				result.individuals().put(source);
			}
		}
		ended = false;
		while (!ended) {
			//

			Family source = readFamilyLine(in);
			if (source == null) {
				ended = true;
			} else {
				result.families().put(source);
			}
		}

		// logger.debug("Done.");

		//
		return result;
	}

	/**
	 * Reads a line of individual data from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of individual data or null.
	 * 
	 * @throws PuckException
	 */
	public static Family readFamilyLine(final BufferedReader in) throws PuckException {
		Family result;

		try {
			//
			in.mark(MAX_LINE_SIZE);
			String line = readNotEmptyLine(in);
			boolean stop = false;
			while (stop == false) {
				in.mark(10000);
				String addLine = readNotEmptyLine(in);
				// System.out.println(addLine);
				if (addLine == null || (addLine.length() > 1 && addLine.substring(0, 2).equals("M$"))) {
					in.reset();
					stop = true;
				} else {
					line += addLine;
				}
			}

			if (line == "" || line == null) {
				result = null;
			} else {
				line = line.replace("S$", "/S$");
				String[] tokens = line.split("\\/");
				if (tokens.length > 0) {
					int id = Integer.parseInt(tokens[0].substring(2));
					result = new Family(id);
					result.setUnionStatus(UnionStatus.MARRIED);
				} else {
					result = null;
				}
				if (tokens.length > 2) {
					result.setAttribute("MARR_DATE", tokens[1]);
				}

				if (tokens.length > 2) {
					String pref = "";
					for (int i = 2; i < tokens.length; i++) {
						if (tokens[i].length() > 1 && tokens[i].substring(0, 2).equals("S$")) {
							tokens[i] = tokens[i].substring(2);
							if (pref == "") {
								pref = "HUSB_";
							} else {
								pref = "WIFE_";
							}
						}
						String[] values = tokens[i].split("\\=");
						result.setAttribute(pref + values[0], values[1]);
					}
				}
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading individual line.");
		}

		//
		return result;
	}

	/**
	 * Reads a line of individual data from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of individual data or null.
	 * 
	 * @throws PuckException
	 */
	public static Individual readIndividualLine(final BufferedReader in) throws PuckException {
		Individual result;

		try {
			//
			in.mark(MAX_LINE_SIZE);
			String line = readNotEmptyLine(in);
			boolean stop = false;
			while (stop == false) {
				in.mark(10000);
				String addLine = readNotEmptyLine(in);
				// System.out.println(addLine);
				if (addLine == null || (addLine.length() > 1 && (addLine.substring(0, 2).equals("B$") || addLine.substring(0, 2).equals("M$")))) {
					in.reset();
					stop = true;
					if ((addLine.length() > 1 && addLine.substring(0, 2).equals("M$"))) {
						line = null;
					}
				} else {
					line += addLine;
				}
			}

			if (line == "" || line == null) {
				result = null;
			} else {
				String[] tokens = line.split("\\/");

				if (tokens.length > 0) {
					int id = Integer.parseInt(tokens[0].substring(2));
					result = new Individual(id);
				} else {
					result = null;
				}
				if (tokens.length > 1) {
					result.setAttribute("BIRT_DATE", tokens[1]);
				}
				if (tokens.length > 2) {
					result.setAttribute("BAP_DATE", tokens[2]);
				}
				if (tokens.length > 3) {
					for (int i = 3; i < tokens.length; i++) {
						String[] values = tokens[i].split("\\=");
						result.setAttribute(values[0], values[1]);
						if (values[0].equals("prenom")) {
							result.setName(values[1]);
						}
						if (values[0].equals("nomnom")) {
							result.setName(result.getName() + " / " + values[1]);
						}
					}
				}
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading individual line.");
		}

		//
		return result;
	}

	/**
	 * Reads a not empty line from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a not empty line or null.
	 * 
	 * @throws PuckException
	 */
	public static String readNotEmptyLine(final BufferedReader in) throws PuckException {
		String result;

		try {
			boolean ended = false;
			result = null;
			while (!ended) {
				//
				String line = in.readLine();

				if (line == null) {
					ended = true;
					result = null;
				} else if (StringUtils.isNotBlank(line)) {
					ended = true;
					result = line;
				}
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading line.");
		}

		//
		return result;
	}
}
