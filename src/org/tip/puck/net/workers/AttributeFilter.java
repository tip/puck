package org.tip.puck.net.workers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 
 * @author TIP
 */
public class AttributeFilter {

	public enum Mode {
		NONE,
		CLEAN_BLANK,
		CLEAN,
		REMOVE,
		REPLACE_BY_BLANK,
		FORCE_TO_BLANK,
		ANONYMIZE_BY_NUMBERING,
		ANONYMIZE_BY_CONSISTENT_NUMBERING,
		ANONYMIZE_BY_GENDER_AND_ID_HF,
		ANONYMIZE_BY_GENDER_AND_ID_MF,
		ANONYMIZE_BY_FIRST_NAME,
		ANONYMIZE_BY_LAST_NAME,
		REDUCE_DATE,
		ANONYMIZE_DATE,
		TRIM_VALUE,
		UPPERCASE_VALUE,
		LOWERCASE_VALUE,
		CAPITALIZE_VALUE
	}

	public enum Scope {
		NONE,
		ALL,
		CORPUS,
		INDIVIDUALS,
		FAMILIES,
		RELATION,
		RELATIONS,
		ACTORS
	}

	private static final Logger logger = LoggerFactory.getLogger(AttributeFilter.class);

	private Scope scope;
	private String optionalRelationName;
	private String label;
	private Mode mode;

	/**
	 * 
	 * @param scope
	 * @param idLabel
	 * @param mode
	 */
	public AttributeFilter() {
		//
		this.scope = Scope.ALL;
		this.optionalRelationName = null;
		this.label = null;
		this.mode = Mode.NONE;
	}

	/**
	 * 
	 * @param scope
	 * @param label
	 * @param filter
	 */
	public AttributeFilter(final Scope scope, final String label, final Mode mode) {
		//
		if (scope == null) {
			this.scope = Scope.NONE;
		} else {
			this.scope = scope;
		}
		this.label = label;
		if (mode == null) {
			this.mode = Mode.NONE;
		} else {
			this.mode = mode;
		}
	}

	/**
	 * 
	 * @param optionalRelationName
	 * @param label
	 * @param filter
	 */
	public AttributeFilter(final String optionalRelationName, final String label, final Mode mode) {
		//
		this.scope = Scope.RELATION;
		this.optionalRelationName = optionalRelationName;
		this.label = label;
		if (mode == null) {
			this.mode = Mode.NONE;
		} else {
			this.mode = mode;
		}
	}

	public String getLabel() {
		return this.label;
	}

	public Mode getMode() {
		return this.mode;
	}

	public String getOptionalRelationName() {
		return this.optionalRelationName;
	}

	public Scope getScope() {
		return this.scope;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public void setMode(final Mode mode) {
		if (mode == null) {
			this.mode = Mode.NONE;
		} else {
			this.mode = mode;
		}
	}

	public void setOptionalRelationName(final String optionalRelationName) {
		this.optionalRelationName = optionalRelationName;
	}

	public void setScope(final Scope scope) {
		if (scope == null) {
			this.scope = Scope.NONE;
		} else {
			this.scope = scope;
		}
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		//
		if ((this.getMode() == null) || (this.getMode() == AttributeFilter.Mode.NONE) || (this.getScope() == null)
				|| (this.getScope() == AttributeFilter.Scope.NONE)) {
			//
			result = null;

		} else {
			//
			String scopeLabel;
			if (this.getScope() == AttributeFilter.Scope.RELATION) {
				scopeLabel = this.getOptionalRelationName();
			} else {
				scopeLabel = this.getScope().name().toLowerCase();
			}

			//
			result = String.format("%s.%s=%s", scopeLabel, this.getLabel(), this.getMode().name().toLowerCase());
		}

		//
		return result;
	}

}
