package org.tip.puck.cli;

import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.tip.puck.alliancenets.AllianceGen;
import org.tip.puck.alliancenets.AllianceNet;
import org.tip.puck.alliancenets.TopologicalIndices;


public class PruneAllianceGen extends Command {

    @Override
    public boolean run(CommandLine cline) {
        if(!cline.hasOption("inet")) {
            setErrorMessage("input network file must be specified");
            return false;
        }
        
        if(!cline.hasOption("prg")) {
            setErrorMessage("generator program file must be specified");
            return false;
        }
        
        if(!cline.hasOption("oprg")) {
            setErrorMessage("generator output program file must be specified");
            return false;
        }
        
        String netfile = cline.getOptionValue("inet");
        String prgFile = cline.getOptionValue("prg");
        String oprgFile = cline.getOptionValue("oprg");
        
        System.out.println("target net: " + netfile);
        
        AllianceNet allianceNet = AllianceNet.load(netfile);
        AllianceGen gen = new AllianceGen(allianceNet.getNodeCount(), allianceNet.getEdgeCount(), new TopologicalIndices(allianceNet));
        try {
            gen.loadProgs(prgFile);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        gen.run();
        gen.dynPruning();
        
        try {
            gen.writeProgs(oprgFile);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return true;
    }
}