package oldcore.random;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeMap;

import oldcore.trash.DistributionType;
import oldcore.trash.Matrix;

import umontreal.iro.lecuyer.randvar.ParetoGen;
import umontreal.iro.lecuyer.randvar.PowerGen;
import umontreal.iro.lecuyer.randvar.RandomVariateGen;
import umontreal.iro.lecuyer.rng.RandomStream;
import umontreal.iro.lecuyer.rng.WELL607;

/**
 * A class of random matrices used for simulation (both apriori and aposteriori). This class requires ssj as an external library
 * <p> A central part of the ANR-SimPa project
 * Needs classes of the ssj library
 * @author Klaus Hamberger
 * @since version 0.9, 10-08-07 (methods with this date were transferred from the formerly used simulation routine Kiss 0.1)
 */
public class RandomMatrix extends Matrix {
	
	/**
	 * an option for running the simulation with comments (for internal control)
	 */
	private boolean comment = false;
	/**
	 * a random variate generator
	 */
	private RandomVariateGen rvg;
	/**
	 * an index of available rows
	 */
	private ArrayList<Integer> rowIndex;
	/**
	 * an index of available columns
	 */
	private ArrayList<Integer> columnIndex;
	/**
	 * an array of remaining row and column sums
	 */
	private int[][] countdown;
	/**
	 * the distribution type
	 */
	private DistributionType dist;

	
	/**
	 * constructs a random matrix for given vertices, arcs, and distribution types
	 * <p> "A priori" simulation
	 * @param vertices the number of vertices
	 * @param arcs the number of arcs
	 * @param dist the distribution type
	 * @see gui.screens.MatrixSimulationScreen#getRelinkingStatistics(int,int)
	 * @since 10-08-08
	 */
	public RandomMatrix(int vertices, int arcs, DistributionType dist, int factor) {
		super(vertices);
		sum = arcs;
		rows = new int[vertices];
		columns = new int[vertices];
		this.dist = dist;
		if (dist!=DistributionType.FREE) {
			if (dist!=DistributionType.BERNOULLI) setRandomVariateGenerator(dist, vertices, factor);
			fill(rows,dist);
			fill(columns,dist);
			setMap();
		}
		fill();
	}

	/**
	 * constructs a random matrix by shuffling of another matrix
	 * <p> "A posteriori" simulation
	 * @param m the matrix to be shuffled
	 * @see matrices.Matrix#shuffle()
	 * @since 10-08-07
	 */
/*	public RandomMatrix(Matrix m) {
		super(m.getRowDim());
		sum = m.getSum();
		map = m.map;
		t = m.t;
		fill();
	}*/
	
	/**
	 * constructs a random matrix by shuffling or permuting another matrix
	 * <p> "A posteriori" simulation
	 * @param m the matrix to be shuffled or permuted
	 * @param exactSums true if row and column sums are to be exactly preserved (permutation), false otherwise (shuffling)
	 * @see matrices.Matrix#permute(exact)
	 * @since 10-08-07, modif 10-08-09
	 */
	public RandomMatrix(Matrix m, boolean exactSums) {
		super(m.getRowDim());
		sum = m.getSum();
		index = m.index;
		if (exactSums){
			rows = m.rows;
			columns = m.columns;
			initialize();
			redistribute();
		} else {
			map = m.map;
//			t = m.t;
			fill();
		}
		if (!comment) return;
		comment("The resulting matrix");
		for (int i=0;i<matrix.length;i++){
			String s="";
			for (int j=0;j<matrix.length;j++){
				s=s+matrix[i][j]+"\t";
			}
			comment(s);
		}
	}
	
	
	/**
	 * prints out a comment of the construction (for internal control) if the "commented" version is chosen
	 * @param s the comment
	 * @since 10-08-08
	 */
	private void comment(String s){
		if (comment) System.out.println(s);
	}
	
	/**
	 * draws a random couple (from available rows and columns), adds an arc and adjusts the parameters for remaining row and column sums and available rows and columns
	 */
	private void draw(){
		int a = (int)(Math.random()*rowIndex.size());
		int b = (int)(Math.random()*columnIndex.size());
		int i = rowIndex.get(a);
		int j = columnIndex.get(b);
		matrix[i][j]++;
		countdown[0][i]--;
		countdown[1][j]--;
		if (countdown[0][i]==0) rowIndex.remove(a);
		if (countdown[1][j]==0) columnIndex.remove(b);
	}
	
	/**
	 * fills a matrix of given dimensions with random values according to the chosen distribution
	 * @since 10-08-08
	 */
	private void fill() {
		for (int k=0;k<sum;k++){
			int[] i = new int[2];
			if (map==null){  //free distribution (no row and col sum constraints)
				i = new int[]{(int)(Math.random()*getRowDim()),(int)(Math.random()*getColDim())};
			} else { //distribution for given row and col sums
				int d = (int)(Math.random()*sum*sum);
				i = map.get(map.ceilingKey(d));
//				System.out.println("arc "+(1+k)+":\tnumber "+d+"\tnextUpperBound "+map.ceilingKey(d)+"\tcell "+Arrays.toString(i)+"\t-> "+matrix[i[0]][i[1]]);
				
			}
			augment(i[0],i[1]);
//			matrix[i[0]][i[1]]++;
//			comment("arc "+(1+k)+":\tnumber "+d+"\tnextUpperBound "+map.ceilingKey(d)+"\tcell "+Arrays.toString(i)+"\t-> "+matrix[i[0]][i[1]]);
		}
	}
	
	public void augment(int i, int j){
		matrix[i][j]++; 
		if (dist!=null) return;
		rows[i]++;
		columns[j]++;
	}
	
	/**
	 * fills a vector of given lenght with random values according to a chosen distribution
	 * <p> used for constructing row and column sum vectors
	 * @param a the vector
	 * @param dist the distirbution type
	 * @since 10-08-08
	 */
	private void fill (int[] a, DistributionType dist){
		int n = a.length;
		if (dist == DistributionType.BERNOULLI) {
			for (int k=0;k<sum;k++){
				int i = (int)(n*Math.random());
				a[i]++;
			}
		} else {
			for (int i=0;i<n;i++){
				a[i]=(int)(rvg.nextDouble());
			}
		}
	}
	
	/**
	 * sets the row and column indices and the initial row and column sums
	 */
	private void initialize() {
		rowIndex = new ArrayList<Integer>();
		columnIndex = new ArrayList<Integer>();
		countdown = new int[2][getRowDim()];
		for (int i=0;i<getRowDim();i++){
			if (rows[i]+columns[i]<2) continue;
			if (rows[i]>0) rowIndex.add(i);
			if (columns[i]>0) columnIndex.add(i);
			countdown[0][i]=rows[i];
			countdown[1][i]=columns[i];
		}
	}
	
	/**
	 * assigns arcs to row and column couples, preserving initial row and column sums 
	 */
	private void redistribute(){
		while (rowIndex.size()>0 && columnIndex.size()>0){
//		for (int i=0;i<sum;i++){
			draw();
		}
	}
	
	
	/**
	 * sets the random variate generator
	 * @param dist the distribution type
	 * @param vertices the number of vertices
	 * @param factor the power law exponent
	 * @since 10-08-08
	 */
	private void setRandomVariateGenerator(DistributionType dist, int vertices, int factor){
		RandomStream s = new WELL607();
		switch(dist){
			case POWER_NONSTANDARD: {
				double b = (sum/vertices)*(factor+1)/factor;
				rvg = new PowerGen(s,0,b,factor);
			}
			case POWER_STANDARD: {
				rvg = new ParetoGen(s,-1.0-factor,1.0);
			}
		}
	}
	



}
