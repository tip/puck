package org.tip.puckgui.views.mas;

/**
 * 
 * @author TIP
 */
public class Divorce2WeightFactor implements WeightFactor {

	private final Type type = Type.DIVORCE2;
	private double femaleProbability;
	private double maleProbability;
	private double bothProbability;

	/**
	 * 
	 */
	public Divorce2WeightFactor() {
		this.femaleProbability = 0.01;
		this.maleProbability = 0.01;
		this.bothProbability = 0.01;
	}

	public double getBothProbability() {
		return bothProbability;
	}

	public double getFemaleProbability() {
		return femaleProbability;
	}

	public double getMaleProbability() {
		return maleProbability;
	}

	@Override
	public Type getType() {
		return type;
	}

	public void setBothProbability(final double bothProbability) {
		this.bothProbability = bothProbability;
	}

	public void setFemaleProbability(final double femaleProbability) {
		this.femaleProbability = femaleProbability;
	}

	public void setMaleProbability(final double maleProbability) {
		this.maleProbability = maleProbability;
	}
}
