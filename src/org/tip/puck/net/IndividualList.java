package org.tip.puck.net;

import java.util.ArrayList;
import java.util.Collections;

import org.tip.puck.net.IndividualComparator.Sorting;

/**
 * 
 * @author TIP
 */
public class IndividualList extends ArrayList<Individual> {

	private static final long serialVersionUID = -4492829824044062064L;

	/**
	 * 
	 */
	public IndividualList() {
		super();
	}

	/**
	 * 
	 */
	public IndividualList(final IndividualList source) {
		super(source);
	}

	/**
	 * 
	 * @return
	 */
	public void sorted(final Sorting sorting) {
		//
		Collections.sort(this, new IndividualComparator(sorting));
	}

	/**
	 * 
	 */
	@Override
	public Individual[] toArray() {
		Individual[] result;

		result = new Individual[this.size()];
		int count = 0;
		for (Individual individual : this) {
			//
			result[count] = individual;
			count += 1;
		}

		//
		return result;
	}
}
