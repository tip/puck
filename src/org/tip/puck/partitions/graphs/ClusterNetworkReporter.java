package org.tip.puck.partitions.graphs;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.census.chains.Chain;
import org.tip.puck.evo.EvoGen;
import org.tip.puck.geo.Geography;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.random.RandomGraphReporter;
import org.tip.puck.graphs.workers.GraphReporter;
import org.tip.puck.graphs.workers.GraphUtils;
import org.tip.puck.matrix.MatrixStatistics;
import org.tip.puck.matrix.MatrixStatistics.DistributionObject;
import org.tip.puck.net.Individual;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.graphs.ClusterNetworkUtils.AllianceType;
import org.tip.puck.partitions.graphs.ClusterNetworkUtils.LineType;
import org.tip.puck.report.Report;
import org.tip.puck.report.ReportRawData;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.util.Chronometer;
import org.tip.puck.util.PuckUtils;
import org.tip.puck.util.ToolBox;
import org.tip.puckgui.views.GenerateRulesCriteria;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class ClusterNetworkReporter {
	private static final Logger logger = LoggerFactory.getLogger(ClusterNetworkReporter.class);

	/**
	 * Default list uses for Alliance Network Pajek export. In futur, user will
	 * choose which paremeter export.
	 */
	public static StringList ALLIANCE_NETWORK_PARTITION_LABELS = new StringList().append("DEGREE", "STRENGTH", "ID", "SIZE", "INDEGREE", "OUTDEGREE",
			"INSTRENGTH", "OUTSTRENGTH", "LOOPSTRENGTH");

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * 
	 * @throws PuckException
	 */
	public static Report reportAllianceNetwork(final Segmentation source, final File currentCorpus, final String label, final AllianceType allianceType,
			final LineType lineType, final int minimalNumberOfLinks, final int minimalNodeStrength, final double minimalLinkWeight) throws PuckException {
		Report result;

		if ((source == null) || (currentCorpus == null) || (label == null) || (lineType == null)) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
		} else {
			//
			Chronometer chrono = new Chronometer();

			// Compute.
			Graph<Cluster<Individual>> graph;
			switch (allianceType) {
				case WIFE_HUSBAND:
					graph = ClusterNetworkUtils.createAllianceNetwork(source, label, lineType);
				break;
				case SISTER_BROTHER:
					graph = ClusterNetworkUtils.createSiblingNetwork(source, label, lineType);
				break;
				case PARENT_CHILD:
					graph = ClusterNetworkUtils.createParentChildNetwork(source, label, lineType);
				break;
				default:
					graph = null;
			}
			graph = GraphUtils.reduce(graph, minimalNumberOfLinks, minimalNodeStrength, minimalLinkWeight);

			// Build general report.
			result = new Report();
			result.setTitle("Alliance Network Report.");
			result.setOrigin("ClusterNetworkReporter.reportInterMarriages()");
			result.setTarget(source.getLabel());

			//
			result.inputs().add("Label", label);
			result.inputs().add("Alliance type", allianceType.toString());
			result.inputs().add("Line type", lineType.toString());
			result.inputs().add("Minimal number of links (node degree)", minimalNumberOfLinks);
			result.inputs().add("Minimal number of alliances per node (node strenght)", minimalNodeStrength);
			result.inputs().add("Minimal number of alliances per link (link weight)", minimalLinkWeight);

			// Build analysis report.
			Report report1 = new Report("Analysis");
			// report1.outputs().appendln(GraphReporter.getGraphStatsAsLine(graph));
			report1.outputs().appendln(GraphReporter.getGraphStats(graph));
			result.outputs().append(report1);

			// Build matrix report.
			Report report2 = new Report("Matrix");
			report2.outputs().appendln(GraphReporter.getMatrixStrings(graph));
			result.outputs().append(report2);

			// Build couples report.
			Report report3 = new Report("Couples");
			report3.outputs().appendln(GraphReporter.getCoupleList(graph, allianceType));
			result.outputs().append(report3);

			// Build sortable report.
			Report report4 = new Report("Sortable list");
			report4.outputs().appendln(GraphReporter.getSortableList(graph, source.getGeography()));
			result.outputs().append(report4);

			// Build sides report.
			Report reportSides = new Report("Sides");
			reportSides.outputs().appendln(GraphReporter.getSides(graph));
			result.outputs().append(reportSides);

			// Build Pajek data.
			{
				ReportRawData rawData = new ReportRawData("Export to Pajek", "Pajek", "paj", new File(currentCorpus.getParent() + File.separator
						+ ToolBox.clean(graph.getLabel()) + ".paj"));

				rawData.setData(PuckUtils.writePajekNetwork(graph, ALLIANCE_NETWORK_PARTITION_LABELS).toString());
				result.outputs().append(rawData);
			}

			// Build Pajek data (edge version).
			{
				ReportRawData rawData = new ReportRawData("Export to Pajek (edge version)", "Pajek", "paj", new File(currentCorpus.getParent() + File.separator
						+ ToolBox.clean(graph.getLabel()) + ".paj"));

				rawData.setData(PuckUtils.writePajekNetwork(GraphUtils.reduceArcToEdge(graph), ALLIANCE_NETWORK_PARTITION_LABELS).toString());
				result.outputs().append(rawData);
			}

			// Build Dat data.
			ReportRawData datData = new ReportRawData("Export Matrix to Dat File", "Dat", "dat", new File(currentCorpus.getParent() + File.separator
					+ ToolBox.clean(graph.getLabel()) + ".dat"));
			datData.setData(GraphReporter.getRawMatrixStrings(graph).toString());
			result.outputs().append(datData);

			//
			result.outputs().append(" ");

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportAnalysis(final Graph<Cluster<Individual>> source) throws PuckException {
		Report result;

		if (source == null) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");

		} else {
			//
			Chronometer chrono = new Chronometer();

			// Build general report.
			result = new Report();
			result.setTitle("Analysis");
			result.setOrigin("ClusterNetworkReporter.reportAnalysis()");
			result.setTarget(source.getLabel());

			//
			// report1.outputs().appendln(GraphReporter.getGraphStatsAsLine(graph));
			result.outputs().appendln(GraphReporter.getGraphStats(source));

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportCircuitIntersectionMatrix(final Graph<Cluster<Chain>> source) throws PuckException {
		Report result;
		
		if (source == null) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");

		} else {
			//
			Chronometer chrono = new Chronometer();

			// Build general report.
			result = new Report("Matrix");
			result.setOrigin("ClusterNetworkReporter.reportMatrix()");
			result.setTarget(source.getLabel());

			// Build matrix report.
			result.outputs().appendln(GraphReporter.getMatrixStrings(source));

			// Add non intersecting marriage numbers
			String circuitString = "Circuits\t";
			String netMarriageString = "Net Marriages\t";
			int netMarriageSum = 0;
			int circuitSum = 0;
			for (Integer[] numbers : ClusterNetworkUtils.getCircuitAndNetMarriageNumbers(source).values()) {
				circuitString += numbers[0] + "\t";
				netMarriageString += numbers[1] + "\t";
				circuitSum += numbers[0];
				netMarriageSum += numbers[1];
			}
			result.outputs().appendln(circuitString + circuitSum);
			result.outputs().appendln(netMarriageString + netMarriageSum);
			//

			result.setTimeSpent(chrono.stop().interval());
		}
		
		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportCouples(final Graph<Cluster<Individual>> source, final AllianceType allianceType) throws PuckException {
		Report result;

		if (source == null) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");

		} else {
			//
			Chronometer chrono = new Chronometer();

			// Build general report.
			result = new Report("Couples");
			result.setOrigin("ClusterNetworkReporter.reportCouples()");
			result.setTarget(source.getLabel());

			//
			result.inputs().add("Type", allianceType.toString());

			//
			result.outputs().appendln(GraphReporter.getCoupleList(source, allianceType));

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportDistributions(final Graph<Cluster<Individual>> source) throws PuckException {
		Report result;

		if (source == null) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");

		} else {
			//
			Chronometer chrono = new Chronometer();

			// Build general report.
			result = new Report("Distribution");
			result.setOrigin("ClusterNetworkReporter.reportDistributions()");
			result.setTarget(source.getLabel());

			//
			Map<Double, Double> weightsDistribution = MatrixStatistics.getDistribution(DistributionObject.WEIGHTS, source);
			result.outputs().appendln("Weights Distributions");
			result.outputs().appendln(RandomGraphReporter.createDistributionChart("Weights Distributions", "Weights", weightsDistribution));
			result.outputs().appendln(RandomGraphReporter.createDistributionTable("Weights Distributions", "Weights", weightsDistribution));
			result.outputs().appendln();

			//
			Map<Double, Double> forcesDistribution = MatrixStatistics.getDistribution(DistributionObject.STRENGTHS, source);
			result.outputs().appendln("Forces Distributions");
			result.outputs().appendln(RandomGraphReporter.createDistributionChart("Forces Distributions", "Forces", forcesDistribution));
			result.outputs().appendln(RandomGraphReporter.createDistributionTable("Forces Distributions", "Forces", forcesDistribution));
			result.outputs().appendln();

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportFlowNetwork(final Segmentation source, final File currentCorpus, final String relationLabel, final String sourceLabel,
			final String targetLabel, final String labelParameter, final int minimalNumberOfLinks) throws PuckException {
		Report result;

		if ((source == null) || (currentCorpus == null) || (sourceLabel == null) || (targetLabel == null)) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");

		} else {
			//
			Chronometer chrono = new Chronometer();

			// Compute.
			Graph graph;

			if (StringUtils.isBlank(relationLabel)) {
				graph = ClusterNetworkUtils.createFlowNetwork(source, sourceLabel, targetLabel, LineType.WEIGHTED_ARC);
			} else {
				graph = ClusterNetworkUtils.createFlowNetwork(source, relationLabel, sourceLabel, targetLabel, labelParameter, LineType.WEIGHTED_ARC);
			}
			logger.debug("FlowNetwork raw: {} nodes", graph.nodeCount());
			graph = GraphUtils.reduce(graph, minimalNumberOfLinks, 0, 0);
			logger.debug("FlowNetwork reduced: {} nodes", graph.nodeCount());

			// Build Pajek data.
			ReportRawData rawData = new ReportRawData("Export to Pajek", "Pajek", "paj", new File(currentCorpus.getParent() + File.separator
					+ ToolBox.clean(graph.getLabel()) + ".paj"));

			StringList partitionLabels = new StringList();
			partitionLabels.append("DEGREE");
			partitionLabels.append("STRENGTH");
			partitionLabels.append("ID");
			partitionLabels.append("SIZE");
			partitionLabels.append("INDEGREE");
			partitionLabels.append("OUTDEGREE");
			partitionLabels.append("INSTRENGTH");
			partitionLabels.append("OUTSTRENGTH");
			rawData.setData(PuckUtils.writePajekNetwork(graph, partitionLabels).toString());

			// Build general report.
			result = new Report();
			result.setTitle("Flow Network Report.");
			result.setOrigin("ClusterNetworkReport.reportFlowNetwork()");
			result.setTarget(source.getLabel());

			//
			result.inputs().add("Source Label", sourceLabel);
			result.inputs().add("Target Label", targetLabel);
			result.inputs().add("Minimal number of links", minimalNumberOfLinks);

			//
			// Build analysis report.
			Report report1 = new Report("Analysis");
			report1.outputs().appendln(GraphReporter.getGraphStats(graph));
			result.outputs().append(report1);

			// Build matrix report.
			Report report2 = new Report("Matrix");
			report2.outputs().appendln(GraphReporter.getMatrixStrings(graph));
			result.outputs().append(report2);
			//

			// Build couples report.
			Report report3 = new Report("Flows");
			if (StringUtils.isBlank(relationLabel)) {
				report3.outputs().appendln(GraphReporter.getIndividualFlowList(graph));
			} else {
				report3.outputs().appendln(GraphReporter.getRelationFlowList(graph));
			}
			result.outputs().append(report3);
			//

			// Build sortable report.
			Report report4 = new Report("Sortable list");
			// report4.outputs().appendln(GraphReporter.getSortableList(graph));
			result.outputs().append(report4);

			// Build Geography report.
			// Report reportGeo = new Report("Geography");
			// result.outputs().append(reportGeo);

			result.outputs().append(rawData);
			result.outputs().appendln(" ");

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static <E> Report reportGenerateRules(final Graph<E> source, final GenerateRulesCriteria criteria, final EvoGen target) throws PuckException {
		Report result;

		if (source == null) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");

		} else {
			//
			Chronometer chrono = new Chronometer();

			// Build general report.
			result = new Report("Generate Rules");
			result.setOrigin("ClusterNetworkReporter.reportGenerateRules()");
			result.setTarget(source.getLabel());

			//
			result.inputs().add("Generation count", criteria.getGenerationCount());

			//
			result.outputs().appendln(target.infoString());
			result.outputs().appendln(target.genInfoString());
			result.outputs().appendln(target.getBestGenerator().toString());

			try {
				result.outputs().appendln("Rule:");
				ByteArrayOutputStream buffer = new ByteArrayOutputStream(1024);
				OutputStreamWriter out = new OutputStreamWriter(buffer);
				target.getBestGenerator().getProgset().write(out, false);
				out.flush();
				result.outputs().appendln(buffer.toString());
			} catch (IOException exception) {
				result.outputs().appendln("ERROR reading the progset.");
			}

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static <E> Report reportMatrix(final Graph<Cluster<E>> source) throws PuckException {
		Report result;

		if (source == null) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");

		} else {
			//
			Chronometer chrono = new Chronometer();

			// Build general report.
			result = new Report("Matrix");
			result.setOrigin("ClusterNetworkReporter.reportMatrix()");
			result.setTarget(source.getLabel());

			// Build matrix report.
			result.outputs().appendln(GraphReporter.getMatrixStrings(source));

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportSides(final Graph<Cluster<Individual>> source) throws PuckException {
		Report result;

		if (source == null) {
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
		} else {
			result = new Report();

			Chronometer chrono = new Chronometer();

			// Build general report.
			result = new Report("Sides");
			result.setOrigin("ClusterNetworkReporter.reportSides()");
			result.setTarget(source.getLabel());

			// Build sides report.
			result.outputs().appendln(GraphReporter.getSides(source));

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportSortableList(final Graph<Cluster<Individual>> source, final Geography geography) throws PuckException {
		Report result;

		if (source == null) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");

		} else {
			//
			Chronometer chrono = new Chronometer();

			// Build general report.
			result = new Report("Sortable list");
			result.setOrigin("ClusterNetworkReporter.reportSortableList()");
			result.setTarget(source.getLabel());

			// Build sortable report.
			result.outputs().appendln(GraphReporter.getSortableList(source, geography));

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}
}
