package org.tip.puck.net;

/**
 * 
 * @author TIP
 */
public enum FiliationType {
	AGNATIC,
	UTERINE,
	COGNATIC,
	BILINEAR,
	IDENTITY,
	SPOUSE,
	VIRI,
	UXORI;

	/**
	 * 
	 * @return
	 */
	public FiliationType invert() {
		FiliationType result;

		switch (this) {
			case AGNATIC:
				result = UTERINE;
				break;
			case UTERINE:
				result = AGNATIC;
				break;
			case VIRI:
				result = UXORI;
				break;
			case UXORI:
				result = VIRI;
				break;
			default:
				result = this;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int toInt() {
		int result;

		switch (this) {
			case AGNATIC:
				result = 0;
			break;
			case UTERINE:
				result = 1;
			break;
			case COGNATIC:
				result = 2;
			break;
			default:
				result = -1;
		}

		//
		return result;
	}
	
	public boolean hasLinkingGender(Gender gender){
		boolean result;
		
		switch (this) {
		case AGNATIC:
			result = (gender == Gender.MALE);
		break;
		case UTERINE:
			result = (gender == Gender.FEMALE);
		break;
		case COGNATIC:
			result = (gender == Gender.MALE || gender == Gender.FEMALE);
		break;
		case VIRI:
			result = (gender == Gender.MALE);
		break;
		case UXORI:
			result = (gender == Gender.FEMALE);
		break;
		default:
			result = false;
	}

	//
	return result;
		
		
	}
	
	public boolean residential (){
		return (this == VIRI || this == UXORI);
	}
	
}
