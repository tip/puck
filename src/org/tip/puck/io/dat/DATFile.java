package org.tip.puck.io.dat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.workers.GraphReporter;
import org.tip.puck.net.Individual;
import org.tip.puck.partitions.Cluster;

/**
 * 
 * @author TIP
 */
public class DATFile {

	private static final Logger logger = LoggerFactory.getLogger(DATFile.class);;
	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	/**
	 * 
	 * TODO optimize with StrintListWriter.
	 * 
	 */
	public static <E> void exportToDAT(final Graph<E> source, final String fileName) throws PuckException {
		PrintWriter out = null;
		try {
			//
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(new File(fileName)), DEFAULT_CHARSET_NAME));

			//
			String datString = GraphReporter.getRawMatrixStrings(source).toString();

			//
			out.print(datString);

		} catch (UnsupportedEncodingException exception) {
			//
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + fileName + "]");

		} catch (FileNotFoundException exception) {
			//
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + fileName + "]");

		} finally {
			//
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * Loads a GED file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Graph<Cluster<Individual>> load(final File file) throws PuckException {
		Graph<Cluster<Individual>> result;

		result = load(file, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Loads a GED file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Graph<Cluster<Individual>> load(final File file, final String charsetName) throws PuckException {
		Graph<Cluster<Individual>> result;

		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), charsetName));
			result = read(in);

			result.setLabel(file.getName());

		} catch (UnsupportedEncodingException exception) {
			//
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");

		} catch (FileNotFoundException exception) {
			//
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");

		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException exception) {
				logger.warn("Not managed error.");
				exception.printStackTrace();
			}
		}

		//
		return result;
	}

	/**
	 * Reads a net from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static Graph<Cluster<Individual>> read(final BufferedReader in) throws PuckException {
		Graph<Cluster<Individual>> result;

		try {
			// Create nodes.
			{
				in.mark(100000);
				String line = in.readLine();
				String[] tokens = line.split("\t");
				result = new Graph<Cluster<Individual>>(0, Array.getLength(tokens));
				for (int tokenIndex = 0; tokenIndex < Array.getLength(tokens); tokenIndex++) {
					result.addNode(tokenIndex + 1, null); // First node id is 1.
				}
			}

			// Create edges.
			in.reset();
			int lineCount = 0;
			boolean ended = false;
			while (!ended) {
				String line = in.readLine();
				if (line == null) {
					ended = true;
				} else {
					lineCount += 1;

					String[] tokens = line.split("\t");
					for (int tokenIndex = 0; tokenIndex < tokens.length; tokenIndex++) {
						double weight = Double.parseDouble(tokens[tokenIndex]);
						if (weight != 0) {
							result.addArcWeight(lineCount, tokenIndex + 1, weight);
						}
					}
				}
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading DAT line.");
		}

		//
		return result;
	}
}
