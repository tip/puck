package org.tip.puck.util;

/**
 * 
 * @author TIP
 */
public class IntWithMax {

	private int value;
	private int max;

	/**
	 * 
	 */
	public IntWithMax() {
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public int challengeMax(final int value) {
		int result;

		result = setMax(Math.max(this.max, value));

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int inc() {
		int result;

		this.value += 1;
		result = this.value;

		//
		return result;
	}

	public int max() {
		return max;
	}

	public void set(final int value) {
		this.value = value;
	}

	public int setMax(final int max) {
		int result;

		this.max = max;

		result = this.max;

		//
		return result;
	}

	public int value() {
		return value;
	}
}
