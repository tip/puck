package oldcore.trash;


/**
 * IndividualEntry adapter class.
 * 
 * @author Edoardo Savoia
 */
public class Individual implements IIndividual {

/*	IndividualEntry vertex;
	private IFamily parentFamily;
	List<IIndividual> parents  = new ArrayList<IIndividual>();
	List<IFamily> personalFamilies = new ArrayList<IFamily>();
	private List<Attribute> attributes;

	
	public Individual(final IndividualEntry vertex) {
//		this.vertex = vertex;
	}
	
	public Vertex(final IndividualEntry vertex, DataModel model) {
		this.vertex = vertex;
        parents = new ArrayList<IVertex>();
		if (vertex.getParents()>-1) {
			parentFamily = new Family(model.families.get(vertex.getParents()),model);
    		if (parentFamily!=null) {
    			parents.add(parentFamily.getHusband());
    			parents.add(parentFamily.getWife());
    		}
		}
		personalFamilies = new ArrayList<IFamily>();
		if (vertex.getPartners()!=null){
			for (int f : vertex.getPartners()){
				personalFamilies.add(new Family(model.families.get(f),model));
			}
		}
	}

	@Override
	public Gender getGender() {
		return vertex.getGender();
	}

	@Override
	public int getId() {
		return vertex.getId();
	}
	
	public int compareTo (IIndividual otherVertex){
		return new Integer(getId()).compareTo(((Individual)otherVertex).getId());
	}

	// should be changed 
	public IndividualProperty getKey(final String s) {
		if (s.equals("BIRTH") || s.equals("BIRT")) {
			return IndividualProperty.BIRTH;
		}
		if (s.equals("DEATH") || s.equals("DEAT")) {
			return IndividualProperty.DEATH;
		}
		if (s.equals("OCCU")) {
			return IndividualProperty.OCCU;
		}
		if (s.equals("CLAN")) {
			return IndividualProperty.CLAN;
		}
		return null;
	}
	
	public List<IIndividual> getParents(){
		return parents;
	}

	@Override
	public String getName() {
		return vertex.getName();
	}
	
	public String getFirstName(){
		String name = getName();
		int idx = name.indexOf("/");
		if (idx==-1) return name;
		return name.substring(0,idx);
	}
	
	public IFamily getParentFamily() {
		return parentFamily;
	}

	public List<IFamily> getPersonalFamilies() {
		return personalFamilies;
	}

	//Change - integrate all kinds of endogenous properties
	@Override
	public Object getPropertyValue(final String propertyLabel) {
		if (propertyLabel.equals("FIRSTN")) return getFirstName();
		IndividualProperty key = getKey(propertyLabel);
		return vertex.getKey(key);
	}

	// K
	@Override
	public boolean isSingle() {
		return (vertex.getPartners() == null);
	}
	
	public boolean equals (Object obj){
		return obj!=null && getId()==((IIndividual)obj).getId();
	}
	
	public List<Attribute> getAttributes(){
		return attributes;
	}
	
    public Object getAlters(String relationLabel){
        Object result = null;
        if (relationLabel.equals("PARENT")){
/*        		IFamily f = getParentFamily();
                List<IVertex> parents = new ArrayList<IVertex>();
        		if (f!=null) {
        			parents.add(f.getHusband());
        			parents.add(f.getWife());
        		}
                result = parents;
        } else if (relationLabel.equals("FATHER")){
        		result = parents.get(0);
//        		IFamily f = getParentFamily();
//       		if (f!=null) result = f.getHusband();
        } else if (relationLabel.equals("MOTHER")){
    			result = parents.get(1);
//        		IFamily f = getParentFamily();
//               if (f!=null)result = f.getWife();
        } else if (relationLabel.equals("SPOUSE")){
                List<IIndividual> spouses = new ArrayList<IIndividual>();
                if (getPersonalFamilies() !=null){
                	for (IFamily k : getPersonalFamilies()){
                		IIndividual sp = k.getSpouse(this);
                		if (sp!=null) spouses.add(sp);
                	}
                	result = spouses;
                }
        } else {
            	List<IIndividual> alters = new ArrayList<IIndividual>();
            	if (getAttributes()!=null){
            		for (Attribute attribute : getAttributes()){
            			if (attribute.getLabel().equals(relationLabel)){
            				IIndividual alter = (IIndividual)attribute.getAlter(this);
            				if (alter!=null) alters.add(alter);
            			}
            			result = alters;
            		}
            	}
        }
        return result;
    }*/	

	// Temp
	/**
	 * sets an attribute with given value and label
	 * 
	 * @param s
	 *            the array of attribute value
	 * @param labels
	 *            the array of attribute label
	 */
/*	public void setAttribute(final String label, final String value) {
		IndividualProperty key = getKey(label);
		if (key != null) {
			vertex.setKey(key, value);
		}
	}
		


	// Temp
	/**
	 * adds a series of attributes with given values, labels and position
	 * indices to the vertex
	 * 
	 * @param s
	 *            the array of attribute values
	 * @param labels
	 *            the array of attribute labels
	 * @param param
	 *            the array of value position indices
	 * @see io.read.TxtReader#updateAttributes(String[])
	 */
/*	public void setAttributes(final String[] values, final String[] labels) {
		for (int i = 1; i < values.length; i++) {
			IndividualProperty key = getKey(labels[i]);
			if (key != null) {
				vertex.setKey(key, values[i]);
			}
		}
	}

	// K
/*	public void setGender(final int i) {
		Gender g = Gender.fromInt(i);
		vertex.setGender(g);
	}
	

    //should probably be in Datemodel.vertex rather than in Network.vertex...
	@Override
	public void setAttribute(Attribute attribute) {
		if (attributes==null) attributes = new ArrayList<Attribute>();
		attributes.add(attribute);
	}

	public void setParentFamily(final IFamily parentFamily) {
		vertex.setParents(parentFamily);
	}*/

}
