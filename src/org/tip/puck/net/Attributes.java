package org.tip.puck.net;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.util.StringList;
import fr.devinsy.util.StringSet;

/**
 * 
 * @author TIP
 */
public class Attributes extends HashMap<String, Attribute> implements Iterable<Attribute> {

	private static final long serialVersionUID = 8456469741805779474L;

	/**
	 *
	 */
	public Attributes() {
		super();
	}

	/**
	 *
	 */
	public Attributes(final Attributes source) {
		super();

		if (source != null) {
			for (Attribute attribute : source.values()) {
				this.put(attribute.getLabel(), new Attribute(attribute.getLabel(), attribute.getValue()));
			}
		}
	}

	/**
	 *
	 */
	public Attributes(final int capacity) {

		super(capacity);
	}

	/**
	 * 
	 * @param attribute
	 */
	public void add(final Attribute attribute) {

		put(attribute);
	}

	/**
	 * 
	 * @param source
	 */
	public void addAll(final Attributes source) {

		if (source != null) {
			//
			for (Attribute attribute : source) {
				//
				this.put(attribute.getLabel(), attribute.getValue());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.HashMap#clone()
	 */
	@Override
	public Attributes clone() {
		Attributes result;

		result = new Attributes();
		for (Attribute attribute : this.values()) {
			//
			this.put(attribute.getLabel(), new Attribute(attribute.getLabel(), attribute.getValue()));
		}

		//
		return result;
	}

	/**
	 * 
	 * 
	 * @param label
	 * @param value
	 * @return
	 */
	public boolean contains(final String label, final String value) {
		boolean result;

		Attribute attribute = get(label);

		if (attribute == null) {

			result = false;

		} else if (StringUtils.equals(value, attribute.getValue())) {

			result = true;

		} else {

			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public Attribute getIgnoreCase(final String key) {
		Attribute result;

		if (key == null) {
			//
			result = null;

		} else {
			//
			boolean ended = false;
			Iterator<Attribute> iterator = iterator();
			result = null;
			while (!ended) {
				//
				if (iterator.hasNext()) {
					//
					Attribute attribute = iterator.next();
					if (StringUtils.equalsIgnoreCase(StringUtils.trim(attribute.getLabel()), key)) {
						//
						ended = true;
						result = attribute;
					}

				} else {
					//
					ended = true;
					result = null;
				}
			}
		}

		//
		return result;
	}
	

	/**
	 * 
	 */
	public String getValue(final String label) {
		String result;

		Attribute attribute = this.get(label);
		if (attribute == null) {
			//
			result = null;

		} else {
			//
			result = attribute.getValue();
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public StringSet getValues() {
		StringSet result;

		result = new StringSet();

		for (Attribute attribute : this) {
			//
			result.add(attribute.getValue());
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Attribute> iterator() {
		Iterator<Attribute> result;

		result = this.values().iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public StringList labels() {
		StringList result;

		//
		result = new StringList();

		//
		for (String label : this.keySet()) {
			//
			result.add(label);
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public void put(final Attribute attribute) {

		if ((attribute != null) && (attribute.getLabel() != null)) {
			//
			this.put(attribute.getLabel(), attribute);
		}
	}

	/**
	 * 
	 * @param label
	 * @param value
	 */
	public Attribute put(final String label, final String value) {
		Attribute result;

		result = this.get(label);
		if (result == null) {
			//
			result = new Attribute(label, value);
			this.put(label, result);

		} else {
			//
			result.setValue(value);
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public void removeBlankAttributes() {
		//
		Iterator<Attribute> iterator = iterator();
		while (iterator.hasNext()) {
			//
			Attribute attribute = iterator.next();

			//
			if (attribute.isBlank()) {
				//
				iterator.remove();
			}
		}
	}

	/**
	 * 
	 * @param attribute
	 * @param newLabel
	 */
	public void rename(final Attribute attribute, final String newLabel) {

		if ((attribute != null) && (this.containsValue(attribute)) && (StringUtils.isNotBlank(newLabel))) {
			//
			this.remove(attribute.getLabel());
			attribute.setLabel(newLabel);
			this.add(attribute);
		}
	}

	/**
	 * 
	 * @param pattern
	 * @return
	 */
	public Attributes searchByLabel(final String pattern) {
		Attributes result;

		result = new Attributes();

		for (Attribute attribute : this) {
			//
			if (attribute.getLabel().matches(pattern)) {
				//
				result.add(attribute);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Attribute> toList() {
		List<Attribute> result;

		result = new ArrayList<Attribute>(values());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Attribute> toSortedList() {
		List<Attribute> result;

		result = toList();
		Collections.sort(result, new AttributeComparator());

		//
		return result;
	}

}
