package org.tip.puck.geo;

import java.text.Collator;
import java.util.Comparator;

/**
 * 
 */
public class PlaceComparator implements Comparator<Place> {

	public enum Sorting {
		DEFAULT,
		TOPONYM
	}

	private Sorting sorting;

	/**
	 * 
	 * @param sorting
	 */
	public PlaceComparator() {
		//
		this.sorting = Sorting.DEFAULT;
	}

	/**
	 * 
	 * @param sorting
	 */
	public PlaceComparator(final Sorting sorting) {
		//
		this.sorting = sorting;
	}

	/**
	 * 
	 */
	@Override
	public int compare(final Place alpha, final Place bravo) {
		int result;

		//
		result = compare(alpha, bravo, this.sorting);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public String getLabel(final Place source) {
		String result;

		if (source == null) {
			result = null;
		} else {
			result = source.getToponym();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param alpha
	 * @param bravo
	 * @param sorting
	 * @return
	 */
	public static int compare(final Place alpha, final Place bravo, final Sorting sorting) {
		int result;

		if ((alpha == null) && (bravo == null)) {
			//
			result = 0;

		} else if (alpha == null) {
			//
			result = -1;

		} else if (bravo == null) {
			//
			result = +1;

		} else {
			//
			switch (sorting) {
				case DEFAULT:
				case TOPONYM:
					//
					result = compare(alpha.getToponym(), bravo.getToponym());
				break;

				default:
					result = 0;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param alpha
	 * @param bravo
	 * @return
	 */
	public static int compare(final String alpha, final String bravo) {
		int result;

		//
		if ((alpha == null) && (bravo == null)) {
			//
			result = 0;

		} else if (alpha == null) {
			//
			result = -1;

		} else if (bravo == null) {
			//
			result = +1;

		} else {
			//
			result = Collator.getInstance().compare(alpha, bravo);
		}

		//
		return result;
	}
}
