package org.tip.puck.geo.io;

import java.io.File;
import java.security.InvalidParameterException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.geo.Geography;

/**
 * This class represents a TXT File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class GEOFile {

	private static final Logger logger = LoggerFactory.getLogger(GEOFile.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	/**
	 * Loads a IUR file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Geography load(final File source) throws PuckException {
		Geography result;

		result = load(source, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Loads a IUR file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Geography load(final File source, final String charsetName) throws PuckException {
		Geography result;

		//
		if (source == null) {
			//
			throw new InvalidParameterException("Null parameter");

		} else {
			//
			String fileName = source.getName().toLowerCase();

			//
			if (fileName.endsWith(".ods")) {
				//
				result = GEOODSFile.load(source);

			} else if (StringUtils.endsWithAny(fileName, ".txt", ".csv")) {
				//
				result = GEOTXTFile.load(source, charsetName);

			} else if (fileName.endsWith(".xls")) {
				//
				result = GEOXLSFile.load(source);

			} else {
				//
				throw new InvalidParameterException("Unknown extension.");
			}
		}

		//
		return result;
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File target, final Geography source) throws PuckException {
		//
		if (source == null) {
			//
			throw new InvalidParameterException("Null parameter");

		} else {
			//
			String fileName = target.getName().toLowerCase();

			//
			if (fileName.endsWith(".ods")) {
				//
				GEOODSFile.save(target, source);

			} else if (StringUtils.endsWithAny(fileName, ".txt", ".csv")) {
				//
				GEOTXTFile.save(target, source);

			} else if (fileName.endsWith(".xls")) {
				//
				GEOXLSFile.save(target, source);

			} else {
				//
				throw new InvalidParameterException("Unknown extension.");
			}
		}
	}
}
