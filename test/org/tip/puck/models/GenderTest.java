package org.tip.puck.models;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.tip.puck.net.Gender;

/**
 * 
 * @author TIP
 */
public class GenderTest {

	/**
     */
	@Test
	public void testCanMarryFromString() {

		Gender source = Gender.valueOf('m');
		Assertions.assertThat(source.canMarry(Gender.FEMALE)).isTrue();
		Assertions.assertThat(source.canMarry(Gender.MALE)).isFalse();
		Assertions.assertThat(source.canMarry(Gender.UNKNOWN)).isTrue();

		source = Gender.valueOf('g');
		Assertions.assertThat(source.canMarry(Gender.FEMALE)).isTrue();
		Assertions.assertThat(source.canMarry(Gender.MALE)).isTrue();
		Assertions.assertThat(source.canMarry(Gender.UNKNOWN)).isTrue();

		source = Gender.valueOf('f');
		Assertions.assertThat(source.canMarry(Gender.FEMALE)).isFalse();
		Assertions.assertThat(source.canMarry(Gender.MALE)).isTrue();
		Assertions.assertThat(source.canMarry(Gender.UNKNOWN)).isTrue();

		source = Gender.valueOf('X');
		Assertions.assertThat(source.canMarry(Gender.FEMALE)).isTrue();
		Assertions.assertThat(source.canMarry(Gender.MALE)).isTrue();
		Assertions.assertThat(source.canMarry(Gender.UNKNOWN)).isTrue();
	}
}
