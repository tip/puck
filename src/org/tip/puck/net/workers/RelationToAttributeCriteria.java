package org.tip.puck.net.workers;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.workers.AttributeWorker.CaseOption;

/**
 * 
 * @author TIP
 */
public class RelationToAttributeCriteria {

	private AttributeWorker.Scope scope;
	private String optionalRelationName;
	private String relationAttributeLabel;
	private String dateLabel;
	private AttributeWorker.CaseOption caseOption;

	/**
	 * 
	 */
	public RelationToAttributeCriteria() {
		this.scope = AttributeWorker.Scope.NONE;
		this.optionalRelationName = null;
		this.relationAttributeLabel = null;
		this.dateLabel = null;
		this.caseOption = AttributeWorker.CaseOption.CASE_SENSITIVE;
	}

	public AttributeWorker.CaseOption getCaseOption() {
		return this.caseOption;
	}

	public String getRelationAttributeLabel() {
		return this.relationAttributeLabel;
	}

	public String getDateLabel() {
		return this.dateLabel;
	}

	public String getOptionalRelationName() {
		return this.optionalRelationName;
	}

	public AttributeWorker.Scope getScope() {
		return this.scope;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isCaseSensitive() {
		boolean result;

		if (this.caseOption == CaseOption.CASE_SENSITIVE) {
			//
			result = true;

		} else {
			//
			result = false;
		}

		//
		return result;
	}

	public void setCaseOption(final AttributeWorker.CaseOption caseOption) {
		this.caseOption = caseOption;
	}

	public void setRelationAttributeLabel(final String label) {
		this.relationAttributeLabel = label;
	}

	public void setDateLabel(final String newLabel) {
		this.dateLabel = newLabel;
	}

	public void setOptionalRelationName(final String optionalRelationName) {
		this.optionalRelationName = optionalRelationName;
	}

	public void setScope(final AttributeWorker.Scope scope) {
		this.scope = scope;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public static boolean isNotValid(final RelationToAttributeCriteria criteria) {
		boolean result;

		result = !isValid(criteria);

		//
		return result;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public static boolean isValid(final RelationToAttributeCriteria criteria) {
		boolean result;

		if ((criteria == null) || (criteria.getScope() == null) || (criteria.getScope() == AttributeWorker.Scope.NONE) || (criteria.getRelationAttributeLabel() == null)
				|| (StringUtils.isBlank(criteria.getDateLabel()))) {
			//
			result = false;

		} else {
			//
			result = true;
		}

		//
		return result;
	}

}
