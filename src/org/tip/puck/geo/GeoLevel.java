package org.tip.puck.geo;

import org.tip.puck.geo.GeoLevel;

import fr.devinsy.util.StringList;

/**
 *
 */
public enum GeoLevel {

	INTERCONTINENTAL,
	CONTINENT,
	COUNTRY,
	DEPARTMENT,
	TOWNSHIP,
	TOWN,
	QUARTER,
	SUBQUARTER,

	TRANSNATIONAL,
	TRANSREGIONAL,
	REGIONAL,
	LOCAL,
	UNDEFINED;

	/**
	 * 
	 * This method builds a string list of enums.
	 * 
	 * @return a string list containing enum labels.
	 */
	public static StringList toStringList() {
		StringList result;

		//
		result = new StringList();

		//
		for (GeoLevel geoLevel : values()) {
			//
			result.add(geoLevel.name());
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public GeoLevel dynamic() {
		GeoLevel result;

		if (this == INTERCONTINENTAL || this == CONTINENT) {
			result = TRANSNATIONAL;
		} else if (this == COUNTRY) {
			result = TRANSREGIONAL;
		} else if (this == DEPARTMENT || this == TOWNSHIP) {
			result = REGIONAL;
		} else {
			result = LOCAL;
		}
		//
		return result;
	}
}