package org.tip.puck.mas;

import org.tip.puck.net.Gender;
import org.tip.puck.util.Distributions;


/**
 * @author Telmo Menezes
 *
 */
public class WFNormalAge implements WeightFactor {

    Gender parent;
    double mean;
    double stdev;
    
    public WFNormalAge(Gender parent, double mean, double stdev) {
        this.parent = parent;
        this.mean = mean;
        this.stdev = stdev;
    }
    
    public String factorName(){
    	return "normalage";
    }
    
    @Override
    public double factor(Agent mother, Agent father) {
        double age = 0;
        if (parent == Gender.FEMALE) {
            age = mother.getAge();
        }
        else {
            age = father.getAge();
        }
        
        //System.out.println("age: " + age);
        
        return Distributions.normal(age, mean, stdev);
    }
    
    @Override
    public boolean appliesToMarriage() {return true;}
    
    @Override
    public boolean appliesToDivorce() {return true;}
}
