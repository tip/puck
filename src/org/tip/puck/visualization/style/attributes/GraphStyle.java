package org.tip.puck.visualization.style.attributes;


/**
 *
 * @author Edoardo Savoia
 */
public enum GraphStyle {
    directed,mixed,undirected;
}
