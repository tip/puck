package org.tip.puck.io.ged;

/**
 * This class represents a line of individual from a TXT file.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class GEDLine {
	private int level;
	private String refPrefix;
	private Integer refId;
	private String ref;
	private String refPostfix;
	private String tag;
	private String valuePrefix;
	private Integer valueId;
	private String value;
	private String valuePostfix;

	/**
	 * 
	 */
	public GEDLine(final int level, final String refPrefix, final Integer refId, final String refPostfix, final String tag, final char value) {
		this.level = level;
		this.refPrefix = refPrefix;
		this.refId = refId;
		this.refPostfix = refPostfix;
		if ((refPrefix == null) || (refId == null) || (refPostfix == null)) {
			this.ref = null;
		} else {
			this.ref = "@" + refPrefix + refId + refPostfix + "@";
		}
		this.tag = tag;
		this.valuePrefix = null;
		this.valueId = null;
		this.value = String.valueOf(value);
	}

	/**
	 * 
	 */
	public GEDLine(final int level, final String refPrefix, final Integer refId, final String refPostfix, final String tag, final String value) {
		this.level = level;
		this.refPrefix = refPrefix;
		this.refId = refId;
		this.refPostfix = refPostfix;
		if ((refPrefix == null) || (refId == null) || (refPostfix == null)) {
			this.ref = null;
		} else {
			this.ref = "@" + refPrefix + refId + refPostfix + "@";
		}
		this.tag = tag;
		this.valuePrefix = null;
		this.valueId = null;
		this.value = value;
	}

	/**
	 * 
	 */
	public GEDLine(final int level, final String refPrefix, final Integer refId, final String refPostfix, final String tag, final String valuePrefix,
			final Integer valueId, final String valuePostfix) {
		this.level = level;
		this.refPrefix = refPrefix;
		this.refId = refId;
		if ((refPrefix == null) || (refId == null)) {
			this.ref = null;
		} else {
			this.ref = "@" + refPrefix + refId + refPostfix + "@";
		}
		this.tag = tag;
		this.valuePrefix = valuePrefix;
		this.valueId = valueId;
		this.valuePostfix = valuePostfix;
		this.value = "@" + valuePrefix + valueId + valuePostfix + "@";
	}

	public int getLevel() {
		return level;
	}

	public String getRef() {
		return ref;
	}

	public int getRefId() {
		return refId;
	}

	public String getRefPostfix() {
		return refPostfix;
	}

	public String getRefPrefix() {
		return refPrefix;
	}

	/**
	 * 
	 * @return
	 */
	public boolean hasRef() {
		boolean result;

		result = (this.ref != null);

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean hasTag() {
		boolean result;

		result = (this.tag != null);

		//
		return result;
	}
	

	/**
	 * 
	 * @return
	 */
	public boolean hasValue() {
		boolean result;

		result = (this.value != null);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isValueId() {
		boolean result;

		result = (this.valueId != null);

		//
		return result;
	}

	public void setLevel(final int level) {
		this.level = level;
	}

	public void setRef(final String ref) {
		this.ref = ref;
	}

	public void setRefId(final int refId) {
		this.refId = refId;
	}

	public void setRefPostfix(final String refPostfix) {
		this.refPostfix = refPostfix;
	}

	public void setRefPrefix(final String refPrefix) {
		this.refPrefix = refPrefix;
	}

	public void setTag(final String tag) {
		this.tag = tag;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	public void setValueId(final Integer valueId) {
		this.valueId = valueId;
	}

	public void setValuePostfix(final String valuePostfix) {
		this.valuePostfix = valuePostfix;
	}

	public void setValuePrefix(final String valuePrefix) {
		this.valuePrefix = valuePrefix;
	}

	public String tag() {
		return tag;
	}

	public String value() {
		return value;
	}
	
	public String trimmedValue(){
		String result;
		if (value!=null){
			result = value.trim();
		} else {
			result = null;
		}
		//
		return result;
	}

	public Integer valueId() {
		return valueId;
	}

	public String valuePostfix() {
		return valuePostfix;
	}

	public String valuePrefix() {
		return valuePrefix;
	}

}
