package org.tip.puck.net;

import java.util.Comparator;

import org.tip.puck.util.PuckUtils;

/**
 * 
 * @author TIP
 * 
 */
public class AttributeComparator implements Comparator<Attribute> {
	/**
	 * 
	 */
	@Override
	public int compare(final Attribute alpha, final Attribute bravo) {
		int result;

		//
		String alphaLabel;
		if (alpha == null) {
			//
			alphaLabel = null;

		} else {
			//
			alphaLabel = alpha.getLabel();
		}

		//
		String bravoLabel;
		if (bravo == null) {
			//
			bravoLabel = null;

		} else {
			//
			bravoLabel = bravo.getLabel();
		}

		//
		result = PuckUtils.compare(alphaLabel, bravoLabel);

		//
		return result;
	}
}
