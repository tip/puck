package org.tip.puck.cli;


import java.io.BufferedWriter;
import java.io.FileWriter;

import org.apache.commons.cli.CommandLine;
import org.tip.puck.evo.EvoGen;
import org.tip.puck.evo.EvoStrategy;
import org.tip.puck.alliancenets.EvoAllianceGen;
import org.tip.puck.alliancenets.AllianceNet;


public class EvoAlliance extends Command {

    @Override
    public boolean run(CommandLine cline) {
        // TODO: make configurable
        int generations = 10000;
        
        if(!cline.hasOption("inet")) {
            setErrorMessage("input network file must be specified");
            return false;
        }
        
        if(!cline.hasOption("odir")) {
            setErrorMessage("output directory must be specified");
            return false;
        }
        
        String netfile = cline.getOptionValue("inet");
        String outdir = cline.getOptionValue("odir");
        
        EvoAllianceGen callbacks = new EvoAllianceGen(AllianceNet.load(netfile), outdir);
        EvoStrategy popGen = new EvoStrategy(1, 1, 1);
        //Tournament popGen = new Tournament(25, 2, 0.1, 0.7);
        EvoGen evo = new EvoGen(popGen, callbacks, generations);
        
        System.out.println("target net: " + netfile);
        System.out.println(evo.infoString());
        
        // write experiment params to file
        try {
            FileWriter fstream = new FileWriter(outdir + "/params.txt");
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(evo.infoString());
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
        evo.run();
        
        return true;
    }
}