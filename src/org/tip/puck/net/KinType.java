package org.tip.puck.net;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author TIP
 */
public enum KinType {
	
	CHILD(Gender.UNKNOWN,"Ch"),
	SPOUSE(Gender.UNKNOWN,"Sp"),
	PARENT(Gender.UNKNOWN,"Pa"),
	SIBLING(Gender.UNKNOWN,"Sb"),
	WIFE(Gender.FEMALE,"W"),
	HUSBAND(Gender.MALE,"H"),
	MOTHER(Gender.FEMALE,"M"),
	FATHER(Gender.MALE,"F"),
	DAUGHTER(Gender.FEMALE,"D"),
	SON(Gender.MALE,"S"),
	BROTHER(Gender.MALE,"B"),
	SISTER(Gender.FEMALE,"Z"),
	SELF,
	UNKNOWN;

	
	private Gender gender;
	private String letter;

	/**
	 * 
	 */
	private KinType() {
		this.gender = Gender.UNKNOWN;
	}
	
	public static List<KinType> basicTypes (){
		List<KinType> result;
		
		result = new ArrayList<KinType>();
		result.add(CHILD);
		result.add(SPOUSE);
		result.add(PARENT);
		
		//
		return result;
		
	}
	
	public static List<KinType> basicTypesWithSiblings (){
		List<KinType> result;
		
		result = new ArrayList<KinType>();
		result.add(CHILD);
		result.add(PARENT);
		result.add(SIBLING);
		result.add(SPOUSE);
		
		//
		return result;
		
	}


	/**
	 * 
	 * @param gender
	 */
	private KinType(final Gender gender, final String letter) {
		this.gender = gender;
		this.letter = letter;
	}
	
	/**
	 * 
	 * @return
	 */
	public Gender getGender() {
		return gender;
	}
	
	public String getLetter(){
		return letter;
	}
	
	public static KinType valueOfLetter(final String letter){
		KinType result;

		result = null;
		
		for (KinType kinType : values()){
			if (kinType.getLetter().equals(letter)){
				result = kinType;
				break;
			}
		}
		//
		return result;
	}
	
	/**
	 * 
	 * @param i
	 * @return
	 * 
	 */
	public static KinType valueOf(final int i) {
		KinType result;

		switch (i) {
			case -1:
				result = CHILD;
			break;
			case 0:
				result = SPOUSE;
			break;
			case 1:
				result = PARENT;
			break;
			default:
				result = null;
		}

		//
		return result;

	}

	
	public String signature(){
		String result;
		
		switch (this) {
			case CHILD:
				result = "children";
			break;
			case SPOUSE:
				result = "spouses";
			break;
			case PARENT:
				result = "parents";
			break;
			default:
				result = null;
		}

		//
		return result;
		
	}
	
	public KinType inverse(){
		KinType result;
		
		switch (this) {
			case CHILD:
				result = PARENT;
			break;
			case PARENT:
				result = CHILD;
			break;
			default:
				result = this;
		}

		//
		return result;
		
	}
	
	
	
	public String toString (Gender gender){
		return gendered(gender).toString();
	}
	
	public KinType gendered (Gender gender){
		KinType result;
		
		switch (gender){
		case MALE:
			switch (this) {
			case CHILD:
				result = SON;
			break;
			case SPOUSE:
				result = HUSBAND;
			break;
			case PARENT:
				result = FATHER;
			break;
			default:
				result = null;
		}
			break;
		case FEMALE:
			switch (this) {
			case CHILD:
				result = DAUGHTER;
			break;
			case SPOUSE:
				result = WIFE;
			break;
			case PARENT:
				result = MOTHER;
			break;
			default:
				result = null;
		}
			break;
		default:
			result = null;
		}

		//
		return result;
		
	}
	
	/**
	 * 
	 * @return
	 */
	public KinType ungendered() {
		KinType result;

		switch (this) {
			case SPOUSE:
			case WIFE:
			case HUSBAND:
				result = SPOUSE;
				break;
			case PARENT:
			case MOTHER:
			case FATHER:
				result = PARENT;
				break;
			case CHILD:
			case DAUGHTER:
			case SON:
				result = CHILD;
				break;
			case SIBLING:
			case BROTHER:
			case SISTER:
				result = SIBLING;
				break;
			default:
				result = this;
		}

		//
		return result;
	}
	
	public Integer toInt() {
		Integer result;
		
		switch (this) {
		case CHILD:
			result = -1;
		break;
		case SPOUSE:
			result = 0;
		break;
		case PARENT:
			result = 1;
		break;
		default:
			result = null;
		}
		//
		return result;
	}
	

	
	public Integer genDistance() {
		Integer result;
		
		switch (this) {
		case CHILD:
			result = -1;
		break;
		case SPOUSE:
			result = 0;
		break;
		case PARENT:
			result = 1;
		break;
		case SIBLING:
			result = 0;
		break;
		default:
			result = null;
		}
		//
		return result;
	}
	

}
