package oldcore.trash;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.tip.puck.census.chains.Chain;
import org.tip.puck.census.chains.ChainMaker;
import org.tip.puck.net.relations.Relation;

public class CircuitFinder  {
	
	Net net;
	StatsPanel panel;
	List<Integer> ids;
	long time;
	HashMap<Integer, Map<Integer, Integer>> consanguines;
	Map<String[],Collection<Chain>> sorter;
	
	   /**
	    * the array of maximal canonic degrees of consanguineous components for given order
	    */
	   private int[] deg;

	   /**
	    * the array of maximal higher-order canonical degrees for given order
	    * <p> used for pivotchain-construction
	    */
	   private int[] maxDeg; 

	
	public static
	<T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
	  List<T> list = new ArrayList<T>(c);
	  java.util.Collections.sort(list);
	  return list;
	}
	
	
	void begin (){
		time = System.currentTimeMillis();
	}
	
	/**
	 * calculates the time spent and reports it
	 */
	void end (){
		end (null);
	}

	/**
	 * calculates the time spent and reports it with a performance message
	 * @param s the performance message
	 */
	void end (String s){
		time = System.currentTimeMillis()-time;
		if (s!=null) write(s);
		write("Time spent :"+time+" ms");
	}
	
	private static int length(String[] s){
		int i=0;
		for (String t : s){
			i = i+t.length();
		}
		return i;
	}
	
	/**
	 * gets the characteristic number corresponding to a chain of gender numbers
	 * @return the characteristic number
	 * @see chains.OldRing#setVector()
	 */
	 public int number (String str) {
		 int n = 0;
		 for (int i=0;i<str.length();i++) {
			 int a = 1;
			 if (str.charAt(i)=='F') a = 2;
//			 int b = 1; Negative sign in case of unknown apical gender
//			 if (str.charAt(i)=='X') b = -1;
			 n = n + new Double(Math.pow(2,i)*a).intValue();
		 }
		 return n;
	 }

	
	public CircuitFinder (Net net, StatsPanel panel){
		
		final class ChainComparator implements Comparator<String[]> {
			public int compare(String[] s1, String[] s2){
				if (s1.length!=s2.length) return new Integer(s1.length).compareTo(s2.length);
				int t1 = length(s1); 
				int t2 = length(s2);
				if (t1!=t2) return new Integer(t1).compareTo(t2);
				for (int i=0;i<s1.length;i++){
					if (s1[i].length()!=s2[i].length()) return new Integer(s2[i].length()).compareTo(s1[i].length());
				}
/*				for (int i=0;i<s1.length;i++){
					if (!s1[i].equals(s2[i])) return new String(s2[i]).compareTo(s1[i]);
				}*/
				for (int i=0;i<s1.length;i++){
					int k1 = number(s1[i]);
					int k2 = number(s2[i]);
					if (k1!=k2) return new Integer(k1).compareTo(k2);
				}
				return 0;
			}
		}
		
		this.net = net;
		this.panel = panel;
		ids = asSortedList(net.ids());
		
		deg = new int[]{3,2};              //should be parametrized by the user
		maxDeg = new int[deg.length];
		for (int i=0;i<deg.length;i++){
			maxDeg[i]=deg[i];
			for (int j=i+1;j<deg.length;j++) {
				if (deg[j]>maxDeg[i]) maxDeg[i]=deg[j];
			}
		}
		
		sorter = new TreeMap<String[],Collection<Chain>>(new ChainComparator());
	}

	
	private void write (String data){
		panel.populateText(data);
	}
	

	
	private List<Integer> getAlters(int id, Relation rel){
		return (List<Integer>)net.getAlters(id, rel);
	}
	
	private Collection<Integer> getAlters (int id, int pos){
		if (pos%2==0) return getAlters(id, Relation.SPOUSE);  //possibility to restrict according to relational properties (define getAlters(ego,rel,prop) method)
		return links(id,maxDeg[(pos+1)/2]);
	}


	
	//to complete
	/**
	 * @param id a potential starting point for circuit search
     * @return true if the individual does not belong to the search domain or does not fulfill the required conditions
	 */
	private boolean cannotPass (int id){
//		if (!net.contains(id)) return true; //and other condition for cluster affiliation, search on subnetworks etc.
//		if (net.get(id).isSingle()) return true; //and other conditions for personal status...
		return false;
	}

	//to complete
	/**
	 * @param chain a pivot chain
	 * @return true if the conditions for admissibility are fulfilled
	 */
	private boolean cannotPass (Chain chain){ //condition for cluster affiliation, search on subnetworks etc.
/*		for (int id : chain.elements) {
			if (net.contains(id)) return false;
		} */
		return false;
	}
	
	/**
	 * checks whether the addition of a new element to the chain violate the order condition for pivot chains of matrimonial circuits
	 * <p> 1. the element must not precede ego
	 * <p> 2. if the element is identical to ego then the last element (ego's second spouse in the chain) must not precede alter
	 * <p> 3. the element must not be identical to any element other than its immediate predecessor or ego
	 */
	private void findBases(int ego, Chain chain,Set<Integer> vertices, int rmax, int r) {
		completeCircuits(chain);
		if (r==rmax) return;
		vertices.add(ego);
		for (int alter : getAlters(ego,r)){
			if (cannotPass(alter)) continue;  // restriction condition for intermediate pivots
			if (alter<chain.getFirst()) continue; // no element must precede ego (avoidance of duplicates)
			if (alter==chain.getFirst() && ego<=chain.getId(1)) continue; //polygamous ego, 2nd vertex must be lower
			if (vertices.contains(alter) && alter!=ego && alter!=chain.getFirst()) continue; // no double occurence (but artificial repetition permitted)
			if (!chain.add(alter,0)) continue;
			findBases(alter,chain,vertices,rmax,r+1);
			chain.removeLast();
		}
		vertices.remove(ego);
	}
	
	  /** 
	   * gets all circuits with a given pivot chain (fills in the bridges)
	   * @param chain the pivot chain
	   */
	private void completeCircuits(Chain chain){
		int n = chain.size()+1;
		if (n%2==1) return;
		if (cannotPass(chain)) return;  // restriction condition for chains (if not already contained in the condition for intermediate pivots)
		List<Chain>[] br = new List[n/2];
		for (int i=0;i<n/2;i++){  //find Bridges for each couple
			int e = (n-1+2*i)%n;  
			int a = (n+2*i)%n;    
			br[i] = new ArrayList(findBridges(chain.getId(e), chain.getId(a), deg[i]));
			if (br[i].size()==0) {
				if (i>0) System.out.println("error in base : "+chain.baseSignature());// for i>0 br[i] cannot be empty
				return;  
			}
		}
		buildCircuits(new ArrayList<Chain>(), br);
	}
	
	private void buildCircuits(List<Chain> chains, List<Chain>[] br){
		if (chains.size()==br.length) {
			Chain circuit = ChainMaker.concatenate(chains, deg[chains.size()-1]);
			put(circuit);
			return;
		}
		for (Chain c : br[chains.size()]){
			System.out.println(c.subchains.get(0).size());

			chains.add(c);
			buildCircuits(chains,br);
			chains.remove(c);
		}
	}
	
	
	public void findCircuits(){
		
		begin();
		
		if (deg.length>1) setConsanguines(maxDeg[1]);
		for (int ego : ids){
			
/*			for (int alter: links(ego,2)){                        // A test for the consanguine map
				write(net.get(ego).getName()+"\t"+net.get(alter).getName());
			}*/
			
			if (cannotPass(ego)) return;  //filter for research domain and starting point status
			
			if (deg.length==1){  //only Consanguine Circuits
				for (int alter : getAlters(ego, Relation.SPOUSE)) {  //search for consanguineous circuits only
					HashSet<Chain> bridges = findBridges(ego,alter,12);
					for (Chain c : bridges){
						put(c);
					}
				}
			} else {
				findBases(ego, new Chain(ego), new HashSet<Integer>(), 2*deg.length-1, 0) ;
			}
			
		}
		end();
		write("");
		
		int i = 1;
		for (Collection<Chain> set : sorter.values()){
			boolean first = true;
			List<Chain> list = asSortedList(set);
			for (Chain c : list){
				if (first) {
					write(i+". "+c.alterSignature()+" ("+list.size()+")");
					write("");
					first = false;
				}
				write("\t"+c.coupleSignature(net)+"\t"+c.signature(0)); 
			}
			i++;
			write("");
		}
	}

	private HashSet<Chain> findBridges(int ego, int alter, int r){
		HashSet<Chain> bridges = new HashSet<Chain>();
		findBridges(ego,alter,bridges,new Chain(ego),new Chain(alter),new HashSet<Integer>(),r,r);
		return bridges;
	}

	private void findBridges(int ego, int alter, Set<Chain> bridges,Chain leftChain, Chain rightChain,Set<Integer> vertices, int rmax, int r) {
		findLeftBridges(ego,alter,bridges,leftChain, rightChain,vertices,rmax);
		if (r==0) return;
		if (ego==alter) return;
		vertices.add(alter);
		for (int newAlter : getAlters(alter, Relation.PARENT)){
			if (vertices.contains(newAlter)) continue;
			if (!rightChain.add(newAlter,1)) continue;
			findBridges(ego,newAlter,bridges,leftChain, rightChain,vertices,rmax,r-1);
			rightChain.removeLast();
		}
		vertices.remove(alter);
	}

	private void findLeftBridges(int ego, int alter, Set<Chain> bridges,Chain leftChain, Chain rightChain, Set<Integer> vertices, int r) {
		if (ego==alter) {
			Chain bridge = ChainMaker.concatenateInv(leftChain, rightChain);
			bridges.add(bridge);
			return;
		}
		if (r==0) return;
		vertices.add(ego);
		for (int newEgo : getAlters(ego, Relation.PARENT)){
			if (vertices.contains(newEgo)) continue;
			if (!leftChain.add(newEgo,1)) continue;
			findLeftBridges(newEgo,alter,bridges,leftChain,rightChain, vertices,r-1);
			leftChain.removeLast();
		}
		vertices.remove(ego);
	}
	
	private void setGender (Chain c){
		for (Link e : c){
			e.setGender(net);
		}
	}
	
	private static boolean add(Collection<Chain> set, Chain c){
		for (Chain e : set){
			if (e.equals(c)) return false;
		}
		return set.add(c);
	}
	
	private void put(Chain c){
		if (c==null) return;  //can happen in case of intersecting bridges
		setGender(c);
//		c.setSubChains();
		Chain cs = c.standard();
		String[] s = cs.subchains.toArray(); //cs.getSubChains(1);  
		if (!sorter.containsKey(s)) sorter.put(s, new HashSet<Chain>());
		add(sorter.get(s),cs);
	}
	

	/**
	 * sets a link of given degree between two vertices
	 * @param e the ID number of ego
	 * @param a the Id number of alter
	 * @param d the consanguineous distance (canonic degree)
	 */
	private static void set(HashMap<Integer, Map<Integer, Integer>> distMap, int e, int a, int d){
		if (distMap.get(e)==null) distMap.put(e,new HashMap<Integer, Integer>());
		Map<Integer,Integer> map = distMap.get(e);
		if (map.get(a)==null || d < map.get(a)) map.put(a,d);
	}

	
	private void setConsanguines (int max){
		
		boolean marriedOnly = true;  //parametrize
		
		HashMap<Integer, Map<Integer, Integer>> asc = new HashMap<Integer, Map<Integer, Integer>>();  // Map of linear distances
		for (int ego : net.ids()){
			createAscendantTies(asc,ego,ego,0,max);
		}
		
		consanguines = new HashMap<Integer, Map<Integer, Integer>>();  // Map of consanguineous distances
		for (int i : asc.keySet()){
			if (marriedOnly && isSingle(i)) continue; //and other restriction conditions
			Map<Integer,Integer> upMap = asc.get(i);
			for (int j : upMap.keySet()){
				int upDistance = upMap.get(j);
				if (upDistance<0) continue;  //the ascendants incl ego
				Map<Integer,Integer> downMap = asc.get(j);
				for (int k : downMap.keySet()){
					if (marriedOnly && isSingle(k)) continue;
					int downDistance = downMap.get(k); 
					if (downDistance > 0) continue;  //the ascendant's descendants incL. the own descendants and ego
					int u = Math.max(upDistance, -downDistance);  //the collateral distance
					set(consanguines,i,k,u);
				}
			}
		}
/*		for (int i : asc.keySet()){                   //fusion of linear and collateral distances - superfluous
			Map<Integer,Integer> linMap = asc.get(i);
			for (int j : linMap.keySet()){
				int u = Math.abs(linMap.get(j)); //the absolute linear distance
				set(cons, i, j, u);
			}
		}*/
		
	}
	
	/**
	 * creates the ascendant ties of a given ego vertex
	 * @param ego the ego vertex
	 * @param alter the current vertex
	 * @param k the current degree
	 * @param max the maximal degree
	 */
	private void createAscendantTies (HashMap<Integer, Map<Integer, Integer>> asc, int ego, int alter, int k, int max){
		set(asc,ego,alter,k);
		set(asc,alter,ego,-k);
		if (k>max) return;
		for (int newAlter: getAlters(alter, Relation.PARENT)) {
			if (newAlter>-1) createAscendantTies (asc,ego,newAlter,k+1,max);
		}
	}
	
	/**
	 * gets the ID number set of consanguineous relatives of given maximal degree
	 * @param ego the ID of ego
	 * @param d the maximal degree
	 * @see chains.NumberChain#links(Map,int)
	 */
	public Collection<Integer> links (int ego, int d){
		Set<Integer> set = new HashSet<Integer>();
		Map<Integer,Integer> cons = consanguines.get(ego);
		for (int j: cons.keySet()){
			if (cons.get(j)<=d) set.add(j);
		}
		return set;
	}
	
	private boolean isSingle(int id){
	    Individual get = net.get(id);
	    if (get == null) {
		get = net.get(id);
	    }
	    return get.isSingle();
	}
	
	
	
}
