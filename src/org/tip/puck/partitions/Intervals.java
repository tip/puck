package org.tip.puck.partitions;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.util.MathUtils;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class Intervals extends ArrayList<Interval> {
	private static final long serialVersionUID = 2789574687542101744L;
	private static final Logger logger = LoggerFactory.getLogger(Intervals.class);

	/**
	 * 
	 */
	public Intervals() {
		super();
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public Interval find(final Double value) {
		Interval result;

		if (value == null) {
			result = null;
		} else {
			result = null;
			boolean ended = false;
			int index = 0;
			while (!ended) {
				if (index < this.size()) {
					Interval interval = this.get(index);
					if (interval.matches(value)) {
						ended = true;
						result = interval;
					} else {
						index += 1;
					}
				} else {
					ended = true;
					result = null;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public String toBasicStepString() {
		String result;

		StringList buffer = new StringList();

		for (int intervalIndex = 0; intervalIndex < this.size(); intervalIndex++) {
			Interval interval = this.get(intervalIndex);
			buffer.append(MathUtils.toString(interval.getMin()));
			buffer.append(" ");
			if (intervalIndex == this.size() - 1) {
				buffer.append(MathUtils.toString(interval.getMax()));
			}
		}

		result = buffer.toString();

		//
		return result;
	}

	/**
	 * 
	 */
	public String toStepString() {
		String result;

		StringList buffer = new StringList();

		buffer.append("(");
		for (int intervalIndex = 0; intervalIndex < this.size(); intervalIndex++) {
			Interval interval = this.get(intervalIndex);
			buffer.append(MathUtils.toString(interval.getMin()));
			buffer.append(",");
			if (intervalIndex == this.size() - 1) {
				buffer.append(MathUtils.toString(interval.getMax()));
			}
		}
		buffer.append(")");

		result = buffer.toString();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		StringList buffer = new StringList();

		for (Interval interval : this) {
			buffer.append(interval.toString());
		}

		result = buffer.toString();

		//
		return result;
	}
}
