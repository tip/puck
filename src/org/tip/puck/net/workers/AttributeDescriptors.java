package org.tip.puck.net.workers;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.workers.AttributeDescriptorComparator.Sorting;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class AttributeDescriptors implements Iterable<AttributeDescriptor> {

	private ArrayList<AttributeDescriptor> attributeDescriptors;

	/**
	 * 
	 */
	public AttributeDescriptors() {
		this.attributeDescriptors = new ArrayList<AttributeDescriptor>();
	}

	/**
	 * 
	 */
	public AttributeDescriptors(final int initialCapacity) {
		this.attributeDescriptors = new ArrayList<AttributeDescriptor>(initialCapacity);
	}

	/**
	 * 
	 * @param log
	 */
	public void add(final AttributeDescriptor source) {
		//
		if (source == null) {
			//
			throw new IllegalArgumentException("source is null.");

		} else {
			//
			this.attributeDescriptors.add(source);
		}
	}

	/**
	 * 
	 * @param log
	 */
	public void addAll(final AttributeDescriptors source) {
		//
		if (source != null) {
			//
			for (AttributeDescriptor descriptor : source) {
				//
				this.attributeDescriptors.add(descriptor);
			}
		}
	}

	/**
	 * 
	 * @param log
	 */
	public void addAll(final Collection<AttributeDescriptor> source) {
		//
		if (source != null) {
			//
			for (AttributeDescriptor descriptor : source) {
				//
				this.attributeDescriptors.add(descriptor);
			}
		}
	}

	/**
	 * 
	 */
	public void clear() {
		this.attributeDescriptors.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public AttributeDescriptors copy() {
		AttributeDescriptors result;

		//
		result = new AttributeDescriptors(this.attributeDescriptors.size());

		//
		for (AttributeDescriptor comment : this.attributeDescriptors) {
			result.add(comment);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param scope
	 * @param optionalRelationName
	 * @return
	 */
	public AttributeDescriptors findByRelationModelName(final String relationName) {
		AttributeDescriptors result;

		//
		result = new AttributeDescriptors();

		//
		if (StringUtils.isNotBlank(relationName)) {
			//
			for (AttributeDescriptor descriptor : this.attributeDescriptors) {
				//
				if ((descriptor.getScope() == AttributeDescriptor.Scope.RELATION) && (StringUtils.equals(descriptor.getOptionalRelationName(), relationName))) {
					result.add(descriptor);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param scope
	 * @param optionalRelationName
	 * @return
	 */
	public AttributeDescriptors findByScope(final AttributeDescriptor.Scope scope) {
		AttributeDescriptors result;

		//
		result = new AttributeDescriptors();

		//
		for (AttributeDescriptor descriptor : this.attributeDescriptors) {
			if (descriptor.getScope() == scope) {
				result.add(descriptor);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public AttributeDescriptors first(final int targetCount) {
		AttributeDescriptors result;

		//
		result = new AttributeDescriptors(targetCount);

		//
		boolean ended = false;
		Iterator<AttributeDescriptor> iterator = iterator();
		int count = 0;
		while (!ended) {
			if ((count > targetCount) || (!iterator.hasNext())) {
				ended = true;
			} else {
				result.add(iterator.next());
				count += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public AttributeDescriptor getByIndex(final int index) {
		AttributeDescriptor result;

		result = this.attributeDescriptors.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		boolean result;

		result = this.attributeDescriptors.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty() {
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<AttributeDescriptor> iterator() {
		Iterator<AttributeDescriptor> result;

		result = this.attributeDescriptors.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public StringList labels() {
		StringList result;

		//
		HashSet<String> labels = new HashSet<String>();

		//
		for (AttributeDescriptor descriptor : this.attributeDescriptors) {
			labels.add(descriptor.getLabel());
		}

		//
		result = new StringList(labels.size());

		//
		for (String label : labels) {
			result.add(label);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public StringList labelsSorted() {
		StringList result;

		//
		result = labels();

		//
		Collections.sort(result, Collator.getInstance());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public AttributeDescriptor last() {
		AttributeDescriptor result;

		if (this.attributeDescriptors.isEmpty()) {
			result = null;
		} else {
			result = this.attributeDescriptors.get(this.attributeDescriptors.size() - 1);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> relationModelNames() {
		List<String> result;

		//
		HashSet<String> names = new HashSet<String>();

		//
		for (AttributeDescriptor descriptor : this.attributeDescriptors) {
			if (descriptor.getScope() == AttributeDescriptor.Scope.RELATION) {
				names.add(descriptor.getOptionalRelationName());
			}
		}

		//
		result = new ArrayList<String>(names.size());
		for (String name : names) {
			result.add(name);
		}

		//
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * 
	 * @param descriptor
	 */
	synchronized public void remove(final AttributeDescriptor descriptor) {
		this.attributeDescriptors.remove(descriptor);
	}

	/**
	 * 
	 * @return
	 */
	public AttributeDescriptors reverse() {
		AttributeDescriptors result;

		//
		Collections.reverse(this.attributeDescriptors);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size() {
		int result;

		result = this.attributeDescriptors.size();

		//
		return result;
	}

	/**
	 * 
	 */
	public AttributeDescriptors sort() {
		AttributeDescriptors result;

		//
		Collections.sort(this.attributeDescriptors, new AttributeDescriptorComparator(Sorting.DEFAULT));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long total() {
		long result;

		//
		result = 0;

		//
		for (AttributeDescriptor descriptor : this.attributeDescriptors) {
			result += descriptor.getCountOfSet();
		}

		//
		return result;
	}
}
