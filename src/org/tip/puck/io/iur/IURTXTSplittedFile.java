package org.tip.puck.io.iur;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.comparator.NameFileComparator;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.geo.io.GEOFile;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.RelationModel;

import fr.devinsy.util.StringList;

/**
 * This class represents a IUR TXT Splitted File exporter.
 * 
 * @author TIP
 */
public class IURTXTSplittedFile {

	private static final Logger logger = LoggerFactory.getLogger(IURTXTSplittedFile.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	/**
	 * 
	 * @param oneFile
	 *            One file from the split.
	 * @return
	 */
	public static Net doImport(final File oneFile) throws PuckException {
		Net result;

		try {

			if (oneFile == null) {
				throw new IllegalArgumentException("NUll source parameter.");
			} else if (!oneFile.isFile()) {
				throw new IllegalArgumentException("Source is not an existing file.");
			} else {
				//
				String path = oneFile.getAbsolutePath();
				int index = path.lastIndexOf('-') - 3;
				String prefix = path.substring(0, index);
				logger.debug("prefix=[{}]", prefix);

				//
				File[] files = oneFile.getParentFile().listFiles((FileFilter) FileFilterUtils.prefixFileFilter(new File(prefix).getName()));
				Arrays.sort(files, NameFileComparator.NAME_COMPARATOR);

				//
				StringList lines = new StringList();
				for (File file : files) {
					if (file.isFile()) {
						lines.addAll(FileUtils.readLines(file, DEFAULT_CHARSET_NAME));
					}
				}
				String fileContent = lines.toStringSeparatedBy(System.getProperty("line.separator"));

				//
				BufferedReader in = new BufferedReader(new InputStreamReader(IOUtils.toInputStream(fileContent)));
				result = IURTXTFile.read(in);
				result.setLabel(prefix);
			}
		} catch (IOException exception) {
			throw PuckExceptions.IO_ERROR.create("Importing IURTXTSplitted file.");
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param targetDirectory
	 * @throws PuckException
	 */
	public static void export(final File targetDirectory, final Net source) throws PuckException {

		if (source == null) {
			throw new IllegalArgumentException("NUll source parameter.");
		} else if (targetDirectory == null) {
			throw new IllegalArgumentException("NUll target parameter.");
		} else if (!targetDirectory.exists()) {
			throw new IllegalArgumentException("Target directory not found  [" + targetDirectory.getAbsolutePath() + "].");
		} else {
			//
			String corpusName = FilenameUtils.getBaseName(source.getLabel());

			//
			if (!source.attributes().isEmpty()) {
				File targetFile = new File(targetDirectory, String.format("%s-00-corpus.iurs.txt", corpusName));

				PrintWriter out = null;
				try {
					out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(targetFile), "UTF-8"));

					IURTXTFile.writeCorpusAttributes(out, source.attributes());

				} catch (UnsupportedEncodingException exception) {
					throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + targetFile + "]");
				} catch (FileNotFoundException exception) {
					throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + targetFile + "]");
				} finally {
					//
					IOUtils.closeQuietly(out);
				}
			}

			//
			int fileIndex = 1;
			{
				File targetFile = new File(targetDirectory, String.format("%s-%02d-individuals.iurs.txt", corpusName, fileIndex));
				fileIndex += 1;

				PrintWriter out = null;
				try {
					out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(targetFile), "UTF-8"));

					IURTXTFile.writeIndividuals(out, source.individuals());

				} catch (UnsupportedEncodingException exception) {
					throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + targetFile + "]");
				} catch (FileNotFoundException exception) {
					throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + targetFile + "]");
				} finally {
					//
					IOUtils.closeQuietly(out);
				}
			}

			//
			{
				File targetFile = new File(targetDirectory, String.format("%s-%02d-families.iurs.txt", corpusName, fileIndex));
				fileIndex += 1;

				PrintWriter out = null;
				try {
					out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(targetFile), "UTF-8"));

					IURTXTFile.writeFamilies(out, source.families());

				} catch (UnsupportedEncodingException exception) {
					throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + targetFile + "]");
				} catch (FileNotFoundException exception) {
					throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + targetFile + "]");
				} finally {
					//
					IOUtils.closeQuietly(out);
				}
			}

			//
			for (RelationModel relationModel : source.relationModels()) {

				File targetFile = new File(targetDirectory, String.format("%s-%02d-%s.iurs.txt", corpusName, fileIndex, relationModel.getName()));
				fileIndex += 1;

				PrintWriter out = null;
				try {
					out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(targetFile), "UTF-8"));

					IURTXTFile.writeRelations(out, relationModel, source.relations());

				} catch (UnsupportedEncodingException exception) {
					throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + targetFile + "]");
				} catch (FileNotFoundException exception) {
					throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + targetFile + "]");
				} finally {
					//
					IOUtils.closeQuietly(out);
				}
			}

			//
			if (source.getGeography() != null) {
				File targetFile = new File(targetDirectory, String.format("%s-%02d-geography.txt", corpusName, fileIndex));

				PrintWriter out = null;
				try {
					out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(targetFile), "UTF-8"));

					GEOFile.save(targetFile, source.getGeography());

				} catch (UnsupportedEncodingException exception) {
					throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + targetFile + "]");
				} catch (FileNotFoundException exception) {
					throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + targetFile + "]");
				} finally {
					//
					IOUtils.closeQuietly(out);
				}
			}
		}
	}
}
