package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.workers.PartitionReporter;
import org.tip.puck.report.Report;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.util.Labels;
import org.tip.puckgui.NetGUI;

/**
 * 
 * @author TIP
 */
public class PartitionInputWindow extends JFrame {

	private static final long serialVersionUID = -7015163064882803997L;
	private static final Logger logger = LoggerFactory.getLogger(PartitionInputWindow.class);

	private JFrame thisJFrame;
	private NetGUI netGUI;
	private JPanel contentPane;
	private PartitionCriteriaPanel partitionCriteriaPanel;

	/**
	 * Create the frame.
	 */
	public PartitionInputWindow(final NetGUI netGUI) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(FooReporterInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		this.netGUI = netGUI;
		setTitle("Partition Inputs");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 454, 317);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					PartitionCriteria criteria = PartitionInputWindow.this.partitionCriteriaPanel.getCriteria();

					Report report = PartitionReporter.reportBasicInformation(netGUI.getNet(), criteria);
					netGUI.addReportTab(report);

					dispose();
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(PartitionInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);

		List<Labels> modelsLabels = Segmentation.buildModelLabels(netGUI.getCurrentIndividuals(), netGUI.getCurrentFamilies(),
				netGUI.getNet().relationModels(), netGUI.getNet().relations());
		this.partitionCriteriaPanel = new PartitionCriteriaPanel(modelsLabels);
		this.contentPane.add(this.partitionCriteriaPanel, BorderLayout.CENTER);
	}
}
