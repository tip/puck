package org.tip.puck.graphs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.tip.puck.PuckException;
import org.tip.puck.graphs.Link.LinkType;

/**
 * 
 * @author TIP
 */
public class RootedLinks<E> implements Iterable<Link<E>> {

	private Node<E> rootNode;
	private HashMap<Node<E>, Link<E>> nodeToLink;

	/**
	 *
	 */
	public RootedLinks(final Node<E> rootNode) {
		this.rootNode = rootNode;
		this.nodeToLink = new HashMap<Node<E>, Link<E>>();
	}

	/**
	 *
	 */
	public RootedLinks(final Node<E> rootNode, final int initialCapacity) {
		this.rootNode = rootNode;
		this.nodeToLink = new HashMap<Node<E>, Link<E>>(initialCapacity);
	}

	/**
	 * 
	 * @param targetNode
	 * @throws PuckException
	 */
	public void add(final Link<E> link) {
		if ((link == null) || (link.getSourceNode() == null) || (link.getTargetNode() == null)) {
			throw new NullPointerException("Null parameter detected.");
		} else if ((link.getSourceNode() != this.rootNode) && (link.getTargetNode() != this.rootNode)) {
			throw new UnsupportedOperationException("Link not rooted to the current root node.");
		} else {
			this.nodeToLink.put(link.getOtherNode(this.rootNode), link);
		}
	}

	/**
	 * 
	 * @param targetNode
	 * @throws PuckException
	 */
	public Link<E> addArc(final Node<E> sourceNode, final Node<E> targetNode) {
		Link<E> result;

		result = addArc(sourceNode, targetNode, 0.0);

		//
		return result;
	}

	/**
	 * 
	 * @param targetNode
	 * @throws PuckException
	 */
	public Link<E> addArc(final Node<E> sourceNode, final Node<E> targetNode, final double weight) {
		Link<E> result;

		if ((sourceNode == null) || (targetNode == null)) {
			throw new NullPointerException("Null parameter detected.");
		} else {
			result = this.nodeToLink.get(targetNode);
			if (result == null) {
				result = new Link<E>(sourceNode, targetNode, LinkType.ARC, weight);
				this.nodeToLink.put(targetNode, result);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetNode
	 * @throws PuckException
	 */
	public Link<E> addEdge(final Node<E> sourceNode, final Node<E> targetNode) {
		Link<E> result;

		result = addEdge(sourceNode, targetNode, 0.0);

		//
		return result;
	}

	/**
	 * 
	 * @param targetNode
	 * @throws PuckException
	 */
	public Link<E> addEdge(final Node<E> sourceNode, final Node<E> targetNode, final double weight) {
		Link<E> result;

		if ((sourceNode == null) || (targetNode == null)) {
			throw new NullPointerException("Null parameter detected.");
		} else {
			result = this.nodeToLink.get(targetNode);
			if (result == null) {
				result = new Link<E>(sourceNode, targetNode, LinkType.EDGE, weight);
				this.nodeToLink.put(targetNode, result);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double averagePositiveWeight() {
		double result;

		//
		double sum = 0;

		//
		int count = 0;
		for (Link<E> link : this.getLinks()) {
			if ((link != null) && (link.getWeight() > 0)) {
				count += 1;
				sum += link.getWeight();
			}
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum / count;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double averageWeight() {
		double result;

		if (size() == 0) {
			result = 0;
		} else {
			result = sumWeight() / size();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Link<E>> getLinks() {
		List<Link<E>> result;

		result = new ArrayList<Link<E>>(this.nodeToLink.values());

		//
		return result;
	}

	/**
	 * 
	 * @param targetNode
	 * @return
	 */
	public Link<E> getLinkWith(final Node<E> otherNode) {
		Link<E> result;

		result = this.nodeToLink.get(otherNode);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Node<E>> getNodes() {
		List<Node<E>> result;

		result = new ArrayList<Node<E>>(this.nodeToLink.keySet());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Node<E>> getNodesSortedByLabel() {
		List<Node<E>> result;

		result = getNodes();
		Collections.sort(result, new NodeComparatorByLabel<E>());

		//
		return result;
	}

	public Node<E> getRootNode() {
		return rootNode;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Link<E>> iterator() {
		Iterator<Link<E>> result;

		result = this.nodeToLink.values().iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double maxWeight() {
		double result;

		if (this.size() == 0) {
			result = 0;
		} else {
			result = Double.MIN_VALUE;
			for (Link<E> link : this.nodeToLink.values()) {
				if ((link != null) && (link.getWeight() > result)) {
					result = link.getWeight();
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double minWeight() {
		double result;

		if (this.size() == 0) {
			result = 0;
		} else {
			result = Double.MAX_VALUE;
			for (Link<E> link : this.nodeToLink.values()) {
				if ((link != null) && (link.getWeight() < result)) {
					result = link.getWeight();
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size() {
		int result;

		result = this.nodeToLink.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double sumWeight() {
		double result;

		result = 0;
		for (Link<E> link : this.nodeToLink.values()) {
			if (link != null) {
				result += link.getWeight();
			}
		}

		//
		return result;
	}
	
	public void removeLinkToNode (Node<E> node){
		
		this.nodeToLink.remove(node);
		
	}

}
