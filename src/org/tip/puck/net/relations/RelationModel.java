package org.tip.puck.net.relations;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.Individual;
import org.tip.puck.net.relations.roles.RoleRelationMaker.RoleRelationRule;
import org.tip.puck.net.relations.roles.RoleRelations;

/**
 * 
 * @author TIP
 */
public class RelationModel {

	private String name;
	private Roles roles;
	private RoleRelations roleRelations;
	private List<RoleRelationRule> rules;

	/**
	 * 
	 * @param source
	 */
	public RelationModel(final RelationModel source) {
		//
		this.name = source.getName();
		this.roles = new Roles(source.roles());
	}

	/**
	 * 
	 * @param name
	 * @param role
	 */
	public RelationModel(final String name) {
		//
		this.name = name;
		this.roles = new Roles();
	}

	/**
	 * 
	 * @param name
	 * @param role
	 */
	public RelationModel(final String name, final List<RoleRelationRule> rules) {
		//
		this.name = name;
		this.roles = new Roles();
		this.rules = rules;
	}

	public String getName() {
		return this.name;
	}

	public Role role(final Individual indi) {
		Role result;

		if (indi == null) {
			result = null;
		} else {
			result = this.roles.getByName(indi.getName());
			if (result == null) {
				result = new Role(indi.getName());
				this.roles.add(result);
			}
		}
		//
		return result;
	}

	public Role role(final String item) {
		Role result;

		if (StringUtils.isEmpty(item)) {
			result = null;
		} else {
			String roleName = item.trim();
			result = this.roles.getByName(roleName);
			if (result == null) {
				result = new Role(roleName);
				this.roles.add(result);
			}
		}
		//
		return result;
	}


	public Roles roles() {
		return this.roles;
	}
	
	public int getRoleIndex (Role role){
		int result;
		
		result = 0;
		
		for (Role thisRole : roles){
			
			if (thisRole.equals(role)){
				break;
			}
			
			result++;
		}
		//
		return result;
	}

	public void setName(final String name) {
		this.name = name;
	}


	public RoleRelations getRoleRelations() {
		return roleRelations;
	}

	public void setRoleRelations(RoleRelations roleRelations) {
		this.roleRelations = roleRelations;
	}

	@Override
	public String toString() {
		return this.name;
	}
	
	public boolean equals(Object obj){
		boolean result;
		
		result = this.name.equals(((RelationModel)obj).getName());
		
		//
		return result;
	}

	public List<RoleRelationRule> getRules() {
		return rules;
	}


}
