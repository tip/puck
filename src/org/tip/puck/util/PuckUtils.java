package org.tip.puck.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.workers.GraphUtils;
import org.tip.puck.graphs.workers.NodeValuator;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individualizable;
import org.tip.puck.net.Individuals;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionMaker;
import org.tip.puck.workers.NodeReferentValuator;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 * 
 */
public class PuckUtils {

	private static final Logger logger = LoggerFactory.getLogger(PuckUtils.class);

	/**
	 * This method compares two nullable string values.
	 * 
	 * The comparison manages the local language alphabet order.
	 * 
	 * <pre>
	 * compare(null, null)   = 0
	 * compare(null, bravo)  < 0
	 * compare(alpha, null)  > 0
	 * compare(alpha, bravo) = alpha.compareTo(bravo)
	 * </pre>
	 * 
	 * @param alpha
	 *            one of the value.
	 * 
	 * @param bravo
	 *            the other value.
	 * 
	 * @return zero or a positive value or a negative value.
	 * 
	 */
	public static int compare(final String alpha, final String bravo) {
		int result;

		//
		if ((alpha == null) && (bravo == null)) {
			//
			result = 0;

		} else if (alpha == null) {
			//
			result = -1;

		} else if (bravo == null) {
			//
			result = +1;

		} else {
			//
			result = Collator.getInstance().compare(alpha, bravo);
		}

		//
		return result;
	}

	public static boolean containsStrings(final List<String> list, final String content) {
		boolean result;

		result = false;

		String[] contents = content.split("\\;");
		for (String item : list) {
			for (String testitem : contents) {
				if (item.equals(testitem)) {
					result = true;
					break;
				}
			}
		}
		//
		return result;
	}

	public static boolean containsSubstring(final List<String> list, final String content) {
		boolean result;

		result = false;

		for (String item : list) {
			if (item.contains(content)) {
				result = true;
				break;
			}
		}
		//
		return result;
	}

	public static List<String> cumulateList(final List<String> source) {
		List<String> result;

		result = new ArrayList<String>();

		result.add(source.get(0));
		for (int i = 1; i < source.size(); i++) {
			result.add(result.get(i - 1) + "-" + source.get(i));
		}
		//
		return result;

	}

	/**
	 * Waiting the StringUtils upgrade.
	 * 
	 * @param string
	 * @param searchStrings
	 * @return
	 */
	public static boolean equalsAny(final CharSequence string, final CharSequence... searchStrings) {
		boolean result;

		if (StringUtils.isEmpty(string) || ArrayUtils.isEmpty(searchStrings)) {
			result = false;
		} else {
			boolean ended = false;
			result = false;
			int index = 0;
			while (!ended) {
				if (index < searchStrings.length) {
					if (StringUtils.equals(string, searchStrings[index])) {
						ended = true;
						result = true;
					} else {
						index += 1;
					}
				} else {
					ended = true;
					result = false;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Waiting the StringUtils upgrade.
	 * 
	 * @param string
	 * @param searchStrings
	 * @return
	 */
	public static boolean equalsAnyIgnoreCase(final CharSequence string, final CharSequence... searchStrings) {
		boolean result;

		if (StringUtils.isEmpty(string) || ArrayUtils.isEmpty(searchStrings)) {
			result = false;
		} else {
			boolean ended = false;
			result = false;
			int index = 0;
			while (!ended) {
				if (index < searchStrings.length) {
					if (StringUtils.equalsIgnoreCase(string, searchStrings[index])) {
						ended = true;
						result = true;
					} else {
						index += 1;
					}
				} else {
					ended = true;
					result = false;
				}
			}
		}

		//
		return result;
	}

	public static NumberedValues[] getGenderedNumberedValues(final NumberedValues values, final Individuals individuals) {
		NumberedValues[] result;

		result = new NumberedValues[3];
		for (int i = 0; i < 3; i++) {
			result[i] = new NumberedValues();
		}

		for (Individual individual : individuals) {
			int id = individual.getId();
			int gender = individual.getGender().toInt();
			result[gender].put(id, values.get(id));
			result[2].put(id, values.get(id));
		}

		//
		return result;

	}
	

	public static <E extends Individualizable> NumberedValues[] getGenderedNumberedValues(final Partition<E> partition) {
		NumberedValues[] result;

		result = new NumberedValues[3];
		for (int i = 0; i < 3; i++) {
			result[i] = new NumberedValues();
		}

		for (E item : partition.getItems()) {
			int id = item.getEgo().getId();
			int gender = item.getEgo().getGender().toInt();
			result[gender].put(id, partition.getValue(item));
			result[2].put(id, partition.getValue(item));
		}

		//
		return result;

	}

	/**
	 * 
	 * @param in
	 * @return
	 * @throws IOException
	 */
	public static List<StringList> readLineBlocks(final BufferedReader in) throws IOException {
		List<StringList> result;

		//
		result = new ArrayList<StringList>();

		//
		boolean ended = false;
		StringList currentBlock = null;
		while (!ended) {
			//
			String line = in.readLine();

			//
			if (line == null) {
				//
				ended = true;

			} else if (StringUtils.isBlank(line)) {
				//
				currentBlock = null;

			} else {
				//
				if (currentBlock == null) {
					currentBlock = new StringList();
					result.add(currentBlock);
				}

				//
				currentBlock.add(line);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param in
	 * @return
	 * @throws IOException
	 */
	public static StringList readLines(final BufferedReader in) throws IOException {
		StringList result;

		result = new StringList();

		boolean ended = false;
		while (!ended) {
			//
			String line = in.readLine();

			//
			if (line == null) {
				ended = true;
			} else {
				result.add(line);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public static <E> StringList writeGISNetwork(final Graph<E> source) throws PuckException {
		StringList result;

		result = null;
		/***
		 * 
		 */
		//
		return result;
	}

	public static StringList writeMatrix(final double[][] matrix) {
		StringList result;

		result = new StringList();
		for (int i = 0; i < matrix.length; i++) {
			String line = "";
			for (int j = 0; j < matrix[i].length; j++) {
				line += matrix[i][j] + "\t";
			}
			result.appendln(line);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public static <E> StringList writePajekNetwork(final Graph<E> source) throws PuckException {
		StringList result;

		result = writePajekNetwork(source, null);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public static <E> StringList writePajekNetwork(final Graph<E> source, final List<String> partitionLabels) throws PuckException {
		return writePajekNetwork(source, partitionLabels, null);
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public static <E> StringList writePajekNetwork(final Graph<E> source, final List<String> partitionLabels,
			final Map<String, Map<Value, Integer>> partitionNumbersMaps) throws PuckException {
		StringList result;

		// Generate core Pajek writing.
		result = GraphUtils.writePajekNetwork(source, NodeValuator.getMatchingLabels(partitionLabels), partitionNumbersMaps);

		// Partitions
		for (String label : NodeValuator.getNotMatchingLabels(partitionLabels)) {
			//
			result.appendln();

			// Build value list sorting exactly as source nodes.
			Values values = new NodeReferentValuator<E>().get(source.getNodes().toListSortedById(), label, null);

			if (values.isNull()) {
				values = NodeValuator.get(source, label);
			}

			if (label.contains("DATE")) {
				values.nullToZero();
			}

			//
			if (values.isNumeric()) {
				if (label.equals("STEP")) {
					result.appendln("*Partition " + label + " " + source.getLabel());
				} else {
					result.appendln("*Vector " + label + " " + source.getLabel());
				}
				result.appendln("*vertices " + source.nodeCount());
				for (Value value : values) {
					if (label.equals("STEP")) {
						result.appendln(value.intValue());
					} else {
						result.appendln(value.doubleValue());
					}
				}
			} else {

				//
				Partition<Value> partition = PartitionMaker.create(label, values);
				Map<Value, Integer> partitionNumbersMap = null;
				if (partitionNumbersMaps != null) {
					partitionNumbersMap = partitionNumbersMaps.get(label);
				}
				partition = PartitionMaker.createNumerized(partition, partitionNumbersMap);

				//
				result.appendln("*Partition " + partition.getLabel() + " " + source.getLabel());
				result.appendln("*vertices " + source.nodeCount());
				for (int valueIndex = 1; valueIndex <= values.size(); valueIndex++) {
					Value value = values.get(valueIndex - 1);
					Value partitionValue = partition.getValue(value);
					if (partitionValue==null){
						result.appendln(0);
					} else {
						result.appendln(partitionValue.intValue());
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public static <E> StringList writePajekPartition(Partition<E> partition, final Comparator<E> comparator, final Map<Value, Integer> partitionNumbersMap)
			throws PuckException {
		StringList result;

		result = new StringList();

		String label = partition.getLabel();

		//
		if (partition.isNumeric()) {
			result.appendln("*Vector " + label);
			result.appendln("*vertices " + partition.itemsCount());
			for (E item : partition.getItemsAsSortedList(comparator)) {
				Value value = partition.getValue(item);
				if (value != null) {
					result.appendln(partition.getValue(item).intValue());
				} else {
					result.appendln(0);
				}
			}
		} else {
			//
			partition = PartitionMaker.createNumerized(partition, partitionNumbersMap);

			//
			result.appendln("*Partition " + label);
			result.appendln("*vertices " + partition.itemsCount());
			for (E item : partition.getItemsAsSortedList(comparator)) {
				result.appendln(partition.getValue(item).intValue());
			}
		}

		//
		return result;
	}

}
