package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.report.Report;
import org.tip.puck.statistics.StatisticsCriteria;
import org.tip.puck.statistics.StatisticsReporter;
import org.tip.puckgui.NetGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class StatisticsInputWindow extends JFrame {

	private static final long serialVersionUID = 8851174229118195213L;
	private static final Logger logger = LoggerFactory.getLogger(StatisticsInputWindow.class);

	private JFrame thisJFrame;
	private NetGUI netGUI;
	private JPanel contentPane;
	private JCheckBox chckbxGenderBiasWeight;
	private JCheckBox chckbxGenderBiasNet;
	private JCheckBox chckbxComponents;
	private JCheckBox chckbxGenealogicalCompleteness;
	private JCheckBox chckbxSibsetDistribution;
	private JCheckBox chckbxMeanClusterValues;
	private PartitionCriteriaShortPanel partitionCriteriaShortPanel_1;
	private PartitionCriteriaShortPanel partitionCriteriaShortPanel_2;
	private PartitionCriteriaShortPanel partitionCriteriaShortPanel_3;
	private PartitionCriteriaShortPanel partitionCriteriaShortPanel_4;
	private PartitionCriteriaShortPanel partitionCriteriaShortPanel_5;
	private PartitionCriteriaShortPanel partitionCriteriaShortPanel_6;
	private PartitionCriteriaShortPanel partitionCriteriaShortPanel_7;
	private PartitionCriteriaShortPanel splitCriteriaShortPanel;
	private static StatisticsCriteria defaultCriteria;
	private JCheckBox chckbxAncestorTypes;
	private JSpinner spinnerAncestorTypesDegree;
	private JCheckBox chckbxCousinMarriages;
	private JCheckBox chckbxConsanguineChains;
	private JSpinner spinnerConsanguineDegree;
	private JButton buttonDefault;

	/**
	 * Create the frame.
	 */
	public StatisticsInputWindow(final NetGUI netGUI) {
		//
		if (defaultCriteria == null) {
			defaultCriteria = new StatisticsCriteria();
		}

		//
		List<String> availableLabels = IndividualValuator.getAttributeLabelSample(netGUI.getNet().individuals());
		setIconImage(Toolkit.getDefaultToolkit().getImage(FooReporterInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		this.netGUI = netGUI;
		setTitle("Statistics Inputs");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 980, 605);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panel_1 = new JPanel();
		this.contentPane.add(panel_1, BorderLayout.NORTH);
		panel_1.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "General", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51,
				51)));
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));

		JPanel panel = new JPanel();
		panel_1.add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		this.chckbxGenderBiasWeight = new JCheckBox("Gender BIAS (weight)");
		panel.add(this.chckbxGenderBiasWeight);

		this.chckbxGenderBiasNet = new JCheckBox("Gender BIAS (net weight)");
		panel.add(this.chckbxGenderBiasNet);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		panel_1.add(horizontalStrut);

		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));

		this.chckbxComponents = new JCheckBox("Components");
		panel_2.add(this.chckbxComponents);

		this.chckbxGenealogicalCompleteness = new JCheckBox("Genealogical Completeness");
		panel_2.add(this.chckbxGenealogicalCompleteness);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		panel_1.add(horizontalStrut_1);

		JPanel panel_3 = new JPanel();
		panel_1.add(panel_3);
		panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.Y_AXIS));

		this.chckbxSibsetDistribution = new JCheckBox("Sibset Distribution");
		panel_3.add(this.chckbxSibsetDistribution);

		this.chckbxCousinMarriages = new JCheckBox("4 cousin Marriages");
		panel_3.add(this.chckbxCousinMarriages);

		Component verticalGlue = Box.createVerticalGlue();
		panel_3.add(verticalGlue);

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		panel_1.add(horizontalStrut_2);

		JPanel panel_6 = new JPanel();
		panel_1.add(panel_6);
		panel_6.setLayout(new BoxLayout(panel_6, BoxLayout.Y_AXIS));

		JPanel panel_8 = new JPanel();
		panel_6.add(panel_8);
		panel_8.setLayout(new FormLayout(new ColumnSpec[] { ColumnSpec.decode("15dlu"), FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		this.chckbxAncestorTypes = new JCheckBox("Ancestor Types");
		panel_8.add(this.chckbxAncestorTypes, "1, 1, 5, 1");

		JLabel lblDegree = new JLabel("(degree:");
		panel_8.add(lblDegree, "2, 2");

		this.spinnerAncestorTypesDegree = new JSpinner();
		panel_8.add(this.spinnerAncestorTypesDegree, "4, 2");
		this.spinnerAncestorTypesDegree.setFont(new Font("Dialog", Font.PLAIN, 10));
		this.spinnerAncestorTypesDegree.setModel(new SpinnerNumberModel(3, 1, 99, 1));

		JLabel lblAaaaa = new JLabel(")");
		panel_8.add(lblAaaaa, "5, 2");

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		panel_1.add(horizontalStrut_3);

		JPanel panel_9 = new JPanel();
		panel_1.add(panel_9);
		panel_9.setLayout(new FormLayout(new ColumnSpec[] { ColumnSpec.decode("15dlu"), FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] {
				FormFactory.DEFAULT_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		this.chckbxConsanguineChains = new JCheckBox("Consanguine Chains");
		panel_9.add(this.chckbxConsanguineChains, "1, 1, 6, 1");

		JLabel lbldegree = new JLabel("(degree:");
		panel_9.add(lbldegree, "2, 2");

		this.spinnerConsanguineDegree = new JSpinner();
		this.spinnerConsanguineDegree.setModel(new SpinnerNumberModel(3, 1, 99, 1));
		this.spinnerConsanguineDegree.setFont(new Font("Dialog", Font.PLAIN, 10));
		panel_9.add(this.spinnerConsanguineDegree, "4, 2");

		JLabel label = new JLabel(")");
		panel_9.add(label, "5, 2");

		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Partition Diagrams Criteria", TitledBorder.LEADING, TitledBorder.TOP,
				null, new Color(51, 51, 51)));
		this.contentPane.add(panel_4, BorderLayout.CENTER);
		panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.Y_AXIS));

		JPanel panel_5 = new JPanel();
		panel_5.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_4.add(panel_5);
		panel_5.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("center:default:grow"),
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("center:default:grow"), FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("center:default:grow"), FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("center:170dlu:grow"),
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("center:35dlu"),
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("center:25dlu"),
				FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblLabel = new JLabel("Label");
		panel_5.add(lblLabel, "2, 2");

		JLabel lblParameter = new JLabel("Parameter");
		panel_5.add(lblParameter, "4, 2");

		JLabel lblType = new JLabel("Type");
		panel_5.add(lblType, "6, 2");

		JLabel lblSpecificData = new JLabel("Specific Data");
		panel_5.add(lblSpecificData, "8, 2");

		JLabel lblCumul = new JLabel("Cumulation");
		panel_5.add(lblCumul, "10, 2");

		JLabel lblFilter = new JLabel("Filter Value");
		panel_5.add(lblFilter, "12, 2");

		JLabel lblSizeFilter = new JLabel("Filter Size");
		panel_5.add(lblSizeFilter, "14, 2");

		JLabel lblClear = new JLabel("Clear");
		panel_5.add(lblClear, "16, 2");

		this.partitionCriteriaShortPanel_1 = new PartitionCriteriaShortPanel(availableLabels, null);
		this.partitionCriteriaShortPanel_1.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_4.add(this.partitionCriteriaShortPanel_1);

		this.partitionCriteriaShortPanel_2 = new PartitionCriteriaShortPanel(availableLabels, null);
		this.partitionCriteriaShortPanel_2.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_4.add(this.partitionCriteriaShortPanel_2);

		this.partitionCriteriaShortPanel_3 = new PartitionCriteriaShortPanel(availableLabels, null);
		this.partitionCriteriaShortPanel_3.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_4.add(this.partitionCriteriaShortPanel_3);

		this.partitionCriteriaShortPanel_4 = new PartitionCriteriaShortPanel(availableLabels, null);
		this.partitionCriteriaShortPanel_4.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_4.add(this.partitionCriteriaShortPanel_4);

		this.partitionCriteriaShortPanel_5 = new PartitionCriteriaShortPanel(availableLabels, null);
		this.partitionCriteriaShortPanel_5.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_4.add(this.partitionCriteriaShortPanel_5);

		this.partitionCriteriaShortPanel_6 = new PartitionCriteriaShortPanel(availableLabels, null);
		this.partitionCriteriaShortPanel_6.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_4.add(this.partitionCriteriaShortPanel_6);

		this.partitionCriteriaShortPanel_7 = new PartitionCriteriaShortPanel(availableLabels, null);
		this.partitionCriteriaShortPanel_7.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_4.add(this.partitionCriteriaShortPanel_7);

		JPanel panel_7 = new JPanel();
		panel_7.setBorder(new TitledBorder(null, "Split Partition Criteria", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.add(panel_7);
		panel_7.setLayout(new BoxLayout(panel_7, BoxLayout.Y_AXIS));

		this.splitCriteriaShortPanel = new PartitionCriteriaShortPanel(availableLabels, null);
		panel_7.add(this.splitCriteriaShortPanel);
		this.splitCriteriaShortPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		this.splitCriteriaShortPanel.setBorder(null);

		this.chckbxMeanClusterValues = new JCheckBox("Mean Cluster Values");
		panel_7.add(this.chckbxMeanClusterValues);

		Component verticalGlue_1 = Box.createVerticalGlue();
		panel_4.add(verticalGlue_1);

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Close.
				dispose();
			}
		});

		JButton buttonClear = new JButton("Clear");
		buttonClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Clear button.
				clear();
			}
		});
		buttonPanel.add(buttonClear);

		this.buttonDefault = new JButton("Default");
		this.buttonDefault.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Default button.
				defaultCriteria = new StatisticsCriteria();
				setCriteria(defaultCriteria);
			}
		});
		buttonPanel.add(this.buttonDefault);
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		getRootPane().setDefaultButton(btnLaunch);
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Launch.
				try {
					StatisticsCriteria criteria = getCriteria();
					defaultCriteria = criteria;
					Report report = StatisticsReporter.reportGraphicsStatistics(netGUI.getSegmentation(), criteria, netGUI.getFile());
					netGUI.addReportTab(report);
					dispose();
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(StatisticsInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);

		// ========================================
		setCriteria(defaultCriteria);
	}

	/**
	 * 
	 */
	public void clear() {
		//
		this.chckbxGenderBiasWeight.setSelected(false);
		this.chckbxGenderBiasNet.setSelected(false);
		this.chckbxComponents.setSelected(false);
		this.chckbxGenealogicalCompleteness.setSelected(false);
		this.chckbxSibsetDistribution.setSelected(false);
		this.chckbxMeanClusterValues.setSelected(false);
		this.chckbxCousinMarriages.setSelected(false);
		this.chckbxAncestorTypes.setSelected(false);
		this.spinnerAncestorTypesDegree.setValue(3);
		this.chckbxConsanguineChains.setSelected(false);
		this.spinnerConsanguineDegree.setValue(3);

		//
		this.partitionCriteriaShortPanel_1.clear();
		this.partitionCriteriaShortPanel_2.clear();
		this.partitionCriteriaShortPanel_3.clear();
		this.partitionCriteriaShortPanel_4.clear();
		this.partitionCriteriaShortPanel_5.clear();
		this.partitionCriteriaShortPanel_6.clear();
		this.partitionCriteriaShortPanel_7.clear();

		//
		this.splitCriteriaShortPanel.clear();
	}

	/**
	 * 
	 * @return
	 */
	public StatisticsCriteria getCriteria() {
		StatisticsCriteria result;

		//
		result = new StatisticsCriteria();

		//
		result.setGenderBIASWeight(this.chckbxGenderBiasWeight.isSelected());
		result.setGenderBIASNetWeight(this.chckbxGenderBiasNet.isSelected());
		result.setComponents(this.chckbxComponents.isSelected());
		result.setGenealogicalCompleteness(this.chckbxGenealogicalCompleteness.isSelected());
		result.setSibsetDistribution(this.chckbxSibsetDistribution.isSelected());
		result.setMeanClusterValues(this.chckbxMeanClusterValues.isSelected());
		result.setFourCousinMarriages(this.chckbxCousinMarriages.isSelected());
		result.setAncestorTypes(this.chckbxAncestorTypes.isSelected());
		result.setAncestorTypesDegree((Integer) this.spinnerAncestorTypesDegree.getValue());
		result.setConsanguineChains(this.chckbxConsanguineChains.isSelected());
		result.setConsanguineDegree((Integer) this.spinnerConsanguineDegree.getValue());

		//
		result.getPartitionCriteriaList().clear();
		result.getPartitionCriteriaList().add(this.partitionCriteriaShortPanel_1.getCriteria());
		result.getPartitionCriteriaList().add(this.partitionCriteriaShortPanel_2.getCriteria());
		result.getPartitionCriteriaList().add(this.partitionCriteriaShortPanel_3.getCriteria());
		result.getPartitionCriteriaList().add(this.partitionCriteriaShortPanel_4.getCriteria());
		result.getPartitionCriteriaList().add(this.partitionCriteriaShortPanel_5.getCriteria());
		result.getPartitionCriteriaList().add(this.partitionCriteriaShortPanel_6.getCriteria());
		result.getPartitionCriteriaList().add(this.partitionCriteriaShortPanel_7.getCriteria());

		//
		result.setSplitCriteria(this.splitCriteriaShortPanel.getCriteria());

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void setCriteria(final StatisticsCriteria source) {

		if (source != null) {
			//
			this.chckbxGenderBiasWeight.setSelected(source.isGenderBIASWeight());
			this.chckbxGenderBiasNet.setSelected(source.isGenderBIASNetWeight());
			this.chckbxComponents.setSelected(source.isComponents());
			this.chckbxGenealogicalCompleteness.setSelected(source.isGenealogicalCompleteness());
			this.chckbxSibsetDistribution.setSelected(source.isSibsetDistribution());
			this.chckbxMeanClusterValues.setSelected(source.isMeanClusterValues());
			this.chckbxCousinMarriages.setSelected(source.isFourCousinMarriages());
			this.chckbxAncestorTypes.setSelected(source.isAncestorTypes());
			this.spinnerAncestorTypesDegree.setValue(source.getAncestorTypesDegree());
			this.chckbxConsanguineChains.setSelected(source.isConsanguineChains());
			this.spinnerConsanguineDegree.setValue(source.getConsanguineDegree());

			//
			this.partitionCriteriaShortPanel_1.setCriteria(getPartitionCriteria(source, 0));
			this.partitionCriteriaShortPanel_2.setCriteria(getPartitionCriteria(source, 1));
			this.partitionCriteriaShortPanel_3.setCriteria(getPartitionCriteria(source, 2));
			this.partitionCriteriaShortPanel_4.setCriteria(getPartitionCriteria(source, 3));
			this.partitionCriteriaShortPanel_5.setCriteria(getPartitionCriteria(source, 4));
			this.partitionCriteriaShortPanel_6.setCriteria(getPartitionCriteria(source, 5));
			this.partitionCriteriaShortPanel_7.setCriteria(getPartitionCriteria(source, 6));

			//
			this.splitCriteriaShortPanel.setCriteria(source.getSplitCriteria());
		}
	}

	/**
	 * 
	 * @param criteria
	 * @param id
	 * @return
	 */
	private static PartitionCriteria getPartitionCriteria(final StatisticsCriteria source, final int index) {
		PartitionCriteria result;

		if ((source == null) || (index >= source.getPartitionCriteriaList().size())) {
			result = null;
		} else {
			result = source.getPartitionCriteriaList().get(index);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param index
	 * @return
	 */
	private static PartitionCriteria getSplitCriteria(final StatisticsCriteria source) {
		PartitionCriteria result;

		if (source == null) {
			result = null;
		} else {
			result = source.getSplitCriteria();
		}

		//
		return result;
	}
}
