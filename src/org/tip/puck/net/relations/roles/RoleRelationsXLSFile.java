package org.tip.puck.net.relations.roles;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ResourceBundle;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.io.iur.IURTXTFile;
import org.tip.puck.io.xls.XLSBufferedReader;
import org.tip.puck.io.xls.XLSWriter;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.report.Report;

import fr.devinsy.util.StringList;

/**
 * This class represents a TXT File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class RoleRelationsXLSFile {

	protected enum Status {
		MANDATORY,
		OPTIONAL
	}

	public static final int MAX_LINE_SIZE = 1024;;

	private static final Logger logger = LoggerFactory.getLogger(RoleRelationsXLSFile.class);

	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static StringList load(final File file) throws PuckException {
		StringList result;

		BufferedReader in = null;
		try {
			in = new XLSBufferedReader(file);
			result = RoleRelationsTXTFile.read(in);

		} catch (UnsupportedEncodingException exception) {
			//
			throw PuckExceptions.UNSUPPORTED_ENCODING.create(exception, "Opening file [" + file + "]");

		} catch (FileNotFoundException exception) {
			//
			throw PuckExceptions.FILE_NOT_FOUND.create(exception, "Opening file [" + file + "]");

		} catch (IOException exception) {
			//
			throw PuckExceptions.IO_ERROR.create(exception, "Opening file [" + file + "]");

		} finally {
			//
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 * @throws IOException 
	 */
	public static Report save(final File file, final RelationModel source) throws PuckException, IOException {
		Report result;
		
		PrintWriter out = null;
		try {
			out = new PrintWriter(new XLSWriter(file, "Term Definitions"));
			result = RoleRelationsTXTFile.write(out,source);

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} finally {
			if (out != null) {
				out.close();
			}
		}
		//
		return result;
	}


}
