package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.relations.RelationModels;
import org.tip.puck.net.workers.AttributeDescriptors;
import org.tip.puck.sequences.workers.SequenceCriteria;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class DiscontinuousItinerariesInputDialog extends JDialog {

	private static final long serialVersionUID = 9154925259628674969L;

	private static final Logger logger = LoggerFactory.getLogger(DiscontinuousItinerariesInputDialog.class);

	private RelationModels relationModels;
	private AttributeDescriptors attributeDescriptors;

	private final JPanel contentPanel = new JPanel();
	private SequenceCriteria dialogCriteria;
	private static SequenceCriteria lastCriteria = new SequenceCriteria();
	private JComboBox cmbxEgoRole;
	private JComboBox cmbxDateLabel;
	private JButton okButton;
	private JComboBox cmbxStartPlaceLabel;
	private JComboBox cmbxEndPlaceLabel;

	/**
	 * Create the dialog.
	 */
	public DiscontinuousItinerariesInputDialog(final RelationModels relationModels, final AttributeDescriptors attributeDescriptors) {
		super();

		this.relationModels = relationModels;
		this.attributeDescriptors = attributeDescriptors;

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Discontinuous Itineraries Input Dialog");
		setIconImage(Toolkit.getDefaultToolkit().getImage(DiscontinuousItinerariesInputDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				DiscontinuousItinerariesInputDialog.this.dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 420, 242);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("max(100dlu;default):grow"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));
		{
			JLabel lblEgoRole = new JLabel("Ego Role:");
			this.contentPanel.add(lblEgoRole, "2, 2, right, default");
		}
		{
			this.cmbxEgoRole = new JComboBox();
			this.contentPanel.add(this.cmbxEgoRole, "4, 2, fill, default");
		}
		{
			JLabel lblDateLabel = new JLabel("Date Label:");
			this.contentPanel.add(lblDateLabel, "2, 4, right, default");
		}
		{
			this.cmbxDateLabel = new JComboBox();
			this.contentPanel.add(this.cmbxDateLabel, "4, 4, fill, default");
		}
		{
			JLabel lblStartPlaceLabel = new JLabel("Start Place Label");
			this.contentPanel.add(lblStartPlaceLabel, "2, 6, right, default");
		}
		{
			this.cmbxStartPlaceLabel = new JComboBox();
			this.contentPanel.add(this.cmbxStartPlaceLabel, "4, 6, fill, default");
		}
		{
			JLabel lblEndPlaceLabel = new JLabel("End Place Label");
			this.contentPanel.add(lblEndPlaceLabel, "2, 8, right, default");
		}
		{
			this.cmbxEndPlaceLabel = new JComboBox();
			this.contentPanel.add(this.cmbxEndPlaceLabel, "4, 8, fill, default");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						DiscontinuousItinerariesInputDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				this.okButton = new JButton("Launch");
				this.okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						SequenceCriteria criteria = getCriteria();

						if (criteria.getEgoRoleName() == null) {
							//
							String title = "Invalid input";
							String message = "An ego role is required to launch.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

						} else if (criteria.getDateLabel() == null) {
							//
							String title = "Invalid input";
							String message = "A valid date label is required to launch.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

						} else if (criteria.getStartDateLabel() == null) {
							//
							String title = "Invalid input";
							String message = "A valid start date label is required to launch.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

						} else if (criteria.getEndDateLabel() == null) {
							//
							String title = "Invalid input";
							String message = "A valid end date label is required to launch.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

						} else {
							//
							lastCriteria = criteria;
							DiscontinuousItinerariesInputDialog.this.dialogCriteria = criteria;

							//
							setVisible(false);
						}
					}
				});
				this.okButton.setActionCommand("OK");
				buttonPane.add(this.okButton);
				getRootPane().setDefaultButton(this.okButton);
			}
		}

		// ////////////////////////
		//
		this.cmbxEgoRole.setModel(new DefaultComboBoxModel(relationModels.roleNames().sort().toArray()));

		StringList labels = this.attributeDescriptors.labels().sort();
		this.cmbxDateLabel.setModel(new DefaultComboBoxModel(labels.toArray()));
		this.cmbxStartPlaceLabel.setModel(new DefaultComboBoxModel(labels.toArray()));
		this.cmbxEndPlaceLabel.setModel(new DefaultComboBoxModel(labels.toArray()));

		setCriteria(lastCriteria);
	}

	/**
	 * 
	 * @return
	 */
	public SequenceCriteria getCriteria() {
		SequenceCriteria result;

		result = new SequenceCriteria();

		//
		if (this.cmbxEgoRole.getSelectedIndex() == -1) {
			//
			result.setEgoRoleName(null);

		} else {
			//
			result.setEgoRoleName((String) this.cmbxEgoRole.getSelectedItem());
		}

		//
		if (this.cmbxDateLabel.getSelectedIndex() == -1) {
			//
			result.setDateLabel(null);

		} else {
			//
			result.setDateLabel((String) this.cmbxDateLabel.getSelectedItem());
		}

		//
		if (this.cmbxStartPlaceLabel.getSelectedIndex() == -1) {
			//
			result.setStartPlaceLabel(null);

		} else {
			//
			result.setStartPlaceLabel((String) this.cmbxStartPlaceLabel.getSelectedItem());
		}

		//
		if (this.cmbxEndPlaceLabel.getSelectedIndex() == -1) {
			//
			result.setEndPlaceLabel(null);

		} else {
			//
			result.setEndPlaceLabel((String) this.cmbxEndPlaceLabel.getSelectedItem());
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public SequenceCriteria getDialogCriteria() {
		SequenceCriteria result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final SequenceCriteria source) {
		//
		if (source != null) {
			//
			if (source.getEgoRoleName() != null) {
				//
				this.cmbxEgoRole.setSelectedItem(source.getEgoRoleName());
			}

			//
			if (source.getDateLabel() != null) {
				//
				this.cmbxDateLabel.setSelectedItem(source.getDateLabel());
			}

			//
			if (source.getStartDateLabel() != null) {
				//
				this.cmbxStartPlaceLabel.setSelectedItem(source.getStartDateLabel());
			}

			//
			if (source.getEndDateLabel() != null) {
				//
				this.cmbxEndPlaceLabel.setSelectedItem(source.getEndDateLabel());
			}
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		/* AttributeFilter criteria = */showDialog(null, null);
	}

	/**
	 * Launch the application.
	 */
	public static SequenceCriteria showDialog(final RelationModels relationModels, final AttributeDescriptors attributeDescriptors) {
		SequenceCriteria result;

		//
		DiscontinuousItinerariesInputDialog dialog = new DiscontinuousItinerariesInputDialog(relationModels, attributeDescriptors);
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
