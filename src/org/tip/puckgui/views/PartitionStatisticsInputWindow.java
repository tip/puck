package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionReporter;
import org.tip.puck.report.Report;
import org.tip.puck.statistics.StatisticsCriteria;
import org.tip.puckgui.NetGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class PartitionStatisticsInputWindow extends JFrame {

	private static final long serialVersionUID = -4591650980341050955L;
	private static final Logger logger = LoggerFactory.getLogger(PartitionStatisticsInputWindow.class);

	private JFrame thisJFrame;
	private NetGUI netGUI;
	private JPanel contentPane;
	private PartitionCriteriaShortPanel partitionCriteriaShortPanel_1;
	private PartitionCriteriaShortPanel partitionCriteriaShortPanel_2;
	private PartitionCriteriaShortPanel partitionCriteriaShortPanel_3;
	private PartitionCriteriaShortPanel partitionCriteriaShortPanel_4;
	private PartitionCriteriaShortPanel partitionCriteriaShortPanel_5;
	private PartitionCriteriaShortPanel partitionCriteriaShortPanel_6;
	private PartitionCriteriaShortPanel partitionCriteriaShortPanel_7;
	private PartitionCriteriaShortPanel splitCriteriaShortPanel;
	private static StatisticsCriteria defaultCriteria;

	/**
	 * Create the frame.
	 */
	public PartitionStatisticsInputWindow(final NetGUI netGUI) {
		//
		if (defaultCriteria == null) {
			defaultCriteria = new StatisticsCriteria();
		}

		//
		List<String> availableLabels = IndividualValuator.getAttributeLabelSample(netGUI.getNet().individuals());
		setIconImage(Toolkit.getDefaultToolkit().getImage(FooReporterInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		this.netGUI = netGUI;
		setTitle("Statistics Inputs");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 980, 605);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panel_1 = new JPanel();
		this.contentPane.add(panel_1, BorderLayout.NORTH);
		panel_1.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "General", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51,
				51)));
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));

		JPanel panel = new JPanel();
		panel_1.add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));

		JPanel panel_3 = new JPanel();
		panel_1.add(panel_3);
		panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.Y_AXIS));

		Component verticalGlue = Box.createVerticalGlue();
		panel_3.add(verticalGlue);

		JPanel panel_6 = new JPanel();
		panel_1.add(panel_6);
		panel_6.setLayout(new BoxLayout(panel_6, BoxLayout.Y_AXIS));

		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Partition Diagrams Criteria", TitledBorder.LEADING, TitledBorder.TOP,
				null, new Color(51, 51, 51)));
		this.contentPane.add(panel_4, BorderLayout.CENTER);
		panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.Y_AXIS));

		JPanel panel_5 = new JPanel();
		panel_5.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_4.add(panel_5);
		panel_5.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("center:default:grow"),
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("center:default:grow"), FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("center:default:grow"), FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("center:170dlu:grow"),
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("center:35dlu"),
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("center:25dlu"),
				FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblLabel = new JLabel("Label");
		panel_5.add(lblLabel, "2, 2");

		JLabel lblParameter = new JLabel("Parameter");
		panel_5.add(lblParameter, "4, 2");

		JLabel lblType = new JLabel("Type");
		panel_5.add(lblType, "6, 2");

		JLabel lblSpecificData = new JLabel("Specific Data");
		panel_5.add(lblSpecificData, "8, 2");

		JLabel lblCumul = new JLabel("Cumulation");
		panel_5.add(lblCumul, "10, 2");

		JLabel lblFilter = new JLabel("Filter Value");
		panel_5.add(lblFilter, "12, 2");

		JLabel lblSizeFilter = new JLabel("Filter Size");
		panel_5.add(lblSizeFilter, "14, 2");

		JLabel lblClear = new JLabel("Clear");
		panel_5.add(lblClear, "16, 2");

		this.partitionCriteriaShortPanel_1 = new PartitionCriteriaShortPanel(availableLabels, getPartitionCriteria(defaultCriteria, 0));
		this.partitionCriteriaShortPanel_1.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_4.add(this.partitionCriteriaShortPanel_1);

		this.partitionCriteriaShortPanel_2 = new PartitionCriteriaShortPanel(availableLabels, getPartitionCriteria(defaultCriteria, 1));
		this.partitionCriteriaShortPanel_2.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_4.add(this.partitionCriteriaShortPanel_2);

		this.partitionCriteriaShortPanel_3 = new PartitionCriteriaShortPanel(availableLabels, getPartitionCriteria(defaultCriteria, 2));
		this.partitionCriteriaShortPanel_3.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_4.add(this.partitionCriteriaShortPanel_3);

		this.partitionCriteriaShortPanel_4 = new PartitionCriteriaShortPanel(availableLabels, getPartitionCriteria(defaultCriteria, 3));
		this.partitionCriteriaShortPanel_4.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_4.add(this.partitionCriteriaShortPanel_4);

		this.partitionCriteriaShortPanel_5 = new PartitionCriteriaShortPanel(availableLabels, getPartitionCriteria(defaultCriteria, 4));
		this.partitionCriteriaShortPanel_5.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_4.add(this.partitionCriteriaShortPanel_5);

		this.partitionCriteriaShortPanel_6 = new PartitionCriteriaShortPanel(availableLabels, getPartitionCriteria(defaultCriteria, 5));
		this.partitionCriteriaShortPanel_6.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_4.add(this.partitionCriteriaShortPanel_6);

		this.partitionCriteriaShortPanel_7 = new PartitionCriteriaShortPanel(availableLabels, getPartitionCriteria(defaultCriteria, 6));
		this.partitionCriteriaShortPanel_7.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_4.add(this.partitionCriteriaShortPanel_7);

		JPanel panel_7 = new JPanel();
		panel_7.setBorder(new TitledBorder(null, "Split Partition Criteria", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.add(panel_7);
		panel_7.setLayout(new BoxLayout(panel_7, BoxLayout.Y_AXIS));

		this.splitCriteriaShortPanel = new PartitionCriteriaShortPanel(availableLabels, getSplitCriteria(defaultCriteria));
		panel_7.add(this.splitCriteriaShortPanel);
		this.splitCriteriaShortPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		this.splitCriteriaShortPanel.setBorder(null);

		Component verticalGlue_1 = Box.createVerticalGlue();
		panel_4.add(verticalGlue_1);

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});

		JButton buttonClear = new JButton("Clear");
		buttonClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Clear button.
				clear();
			}
		});
		buttonPanel.add(buttonClear);
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		getRootPane().setDefaultButton(btnLaunch);
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					StatisticsCriteria criteria = getCriteria();
					defaultCriteria = criteria;
					Report report = PartitionReporter.reportPartitionStatistics(netGUI.getSegmentation(), criteria);
					// Report report =
					// StatisticsReporter.reportGraphicsStatistics(netGUI.getSegmentation(),
					// criteria);
					netGUI.addReportTab(report);
					dispose();
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(PartitionStatisticsInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);

	}

	/**
	 * 
	 */
	public void clear() {

		//
		this.partitionCriteriaShortPanel_1.clear();
		this.partitionCriteriaShortPanel_2.clear();
		this.partitionCriteriaShortPanel_3.clear();
		this.partitionCriteriaShortPanel_4.clear();
		this.partitionCriteriaShortPanel_5.clear();
		this.partitionCriteriaShortPanel_6.clear();
		this.partitionCriteriaShortPanel_7.clear();

		//
		this.splitCriteriaShortPanel.clear();
	}

	/**
	 * 
	 * @return
	 */
	public StatisticsCriteria getCriteria() {
		StatisticsCriteria result;

		//
		result = new StatisticsCriteria();

		//
		result.getPartitionCriteriaList().clear();
		result.getPartitionCriteriaList().add(this.partitionCriteriaShortPanel_1.getCriteria());
		result.getPartitionCriteriaList().add(this.partitionCriteriaShortPanel_2.getCriteria());
		result.getPartitionCriteriaList().add(this.partitionCriteriaShortPanel_3.getCriteria());
		result.getPartitionCriteriaList().add(this.partitionCriteriaShortPanel_4.getCriteria());
		result.getPartitionCriteriaList().add(this.partitionCriteriaShortPanel_5.getCriteria());
		result.getPartitionCriteriaList().add(this.partitionCriteriaShortPanel_6.getCriteria());
		result.getPartitionCriteriaList().add(this.partitionCriteriaShortPanel_7.getCriteria());

		//
		result.setSplitCriteria(this.splitCriteriaShortPanel.getCriteria());

		//
		return result;
	}

	/**
	 * 
	 * @param criteria
	 * @param id
	 * @return
	 */
	private PartitionCriteria getPartitionCriteria(final StatisticsCriteria source, final int index) {
		PartitionCriteria result;

		if ((source == null) || (index >= source.getPartitionCriteriaList().size())) {
			result = null;
		} else {
			result = source.getPartitionCriteriaList().get(index);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param index
	 * @return
	 */
	private PartitionCriteria getSplitCriteria(final StatisticsCriteria source) {
		PartitionCriteria result;

		if (source == null) {
			result = null;
		} else {
			result = source.getSplitCriteria();
		}

		//
		return result;
	}

}
