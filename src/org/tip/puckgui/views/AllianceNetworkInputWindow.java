package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.workers.GraphUtils;
import org.tip.puck.net.Individual;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.graphs.ClusterNetworkReporter;
import org.tip.puck.partitions.graphs.ClusterNetworkUtils;
import org.tip.puck.partitions.graphs.ClusterNetworkUtils.AllianceType;
import org.tip.puck.partitions.graphs.ClusterNetworkUtils.LineType;
import org.tip.puck.report.Report;
import org.tip.puckgui.GroupNetGUI;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.PuckGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class AllianceNetworkInputWindow extends JFrame {

	private static final long serialVersionUID = 2338866020660700892L;
	private static final Logger logger = LoggerFactory.getLogger(AllianceNetworkInputWindow.class);

	private JFrame thisJFrame;
	private NetGUI netGUI;
	private JPanel contentPane;
	private static AllianceNetworkCriteria lastCriteria;
	private JComboBox cmbBxLabel;
	private JComboBox comboBoxType;
	private JSpinner spinnerMinimalDegree;
	private JCheckBox chckbxWeightedArcs;
	private JSpinner spinnerMinimalLinkWeight;
	private final ButtonGroup buttonGroupLinkType = new ButtonGroup();
	private JSpinner spinnerMinimalNodeStrength;

	/**
	 * Create the frame.
	 */
	public AllianceNetworkInputWindow(final NetGUI netGUI) {
		//
		List<String> availableLabels = IndividualValuator.getAttributeLabelSample(netGUI.getNet().individuals());

		//
		if (lastCriteria == null) {
			lastCriteria = new AllianceNetworkCriteria();
		}

		// /////////////////////////////////
		setIconImage(Toolkit.getDefaultToolkit().getImage(AllianceNetworkInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		this.netGUI = netGUI;
		setTitle("Alliance Network Inputs");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 410, 330);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		this.contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));

		JLabel lblLabel = new JLabel("Label:");
		panel.add(lblLabel, "2, 2, right, default");

		this.cmbBxLabel = new JComboBox(availableLabels.toArray());
		this.cmbBxLabel.setMaximumRowCount(12);
		this.cmbBxLabel.setEditable(true);
		panel.add(this.cmbBxLabel, "4, 2, fill, default");

		JLabel lblMinimalNumberOf = new JLabel("<html><div style=\"text-align:right\">Minimal number of<br/>lines per node (node degree):</div></html>");
		panel.add(lblMinimalNumberOf, "2, 8, right, default");

		this.spinnerMinimalDegree = new JSpinner();
		this.spinnerMinimalDegree.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		panel.add(this.spinnerMinimalDegree, "4, 8");

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		getRootPane().setDefaultButton(btnLaunch);
		btnLaunch.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Launch.
				try {
					AllianceNetworkCriteria criteria = getCriteria();

					//
					lastCriteria = criteria;

					//
					if (StringUtils.isBlank(criteria.getLabel())) {
						//
						String title = "Bad input";
						String message = "Please, enter none empty label.";

						//
						JOptionPane.showMessageDialog(AllianceNetworkInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
					} else {
						//
						Graph<Cluster<Individual>> graph;
						switch (criteria.getAllianceType()) {
							case WIFE_HUSBAND:
								graph = ClusterNetworkUtils.createAllianceNetwork(netGUI.getSegmentation(), criteria.getLabel(), criteria.getLineType());
							break;

							case SISTER_BROTHER:
								graph = ClusterNetworkUtils.createSiblingNetwork(netGUI.getSegmentation(), criteria.getLabel(), criteria.getLineType());
							break;

							case PARENT_CHILD:
								graph = ClusterNetworkUtils.createParentChildNetwork(netGUI.getSegmentation(), criteria.getLabel(), criteria.getLineType());
							break;

							default:
								graph = null;
						}

						graph = GraphUtils.reduce(graph, criteria.getMinimalDegree(), criteria.getMinimalNodeStrength(), criteria.getMinimalLinkWeight());

						//
						graph.attributes().put("Label", criteria.getLabel());
						graph.attributes().put("Alliance Type", criteria.getAllianceType().toString());
						if (criteria.getLineType() == LineType.WEIGHTED_ARC) {
							graph.attributes().put("Weighted Arcs", "true");
						} else {
							graph.attributes().put("Weighted Arcs", "false");
						}
						graph.attributes().put("Minimal number of links (node degree)", String.valueOf(criteria.getMinimalDegree()));
						graph.attributes().put("Minimal number of alliances per node (node strength)", String.valueOf(criteria.getMinimalNodeStrength()));
						graph.attributes().put("Minimal number of alliances per link (link weight)", String.valueOf(criteria.getMinimalLinkWeight()));

						//
						GroupNetGUI groupNetGUI = PuckGUI.instance().createGroupNetGUI(graph);
						groupNetGUI.setGeography(netGUI.getNet().getGeography());

						//
						dispose();
					}
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(AllianceNetworkInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JButton btnLaunchold = new JButton("Statistics");
		btnLaunchold.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Old Launch.
				try {
					AllianceNetworkCriteria criteria = getCriteria();

					//
					lastCriteria = criteria;

					//
					if (StringUtils.isBlank(criteria.getLabel())) {
						//
						String title = "Bad input";
						String message = "Please, enter none empty label.";

						//
						JOptionPane.showMessageDialog(AllianceNetworkInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
					} else {
						Report report = ClusterNetworkReporter.reportAllianceNetwork(netGUI.getSegmentation(), netGUI.getFile(), criteria.getLabel(),
								criteria.getAllianceType(), criteria.getLineType(), criteria.getMinimalDegree(), criteria.getMinimalNodeStrength(),
								criteria.getMinimalLinkWeight());
						netGUI.addReportTab(report);
						dispose();
					}
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(AllianceNetworkInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunchold);
		buttonPanel.add(btnLaunch);

		JLabel lblAllianceType = new JLabel("Alliance Type:");
		panel.add(lblAllianceType, "2, 4, right, default");

		this.comboBoxType = new JComboBox(new Object[] { "Wife-Husband", "Sister-Brother", "Parent-Child" });
		this.comboBoxType.setSelectedIndex(0);
		this.comboBoxType.setMaximumRowCount(12);
		this.comboBoxType.setEditable(true);
		panel.add(this.comboBoxType, "4, 4, fill, default");

		this.chckbxWeightedArcs = new JCheckBox("Weighted Arcs");
		this.chckbxWeightedArcs.setSelected(true);
		panel.add(this.chckbxWeightedArcs, "4, 6");

		JLabel lblMinimalNodeStrength = new JLabel(
				"<html><div style=\"text-align:right\">Minimal line value sum of<br/> per node:<br/>(node strength)</div></html>\r\n");
		panel.add(lblMinimalNodeStrength, "2, 10, right, default");

		this.spinnerMinimalNodeStrength = new JSpinner(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		panel.add(this.spinnerMinimalNodeStrength, "4, 10");

		JLabel lblMinimalArcWeight = new JLabel(
				"<html><div style=\"text-align:right\">Minimal line value </div></html>");
		panel.add(lblMinimalArcWeight, "2, 12, right, default");

		this.spinnerMinimalLinkWeight = new JSpinner(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		panel.add(this.spinnerMinimalLinkWeight, "4, 12");

		// //////////////////
		setCriteria(lastCriteria);
	}

	/**
	 * 
	 * @return
	 */
	public AllianceNetworkCriteria getCriteria() {
		AllianceNetworkCriteria result;

		//
		result = new AllianceNetworkCriteria();

		//
		result.setLabel((String) this.cmbBxLabel.getSelectedItem());

		//
		AllianceType allianceType;
		switch (this.comboBoxType.getSelectedIndex()) {
			case 0:
				allianceType = AllianceType.WIFE_HUSBAND;
			break;
			case 1:
				allianceType = AllianceType.SISTER_BROTHER;
			break;
			case 2:
				allianceType = AllianceType.PARENT_CHILD;
			break;
			default:
				allianceType = AllianceType.WIFE_HUSBAND;
		}
		result.setAllianceType(allianceType);

		//
		LineType lineType;
		if (this.chckbxWeightedArcs.isSelected()) {
			lineType = LineType.WEIGHTED_ARC;
		} else {
			lineType = LineType.SIMPLE_ARC;
		}
		result.setLineType(lineType);

		//
		result.setMinimalDegree((Integer) this.spinnerMinimalDegree.getValue());

		//
		result.setMinimalNodeStrength((Integer) this.spinnerMinimalNodeStrength.getValue());

		//
		result.setMinimalLinkWeight((Integer) this.spinnerMinimalLinkWeight.getValue());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public void setCriteria(final AllianceNetworkCriteria source) {

		if (source != null) {
			//
			this.cmbBxLabel.setSelectedItem(source.getLabel());

			//
			switch (source.getAllianceType()) {
				case WIFE_HUSBAND:
					this.comboBoxType.setSelectedIndex(0);
				break;
				case SISTER_BROTHER:
					this.comboBoxType.setSelectedIndex(1);
				break;
				case PARENT_CHILD:
					this.comboBoxType.setSelectedIndex(2);
				break;
				default:
					this.comboBoxType.setSelectedIndex(0);
			}

			//
			if (source.getLineType() == LineType.WEIGHTED_ARC) {
				this.chckbxWeightedArcs.setSelected(true);
			} else {
				this.chckbxWeightedArcs.setSelected(false);
			}

			//
			this.spinnerMinimalDegree.setValue(source.getMinimalDegree());

			//
			this.spinnerMinimalNodeStrength.setValue(source.getMinimalNodeStrength());

			//
			this.spinnerMinimalLinkWeight.setValue(source.getMinimalLinkWeight());
		}
	}

}
