package org.tip.puck.net.relations.workers;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class AddChildRolesCriteria {

	private String relationModelName;
	private String egoRoleName;
	private StringList alterRoleNames;
	private int maxAge;
	private String dateLabel;

	/**
	 * 
	 */
	public AddChildRolesCriteria() {
		//
		this.relationModelName = null;
		this.egoRoleName = null;
		this.alterRoleNames = new StringList();
		this.maxAge = 0;
		this.dateLabel = null;
	}

	public StringList getAlterRoleNames() {
		return this.alterRoleNames;
	}

	public String getDateLabel() {
		return this.dateLabel;
	}

	public String getEgoRoleName() {
		return this.egoRoleName;
	}

	public int getMaxAge() {
		return this.maxAge;
	}

	public String getRelationModelName() {
		return this.relationModelName;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isValid() {
		boolean result;

		if ((this.relationModelName == null) || (this.egoRoleName == null) || (this.alterRoleNames.isEmpty()) || (this.dateLabel == null)) {
			//
			result = false;

		} else {
			//
			result = true;
		}

		//
		return result;
	}

	public void setAlterRoleNames(final StringList alterRoleNames) {
		this.alterRoleNames = alterRoleNames;
	}

	public void setDateLabel(final String dateLabel) {
		this.dateLabel = dateLabel;
	}

	public void setEgoRoleName(final String egoRoleName) {
		this.egoRoleName = egoRoleName;
	}

	public void setMaxAge(final int maxAge) {
		this.maxAge = maxAge;
	}

	public void setRelationModelName(final String relationModelName) {
		this.relationModelName = relationModelName;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		StringList buffer = new StringList();

		buffer.append(this.relationModelName);
		buffer.append(this.egoRoleName);
		buffer.append("{" + this.alterRoleNames.toStringWithCommas() + "}");
		buffer.append(this.maxAge);
		buffer.append(this.dateLabel);

		result = buffer.toStringWithCommas();

		//
		return result;
	}
}
