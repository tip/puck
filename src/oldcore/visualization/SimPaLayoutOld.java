/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package oldcore.visualization;


import java.util.ArrayList;
import java.util.List;

import oldcore.visualization.PlotOptions.HeritageType;

import org.gephi.graph.api.Edge;
import org.gephi.graph.api.HierarchicalGraph;
import org.gephi.graph.api.Node;
import org.gephi.graph.api.NodeData;
import org.gephi.layout.plugin.AbstractLayout;
import org.gephi.layout.plugin.force.Displacement;
import org.gephi.layout.plugin.force.ForceVector;
import org.gephi.layout.spi.Layout;
import org.gephi.layout.spi.LayoutBuilder;
import org.gephi.layout.spi.LayoutProperty;

/**
 *
 * @author Andrzej Kabat
 */

public class SimPaLayoutOld extends AbstractLayout implements Layout {

	private float minY = Float.POSITIVE_INFINITY;
	private float maxY = Float.NEGATIVE_INFINITY;
    
    public static enum TieType {parental, conjugal};
    public static enum Gender {male, female};

    private float optimalDistance;
    //private float relativeStrength;
    //private float step;
    //private float initialStep;
    //private float stepRatio;
    private float convergenceThreshold;
    //private boolean adaptiveCooling;
    //private Displacement displacement;
    private HierarchicalGraph graph;
    private HeritageType heritage = null;
    
    List<Edge> parentalLine;
    List<Edge> parentalNoLine;
    List<Edge> conjugal;

    public SimPaLayoutOld(LayoutBuilder layoutBuilder, Displacement displacement){
        super(layoutBuilder);
        //this.displacement = displacement;
    }
    
    @Override
    public void resetPropertiesValues() {
        //setStepRatio((float) 0.95);
        //setRelativeStrength((float) 0.2);
        /*
        if (graph != null) {
            setOptimalDistance((float) (Math.pow(getRelativeStrength(), 1.0 / 3) * GraphUtils.getAverageEdgeLength(graph)));
        } else {
            setOptimalDistance(100.0f);
        }
        */
        setOptimalDistance(100.0f);
        //setInitialStep(optimalDistance / 5);
        //setStep(initialStep);
        //setAdaptiveCooling(true);
        setConvergenceThreshold(1e-4f);
    }
    
    @Override
    public void initAlgo() {
        if (graphModel == null) {
            return;
        }
        graph = graphModel.getHierarchicalGraphVisible();
        for (Node n : graph.getNodes()) {
            NodeData data = n.getNodeData();
            data.setLayoutData(new ForceVector());
        }
        setConverged(false);
        //setStep(initialStep);
        
        parentalLine = new ArrayList<Edge>();
        parentalNoLine = new ArrayList<Edge>();
        conjugal = new ArrayList<Edge>();
        for (Edge e : graph.getEdges()) {
            TieType t = TieType.valueOf((String) e.getEdgeData().getAttributes().getValue("tie type"));
            
        	if(t == TieType.parental){
        		Gender g1 = Gender.valueOf((String) e.getSource().getNodeData().getAttributes().getValue("gender"));
                Gender g2 = Gender.valueOf((String) e.getTarget().getNodeData().getAttributes().getValue("gender"));
                if(getHeritageType() == HeritageType.agnatic){
                   if(g1 == Gender.male) {
                        if (g2 == Gender.male)
                        	parentalLine.add(e);
                        else
                        	parentalNoLine.add(e);
                    } else {
                    	parentalNoLine.add(e);
                    }
                } else if(getHeritageType() == HeritageType.uterine){
                   if(g1 == Gender.female) {
                        if (g2 == Gender.female)
                        	parentalLine.add(e);
                        else
                        	parentalNoLine.add(e);
                    } else {
                    	parentalNoLine.add(e);
                    }
                } else if(getHeritageType() == HeritageType.cognatic){
                	parentalLine.add(e);
                }
        	} 
        	else conjugal.add(e);
        }
    }

    @Override
    public void goAlgo() {
        graph = graphModel.getHierarchicalGraphVisible();
        graph.readLock();
        Node[] nodes = graph.getNodes().toArray();
        for (Node n : nodes) {
            if (n.getNodeData().getLayoutData() == null || !(n.getNodeData().getLayoutData() instanceof ForceVector)) {
                n.getNodeData().setLayoutData(new ForceVector());
            }
        }

        verticalOrdering(nodes);
        //horizontalOrdering(nodes);
        //joinedHorizontalAndVerticalCouplesOrdering(nodes);
        graph.readUnlock();
    }

	@Override
	public void endAlgo() {
		for (Node n : graph.getNodes()) {
			n.getNodeData().setLayoutData(null);
		}
		parentalLine.clear();
		parentalNoLine.clear();
		conjugal.clear();
		parentalLine = null;
		parentalNoLine = null;
		conjugal = null;
	}   
	
    
    
    
    
    
    
    
    private ForceVector getParentalVerticalForce(NodeData n1, NodeData n2) {
        ForceVector f = new ForceVector(0, (n2.y()+optimalDistance-n1.y())/2); //  /optimalDistance
        return f;
    }
    
    private ForceVector getConjugalVerticalForce(NodeData n1, NodeData n2) {
    	/*
    	float dy = n2.y() - n1.y(); 
        ForceVector f = new ForceVector(0, dy);
        if(f.getNorm()>1f){
        	f.normalize();
        	float coef = dy / optimalDistance;
        	f.multiply(coef);
        	return f;
        }
        else{ 
        	f.multiply(0.33f);
        	return f;
        }
        */
    	ForceVector f = new ForceVector(0, (n2.y()-n1.y())/2); // /optimalDistance 
        return f;
    }
        
    private ForceVector getParentalHorizontalForce(NodeData n1, NodeData n2) {
    	ForceVector f = null;
    	f = new ForceVector((n2.x() - n1.x())/2, 0);
    	/*
    	//float coef = ForceVectorUtils.distance(n1, n2) / optimalDistance;
        //Gender g1 = Gender.valueOf((String) n1.getAttributes().getValue("gender"));
        Gender g2 = Gender.valueOf((String) n2.getAttributes().getValue("gender"));
        if(getHeritageType() == HeritageType.agnatic){
           //if(g1 == Gender.male) {
                if (g2 == Gender.male)
                	f = new ForceVector((n2.x() - n1.x()), 0);
                else
                	f = new ForceVector((n2.x() - n1.x()), 0);
            //} else {
            //	f = new ForceVector();
            //}
        } else if(getHeritageType() == HeritageType.uterine) {
            //if(g1 == Gender.female) {
        	if (g2 == Gender.female)
            	f = new ForceVector((n2.x() - n1.x()), 0);
            else
            	f = new ForceVector((n2.x() - n1.x()), 0);
            //} else {
            //	f = new ForceVector();
            //}
        } else {
        	f = new ForceVector((n2.x() - n1.x()), 0);
            //if(g1 == Gender.male) 
            //else f = new ForceVector();
        }
        */
        if(f.getNorm()>1f) return f.normalize();
        else {
        	f.multiply(0.33f);
        	return f;
        }
    }
    
    private ForceVector getConjugalHorizontalForce(NodeData n1, NodeData n2) {
    	//float coef = ForceVectorUtils.distance(n1, n2) / optimalDistance;
        ForceVector f = new ForceVector((n2.x()-n1.x()), 0);
        if(f.getNorm()>1f) return f.normalize();
        else {
        	f.multiply(0.33f);
        	return f;
        }
    }

    private ForceVector getConjugalForce(NodeData n1, NodeData n2) {
    	//float coef = ForceVectorUtils.distance(n1, n2) / optimalDistance;
        ForceVector f = new ForceVector(n2.x()-n1.x(), n2.y()-n1.y());
        //f.multiply(coef*1000);
        if(f.getNorm()>1f) return f.normalize();
        else {
        	f.multiply(0.33f);
        	return f;
        }
    }
    

    
    
    
    
    private boolean moveNodes(Node[] nodes){
    	boolean result = false;
    	for (Node n : nodes) {
            NodeData data = n.getNodeData();
            if (!data.isFixed()) {
                ForceVector force = data.getLayoutData();
                //force.multiply(0.5f);
                //getDisplacement().moveNode(data, force);
                
                float x = data.x() + force.x();
                float y = data.y() + force.y();
                
                if (!Float.isInfinite(x) && !Float.isNaN(x)) {
                    data.setX(x);
                }
                if (!Float.isInfinite(y) && !Float.isNaN(y)) {
                    data.setY(y);
                }
                
                if(result ==  false && (force.x()!=0 || force.y()!=0)) result = true;
                force.setX(0f);
                force.setY(0f);
            }
        }
    	return result;
    }

    private boolean moveNodes(Edge e){
    	boolean result = false;
    	Node[] nodes = {e.getSource(), e.getTarget()};
    	for(Node n : nodes){
            NodeData data = n.getNodeData();
            if (!data.isFixed()) {
                ForceVector force = data.getLayoutData();
                float x = data.x() + force.x();
                float y = data.y() + force.y();
                if (!Float.isInfinite(x) && !Float.isNaN(x)) {
                    data.setX(x);
                }
                if (!Float.isInfinite(y) && !Float.isNaN(y)) {
                    data.setY(y);
                }
                if(result ==  false && (Math.abs(force.x())>1 || Math.abs(force.y())>1)) result = true;
                force.setX(0f);
                force.setY(0f);
            }
    	}
    	return result;
    }
    
    
    
    
    private void verticalOrdering(Node[] nodes){
    	boolean keepMoving = true;
    	int iter;
    	for(iter = 0; iter<10000 && keepMoving; iter++){
    		keepMoving = false;
            for (Edge e : graph.getEdges()) {
            	NodeData n1 = e.getSource().getNodeData();
                NodeData n2 = e.getTarget().getNodeData();
                ForceVector f1 = n1.getLayoutData();
                ForceVector f2 = n2.getLayoutData();
                TieType t = TieType.valueOf((String) e.getEdgeData().getAttributes().getValue("tie type"));
                ForceVector f = null;
            	if(t == TieType.parental) f = getParentalVerticalForce(n1, n2);
            	else f = getConjugalVerticalForce(n1, n2);
            	f1.add(f);
                f2.subtract(f);
                boolean aux = moveNodes(e);
                keepMoving = keepMoving || aux;
            }
    		//keepMoving = moveNodes(nodes);
    	}
    	System.out.println("Vertical ordering stopped after "+iter+" iterations.");
    }
    
    
    
    
    
    
    private void horizontalNormalization(Node[] nodes){
    	int nbr_gen = 20;
    	//if(minY == Float.POSITIVE_INFINITY && maxY == Float.NEGATIVE_INFINITY){
	    	List<Node> l = new ArrayList<Node>();
	        for (Node node : graph.getNodes()) {
	        	if(graph.getDegree(node)>0){
		            NodeData data = node.getNodeData();
		            minY = Math.min(minY, data.y());
		            maxY = Math.max(maxY, data.y());
	        	}
	        	else l.add(node);
	        }
	        for(Node n : l) {
	        	NodeData data = n.getNodeData();
	        	data.setY(minY + (minY+maxY)/5);
	        }
	        l = null;
    	//}
        float sizeY = maxY - minY;
        System.out.println("minY " + minY + " maxY "+ maxY + " sizeY " + sizeY);
        
        float stepY = sizeY / nbr_gen;
        float[] borders = new float [nbr_gen+1];
        borders[0] = Float.NEGATIVE_INFINITY;
        for(int i=1; i<nbr_gen; i++)
        	borders[i] = minY + i * stepY;
        borders[nbr_gen] = Float.POSITIVE_INFINITY;
        
        int[] total = new int [nbr_gen];
        for(int i=0; i<nbr_gen; i++) total[i] = 0;
        for (Node node : graph.getNodes()) {
            NodeData data = node.getNodeData();
            boolean aux = false;
            for(int i=1; i<=nbr_gen; i++)
            	if(data.y()>=borders[i-1] && data.y()<=borders[i]){
            		total[i-1]++;
            		data.getAttributes().setValue("zoneY", new Integer(i));
            		aux = true;
            		break;
            	}
            
            if(aux==false) {
            	System.out.println("ten nod nie ma przydzielonej zony : " + data.y());
            }
        }
        
        float[] stepX = new float [nbr_gen];
        for(int i=0; i<nbr_gen; i++) stepX[i] = (total[i]>0)? sizeY / total[i] : 0;
        
        int[] actual = new int [nbr_gen];
        for(int i=0; i<nbr_gen; i++) actual[i] = 0;
        
        for (Node node : graph.getNodes()) {
            NodeData data = node.getNodeData();
            int zoneY = (Integer)data.getAttributes().getValue("zoneY");
            float X = minY + actual[zoneY-1]*stepX[zoneY-1];
            data.setX(X);
            actual[zoneY-1]++;
        }
        
    }
    
    
    
    private void horizontalOrdering(Node[] nodes){
    	for(int iterOut=0; iterOut<10; iterOut++){
    		horizontalNormalization(nodes);
    		for(int iterInt=0; iterInt<30; iterInt++){
    			
    			for(Edge e : conjugal) {
    				NodeData n1 = e.getSource().getNodeData();
	                NodeData n2 = e.getTarget().getNodeData();
	                ForceVector f1 = n1.getLayoutData();
	                ForceVector f2 = n2.getLayoutData();
	                ForceVector f = getConjugalHorizontalForce(n1, n2);
	                f1.add(f);
	                f2.subtract(f);
    			}
    			for(Edge e : parentalLine) {
    				NodeData n1 = e.getSource().getNodeData();
	                NodeData n2 = e.getTarget().getNodeData();
	                ForceVector f1 = n1.getLayoutData();
	                ForceVector f2 = n2.getLayoutData();
	                ForceVector f = getParentalHorizontalForce(n1, n2);
	                f1.add(f);
	                f2.subtract(f);
    			}
    			for(Edge e : parentalNoLine) {
    				NodeData n1 = e.getSource().getNodeData();
	                NodeData n2 = e.getTarget().getNodeData();
	                ForceVector f1 = n1.getLayoutData();
	                ForceVector f2 = n2.getLayoutData();
	                ForceVector f = getParentalHorizontalForce(n1, n2);
	                f1.add(f);
	                f2.subtract(f);
    			}
    			moveNodes(nodes);
    			
    			for(Edge e : conjugal) {
    				NodeData n1 = e.getSource().getNodeData();
	                NodeData n2 = e.getTarget().getNodeData();
	                ForceVector f1 = n1.getLayoutData();
	                ForceVector f2 = n2.getLayoutData();
	                ForceVector f = getConjugalHorizontalForce(n1, n2);
	                f1.add(f);
	                f2.subtract(f);
    			}
    			for(Edge e : parentalLine) {
    				NodeData n1 = e.getSource().getNodeData();
	                NodeData n2 = e.getTarget().getNodeData();
	                ForceVector f1 = n1.getLayoutData();
	                ForceVector f2 = n2.getLayoutData();
	                ForceVector f = getParentalHorizontalForce(n1, n2);
	                f1.add(f);
	                f2.subtract(f);
    			}
    			moveNodes(nodes);
    			
    			
    			/*
    			for (Edge e : graph.getEdges()) {
    	            NodeData n1 = e.getSource().getNodeData();
    	            NodeData n2 = e.getTarget().getNodeData();
    	            ForceVector f1 = n1.getLayoutData();
    	            ForceVector f2 = n2.getLayoutData();
    	            ForceVector f = null;
    	            TieType t = TieType.valueOf((String) e.getEdgeData().getAttributes().getValue("tie type"));
    	            if(t == TieType.parental){
    	            	//ForceVector fbis = getParentalVerticalForce(n1, n2);
    	            	f = getParentalHorizontalForce(n1, n2);
    	            	//f.add(fbis);
    	            	//f = new ForceVector();
    	            } else if(t == TieType.conjugal) {
    	            	f = getConjugalHorizontalForce(n1, n2);
    	            }	
    	            f1.add(f);
    	            f2.subtract(f);
    			}
    	    	moveNodes(nodes);
    	    	*/
    			
    	    	
    		}
    	}
    }
    
    
    
    private void joinedHorizontalAndVerticalCouplesOrdering(Node[] nodes){
    	for(int iter = 0; iter<100; iter++){
            for (Edge e : graph.getEdges()) {
            	TieType t = TieType.valueOf((String) e.getEdgeData().getAttributes().getValue("tie type"));
            	if(t == TieType.conjugal){
            		NodeData n1 = e.getSource().getNodeData();
                    NodeData n2 = e.getTarget().getNodeData();
                    ForceVector f1 = n1.getLayoutData();
                    ForceVector f2 = n2.getLayoutData();
                    ForceVector f = getConjugalForce(n1, n2);
            		f1.add(f);
                    f2.subtract(f);            		
            	}
            }
    		moveNodes(nodes);
    	}
    }
    
    
    
    
    /*
    private void paintConjugalTie(Edge e){
        EdgeData ed = e.getEdgeData();
        ed.setColor(128f/255, 128f/255, 128f/255);
    }

    private void paintLinearTie(Edge e){
        EdgeData ed = e.getEdgeData();
        float r = (float) (Math.random());
        float g = (float) (Math.random());
        float b = (float) (Math.random());
        ed.setColor(r, g, b);
    }

    private void paintNonLinearTie(Edge e) {
        EdgeData ed = e.getEdgeData();
        ed.setColor(192f/255f, 192f/255f, 192f/255f);
    }

    private void paintInvisible(Edge e){
        EdgeData ed = e.getEdgeData();
        ed.setColor(1.0f, 1.0f, 1.0f);
    }
    */
    
    
    
    
    
    
    /*
    protected void postAlgo() {
        updateStep();
        if (Math.abs((energy - energy0) / energy) < getConvergenceThreshold()) {
            setConverged(true);
        }
    }

    private void updateStep() {
        if (isAdaptiveCooling()) {
            if (energy < energy0) {
                progress++;
                if (progress >= 5) {
                    progress = 0;
                    setStep(step / getStepRatio());
                }
            } else {
                progress = 0;
                setStep(step * getStepRatio());
            }
        } else {
            setStep(step * getStepRatio());
        }
    }
    */
    
    
    
    
    
    
    
    
    /*
    private Displacement getDisplacement() {
        //displacement.setStep(step);
        //return displacement;
    	return null;
    }
    */

    @Override
    public LayoutProperty[] getProperties() {
        List<LayoutProperty> properties = new ArrayList<LayoutProperty>();
        final String GL_CATEGORY = "Genealogical layout properties";
        try {
            properties.add(LayoutProperty.createProperty(
                    this, Float.class, "Optimal Distance", GL_CATEGORY,
                    "The natural length of the springs. Bigger values mean nodes will be farther apart.",
                    "getOptimalDistance", "setOptimalDistance"));
            properties.add(LayoutProperty.createProperty(
                    this, Float.class, "Relative Strength", GL_CATEGORY,
                    "The relative strength between electrical force (repulsion) and spring force (attraction).",
                    "getRelativeStrength", "setRelativeStrength"));
            properties.add(LayoutProperty.createProperty(
                    this, Float.class, "Initial Step size", GL_CATEGORY,
                    "The initial step size used in the integration phase. Set this value to a meaningful size compared to the optimal distance (10% is a good starting point).",
                    "getInitialStep", "setInitialStep"));
            properties.add(LayoutProperty.createProperty(
                    this, Float.class, "Step ratio", GL_CATEGORY,
                    "The ratio used to update the step size across iterations.",
                    "getStepRatio", "setStepRatio"));
            properties.add(LayoutProperty.createProperty(
                    this, Boolean.class, "Adaptive Cooling", GL_CATEGORY,
                    "Controls the use of adaptive cooling. It is used help the layout algoritm to avoid energy local minima.",
                    "isAdaptiveCooling", "setAdaptiveCooling"));
            properties.add(LayoutProperty.createProperty(
                    this, Float.class, "Convergence Threshold", GL_CATEGORY,
                    "Relative energy convergence threshold. Smaller values mean more accuracy.",
                    "getConvergenceThreshold", "setConvergenceThreshold"));
            properties.add(LayoutProperty.createProperty(
                    this, HeritageType.class, "Inheritance type", GL_CATEGORY,
                    "The way that children inherit from parents can influence the society structure.",
                    "getHeritageType", "setHeritageType"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return properties.toArray(new LayoutProperty[0]);
    }

    /**
     * @return the optimalDistance
     */
    public Float getOptimalDistance() {
        return optimalDistance;
    }

    /**
     * @param optimalDistance the optimalDistance to set
     */
    public void setOptimalDistance(Float optimalDistance) {
        this.optimalDistance = optimalDistance;
    }

    /**
     * @return the relativeStrength
     */
    /*
    public Float getRelativeStrength() {
        return relativeStrength;
    }
    */

    /**
     * @param relativeStrength the relativeStrength to set
     */
    /*
    public void setRelativeStrength(Float relativeStrength) {
        this.relativeStrength = relativeStrength;
    }
    */

    /**
     * @param step the step to set
     */
    /*
    public void setStep(Float step) {
        this.step = step;
    }
    */

    /**
     * @return the adaptiveCooling
     */
    /*
    public Boolean isAdaptiveCooling() {
        return adaptiveCooling;
    }
    */

    /**
     * @param adaptiveCooling the adaptiveCooling to set
     */
    /*
    public void setAdaptiveCooling(Boolean adaptiveCooling) {
        this.adaptiveCooling = adaptiveCooling;
    }
    */

    /**
     * @return the stepRatio
     */
    /*
    public Float getStepRatio() {
        return stepRatio;
    }
    */

    /**
     * @param stepRatio the stepRatio to set
     */
    /*
    public void setStepRatio(Float stepRatio) {
        this.stepRatio = stepRatio;
    }
    */

    /**
     * @return the convergenceThreshold
     */
    public Float getConvergenceThreshold() {
        return convergenceThreshold;
    }

    /**
     * @param convergenceThreshold the convergenceThreshold to set
     */
    public void setConvergenceThreshold(Float convergenceThreshold) {
        this.convergenceThreshold = convergenceThreshold;
    }

    /**
     * @return the initialStep
     */
    /*
    public Float getInitialStep() {
        return initialStep;
    }
    */

    /**
     * @param initialStep the initialStep to set
     */
    /*
    public void setInitialStep(Float initialStep) {
        this.initialStep = initialStep;
    }
    */

    public HeritageType getHeritageType(){
        return heritage;
    }

    public void setHeritageType(HeritageType s){
        heritage = s;
    }

    
    
    
	public void equalizeGenerations(int depth) {
		depth++;
		if(minY == Float.POSITIVE_INFINITY && maxY == Float.NEGATIVE_INFINITY){
			for (Node node : graph.getNodes()) {
	            NodeData data = node.getNodeData();
	            minY = Math.min(minY, data.y());
	            maxY = Math.max(maxY, data.y());
	        }
		}
		
		float size = maxY - minY;
		float step = size / (depth-1);
		float halfStep = step / 2;
		float[] borders = new float [depth+1];
		borders[0] = minY-halfStep;
		int i = 1;
		for(; i<=depth; i++){
			borders[i] = borders[i-1] + step;
		}
		
		for (Node node : graph.getNodes()) {
            NodeData data = node.getNodeData();
            for(int d=1; d<=depth; d++)
            	if(data.y()>=borders[d-1] && data.y()<=borders[d]){
            		data.setY(borders[d]-halfStep);
            		break;
            	}
        }
	}    
}
