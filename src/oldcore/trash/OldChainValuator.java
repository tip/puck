package oldcore.trash;

import java.util.ArrayList;
import java.util.List;

import org.tip.puck.census.chains.Chain;



public class OldChainValuator {

	/**
	 * gets the skewedness of the ith consanguineous component of a ring from the sequence of component linear path lengths
	 * @param p the list of component path lengths
	 * @param i the index of the consanguinous component
	 * @return the skewedness character (H horizontal, O oblique, A alternate)
	 */
	static String absoluteSkewedness (List<Integer> p, int i){
		int s = OldChainValuator.skewedness (p,i);
		if (s==0) return "H";
		if (Math.abs(s)==1) return "O";
		return "A";
	}

	/**
	 * computes the array of lengths of the linear subchains from the characteristic vector 
	 * @param vec the characteristic vector
	 * @return the array of lengths of the linear subchains from the characteristic vector
	 */
	public static int[] getDepths(int[] vec){
		int[] d = new int[vec.length];
		for (int i=0;i<vec.length;i++){
			d[i]=new Double(Math.floor(Math.log(Math.abs(vec[i])+1)/Math.log(2))).intValue()-1;
		}
		return d;
	}

	/**
	 * gets the skewedness of the ith consanguinous component according to the list of linear path lengths
	 * @param p the list of component linear path length
	 * @param i the index of the consanguinous component
	 * @return the skewedness of the ith consanguinous component 
	 * @since last modified 10/04/12
	 */
	static int skewedness (List<Integer> p, int i){
		if (2*i>=p.size()) return 0;   // i>=dim()
		return p.get(2*i)-p.get(2*i+1);
	}

	//check
		//headlines 14
		/**
		 * returns the dravidian status of a partial consanguinous ring<p>
		 * value of the "DRAV" property
		 * @param p the component (ascending and descending) paths of the ring
		 * @param i the position of the partial ring
		 * @return the dravidian crossness ("#" / "=" / "?")
		 */
		static String crossness (List<Chain> p, int i) {
			if (dravidian(p,i)==-1) return "# "; //t(27)
			if (dravidian(p,i)==1) return "= "; //t(28)
			return "# "; //G+2 cross
	//		return t(29)+" "; //G+2 not defined
		}

	/**
	 * returns the dravidian status of a partial consanguinous ring<p>
	 * @see OldRing#dravidian(ArrayList, int)
	 * @param p the component (ascending and descending) paths of the ring
	 * @param i the position of the partial ring
	 * @return the dravidian crossness (0 parallel / 1 cross)
	 */
	static int dravidian (List<Chain> p, int i) {
		return dravidian(p.get(2*i), p.get(2*i+1));
	}

	/**
		 * checks the dravidian crossness of a cousin relation by comparison of its two branches
		 * @param right the other branch of the cousin relation
		 * @return the dravidian crossness of the relation (0 parallel, 1 cross)
		 */
		static int dravidian (Chain left, Chain right) {
			int d = Math.abs(left.size()-right.size());
			if (d > 1) return 0;
			int n = Math.min(left.size(),right.size())-1;
			if (d > 0) return dravidian (left,right,n+1);
			return dravidian (left,right,n);      
	   }

	/**
	 * checks the dravidian crossness of a cousin relation at the ith level from top
	 * @param right the branch of the cousin
	 * @param i the distance from the common ancestor
	 * @return the dravidian crossness of the relation (0 parallel, 1 cross)
	 */
	static int dravidian (Chain left, Chain right, int i) {
	   if (i<=1) return 1;
	   int a = left.getLast().getGender().toInt();
	   int b = right.getLast().getGender().toInt(); 
	   return dravidian(left,right,i-1)*(1-2*Math.abs(a-b));
	}

	/**
	 * returns the dravidian status of a consanguinous ring<p>
	 * value of the "DRAV-H" property
	 * @param p the component (ascending and descending) paths of the ring
	 * @param s the component path lengths
	 * @return the dravidian status ("ok" / "no" / "?")
	 */
	static String crossness (List<Chain> p, List<Integer> s) {
		if (dravidian(p,s)==-1) return "ok";
	    if (dravidian(p,s)==1) return "no";
	    return "?";
	}

	/**
	 * returns the dravidian status of a consanguinous ring
	 * @param p the component (ascending and descending) paths of the ring
	 * @param s the component path lengths
	 * @return the dravidian status (-1 / 1 / 0)
	 * @see OldRing#crossness(ArrayList, ArrayList)
	 */
	private static int dravidian (List<Chain> p, List<Integer> s) {
		  
		int s0 = skewedness(s,0);
		int s1 = skewedness(s,1);
	    int a = dravidian(p,0);
	    if (p.size()==2) { // dim()==1;
	    	if (s0!=0) return 1;
	    	return a;
	    }
	    if (p.size()>4) return 0;  // dim()>2
	    if (s0+s1!=0) return 1;
	    if ((s0>1) || (s1>1)) return 1;
	    int b = dravidian(p,1);
	    int c = -1;
	    if (s0!=0) c = 1;
	    return (a*b*c); 
	}

	/**
		 * checks the dravidian crossness of a cousin relation by comparison of its two branches
		 * @param right the other branch of the cousin relation
		 * @return the dravidian crossness of the relation (0 parallel, 1 cross)
		 */
		static int getDravidianCrossness (Chain left, Chain right) {
			int d = Math.abs(left.size()-right.size());
			if (d > 1) return 0;
			int n = Math.min(left.size(),right.size())-1;
			if (d > 0) return getDravidianCrossness (left,right,n+1);
			return getDravidianCrossness (left,right,n);      
	   }

	/**
	 * checks the dravidian crossness of a cousin relation at the ith level from top
	 * @param right the branch of the cousin
	 * @param i the distance from the common ancestor
	 * @return the dravidian crossness of the relation (0 parallel, 1 cross)
	 */
	private static int getDravidianCrossness (Chain left, Chain right, int i) {
	   if (i<=1) return 1;
	   return getDravidianCrossness(left,right,i-1)*(1-2*Math.abs(left.getGenderInt(left.size()-i)-right.getGenderInt(right.size()-i)));
	}

	//headlines 14
	//check output
	/**
	 * returns the parakana status of a partial consanguinous ring<p>
	 * value of the "DRAV-O" property
	 * @param p the component (ascending and descending) paths of the ring
	 * @param i the position of the partial ring
	 * @return the dravidian crossness ("#" / "=" / "?")
	 */
	static String paracrossness (List<Chain> p, int i) {
		if (parakana(p,i)==-1) return " ";//t(29)
		if (parakana(p,i)==1) return " ";//t(25);
		if (parakana(p,i)==2) return " ";//t(25)
		return " ";//t(24)+
	}

	/**
	 * returns the dravidian status of a partial consanguinous ring<p>
	 * @see OldRing#dravidian(ArrayList, int)
	 * @param p the component (ascending and descending) paths of the ring
	 * @param i the position of the partial ring
	 * @return the dravidian crossness (0 parallel / 1 cross)
	 */
	static int parakana (List<Chain> p, int i) {
		return parakana(p.get(2*i),p.get(2*i+1));
	}

	/**
		 * checks the dravidian crossness of a cousin relation by comparison of its two branches
		 * @param right the other branch of the cousin relation
		 * @return the dravidian crossness of the relation (0 parallel, 1 cross)
		 */
		static int parakana (Chain left, Chain right) {
			int d = left.size()-right.size();
			if (Math.abs(d) > 1) return 2;
			if (d==0) return getDravidianCrossness (left,right,left.size()-1);
			int n = Math.min(left.size(),right.size())-1;
			int x = getDravidianCrossness (left,right,n);
			int y = Math.abs(left.getGenderInt(left.size()-n-1)-right.getGenderInt(right.size()-n-1));
			if (d > 0) { //aunts
				if (y==0 || x==1) {
					if (n<3) return 1;
					if (Math.abs(left.getGenderInt(left.size()-2)-right.getGenderInt(right.size()-2))==0) return 1;
				}
				return -1;	
			} else { //nieces
				if (y==1 && x==1) {
					if (n==1) return 0;
					if (Math.abs(left.getGenderInt(left.size()-2)-right.getGenderInt(right.size()-2))==0) return 0;
				}
				if ((y==1 && x==-1) || (y==0 && x==1)) return 1;
				return -1;
			}
	   }

}
