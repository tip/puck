package org.tip.puck.io.selz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Attributes;
import org.tip.puck.net.Families;
import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.net.UnionStatus;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Actors;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.RelationModels;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.report.Report;
import org.tip.puck.util.LogHelper;

import fr.devinsy.util.StringList;

/**
 * This class represents a TXT File reader and writer.
 * 
 * @author TIP
 */
public class SELZTXTFile {
	private static final Logger logger = LoggerFactory.getLogger(SELZTXTFile.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	public static final int MAX_LINE_SIZE = 2048;

	public static Map<String, Integer> idMap;
	public static Map<String, Integer> ordMap;

	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file) throws PuckException {
		Net result;

		result = load(file, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file, final String charsetName) throws PuckException {
		Net result;

		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), charsetName));
			result = read(in);

			result.setLabel(file.getName());

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException exception) {
				logger.warn("Not managed error.");
				exception.printStackTrace();
			}
		}

		//
		return result;
	}

	/**
	 * Reads a net from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static Net read(final BufferedReader in) throws PuckException {
		Net result;

		//
		result = new Net();

		//
		readCorpusAttributes(result.attributes(), in);

		//
		readIndividuals(result, in);

		//
		readFamilies(result, in);

		//
		boolean ended = false;
		while (!ended) {
			if (!readRelations(result, in)) {
				ended = true;
			}
		}
		
//		readMacherelNames(result);
		readPGraphNames(result);

		//
		return result;
	}
	
	/***
	 * Temporary method for particular dataset
	 */
	private static void readMacherelNames(Net net){
		for (Family family : net.families()){
			String fatherName = "";
			String motherName = "";
			String firstNamesAttribute = family.getAttributeValue("FIRSTNAMES");
			if (firstNamesAttribute!=null){
				String[] firstNames = firstNamesAttribute.split("-");
				fatherName = firstNames[0]+" / "+family.getAttributeValue("LIGNEE_H");
				if (firstNames.length>1){
					motherName = firstNames[1]+" / "+family.getAttributeValue("LIGNEE_W");
				}
				Individual father = family.getFather();
				if (father!=null){
					if (StringUtils.isEmpty(father.getName())){
						father.setName(fatherName);
					} else if (father.getName().equals("fatherName")){
						System.err.println("Name divergence "+father.getId()+" "+father.getName()+" vs "+fatherName);
					}
				} else if (!StringUtils.isEmpty(fatherName)){
					family.setFather(net.createIndividual(fatherName, Gender.MALE));
					System.out.println("created "+family.getFather()+" "+family);
				}
				Individual mother = family.getMother();
				if (mother!=null){
					if (StringUtils.isEmpty(mother.getName())){
						mother.setName(motherName);
					} else if (mother.getName().equals("motherName")){
						System.err.println("Name divergence "+mother.getId()+" "+mother.getName()+" vs "+motherName);
					}
				} else if (!StringUtils.isEmpty(motherName)){
					family.setMother(net.createIndividual(motherName, Gender.FEMALE));
					System.out.println("created "+family.getMother()+" "+family);
				}
			}
		}
	}
	
	/***
	 * Temporary method for particular dataset
	 * @throws PuckException 
	 */
	private static void readPGraphNames(Net net) throws PuckException{
		for (Family family : net.families()){
			String fatherName = family.getAttributeValue("NameH");
			String motherName = family.getAttributeValue("NameW");
			if (fatherName!=null){
				Individual father = family.getFather();
				if (father!=null){
					if (StringUtils.isEmpty(father.getName())){
						father.setName(fatherName);
						String fatherBirthDate = family.getAttributeValue("H_BIRT_DATE");
						if (fatherBirthDate!=null){
							father.setAttribute("BIRT_DATE", fatherBirthDate);
						}
						String fatherDeathDate = family.getAttributeValue("H_DEAT_DATE");
						if (fatherDeathDate!=null){
							father.setAttribute("DEAT_DATE", fatherDeathDate);
						}
					} else if (father.getName().equals("fatherName")){
						System.err.println("Name divergence "+father.getId()+" "+father.getName()+" vs "+fatherName);
					}
				}
			}
			if (motherName!=null){
				Individual mother = family.getMother();
				if (mother!=null){
					if (StringUtils.isEmpty(mother.getName())){
						mother.setName(motherName);
						String motherBirthDate = family.getAttributeValue("W_BIRT_DATE");
						if (motherBirthDate!=null){
							mother.setAttribute("BIRT_DATE", motherBirthDate);
						}
						String motherDeathDate = family.getAttributeValue("W_DEAT_DATE");
						if (motherDeathDate!=null){
							mother.setAttribute("DEAT_DATE", motherDeathDate);
						}
					} else if (mother.getName().equals("motherName")){
						System.err.println("Name divergence "+mother.getId()+" "+mother.getName()+" vs "+motherName);
					}
				} else if (!StringUtils.isEmpty(motherName)){
					family.setMother(net.createIndividual(motherName, Gender.FEMALE));
					System.out.println("created "+family.getMother()+" "+family);
				}
			}
		}
		
		for (Family family: net.families()){
			for (Individual child : family.getChildren()){
				for (Individual otherChild : family.getChildren()){
					if (otherChild.getId()<child.getId()){
						if (child.getName()!=null && child.getName().equals(otherChild.getName())){
							child.setName(otherChild.getId()+"");
						}
					} else {
						break;
					}
				}
			}
		}
		
		NetUtils.eliminateDoubleIndividuals(net, net.individuals(),new Report());
	}


	/**
	 * Reads a line of corpus attribute labels from a BufferedReader.
	 * 
	 * More over than readLabelsLine method, it detect the absence of the
	 * optional corpus attribute list.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of labels or null.
	 * 
	 * @throws PuckException
	 */
	public static SELZTXTLabelsLine readCorpusAttributeLabelsLine(final BufferedReader in) throws PuckException {
		SELZTXTLabelsLine result;

		try {
			//
			in.mark(MAX_LINE_SIZE);
			String line = readNotEmptyLine(in);

			if (line == null) {
				result = null;
			} else if ((line.matches("^[Ii][Dd]\\t.*$")) || (line.matches("^\\d.*$"))) {
				in.reset();
				result = null;
			} else {
				//
				String[] tokens = line.split("\\t");

				//
				result = new SELZTXTLabelsLine();
				for (String token : tokens) {
					result.add(token);
				}
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading labels line.");
		}

		//
		return result;
	}

	/**
	 * Reads a net from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static void readCorpusAttributes(final Attributes target, final BufferedReader in) throws PuckException {

		logger.debug("Read corpus attributes.");

		//
		SELZTXTLabelsLine labelsLine = readCorpusAttributeLabelsLine(in);
		if (labelsLine != null) {
			SELZTXTLabelsLine valuesLine = readLabelsLine(in);

			// Set other attributes.
			if (valuesLine != null) {
				for (int valueCount = 0; valueCount < valuesLine.size(); valueCount++) {
					//
					String label = labelsLine.get(valueCount);
					String value = valuesLine.get(valueCount);

					//
					if (StringUtils.isNotBlank(value)) {
						target.put(label, value);
					}
				}
			}
		}
	}

	/**
	 * Reads a net from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static void readFamilies(final Net target, final BufferedReader in) throws PuckException {

		logger.debug("Read families.");

		// Read families labels.
		SELZTXTLabelsLine labelsLine = readLabelsLine(in);
		// logger.debug(toString(labelsLine));

		// Read families data.
		boolean ended = false;
		while (!ended) {
			//
			SELZTXTFamilyLine source = readFamilyLine(in);
			// logger.debug(toString(source));

			if (source == null) {
				ended = true;
			} else if (source.getId() != SELZTXTFamilyLine.NOID) {

				//
				Individual father;
				if (source.getFatherId() == SELZTXTFamilyLine.NOID) {
					father = null;
				} else {
					father = target.individuals().getById(source.getFatherId());
					if (father == null) {
						father = target.createIndividual(source.getFatherId());
						father.setGender(Gender.MALE);
						father.setAttribute("ID-Selz", source.getFatherFamilyId() + "-" + source.getFatherOrd());
					}
				}

				//
				Individual mother;
				if (source.getMotherId() == SELZTXTFamilyLine.NOID) {
					mother = null;
				} else {
					mother = target.individuals().getById(source.getMotherId());
					if (mother == null) {
						mother = target.createIndividual(source.getMotherId());
						mother.setGender(Gender.FEMALE);
						mother.setAttribute("ID-Selz", source.getMotherFamilyId() + "-" + source.getMotherOrd());
					}
				}

				//
				UnionStatus status;
				switch (source.getStatus()) {
					case SELZTXTFamilyLine.UNMARRIED:
						status = UnionStatus.UNMARRIED;
					break;
					case SELZTXTFamilyLine.MARRIED:
						status = UnionStatus.MARRIED;
					break;
					case SELZTXTFamilyLine.DIVORCED:
						status = UnionStatus.DIVORCED;
					break;
					default:
						throw PuckExceptions.BAD_FILE_FORMAT.create("Unknown union status [" + source.getStatus() + "]");
				}

				//
				Family family = target.createFamily(source.getId(), father, mother, status);
				family.setHusbandOrder(source.getFatherOrd());
				family.setWifeOrder(source.getMotherOrd());

				// Set children.
				if (StringUtils.isNotBlank(source.getChildIds())) {
					String[] ids = source.getChildIds().split("[ .,;]+");
					for (String childId : ids) {

						if (NumberUtils.isNumber(childId)) {
							//
							Individual child = target.individuals().getById(Integer.parseInt(childId));
							if (child == null) {
								child = target.createIndividual(Integer.parseInt(childId));
							}

							if (child.getOriginFamily() == null) {
								//
								child.setOriginFamily(family);
								family.getChildren().add(child);
							} else {
								throw PuckExceptions.BAD_FILE_FORMAT.create("Child [" + child.getId() + "] defined in two different families ["
										+ child.getOriginFamily().getId() + "][" + family.getId() + "]");
							}
						}
					}
				}

				// Set other attributes.
				{

					// logger.debug("source=" + LogHelper.toString(source));
					for (int valueCount = 0; valueCount < source.attributeValues().size(); valueCount++) {
						logger.debug("valueCount=" + valueCount + " " + source.attributeValues().size() + " " + source.attributeValues().get(valueCount));
						//
						String label = labelsLine.get(valueCount + 5);
						String value = source.attributeValues().get(valueCount);

						//
						if (StringUtils.isNotBlank(value)) {
							family.attributes().put(label, value);
						}
					}
				}

				// logger.debug("put : " + individual);
			}
		}

		// Read map data

		if (idMap != null) {
			for (String token : idMap.keySet()) {
				//
				Individual child = target.individuals().getById(idMap.get(token));
		
				String[] tokens = token.split("\\;");
				int familyId = Double.valueOf(tokens[0]).intValue();

				Family family = target.families().getById(familyId);
				if (family != null) {
					if (child.getOriginFamily() == null) {
						//
						child.setOriginFamily(family);
						family.getChildren().add(child);
						child.setAttribute("ORD", tokens[1]);
						child.setBirthOrder(Double.valueOf(tokens[1]).intValue());
					} else {
						throw PuckExceptions.BAD_FILE_FORMAT.create("Child [" + child.getId() + " "+child.getName()+ "] defined in two different families ["
								+ child.getOriginFamily().getId() + "][" + family.getId() + "]");
					}
				} else {
					System.err.println("Warning: Family with id " + familyId + " does not exist");
				}

			}
		}

	}

	/**
	 * Reads a line of individual data from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of individual data or null.
	 * 
	 * @throws PuckException
	 */
	public static SELZTXTFamilyLine readFamilyLine(final BufferedReader in) throws PuckException {
		SELZTXTFamilyLine result;

		try {
			//
			in.mark(MAX_LINE_SIZE);
			String line = readNotEmptyLine(in);

			if (line == null) {
				result = null;
			} else if (line.matches("^\\d.*$")) {

				//
				String[] tokens = line.split("\\t");

				//
				result = new SELZTXTFamilyLine();

				//
				result.setId((Double.valueOf(tokens[0]).intValue()));

				//
				if (tokens.length > 1) {
					String statusToken = tokens[1].trim().toUpperCase();
					if (statusToken.length() == 0) {
						result.setStatus(SELZTXTFamilyLine.UNMARRIED);
					} else if (statusToken.length() == 1) {
						result.setStatus(statusToken.charAt(0));
					} else {
						throw PuckExceptions.BAD_FILE_FORMAT.create("Bad union status [" + tokens[1] + "].");
					}
				}

				//
				if (tokens.length > 2 && !tokens[2].equals("0;0")) {
					try {
						if (StringUtils.isNotBlank(tokens[2])) {
							if (ordMap == null) {
								ordMap = new TreeMap<String, Integer>();
							}
							if (idMap == null) {
								idMap = new TreeMap<String, Integer>();
							}
							Integer ord = ordMap.get(tokens[2]);
							if (ord==null){
								ord = 1;
							} else {
								ord=ord+1;
							}
							ordMap.put(tokens[2], ord);
							result.setFatherId(idMap.size() + 1);
							idMap.put(tokens[2]+";"+ord, idMap.size() + 1);
							result.setFatherFamilyId(Double.valueOf(tokens[2]).intValue());
							result.setFatherOrd(ord);
//							result.setFatherId((Double.valueOf(tokens[2]).intValue()));
						} else {
							result.setFatherId(0);
						}
					} catch (NumberFormatException nfe) {
						Integer fatherId = idMap.get(tokens[2]);
						if (fatherId != null) {
							result.setFatherId(fatherId);
						} else {
							result.setFatherId(idMap.size() + 1);
							idMap.put(tokens[2], idMap.size() + 1);
							String[] fathertokens = tokens[2].split("\\;");
							result.setFatherFamilyId(Double.valueOf(fathertokens[0]).intValue());
							result.setFatherOrd(Double.valueOf(fathertokens[1]).intValue());
						}
					}
				}

				//
				if (tokens.length > 3 && !tokens[3].equals("0;0")) {
					try {
						if (StringUtils.isNotEmpty(tokens[3])) {
							if (ordMap == null) {
								ordMap = new TreeMap<String, Integer>();
							}
							if (idMap == null) {
								idMap = new TreeMap<String, Integer>();
							}
							Integer ord = ordMap.get(tokens[3]);
							if (ord==null){
								ord = 1;
							} else {
								ord=ord+1;
							}
							ordMap.put(tokens[3], ord);
							result.setMotherId(idMap.size() + 1);
							idMap.put(tokens[3]+";"+ord, idMap.size() + 1);
							result.setMotherFamilyId(Double.valueOf(tokens[3]).intValue());
							result.setMotherOrd(ord);
//							result.setMotherId((Double.valueOf(tokens[3]).intValue()));
						} else {
							result.setMotherId(0);
						}
					} catch (NumberFormatException nfe) {
						Integer motherId = idMap.get(tokens[3]);
						if (motherId != null) {
							result.setMotherId(motherId);
						} else {
							result.setMotherId(idMap.size() + 1);
							idMap.put(tokens[3], idMap.size() + 1);
							String[] mothertokens = tokens[3].split("\\;");
							result.setMotherFamilyId(Double.valueOf(mothertokens[0]).intValue());
							result.setMotherOrd(Double.valueOf(mothertokens[1]).intValue());
						}
					}
				}

				// Set children.
				if ((tokens.length > 4) && (StringUtils.isNotBlank(tokens[4]))) {
					result.setChildIds(tokens[4]);
				}

				// Set attributes.
				for (int tokenCount = 5; tokenCount < tokens.length; tokenCount++) {
					result.attributeValues().add(tokens[tokenCount]);
				}
			} else {
				in.reset();
				result = null;
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading family line.");
		}

		//
		return result;
	}

	/**
	 * Reads a line of individual data from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of individual data or null.
	 * 
	 * @throws PuckException
	 */
	public static SELZTXTIndividualLine readIndividualLine(final BufferedReader in) throws PuckException {
		SELZTXTIndividualLine result;

		try {
			//
			in.mark(MAX_LINE_SIZE);
			String line = readNotEmptyLine(in);

			if (line == null) {
				result = null;
			} else if (line.matches("^\\d.*$")) {

				//
				String[] tokens = line.split("\\t");

				//
				result = new SELZTXTIndividualLine();

				//
				try {
					result.setId((Double.valueOf(tokens[0]).intValue()));
				} catch (NumberFormatException nfe) {
					if (idMap == null) {
						idMap = new TreeMap<String, Integer>();
					}
					String[] firsttokens = tokens[0].split("\\;");
					result.setFamilyId(Double.valueOf(firsttokens[0]).intValue());
					result.setFamilyOrd(Double.valueOf(firsttokens[1]).intValue());
					Integer id = idMap.get(tokens[0]);
					if (id != null) {
						result.setId(id);
					} else {
						result.setId(idMap.size() + 1);
						idMap.put(tokens[0], idMap.size() + 1);
					}
				}

				//
				if (tokens.length > 1) {
					result.setName(tokens[1]);
				}

				//
				if (tokens.length > 2 && tokens[2].length() > 0) {
					result.setGender(tokens[2].charAt(0));
				} else {
					result.setGender('X');
				}

				//
				for (int tokenCount = 3; tokenCount < tokens.length; tokenCount++) {
					result.attributeValues().add(tokens[tokenCount]);
				}
			} else {
				in.reset();
				result = null;
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading individual line.");
		}

		//
		return result;
	}

	/**
	 * Reads a net from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static void readIndividuals(final Net target, final BufferedReader in) throws PuckException {

		logger.debug("Read individuals.");

		// Read individuals labels.
		SELZTXTLabelsLine labelsLine = readLabelsLine(in);
		// logger.debug(toString(labelsLine));

		// Read individuals data.
		boolean ended = false;
		while (!ended) {
			//
			SELZTXTIndividualLine source = readIndividualLine(in);
			// logger.debug(toString(source));

			// logger.debug("source = " + LogHelper.toString(source));

			if (source == null) {
				ended = true;
			} else if (source.getId() != SELZTXTIndividualLine.NOID) {

				// Create a new individual.
				Individual individual;
				try {
					individual = target.createIndividual(source.getId());
					individual.setAttribute("ID-Selz", source.getFamilyId() + "-" + source.getFamilyOrd());
				} catch (final PuckException exception) {
					throw PuckExceptions.BAD_FILE_FORMAT.create("Individual [" + source.getId() + "] define twice.");
				}

				//
				if (!StringUtils.isBlank(source.getName())) {
					individual.setName(source.getName());
				}

				//
				if (Gender.valueOf(source.getGender()) != Gender.UNKNOWN) {
					individual.setGender(Gender.valueOf(source.getGender()));
				}

				// Set other attributes.
				{
					// logger.debug("source=" + LogHelper.toString(source));
					for (int valueCount = 0; valueCount < source.attributeValues().size(); valueCount++) {
						//
						String label = labelsLine.get(valueCount + 3);
						String value = source.attributeValues().get(valueCount);

						//
						if (StringUtils.isNotBlank(value)) {
							individual.attributes().put(label, value);
						}
					}
				}

				// logger.debug("put : " + LogHelper.toString(individual));
			}
		}
	}

	/**
	 * Reads a line of labels from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of labels or null.
	 * 
	 * @throws PuckException
	 */
	public static SELZTXTLabelsLine readLabelsLine(final BufferedReader in) throws PuckException {
		SELZTXTLabelsLine result;

		try {
			//
			in.mark(MAX_LINE_SIZE);
			String line = readNotEmptyLine(in);

			if (line == null) {
				result = null;
			} else if (line.matches("^\\d.*$")) {
				in.reset();
				result = null;
			} else {
				//
				String[] tokens = line.split("\\t");

				//
				result = new SELZTXTLabelsLine();
				for (String token : tokens) {
					result.add(token);
				}
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading labels line.");
		}

		//
		return result;
	}

	/**
	 * Reads a not empty line from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a not empty line or null.
	 * 
	 * @throws PuckException
	 */
	public static String readNotEmptyLine(final BufferedReader in) throws PuckException {
		String result;

		try {
			boolean ended = false;
			result = null;
			while (!ended) {
				//
				String line = in.readLine();

				if (line == null) {
					ended = true;
					result = null;
				} else if (StringUtils.isNotBlank(line)) {
					ended = true;
					result = line;
				}
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading line.");
		}

		//
		return result;
	}

	/**
	 * Reads a line of attributes from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of attributes or null if no more.
	 * 
	 * @throws PuckException
	 */
	public static SELZTXTRelationLine readRelationLine(final BufferedReader in) throws PuckException {
		SELZTXTRelationLine result;

		try {
			//
			in.mark(MAX_LINE_SIZE);
			String line = readNotEmptyLine(in);

			if (line == null) {
				result = null;
			} else if (line.matches("^\\d.*$")) {
				String[] tokens = line.split("\\t");

				//
				result = new SELZTXTRelationLine();

				//
				result.setId((Double.valueOf(tokens[0]).intValue()));

				//
				result.setName(tokens[1]);

				//
				for (int tokenCount = 2; tokenCount < tokens.length; tokenCount++) {
					result.values().add(tokens[tokenCount]);
				}
			} else {
				in.reset();
				result = null;
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading individual line.");
		}

		//
		return result;
	}

	/**
	 * Reads a net from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static boolean readRelations(final Net target, final BufferedReader in) throws PuckException {
		boolean result;

		// Read relation model name.
		SELZTXTLabelsLine labelsLine = readLabelsLine(in);
		// logger.debug("Labels=" + LogHelper.toString(labelsLine));
		if ((labelsLine == null) || (labelsLine.size() != 1)) {
			result = false;
		} else {
			//
			String name;
			if (labelsLine.get(0).length() > 5) {
				name = WordUtils.capitalizeFully(labelsLine.get(0));
			} else {
				name = labelsLine.get(0);
			}

			//
			RelationModel model = target.relationModels().getByName(name);
			if (model == null) {
				model = new RelationModel(name);
			} else {
				throw PuckExceptions.BAD_FILE_FORMAT.create("Relation model [" + name + "] is defined twice.");
			}

			//
			target.relationModels().add(model);
			logger.debug("Relation model=" + model.getName());

			// Read relation roles and attributes labels.
			labelsLine = readLabelsLine(in);
			// logger.debug("Labels=" + LogHelper.toString(labelsLine));
			if ((labelsLine != null) && (labelsLine.size() > 1)) {
				for (int labelIndex = 2; labelIndex < labelsLine.size(); labelIndex++) {
					String label = labelsLine.get(labelIndex);
					if (!label.startsWith("#")) {
						model.roles().add(new Role(label, 0));
					}
				}

				if ((labelsLine != null) && (labelsLine.size() > 2)) {
					// In a relation block, if several line have the same
					// typeId, we must not create only one line.
					// To check that, we will manage a locale list of
					// relations.
					Map<Integer, Relation> relationTypedIdIndex = new HashMap<Integer, Relation>();

					//
					boolean ended = false;
					while (!ended) {
						//
						SELZTXTRelationLine source = readRelationLine(in);
						// logger.debug("====>" + LogHelper.toString(source));
						if (source == null) {
							//
							ended = true;

						} else if ((source.getId() != SELZTXTRelationLine.NOID) && (StringUtils.isNotBlank(source.getName()))) {
							//
							Relation relation = relationTypedIdIndex.get(source.getId());
							if (relation == null) {
								// Note: the line id goes to the typedId.
								relation = target.createRelation(source.getId(), source.getName(), model);
								relationTypedIdIndex.put(source.getId(), relation);
							}

							// logger.debug("source=" +
							// LogHelper.toString(source));
							for (int valueCount = 0; valueCount < source.values().size(); valueCount++) {
								//
								String label = labelsLine.get(valueCount + 2);
								String value = source.values().get(valueCount);

								//
								if (StringUtils.isNotBlank(value)) {
									if (label.startsWith("#")) {
										relation.attributes().add(new Attribute(label.substring(1), value));
									} else {
										String[] ids = value.split("[ .,;]+");
										for (String id : ids) {
											target.createRelationActor(relation, Integer.parseInt(id), label);
										}
									}
								}
							}
						}
					}
				}
			}

			//
			result = true;
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File file, final Net source) throws PuckException {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));

			write(out, source);

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static String toString(final SELZTXTFamilyLine source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			result = String.format("[id=%d][status=%d][fatherId=%d][motherId=%d][childIds=%s][attributeValues=%s]", source.getId(), source.getStatus(),
					source.getFatherId(), source.getMotherId(), source.getChildIds(), LogHelper.toString(source.attributeValues()));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static String toString(final SELZTXTIndividualLine source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			result = String.format("[id=%d][name=%s][gender=%c][attributeValues=%s]", source.getId(), source.getName(), source.getGender(),
					LogHelper.toString(source.attributeValues()));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final SELZTXTLabelsLine source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			StringList buffer = new StringList();
			buffer.append("[");
			for (String label : source) {
				buffer.append(label);
				buffer.append(",");
			}
			buffer.removeLast();
			buffer.append("]");

			//
			result = buffer.toString();
		}

		//
		return result;
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 * @throws PuckException
	 */
	public static void write(final PrintWriter out, final Net source) throws PuckException {

		if (!source.attributes().isEmpty()) {
			//
			writeCorpusAttributes(out, source.attributes());
		}
		writeIndividuals(out, source.individuals());
		writeFamilies(out, source.families());
		writeRelations(out, source.relationModels(), source.relations());

	}

	/**
	 * Writes a line of family data.
	 * 
	 * @param out
	 *            Target.
	 * @param source
	 *            Source.
	 */
	public static void write(final PrintWriter out, final SELZTXTFamilyLine source) {

		//
		StringBuffer buffer = new StringBuffer(512);

		//
		buffer.append(String.format("%d\t%c\t%d\t%d", source.getId(), source.getStatus(), source.getFatherId(), source.getMotherId()));

		//
		buffer.append("\t");
		if (source.getChildIds() != null) {
			buffer.append(source.getChildIds());
		}

		//
		for (String value : source.attributeValues()) {
			buffer.append("\t");
			buffer.append(value);
		}

		//
		out.println(buffer.toString());
	}

	/**
	 * Writes a line of individual data.
	 * 
	 * @param out
	 *            Target.
	 * @param source
	 *            Source.
	 */
	public static void write(final PrintWriter out, final SELZTXTIndividualLine source) {

		//
		StringBuffer buffer = new StringBuffer(512);

		//
		buffer.append(String.format("%d\t%s\t%c", source.getId(), source.getName(), source.getGender()));

		//
		for (String value : source.attributeValues()) {
			buffer.append("\t");
			buffer.append(value);
		}

		//
		out.println(buffer.toString());
	}

	/**
	 * Writes a line of labels.
	 * 
	 * @param out
	 *            Target.
	 * @param source
	 *            Source.
	 */
	public static void write(final PrintWriter out, final SELZTXTLabelsLine source) {

		StringBuffer buffer = new StringBuffer(192);
		for (String value : source) {
			buffer.append(value);
			buffer.append("\t");
		}
		if (buffer.length() > 0) {
			buffer.deleteCharAt(buffer.length() - 1);
		}

		//
		out.println(buffer.toString());
	}

	/**
	 * Writes a line of family data.
	 * 
	 * @param out
	 *            Target.
	 * @param source
	 *            Source.
	 */
	public static void write(final PrintWriter out, final SELZTXTRelationLine source) {

		StringBuffer buffer = new StringBuffer(512);
		buffer.append(String.format("%d\t%s", source.getId(), source.getName()));
		for (String value : source.values()) {
			buffer.append("\t");
			buffer.append(value);
		}
		out.println(buffer.toString());
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 */
	public static void writeCorpusAttributes(final PrintWriter out, final Attributes source) {

		if (!source.isEmpty()) {
			//
			logger.debug("Write corpus attributes block.");
			SELZTXTLabelsLine labelsLine = new SELZTXTLabelsLine();
			labelsLine.addAll(source.labels().sort());
			write(out, labelsLine);

			//
			SELZTXTLabelsLine valuesLine = new SELZTXTLabelsLine();

			// Build corpus attributes data line.
			logger.debug("Write corpus attributes data.");
			for (String label : labelsLine) {

				String value = source.getValue(label);
				if (value == null) {
					valuesLine.add("");
				} else {
					valuesLine.add(value.replace('\t', ' '));
				}
			}

			// Write corpus attributes data line.
			write(out, valuesLine);
		}

		// Write block separation.
		out.println();
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 * @throws PuckException
	 */
	public static void writeFamilies(final PrintWriter out, final Families source) throws PuckException {

		//
		logger.debug("Write families block.");
		SELZTXTLabelsLine labelsLine = new SELZTXTLabelsLine();
		// Never write "ID" cause a Microsoft Excel bug.
		labelsLine.add("Id");
		labelsLine.add("Status");
		labelsLine.add("FatherId");
		labelsLine.add("MotherId");
		labelsLine.add("Children");
		labelsLine.addAll(source.getAttributeLabels().sort());
		write(out, labelsLine);

		// Write families data.
		logger.debug("Write families data.");
		for (Family family : source.toSortedList()) {
			//
			SELZTXTFamilyLine target = new SELZTXTFamilyLine();

			//
			target.setId(family.getId());

			//
			char status;
			switch (family.getUnionStatus()) {
				case UNMARRIED:
					status = SELZTXTFamilyLine.UNMARRIED;
				break;
				case MARRIED:
					status = SELZTXTFamilyLine.MARRIED;
				break;
				case DIVORCED:
					status = SELZTXTFamilyLine.DIVORCED;
				break;
				default:
					throw PuckExceptions.INVALID_PARAMETER.create("Unknown union status code  [" + family.getUnionStatus().toString() + "]");
			}
			target.setStatus(status);

			//
			if (family.getFather() == null) {
				target.setFatherId(SELZTXTFamilyLine.NOID);
			} else {
				target.setFatherId(family.getFather().getId());
			}

			//
			if (family.getMother() == null) {
				target.setMotherId(SELZTXTFamilyLine.NOID);
			} else {
				target.setMotherId(family.getMother().getId());
			}

			// Write children.
			StringList buffer = new StringList();
			for (Individual individual : family.getChildren()) {
				buffer.append(individual.getId());
				buffer.append(';');
			}
			buffer.removeLast();
			target.setChildIds(buffer.toString());

			// Build family data line.
			if (!family.attributes().isEmpty()) {
				// Fill attributes.
				for (int labelCount = 5; labelCount < labelsLine.size(); labelCount++) {
					String label = labelsLine.get(labelCount);
					String value = family.getAttributeValue(label);
					if (value == null) {
						target.attributeValues().add("");
					} else {
						target.attributeValues().add(value.replace('\t', ' '));
					}
				}
			}

			// Write family data line.
			write(out, target);
		}

		// Write block separation.
		out.println();
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 */
	public static void writeIndividuals(final PrintWriter out, final Individuals source) {

		//
		logger.debug("Write individuals block.");
		SELZTXTLabelsLine labelsLine = new SELZTXTLabelsLine();
		// Never write "ID" cause a Microsoft Excel bug.
		labelsLine.add("Id");
		labelsLine.add("Name");
		labelsLine.add("Gender");
		labelsLine.addAll(source.getAttributeLabels().sort());
		write(out, labelsLine);

		// Write individuals data.
		logger.debug("Write individuals data.");
		for (Individual individual : source.toSortedList()) {
			//
			SELZTXTIndividualLine target = new SELZTXTIndividualLine();

			target.setId(individual.getId());
			target.setName(individual.getName());
			target.setGender(individual.getGender().toChar());

			// Build individual data line.
			if (!individual.attributes().isEmpty()) {
				// Fill attributes.
				for (int labelCount = 3; labelCount < labelsLine.size(); labelCount++) {
					String label = labelsLine.get(labelCount);
					String value = individual.getAttributeValue(label);
					if (value == null) {
						target.attributeValues().add("");
					} else {
						target.attributeValues().add(value.replace('\t', ' '));
					}
				}
			}

			// Write individual data line.
			write(out, target);
		}

		// Write block separation.
		out.println();
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 */
	public static void writeRelations(final PrintWriter out, final RelationModels models, final Relations source) {

		for (RelationModel relationModel : models) {
			//
			Relations relations = source.getByModel(relationModel);

			//
			out.println(relationModel.getName());

			//
			logger.debug("Find labels.");
			SELZTXTLabelsLine labelsLine = new SELZTXTLabelsLine();
			// Never write "ID" cause a Microsoft Excel bug.
			labelsLine.add("Id");
			labelsLine.add("Name");
			labelsLine.addAll(relationModel.roles().toNameList()); // Do not sort!
			for (String attributeLabel : relations.getAttributeLabels().sort()) {
				labelsLine.add("#" + attributeLabel);
			}

			//
			write(out, labelsLine);

			//
			if (!relations.isEmpty()) {

				// Build and write relation lines.
				for (Relation relation : relations.toSortedList()) {
					// Build relation line.
					SELZTXTRelationLine target = new SELZTXTRelationLine();

					//
					target.setId(relation.getTypedId());
					target.setName(relation.getName());

					// Cf. "Do not sort!"
					for (Role role : relationModel.roles()) {

						Actors actors = relation.actors().getByRole(role);

						StringList buffer = new StringList();
						for (Actor actor : actors) {
							buffer.append(actor.getId());
							buffer.append(';');
						}
						buffer.removeLast();
						target.values().add(buffer.toString());
					}

					if (!relation.attributes().isEmpty()) {
						// Fill attributes.
						for (int labelCount = 2 + relationModel.roles().size(); labelCount < labelsLine.size(); labelCount++) {
							String label = labelsLine.get(labelCount);

							String value = relation.getAttributeValue(label.substring(1));
							if (value == null) {
								target.values().add("");
							} else {
								target.values().add(value.replace('\t', ' '));
							}
						}
					}

					// Write attributes lines.
					// logger.debug("Write attribute: " +
					// LogHelper.toString(target));
					write(out, target);
				}
			}

			// Write block separation.
			out.println();
		}
	}

}
