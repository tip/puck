package org.tip.puckgui.views.filebatchconverter;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author cpm
 */
public class FileBatchConverterTargetDirectorySelector extends JFileChooser {

	private static final long serialVersionUID = 4812091623599482481L;
	private static final Logger logger = LoggerFactory.getLogger(FileBatchConverterTargetDirectorySelector.class);

	/**
	 * 
	 */
	public FileBatchConverterTargetDirectorySelector(final File sourceDirectory) {
		super();

		//
		File targetDirectory;
		if (sourceDirectory == null) {
			targetDirectory = null;
		} else if (sourceDirectory.isDirectory()) {
			targetDirectory = sourceDirectory;
		} else {
			targetDirectory = sourceDirectory.getParentFile();
		}

		//
		setSelectedFile(targetDirectory);
		setDialogTitle("File Batch Converter target directory");
		setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		setAcceptAllFileFilterUsed(false);
		setApproveButtonText("Set Target");
		setDialogType(CUSTOM_DIALOG);

		//
		// GenericFileFilter defaultFileFilter = new
		// GenericFileFilter("IUR TXT Splitted files (*.iurs.txt)", "iurs.txt");
		// addChoosableFileFilter(defaultFileFilter);
		// setFileFilter(defaultFileFilter);
	}

	/**
	 * 
	 */
	@Override
	public void cancelSelection() {
		//
		logger.debug("Cancel selection.");
		super.cancelSelection();
	}

	/**
	 * 
	 */
	@Override
	public void setSelectedFile(final File file) {
		//
		super.setSelectedFile(file);

		System.out.println("==== SET SELECTED FILE=================");
		System.out.println("SELECED FILE " + file);
	}

	/**
	 * This method is the main one of the selector.
	 * 
	 * @param parent
	 * @return
	 */
	public static File showSelectorDialog(final Component parent, final File targetFile) {
		File result;

		//
		FileBatchConverterTargetDirectorySelector selector = new FileBatchConverterTargetDirectorySelector(targetFile);

		//
		if (selector.showDialog(parent, null) == JFileChooser.APPROVE_OPTION) {

			logger.debug("getCurrentDirectory(): {}", selector.getCurrentDirectory());
			logger.debug("getSelectedFile() : {}", selector.getSelectedFile());
			result = selector.getSelectedFile();

		} else {

			result = null;
		}

		//
		return result;
	}
}
