package org.tip.puck.net.workers;

import java.text.Collator;
import java.util.Comparator;

/**
 * 
 * @author TIP
 * 
 */
public class AttributeDescriptorComparator implements Comparator<AttributeDescriptor> {

	public enum Sorting {
		DEFAULT,
		SCOPE,
		LABEL
	}

	private Sorting sorting;

	/**
	 * 
	 * @param sorting
	 */
	public AttributeDescriptorComparator(final Sorting sorting) {
		//
		this.sorting = sorting;
	}

	/**
	 * 
	 */
	@Override
	public int compare(final AttributeDescriptor alpha, final AttributeDescriptor bravo) {
		int result;

		//
		result = compare(alpha, bravo, this.sorting);

		//
		return result;
	}

	/**
	 * 
	 * @param alpha
	 * @param bravo
	 * @param sorting
	 * @return
	 */
	public static int compare(final AttributeDescriptor alpha, final AttributeDescriptor bravo, final Sorting sorting) {
		int result;

		if ((alpha == null) && (bravo == null)) {
			//
			result = 0;

		} else if (alpha == null) {
			//
			result = -1;

		} else if (bravo == null) {
			//
			result = +1;

		} else {
			//
			switch (sorting) {
				case DEFAULT:
					//
					result = compare(alpha, bravo, Sorting.SCOPE);

					//
					if (result == 0) {
						result = compare(alpha, bravo, Sorting.LABEL);
					}
				break;
				case SCOPE:
					//
					result = compare(alpha.getScope(), bravo.getScope());

					//
					if ((result == 0) && (alpha.getScope() != null) && (alpha.getScope() == AttributeDescriptor.Scope.RELATION)) {
						//
						result = compare(alpha.getOptionalRelationName(), bravo.getOptionalRelationName());
					}
				break;
				case LABEL:
					//
					result = compare(alpha.getLabel(), bravo.getLabel());
				break;
				default:
					result = 0;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param alpha
	 * @param bravo
	 * @return
	 */
	public static int compare(final AttributeDescriptor.Scope alpha, final AttributeDescriptor.Scope bravo) {
		int result;

		//
		if ((alpha == null) && (bravo == null)) {
			//
			result = 0;

		} else if (alpha == null) {
			//
			result = -1;

		} else if (bravo == null) {
			//
			result = +1;

		} else if (alpha == bravo) {
			//
			result = 0;

		} else {
			//
			int alphaValue = position(alpha);
			int bravoValue = position(bravo);

			//
			if (alphaValue > bravoValue) {
				//
				result = +1;

			} else {
				//
				result = -1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param alpha
	 * @param bravo
	 * @return
	 */
	public static int compare(final String alpha, final String bravo) {
		int result;

		//
		if ((alpha == null) && (bravo == null)) {
			//
			result = 0;

		} else if (alpha == null) {
			//
			result = -1;

		} else if (bravo == null) {
			//
			result = +1;

		} else {
			//
			result = Collator.getInstance().compare(alpha, bravo);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param scope
	 * @return
	 */
	private static int position(final AttributeDescriptor.Scope value) {
		int result;

		switch (value) {
			case CORPUS:
				result = 1;
			break;
			case INDIVIDUALS:
				result = 2;
			break;
			case FAMILIES:
				result = 3;
			break;
			case RELATION:
				result = 4;
			break;
			case ACTORS:
				result = 5;
			break;
			case NONE:
			default:
				result = 0;
		}

		//
		return result;
	}
}
