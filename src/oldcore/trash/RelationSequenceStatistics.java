package oldcore.trash;

import java.util.List;

import org.tip.puck.net.relations.Relation;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.Sequences;
import org.tip.puck.sequences.workers.SequenceStatistics;
import org.tip.puck.sequences.workers.SequenceCriteria;

public class RelationSequenceStatistics extends SequenceStatistics<RelationSequence,Relation>{
	
//	RelationSequences sequences;
//	List<Ordinal> times;
//	Map<RelationSequence,Map<Ordinal,Map<String,Object>>> valuesByHouses;
//	List<String> indicators;
//	String pattern;
//	Map<Individual,Map<Ordinal,Map<String,Object>>> valuesByIndividuals;
	
//	Map<Individual,Map<Ordinal,Relation>> relationsByIndividuals;
//	Map<Ordinal,Partition<RelationSequence>> referentChainCensus;
//	Map<Ordinal,Partition<RelationSequence>> referentKinCensus;
//	String localUnitLabel;
//	String dateLabel;
/*	String placeLabel;
	String relationModelName;
	String startDateLabel;
	String endDateLabel;
	String egoRoleName;*/
	
  public RelationSequenceStatistics (Segmentation segmentation, Sequences sequences, SequenceCriteria criteria, List<String> indicators){

		super(segmentation, sequences,criteria,indicators);
		
/*		this.affiliationLabel = criteria.getGroupAffiliationLabel();
		this.localUnitLabel = criteria.getLocalUnitLabel();
//		this.relationsByIndividuals = new TreeMap<Individual,Map<Ordinal,Relation>>();
		this.dateLabel = criteria.getDateLabel();
		this.placeLabel = criteria.getPlaceLabel();
		this.relationModelName = criteria.getRelationModelName();
		this.startDateLabel = criteria.getStartDateLabel();
		this.endDateLabel = criteria.getEndDateLabel();
		this.egoRoleName = criteria.getEgoRoleName();
		
		for (RelationSequence house : sequences.toSortedList()){
			
			Map<Ordinal,Map<String,Object>> houseValues = new TreeMap<Ordinal,Map<String,Object>>();
			valuesBySequences.put(house, houseValues);

			for (Ordinal time : times){
				
				houseValues.put(time, new HashMap<String,Object>());
			}
		}*/
	}
	
/*	public void getStatistics(){
		
		for (RelationSequence sequence : sequences.toSortedList()){
			
			Map<Ordinal,Map<String,Object>> houseValues = valuesBySequences.get(sequence);
			
			for (Ordinal time : times){
				
				Relation relation = sequence.getStation(time);
				houseValues.get(time).putAll(RelationWorker.getStatistics(relation, indicators, pattern));
					
			}
		}
	}
	
	public void getMemberValues(Segmentation segmentation){
		
		for (Individual member : sequences.individuals(segmentation).toSortedList()){
			
			Map<Ordinal,Map<String,Value>> memberValues = new TreeMap<Ordinal,Map<String,Value>>();
			Map<Ordinal,Relation> memberRelations = new TreeMap<Ordinal,Relation>();
			
			relationsByIndividuals.put(member, memberRelations);
			valuesByIndividuals.put(member, memberValues);

			Ordinal previousTime = null;
			
			for (Ordinal time : times){
				
				memberValues.put(time, new HashMap<String,Value>());
				// Warning! supposes that there is only one relation by year...
				Relation relation = member.relations().getByModelName(relationModelName).getByTime(dateLabel, time.getYear()).getFirst();
				memberRelations.put(time, relation);
				
				// Extrapolation of previous stations 
				if (relation!=null && previousTime!=null && memberRelations.get(previousTime)==null){
					// Warning! supposes that there is only one relation by year...
					Relation previousRelation = member.relations().getByModelName(relationModelName).getPredecessors(relation, member, egoRoleName, dateLabel, startDateLabel, endDateLabel, previousTime.getYear()).getFirst();
					if (previousRelation!=null){
						memberRelations.put(previousTime, previousRelation);
					} 
				}
				
				previousTime = time;
			}
		}

		
		for (Individual member : sequences.individuals(segmentation).toSortedList()){
			
			Map<Ordinal,Map<String,Value>> memberValues = valuesByIndividuals.get(member);
			Map<Ordinal,Relation> memberRelations = relationsByIndividuals.get(member);
			
			
			for (Ordinal time : times){
				
				Map<String,Value> valueMap = new HashMap<String,Value>();

				String lifeStatus = IndividualValuator.lifeStatusAtYear(member, time.getYear());
				
				Relation relation = memberRelations.get(time);
				if (relation==null){
					if (time.getYear().toString().equals(member.getAttributeValue("BIRT_DATE"))){
						lifeStatus = "UNBORN";
					} else if (time.getYear().toString().equals(member.getAttributeValue("DEAT_DATE"))){
						lifeStatus = "DEAD";
					}
				}
				
				for (String indicator : indicators){
					if (lifeStatus.equals("DEAD") || lifeStatus.equals("UNBORN")){
						valueMap.put(indicator, new Value(lifeStatus));
					} else if (relation!=null){
						if (indicator.equals("PLACE")){
							String placeValue = relation.getAttributeValue(localUnitLabel);
							if (placeValue==null){
								placeValue = relation.getAttributeValue(placeLabel);
							}
							valueMap.put(indicator, Value.valueOf(placeValue));
						} else {
							Actor actor = relation.getActor(member, egoRoleName);
							if (indicator.equals("REFERENT")){
								valueMap.put(indicator, Value.valueOf(actor.getReferent()));
							} else if (indicator.equals("REFERENT_KIN")){
								valueMap.put(indicator, Value.valueOf(RelationWorker.getReferentRole(actor, pattern, affiliationLabel, relation)));
							} else if (indicator.equals("REFERENT_CHAIN")){
								valueMap.put(indicator, Value.valueOf(RelationWorker.getReferentChainGenderString(actor, affiliationLabel, relation)));
							} else if (indicator.equals("REFERENT_KIN_TYPE")){
								valueMap.put(indicator, Value.valueOf(RelationWorker.getReferentRoleShort(actor, pattern, affiliationLabel, relation)));
							} else if (indicator.equals("REFERENT_CHAIN_TYPE")){
								valueMap.put(indicator, Value.valueOf(RelationWorker.getReferentChainNumber(actor, relation)));
							}
						}
					}
				}
				memberValues.put(time, valueMap);
			}
		}
	}
	
	public void getReferentKinCensus(){
		
		for (House house : houses.toSortedList()){
			
			Map<Ordinal,Map<String,Object>> houseValues = valuesByHouses.get(house);
			
			for (Integer year : dates){
				
				Ordinal time = new Ordinal(year);
				Relation relation = house.getByOrdinal(time);
				houseValues.get(time).putAll(RelationWorker.getReferentKinCensus(relation, pattern, affiliationLabel));
				
				for (String indicator : indicators){
					if (!houseValues.get(time).containsKey(indicator)){
						houseValues.get(time).put(indicator, 0.);
					}
				}
					
			}
		}
	}*/
	
/*	public Map<Ordinal,Partition<RelationSequence>>  getReferentChainCensus(){
		
		if (referentChainCensus==null){
			referentChainCensus = new TreeMap<Ordinal,Partition<RelationSequence>>();
			
			for (Ordinal time : times){

				referentChainCensus.put(time, new Partition<RelationSequence>());
			}
				
			for (RelationSequence house : sequences.toSortedList()){
				
				Map<Ordinal,Map<String,Value>> houseValues = valuesBySequences.get(house);
				
				for (Ordinal time : times){
					
					Relation relation = house.getStation(time);
					Map<String,Value> map = RelationWorker.getReferentChainCensus(relation, affiliationLabel);
					houseValues.get(time).putAll(map);
					
					for (String indicator : houseValues.get(time).keySet()){
						if (!indicators.contains(indicator)){
							indicators.add(indicator);
						}
					}
					
					Partition<RelationSequence> chainCensus = referentChainCensus.get(time);
					Value referentChainType = map.get("Types");
					if (referentChainType!=null){
						chainCensus.put(house, referentChainType);
					}
				}
			}
		}
		
		return referentChainCensus;
	}
	
	public Map<Ordinal,Partition<RelationSequence>>  getReferentKinCensus(){
		
		if (referentKinCensus==null){
			referentKinCensus = new TreeMap<Ordinal,Partition<RelationSequence>>();
			
			for (Ordinal time : times){
				referentKinCensus.put(time, new Partition<RelationSequence>());
			}
				
			for (RelationSequence house : sequences.toSortedList()){
				
				Map<Ordinal,Map<String,Value>> houseValues = valuesBySequences.get(house);
				
				for (Ordinal time : times){
					
					Relation relation = house.getStation(time);
					Map<String,Value> map = RelationWorker.getReferentKinCensus(relation, pattern, affiliationLabel);
					houseValues.get(time).putAll(map);
					
					for (String indicator : houseValues.get(time).keySet()){
						if (!indicators.contains(indicator)){
							indicators.add(indicator);
						}
					}
					
					Partition<RelationSequence> kinCensus = referentKinCensus.get(time);
					Value referentKinType = map.get("Types");
					if (referentKinType!=null){
						kinCensus.put(house, new Value(referentKinType));
					}
				}
			}
		}
		
		return referentKinCensus;
	}
	
	public void getAllKinCensus(Segmentation segmentation, CensusCriteria censusCriteria){
		
		
		for (RelationSequence sequence : sequences.toSortedList()){
			
			Map<Ordinal,Map<String,Value>> sequenceValues = valuesBySequences.get(sequence);
			
			for (Ordinal time : times){
				
				Relation station = sequence.getStation(time);
				sequenceValues.get(time).putAll(RelationWorker.getAllKinCensus(segmentation, station, censusCriteria));
				
				for (String indicator : sequenceValues.get(time).keySet()){
					if (!indicators.contains(indicator)){
						indicators.add(indicator);
					}
				}
					
			}
		}
	}*/
	
/*	public void put(RelationSequence house, Ordinal time, String indicator, Object value){
		
		Map<Ordinal,Map<String,Object>> sequenceValues = valuesBySequences.get(house);
		
		if (sequenceValues != null){
			
			Map<String,Object> stationValues = sequenceValues.get(time);
			
			if (stationValues != null){
				
				stationValues.put(indicator,value);
			}
		}
	}
	
	private static Actors getNewActors (Relation alpha, Relation beta){
		Actors result;
		
		result = new Actors();
		
		if (alpha!=null){
			
			if (beta==null){
				
				result.addAll(alpha.actors());

			} else {
			
				for (Actor actor: alpha.actors()){
					if (!beta.actors().contains(actor)){
						result.add(actor);
					}
				}
			}
		}
		
		//
		return result;
	}
	
	public Partition<String> getFlows (String direction){
		Partition<String> result;
		
		result = new Partition<String>();
		
		int[] maxDegrees = ToolBox.stringsToInts(pattern);
		
		Ordinal former = null;
		for (Ordinal later : times){
			if (former!=null){
				
				Ordinal current = null;
				if (direction.equals("OUT")){
					current = former;
				} else if (direction.equals("IN")){
					current = later;
				}
				
				for (RelationSequence house : sequences){
					
					Relation formerRelation = house.getStation(former);
					Relation laterRelation = house.getStation(later);
					Relation currentRelation = house.getStation(current);
					
					Actors migrants = null;
					if (currentRelation != null){
						
						if (direction.equals("OUT")){
							migrants = currentRelation.getDifferentwActors(laterRelation);
						} else if (direction.equals("IN")){
							migrants = currentRelation.getDifferentwActors(formerRelation);
						}
						
						for (Actor actor : migrants){
							
							Individual referent = actor.getReferent();
							String link = "UNKNOWN";
							if (referent!=null){
								link = 	NetUtils.getAlterRole(actor.getIndividual(), referent, maxDegrees, null);
							}
							Individual otherReferent = null;
							Actor otherActor = RelationWorker.getClosestHomologue(currentRelation, actor, current.getYear(), dateLabel,direction);
							if (otherActor!=null){
								otherReferent = otherActor.getReferent();
							} else {
//								System.err.println("Missing homologue "+actor+" "+direction+" "+year);
							}
							
							String otherLink = "UNKNOWN";
							if (otherReferent!=null){
								otherLink = NetUtils.getAlterRole(actor.getIndividual(), otherReferent, maxDegrees, null);
							} 
							
							String change = null;
							if (referent!=null && referent.equals(otherReferent)){
								change = "IDENTICAL";
							} else if (direction.equals("OUT")){
								change = link+">"+otherLink;
							} else if (direction.equals("IN")){
								change = otherLink+">"+link;
							}
							result.put(house+"\t"+former+"-"+later+"\t"+direction+"\t"+actor.getIndividual()+"\t"+referent+"\t"+otherReferent, new Value(change));
						}

					}
					
				}
			}
			former = later;
		}

		//
		return result;
	}
	
	public Object get(RelationSequence house, Ordinal time, String indicator){
		Object result;
		
		result = null;
		
		Map<Ordinal,Map<String,Object>> houseValues = valuesBySequences.get(house);
		
		if (houseValues != null){
			
			Map<String,Object> relationValues = houseValues.get(time);
			
			if (relationValues != null){
				
				result = relationValues.get(indicator);
			}
		}
		
		//
		return result;
		
	}
	
	public Object getByMember(Individual member, Ordinal time, String indicator){
		Object result;
		
		result = null;
		
		Map<Ordinal,Map<String,Object>> memberValues = valuesByIndividuals.get(member);
		
		if (memberValues != null){
			
			Map<String,Object> relationValues = memberValues.get(time);
			
			if (relationValues != null){
				
				result = relationValues.get(indicator);
			}
		}
		
		//
		return result;
	}
	
	public Map<Ordinal,Partition<Individual>> getIndividualValueCensus (Segmentation segmentation, String indicator){
		Map<Ordinal,Partition<Individual>> result;
		
		result = new TreeMap<Ordinal,Partition<Individual>>();
		
		for (Ordinal time : times){
			result.put(time, new Partition<Individual>());
		}
		
		for (Individual member : sequences.individuals(segmentation)){
			
			for (Ordinal time : times){
				
				Object object = getByIndividual(member, time, indicator);

				if (object!=null){

					result.get(time).put(member, new Value(object));

				}
			}
		}
		//
		return result;
	}
	
	
	public Partition<Individual> getIndividualSequenceCensus (Segmentation segmentation, String indicator){
		Partition<Individual> result;
		
		result = new Partition<Individual>();
		
		for (Individual member : sequences.individuals(segmentation)){
			
			String sequence = "";
			
			for (Ordinal time: times){
				
				Value value = getByIndividual(member, time, indicator);
				
				if (value!=null){
					sequence += value +" ";
				} else {
					sequence += "_ ";
				}
			}
			result.put(member, new Value(sequence));
		}
		//
		return result;
	}
	
	public Map<String,Double> getMeanIndividualValueFrequencies(Segmentation segmentation, String indicator, Integer[] dates){
		Map<String,Double> result;
		
		result = new TreeMap<String,Double>();
		
		for (Individual member : sequences.individuals(segmentation)){
			
			for (int i=0;i<dates.length;i++){
				
				Object object = getByIndividual(member, new Ordinal(dates[i]), indicator);

				if (object!=null){
					String value = object.toString();
					Double count = result.get(value);
					if (count==null){
						count = 1.;
					} else {
						count += 1.;
					}
					result.put(value,count);
				}
			}
		}
		
		for (String value : result.keySet()){
			if (result.get(value)==null){
				result.put(value,result.get(value)/new Double(dates.length));
			}
		}
		//
		return result;
	}

	
	public Matrix getHouseTransitionMatrix(Map<Ordinal,Partition<RelationSequence>> census, Integer[] dates){
		Matrix result;
		
		Map<String,Map<String,Integer>> transitionMap = new TreeMap<String,Map<String,Integer>>();
		List<String> values = new ArrayList<String>();
		
		for (RelationSequence house : sequences){
			
			for (int i=1;i<dates.length;i++){
				
				Value object1 = census.get(new Ordinal((dates[i-1]))).getValue(house);
				Value object2 = census.get(new Ordinal((dates[i]))).getValue(house);
				
				if (object1!=null && object2!=null){

					String value1 = object1.toString();
					String value2 = object2.toString();
					
					if (!values.contains(value1)){
						values.add(value1);
					}
					if (!values.contains(value2)){
						values.add(value2);
					}
					
					Map<String,Integer> targetMap = transitionMap.get(value1);
					if (targetMap==null){
						targetMap = new TreeMap<String,Integer>();
						transitionMap.put(value1,targetMap);
					}

					Integer count = targetMap.get(value2);
					if (count==null){
						count = 1;
					} else {
						count += 1;
					}
					targetMap.put(value2,count);
				}
			}
		}
		
		Collections.sort(values);
		String[] labels = new String[values.size()];
		for (int i=0;i<values.size();i++){
			labels[i] = values.get(i)+"";
		}
		
		result = new Matrix(values.size(),values.size());
		result.setRowLabels(labels);
		result.setColLabels(labels);
		
		for (Object value1 : transitionMap.keySet()){
			
			Map<String,Integer> targetMap = transitionMap.get(value1);

			for (Object value2 : targetMap.keySet()){
				
				result.augment(values.indexOf(value1), values.indexOf(value2), targetMap.get(value2));

			}
		}
		
		//
		return result;
	}


	public Matrix getIndividualTransitionMatrix(Segmentation segmentation, String indicator){
		Matrix result;
		
		Map<String,Map<String,Integer>> transitionMap = new TreeMap<String,Map<String,Integer>>();
		List<String> values = new ArrayList<String>();
		
		for (Individual member : sequences.individuals(segmentation)){
			
			for (int i=1;i<times.size();i++){
				
				Value object1 = getByIndividual(member, times.get(i-1), indicator);
				Value object2 = getByIndividual(member, times.get(i), indicator);
				
				if (object1!=null && object2!=null){

					String value1 = object1.toString();
					String value2 = object2.toString();
					
					if (!values.contains(value1)){
						values.add(value1);
					}
					if (!values.contains(value2)){
						values.add(value2);
					}
					
					Map<String,Integer> targetMap = transitionMap.get(value1);
					if (targetMap==null){
						targetMap = new TreeMap<String,Integer>();
						transitionMap.put(value1,targetMap);
					}

					Integer count = targetMap.get(value2);
					if (count==null){
						count = 1;
					} else {
						count += 1;
					}
					targetMap.put(value2,count);
				}
			}
		}
		
		Collections.sort(values);
		String[] labels = new String[values.size()];
		for (int i=0;i<values.size();i++){
			labels[i] = values.get(i)+"";
		}
		
		result = new Matrix(values.size(),values.size());
		result.setRowLabels(labels);
		result.setColLabels(labels);
		
		for (Object value1 : transitionMap.keySet()){
			
			Map<String,Integer> targetMap = transitionMap.get(value1);

			for (Object value2 : targetMap.keySet()){
				
				result.augment(values.indexOf(value1), values.indexOf(value2), targetMap.get(value2));

			}
		}
		
		//
		return result;
	}
	
		public String getTrend(RelationSequence house, String indicator){
		String result;
		
		result = null;
		Object lastValue = null;
		
		for (Ordinal time : times){
			
			Object value = getBySequence(house,time,indicator);

			if (lastValue != null){

				if (!(lastValue instanceof Number) || !(value instanceof Number)){
					
					break;
					
				} else {
				
					System.out.println(value +" "+lastValue);
					
					int comp = ((Comparable)value).compareTo(lastValue);
					String trend = null;
					
					if (comp < 0){
						trend = "DECLINING";
					} else if (comp > 0) {
						trend = "AUGMENTING";
					} else if (comp == 0){
						trend = "CONSTANT";
					}
					
					if (result == null || result.equals("CONSTANT")){
						result = trend;
					} else if (!trend.equals("CONSTANT") && !trend.equals(result)){
						result = "VARIABLE";
						break;
					} 
				}
			}

			lastValue = value;
				
		}
		
		//
		return result;
	}
	
	public Double meanOverSequences (Ordinal time, String indicator){
		Double result;
		
		result = null;
		Double sum = 0.;
		
		for (RelationSequence house : sequences){
			
			Object value = getBySequence(house,time,indicator);
			
			if (value instanceof Number){
				
				sum += ((Number)value).doubleValue();
				
			} else {
				
				result = null;
				break;
			}
		}
		
		result = sum/new Double(sequences.size());
		
		//
		return result;
	}*/
	

	

}
