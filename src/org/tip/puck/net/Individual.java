package org.tip.puck.net;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.IndividualComparator.Sorting;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.statistics.StatisticsWorker;
import org.tip.puck.util.Numberable;
import org.tip.puck.util.NumberedIntegers;
import org.tip.puck.util.Trafo;

/**
 * 
 * 
 * @author TIP
 */
public class Individual implements Comparable<Individual>, Numberable, Attributable, Individualizable {

	private int id;
	private Gender gender;
	private String name;
	private Family originFamily;
	private final Families personalFamilies;
	private final Attributes attributes;
	private final Relations relations;
//	private Map<String, Individuals> relations;
	private Integer birthOrder;

	/**
	 * 
	 */
	public Individual(final int id) {
		this.id = id;
		this.gender = Gender.UNKNOWN;
		this.personalFamilies = new Families();
		this.relations = new Relations();
		this.attributes = new Attributes();
	}

	/**
	 * 
	 */
	public Individual(final int id, final String name, final Gender gender) {
		this.id = id;
		this.setName(name);
		if (gender == null) {
			this.gender = Gender.UNKNOWN;
		} else {
			this.gender = gender;
		}
		this.personalFamilies = new Families();
		this.relations = new Relations();
		this.attributes = new Attributes();
	}

	/**
	 * 
	 * @param family
	 */
	public void addPersonalFamily(final Family family) {
		this.personalFamilies.add(family);
	}

	/**
	 * 
	 * @param label
	 * @param alter
	 */
/*	public void addRelation(final String label, final Individual alter) {
		if (this.relations == null) {
			this.relations = new HashMap<String, Individuals>();
		}
		if (this.relations.get(label) == null) {
			this.relations.put(label, new Individuals());
		}
		this.relations.get(label).add(alter);
	}*/

	/**
	 * 
	 * @param other
	 */
	public void adjust(final Individual other) {

		this.name = other.getName();
		this.gender = other.getGender();
		for (Attribute attribute : other.attributes().values()) {
			this.attributes.put(attribute.getLabel(), new Attribute(attribute.getLabel(), attribute.getValue()));
		}

	}
	
	public Individual getEgo(){
		return this;
	}

	public Attributes attributes() {
		return this.attributes;
	}

	public Individuals bilateralCousins(final FiliationType filiation, final boolean cross) {
		Individuals result;

		result = new Individuals();
		for (Individual paternal : firstCousins(Gender.MALE, filiation, cross)) {
			for (Individual maternal : firstCousins(Gender.FEMALE, filiation, cross)) {
				if (paternal.equals(maternal)) {
					result.add(paternal);
				}
			}
		}
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individuals children() {
		Individuals result;

		result = this.personalFamilies.children();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Individual clone() {
		Individual result;

		result = new Individual(this.id, this.name, this.gender);

		//
		return result;
	}
	
	public Individual clone(int id){
		Individual result;
		
		result = clone();
		result.setId(id);
		
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individual cloneWithAttributes() {
		Individual result;

		result = new Individual(this.id, this.name, this.gender);

		for (Attribute attribute : this.attributes().values()) {
			result.attributes().put(attribute.getLabel(), new Attribute(attribute.getLabel(), attribute.getValue()));
		}

		//
		return result;
	}

	@Override
	public int compareTo(final Individual other) {
		int result;

		if (other == null) {
			result = 1;
		} else {
			result = new Integer(getId()).compareTo(other.getId());
		}

		//
		return result;
	}

	public Individuals coSpouses() {
		Individuals result;

		//
		result = new Individuals();

		for (Individual spouse : spouses()) {
			for (Individual coSpouse : spouse.spouses()) {
				if (coSpouse != this) {
					result.add(coSpouse);
				}
			}
		}
		//
		return result;

	}

	public Individuals crossSexCousins(final FiliationType type) {
		Individuals result;

		result = new Individuals();

		for (Individual parent : getParents()) {
			if (parent.hasLinkingGender(type)) {
				for (Individual grandparent : parent.getParents()) {
					if (grandparent.hasLinkingGender(type) && !grandparent.isSterile()) {
						for (Individual oblique : grandparent.children()) {
							if (oblique.hasLinkingGender(type) && oblique != parent && !oblique.isSterile()) {
								for (Individual cousin : oblique.children()) {
									if (cousin.getGender() != getGender()) {
										result.put(cousin);
									}
								}
							}
						}
					}
				}
			}
		}
		//
		return result;

	}

	/**
	 * 
	 * @return
	 */
	public int depth() {
		int result;

		result = StatisticsWorker.depth(this, new NumberedIntegers(1000), FiliationType.COGNATIC);

		//
		return result;
	}

	public Individuals doubleCrossCousins(final Gender gender, final FiliationType filiation) {
		Individuals result;

		result = new Individuals();
		for (Individual cousin : firstCousins(gender.invert(), FiliationType.COGNATIC, true)) {
			Individual otherGrandParent = getParent(gender.invert()).getParent(filiation);
			Individual cousinsOtherGrandParent = cousin.getParent(gender).getParent(filiation);
			for (Individual grandOblique : otherGrandParent.siblings(FiliationType.COGNATIC)) {
				if (grandOblique.equals(cousinsOtherGrandParent)) {
					result.add(cousin);
				}
			}
		}
		//
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		boolean result;

		result = obj != null && getId() == ((Individual) obj).getId();

		//
		return result;
	}

	public Individuals firstCousins() {
		Individuals result;

		result = new Individuals();
		for (Individual parent : getParents()) {
			for (Individual auntOrUncle : parent.siblings()) {
				for (Individual cousin : auntOrUncle.children()) {
					result.add(cousin);
				}
			}
		}

		//
		return result;
	}

	public Individuals firstCousins(final Gender parentGender, final FiliationType filiation, final boolean cross) {
		Individuals result;

		result = new Individuals();

		Individual parent = getParent(parentGender);
		if (parent != null) {
			for (Individual auntOrUncle : parent.siblings(filiation)) {
				if ((auntOrUncle.getGender() != parentGender == cross) || !auntOrUncle.isSterile()) {
					for (Individual cousin : auntOrUncle.children()) {
						result.add(cousin);
					}
				}
			}
		}
		//
		return result;
	}

	public Individuals fullSameSexSiblings() {
		Individuals result;

		result = this.originFamily.getChildren(getGender());
		result.removeById(getId());

		//
		return result;
	}

	public Individuals fullSiblings() {
		Individuals result;

		result = new Individuals();

		if (this.originFamily!=null){
			result.add(this.originFamily.getChildren());
			result.removeById(getId());
		}

		//
		return result;
	}

	public Families getActiveUnionsAt(final int year) {
		Families result;

		result = new Families();
		for (Family family : this.personalFamilies) {
			if (family.isActiveAt(year)) {
				result.add(family);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public String getAttributeValue(final String label) {
		String result;

		result = this.attributes.getValue(label);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Integer getBirthOrder() {
		return this.birthOrder;
	}
	
	public AlterAge getRelativeAge(Individual alter){
		AlterAge result;

		result = AlterAge.UNKNOWN;

		if (fullSiblings().contains(alter) && this.birthOrder!=null && alter.birthOrder!=null){
			
			if (this.birthOrder < alter.birthOrder){
				
				result = AlterAge.ELDER;
				
			} else if (this.birthOrder > alter.birthOrder) {
				
				result = AlterAge.YOUNGER;
				
			}
		}
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individual getFather() {
		Individual result;

		if (this.originFamily == null) {
			result = null;
		} else {
			result = this.originFamily.getFather();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param name
	 */
	public String getFirstName() {
		String result;

		if (this.name == null) {
			result = null;
		} else {
			int slashIndex = this.name.indexOf('/');
			if (slashIndex == -1) {
				result = this.name.trim();
			} else {
				result = this.name.substring(0, slashIndex).trim();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public Gender getGender() {
		Gender result;

		result = this.gender;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getId() {
		int result;

		result = this.id;

		//
		return result;
	}

	public Individuals getInlaws(final FiliationType filiation) {
		Individuals result;

		result = new Individuals();
		for (Individual sibling : siblings(filiation)) {
			if (!sibling.hasLinkingGender(filiation)) {
				for (Individual inLaw : sibling.spouses()) {
					result.add(inLaw);
				}
			}
		}
		for (Individual spouse : spouses()) {
			for (Individual inLaw : spouse.siblings(filiation)) {
				result.add(inLaw);
			}
		}
		return result;
	}

	public Individuals getKin() {
		Individuals result;

		result = new Individuals();

		for (KinType type : KinType.basicTypes()) {
			result.add(getKin(type));
		}

		//
		return result;
	}
	

	/**
	 * FIXME TO BE REWROTE
	 * 
	 * @deprecated
	 * @param key
	 * @return
	 */
	@Deprecated
	public Individuals getKin(final int key) {
		Individuals result;

		switch (key) {
			case -1:
				result = children();
			break;
			case 0:
				result = spouses();
			break;
			case 1:
				result = getParents();
			break;
			default:
				result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param type
	 * @return
	 */
	public Individuals getKin(final KinType type) {
		Individuals result;

		switch (type) {
			case CHILD:
				result = children();
			break;
			case SPOUSE:
				result = spouses();
			break;
			case PARENT:
				result = getParents();
			break;
			case SIBLING:
				result = fullSiblings();
			break;
			default:
				result = new Individuals();
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @param type
	 * @return
	 */
	public Individuals getKin(final KinType type, final Gender alterGender) {
		Individuals result;

		result = new Individuals();
		
		for (Individual alter : getKin(type)){
			if (alter.getGender().specifies(alterGender)){
				result.add(alter);
			}
		}
		//
		return result;
	}
	
	/**
	 * 
	 * @param type
	 * @return
	 */
	public Individuals getPotentialKin(final KinType type) {
		Individuals result;

		switch (type) {
			case CHILD:
				result = children();
				for (Individual spouse : getPartners()){
					for (Individual stepChild : spouse.children()){
						result.addNew(stepChild);
					}
				}
				for (Individual child : children()){
					for (Individual childSibling : child.getRelated("SIBLING")){
						result.addNew(childSibling);
					}
				}
				break;
			case SPOUSE:
				result = getPartners();
				for (Individual child : children()){
					for (Individual childsParent : child.getParents()){
						result.addNew(childsParent);
					}
				}
				for (Individual spouse : getPartners()){
					for (Individual siblingInLaw : spouse.getRelated("SIBLING")){
						result.addNew(siblingInLaw);
					}
				}
				for (Individual sibling : getRelated("SIBLING")){
					for (Individual siblingInLaw : sibling.getPartners()){
						result.addNew(siblingInLaw);
					}
				}
				break;
			case PARENT:
				result = getParents();
				for (Individual parent : getParents()){
					for (Individual stepParent : parent.getPartners()){
						result.addNew(stepParent);
					}
				}
				for (Individual sibling : getRelated("SIBLING")){
					for (Individual siblingParent : sibling.getParents()){
						result.addNew(siblingParent);
					}
				}
				break;
			case SIBLING:
				result = getRelated("SIBLING");
				for (Individual parent : getParents()){
					for (Individual parentChild: parent.children()){
						result.addNew(parentChild);
					}
				}
				for (Individual sibling: getRelated("SIBLING")){
					for (Individual siblingsSibling : sibling.getRelated("SIBLING")){
						if (!this.equals(sibling)){
							result.addNew(siblingsSibling);
						}
					}
				}
				break;
			default:
				result = new Individuals();
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @param kinType
	 * @return
	 */
	public Individuals getKin(final KinType kinType, final FiliationType filiationType) {
		Individuals result;

		switch (kinType) {
			case CHILD:
				if (filiationType.hasLinkingGender(this.gender)) {
					result = children();
				} else {
					result = new Individuals();
				}
			break;
			case SPOUSE:
				result = spouses();
			break;
			case PARENT:
				if (filiationType == FiliationType.COGNATIC) {
					result = getParents();
				} else {
					result = new Individuals();
					if (filiationType == FiliationType.AGNATIC) {
						result.add(getFather());
					} else if (filiationType == FiliationType.UTERINE) {
						result.add(getMother());
					}
				}
			break;
			default:
				result = new Individuals();
		}

		//
		return result;
	}

	public Individual getKin(final KinType type, final Sorting sorting, final int order) {
		Individual result;

		result = null;
		Individuals kin = getKin(type);
		if (order <= kin.size()) {
			result = kin.toSortedList(sorting).get(order - 1);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param name
	 */
	public String getLastName() {
		String result;

		if (this.name == null) {
			result = null;
		} else {
			int slashIndex = this.name.indexOf('/');
			if (slashIndex == -1) {
				result = null;
			} else {
				result = this.name.substring(slashIndex + 1).replaceAll("/", " ").trim();
				if (result.length() == 0) {
					result = null;
				}
			}
		}

		//
		return result;
	}

	public Individuals getMarriedSpouses() {
		Individuals result;

		result = this.personalFamilies.marriedSpouses(this);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individual getMother() {
		Individual result;

		if (this.originFamily == null) {
			result = null;
		} else {
			result = this.originFamily.getMother();
		}

		//
		return result;
	}

	public String getName() {
		String result;

		result = this.name;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */

	public String getNamePart(final int subnames) {
		String result;

		if (subnames == 0) {
			result = this.name;
		} else {
			String[] splitName = this.name.split("\\/");
			result = "";
			for (int i = 0; i < Math.min(subnames, splitName.length); i++) {
				result = result + " " + splitName[i];
			}
		}

		result = Trafo.trim(result);

		//
		return result;
	}

	public String getNameTrimmed() {
		String result;

		result = this.name;

		if (result != null) {
			result = result.trim();
		}

		//
		return result;

	}

	/**
	 * 
	 * @return
	 */
	public Family getOriginFamily() {
		return this.originFamily;
	}

	/**
	 * 
	 * @param parent
	 * @return
	 */
	public Individual getOtherParent(final Individual parent) {
		Individual result;

		if (this.originFamily == null) {
			result = null;
		} else {
			result = this.originFamily.getOtherParent(parent);
		}

		//
		return result;
	}

	public Individual getParent(final FiliationType filiation) {
		Individual result;

		if (filiation == null) {

			throw new IllegalArgumentException("Null parameter.");

		} else {
			switch (filiation) {
				case AGNATIC:
					result = getFather();
				break;
				case UTERINE:
					result = getMother();
				break;
				default:
					result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param gender
	 * @return
	 */
	public Individual getParent(final Gender gender) {
		Individual result;

		if (gender == null) {

			throw new IllegalArgumentException("Null parameter.");

		} else {
			switch (gender) {
				case MALE:
					result = getFather();
				break;
				case FEMALE:
					result = getMother();
				break;
				default:
					result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	@Deprecated
	public Individual getParent(final int key) {
		Individual result = null;

		switch (key) {
			case 0:
				result = getFather();
			case 1:
				result = getMother();
		}
		//
		return result;

	}

	/**
	 * 
	 * @return
	 */
	public Individuals getParents() {
		Individuals result;

		result = new Individuals();
		if (this.originFamily != null) {
			if (this.originFamily.getHusband() != null) {
				result.add(this.originFamily.getHusband());
			}
			if (this.originFamily.getWife() != null) {
				result.add(this.originFamily.getWife());
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individuals getParents(final FiliationType line) {
		Individuals result;

		result = new Individuals();
		if (this.originFamily != null) {
			if (line.hasLinkingGender(Gender.MALE) && this.originFamily.getHusband() != null) {
				result.add(this.originFamily.getHusband());
			}
			if (line.hasLinkingGender(Gender.FEMALE) && this.originFamily.getWife() != null) {
				result.add(this.originFamily.getWife());
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param spouse
	 * @return
	 */
	public Individuals getPartners() {
		Individuals result;

		result = this.personalFamilies.getPartners(this);

		//
		return result;
	}

	public Families getPersonalFamilies() {
		return this.personalFamilies;
	}
	
	public Individuals getRelated(final String relationModelName) {
		return getRelated(relationModelName,null,null);
	}


	public Individuals getRelated(final String relationModelName, String egoRoleName, String alternativeEgoRoleName) {
		Individuals result;

		result = new Individuals();
		Relations relations = relations();
		if (!StringUtils.isBlank(relationModelName)){
			relations = relations.getByModelName(relationModelName);
		}

		for (Relation relation : relations) {
			
			if ((StringUtils.isBlank(egoRoleName) || relation.getRoleNames(this).contains(egoRoleName)) ||
				(!StringUtils.isBlank(alternativeEgoRoleName) && relation.getRoleNames(this).contains(alternativeEgoRoleName)))	{

				for (Individual individual : relation.getIndividuals()) {
					if (individual != this){
						result.put(individual);
					}
				}
				for (Actor actor : relation.actors().getByIndividual(this)){
					Individual individual = actor.getReferent();
					if (individual!=null && individual!=this){
						result.put(individual);
					}
				}
			}
		}

		//
		return result;
	}

	public Individuals getRelated(final String relationType, final String egoRoleName, final String alterRoleName, final String attributeLabel,
			final Integer time) {
		Individuals result;

		result = new Individuals();

		for (Relation relation : relations().getByModelName(relationType)) {
			if (relation.hasActor(this, egoRoleName) && relation.hasAttributeValue(attributeLabel) && relation.hasTime("TIME", time)) {
				for (Individual alter : relation.getIndividuals()) {
					if (alter != this && relation.hasActor(alter, alterRoleName)) {
						result.put(alter);
					}
				}
			}
		}

		//
		return result;
	}

	public String getSmoothName() {
		String result;

		if (this.name == null) {
			result = null;
		} else {
			result = this.name.trim().replaceAll(" / ", " ");
		}
		//
		return result;
	}

	public Individuals getSpousesAt(final int year) {
		Individuals result;

		result = getActiveUnionsAt(year).getPartners(this);

		//
		return result;
	}

	public String getTrimmedName() {
		String result;

		if (this.name == null) {
			result = null;
		} else {
			result = this.name.trim();
		}
		//
		return result;
	}

	public Families getUnions(final boolean married) {
		Families result;

		result = new Families();
		
		for (Family family : this.personalFamilies) {
		
			if (family.hasMarried() || !married) {
			
				result.add(family);
			}
		}
		return result;
	}

	/**
	 * 
	 * @param label
	 * @param value
	 * @return
	 */
	public boolean hasAttributeValue(final String label, final String value) {
		boolean result;

		result = this.attributes.contains(label, value);

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int hashCode() {
		return this.id;
	}

	/**
	 * 
	 */
	@Override
	public String hashKey() {
		return this.id + "";
	}

	/**
	 * 
	 * @param type
	 * @return
	 */
	public boolean hasLinkingGender(final FiliationType type) {
		boolean result;

		if (type == null) {

			throw new IllegalArgumentException("Null parameter.");

		} else {
			switch (type) {
				case COGNATIC:
					result = true;
				break;
				case AGNATIC:
					result = isMale();
				break;
				case UTERINE:
					result = isFemale();
				break;
				default:
					result = false;
			}
		}

		//
		return result;
	}

	public boolean hasNameConsistentWith(final String firstName, final String lastName) {
		boolean result;

		result = ((StringUtils.isBlank(firstName) || firstName.equals(getFirstName())) && (StringUtils.isBlank(lastName) || lastName.equals(getLastName())));

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int height() {
		int result;

		result = height(new NumberedIntegers(1000));

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int height(final NumberedIntegers depthData) {
		int result;

		Individuals children = this.children();
		if (children.size() == 0) {
			result = 0;
		} else {
			// Calculate the max of children depth.
			result = 0;
			for (Individual child : children) {
				Integer childDepth = depthData.get(child.getId());
				if (childDepth == null) {
					childDepth = child.height(depthData);
					depthData.put(child.getId(), childDepth);
				}

				result = Math.max(result, childDepth);
			}

			//
			result += 1;
		}

		//
		return result;
	}

	public boolean isBrotherOf(final Individual source) {
		boolean result;

		result = this.isMale() && this.isSiblingOf(source);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isChildOf(final Individual parent) {
		boolean result;

		if ((this.originFamily == null)) {
			//
			result = false;

		} else {
			//
			result = this.originFamily.isParent(parent);
		}

		//
		return result;
	}

	public boolean isElderBrotherOf(final Individual source) {
		boolean result;

		result = this.isBrotherOf(source) && this.getBirthOrder() < source.getBirthOrder();

		//
		return result;
	}

	public boolean isElderSisterOf(final Individual source) {
		boolean result;

		result = this.isSisterOf(source) && this.getBirthOrder() < source.getBirthOrder();

		//
		return result;
	}

	public boolean isElderThan(final Individual alter) {
		boolean result;

		result = (alter != null && this.birthOrder != null && alter.birthOrder != null && this.birthOrder < alter.birthOrder);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isFemale() {
		boolean result;

		result = this.gender.isFemale();

		//
		return result;
	}

	/**
	 * Checks whether the individual is with children
	 * 
	 * @return true if the individual has no children, false otherwise.
	 */
	public boolean isFertile() {
		boolean result;

		result = this.personalFamilies.containsChild();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isHalfOrphan() {
		boolean result;

		if ((this.originFamily == null) || ((this.originFamily.getHusband() != null) && (this.originFamily.getWife() != null))) {
			result = false;
		} else {
			result = true;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isMale() {
		boolean result;

		result = this.gender.isMale();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotOrphan() {
		boolean result;

		result = !isOrphan();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotOrphanOfFather() {
		boolean result;

		result = !isOrphanOfFather();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotOrphanOfMother() {
		boolean result;

		result = !isOrphanOfMother();

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public boolean isNotPartnerWith(final Individual source) {
		boolean result;

		result = !isPartnerWith(source);

		//
		return result;
	}

	/**
	 * Checks whether the individual is single.
	 * 
	 * @return true if the individual has no spouses, false otherwise.
	 */
	public boolean isNotSingle() {
		boolean result;

		result = !isSingle();

		//
		return result;
	}
	
	public boolean isIsolate(){
		boolean result;
		
		result = isOrphan() && isSingle() && isSterile();
		
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isOrphan() {
		boolean result;

		if ((this.originFamily == null) || this.originFamily.isOrphan()) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isOrphanOfFather() {
		boolean result;

		if ((this.originFamily == null) || ((this.originFamily.getFather() == null) && (this.originFamily.getMother() != null))) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isOrphanOfMother() {
		boolean result;

		if ((this.originFamily == null) || ((this.originFamily.getFather() != null) && (this.originFamily.getMother() == null))) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public boolean isPartnerWith(final Individual source) {
		boolean result;

		//
		Family target = this.personalFamilies.getBySpouses(this, source);

		//
		if (target == null) {
			result = false;
		} else {
			result = true;
		}

		//
		return result;
	}

	public boolean isSiblingOf(final Individual source) {
		boolean result;

		result = (source.siblings() != null && source.siblings().contains(this));

		//
		return result;
	}

	/**
	 * Checks whether the individual is single.
	 * 
	 * @return true if the individual has no spouses, false otherwise.
	 */
	public boolean isSingle() {
		boolean result;

		result = this.getUnions(false).getPartners(this).size() == 0;

		//
		return result;
	}

	/**
	 * Checks whether the individual is single.
	 * 
	 * @return true if the individual has no spouses, false otherwise.
	 */
	public boolean isSingle(final String relationType) {
		boolean result;

		if (relationType.equals("SPOUSE")) {
			result = this.getUnions(true).getPartners(this).size() == 0;
		} else {
			result = this.getUnions(false).getPartners(this).size() == 0;
		}

		//
		return result;
	}

	public boolean isSisterOf(final Individual source) {
		boolean result;

		result = this.isFemale() && this.isSiblingOf(source);

		//
		return result;
	}

	/**
	 * Checks whether the individual is without children
	 * 
	 * @return true if the individual has no children, false otherwise.
	 */
	public boolean isSterile() {
		boolean result;

		result = !this.personalFamilies.containsChild();

		//
		return result;
	}

	public boolean isUnique() {
		boolean result;

		result = this.siblings().size() == 0;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isUnknown() {
		boolean result;

		result = this.gender.isUnknown();

		//
		return result;
	}

	/**
	 * Checks whether the individual is single.
	 * 
	 * @return true if the individual has no spouses, false otherwise.
	 */
	public boolean isUnmarried() {
		boolean result;

		result = this.getUnions(true).size() == 0;

		//
		return result;
	}

	public boolean isYoungerBrotherOf(final Individual source) {
		boolean result;

		result = this.isBrotherOf(source) && this.getBirthOrder() > source.getBirthOrder();

		//
		return result;
	}

	public boolean isYoungerSisterOf(final Individual source) {
		boolean result;

		result = this.isSisterOf(source) && this.getBirthOrder() > source.getBirthOrder();

		//
		return result;
	}

	public boolean isYoungerThan(final Individual alter) {
		boolean result;

		result = (alter != null && this.birthOrder != null && alter.birthOrder != null && this.birthOrder > alter.birthOrder);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int numberOfParents() {
		int result;

		if (this.originFamily == null) {
			//
			result = 0;

		} else {
			//
			result = this.originFamily.numberOfParents();
		}

		//
		return result;
	}

	public Individuals obliqueCousins(final Gender gender) {
		Individuals result;

		result = new Individuals();
		for (Individual crossCousin : firstCousins(gender.invert(), FiliationType.COGNATIC, true)) {
			for (Individual oblique : obliques(gender, gender.invert(), FiliationType.COGNATIC)) {
				if (crossCousin.equals(oblique) && !result.contains(crossCousin)) {
					result.add(crossCousin);
				}
			}
			if (crossCousin.equals(getParent(gender)) && !result.contains(crossCousin)) {
				result.add(crossCousin);
			}
		}
		//
		return result;
	}

	/**
	 * gets the nephews and nieces
	 */
	public Individuals obliques(final FiliationType filiation) {
		Individuals result;

		result = new Individuals();
		for (Individual sibling : siblings(filiation)) {
			if (sibling.hasLinkingGender(filiation)) {
				for (Individual auntOrUncle : sibling.children()) {
					result.add(auntOrUncle);
				}
			}
		}
		//
		return result;
	}

	/**
	 * gets the uncles and aunts (parents' siblings) of a vertex
	 * 
	 * @param parentGender
	 *            the gender of the linking parent (0 = F, 1 = M)
	 * @param alterGender
	 *            the gender of alter
	 * @param filiation
	 *            the character of the sibling relation (0=agnatic, 1=uterine,
	 *            2=indifferent)
	 * @return the list of uncles and aunts
	 */
	public Individuals obliques(final Gender parentGender, final Gender alterGender, final FiliationType filiation) {
		Individuals result;

		result = new Individuals();
		Individual parent = getParent(parentGender);
		if (parent != null) {
			for (Individual auntOrUncle : parent.siblings(filiation)) {
				if (auntOrUncle.getGender() != alterGender) {
					result.add(auntOrUncle);
				}
			}
		}
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Relations relations() {
		Relations result;

		result = this.relations;

		//
		return result;
	}
	
	public Relations relations (String relationModelName, String egoRoleName, String attributeLabel, String dateLabel, Integer date){
		Relations result;
		
		result = new Relations();
		
		for (Relation relation: relations().getByModelName(relationModelName)){
			if (relation.hasActor(this,egoRoleName) && relation.hasAttributeValue(attributeLabel) && relation.hasTime(dateLabel, date)){
				result.put(relation);
			}
		}
		//
		return result;
	}


	public void setAttribute(final String label, final String value) {
		this.attributes.put(label, value);
	}

	/**
	 * This method sets the birth order for this individual.
	 * 
	 * @param order
	 *            positive integer (> 0), any else value sets null.
	 */
	public void setBirthOrder(final Integer order) {
		//
		if ((order == null) || (order < 1)) {
			//
			this.birthOrder = null;

		} else {
			//
			this.birthOrder = order;
		}
	}

	/**
	 * By default, name is first name.
	 * 
	 * @param value
	 */
	public void setFirstName(final String value) {

		setName(value, this.getLastName());
	}

	/**
	 * 
	 * @param value
	 */
	public void setGender(final Gender value) {
		if (value == null) {
			this.gender = Gender.UNKNOWN;
		} else {
			this.gender = value;
		}
	}

	/**
	 * 
	 */
	@Override
	public void setId(final int id) {
		this.id = id;
	}

	/**
	 * By default, name is first name.
	 * 
	 * @param value
	 */
	public void setLastName(final String value) {

		setName(this.getFirstName(), value);
	}

	public boolean setMinimalAttributeValue(final String label, final String value) {
		boolean result;

		result = false;
		String oldStringValue = getAttributeValue(label);
		if (oldStringValue == null || Integer.parseInt(oldStringValue) > Integer.parseInt(value)) {
			setAttribute(label, value);
			result = true;
		}
		//
		return result;

	}

	/**
	 * 
	 * @param name
	 */
	public void setName(final String name) {
		if (name == null) {
			this.name = null;
		} else {
			this.name = name.trim();
		}
	}

	/**
	 * By default, name is first name.
	 * 
	 * @param value
	 */
	public void setName(final String firstName, final String lastName) {

		//
		String newName;
		if (firstName == null) {
			if (StringUtils.isBlank(lastName)) {
				newName = "";
			} else {
				newName = "/" + lastName.trim();
			}
		} else {
			if (StringUtils.isBlank(lastName)) {
				newName = firstName.trim();
			} else {
				newName = firstName.trim() + "/" + lastName.trim();
			}
		}

		//
		this.name = newName;
	}

	/**
	 * 
	 * @param sourceFamily
	 */
	public void setOriginFamily(final Family family) {
		this.originFamily = family;
	}

	/**
	 * gets the siblings that are at the same time cousins
	 */
	public Individuals siblingCousins(final FiliationType filiation) {
		Individuals result;

		result = new Individuals();

		FiliationType otherFiliation = filiation.invert();

		Individual otherParent = getParent(otherFiliation);
		if (otherParent != null) {
			for (Individual sibling : siblings(filiation)) {
				Individual stepParent = sibling.getParent(otherFiliation);
				if (otherParent.siblings(filiation).contains(stepParent)) {
					result.add(sibling);
				}
			}
		}
		//
		return result;
	}

	public Individuals siblings() {
		Individuals result;

		result = new Individuals();

		if (this.originFamily != null) {
			// get full siblings
			for (Individual sibling : this.originFamily.getChildren()) {
				result.add(sibling);
			}
			// get half-siblings
			for (Individual parent : getParents()) {
				for (Individual sibling : parent.children()) {
					if (!result.contains(sibling)) {
						result.add(sibling);
					}
				}
			}
		}
		//
		return result;
	}

	public Individuals siblings(final FiliationType filiation) {
		Individuals result;

		result = new Individuals();
		for (Individual parent : getParents()) {
			if (parent.hasLinkingGender(filiation)) {
				for (Individual sibling : parent.children()) {
					if (sibling != this) {
						result.add(sibling);
					}
				}
			}
		}
		//
		return result;
	}

	public String signature() {
		String result;

		result = this.id + " " + this.getSmoothName();

		//
		return result;
	}

	public String signatureTab() {
		String result;

		result = this.id + "\t" + this.getSmoothName();

		//
		return result;
	}

	/**
	 * Includes also ex-spouses but not unmarried partners
	 * 
	 * @return
	 */
	public Individuals spouses() {
		Individuals result;

		result = getUnions(true).getPartners(this);

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		result = this.getSmoothName() + " (" + this.id + ")";

		// result = String.format("[id=%d,name=%s]", this.id, this.name);

		//
		return result;
	}

}
