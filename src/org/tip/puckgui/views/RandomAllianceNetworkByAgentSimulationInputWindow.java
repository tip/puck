package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.random.DistributionType;
import org.tip.puck.graphs.random.RandomCriteria;
import org.tip.puck.graphs.random.RandomGraphMaker;
import org.tip.puck.graphs.random.RandomGraphReporter;
import org.tip.puck.graphs.workers.GraphReporter;
import org.tip.puck.matrix.MatrixStatistics;
import org.tip.puck.net.Individual;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.report.Report;
import org.tip.puckgui.GroupNetGUI;
import org.tip.puckgui.PuckGUI;
import org.tip.puckgui.WindowGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class RandomAllianceNetworkByAgentSimulationInputWindow extends JFrame {

	private static final long serialVersionUID = -439244697759332860L;
	private static final Logger logger = LoggerFactory.getLogger(RandomAllianceNetworkByAgentSimulationInputWindow.class);

	private JFrame thisJFrame;
	private JPanel contentPane;
	private JSpinner spinnerArcCount;
	private JSpinner spinnerNodeCount;
	private JSpinner spinnerRuns;
	private JSpinner spnrInertia0;
	private JSpinner spnrInertia1;
	private JSpinner spnrInertia2;
	private JSpinner spnrOutpreference;
	private JCheckBox chckbxExtract;
	private JCheckBox chckbxShowProbabilityEvolution;
	private JComboBox cbBoxDistribution;

	/**
	 * Similar with VirtualFielworkInputWindow.
	 */
	public RandomAllianceNetworkByAgentSimulationInputWindow(final WindowGUI gui) {

		// /////////////////////////////////
		setIconImage(Toolkit.getDefaultToolkit().getImage(
				RandomAllianceNetworkByAgentSimulationInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		setTitle("Random Alliance Network");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 365, 400);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});

		JButton btnRestoreDefaults = new JButton("Restore defaults");
		btnRestoreDefaults.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Restore Defaults.
				setCriteria(new RandomCriteria());
			}
		});
		buttonPanel.add(btnRestoreDefaults);
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		getRootPane().setDefaultButton(btnLaunch);
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					//
					RandomCriteria criteria = getCriteria();

					//
					PuckGUI.instance().getPreferences().setAgentSimulationCriteria(criteria);

					// Compute statistics (based on
					// RandomGraphReporter.reportRandomGraph(criteria,
					// runs);

					RandomGraphMaker<Cluster<Individual>> randomGraphMaker = new RandomGraphMaker<Cluster<Individual>>(criteria);

					List<Graph<Cluster<Individual>>> graphs = randomGraphMaker.createRandomGraphsByAgentSimulation(criteria.getRunCount());

					System.out.println("Graphs constructed");

					double[][] probabilityEvolution;
					if (criteria.isShowProbabilityEvolution()) {
						probabilityEvolution = randomGraphMaker.getProbaEvolution();
					} else {
						probabilityEvolution = null;
					}

					MatrixStatistics matrixStatistics = GraphReporter.getMatrixStatistics(graphs);

					// Build report.
					Report report = RandomGraphReporter.reportRandomAllianceNetworkByAgentSimulation(criteria, matrixStatistics, probabilityEvolution);

					// Manage window.
					if (criteria.isExtractRepresentative()) {
						// Create new window and fill it with a report tab.
						if (StringUtils.isBlank(matrixStatistics.getGraph().getLabel())) {
							matrixStatistics.getGraph().setLabel("Random Group Network");
						}
						GroupNetGUI newGUI = PuckGUI.instance().createGroupNetGUI(matrixStatistics.getGraph());
						newGUI.addReportTab(report);

					} else {
						gui.addReportTab(report);
					}

					dispose();
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(RandomAllianceNetworkByAgentSimulationInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);

		JPanel panel = new JPanel();
		this.contentPane.add(panel, BorderLayout.NORTH);
		panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblNrNodes = new JLabel("Number of nodes:");
		lblNrNodes.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblNrNodes, "2, 4, right, default");

		this.spinnerNodeCount = new JSpinner();
		this.spinnerNodeCount.setModel(new SpinnerNumberModel(new Integer(100), new Integer(0), null, new Integer(10)));
		panel.add(this.spinnerNodeCount, "4, 4");

		JLabel lblNrArcs = new JLabel("Number of arcs:");
		lblNrArcs.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblNrArcs, "2, 6, right, default");

		this.spinnerArcCount = new JSpinner();
		this.spinnerArcCount.setModel(new SpinnerNumberModel(new Integer(1000), new Integer(0), null, new Integer(100)));
		panel.add(this.spinnerArcCount, "4, 6");

		JLabel lblNrRuns = new JLabel("Number of runs:");
		lblNrRuns.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblNrRuns, "2, 8, right, default");

		this.spinnerRuns = new JSpinner();
		this.spinnerRuns.setModel(new SpinnerNumberModel(new Integer(100), new Integer(0), null, new Integer(100)));
		panel.add(this.spinnerRuns, "4, 8");

		JLabel lbInertia0 = new JLabel("Inertia 0:");
		panel.add(lbInertia0, "2, 10, right, default");

		this.spnrInertia0 = new JSpinner();
		this.spnrInertia0.setModel(new SpinnerNumberModel(new Double(1.0), new Double(0), null, new Double(0.1)));
		panel.add(this.spnrInertia0, "4, 10");

		JLabel lblInertia1 = new JLabel("Inertia 1:");
		lblInertia1.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblInertia1, "2, 12, right, default");

		this.spnrInertia1 = new JSpinner();
		this.spnrInertia1.setModel(new SpinnerNumberModel(new Double(1.0), new Double(0), null, new Double(0.1)));
		panel.add(this.spnrInertia1, "4, 12");

		JLabel lblInertia2 = new JLabel("Inertia 2:");
		lblInertia2.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblInertia2, "2, 14, right, default");

		this.spnrInertia2 = new JSpinner();
		this.spnrInertia2.setModel(new SpinnerNumberModel(new Double(1.0), new Double(0), null, new Double(0.1)));
		panel.add(this.spnrInertia2, "4, 14");

		JLabel lblOutPref = new JLabel("Out preference:");
		lblOutPref.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblOutPref, "2, 16, right, default");

		this.spnrOutpreference = new JSpinner();
		this.spnrOutpreference.setModel(new SpinnerNumberModel(new Double(0.5), new Double(0), null, new Double(0.1)));
		((JSpinner.NumberEditor) this.spnrOutpreference.getEditor()).getFormat().setMinimumFractionDigits(1);
		panel.add(this.spnrOutpreference, "4, 16");

		JLabel lblEgoDistribution = new JLabel("Ego distribution:");
		lblEgoDistribution.setHorizontalAlignment(SwingConstants.RIGHT);
		panel.add(lblEgoDistribution, "2, 18, right, default");

		this.cbBoxDistribution = new JComboBox();
		this.cbBoxDistribution.setModel(new DefaultComboBoxModel(DistributionType.values()));
		panel.add(this.cbBoxDistribution, "4, 18, fill, default");

		this.chckbxExtract = new JCheckBox("Extract a representative network");
		panel.add(this.chckbxExtract, "2, 20");

		this.chckbxShowProbabilityEvolution = new JCheckBox("Show probability evolution");
		panel.add(this.chckbxShowProbabilityEvolution, "2, 22");

		// //////////////////
		setCriteria(PuckGUI.instance().getPreferences().getAgentSimulationCriteria());
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public RandomCriteria getCriteria() throws PuckException {
		RandomCriteria result;

		//
		result = new RandomCriteria();

		//
		result.setNodeCount((Integer) this.spinnerNodeCount.getValue());
		result.setArcWeightSum((Integer) this.spinnerArcCount.getValue());
		result.setInertia0((Double) this.spnrInertia0.getValue());
		result.setInertia1((Double) this.spnrInertia1.getValue());
		result.setInertia2((Double) this.spnrInertia2.getValue());
		result.setOutPreference((Double) this.spnrOutpreference.getValue());
		result.setRunCount((Integer) this.spinnerRuns.getValue());
		result.setExtractRepresentative(this.chckbxExtract.isSelected());
		result.setShowProbabilityEvolution(this.chckbxShowProbabilityEvolution.isSelected());
		result.setDistributionType((DistributionType) this.cbBoxDistribution.getSelectedItem());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public void setCriteria(final RandomCriteria source) {
		//
		if (source != null) {
			//
			this.spinnerNodeCount.setValue(source.getNodeCount());
			this.spinnerArcCount.setValue(source.getArcWeightSum());
			this.spnrInertia0.setValue(source.getInertia()[0]);
			this.spnrInertia1.setValue(source.getInertia()[1]);
			this.spnrInertia2.setValue(source.getInertia()[2]);
			this.spnrOutpreference.setValue(source.getOutPreference());
			this.spinnerRuns.setValue(source.getRunCount());
			this.chckbxExtract.setSelected(source.isExtractRepresentative());
			this.chckbxShowProbabilityEvolution.setSelected(source.isShowProbabilityEvolution());
			this.cbBoxDistribution.setSelectedItem(source.getDistributionType());
		}
	}
}
