package org.tip.puck.graphs.workers;

import java.util.ArrayList;
import java.util.List;

import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.Node;
import org.tip.puck.graphs.Graph.GraphMode;
import org.tip.puck.util.Value;
import org.tip.puck.util.Values;

public class NodeValuator<E> {

	public enum EndogenousLabel {
		ID,
		LABEL,
		DEGREE,
		INDEGREE,
		OUTDEGREE,
		EDGEDEGREE,
		STRENGTH,
		INSTRENGTH,
		OUTSTRENGTH,
		EDGESTRENGTH,
		LOOPDEGREE,
		LOOPSTRENGTH,
		BETWEENNESS,
		ORIENTATION,
		MAXPREDECESSOR,
		MAXSUCCESSOR,
		MAXINWEIGHT,
		MAXOUTWEIGHT
	}

	/**
	 * The order of the returned list is matching the order of the node returned by the
	 * <i>toList</i> method.
	 * 
	 * @param source
	 * 
	 * @param label
	 * 
	 * @return
	 */
	public static <E> Values get(final Graph<E> source, final String label) {
		Values result;

		if ((source == null) || (label == null)) {
			//
			result = new Values();

		} else if (label.equals("DIRBETWEENNESS") && source.nodeCount()>0 && source.getNode(1).getAttributeValue("DIRBETWEENNESS")==null) {
			
			//
			result = new Values(source.nodeCount());
			double[] betweenness = GraphUtils.betweenness(source, GraphMode.DIRECTED);
			for (int index = 0; index < source.nodeCount(); index++) {
				//
				result.add(new Value(betweenness[index]));
			}

		} else if (label.equals("BETWEENNESS") && source.nodeCount()>0 && source.getNode(1).getAttributeValue("BETWEENNESS")==null) {
			
			//
			result = new Values(source.nodeCount());
			double[] betweenness = GraphUtils.betweenness(source);
			for (int index = 0; index < source.nodeCount(); index++) {
				//
				result.add(new Value(betweenness[index]));
			}

		} else {
			//
			result = new Values(source.nodeCount());
			for (Node<E> node : source.getNodes().toListSortedById()) {
				//
				result.add(get(node, label));
			}
		}

		//
		return result;
	}
	
	

	/**
	 * 
	 * @param node
	 * @param label
	 * @return
	 */
	public static <E> Value get(final Node<E> source, final String label) {
		Value result;

		//
		EndogenousLabel endogenousLabel;
		
		String attributeValue = source.getAttributeValue(label);
		
		if (attributeValue!=null){
			result = new Value(attributeValue);
		} else {
			
			try {
				endogenousLabel = EndogenousLabel.valueOf(label.replace(" ", "_"));
			} catch (IllegalArgumentException exception) {
				endogenousLabel = null;
			}
			
			if (endogenousLabel == null) {
				result = null;
			} else {
				switch (endogenousLabel) {
					case ID:
						result = new Value(source.getId());
					break;
					case DEGREE:
						result = new Value(source.getDegree());
					break;
					case INDEGREE:
						result = new Value(source.getInDegree());
					break;
					case OUTDEGREE:
						result = new Value(source.getOutDegree());
					break;
					case EDGEDEGREE:
						result = new Value(source.getEdgeDegree());
					break;
					case STRENGTH:
						result = new Value(source.getForce());
					break;
					case INSTRENGTH:
						result = new Value(source.getInForce());
					break;
					case OUTSTRENGTH:
						result = new Value(source.getOutForce());
					break;
					case EDGESTRENGTH:
						result = new Value(source.getEdgeForce());
					break;
					case LOOPDEGREE:
						result = new Value(source.getLoopDegree());
					break;
					case LOOPSTRENGTH:
						result = new Value(source.getLoopForce());
					break;
					case ORIENTATION:
						result = new Value(source.getOrientation());
					break;
					case MAXPREDECESSOR:
						result = new Value(source.getMaxPredecessors());
					break;
					case MAXSUCCESSOR:
						result = new Value(source.getMaxSuccessors());
					break;
					case MAXINWEIGHT:
						result = new Value(source.getMaxInWeight());
					break;
					case MAXOUTWEIGHT:
						result = new Value(source.getMaxOutWeight());
					break;
					default:
						result = null;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Builds endogenous labels as list of string.
	 * 
	 * @return The list of endogenous labels.
	 */
	public static List<String> getAttributeLabels() {
		List<String> result;

		//
		EndogenousLabel[] enumEntries = EndogenousLabel.values();

		//
		result = new ArrayList<String>();
		for (EndogenousLabel enumEntry : enumEntries) {
			result.add(enumEntry.name());
		}

		//
		return result;
	}

	/**
	 * Extracts endogenous labels from a label list.
	 * 
	 * @param sourceLabels
	 *            From where extract endogenous labels.
	 * 
	 * @return Endogenous labels extracted from the source.
	 */
	public static List<String> getMatchingLabels(final List<String> sourceLabels) {
		List<String> result;

		if (sourceLabels == null) {
			result = new ArrayList<String>();
		} else {
			result = new ArrayList<String>(sourceLabels);
			result.retainAll(getAttributeLabels());
		}

		//
		return result;
	}

	/**
	 * Extracts not endogenous labels from a label list.
	 * 
	 * @param sourceLabels
	 *            From where extract not endogenous labels.
	 * 
	 * @return Not endogenous labels extracted from the source.
	 */
	public static List<String> getNotMatchingLabels(final List<String> sourceLabels) {
		List<String> result;

		if (sourceLabels == null) {
			result = new ArrayList<String>();
		} else {
			result = new ArrayList<String>(sourceLabels);
			result.removeAll(getAttributeLabels());
		}

		//
		return result;
	}

}
