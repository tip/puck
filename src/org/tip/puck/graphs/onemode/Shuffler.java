/**
 * 
 */
package org.tip.puck.graphs.onemode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.tip.puck.PuckException;
import org.tip.puck.census.workers.CensusCriteria;
import org.tip.puck.census.workers.CircuitFinder;
import org.tip.puck.graphs.onemode.OMGraph.GraphMode;
import org.tip.puck.net.Net;
import org.tip.puck.report.Report;
import org.tip.puck.segmentation.Segmentation;

/**
 * @author Telmo Menezes
 *
 */
public class Shuffler {
	
	public static Net shuffle (Net net, ShuffleCriteria criteria, Report report) throws PuckException{
		Net result;
		
		GraphMode mode = criteria.getMode();
		
		OMGraph omgraph = GraphModeTransformer.netToOmGraph(net,mode);
		shuffle(omgraph,criteria,report);
		result = GraphModeTransformer.omGraphToNet(omgraph,mode);
		
		//
		return result;
	}
	

	private static void removeLink(final OMLink link) {
		if (link.isWifeLink()) {
			link.getTarget().setFemaleLink(null);
		} else {
			link.getTarget().setFemaleLink(null);
		}
	}	
	
	public static CircuitFinder findCircuits (Net net, ShuffleCriteria shuffleCriteria, int runs, CensusCriteria censusCriteria) throws PuckException{
		CircuitFinder result;
		
		result = new CircuitFinder(new Segmentation(net),censusCriteria);
		
		result.initializeCounts();

		for (int run=0; run < runs; run++){
			Net shuffledNet = shuffle(net,shuffleCriteria,null);
			CircuitFinder finder = new CircuitFinder(new Segmentation(shuffledNet),censusCriteria);
			finder.findCircuits();
			if (censusCriteria.isOpenChainFrequencies()){
				finder.getOpenChains();
			}
			result.incrementCounts(finder);
		}
		
		result.normalizeCounts(runs);
		
		//
		return result;
	}
	

	public static double distance(OMGraph graph1, OMGraph graph2) {
		double dist = 0;
		double count = 0;
//		for (Map.Entry<String, OMNode> entry : graph1.nodes.entrySet()) {
//			String key = entry.getKey();
		for (OMNode node1  : graph1.getNodes()) {
//			OMNode node1 = entry.getValue();
//			OMNode node2 = graph2.getMarriages().get(key);
			OMNode node2 = graph2.getNode(node1.hashKey());	
			dist += node1.distance(node2);
			count += 1.0;
		}
		
		return dist / count;
	}	

	public static void shuffle(OMGraph pgraph, ShuffleCriteria criteria, Report report) {
				
		OMGraph origPGraph = pgraph.clone(); // new OMGraph(pgraph);
		
		pgraph.storeOriginalDescendants();
		pgraph.storeOriginalRelatives();
		
		int k = criteria.getSwitchesPerIteration();
		int maxGenDist = criteria.getMaxGenerationalDistance();
		double minShufflePer = criteria.getMinShufflePercentage();
		int minStableIter = criteria.getMinStableIterations();
		
		int iteration = 0;
		int switches = 0;
		double maxDist = 0;
		double shuffled = 0.;
		int stableIterations = 0;
		
		boolean stop = false;
		while (!stop) {
			shuffled = pgraph.percentageShuffledLinks();
			double dist = distance(pgraph,origPGraph);
			
			System.out.println("#iter " + iteration + "\t\tswaps: " + switches + "\t\tstable iter: " + stableIterations);
			System.out.println("  shuffled: " + shuffled + "%; dist: " + dist + "; max dist: " + maxDist);
			
			if (dist > maxDist) {
				maxDist = dist;
				stableIterations = 0;
			}
			else {
				stableIterations++;
			}
			if (switchLinks(pgraph, k, maxGenDist)) {
				switches++;
			}
			iteration++;
			
			if ((shuffled >= minShufflePer) && (stableIterations >= minStableIter)) {
				stop = true;
			}
		}
		
		if (report!=null){
			report.outputs().appendln("Reshuffling percentage: "+shuffled+"%");
			report.outputs().appendln("Maximal difference from original network: "+100*maxDist+"%");
		}

	}
	
	private static boolean addLink(final OMGraph graph, final OMLink link, final int maxGenDist) {
		OMNode orig = link.getOrigin();
		OMNode targ = link.getTarget();

		// marriages cannot link to themselves
		if (orig == targ) {
			// System.out.println("#1: self-link");
			return false;
		}

		// is origin a descendant of target?
		if (targ.hasDescendant(orig)) {
			// System.out.println("#2: orig is descendant of target");
			return false;
		}

		// is original generational distance within limit?
		int dist = orig.originalGenerationalDistanceNew(targ);
		int dist1 = orig.originalGenerationalDistance(targ);
//		System.out.println(dist+"    "+dist1);
		if ((dist < 0) || (dist > maxGenDist + 1)) {
//		if (dist > maxGenDist) {
			//System.out.println("#3: original generational distance not within limit " + dist);
			return false;
		}
		
		// is target node a virtual child?
		if (targ.isVirtual()){
			return false;
		}

		// set link on target
		if (link.isWifeLink()) {
			link.getTarget().setFemaleLink(link.getOrigin());
		} else {
			link.getTarget().setMaleLink(link.getOrigin());
		}
		
		// recompute ancestors
		graph.clearAncestors();
		graph.computeAncestors();

		return true;
	}
	
	public static boolean switchLinks(final OMGraph pgraph, final int k, final int maxGenDist) {
		// find list of links to shuffle, no repetitions, same gender target
		Set<OMLink> linkSet = new HashSet<OMLink>();
		OMLink link = pgraph.getRandomLink();
		linkSet.add(link);
		boolean gender = link.isWifeLink();
		while (linkSet.size() < k) {
			link = pgraph.getRandomLink();
			if (link.isWifeLink() == gender) {
				linkSet.add(link);
			}
		}
		List<OMLink> links = new ArrayList<OMLink>(linkSet);

		// create a perfect shuffle of the previous list
		List<OMLink> links2 = new ArrayList<OMLink>(links);
		boolean perfectShuffle = false;
		while (!perfectShuffle) {
			perfectShuffle = true;
			for (int i = 0; i < k; i++) {
				if (links.get(i) == links2.get(i)) {
					perfectShuffle = false;
				}
			}
			Collections.shuffle(links2);
		}

		// create new links list
		List<OMLink> newLinks = new ArrayList<OMLink>();
		for (int i = 0; i < k; i++) {
			OMLink newLink = new OMLink(links.get(i).getOrigin(), links2.get(i).getTarget(), links.get(i).isWifeLink());
			newLinks.add(newLink);
		}

		// remove old links
		for (OMLink oldLink : links) {
			removeLink(oldLink);
		}

		// recompute ancestors
		pgraph.clearAncestors();
		pgraph.computeAncestors();

		// try to add new links
		boolean fail = false;
		for (OMLink newLink : newLinks) {
			if (!addLink(pgraph,newLink, maxGenDist)) {
				fail = true;
				break;
			}
		}

		// revert changes in case of failure
		if (fail) {
			for (OMLink newLink : newLinks) {
				removeLink(newLink);
			}
			for (OMLink oldLink : links) {
				addLink(pgraph,oldLink, maxGenDist);
			}
		}
		else {
			// update link objects
			for (int i = 0; i < k; i++) {
				links.get(i).setTarget(links2.get(i).getTarget());
				links.get(i).setShuffled(true);
			}
		}

		return !fail;
	}
	


}
