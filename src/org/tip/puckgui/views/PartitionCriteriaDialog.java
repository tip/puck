package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.util.Labels;

/**
 * 
 * @author TIP
 */
public class PartitionCriteriaDialog extends JDialog {

	private static final long serialVersionUID = -4696599918449480365L;
	private final JPanel contentPanel = new JPanel();
	private PartitionCriteria inputedCriteria = null;
	private PartitionCriteriaPanel partitionCriteriaPanel;

	/**
	 * Create the dialog.
	 */
	public PartitionCriteriaDialog(final List<Labels> modelLabels) {
		//
		// super((Frame) null, true);
		super();
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Partition Criteria Input");
		setIconImage(Toolkit.getDefaultToolkit().getImage(PartitionCriteriaDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				inputedCriteria = null;
				setVisible(false);
			}
		});

		// //////////////////////
		setBounds(100, 100, 440, 325);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			partitionCriteriaPanel = new PartitionCriteriaPanel(modelLabels);
			contentPanel.add(partitionCriteriaPanel);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						inputedCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						inputedCriteria = partitionCriteriaPanel.getCriteria();
						setVisible(false);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	public PartitionCriteria getCriteria() {
		PartitionCriteria result;

		result = inputedCriteria;

		//
		return result;
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		PartitionCriteria criteria = showDialog(null);
		System.out.println("return=" + criteria);
	}

	/**
	 * Launch the application.
	 */
	public static PartitionCriteria showDialog(final List<Labels> modelLabels) {
		PartitionCriteria result;

		//
		PartitionCriteriaDialog dialog = new PartitionCriteriaDialog(modelLabels);
		// dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getCriteria();

		//
		return result;
	}
}
