package org.tip.puck.io.iur;

import fr.devinsy.util.StringList;

/**
 * This class represents a line of individual from a TXT file.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class IURTXTRelationLine {
	public static final int NOID = 0;

	private int id;
	private String name;
	private StringList values;

	/**
	 * 
	 */
	public IURTXTRelationLine() {
		this.id = NOID;
		this.name = null;
		this.values = new StringList();
	}

	public String getName() {
		return this.name;
	}

	public int id() {
		return this.id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		StringList buffer = new StringList();
		buffer.append(this.id).append("\t").append(this.name).append("\t").append(this.values.toStringSeparatedBy("\t"));

		result = buffer.toString();

		//
		return result;
	}

	public StringList values() {
		return this.values;
	}
}
