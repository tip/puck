package org.tip.puck.report;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author TIP
 */
public class DataLine {

	private String title;
	private List<XYData> values;

	/**
	 * 
	 */
	public DataLine() {
		this.title = null;
		this.values = new ArrayList<XYData>();
	}

	/**
	 * 
	 * @param value
	 */
	public void addValue(final double y) {
		this.values.add(new XYData(y));
	}

	/**
	 * 
	 * @param value
	 */
	public void addValue(final double x, final double y) {
		this.values.add(new XYData(x, y));
	}

	/**
	 * 
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @param columnIndex
	 * @return
	 */
	public Double getXValue(final int columnIndex) {
		Double result;

		if (columnIndex < this.values.size()) {
			result = this.values.get(columnIndex).getXValue();
		} else {
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param columnIndex
	 * @return
	 */
	public Double getYValue(final int columnIndex) {
		Double result;

		if (columnIndex < this.values.size()) {
			result = this.values.get(columnIndex).getYValue();
		} else {
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param title
	 */
	public void setTitle(final String title) {
		this.title = title;
	}

	/**
	 * 
	 * @param value
	 * @param columnIndex
	 */
	public void setXValue(final double x, final int columnIndex) {
		if (columnIndex < this.values.size()) {
			this.values.get(columnIndex).setXValue(x);
		}
	}

	/**
	 * 
	 * @param value
	 * @param columnIndex
	 */
	public void setXYValue(final Double x, final Double y, final int columnIndex) {
		if (columnIndex < this.values.size()) {
			this.values.get(columnIndex).set(x, y);
		}
	}

	/**
	 * 
	 * @param value
	 * @param columnIndex
	 */
	public void setYValue(final double y, final int columnIndex) {
		while (columnIndex >= this.values.size()) {
			this.addValue(0);
		}

		if (columnIndex < this.values.size()) {
			this.values.get(columnIndex).setYValue(y);
		}
	}

	/**
	 * 
	 * @return
	 */
	public int size() {
		int result;

		result = this.values.size();

		//
		return result;
	}
}
