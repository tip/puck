package org.tip.puck.census.workers;

import java.util.Arrays;
import java.util.List;

import org.tip.puck.census.chains.Chain;
import org.tip.puck.census.chains.ChainMaker;
import org.tip.puck.census.chains.Chains;
import org.tip.puck.census.chains.Couple;
import org.tip.puck.census.chains.Notation;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.NumberedValues;
import org.tip.puck.util.Value;

/**
 * 
 * @author TIP
 */
public class ChainValuator {

	public enum ChainProperty {
		SIMPLE,
		POSITIONAL,
		POSITIONAL_NEUTRAL,
		CLASSIC,
		CLASSIC_GENDERED,
		LENGTH,
		DEPTH,
		ORDER,
		HETERO,
		DEGREE_ROM,
		DEGREE_GER,
		ENDS,
		APICES,
		SIDE,
		SKEW,
		SKEWSUM,
		SYM,
		LINE,
		AGNA,
		UTER,
		DRAV,
		DRAV_H,
		DRAV_O,
		SWITCHES,
		ARCH,
		SKEWTYPE,
		EGOGENDER,
		ALTERGENDER,
		APEXID,
		FIRSTBRANCH,
		SUBCHAINS,
		PERMUTATIONS,
		ALTERNAME,
		MIXED,
		MIXED2;
	}
	
	public enum Dravidian {
		PARALLEL,
		CROSS,
		NEUTRAL;
		
		private Dravidian inverse(){
			Dravidian result;
			
			switch (this){
			case PARALLEL:
				result = CROSS;
				break;
			case CROSS:
				result = PARALLEL;
				break;
			case NEUTRAL:
				result = NEUTRAL;
				break;
			default:
				result = null;
			}
			//
			return result;
		}
		
		public String toString(){
			String result;

			switch (this){
			case PARALLEL:
				result = "=";
				break;
			case CROSS:
				result = "#";
				break;
			case NEUTRAL:
				result = "2";
				break;
			default:
				result = null;
			}
			//
			return result;
			
		}
	}
	
	public static boolean hasLineValue (final Chain source, final String label, final Value value){
		boolean result = true;
		String[] valueStrings = get(source,label).toString().split("\\s");
		for (String valueString : valueStrings){
			if (!valueString.equals("IDENTITY") && !valueString.equals("BILINEAR") && !valueString.equals(value.toString())){
				result = false;
				break;
			}
		}
		//
		return result;
	}
	
	public static boolean hasValue (final Chain source, final String label, final Value value){
		return get(source,label).equals(value);
	}
	

	/**
	 * 
	 * @param individual
	 * @param label
	 * @return
	 */
	public static Value get(final Chain source, final String label) {
		Value result;

		result = get(source, label, null);

		//
		return result;
	}
	
	/**
	 * 
	 * @param individual
	 * @param label
	 * @return
	 */
	public static NumberedValues get(final Chains source, final String label, final Object parameter) {
		NumberedValues result;

		//
		result = new NumberedValues();

		for (Chain chain : source) {
			result.put(chain.getId(), get(chain, label, parameter));
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @param individual
	 * @param label
	 * @return
	 */
	public static Value get(final Chain source, final String label, final Object parameter) {
		Value result;
		
		if (label == null || source == null) {
			result = null;
		} else {
			// Check for multiple labels
			String[] labels = label.split("AND");
			if (labels.length>1){
				Value value1 = get(source,labels[0],parameter);
				Value value2 = get(source,labels[1],parameter);
				result = new Value(value1.toString()+" AND "+value2.toString());
				return result;
			}

			ChainProperty endogenousLabel;
			try {
				endogenousLabel = ChainProperty.valueOf(label.replace(" ", "_"));
			} catch (IllegalArgumentException exception) {
				endogenousLabel = null;
			}
			
			switch (endogenousLabel) {
			case SIMPLE: 
				
				result = new Value(source.getCharacteristicVector());
				break;
				
			case POSITIONAL: 
				
				result = new Value(source.signature(Notation.POSITIONAL));
				break;
				
			case POSITIONAL_NEUTRAL: 
				
				result = new Value(source.signature(Notation.POSITIONAL_NEUTRAL));
				break;
				
			case CLASSIC: 
				
				result = new Value(source.signature(Notation.CLASSIC));
				break;
				
			case CLASSIC_GENDERED: 
				
				result = new Value(source.signature(Notation.CLASSIC_GENDERED));
				break;
				
			case LENGTH: 
				
				result = new Value (source.length());
				break;
				
			case DEPTH:
				
				result = new Value (source.depth());
				break;
				
			case ORDER:
				
				result = new Value (source.dim());
				break;
				
			case SYM: 
				result = new Value (source.sym());
				break;
				
			case HETERO:
				
				result= new Value (source.isHetero());
				break;

			case EGOGENDER:
				
				result= new Value (source.getFirst().getGender());
				break;
				
			case ALTERGENDER:
				
				result= new Value (source.getLast().getGender());
				break;
				
			case ALTERNAME:
				
				result= new Value (source.getLast().getName());
				break;
				
			case SUBCHAINS:
				
				String string = "";
				int k = 1;
				for (Chain subchain : source.subchains){
					string = string + k+". "+subchain.signature(Notation.POSITIONAL)+"\t";
					k++;
				}
				result = new Value(string);
				break;
				
			case PERMUTATIONS:
				
				result = new Value(ChainMaker.getPermutations(source).size());
				break;
				
			case SKEWSUM:
				
				int sum = 0;
				for (int i=0;i<source.dim();i++){
					sum = sum + get(source,ChainProperty.SKEW,i).intValue();
				}
				
				result = new Value(sum);
				break;
				
			case MIXED:
				
				result = new Value("UNKNOWN");
				if (source.dim()==1){
					int skew = get(source,"SKEWSUM").intValue();
					if (source.length()==1){
						if (skew == 1){
							if (source.getLast().isMale()){
								result = new Value("FATHER");
							} else if (source.getLast().isFemale()){
								result = new Value("MOTHER");
							}
						} else if (skew == -1){
							result = new Value("CHILD");
						}
					} else if (source.length()==2 && skew == 0){
						result = new Value("SIBLING");
					} else {
						result = new Value("RELATIVE_"+get(source, "LINE"));
					}
				} else if (source.length()==1){
					result = new Value("SPOUSE");
				} else {
					result = new Value("AFFINE");
				}
				break;
				
			case MIXED2:
				
				result = get(source,"MIXED");
				String signature = source.signature(Notation.CLASSIC);
				
				if (signature.equals("MM")){
					result = new Value("MATERNAL_GRANDMOTHER");
				} else if (signature.equals("FM")){
					result = new Value("PATERNAL_GRANDMOTHER");
				} else if (signature.equals("MF")){
					result = new Value("MATERNAL_GRANDFATHER");
				} else if (signature.equals("FF")){
					result = new Value("PATERNAL_GRANDFATHER");
				} else if (signature.equals("MB") || signature.equals("MFS") || signature.equals("MMS")){
					result = new Value("MATERNAL_UNCLE");
				} else if (signature.equals("FB") || signature.equals("FFS") || signature.equals("FMS")){
					result = new Value("PATERNAL_UNCLE");
				} else if (signature.equals("MZ") || signature.equals("MFD") || signature.equals("MMD")){
					result = new Value("MATERNAL_AUNT");
				} else if (signature.equals("FZ") || signature.equals("FFD") || signature.equals("FMD")){
					result = new Value("PATERNAL_AUNT");
				} else if (result.stringValue().equals("RELATIVE_COGNATIC")){
					if (signature.charAt(0)=='M'){
						result = new Value("RELATIVE_COGNATIC_MATERNAL");
					} else if (signature.charAt(0)=='F'){
						result = new Value("RELATIVE_COGNATIC_PATERNAL");
					} else if (signature.charAt(0)=='B' || signature.charAt(0)=='Z'){
						result = new Value("RELATIVE_COGNATIC_BILATERAL");
					} else if (signature.charAt(0)=='S' || signature.charAt(0)=='D'){
						result = new Value("RELATIVE_COGNATIC_FILIAL");
					}
				} else if (result.stringValue().equals("SIBLING")){
					if (source.getLast().isMale()){
						result = new Value("BROTHER");
					} else if (source.getLast().isFemale()){
						result = new Value("SISTER");
					}
				} else if (result.stringValue().equals("CHILD")){
					if (source.getLast().isMale()){
						result = new Value("SON");
					} else if (source.getLast().isFemale()){
						result = new Value("DAUGHTER");
					}
				} 
				if (result.stringValue().equals("AFFINE") || result.stringValue().contains("RELATIVE")){
					if (source.getLast().isMale()){
						result = new Value(result.stringValue()+"_MALE");
					} else if (source.getLast().isFemale()){
						result = new Value(result.stringValue()+"_FEMALE");
					}
				}
				break;
				
			default: 

				String s = "";
				for (int i=0;i<source.dim();i++){
					s = s + get(source,endogenousLabel,i)+" ";
				}
				result = new Value(s.trim());
			}
		}
		

		//
		return result;
	}
	
	
	private static Value get(final Chain source, final ChainProperty label, final int i) {
		Value result = null;

		Chain left = source.subchains.get(2*i);
		Chain right = source.subchains.get(2*i+1);
		
		switch(label){
		case DEGREE_ROM:

			result= new Value(left.length()+right.length());
			break;
			
		case DEGREE_GER:

			result= new Value(Math.max(left.length(),right.length()));
			break;
			
		case ENDS:
			
			result= new Value(left.getFirst().getGender().toChar()+""+right.getFirst().getGender().toChar());
			break;
			
		case APICES:
			
			if (left.getLast() instanceof Couple){
				result = new Value('X');
			} else {
				result= new Value(left.getLast().getGender().toChar());
			}

			break;

		case APEXID:
			
			if (left.getLast() instanceof Couple){
				result = new Value(0);
			} else {
				result= new Value(left.getLast().getId());
			}

			break;
			
		case SIDE:
			
			if (left.length()>1) {
				if (left.get(1).isMale()){
					result = new Value("PATERNAL");
				} else if (left.get(1).isFemale()){
					result = new Value("MATERNAL");
				} else if (left.get(1) instanceof Couple){
					result = new Value ("BILATERAL");
				} else {
					result = new Value("UNKNOWN");
				}
			} else {
				result = new Value("NONE");
			}
			
			break;

		case SKEW:
			
			result= new Value(left.length()-right.length());
			break;
			
		case SKEWTYPE:
			
			int skew = get(source,ChainProperty.SKEW,i).intValue();
			if (skew==0){
				result = new Value("H"); // horizontal
			} else if (Math.abs(skew)==1){
				result = new Value("O"); // oblique
			} else if (Math.abs(skew)==2){
				result = new Value("A"); // alternate
			} else {
				result = new Value("X"); // not defined
			}
			break;

		case LINE:
			
			if (get(source,ChainProperty.DEGREE_ROM,0).intValue()==0 && get(source,ChainProperty.DEGREE_ROM,1).intValue()==0){
				result = new Value(FiliationType.SPOUSE);
			} else if (get(source,ChainProperty.DEGREE_ROM,i).intValue()==0){
				result = new Value(FiliationType.IDENTITY);
			} else if (get(source,ChainProperty.SWITCHES,i).intValue()>0) {
				result = new Value(FiliationType.COGNATIC);
			} else if (left.getLast() instanceof Couple){
				if (left.length()==1 && right.length()==1){
					result = new Value(FiliationType.BILINEAR);
				} else {
					Gender gender;
					if (left.length()>1) {
						gender = left.get(left.length()-1).getGender();
					} else {
						gender = right.get(right.length()-1).getGender();
					}
					if (gender.isMale()){
						result = new Value(FiliationType.AGNATIC);
					} else if (gender.isFemale()) {
						result = new Value(FiliationType.UTERINE);
					}
				}
			} else if (left.getLast().getGender().isMale()){
				result = new Value(FiliationType.AGNATIC);
			} else if (left.getLast().getGender().isFemale()) {
				result = new Value(FiliationType.UTERINE);
			}
			
			break;
			
		case AGNA:
			
			result = new Value(MathUtils.percent(filiationCount(left,Gender.MALE)+filiationCount(right,Gender.MALE),left.length()+right.length())); 
			break;
			
		case UTER:
			
			result = new Value(MathUtils.percent(filiationCount(left,Gender.FEMALE)+filiationCount(right,Gender.FEMALE),left.length()+right.length())); 
			break;
			
		case DRAV:

			result = new Value(dravidian(left,right).toString());
			break;

		case DRAV_H:

			String skewtype = get(source,ChainProperty.SKEWTYPE,i).stringValue();
			String drav = get(source,ChainProperty.DRAV,i).stringValue();
			
			if (skewtype.equals("H") && drav.equals("#")){
				result = new Value("ok");
			} else if (skewtype.equals("O") || drav.equals("=")){
				result = new Value("no");
			} else {
				result = new Value("?");
			}
			break;

		//check and correct
		case DRAV_O:
			
			if (parakana(left,right)==-1) {
				result = new Value(" ");//t(29)
			} else if (parakana(left, right)==1) {
				result = new Value(" ");//t(25);
			} else if (parakana(left, right)==2) {
				result = new Value(" ");//t(25)
			} else {
				result = new Value(" ");//t(24)+
			}
			break;

		case SWITCHES:
			
			int switches = 0;
			
			if (left.length()>0){
				for (int j=1;j<left.length();j++){
					if (differentGender(left.get(j),left.get(j+1))) {
						switches++;
					}
				}
			}
			if (left.getLast() instanceof Couple){

				if (left.length() > 1 && right.length()> 1 && differentGender(left.get(left.length()-1),right.get(right.length()-1))){
					switches++;
				}
			}
			
			if (right.length()>0){
				for (int j=1;j<right.length();j++){
					if (differentGender(right.get(j),right.get(j+1))) {
						switches++;
					}
				}
			}
			
			result = new Value(switches);
			break;
			
		case ARCH:
			
			if (left.length()==0 || right.length()==0) {
				result = new Value("-");
			} else {
				result = new Value(left.get(left.size()-2).getGender().toChar()+""+right.get(right.size()-2).getGender().toChar());
			}
			break;
			
		case FIRSTBRANCH:
			
			result = new Value(left.signature(Notation.POSITIONAL));
			break;
			
			
		default:
			result = null;
		}

		return result;
		
	}
	
	private static boolean differentGender(Individual first, Individual second){
		boolean result;
		if (first instanceof Couple || second instanceof Couple) {
			result = false;
		} else {
			result = !first.getGender().matchs(second.getGender());
		}
		//
		return result;
	}
	
	
	public static String getValueString (final Chain source, final List<String> labels, final Object parameter){
		String result;
		
		result = "";
		for (String label : labels){
			result = result+get(source,label,parameter).toString()+"\t";
		}
		return result;
	}
	
	private static int filiationCount(Chain chain, Gender gender){
		int result = 0;
		if (chain.length()>0){
			for (int i=1;i<chain.size();i++){
				if (chain.get(i).getGender().matchs(gender)){
					result++;
				}
			}
		}
		return result;
	}

	/**
		 * checks the dravidian crossness of a cousin relation by comparison of its two branches
		 * @param right the other branch of the cousin relation
		 * @return the dravidian crossness of the relation (0 parallel, 1 cross)
		 */
	private	static Dravidian dravidian (Chain left, Chain right) {
		Dravidian result;
		
		int skew = Math.abs(left.size()-right.size());
		
		if (skew > 1) {
			result = Dravidian.NEUTRAL;
		} else {
			result = dravidian (left,right,Math.min(left.length(), right.length()));      
		}
		//
		return result;
	   }

	/**
	 * checks the dravidian crossness of a cousin relation at the ith level from top
	 * @param right the branch of the cousin
	 * @param fromTop the distance from the common ancestor
	 * @return the dravidian crossness of the relation (0 parallel, 1 cross)
	 */
	private static Dravidian dravidian (Chain left, Chain right, int fromTop) {
		Dravidian result;
		if (fromTop<=1) {
			result = Dravidian.PARALLEL;
		} else {
			Gender leftGender = left.get(left.length()-fromTop+1).getGender();
			Gender rightGender = right.get(right.length()-fromTop+1).getGender();
			if (leftGender == rightGender){
				result = dravidian(left,right,fromTop-1);
			} else {
				result = dravidian(left,right,fromTop-1).inverse();
			}
		}
		//
		return result;
	}

	/**
	 * checks the dravidian crossness of a cousin relation at the ith level from top
	 * @param right the branch of the cousin
	 * @param i the distance from the common ancestor
	 * @return the dravidian crossness of the relation (0 parallel, 1 cross)
	 */
	private static int getDravidianCrossness (Chain left, Chain right, int i) {
	   if (i<=1) return 1;
	   return getDravidianCrossness(left,right,i-1)*(1-2*Math.abs(left.getGenderInt(left.size()-i)-right.getGenderInt(right.size()-i)));
	}


	/**
		 * checks the dravidian crossness of a cousin relation by comparison of its two branches
		 * @param right the other branch of the cousin relation
		 * @return the dravidian crossness of the relation (0 parallel, 1 cross)
		 */
	private	static int parakana (Chain left, Chain right) {
			int d = left.size()-right.size();
			if (Math.abs(d) > 1) return 2;
			if (d==0) return getDravidianCrossness (left,right,left.size()-1);
			int n = Math.min(left.size(),right.size())-1;
			int x = getDravidianCrossness (left,right,n);
			int y = Math.abs(left.getGenderInt(left.size()-n-1)-right.getGenderInt(right.size()-n-1));
			if (d > 0) { //aunts
				if (y==0 || x==1) {
					if (n<3) return 1;
					if (Math.abs(left.getGenderInt(left.size()-2)-right.getGenderInt(right.size()-2))==0) return 1;
				}
				return -1;	
			} else { //nieces
				if (y==1 && x==1) {
					if (n==1) return 0;
					if (Math.abs(left.getGenderInt(left.size()-2)-right.getGenderInt(right.size()-2))==0) return 0;
				}
				if ((y==1 && x==-1) || (y==0 && x==1)) return 1;
				return -1;
			}
	   }
	
	/**
	 * Build an array containing endogenous labels sorted. 
	 * @return an array containing endogenous labels sorted.
	 */
	public static String[] getEndogenousLabels()
	{
		String[] result;

		ChainProperty[] labels = ChainProperty.values();
		result = new String[ChainProperty.values().length];

		for (int labelIndex = 0; labelIndex < labels.length; labelIndex++) {
			result[labelIndex] = labels[labelIndex].name();
		}
		Arrays.sort(result);

		//
		return result;
	}
}
