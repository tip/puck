package org.tip.puck.net.workers;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import oldcore.trash.OldNet;

import org.tip.puck.net.Individual;
import org.tip.puck.net.KinType;
import org.tip.puck.net.Net;
import org.tip.puck.report.Report;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class HairCutWorker {

	// 2
	/**
	 * 
	 * @param net
	 * @return
	 */
	public static Net copyShave(final Net net, final Report report) {
		Net result;

		result = new Net(net);
		shave(result,report);

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static int shave(final Net source, final Report report) {
		int result;

		result = 0;
		Map<Individual,Integer> colors = getTreeColors(source);
		StringList list = new StringList();
		for (Individual individual : colors.keySet()) {
			if (colors.get(individual)==-1) {
				source.remove(individual);
				list.appendln(individual.toString());
				result += 1;
			}
		}
		
//		Collections.sort(list);  // throws error java.lang.NegativeArraySizeException
		
		report.outputs().appendln(result+" Individuals removed:");
		report.outputs().appendln();
		report.outputs().append(list);

		//
		return result;
	}

	/**
	 * sets the color of the vertex according to its belonging to a tree
	 * 
	 * @param root
	 *            the root of the potential tree
	 * @return the tree color of the vertex (0 not yet marked, 1 in the course
	 *         of being marked, 2 not a tree vertex, -1 a tree vertex)
	 */
	public static Integer getTreeColor(final Map<Individual,Integer> colors, final Individual ego, final Individual root) {
		Integer result;
		
		result = colors.get(ego);

		// ego has not yet been visited
		if (result==null) {
			
			//mark ego as currently visited
			colors.put(ego,1);

			//
			int nrOfLinks = 0;

			//
			if (root != null) {
				nrOfLinks = 1;
			}

			for (KinType kinType : KinType.basicTypes()) {
				for (Individual relative : ego.getKin(kinType)) {
					
					// ego is linked to a visited individual (other than its predecessor)
					if (relative != root && getTreeColor(colors, relative, ego)>-1) {
						nrOfLinks++;
						if (nrOfLinks==2){
							break;
						}
					}
				}
				if (nrOfLinks==2){
					break;
				}
			}
			
			// mark ego as non-visited (since it has no more links, it will not be visited again)
			if (nrOfLinks < 2) {
				colors.put(ego,-1);
			} 
			
			// if ego remains marked it is not in a tree
			result = colors.get(ego);
		}
	
		//
		return result;
	}

	/**
	 * sets the color of the vertex according to its belonging to a tree,
	 * excluding structural children
	 * 
	 * @param root
	 *            the root of the potential tree
	 * @param j
	 *            the direction (ungendered kin tie index) of the search (1
	 *            ascending, 0 horizontal, -1 descending)
	 * @return the tree color of the vertex (0 not yet marked, 1 in the course
	 *         of being marked, 2 not a tree vertex, -1 a tree vertex)
	 */
	public static int getTreeColorExcludingStructuralChildren(final Map<Individual, Integer> colors, final Individual ego, final Individual root,
			final KinType foo) {

		if (colors.get(ego) != 0) {
			return colors.get(ego);
		}
		colors.put(ego, 1);

		//
		if (ego.isSterile() && ego.isSingle()) {
			colors.put(ego, -1);
			return colors.get(ego);
		}

		int k = 0;

		//
		int h = 0;

		for (KinType kinType : KinType.basicTypes()) {
			for (Individual v : ego.getKin(kinType)) {
				if (getTreeColorExcludingStructuralChildren(colors, v, ego, kinType) != -1) {
					k++;
					if (kinType != KinType.CHILD) {
						h++;
					}
				}
				if (k > 1 && h > 0) {
					colors.put(ego, 2);
					return colors.get(ego);
				}
			}
		}
		colors.put(ego, -1);
		return colors.get(ego);
	}

	/**
	 * sets the colors of the verticeds of the Net according to their belonging
	 * to a tree
	 * 
	 * @param matrim
	 *            true if structural children shall be excluded
	 * @see OldNet.CopyOfNet#setColors(int)
	 */
	private static Map<Individual,Integer> getTreeColors(final Net net) {
		Map<Individual,Integer> result;

		result = new HashMap<Individual,Integer>();
		
		for (Individual v : net.individuals()) {
			getTreeColor(result, v, null);
		}

		//
		return result;
	}

	/**
	 * sets the colors of the vertices of the Net according to their belonging
	 * to a tree
	 * 
	 * @param matrim
	 *            true if structural children shall be excluded
	 * @see OldNet.CopyOfNet#setColors(int)
	 */
	private static Map<Individual, Integer> getTreeColorsExcludingStructuralChildren(final Net net) {
		Map<Individual, Integer> result;

		result = new HashMap<Individual, Integer>();
		for (Individual v : net.individuals()) {
			getTreeColorExcludingStructuralChildren(result, v, null, KinType.CHILD);
		}
		//
		return result;
	}

}
