package org.tip.puckgui.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.util.Chronometer;

/**
 * 
 * @author TIP
 */
public class ProgressStatus {

	private static final Logger logger = LoggerFactory.getLogger(ProgressStatus.class);

	private long current;
	private long max;
	private long extra1;
	private String description;
	private Chronometer chrono;

	/**
	 * 
	 */
	public ProgressStatus() {
		this.current = 0;
		this.max = 0;
		this.extra1 = 0;
		this.chrono = new Chronometer().reset().start();
	}

	/**
	 * 
	 */
	public ProgressStatus(final long start, final long max) {
		this.current = start;
		this.max = max;
	}

	public Chronometer getChrono() {
		return this.chrono;
	}

	public long getCurrent() {
		return this.current;
	}

	/**
	 * 
	 * @return
	 */
	public int getCurrentRate() {
		int result;

		if (this.max == 0) {
			result = 0;
		} else {
			result = (int) (this.current * 100 / this.max);
		}

		return result;
	}

	public String getDescription() {
		return this.description;
	}

	public long getExtra1() {
		return this.extra1;
	}

	public long getMax() {
		return this.max;
	}

	public void inc() {
		this.current += 1;
	}

	public void incExtra1() {
		this.extra1 += 1;
	}

	public void setCurrent(final long current) {
		this.current = current;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public void setExtra1(final long extra1) {
		this.extra1 = extra1;
	}

	public void setMax(final long max) {
		this.max = max;
	}
}
