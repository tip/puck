package org.tip.puck.geo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.geo.PlaceComparator.Sorting;

import fr.devinsy.util.StringSet;

/**
 * 
 */
public class Places extends ArrayList<Place> {

	private static final long serialVersionUID = -8277613440390116082L;

	/**
	 * 
	 */
	public Places() {
		super();
	}

	/**
	 * 
	 */
	public Places(final int capacity) {
		super(capacity);
	}

	/**
	 * 
	 */
	public Places(final Places source) {
		super();

		if (source != null) {
			//
			for (Place place : source) {
				//
				this.add(place);
			}
		}
	}

	/**
	 * 
	 * @param searchedName
	 * @return
	 */
	public Place getByToponym(final String searchedName) {
		Place result;

		boolean ended = false;
		Iterator<Place> iterator = this.iterator();
		result = null;
		while (!ended) {
			//
			if (iterator.hasNext()) {

				Place current = iterator.next();

				if (StringUtils.equals(current.getToponym(), searchedName)) {
					//
					ended = true;
					result = current;
				}

			} else {
				//
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public void sortByToponym() {
		//
		Collections.sort(this, new PlaceComparator(Sorting.TOPONYM));
	}

	/**
	 * 
	 * @return
	 */
	public StringSet toponyms() {
		StringSet result;

		result = new StringSet();

		for (Place place : this) {
			result.add(place.getToponym());
		}

		//
		return result;
	}
}
