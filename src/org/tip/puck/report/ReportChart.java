package org.tip.puck.report;

import org.tip.puck.util.MathUtils;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class ReportChart {

	public enum GraphType {
		STACKED_BARS,
		LINES,
		SCATTER,
		BAR_BOARD,
		SURFACE
	}

	public enum LogarithmType {
		NONE,
		VERTICAL,
		HORIZONTAL
	}

	private String title;
	private GraphType type;
	private HeaderLine headerLine;
	private DataLines dataLines;
	private LogarithmType logarithmType;
	private Double verticalMax;
	private boolean integerHorizontalUnit;

	/**
	 * 
	 */
	public ReportChart(final GraphType type) {
		this.title = null;
		this.type = type;
		this.headerLine = new HeaderLine();
		this.dataLines = new DataLines();
		this.logarithmType = LogarithmType.NONE;
		this.integerHorizontalUnit = false;
	}

	/**
	 * 
	 */
	public ReportChart(final String title, final GraphType type) {
		this.title = title;
		this.type = type;
		this.headerLine = new HeaderLine();
		this.dataLines = new DataLines();
		this.logarithmType = LogarithmType.NONE;
		this.integerHorizontalUnit = false;
	}

	/**
	 * 
	 * @param x
	 * @param y
	 * @param rowIndex
	 */
	public void addValue(final double x, final double y, final int rowIndex) {
		this.dataLines.getLine(rowIndex).addValue(x, y);
	}

	/**
	 * 
	 * @param value
	 * @param rowIndex
	 */
	public void addValue(final double value, final int rowIndex) {
		this.dataLines.getLine(rowIndex).addValue(value);
	}

	/**
	 * 
	 * @param x
	 * @param y
	 * @param rowIndex
	 */
	public void addValue(final Double x, final Double y, final int rowIndex) {
		this.dataLines.getLine(rowIndex).addValue(x, y);
	}

	/**
	 * 
	 * @return
	 */
	public ReportTable createReportTable() {
		ReportTable result;

		//
		result = new ReportTable(this.getRowCount() + 1, this.getColumnCount() + 1);

		//
		result.setTitle(this.title);

		// Build header line.
		if (this.headerLine.getHeaders().isEmpty()) {
			if (this.getRowCount() > 0) {
				for (int columnIndex = 0; columnIndex < this.getColumnCount(); columnIndex++) {
					// Set horizontal headers.
					Double headerValue = this.getXValue(0, columnIndex);
					if (headerValue == null) {
						headerValue = Double.valueOf(columnIndex + 1);
					}

					result.set(0, columnIndex + 1, MathUtils.toString(headerValue));
				}
			}
		} else {
			for (int columnIndex = 0; columnIndex < this.headerLine.getHeaders().size(); columnIndex++) {
				result.set(0, columnIndex + 1, this.headerLine.getHeader(columnIndex));
			}
		}

		//
		for (int rowIndex = 0; rowIndex < this.getRowCount(); rowIndex++) {
			// Set line header.
			result.set(rowIndex + 1, 0, this.getLineTitle(rowIndex));

			//
			for (int columnIndex = 0; columnIndex < this.getColumnCount(rowIndex); columnIndex++) {
				result.set(rowIndex + 1, columnIndex + 1, MathUtils.toString(this.getYValue(rowIndex, columnIndex)));
			}
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public ReportTable createReportTableWithSum() {
		ReportTable result;
		
		//
		boolean rowSumEnable;
		if (this.getRowCount() > 1) {
			rowSumEnable = true;
		} else {
			rowSumEnable = false;
		}

		boolean columnSumEnable;
		if (this.getColumnCount() > 1) {
			columnSumEnable = true;
		} else {
			columnSumEnable = false;
		}

		//
		int rowCount = this.getRowCount() + 1;
		if (rowSumEnable) {
			rowCount += 1;
		}
		int columnCount = this.getColumnCount() + 1;
		if (columnSumEnable) {
			columnCount += 1;
		}
		result = new ReportTable(rowCount, columnCount);

		//
		result.setTitle(this.title);

		// Build header line.
		if (this.headerLine.getHeaders().isEmpty()) {
			if (this.getRowCount() > 0) {
				for (int columnIndex = 0; columnIndex < this.getColumnCount(); columnIndex++) {
					// Set horizontal headers.
					Double headerValue = this.getXValue(0, columnIndex);
					if (headerValue == null) {
						headerValue = Double.valueOf(columnIndex + 1);
					}

					result.set(0, columnIndex + 1, MathUtils.toString(headerValue));
				}
			}
		} else {
			for (int columnIndex = 0; columnIndex < this.headerLine.getHeaders().size(); columnIndex++) {
				result.set(0, columnIndex + 1, this.headerLine.getHeader(columnIndex));
			}
		}


		//
		double rowsSum[] = new double[result.getColumnCount()];
		for (int rowIndex = 0; rowIndex < this.getRowCount(); rowIndex++) {
			// Set line header.
			result.set(rowIndex + 1, 0, this.getLineTitle(rowIndex));

			//
			double columnSum = 0;
			for (int columnIndex = 0; columnIndex < this.getColumnCount(rowIndex); columnIndex++) {
				double value = this.getYValue(rowIndex, columnIndex);
				result.set(rowIndex + 1, columnIndex + 1, MathUtils.toString(value));
				columnSum += value;
				rowsSum[columnIndex] += value;
			}
			if (columnSumEnable) {
				result.set(0, this.getColumnCount() + 1, "Total");
				result.set(rowIndex + 1, result.getColumnCount() - 1, MathUtils.toString(columnSum));
			}
		}

		//
		if (rowSumEnable) {
			double totalSum = 0.;
			for (int columnIndex = 0; columnIndex < this.getColumnCount(); columnIndex++) {
				result.set(result.getRowCount() - 1, columnIndex + 1, MathUtils.toString(rowsSum[columnIndex]));
				totalSum += rowsSum[columnIndex];
			}
			result.set(result.getRowCount() - 1, 0, "Total");
			if (columnSumEnable){
				result.set(result.getRowCount() - 1, this.getColumnCount() + 1, MathUtils.toString(totalSum));
			}
		}
		

		//
		return result;
	}

	/***
	 * 
	 * @return
	 */
	public int getColumnCount() {
		int result;

		result = this.dataLines.getMaxSize();

		//
		return result;
	}

	/***
	 * 
	 * @return
	 */
	public int getColumnCount(final int rowIndex) {
		int result;

		result = this.dataLines.getLine(rowIndex).size();

		//
		return result;
	}

	/**
	 * 
	 * @param columnIndex
	 * @return
	 */
	public String getHeader(final int columnIndex) {
		String result;

		result = this.headerLine.getHeader(columnIndex);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public String getHeadersLegend() {
		String result;

		result = this.headerLine.getLegend();

		//
		return result;
	}

	/**
	 * 
	 * @param rowIndex
	 * @return
	 */
	public String getLinesLegend() {
		String result;

		result = this.dataLines.getLegend();

		//
		return result;
	}

	public String getLineTitle(final int rowIndex) {
		String result;

		result = this.dataLines.getLine(rowIndex).getTitle();

		//
		return result;
	}

	public LogarithmType getLogarithmType() {
		return this.logarithmType;
	}

	/***
	 * 
	 * @return
	 */
	public int getRowCount() {
		int result;

		result = this.dataLines.getLineCount();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * 
	 * @return
	 */
	public GraphType getType() {
		return this.type;
	}

	public Double getVerticalMax() {
		return this.verticalMax;
	}

	/**
	 * 
	 * @param rowIndex
	 * @param columnIndex
	 * @return
	 */
	public Double getXValue(final int rowIndex, final int columnIndex) {
		Double result;

		result = this.dataLines.getLine(rowIndex).getXValue(columnIndex);

		//
		return result;
	}

	/**
	 * 
	 * @param rowIndex
	 * @param columnIndex
	 * @return
	 */
	public Double getYValue(final int rowIndex, final int columnIndex) {
		Double result;

		result = this.dataLines.getLine(rowIndex).getYValue(columnIndex);

		//
		return result;
	}

	public boolean isIntegerHorizontalUnit() {
		return this.integerHorizontalUnit;
	}

	/**
	 * 
	 * @param label
	 * @param columnIndex
	 */
	public void setHeader(final String label, final int columnIndex) {
		this.headerLine.setHeader(label, columnIndex);
	}

	/**
	 * 
	 * @param label
	 */
	public void setHeadersLegend(final String label) {
		this.headerLine.setLegend(label);
	}

	public void setIntegerHorizontalUnit(final boolean value) {
		this.integerHorizontalUnit = value;
	}

	/**
	 * 
	 * @param label
	 * @param rowIndex
	 */
	public void setLinesLegend(final String label) {
		this.dataLines.setLegend(label);
	}

	/**
	 * 
	 * @param label
	 * @param rowIndex
	 */
	public void setLineTitle(final String label, final int rowIndex) {
		this.dataLines.getLine(rowIndex).setTitle(label);
	}

	public void setLogarithmType(final LogarithmType logarithmType) {
		this.logarithmType = logarithmType;
	}

	/**
	 * 
	 * @param title
	 */
	public void setTitle(final String title) {
		this.title = title;
	}

	/**
	 * 
	 * @param type
	 */
	public void setType(final GraphType type) {
		this.type = type;
	}

	/**
	 * 
	 * @param value
	 * @param rowIndex
	 * @param columnIndex
	 */
	public void setValue(final double value, final int rowIndex, final int columnIndex) {
		this.dataLines.getLine(rowIndex).setYValue(value, columnIndex);
	}

	/**
	 * 
	 * @param value
	 * @param rowIndex
	 * @param columnIndex
	 */
	public void setValue(final Double x, final Double y, final int rowIndex, final int columnIndex) {
		this.dataLines.getLine(rowIndex).setXYValue(x, y, columnIndex);
	}

	/**
	 * Allow to set a vertical max value. For example, in a graph of percentage,
	 * the limit must not exceed 100. If value is null then the vertical max
	 * value is an automatic default one.
	 * 
	 * @param verticalMax
	 */
	public void setVerticalMax(final Double verticalMax) {
		this.verticalMax = verticalMax;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		StringList buffer = new StringList();

		buffer.appendln("Title=" + this.title);
		for (int rowIndex = 0; rowIndex < this.dataLines.getLineCount(); rowIndex++) {
			buffer.append("  Line (");
			buffer.append(rowIndex);
			buffer.append(") ");
			buffer.append(this.dataLines.getLine(rowIndex).getTitle());
			buffer.append(" ");
			buffer.append(this.dataLines.getLine(rowIndex).size());
			buffer.appendln();
		}

		result = buffer.toString();

		//
		return result;
	}
}
