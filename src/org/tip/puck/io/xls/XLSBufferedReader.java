package org.tip.puck.io.xls;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents a TXT File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class XLSBufferedReader extends BufferedReader {

	private static final Logger logger = LoggerFactory.getLogger(XLSBufferedReader.class);

	private Workbook workbook;
	private int currentSheet;
	private int currentRow;
	private int savedSheet;
	private int savedRow;

	/**
	 * @throws IOException
	 * @throws BiffException
	 * 
	 */
	public XLSBufferedReader(final File file) throws IOException {
		super(new InputStreamReader(new FileInputStream(file)));
		try {
			WorkbookSettings settings = new WorkbookSettings();
			settings.setEncoding("CP1252");
			this.workbook = Workbook.getWorkbook(file, settings);
			this.currentSheet = 0;
			this.currentRow = 0;
		} catch (BiffException exception) {
			logger.error("JXL error: " + exception.getMessage());
			throw new IOException("JXL error.", exception);
		}
	}

	/**
	 * @throws IOException
	 * @throws BiffException
	 * 
	 */
	public XLSBufferedReader(final InputStream in) throws IOException {
		super(new InputStreamReader(in));
		try {
			WorkbookSettings settings = new WorkbookSettings();
			settings.setEncoding("CP1252");
			this.workbook = Workbook.getWorkbook(in, settings);
			this.currentSheet = 0;
			this.currentRow = 0;
		} catch (BiffException exception) {
			logger.error("JXL error: " + exception.getMessage());
			throw new IOException("JXL error.", exception);
		}
	}

	/**
	 * @throws IOException
	 * 
	 */
	@Override
	public void close() throws IOException {
		super.close();
		this.workbook.close();
	}

	/**
	 * 
	 */
	@Override
	public void mark(final int size) {
		this.savedSheet = this.currentSheet;
		this.savedRow = this.currentRow;
	}

	/**
	 * 
	 */
	@Override
	public boolean markSupported() {
		boolean result;

		result = true;

		//
		return result;
	}

	/**
	 * @throws IOException
	 * 
	 */
	@Override
	public String readLine() throws IOException {
		String result;

		//
		if (this.currentSheet < this.workbook.getNumberOfSheets()) {
			Sheet sheet = this.workbook.getSheet(this.currentSheet);

			if (this.currentRow < sheet.getRows()) {
				//
				result = toTXTLine(sheet.getRow(this.currentRow));
				this.currentRow += 1;
			} else {
				result = "";
				this.currentSheet += 1;
				this.currentRow = 0;
			}
		} else {
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public void reset() {
		this.currentSheet = this.savedSheet;
		this.currentRow = this.savedRow;
	}

	/**
	 * 
	 * @param cell
	 * @return
	 */
	public static String toTXTLine(final Cell[] cells) {
		String result;

		//
		StringBuffer buffer = new StringBuffer(1024);
		for (Cell cell : cells) {
			//
			if (buffer.length() != 0) {
				buffer.append("\t");
			}

			//
			String cellContent = cell.getContents();

			// Remove special character from the cell.
			if (StringUtils.isNotBlank(cellContent)) {
				cellContent = cellContent.replaceAll("[\r\n\t]+", "");
			}

			//
			buffer.append(cellContent);
		}
		result = buffer.toString();

		//
		return result;
	}

}
