package org.tip.puck.statistics;

import java.util.ArrayList;

/**
 * 
 * @author TIP
 */
public class FiliationCounts extends ArrayList<FiliationCount> {

	private static final long serialVersionUID = 337533622477025704L;

	/**
	 * 
	 */
	public FiliationCounts() {
		super();
	}

	/**
	 * 
	 * @param capacity
	 */
	public FiliationCounts(final int capacity) {
		super(capacity);
	}

	/**
	 * 
	 * @param capacity
	 */
	public FiliationCounts(final int capacity, final int filledCapacity) {
		super(capacity);

		// Fill array.
		while (capacity >= this.size()) {
			this.add(new FiliationCount());
		}
	}

	/**
	 * 
	 * @return
	 */
	public int agnaticSum() {
		int result;

		result = 0;
		for (FiliationCount count : this) {
			result += count.getAgnatic();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int cognaticSum() {
		int result;

		result = 0;
		for (FiliationCount count : this) {
			result += count.getCognatic();
		}

		//
		return result;
	}

	/**
	 * @return
	 * 
	 */
	@Override
	public FiliationCount get(final int index) {
		FiliationCount result;

		// Fill array.
		while (index >= this.size()) {
			this.add(new FiliationCount());
		}

		//
		result = super.get(index);

		//
		return result;
	}

	/**
	 * @return
	 * 
	 */
	@Override
	public FiliationCount set(final int index, final FiliationCount value) {
		FiliationCount result;

		// Fill array.
		while (index >= this.size()) {
			this.add(new FiliationCount());
		}

		//
		result = super.set(index, value);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public FiliationCount sum() {
		FiliationCount result;

		result = new FiliationCount();
		for (FiliationCount count : this) {
			result.addAgnatic(count.getAgnatic());
			result.addCognatic(count.getCognatic());
			result.addUterine(count.getUterine());
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int uterineSum() {
		int result;

		result = 0;
		for (FiliationCount count : this) {
			result += count.getUterine();
		}

		//
		return result;
	}
}
