package org.tip.puckgui.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.UIManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author TIP
 * 
 */
public class GUIToolBox {
	private static final Logger logger = LoggerFactory.getLogger(GUIToolBox.class);

	/**
	 * 
	 * @return
	 */
	public static List<String> availableLookAndFeels() {
		List<String> result;

		//
		result = new ArrayList<String>();

		//
		for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
			//
			result.add(info.getName());
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public static void copyToClipboard(final Image source) {
		//
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

		if (clipboard != null) {
			//
			ImageTransferable selection = new ImageTransferable(source);
			clipboard.setContents(selection, null);
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static BufferedImage crop(final BufferedImage source) {
		BufferedImage result;

		if (source == null) {
			//
			result = null;

		} else {
			//
			int firstLine;
			int firstColumn;
			int lastColumn;
			int lastLine;

			//
			firstLine = 0;
			while ((firstLine < source.getHeight()) && (isWhiteLine(source, firstLine))) {
				firstLine += 1;
			}

			//
			if (firstLine == source.getHeight()) {
				//
				firstLine = 0;
				firstColumn = 0;
				lastColumn = source.getWidth() - 1;
				lastLine = source.getHeight() - 1;

			} else {
				//
				lastLine = source.getHeight() - 1;
				while ((firstLine > 0) && (isWhiteLine(source, lastLine))) {
					lastLine -= 1;
				}

				//
				firstColumn = 0;
				while ((firstColumn < source.getWidth()) && (isWhiteColumn(source, firstColumn))) {
					firstColumn += 1;
				}

				//
				lastColumn = source.getWidth() - 1;
				while ((lastColumn > 0) && (isWhiteColumn(source, lastColumn))) {
					lastColumn -= 1;
				}
			}

			//
			result = new BufferedImage((lastColumn - firstColumn + 1), (lastLine - firstLine + 1), BufferedImage.TYPE_INT_ARGB);
			result.getGraphics().drawImage(source, -firstColumn, -firstLine, null);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param line
	 * @return
	 */
	public static boolean isWhiteColumn(final BufferedImage source, final int column) {
		boolean result;

		int line = 0;
		boolean ended = false;
		result = false;
		while (!ended) {
			if (line < source.getHeight()) {
				if (source.getRGB(column, line) == Color.white.getRGB()) {
					line += 1;
				} else {
					ended = true;
					result = false;
				}
			} else {
				ended = true;
				result = true;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param line
	 * @return
	 */
	public static boolean isWhiteLine(final BufferedImage source, final int line) {
		boolean result;

		int column = 0;
		boolean ended = false;
		result = false;
		while (!ended) {
			if (column < source.getWidth()) {
				if (source.getRGB(column, line) == Color.white.getRGB()) {
					column += 1;
				} else {
					ended = true;
					result = false;
				}
			} else {
				ended = true;
				result = true;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param target
	 * @throws IOException
	 */
	public static void saveScreenshot(final Component source, final File target) throws IOException {
		//
		BufferedImage targetImage = crop(takeScreenshot(source));

		// File f = File.createTempFile("myOutputFile.jpg");
		ImageIO.write(targetImage, "png", target);
	}

	/**
	 * 
	 * @param source
	 * @param target
	 * @throws IOException
	 */
	public static BufferedImage takeScreenshot(final Component source) {
		BufferedImage result;

		//
		result = new BufferedImage(source.getWidth(), source.getHeight(), BufferedImage.TYPE_INT_RGB);

		// Now paint the component directly onto the image
		Graphics2D imageGraphics = result.createGraphics();
		source.paint(imageGraphics);

		//
		return result;
	}

}
