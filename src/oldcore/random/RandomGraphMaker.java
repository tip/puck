package oldcore.random;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import org.tip.puck.graphs.Graph;

public class RandomGraphMaker<E>  {
	
	public static <E> Graph<E> reshuffleFree (Graph<E> source){
		Graph<E> result;
		
		result = new Graph<E>(source.getLabel()+" reshuffled (free)");
		int size = source.nodeCount();
		int nrArcs = source.arcCount();
		
		for (int k=0;k<nrArcs;k++){
			result.incArcWeight((int)(Math.random()*size), (int)Math.random()*size);

//			comment("arc "+(1+k)+":\tnumber "+d+"\tnextUpperBound "+map.ceilingKey(d)+"\tcell "+Arrays.toString(i)+"\t-> "+matrix[i[0]][i[1]]);
		}
		
		//
		return result;
		
	}

	public static <E> Graph<E> reshuffleMultiNomial (Graph<E> source){
		Graph<E> result;
		
		result = new Graph<E>(source.getLabel()+" reshuffled (multinomial)");
		int size = source.nodeCount();
		int nrArcs = source.arcCount();
		
		//maps cell indices to upper segment bounds in the (0,t) interval, where segment size corresponds to row and column sum products
		TreeMap<Integer,int[]> map = new TreeMap<Integer,int[]>();
		int t = 0;
		for (int i=0;i<size;i++){
			for (int j=0;j<size;j++){
				int d = new Double(source.getOutForce(i)*source.getInForce(j)).intValue();
				if (d==0) continue;
				t=t+d;
				map.put(t, new int[]{i,j});
			}
		}
		 
		for (int k=0;k<nrArcs;k++){
			int[] i = map.get(map.ceilingKey((int)(Math.random()*nrArcs*nrArcs)));
			result.incArcWeight(i[0], i[1]);

//			comment("arc "+(1+k)+":\tnumber "+d+"\tnextUpperBound "+map.ceilingKey(d)+"\tcell "+Arrays.toString(i)+"\t-> "+matrix[i[0]][i[1]]);
		}
		
		//
		return result;
		
	}
	
	public static <E> Graph<E> permute (Graph<E> source){
		Graph<E> result;
		
		result = new Graph<E>(source.getLabel()+" permuted");
		int size = source.nodeCount();
		
		List<Integer> rowIndex = new ArrayList<Integer>();
		List<Integer> columnIndex = new ArrayList<Integer>();
		double[][] countdown = new double[2][size];
		
		for (int i=0;i<size;i++){
			double outForce = source.getOutForce(i);
			double inForce = source.getInForce(i);
			if (inForce+outForce<2) continue;
			if (outForce>0) rowIndex.add(i);
			if (inForce>0) columnIndex.add(i);
			countdown[0][i]=outForce;
			countdown[1][i]=inForce;
		}
		
		while (rowIndex.size()>0 && columnIndex.size()>0){
			int a = (int)(Math.random()*rowIndex.size());
			int b = (int)(Math.random()*columnIndex.size());
			int i = rowIndex.get(a);
			int j = columnIndex.get(b);
			result.incArcWeight(i, j);
			countdown[0][i]--;
			countdown[1][j]--;
			if (countdown[0][i]==0) rowIndex.remove(a);
			if (countdown[1][j]==0) columnIndex.remove(b);
		}
		
		//
		return result;
	}
	
	/**
	 * @author telmo menezes
	 * @param source
	 * @param k
	 * @param maxGenDist
	 * @param minShufflePer
	 * @param minStableIter
	 * @return
	 */
	public static <E>  Graph<E> reshuffleByIterations (Graph<E> source, int k, int maxGenDist,
			double minShufflePer, int minStableIter) {
		
		Graph<E> result;
		
		result = source.clone();
		
		result.setLabel(source.getLabel()+" reshuffled by Iterations");
		
//		PGraph origPGraph = new PGraph(source);
//		source.storeOriginalDescendants();
		
		int iteration = 0;
		int switches = 0;
		double maxDist = 0;
		int stableIterations = 0;
		
		boolean stop = false;
		while (!stop) {
			
			double shuffled = percentageShuffledLinks(result);
			double dist = distance(result, source);
			
			System.out.println("#iter " + iteration + "\t\tswaps: " + switches + "\t\tstable iter: " + stableIterations);
			System.out.println("  shuffled: " + shuffled + "%; dist: " + dist + "; max dist: " + maxDist);
			
			if (dist > maxDist) {
				maxDist = dist;
				stableIterations = 0;
			}
			else {
				stableIterations++;
			}
			if (switchLinks(result, k, maxGenDist)) {
				switches++;
			}
			iteration++;
			
			if ((shuffled >= minShufflePer) && (stableIterations >= minStableIter)) {
				stop = true;
			}
		}
		//
		return result;
	}

	
	
	
}
