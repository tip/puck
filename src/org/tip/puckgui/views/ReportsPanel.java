package org.tip.puckgui.views;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.PuckManager;
import org.tip.puck.report.Report;
import org.tip.puckgui.WindowGUI;
import org.tip.puckgui.util.GenericFileFilter;

/**
 * 
 * @author TIP
 */
public class ReportsPanel extends JPanel {

	private static final Logger logger = LoggerFactory.getLogger(ReportsPanel.class);
	private static final long serialVersionUID = -1537588159515830202L;

	private File file;
	private Report report;

	/**
	 * Create the panel.
	 */
	public ReportsPanel(final WindowGUI parent, final Report report) {

		this.report = report;

		// Initialize specific variables.
		this.file = new File("report-" + new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime()) + ".txt");

		// /////////////////////////
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		JTabbedPane tabbedPaneReports = new JTabbedPane(JTabbedPane.TOP);
		tabbedPaneReports.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		add(tabbedPaneReports);

		//
		{
			ReportPanel reportPanel = new ReportPanel(parent, report);
			tabbedPaneReports.addTab(report.title(), null, reportPanel, null);
			for (Report subReport : report.subReports()) {
				reportPanel = new ReportPanel(parent, subReport);
				tabbedPaneReports.addTab(subReport.title(), null, reportPanel, null);
			}
			tabbedPaneReports.setSelectedIndex(0);
		}

		JPanel panelButtons = new JPanel();
		add(panelButtons);
		panelButtons.setLayout(new BoxLayout(panelButtons, BoxLayout.X_AXIS));

		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				parent.closeCurrentTab();
			}
		});
		panelButtons.add(btnClose);

		Component horizontalGlue = Box.createHorizontalGlue();
		panelButtons.add(horizontalGlue);

		JButton btnSaveButton = new JButton("Save");
		btnSaveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Save.
				try {
					File targetFile = new File("report-" + report.target() + "-"
							+ (new SimpleDateFormat("yyyyMMddhhmmss").format(Calendar.getInstance().getTime())) + ".txt");

					boolean ended = false;
					while (!ended) {

						// Manage possibility to select a target file.
						JFileChooser chooser = new JFileChooser();
						chooser.setSelectedFile(targetFile);

						chooser.setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.saveAsFileChooser.text"));
						chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
						chooser.setAcceptAllFileFilterUsed(false);
						GenericFileFilter defaultFileFilter = new GenericFileFilter("Report Formats (*.txt, *.xls)", "txt", "xls");
						chooser.addChoosableFileFilter(defaultFileFilter);
						chooser.addChoosableFileFilter(new GenericFileFilter("Text (*.txt)", "txt"));
						chooser.addChoosableFileFilter(new GenericFileFilter("Microsoft Excel (*.xls)", "xls"));
						chooser.setFileFilter(defaultFileFilter);

						//
						if (chooser.showSaveDialog(parent.getJFrame()) == JFileChooser.APPROVE_OPTION) {
							System.out.println("getCurrentDirectory(): " + chooser.getCurrentDirectory());
							System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
							targetFile = chooser.getSelectedFile();
						} else {
							// Nothing to do.
							System.out.println("No Selection ");
							ended = true;
						}

						//
						if (!ended) {
							boolean doSave;
							if (targetFile.exists()) {
								// Manage confirmation dialog.
								String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.title");
								String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.text");

								int response = JOptionPane.showConfirmDialog(parent.getJFrame(), message, title, JOptionPane.YES_NO_CANCEL_OPTION);

								if (response == JOptionPane.YES_OPTION) {
									doSave = true;
									ended = true;
								} else if (response == JOptionPane.NO_OPTION) {
									doSave = false;
									ended = false;
								} else {
									doSave = false;
									ended = true;
								}
							} else {
								doSave = true;
								ended = true;
							}

							if (doSave) {
								PuckManager.saveReport(targetFile, report);
							}
						}
					}
				} catch (final PuckException exception) {
					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedFileFormat");
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.notAFile");
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedEncoding");
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.default");
					}

					//
					JOptionPane.showMessageDialog(parent.getJFrame(), message, title, JOptionPane.ERROR_MESSAGE);
				}
			}

		});
		panelButtons.add(btnSaveButton);
	}
}
