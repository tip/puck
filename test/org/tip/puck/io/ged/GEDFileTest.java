package org.tip.puck.io.ged;

import java.io.File;
import java.util.Calendar;
import java.util.regex.Matcher;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Net;
import org.tip.puck.util.Chronometer;
import org.tip.puck.util.LogHelper;

/**
 * 
 * @author TIP
 */
public class GEDFileTest {
	private static final Logger logger = LoggerFactory.getLogger(GEDFileTest.class);

	public static final String SIMPLE_FILE = "test/Resources/Chimane6.ged";
	public static final String COMPLEX_FILE = "test/Resources/Ebrei_08.ged";

	/**
	 * 
     */
	@Test
	public void testGEDLinePattern01() throws Exception {

		logger.debug("========================== testGEDLinePattern01");

		Matcher matcher = GEDFile.GEDLINE_PATTERN.matcher("0 HEAD");

		System.out.println("find=" + matcher.find());
		System.out.println("count=" + matcher.groupCount());

		Assertions.assertThat(matcher.find()).isTrue();
		Assertions.assertThat(matcher.groupCount()).equals(12);
	}

	/**
	 * 
     */
	@Test
	public void testGEDLinePattern02() throws Exception {

		logger.debug("========================== testGEDLinePattern02");

		Matcher matcher = GEDFile.GEDLINE_PATTERN.matcher("\uFEFF0 HEAD");

		System.out.println("find=" + matcher.find());
		System.out.println("count=" + matcher.groupCount());

		Assertions.assertThat(matcher.find()).equals(true);
		Assertions.assertThat(matcher.groupCount()).equals(12);
	}

	/**
     */
	@Test
	public void testLoad01() throws Exception {

		logger.debug("========================== testLoad01");

		Chronometer chrono = new Chronometer();
		Net target = GEDFile.load(new File(SIMPLE_FILE));
		logger.debug("TimeSpent=" + chrono.stop().interval());
		logger.debug("Net=" + LogHelper.toString(target));

		Assertions.assertThat(target).isNotNull();

		Assertions.assertThat(target.individuals().size()).isEqualTo(2642);
		Assertions.assertThat(target.families().size()).isEqualTo(818);

		Assertions.assertThat(target.individuals().getById(50).getId()).isEqualTo(50);
		Assertions.assertThat(target.individuals().getById(50).getName()).isEqualTo("Delicia/");
		Assertions.assertThat(target.individuals().getById(50).getGender()).isEqualTo(Gender.FEMALE);
		Assertions.assertThat(target.individuals().getById(50).getMother()).isNotNull();
		Assertions.assertThat(target.individuals().getById(50).getMother().getId()).isEqualTo(44);
		Assertions.assertThat(target.individuals().getById(50).getFather()).isNotNull();
		Assertions.assertThat(target.individuals().getById(50).getFather().getId()).isEqualTo(20);
		Assertions.assertThat(target.individuals().getById(50).spouses().size()).isEqualTo(3);
		Assertions.assertThat(target.individuals().getById(50).children().size()).isEqualTo(3);
		Assertions.assertThat(target.individuals().getById(50).attributes().size()).isEqualTo(0);
	}

	/**
     */
	@Test
	public void testLoad02() throws Exception {

		logger.debug("========================== testLoad02");

		Chronometer chrono = new Chronometer();
		Net target = GEDFile.load(new File(COMPLEX_FILE));
		logger.debug("TimeSpent=" + chrono.stop().interval());
		logger.debug("Net=" + LogHelper.toString(target));

		Assertions.assertThat(target).isNotNull();

		Assertions.assertThat(target.individuals().size()).isEqualTo(4304);
		Assertions.assertThat(target.families().size()).isEqualTo(2242);

		Assertions.assertThat(target.individuals().getById(116).getId()).isEqualTo(116);
		Assertions.assertThat(target.individuals().getById(116).getName()).isEqualTo("Salomone /Mondolfo/");
		Assertions.assertThat(target.individuals().getById(116).getGender()).isEqualTo(Gender.MALE);
		Assertions.assertThat(target.individuals().getById(116).getMother()).isNotNull();
		Assertions.assertThat(target.individuals().getById(116).getMother().getId()).isEqualTo(444);
		Assertions.assertThat(target.individuals().getById(116).getFather()).isNotNull();
		Assertions.assertThat(target.individuals().getById(116).getFather().getId()).isEqualTo(114);
		Assertions.assertThat(target.individuals().getById(116).spouses().size()).isEqualTo(2);
		Assertions.assertThat(target.individuals().getById(116).children().size()).isEqualTo(4);
		Assertions.assertThat(target.individuals().getById(116).attributes().size()).isEqualTo(5);
	}

	/**
     */
	@Test
	public void testSave01() throws Exception {

		logger.debug("========================== testSave01");

		// Load source.
		File sourceFile = new File(SIMPLE_FILE);
		Chronometer chrono = new Chronometer();
		Net source = GEDFile.load(sourceFile);
		logger.debug("TimeSpent=" + chrono.stop().interval());
		logger.debug("Net=" + LogHelper.toString(source));

		// Write target.
		File targetFile = new File("tmp/target-" + Calendar.getInstance().getTimeInMillis() + ".txt");
		targetFile.delete();
		chrono.reset().start();
		GEDFile.save(targetFile, source);
		logger.debug("TimeSpent=" + chrono.stop().interval());

		Assertions.assertThat(targetFile.exists()).isTrue();
		Assertions.assertThat(targetFile.length()).isNotEqualTo(0);

		// Load target.
		chrono.reset().start();
		Net target = GEDFile.load(targetFile);
		logger.debug("TimeSpent=" + chrono.stop().interval());
		logger.debug("Net=" + LogHelper.toString(source));

		Assertions.assertThat(target).isNotNull();

		Assertions.assertThat(target.individuals().size()).isEqualTo(source.individuals().size());
		Assertions.assertThat(target.families().size()).isEqualTo(source.families().size());

		Assertions.assertThat(target.individuals().getById(50).getId()).isEqualTo(50);
		Assertions.assertThat(target.individuals().getById(50).getName()).isEqualTo("Delicia/");
		Assertions.assertThat(target.individuals().getById(50).getGender()).isEqualTo(Gender.FEMALE);
		Assertions.assertThat(target.individuals().getById(50).getMother()).isNotNull();
		Assertions.assertThat(target.individuals().getById(50).getMother().getId()).isEqualTo(44);
		Assertions.assertThat(target.individuals().getById(50).getFather()).isNotNull();
		Assertions.assertThat(target.individuals().getById(50).getFather().getId()).isEqualTo(20);
		Assertions.assertThat(target.individuals().getById(50).spouses().size()).isEqualTo(3);
		Assertions.assertThat(target.individuals().getById(50).children().size()).isEqualTo(3);
		Assertions.assertThat(target.individuals().getById(50).attributes().size()).isEqualTo(0);

		//
		targetFile.delete();
	}

	/**
     */
	@Test
	public void testSave02() throws Exception {

		logger.debug("========================== testSave02");

		// Load source.
		File sourceFile = new File(COMPLEX_FILE);
		Chronometer chrono = new Chronometer();
		Net source = GEDFile.load(sourceFile);
		logger.debug("TimeSpent=" + chrono.stop().interval());
		logger.debug("Net=" + LogHelper.toString(source));

		// Write target.
		File targetFile = new File("tmp/target-" + Calendar.getInstance().getTimeInMillis() + ".txt");
		targetFile.delete();
		chrono.reset().start();
		GEDFile.save(targetFile, source);
		logger.debug("TimeSpent=" + chrono.stop().interval());

		Assertions.assertThat(targetFile.exists()).isTrue();
		Assertions.assertThat(targetFile.length()).isNotEqualTo(0);

		// Load target.
		chrono.reset().start();
		Net target = GEDFile.load(targetFile);
		logger.debug("TimeSpent=" + chrono.stop().interval());
		logger.debug("Net=" + LogHelper.toString(source));

		Assertions.assertThat(target).isNotNull();

		Assertions.assertThat(target.individuals().size()).isEqualTo(source.individuals().size());
		Assertions.assertThat(target.families().size()).isEqualTo(source.families().size());

		Assertions.assertThat(target.individuals().getById(116).getId()).isEqualTo(116);
		Assertions.assertThat(target.individuals().getById(116).getName()).isEqualTo("Salomone /Mondolfo/");
		Assertions.assertThat(target.individuals().getById(116).getGender()).isEqualTo(Gender.MALE);
		Assertions.assertThat(target.individuals().getById(116).getMother()).isNotNull();
		Assertions.assertThat(target.individuals().getById(116).getMother().getId()).isEqualTo(444);
		Assertions.assertThat(target.individuals().getById(116).getFather()).isNotNull();
		Assertions.assertThat(target.individuals().getById(116).getFather().getId()).isEqualTo(114);
		Assertions.assertThat(target.individuals().getById(116).spouses().size()).isEqualTo(2);
		Assertions.assertThat(target.individuals().getById(116).children().size()).isEqualTo(4);
		Assertions.assertThat(target.individuals().getById(116).attributes().size()).isEqualTo(5);

		//
		targetFile.delete();
	}

}
