package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellEditor;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.PuckManager;
import org.tip.puck.io.paj.PAJFile;
import org.tip.puck.net.Attributes;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.relations.roles.RoleRelationMaker;
import org.tip.puck.net.relations.roles.RoleRelationReporter;
import org.tip.puck.net.relations.roles.RoleRelationWorker;
import org.tip.puck.net.workers.AttributeWorker;
import org.tip.puck.report.Report;
import org.tip.puck.util.PuckUtils;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.PuckGUI;
import org.tip.puckgui.models.ActorsModel;
import org.tip.puckgui.models.RelationsCellRenderer;
import org.tip.puckgui.models.RelationsModel;
import org.tip.puckgui.util.AutoComboBox;
import org.tip.puckgui.util.ComboBoxIds;
import org.tip.puckgui.util.GenericFileFilter;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.StringList;
import fr.devinsy.util.StringSet;

/**
 * 
 * @author TIP
 */
public class RelationsPanel extends JPanel {

	private static final long serialVersionUID = -6488494509652107834L;

	private static final Logger logger = LoggerFactory.getLogger(RelationsPanel.class);

	int reportCounter = 0;
	private StringSet relationAttributeTemplates;
	private StringSet actorAttributeTemplates;
	private JPanel thisJPanel;
	private RelationModel relationModel;
	private JTextField txtfldSearchRelation;
	private NetGUI netGUI;
	private JTable tableRelationActors;
	private JLabel lblRelationId;
	private JLabel lblRelationName;
	JLabel lblRelationModel;
	JList relationList;
	JScrollPane relationsScrollPane;
	private JLabel lblRelationActors;
	private JLabel lblPosition;
	private JTextField txtfldRelationName;
	private AutoComboBox cmbbxActorIds;
	private AutoComboBox cmbbxActorRoles;
	private AutoComboBox cmbbxRelationOrders;
	private AttributesPanel relationAttributesPanel;
	private AttributesPanel actorAttributesPanel;

	/**
	 * Initialize the contents of the frame.
	 */
	public RelationsPanel(final NetGUI guiManager, final RelationModel relationModel) {

		//
		this.thisJPanel = this;
		this.netGUI = guiManager;
		this.relationModel = relationModel;
		this.relationAttributeTemplates = new StringSet();
		this.actorAttributeTemplates = new StringSet();
		updateAttributeTemplates();

		//
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		JSplitPane relationsSplit = new JSplitPane();
		add(relationsSplit);
		relationsSplit.setAlignmentX(Component.CENTER_ALIGNMENT);

		JPanel relationsPanel = new JPanel();
		relationsPanel.setMinimumSize(new Dimension(200, 10));
		relationsSplit.setLeftComponent(relationsPanel);
		relationsPanel.setLayout(new BoxLayout(relationsPanel, BoxLayout.Y_AXIS));

		this.relationsScrollPane = new JScrollPane();
		this.relationsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		relationsPanel.add(this.relationsScrollPane);

		this.relationList = new JList();
		this.relationList.setDoubleBuffered(true);
		this.relationList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(final ListSelectionEvent event) {
				//
				if (!event.getValueIsAdjusting()) {
					// Selected.
					if (RelationsPanel.this.relationList.getSelectedIndex() != -1) {
						Relation relation = (Relation) ((JList) event.getSource()).getModel().getElementAt(RelationsPanel.this.relationList.getSelectedIndex());
						updateRelationIdentity(relation);
					}
				}
			}
		});
		this.relationList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		this.relationList.setCellRenderer(new RelationsCellRenderer());
		this.relationList.setModel(new RelationsModel(this.netGUI.getSegmentation().getCurrentRelations().getByModel(relationModel)));
		this.relationsScrollPane.setViewportView(this.relationList);

		JPanel relationPanel = new JPanel();
		relationsSplit.setRightComponent(relationPanel);
		relationPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		relationPanel.setAlignmentY(Component.TOP_ALIGNMENT);
		relationPanel.setLayout(new BoxLayout(relationPanel, BoxLayout.Y_AXIS));

		JPanel panel_4 = new JPanel();
		panel_4.setAlignmentX(Component.LEFT_ALIGNMENT);
		relationPanel.add(panel_4);
		panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.X_AXIS));

		this.lblRelationModel = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("RelationsPanel.lblRelationModel.text")); //$NON-NLS-1$ //$NON-NLS-2$
		this.lblRelationModel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Relation Model double clicked.
				if (event.getClickCount() == 2) {
					int selectedIndex = RelationsPanel.this.relationList.getSelectedIndex();
					if (selectedIndex != -1) {
					}
				}
			}
		});

		JLabel lblModel = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("RelationsPanel.lblModel.text")); //$NON-NLS-1$ //$NON-NLS-2$
		panel_4.add(lblModel);

		Component horizontalStrut_2 = Box.createHorizontalStrut(10);
		panel_4.add(horizontalStrut_2);
		panel_4.add(this.lblRelationModel);

		Component horizontalStrut_4 = Box.createHorizontalStrut(10);
		panel_4.add(horizontalStrut_4);

		JButton btnEditModel = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("RelationsPanel.btnEditModel.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnEditModel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Edit Relation Model.
				try {
					RelationModelInputWindow window = new RelationModelInputWindow(RelationsPanel.this.netGUI, relationModel);
					window.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					window.setVisible(true);
				} catch (final Exception exception) {

					//
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JButton btnSaveModel = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("RelationsPanel.btnSaveModel.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnSaveModel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				// Save.
				try {
					File targetFile = new File(relationModel.getName() + ".txt");

					boolean ended = false;
					while (!ended) {

						// Manage possibility to select a target file.
						JFileChooser chooser = new JFileChooser();
						chooser.setSelectedFile(targetFile);

						chooser.setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.saveAsFileChooser.text"));
						chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
						chooser.setAcceptAllFileFilterUsed(false);
						GenericFileFilter defaultFileFilter = new GenericFileFilter("Report Formats (*.txt, *.xls)", "txt", "xls");
						chooser.addChoosableFileFilter(defaultFileFilter);
						chooser.addChoosableFileFilter(new GenericFileFilter("Text (*.txt)", "txt"));
						chooser.addChoosableFileFilter(new GenericFileFilter("Microsoft Excel (*.xls)", "xls"));
						chooser.addChoosableFileFilter(new GenericFileFilter("KinTermMap (*.xml)", "xml"));
						chooser.setFileFilter(defaultFileFilter);

						//
						if (chooser.showSaveDialog(RelationsPanel.this.thisJPanel) == JFileChooser.APPROVE_OPTION) {
							System.out.println("getCurrentDirectory(): " + chooser.getCurrentDirectory());
							System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
							targetFile = chooser.getSelectedFile();
						} else {
							// Nothing to do.
							System.out.println("No Selection ");
							ended = true;
						}

						//
						if (!ended) {
							boolean doSave;
							if (targetFile.exists()) {
								// Manage confirmation dialog.
								String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.title");
								String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.text");

								int response = JOptionPane.showConfirmDialog(RelationsPanel.this.thisJPanel, message, title, JOptionPane.YES_NO_CANCEL_OPTION);

								if (response == JOptionPane.YES_OPTION) {
									doSave = true;
									ended = true;
								} else if (response == JOptionPane.NO_OPTION) {
									doSave = false;
									ended = false;
								} else {
									doSave = false;
									ended = true;
								}
							} else {
								doSave = true;
								ended = true;
							}

							if (doSave) {
								Report exportReport = PuckManager.saveRelationModel(targetFile, relationModel);
								RelationsPanel.this.netGUI.addReportTab(exportReport);
							}
						}
					}
				} catch (final PuckException exception) {
					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedFileFormat");
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.notAFile");
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedEncoding");
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.default");
					}

					//
					JOptionPane.showMessageDialog(RelationsPanel.this.thisJPanel, message, title, JOptionPane.ERROR_MESSAGE);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		});
		panel_4.add(btnSaveModel);
		panel_4.add(btnEditModel);

		JButton btnApplyModel = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("RelationsPanel.btnApplyModel.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnApplyModel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				
				int maxIterations = PuckGUI.instance().getPreferences().getTerminologyMaxIterations();
				
				try {
					
					RoleRelationMaker.applyModel(RelationsPanel.this.netGUI.getNet(), RelationsPanel.this.netGUI.getSegmentation(), relationModel, maxIterations);
					//
					RelationsPanel.this.update();
					RelationsPanel.this.netGUI.setChanged(true);
					RelationsPanel.this.netGUI.updateAll();

				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panel_4.add(btnApplyModel);

		JButton btnGraphButton = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("RelationsPanel.btnNewButton.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnGraphButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				//
				try {
					File targetFile = new File("modelGraphExport.paj");
					boolean ended = false;
					while (!ended) {
						JFileChooser chooser = new JFileChooser();
						chooser.setSelectedFile(targetFile);

						chooser.setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.saveAsFileChooser.text"));
						chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
						chooser.setAcceptAllFileFilterUsed(false);
						GenericFileFilter defaultFileFilter = new GenericFileFilter("Pajek files (*.paj)", "paj");
						chooser.addChoosableFileFilter(defaultFileFilter);
						chooser.setFileFilter(defaultFileFilter);

						//
						if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
							targetFile = chooser.getSelectedFile();

							boolean doSave;
							if (targetFile.exists()) {
								// Manage confirmation dialog.
								String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.title");
								String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.text");

								int response = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_CANCEL_OPTION);

								if (response == JOptionPane.YES_OPTION) {
									doSave = true;
									ended = true;
								} else if (response == JOptionPane.NO_OPTION) {
									doSave = false;
									ended = false;
								} else {
									doSave = false;
									ended = true;
								}
							} else {
								doSave = true;
								ended = true;
							}

							if (doSave) {
								StringList pajekBuffer = new StringList();
								List<String> partitionLabels = Arrays.asList(new String[]{"GENDER"});
								
								RoleRelationWorker worker = new RoleRelationWorker(relationModel,0);
								
								pajekBuffer.addAll(PuckUtils.writePajekNetwork(worker.getRoleRelationGraph(Gender.MALE),partitionLabels));
								pajekBuffer.addAll(PuckUtils.writePajekNetwork(worker.getRoleRelationGraph(Gender.FEMALE),partitionLabels));
								pajekBuffer.addAll(PuckUtils.writePajekNetwork(worker.getRoleRelationGraph(null),partitionLabels));
								pajekBuffer.addAll(PuckUtils.writePajekNetwork(worker.getRoleRelationGraphInternal(null),partitionLabels));
//								pajekBuffer.addAll(PuckUtils.writePajekNetwork(GraphUtils.insideClusters(worker.getRoleRelationGraph(Gender.UNKNOWN),"GENDER")));
								
								PAJFile.exportToPajek(pajekBuffer, targetFile.getAbsolutePath(), null);
							}

						} else {
							// Nothing to do.
							System.out.println("No Selection ");
							ended = true;
						}
					}
				} catch (final PuckException exception) {
					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedFileFormat");
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.notAFile");
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedEncoding");
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.default");
					}

					//
					JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panel_4.add(btnGraphButton);

		JButton btnAnalyseModel = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("RelationsPanel.btnAnalyseModel.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnAnalyseModel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				try {
					int maxIterations = PuckGUI.instance().getPreferences().getTerminologyMaxIterations();
					Report report = RoleRelationReporter.reportRoleRelations(relationModel,maxIterations);
					RelationsPanel.this.netGUI.addReportTab(report);
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panel_4.add(btnAnalyseModel);

		Component horizontalGlue = Box.createHorizontalGlue();
		panel_4.add(horizontalGlue);

		JButton btnDeleteModel = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("RelationsPanel.btnDeleteModel.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnDeleteModel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Remove Model.
				if (RelationsPanel.this.netGUI.getNet().relations().getByModel(relationModel).size() == 0) {
					//
					int response = JOptionPane.showConfirmDialog(null, "Confirm relation model delete?", "Confirm", JOptionPane.YES_NO_OPTION);

					//
					if (response == JOptionPane.YES_OPTION) {
						RelationsPanel.this.netGUI.getNet().relationModels().remove(relationModel);
						RelationsPanel.this.netGUI.closeRelationTab(relationModel);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Relation list of this model is not empty.", "Delete Model", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		panel_4.add(btnDeleteModel);

		JPanel panelRelationIdentity = new JPanel();
		panelRelationIdentity.setAlignmentX(Component.LEFT_ALIGNMENT);
		relationPanel.add(panelRelationIdentity);
		panelRelationIdentity.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, }));

		JPanel panelId = new JPanel();
		panelRelationIdentity.add(panelId, "4, 2");
		panelId.setLayout(new BoxLayout(panelId, BoxLayout.Y_AXIS));

		Component verticalStrut_9 = Box.createVerticalStrut(10);
		verticalStrut_9.setMaximumSize(new Dimension(20, 20));
		panelId.add(verticalStrut_9);

		this.lblRelationId = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.label.text_2"));

		this.lblRelationId.setAlignmentX(Component.RIGHT_ALIGNMENT);
		panelId.add(this.lblRelationId);

		Component verticalStrut_8 = Box.createVerticalStrut(10);
		verticalStrut_8.setMaximumSize(new Dimension(20, 10));
		panelId.add(verticalStrut_8);

		this.lblPosition = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.lblNewLabel.text")); //$NON-NLS-1$ //$NON-NLS-2$
		this.lblPosition.setAlignmentX(Component.RIGHT_ALIGNMENT);
		panelId.add(this.lblPosition);

		this.lblRelationName = new JLabel("----");
		panelRelationIdentity.add(this.lblRelationName, "6, 2");
		this.lblRelationName.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Relation name clicked.
				if (event.getButton() == MouseEvent.BUTTON1) {
					//
					setRelationNameEditorOn();
				}
			}
		});
		this.lblRelationName.setFont(new Font("Dialog", Font.PLAIN, 24));

		this.txtfldRelationName = new JTextField();
		this.txtfldRelationName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent event) {
				if (event.getKeyCode() == KeyEvent.VK_ESCAPE) {
					logger.debug("escape pressed");
					RelationsPanel.this.txtfldRelationName.setText(RelationsPanel.this.lblRelationName.getText());
					RelationsPanel.this.txtfldRelationName.transferFocus();
				}
			}
		});
		this.txtfldRelationName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(final FocusEvent event) {
				// Relation name changed.
				logger.debug("name focus lost");
				if (StringUtils.isBlank(RelationsPanel.this.txtfldRelationName.getText())) {
					if (StringUtils.isBlank(RelationsPanel.this.lblRelationName.getText())) {
						Relation currentRelation = getSelectedRelation();
						currentRelation.setName("?");
						update();
					}
				} else if (!StringUtils.equals(RelationsPanel.this.txtfldRelationName.getText(), RelationsPanel.this.lblRelationName.getText())) {
					Relation currentRelation = getSelectedRelation();
					currentRelation.setName(RelationsPanel.this.txtfldRelationName.getText());
					update();
				}

				RelationsPanel.this.txtfldRelationName.setVisible(false);
				RelationsPanel.this.lblRelationName.setVisible(true);
			}
		});
		this.txtfldRelationName.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Relation name changed on Enter pressed.
				logger.debug("perform");
				RelationsPanel.this.txtfldRelationName.transferFocus();
			}
		});
		panelRelationIdentity.add(this.txtfldRelationName, "8, 2");
		this.txtfldRelationName.setText("");
		this.txtfldRelationName.setColumns(10);

		JPanel panel_6 = new JPanel();
		panel_6.setAlignmentX(Component.LEFT_ALIGNMENT);
		relationPanel.add(panel_6);
		panel_6.setLayout(new BorderLayout(0, 0));

		JSplitPane splitPane_1 = new JSplitPane();
		panel_6.add(splitPane_1, BorderLayout.CENTER);
		splitPane_1.setOrientation(JSplitPane.VERTICAL_SPLIT);

		JPanel actorsPanel = new JPanel();
		actorsPanel.setMinimumSize(new Dimension(10, 200));
		actorsPanel.setBorder(new EmptyBorder(0, 0, 0, 10));
		actorsPanel.setAlignmentY(Component.TOP_ALIGNMENT);
		actorsPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		splitPane_1.setLeftComponent(actorsPanel);
		actorsPanel.setLayout(new BoxLayout(actorsPanel, BoxLayout.Y_AXIS));

		Component verticalStrut_6 = Box.createVerticalStrut(5);
		actorsPanel.add(verticalStrut_6);

		JPanel panel = new JPanel();
		panel.setAlignmentX(Component.LEFT_ALIGNMENT);
		actorsPanel.add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

		this.lblRelationActors = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.lblActors.text"));
		panel.add(this.lblRelationActors);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		panel.add(horizontalStrut);

		JButton btnAddActor = new JButton(" + ");
		btnAddActor.addActionListener(new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				try {
					// Add an actor in relation.
					if (relationModel.roles().isEmpty()) {
						//
						String title = "Actor creation impossible";
						String message = "None role available.\nPlease, in order to create an actor,\nyou have to create role in relation model before.";

						//
						JOptionPane.showMessageDialog(RelationsPanel.this.thisJPanel, message, title, JOptionPane.INFORMATION_MESSAGE);

					} else if (RelationsPanel.this.netGUI.getCurrentIndividuals().size() > 0) {
						//
						if (((ActorsModel) RelationsPanel.this.tableRelationActors.getModel()).getSource() != null) {
							// Add Role.
							if (RelationsPanel.this.tableRelationActors.getModel().getRowCount() == ((ActorsModel) RelationsPanel.this.tableRelationActors
									.getModel()).getSource().size()) {
								((ActorsModel) RelationsPanel.this.tableRelationActors.getModel()).setNewItem();

								RelationsPanel.this.tableRelationActors.setRowSelectionInterval(RelationsPanel.this.tableRelationActors.getModel()
										.getRowCount() - 1, RelationsPanel.this.tableRelationActors.getModel().getRowCount() - 1);
								RelationsPanel.this.tableRelationActors.setRowSelectionInterval(RelationsPanel.this.tableRelationActors.getModel()
										.getRowCount() - 1, RelationsPanel.this.tableRelationActors.getModel().getRowCount() - 1);
								RelationsPanel.this.tableRelationActors.setColumnSelectionInterval(0, 0);
								RelationsPanel.this.tableRelationActors.editCellAt(RelationsPanel.this.tableRelationActors.getModel().getRowCount() - 1, 0);
								RelationsPanel.this.tableRelationActors.getEditorComponent().requestFocus();
							}
						}
					}
				} catch (final Exception exception) {
					//
					exception.printStackTrace();
				}
			}
		});
		btnAddActor.setBorder(new EmptyBorder(0, 0, 0, 0));
		panel.add(btnAddActor);

		Component verticalStrut = Box.createVerticalStrut(5);
		actorsPanel.add(verticalStrut);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBackground(new Color(255, 255, 255));
		scrollPane_1.setAlignmentX(Component.LEFT_ALIGNMENT);
		actorsPanel.add(scrollPane_1);

		this.tableRelationActors = new JTable();
		this.tableRelationActors.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			/**
			 * This method refresh the actor attributes table.
			 */
			@Override
			public void valueChanged(final ListSelectionEvent event) {

				if (!event.getValueIsAdjusting()) {

					int selectedIndex = RelationsPanel.this.tableRelationActors.getSelectedRow();

					if (selectedIndex == -1) {

						RelationsPanel.this.actorAttributesPanel.setSource(null);

					} else {
						// Redirect to an individual.
						Actor selected = ((ActorsModel) RelationsPanel.this.tableRelationActors.getModel()).getSource().get(selectedIndex);
						if (selected != null) {
							//
							RelationsPanel.this.actorAttributesPanel.setSource(selected.attributes());
						}
					}
				}
			}
		});
		this.tableRelationActors.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Relation actor clicked.
				try {
					if (event.getClickCount() == 1) {
						//
						int selectedIndex = RelationsPanel.this.tableRelationActors.getSelectedRow();
						int selectedColumn = RelationsPanel.this.tableRelationActors.getSelectedColumn();
						if ((selectedIndex != -1) && (selectedColumn != -1)) {
							// Redirect to an individual.
							Actor selected = ((ActorsModel) RelationsPanel.this.tableRelationActors.getModel()).getSource().get(selectedIndex);
							if (selected != null) {
								//
								RelationsPanel.this.actorAttributesPanel.setSource(selected.attributes());
							}
						}
					} else if (event.getClickCount() == 2) {
						//
						int selectedIndex = RelationsPanel.this.tableRelationActors.getSelectedRow();
						int selectedColumn = RelationsPanel.this.tableRelationActors.getSelectedColumn();
						if ((selectedIndex != -1) && (selectedColumn != -1)) {
							// Redirect to an individual.
							Individual selected = ((ActorsModel) RelationsPanel.this.tableRelationActors.getModel()).getSource().get(selectedIndex)
									.getIndividual();
							if (selected != null) {
								//
								RelationsPanel.this.netGUI.changeSegmentationToCluster(selected);
							}
						}
					}
				} catch (final Exception exception) {
					//
					exception.printStackTrace();
				}
			}
		});

		JPopupMenu popupMenu = new JPopupMenu();
		addPopup(this.tableRelationActors, popupMenu);

		JMenuItem mntmDelete = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("RelationsPanel.mntmDelete.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmDelete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Delete family children.
				logger.debug("Delete " + ArrayUtils.toString(RelationsPanel.this.tableRelationActors.getSelectedRows()));

				int[] selectedRowIds = RelationsPanel.this.tableRelationActors.getSelectedRows();
				if (selectedRowIds.length != 0) {
					Relation currentRelation = getIdentityRelation();
					ArrayUtils.reverse(selectedRowIds);
					for (int rowIndex : selectedRowIds) {
						RelationsPanel.this.netGUI.getNet().removeRelationActor(currentRelation,
								((ActorsModel) RelationsPanel.this.tableRelationActors.getModel()).getTarget().get(rowIndex));
					}

					//
					RelationsPanel.this.netGUI.setChanged(true);
					RelationsPanel.this.netGUI.updateAll();
				}
			}
		});

		JMenuItem mntmBrowse = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("RelationsPanel.mntmBrowse.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmBrowse.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Browse individual from relation.
				try {
					//
					int selectedIndex = RelationsPanel.this.tableRelationActors.getSelectedRow();
					if (selectedIndex != -1) {
						// Redirect to an individual.
						Individual selected = ((ActorsModel) RelationsPanel.this.tableRelationActors.getModel()).getSource().get(selectedIndex).getIndividual();
						if (selected != null) {
							//
							RelationsPanel.this.netGUI.changeSegmentationToCluster(selected);
						}
					}
				} catch (final Exception exception) {
					//
					exception.printStackTrace();
				}
			}
		});
		popupMenu.add(mntmBrowse);

		JSeparator separator = new JSeparator();
		popupMenu.add(separator);
		popupMenu.add(mntmDelete);

		//
		scrollPane_1.setViewportView(this.tableRelationActors);
		this.tableRelationActors.setBorder(new LineBorder(new Color(169, 169, 169)));
		this.tableRelationActors.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.tableRelationActors.setModel(new ActorsModel(null, this.netGUI, null));
		this.tableRelationActors.getTableHeader().setReorderingAllowed(false);

		//
		this.cmbbxActorRoles = new AutoComboBox(null);
		this.cmbbxActorRoles.getEditor().getEditorComponent().addFocusListener(new FocusListener() {
			@Override
			public void focusGained(final FocusEvent arg0) {
			}

			@Override
			public void focusLost(final FocusEvent arg0) {
				logger.debug("FOCUS LOST RELATION TABLE MODEL" + RelationsPanel.this.tableRelationActors.isEditing());
				if (RelationsPanel.this.tableRelationActors.isEditing()) {
					RelationsPanel.this.tableRelationActors.getColumnModel().getColumn(0).getCellEditor().cancelCellEditing();
				}
			}
		});
		this.cmbbxActorRoles.setEditable(true);
		this.cmbbxActorRoles.setStrict(true);
		this.cmbbxActorRoles.setMaximumRowCount(10);
		TableCellEditor editor = new DefaultCellEditor(this.cmbbxActorRoles);
		this.tableRelationActors.getColumnModel().getColumn(0).setCellEditor(editor);
		this.tableRelationActors.getColumnModel().getColumn(0).setMinWidth(10);
		this.tableRelationActors.getColumnModel().getColumn(0).setPreferredWidth(90);
		this.tableRelationActors.getColumnModel().getColumn(0).setMaxWidth(150);

		//
		this.cmbbxActorIds = new AutoComboBox(null);
		this.cmbbxActorIds.getEditor().getEditorComponent().addFocusListener(new FocusListener() {
			@Override
			public void focusGained(final FocusEvent arg0) {
			}

			@Override
			public void focusLost(final FocusEvent arg0) {
				logger.debug("FOCUS LOST RELATION TABLE INDIVIDUAL" + RelationsPanel.this.tableRelationActors.isEditing());
				if (RelationsPanel.this.tableRelationActors.isEditing()) {
					RelationsPanel.this.tableRelationActors.getColumnModel().getColumn(1).getCellEditor().cancelCellEditing();
				}
			}
		});
		this.cmbbxActorIds.setEditable(true);
		this.cmbbxActorIds.setStrict(false);
		this.cmbbxActorIds.setMaximumRowCount(10);
		TableCellEditor editor2 = new DefaultCellEditor(this.cmbbxActorIds);
		this.tableRelationActors.getColumnModel().getColumn(1).setCellEditor(editor2);
		this.tableRelationActors.getColumnModel().getColumn(1).setMinWidth(10);
		this.tableRelationActors.getColumnModel().getColumn(1).setPreferredWidth(90);
		this.tableRelationActors.getColumnModel().getColumn(1).setMaxWidth(110);
		this.tableRelationActors.getColumnModel().getColumn(1).setResizable(false);

		List<Integer> relationOrderList = new ArrayList<Integer>(99);
		for (int order = 1; order < 100; order++) {
			relationOrderList.add(order);
		}
		this.cmbbxRelationOrders = new AutoComboBox(relationOrderList);
		this.cmbbxRelationOrders.getEditor().getEditorComponent().addFocusListener(new FocusListener() {
			@Override
			public void focusGained(final FocusEvent arg0) {
			}

			@Override
			public void focusLost(final FocusEvent arg0) {
				logger.debug("FOCUS LOST ACTORS TABLE(2) " + RelationsPanel.this.tableRelationActors.isEditing());
				if (RelationsPanel.this.tableRelationActors.isEditing()) {
					RelationsPanel.this.tableRelationActors.getColumnModel().getColumn(3).getCellEditor().cancelCellEditing();
				}

				// if (((ActorsModel)
				// RelationsPanel.this.tableRelationActors.getModel()).isNewEditionOn())
				// {
				// ((ActorsModel)
				// RelationsPanel.this.tableRelationActors.getModel()).escapeNewEdition();
				// }
			}
		});
		this.cmbbxRelationOrders.setEditable(true);
		this.cmbbxRelationOrders.setStrict(false);
		this.cmbbxRelationOrders.setMaximumRowCount(10);
		TableCellEditor relationOrderEditor = new DefaultCellEditor(this.cmbbxRelationOrders);
		this.tableRelationActors.getColumnModel().getColumn(3).setCellEditor(relationOrderEditor);
		this.tableRelationActors.getColumnModel().getColumn(3).setMinWidth(10);
		this.tableRelationActors.getColumnModel().getColumn(3).setPreferredWidth(50);
		this.tableRelationActors.getColumnModel().getColumn(3).setMaxWidth(50);

		Component verticalStrut_2 = Box.createVerticalStrut(10);
		actorsPanel.add(verticalStrut_2);

		JSplitPane splitPane = new JSplitPane();
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane_1.setRightComponent(splitPane);

		this.actorAttributesPanel = new AttributesPanel(this.netGUI, (Attributes) null, this.actorAttributeTemplates, "Selected actor");
		this.actorAttributesPanel.setMinimumSize(new Dimension(283, 150));
		splitPane.setLeftComponent(this.actorAttributesPanel);

		this.relationAttributesPanel = new AttributesPanel(this.netGUI, (Attributes) null, this.relationAttributeTemplates, "Selected relation");
		splitPane.setRightComponent(this.relationAttributesPanel);

		JPanel buttonsPanel = new JPanel();
		add(buttonsPanel);
		buttonsPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));

		JButton btnRemoveRelation = new JButton("");
		btnRemoveRelation.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Remove relation.
				Relation target = getSelectedRelation();
				if (target != null) {
					//
					int response = JOptionPane.showConfirmDialog(RelationsPanel.this.thisJPanel,
							"Do you really want to delete relation nr. " + target.getTypedId() + "?", "Relation delete confirm", JOptionPane.YES_NO_OPTION);

					if (response == JOptionPane.YES_OPTION) {
						//
						int selectedIndex = RelationsPanel.this.relationList.getSelectedIndex();

						// Remove.
						RelationsPanel.this.netGUI.getNet().remove(target);

						// Refresh.
						Relations tabRelations = RelationsPanel.this.netGUI.getNet().relations().getByModel(relationModel);
						((RelationsModel) RelationsPanel.this.relationList.getModel()).setSource(tabRelations);
						if (tabRelations.size() != 0) {
							if (selectedIndex == tabRelations.size()) {
								selectedIndex -= 1;
							}
							RelationsPanel.this.relationList.setSelectedIndex(selectedIndex);
						}

						// Refresh.
						RelationsPanel.this.netGUI.updateAll();

						//
						RelationsPanel.this.netGUI.setChanged(true);
					}
				}
			}
		});
		btnRemoveRelation.setToolTipText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnNewButton.toolTipText"));
		btnRemoveRelation.setIcon(new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/remove.png")));
		buttonsPanel.add(btnRemoveRelation);

		JButton btnAddRelation = new JButton("");
		btnAddRelation.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Add a relation.
				addRelation();
			}
		});
		btnAddRelation.setToolTipText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnNewButton_1.toolTipText"));
		btnAddRelation.setIcon(new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/add.png")));
		buttonsPanel.add(btnAddRelation);

		Component horizontalGlue_3 = Box.createHorizontalGlue();
		buttonsPanel.add(horizontalGlue_3);

		JButton btnPreviousRelation = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnPrevious.text"));
		btnPreviousRelation.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Previous individual.
				int selectedIndex = RelationsPanel.this.relationList.getSelectedIndex();
				if (selectedIndex == -1) {
					int size = RelationsPanel.this.relationList.getModel().getSize();
					if (size != 0) {
						selectedIndex = size - 1;
						RelationsPanel.this.relationList.setSelectedIndex(selectedIndex);
						RelationsPanel.this.relationList.ensureIndexIsVisible(selectedIndex);
					}
				} else if (selectedIndex > 0) {
					selectedIndex -= 1;
					RelationsPanel.this.relationList.setSelectedIndex(selectedIndex);
					RelationsPanel.this.relationList.ensureIndexIsVisible(selectedIndex);
				}
			}
		});
		buttonsPanel.add(btnPreviousRelation);

		JButton btnNextRelation = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnNext.text"));
		btnNextRelation.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Next individual.
				int selectedIndex = RelationsPanel.this.relationList.getSelectedIndex();
				int size = RelationsPanel.this.relationList.getModel().getSize();
				if (selectedIndex == -1) {
					if (size != 0) {
						selectedIndex = 0;
						RelationsPanel.this.relationList.setSelectedIndex(selectedIndex);
						RelationsPanel.this.relationList.ensureIndexIsVisible(selectedIndex);
					}
				} else if (selectedIndex < size - 1) {
					selectedIndex += 1;
					RelationsPanel.this.relationList.setSelectedIndex(selectedIndex);
					RelationsPanel.this.relationList.ensureIndexIsVisible(selectedIndex);
				}
			}
		});
		buttonsPanel.add(btnNextRelation);

		Component horizontalGlue_1 = Box.createHorizontalGlue();
		buttonsPanel.add(horizontalGlue_1);

		JButton btnRta = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("RelationsPanel.btnRta.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnRta.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// RTA : Reset Templated Attributes.
				updateAttributeTemplates();
			}
		});
		btnRta.setToolTipText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("RelationsPanel.btnRta.toolTipText")); //$NON-NLS-1$ //$NON-NLS-2$
		buttonsPanel.add(btnRta);

		Component horizontalGlue_2 = Box.createHorizontalGlue();
		buttonsPanel.add(horizontalGlue_2);

		JLabel lblRelationsSearch = new JLabel(" ");
		lblRelationsSearch.setToolTipText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.lblSearch.text")); //$NON-NLS-1$ //$NON-NLS-2$
		lblRelationsSearch.setIcon(new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/find.png")));
		buttonsPanel.add(lblRelationsSearch);
		lblRelationsSearch.setMinimumSize(new Dimension(300, 15));

		this.txtfldSearchRelation = new JTextField();
		this.txtfldSearchRelation.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Search relation.
				String pattern = RelationsPanel.this.txtfldSearchRelation.getText();
				logger.debug("Search relation=[" + pattern + "]");
				if (StringUtils.isNotBlank(pattern)) {
					int index = ((RelationsModel) RelationsPanel.this.relationList.getModel()).nextSearchedIndex(pattern);
					if (index != -1) {
						RelationsPanel.this.relationList.setSelectedIndex(index);
						RelationsPanel.this.relationList.ensureIndexIsVisible(index);
					}
				}
			}
		});
		buttonsPanel.add(this.txtfldSearchRelation);
		this.txtfldSearchRelation.setMaximumSize(new Dimension(500, 50));
		this.txtfldSearchRelation.setText("");
		this.txtfldSearchRelation.setColumns(15);

		//
		if (this.relationList.getModel().getSize() != 0) {
			this.relationList.setSelectedIndex(0);
		}
	}

	public void addRelation() {
		try {
			//
			Relation relation = this.netGUI.getNet().createRelation("", this.relationModel);
			((RelationsModel) this.relationList.getModel()).setSource(this.netGUI.getCurrentRelations().getByModel(this.relationModel));

			select(relation);
			// netGUI.updateAll();
			// netGUI.selectRelationsTab(individual);

			//
			setRelationNameEditorOn();
		} catch (final Exception exception) {
			// Show trace.
			exception.printStackTrace();

			//
			String title = "Error computerum est";
			String message = "Error occured during working: " + exception.getMessage();

			//
			JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * 
	 * @return
	 */
	public Relation getIdentityRelation() {
		Relation result;

		if (NumberUtils.isDigits(this.lblRelationId.getText())) {
			result = this.netGUI.getNet().relations().getById(Integer.parseInt(this.lblRelationId.getText()));
		} else {
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public RelationModel getRelationModel() {
		RelationModel result;

		result = this.relationModel;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Relation getSelectedRelation() {
		Relation result;

		if (this.relationList.getSelectedIndex() == -1) {
			result = null;
		} else {
			result = (Relation) ((RelationsModel) this.relationList.getModel()).getElementAt(this.relationList.getSelectedIndex());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 */
	public void select(final Relation relation) {
		int relationIndex = ((RelationsModel) this.relationList.getModel()).indexOf(relation);

		if ((relationIndex >= 0) && (relationIndex < ((RelationsModel) this.relationList.getModel()).getSize())) {
			this.relationList.setSelectedIndex(relationIndex);
			this.relationList.ensureIndexIsVisible(relationIndex);
		} else if (((RelationsModel) this.relationList.getModel()).getSize() != 0) {
			this.relationList.setSelectedIndex(0);
			this.relationList.ensureIndexIsVisible(0);
		} else {
			updateRelationIdentity(null);
		}
	}

	private void setRelationNameEditorOn() {
		this.lblRelationName.setVisible(false);
		this.txtfldRelationName.setVisible(true);
		this.txtfldRelationName.setText(this.lblRelationName.getText());
		this.txtfldRelationName.requestFocus();
	}

	/**
	 * 
	 */
	public void update() {
		int currentIndex = this.relationList.getSelectedIndex();
		((RelationsModel) this.relationList.getModel()).setSource(this.netGUI.getCurrentRelations().getByModel(this.relationModel));
		if (currentIndex < this.relationList.getModel().getSize()) {
			this.relationList.setSelectedIndex(currentIndex);
			updateRelationIdentity();
		} else {
			this.relationList.setSelectedIndex(0);
			updateRelationIdentity();
		}
	}

	/**
	 * 
	 */
	public void updateAttributeTemplates() {
		//
		this.relationAttributeTemplates.clear();
		if (this.netGUI.getNet() != null) {
			//
			this.relationAttributeTemplates.addAll(AttributeWorker.getExogenousAttributeDescriptors(
					this.netGUI.getNet().relations().getByModel(this.relationModel), null).labels());
		}

		//
		this.actorAttributeTemplates.clear();
		if (this.netGUI.getNet() != null) {
			//
			this.actorAttributeTemplates.addAll(AttributeWorker.getExogenousAttributeDescriptors(this.netGUI.getNet().relations().getActors()).labels());
		}
	}

	/**
	 * 
	 */
	public void updateRelationIdentity() {
		updateRelationIdentity(getSelectedRelation());
	}

	/**
	 * 
	 */
	public void updateRelationIdentity(final Relation source) {

		if (source == null) {
			//
			this.lblRelationModel.setText("----");

			//
			this.lblRelationId.setText("---");
			this.lblRelationName.setVisible(true);
			this.lblRelationName.setText("----");
			this.txtfldRelationName.setVisible(false);
			this.lblPosition.setText("(--/--)");

			//
			((ActorsModel) this.tableRelationActors.getModel()).setSource(null, null, null);
			this.lblRelationActors.setText("Actors");

			//
			this.cmbbxActorIds.setDataList(new ArrayList<String>(0));

			//
			this.cmbbxActorRoles.setDataList(new ArrayList<String>(0));

			//
			this.actorAttributesPanel.setSource(null);

			//
			this.relationAttributesPanel.setSource(null);

		} else {
			//
			this.lblRelationModel.setText(source.getModel().getName());

			//
			this.lblRelationId.setText(String.valueOf(source.getId()));
			this.lblRelationName.setVisible(true);
			this.lblRelationName.setText(source.getName());
			this.txtfldRelationName.setVisible(false);
			this.lblPosition.setText("(" + (this.relationList.getSelectedIndex() + 1) + "/" + this.relationList.getModel().getSize() + ")");

			//
			((ActorsModel) this.tableRelationActors.getModel()).setSource(source.actors(), this.netGUI, source);
			this.lblRelationActors.setText("Actors (" + source.actors().size() + ")");

			//
			this.cmbbxActorIds.setDataList(ComboBoxIds.instance().items());

			//
			this.actorAttributesPanel.setSource(null);

			//
			this.relationAttributesPanel.setSource(source.attributes());

			//
			this.cmbbxActorRoles.setDataList(this.relationModel.roles().toSortedNameList());
		}
	}

	private static void addPopup(final Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			@Override
			public void mouseReleased(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(final MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
