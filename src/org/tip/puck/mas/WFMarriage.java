package org.tip.puck.mas;

public class WFMarriage implements WeightFactor {
    private double wgtCurrentMarraige;
    private double wgtPreviousMarriage;
    
    public WFMarriage(double wgtCurrentMarraige, double wgtPreviousMarriage) {
        this.wgtCurrentMarraige = wgtCurrentMarraige;
        this.wgtPreviousMarriage = wgtPreviousMarriage;
    }
    
    public String factorName(){
    	return "marriage";
    }
    
    @Override
    public double factor(Agent mother, Agent father) {
        if (father.isPartnerOf(mother)) {
            return wgtCurrentMarraige;
        }
        else if (father.wasPartnerOf(mother)) {
        	return wgtPreviousMarriage;
        }
        else {
            return 1.0;
        }
    }
    
    @Override
    public boolean appliesToMarriage() {return false;}
    
    @Override
    public boolean appliesToDivorce() {return false;}
}