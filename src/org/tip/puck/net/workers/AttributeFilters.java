package org.tip.puck.net.workers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class AttributeFilters implements Iterable<AttributeFilter> {

	private ArrayList<AttributeFilter> attributeFilters;

	/**
	 * 
	 */
	public AttributeFilters() {

		this.attributeFilters = new ArrayList<AttributeFilter>();
	}

	/**
	 * 
	 */
	public AttributeFilters(final int initialCapacity) {

		this.attributeFilters = new ArrayList<AttributeFilter>(initialCapacity);
	}

	/**
	 * 
	 * @param log
	 */
	public void add(final AttributeFilter source) {
		//
		if (source == null) {
			//
			throw new IllegalArgumentException("source is null.");

		} else {
			//
			this.attributeFilters.add(source);
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public AttributeFilters addAll(final AttributeFilters source) {
		AttributeFilters result;

		//
		if (source != null) {
			//
			for (AttributeFilter filter : source) {
				//
				add(filter);
			}
		}

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 */
	public void clear() {

		this.attributeFilters.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public AttributeFilters copy() {
		AttributeFilters result;

		//
		result = new AttributeFilters(this.attributeFilters.size());

		//
		for (AttributeFilter comment : this.attributeFilters) {
			result.add(comment);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public AttributeFilters findBy(final AttributeFilter.Mode mode) {
		AttributeFilters result;

		//
		result = new AttributeFilters();

		//
		for (AttributeFilter filter : this) {
			//
			if (filter.getMode() == mode) {
				//
				result.add(filter);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public AttributeFilters findBy(final AttributeFilter.Scope scope) {
		AttributeFilters result;

		//
		result = new AttributeFilters();

		//
		for (AttributeFilter filter : this) {
			//
			if (filter.getScope() == scope) {
				//
				result.add(filter);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public AttributeFilters findBy(final AttributeFilter.Scope scope, final AttributeFilter.Mode mode) {
		AttributeFilters result;

		//
		result = new AttributeFilters();

		//
		for (AttributeFilter filter : this) {
			//
			if ((filter.getScope() == scope) & (filter.getMode() == mode)) {
				//
				result.add(filter);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public AttributeFilters findBy(final AttributeFilter.Scope scope, final String label) {
		AttributeFilters result;

		//
		result = new AttributeFilters();

		//
		for (AttributeFilter filter : this) {
			//
			if ((filter.getScope() == scope) && (StringUtils.equals(filter.getLabel(), label))) {
				//
				result.add(filter);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public AttributeFilters findBy(final String relationName, final String label) {
		AttributeFilters result;

		//
		result = new AttributeFilters();

		//
		for (AttributeFilter filter : this) {
			//
			if ((filter.getScope() == AttributeFilter.Scope.RELATION) && (StringUtils.equals(filter.getOptionalRelationName(), relationName))
					&& (StringUtils.equals(filter.getLabel(), label))) {
				//
				result.add(filter);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public AttributeFilters first(final int targetCount) {
		AttributeFilters result;

		//
		result = new AttributeFilters(targetCount);

		//
		boolean ended = false;
		Iterator<AttributeFilter> iterator = iterator();
		int count = 0;
		while (!ended) {
			if ((count > targetCount) || (!iterator.hasNext())) {
				ended = true;
			} else {
				result.add(iterator.next());
				count += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public AttributeFilter getByIndex(final int index) {
		AttributeFilter result;

		result = this.attributeFilters.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		boolean result;

		result = this.attributeFilters.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty() {
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<AttributeFilter> iterator() {
		Iterator<AttributeFilter> result;

		result = this.attributeFilters.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public AttributeFilter last() {
		AttributeFilter result;

		if (this.attributeFilters.isEmpty()) {
			result = null;
		} else {
			result = this.attributeFilters.get(this.attributeFilters.size() - 1);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final AttributeFilter message) {
		this.attributeFilters.remove(message);
	}

	/**
	 * 
	 * @return
	 */
	public AttributeFilters reverse() {
		AttributeFilters result;

		//
		Collections.reverse(this.attributeFilters);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size() {
		int result;

		result = this.attributeFilters.size();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		//
		StringList buffer = new StringList();

		//
		for (AttributeFilter attributeFilter : this) {
			//
			String infos = attributeFilter.toString();

			if (infos != null) {
				//
				buffer.append(infos);
			}
		}

		result = buffer.toStringWithCommas();

		//
		return result;
	}
}
