/**
 * 
 */
package org.tip.puck.graphs.onemode;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.util.Numberable;
import org.tip.puck.util.RandomGenerator;

/**
 * @author Telmo Menezes
 * @author Klaus Hamberger
 * 
 */
public class OMGraph {

	public enum GraphMode {
		OREGRAPH,
		PGRAPH;
	}
	Map<String, OMNode> nodes;
	private Vector<OMLink> linksVector;
//	private int totalLinks;
	private SiblingMarriageDistribution siblingMarriageDistribution;
	private GraphMode mode;


	public OMGraph (GraphMode mode){

//		totalLinks = 0;
		nodes = new HashMap<String, OMNode>();
		linksVector = new Vector<OMLink>();
		this.mode = mode;
		
	}
	
	public OMGraph clone() {
		OMGraph result;
		
		result = new OMGraph(mode);
		
	    result.siblingMarriageDistribution = siblingMarriageDistribution;
		
		// copy nodes
		for (OMNode node : getNodes()) {
			result.putNode(node.cloneWithLinks());
		}
		
		// create links vector
		result.putLinks();
		
		// compute out degrees and ancestors
		result.computeOutDegrees();
		result.computeAncestors();
		
		//
		return result;
	}
	
/*	public OMGraph(final OMGraph pg) {
	    siblingMarriageDistribution = pg.siblingMarriageDistribution;
//		totalLinks = pg.totalLinks;

		// copy nodes
		nodes = new HashMap<String, OMNode>();
		for (Map.Entry<String, OMNode> entry : pg.nodes.entrySet()) {
			String key = entry.getKey();
			OMNode node = new OMNode(entry.getValue());
			nodes.put(key, node);
		}

		// create links vector
		linksVector = new Vector<OMLink>();
		for (Map.Entry<String, OMNode> entry : nodes.entrySet()) {
			OMNode marriage = entry.getValue();
			OMNode wifeLink = marriage.getWifeLink();
			OMNode husbandLink = marriage.getHusbandLink();
			if (wifeLink != null) {
				OMLink link = new OMLink(wifeLink, marriage, true);
				linksVector.add(link);
			}
			if (husbandLink != null) {
				OMLink link = new OMLink(husbandLink, marriage, false);
				linksVector.add(link);
			}
		}

		// compute out degrees and ancestors
		computeOutDegrees();
		computeAncestors();
	}*/
	
	
	public float averageNumberOfDescendants() {
		float avg = 0;

		for (Map.Entry<String, OMNode> entry : nodes.entrySet()) {
			OMNode marriage = entry.getValue();
			avg += marriage.numberOfDescendants();
		}

		avg /= numberOfNodes();

		return avg;
	}

	
	// Move to Shuffler
	void clearAncestors() {
		for (Map.Entry<String, OMNode> entry : nodes.entrySet()) {
			OMNode node = entry.getValue();
			node.clearDescendants();
		}
	}
	
	// Move to shuffler
	void computeAncestors() {
		for (Map.Entry<String, OMNode> entry : nodes.entrySet()) {
			OMNode node = entry.getValue();
			node.addDescendant(node, 0);
		}
	}
	
	void computeOutDegrees() {
		for (Map.Entry<String, OMNode> entry : nodes.entrySet()) {
			OMNode marriage = entry.getValue();
			OMNode wifeLink = marriage.getFemaleLink();
			OMNode husbandLink = marriage.getMaleLink();
			if (wifeLink != null) {
				wifeLink.incOutDegree();
				wifeLink.incOutDegreeF();
			}
			if (husbandLink != null) {
				husbandLink.incOutDegree();
				husbandLink.incOutDegreeM();
			}
		}
	}


/*	public Map<String, OMNode> getMarriages() {
		return nodes;
	}*/
	
	public Collection<OMNode> getNodes() {
		return nodes.values();
	}
	
	public OMNode getNode(String key){
		return nodes.get(key);
	}

	public OMNode getNode (Numberable referent){
		return nodes.get(referent.hashKey());
	}

	public int getProbableNumberOfSpouses(Gender gender, int monogamousSameSexSiblings){
		int result;
		
		if (gender == Gender.UNKNOWN){
			result = 0;
		} else {
			result = siblingMarriageDistribution.howManySpouses(gender, monogamousSameSexSiblings);
		}
		//
		return result;
	}

/*	public int getTotalLinks() {
		return totalLinks;
	}*/

	public float maxNumberOfDescendants() {
		float max = 0;
		
		for (Map.Entry<String, OMNode> entry : nodes.entrySet()) {
			OMNode marriage = entry.getValue();
			float descs = marriage.numberOfDescendants();
			if (descs > max) {
				max = descs;
			}
		}

		return max;
	}

	public int numberOfNodes() {
		return nodes.size();
	}
	
	public void putNode(OMNode node){
		nodes.put(node.hashKey(), node);
	}
	
	public void putNode(Numberable referent){
		nodes.put(referent.hashKey(), new OMNode(referent));
	}
	
	
	public void putNode(Individual individual, GraphMode mode){
		if (mode == GraphMode.OREGRAPH){
			putNode(individual);
		} else if (mode == GraphMode.PGRAPH){
			if (individual.isMale()) {
				putNode(new Family(individual,null));
//				husband = individual;
			} else {
				putNode(new Family(null,individual));
//				wife = individual;
			}
		}
	}	
	
	public void putLink (OMLink link){
		linksVector.add(link);
	}
	
	public void putLinks(){

//		for (Map.Entry<String, OMNode> entry : pGraph.nodes.entrySet()) {
//		OMNode node = entry.getValue();

		for (OMNode node : getNodes()) {
			OMNode wifeLink = node.getFemaleLink();
			OMNode husbandLink = node.getMaleLink();
			if (wifeLink != null) {
				putLink(new OMLink(wifeLink, node, true));
			}
			if (husbandLink != null) {
				putLink(new OMLink(husbandLink, node, false));
			}
		}
	}

	public double percentageShuffledLinks() {
		double shuffled = 0;
		double total = 0;
		for (OMLink link : linksVector) {
			if (link.isShuffled()) {
				shuffled += 1.0;
			}
			total += 1.0;
		}
		
		return (shuffled / total) * 100.0;
	}


	public void setSiblingMarriageDistribution(Net net) {
		siblingMarriageDistribution = new SiblingMarriageDistribution(net);
	}
	
	public void storeOriginalDescendants() {
		for (Map.Entry<String, OMNode> entry : nodes.entrySet()) {
			OMNode marriage = entry.getValue();
			marriage.storeOriginalDescendants();
		}
	}

	public void storeOriginalRelatives() {
		for (Map.Entry<String, OMNode> entry : nodes.entrySet()) {
			OMNode marriage = entry.getValue();
			marriage.storeOriginalRelatives();
		}
	}
	
	
	
	public OMLink getRandomLink (){
		OMLink result;
		
		result = linksVector.get(RandomGenerator.instance().random.nextInt(linksVector.size()));

		//
		return result;
	}
	

	@Override
	public String toString() {
		String str = "Number of marriages: " + numberOfNodes() + "\n";
//		str += "Number of links: " + getTotalLinks() + "\n";
		str += "Average number of descendants: " + averageNumberOfDescendants() + "\n";
		str += "Max number of descendants: " + maxNumberOfDescendants() + "\n";
		return str;
	}
}
