package oldcore.trash;


public class SliceMaker {
	
/*	public static RelationSetSequence createRelationSetSequence(Segmentation segmentation, SequenceCriteria criteria) throws PuckException{
		RelationSetSequence result;
		
		result = new RelationSetSequence(criteria.getLocalUnitLabel(),0);
		
		Relations currentRelations = segmentation.getCurrentRelations().getByModelName(criteria.getRelationModelName());
//		Individuals allIndividuals = segmentation.getAllIndividuals();
		
		for (int time : criteria.getDates()){
			Relations relations = new Relations();
			relations.setId(time);
			result.put(new Ordinal(time), relations);
//			result.put(new Ordinal(time), new RelationSet(time));
		}

		// Set All Relations
		
		Relations allRelations = segmentation.getAllRelations().getByModelName(criteria.getRelationModelName());
		for (Relation relation : allRelations){
			
			Integer time = relation.getTime(criteria.getDateLabel());
			
			if (time!=null && result.getTimes().contains(new Ordinal(time))){

				RelationSet slice = result.getStation(new Ordinal(time)); 
				slice.allRelations().put(relation);
			}
			
		}

		for (Relation relation : currentRelations.getByAttribute(criteria.getLocalUnitLabel(), null)){
			
			Integer time = relation.getTime(criteria.getDateLabel());
						
			if (time!=null && result.getTimes().contains(new Ordinal(time))){

				Relations slice = result.getStation(new Ordinal(time)); 
				slice.put(relation);
								
				// set relation population
				
				for (Actor actor : relation.actors()){
					
					Individual member = actor.getIndividual();
//					slice.relationsByIndividuals().put(member,relation);
					
					String idValue = relation.getAttributeValue(result.idLabel());
					if (idValue!=null && result.getTimes().contains(time)){
						
						Individuals members = result.membersByRelationId().get(idValue);
						if (members == null){
							members = new Individuals();
							result.membersByRelationId().put(idValue, members);
						}
						members.put(member);

					}
				}
				
				// put relation into relation sequence
				
				String idValue = relation.getAttributeValue(result.idLabel());
				if (StringUtils.isNotEmpty(idValue) && StringUtils.isNumeric(idValue)){
					Integer id = Integer.parseInt(idValue);
					EgoRelationSequence groupSequence = result.toSequencesByIdValue().getById(id);
					if (groupSequence==null){
						groupSequence = new EgoRelationSequence(id);
						// Temporary method, since census methods are gender sensitive (separate index for genders necessary)
						groupSequence.setEgo(new Individual(id,id+"",Gender.UNKNOWN));
						result.toSequencesByIdValue().put(groupSequence);
					}
					groupSequence.put(new Ordinal(time), relation);

				} else {
					System.err.println("Invalid local unit parameter "+idValue);
//					throw PuckExceptions.INVALID_PARAMETER.create("Invalid local unit label.");
				}
				
				// put relation into individual sequence
				
				for (Actor actor: relation.actors()){
					Individual ego = actor.getIndividual();
					EgoRelationSequence indiSequence = result.toSequencesByEgo().getById(ego.getId());
					if (indiSequence==null){
						indiSequence = new EgoRelationSequence(ego.getId());
						indiSequence.setEgo(ego);
						result.toSequencesByEgo().put(indiSequence);
					}
					indiSequence.put(new Ordinal(time), relation);
				}

			}
		}
		
		
		//
		return result;
		
	}
	
	public static Relations createRelationSet (Individuals source, String relationModelName, String egoRoleName, String attributeLabel, String dateLabel, Integer date){
		Relations result;
		
		result = new Relations();
		
		for (Individual ego : source){
			result.add(ego.relations(relationModelName, egoRoleName, attributeLabel, dateLabel, date));
		}
		
		//
		return result;
	}*/


}
