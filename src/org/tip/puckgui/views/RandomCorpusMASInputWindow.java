package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Net;
import org.tip.puck.net.random.RandomNetMaker;
import org.tip.puck.net.workers.NetReporter;
import org.tip.puck.report.Report;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.PuckGUI;
import org.tip.puckgui.views.mas.AgnaticCousinsWeightFactor;
import org.tip.puckgui.views.mas.CousinsWeightFactor;
import org.tip.puckgui.views.mas.Divorce2WeightFactor;
import org.tip.puckgui.views.mas.DivorceWeightFactor;
import org.tip.puckgui.views.mas.NormalAgeDifferenceWeightFactor;
import org.tip.puckgui.views.mas.NormalAgeWeightFactor;
import org.tip.puckgui.views.mas.PregnancyWeightFactor;
import org.tip.puckgui.views.mas.UterineCousinsWeightFactor;
import org.tip.puckgui.views.mas.WeightFactor;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class RandomCorpusMASInputWindow extends JFrame {

	private static final long serialVersionUID = -1690866337078944374L;
	private static final Logger logger = LoggerFactory.getLogger(RandomCorpusMASInputWindow.class);

	private JFrame thisJFrame;
	private JPanel contentPane;
	private JSpinner spnrInitialPopulation;
	private JSpinner spnrFertilityRate;
	private JSpinner spnrMaxAge;
	private JSpinner spnrYear;
	private JSpinner spnrDivorceProbability;
	private JCheckBox chckbxDivorce;
	private JCheckBox chckbxDivorce2;
	private JCheckBox chckbxNormalAgeFemale;
	private JCheckBox chckbxNormalAgeMale;
	private JCheckBox chckbxNormalAgeUnknown;
	private JCheckBox chckbxNormalAgeDifference;
	private JCheckBox chckbxAgnaticCousins;
	private JCheckBox chckbxPregnancy;
	private JPanel panelDivorce;
	private JPanel panelDivorce2;
	private JPanel panelNormalAgeDifference;
	private JPanel panelCousins;
	private JPanel panelPregnancy;
	private JPanel panelNormalAgeMale;
	private JPanel panelNormalAgeUnknown;
	private JPanel panelNormalAgeFemale;
	private JSpinner spnrNormalAgeDifferenceStdev;
	private JSpinner spnrNormalAgeDifferenceMean;
	private JSpinner spnrNormalAgeMaleMean;
	private JSpinner spnrNormalAgeMaleStdev;
	private JSpinner spnrNormalAgeFemaleMean;
	private JSpinner spnrNormalAgeFemaleStdev;
	private JSpinner spnrFemaleDivorceWeight;
	private JSpinner spnrMaleDivorceWeight;
	private JSpinner spnrBothDivorceWeight;
	private JSpinner spnrAgnaticCousinsCharlie;
	private JSpinner spnrPregnancyAlpha;
	private JSpinner spnrNormalAgeUnknownMean;
	private JSpinner spnrNormalAgeUnknownStdev;
	private JSpinner spnrAgnaticCousinsAlpha;
	private JSpinner spnrAgnaticCousinsBravo;
	private JLabel lblChildren;
	private JCheckBox chckbxChildren;
	private JPanel panelChildren;
	private JLabel label_12;
	private JSpinner spinner;

	/**
	 * Similar with VirtualFielworkInputWindow.
	 */
	public RandomCorpusMASInputWindow(final NetGUI gui) {

		// /////////////////////////////////
		setIconImage(Toolkit.getDefaultToolkit().getImage(RandomCorpusMASInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		setTitle("Random Corpus");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 676, 657);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});

		JButton btnRestoreDefaults = new JButton("Restore defaults");
		btnRestoreDefaults.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Restore Defaults.
				setDefaultCriteria();
			}
		});
		buttonPanel.add(btnRestoreDefaults);
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		getRootPane().setDefaultButton(btnLaunch);
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					//
					RandomCorpusCriteria criteria = getCriteria();

					//
					PuckGUI.instance().getPreferences().setRandomCorpusCriteria(criteria);
					criteria.setMas(true);
					//

					RandomNetMaker randomNetMaker = new RandomNetMaker(criteria);
					Net targetNet = randomNetMaker.createRandomMASNet();

					// Build report.
					Report report = NetReporter.reportRandomCorpusMAS(criteria, randomNetMaker.getMas(), targetNet);

					//
					NetGUI newGUI = PuckGUI.instance().createNetGUI(new File(targetNet.getLabel()), targetNet);
					newGUI.addReportTab(report);

					//
					dispose();

				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(RandomCorpusMASInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Simulation parameters", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.contentPane.add(panel, BorderLayout.NORTH);
		panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("100dlu"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));

		JLabel lblYear = new JLabel("Year:");
		lblYear.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblYear, "2, 2, right, default");

		this.spnrYear = new JSpinner();
		this.spnrYear.setModel(new SpinnerNumberModel(new Integer(300), new Integer(1), null, new Integer(1)));
		panel.add(this.spnrYear, "4, 2");

		JLabel lblInitialPopulation = new JLabel("Initial population:");
		panel.add(lblInitialPopulation, "2, 4, right, default");

		this.spnrInitialPopulation = new JSpinner();
		this.spnrInitialPopulation.setModel(new SpinnerNumberModel(new Integer(100), new Integer(1), null, new Integer(1)));
		panel.add(this.spnrInitialPopulation, "4, 4");

		this.spnrFertilityRate = new JSpinner();
		this.spnrFertilityRate.setModel(new SpinnerNumberModel(new Double(2.), new Double(0.0), new Double(100.0), new Double(1.)));
		((JSpinner.NumberEditor) this.spnrFertilityRate.getEditor()).getFormat().setMinimumFractionDigits(5);

		JLabel lblFertilityRate = new JLabel("Fertility rate:");
		panel.add(lblFertilityRate, "2, 6, right, default");
		panel.add(this.spnrFertilityRate, "4, 6");

		JLabel lblMaxAge = new JLabel("Max. age:");
		panel.add(lblMaxAge, "2, 8, right, default");

		this.spnrMaxAge = new JSpinner();
		this.spnrMaxAge.setModel(new SpinnerNumberModel(new Integer(70), new Integer(1), null, new Integer(1)));
		panel.add(this.spnrMaxAge, "4, 8");

		JPanel panel_3 = new JPanel();
		this.contentPane.add(panel_3, BorderLayout.CENTER);
		panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.Y_AXIS));

		JPanel panel_1 = new JPanel();
		panel_3.add(panel_1);

		JPanel panelWeightFactors = new JPanel();
		panel_3.add(panelWeightFactors);
		panelWeightFactors.setBorder(new TitledBorder(null, "Weight factors", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelWeightFactors.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), },
				new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, }));

		JLabel lblDivorce = new JLabel("Divorce");
		panelWeightFactors.add(lblDivorce, "2, 2");

		this.chckbxDivorce = new JCheckBox("");
		this.chckbxDivorce.setSelected(true);
		this.chckbxDivorce.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				//
				RandomCorpusMASInputWindow.this.panelDivorce.setVisible(RandomCorpusMASInputWindow.this.chckbxDivorce.isSelected());
			}
		});
		panelWeightFactors.add(this.chckbxDivorce, "4, 2");

		this.panelDivorce = new JPanel();
		this.panelDivorce.setEnabled(false);
		panelWeightFactors.add(this.panelDivorce, "6, 2, fill, fill");
		this.panelDivorce.setLayout(new FormLayout(
				new ColumnSpec[] { FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("50dlu"), },
				new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, }));

		JLabel label = new JLabel("Probability:");
		this.panelDivorce.add(label, "1, 1");

		this.spnrDivorceProbability = new JSpinner();
		this.spnrDivorceProbability.setModel(new SpinnerNumberModel(new Double(0.01), new Double(0.0), new Double(1.0), new Double(0.01)));
		((JSpinner.NumberEditor) this.spnrDivorceProbability.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelDivorce.add(this.spnrDivorceProbability, "3, 1");

		JLabel lblDivorce_1 = new JLabel("Divorce2");
		panelWeightFactors.add(lblDivorce_1, "2, 4");

		this.chckbxDivorce2 = new JCheckBox("");
		this.chckbxDivorce2.setSelected(true);
		this.chckbxDivorce2.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				//
				RandomCorpusMASInputWindow.this.panelDivorce2.setVisible(RandomCorpusMASInputWindow.this.chckbxDivorce2.isSelected());
			}
		});
		panelWeightFactors.add(this.chckbxDivorce2, "4, 4");

		this.panelDivorce2 = new JPanel();
		panelWeightFactors.add(this.panelDivorce2, "6, 4, fill, fill");
		this.panelDivorce2.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu"), }, new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, }));

		JLabel label_1 = new JLabel("Female:");
		this.panelDivorce2.add(label_1, "1, 1");

		this.spnrFemaleDivorceWeight = new JSpinner();
		this.spnrFemaleDivorceWeight.setModel(new SpinnerNumberModel(new Double(0.01), new Double(0.0), new Double(1.0), new Double(0.01)));
		((JSpinner.NumberEditor) this.spnrFemaleDivorceWeight.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelDivorce2.add(this.spnrFemaleDivorceWeight, "3, 1");

		JLabel label_2 = new JLabel("Male:");
		this.panelDivorce2.add(label_2, "5, 1");

		this.spnrMaleDivorceWeight = new JSpinner();
		this.spnrMaleDivorceWeight.setModel(new SpinnerNumberModel(new Double(0.01), new Double(0.0), new Double(1.0), new Double(0.01)));
		((JSpinner.NumberEditor) this.spnrMaleDivorceWeight.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelDivorce2.add(this.spnrMaleDivorceWeight, "7, 1");

		JLabel label_3 = new JLabel("Both:");
		this.panelDivorce2.add(label_3, "9, 1");

		this.spnrBothDivorceWeight = new JSpinner();
		this.spnrBothDivorceWeight.setModel(new SpinnerNumberModel(new Double(0.01), new Double(0.0), new Double(1.0), new Double(0.01)));
		((JSpinner.NumberEditor) this.spnrBothDivorceWeight.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelDivorce2.add(this.spnrBothDivorceWeight, "11, 1");

		JLabel lblNormalAgeFemale = new JLabel("Normal age female");
		panelWeightFactors.add(lblNormalAgeFemale, "2, 6");

		this.chckbxNormalAgeFemale = new JCheckBox("");
		this.chckbxNormalAgeFemale.setSelected(true);
		this.chckbxNormalAgeFemale.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				//
				RandomCorpusMASInputWindow.this.panelNormalAgeFemale.setVisible(RandomCorpusMASInputWindow.this.chckbxNormalAgeFemale.isSelected());
			}
		});
		panelWeightFactors.add(this.chckbxNormalAgeFemale, "4, 6");

		this.panelNormalAgeFemale = new JPanel();
		panelWeightFactors.add(this.panelNormalAgeFemale, "6, 6, fill, fill");
		this.panelNormalAgeFemale.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu"), }, new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, }));

		JLabel label_4 = new JLabel("Mean:");
		this.panelNormalAgeFemale.add(label_4, "1, 1");

		this.spnrNormalAgeFemaleMean = new JSpinner();
		this.spnrNormalAgeFemaleMean.setModel(new SpinnerNumberModel(new Double(25), new Double(0.0), null, new Double(1)));
		((JSpinner.NumberEditor) this.spnrNormalAgeFemaleMean.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelNormalAgeFemale.add(this.spnrNormalAgeFemaleMean, "3, 1");

		JLabel label_5 = new JLabel("Stdev:");
		this.panelNormalAgeFemale.add(label_5, "5, 1");

		this.spnrNormalAgeFemaleStdev = new JSpinner();
		this.spnrNormalAgeFemaleStdev.setModel(new SpinnerNumberModel(new Double(5), new Double(0.0), null, new Double(1)));
		((JSpinner.NumberEditor) this.spnrNormalAgeFemaleStdev.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelNormalAgeFemale.add(this.spnrNormalAgeFemaleStdev, "7, 1");

		JLabel lblNormalAgeMale = new JLabel("Normal age male");
		panelWeightFactors.add(lblNormalAgeMale, "2, 8");

		this.chckbxNormalAgeMale = new JCheckBox("");
		this.chckbxNormalAgeMale.setSelected(true);
		this.chckbxNormalAgeMale.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				//
				RandomCorpusMASInputWindow.this.panelNormalAgeMale.setVisible(RandomCorpusMASInputWindow.this.chckbxNormalAgeMale.isSelected());
			}
		});
		panelWeightFactors.add(this.chckbxNormalAgeMale, "4, 8");

		this.panelNormalAgeMale = new JPanel();
		panelWeightFactors.add(this.panelNormalAgeMale, "6, 8, fill, fill");
		this.panelNormalAgeMale.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu"), }, new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, }));

		JLabel label_6 = new JLabel("Mean:");
		this.panelNormalAgeMale.add(label_6, "1, 1");

		this.spnrNormalAgeMaleMean = new JSpinner();
		this.spnrNormalAgeMaleMean.setModel(new SpinnerNumberModel(new Double(25), new Double(0.0), null, new Double(1)));
		((JSpinner.NumberEditor) this.spnrNormalAgeMaleMean.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelNormalAgeMale.add(this.spnrNormalAgeMaleMean, "3, 1");

		JLabel label_7 = new JLabel("Stdev:");
		this.panelNormalAgeMale.add(label_7, "5, 1");

		this.spnrNormalAgeMaleStdev = new JSpinner();
		this.spnrNormalAgeMaleStdev.setModel(new SpinnerNumberModel(new Double(5), new Double(0.0), null, new Double(1)));
		((JSpinner.NumberEditor) this.spnrNormalAgeMaleStdev.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelNormalAgeMale.add(this.spnrNormalAgeMaleStdev, "7, 1");

		JLabel lblNormalAgeUnknown = new JLabel("Normal age unknown");
		panelWeightFactors.add(lblNormalAgeUnknown, "2, 10");

		this.chckbxNormalAgeUnknown = new JCheckBox("");
		this.chckbxNormalAgeUnknown.setSelected(true);
		this.chckbxNormalAgeUnknown.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				//
				RandomCorpusMASInputWindow.this.panelNormalAgeUnknown.setVisible(RandomCorpusMASInputWindow.this.chckbxNormalAgeUnknown.isSelected());
			}
		});
		panelWeightFactors.add(this.chckbxNormalAgeUnknown, "4, 10");

		this.panelNormalAgeUnknown = new JPanel();
		panelWeightFactors.add(this.panelNormalAgeUnknown, "6, 10, fill, fill");
		this.panelNormalAgeUnknown.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu"), }, new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, }));

		JLabel label_8 = new JLabel("Mean:");
		this.panelNormalAgeUnknown.add(label_8, "1, 1");

		this.spnrNormalAgeUnknownMean = new JSpinner();
		this.spnrNormalAgeUnknownMean.setModel(new SpinnerNumberModel(new Double(25), new Double(0.0), null, new Double(1)));
		((JSpinner.NumberEditor) this.spnrNormalAgeUnknownMean.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelNormalAgeUnknown.add(this.spnrNormalAgeUnknownMean, "3, 1");

		JLabel label_9 = new JLabel("Stdev:");
		this.panelNormalAgeUnknown.add(label_9, "5, 1");

		this.spnrNormalAgeUnknownStdev = new JSpinner();
		this.spnrNormalAgeUnknownStdev.setModel(new SpinnerNumberModel(new Double(5), new Double(0.0), null, new Double(1)));
		((JSpinner.NumberEditor) this.spnrNormalAgeUnknownStdev.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelNormalAgeUnknown.add(this.spnrNormalAgeUnknownStdev, "7, 1");

		JLabel lblNormalAgeDifference = new JLabel("Normal age difference");
		panelWeightFactors.add(lblNormalAgeDifference, "2, 12");

		this.chckbxNormalAgeDifference = new JCheckBox("");
		this.chckbxNormalAgeDifference.setSelected(true);
		this.chckbxNormalAgeDifference.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				//
				RandomCorpusMASInputWindow.this.panelNormalAgeDifference.setVisible(RandomCorpusMASInputWindow.this.chckbxNormalAgeDifference.isSelected());
			}
		});
		panelWeightFactors.add(this.chckbxNormalAgeDifference, "4, 12");

		this.panelNormalAgeDifference = new JPanel();
		panelWeightFactors.add(this.panelNormalAgeDifference, "6, 12, fill, fill");
		this.panelNormalAgeDifference.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu"), }, new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, }));

		JLabel label_10 = new JLabel("Mean:");
		this.panelNormalAgeDifference.add(label_10, "1, 1");

		this.spnrNormalAgeDifferenceMean = new JSpinner();
		this.spnrNormalAgeDifferenceMean.setModel(new SpinnerNumberModel(new Double(0.0), new Double(0.0), null, new Double(1)));
		((JSpinner.NumberEditor) this.spnrNormalAgeDifferenceMean.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelNormalAgeDifference.add(this.spnrNormalAgeDifferenceMean, "3, 1");

		JLabel label_11 = new JLabel("Stdev:");
		this.panelNormalAgeDifference.add(label_11, "5, 1");

		this.spnrNormalAgeDifferenceStdev = new JSpinner();
		this.spnrNormalAgeDifferenceStdev.setModel(new SpinnerNumberModel(new Double(5), new Double(0.0), null, new Double(1)));
		((JSpinner.NumberEditor) this.spnrNormalAgeDifferenceStdev.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelNormalAgeDifference.add(this.spnrNormalAgeDifferenceStdev, "7, 1");

		JLabel lblAgnaticCousins = new JLabel("Agnatic Cousins");
		panelWeightFactors.add(lblAgnaticCousins, "2, 14");

		this.panelCousins = new JPanel();
		panelWeightFactors.add(this.panelCousins, "6, 14, fill, fill");
		this.panelCousins.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("50dlu"),
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("50dlu"),
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("50dlu"), },
				new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblFirst = new JLabel("First:");
		this.panelCousins.add(lblFirst, "1, 1");

		this.spnrAgnaticCousinsAlpha = new JSpinner();
		this.spnrAgnaticCousinsAlpha.setModel(new SpinnerNumberModel(new Double(1), new Double(0.0), null, new Double(1)));
		((JSpinner.NumberEditor) this.spnrAgnaticCousinsAlpha.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelCousins.add(this.spnrAgnaticCousinsAlpha, "3, 1");

		JLabel lblBravo = new JLabel("Second:");
		this.panelCousins.add(lblBravo, "5, 1");

		this.spnrAgnaticCousinsBravo = new JSpinner();
		this.spnrAgnaticCousinsBravo.setModel(new SpinnerNumberModel(new Double(1), new Double(0.0), null, new Double(1)));
		((JSpinner.NumberEditor) this.spnrAgnaticCousinsBravo.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelCousins.add(this.spnrAgnaticCousinsBravo, "7, 1");

		JLabel lblCharlie = new JLabel("Third:");
		this.panelCousins.add(lblCharlie, "9, 1");

		this.spnrAgnaticCousinsCharlie = new JSpinner();
		this.spnrAgnaticCousinsCharlie.setModel(new SpinnerNumberModel(new Double(1), new Double(0.0), null, new Double(1)));
		((JSpinner.NumberEditor) this.spnrAgnaticCousinsCharlie.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelCousins.add(this.spnrAgnaticCousinsCharlie, "11, 1");

		JLabel lblPregnancy = new JLabel("Pregnancy");
		panelWeightFactors.add(lblPregnancy, "2, 16");

		this.chckbxAgnaticCousins = new JCheckBox("");
		this.chckbxAgnaticCousins.setSelected(true);
		this.chckbxAgnaticCousins.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				//
				RandomCorpusMASInputWindow.this.panelCousins.setVisible(RandomCorpusMASInputWindow.this.chckbxAgnaticCousins.isSelected());
			}
		});
		panelWeightFactors.add(this.chckbxAgnaticCousins, "4, 14");

		this.panelPregnancy = new JPanel();
		panelWeightFactors.add(this.panelPregnancy, "6, 16, fill, fill");
		this.panelPregnancy.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu"), }, new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblAlpha_1 = new JLabel("Alpha:");
		this.panelPregnancy.add(lblAlpha_1, "1, 1");

		this.spnrPregnancyAlpha = new JSpinner();
		this.spnrPregnancyAlpha.setModel(new SpinnerNumberModel(new Double(0), new Double(0.0), null, new Double(1)));
		((JSpinner.NumberEditor) this.spnrPregnancyAlpha.getEditor()).getFormat().setMinimumFractionDigits(3);
		this.panelPregnancy.add(this.spnrPregnancyAlpha, "3, 1");

		this.chckbxPregnancy = new JCheckBox("");
		this.chckbxPregnancy.setSelected(true);
		this.chckbxPregnancy.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				//
				RandomCorpusMASInputWindow.this.panelPregnancy.setVisible(RandomCorpusMASInputWindow.this.chckbxPregnancy.isSelected());
			}
		});
		panelWeightFactors.add(this.chckbxPregnancy, "4, 16");

		this.lblChildren = new JLabel("Children");
		panelWeightFactors.add(this.lblChildren, "2, 18");

		this.chckbxChildren = new JCheckBox("");
		this.chckbxChildren.setSelected(true);
		panelWeightFactors.add(this.chckbxChildren, "4, 18");

		this.panelChildren = new JPanel();
		FlowLayout flowLayout = (FlowLayout) this.panelChildren.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		panelWeightFactors.add(this.panelChildren, "6, 18, fill, fill");

		this.label_12 = new JLabel("0:");
		this.panelChildren.add(this.label_12);

		this.spinner = new JSpinner();
		this.panelChildren.add(this.spinner);

		// //////////////////
		setDefaultCriteria(); // Useful to set default weight factors values.
		setCriteria(PuckGUI.instance().getPreferences().getRandomCorpusMASCriteria());
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public RandomCorpusCriteria getCriteria() throws PuckException {
		RandomCorpusCriteria result;

		//
		result = new RandomCorpusCriteria();

		//
		result.setYear((Integer) this.spnrYear.getValue());
		result.setInitialPopulation((Integer) this.spnrInitialPopulation.getValue());
		result.setFertilityRate((Double) this.spnrFertilityRate.getValue());
		result.setMaxAge((Integer) this.spnrMaxAge.getValue());

		//
		result.weightFactors().clear();

		if (this.chckbxDivorce.isSelected()) {
			DivorceWeightFactor factor = new DivorceWeightFactor();
			factor.setProbability((Double) this.spnrDivorceProbability.getValue());
			result.weightFactors().add(factor);
		}

		if (this.chckbxDivorce2.isSelected()) {
			Divorce2WeightFactor factor = new Divorce2WeightFactor();
			factor.setFemaleProbability((Double) this.spnrFemaleDivorceWeight.getValue());
			factor.setMaleProbability((Double) this.spnrMaleDivorceWeight.getValue());
			factor.setBothProbability((Double) this.spnrBothDivorceWeight.getValue());
			result.weightFactors().add(factor);
		}

		if (this.chckbxNormalAgeFemale.isSelected()) {
			NormalAgeWeightFactor factor = new NormalAgeWeightFactor();
			factor.setGender(Gender.FEMALE);
			factor.setMean((Double) this.spnrNormalAgeFemaleMean.getValue());
			factor.setStdev((Double) this.spnrNormalAgeFemaleStdev.getValue());
			result.weightFactors().add(factor);
		}

		if (this.chckbxNormalAgeMale.isSelected()) {
			NormalAgeWeightFactor factor = new NormalAgeWeightFactor();
			factor.setGender(Gender.MALE);
			factor.setMean((Double) this.spnrNormalAgeMaleMean.getValue());
			factor.setStdev((Double) this.spnrNormalAgeMaleStdev.getValue());
			result.weightFactors().add(factor);
		}

		if (this.chckbxNormalAgeUnknown.isSelected()) {
			NormalAgeWeightFactor factor = new NormalAgeWeightFactor();
			factor.setGender(Gender.UNKNOWN);
			factor.setMean((Double) this.spnrNormalAgeUnknownMean.getValue());
			factor.setStdev((Double) this.spnrNormalAgeUnknownStdev.getValue());
			result.weightFactors().add(factor);
		}

		if (this.chckbxNormalAgeDifference.isSelected()) {
			NormalAgeDifferenceWeightFactor factor = new NormalAgeDifferenceWeightFactor();
			factor.setMean((Double) this.spnrNormalAgeDifferenceMean.getValue());
			factor.setStdev((Double) this.spnrNormalAgeDifferenceStdev.getValue());
			result.weightFactors().add(factor);
		}

		if (this.chckbxAgnaticCousins.isSelected()) {
			CousinsWeightFactor factor = new CousinsWeightFactor();
			factor.setFirst((Double) this.spnrAgnaticCousinsAlpha.getValue());
			factor.setSecond((Double) this.spnrAgnaticCousinsBravo.getValue());
			factor.setThird((Double) this.spnrAgnaticCousinsCharlie.getValue());
			result.weightFactors().add(factor);
		}

		if (this.chckbxPregnancy.isSelected()) {
			PregnancyWeightFactor factor = new PregnancyWeightFactor();
			factor.setFirst((Double) this.spnrPregnancyAlpha.getValue());
			result.weightFactors().add(factor);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public void setCriteria(final RandomCorpusCriteria source) {
		//
		if (source == null) {
			setDefaultCriteria();
		} else {
			//
			this.spnrYear.setValue(source.getYear());
			this.spnrInitialPopulation.setValue(source.getInitialPopulation());
			this.spnrFertilityRate.setValue(source.getFertilityRate());
			this.spnrMaxAge.setValue(source.getMaxAge());

			this.chckbxDivorce.setSelected(false);
			this.chckbxDivorce2.setSelected(false);
			this.chckbxNormalAgeFemale.setSelected(false);
			this.chckbxNormalAgeMale.setSelected(false);
			this.chckbxNormalAgeUnknown.setSelected(false);
			this.chckbxNormalAgeDifference.setSelected(false);
			this.chckbxAgnaticCousins.setSelected(false);
			this.chckbxPregnancy.setSelected(false);

			//
			for (WeightFactor factor : source.weightFactors()) {
				switch (factor.getType()) {
					case DIVORCE:
						this.chckbxDivorce.setSelected(true);
						this.spnrDivorceProbability.setValue(((DivorceWeightFactor) factor).getProbability());
					break;

					case DIVORCE2:
						this.chckbxDivorce2.setSelected(true);
						this.spnrFemaleDivorceWeight.setValue(((Divorce2WeightFactor) factor).getFemaleProbability());
						this.spnrMaleDivorceWeight.setValue(((Divorce2WeightFactor) factor).getMaleProbability());
						this.spnrBothDivorceWeight.setValue(((Divorce2WeightFactor) factor).getBothProbability());
					break;

					case NORMAL_AGE:
						NormalAgeWeightFactor normalAgeFactor = (NormalAgeWeightFactor) factor;
						switch (normalAgeFactor.getGender()) {
							case FEMALE:
								this.chckbxNormalAgeFemale.setSelected(true);
								this.spnrNormalAgeFemaleMean.setValue(normalAgeFactor.getMean());
								this.spnrNormalAgeFemaleStdev.setValue(normalAgeFactor.getStdev());
							break;

							case MALE:
								this.chckbxNormalAgeMale.setSelected(true);
								this.spnrNormalAgeMaleMean.setValue(normalAgeFactor.getMean());
								this.spnrNormalAgeMaleStdev.setValue(normalAgeFactor.getStdev());
							break;

							case UNKNOWN:
								this.chckbxNormalAgeUnknown.setSelected(true);
								this.spnrNormalAgeUnknownMean.setValue(normalAgeFactor.getMean());
								this.spnrNormalAgeUnknownStdev.setValue(normalAgeFactor.getStdev());
							break;
						}
					break;

					case NORMAL_AGE_DIFFERENCE:
						this.chckbxNormalAgeDifference.setSelected(true);
						this.spnrNormalAgeDifferenceMean.setValue(((NormalAgeDifferenceWeightFactor) factor).getMean());
						this.spnrNormalAgeDifferenceStdev.setValue(((NormalAgeDifferenceWeightFactor) factor).getStdev());
					break;

					/*					case COUSINS:
											chckbxCousins.setSelected(true);
											spnrCousinsAlpha.setValue(((CousinsWeightFactor) factor).getFirst());
											spnrCousinsBravo.setValue(((CousinsWeightFactor) factor).getSecond());
											spnrCousinsCharlie.setValue(((CousinsWeightFactor) factor).getThird());
										break;*/

					case AGNATICCOUSINS:
						this.chckbxAgnaticCousins.setSelected(true);
						this.spnrAgnaticCousinsAlpha.setValue(((AgnaticCousinsWeightFactor) factor).getFirst());
						this.spnrAgnaticCousinsBravo.setValue(((AgnaticCousinsWeightFactor) factor).getSecond());
						this.spnrAgnaticCousinsCharlie.setValue(((AgnaticCousinsWeightFactor) factor).getThird());
					break;

					/*					case UTERINECOUSINS:
											chckbxCousins.setSelected(true);
											spnrCousinsAlpha.setValue(((CousinsWeightFactor) factor).getFirst());
											spnrCousinsBravo.setValue(((CousinsWeightFactor) factor).getSecond());
											spnrCousinsCharlie.setValue(((CousinsWeightFactor) factor).getThird());
										break;*/

					case PREGNANCY:
						this.chckbxPregnancy.setSelected(true);
						this.spnrPregnancyAlpha.setValue(((PregnancyWeightFactor) factor).getFirst());
					break;
				}
			}
		}
	}

	/**
	 * 
	 */
	public void setDefaultCriteria() {
		// First pass with all fields.
		RandomCorpusCriteria defaultCriteria = new RandomCorpusCriteria();
		defaultCriteria.weightFactors().add(new DivorceWeightFactor());
		defaultCriteria.weightFactors().add(new Divorce2WeightFactor());
		{
			NormalAgeWeightFactor factor = new NormalAgeWeightFactor();
			factor.setGender(Gender.MALE);
			defaultCriteria.weightFactors().add(factor);
		}
		{
			NormalAgeWeightFactor factor = new NormalAgeWeightFactor();
			factor.setGender(Gender.FEMALE);
			defaultCriteria.weightFactors().add(factor);
		}
		{
			NormalAgeWeightFactor factor = new NormalAgeWeightFactor();
			factor.setGender(Gender.UNKNOWN);
			defaultCriteria.weightFactors().add(factor);
		}
		defaultCriteria.weightFactors().add(new NormalAgeDifferenceWeightFactor());
		defaultCriteria.weightFactors().add(new CousinsWeightFactor());
		defaultCriteria.weightFactors().add(new AgnaticCousinsWeightFactor());
		defaultCriteria.weightFactors().add(new UterineCousinsWeightFactor());
		defaultCriteria.weightFactors().add(new PregnancyWeightFactor());

		setCriteria(defaultCriteria);

		// Second pass without weight factors field.
		defaultCriteria.weightFactors().clear();
		setCriteria(defaultCriteria);
	}
}
