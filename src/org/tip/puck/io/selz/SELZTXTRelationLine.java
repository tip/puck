package org.tip.puck.io.selz;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a line of individual from a TXT file.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class SELZTXTRelationLine {
	public static final int NOID = 0;

	private int id;
	private String name;
	private List<String> values;

	/**
	 * 
	 */
	public SELZTXTRelationLine() {
		this.id = NOID;
		this.name = null;
		this.values = new ArrayList<String>();
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public List<String> values() {
		return values;
	}
}
