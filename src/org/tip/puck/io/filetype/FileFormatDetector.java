package org.tip.puck.io.filetype;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.io.filetype.FileFormat.Format;

import fr.devinsy.util.FileTools;

/**
 * DRAFT
 * 
 * 
 * @author TIP
 * 
 */
public class FileFormatDetector {

	private static final Logger logger = LoggerFactory.getLogger(FileFormatDetector.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	/**
	 * 
	 * @param source
	 */
	public FileFormat dectectFormat(final File source) {
		FileFormat result;

		//
		result = new FileFormat();

		//
		if (source != null) {
			//
			result.setFormat(Format.UNKNOWN);

		} else {
			//
			String extension = StringUtils.lowerCase(FileTools.getExtension(source.getName()));

			//
			if (extension.endsWith("ged")) {
				//
				result.setFormat(Format.GEDCOM);

			} else if (extension.endsWith("ods")) {
				//
				if (source.exists()) {
					//
					// if (isBAR(source)) {
					// result.addFormat(Format.BAR_ODS);
					// }
					//
					// //
					// if (isSELZ(source)) {
					// result.addFormat(Format.SELZ_ODS);
					// }
					//
					// //
					// if (isIUR(source)) {
					// result.addFormat(Format.IUR_ODS);
					// }

				} else {
					//
					result.setFormat(Format.UNKNOWN);
				}

			} else if (extension.endsWith("paj")) {
				//
				result.setFormat(Format.PAJEK);

			} else if (extension.endsWith("pl")) {
				//
				result.setFormat(Format.PROLOG);

			} else if (extension.endsWith("puc")) {
				//
				result.setFormat(Format.PUC);

			} else if (extension.endsWith("tip")) {
				//
				result.setFormat(Format.TIP);

			} else if (extension.endsWith("txt")) {
				//
				if (source.exists()) {
					// if (isBAR(source)) {
					// result.addFormat(Format.BAR_XLS);
					// }
					//
					// //
					// if (isSELZ(source)) {
					// result.addFormat(Format.SELZ_XLS);
					// }
					//
					// //
					// if (isIUR(source)) {
					// result.addFormat(Format.IUR_XLS);
					// }

				} else {
					//
					result.setFormat(Format.UNKNOWN);
				}

			} else if (extension.endsWith("txt1")) {
				//
				result.setFormat(Format.BAR_TXT);

			} else if (extension.endsWith("txt3")) {
				//
				result.setFormat(Format.IUR_TXT);

			} else if (extension.endsWith("xls")) {
				//
				if (source.exists()) {
					//
					// if (isBAR(source)) {
					// result.addFormat(Format.BAR_XLS);
					// }
					//
					// //
					// if (isSELZ(source)) {
					// result.addFormat(Format.SELZ_XLS);
					// }
					//
					// //
					// if (isIUR(source)) {
					// result.addFormat(Format.IUR_XLS);
					// }

				} else {
					//
					result.setFormat(Format.UNKNOWN);
				}

			} else {
				//
				result.setFormat(Format.UNKNOWN);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static boolean isBARTXT(final File source) {
		boolean result;

		if (source == null) {
			result = false;
		} else {
			result = false;
		}

		//
		return result;
	}
}
