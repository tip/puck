package org.tip.puckgui.views.kinoath;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;

import nl.mpi.kinnate.kindata.EntityData;

import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.swing.JSVGScrollPane;
import org.apache.batik.swing.gvt.AbstractPanInteractor;
import org.apache.batik.swing.gvt.Interactor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.kinoath.KinOathDiagram;
import org.tip.puck.kinoath.io.KinOathFile;
import org.tip.puckgui.util.GUIToolBox;
import org.w3c.dom.Document;

/**
 * 
 * @author TIP
 */
public class KinOathDiagramPanel extends JSVGScrollPane {

	private static final long serialVersionUID = -8822574190413071405L;
	private static final Logger logger = LoggerFactory.getLogger(KinOathDiagramPanel.class);

	private KinOathDiagram diagram;

	/**
	 * 
	 * @param canvas
	 */
	public KinOathDiagramPanel() {
		this(null);
	}

	/**
	 * 
	 * @param canvas
	 */
	public KinOathDiagramPanel(final EntityData[] source) {
		super(new JSVGCanvas());

		//
		getCanvas().setEnableImageZoomInteractor(true);
		getCanvas().setEnablePanInteractor(true);
		getCanvas().setEnableRotateInteractor(true);
		getCanvas().setEnableZoomInteractor(true);

		//
		updateSource(source);

		// Allow scroll with the mouse left button.
		Interactor panInteractor = new AbstractPanInteractor() {
			/**
			 * 
			 */
			@Override
			public boolean startInteraction(final InputEvent event) {
				boolean result;

				int mods = event.getModifiers();

				if ((event.getID() == MouseEvent.MOUSE_PRESSED) && ((mods & InputEvent.BUTTON1_MASK) != 0)) {
					result = true;
				} else {
					result = false;
				}

				//
				return result;
			}
		};

		getCanvas().getInteractors().add(panInteractor);

		// Allow zoom actions with the mouse wheel.
		getCanvas().addMouseWheelListener(new MouseWheelListener() {
			/*
			 * (non-Javadoc)
			 * @see java.awt.event.MouseWheelListener#mouseWheelMoved(java.awt.event.MouseWheelEvent)
			 */
			@Override
			public void mouseWheelMoved(final MouseWheelEvent event) {

				if (event.getID() == MouseEvent.MOUSE_WHEEL) {

					if (event.getWheelRotation() > 0) {
						getCanvas().getActionMap().get(JSVGCanvas.ZOOM_OUT_ACTION).actionPerformed(null);
					} else {
						getCanvas().getActionMap().get(JSVGCanvas.ZOOM_IN_ACTION).actionPerformed(null);
					}
				}
			}
		});
	}

	/**
	 * 
	 */
	public void copyToClipboard() {
		//
		BufferedImage image = GUIToolBox.takeScreenshot(getCanvas());

		image = GUIToolBox.crop(image);

		GUIToolBox.copyToClipboard(image);
	}

	/**
	 * @throws IOException
	 * @throws PuckException
	 * 
	 */
	public void export() throws IOException, PuckException {
		//
		File targetFile = KinOathExportFileSelector.showSelectorDialog(null, null);

		//
		if (targetFile != null) {
			//
			if (targetFile.getName().endsWith("png")) {
				//
				GUIToolBox.saveScreenshot(getCanvas(), targetFile);

			} else {
				//
				KinOathFile.save(targetFile, this.diagram, "Exported from PUCK");
			}
		}
	}

	/**
	 * 
	 */
	public void resetZoom() {
		//
		this.canvas.resetRenderingTransform();
	}

	/**
	 * @param document
	 */
	public void setDocument(final Document document) {
		//
		if (getCanvas() != null) {
			getCanvas().setDocument(document);
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void updateSource(final EntityData[] source) {

		if (source == null) {
			//
			this.diagram = new KinOathDiagram(new EntityData[0]);

		} else if (source.length <= 500) {
			//
			this.diagram = new KinOathDiagram(source);

		} else {
			//
			String buttons[] = { "Continue", "Abort" };
			int response = JOptionPane.showOptionDialog(null, "Individual count is huge: " + source.length
					+ ".\n It can take a very long time for a low usable result.", "Warning", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE,
					null, buttons, buttons[0]);

			if (response == 0) {
				//
				this.diagram = new KinOathDiagram(source);

			} else {
				//
				this.diagram = new KinOathDiagram(new EntityData[0]);
			}
		}

		//
		setDocument(this.diagram.getDocument());
	}
}
