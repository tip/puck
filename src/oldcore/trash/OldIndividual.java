package oldcore.trash;




/**
 * This class defines a standard vertex, representing an individual. 
 * @author Klaus Hamberger
 * @since Version 1.0
 */
public class OldIndividual {

	//economize dsf fields: color, vistited1/2, visitor
	/**
	 * the apical couple represented by a neutralized apical vertex
	 */
//	private OldIndividual[] apiCouple;
	/**
	 * the numbers of known (agnatic, uterine and cognatic) ascendants in each ascending generation
	 */
//	private int[][] ascendants;
	/**
	 * the attributes of the Vertex
	 */
//	private Attributes attributes;
	/**
	 * the color mark of the vertex
	 * <p> used for recursive partitioning
	 */
//	private int color;
	/**
	 * the discovery time
	 * used for dfs search methods
	 */
//	private int discovery;
	//Check
	/**
	 * the generational level of the vertex
	 */
//	private int level;
	/**
	 * the p-links of a vertex in a p-graph
	 */
//	private ArrayList<OldIndividual> pLinks;
	/**
	 * the group of relatives of a given kin type (-1 = ch, 0 = sp, 1 = f/m)
	 */
//	private KinGroup[] relatives;


	/**
	 * checks whether a string is equal to another or represents an integer that is within an interval represented by another string (in the format "a-b")
	 * <p> used for binarizing partitions
	 * @param str1 the first string
	 * @param str2 the second string
	 * @return true if the two strings are equal or if the first represents an integer within the interval represented by the other 
	 * @see OldIndividual#getCluster(String, String, int, boolean)
	 */
/*	private static boolean binarize (String str1, String str2){
		if (str1==null) return false;
		if (str2.indexOf('-')>-1) return isBetween(Integer.parseInt(str1),span(str2));
		if (str2.indexOf('*')>-1) {
			str2 = str2.replaceAll("\\*","");
			return str1.trim().indexOf(str2)>-1;
		}
    	return str1.trim().equals(str2);
	}*/
	


	/**
	 * returns the negative value of an integer
	 * @param t an integer
	 * @return its negative value
	 */
/*	private static int i (int t){
		return (-1)*t;
	}*/

	/**
	 * checks whether an integer is between given bounds
	 * @param i the integer to be checked
	 * @param s the array of bounds
	 * @return true if the integer is between the bounds
	 */
/*	private static boolean isBetween (int i, int[] s){
		return (i>=s[0] && i<=s[1]);
	}*/



	/**
	 * transforms the string expression of a time span into an integer array (consisting of the lower and the upper bound)
	 * @param lb the string expression of the time span (in format "x-y")
	 * @return the integer array expression of the time span (in format {x,y})
	 * @see OldIndividual#binarize(String, String)
	 */
/*	private static int[] span (String lb){
		String[] str = lb.split("-");
		return new int[] {Integer.parseInt(str[0].trim()),Integer.parseInt(str[1].trim())};
	}*/
   
		//generalize
	/**
	 * standardizes the expressions for attribute positions  (date, place, seedReferenceYear) in cluster labels
	 * @param lb the cluster label
	 * @return the standardized cluster label
	 */
/*	private static String standardize (String lb){
		return lb.replaceAll("YEAR", "Year").replaceAll("DATE", "Date").replaceAll("PLACE", "Place");			
	}*/
	
	/**
	 * translates the status index into a status label
	 * @param the index of the status label
	 * @return the status label
	 * @see OldIndividual#getStatus(int, boolean)
	 */
/*	public static String status (int i){
		return (String)Puck.getParam("Status Labels",i);
	}*/
	
	/**
	 * returns the string representation of a boolean
	 * @param b the boolean variable
	 * @return the string representation of the boolean ("true", "false")
	 */
/*	private static String toString (boolean b){
		if (b) return "true";
		return "false";
	}*/
	
	/**
	 * translates gendered into ungendered kin type indices <p>
	 * @param t the gendered kin type index (ch -1, f 0, m 1, sp 2)
	 * @return the ungendered kin type index of the kin type (ch -1, sp 0, f/m 1)
	 */
/*	private static int type (int t) {
		if (t == 2) return 0;
		if (t == 0) return 1;
		return t;
	}*/
   
	/**
	 * constructs a vertex and initializes the parent list
	 */
/*	public OldIndividual () {
		setParents();
	}*/
	
	/**
	 * constructs a vertex with given number, label and gender and initializes the parent list
	 * @param i the vertice's identity number
	 * @param str the vertice's label
	 * @param g the vertice's gender
	 */
/*	public OldIndividual (int i, String str, int g) {
		setNumber(i);
		setGender(g);
		setLabel(str);
		setParents();
	}*/


	


	
	/**
	 * adds a relative, differentiating between father and mother (fathers and mothers are differentiated by indicating the position within the kin group,
	 * 0 (f) or 1 (m), in addition to the kin type index, 1) 
	 * @param v the vertex to be added
	 * @param t the kin type index (-1 ch, 0 sp, 1 f/m)
	 * @param i the position index (0 f, 1 m), only taken into account for parents
	 * @param rec the reciprocal character of the operation
	 */
/*	public void addKin (OldIndividual v, int t, int i, boolean rec){
		if (t==1) setKin (v, t, i, rec);
		else addKin (v,t,rec);
	}*/
	
	//Simplify (by direct reference to cluster instead of detour via label?)
	/**
	 * gets the kinship affiliation profile of the vertex to a cluster to which it belongs otherwise
	 * @param lb1 the label of the relative's attribute
	 * @param lb2 the label of the vertice's attribute which is to coincide with the relative's attribute
	 * @return the affiliation profile as an integer array
	 * @see partitions.NodePartition#affiliationStatistics(String, String)
	 */
/*	public int[] affiliationStatistics (String lb1, String lb2){
		int[] af = new int[16];
		if (hasClusterLink(this,lb1,lb2)) {
			af[0]=1;
			return af;
		} else if (hasClusterLink(spouses(),lb1,lb2)){
			af[9]=1;
		} else {
			for (int i=0;i<2;i++){
				OldIndividual v = getParent(i);
				if (hasClusterLink(v,lb1,lb2)) {
					af[1+i]=1;
					if (i==1) af[10]=0;
				} else if (v!=null) {
					if (hasClusterLink(v.spouses(),lb1,lb2)) af[10+i]=1;
					for (int j=0;j<2;j++){
						OldIndividual w = v.getParent(j);
						if (hasClusterLink(w,lb1,lb2)) {
							af[3+2*i+j]=1;
							if (i==0 && j==1) af[12]=0;
						} else if (w!=null && i==j){
							if (hasClusterLink(w.spouses(),lb1,lb2)) af[12+i]=1;
							if (hasClusterLink(w.getParent(i),lb1,lb2)) af[7+i]=1;
						}
					}
				}
			}
		}
		if (isSingle() || af[9]==1) return af;
		for (OldIndividual v : spouses()){
			for (int j=0;j<2;j++){
				if (hasClusterLink(v.getParent(j),lb1,lb2))	af[14+j]=1;
			}
		}
		return af;
	}*/

	/**
	 * returns the gender of possible spouses
	 * @return the gender of possible spouses
	 */
/*	public Gender alterGender () {
		return getGender().invert();
	}*/

	/**
	 * returns the gender of possible relatives according to their kinship relation
	 * @param i the gendered kinship relation index (-1 child, 0 mother, 1 father, 2 spouse)
	 * @return the gender of possible relatives
	 */
/*	public int alterGender (int i) {
		if (i == -1) return 2;
		if (i < 2) return i;
		if ((getGender()==-1) || (getGender()==2)) return 2;
		return (getGender()+1)%2;
	}*/
	


	//refine, with "equal" function for attributes
	/**
	 * returns the index of an attribute with given label and alter
	 * @param a the label of the attribute
	 * @param alter the number of alter
	 * @see OldIndividual#setAttribute(String, String, int, int)
	 */
/*	private int attNr (String a, int alter) {
		if (attributes == null) return -1;
		for (int i=0;i<attributes.size();i++) {
			Attribute att = attributes.get(i);  
		    if (att.getLabel().equals(a) && att.getID()==alter) return i;
		}
		return -1;
	}*/
	   
	/**
	 * returns the list of attributes (total or partial)
	 * @param separate true if partial attributes (date, place, etc.) are to be listed separately
	 * @return the list of attributes 
	 */
/*	private Attributes attributes (boolean separate) {
		if (!separate) return attributes;
		Attributes att = new Attributes();      
		for (Attribute a : attributes) {
			for (int i=0;i<5;i++) {
				if (a.get(i) != null) att.add(a.extract(i));
			}
		}
		return att;
	}*/
	 

   

	
		/**
	 * checks for filiation cycles in the network
	 * @param control the protocol to note found cycles
	 * @param k the maximal cycle length to be checked
	 */
/*	public void checkCycles (List<Chain> cycles, int k) {
		
		checkCycles(cycles,this,k,new Chain());
	}*/
	
	/**
	 * checks for filiation cycles in the network
	 * @param cycles the protocol to note found cycles
	 * @param k the maximal cycle length to be checked
	 * @param v the last vertex of the chain
	 * @param str a string containing the preceding kinship chain in positional notation 
	 */
/*	private void checkCycles (List<Chain> cycles, Individual v, int k, Chain chain) {
		if (k==0) return;
		chain.add(this);
		if (equals(v)) cycles.add(chain);
		for (Individual w : getParents()) {
			if (w==null) continue;
			w.checkCycles(cycles,v,k-1,chain);
		}
	}*/



	/**
	 * transfers relatives from the vertice's double in another network
	 * <p> used for subnet construction
	 * @param net the double's home network
	 * @see maps#Subnet#SubNet(Net,int)
	 */
/*	private void copyKin (Net net) {
		for (int i=-1;i<2;i++) {
			copyKin(net.get(getID()),i);
		}
	}*/

	/**
	 * transfers relatives from one vertex to another
	 * @param v the vertex whose relatives are transferred
	 * @param t the kin type index
	 */
/*	private void copyKin (Vertex v, int t){
		if (v.kin(t)==null) return;
		int i=0;
		for (Vertex w : v.kin(t)){
			if (w==null) continue;
			addKin(((Net)home).getCopy(w),t,i,false);
			i++;
		}
	}*/
	

	

   
	/**
	 * counts the number of known ascendents of a given generational layer
	 * an intermediate method used for the computation of mean generational depth
	 * @param depth a list of numbers of known ascendants of ascending generational layers
	 * @param t the generational layer
	 * @see OldIndividual#meanDepth()
	 */
/*	private void count (ArrayList<Integer> depth, int t){
		if (t>depth.size()) depth.add(0);
		if (t>0) depth.set(t-1,depth.get(t-1)+1);
		for (int i=0;i<2;i++){
			OldIndividual p = getKin(1,i);
			if (p!=null) p.count(depth, t+1);
		}
	}*/
	
	//test!
	/**
	 * gets the maximal generational depth of a vertex 
	 * @param g the filter criterion for ascendant lines (0 agnatic, 1 uterine, 2 cognatic) 
	 * @return the maximal generational depth
	 * @author modified by Andrzej Kabat
	 */
/*	public int depth (int g) {
		int[] d = {0,0};
		int i = 0;
		for (Individual v : getParents()){
			if (v==null) continue;
			d[i] = v.depth(g)+1;
			i++;
		}
		switch(g){
		case 0: return d[0];
		case 1: return d[1];
		case 2: return Math.max(d[0], d[1]);
		default: return 0;
		}
	}*/
	/*
	public int depth (int g) {
		int[] d = {0,0};
		int i = -1;
		for (Vertex v : getParents()){
			i++;
			if (Mat.noMatch(i,g)) continue;  //g>0 && g!=i??
			if (v==null) continue;
			d[i] = v.depth(g)+1;
		}
		return Math.max(d[0], d[1]);
	}
	*/
	

	

	






	/**
	 * gets the most remote ancestor of a vertex
	 * @param g the filter criterion for ascendant lines (0 agnatic, 1 uterine, 2 cognatic) 
	 * @return the most remote ancestor 
	 */
/*	private Object getAncestor (int g){
		return getAncestor(g,null);
	}*/

	//check and test
	/**
	 * gets the most remote ancestor of a vertex
	 * @param g the filter criterion for ascendant lines (0 agnatic, 1 uterine, 2 cognatic) 
	 * @param label ***
	 * @return the most remote ancestor 
	 */
/*	public Object getAncestor (int g, String label){
		if (cluster!=null) return cluster;
		OldIndividual v = getKin(1,g);
		if (label!=null) setCluster(getAttributeValue(label));
		else if (v==null) setCluster(this);
		if (v==null || cluster!=null) return cluster;
		Object a = v.getCluster();
		if (a!=null) setCluster(a);
		else setCluster(v.getAncestor(g,label));
		return cluster;
	}*/


	/**
    * gets the arc representations of kinship ties and stores them in an arc list
    * @param arcs the arc list  
    * @param t the code for the graph type (0 ore, 1 pgraph, 2 tip)  
    */
/*	public void getArcs (List<Arc> arcs, int t){
		for (int i=0;i<2;i++){
			if (getKin(i)!=null) getKin(i).getArcs(arcs,t);
		}
	}      */                          

	/**
	 * sets the numbers of known (agnatic, uterine and cognatic) ascendants in each ascending generation  
	 * @return an array containing the numbers of known (agnatic, uterine and cognatic) ascendants in each ascending generation
	 * @see maps.Net#setAscendants(int)
	 * @see maps.Net#completeness(ArrayList, int)
	 */
/*	public int[][] getAscendants() {
		return ascendants;
	}*/


   
	//Check attribute types
	/**
	 * returns the attributes of a given attribute type
	 * @param t the attribute type (0 properties, 1 events, 2 relations, 3 notes)
	 * @return the list of attributes of the required type
	 * @since modified by Andrzej Kabat
	 */
/*	public AttributeGroup getAttributes (int t){
		AttributeGroup att = new AttributeGroup();
		if (attributes==null) return att;
		if (t==3) return attributes;
		for (Attribute a : attributes) {
			if (a.getType() == t)  att.add(a);
		}
		return att;
	}*/

	/**
	 * gets the bridges between the vertex and an alter vertex
	 * @param v the alter vertex
	 * @param r the maximal depth of the bridge
	 * @param g the filter criterion for ascendant lines (0 agnatic, 1 uterine, 2 cognatic) 
	 * @param sib the sibling mode
	 * @return the list of bridges
	 */
/*	public ArrayList<Ring> getBridges(Vertex v, int r, int g, int sib){
		ArrayList<Ring> bridges = new ArrayList<Ring>();
		if (equals(v)) bridges.add(new Ring(new Path(this),new Path(v)));
		else new Path(this).getBridges(bridges,v,r,g,sib);
		return bridges;
	}*/

	//rename
	/**
	 * gets the kinship chains between the ego vertex and an alter vertex
	 * @param v the alter vertex
	 * @param k the maximal depth of the chains
	 * @param w the maximal width (order) of the chains
	 * @return the list of kinship chains between ego and alter
	 */
/*	public ArrayList<String> getChains (OldIndividual v, int k, int w) {
//		((Net)home).begin();
	    ArrayList<String> chains = new ArrayList<String>();
	    for (Ring p : search(v,k,w)){
	    	chains.add(p.signature(12));
//	    	chains.add(p.signature1());
	    }
//	    ((Net)home).end();
	    return chains;
	}*/
   



	/**
	 * gets the cluster to which the vertex belongs. 
	 * <p>Used in partitioning
	 * @param lb the attribute label of the partition 
	 * @param bin the cluster value or interval used for binarization
	 * @param deg the degree of the kinship relation (in case of PEDG and PROG labels) 
	 * @return the cluster to which the vertex belongs
	 */
/*	public Object getCluster (String lb, String bin, int deg){
		return getCluster(lb,bin,deg,false);
	}*/
   
	//check for multi=true + bin=true and simplify! 
	//check pertinence of multi-variable (only serves to distinguish from previous method) 
	/**
	 * gets the cluster to which the vertex belongs. 
	 * <p>Used in partitioning
	 * @param lb the attribute label of the partition 
	 * @param bin the cluster value or interval used for binarization
	 * @param deg the degree of the kinship relation (in case of PEDG and PROG labels) 
	 * @param multi true if the attribute allows for multiple values
	 * @return the cluster to which the vertex belongs
	 */
/*	public Object getCluster (String lb, String bin, int deg, boolean multi) {
		try {
			int i = Integer.parseInt(lb);
			return getStatus(i);
		} catch (NumberFormatException nfe) {};
		
		if (bin!=null) return toString(binarize((String)(getCluster(lb.trim(),null,deg)+""),bin.trim()));

		if (lb.equals("PEDG")) return getNrLinKin(deg);
		if (lb.equals("PROG")) return getNrLinKin(-deg);
		if (lb.equals("GEN")) return getLevel();
		if (lb.equals("MDEPTH")) return meanDepth();
		if (lb.equals("SPOU")) return nrSpouses();
		if (lb.equals("PROG 1")) return nrChildren();
		if (lb.equals("SEX")) return getGender(4);
		if (lb.equals("FIRSTN")) return getFirstName();
		if (lb.equals("LASTN")) return getLastName();
	    if (lb.equals("SUB")) return getColor();
		if (lb.equals("SIMPLE")) return getLabel();
		if (lb.equals("ID")) return getID();
		if (lb.equals("SIBSETP")) return getFratry(0);
		if (lb.equals("SIBSETM")) return getFratry(1);
		lb = standardize(lb);
		if (bin!=null){ //lb.indexOf('=')>-1
//			String[] bin = lb.split("="); //bin[0] = lb, bin[1]=bin
//			if (!multi) return toString(binarize((String)getCluster(lb.trim()),bin.trim()));
			if (attributes()==null) return null;
			TreeMap<Integer,String> map = new TreeMap<Integer,String>();
		    for (Attribute a : attributes(lb.split(" ").length>1)) {
		    	if (a.getLabel().equals(lb.trim())) {
		    		if (binarize(a.get(0),bin)) map.put(a.getID(), a.get(0));
		    	}
		    }
	    	return map;
		}
		if (attributes()==null) return null;
		TreeMap<Integer,String> map = new TreeMap<Integer,String>();
	    for (Attribute a : attributes(lb.split(" ").length>1)) {
	    	if (a.getLabel().equals(lb)) {
	    		if (multi && a.getID()!=0) map.put(a.getID(), a.get(5)); //get(0)
	    		else return a.get(5);
//	    		if (!multi) return a.get(5); //get(0)
//    			map.put(a.getID(), a.get(5)); //get(0)
	    	}
	    }
	    if (map.size()>0) return map;
//	    if (multi) return map;
	    return null;
	}*/
	
	/**
	 * gets a string representation of the cluster to which the vertex belongs
	 * @param lb the label of the partition attribute
	 * @return a string representation of the cluster to which the vertex belongs
	 * @see OldIndividual#hasClusterLink(List, String, String)
	 */
/*	private String getClusterString (String lb){
		Object clu = getAttributeValue(lb);
		if (clu==null) return "";
		return clu.toString().trim();
	}*/
	
	
	/**
	 * gets the common marriage seedReferenceYear of two vertices, or 0 if there is none
	 * @param v the alter vertex 
	 * @return the common marriage seedReferenceYear of two vertices, or 0 if there is none
	 * @see chains.NumberChain#getlastMarried(Net)
	 */
/*	public int getCommonMarriageYear (OldIndividual v){
		for (int i : getMarriageYears()){
			for (int j : v.getMarriageYears()){
				if (i==j) return i;
			}
		}
		return 0;
	}*/
	
	/**
	 * returns a new vertex of equal number, label and gender
	 * @return the copy of the vertex
	 */
/*	public OldIndividual getCopy(){
		return new OldIndividual(getID(),getLabel(),getGender());
	}*/


	


	/**
	 * returns the maximal generational depth of the vertice's pedigree (agnatic, uterine or indifferent)
	 * @param g 0 for agnatic, 1 for uterine, 2 for indifferent pedigree
	 * @return the maximal depth of the pedigree
	 */
/*	public int getDepth (int g){
		if (cluster!=null) return (Integer)cluster;
		if (g<2){
			OldIndividual v = getKin(1,g);
			if (v==null || v.ignore()) return 0;
			if (v.getCluster()!=null) {
				setCluster((Integer)v.getCluster()+1);
				return (Integer)cluster;
			}
			setCluster(v.getDepth(g)+1);
			return (Integer)cluster;
		} else {
			int[] s = {0,0};
			for (int k=0;k<2;k++){
				OldIndividual v = getKin(1,k);
				if (v==null || v.ignore()) continue;
				if (v.getCluster()!=null) {
					s[k]= (Integer)v.getCluster()+1;
				} else {
					s[k] = v.getDepth(g)+1;
				}
			}
			setCluster(Math.max(s[0], s[1]));
			return (Integer)cluster;
		}
	}*/
	
	/**
	 * gets the discovery time
	 * <p> used for dfs search methods
	 * @return the discovery time
	 * @see OldIndividual.nodes.Vertex#bicomp(OldIndividual, Stack, int, int, Net, boolean, boolean)
	 * @see maps.Net#bicomp(OldIndividual, OldIndividual, Stack, int, int, boolean, boolean)
	 */
/*	public int getDiscovery() {
		return discovery;
	}*/
	
/*	private KinGroup getCoSpouses (){
		KinGroup csp = new KinGroup();
		if (isSingle()) return csp;
		for (Vertex v : getSpouses()) {
			for (Vertex w : v.getSpouses()){
				if (!v.equals(this)) csp.add(w);
			}
		}
		return csp;
	}*/
	


/*	public int[][] relationStatistics1 (List<Vertex> clu){
		int[][] md = new int[9][2];
		for (Vertex v : clu){
			int k = 0;
			if (getGender()!=v.getGender()) k = 1;
			if (v.getID()<=getID()) continue;
			int bil = 0;
//			boolean unil = false;
			for (int g=0;g<2;g++){
				int h = getAffinalDistance(v,g);
				if (h<1) continue;
				bil++;
//				unil = true;
				md[2*g][k]++;
				md[2*g+1][k]=md[2*g+1][k]+h;
			}
			if (bil==2) md[6][k]++;
			if (bil>0) { //unil
				md[4][k]++;
				int m = Math.min(md[1][k],md[3][k]);
				if (m==0) m = md[1][k]+md[3][k];
				md[5][k]=md[5][k]+m;
//				System.out.println(md[1][k]+" "+md[3][k]+" "+md[5][k]);
				continue;
			}
			int h = getAffinalDistance(v,-1);
			if (h<1) continue;
			md[4][k]++;
			md[5][k]=md[5][k]+h;
		}
		return md;
	}*/


	

	
	/**
	 * returns the parent's identity number, or 0 if there is no parent
	 * <p> this number serves as an identity number of the vertices sibset (half-sibling group)
	 * @param g the character of the sibling relation (0=agnatic, 1=uterine, 2=indifferent)
	 * @return the parent's identity number, or the negative of the individual's identity number if there is no parent
	 * @since last modified 10-05-17
	 */
/*	public int getFratry (int g){
		OldIndividual p = getParent(g);
		if (p==null) return -getID();
		return p.getID();
	}*/


	

	


	
	/**
	 * translates a kin tie index into a kin term
	 * @param k the gendered kin type index (ch -1, f 0, m 1, sp 2)
	 * @return the corresponding kin term
	 * @see maps.Net#getLinked(OldIndividual, int, int)
	 */
/*	public String getKinTerm (int k) {
//		String[] title = (String[])Puck.getParam("KinGroup Terms");
		int i = k;
		if (k == 2) i = 3;
		if (k == -1) i = 6;
		return Puck.getKinTerm(i+getGender());
	}*/




	//check!
	/**
	 * gets the generational level of the vertex
	 * @param k -1 if direction is descending, 1 if it is ascending //?? 
	 * @return the generational level of the vertex
	 * @see OldIndividual#setDepth(int, int, int)
	 */
/*	private int getLevel(int k){
		if (k==-1) return color;
		return level;
	}*/



	/**
	 * gets the list of marriage years
	 * @return the list of marriage years
	 * @see OldIndividual#getCommonMarriageYear(OldIndividual)
	 */
/*	private ArrayList<Integer> getMarriageYears (){
		ArrayList<Integer> list = new ArrayList<Integer>();
		if (attributes==null) return list;
		for (Attribute a : attributes){
			if (a.getLabel().equals("MARR")) {
				String y = a.getSeedReferenceYear();
				if (y!=null) list.add(Integer.parseInt(y));
			}
		}
		return list;
	}*/

	// for old CountConsanguines-Method, replace!
	/**
	 * gets the number of consanguines
	 * @param dmax the maximal depth of the consanguine relations
	 * @param g the filter criterion for ascendant lines (0 agnatic, 1 uterine, 2 cognatic)
	 * @see OldIndividual#nrArcs(int, int) 
	 */
/*	public int[] getNrConsanguines (int dmax, int g) {	
		int[] cons = new int[dmax+1];
		nrConsanguines(getID(),cons,0,0,dmax,dmax,g);
		if (dmax<2) return cons;
		for (int i=2;i<dmax+1;i++){
			cons[i]=cons[i]+cons[i-1];
		}
		return cons;
	}*/
	

	/**
	 * gets the number of linear ascendants or descendants of a given degree<p>
	 * used for PEDG and PROG partitions
	 * @param deg the degree of ascendance (>0) or descendance (<0)
	 * @return the number of linear ascendants or descendants of degree deg
	 */
/*	private int getNrLinKin (int deg){
		if (deg==1) return nrParents();
		if (deg==-1) return nrChildren();
		int t = deg/Math.abs(deg);
		if (getKin(t)==null) return 0;
		int n = 0;
		for (OldIndividual v : getKin(t)){
			if (v==null) continue;
			n = n+v.getNrLinKin(deg-t);
		}
		return n;
	}*/
   

   

   


	//-> P-graph
	//change...
	/**
	 * gets the p-links of a vertex in a p-graph
	 * @return the p-links of the vertex
	 */
/*	public ArrayList<OldIndividual> getPLinks() {
		return pLinks;
	}*/






	//Render status codes explicit
	/**
	 * gets the vertice's status (dead, alive, married, divorced etc.) at a given point of time (seedReferenceYear)
	 * @param i the point of time
	 * @return the vertice's status in string format
	 * @see OldIndividual#getCluster(String, String, int, boolean)
	 */
/*	public String getStatus (int i){
		return getStatus (i, true);
	}*/
	
	/**
	 * gets the vertice's status (dead, alive, married, divorced etc.) at a given point of time (seedReferenceYear)
	 * @param i the point of time
	 * @param withMarr true if matrimonial status is also to be checked
	 * @return the vertice's status in string format
	 * @see OldIndividual#getStatus(int)
	 */
/*	private String getStatus (int i, boolean withMarr){
		
		if (attributes==null) return null;
		String s = "";
		String alt = "";
		int birt = 0;
		int deat = 0;
		int marr = 0;
		for (Attribute a : attributes) {
			if (birt==0) birt = a.getYear("BIRT");
			if (deat==0) deat = a.getYear("DEAT");
			if (!withMarr) continue;
			int m = a.getYear("MARR");
			if (m<=Math.abs(marr)) continue;
			marr = m;
			OldIndividual alter = ((Net)home).get(a.getID());
			if (alter!=null) alt = alter.getStatus(i,false);
			if (alt.equals(status(0))) marr = -marr;
		}
		if (deat>0 && deat<i) return status(0);
		if (birt>i) return status(2);
		if (birt==0 || deat==0) return status(1);
		if (marr>0) s = status(3);
		if (marr<0) s = status(4);
		if (!s.equals("")) return s;
		return status(5);
	}*/



   
	/**
	 * gets the seedReferenceYear of an event in the vertice's lifetime
	 * <p> used for printing birth years and death years in xml files
	 * @param lb the label of the event
	 * @return the seedReferenceYear of the event
	 * @see io.write.AbstractWriter#writeXml(Net,int,String,int)
	 */
/*	public String getYear (String lb){
        if (attributes== null) return "na";
       	for (Attribute a : attributes) {
       		if (!a.getLabel().equals(lb)) continue;
       		String y = a.getSeedReferenceYear();
       		if (y!=null && !y.equals("0")) return y;
       	}
       	return "na";
	}*/

	//function is now accomplished by "Families.getFertileCount
	/**
	 * checks the existence of common children with another vertex
	 * @param v the partner vertex
	 * @return true if there are common children
	 * @see maps.Net#nrFertileCouples()
	 */
/*	public boolean hasChildrenWith (OldIndividual v){
		if (isSterile()) return false;
		for (OldIndividual c : getChildren()){
			OldIndividual p = c.getParent(alterGender());
			if (p!=null && p.equals(v)) return true;
		}
		return false;
	}*/

	/**
	 * checks whether a cluster of the vertex in partition A correpsonds to the cluster of one of the vertices in a list in partition B 
	 * <p> used for determining kinship-mediated cluster affiliations 
	 * @param list the list of vertices to be compared
	 * @param lb1 the label of the first partition
	 * @param lb2 the label of the second partition
	 * @return true if one of the B-clusters of the vertices in the list coincide with the A-cluster of ego 
	 * @see OldIndividual#affiliationStatistics(String, String)
	 */
/*	private boolean hasClusterLink (List<OldIndividual> list, String lb1, String lb2){
		if (list==null) return false;
		for (OldIndividual v : list){
			if (hasClusterLink(v,lb1,lb2)) return true;
		}
		return false;
	}*/
	
	/**
	 * checks whether a cluster of the vertex in partition A correpsonds to the cluster of another vertex in partition B 
	 * <p> used for determining kinship-mediated cluster affiliations 
	 * @param v the alter vertex
	 * @param lb1 the label of the first partition
	 * @param lb2 the label of the second partition
	 * @return true if the B-cluster of alter coincides with the A-cluster of ego
	 * @see OldIndividual#affiliationStatistics(String, String)
	 */
/*	private boolean hasClusterLink (OldIndividual v, String lb1, String lb2){
		if (v==null) return false;
		String s1 = v.getClusterString(lb1);
		String s2 = getClusterString(lb2);
		return (s1.equals(s2));
	}*/
		


/*	public boolean isMarried () {  // replaced by !isSingle()
		return nrSpouses()>0;
	}*/

	/**
	 * checks whether the vertex shall be ignored in a method
	 * @return true if the vertex shall be ignored
	 * @since last modified 10/04/12
	 */
/*	public boolean ignore (boolean condition) {
		if (!condition) return false;
		if (Trafo.isNull(getLabel())) return true;
		if (getLabel().charAt(0)=='#') return true;
		return false;
	}*/

	/**
	 * checks whether the vertex shall be ignored in a method
	 * @return true if the vertex shall be ignored
	 */
/*	public boolean ignore (){
		return ignore(true);
	}*/



	/**
	 * checks whether there is a simple kinship link (parent-child or spouse) to another vertex
	 * @param v the alter vertex
	 * @return true if ego and alter are linked by a simple kinship tie
	 * @see CircuitFinder#isApiLinked(OldIndividual)
	 */
/*	public boolean isDirectlyLinked (OldIndividual v) {
	    if (isSpouse(v)) return true;     
		if (getParents().contains(v)) return true;
	    if (v.getParents().contains(this)) return true;
	    return false;	
	}    */



	/* Equivalent � isSpouse()
	private boolean isMarriedTo (Vertex v) {
		if (isSingle()) return false;
		for (Vertex w : getSpouses()) {
			if (w.equals(v)) return true;
		}
		return false;
	}*/
	
	/*
	private boolean isSpouseOrCoSpouse (Vertex v){
		if (isSingle()) return false;
		for (Vertex w : getSpouses()){
			if (w.equals(v) || w.isSpouse(v)) return true;
		}
		return false;
	}*/

	/**
	 * checks whether another vertex belongs to the same network<p>
	 * used for restricting relatives to given subnetworks, for instance for calculating orphans or widows at a given period 
	 * @see OldIndividual#nrParents(boolean)
	 * @param v the vertex to be checked
	 * @return true of v belongs to the same network as the vertex
	 */
/*	private boolean isNetFellow (OldIndividual v){
		return home.contains(v);
	}*/



	/**
	 * checks whether the vertex is an orphan has no parents (at all or in its own network)
	 * @param sameNet true if the parents have to be in the same network as the vertex
	 * @return true if the vertex has no parents
	 * @see OldIndividual#isOrphan()
	 */
/*	private boolean isOrphan(boolean sameNet){
		return nrParents(sameNet)==0;
	}*/



   /**
 * checks whether the vertex is married to another one
 * @param v the alter vertex
 * @return true if ego and alter are married
 * @see OldIndividual#isDirectlyLinked(OldIndividual)
 */
/*public boolean isSpouse (OldIndividual v){
	try {
		return spouses().contains(v);
	} catch (NullPointerException npe){
		return false;
	}
}*/




	/**
	 * computes the mean generational depth of the pedigree, according to the formula of Cazes and Cazes 1996<p>
	 * see Cazes, Marie-H�l�ne et Pierre Cazes (1996), � Comment mesurer la profondeur g�n�alogique d�une ascendance ? �, Population, 51 (1), 117-140
	 * @return the mean generational depth
	 */
/*	public double meanDepth (){
		ArrayList<Integer> d = new ArrayList<Integer>();
		double depth = 0.;
		count(d,0);
		int g = 1;
		for (int i:d){
			depth = depth+util.Mat.percent(i,100*Mat.pow2(g));
			g++;
		}
		return depth;
	}*/

	/**
	 * returns the number of children
	 * @return the number of children
	 */
/* private int nrChildren (){
		return getNrKin(-1);
	}*/

	// old CountConsanguines-Method, replace!
	/**
	 * counts the consanguines of a vertex (recursive part)
	 * @param ego the ego vertex
	 * @param cons an integer array counting the number of consanguines of given canonic degree
	 * @param up the number of steps in upward direction
	 * @param down the number of steps in downward direction
	 * @param upmax the upper bound for ascending search
	 * @param downmax the lower bound for descending search
	 * @param g the filter criterion for ascendant lines (0 agnatic, 1 uterine, 2 cognatic) 
	 * @see OldIndividual#getNrConsanguines(int, int)
	 */
/*	private void nrConsanguines (int ego, int[] cons, int up, int down, int upmax, int downmax, int g) {	
		int d = Math.max(up,down);
		if (getVisitor()==ego) return;
		if (up>0){
//			if (wrongGender(g)) return;
			if (Mat.noMatch(getGender(),g)) return;
		}
		if (d>0) cons[d]++;
		setVisitor(ego);
		if ((down==0) && (up<upmax)) {
			for (OldIndividual v : getParents()){
				if (v==null) continue;
//				if ((up==0) && (!v.wrongGender(g))) cons[0]++;
				if ((up==0) && (!Mat.noMatch(getGender(),g))) cons[0]++;
				v.nrConsanguines(ego,cons,up+1,down,upmax,downmax,g);
			}
		}
//		if (wrongGender(g)) return;
		if (Mat.noMatch(getGender(),g)) return;
		if (down<downmax) {
			if (isSterile()) return;
			for (OldIndividual v : children()){
				v.nrConsanguines(ego,cons,up,down+1,upmax,downmax,g); 
			}
		}
	}*/
   
	/**
	 * returns the number of parents
	 * @return the number of parents
	 */
/*	public int nrParents () {
		return nrParents(false);
	}*/

	/**
	 * returns the number of parents, with a possible restriction to the same (sub)network
	 * <p>used for calculating orphans or widows at a given period
	 * @param sameNet the condition of parents being in the same (sub)network 
	 * @return the number of parents
	 */
/*	public int nrParents (boolean sameNet) {
		int k=0;
		for (int i=0;i<2;i++) {
			OldIndividual p = getParents().get(i);
			if (p==null) continue;
			if (p.ignore()) continue;
			if (sameNet && !isNetFellow(p)) continue;
			k++;
		}
		return k;
	}*/

	//check utility
	/**
	 * returns 1 if a parent of given gender exists, 0 otherwise
	 * @param g the parent's gender
	 * @return 1 if a parent of given gender exists, 0 otherwise
	 * @see maps.Net#completeness(ArrayList, int), {@link maps.Net#nrArcs(int, int)}
	 */
/*	public int nrParents(int g){
		if (g==-1) return nrParents();
		if (getParents().get(g)!=null) return 1;
		return 0;
	}*/

	/**
	 * returns the number of siblings
	 * @return the number of siblings
	 */
/*	public int nrSiblings () {
		return getSiblings().nodeCount();
	}*/

	/**
	 * returns the number of spouses
	 * @return the number of spouses
	 */
/*	public int nrSpouses () {
		return getNrKin(0); 
	}*/


	
	//function now accomplished by alternative PGraph construction
	/**
	 * returns the list of co-parents and spouses
	 * used for construction of p-graphs and for gedcom exportation
	 * @return partners the list of co-parents and spouses
	 */
/*	public KinGroup partners() {
		KinGroup p = new KinGroup();
		if (!isSingle()) p.addAll(getSpouses());
		if (isSterile()) return p;
		for (OldIndividual c : getChildren()) {
			OldIndividual v = c.getParents().get(alterGender());
			if (v!=null) p.add(v);
		}
		return p;
	}*/
	
	/**
	 * returns the attribute values of a vertex (according to a list of attribute labels) as a single string
	 * @param labels the list of attribute labels
	 * @return the string of attribute values
	 * @see io.write.AbstractWriter#writeTxt(Net, int, String, int)
	 */
/*	public String passport (List<String> labels) {
		return passport(labels, true);
	}*/
	
	/**
	 * returns the attribute values of a vertex (according to a list of attribute labels) as a single string
	 * @param labels the list of attribute labels
	 * @param withNumber true if the identity number of the vertex shall be exported at the beginning of the string
	 * @return the string of attribute values
	 * @see OldIndividual#passport(List)
	 * @see gui.screens.OptionsScreen#optionalAction(int)
	 */
/*	public String passport (List<String> labels, boolean withNumber) {
		String str = "";
		if (withNumber) str = Integer.toString(getID());
		boolean empty = true;
		for (String lb : labels) {
			String clu = "";
			boolean lbalt = lb.charAt(lb.length()-1)=='#';
//			String lb0 = lb.split("\\s")[0];
			try{
//			if (lb0.equals("MARR") || lb0.equals("MARC")) {
				TreeMap<Integer,String> map = (TreeMap<Integer,String>)getCluster(lb,null,0,true);
				clu = "";
				if (map!=null) {
					for (Integer alt : map.keySet()){
						String s = "";
						if (!lbalt) s = map.get(alt)+" ";
						clu = clu+"#"+alt+" "+s;
					}
				}
			} catch (ClassCastException cce) {
				if (!lbalt) clu = (String)getAttributeValue(lb);
			}
//			} else clu = (String)getCluster(lb);
			if (lb.equals("NOTE") && clu!=null) clu = clu.replace("\n"," // "); 
			if (!Trafo.isNull(getAttributeValue(lb))) empty = false;
			else clu="";
			str = str + "\t" + clu;
		}
		if (empty) return ""; 
		return str;      
	}*/

	
	/**
	 * writes the list of relatives into a string
	 * @param i the ungendered kin type (-1 children, 0 spouses, 1 parents)
	 * @return the list of relativs of type i as a string
	 * @see maps.Net#writeRelatives(gui.screens.StartScreen, String)
	 */
/*	public String printRelatives(int i){
		String s = "";
		if (getKin(i)==null) return s;
		for (OldIndividual v : getKin(i)){
			s = s+v.signature()+"; ";
		}
		return s;
	}*/
	
	//Check and comment! and integrate with other signature-methods
	//??durch getValue() ersetzen (aber dzt falscher override in Partition!)
	/**
	 * @return **
	 * @see io.write.AbstractWriter#writePartition(trash.Part)
	 */
/*	public String pSignature () {
		return getGender()+" "+getValue();    
	}*/
	
	/**
	 * ranks children according to age
	 * <p> used for ORD partitions
	 * @param g the vertice's gender required for starting the ranking (necessary to avoid double ranking)
	 */
/*	public void rankChildren (int g) {
		if (Mat.noMatch(getGender(), g)) return;
		KinGroup c = children();
		if (c==null) return;
		c.sort("BIRT Year", true);
		int i=1;
		for (OldIndividual v : c) {
			if (v.getAttributeValue("BIRT Year")==null) v.setCluster(0);
			else v.setCluster(i);
			i++;
		}
	}*/



	/**
	 * cuts all kin ties of the vertex (removing it as a relative from all its relatives)
	 * @see Net#remove(int)
	 */
/*	public void removeAsKin (){
		for (int t=-1;t<2;t++){
			if (getKin(t)==null) continue;
			for (OldIndividual v: getKin(t)) {
				v.removeKin(this,(-1)*t,false);
			}
		}
	}*/
	
	/**
	 * removes a relative of a given kin type
	 * @param v the relative to be removed
	 * @param t the kin type index (-1 = ch, 0 = sp, 1 = f/m)
	 * @param rec the reciprocal character of the operation
	 */
/*	public void removeKin (OldIndividual v, int t, boolean rec){
		if (getKin(t)==null) return;
		if (!getKin(t).remove(v)) return;
		if (rec) v.removeKin(this,i(t),false);
	}*/

/*	private boolean checkKin (Vertex v, int t){
		if (v==null) return false;
		setKin(t);
		return true;
	}*/

	/**
	 * checks whethre a vertex has the same gender as another
	 * @param v the alter vertex
	 * @return true if ego and alter have the same gender
	 * @see chains.Ring#isHetero()
	 */
/*	public boolean sameSex (OldIndividual v) {
		return (getGender() == v.getGender());
	}*/






	/**
	 * gets the numbers of known (agnatic, uterine and cognatic) ascendants in each ascending generation  
	 * @param ascendants the array containing the numbers of known (agnatic, uterine and cognatic) ascendants in each ascending generation
	 * @see maps.Net#setAscendants(int)
	 */
/*	public void setAscendants(int[][] ascendants) {
		this.ascendants = ascendants;
	}*/
	
	/**
	 * adds an attribute to the vertex
	 * @param a the attribute to be added
	 */
/*	public void setAttribute (Attribute a) {
		if (attributes == null) attributes = new AttributeGroup();
		attributes.add(a);
	}*/
		   
	/**
	 * adds a new attribute with given value and label to the vertex
	 * @param s the attribute value
	 * @param lb the attribute label
	 */
/*	public void setAttribute (String s, String lb) {
		int i = AbstractReader.param(lb);
		if (i > 0 && lb.charAt(0)!='#') lb = lb.substring(0,lb.lastIndexOf(" "));
		setAttribute(s,lb,i);
	}*/
	
	/**
	 * adds a new attribute with given value, value position and label to the vertex
	 * @param s the attribute value
	 * @param lb the attribute label
	 * @param i the index of the position of the attribute value (0 quality, 1 place, 2 date, 3 alter)
	 */
/*	public void setAttribute (String s, String lb, int i){
		if (Trafo.isNull(s)) return;
		if (i==-1) return;
		if (lb.equals("NOTE")) s = s.replace(" // ","\n");
//		if (!lb.equals("MARR") || s.indexOf('#')==-1) {
		if (s.indexOf('#')==-1) {
			if (i!=3) setAttribute (s,lb,i,0);
			else setAttribute("",lb,i,Integer.parseInt(s));
			return;
		} 
		for (String str : s.split("\\#")){
			int idx = str.indexOf(" ");
			if (idx==-1) continue;
			setAttribute(str.substring(idx+1),lb,i,Integer.parseInt(str.substring(0,idx)));
		}
	}*/

	/**
	 * adds a new attribute with given value, value position, label and alter to the vertex
	 * @param s the attribute value
	 * @param lb the attribute label
	 * @param i the index of the position of the attribute value (0 quality, 1 place, 2 date, 3 alter)
	 * @param alt the ID number of alter
	 * @see OldIndividual#setAttribute(String, String, int)
	 */
/*	public void setAttribute (String s, String lb, int i, int alt) {
		String v = s.trim();
		if (i==3) v = alt+"";
		int a = attNr(lb,alt);
		if (a==-1) {
			setAttribute(new Attribute(v,lb,i,alt));
			return;
		}
		if (i==3) return;
		attributes.get(a).set(i,v);
	}	*/
	
	/**
	 * adds a new attribute with given label and given values for quality, date, place and alter number (in string form) to the vertex
	 * @param lb the attribute label
	 * @param qu the quality
	 * @param dt the date
	 * @param pl the place
	 * @param nm the alter number
	 * @see io.read.TipReader#updateAttributes(String[])
	 */
/*	public void setAttribute (String lb, String qu, String dt, String pl, String nm) {
		setAttribute(new Attribute(lb,qu,dt,pl,nm));
	}*/
	
	/**
	 * adds a series of attributes with given values, labels and position indices to the vertex
	 * @param s the array of attribute values
	 * @param labels the array of attribute labels
	 * @param param the array of value position indices
	 * @see io.read.TxtReader#updateAttributes(String[])
	 */
/*	public void setAttributes (String[] s, String[] labels, int[] param) {
		for (int i=1;i<s.length;i++) {
			setAttribute(s[i],labels[i],param[i]);
		}
	}*/



	
	/**
	 * a filter for generation assignment, which is postponed when the return is true
	 * @author Klaus
	 * @param nstep the number of the iteration cycle
	 * @param k the kin relation index (-1 child, 0 spouse, 1 parent)
	 * @param v the vertex whose generation shall be assigned
	 * @param constraint the index of the constraint chosen
	 * @return true if generation assignment shall be postponed
	 * @since 10-11-26
	 */
/*	private boolean postponeGenerationAssignment(int nstep,int k, OldIndividual v, int constraint){
		switch (constraint){
			case 0: return true;
			case 1: return testConstraint(nstep,k,v);
		}
		return true;
	}*/
	
	
	/**
	 * sets the generational depth of the neighbors of the vertex
	 * @param m the minimal generational level (as a pseudo-array with only one field)
	 * @param stack the stack of neighbors still to be examined
	 * @since last modification 10-05-14, 10-11-26 KH
	 */
/*	public void setDepth (int[] m, Stack<OldIndividual> stack){
		int constraint = 1; //change this index according to the constraint chosen
		int nstep=0;
		int d = getLevel();
		int[] kn = {0,-1,1};
		for (int k:kn){
			if (getKin(k)==null) continue;
			for (OldIndividual w : getKin(k)){
				if (w==null) continue;
				if (w.getLevel()!=0 || postponeGenerationAssignment(nstep,k,w,constraint)) continue; 
				nstep++;
				int e = d-k;
				w.setLevel(e);
				if (m[0]>e) m[0]=e;
				stack.push(w);
			}
		}
	}*/

	/**
	 * sets the generational depth of the vertex
	 * @param m the minimal generational level (as a pseudo-array with only one field)
	 * @param n the default level
	 */
/*	public void setDepth (int[]m, int n){
		Stack<OldIndividual> stack = new Stack<OldIndividual>();
		setLevel(n);
		stack.push(this);
		while (!stack.isEmpty()){
			stack.pop().setDepth(m, stack);
		}
	}*/
	
	//Preliminary method
	//Check and Simplify
	
	/**
	 * @author Floriana Gargiulo
	 * @param nstep the number of the iteration cycle
	 * @param k the kin relation index (-1 child, 0 spouse, 1 parent)
	 * @param w the alter vertex
	 * @since 10-11-26 FG, modif. 10-11-26 KH
	 */
/*	public boolean testConstraint(int nstep, int k, OldIndividual w){
		//int increase=(int)(nstep/100000);
		//this var increase the level of acceptance on a link according to the time spent in the calculation
		//it's a way to exit sooner the loop. For the moment I don't consider it.
		int increase=0; 
		int a = getLevel();
		int b = w.getLevel();
		int d = 3+increase;
		if (Math.abs(a-b)>d) return false; //maximal distance constraint
		if (k*(a-b)<0) return false;//parents above children constraint
//		if(k==1 && b > a && b-a<-d) return false;
//		if(k==-1 && (b < a || b-a>d))return false;	
//		if(k==0 && (b-a>d ||  b-a<-d)) return false;
		return true;
	}*/
	
	/**
	 * sets the generational depth of the vertex (recursive part)
	 * @param d the provisional generational level
	 * @param i the level difference (0 for horizontal, 1 for ascending direction)
	 * @param k the search direction (0 horizontal, 1 ascending)
	 * @see maps.Net#setGenerations()
	 */
/*	public void setDepth (int d, int i, int k){
		if (getLevel(k)>d-i) return;
		setLevel(d,k);
		if (getKin(k)!=null){
			for (Vertex w : getKin(k)){
				if (w!=null) w.setDepth(d+1,0,k);
			}
		}
		if (!isSingle()) {
			for (Vertex w : getSpouses()){
				w.setDepth(d,1,k);
			}
		}
	}*/

	/**
	 * set the discovery time
	 * <p> used for dfs search methods
	 * @param i the discovery time
	 * @see maps.Net#bicomp(OldIndividual, OldIndividual, Stack, int, int, boolean, boolean)
	 */
/*	public void setDiscovery (int i){
		discovery = i;
	}*/


	
	/**
	 * initializes a kin list of a given type
	 * @param t the kin type index (-1 = ch, 0 = sp, 1 = f/m)
	 */
/*	private void setKin (int t){
		if (relatives==null) relatives = new KinGroup[3];
		if (relatives[t+1]==null) relatives[t+1] = new KinGroup(this,t);
	}*/
	
	/**
	 * sets a relative of a given type
	 * @param v the relative to be set
	 * @param t the kin type index (-1 = ch, 0 = sp, 1 = f/m)
	 * @param i the position of the vertex (for parents: 0 = f, 1 = m)
	 * @param rec the reciprocal character of the operation
	 */
/*	public void setKin (OldIndividual v, int t, int i, boolean rec){
		setKin(t);
		OldIndividual w = getKin(t,i);
		if (w!=null) w.removeKin(this,i(t),false);
		if ((v==null) && (t!=1)) removeKin(w,t,false);
		else getKin(t).set(i,(OldIndividual)v);
		if (v==null) return;
		if (rec) v.addRelative(this,i(t),getGender(),false);
	}*/
	
	/**
	 * set the generational level of the vertex
	 * @param i the level of the vertex
	 * @see maps.Net#setGenerations()
	 */
/*	public void setLevel(int i){
		level = i;
	}*/
	
/*	public void setPLinks(ArrayList<Vertex> pLinks) {
		this.pLinks = pLinks;
	}*/
	
	//Simplify
	/**
	 * sets the generational levels of the vertex (the color field is used for levels in descending direction)
	 * @param i the level of the vertex
	 * @param k the direction (1 ascending, -1 descending)
	 */
/*	public void setLevel(int i, int k){
		if (k==-1) color = i;
		else level = i;
	}*/
	
	/**
	 * adds a relative according with a given gendered kin tie index
	 * @param v the relative to be added
	 * @param i the gendered kin tie index (ch -1, f 0, m 1, sp 2)
	 * @param rec the reciprocal character of the operation
	 */
/*	public void setLink (OldIndividual v, int i, boolean rec) {
		addKin(v,type(i),i,rec);
	}*/
	
	/**
	 * initializes a parent list
	 */
/*	void setParents(){
		setKin(1);
	}*/

	//->P-graph
	/**
	 * adds a link to a vertex in a p-graph
	 * @param c the alter vertex
	 * @see maps.DualNet#setVertex(OldIndividual,OldIndividual)
	 */
/*	public void setPLink (OldIndividual c) {
		if (pLinks==null) pLinks = new ArrayList<OldIndividual>();
		pLinks.add(c);
	}*/


	
	/**
	 * sets the seedReferenceYear of a life event
	 * @param lb the label of the event (e.g. BIRT, DEAT)
	 * @param seedReferenceYear the seedReferenceYear of the event
	 * @see io.read.XmlReader#updateVertex(String[])
	 */
/*	public void setYear (String lb, String year){
		setAttribute(year,lb,2);
	}*/
	

	
	/**
	 * returns a signature in tip format
	 * @param i the export format index (-1 = numbered)
	 * @return the signature in tip format
	 * @see io.write.AbstractWriter#writeTip(Net,int,String,int)
	 */
/*	public String tipSignature (int i) {
		String s = "0\t"+getID()+"\t"+getGender()+"\t";
		if (Trafo.isNull(getLabel())) return s+"/";
		if (i == -1) return s+getGender(6)+" "+getID();
		return s+getLabel();
	}*/
	
	//eventually to be removed
	/**
	 * transfers children to the eldest g-sibling
	 * <p> used for the construction of lineage networks and spiral computation
	 * @param g the gender number of the sibling relation (0 = agnatic, 1 = uterine)
	 * @return true if children have been transferred
	 * @see LigNet#transferChildren(int)
	 */
/*	public boolean transferChildren (Gender g){
		
		if (getGender()==g && !isSterile()){
			Individual e = elder(g);
			if (e == null) return false;
			for (Individual c : children()){
				c.getKin(1).set(g,e);
				e.addKin(c,-1,false);
			}
			relatives[0]=null;
			return true;
			
		}
	}*/
	
	/**
	 * returns the "eldest" sibling of a given gender (passing by parent of the same gender)<p>
	 * an intermediate method for transferring children in lineage networks ("spiral" construction)
	 * @param g the parent's and sibling's gender
	 * @return the "eldest" g-sibling of gender g
	 * @see OldIndividual#transferChildren(int)
	 */
/*	private Individual elder (FiliationType g){
		Individual result = null;
		
		for (Individual s : siblings(g)){
			if (s.getGender().toInt()==g.toInt()) {
				result = s;	
				break;
			}
		}
		
		return result;
	}*/
	

	


	/**
	 * updates a vertex attributes on the model of another attribute
	 * @param b the model attribute
	 * @see Attribute#updateLinkedAttribute(int, Net)
	 */
/*	public void updateAttribute (Attribute b) {
		if (attributes == null) attributes = new AttributeGroup();
		for (Attribute a : attributes) {
			if (a.equals(b)) a.update(b);
			return;
		}
		attributes.add(b);
	}*/




/*	private boolean wrongGender (int g){
		if (g==-1) return false;
		return gender!=g;
	}*/
	
/*	public void dispose(){
		apiCouple = null;
		if(attributes != null){
			attributes.dispose();
			attributes = null;
		}
		if(pLinks != null){
			pLinks.clear();
			pLinks = null;
		}
		if (relatives != null) {
			for(KinGroup k : relatives) if(k != null) k.dispose();
			relatives = null;
		}
		super.dispose();
	}*/
	

}



