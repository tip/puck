package org.tip.puckgui.views;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionCriteria.CumulationType;
import org.tip.puck.partitions.PartitionCriteria.PartitionType;
import org.tip.puck.partitions.PartitionCriteria.SizeFilter;
import org.tip.puck.partitions.PartitionCriteria.ValueFilter;
import org.tip.puck.util.MathUtils;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

public class PartitionCriteriaShortPanel extends JPanel {

	private static final long serialVersionUID = 7924173874220862541L;
	private static final Logger logger = LoggerFactory.getLogger(PartitionCriteriaShortPanel.class);
	private List<String> targetLabels;
	private JTextField txtfldParameter;
	private JTextField txtfldPattern;
	private JTextField txtfldCountedStart;
	private JTextField txtfldCountedCount;
	private JTextField txtfldCountedEnd;
	private JTextField txtfldSteps;
	private JTextField txtfldSizedStart;
	private JTextField txtfldSizedSize;
	private JTextField txtfldSizedEnd;
	private JPanel panelRaw;
	private JPanel panelBinarization;
	private JPanel panelFreeGrouping;
	private JPanel panelCountedGrouping;
	private JPanel panelSizedGrouping;
	private JComboBox comboBoxLabel;
	private JComboBox comboBoxType;
	private JLabel lblnone;
	private JComboBox cmbbxCumulationType;
	private JButton buttonClear;
	private JComboBox cmbbxValueFilter;
	private JComboBox cmbbxSizeFilter;

	/**
	 * Create the panel.
	 */
	public PartitionCriteriaShortPanel(final List<String> labels, final PartitionCriteria defaultCriteria) {
		// Set default labels.
		if (labels == null) {
			this.targetLabels = new ArrayList<String>();
			this.targetLabels.add("");
		} else {
			this.targetLabels = new ArrayList<String>(labels.size() + 1);
			this.targetLabels.add("");
			this.targetLabels.addAll(labels);
		}

		setBorder(null);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		JPanel panel_1 = new JPanel();
		add(panel_1);
		panel_1.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("left:30dlu"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.MIN_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.MIN_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.MIN_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.MIN_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.MIN_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, }));

		this.comboBoxLabel = new JComboBox(this.targetLabels.toArray());
		this.comboBoxLabel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				//
				if (StringUtils.equals((String) PartitionCriteriaShortPanel.this.comboBoxLabel.getSelectedItem(), "PEDG")) {
					PartitionCriteriaShortPanel.this.cmbbxSizeFilter.setSelectedIndex(3);
				}
			}
		});
		panel_1.add(this.comboBoxLabel, "2, 2");
		this.comboBoxLabel.setSelectedIndex(0);
		this.comboBoxLabel.setMaximumRowCount(12);
		this.comboBoxLabel.setEditable(true);

		this.txtfldParameter = new JTextField();
		this.txtfldParameter.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(final FocusEvent e) {
				//
				if ((StringUtils.equals((String) PartitionCriteriaShortPanel.this.comboBoxLabel.getSelectedItem(), "PEDG"))
						&& (NumberUtils.isNumber(PartitionCriteriaShortPanel.this.txtfldParameter.getText()))) {
					String max = String.valueOf((int) Math.pow(2., new Double(PartitionCriteriaShortPanel.this.txtfldParameter.getText())));
					PartitionCriteriaShortPanel.this.txtfldCountedEnd.setText(max);
					PartitionCriteriaShortPanel.this.txtfldSizedEnd.setText(max);
				}
			}
		});
		panel_1.add(this.txtfldParameter, "4, 2");
		this.txtfldParameter.setColumns(10);

		this.comboBoxType = new JComboBox(new Object[] {});
		this.comboBoxType.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent event) {
				//
				if (event.getStateChange() == ItemEvent.SELECTED) {
					PartitionType type;
					switch (PartitionCriteriaShortPanel.this.comboBoxType.getSelectedIndex()) {
						case 0:
							type = PartitionType.RAW;
						break;
						case 1:
							type = PartitionType.BINARIZATION;
						break;
						case 2:
							type = PartitionType.FREE_GROUPING;
						break;
						case 3:
							type = PartitionType.COUNTED_GROUPING;
						break;
						case 4:
							type = PartitionType.SIZED_GROUPING;
						break;
						default:
							type = PartitionType.RAW;
					}
					display(type);
				}
			}
		});
		this.comboBoxType.setModel(new DefaultComboBoxModel(new String[] { "Raw", "Binarization", "Free Grouping", "Counted Grouping", "Sized Grouping" }));
		this.comboBoxType.setSelectedIndex(0);
		this.comboBoxType.setMaximumRowCount(12);
		panel_1.add(this.comboBoxType, "6, 2, fill, default");

		JPanel panel = new JPanel();
		panel_1.add(panel, "8, 2");
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		this.panelRaw = new JPanel();
		panel.add(this.panelRaw);
		this.panelRaw.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		this.lblnone = new JLabel("(none)");
		this.lblnone.setHorizontalAlignment(SwingConstants.CENTER);
		this.panelRaw.add(this.lblnone, "2, 2");

		this.panelBinarization = new JPanel();
		panel.add(this.panelBinarization);
		this.panelBinarization.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		this.txtfldPattern = new JTextField();
		this.panelBinarization.add(this.txtfldPattern, "2, 2, fill, default");
		this.txtfldPattern.setColumns(20);

		JLabel lblPattern = new JLabel("(pattern)");
		this.panelBinarization.add(lblPattern, "4, 2, left, default");

		this.panelFreeGrouping = new JPanel();
		panel.add(this.panelFreeGrouping);
		this.panelFreeGrouping.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.MIN_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, }));

		this.txtfldSteps = new JTextField();
		this.panelFreeGrouping.add(this.txtfldSteps, "2, 2, fill, default");
		this.txtfldSteps.setColumns(20);

		JLabel lblSteps = new JLabel("(steps)");
		this.panelFreeGrouping.add(lblSteps, "4, 2, left, default");

		this.panelCountedGrouping = new JPanel();
		panel.add(this.panelCountedGrouping);
		this.panelCountedGrouping.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		this.txtfldCountedStart = new JTextField();
		this.panelCountedGrouping.add(this.txtfldCountedStart, "2, 2, fill, default");
		this.txtfldCountedStart.setColumns(10);

		this.txtfldCountedCount = new JTextField();
		this.panelCountedGrouping.add(this.txtfldCountedCount, "4, 2, fill, default");
		this.txtfldCountedCount.setColumns(10);

		this.txtfldCountedEnd = new JTextField();
		this.panelCountedGrouping.add(this.txtfldCountedEnd, "6, 2, fill, default");
		this.txtfldCountedEnd.setColumns(10);

		JLabel lblStart = new JLabel("(start, count, end)");
		this.panelCountedGrouping.add(lblStart, "8, 2, right, default");

		this.panelSizedGrouping = new JPanel();
		panel.add(this.panelSizedGrouping);
		this.panelSizedGrouping.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		this.txtfldSizedStart = new JTextField();
		this.panelSizedGrouping.add(this.txtfldSizedStart, "2, 2, fill, default");
		this.txtfldSizedStart.setColumns(10);

		this.txtfldSizedSize = new JTextField();
		this.panelSizedGrouping.add(this.txtfldSizedSize, "4, 2, fill, default");
		this.txtfldSizedSize.setColumns(10);

		this.txtfldSizedEnd = new JTextField();
		this.panelSizedGrouping.add(this.txtfldSizedEnd, "6, 2, fill, default");
		this.txtfldSizedEnd.setColumns(10);

		JLabel lblSize = new JLabel("(start, size, end)");
		this.panelSizedGrouping.add(lblSize, "8, 2, right, default");

		this.cmbbxCumulationType = new JComboBox();
		this.cmbbxCumulationType.setModel(new DefaultComboBoxModel(new String[] { "None", "Asc.", "Desc." }));
		panel_1.add(this.cmbbxCumulationType, "10, 2, fill, default");

		this.cmbbxValueFilter = new JComboBox();
		this.cmbbxValueFilter.setModel(new DefaultComboBoxModel(new String[] { "None", "Null", "Zero" }));
		panel_1.add(this.cmbbxValueFilter, "12, 2, fill, default");

		this.buttonClear = new JButton("");
		this.buttonClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Clear button.
				clear();
			}
		});

		this.cmbbxSizeFilter = new JComboBox();
		this.cmbbxSizeFilter.setModel(new DefaultComboBoxModel(new String[] { "None", "Trim", "Empty", "Holes" }));
		panel_1.add(this.cmbbxSizeFilter, "14, 2, fill, default");
		this.buttonClear.setMaximumSize(new Dimension(18, 18));
		this.buttonClear.setIcon(new ImageIcon(PartitionCriteriaShortPanel.class.getResource("/org/tip/puckgui/images/edit-clear-16x16.png")));
		panel_1.add(this.buttonClear, "16, 2");

		// =========================================
		//
		display(PartitionType.RAW);
		setCriteria(defaultCriteria);
	}

	/**
	 * 
	 */
	public void clear() {

		this.targetLabels.set(0, "");
		this.comboBoxLabel.setModel(new DefaultComboBoxModel(this.targetLabels.toArray()));
		display(PartitionType.RAW);
		this.comboBoxType.setSelectedIndex(0);
		this.txtfldParameter.setText("");
		this.txtfldPattern.setText("");
		this.txtfldSteps.setText("");
		this.txtfldCountedStart.setText("");
		this.txtfldCountedCount.setText("");
		this.txtfldCountedEnd.setText("");
		this.txtfldSizedStart.setText("");
		this.txtfldSizedSize.setText("");
		this.txtfldSizedEnd.setText("");
		this.cmbbxCumulationType.setSelectedIndex(0);
		this.cmbbxValueFilter.setSelectedIndex(0);
		this.cmbbxSizeFilter.setSelectedIndex(0);
	}

	/**
	 * 
	 * @param type
	 */
	public void display(final PartitionType type) {
		//
		this.panelRaw.setVisible(false);
		this.panelBinarization.setVisible(false);
		this.panelFreeGrouping.setVisible(false);
		this.panelCountedGrouping.setVisible(false);
		this.panelSizedGrouping.setVisible(false);

		//
		switch (type) {
			case RAW:
				this.panelRaw.setVisible(true);
			break;

			case BINARIZATION:
				this.panelBinarization.setVisible(true);
			break;

			case FREE_GROUPING:
				this.panelFreeGrouping.setVisible(true);
			break;

			case COUNTED_GROUPING:
				this.panelCountedGrouping.setVisible(true);
			break;

			case SIZED_GROUPING:
				this.panelSizedGrouping.setVisible(true);
			break;
		}
	}

	/**
	 * 
	 * @return
	 */
	public PartitionCriteria getCriteria() {
		PartitionCriteria result;

		//
		String label = (String) this.comboBoxLabel.getSelectedItem();

		//
		if (this.panelRaw.isVisible()) {
			result = PartitionCriteria.createRaw(label, this.txtfldParameter.getText());
		} else if (this.panelBinarization.isVisible()) {
			result = PartitionCriteria.createBinarization(label, this.txtfldParameter.getText(), this.txtfldPattern.getText());
		} else if (this.panelFreeGrouping.isVisible()) {
			result = PartitionCriteria.createGrouping(label, this.txtfldParameter.getText(), this.txtfldSteps.getText());
		} else if (this.panelCountedGrouping.isVisible()) {
			//
			Double start;
			if (NumberUtils.isNumber(this.txtfldCountedStart.getText())) {
				start = Double.parseDouble(this.txtfldCountedStart.getText());
			} else {
				start = null;
			}

			//
			Double count;
			if (NumberUtils.isNumber(this.txtfldCountedCount.getText())) {
				count = Double.parseDouble(this.txtfldCountedCount.getText());
			} else {
				count = null;
			}

			//
			Double end;
			if (NumberUtils.isNumber(this.txtfldCountedEnd.getText())) {
				end = Double.parseDouble(this.txtfldCountedEnd.getText());
			} else {
				end = null;
			}

			//
			result = PartitionCriteria.createCountedGrouping(label, this.txtfldParameter.getText(), start, count, end);
		} else if (this.panelSizedGrouping.isVisible()) {
			//
			Double start;
			if (NumberUtils.isNumber(this.txtfldSizedStart.getText())) {
				start = Double.parseDouble(this.txtfldSizedStart.getText());
			} else {
				start = null;
			}

			//
			Double size;
			if (NumberUtils.isNumber(this.txtfldSizedSize.getText())) {
				size = Double.parseDouble(this.txtfldSizedSize.getText());
			} else {
				size = null;
			}

			//
			Double end;
			if (NumberUtils.isNumber(this.txtfldSizedEnd.getText())) {
				end = Double.parseDouble(this.txtfldSizedEnd.getText());
			} else {
				end = null;
			}

			//
			result = PartitionCriteria.createSizedGrouping(label, this.txtfldParameter.getText(), start, size, end);
		} else {
			result = null;
		}

		//
		if (result != null) {
			//
			switch (this.cmbbxCumulationType.getSelectedIndex()) {
				case 0:
					result.setCumulationType(CumulationType.NONE);
				break;
				case 1:
					result.setCumulationType(CumulationType.ASCENDANT);
				break;
				case 2:
					result.setCumulationType(CumulationType.DESCENDANT);
				break;
				default:
					result.setCumulationType(CumulationType.NONE);
			}

			//
			switch (this.cmbbxValueFilter.getSelectedIndex()) {
				case 0:
					result.setValueFilter(ValueFilter.NONE);
				break;
				case 1:
					result.setValueFilter(ValueFilter.NULL);
				break;
				case 2:
					result.setValueFilter(ValueFilter.ZERO);
				break;
				default:
					result.setValueFilter(ValueFilter.NONE);
			}

			//
			switch (this.cmbbxSizeFilter.getSelectedIndex()) {
				case 0:
					result.setSizeFilter(SizeFilter.NONE);
				break;
				case 1:
					result.setSizeFilter(SizeFilter.TRIM);
				break;
				case 2:
					result.setSizeFilter(SizeFilter.EMPTY);
				break;
				case 3:
					result.setSizeFilter(SizeFilter.HOLES);
				break;
				default:
					result.setSizeFilter(SizeFilter.NONE);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void setCriteria(final PartitionCriteria source) {

		if (source != null) {
			//
			this.comboBoxLabel.setModel(new DefaultComboBoxModel(this.targetLabels.toArray()));
			if (source.getLabel() != null) {
				int index = this.targetLabels.indexOf(source.getLabel());
				if (index != -1) {
					this.comboBoxLabel.setSelectedIndex(index);
				}
			}

			//
			if (source.getLabelParameter() != null) {
				this.txtfldParameter.setText(source.getLabelParameter());
			}

			//
			if (source.getType() != null) {
				switch (source.getType()) {
					case RAW:
						this.comboBoxType.setSelectedIndex(0);
					break;

					case BINARIZATION:
						this.comboBoxType.setSelectedIndex(1);

						if (source.getPattern() != null) {
							this.txtfldPattern.setText(source.getPattern());
						}
					break;

					case FREE_GROUPING:
						this.comboBoxType.setSelectedIndex(2);

						if (source.getIntervals() != null) {
							this.txtfldSteps.setText(source.getIntervals().toBasicStepString());
						}
					break;

					case COUNTED_GROUPING:
						this.comboBoxType.setSelectedIndex(3);

						if (source.getStart() != null) {
							this.txtfldCountedStart.setText(MathUtils.toString(source.getStart()));
						}

						if (source.getCount() != null) {
							this.txtfldCountedCount.setText(MathUtils.toString(source.getCount()));
						}

						if (source.getEnd() != null) {
							this.txtfldCountedEnd.setText(MathUtils.toString(source.getEnd()));
						}
					break;

					case SIZED_GROUPING:
						this.comboBoxType.setSelectedIndex(4);

						if (source.getStart() != null) {
							this.txtfldSizedStart.setText(MathUtils.toString(source.getStart()));
						}

						if (source.getSize() != null) {
							this.txtfldSizedSize.setText(MathUtils.toString(source.getSize()));
						}

						if (source.getEnd() != null) {
							this.txtfldSizedEnd.setText(MathUtils.toString(source.getEnd()));
						}
					break;
				}
			}

			//
			if (source.getCumulationType() != null) {
				switch (source.getCumulationType()) {
					case NONE:
						this.cmbbxCumulationType.setSelectedIndex(0);
					break;
					case ASCENDANT:
						this.cmbbxCumulationType.setSelectedIndex(1);
					break;
					case DESCENDANT:
						this.cmbbxCumulationType.setSelectedIndex(2);
					break;
					default:
						this.cmbbxCumulationType.setSelectedIndex(0);
				}

			}

			//
			if (source.getValueFilter() != null) {
				switch (source.getValueFilter()) {
					case NONE:
						this.cmbbxValueFilter.setSelectedIndex(0);
					break;
					case NULL:
						this.cmbbxValueFilter.setSelectedIndex(1);
					break;
					case ZERO:
						this.cmbbxValueFilter.setSelectedIndex(2);
					break;
					default:
						this.cmbbxValueFilter.setSelectedIndex(0);
				}
			}

			//
			if (source.getSizeFilter() != null) {
				switch (source.getSizeFilter()) {
					case NONE:
						this.cmbbxSizeFilter.setSelectedIndex(0);
					break;
					case TRIM:
						this.cmbbxSizeFilter.setSelectedIndex(1);
					break;
					case EMPTY:
						this.cmbbxSizeFilter.setSelectedIndex(2);
					break;
					case HOLES:
						this.cmbbxSizeFilter.setSelectedIndex(3);
					break;
					default:
						this.cmbbxValueFilter.setSelectedIndex(0);
				}
			}
		}
	}
}
