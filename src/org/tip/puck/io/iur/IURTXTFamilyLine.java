package org.tip.puck.io.iur;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a line of individual from a TXT file.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class IURTXTFamilyLine {
	public static final int NOID = 0;
	public static final char UNMARRIED = 'U';
	public static final char MARRIED = 'M';
	public static final char DIVORCED = 'D';

	private int id;
	private int fatherId;
	private int motherId;
	private char status;
	private String childIds;
	private List<String> attributesValues;

	/**
	 * 
	 */
	public IURTXTFamilyLine() {
		this.id = NOID;
		this.status = UNMARRIED;
		this.fatherId = NOID;
		this.motherId = NOID;
		this.childIds = null;
		this.attributesValues = new ArrayList<String>();
	}

	/**
	 * 
	 * @return
	 */
	public List<String> attributeValues() {
		List<String> result;

		result = this.attributesValues;

		//
		return result;
	}

	public String getChildIds() {
		return childIds;
	}

	public int getFatherId() {
		return fatherId;
	}

	public int getId() {
		return id;
	}

	public int getMotherId() {
		return motherId;
	}

	public char getStatus() {
		return status;
	}

	public void setChildIds(final String childIds) {
		this.childIds = childIds;
	}

	public void setFatherId(final int fatherId) {
		this.fatherId = fatherId;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public void setMotherId(final int motherId) {
		this.motherId = motherId;
	}

	public void setStatus(final char status) {
		this.status = status;
	}
}
