package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import org.tip.puck.net.workers.GeoPlaceNetCriteria;
import org.tip.puck.net.workers.AttributeType;
import org.tip.puck.net.workers.AttributeType.Scope;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class GeoPlaceNetDialog extends JDialog {

	private static final long serialVersionUID = -6151594508277487557L;
	private final JPanel contentPanel = new JPanel();
	private GeoPlaceNetCriteria dialogCriteria;
	private static GeoPlaceNetCriteria lastCriteria = new GeoPlaceNetCriteria();
	private JCheckBox chckbxWeight;
	private JPanel pnlSelections;
	private StringList targetLabels;

	/**
	 * Create the dialog.
	 */
	public GeoPlaceNetDialog(final List<String> relationModelNames) {
		super();

		//
		this.targetLabels = new StringList();
		this.targetLabels.add(AttributeType.Scope.CORPUS.toString());
		this.targetLabels.add(AttributeType.Scope.INDIVIDUALS.toString());
		this.targetLabels.add(AttributeType.Scope.FAMILIES.toString());
		this.targetLabels.add(AttributeType.Scope.ACTORS.toString());
		this.targetLabels.addAll(relationModelNames);

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("GeoPlaces Scope Selector");
		setIconImage(Toolkit.getDefaultToolkit().getImage(GeoPlaceNetDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				GeoPlaceNetDialog.this.dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 420, 278);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));
		{
			JLabel lblNewLabel = new JLabel(
					"<html>\n<p>This menu item will display places from the net<br:>\nnodes (attributes with labels containing \"_PLAC\").</p>\n<p>In order to search for those attributes, please<br/> select the node types to manage.</p>\n<br/>\n</html>");
			this.contentPanel.add(lblNewLabel, "2, 2, 3, 1");
		}
		{
			JLabel lblScopes = new JLabel("Scopes:");
			this.contentPanel.add(lblScopes, "2, 4, right, top");
		}
		{
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			this.contentPanel.add(scrollPane, "4, 4, fill, fill");
			{
				this.pnlSelections = new JPanel();
				scrollPane.setViewportView(this.pnlSelections);
				this.pnlSelections.setLayout(new BoxLayout(this.pnlSelections, BoxLayout.Y_AXIS));
			}
		}
		{
			JLabel lblNewLabel_1 = new JLabel("Weight");
			this.contentPanel.add(lblNewLabel_1, "2, 6");
		}
		{
			this.chckbxWeight = new JCheckBox("");
			this.chckbxWeight.setSelected(true);
			this.contentPanel.add(this.chckbxWeight, "4, 6");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						GeoPlaceNetDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("Launch");
				okButton.addActionListener(new ActionListener() {
					/**
					 * 
					 */
					@Override
					public void actionPerformed(final ActionEvent event) {
						//
						GeoPlaceNetCriteria criteria = getCriteria();
						if (criteria.getScopes().isEmpty()) {
							//
							String title = "Invalid input";
							String message = "Please, select at least one node type.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						} else {
							//
							lastCriteria = criteria;
							GeoPlaceNetDialog.this.dialogCriteria = criteria;

							//
							setVisible(false);
						}

					}
				});
				{
					Component horizontalStrut = Box.createHorizontalStrut(20);
					buttonPane.add(horizontalStrut);
				}
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}

		// ////////////////////////
		setCriteria(lastCriteria);
	}

	/**
	 * 
	 * @return
	 */
	public GeoPlaceNetCriteria getCriteria() {
		GeoPlaceNetCriteria result;

		result = new GeoPlaceNetCriteria();

		//
		result.setWeight(this.chckbxWeight.isSelected());

		result.getScopes().clear();
		for (int index = 0; index < this.pnlSelections.getComponentCount(); index++) {
			//
			Component component = this.pnlSelections.getComponent(index);

			//
			if (((JCheckBox) component).isSelected()) {
				//
				switch (index) {
					case 0:
						result.getScopes().add(new AttributeType(Scope.CORPUS));
					break;

					case 1:
						result.getScopes().add(new AttributeType(Scope.INDIVIDUALS));
					break;

					case 2:
						result.getScopes().add(new AttributeType(Scope.FAMILIES));
					break;

					case 3:
						result.getScopes().add(new AttributeType(Scope.ACTORS));
					break;

					default:
						result.getScopes().add(new AttributeType(((JCheckBox) component).getText()));
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public GeoPlaceNetCriteria getDialogCriteria() {
		GeoPlaceNetCriteria result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final GeoPlaceNetCriteria source) {
		//
		if (source != null) {
			//
			for (String label : this.targetLabels) {
				//
				JCheckBox checkBox = new JCheckBox(label);
				checkBox.setSelected(true);
				this.pnlSelections.add(checkBox);
			}

			//
			this.chckbxWeight.setSelected(source.isWeight());
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		/* AttributeFilter criteria = */showDialog(null);
	}

	/**
	 * Launch the application.
	 */
	public static GeoPlaceNetCriteria showDialog(final List<String> relationModelNames) {
		GeoPlaceNetCriteria result;

		//
		GeoPlaceNetDialog dialog = new GeoPlaceNetDialog(relationModelNames);
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
