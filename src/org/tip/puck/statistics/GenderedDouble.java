package org.tip.puck.statistics;

/**
 * 
 * @author TIP
 */
public class GenderedDouble {

	private double value;
	private double menValue;
	private double womenValue;
	private double unknownValue;

	/**
	 * 
	 */
	public GenderedDouble()
	{
	}

	/**
	 * 
	 */
	public GenderedDouble(final double value, final double menValue, final double womenValue, final double unknownValue)
	{
		set(value, menValue, womenValue, unknownValue);
	}

	public double get() {
		return value;
	}

	public double getMenValue() {
		return menValue;
	}

	public double getUnknownValue() {
		return unknownValue;
	}

	public double getWomenValue() {
		return womenValue;
	}

	public void set(final double value) {
		this.value = value;
	}

	/**
	 * 
	 * @param value
	 * @param menValue
	 * @param womenValue
	 * @param unknownValue
	 * 
	 * @return
	 */
	public GenderedDouble set(final double value, final double menValue, final double womenValue, final double unknownValue)
	{
		GenderedDouble result;

		this.value = value;
		this.menValue = menValue;
		this.womenValue = womenValue;
		this.unknownValue = unknownValue;

		result = this;

		//
		return result;
	}

	public void setMenValue(final double value) {
		this.menValue = value;
	}

	public void setUnknownValue(final double unknownValue) {
		this.unknownValue = unknownValue;
	}

	public void setWomenValue(final double value) {
		this.womenValue = value;
	}
}
