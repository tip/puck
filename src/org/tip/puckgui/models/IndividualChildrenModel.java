package org.tip.puckgui.models;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puckgui.InputSettings.CheckLevel;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.PuckGUI;
import org.tip.puckgui.util.ComboBoxIds;

/**
 * 
 * @author TIP
 */
public class IndividualChildrenModel extends AbstractTableModel {
	private static final long serialVersionUID = -2256548511813504467L;
	private static final Logger logger = LoggerFactory.getLogger(IndividualChildrenModel.class);
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.tip.puckgui.messages");

	private static ImageIcon smallFemaleIcon = new ImageIcon(IndividualChildrenModel.class.getResource("/org/tip/puckgui/images/female-16x16.png"));
	private static ImageIcon smallMaleIcon = new ImageIcon(IndividualChildrenModel.class.getResource("/org/tip/puckgui/images/male-16x16.png"));
	private static ImageIcon smallUnknowIcon = new ImageIcon(IndividualChildrenModel.class.getResource("/org/tip/puckgui/images/unknown-16x16.png"));

	private NetGUI netGUI;
	private Individual source;
	private List<Individual> children;

	/**
	 * 
	 */
	public IndividualChildrenModel(final NetGUI netGUI, final Individual source) {
		super();
		this.netGUI = netGUI;
		this.source = source;
		if (source == null) {
			this.children = new ArrayList<Individual>();
		} else {
			this.children = this.source.children().toListSortedByOrder();
		}
	}

	/**
	 * 
	 * @return
	 */
	public List<Individual> children() {
		List<Individual> result;

		result = this.children;

		//
		return result;
	}

	/**
	 * 
	 */
	public void escapeNewEdition() {
		if (isNewEditionOn()) {
			logger.debug("ESCAPE d");
			int index = this.children.size() - 1;
			this.children.remove(index);
			fireTableRowsDeleted(index, index);
		}
	}

	/**
	 * 
	 */
	@Override
	public Class getColumnClass(final int columnIndex) {
		Class result;

		switch (columnIndex) {
			case 0:
				result = Number.class;
			break;
			case 1:
				result = ImageIcon.class;
			break;
			case 2:
				result = String.class;
			break;
			case 3:
				result = Number.class;
			break;
			case 4:
				result = Number.class;
			break;
			case 5:
				result = String.class;
			break;
			default:
				result = String.class;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getColumnCount() {
		int result;

		result = 6;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String getColumnName(final int columnIndex) {
		String result;

		switch (columnIndex) {
			case 0:
				result = BUNDLE.getString("MainWindow.individual.id");
			break;
			case 1:
				result = BUNDLE.getString("MainWindow.individual.gender");
				;
			break;
			case 2:
				result = BUNDLE.getString("MainWindow.individual.name");
				;
			break;
			case 3:
				result = BUNDLE.getString("MainWindow.individual.position");
				;
			break;
			case 4:
				result = BUNDLE.getString("MainWindow.individual.sibset");
				;
			break;
			case 5:
				result = BUNDLE.getString("MainWindow.individual.otherparent");
				;
			break;
			default:
				result = "";
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getRowCount() {
		int result;

		result = this.children.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individual getSource() {
		Individual result;

		result = this.source;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Object getValueAt(final int rowIndex, final int columnIndex) {
		Object result;

		if (this.source == null) {
			//
			result = null;

		} else if ((isNewEditionOn() && (rowIndex == this.children.size() - 1))) {
			//
			switch (columnIndex) {
				case 3:
					result = this.children.get(rowIndex).getBirthOrder();
				break;

				case 4:
					Family originFamily = this.children.get(rowIndex).getOriginFamily();
					if (originFamily == null) {
						result = null;
					} else {
						result = originFamily.getId();
					}
				break;

				case 5:
					Individual otherParent = this.children.get(rowIndex).getOtherParent(this.source);
					if (otherParent == null) {
						result = null;
					} else {
						result = String.format("%c (%d) %s", otherParent.getGender().toChar(), otherParent.getId(), otherParent.getName());
					}
				break;

				default:
					result = "";
			}
		} else {
			//
			switch (columnIndex) {
				case 0:
					result = this.children.get(rowIndex).getId();
				break;

				case 1:
					switch (this.children.get(rowIndex).getGender()) {
						case FEMALE:
							result = smallFemaleIcon;
						break;
						case MALE:
							result = smallMaleIcon;
						break;
						case UNKNOWN:
							result = smallUnknowIcon;
						break;
						default:
							result = smallUnknowIcon;
					}
				break;

				case 2:
					result = this.children.get(rowIndex).getName();
				break;

				case 3:
					result = this.children.get(rowIndex).getBirthOrder();
				break;

				case 4:
					Family originFamily = this.children.get(rowIndex).getOriginFamily();
					if (originFamily == null) {
						result = null;
					} else {
						result = originFamily.getId();
					}
				break;

				case 5:
					Individual otherParent = this.children.get(rowIndex).getOtherParent(this.source);
					if (otherParent == null) {
						result = null;
					} else {
						result = String.format("%c (%d) %s", otherParent.getGender().toChar(), otherParent.getId(), otherParent.getName());
					}
				break;

				default:
					result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public boolean isCellEditable(final int row, final int col) {
		boolean result;

		switch (col) {
			case 0:
			case 3:
				result = true;
			break;

			default:
				result = false;
		}

		//
		return result;
	}

	/**
	 * This method indicates if a new line is editing.
	 * 
	 * @return
	 */
	public boolean isNewEditionOn() {
		boolean result;

		if (this.source == null) {
			//
			result = false;

		} else if (this.children.size() == this.source.children().size() + 1) {
			//
			result = true;

		} else {
			//
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public void setNewItem(final Family family) {

		Individual newIndividual = new Individual(0, "?", Gender.UNKNOWN);
		newIndividual.setOriginFamily(family);
		this.children.add(newIndividual);
		fireTableDataChanged();
	}

	/**
	 * 
	 * @param source
	 */
	public void setSource(final Individual source) {

		//
		this.source = source;

		//
		if (source == null) {
			//
			this.children = new ArrayList<Individual>();

		} else {
			//
			this.children = this.source.children().toListSortedByOrder();
		}

		//
		fireTableDataChanged();
	}

	/**
	 * 
	 */
	@Override
	public void setValueAt(final Object value, final int rowIndex, final int columnIndex) {
		logger.debug("setValueAt(" + rowIndex + ", " + columnIndex + ", " + value + ")");

		//
		if (this.netGUI != null) {
			//
			if (columnIndex == 0) {
				// In case of new child created with id lesser than
				// the current individual, the current index
				// value becomes wrong. So we need to save the
				// previous one.
				Individual currentIndividual = this.netGUI.selectedIndividual();

				if (isNewEditionOn()) {
					//
					Individual newChild = ComboBoxIds.getOrCreateIndividualFromInput(this.netGUI.getNet(), value, Gender.UNKNOWN);
					Family targetFamily = this.children.get(rowIndex).getOriginFamily();

					if (newChild == null) {
						//
						this.children.remove(rowIndex);
						fireTableDataChanged();

					} else {
						// Check undefinedRoles.
						String errorMessage = controlChildKinError(this.source, newChild);
						if (errorMessage != null) {
							//
							JOptionPane.showMessageDialog(null, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);

						} else {
							// Check warning.
							if (controlChildKinWarningOK(this.source, newChild, targetFamily)) {
								//
								if (targetFamily == null) {
									//
									switch (newChild.numberOfParents()) {
										case 2: {
											//
											this.netGUI.getNet().removeChild(newChild);

											//
											if (NetUtils.isRolesFixedByGender(this.source, null)) {
												//
												this.netGUI.getNet().createFamily(this.source, null, newChild);

											} else {
												//
												this.netGUI.getNet().createFamily((Individual) null, this.source, newChild);
											}
										}
										break;

										case 1: {
											//
											targetFamily = newChild.getOriginFamily();

											//
											if (targetFamily.getFather() == null) {
												//
												targetFamily.setFather(this.source);

											} else {
												//
												targetFamily.setMother(this.source);
											}

											//
											this.source.addPersonalFamily(targetFamily);
										}
										break;

										case 0: {
											if (newChild.getOriginFamily() == null) {
												//
												if (NetUtils.isRolesFixedByGender(this.source, null)) {
													//
													this.netGUI.getNet().createFamily(this.source, null, newChild);

												} else {
													//
													this.netGUI.getNet().createFamily((Individual) null, this.source, newChild);
												}
											} else {
												//
												if (NetUtils.isRolesFixedByGender(this.source, null)) {
													//
													newChild.getOriginFamily().setFather(this.source);

												} else {
													//
													newChild.getOriginFamily().setMother(this.source);
												}
												this.source.addPersonalFamily(newChild.getOriginFamily());
											}
										}
										default:
									}
								} else {
									//
									this.netGUI.getNet().removeChild(newChild);
									this.netGUI.getNet().addChild(targetFamily, newChild);
								}

								//
								this.netGUI.setChanged(true);
								this.netGUI.updateAll();

								// In case of new child created with id
								// lesser
								// than the current individual, the current
								// index value becomes wrong. So we need to
								// set
								// the previous one.
								this.netGUI.selectIndividualsTab(currentIndividual);
							}
						}
					}
				} else {
					//
					Individual oldChild = this.children.get(rowIndex);
					Individual newChild = ComboBoxIds.getIndividualFromInput(this.netGUI.getNet(), value);

					//
					if (newChild == null) {
						//
						this.netGUI.getNet().removeChild(oldChild);
						this.netGUI.setChanged(true);
						setSource(this.source);

					} else if (newChild != oldChild) {
						//
						JOptionPane.showMessageDialog(null, "Operation not permitted.\nPlease, use the family tab to manage family.", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			} else if (columnIndex == 3) {
				//
				Integer newBirthOrderValue;
				if (value instanceof String) {
					//
					if (NumberUtils.isDigits((String) value)) {
						//
						newBirthOrderValue = Integer.parseInt((String) value);

					} else {
						//
						newBirthOrderValue = null;
					}
				} else if (value instanceof Integer) {
					//
					newBirthOrderValue = (Integer) value;

				} else {
					//
					newBirthOrderValue = null;
				}

				//
				if ((newBirthOrderValue == null) || (newBirthOrderValue > 0)) {
					//
					Individual currentIndividual = this.netGUI.selectedIndividual();
					Individual child = this.children.get(rowIndex);

					//
					child.setBirthOrder(newBirthOrderValue);

					//
					this.netGUI.setChanged(true);
					this.netGUI.updateAll();
					this.netGUI.selectIndividualsTab(currentIndividual);
				}
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	public Individual source() {
		Individual result;

		result = this.source;

		//
		return result;
	}

	/**
	 * 
	 * @param level
	 * @param ego
	 * @param child
	 * @return
	 */
	public static String controlChildKinError(final Individual ego, final Individual newChild) {
		String result;

		//
		if (ego == newChild) {
			//
			result = "Same parent child detected.";

		} else if (ego.children().contains(newChild)) {
			//
			result = "Already child detected.";

		} else if ((PuckGUI.instance().getPreferences().getInputSettings().getParentChildMarriages() == CheckLevel.ERROR)
				&& (NetUtils.isParentChildMarriage(ego, newChild))) {
			//
			result = "Parent-child marriages detected.";

		} else {
			//
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param family
	 * @param newChild
	 * @return
	 */
	public static boolean controlChildKinWarningOK(final Individual ego, final Individual newChild, final Family targetFamily) {
		boolean result;

		//
		result = true;

		//
		if ((PuckGUI.instance().getPreferences().getInputSettings().getParentChildMarriages() == CheckLevel.WARNING)
				&& (NetUtils.isParentChildMarriage(ego, newChild))) {
			//
			String buttons[] = { "Continue", "Abort" };
			int response = JOptionPane.showOptionDialog(null, "Parent-child marriages detected.\nThis new child already is married with this individual.",
					"Warning", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null, buttons, buttons[0]);

			if (response == 0) {
				//
				result = true;

			} else {
				//
				result = false;
			}

		}

		//
		if ((result == true) && (targetFamily == null) && (newChild.numberOfParents() != 0)) {
			//
			switch (newChild.numberOfParents()) {
				case 1: {
					//
					String buttons[] = { "Continue", "Abort" };
					int response = JOptionPane.showOptionDialog(null, "This new child already has a single parent family.\nThe action will fill his family.",
							"Warning", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null, buttons, buttons[0]);

					if (response == 0) {
						//
						result = true;

					} else {
						//
						result = false;
					}
				}
				break;

				case 2: {
					//
					String buttons[] = { "Continue", "Abort" };
					int response = JOptionPane.showOptionDialog(null,
							"This new child already has 2 parents family.\nThe action will remove him of this family.", "Warning",
							JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null, buttons, buttons[0]);

					if (response == 0) {
						//
						result = true;

					} else {
						//
						result = false;
					}
				}
				break;

				case 0:
				default:
					result = true;
			}
		}

		//
		if ((result == true) && (targetFamily != null) && (newChild.getOriginFamily() != null)) {
			//
			String buttons[] = { "Continue", "Abort" };
			int response = JOptionPane.showOptionDialog(null, "This new child already has family.\nThe action will remove him of this family.", "Warning",
					JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null, buttons, buttons[0]);

			if (response == 0) {
				//
				result = true;

			} else {
				//
				result = false;
			}
		}

		//
		return result;
	}
}
