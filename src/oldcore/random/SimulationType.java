package oldcore.random;

/**
 * an enumeration of simulation types used for alliance matrix simulation
 * @author Klaus Hamberger
 * @since version 0.95, 11-05-22
 *
 */
public enum SimulationType {
	FREE(0),MOIETIES(1);
	
	/**
	 * the index of the type
	 */
	public final int id;
	
	/**
	 * gets the distribution type with a given index
	 * @param id the index
	 * @return the distribution type
	 * @see gui.screens.MatrixSimulationScreen#dist()
	 */
    public static SimulationType id(final int id) {
        for (SimulationType e : SimulationType.values()) {
            if (e.id == id) {
                return e;
            }
        }
        return null;
    }
	
	/**
	 * constructs a distribution type 
	 * @param id
	 */
	private SimulationType(int id){
		this.id = id;
	}
}


