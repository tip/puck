package org.tip.puck.net.workers;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.workers.AttributeWorker.CaseOption;

/**
 * 
 * @author TIP
 */
public class AttributeRenameCriteria {

	private AttributeWorker.Scope scope;
	private String optionalRelationName;
	private String label;
	private String newLabel;
	private AttributeWorker.CaseOption caseOption;

	/**
	 * 
	 */
	public AttributeRenameCriteria() {
		this.scope = AttributeWorker.Scope.NONE;
		this.optionalRelationName = null;
		this.label = null;
		this.newLabel = null;
		this.caseOption = AttributeWorker.CaseOption.CASE_SENSITIVE;
	}

	public AttributeWorker.CaseOption getCaseOption() {
		return this.caseOption;
	}

	public String getLabel() {
		return this.label;
	}

	public String getNewLabel() {
		return this.newLabel;
	}

	public String getOptionalRelationName() {
		return this.optionalRelationName;
	}

	public AttributeWorker.Scope getScope() {
		return this.scope;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isCaseSensitive() {
		boolean result;

		if (this.caseOption == CaseOption.CASE_SENSITIVE) {
			//
			result = true;

		} else {
			//
			result = false;
		}

		//
		return result;
	}

	public void setCaseOption(final AttributeWorker.CaseOption caseOption) {
		this.caseOption = caseOption;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public void setNewLabel(final String newLabel) {
		this.newLabel = newLabel;
	}

	public void setOptionalRelationName(final String optionalRelationName) {
		this.optionalRelationName = optionalRelationName;
	}

	public void setScope(final AttributeWorker.Scope scope) {
		this.scope = scope;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public static boolean isNotValid(final AttributeRenameCriteria criteria) {
		boolean result;

		result = !isValid(criteria);

		//
		return result;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public static boolean isValid(final AttributeRenameCriteria criteria) {
		boolean result;

		if ((criteria == null) || (criteria.getScope() == null) || (criteria.getScope() == AttributeWorker.Scope.NONE) || (criteria.getLabel() == null)
				|| (StringUtils.isBlank(criteria.getNewLabel()))) {
			//
			result = false;

		} else {
			//
			result = true;
		}

		//
		return result;
	}

}
