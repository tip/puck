package org.tip.puck.io.ods;

import java.io.File;
import java.io.IOException;
import java.io.Writer;

import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang3.math.NumberUtils;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents a translation writer from TXT to XLS.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class ODSWriter extends Writer {

	private static final Logger logger = LoggerFactory.getLogger(ODSWriter.class);

	private File file;
	private SpreadSheet workbook;
	private Sheet currentSheet;
	private int currentSheetIndex;
	private int currentRow;
	private int currentColumn;

	/**
	 * @throws Exception
	 * 
	 */
	public ODSWriter(final File file) throws Exception {

		this.file = file;
		this.workbook = SpreadSheet.createEmpty(new DefaultTableModel());

		this.workbook.getSheet(0).setName("Kinship Relations");
		this.workbook.addSheet("Attributs");

		this.currentSheetIndex = 0;
		this.currentSheet = this.workbook.getSheet(this.currentSheetIndex);
		this.currentRow = 0;
		this.currentColumn = 0;
	}

	/**
	 * 
	 */
	@Override
	public void close() throws IOException {
		this.workbook.saveAs(this.file);
	}

	/**
	 * 
	 */
	@Override
	public void flush() {
	}

	/**
	 * 
	 */
	@Override
	public void write(final char[] cbuf, final int off, final int len) throws IOException {
		write(new String(cbuf, off, len));
	}

	/**
	 * 
	 */
	@Override
	public void write(final String str, final int off, final int len) throws IOException {
		//
		String[] tokens = str.split("\t");

		//
		for (String token : tokens) {
			if (token.contains("\n")) {
				if (this.currentColumn == 0) {
					this.currentSheetIndex += 1;
					if (this.currentSheetIndex < this.workbook.getSheetCount()) {
						this.currentSheet = this.workbook.getSheet(this.currentSheetIndex);
					} else {
						this.currentSheet = null;
					}
					this.currentRow = 0;
					this.currentColumn = 0;
				} else {
					this.currentRow += 1;
					this.currentColumn = 0;
				}
			} else {
				//
				if (this.currentSheet == null) {
					// Create relation sheet with relation model name.
					this.currentSheet = this.workbook.addSheet(token);
				}

				//
				this.currentSheet.ensureColumnCount(this.currentColumn + 1);
				this.currentSheet.ensureRowCount(this.currentRow + 1);
				if (NumberUtils.isNumber(token)) {
					this.currentSheet.getCellAt(this.currentColumn, this.currentRow).setValue(Double.valueOf(token));
				} else {
					//
					this.currentSheet.getCellAt(this.currentColumn, this.currentRow).setValue(token);
				}
				//
				this.currentColumn += 1;
			}
		}
	}
}
