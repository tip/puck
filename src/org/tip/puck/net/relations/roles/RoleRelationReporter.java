package org.tip.puck.net.relations.roles;

import java.util.List;
import java.util.Map;

import org.tip.puck.net.Gender;
import org.tip.puck.net.KinType;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.relations.Roles;
import org.tip.puck.net.relations.roles.RoleRelationWorker.CousinClassification;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Partition;
import org.tip.puck.report.Report;
import org.tip.puck.util.ToolBox;
import org.tip.puck.util.Value;

import fr.devinsy.util.StringList;

public class RoleRelationReporter {

		
	private static Report reportCompositions(final RoleRelationWorker worker) {
		Report result;

		result = new Report("Compositions");

		result.outputs().appendln("Compositions ("+worker.getMaxIterations()+" iterations): Relation Model " + worker.getTitle());
		result.outputs().appendln();
				
		Map<Role,Map<Role,Roles>> compositions = worker.getCompositions();
		
		String headLine = "°";
		for (Role role : compositions.keySet()){
			headLine+="\t"+role;
		}
		result.outputs().appendln(headLine);
		
		for (Role alpha : compositions.keySet()){
			
			String compositionLine = alpha+"\t";
			Map<Role,Roles> map = compositions.get(alpha);
					
			for (Role beta : map.keySet()){
				
				String cell = "";

				for (Role gamma : map.get(beta)){
					
					if (cell.isEmpty()){
						cell=gamma+"";
					} else {
						cell+=";"+gamma;
					}
				}
				compositionLine+=cell+"\t";
			}
			result.outputs().appendln(compositionLine);
		}
		result.outputs().appendln();
		
		Map<KinType,Roles> recursiveRoles = worker.getRecursiveRoles();
		result.outputs().appendln(recursiveRoles.get(KinType.UNKNOWN).size()+" Recursive terms");
		result.outputs().appendln();
		
		result.outputs().appendln(ToolBox.toLine(recursiveRoles.get(KinType.UNKNOWN), "TOTAL",": ",";"));

		for (KinType kinType : KinType.basicTypesWithSiblings()){
			Roles roles = recursiveRoles.get(kinType);
			if (!roles.isEmpty()){
				result.outputs().appendln(ToolBox.toLine(roles, kinType.toString()," ("+roles.size()+"): ",";"));
			}
		}
						
		//
		return result;
	}
	
	private static Report reportReciprocals(final RoleRelationWorker worker) {
		Report result;

		result = new Report("Reciprocals");

		result.outputs().appendln("Reciprocals: Relation Model " + worker.getTitle());
		result.outputs().appendln();
		
		String autoReciprocalLine = null;
		
		Map<Role,Roles> reciprocalRoles = worker.getReciprocalRoles();
		Roles autoReciprocalRoles = worker.getAutoReciprocalRoles();
		
		for (Role role : reciprocalRoles.keySet()){
			
			String reciprocalLine = role+"\t";
					
			for (Role reciprocalRole : reciprocalRoles.get(role)){
				
				if (reciprocalLine.equals(role+"\t")){
					reciprocalLine+=reciprocalRole;
				} else {
					reciprocalLine+=";"+reciprocalRole;
				}
			}
			result.outputs().appendln(reciprocalLine);
			
			if (autoReciprocalRoles.contains(role)){

				if (autoReciprocalLine==null){
					autoReciprocalLine = role+"";
				} else {
					autoReciprocalLine += ";"+role;
				}
			}
		}
		result.outputs().appendln();
		
		if (autoReciprocalRoles.isEmpty()){
			result.outputs().appendln("0 autoreciprocal terms");
		} else {
			result.outputs().appendln(autoReciprocalRoles.size()+" autoreciprocal terms:\t"+autoReciprocalLine);
		}
		//
		return result;
	}
	
	private static Report reportNetwork(final RoleRelationWorker worker) {
		Report result;

		result = new Report("Network");

		result.outputs().appendln("Kin Term Network Analysis: Relation Model " + worker.getTitle());
		result.outputs().appendln(worker.getRules());
		result.outputs().appendln();
		result.outputs().appendln(worker.getNetworkAnalysis());

		//
		return result;
	}
		


	private static Report reportBasicCompositions(final RoleRelationWorker worker) {
		Report result;

		result = new Report("Basic Compositions");

		result.outputs().appendln("Compositions: Relation Model " + worker.getTitle());
		result.outputs().appendln();
		
		Roles metaRoles = worker.getGenerators();
		String headLine = "SELF\t";
		for (Role role : metaRoles.toSortedList()){
			if (!role.getName().equals("SELF")){
				headLine += role+"\t";
			}
		}
		result.outputs().appendln(headLine);
		
		Map<Role,Partition<Role>> basicCompositions = worker.getBasicCompositions();
		
		for (Role self : basicCompositions.keySet()){
			
			String roleLine = self+"\t";
			Partition<Role> partition = basicCompositions.get(self);
			
			for (Role role : metaRoles.toSortedList()){
				if (!((MetaRole)role).isSelf()){
					Cluster<Role> cluster = partition.getCluster(new Value(role));
					if (cluster != null){
						roleLine += cluster.getItemsAsSortedString()+"\t";
					} else {
						roleLine += "\t";
					}
				}
			}
			//
			result.outputs().appendln(roleLine);
		}
		
		//
		return result;
	}
	

	private static Report reportDefinitions(final RoleRelationWorker worker) {
		Report result;

		result = new Report("Definitions");

		result.outputs().appendln("Relation Model " + worker.getTitle());
		result.outputs().appendln();
		
		Map<Role,RoleDefinitions> roleDefininitionsByRole = worker.getRoleDefininitionsByRole();

		for (Role role : roleDefininitionsByRole.keySet()){
			
			result.outputs().appendln(role);
			
			for (RoleDefinition definition : roleDefininitionsByRole.get(role)) { 
				
				String emic = "";
				List<Role> emicList = worker.getEmicGenerator(definition).toSortedList();
				if (!emicList.isEmpty()){
					emic = emicList.toString();
				}
				
				result.outputs().appendln("\t"+definition.toString()+"\t"+emic);
			}
		}
		//
		return result;
	}

	public static Report reportRoleRelations(final RelationModel model, final int maxIterations) {
		Report result;
		
		String title = model.getName();

		result = new Report("Terminology " + title);
		
		RoleRelationWorker worker = new RoleRelationWorker(model,maxIterations);

		result.outputs().append(reportBasicStatistics(worker));
		result.outputs().append(reportDetailedStatistics(worker));
		result.outputs().append(reportPositions(worker));
		result.outputs().append(reportDefinitions(worker));
		result.outputs().append(reportBasicCompositions(worker));
		result.outputs().append(reportCompositions(worker));
		result.outputs().append(reportReciprocals(worker));
		result.outputs().append(reportNetwork(worker));

		//
		return result;
	}


	private static Report reportPositions(final RoleRelationWorker worker){
		Report result;
		
		result = new Report("Positions");

		result.outputs().appendln("Positions ("+worker.getMaxIterations()+" iterations): Relation Model " + worker.getTitle());
		result.outputs().appendln();
		
		Map<Role,StringList> positions = worker.getPositions();
				
		for (Role role : positions.keySet()){
			
			String positionString = role.toString();
			
			for (String position : positions.get(role)){
				
				if (positionString.equals(role.toString())){
					positionString +="\t"+position;
				} else {
					positionString += ";"+position;
				}
			}
			//
			result.outputs().appendln(positionString);
		}
		
		//
		return result;

	}
	
	private static Report reportBasicStatistics(final RoleRelationWorker worker) {
		Report result;
		
		result = new Report("Overview");

		result.outputs().appendln("Relation Model " + worker.getTitle());
		result.outputs().appendln();
		result.outputs().appendln("Self term = "+worker.getRelations().getSelfName());
		result.outputs().appendln();
		result.outputs().appendln("Gender pattern:");
		result.outputs().appendln("Terms:\t" + worker.getTerms().size());
		result.outputs().appendln("Male\t" + worker.getTermsByGender(Gender.MALE).size() +"\t("+worker.getTermsByExclusiveGender(Gender.MALE).size()+" excl.)");
		result.outputs().appendln("Female:\t" + worker.getTermsByGender(Gender.FEMALE).size() +"\t("+worker.getTermsByExclusiveGender(Gender.FEMALE).size()+" excl.)");
		result.outputs().appendln("Male Speaker:\t" + worker.getTermsByEgoGender(Gender.MALE).size() +"\t("+worker.getTermsByExclusiveEgoGender(Gender.MALE).size()+" excl.)");
		result.outputs().appendln("Female Speaker:\t" + worker.getTermsByEgoGender(Gender.FEMALE).size() +"\t("+worker.getTermsByExclusiveEgoGender(Gender.FEMALE).size()+" excl.)");
		result.outputs().appendln();
		result.outputs().appendln("Generation pattern (+|0|-):");
		result.outputs().appendln("Total:\t" + worker.getGenerationPatterns()[3]);
		result.outputs().appendln("Male:\t" + worker.getGenerationPatterns()[0]);
		result.outputs().appendln("Female:\t" + worker.getGenerationPatterns()[1]);
		result.outputs().appendln();
		result.outputs().appendln("Composition structure:");
		result.outputs().appendln("Autoreciprocal Terms:\t"+worker.getAutoReciprocalRoles().size());
		result.outputs().appendln("Recursive Terms:\t"+worker.getRecursiveRolePattern());
		result.outputs().appendln();
		result.outputs().appendln("Typology:");
		result.outputs().appendln("Ascendant classification:\t" + worker.getCollateralClassification()[0]);
		result.outputs().appendln("Cousin classification:\t" + worker.getCollateralClassification()[1]);
		result.outputs().appendln();
		result.outputs().appendln("Kin Term Network (m.s. / f.s.):");
		result.outputs().appendln("Density:\t" + worker.getGraphStatistics(Gender.MALE,"DENSITY")+"\t"+worker.getGraphStatistics(Gender.FEMALE,"DENSITY"));
		result.outputs().appendln("Mean Degree:\t" + worker.getGraphStatistics(Gender.MALE,"MEANDEGREE")+"\t"+worker.getGraphStatistics(Gender.FEMALE,"MEANDEGREE"));
		result.outputs().appendln("Mean Clustering Coefficient:\t" + worker.getGraphStatistics(Gender.MALE,"MEANCLUSTERINGCOEFF")+"\t"+worker.getGraphStatistics(Gender.FEMALE,"MEANCLUSTERINGCOEFF"));
		result.outputs().appendln("Components:\t" + worker.getGraphStatistics(Gender.MALE,"NRCOMPONENTS")+"\t"+worker.getGraphStatistics(Gender.FEMALE,"NRCOMPONENTS"));
		result.outputs().appendln("Concentration:\t" + worker.getGraphStatistics(Gender.MALE,"CONCENTRATION")+"\t"+worker.getGraphStatistics(Gender.FEMALE,"CONCENTRATION"));
		
		//
		return result;
	}
		


	private static Report reportDetailedStatistics(final RoleRelationWorker worker) {
		Report result;
		
		result = new Report("Details");

		result.outputs().appendln("Relation Model " + worker.getTitle());
		result.outputs().appendln();
		result.outputs().appendln("Self term = "+worker.getRelations().getSelfName());
		result.outputs().appendln();

		result.outputs().appendln("Terms:\t" + worker.getTerms().size()+"\t"+worker.getTerms());
		
		for (Gender gender : Gender.valuesNotUnknown()){
			Roles terms = worker.getTermsByEgoGender(gender);
			Roles exclusiveTerms = worker.getTermsByExclusiveEgoGender(gender);
			result.outputs().appendln("Terms for "+gender+" speaker:\t" + terms.size()+"\t"+terms.toSortedList());
			result.outputs().appendln("Terms exclusive for "+gender+" speaker:\t" + exclusiveTerms.size()+"\t"+exclusiveTerms.toSortedList());
		}
		
		for (Gender gender : Gender.valuesNotUnknown()){
			Roles terms = worker.getTermsByGender(gender);
			Roles exclusiveTerms = worker.getTermsByExclusiveGender(gender);
			result.outputs().appendln(gender+" Terms:\t" + terms.size()+"\t"+terms.toSortedList());
			result.outputs().appendln("Exclusively "+gender+" terms:\t" + exclusiveTerms.size()+"\t"+exclusiveTerms.toSortedList());
		}
		
		Map<Role,List<Integer>> generations = worker.getGenerations();

		result.outputs().appendln();
		result.outputs().appendln("Gender and Generation: ");
		result.outputs().appendln("Generation\tMale\tFemale\tUnkown\tTotal");
		
		for (Integer generation : worker.getGenerationLevels()){
			String line = generation+"";
			for (Integer byGender : worker.getTermCountsByGenerationAndGender().get(generation)){
				line += "\t"+byGender;
			}
			result.outputs().appendln(line);
		}
		
		result.outputs().appendln();
		result.outputs().appendln("Term\tEgo Gender\tAlter Gender\tGenerations");

		for (Role role : worker.getTerms().toSortedList()) {
			result.outputs().appendln(
					role.getName() + "\t" + worker.getEgoGender(role)+"\t"+worker.getGender(role)+"\t"+generations.get(role));
		}
		
		result.outputs().appendln();
		result.outputs().appendln(reportCousinTerms(worker));
		
		//
		return result;
	}
	
	private static StringList reportCousinTerms (RoleRelationWorker worker){
		StringList result;
		
		result = new StringList();

		Map<MetaRole,CousinClassification>[] cousinClassifications = worker.getCollateralClassifications();
		Map<MetaRole,List<List<Role>>>[] cousinTerms = worker.getCollateralTerms();
		
		String[] firstHeadLine = new String[]{"Ascendant Classification","Cousin Classification"};
		String[] secondHeadLine = new String[]{"Parents\tParallel Ascendants\tCross Ascendants","Siblings\tParallel Cousins\tCross Cousins"};
		
		for (int i=0;i<2;i++){
			
			result.appendln();
			result.appendln(firstHeadLine[i]+"\t"+worker.getCollateralClassification()[i]);
			result.appendln();
			result.appendln("Config\tType\t"+secondHeadLine[i]);
			
			for (MetaRole config: cousinTerms[i].keySet()){
						
				List<List<Role>> terms = cousinTerms[i].get(config);
				
				result.appendln(config+"\t"+cousinClassifications[i].get(config)+"\t"+terms.get(0)+"\t"+terms.get(1)+"\t"+terms.get(2));
			}
		}
		//
		return result;
	}
	

	

}
