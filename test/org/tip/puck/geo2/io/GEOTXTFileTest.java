package org.tip.puck.geo2.io;

import java.io.File;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.geo.Geography;
import org.tip.puck.geo.Place;
import org.tip.puck.geo.Places;
import org.tip.puck.geo.io.GEOTXTFile;

/**
 * 
 * @author TIP
 */
public class GEOTXTFileTest {
	private static final Logger logger = LoggerFactory.getLogger(GEOTXTFileTest.class);

	/**
     */
	@Test
	public void testLoad01() throws Exception {

		logger.debug("========================== testLoad01");
		Geography source = GEOTXTFile.load(new File("test/org/tip/puck/geo2/io/SimpleTest.geo.csv"));

		Assertions.assertThat(source).isNotNull();

		Places places = source.getPlaces();
		Assertions.assertThat(places.isEmpty()).isFalse();
		Assertions.assertThat(places.getByToponym("Massy")).isNotNull();
	}

	/**
     */
	@Test
	public void testSave01() throws Exception {

		logger.debug("========================== testSave01");

		Geography source = new Geography();

		{
			Place place = new Place("Paris");
			place.getHomonyms().add("Lutèce");
			place.getHomonyms().add("Lutecia");
			place.setGeoLevel(GeoLevel.TOWN);

			source.addPlace(place);
		}

		{
			Place place = new Place("Massy");
			place.setGeoLevel(GeoLevel.TOWN);
			place.setLatitude(48.730946);
			place.setLongitude(2.271316);
			place.setElevation(100);
			place.setComment("91300");

			source.addPlace(place);
		}

		File target = File.createTempFile("test", "csv");

		GEOTXTFile.save(target, source);

		Assertions.assertThat(source).isNotNull();
	}
}
