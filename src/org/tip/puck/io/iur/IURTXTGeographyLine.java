package org.tip.puck.io.iur;


public class IURTXTGeographyLine {
	
	public static final int NOID = 0;

	private int id;
	private String name;
	private String countryCode;
	private String longitude;
	private String latitude;

	/**
	 * 
	 */
	public IURTXTGeographyLine() {
		this.id = NOID;
		this.name = null;
		
	}

	public int id() {
		return id;
	}
	

	public String getName() {
		return name;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String coutnryCode) {
		this.countryCode = coutnryCode;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}



}
