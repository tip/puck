package org.tip.puck.net;

import java.util.ArrayList;
import java.util.List;

public enum AlterAge {

	ELDER('e'),
	YOUNGER('y'),
	UNKNOWN(' ');

	private char letter;
	
	private AlterAge(char letter){
		this.letter = letter;
	}
	
	public char toLetter(){
		return letter;
	}
	
	public AlterAge invert() {
		AlterAge result;

		if (this == ELDER) {
			result = YOUNGER;
		} else if (this == YOUNGER) {
			result = ELDER;
		} else {
			result = this;
		}
		//
		return result;
	}
	
	public static AlterAge invert(final AlterAge age){
		AlterAge result;
		
		if (age == null){
			result = null;
		} else {
			result = age.invert();
		}
		//
		return result;
	}
	
	/**
	 * 
	 * @param gender
	 * @return
	 */
	public boolean specifies(final AlterAge age) {
		boolean result;

		if ((age == UNKNOWN) || (this == age)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @param age
	 * @return
	 */
	public boolean matchs(final AlterAge age) {
		boolean result;

		if ((age == null) || (this == UNKNOWN) || (age == UNKNOWN) || (this == age)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}


	public boolean isUnknown(){
		return this == UNKNOWN;
	}
	
	public boolean isElder(){
		return this == ELDER;
	}
	
	public boolean isYounger(){
		return this == YOUNGER;
	}
	
	public static AlterAge valueOfLetter(char item){
		AlterAge result;
		
		result = null;

		for (AlterAge age : values()){
			if (age.toLetter()==item){
				result = age;
				break;
			}
		}
		
		//
		return result;
	}
	
	public static List<AlterAge> valuesNotUnknown(){
		List<AlterAge> result;
		
		result = new ArrayList<AlterAge>();
		
		for (AlterAge AlterAge : values()){
			if (!AlterAge.isUnknown()){
				result.add(AlterAge);
			}
		}
		//
		return result;
	}
	
	
	public static List<AlterAge> values(boolean unknownOnly){
		List<AlterAge> result;
		
		if (unknownOnly){
			result = new ArrayList<AlterAge>();
			result.add(UNKNOWN);
		} else {
			result = valuesNotUnknown();
		}
		//
		return result;
	}
	
	/**
	 * 
	 * @param gender
	 * @return
	 */
	public boolean matches(final AlterAge age) {
		boolean result;

		if ((age == null) || (this == UNKNOWN) || (age == UNKNOWN) || (this == age)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}
	
	public String toStringNoUnknown(){
		String result;
		
		if (isUnknown()){
			result = "";
		} else {
			result = toString();
		}
		//
		return result;
	}



}
