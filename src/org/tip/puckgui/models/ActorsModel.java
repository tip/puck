package org.tip.puckgui.models;

import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Actors;
import org.tip.puck.net.relations.Relation;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.util.ComboBoxIds;

/**
 * 
 * @author TIP
 */
public class ActorsModel extends AbstractTableModel {

	private static final long serialVersionUID = -6804500228130775932L;
	private static final Logger logger = LoggerFactory.getLogger(ActorsModel.class);
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.tip.puckgui.messages");

	private NetGUI netGUI;
	private Actors source;
	private Actors delegate;
	private Relation relation;

	/**
	 * 
	 */
	public ActorsModel(final Actors source, final NetGUI netGUI, final Relation selectedRelation) {
		super();
		setSource(source, netGUI, selectedRelation);
	}

	/**
	 * 
	 */
	@Override
	public Class getColumnClass(final int columnIndex) {
		Class result;

		if (this.source == null) {
			//
			result = String.class;

		} else {
			//
			switch (columnIndex) {
				case 0:
					result = String.class;
				break;
				case 1:
					result = Number.class;
				break;
				case 2:
					result = String.class;
				break;
				case 3:
					result = Number.class;
				break;
				default:
					result = String.class;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getColumnCount() {
		int result;

		result = 4;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String getColumnName(final int columnIndex) {
		String result;

		switch (columnIndex) {
			case 0:
				result = "Role";
			break;
			case 1:
				result = "Id";
			break;
			case 2:
				result = "Name";
			break;
			case 3:
				result = "Order";
			break;
			default:
				result = "";
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getRowCount() {
		int result;

		result = this.delegate.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Actors getSource() {
		Actors result;

		result = this.source;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Actors getTarget() {
		Actors result;

		result = this.delegate;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Object getValueAt(final int rowIndex, final int columnIndex) {
		Object result;

		if (this.delegate == null) {
			//
			result = null;

		} else if (this.delegate.get(rowIndex) == null) {
			//
			result = "";

		} else {
			//
			switch (columnIndex) {
				case 0:
					result = this.delegate.get(rowIndex).getRole().getName();
				break;
				case 1:
					result = this.delegate.get(rowIndex).getIndividual().getId();
				break;
				case 2:
					result = this.delegate.get(rowIndex).getIndividual().getName();
				break;
				case 3:
					result = this.delegate.get(rowIndex).getRelationOrder();
				break;
				default:
					result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public boolean isCellEditable(final int rowIndex, final int columnIndex) {
		boolean result;

		switch (columnIndex) {
			case 0:
			case 1:
			case 3:
				result = true;
			break;

			default:
				result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param guiManager
	 */
	public void setNetGUI(final NetGUI guiManager) {
		this.netGUI = guiManager;
	}

	/**
	 * @throws PuckException
	 * 
	 */
	public void setNewItem() throws PuckException {
		//
		int individualId = this.netGUI.getCurrentIndividuals().getFirstId();

		//
		this.netGUI.getNet().createRelationActorForced(this.relation, this.netGUI.getCurrentIndividuals().getFirstId(),
				this.relation.getModel().roles().get(0).getName());
		this.netGUI.updateIndividualIdentity();
		this.netGUI.updateRelationIdentity(this.relation.getModel());
		// fireTableDataChanged();
	}

	/**
	 * 
	 * @param source
	 */
	public void setSource(final Actors source, final NetGUI netGUI, final Relation selectedRelation) {
		//
		this.source = source;
		if (source == null) {
			//
			this.delegate = new Actors();

		} else {
			//
			this.delegate = source;
		}

		//
		this.netGUI = netGUI;
		this.relation = selectedRelation;

		//
		fireTableDataChanged();
	}

	/**
	 * 
	 */
	@Override
	public void setValueAt(final Object value, final int rowIndex, final int columnIndex) {
		logger.debug("setValueAt " + rowIndex + " " + columnIndex + " " + value);

		//
		Actor actor = this.delegate.get(rowIndex);
		if (columnIndex == 0) {
			//
			actor.setRole(this.relation.getModel().roles().getByName((String) value));

			//
			this.netGUI.setChanged(true);
			this.netGUI.updateAll();

		} else if (columnIndex == 1) {
			//
			if (this.netGUI != null) {
				//
				Individual newIndividual = ComboBoxIds.getOrCreateIndividualFromInput(this.netGUI.getNet(), value, Gender.UNKNOWN);

				if (newIndividual == null) {
					//
					this.netGUI.getNet().removeRelationActor(this.relation, actor);

					//
					this.netGUI.setChanged(true);
					this.netGUI.updateAll();

				} else if (newIndividual != actor.getIndividual()) {
					//
					this.netGUI.getNet().replaceIndividualInActor(this.relation, actor, newIndividual);
					actor.setIndividual(newIndividual);

					//
					this.netGUI.setChanged(true);
					this.netGUI.updateAll();
				}
			}
		} else if (columnIndex == 3) {

			// Extract an integer value.
			Integer targetValue;
			if (value instanceof Integer) {

				targetValue = (Integer) value;

			} else if (value instanceof String) {

				if (StringUtils.isBlank((String) value)) {

					targetValue = null;

				} else if (NumberUtils.isDigits(((String) value).trim())) {

					targetValue = Integer.valueOf((String) value);

				} else {

					targetValue = null;
				}
			} else {

				targetValue = null;
			}

			//
			actor.setRelationOrder(targetValue);

			//
			this.netGUI.setChanged(true);
			this.netGUI.updateAll();
		}
	}
}
