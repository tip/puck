package org.tip.puck.net.workers;

import java.util.ArrayList;
import java.util.Collections;

import org.tip.puck.net.workers.AttributeTypeComparator.Sorting;

/**
 * 
 * @author TIP
 */
public class AttributeTypes extends ArrayList<AttributeType> {

	private static final long serialVersionUID = -3364060526230305991L;

	/**
	 * 
	 */
	public AttributeTypes() {
		super();
	}

	/**
	 * 
	 */
	public AttributeTypes(final AttributeTypes source) {
		super(source);
	}

	/**
	 * 
	 * @return
	 */
	public void sorted(final Sorting sorting) {
		//
		Collections.sort(this, new AttributeTypeComparator(sorting));
	}

	/**
	 * 
	 */
	@Override
	public AttributeType[] toArray() {
		AttributeType[] result;

		result = new AttributeType[this.size()];
		int count = 0;
		for (AttributeType node : this) {
			//
			result[count] = node;
			count += 1;
		}

		//
		return result;
	}
}
