package org.tip.puck.net.random;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.tip.puck.mas.MAS;
import org.tip.puck.mas.MASConfig;
import org.tip.puck.net.Families;
import org.tip.puck.net.Family;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.util.Distributions;
import org.tip.puck.util.RandomUtils;
import org.tip.puckgui.views.RandomCorpusCriteria;

public class RandomNetMaker {
	
	private MAS mas;
	private MASConfig config;
	
	Random randGen;
	
	int initSize;
	int years;
	int maxAge;
	int minAge;
	double meanAgeDifference;
	double stdevAgeDifference;
	double marriageRate;
	double fertilityRate;
	double divorceRate;
	double polygamyRateMen;
	double polygamyRateWomen;
	
    private int samples;
    private Individual[] wives;
    private Individual[] husbands;
    private double[] weights;
    
    FiliationType cousinPreferenceType;
	double cousinPreferenceWeight;
	
	Map<Integer,List<Integer>> weightIndexMap;
	double totalWeight;
	
	Families fertileFamilies;
	List<Individual> disposibleMen; 
	List<Individual> disposibleWomen;
	Individuals children;
	Individuals adults;
	
	Map<Integer,Individuals> preferences;
	Map<Integer,Individuals> avoidances;

	
	public RandomNetMaker (RandomCorpusCriteria criteria){
		
		if (criteria.isMas()){
			
			config = new MASConfig();
			config.fromString(criteria.toMASConfig());
//	        config.fromFile("experiments/mas/example.txt");
			mas = config.getMas();
			
		} else {
			
			initSize = criteria.getInitialPopulation();
			years = criteria.getYear();
			maxAge = criteria.getMaxAge();
			minAge = criteria.getMinAge();
			meanAgeDifference = criteria.getMeanAgeDifference();
			stdevAgeDifference = criteria.getStdevAgeDifference();
			marriageRate = criteria.getMarriageRate();
			fertilityRate = criteria.getFertilityRate();
			divorceRate = criteria.getDivorceRate();
			polygamyRateMen = criteria.getPolygamyRateMen();
			polygamyRateWomen = criteria.getPolygamyRateWomen();

			cousinPreferenceType = criteria.getCousinPreferenceType();
			cousinPreferenceWeight = criteria.getCousinPreferenceWeight();

			randGen = new Random();
			
			fertileFamilies = new Families();
			disposibleMen = new ArrayList<Individual>();
			disposibleWomen = new ArrayList<Individual>();
			children = new Individuals();
			adults = new Individuals();
			
			preferences = new HashMap<Integer,Individuals>();
			avoidances = new HashMap<Integer,Individuals>();
			
		}
	}
	
	
	public MASConfig getConfig() {
		return config;
	}


	public Net createRandomMASNet() {
		Net result;
		
		getMas().run();
		result = getMas().toNet();
		
		//
		return result;
	}
	
	public static int getAge(Individual individual, int year){
		return year-Integer.parseInt(individual.getAttributeValue("BIRT"));
	}
	
	public void getWeights (int n, int year){
		
		weightIndexMap = new HashMap<Integer,List<Integer>>();
		
		totalWeight = 0.;
		
		husbands = new Individual[n];
		wives = new Individual[n];
		weights = new double[n];
		
		int i = 0;
        for (Individual husband : disposibleMen) {	
        	List<Integer> husbandIndexList = new ArrayList<Integer>();
        	weightIndexMap.put(husband.getId(), husbandIndexList);
        	for (Individual wife : disposibleWomen){
        		List<Integer> wifeIndexList = weightIndexMap.get(wife.getId());
        		if (wifeIndexList == null){
        			wifeIndexList = new ArrayList<Integer>();
                	weightIndexMap.put(wife.getId(), wifeIndexList);
        		}
        		
        		husbandIndexList.add(i);
        		wifeIndexList.add(i);
        		
				double weight;
				
				if (avoidances.get(husband.getId()) != null && avoidances.get(husband.getId()).contains(wife)){
					weight = 0;
				} else if (preferences.get(husband.getId()) != null && preferences.get(husband.getId()).contains(wife) && husband.isSingle() && wife.isSingle()){
					weight = cousinPreferenceWeight;
				} else {
					weight = 1;
				}
				
				int ageDiff = getAge(husband,year)-getAge(wife,year);
				double ageDiffFactor = Distributions.normal(ageDiff, meanAgeDifference, stdevAgeDifference);
				
				weight = weight*ageDiffFactor;
				
	            totalWeight += weight;
	                
	            wives[i] = wife;
	            husbands[i] = husband;
	            weights[i] = weight;
	            
	            i++;
        	}
        }
	}
	
	/**
	 * @return
	 */
	public void marriage (Families families, int year, int n){
		
		if (totalWeight<=0) {
			return;
		}
        // random position in ]0, totalWeight]
        double pos = 0.;
        while (pos == 0) {
        	pos = randGen.nextDouble() * totalWeight;
        }
            
        double sum = 0;
        for (int i = 0; i < n; i++) {
            sum += weights[i];
            if (sum > pos) {
            	Individual husband = husbands[i];
            	Individual wife = wives[i];
            	if (husband!=null && wife!=null){
                	Family family = new Family(families.size()+1,husband,wife);
//                	System.out.println(husband+" "+weightIndexMap.get(husband.getId()).toString()+" "+getAge(husband,seedReferenceYear)+" "+pos+" "+sum+" "+totalWeight);
//                	System.out.println(wife+" "+weightIndexMap.get(wife.getId()).toString()+" "+getAge(wife,seedReferenceYear)+" "+pos+" "+sum+" "+totalWeight);
                	if (preferences.get(husband.getId()).contains(wife)){
                		family.setAttribute("PREFERENCE", "true");
                	}
    				husband.addPersonalFamily(family);
    				wife.addPersonalFamily(family);
    				family.setMarried(true);
    				families.put(family);
    				if (getAge(husband, year) <= maxAge && getAge(wife, year) <= maxAge){
    					fertileFamilies.add(family);
    				}
    				if (polygamyRateMen==0 || !RandomUtils.event(polygamyRateMen,randGen)){
        				disposibleMen.remove(husband);
    				}
    				if (polygamyRateWomen==0 || !RandomUtils.event(polygamyRateWomen,randGen)){
        				disposibleWomen.remove(wife);
    				}
    				
    				//AdjustWeights
    				double indexSum = 0.;
    				for (int husbandIndex : weightIndexMap.get(husband.getId())){
    					indexSum += weights[husbandIndex];
    					weights[husbandIndex] = 0;
    				}
    				for (int wifeIndex : weightIndexMap.get(wife.getId())){
    					indexSum += weights[wifeIndex];
    					weights[wifeIndex] = 0;
    				}
    				
    				totalWeight = totalWeight - indexSum;

                	break;
            		
            	}
            }
        }
        
		
	}
	/**
	 * Samples Technique by Telmo (cf. MAS)
	 * @param samples
	 * @return
	 */
	public void marriageBySamples (Families families, int year){
		
		husbands = new Individual[samples];
		wives = new Individual[samples];
		weights = new double[samples];
		
    	double totalWeight = 0;
    	
        for (int i = 0; i < samples; i++) {	
			Individual husband = RandomUtils.draw(disposibleMen);
			Individual wife = RandomUtils.draw(disposibleWomen);
			if (husband!=null && wife!=null){
				double weight;
				
				if (avoidances.get(husband.getId()) != null && avoidances.get(husband.getId()).contains(wife)){
					weight = 0;
				} else if (preferences.get(husband.getId()) != null && preferences.get(husband.getId()).contains(wife)){
					weight = cousinPreferenceWeight;
				} else {
					weight = 1;
				}
				
	            totalWeight += weight;
	                
	            wives[i] = wife;
	            husbands[i] = husband;
	            weights[i] = weight;
			}
        }
        
        // random position in [0, totalWeight]
        double pos = randGen.nextDouble() * totalWeight;
            
        totalWeight = 0;
        for (int i = 0; i < samples; i++) {
            totalWeight += weights[i];
            if (totalWeight > pos) {
            	Individual husband = husbands[i];
            	Individual wife = wives[i];
            	if (husband!=null && wife!=null){
                	Family family = new Family(families.size()+1,husband,wife);
                	if (preferences.get(husband.getId()).contains(wife)){
                		family.setAttribute("PREFERENCE", "true");
                	}
    				husband.addPersonalFamily(family);
    				wife.addPersonalFamily(family);
    				family.setMarried(true);
    				families.put(family);
    				if (getAge(husband, year) <= maxAge && getAge(wife, year) <= maxAge){
    					fertileFamilies.add(family);
    				}
    				disposibleMen.remove(husband);
    				disposibleWomen.remove(wife);

                	break;
            	}
            }
        }
	}
	
	/**
	 * Technique of setting predetermined birth rate by Telmo 
	 * @return
	 */
	public Net createRandomNet (){
		Net result;
		
		result = new Net();
		
		//Initial Population
		for (int id=1;id<initSize+1;id++){
			int age = randGen.nextInt(maxAge+1);
			Gender gender = Gender.valueOf(randGen.nextInt(2));
			Individual indi = new Individual(id, gender.toChar()+" "+id, gender);
			indi.setAttribute("BIRT", (-age)+"");
			result.individuals().put(indi);
			if (indi.getGender()==Gender.MALE){
				preferences.put(indi.getId(),new Individuals());
				avoidances.put(indi.getId(),new Individuals());
			}
			
			if (age>=minAge && age<=maxAge){
				adults.put(indi);
				if (indi.isMale()){
					disposibleMen.add(indi);
//					System.out.println("initialized "+indi.getId());
				} else if (indi.isFemale()) {
					disposibleWomen.add(indi);
				}
			} else {
				children.put(indi);
			}
		}
		
		for (int year=0;year<years;year++){
			
			// Marriages
			int marriages = RandomUtils.randomRound(((disposibleMen.size()+disposibleWomen.size())/2)*marriageRate,randGen);
			
			int n = disposibleMen.size()*disposibleWomen.size();

			getWeights(n, year);
			
			for (int i=0;i<marriages;i++){
				marriage(result.families(), year, n);
			}
			
			// Births
			List<Family> disposibleFamilies = fertileFamilies.toList();
			
			int births = RandomUtils.randomRound(fertilityRate*adults.size()/(2*(maxAge-minAge)),randGen);
			for (int i = 0;i<births;i++){
				Family family = RandomUtils.draw(disposibleFamilies);
				if (family != null){
					Gender gender = Gender.valueOf(randGen.nextInt(2));
					int id = result.individuals().size()+1;
					Individual indi = new Individual(id, gender.toChar()+" "+id, gender);
					indi.setAttribute("BIRT", year+"");
					result.individuals().put(indi);
					children.put(indi);
					family.getChildren().put(indi);
					indi.setOriginFamily(family);
					disposibleFamilies.remove(family);
					
					if (indi.isMale()){
						preferences.put(indi.getId(),new Individuals());
						avoidances.put(indi.getId(),new Individuals());
					}
					Individuals preferred = preferences.get(indi.getId());
					for (Individual cousin : indi.crossSexCousins(cousinPreferenceType)){
						if (preferences.get(cousin.getId())!=null){
							if (indi.isMale()) {
								preferred.put(cousin);
							} else {
								preferences.get(cousin.getId()).put(indi);
							}
						}
					}
					Individuals avoided = avoidances.get(indi.getId());
					for (Individual parent : indi.getParents()){
						if (indi.isMale() && parent.isFemale()) {
							avoided.put(parent);
						} else if (indi.isFemale() && parent.isMale()){
							avoidances.get(parent.getId()).put(indi);
						}
						for (Individual sibling : parent.children()){
							if (indi.isMale() && sibling.isFemale()) {
								avoided.put(sibling);
							} else if (indi.isFemale() && sibling.isMale()){
								avoidances.get(sibling.getId()).put(indi);
							}
						}
					}
				}
			}
			
			// Maturation
			for (Individual indi : children.toList()){
				if (getAge(indi, year)>=minAge){
					children.removeById(indi.getId());
					adults.put(indi);
					if (indi.isMale()){
						disposibleMen.add(indi);
//						System.out.println("matured "+indi.getId());
					} else if (indi.isFemale()) {
						disposibleWomen.add(indi);
					}
				} 
			}
			for (Individual indi : adults.toList()){
				if (getAge(indi, year)>maxAge){
					adults.removeById(indi.getId());
					preferences.remove(indi.getId());
					avoidances.remove(indi.getId());
					if (indi.isMale()){
						disposibleMen.remove(indi);
//						System.out.println("removed old  "+indi.getId());
					} else if (indi.isFemale()) {
						disposibleWomen.remove(indi);
					}
					for (Family family : indi.getPersonalFamilies()){
						fertileFamilies.removeById(family.getId());
					}
				} 
			}
			
			//Divorces
			for (Family family : fertileFamilies.toList()){
				if (RandomUtils.event(divorceRate,randGen)){
					fertileFamilies.removeById(family.getId());
					disposibleMen.add(family.getHusband());
//					System.out.println("divorced "+family.getHusband().getId());
					disposibleWomen.add(family.getWife());
				}
			}
			
//			System.out.println(seedReferenceYear+"\t"+marriages+"\t"+births+"\t"+fertileFamilies.size()+"\t"+adults.size()+"\t"+children.size()+"\t"+result.individuals().size());
		}
		
		//
		return result;
	}


	public MAS getMas() {
		return mas;
	}



}
