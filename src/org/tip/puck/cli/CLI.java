/**
 * 
 */
package org.tip.puck.cli;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * @author telmo
 *
 */
public class CLI {
    private Options options;
    private CommandLine cline;
    
    private void printHelpMessage() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("evo", options);
    }
    
    private void printErrorMessage(String msg) {
        System.err.println(msg);
        printHelpMessage();
    }
    
    public void run(String[] args) {
        // args = new String[]{"evo", "-inet", "chimane.dat", "-odir", "test"};
    	// args = new String[]{"mas", "-sim", "experiments/mas/example.txt", "-onet", "test.tip", "-onet2", "test2.tip"};
        
        CommandLineParser parser = new GnuParser();
        options = new Options();
        options.addOption("inet", true, "input net file");
        options.addOption("onet", true, "output net file");
        options.addOption("onet2", true, "second output net file");
        options.addOption("odir", true, "output directory");
        options.addOption("prg", true, "generator program file");
        options.addOption("oprg", true, "generator output program file");
        options.addOption("ocsv", true, "output csv file");
        options.addOption("sim", true, "kinship simulation config file");
        
        try {
            cline = parser.parse(options, args);

            String cmd = args[0];
            Command cmdObj = null;
            
            if (cmd.equals("help")) {
                printHelpMessage();
            }
            else if (cmd.equals("evo")) {
                cmdObj = new EvoAlliance();
            }
            else if (cmd.equals("prune")) {
                cmdObj = new PruneAllianceGen();
            }
            else if (cmd.equals("run")) {
                cmdObj = new RunAllianceGen();
            }
            else if (cmd.equals("random")) {
                cmdObj = new RandomAllianceGen();
            }
            else if (cmd.equals("mas")) {
                cmdObj = new RunMAS();
            }
            else {
                printErrorMessage("Command '" + cmd + "' does not exist.");
            }
            
            if (cmdObj != null) {
                if (!cmdObj.run(cline)) {
                    printErrorMessage(cmdObj.getErrorMessage());
                }
            }
        }
        catch (ParseException e) {
           String msg = e.getMessage();
           if (msg == null) {
               msg = "unkown error";
           }
           printErrorMessage(msg);
        }
        
        System.exit(0);
    }
    
    public static void main(String[] args) {
        (new CLI()).run(args);
    }
}