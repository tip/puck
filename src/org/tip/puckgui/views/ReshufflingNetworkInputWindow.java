package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.graphs.onemode.OMGraph.GraphMode;
import org.tip.puck.graphs.onemode.ShuffleCriteria;
import org.tip.puck.graphs.onemode.ShuffleReporter;
import org.tip.puck.graphs.onemode.Shuffler;
import org.tip.puck.net.Net;
import org.tip.puck.report.Report;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.PuckGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class ReshufflingNetworkInputWindow extends JFrame {

	private static final long serialVersionUID = 8620018776850245508L;
	private static final Logger logger = LoggerFactory.getLogger(ReshufflingNetworkInputWindow.class);

	private JFrame thisJFrame;
	private NetGUI netGUI;
	private JPanel contentPane;
	private JSpinner spinnerPermutationEdge;
	private JSpinner spinnerMaximumGenerationDistance;
	private JSpinner spinnerMaximumShufflePercentage;
	private JSpinner spinnerMinimumStableIterations;
	private JComboBox cbBoxMode;

	/**
	 * Create the frame.
	 */
	public ReshufflingNetworkInputWindow(final NetGUI netGUI) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(FooReporterInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		this.netGUI = netGUI;
		setTitle("Random Reshuffling");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 237);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel inputPanel = new JPanel();
		this.contentPane.add(inputPanel, BorderLayout.CENTER);
		inputPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblAlpha = new JLabel("Number of edge permutations per step:");
		inputPanel.add(lblAlpha, "2, 2, left, default");

		this.spinnerPermutationEdge = new JSpinner();
		this.spinnerPermutationEdge.setModel(new SpinnerNumberModel(new Integer(2), new Integer(0), null, new Integer(1)));
		inputPanel.add(this.spinnerPermutationEdge, "4, 2");

		JLabel lblBravo = new JLabel("Maximum generational distance:");
		inputPanel.add(lblBravo, "2, 4");

		this.spinnerMaximumGenerationDistance = new JSpinner();
		this.spinnerMaximumGenerationDistance.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		inputPanel.add(this.spinnerMaximumGenerationDistance, "4, 4");

		JLabel lblCharlie = new JLabel("Minimum shuffle percentage (stop condition):");
		inputPanel.add(lblCharlie, "2, 6");

		this.spinnerMaximumShufflePercentage = new JSpinner();
		this.spinnerMaximumShufflePercentage.setModel(new SpinnerNumberModel(100, 0, 100, 1));
		inputPanel.add(this.spinnerMaximumShufflePercentage, "4, 6");

		JLabel lblDelta = new JLabel("Minimum stable iterations (stop condition):");
		inputPanel.add(lblDelta, "2, 8");

		this.spinnerMinimumStableIterations = new JSpinner();
		this.spinnerMinimumStableIterations.setModel(new SpinnerNumberModel(new Integer(1000), new Integer(0), null, new Integer(1)));
		inputPanel.add(this.spinnerMinimumStableIterations, "4, 8");

		JLabel lblNewLabel = new JLabel("Graph mode:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		inputPanel.add(lblNewLabel, "2, 10, right, default");

		this.cbBoxMode = new JComboBox();
		this.cbBoxMode.setModel(new DefaultComboBoxModel(GraphMode.values()));
		this.cbBoxMode.setSelectedIndex(1);
		inputPanel.add(this.cbBoxMode, "4, 10, fill, default");

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					ShuffleCriteria criteria = new ShuffleCriteria();
					criteria.setSwitchesPerIteration((Integer) ReshufflingNetworkInputWindow.this.spinnerPermutationEdge.getValue());
					criteria.setMaxGenerationalDistance((Integer) ReshufflingNetworkInputWindow.this.spinnerMaximumGenerationDistance.getValue());
					criteria.setMinShufflePercentage((Integer) ReshufflingNetworkInputWindow.this.spinnerMaximumShufflePercentage.getValue());
					criteria.setMinStableIterations((Integer) ReshufflingNetworkInputWindow.this.spinnerMinimumStableIterations.getValue());
					criteria.setMode((GraphMode) ReshufflingNetworkInputWindow.this.cbBoxMode.getSelectedItem());

					Report report = ShuffleReporter.reportReshuffling(netGUI.getNet(), criteria);
					Net targetNet = Shuffler.shuffle(netGUI.getNet(), criteria, report);

					//
					NetGUI newNetGui = PuckGUI.instance().createNetGUI(netGUI.getFile(), targetNet);
					newNetGui.setChanged(true, "-reshuffled");

					newNetGui.addReportTab(report);

					dispose();
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(ReshufflingNetworkInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);
	}
}
