package org.tip.puck.kinoath;

import java.util.ArrayList;
import java.util.Collections;

/**
 * 
 * @author TIP
 */
public class IndividualGroupList extends ArrayList<IndividualGroup> {

	private static final long serialVersionUID = 4598257124096716535L;

	/**
	 * 
	 */
	public IndividualGroupList() {
		super();
	}

	/**
	 * 
	 */
	public IndividualGroupList(final int initialCapacity) {
		super(initialCapacity);
	}

	/**
	 * 
	 */
	public IndividualGroupList(final IndividualGroupList source) {
		super(source);
	}

	/**
	 * 
	 * @return
	 */
	public IndividualGroupList sortById() {
		IndividualGroupList result;

		//
		Collections.sort(this, new IndividualGroupComparator(IndividualGroupComparator.Sorting.ID));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public IndividualGroupList sortByReversedSize() {
		IndividualGroupList result;

		//
		Collections.sort(this, new IndividualGroupComparator(IndividualGroupComparator.Sorting.REVERSED_SIZE));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public IndividualGroupList sortBySize() {
		IndividualGroupList result;

		//
		Collections.sort(this, new IndividualGroupComparator(IndividualGroupComparator.Sorting.SIZE));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public IndividualGroup[] toArray() {
		IndividualGroup[] result;

		result = new IndividualGroup[this.size()];
		int count = 0;
		for (IndividualGroup individual : this) {
			//
			result[count] = individual;
			count += 1;
		}

		//
		return result;
	}
}
