package oldcore.trash;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

import org.tip.puck.net.Gender;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.relations.Roles;
import org.tip.puck.net.relations.roles.RoleRelationMaker;
import org.tip.puck.util.NumberablesHashMap;

import fr.devinsy.util.StringList;

public class RoleDefinitions extends NumberablesHashMap<RoleDefinition> {
	
	boolean egoGenderDistinction;
	RoleDefinitions recursiveDefinitions;
	Map<Role,Roles> factors;
	Roles uncomposableRoles;
	
	public RoleDefinitions (){
		
	}
	
	public RoleDefinitions (RoleDefinitions roleDefinitions){
		
		this.uncomposableRoles = roleDefinitions.uncomposableRoles;
		
		for (RoleDefinition roleDefinition: roleDefinitions){
			add(roleDefinition.clone());
		}
	}
	
	private Role getFatherRole(){
		Role result;

		result = null;
		
		for (RoleDefinition roleDefinition : this){
			if (roleDefinition.primary()!=null && roleDefinition.primary().equals(Primary.PARENT) && roleDefinition.alterGender().isMale()){
				result = roleDefinition.role();
				break;
			}
		}
		
		//
		return result;
	}
	
	private Role getMotherRole(){
		Role result;

		result = null;
		
		for (RoleDefinition roleDefinition : this){
			if (roleDefinition.primary()!=null && roleDefinition.primary().equals(Primary.PARENT) && roleDefinition.alterGender().isFemale()){
				result = roleDefinition.role();
				break;
			}
		}
		
		//
		return result;
	}
	
	private Role getNeutralParentRole(){
		Role result;

		result = null;
		
		for (RoleDefinition roleDefinition : this){
			if (roleDefinition.primary()!=null && roleDefinition.primary().equals(Primary.PARENT) && roleDefinition.alterGender().isUnknown()){
				result = roleDefinition.role();
				break;
			}
		}
		
		//
		return result;
	}

	public void setInversePrimaryTerms (){
		
		Role fatherRole = getFatherRole();
		Role motherRole = getMotherRole();
		Role parentRole = getNeutralParentRole();

		if (fatherRole!=null || motherRole!=null || parentRole!=null){
			for (RoleDefinition def : toList()){
				if (def.inversion()!=null && def.inversion().toString().equals("[Parent]")){
					if (fatherRole!=null){
						RoleDefinition parentDefinition = def.clone();
						parentDefinition.setId(getLastId()+1);
						parentDefinition.setInversion(fatherRole);
						addNew(parentDefinition);
					}
					if (motherRole!=null){
						RoleDefinition parentDefinition = def.clone();
						parentDefinition.setId(getLastId()+1);
						parentDefinition.setInversion(motherRole);
						addNew(parentDefinition);
					}
					if (parentRole!=null){
						RoleDefinition parentDefinition = def.clone();
						parentDefinition.setId(getLastId()+1);
						parentDefinition.setInversion(parentRole);
						addNew(parentDefinition);
					}
					removeById(def.getId());
				}
			}
		}
	}
	
	public static boolean setPrimaryFromString (RoleDefinition roleDefinition, String item){
		boolean result;
		
		result = false;
		
		if (item.length()==1){
			
			result = true;
			
			if (item.equals("M")){
				
				roleDefinition.setPrimary(Primary.PARENT);
				roleDefinition.setAlterGender(Gender.FEMALE);
				
			} else if (item.equals("F")){
				
				roleDefinition.setPrimary(Primary.PARENT);
				roleDefinition.setAlterGender(Gender.MALE);

			} else if (item.equals("W")){
				
				roleDefinition.setPrimary(Primary.SPOUSE);
				roleDefinition.setAlterGender(Gender.FEMALE);
				// Heterosexual Marriage
				if (roleDefinition.egoGender().isUnknown()){
					roleDefinition.setEgoGender(Gender.MALE);
				}

			} else if (item.equals("H")){
				
				roleDefinition.setPrimary(Primary.SPOUSE);
				roleDefinition.setAlterGender(Gender.MALE);
				// Heterosexual Marriage
				if (roleDefinition.egoGender().isUnknown()){
					roleDefinition.setEgoGender(Gender.FEMALE);
				}

			} else if (item.equals("Z")){
				
				roleDefinition.setPrimary(Primary.SIBLING);
				roleDefinition.setAlterGender(Gender.FEMALE);

			} else if (item.equals("B")){
				
				roleDefinition.setPrimary(Primary.SIBLING);
				roleDefinition.setAlterGender(Gender.MALE);

			} else if (item.equals("D")){
				
				roleDefinition.setInversion(new Role("[Parent]"));
				roleDefinition.setAlterGender(Gender.FEMALE);

			} else if (item.equals("S")){
				
				roleDefinition.setInversion(new Role("[Parent]"));
				roleDefinition.setAlterGender(Gender.MALE);

			} else {
				
				result = false;
			}
		}
		
		
		//
		return result;
	}
	
	public Roles getPrimaryTerms (String item, Gender egoGender, AlterAge alterAge, Gender alterGender){
		Roles result;
		
		result = new Roles();
		
		RoleDefinition roleDefinition = new RoleDefinition(0);
		setPrimaryFromString(roleDefinition, item);
		if (roleDefinition.egoGender().isUnknown()){
			roleDefinition.setEgoGender(egoGender);
		}
		if (roleDefinition.alterGender().isUnknown()){
			roleDefinition.setAlterGender(alterGender);
		}
		if (roleDefinition.alterAge()==null){
			roleDefinition.setAlterAge(alterAge);
		}
		
		if (roleDefinition.inversion()!=null && roleDefinition.inversion().getName().equals("[Parent]")){
			Roles inverseRoles = new Roles();
			for (RoleDefinition definition : get(Primary.PARENT,null,null,egoGender,null,roleDefinition.alterGender())){
				if (!inverseRoles.contains(definition.role())){
					inverseRoles.add(definition.role());
				}
			}
			for (Role inverseRole : inverseRoles){
				for (RoleDefinition definition : get(roleDefinition.primary(),inverseRole,roleDefinition.composition(),roleDefinition.alterGender(),roleDefinition.alterAge(),roleDefinition.egoGender())){
					if (!result.contains(definition.role())){
						result.add(definition.role());
					}
				}
			}
		}
		
		for (RoleDefinition definition : getMostPrecise(roleDefinition.primary(),roleDefinition.inversion(),roleDefinition.composition(),roleDefinition.alterGender(),roleDefinition.alterAge(),roleDefinition.egoGender())){
			if (!result.contains(definition.role())){
				result.add(definition.role());
			}
		}
		//
		return result;
	}
	
	public RoleDefinitions get (Primary primary, Role inversion, Roles composition, Gender alterGender, AlterAge alterAge, Gender egoGender){
		RoleDefinitions result;
		
		result = new RoleDefinitions();
		
		for (RoleDefinition def : this){
			
			if ((def.primary() == primary)
				&& (def.inversion() == inversion)
				&& ((def.composition() == null && composition == null) || (def.composition()!=null && def.composition().equals(composition)))	
				&& (def.alterGender() == alterGender || def.alterGender().isUnknown() || alterGender.isUnknown())
				&& (def.alterAge() == alterAge || def.alterAge()==null || alterAge==null)
				&& (def.egoGender() == egoGender || def.egoGender().isUnknown() || egoGender.isUnknown())) {
				
				result.addNew(def);
			}
		}
		
		//
		return result;
	}
	
	public RoleDefinitions getMostPrecise (Primary primary, Role inversion, Roles composition, Gender alterGender, AlterAge alterAge, Gender egoGender){
		RoleDefinitions result;
		
		result = new RoleDefinitions();
		RoleDefinitions preResult = get(primary, inversion, composition, alterGender, alterAge, egoGender);
		
		for (RoleDefinition definition : preResult){
			
			boolean isMostPrecise = true;
			for (RoleDefinition otherDefinition : preResult){
				int compareGenderPrecision = 0;
				int compareAgePrecision = 0;
				if (!alterGender.isUnknown() && definition.alterGender().isUnknown() && !otherDefinition.alterGender().isUnknown()){
					compareGenderPrecision = -1;
				}
				if (!alterGender.isUnknown() && !definition.alterGender().isUnknown() && otherDefinition.alterGender().isUnknown()){
					compareGenderPrecision = 1;
				}
				if (alterAge!=null && definition.alterAge()==null && otherDefinition.alterAge()!=null){
					compareAgePrecision = -1;
				}
				if (alterAge!=null && definition.alterAge()!=null && otherDefinition.alterAge()==null){
					compareAgePrecision = 1;
				}
				if (compareGenderPrecision+compareAgePrecision<0){
//					System.out.println(definition+" less precise than "+otherDefinition);
					isMostPrecise =false;
					break;
				}
			}
			if (isMostPrecise){
				result.addNew(definition);
			}
		}
		
		//
		return result;
	}
	
	public boolean isPrimaryParent (RoleDefinition roleDefinition){
		return roleDefinition.primary() == Primary.PARENT;
	}
	
	public boolean isPrimarySibling (RoleDefinition roleDefinition){
		return roleDefinition.primary() == Primary.SIBLING;
	}
	
	public boolean isPrimarySpouse (RoleDefinition roleDefinition){
		return roleDefinition.primary() == Primary.SPOUSE;
	}
	
	public boolean isPrimaryChild (RoleDefinition roleDefinition){
		boolean result;
		
		result = false;
		
		for (RoleDefinition inverseDefinition : getDefinitions(roleDefinition.inversion())){
			if (isPrimaryParent(inverseDefinition)){
				result = true;
			}
		}
		//
		return result;
	}
	
	public Roles getCompositeRoles(Role alpha, Role beta, int iterations, int maxIterations){
		Roles result;

		result = new Roles();
		
		if (iterations<=maxIterations){
			
			for (RoleDefinition alphaDef : getDefinitions(alpha)){
				for (RoleDefinition betaDef : getDefinitions(beta)){
					if (isPrimaryChild(betaDef)){
						if (isPrimaryParent(alphaDef)){
							for (RoleDefinition siblingDef : get(Primary.SIBLING,null,null,betaDef.alterGender(),betaDef.alterAge(),alphaDef.egoGender())){
								if (!result.contains(siblingDef.role())){
									result.add(siblingDef.role());
								}
							}
						} else if (alphaDef.composition()!=null){
							Role gamma = alphaDef.composition().get(0);
							Role delta = alphaDef.composition().get(1);
							for (RoleDefinition deltaDef : getDefinitions(delta)){
								if (isPrimaryParent(deltaDef)){
									for (RoleDefinition siblingDef : get(Primary.SIBLING,null,null,betaDef.alterGender(),betaDef.alterAge(),deltaDef.egoGender())){
										Role deltaBeta = siblingDef.role();
										for (Role gammaDeltaBeta : getCompositeRoles(gamma,deltaBeta,iterations+1,maxIterations)){
											if (!result.contains(gammaDeltaBeta)){
												result.add(gammaDeltaBeta);
											}
										}
									}
								}
							}
						}
					} else if (isPrimaryParent(betaDef)){
						if (isPrimaryChild(alphaDef)){
							for (RoleDefinition spouseDef : get(Primary.SPOUSE,null,null,betaDef.alterGender(),betaDef.alterAge(),alphaDef.egoGender())){
								if (!result.contains(spouseDef.role())){
									result.add(spouseDef.role());
								}
							}
						} else if (alphaDef.composition()!=null){
							Role gamma = alphaDef.composition().get(0);
							Role delta = alphaDef.composition().get(1);
							for (RoleDefinition deltaDef : getDefinitions(delta)){
								if (isPrimaryChild(deltaDef)){
									for (RoleDefinition spouseDef : get(Primary.SPOUSE,null,null,betaDef.alterGender(),betaDef.alterAge(),deltaDef.egoGender())){
										Role deltaBeta = spouseDef.role();
										for (Role gammaDeltaBeta : getCompositeRoles(gamma,deltaBeta,iterations+1,maxIterations)){
											if (!result.contains(gammaDeltaBeta)){
												result.add(gammaDeltaBeta);
											}
										}
									}
								}
							}
						}
					}
				}
			}

			if (hasNonCompositeDefinition(beta)){
				for (RoleDefinition def : this){
					if (!isNullOrHasNullFactor(def.composition()) && def.composition().get(0).equals(alpha) && def.composition().get(1).equals(beta) && !result.contains(def.role())){
						result.add(def.role());
					}
				}
			} else {
				for (RoleDefinition compositeDefinition : getCompositeDefinitions(beta)){
					if (!isRecursive(compositeDefinition)){
						Role gamma = compositeDefinition.composition().get(0);
						Role delta = compositeDefinition.composition().get(1);
						for (Role alphaGamma: getCompositeRoles(alpha,gamma,iterations+1,maxIterations)){
							for (Role alphaGammaDelta : getCompositeRoles(alphaGamma,delta,iterations+1,maxIterations)){
								if (!result.contains(alphaGammaDelta)){
									result.add(alphaGammaDelta);
								}
							}
						}
					}
				}
			}
			Collections.sort(result);
		}


		//
		return result;
	}
	
	private static AlterAge inverse(AlterAge age){
		AlterAge result;
		
		result = null;
		if (age!=null){
			result = age.invert();
		}
		//
		return result;
	}
	
	public boolean isRecursive(RoleDefinition roleDefinition){
		boolean result;
		
		result = recursiveDefinitions().contains(roleDefinition);
		
		//
		return result;
		
	}
	
	public Roles getReciprocalRoles (Role role, int iterations, int maxIterations) {
		Roles result;
		
		result = new Roles();
		
		for (Role inverseRole : getInverseRoles(role)){
			if (!result.contains(inverseRole)){
				result.add(inverseRole);
			}
		}
		
		if (iterations<=maxIterations){
			for (RoleDefinition compositeDefinition : getCompositeDefinitions(role)){
//				if (!isRecursive(compositeDefinition)){
					for (Role alpha: getReciprocalRoles(compositeDefinition.composition().get(0),iterations+1,maxIterations)){
						for (Role beta: getReciprocalRoles(compositeDefinition.composition().get(1),iterations+1,maxIterations)){
							for (Role betaAlpha : getCompositeRoles(beta,alpha,0,maxIterations)){
								if (!result.contains(betaAlpha)){
									result.add(betaAlpha);
								}
							}
						}
//					}
				}
			}
		}
		
		
		//
		return result;
	}
	


	public boolean isPrimary (Role role){
		boolean result;
		
		result = getPrimaryDefinitions(role).size()>0;
		
		//
		return result;
	}
	
	public RoleDefinitions getPrimaryDefinitions(Role role){
		RoleDefinitions result;
		
		result = new RoleDefinitions();
		
		for (RoleDefinition definition : getDefinitions(role)){
			if (definition.primary()!=null){
				result.add(definition);
			}
		}
		
		//
		return result;
	}
	
	public Roles getInverseRoles(Role role){
		Roles result;
		
		result = new Roles();
		
		for (RoleDefinition def : getDefinitions(role)){
			
			if (isPrimarySibling(def)){
				for (RoleDefinition siblingDef : get(Primary.SIBLING,null,null,def.egoGender(),adjusted(def.alterAge()),def.alterGender())){
					if (siblingDef!=null && !result.contains(siblingDef.role())){
						result.add(siblingDef.role());
					}
				}
			} else if (isPrimarySpouse(def)){
				for (RoleDefinition spouseDef : get(Primary.SPOUSE,null,null,def.egoGender(),adjusted(def.alterAge()),def.alterGender())){
					if (spouseDef!=null && !result.contains(spouseDef.role())){
						result.add(spouseDef.role());
					}
				}
			} else if (def.inversion()!=null){
				result.add(def.inversion());
			} else {
				for (RoleDefinition otherDefinition : this){
					if (otherDefinition.inversion()!=null && otherDefinition.inversion().equals(role) && !result.contains(otherDefinition.role())){
						result.add(otherDefinition.role());
					}
				}
			}
		}
		
		//
		return result;
	}
	
	public RoleDefinitions getCompositeDefinitions(Role role){
		RoleDefinitions result;
		
		result = new RoleDefinitions();
		
		for (RoleDefinition definition : getDefinitions(role)){
			if (definition.composition()!=null){
				result.add(definition);
			}
		}
		
		//
		return result;
	}
	
	public boolean hasNonCompositeDefinition(Role role){
		boolean result;
		
		result = false;
		
		for (RoleDefinition definition : getDefinitions(role)){
			if (definition.composition()==null){
				result=true;
				break;
			}
		}
		
		//
		return result;
	}
	
	public RoleDefinitions getNonRecursiveDefinitions(Role role){
		RoleDefinitions result;
		
		result = new RoleDefinitions();
		
		for (RoleDefinition roleDefinition : this){
			if (roleDefinition.role().equals(role) && !recursiveDefinitions().contains(roleDefinition)){
				result.add(roleDefinition);
			}
		}
		//
		return result;
	}
		
	public RoleDefinitions getDefinitions(Role role){
		RoleDefinitions result;
		
		result = new RoleDefinitions();
		
		for (RoleDefinition roleDefinition : this){
			if (roleDefinition.role().equals(role)){
				result.add(roleDefinition);
			}
		}
		//
		return result;
	}
	
	public RoleDefinition getEgoGenderComplement (RoleDefinition source){
		RoleDefinition result;
		
		result = null;
		
		for (RoleDefinition def : this){
			if ((def.primary() == source.primary())
				&& (Role.equalOrBothNull(def.inversion(),source.inversion()))
				&& (Roles.equalOrBothNull(def.composition(), source.composition()))
				&& (def.alterGender() == source.alterGender())
				&& (def.alterAge() == source.alterAge())
				&& (def.egoGender() == source.egoGender().invert())) {
				
				result = def;
				break;
			}
		}
		
		//
		return result;
	}
	
	public RoleDefinition getEgoGenderComplementAgeNeutral (RoleDefinition source){
		RoleDefinition result;
		
		result = null;
		
		for (RoleDefinition def : this){
			
			if ((def.primary() == source.primary())
				&& (Role.equalOrBothNull(def.inversion(),source.inversion()))
				&& (Roles.equalOrBothNull(def.composition(), source.composition()))
				&& (def.alterGender() == source.alterGender())
				&& (source.alterAge()==null || def.alterAge() == null || def.alterAge() == source.alterAge())
				&& (def.egoGender() == source.egoGender().invert())) {
				
				result = def;
				break;
			}
		}
		
		//
		return result;
	}
	
	public RoleDefinition getAlterGenderComplement (RoleDefinition source){
		RoleDefinition result;
		
		result = null;
		
		for (RoleDefinition def : this){
			
			if ((def.primary() == source.primary())
				&& (Role.equalOrBothNull(def.inversion(),source.inversion()))
				&& (Roles.equalOrBothNull(def.composition(), source.composition()))
				&& (def.alterGender() == source.alterGender().invert())
				&& (def.alterAge() == source.alterAge())
				&& (def.egoGender() == source.egoGender())) {
				
				result = def;
				break;
			}
			
		}
		//
		return result;
	}
	
	public RoleDefinition getAlterGenderComplementAgeNeutral (RoleDefinition source){
		RoleDefinition result;
		
		result = null;
		
		for (RoleDefinition def : this){
			
			if ((def.primary() == source.primary())
				&& (Role.equalOrBothNull(def.inversion(),source.inversion()))
				&& (Roles.equalOrBothNull(def.composition(), source.composition()))
				&& (def.alterGender() == source.alterGender().invert())
				&& (source.alterAge()==null || def.alterAge() == null || def.alterAge() == source.alterAge())
				&& (def.egoGender() == source.egoGender())) {
				
				result = def;
				break;
			}
			
		}
		//
		return result;
	}
	
	public RoleDefinition getAlterAgeComplement (RoleDefinition source){
		RoleDefinition result;
		
		result = null;
		
		for (RoleDefinition def : this){
			if ((def.primary() == source.primary())
				&& (Role.equalOrBothNull(def.inversion(),source.inversion()))
				&& (Roles.equalOrBothNull(def.composition(), source.composition()))
				&& (def.alterGender() == source.alterGender())
				&& (source.alterAge()!=null && def.alterAge() == source.alterAge().invert())
				&& (def.egoGender() == source.egoGender())) {
				
				result = def;
				break;
			}
		}
		
		//
		return result;
	}
	
	public Roles getRoles (Primary primary, Gender alterGender, AlterAge alterAge, Gender egoGender){
		Roles result;
		
		result = new Roles();
		
		for (RoleDefinition def : this){
			if ((def.primary() == primary)
				&& (def.alterGender()==Gender.UNKNOWN || alterGender == Gender.UNKNOWN || alterGender == def.alterGender())
				&& (def.egoGender()==Gender.UNKNOWN || egoGender == Gender.UNKNOWN || egoGender == def.egoGender())
				&& (def.alterAge()==alterAge || def.alterAge()==null || alterAge == null)){
				
				if (!result.contains(def.role())){
					result.add(def.role());
				}
			}
		}
		
		//
		return result;
	}
	
	public Role getRoleByName (String name){
		Role result;
		
		result = null;
		
		for (RoleDefinition def : this){
			if (def.role()!=null && def.role().getName().equals(name)){
				result = def.role();
				break;
			}
		}
		//
		return result;
	}
	
	public Role getInverseRole (Role role, Gender egoGender, Gender alterGender){
		Role result;
		
		if (egoGender==null){
			System.err.println("Null ego gender: "+role);
		}
		if (alterGender==null){
			System.err.println("Null alter gender: "+role);
		}
		
		result = null;
		
		for (RoleDefinition def : this){
			
			if (def.inversion()!=null && def.inversion().equals(role) && genderMatch(egoGender,def.alterGender()) && genderMatch(def.egoGender(), alterGender)){
				result = def.role();
				break;
			} 
			if (def.inversion()!=null && def.role().equals(role) && genderMatch(def.egoGender(),egoGender) && genderMatch(def.alterGender(),alterGender)){				
				result = def.inversion();
				break;
			}
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<RoleDefinition> toSortedList() {
		List<RoleDefinition> result;

		result = toList();
		Collections.sort(result, new RoleDefinitionComparator());

		//
		return result;
	}

	private static boolean genderMatch (Gender alphaGender, Gender betaGender){
		return (alphaGender==Gender.UNKNOWN || alphaGender == betaGender);
	}

	public boolean isEgoGenderDistinction() {
		return egoGenderDistinction;
	}

	public void setEgoGenderDistinction(boolean egoGenderDistinction) {
		this.egoGenderDistinction = egoGenderDistinction;
	}
	
	public List<Gender> getAlterGenders(Role role){
		List<Gender> result;
		
		result = new ArrayList<Gender>();
		
		for (RoleDefinition def : getDefinitions(role)){
			if (!result.contains(def.alterGender())){
				result.add(def.alterGender());
			}
		}
		//
		return result;
	}
	
	public List<Gender> getMediusGenders(Role role){
		List<Gender> result;

		result = new ArrayList<Gender>();
		
		for (RoleDefinition def : getDefinitions(role)){
			if (def.composition()!=null){
				for (RoleDefinition mediusDef : getDefinitions(def.composition().get(0))){
					if (!result.contains(mediusDef.alterGender())){
						result.add(mediusDef.alterGender());
					}
				}
			}
			
		}
		//
		return result;
		
	}
	
/*	public RoleDefinitions neutralize (){
		RoleDefinitions result;
		
		result = new RoleDefinitions(this);
		
		for (RoleDefinition def : this){
			
			RoleDefinition newDef = result.getById(def.getId());
			
			if (newDef!=null && newDef.egoGender()!=Gender.UNKNOWN){
				RoleDefinition egoGenderComplement = getEgoGenderComplement(def);
				if (egoGenderComplement == null){
					egoGenderComplement = getEgoGenderComplementAgeNeutral(def);
				}
				
				if (egoGenderComplement==null) {
					newDef.setEgoGender(Gender.UNKNOWN);
				} else if (egoGenderComplement.role().equals(newDef.role())){
					newDef.setEgoGender(Gender.UNKNOWN);
					result.removeById(egoGenderComplement.getId());
				}

			}
			
		}
		
		for (RoleDefinition def : this){
			
			RoleDefinition newDef = result.getById(def.getId());

			if (newDef!=null && newDef.alterGender()!=Gender.UNKNOWN){
				RoleDefinition alterGenderComplement = getAlterGenderComplement(def);
				if (alterGenderComplement==null){
					alterGenderComplement = getAlterGenderComplementAgeNeutral(def);
				}
				
				if (alterGenderComplement==null) {
//					newDef.setAlterGender(Gender.UNKNOWN);
				} else if (alterGenderComplement.role().equals(newDef.role())){
					newDef.setAlterGender(Gender.UNKNOWN);
					result.removeById(alterGenderComplement.getId());
				}
			}
		}
		//
		return result;
	}*/
	
	private Roles roles(){
		Roles result;
		
		result = new Roles();
		
		for (RoleDefinition definition : this){
			if (definition.role()!=null && !result.contains(definition.role())){
				result.add(definition.role());
			}
		}
		//
		return result;
	}
	
	public Map<Role,Roles> factors (){
		
		if (factors==null){
			factors = new TreeMap<Role,Roles>();
			recursiveDefinitions = new RoleDefinitions();
			uncomposableRoles = new Roles();
			
			for (Role role : roles()){
				factors.put(role, factors(role));
			}
			
		}
		
		//
		return factors;
	}
	
	public RoleDefinitions recursiveDefinitions(){
		
		if (recursiveDefinitions==null){
			factors();
		}
		
		return recursiveDefinitions;
	}
	
	private Roles factors(Role role){
		Roles result;
		
		result = new Roles();
		
		
		Queue<Role> queue = new LinkedList<Role>();
		Roles visited = new Roles();
		
		queue.add(role);
		visited.add(role);
		
		while (!queue.isEmpty()){
			Role factor = queue.remove();
			for (RoleDefinition definition : getDefinitions(factor)){
				if (definition.primary()!=null){
					if (!result.contains(factor)){
						result.add(factor);
					}
				} else if (definition.inversion()!=null){
					Role inv = definition.inversion();
					if (inv.equals(role)){
						recursiveDefinitions.add(definition);
					} else if (!visited.contains(inv)){
						queue.add(inv);
						visited.add(inv);
					}
				} else if (!isNullOrHasNullFactor(definition.composition())){
					for (Role comp : definition.composition()){
						if (comp.equals(role)){
							recursiveDefinitions.add(definition);
						} else if (!visited.contains(comp)){
							queue.add(comp);
							visited.add(comp);
						}
					}
				} else if (definition.composition()==null) {
					System.err.println("Null compositions: "+definition+" "+definition.primary());
					if (!uncomposableRoles.contains(role)){
						uncomposableRoles.add(role);
					}
				} else {
					System.err.println("Null factors: "+definition);
					if (!uncomposableRoles.contains(role)){
						uncomposableRoles.add(role);
					}
				}
			}
		}
		
		//
		return result;
	}
	
	public static boolean isNullOrHasNullFactor(Roles composition){
		boolean result;
		
		result = false;
		if (composition==null){
			result = true;
		} else if (composition.size()<2 || composition.get(0)==null || composition.get(1)==null) {
			result = true;
		}
		//
		return result;
	}

	public Roles getUncomposableRoles() {
		return uncomposableRoles;
	}
	
	public String toString (){
		String result;
		
		result = this.toList().toString();
		
		//
		return result;
	}
	
	public RoleDefinitions clean (){
		RoleDefinitions result;
		
		result = new RoleDefinitions();
		
		for (RoleDefinition def : toSortedList()){
			
			boolean add = true;
			
			RoleDefinitions egoGenderInverts = result.get(def.primary(), def.inversion(), def.composition(), def.alterGender(), def.alterAge(), def.egoGender().invert());
			
			for (RoleDefinition egoGenderInvert : egoGenderInverts){
				
				if (def.role().equals(egoGenderInvert.role())){
					egoGenderInvert.setEgoGender(Gender.UNKNOWN);
					add = false;
				}
			}
			 
			RoleDefinitions alterGenderInverts = result.get(def.primary(), def.inversion(), def.composition(), def.alterGender().invert(), def.alterAge(), def.egoGender());
			for (RoleDefinition alterGenderInvert : alterGenderInverts){
				if (def.role().equals(alterGenderInvert.role())){
					alterGenderInvert.setAlterGender(Gender.UNKNOWN);
					add = false;
				}
			}
			
			AlterAge invAlterAge = null;
			if (def.alterAge()!=null){
				invAlterAge = def.alterAge().invert();
			}
			
			RoleDefinitions alterAgeInverts = result.get(def.primary(), def.inversion(), def.composition(), def.alterGender(), invAlterAge, def.egoGender());
			for (RoleDefinition alterAgeInvert : alterAgeInverts){
				if (def.role().equals(alterAgeInvert.role())){
					alterAgeInvert.setAlterAge(null);
					add = false;
				}
			}

			if (add){
				result.addNew(def);
			}
		}
		
		//
		return result;
		
	}
	
	public boolean isSpouseRole(Role role){
		boolean result;
		
		result = false;
		
		for (RoleDefinition definition: getDefinitions(role)){
			if (definition.primary()==Primary.SPOUSE){
				result = true;
				break;
			}
		}
		//
		return result;
	}

	

}
