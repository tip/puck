package org.tip.puckgui.models;

import java.awt.Color;
import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.tip.puck.net.Family;

/**
 * This class provides implementation for displaying decorated family list.
 * 
 * @author Edoardo Savoia
 * @author TIP
 */
public class FamiliesCellRenderer extends JLabel implements ListCellRenderer {
	private static final long serialVersionUID = -8495549456630396678L;
	private static ImageIcon femaleIcon;
	private static ImageIcon maleIcon;
	private static ImageIcon unknowIcon;

	public FamiliesCellRenderer() {
		super();
		setOpaque(true);
		if (femaleIcon == null) {
			femaleIcon = new ImageIcon(FamiliesCellRenderer.class.getResource("/org/tip/puckgui/images/female-16x16.png"));
		}
		if (maleIcon == null) {
			maleIcon = new ImageIcon(FamiliesCellRenderer.class.getResource("/org/tip/puckgui/images/male-16x16.png"));
		}
		if (unknowIcon == null) {
			unknowIcon = new ImageIcon(FamiliesCellRenderer.class.getResource("/org/tip/puckgui/images/unknown-16x16.png"));
		}
	}

	/**
	 * 	
	 */
	@Override
	public Component getListCellRendererComponent(final JList list, final Object lineObject, final int index, final boolean isSelected,
			final boolean cellHasFocus) {

		//
		if (lineObject == null) {
			setText("");
		} else if (lineObject instanceof String) {
			setText((String) lineObject);
		} else {
			Family family = (Family) lineObject;

			StringBuffer buffer = new StringBuffer(192);
			buffer.append("(");
			buffer.append(family.getId());
			buffer.append(") ");

			if (family.getHusband() == null) {
				buffer.append("Unknown");
			} else {
				buffer.append(family.getHusband().getGender().toChar());
				buffer.append(" (");
				buffer.append(family.getHusband().getId());
				buffer.append(") ");
				buffer.append(family.getHusband().getName());
			}

			buffer.append(" - ");

			if (family.getWife() == null) {
				buffer.append("Unknown");
			} else {
				buffer.append(family.getWife().getGender().toChar());
				buffer.append(" (");
				buffer.append(family.getWife().getId());
				buffer.append(") ");
				buffer.append(family.getWife().getName());
			}

			setText(buffer.toString());

			// setIcon(ResourceManager.iconForGender(val.gender, 16));
			// setText(val.name);
		}

		//
		Color background = null;
		Color foreground = null;
		if (isSelected) {
			background = list.getSelectionBackground();
			foreground = list.getSelectionForeground();
		} else {
			background = list.getBackground();
			foreground = list.getForeground();
		}
		setBackground(background);
		setForeground(foreground);

		//
		return this;
	}
}
