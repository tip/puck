package org.tip.puck.net.relations.roles.io;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.io.bar.BARODSFile;
import org.tip.puck.io.bar.BARTXTFile;
import org.tip.puck.io.bar.BARXLSFile;
import org.tip.puck.io.iur.IURODSFile;
import org.tip.puck.io.iur.IURTXTFile;
import org.tip.puck.io.iur.IURXLSFile;
import org.tip.puck.io.puc.PUCFile;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.roles.RoleRelationMaker;
import org.tip.puck.report.Report;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class TermGenealogyFile {

	/**
	 * 
	 * @param source
	 * @param charsetName
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel load(final File source, final Report report) throws PuckException {
		RelationModel result;

		if (source == null) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("File=[" + source + "].");

		} else if (!source.exists()) {
			//
			throw PuckExceptions.FILE_NOT_FOUND.create("File=[" + source + "].");

		} else if (!source.isFile()) {
			//
			throw PuckExceptions.NOT_A_FILE.create("File=[" + source + "].");

		} else {

			String name = StringUtils.lowerCase(source.getName());

			if (name.endsWith(".term.puc")) {

				result = loadFromPUCFile(source, report);

			} else if (name.endsWith(".term.iur.txt")) {

				result = loadFromIURTXTFile(source, report);

			} else if (name.endsWith(".term.iur.ods")) {

				result = loadFromIURODSFile(source, report);

			} else if (name.endsWith(".term.iur.xls")) {

				result = loadFromIURXLSFile(source, report);

			} else if (name.endsWith(".term.bar.txt")) {

				result = loadFromBARTXTFile(source, report);

			} else if (name.endsWith(".term.bar.ods")) {

				result = loadFromBARODSFile(source, report);

			} else if (name.endsWith(".term.bar.xls")) {

				result = loadFromBARXLSFile(source, report);

			} else {
				//
				throw PuckExceptions.UNSUPPORTED_FILE_FORMAT.create("File=[" + source + "].");
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @param report
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel loadFromBARODSFile(final File file, final Report report) throws PuckException {
		RelationModel result;

		Net net = BARODSFile.load(file);
		result = RoleRelationMaker.create(net, report);

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @param report
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel loadFromBARTXTFile(final File file, final Report report) throws PuckException {
		RelationModel result;

		Net net = BARTXTFile.load(file);
		result = RoleRelationMaker.create(net, report);

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @param report
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel loadFromBARXLSFile(final File file, final Report report) throws PuckException {
		RelationModel result;

		Net net = BARXLSFile.load(file);
		result = RoleRelationMaker.create(net, report);

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @param report
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel loadFromIURODSFile(final File file, final Report report) throws PuckException {
		RelationModel result;

		Net net = IURODSFile.load(file);
		result = RoleRelationMaker.create(net, report);

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @param report
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel loadFromIURTXTFile(final File file, final Report report) throws PuckException {
		RelationModel result;

		Net net = IURTXTFile.load(file);
		result = RoleRelationMaker.create(net, report);

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @param report
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel loadFromIURXLSFile(final File file, final Report report) throws PuckException {
		RelationModel result;

		Net net = IURXLSFile.load(file);
		result = RoleRelationMaker.create(net, report);

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @param report
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel loadFromPUCFile(final File file, final Report report) throws PuckException {
		RelationModel result;

		Net net = PUCFile.load(file);
		result = RoleRelationMaker.create(net, report);

		//
		return result;
	}

}
