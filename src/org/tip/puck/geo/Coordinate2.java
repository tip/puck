package org.tip.puck.geo;

/**
 * 
 */
public class Coordinate2 {

	private double latitude;
	private double longitude;
	private double elevation;

	/**
	 * 
	 */
	public Coordinate2() {
		this(0, 0, 0);
	}

	/**
	 * 
	 */
	public Coordinate2(final double latitude, final double longitude) {
		this(latitude, longitude, 0);
	}

	/**
	 * 
	 */
	public Coordinate2(final double latitude, final double longitude, final double elevation) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.elevation = elevation;
	}

	public double getElevation() {
		return this.elevation;
	}

	public double getLatitude() {
		return this.latitude;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public void setElevation(final double elevation) {
		this.elevation = elevation;
	}

	public void setLatitude(final double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(final double longitude) {
		this.longitude = longitude;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isCoordinateValue(final String value) {
		boolean result;

		if (value == null) {

			result = false;

		} else {

			if (value.matches("-?\\d+(\\.\\d*)?")) {

				result = true;
			} else {

				result = false;
			}
		}

		//
		return result;
	}
}
