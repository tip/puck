package org.tip.puck.census.workers;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author TIP
 */
public class CensusDetail {

	private String label;

	private boolean report;

	private boolean diagram;

	/**
	 * 
	 */
	public CensusDetail() {
		this.label = "SIMPLE";
		this.report = false;
		this.diagram = false;
	}

	/**
	 * 
	 */
	public CensusDetail(final String label, final boolean report, final boolean diagram) {
		this.label = label;
		this.report = report;
		this.diagram = diagram;
	}

	public String getLabel() {
		return label;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isAvailable() {
		boolean result;

		if ((StringUtils.isBlank(label) || ((!this.report) && (!this.diagram)))) {
			result = false;
		} else {
			result = true;
		}

		//
		return result;
	}

	public boolean isDiagram() {
		return diagram;
	}

	public boolean isReport() {
		return report;
	}

	public void setDiagram(final boolean diagram) {
		this.diagram = diagram;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public void setReport(final boolean report) {
		this.report = report;
	}

}
