package org.tip.puck.io.kinsources;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.Security;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.io.puc.PUCFile;
import org.tip.puck.net.Net;

import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLBadFormatException;
import fr.devinsy.util.xml.XMLZipReader;

/**
 * This class represents a PUC File reader and writer.
 * 
 * @author TIP
 */
public class KinsourcesFile {

	private static final Logger logger = LoggerFactory.getLogger(KinsourcesFile.class);

	public static String DEFAULT_CHARSET_NAME = "UTF-8";

	private static final String KINSOURCES_URL = "https://www.kinsources.net/";
	private static final String KINSOURCES_CATALOG_URL = KINSOURCES_URL + "kidarep/catalog.xhtml";
	private static final String KINSOURCES_DATASET_FILE_URL = KINSOURCES_URL + "kidarep/dataset_public_file-/%d/for_puck.puc";

	/**
	 * 
	 * @param source
	 * @return
	 */
	private static String extractFilename(final URLConnection source) {
		String result;

		/* Example: content-disposition=attachment; filename="kinsources-ainu_1880_as01.puc" */

		if (source == null) {
			//
			result = null;

		} else {
			//
			String contentDisposition = source.getHeaderField("Content-Disposition");
			logger.debug("content-disposition=[{}]", contentDisposition);

			if (contentDisposition == null) {
				//
				result = null;

			} else {
				//
				Matcher matcher = Pattern.compile("filename=\"(.*)\"").matcher(contentDisposition);
				if ((matcher.find()) && (matcher.groupCount() == 1)) {
					//
					result = matcher.group(1);

				} else {
					//
					result = null;
				}
			}
		}

		logger.debug("fileName=[{}]", result);

		//
		return result;
	}

	/**
	 * This method fixes the prime size limit in Java 6, which is no more
	 * compatible with the SSL certificate of Kinsources web site since January
	 * 2016.
	 * <p>
	 * Issue comes from the prime size. The maximum-acceptable size that Java 6
	 * accepts is 1024 bits. This is a known issue (see JDK-6521495). This was
	 * reported as bug JDK-7044060 and fixed recently.
	 * <p>
	 * The solution requires the use of the following jar file:
	 * <ul>
	 * <li>bcprov-jdk15on-152.jar</li>
	 * <li>bcprov-ext-jdk15on-152.jar</li>
	 * </ul>
	 * <p>
	 * Then add the <code>BouncyCastleProvider</code> in the
	 * <code>Security</code> class.
	 * 
	 */
	public static void fixPrimeSizeLimitInJava6() {
		//
		Security.addProvider(new BouncyCastleProvider());
	}

	/**
	 * Loads a PUC file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	public static Net load(final long id) throws PuckException {
		Net result;

		String datasetURL = String.format(KINSOURCES_DATASET_FILE_URL, id);
		URLConnection connection = null;
		try {
			// Download file.
			//
			URL source = new URL(datasetURL);

			connection = source.openConnection();
			connection.setConnectTimeout(25000);
			connection.setReadTimeout(25000);

			//
			String fileName = extractFilename(connection);

			if (StringUtils.isBlank(fileName)) {
				fileName = "noname.puc";
			}

			//
			byte[] data = IOUtils.toByteArray(connection);
			logger.info("Kinsources dataset file size=" + data.length);

			// Load net.
			result = PUCFile.read(new XMLZipReader(new ByteArrayInputStream(data)));

			result.setLabel(fileName);

		} catch (MalformedURLException exception) {
			//
			exception.printStackTrace();
			result = null;

		} catch (IOException exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.IO_ERROR.create("Error reading file [" + datasetURL + "]: " + exception.getMessage(), exception);

		} catch (XMLStreamException exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.BAD_FILE_FORMAT.create("Unexpected process error on [" + datasetURL + "]:" + exception.getMessage(), exception);

		} catch (XMLBadFormatException exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.BAD_FILE_FORMAT.create("Bad XML format in file [" + datasetURL + "]: " + exception.getMessage(), exception);

		} finally {
			//
			IOUtils.close(connection);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static Catalog loadCatalog() {
		Catalog result;

		//
		URLConnection connection = null;
		try {
			// Load catalog file.
			URL source = new URL(KINSOURCES_CATALOG_URL);

			connection = source.openConnection();
			connection.setConnectTimeout(15000);
			connection.setReadTimeout(15000);

			byte[] data = IOUtils.toByteArray(connection);
			logger.info("Kinsources catalog size=" + data.length);

			//
			StringList lines = toStringList(data, DEFAULT_CHARSET_NAME);

			//
			Iterator<String> iterator = lines.iterator();

			// Read header.
			{
				boolean ended = false;
				while (!ended) {
					if (iterator.hasNext()) {
						//
						String line = iterator.next();

						if (StringUtils.isBlank(line)) {
							//
							ended = true;
						}
					} else {
						//
						ended = true;
					}
				}
			}

			// Read column names.
			if (!iterator.hasNext()) {
				//
				result = null;

			} else {
				//
				String labelsLine = iterator.next();
				String[] labels = labelsLine.split("\t");

				//
				result = new Catalog();
				result.setSource(KINSOURCES_CATALOG_URL);

				//
				boolean ended = false;
				while (!ended) {
					//
					if (iterator.hasNext()) {
						//
						String line = iterator.next();

						if (StringUtils.isNotBlank(line)) {
							//
							CatalogItem item = new CatalogItem();

							//
							String[] values = line.split("\t");

							//
							for (int index = 0; index < values.length; index++) {
								//
								String label = labels[index];
								String value = values[index];

								//
								if (label.equalsIgnoreCase("ID")) {
									//
									if (NumberUtils.isDigits(value)) {
										//
										item.setId(Long.parseLong(value));

									} else {
										//
										throw new IllegalArgumentException("ID is not a number.");
									}

								} else if (label.equalsIgnoreCase("Name")) {
									//
									item.setName(value);

								} else if (label.equalsIgnoreCase("Contributor")) {
									//
									item.setContributor(value);

								} else if (label.equalsIgnoreCase("Author")) {
									//
									item.setAuthor(value);

								} else if (label.equalsIgnoreCase("Permanent link")) {
									//
									item.setPermanentLink(value);

								} else if (label.equalsIgnoreCase("License")) {
									//
									item.setLicense(value);

								} else if (label.equalsIgnoreCase("Publication date")) {
									//
									item.setPublicationDate(value);

								} else if (label.equalsIgnoreCase("Individuals")) {
									//
									if (NumberUtils.isDigits(value)) {
										//
										item.setIndividualCount(Long.parseLong(value));
									}
								}
							}

							//
							result.add(item);
						}
					} else {
						//
						ended = true;
					}
				}
			}

		} catch (MalformedURLException exception) {
			//
			exception.printStackTrace();
			result = null;

		} catch (IOException exception) {
			//
			exception.printStackTrace();
			result = null;

		} finally {
			//
			IOUtils.close(connection);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param charsetName
	 * @return
	 * @throws IOException
	 */
	public static StringList toStringList(final byte[] source, final String charsetName) throws IOException {
		StringList result;

		result = toStringList(new ByteArrayInputStream(source), charsetName);

		// Note: no need to close the input stream.

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param charsetName
	 * @return
	 * @throws IOException
	 */
	public static StringList toStringList(final InputStream source, final String charsetName) throws IOException {
		StringList result;

		BufferedReader in = null;
		try {
			//
			in = new BufferedReader(new InputStreamReader(source, charsetName));

			//
			boolean ended = false;
			result = new StringList();
			while (!ended) {
				//
				String line = in.readLine();
				if (line == null) {
					//
					ended = true;

				} else {
					//
					result.append(line);
				}
			}

		} finally {
			//
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}
}
