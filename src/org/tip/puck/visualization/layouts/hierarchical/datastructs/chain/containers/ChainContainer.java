package org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.containers;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.gephi.graph.api.Node;
import org.tip.puck.visualization.layouts.GraphSource;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.ChainCluster;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.ColoredChain;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.NodeLayoutData;

/**
 * @date Jun 24, 2012
 * @author Edoardo Savoia
 */
public class ChainContainer {
    final ColoredChain chain;
    protected Set<ChainContainer> children = new HashSet<ChainContainer>();
    protected ChainCluster rootCluster = null;
    protected int origin;
    
    public void setRootCluster(ChainCluster rootCluster) {
	this.rootCluster = rootCluster;
    }

    public ChainCluster getRootCluster() {
	return rootCluster;
    }

    protected ChainContainer(ColoredChain chain) {
	this.chain = chain;
    }

    public final static ChainContainer getContainer(Node root,final GraphSource graphSource){
	ColoredChain chain = ColoredChain.createDownChain(root, graphSource);
	ChainContainer result = null;

	final Node bottomNode = chain.getBottomNode();
	Set<Node> children = graphSource.getChildren(bottomNode);
	if (children.isEmpty()) {
	    result = new LeafContainer(chain);
	} else {
	    Iterator<Node> iterator = children.iterator();
	    boolean canGo = true;
	    for (; iterator.hasNext()&&canGo;) {
		Node child = iterator.next();
		if (NodeLayoutData.isColored(child, NodeLayoutData.Color.RED)){
		    canGo = false;
		}
	    }
	    if (!canGo) {
		result = new BorderContainer(chain);
	    } else {
		result = new ChainContainer(chain);
	    }
	}
	return result;
    }

    public void populateChildren(final GraphSource graphSource){
	// children = new HashSet<ChainContainer>();
	final Node bottomNode = chain.getBottomNode();
	Set<Node> bottomChildren = graphSource.getChildren(bottomNode);
	for (Node child : bottomChildren) {
	    if (NodeLayoutData.isColored(child, NodeLayoutData.Color.RED)) {
		System.out.println("Error ChainContainer has Red Child");
	    } else {
		ChainContainer container = getContainer(child, graphSource);
		container.setRootCluster(rootCluster);
		container.setOrigin(origin+chain.getHeight()+1);
		if (container instanceof BorderContainer) {
		    rootCluster.addBorder((BorderContainer) container);
		}
		this.children.add(container);
	    }
	}
	// propagate
	for (ChainContainer child : this.children) {
	    child.populateChildren(graphSource);
	}
    }

    public boolean isLeaf() {
	return false;
    }

    public ColoredChain getChain() {
	return chain;
    }

    public Set<ChainContainer> getChildren() {
	if (isLeaf()) {
	    return Collections.emptySet();
	}
	return children;
    }

    public void setOrigin(int origin) {
	this.origin = origin;
    }

    public int getOrigin() {
	return origin;
    }

    public int getHeight(){
	int result = chain.getHeight();
	int maxHeight = 0;
	if (!isLeaf()) {
	    for (ChainContainer child : children) {
		maxHeight = Math.max(maxHeight, child.getHeight());
	    }
	}
	return result+maxHeight;
    }

    public int getWidth(){
	int width = 0;
	if (!isLeaf()) {
	    for (ChainContainer child : children) {
		width +=child.getWidth();
	    }
	}
	return Math.max(1, width);
    }

    public int getSize(){
	int size = chain.getHeight();
	for (ChainContainer chainContainer : children) {
	    size+=chainContainer.getSize();
	}
	return size;
    }

    public int getArea(){
	return getWidth()*getHeight();
    }

    public float getDensity(){
	return (getSize()*1.0f)/(getArea()*1.0f);
    }

}
