package org.tip.puckgui.views.geographyEditor;

import javax.swing.table.AbstractTableModel;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.geo.Place;
import org.tip.puck.geo.Places;
import org.tip.puck.net.workers.AttributeValueDescriptors;

/**
 * 
 * @author TIP
 */
public class PlaceLineModel extends AbstractTableModel {

	private static final long serialVersionUID = 3963110056313246593L;

	private static final Logger logger = LoggerFactory.getLogger(PlaceLineModel.class);

	public static final int COLUMN_TOPONYM = 0;
	public static final int COLUMN_USES = 1;
	public static final int COLUMN_LATITUDE = 2;
	public static final int COLUMN_LONGITUDE = 3;
	public static final int COLUMN_ELEVATION = 4;
	public static final int COLUMN_LEVEL = 5;
	public static final int COLUMN_ABOVE = 6;
	public static final int COLUMN_EXTRA_DATA = 7;
	public static final int COLUMN_ALTERNATE_TOPONYMS = 8;
	public static final int COLUMN_COMMENT = 9;
	public static final int COLUMN_COUNT = 10;

	private Places source;
	private AttributeValueDescriptors valueDescriptors;
	private int lastSearchIndex;
	private String lastSearchPattern;

	/**
	 * 
	 */
	public PlaceLineModel(final Places source, final AttributeValueDescriptors valueDescriptors) {
		//
		super();

		this.valueDescriptors = valueDescriptors;

		//
		setSource(source);
	}

	/**
	 * 
	 */
	@Override
	public Class getColumnClass(final int columnIndex) {
		Class result;

		switch (columnIndex) {
			case COLUMN_TOPONYM:
				result = String.class;
			break;
			case COLUMN_USES:
				result = Long.class;
			break;
			case COLUMN_LATITUDE:
				result = Double.class;
			break;
			case COLUMN_LONGITUDE:
				result = Double.class;
			break;
			case COLUMN_ELEVATION:
				result = Double.class;
			break;
			case COLUMN_LEVEL:
				result = GeoLevel.class;
			break;
			case COLUMN_ABOVE:
				result = String.class;
			break;
			case COLUMN_EXTRA_DATA:
				result = String.class;
			break;
			case COLUMN_ALTERNATE_TOPONYMS:
				result = String.class;
			break;
			case COLUMN_COMMENT:
				result = String.class;
			break;
			default:
				result = String.class;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getColumnCount() {
		int result;

		result = COLUMN_COUNT;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String getColumnName(final int columnIndex) {
		String result;

		switch (columnIndex) {
			case COLUMN_TOPONYM:
				result = "Toponym";
			break;

			case COLUMN_USES:
				result = "Uses";
			break;

			case COLUMN_LATITUDE:
				result = "Latitude";
			break;

			case COLUMN_LONGITUDE:
				result = "Longitude";
			break;

			case COLUMN_ELEVATION:
				result = "Elevation";
			break;

			case COLUMN_LEVEL:
				result = "Level";
			break;

			case COLUMN_ABOVE:
				result = "Above";
			break;

			case COLUMN_EXTRA_DATA:
				result = "Extra Data";
			break;

			case COLUMN_ALTERNATE_TOPONYMS:
				result = "Alternate Toponyms";
			break;

			case COLUMN_COMMENT:
				result = "Comment";
			break;

			default:
				result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getRowCount() {
		int result;

		if (this.source == null) {

			result = 0;

		} else {

			result = this.source.size();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Places getSource() {
		Places result;

		result = this.source;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Object getValueAt(final int rowIndex, final int columnIndex) {
		Object result;

		Place line = this.source.get(rowIndex);

		if (line == null) {

			result = null;
		} else {

			switch (columnIndex) {
				case COLUMN_TOPONYM:
					result = line.getToponym();
				break;

				case COLUMN_USES:
					result = this.valueDescriptors.getCountOf(line.getToponyms());
				break;

				case COLUMN_LATITUDE:
					result = line.getLatitude();
				break;

				case COLUMN_LONGITUDE:
					result = line.getLongitude();
				break;

				case COLUMN_ELEVATION:
					result = line.getElevation();
				break;

				case COLUMN_LEVEL:
					result = line.getGeoLevel();
				break;

				case COLUMN_ABOVE:
					result = line.getSuperiorToponym();
				break;

				case COLUMN_EXTRA_DATA:
					result = line.getExtraData();
				break;

				case COLUMN_ALTERNATE_TOPONYMS:
					result = line.getHomonyms().toStringSeparatedBy(";");
				break;

				case COLUMN_COMMENT:
					result = line.getComment();
				break;

				default:
					result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public int indexOf(final Place target) {
		int result;

		if (this.source == null) {
			//
			result = -1;

		} else {

			result = this.source.indexOf(target);
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public boolean isCellEditable(final int row, final int col) {
		boolean result;

		switch (col) {
			case COLUMN_TOPONYM:
			case COLUMN_LATITUDE:
			case COLUMN_LONGITUDE:
			case COLUMN_ELEVATION:
			case COLUMN_LEVEL:
			case COLUMN_ABOVE:
			case COLUMN_EXTRA_DATA:
			case COLUMN_ALTERNATE_TOPONYMS:
			case COLUMN_COMMENT:
				result = true;
			break;

			case COLUMN_USES:
			default:
				result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isSet() {
		boolean result;

		if (this.source == null) {

			result = false;
		} else {

			result = true;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param pattern
	 */
	public Place nextSearched(final String pattern) {
		Place result;

		result = null;

		//
		return result;
	}

	/**
	 * 
	 * @param pattern
	 */
	public int nextSearchedIndex(final String pattern) {
		int result;

		result = indexOf(nextSearched(pattern));

		//
		return result;
	}

	/**
	 * 
	 */
	public Place removeItem(final int rowIndex) {
		Place result;

		if ((rowIndex >= 0) && (rowIndex < this.source.size())) {

			result = this.source.get(rowIndex);
			this.source.remove(result);

			fireTableRowsDeleted(rowIndex, rowIndex);

		} else {

			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public void resetSearch() {
		this.lastSearchIndex = -1;
		this.lastSearchPattern = null;
	}

	/**
	 * 
	 */
	public void setNewItem(final Place newPlace) {

		this.source.add(newPlace);

		fireTableRowsInserted(this.source.size(), this.source.size());
	}

	/**
	 * 
	 * @param source
	 */
	public void setSource(final Places source) {
		//
		resetSearch();

		// Set an empty JTable.
		if (this.source != null) {
			//
			fireTableRowsDeleted(0, this.source.size());
		}

		//
		if (source == null) {
			//
			this.source = new Places();

		} else {
			//
			this.source = source;
			this.source.sortByToponym();
		}

		//
		fireTableDataChanged();
	}

	/**
	 * 
	 */
	@Override
	public void setValueAt(final Object value, final int rowIndex, final int columnIndex) {
		logger.debug("setValueAt " + rowIndex + " " + columnIndex);

		Place current = this.source.get(rowIndex);

		switch (columnIndex) {
			case COLUMN_TOPONYM: {
				//
				if (StringUtils.isNotBlank((String) value)) {
					//
					current.setToponym(StringUtils.trim((String) value));
					this.source.sortByToponym();
				}
			}
			break;

			case COLUMN_LATITUDE:
				current.setLatitude((Double) value);
			break;

			case COLUMN_LONGITUDE:
				current.setLongitude((Double) value);
			break;

			case COLUMN_ELEVATION:
				current.setElevation((Double) value);
			break;

			case COLUMN_LEVEL:
				current.setGeoLevel((GeoLevel) value);
			break;

			case COLUMN_ABOVE:
				current.setSuperiorToponym((String) value);
			break;

			case COLUMN_EXTRA_DATA:
				current.setExtraData((String) value);
			break;

			case COLUMN_ALTERNATE_TOPONYMS:
				current.setAlternateToponyms((String) value);
			break;

			case COLUMN_COMMENT:
				current.setComment((String) value);
			break;

			case COLUMN_USES:
			default:
		}

		//
		fireTableDataChanged();
	}
}
