package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.Attributes;
import org.tip.puckgui.NetGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class CorpusPanel extends JPanel {

	private static final long serialVersionUID = 1990081342527870857L;

	private static final Logger logger = LoggerFactory.getLogger(CorpusPanel.class);

	int reportCounter = 0;
	private JPanel thisJPanel;
	private NetGUI netGUI;
	JLabel lblFileNameValue;
	JLabel lblFileDateValue;
	JLabel lblFileSizeValue;
	JLabel lblIndividualCount;
	JLabel lblFamilyCount;
	private AttributesPanel attributesPanel;
	private JLabel lblRelationCount;
	private JLabel lblRelationModelCount;

	/**
	 * 
	 * @param guiManager
	 */
	public CorpusPanel(final NetGUI guiManager) {
		//
		this.thisJPanel = this;
		this.netGUI = guiManager;
		setVisible(true);
		setLayout(new BorderLayout(0, 0));

		JPanel generalPanel = new JPanel();
		add(generalPanel, BorderLayout.NORTH);
		generalPanel.setLayout(new BoxLayout(generalPanel, BoxLayout.Y_AXIS));

		JPanel corpusStaticDataPanel = new JPanel();
		corpusStaticDataPanel.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "General", TitledBorder.LEADING, TitledBorder.TOP, null,
				new Color(51, 51, 51)));
		corpusStaticDataPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		generalPanel.add(corpusStaticDataPanel);
		corpusStaticDataPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblFileName = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("CorpusPanel.lblFileName.text")); //$NON-NLS-1$ //$NON-NLS-2$
		lblFileName.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(lblFileName, "2, 2");

		this.lblFileNameValue = new JLabel("-");
		this.lblFileNameValue.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(this.lblFileNameValue, "4, 2");

		JLabel lblFileDate = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("CorpusPanel.lblFileDate.text")); //$NON-NLS-1$ //$NON-NLS-2$
		lblFileDate.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(lblFileDate, "2, 4");

		this.lblFileDateValue = new JLabel("-");
		this.lblFileDateValue.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(this.lblFileDateValue, "4, 4");

		JLabel lblFileSize = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("CorpusPanel.lblFileSize.text")); //$NON-NLS-1$ //$NON-NLS-2$
		lblFileSize.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(lblFileSize, "2, 6");

		this.lblFileSizeValue = new JLabel("-");
		this.lblFileSizeValue.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(this.lblFileSizeValue, "4, 6");

		JLabel lblIndividuals = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("CorpusPanel.lblIndividuals.text")); //$NON-NLS-1$ //$NON-NLS-2$
		lblIndividuals.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(lblIndividuals, "2, 8");

		this.lblIndividualCount = new JLabel("-");
		this.lblIndividualCount.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(this.lblIndividualCount, "4, 8");

		JLabel lblFamilies = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("CorpusPanel.lblFamilies.text")); //$NON-NLS-1$ //$NON-NLS-2$
		lblFamilies.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(lblFamilies, "2, 10");

		this.lblFamilyCount = new JLabel("-");
		this.lblFamilyCount.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(this.lblFamilyCount, "4, 10");

		JLabel lblRelationModels = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("CorpusPanel.lblRelationModels.text")); //$NON-NLS-1$ //$NON-NLS-2$
		lblRelationModels.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(lblRelationModels, "2, 12");

		this.lblRelationModelCount = new JLabel("-");
		this.lblRelationModelCount.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(this.lblRelationModelCount, "4, 12");

		JLabel lblRelations = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("CorpusPanel.lblRelations.text")); //$NON-NLS-1$ //$NON-NLS-2$
		lblRelations.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(lblRelations, "2, 14");

		this.lblRelationCount = new JLabel("-");
		this.lblRelationCount.setFont(new Font("Dialog", Font.PLAIN, 12));
		corpusStaticDataPanel.add(this.lblRelationCount, "4, 14");

		Component verticalStrut_10 = Box.createVerticalStrut(10);
		generalPanel.add(verticalStrut_10);

		JSeparator separator_5 = new JSeparator();
		generalPanel.add(separator_5);

		Component verticalStrut_9 = Box.createVerticalStrut(10);
		generalPanel.add(verticalStrut_9);

		this.attributesPanel = new AttributesPanel(this.netGUI, (Attributes) null, null);
		add(this.attributesPanel, BorderLayout.CENTER);
	}

	/**
	 * 
	 */
	public void update() {
		this.lblFileNameValue.setText(this.netGUI.getFile().getName());
		this.lblFileNameValue.setToolTipText(this.netGUI.getFile().getAbsolutePath());
		//
		long time = this.netGUI.getFile().lastModified();
		String timeText;
		if (time == 0) {
			timeText = "-";
		} else {
			timeText = (new SimpleDateFormat("dd/MM/yyyy HH':'mm")).format(new Date(time));
		}
		this.lblFileDateValue.setText(timeText);

		this.lblFileSizeValue.setText(this.netGUI.getFile().length() + " octets");
		this.lblIndividualCount.setText(String.valueOf(this.netGUI.getNet().individuals().size()));
		this.lblFamilyCount.setText(String.valueOf(this.netGUI.getNet().families().size()));
		this.lblRelationModelCount.setText(String.valueOf(this.netGUI.getNet().relationModels().size()));
		this.lblRelationCount.setText(String.valueOf(this.netGUI.getNet().relations().size()));

		//
		this.attributesPanel.setSource(this.netGUI.getNet().attributes());
	}

}
