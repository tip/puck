package org.tip.puck.graphs;

import java.util.Comparator;

/**
 * 
 * @author TIP
 * 
 */
public class NodeComparatorByLabel<E> implements Comparator<Node<E>> {
	/**
	 * 
	 */
	@Override
	public int compare(final Node<E> alpha, final Node<E> bravo) {
		int result;

		//
		String aphaLabel;
		if (alpha == null) {
			aphaLabel = null;
		} else {
			aphaLabel = alpha.getLabel();
		}

		//
		String bravoLabel;
		if (bravo == null) {
			bravoLabel = null;
		} else {
			bravoLabel = bravo.getLabel();
		}

		//
		if ((aphaLabel == null) && (bravoLabel == null)) {
			result = 0;
		} else if (aphaLabel == null) {
			result = -1;
		} else if (bravoLabel == null) {
			result = +1;
		} else {
			result = aphaLabel.compareTo(bravoLabel);
		}

		//
		return result;
	}
}
