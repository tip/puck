package org.tip.puck.sequences.workers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.geo.Geography;
import org.tip.puck.geo.Geography;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.workers.ExpansionMode;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionCriteria.PartitionType;
import org.tip.puck.partitions.PartitionCriteria.SizeFilter;
import org.tip.puck.partitions.PartitionCriteria.ValueFilter;
import org.tip.puck.partitions.PartitionCriteriaList;
import org.tip.puck.partitions.PartitionMaker;
import org.tip.puck.util.ToolBox;

import fr.devinsy.util.StringList;

/**
 * 
 * @author Klaus Hamberger
 * 
 */
public class SequenceCriteria {

	public enum CensusType {
		GENERAL,
		EGONETWORKS,
		SEQUENCENETWORKS,
		PARCOURSSIMILARITYNETWORKS,
		PARCOURSINTERSECTIONNETWORKS,
		PARCOURS;

		@Override
		public String toString() {
			String result;

			switch (this) {
				case GENERAL:
					result = "General";
				break;
				case EGONETWORKS:
					result = "Ego Networks";
				break;
				case SEQUENCENETWORKS:
					result = "Sequence Networks";
				break;
				case PARCOURSINTERSECTIONNETWORKS:
					result = "Parcours Intersection Networks";
				break;
				case PARCOURSSIMILARITYNETWORKS:
					result = "Parcours Similarity Networks";
				break;
				case PARCOURS:
					result = "Parcours";
				break;
				default:
					result = null;
			}

			//
			return result;
		}
	}

	public enum EgoNetworksOperation {
		GENERAL,
		CENTRALITY,
		COHESION,
		RELATIONS,
		EXPORT_EGO_NETWORKS
	}

	public enum ParcoursNetworksOperation {
		GENERAL,
		CENTRALITY,
		COHESION, // change
		RELATIONS, // change
		EXPORT_PARCOURS,
		EXPORT_EXTENDED_PARCOURS,
		EXPORT_MULTIPLE_PARCOURS	}

	public enum ParcoursIntersectionNetworksOperation {
		GENERAL,
		CENTRALITY,
		COHESION,
		RELATIONS,
		EXPORT_PARCOURS_INTERSECTION_NETWORKS,
		EXPORT_SIMILARY_TREES
	}

	public enum ParcoursSimilarityNetworksOperation {
		GENERAL,
		CENTRALITY,
		COHESION,
		RELATIONS,
		EXPORT_PARCOURS_SIMILARITY_NETWORKS,
		EXPORT_SIMILARY_TREES
	}

	public enum ValueSequenceLabel {
		TYPEDID,
		HOST,
		MIG,
		HOSTMIG,
		MIGRATIONTYPE,
		CHILDMIGRATIONTYPE,
		TREES,
		DISTANCE,
		PLACE,
		REGION,
		MOVEMENT,
		TURNOVER,
		COMPONENTS,
		DATE,
		AGE
	}

	public enum SequenceGeneralStatistics {
		EVENTS,
		AGE,
		RELATIONS
	}

	public enum SequenceReportType {
		ITINERARIES_SURVEY,
		ITINERARIES_DETAILS,
		BIOGRAPHIES,
		EXTENDED_BIOGRAPHIES,
		ACTOR_EVENT_TABLES,
		INTERACTION_TABLES,
		EXPORT_RELATION_GRAPH
	}

	public enum SliceGeneralStatistics {
		METRICS,
		MORPHOLOGY,
		POPULATION,
		POSITIONS,
		DYNAMICS,
		CIRCUITS
	}

	public enum SliceReportType {
		MEMBERSHIP,
		TURNOVER
	}

	public enum TrajectoriesOperation {
		GENERAL,
		LIST_TREES,
		EXPORT_AGGREGATE_SEQUENCE_NETWORK,
		EXPORT_SEQUENCE_TYPE_NETWORK,
		DRAW,
		EXPORT_SEQUENCE_NETWORKS,
		EXPORT_SIMILARY_TREES

	}
	
/*	public enum ReferentNetworkOperation {
		GROUP_METRICS,
		GROUP_MORPHOLOGY,
		INDIVIDUAL_POSITIONS,
		CIRCUITS,
		DIFFERENTIAL_CENSUS
	}*/

	// General criteria for space time analysis

	private String relationModelName;

	// Attribute labels

	private String dateLabel;
	private String startDateLabel;
	private String endDateLabel;
	private String placeLabel;
	private String startPlaceLabel;
	private String endPlaceLabel;
	private String localUnitLabel;
	private String groupAffiliationLabel;
	private String referentRelationLabel;
	private String impersonalAlterLabel;

	// Role names

	private String egoRoleName;
	private String defaultReferentRoleName;
	private StringList roleNames;

	// Time criteria

	private int maxAge;
	private int minAge;
	private int threshold;
	private Integer[] dates;
	private int referenceYear;

	// Geography criteria

	private Geography geography;
	private GeoLevel level;
	private StringList minimalPlaceNames;

	// Parameters for sequence expansion

	private ExpansionMode expansionMode;
	private FiliationType filiationType;

	// General census criteria

	private CensusType censusType;

	// Circuit census criteria

	private String pattern;
	private String chainClassification;
	private List<String> relationModelNames;

	// Partition criteria

	private String individualClassificationType;
	private List<ValueSequenceLabel> valueSequenceLabels;
	private List<ValueSequenceLabel> mainValueSequenceLabels; // for
																				// parcours
																				// networks

	private List<ValueSequenceLabel> trajectoriesRelationClassificationTypes;

	// Census criteria

	private List<String> sequenceValueLabels;
	private List<String> networkTitles;
	private Map<String, List<String>> partitionLabels;
	
	private PartitionCriteriaList valueSequenceCriteriaList;
	private PartitionCriteriaList sequenceValueCriteriaList;
	
	// Node statistics criteria
	
	private List<String> nodeStatisticsLabels;

	// private double threshold;

	// Filter for alters in ego and parcours intersection networks

	private String alterFilterRoleName;
	private String alterFilterAttributeLabel;
	private String alterFilterAttributeValue;

	//
	private List<SequenceReportType> sequenceReportTypes;
	private List<SliceReportType> sliceReportTypes;
	private List<SequenceGeneralStatistics> sequenceGeneralStatistics;
	private List<SliceGeneralStatistics> sliceGeneralStatistics;
	private List<EgoNetworksOperation> egoNetworksOperations;
	private List<TrajectoriesOperation> trajectoriesOperations;
	private List<ParcoursNetworksOperation> parcoursNetworkdsOperations;
	private List<ParcoursIntersectionNetworksOperation> parcoursIntersectionNetworksOperations;
	private List<ParcoursSimilarityNetworksOperation> parcoursSimilarityNetworksOperations;
//	private List<ReferentNetworkOperation> referentNetworkOperations;
		
	/**
	 * 
	 */
	public SequenceCriteria() {
		//
		this.sequenceReportTypes = new ArrayList<SequenceReportType>();
		this.sliceReportTypes = new ArrayList<SliceReportType>();
		this.sequenceGeneralStatistics = new ArrayList<SequenceGeneralStatistics>();
		this.sliceGeneralStatistics = new ArrayList<SliceGeneralStatistics>();
		this.egoNetworksOperations = new ArrayList<EgoNetworksOperation>();
		this.trajectoriesRelationClassificationTypes = new ArrayList<ValueSequenceLabel>();
		this.trajectoriesOperations = new ArrayList<TrajectoriesOperation>();
		this.parcoursNetworkdsOperations = new ArrayList<ParcoursNetworksOperation>();
		this.parcoursIntersectionNetworksOperations = new ArrayList<ParcoursIntersectionNetworksOperation>();
		this.parcoursSimilarityNetworksOperations = new ArrayList<ParcoursSimilarityNetworksOperation>();
//		this.referentNetworkOperations = new ArrayList<ReferentNetworkOperation>();
		this.valueSequenceCriteriaList = new PartitionCriteriaList();
		this.sequenceValueCriteriaList = new PartitionCriteriaList();

		//
		setDefaultCriteria();
	}
	
	public void setCensusParameters(){
		
		// Set Census Operation Labels
		
		this.sequenceValueLabels = new ArrayList<String>();

		this.sequenceValueLabels.add("NREVENTS");

		if (!getSequenceGeneralStatistics().isEmpty()){
			if (!this.valueSequenceLabels.contains(ValueSequenceLabel.PLACE)) {
				this.valueSequenceLabels.add(ValueSequenceLabel.PLACE);
			}
			if (!this.valueSequenceLabels.contains(ValueSequenceLabel.DISTANCE)) {
				this.valueSequenceLabels.add(ValueSequenceLabel.DISTANCE);
			}
		}

		if (getSequenceGeneralStatistics().contains(SequenceGeneralStatistics.EVENTS)){

			this.sequenceValueLabels.add("NREXTERNALMOVES#PLACE_"+level);
			this.sequenceValueLabels.add("MAX_DISTANCE");
			this.sequenceValueLabels.add("MEAN_NR_MOVES");
			
			for (ValueSequenceLabel type : this.valueSequenceLabels) {
				if (type == ValueSequenceLabel.PLACE){
					this.sequenceValueLabels.add("SUPPORT#" + placeLabel+"_"+level);
					this.sequenceValueLabels.add("PROFILE#" + placeLabel+"_"+level);
				} else {
					this.sequenceValueLabels.add("SUPPORT#" + type);
				}
			}
		}
		
		if (getSequenceGeneralStatistics().contains(SequenceGeneralStatistics.AGE)){
			this.sequenceValueLabels.add("AGEFIRST_DISTANCE");
			this.sequenceValueLabels.add("AGEFIRST_CHILDMIGRATIONTYPE_NOPARENTS");
			this.sequenceValueLabels.add("AGEFIRST_DISTANCEDYNAMIC_TRANSNATIONAL");
		}
		
		if (getSequenceGeneralStatistics().contains(SequenceGeneralStatistics.RELATIONS)){
			this.sequenceValueLabels.add("SAMESEXALTERS#ALL");
			this.sequenceValueLabels.add("SAMEPLACEALTERS#ALL");

			for (String roleName : this.roleNames) {
				this.sequenceValueLabels.add("NRALTERS#" + roleName);
				this.sequenceValueLabels.add("NRALTERSPEREVENT#" + roleName);
			}
			
			this.sequenceValueLabels.add("MEAN_COVERAGE");
			this.sequenceValueLabels.add("MAX_COVERAGE");

			for (String roleName : this.roleNames) {
				this.sequenceValueLabels.add("RELATIONS#" + roleName);
			}

			this.sequenceValueLabels.add("MAIN_ALTERS");
			this.sequenceValueLabels.add("MAIN_RELATIONS");

		}
		
		if (getEgoNetworksOperations().contains(EgoNetworksOperation.GENERAL)){
			this.sequenceValueLabels.add("SIZE");
			this.sequenceValueLabels.add("TIES");
			this.sequenceValueLabels.add("DENSITY");
			this.sequenceValueLabels.add("DENSITY_NOLOOPS");
			this.sequenceValueLabels.add("MEANDEGREE");
			this.sequenceValueLabels.add("MEANDEGREE_NOLOOPS");
			this.sequenceValueLabels.add("MEANDEGREE_NOLOOPS_NORM");
		}
		
		if (getEgoNetworksOperations().contains(EgoNetworksOperation.CENTRALITY)){
			this.sequenceValueLabels.add("EGO-BETWEENNESS");
			this.sequenceValueLabels.add("MEAN_BETWEENNESS");
			this.sequenceValueLabels.add("MAX_BETWEENNESS");
			this.sequenceValueLabels.add("ECCENTRICITY");
		}
		
		if (getEgoNetworksOperations().contains(EgoNetworksOperation.COHESION)){
			this.sequenceValueLabels.add("NRCOMPONENTS");
			this.sequenceValueLabels.add("NRISOLATES");
			this.sequenceValueLabels.add("MAXCOMPONENT");
			this.sequenceValueLabels.add("NRCOMPONENTS_NORM");
			this.sequenceValueLabels.add("NRISOLATES_NORM");
			this.sequenceValueLabels.add("MAXCOMPONENT_NORM");
			this.sequenceValueLabels.add("CONCENTRATION");
			this.sequenceValueLabels.add("BROKERAGE");
			this.sequenceValueLabels.add("EFFICIENT_SIZE");
			this.sequenceValueLabels.add("EFFICIENCY");
		}
		
		if (getEgoNetworksOperations().contains(EgoNetworksOperation.RELATIONS)){
			this.sequenceValueLabels.add("NETWORK_RELATIONS");
//			this.sequenceValueLabels.add("CONNECTED_NETWORK_RELATIONS");

			this.sequenceValueLabels.add("CENTRAL_ALTERS");
			this.sequenceValueLabels.add("CENTRAL_RELATIONS");

			this.sequenceValueLabels.add("SDENSITY_PARENT-CHILD");
			this.sequenceValueLabels.add("SDENSITY_SPOUSE");
			this.sequenceValueLabels.add("SDENSITY_SIBLING");
			this.sequenceValueLabels.add("SDENSITY_RELATIVE");
			this.sequenceValueLabels.add("SDENSITY_AFFINE");
//			this.censusOperationLabels.add("SDENSITY_EMPLOYMENT");
//			this.censusOperationLabels.add("SDENSITY_RENT");
			
		}
		
		for (ValueSequenceLabel valueSequenceLabel : this.mainValueSequenceLabels) {
			
			if (getParcoursNetworksOperations().contains(ParcoursNetworksOperation.GENERAL)){
				this.sequenceValueLabels.add("SIZE#" + valueSequenceLabel);
				this.sequenceValueLabels.add("TIES#" + valueSequenceLabel);
				this.sequenceValueLabels.add("DENSITY#" + valueSequenceLabel);
				this.sequenceValueLabels.add("DENSITY_NOLOOPS#" + valueSequenceLabel);
				this.sequenceValueLabels.add("MEANDEGREE#" + valueSequenceLabel);
				this.sequenceValueLabels.add("MEANDEGREE_NOLOOPS#" + valueSequenceLabel);
				this.sequenceValueLabels.add("MEANDEGREE_NOLOOPS_NORM#" + valueSequenceLabel);
			}

			if (getParcoursNetworksOperations().contains(ParcoursNetworksOperation.CENTRALITY)){
				this.sequenceValueLabels.add("MEAN_DIRBETWEENNESS#" + valueSequenceLabel);
				this.sequenceValueLabels.add("MAX_DIRBETWEENNESS#" + valueSequenceLabel);
			}
			
			if (getParcoursNetworksOperations().contains(ParcoursNetworksOperation.RELATIONS)){
				this.sequenceValueLabels.add("CENTRAL_ALTERS#" + valueSequenceLabel);
			}

			
		}
		
		if (!getParcoursNetworksOperations().isEmpty()){
			for (ValueSequenceLabel mainEventType : this.mainValueSequenceLabels) {
				addNetworkTitle("Sequence Network#" + mainEventType);
				this.partitionLabels.get("Sequence Network#" + mainEventType).add("DEGREE");
				this.partitionLabels.get("Sequence Network#" + mainEventType).add(mainEventType.toString());
			}
		}
		
		for (ValueSequenceLabel valueSequenceLabel : this.mainValueSequenceLabels) {
			
			if (getParcoursIntersectionNetworksOperations().contains(ParcoursIntersectionNetworksOperation.GENERAL)){
				this.sequenceValueLabels.add("SIZE#" + valueSequenceLabel);
				this.sequenceValueLabels.add("TIES#" + valueSequenceLabel);
				this.sequenceValueLabels.add("DENSITY#" + valueSequenceLabel);
				this.sequenceValueLabels.add("DENSITY_NOLOOPS#" + valueSequenceLabel);
				this.sequenceValueLabels.add("MEANDEGREE#" + valueSequenceLabel);
				this.sequenceValueLabels.add("MEANDEGREE_NOLOOPS#" + valueSequenceLabel);
				this.sequenceValueLabels.add("MEANDEGREE_NOLOOPS_NORM#" + valueSequenceLabel);
			}

			if (getParcoursIntersectionNetworksOperations().contains(ParcoursIntersectionNetworksOperation.CENTRALITY)){
				this.sequenceValueLabels.add("EGO-BETWEENNESS#" + valueSequenceLabel);
				this.sequenceValueLabels.add("MEAN_BETWEENNESS#" + valueSequenceLabel);
				this.sequenceValueLabels.add("MAX_BETWEENNESS#" + valueSequenceLabel);
				this.sequenceValueLabels.add("ECCENTRICITY#" + valueSequenceLabel);
			}
			
			if (getParcoursIntersectionNetworksOperations().contains(ParcoursIntersectionNetworksOperation.COHESION)){
				this.sequenceValueLabels.add("NRCOMPONENTS#" + valueSequenceLabel);
				this.sequenceValueLabels.add("NRISOLATES#" + valueSequenceLabel);
				this.sequenceValueLabels.add("MAXCOMPONENT#" + valueSequenceLabel);
				this.sequenceValueLabels.add("NRCOMPONENTS_NORM#" + valueSequenceLabel);
				this.sequenceValueLabels.add("NRISOLATES_NORM#" + valueSequenceLabel);
				this.sequenceValueLabels.add("MAXCOMPONENT_NORM#" + valueSequenceLabel);
				this.sequenceValueLabels.add("CONCENTRATION#" + valueSequenceLabel);
				this.sequenceValueLabels.add("BROKERAGE#" + valueSequenceLabel);
				this.sequenceValueLabels.add("EFFICIENT_SIZE#" + valueSequenceLabel);
				this.sequenceValueLabels.add("EFFICIENCY#" + valueSequenceLabel);
			}
			
			if (getParcoursIntersectionNetworksOperations().contains(ParcoursIntersectionNetworksOperation.GENERAL)){
				this.sequenceValueLabels.add("NETWORK_RELATIONS#" + valueSequenceLabel);
				this.sequenceValueLabels.add("CONNECTED_NETWORK#RELATIONS#" + valueSequenceLabel);
				this.sequenceValueLabels.add("SIMILARITY#" + valueSequenceLabel);
			}
			
		}
		
		if (!getTrajectoriesOperations().isEmpty()){

			if (!this.valueSequenceLabels.contains(ValueSequenceLabel.PLACE)) {
				this.valueSequenceLabels.add(ValueSequenceLabel.PLACE);
			}
			if (!this.valueSequenceLabels.contains(ValueSequenceLabel.DISTANCE)) {
				this.valueSequenceLabels.add(ValueSequenceLabel.DISTANCE);
			}
			
			if (getTrajectoriesOperations().contains(TrajectoriesOperation.DRAW) && !trajectoriesRelationClassificationTypes.contains(ValueSequenceLabel.PLACE)){
				this.trajectoriesRelationClassificationTypes.add(ValueSequenceLabel.PLACE);
			}
			
			this.sequenceValueLabels.add("NREVENTS");
			this.sequenceValueLabels.add("NREXTERNALMOVES#PLACE_"+level);

			for (ValueSequenceLabel valueSequenceLabel : this.trajectoriesRelationClassificationTypes) {
				this.sequenceValueLabels.add("PROFILE#" + valueSequenceLabel);
				this.sequenceValueLabels.add("SUPPORT#" + valueSequenceLabel);
				this.sequenceValueLabels.add("NRSTATIONS#" + valueSequenceLabel);
				if (valueSequenceLabel == ValueSequenceLabel.PLACE || valueSequenceLabel == ValueSequenceLabel.HOST) {
					this.sequenceValueLabels.add("CENTERS#" + valueSequenceLabel);
					this.sequenceValueLabels.add("CENTERSNOSTART#" + valueSequenceLabel);
					this.sequenceValueLabels.add("NRCENTERS#" + valueSequenceLabel);
					this.sequenceValueLabels.add("NRCENTERSNOSTART#" + valueSequenceLabel);
					this.sequenceValueLabels.add("NRINTERNALMOVES#" + valueSequenceLabel);
					this.sequenceValueLabels.add("NRDIRECTRETURNS#" + valueSequenceLabel);
					this.sequenceValueLabels.add("NRCYCLES#" + valueSequenceLabel);
					this.sequenceValueLabels.add("NRDIRECTRETURNS_NORM#" + valueSequenceLabel);
					this.sequenceValueLabels.add("NRCYCLES_NORM#" + valueSequenceLabel);
				}
			}

		}
		
		// Consolidate main event types with event types
		
		for (ValueSequenceLabel mainEventType : this.mainValueSequenceLabels) {
			if (!this.valueSequenceLabels.contains(mainEventType)) {
				this.valueSequenceLabels.add(mainEventType);
			}
		}

		for (ValueSequenceLabel mainEventType : this.trajectoriesRelationClassificationTypes) {
			if (!this.valueSequenceLabels.contains(mainEventType)) {
				this.valueSequenceLabels.add(mainEventType);
			}
		}
		
		// Set Network Titles
		
		this.networkTitles = new ArrayList<String>();
		
		if (!getEgoNetworksOperations().isEmpty()){
			addNetworkTitle("Ego Network");
			addNetworkTitle("Nonmediated Ego Network");
			this.partitionLabels.get("Ego Network").add("EGO-RELATION");
			this.partitionLabels.get("Ego Network").add("DEGREE");
			this.partitionLabels.get("Nonmediated Ego Network").add("BETWEENNESS");
			this.partitionLabels.get("Nonmediated Ego Network").add("EGO-RELATION");
			this.partitionLabels.get("Nonmediated Ego Network").add("DEGREE");
			this.partitionLabels.get("Nonmediated Ego Network").add("COMPONENT");

		// Temporary Solution:

			addNetworkTitle("Parcours Intersection Network");
			this.partitionLabels.get("Parcours Intersection Network").add("EGO-RELATION");
			this.partitionLabels.get("Parcours Intersection Network").add("NREVENTS");
		
			for (ValueSequenceLabel mainEventType : this.mainValueSequenceLabels) {
				addNetworkTitle("Parcours Similarity Network#" + mainEventType);
				this.partitionLabels.get("Parcours Similarity Network#" + mainEventType).add("EGO-RELATION");
				this.partitionLabels.get("Parcours Similarity Network#" + mainEventType).add("NRTRANSITIONS");
			}

		}
		
		if (!getParcoursIntersectionNetworksOperations().isEmpty()){
			addNetworkTitle("Parcours Intersection Network");
			this.partitionLabels.get("Parcours Intersection Network").add("EGO-RELATION");
			this.partitionLabels.get("Parcours Intersection Network").add("NREVENTS");
		}
		
		if (!getParcoursSimilarityNetworksOperations().isEmpty()){
			addNetworkTitle("Parcours Similarity Network");
			this.partitionLabels.get("Parcours Similarity Network").add("EGO-RELATION");
			this.partitionLabels.get("Parcours Similarity Network").add("NREVENTS");
		}
		
		if (getParcoursNetworksOperations().contains(ParcoursNetworksOperation.EXPORT_PARCOURS)){
			addNetworkTitle("Parcours");
			this.partitionLabels.get("Parcours").add("DATE");
			this.partitionLabels.get("Parcours").add("DISTANCE");
		}
		
		if (getParcoursNetworksOperations().contains(ParcoursNetworksOperation.EXPORT_EXTENDED_PARCOURS)){
			addNetworkTitle("Extended Parcours");
			this.partitionLabels.get("Extended Parcours").add("ORDER");
			this.partitionLabels.get("Extended Parcours").add("TYPE");
		}
		
		if (getParcoursNetworksOperations().contains(ParcoursNetworksOperation.EXPORT_MULTIPLE_PARCOURS)){
			addNetworkTitle("Multiple Parcours");
			this.partitionLabels.get("Multiple Parcours").add("DATE");
			this.partitionLabels.get("Multiple Parcours").add("DISTANCE");
		}
		
		if (getTrajectoriesOperations().contains(TrajectoriesOperation.EXPORT_SEQUENCE_NETWORKS)){
			for (ValueSequenceLabel mainEventType : this.trajectoriesRelationClassificationTypes) {
				addNetworkTitle("Sequence Network#" + mainEventType);
				this.partitionLabels.get("Sequence Network#" + mainEventType).add("DEGREE");
				this.partitionLabels.get("Sequence Network#" + mainEventType).add(mainEventType.toString());
			}
		}
		
		if (!getTrajectoriesOperations().isEmpty()){
			for (ValueSequenceLabel mainEventType : this.trajectoriesRelationClassificationTypes) {
				addNetworkTitle("Sequence Network#" + mainEventType);
				this.partitionLabels.get("Sequence Network#" + mainEventType).add("DEGREE");
				this.partitionLabels.get("Sequence Network#" + mainEventType).add(mainEventType.toString());
			}
		}
		
		if (getTrajectoriesOperations().contains(TrajectoriesOperation.EXPORT_SIMILARY_TREES)){
			for (ValueSequenceLabel mainEventType : this.trajectoriesRelationClassificationTypes) {
				addNetworkTitle("Sequence Network Similarity Tree#" + mainEventType);
			}
		}
		
		if (getTrajectoriesOperations().contains(TrajectoriesOperation.EXPORT_AGGREGATE_SEQUENCE_NETWORK)){
			for (ValueSequenceLabel mainEventType : this.trajectoriesRelationClassificationTypes) {
				addNetworkTitle("Aggregate Sequence Network#" + mainEventType);
				this.partitionLabels.get("Aggregate Sequence Network#" + mainEventType).add("DEGREE");
				this.partitionLabels.get("Aggregate Sequence Network#" + mainEventType).add(mainEventType.toString());
			}
		}
		
		if (!getTrajectoriesOperations().isEmpty()){
//			addNetworkTitle("Event Type Network");
//			addNetworkTitle("Sequence Type Network");
			this.nodeStatisticsLabels = new ArrayList<String>();
			this.nodeStatisticsLabels.add("NUMBER");
			this.nodeStatisticsLabels.add("INDEGREE");
			this.nodeStatisticsLabels.add("OUTDEGREE");
			this.nodeStatisticsLabels.add("INSTRENGTH");
			this.nodeStatisticsLabels.add("OUTSTRENGTH");
			this.nodeStatisticsLabels.add("ORIENTATION");
			this.nodeStatisticsLabels.add("DIRBETWEENNESS");
			this.nodeStatisticsLabels.add("MAXINWEIGHT");
			this.nodeStatisticsLabels.add("MAXOUTWEIGHT");
			this.nodeStatisticsLabels.add("MAXPREDECESSOR");
			this.nodeStatisticsLabels.add("MAXSUCCESSOR");
		}
		
		// Set node statistics criteria;
		
		if (!getParcoursNetworksOperations().isEmpty()){
//		if (getParcoursNetworksOperations().contains(ParcoursNetworksOperation.EXPORT_PARCOURS_NETWORKS)){
		
			this.nodeStatisticsLabels = new ArrayList<String>();
			this.nodeStatisticsLabels.add("NUMBER");
			this.nodeStatisticsLabels.add("INDEGREE");
			this.nodeStatisticsLabels.add("OUTDEGREE");
			this.nodeStatisticsLabels.add("INSTRENGTH");
			this.nodeStatisticsLabels.add("OUTSTRENGTH");
			this.nodeStatisticsLabels.add("ORIENTATION");
			this.nodeStatisticsLabels.add("DIRBETWEENNESS");
			this.nodeStatisticsLabels.add("MAXINWEIGHT");
			this.nodeStatisticsLabels.add("MAXOUTWEIGHT");
			this.nodeStatisticsLabels.add("MAXPREDECESSOR");
			this.nodeStatisticsLabels.add("MAXSUCCESSOR");
		}
	}
	
	public void setPartitionCriteria(){
		
		// Set Partition Criteria for Sequence Values
		
		// Set sequence value criteria 
		for (String label : getSequenceValueLabels()){
			addSequenceValueCriteria(new PartitionCriteria(label));
		}
		
		addSequenceValueCriteria(new PartitionCriteria("PROFILE#AGE"));
		addSequenceValueCriteria(new PartitionCriteria("PROFILE#"+dateLabel));
		
		// Set value sequence criteria  
		for (String label : getSequenceValueCriteriaList().getLabels()){
			
			String[] labels = ToolBox.splitLastPart(label,"#");
			if (labels[1] != null && !labels[0].contains("ALTERS") && !labels[0].contains("RELATION")){
				addValueSequenceCriteria(new PartitionCriteria(labels[1]));
			}
		}

		// Set sequence value criteria (general)
		
		for (PartitionCriteria sequenceValueCriteria : getSequenceValueCriteriaList()){
			
			String label = sequenceValueCriteria.getLabel();

			if (label.equals("NREVENTS") || label.contains("NRSTATIONS")) {
				sequenceValueCriteria.setType(PartitionType.FREE_GROUPING);
				sequenceValueCriteria.setIntervals(PartitionMaker.getIntervals("1 5 10 15 20 25"));
				// partitionCriteria.setCumulationType(CumulationType.DESCENDANT);
			} else if (label.contains("AGEFIRST")) {
				sequenceValueCriteria.setType(PartitionType.SIZED_GROUPING);
				sequenceValueCriteria.setStart(0.);
				sequenceValueCriteria.setSize(5.);
			} else if (label.equals("ECCENTRICITY")) {
				sequenceValueCriteria.setType(PartitionType.SIZED_GROUPING);
				sequenceValueCriteria.setStart(-100.);
				sequenceValueCriteria.setSize(20.);
			} else if (label.contains("COVERAGE") || label.contains("SAME") || label.contains("NORM") || label.contains("DENSITY")
					|| label.contains("BETWEENNESS") || label.contains("EFFICIENCY") || label.contains("CONCENTRATION")) {
				sequenceValueCriteria.setType(PartitionType.SIZED_GROUPING);
				sequenceValueCriteria.setStart(0.);
				sequenceValueCriteria.setSize(20.);
			} else if (label.contains("MEAN") || label.contains("COVERAGE") || label.contains("PEREVENT") || label.contains("DIRBETWEENNESS")|| label.contains("BETWEENNESS")
					|| label.contains("BROKERAGE") || label.contains("EFFICIENT_SIZE")) {
				sequenceValueCriteria.setType(PartitionType.SIZED_GROUPING);
				sequenceValueCriteria.setStart(0.);
				sequenceValueCriteria.setSize(1.);
			} else if (label.substring(0, 3).equals("AGE")) {
				sequenceValueCriteria.setType(PartitionType.RAW);
				sequenceValueCriteria.setSizeFilter(SizeFilter.HOLES);
				sequenceValueCriteria.setValueFilter(ValueFilter.NULL);
			} else {
				sequenceValueCriteria.setType(PartitionType.RAW);
			}
			
			if (!label.contains("ALTERS") && !label.contains("PROFILE") && !label.contains("SUPPORT")) {
				sequenceValueCriteria.setWithDiagram(true);
			}
		}
		
		// Set sequence value criteria (special)  // Optionalize
		
		
		if (getSliceGeneralStatistics().contains(SliceGeneralStatistics.POPULATION)){
			
			List<String> labels = Arrays.asList(new String[]{"NRINDIVIDUALS","GENDER_RATIO","GENDER","BIRT_PLACE","QUARTER","QUARTER_POPULATION","PATRIL","OCCU","MATRISTATUS","AGE"});
			setValueSequenceCriteria(labels, labels, null, null, labels);
			
			getValueSequenceCriteriaByLabel("BIRT_PLACE").setLabelParameter("Bas-Mono");
			getValueSequenceCriteriaByLabel("AGE").setSizedGrouping(0., 20., null);
			getValueSequenceCriteriaByLabel("NRINDIVIDUALS").setFreeGrouping("0 1 5 10 25 50 100");
			getValueSequenceCriteriaByLabel("GENDER_RATIO").setSizedGrouping(0., 0.1, null);
			setRoleNames(Arrays.asList(new String[] { "REFERENT" }));
		}
		
		if (getSliceGeneralStatistics().contains(SliceGeneralStatistics.POSITIONS)){
			
			List<String> labels = Arrays.asList(new String[]{"PLACE","REFERENT_CHAIN","REFERENT_CHAIN_TYPE","REFERENT_KIN","REFERENT_KIN_TYPE","REFERENT"});
			List<String> withMatrix = Arrays.asList(new String[]{"REFERENT_CHAIN","REFERENT_CHAIN_TYPE","REFERENT_KIN","REFERENT_KIN_TYPE"});
			setValueSequenceCriteria(labels, withMatrix, withMatrix, withMatrix, null);
			setRoleNames(Arrays.asList(new String[] { "REFERENT" }));
			getValueSequenceCriteriaByLabel("REFERENT_CHAIN_TYPE").setWithDiagram(true);
			getValueSequenceCriteriaByLabel("REFERENT_KIN_TYPE").setWithDiagram(true);
			this.sequenceValueLabels.add("PROFILE#REFERENT_CHAIN_TYPE");
			this.sequenceValueLabels.add("PROFILE#REFERENT_KIN_TYPE");
		}
		
		if (getSliceGeneralStatistics().contains(SliceGeneralStatistics.METRICS)){
			
			List<String> labels = Arrays.asList(new String[]{"SIZE","MAXDEPTH","MEANDEPTH","MEANINDEGREE","DIAMETER","NRCOMPONENTS","MAXCOMPONENT","CONCENTRATION"});			
			setValueSequenceCriteria(labels, labels, null, null, null);
			setRoleNames(Arrays.asList(new String[] { "REFERENT" }));
		}
		
		if (getSliceGeneralStatistics().contains(SliceGeneralStatistics.MORPHOLOGY)){
			
			List<String> labels = Arrays.asList(new String[]{"TREES_BY_ID","TREES_BY_GENDER","TREES_BY_GENDER_REDUCED","TREES_BY_KIN","REFERENT_CHAIN","REFERENT_CHAIN_TYPES","REFERENT_KIN","ALL_KIN"});
			List<String> withMatrix = Arrays.asList(new String[]{"REFERENT_CHAIN","REFERENT_KIN"});
			setValueSequenceCriteria(labels, labels, withMatrix, withMatrix, null);
			setRoleNames(Arrays.asList(new String[] { "REFERENT" }));
		}

		if (getSliceGeneralStatistics().contains(SliceGeneralStatistics.DYNAMICS)){
			
			List<String> labels = Arrays.asList(new String[]{"PLACE"});
			setValueSequenceCriteria(labels, null, null, null, null);
			getValueSequenceCriteriaByLabel("PLACE").setLabelParameter("Bas-Mono");
			getValueSequenceCriteriaByLabel("PLACE").setWithFlow(true);
			setRoleNames(Arrays.asList(new String[] { "REFERENT" }));
		}
		
	}




	/**
	 * Temporary Method to set default criteria
	 * 
	 * @param censusType
	 */
/*	public SequenceCriteria(final CensusType censusType) {

		setDefaultCriteria();

		this.censusType = censusType;

		// Set alter attribute label+value [choose from individual attribute
		// censusOperationLabels]
		if (censusType == CensusType.SEQUENCENETWORKS) {
			this.alterFilterAttributeLabel = "INTERV";
			this.alterFilterAttributeValue = "Yes";
		}

		// Set relation attribute types [Choose among values of enum
		// RelationClassificationType]
		switch (censusType) {
			case GENERAL:
				this.relationClassificationTypes.add(RelationClassificationType.PLACE);
				this.relationClassificationTypes.add(RelationClassificationType.MIGRATIONTYPE);
				this.relationClassificationTypes.add(RelationClassificationType.TREES);
				this.relationClassificationTypes.add(RelationClassificationType.CHILDMIGRATIONTYPE);
				this.relationClassificationTypes.add(RelationClassificationType.DISTANCE);
				this.relationClassificationTypes.add(RelationClassificationType.REGION);
			break;
			case EGONETWORKS:
				this.relationClassificationTypes.add(RelationClassificationType.PLACE);
			break;
			case SEQUENCENETWORKS:
				this.relationClassificationTypes.add(RelationClassificationType.PLACE);
			break;
			case PARCOURS:
				this.relationClassificationTypes.add(RelationClassificationType.PLACE);
			break;
		}

		// Set main relation attribute types types [Choose among values of enum
		// RelationClassificationType]
		switch (censusType) {
			case GENERAL:
			break;
			case EGONETWORKS:
			break;
			case SEQUENCENETWORKS:
				this.mainRelationClassificationTypes.add(RelationClassificationType.MIGRATIONTYPE);
				this.mainRelationClassificationTypes.add(RelationClassificationType.TYPEDID);
			break;
			case PARCOURS:
				this.mainRelationClassificationTypes.add(RelationClassificationType.PLACE);
				this.mainRelationClassificationTypes.add(RelationClassificationType.DISTANCE);
				this.mainRelationClassificationTypes.add(RelationClassificationType.TREES);
				this.mainRelationClassificationTypes.add(RelationClassificationType.MIGRATIONTYPE);
			// mainRelationClassificationTypes.add(EventType.COMPONENTS);
			break;
		}

		// Consolidate main event types with event types
		for (RelationClassificationType mainEventType : this.mainRelationClassificationTypes) {
			if (!this.relationClassificationTypes.contains(mainEventType)) {
				this.relationClassificationTypes.add(mainEventType);
			}
		}

		// Set network titles [choose from list]
		this.networkTitles = new ArrayList<String>();
		this.partitionLabels = new HashMap<String, List<String>>();
		switch (censusType) {
			case GENERAL:
			break;
			case EGONETWORKS:
				addNetworkTitle("Ego Network");
				addNetworkTitle("Nonmediated Ego Network");
			break;
			case SEQUENCENETWORKS:
				addNetworkTitle("Parcours");
				addNetworkTitle("Extended Parcours");
				addNetworkTitle("Multiple Parcours");
				for (RelationClassificationType mainEventType : this.mainRelationClassificationTypes) {
					addNetworkTitle("Parcours Network#" + mainEventType);
					addNetworkTitle("Parcours Similarity Tree#" + mainEventType);
				}
			break;
			case PARCOURS:
				addNetworkTitle("Event Type Network");
				addNetworkTitle("Sequence Type Network");
			break;
		}

		// Set partition Labels
		switch (censusType) {
			case GENERAL:
			break;
			case EGONETWORKS:
				this.partitionLabels.get("Ego Network").add("EGO-RELATION");
				this.partitionLabels.get("Ego Network").add("DEGREE");
				this.partitionLabels.get("Nonmediated Ego Network").add("BETWEENNESS");
				this.partitionLabels.get("Nonmediated Ego Network").add("EGO-RELATION");
				this.partitionLabels.get("Nonmediated Ego Network").add("DEGREE");
				this.partitionLabels.get("Nonmediated Ego Network").add("COMPONENT");
			break;
			case SEQUENCENETWORKS:
				this.partitionLabels.get("Parcours").add("DATE");
				this.partitionLabels.get("Parcours").add("DISTANCE");
				this.partitionLabels.get("Multiple Parcours").add("DATE");
				this.partitionLabels.get("Multiple Parcours").add("DISTANCE");
				this.partitionLabels.get("Extended Parcours").add("ORDER");
				this.partitionLabels.get("Extended Parcours").add("TYPE");
			break;
			case PARCOURSINTERSECTIONNETWORKS:
				this.partitionLabels.get("Parcours Intersection Network").add("EGO-RELATION");
			break;
			case PARCOURSSIMILARITYNETWORKS:
				this.partitionLabels.get("Parcours Intersection Network").add("EGO-RELATION");
			break;
			case PARCOURS:
			break;
		}

		// Set ego network and tree network partition censusOperationLabels
		if (censusType == CensusType.SEQUENCENETWORKS) {
			for (RelationClassificationType mainEventType : this.mainRelationClassificationTypes) {
				List<String> egoNetworkPartitionLabels = this.partitionLabels.get("Parcours Network_" + mainEventType);
				egoNetworkPartitionLabels.add("BETWEENNESS");
				egoNetworkPartitionLabels.add("EGO-RELATION");
				egoNetworkPartitionLabels.add("DEGREE");
				egoNetworkPartitionLabels.add("COMPONENT");

				List<String> treePartitionLabels = this.partitionLabels.get("Parcours Similarity Tree_" + mainEventType);
				treePartitionLabels.add("SIZE");
				treePartitionLabels.add("STEP");
				treePartitionLabels.add("EGO-RELATION");
			}
		}

		// Set censusOperationLabels of census operations
		this.censusOperationLabels = new ArrayList<String>();
		switch (censusType) {
			case GENERAL:

				this.censusOperationLabels.add("NREVENTS");
				this.censusOperationLabels.add("NREXTERNALMOVES#PLACE_"+level);

				this.censusOperationLabels.add("AGEFIRST");
				this.censusOperationLabels.add("AGEFIRST_CHILDMIGRATIONTYPE_NOPARENTS");
				this.censusOperationLabels.add("AGEFIRST_DISTANCE_TRANSNATIONAL");
				this.censusOperationLabels.add("MAX_DISTANCE");
				this.censusOperationLabels.add("MEAN_NR_MOVES");
				this.censusOperationLabels.add("SAMESEXALTERS_ALL");
				this.censusOperationLabels.add("SAMEPLACEALTERS_ALL");

				for (String roleName : this.roleNames) {
					this.censusOperationLabels.add("NRALTERS_" + roleName);
					this.censusOperationLabels.add("NRALTERSPEREVENT_" + roleName);
				}

				for (RelationClassificationType type : this.relationClassificationTypes) {
					this.censusOperationLabels.add("EVENTS_" + type);
				}

				this.censusOperationLabels.add("MEAN_COVERAGE");
				this.censusOperationLabels.add("MAX_COVERAGE");

				for (String roleName : this.roleNames) {
					this.censusOperationLabels.add("RELATIONS_" + roleName);
				}

				this.censusOperationLabels.add("MAIN_ALTERS");
				this.censusOperationLabels.add("MAIN_RELATIONS");

			break;

			case EGONETWORKS:

				this.censusOperationLabels.add("NREVENTS");
				this.censusOperationLabels.add("NREXTERNALMOVES#PLACE_"+level);

				this.censusOperationLabels.add("SIZE");
				this.censusOperationLabels.add("TIES");
				this.censusOperationLabels.add("EGO-BETWEENNESS");
				this.censusOperationLabels.add("MEAN_BETWEENNESS");
				this.censusOperationLabels.add("MAX_BETWEENNESS");
				this.censusOperationLabels.add("ECCENTRICITY");
				this.censusOperationLabels.add("DENSITY");
				this.censusOperationLabels.add("DENSITY_NOLOOPS");
				this.censusOperationLabels.add("MEANDEGREE");
				this.censusOperationLabels.add("MEANDEGREE_NOLOOPS");
				this.censusOperationLabels.add("MEANDEGREE_NOLOOPS_NORM");
				this.censusOperationLabels.add("NRCOMPONENTS");
				this.censusOperationLabels.add("NRISOLATES");
				this.censusOperationLabels.add("MAXCOMPONENT");
				this.censusOperationLabels.add("NRCOMPONENTS_NORM");
				this.censusOperationLabels.add("NRISOLATES_NORM");
				this.censusOperationLabels.add("MAXCOMPONENT_NORM");
				this.censusOperationLabels.add("CONCENTRATION");
				this.censusOperationLabels.add("BROKERAGE");
				this.censusOperationLabels.add("EFFICIENT_SIZE");
				this.censusOperationLabels.add("EFFICIENCY");
				this.censusOperationLabels.add("NETWORK_RELATIONS");
				this.censusOperationLabels.add("CONNECTED_NETWORK_RELATIONS");

				this.censusOperationLabels.add("CENTRAL_ALTERS");
				this.censusOperationLabels.add("CENTRAL_RELATIONS");

				this.censusOperationLabels.add("SDENSITY_PARENT-CHILD");
				this.censusOperationLabels.add("SDENSITY_SPOUSE");
				this.censusOperationLabels.add("SDENSITY_SIBLING");
				this.censusOperationLabels.add("SDENSITY_RELATIVE");
				this.censusOperationLabels.add("SDENSITY_AFFINE");
				this.censusOperationLabels.add("SDENSITY_EMPLOYMENT");
				this.censusOperationLabels.add("SDENSITY_RENT");

			break;

			case SEQUENCENETWORKS:

				this.censusOperationLabels.add("NREVENTS");
				this.censusOperationLabels.add("NREXTERNALMOVES#PLACE_"+level);

				for (RelationClassificationType mainEventType : this.mainRelationClassificationTypes) {
					this.censusOperationLabels.add("SIZE#" + mainEventType);
					this.censusOperationLabels.add("TIES#" + mainEventType);
					this.censusOperationLabels.add("EGO-BETWEENNESS#" + mainEventType);
					this.censusOperationLabels.add("MEAN_BETWEENNESS#" + mainEventType);
					this.censusOperationLabels.add("MAX_BETWEENNESS#" + mainEventType);
					this.censusOperationLabels.add("ECCENTRICITY#" + mainEventType);
					this.censusOperationLabels.add("DENSITY#" + mainEventType);
					this.censusOperationLabels.add("DENSITY_NOLOOPS#" + mainEventType);
					this.censusOperationLabels.add("MEANDEGREE#" + mainEventType);
					this.censusOperationLabels.add("MEANDEGREE_NOLOOPS#" + mainEventType);
					this.censusOperationLabels.add("MEANDEGREE_NOLOOPS_NORM#" + mainEventType);
					this.censusOperationLabels.add("NRCOMPONENTS#" + mainEventType);
					this.censusOperationLabels.add("NRISOLATES#" + mainEventType);
					this.censusOperationLabels.add("MAXCOMPONENT#" + mainEventType);
					this.censusOperationLabels.add("NRCOMPONENTS_NORM#" + mainEventType);
					this.censusOperationLabels.add("NRISOLATES_NORM#" + mainEventType);
					this.censusOperationLabels.add("MAXCOMPONENT_NORM#" + mainEventType);
					this.censusOperationLabels.add("CONCENTRATION#" + mainEventType);
					this.censusOperationLabels.add("BROKERAGE#" + mainEventType);
					this.censusOperationLabels.add("EFFICIENT_SIZE#" + mainEventType);
					this.censusOperationLabels.add("EFFICIENCY#" + mainEventType);
					this.censusOperationLabels.add("NETWORK_RELATIONS#" + mainEventType);
					this.censusOperationLabels.add("CONNECTED_NETWORK_RELATIONS#" + mainEventType);
					this.censusOperationLabels.add("SIMILARITY#" + mainEventType);
				}

			break;

			case PARCOURS:
				this.censusOperationLabels.add("NREVENTS");
				this.censusOperationLabels.add("NREXTERNALMOVES#PLACE_"+level);

				for (RelationClassificationType relationClassificationType : this.mainRelationClassificationTypes) {
					this.censusOperationLabels.add("PROFILE#" + relationClassificationType);
					this.censusOperationLabels.add("SUPPORT#" + relationClassificationType);
					if (relationClassificationType == RelationClassificationType.PLACE) {
						this.censusOperationLabels.add("CENTERS#" + relationClassificationType);
						this.censusOperationLabels.add("NRINTERNALMOVES#" + relationClassificationType);
						this.censusOperationLabels.add("NRDIRECTRETURNS#" + relationClassificationType);
						this.censusOperationLabels.add("NRCYCLES#" + relationClassificationType);
						this.censusOperationLabels.add("NRDIRECTRETURNS_NORM#" + relationClassificationType);
						this.censusOperationLabels.add("NRCYCLES_NORM#" + relationClassificationType);
					}
				}

			break;
		}

	}*/

	private void addNetworkTitle(final String networkTitle) {
		
		if (!networkTitles.contains(networkTitle)){
			this.networkTitles.add(networkTitle);
			this.partitionLabels.put(networkTitle, new ArrayList<String>());
		}
	}

	public String getAlterFilterAttributeLabel() {
		return this.alterFilterAttributeLabel;
	}

	public String getAlterFilterAttributeValue() {
		return this.alterFilterAttributeValue;
	}

	public String getAlterFilterRoleName() {
		return this.alterFilterRoleName;
	}

	public List<String> getSequenceValueLabels() {
		return this.sequenceValueLabels;
	}

	public CensusType getCensusType() {
		return this.censusType;
	}

	public String getChainClassification() {
		return this.chainClassification;
	}

	public String getDateLabel() {
		return this.dateLabel;
	}

	public Integer[] getDates() {
		return this.dates;
	}

	public String getDefaultReferentRoleName() {
		return this.defaultReferentRoleName;
	}

	public List<EgoNetworksOperation> getEgoNetworksOperations() {
		return this.egoNetworksOperations;
	}

	public String getEgoRoleName() {
		return this.egoRoleName;
	}

	public String getEndDateLabel() {
		return this.endDateLabel;
	}

	public String getEndPlaceLabel() {
		return this.endPlaceLabel;
	}

	public List<ValueSequenceLabel> getEventTypes() {
		return this.valueSequenceLabels;
	}

	public ExpansionMode getExpansionMode() {
		return this.expansionMode;
	}

	public FiliationType getFiliationType() {
		return this.filiationType;
	}

	public Geography getGeography() {
		return this.geography;
	}

	public GeoLevel getLevel() {
		return this.level;
	}

	public String getLocalUnitLabel() {
		return this.localUnitLabel;
	}

	public List<ValueSequenceLabel> getMainValueSequenceLabels() {
		return this.mainValueSequenceLabels;
	}

	public int getMaxAge() {
		return this.maxAge;
	}

	public int getMinAge() {
		return this.minAge;
	}

	public StringList getMinimalPlaceNames() {
		return this.minimalPlaceNames;
	}

	public List<String> getNetworkTitles() {
		return this.networkTitles;
	}

	public List<ParcoursNetworksOperation> getParcoursNetworksOperations() {
		return this.parcoursNetworkdsOperations;
	}

	public List<ParcoursIntersectionNetworksOperation> getParcoursIntersectionNetworksOperations() {
		return this.parcoursIntersectionNetworksOperations;
	}

	public List<ParcoursSimilarityNetworksOperation> getParcoursSimilarityNetworksOperations() {
		return this.parcoursSimilarityNetworksOperations;
	}

	public String getIndividualPartitionLabel() {
		return this.individualClassificationType;
	}

	public Map<String, List<String>> getPartitionLabels() {
		return this.partitionLabels;
	}

	public String getPattern() {
		return this.pattern;
	}

	public String getPlaceLabel() {
		return this.placeLabel;
	}

	public List<ValueSequenceLabel> getValueSequenceLabels() {
		return this.valueSequenceLabels;
	}

	public String getRelationModelName() {
		return this.relationModelName;
	}

	public List<String> getRelationModelNames() {
		return this.relationModelNames;
	}

	public StringList getRoleNames() {
		return this.roleNames;
	}

	public List<SequenceGeneralStatistics> getSequenceGeneralStatistics() {
		return this.sequenceGeneralStatistics;
	}

	public List<SequenceReportType> getSequenceReportTypes() {
		return this.sequenceReportTypes;
	}

	public List<SliceGeneralStatistics> getSliceGeneralStatistics() {
		return this.sliceGeneralStatistics;
	}

	public List<SliceReportType> getSliceReportTypes() {
		return this.sliceReportTypes;
	}

	public String getStartDateLabel() {
		return this.startDateLabel;
	}

	public String getStartPlaceLabel() {
		return this.startPlaceLabel;
	}

	public List<TrajectoriesOperation> getTrajectoriesOperations() {
		return this.trajectoriesOperations;
	}

	public List<ValueSequenceLabel> getTrajectoriesRelationClassificationTypes() {
		return this.trajectoriesRelationClassificationTypes;
	}

	public List<ValueSequenceLabel> getTypes() {
		return this.valueSequenceLabels;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	boolean hasVariableIntervals() {
		boolean result;

		if ((this.dates.length == 0) && (StringUtils.isBlank(this.localUnitLabel))) {
			//
			result = true;
		} else {
			//
			result = false;
		}

		//
		return result;
	}

	public void setAlterAttributeLabel(final String alterAttributeLabel) {
		this.alterFilterAttributeLabel = alterAttributeLabel;
	}

	public void setAlterAttributeValue(final String alterAttributeValue) {
		this.alterFilterAttributeValue = alterAttributeValue;
	}

	public void setAlterFilterRoleName(final String alterFilterRoleName) {
		this.alterFilterRoleName = alterFilterRoleName;
	}

/*	public void setCensusOperationLabels() {

		this.censusOperationLabels = new ArrayList<String>();

		if (!getSequenceGeneralStatistics().isEmpty()) {
			if (!this.relationClassificationTypes.contains(RelationClassificationType.PLACE)) {
				this.relationClassificationTypes.add(RelationClassificationType.PLACE);
			}
			if (!this.relationClassificationTypes.contains(RelationClassificationType.DISTANCE)) {
				this.relationClassificationTypes.add(RelationClassificationType.DISTANCE);
			}
		}

		if (getSequenceGeneralStatistics().contains(SequenceGeneralStatistics.EVENTS)) {

			this.censusOperationLabels.add("NREVENTS");
			this.censusOperationLabels.add("NREXTERNALMOVES");
			this.censusOperationLabels.add("MAX_DISTANCE");
			this.censusOperationLabels.add("MEAN_NR_MOVES");

			for (RelationClassificationType type : this.relationClassificationTypes) {
				this.censusOperationLabels.add("EVENTS_" + type);
			}

		}

		if (getSequenceGeneralStatistics().contains(SequenceGeneralStatistics.AGE)) {
			this.censusOperationLabels.add("AGEFIRST");
			this.censusOperationLabels.add("AGEFIRST_CHILDMIGRATIONTYPE_NOPARENTS");
			this.censusOperationLabels.add("AGEFIRST_DISTANCE_TRANSNATIONAL");
		}

		if (getSequenceGeneralStatistics().contains(SequenceGeneralStatistics.RELATIONS)) {
			this.censusOperationLabels.add("SAMESEXALTERS_ALL");
			this.censusOperationLabels.add("SAMEPLACEALTERS_ALL");

			for (String roleName : this.roleNames) {
				this.censusOperationLabels.add("NRALTERS_" + roleName);
				this.censusOperationLabels.add("NRALTERSPEREVENT_" + roleName);
			}

			this.censusOperationLabels.add("MEAN_COVERAGE");
			this.censusOperationLabels.add("MAX_COVERAGE");

			for (String roleName : this.roleNames) {
				this.censusOperationLabels.add("RELATIONS_" + roleName);
			}

			this.censusOperationLabels.add("MAIN_ALTERS");
			this.censusOperationLabels.add("MAIN_RELATIONS");

		}

				switch (censusType) {

					case EGONETWORKS:

						this.censusOperationLabels.add("NREVENTS");
						this.censusOperationLabels.add("NREXTERNALMOVES");

						this.censusOperationLabels.add("SIZE");
						this.censusOperationLabels.add("TIES");
						this.censusOperationLabels.add("EGO-BETWEENNESS");
						this.censusOperationLabels.add("MEAN_BETWEENNESS");
						this.censusOperationLabels.add("MAX_BETWEENNESS");
						this.censusOperationLabels.add("ECCENTRICITY");
						this.censusOperationLabels.add("DENSITY");
						this.censusOperationLabels.add("DENSITY_NOLOOPS");
						this.censusOperationLabels.add("MEANDEGREE");
						this.censusOperationLabels.add("MEANDEGREE_NOLOOPS");
						this.censusOperationLabels.add("MEANDEGREE_NOLOOPS_NORM");
						this.censusOperationLabels.add("NRCOMPONENTS");
						this.censusOperationLabels.add("NRISOLATES");
						this.censusOperationLabels.add("MAXCOMPONENT");
						this.censusOperationLabels.add("NRCOMPONENTS_NORM");
						this.censusOperationLabels.add("NRISOLATES_NORM");
						this.censusOperationLabels.add("MAXCOMPONENT_NORM");
						this.censusOperationLabels.add("CONCENTRATION");
						this.censusOperationLabels.add("BROKERAGE");
						this.censusOperationLabels.add("EFFICIENT_SIZE");
						this.censusOperationLabels.add("EFFICIENCY");
						this.censusOperationLabels.add("NETWORK_RELATIONS");
						this.censusOperationLabels.add("CONNECTED_NETWORK_RELATIONS");

						this.censusOperationLabels.add("CENTRAL_ALTERS");
						this.censusOperationLabels.add("CENTRAL_RELATIONS");

						this.censusOperationLabels.add("SDENSITY_PARENT-CHILD");
						this.censusOperationLabels.add("SDENSITY_SPOUSE");
						this.censusOperationLabels.add("SDENSITY_SIBLING");
						this.censusOperationLabels.add("SDENSITY_RELATIVE");
						this.censusOperationLabels.add("SDENSITY_AFFINE");
						this.censusOperationLabels.add("SDENSITY_EMPLOYMENT");
						this.censusOperationLabels.add("SDENSITY_RENT");

					break;

					case SEQUENCENETWORKS:

						this.censusOperationLabels.add("NREVENTS");
						this.censusOperationLabels.add("NREXTERNALMOVES");

						for (RelationClassificationType mainEventType : this.mainRelationClassificationTypes) {
							this.censusOperationLabels.add("SIZE_" + mainEventType);
							this.censusOperationLabels.add("TIES_" + mainEventType);
							this.censusOperationLabels.add("EGO-BETWEENNESS_" + mainEventType);
							this.censusOperationLabels.add("MEAN_BETWEENNESS_" + mainEventType);
							this.censusOperationLabels.add("MAX_BETWEENNESS_" + mainEventType);
							this.censusOperationLabels.add("ECCENTRICITY_" + mainEventType);
							this.censusOperationLabels.add("DENSITY_" + mainEventType);
							this.censusOperationLabels.add("DENSITY_NOLOOPS_" + mainEventType);
							this.censusOperationLabels.add("MEANDEGREE_" + mainEventType);
							this.censusOperationLabels.add("MEANDEGREE_NOLOOPS_" + mainEventType);
							this.censusOperationLabels.add("MEANDEGREE_NOLOOPS_NORM_" + mainEventType);
							this.censusOperationLabels.add("NRCOMPONENTS_" + mainEventType);
							this.censusOperationLabels.add("NRISOLATES_" + mainEventType);
							this.censusOperationLabels.add("MAXCOMPONENT_" + mainEventType);
							this.censusOperationLabels.add("NRCOMPONENTS_NORM_" + mainEventType);
							this.censusOperationLabels.add("NRISOLATES_NORM_" + mainEventType);
							this.censusOperationLabels.add("MAXCOMPONENT_NORM_" + mainEventType);
							this.censusOperationLabels.add("CONCENTRATION_" + mainEventType);
							this.censusOperationLabels.add("BROKERAGE_" + mainEventType);
							this.censusOperationLabels.add("EFFICIENT_SIZE_" + mainEventType);
							this.censusOperationLabels.add("EFFICIENCY_" + mainEventType);
							this.censusOperationLabels.add("NETWORK_RELATIONS_" + mainEventType);
							this.censusOperationLabels.add("CONNECTED_NETWORK_RELATIONS_" + mainEventType);
							this.censusOperationLabels.add("SIMILARITY_" + mainEventType);
						}

					break;

					case PARCOURS:
						this.censusOperationLabels.add("NREVENTS");
						this.censusOperationLabels.add("NREXTERNALMOVES");

						for (RelationClassificationType relationClassificationType : this.mainRelationClassificationTypes) {
							this.censusOperationLabels.add("PROFILE_" + relationClassificationType);
							this.censusOperationLabels.add("SUPPORT_" + relationClassificationType);
							if (relationClassificationType == RelationClassificationType.PLACE) {
								this.censusOperationLabels.add("CENTERS_" + relationClassificationType);
								this.censusOperationLabels.add("NRINTERNALMOVES_" + relationClassificationType);
								this.censusOperationLabels.add("NRDIRECTRETURNS_" + relationClassificationType);
								this.censusOperationLabels.add("NRCYCLES_" + relationClassificationType);
								this.censusOperationLabels.add("NRDIRECTRETURNS_NORM_" + relationClassificationType);
								this.censusOperationLabels.add("NRCYCLES_NORM_" + relationClassificationType);
							}
						}

					break;
				}

	}

	public void setCensusType(final CensusType censusType) {
		this.censusType = censusType;
	}*/

	public void setChainClassification(final String chainClassification) {
		this.chainClassification = chainClassification;
	}

	public void setDateLabel(final String dateLabel) {
		this.dateLabel = dateLabel;
	}

	public void setDates(final Integer[] dates) {
		this.dates = dates;
	}

	private void setDefaultCriteria() {

		// Set labels of attribute containing the date [Choose among relation
		// attribute labels]
		this.dateLabel = "DATE";
		this.startDateLabel = "START_DATE";
		this.endDateLabel = "END_DATE";

		// Set labels of attribute concerning the place
		this.placeLabel = "PLACE";
		this.startPlaceLabel = "START_PLACE";
		this.endPlaceLabel = "END_PLACE";

		// Set alter role name [Choose among event role names]
		this.alterFilterRoleName = "ALL";

		// Set event role names [Choose among roles of relationModel that has
		// been chosen as eventModel; in addition: "ALL"]
		this.roleNames = new StringList();
		this.roleNames.add("ALL");
		// *
//		this.roleNames.add("HOST");
//		this.roleNames.add("MIG");

		// Set default roleName of referent [Choose among role names]
		this.defaultReferentRoleName = "HOST";

		// Set age thresholds [Spinner integers with interval 1; Default: min =
		// 0, max = 1000]
		this.minAge = 0;
		this.maxAge = 1000;

		// Set relation model names [Choose among remaining relation model
		// names]
		this.relationModelNames = new ArrayList<String>();
//		this.relationModelNames.add("Apprenticeship");
//		this.relationModelNames.add("Housing");
//		this.relationModelNames.add("Employment");

		// Set relation attribute types [Choose among values of enum
		// RelationClassificationType]
		this.valueSequenceLabels = new ArrayList<ValueSequenceLabel>();

		// Set main relation attribute types types [Choose among values of enum
		// RelationClassificationType]
		this.mainValueSequenceLabels = new ArrayList<ValueSequenceLabel>();

		// Consolidate main event types with event types
		for (ValueSequenceLabel mainEventType : this.mainValueSequenceLabels) {
			if (!this.valueSequenceLabels.contains(mainEventType)) {
				this.valueSequenceLabels.add(mainEventType);
			}
		}

		// Set geography criteria [consolidate with new module!]
//		this.geography = Geography.getInstance();
		this.level = GeoLevel.TOWN;
		// Set names of minimal places to be considered [write as string
		// separated by comma?]
		if (this.censusType == CensusType.GENERAL) {
			this.minimalPlaceNames = new StringList(Arrays.asList(new String[] { "Afagnan", "Lom�", "Bas-Mono", "Togo", "B�nin", "Ghana", "Nig�ria",
					"C�te d'Ivoire", "Afrique" }));
		} else {
			this.minimalPlaceNames = new StringList();
		}

		// Set chain census criteria
		this.pattern = "4 1";
		this.chainClassification = "LINE";
		// threshold = 0.01;

		// Set statistics criteria
		this.individualClassificationType = "GENDER";

		// Set network titles [choose from list]
		this.networkTitles = new ArrayList<String>();
		this.partitionLabels = new HashMap<String, List<String>>();
		for (String networkTitle : this.networkTitles) {
			this.partitionLabels.put(networkTitle, new ArrayList<String>());
		}
		
		// Set network partition Labels [choose from list]

		// Set censusOperationLabels of census operations
		this.sequenceValueLabels = new ArrayList<String>();

		// Set expansion mode and filiation type for extended sequences [choose
		// from values of enums ExpansionMode and FiliationType]
		this.expansionMode = ExpansionMode.RELATED;
		this.filiationType = FiliationType.COGNATIC;
	}

	public void setDefaultReferentRoleName(final String defaultReferentRoleName) {
		this.defaultReferentRoleName = defaultReferentRoleName;
	}

	public void setEgoRoleName(final String egoRoleName) {
		this.egoRoleName = egoRoleName;
	}

	public void setEndDateLabel(final String endDateLabel) {
		this.endDateLabel = endDateLabel;
	}

	public void setEndPlaceLabel(final String endPlaceLabel) {
		this.endPlaceLabel = endPlaceLabel;
	}

	public void setEventTypes(final List<ValueSequenceLabel> relationClassificationTypes) {
		this.valueSequenceLabels = relationClassificationTypes;
	}

	public void setExpansionMode(final ExpansionMode expansionMode) {
		this.expansionMode = expansionMode;
	}

	public void setFiliationType(final FiliationType filiationType) {
		this.filiationType = filiationType;
	}

	public void setGeography(final Geography geography) {
		this.geography = geography;
	}

	public void setLabels(final List<String> labels) {
		this.sequenceValueLabels = labels;
	}

	public void setLevel(final GeoLevel level) {
		this.level = level;
	}

	public void setLocalUnitLabel(final String constantAttributeFilterLabel) {
		this.localUnitLabel = constantAttributeFilterLabel;
	}

	public void setMainEventType(final List<ValueSequenceLabel> mainEventTypes) {
		this.mainValueSequenceLabels = mainEventTypes;
	}

	public void setMaxAge(final int maxAge) {
		this.maxAge = maxAge;
	}

	public void setMinAge(final int minAge) {
		this.minAge = minAge;
	}

	public void setPartitionLabel(final String partitionLabel) {
		this.individualClassificationType = partitionLabel;
	}

	public void setPattern(final String pattern) {
		this.pattern = pattern;
	}

	public void setPlaceLabel(final String placeLabel) {
		this.placeLabel = placeLabel;
	}

	public void setValueSequenceLabels(final List<ValueSequenceLabel> relationClassificationTypes) {
		this.valueSequenceLabels = relationClassificationTypes;
	}

	public void setRelationModelName(final String relationModelName) {
		this.relationModelName = relationModelName;
	}

	public void setRelationModelNames(final List<String> relationModelNames) {
		this.relationModelNames = relationModelNames;
	}

	public void setRoleNames(final List<String> alterRoleNames) {
		this.roleNames = new StringList(alterRoleNames);
	}

	public void setStartDateLabel(final String startDateLabel) {
		this.startDateLabel = startDateLabel;
	}

	public void setStartPlaceLabel(final String startPlaceLabel) {
		this.startPlaceLabel = startPlaceLabel;
	}

	public void setTypes(final List<ValueSequenceLabel> types) {
		this.valueSequenceLabels = types;
	}

	public List<String> getNodeStatisticsLabels() {
		return nodeStatisticsLabels;
	}

	public String getGroupAffiliationLabel() {
		return groupAffiliationLabel;
	}

	public void setGroupAffiliationLabel(String groupAffiliationLabel) {
		this.groupAffiliationLabel = groupAffiliationLabel;
	}
	
	

/*	public List<ReferentNetworkOperation> getReferentNetworkOperations() {
		return referentNetworkOperations;
	}*/

	public String getReferentRelationLabel() {
		return referentRelationLabel;
	}

	public void setReferentRelationLabel(String referentRelationLabel) {
		this.referentRelationLabel = referentRelationLabel;
	}

	public int getReferenceYear() {
		return referenceYear;
	}

	public void setReferenceYear(int referenceYear) {
		this.referenceYear = referenceYear;
	}

	public PartitionCriteriaList getValueSequenceCriteriaList() {
		return valueSequenceCriteriaList;
	}

	public void setValueSequenceCriteriaList(
			PartitionCriteriaList valueSequenceCriteriaList) {
		this.valueSequenceCriteriaList = valueSequenceCriteriaList;
	}
	
	boolean addSequenceValueCriteria(PartitionCriteria criteria){
		boolean result;
		
		if (sequenceValueCriteriaList.contains(criteria)){
			result = false;
		} else {
			result = sequenceValueCriteriaList.add(criteria);
		}
		//
		return result;
	}
	
	boolean addValueSequenceCriteria(PartitionCriteria criteria){
		boolean result;
		
		if (valueSequenceCriteriaList.contains(criteria)){
			result = false;
		} else {
			result = valueSequenceCriteriaList.add(criteria);
		}
		//
		return result;
	}

	public PartitionCriteriaList getSequenceValueCriteriaList() {
		return sequenceValueCriteriaList;
	}

	public void setSequenceValueCriteriaList(
			PartitionCriteriaList sequenceValueCriteriaList) {
		this.sequenceValueCriteriaList = sequenceValueCriteriaList;
	}
	

	public void setValueSequenceCriteria(List<String> labels, List<String> withCensus, List<String> withMatrix, List<String> withGraph, List<String> withDiagram) {
		
		if (this.valueSequenceCriteriaList==null){
			this.valueSequenceCriteriaList = new PartitionCriteriaList();
		}
		
		for (String label : labels){
			
			PartitionCriteria criteria = new PartitionCriteria(label);
			
			if (withCensus != null && withCensus.contains(label)){
				criteria.setWithCensus(true);
			}
			if (withMatrix != null && withMatrix.contains(label)){
				criteria.setWithMatrix(true);
			}
			if (withGraph != null && withGraph.contains(label)){
				criteria.setWithGraph(true);
			}
			if (withDiagram != null && withDiagram.contains(label)){
				criteria.setWithDiagram(true);
			}
			
			valueSequenceCriteriaList.add(criteria);
		}
	}
	
	
	public PartitionCriteria getValueSequenceCriteriaByLabel(String label){
		PartitionCriteria result;
		
		result = null;

		if (valueSequenceCriteriaList != null){

			for (PartitionCriteria partitionCriteria : valueSequenceCriteriaList){
				
				if (partitionCriteria.getLabel().equals(label)){
					
					result = partitionCriteria;
					break;
				}
			}
		}
		//
		return result;
	}

	public int getThreshold() {
		return threshold;
	}

	public void setThreshold(int threshold) {
		this.threshold = threshold;
	}

	public String getImpersonalAlterLabel() {
		return impersonalAlterLabel;
	}

	public void setImpersonalAlterLabel(String impersonalAlterLabel) {
		this.impersonalAlterLabel = impersonalAlterLabel;
	}
	
	


	

	
	
	
}
