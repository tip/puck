package org.tip.puck.kinoath;

import java.awt.Rectangle;
import java.io.IOException;

import nl.mpi.kinnate.kindata.DataTypes;
import nl.mpi.kinnate.kindata.EntityData;
import nl.mpi.kinnate.kindata.RelationTypeDefinition;
import nl.mpi.kinnate.kindata.UnsortablePointsException;
import nl.mpi.kinnate.svg.DiagramSettings;
import nl.mpi.kinnate.svg.EntitySvg;
import nl.mpi.kinnate.svg.OldFormatException;
import nl.mpi.kinnate.svg.SvgDiagram;
import nl.mpi.kinnate.svg.SvgUpdateHandler;
import nl.mpi.kinoath.graph.DefaultSorter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.svg.SVGDocument;

/**
 * 
 * @author TIP
 */
public class KinOathDiagram {

	private static final Logger logger = LoggerFactory.getLogger(KinOathDiagram.class);

	private SvgDiagram svgDiagram;

	/**
	 * 
	 * @param source
	 * @throws IOException
	 * @throws UnsortablePointsException
	 * @throws OldFormatException
	 * @throws DOMException
	 */
	public KinOathDiagram(final EntityData[] source) {

		long timer = org.joda.time.DateTimeUtils.currentTimeMillis();

		//
		final EventListener eventListener = new EventListener() {

			@Override
			public void handleEvent(final Event event) {
				// throw new
				// UnsupportedOperationException("Not supported yet."); //To
				// change body of generated methods, choose Tools | Templates.
			}
		};

		final EntitySvg entitySvg = new EntitySvg(eventListener);

		//
		this.svgDiagram = new SvgDiagram(new DiagramSettings() {

			@Override
			public String defaultSymbol() {
				return "rhombus";
			}

			@Override
			public RelationTypeDefinition[] getRelationTypeDefinitions() {
				return new DataTypes().getReferenceRelations();
			}

			@Override
			public boolean highlightRelationLines() {
				return true;
			}

			@Override
			public boolean showDateLabels() {
				return true;
			}

			@Override
			public boolean showDiagramBorder() {
				return true;
			}

			@Override
			public boolean showExternalLinks() {
				return true;
			}

			@Override
			public boolean showIdLabels() {
				return true;
			}

			@Override
			public boolean showKinTermLines() {
				return true;
			}

			@Override
			public boolean showKinTypeLabels() {
				return true;
			}

			@Override
			public boolean showLabels() {
				return true;
			}

			@Override
			public boolean showSanguineLines() {
				return true;
			}

			@Override
			public boolean snapToGrid() {
				return true;
			}

			@Override
			public void storeAllData(final SVGDocument doc) {
				// throw new
				// UnsupportedOperationException("Not supported yet."); //To
				// change body of generated methods, choose Tools | Templates.
			}
		}, entitySvg);

		//
		try {
			this.svgDiagram.generateDefaultSvg(eventListener, new DefaultSorter());

			final SvgUpdateHandler svgUpdateHandler = new SvgUpdateHandler(this.svgDiagram);

			this.svgDiagram.graphData.setEntitys(source);

			svgUpdateHandler.drawEntities(new Rectangle(1000, 600));

		} catch (IOException exception) {
			// TODO Auto-generated catch block
			exception.printStackTrace();
		} catch (DOMException exception) {
			// TODO Auto-generated catch block
			exception.printStackTrace();
		} catch (OldFormatException exception) {
			// TODO Auto-generated catch block
			exception.printStackTrace();
		} catch (UnsortablePointsException exception) {
			// TODO Auto-generated catch block
			exception.printStackTrace();
		}

		//
		logger.debug("...done (" + (org.joda.time.DateTimeUtils.currentTimeMillis() - timer) + " ms)");
	}

	/**
	 * 
	 * @return
	 */
	public Document getDocument() {
		Document result;

		if (this.svgDiagram == null) {
			//
			result = null;

		} else {
			//
			result = this.svgDiagram.getDoc();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void setEntities(final EntityData[] source) {
		//
		this.svgDiagram.graphData.setEntitys(source);
	}
}
