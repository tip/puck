package org.tip.puck.net;

import org.apache.commons.lang3.StringUtils;

/**
 * This class represents a simple attribute.
 * 
 * @author TIP
 */
public class Attribute {

	private String label;
	private String value;

	/**
	 * 
	 */
	public Attribute() {

		this.label = null;
		this.value = null;
	}

	/**
	 * 
	 * @param label
	 * @param value
	 */
	public Attribute(final String label, final String value) {

		this.label = label;
		this.value = value;
	}

	/**
	 * 
	 */
	@Override
	public Attribute clone() {
		Attribute result;

		result = new Attribute(this.label, this.value);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public String getLabel() {
		return this.label;
	}

	/**
	 * 
	 * @return
	 */
	public String getValue() {
		return this.value;
	}

	/**
	 * 
	 * @param other
	 * @return
	 */
	public boolean hasEqualValue(final Object other) {
		boolean result;

		if (other == null) {
			result = false;
		} else if ((StringUtils.equals(this.label, ((Attribute) other).label)) && (StringUtils.equals(this.value, ((Attribute) other).value))) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isBlank() {
		boolean result;

		if ((StringUtils.isBlank(this.label)) || (StringUtils.isBlank(this.value))) {
			//
			result = true;

		} else {
			//
			result = false;
		}

		//
		return result;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		result = this.label + " " + this.value;

		//
		return result;
	}

}
