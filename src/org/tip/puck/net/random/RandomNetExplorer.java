package org.tip.puck.net.random;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.tip.puck.PuckException;
import org.tip.puck.census.workers.CircuitFinder;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.KinType;
import org.tip.puck.net.Net;
import org.tip.puck.net.workers.MemoryCriteria;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.report.Report;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.statistics.BIASCounts;
import org.tip.puck.statistics.StatisticsWorker;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.RandomUtils;
import org.tip.puck.util.Value;

public class RandomNetExplorer {
	
	private BIASCounts biasCounts;
	private Map<Value,Double[]> closureRates;
	private Double explorationRateIndi;
	private Double meanInformation;
	private Double explorationRateUnions;
	private Double meanDegree;
	public Report getReport() {
		return report;
	}


	private Net virtualNet;
	private Segmentation source;
	private MemoryCriteria criteria;
//	private int genMax;
//	private int runs;
	private Random randGen;
	private Report report;
	Map<Individual,Set<Individual>> neighborSets;
	
	
	public Net getVirtualNet() {
		return virtualNet;
	}


	public Double getMeanDegree() {
		return meanDegree;
	}

	public BIASCounts getBiasCounts(){
		return biasCounts;
	}
	
	
	public Map<Value, Double[]> getClosureRates() {
		return closureRates;
	}


	public RandomNetExplorer (final Segmentation source, final MemoryCriteria criteria, final Report report, final Map<Individual,Set<Individual>> neighborSets){
		 this.source = source;
		 this.criteria = criteria;
		 this.report = report;
		 randGen = new Random();
		 this.neighborSets = neighborSets;
	}
	
	public RandomNetExplorer (final Segmentation source, final MemoryCriteria criteria, final Report report){
		 this.source = source;
		 this.criteria = criteria;
		 this.report = report;
		 randGen = new Random();
	}
	
	private void incrementArray(Double[] total, Double[] inc){
		for (int i=0;i<total.length;i++){
			total[i] = total[i] + inc[i];
		}
	}
	private void divideArray(Double[] total, Double sum){
		for (int i=0;i<total.length;i++){
			total[i] = total[i]/sum;
		}
	}
	
	public void getStatistics (int runs, int genMax) throws PuckException{

		biasCounts = new BIASCounts(genMax,genMax);
		closureRates = new HashMap<Value,Double[]>();
		
		Value agn = new Value("AGNATIC");
		Value ute = new Value("UTERINE");
		
		meanInformation = 0.;
		explorationRateIndi = 0.;
		int totalIndi = source.getAllIndividuals().size();
		int totalUnions = StatisticsWorker.numberOfUnions(source.getAllFamilies());
		
		explorationRateUnions = 0.;
		meanDegree = 0.;
		
		closureRates.put(agn, new Double[]{0.,0.,0.});
		closureRates.put(ute, new Double[]{0.,0.,0.});
		
		for (int run=0; run<runs; run++){
			virtualNet = createRandomNetByObserverSimulation();

			explorationRateIndi+=MathUtils.percent(virtualNet.individuals().size(),totalIndi);
			int unions = StatisticsWorker.numberOfUnions(virtualNet.families());
			explorationRateUnions+=MathUtils.percent(unions,totalUnions);
			meanDegree+=MathUtils.percent(2*unions,100*virtualNet.individuals().size());
	
			biasCounts.add(StatisticsWorker.biasWeights(virtualNet.individuals()));
			
			Double[] agnaticClosure = CircuitFinder.getClosureRates(new Segmentation(virtualNet), "LINE", FiliationType.AGNATIC, genMax).get(agn);
			Double[] uterineClosure = CircuitFinder.getClosureRates(new Segmentation(virtualNet), "LINE", FiliationType.UTERINE, genMax).get(ute);
			
			if (agnaticClosure!=null){
				incrementArray(closureRates.get(agn),agnaticClosure);
//				closureRates.put(agn, closureRates.get(agn)+agnaticClosure);
			}
			if (uterineClosure!=null){
				incrementArray(closureRates.get(ute),uterineClosure);
//				closureRates.put(ute, closureRates.get(ute)+uterineClosure);
			}
		}
		
		biasCounts.divide(new Double(runs));
		explorationRateIndi = explorationRateIndi/new Double(runs);
		explorationRateUnions = explorationRateUnions/new Double(runs);
		meanDegree = meanDegree/new Double(runs);
		meanInformation = meanInformation/new Double(runs*criteria.getNrInformants());

		for (Value key : closureRates.keySet()){
			divideArray(closureRates.get(key),new Double(runs));
//			closureRates.put(key, closureRates.get(key)/new Double(runs));
		}
		
		//
	}
	
	public void setReport(Report report) {
		this.report = report;
	}


	public Double getExplorationRateIndi() {
		return explorationRateIndi;
	}


	public Double getExplorationRateUnions() {
		return explorationRateUnions;
	}


	/**
	 * 
	 * @return
	 * @throws PuckException 
	 */
	public Net createRandomNetByObserverSimulation() throws PuckException {
		Net result;

		Net preresult = new Net();
		
		int nrInterviews = criteria.getNrInformants();
		FiliationType distanceType = criteria.getDistanceType();
		int maxDistance = criteria.getMaxDistance();
	    Individual informant = null;
	    
	    double inertia = criteria.getDistanceWeight();
	    if (meanInformation==null) meanInformation=0.;
	    
	    Set<Individual> oldInformants = new HashSet<Individual>();
		
		int interviews = 0;
		
		List<Integer> groupProfile = new ArrayList<Integer>();
		int groupSize = 0;
		
		while (interviews < nrInterviews) {
			
			Set<Individual> neighbors = new HashSet<Individual>();
			
		    if (distanceType==null || informant ==null){
		    	informant = RandomUtils.draw(source.getCurrentIndividuals().toList());
		    } else {

		    	if (neighborSets!=null){
		    		neighbors = neighborSets.get(informant);
			    	informant = RandomUtils.draw(source.getCurrentIndividuals().toList(),neighbors,randGen,inertia);
		    	} else {
		    		Map<Individual,Integer> distances = StatisticsWorker.distances(informant, maxDistance, distanceType);
			    	Map<Individual, Double> weights = new HashMap<Individual,Double>();
			    	for (Individual neighbor : source.getCurrentIndividuals().toList()){
			    		if (distances.get(neighbor)!=null && distances.get(neighbor)>0 && distanceType.hasLinkingGender(neighbor.getGender())){
			    			weights.put(neighbor, inertia);
//				    		weights.put(neighbor, inertia*new Double(maxDistance)/new Double(distances.get(neighbor)));
			    		} else {
			    			weights.put(neighbor, 1.);
			    		}
					}
			    	informant = RandomUtils.draw(weights,randGen);
		    	}
		    		
		    }
		    
		    if (!oldInformants.contains(informant) && accepts(informant,criteria)){
				interviews++;
				oldInformants.add(informant);
				
				String distance = "";

				if (neighbors.contains(informant)){
					groupSize++;
					distance = distanceType.toString();
				} else if (groupSize!=0){
					groupProfile.add(groupSize);
					groupSize = 1;
					distance = "NOLINK";
				}
				distance+= " "+(groupProfile.size()+1)+"-"+groupSize;
				
				if (report!=null){
					String line = "Informant Nr. "+interviews+" "+" "+informant.signature()+" "+informant.getGender()+" "+distance+" ";
				    report.outputs().appendln(line);
				}
				Map<Individual,Integer> reportedAlters = new HashMap<Individual,Integer>();
				reportedAlters.put(informant,0);
			    explore(preresult, informant, informant,criteria,1,report, reportedAlters);
				meanInformation += reportedAlters.size();
		    }
		}
		if (report!=null && neighborSets!=null){
			groupProfile.add(groupSize);
			report.outputs().appendln();
			report.outputs().appendln(Arrays.toString(groupProfile.toArray()));
		}
		result = NetUtils.buildCleanedNet(preresult);
		result.adjustIndividuals(source.getAllIndividuals());
		result.setLabel(source.getLabel()+" explored "+criteria.getDistanceFactor()+" "+criteria.getDistanceWeight());

		//
		return result;
	}
	
	
	public Double getMeanInformation() {
		return meanInformation;
	}


	private static boolean accepts (Individual informant, MemoryCriteria criteria){
		boolean result;
		if (informant.isMale() && RandomUtils.event(criteria.getMaleAcceptance()) ||
				(informant.isFemale() && RandomUtils.event(criteria.getFemaleAcceptance()))){
			result = true;
		} else {
			result = false;
		}
		//
		return result;
	}

	
	private static void explore(Net net, Individual informant, Individual ego, MemoryCriteria criteria, int distance, Report report, Map<Individual,Integer> reportedAlters) throws PuckException{

		double factor = Math.pow(criteria.getDistanceFactor(),distance);
		
		String inset = "\t";
		for (int i=0;i<distance;i++){
			inset = inset+"\t";
		}
		
		for (KinType type : KinType.basicTypes()){
			for (Individual alter : ego.getKin(type)){
				if (alter.getGender()==Gender.UNKNOWN) continue; // temporary condition to avoid ArrayIndexOutOfBoundsException
				if (reportedAlters.get(alter)==null || reportedAlters.get(alter)>distance){
					reportedAlters.put(alter,distance);
					NetUtils.setKinRelation(net,ego,alter,type);
					double memory = factor*criteria.getMemory(ego.getGender(),alter.getGender(),type, informant.getGender());
					if (report!=null){
						report.outputs().appendln(distance+" "+inset+memory+" "+type.toString(alter.getGender())+" "+alter.signature());
					}
					if (RandomUtils.event(memory)){
						explore(net, informant, alter,criteria,distance+1,report,reportedAlters);
					}
				}
			}
		}
	}
	

	


	

}
