package org.tip.puck.io.kinsources;

import java.util.Comparator;

import org.tip.puck.util.MathUtils;
import org.tip.puck.util.PuckUtils;

/**
 * 
 * @author TIP
 */
public class CatalogItemComparator implements Comparator<CatalogItem> {

	public enum Criteria {
		AUTHOR,
		CONTRIBUTOR,
		DATASET_NAME,
		ID,
		INDIVIDUAL_COUNT,
		NAME,
	}

	private Criteria criteria;

	/**
	 * 
	 * @param criteria
	 */
	public CatalogItemComparator(final Criteria criteria) {
		//
		this.criteria = criteria;
	}

	/**
	 * 
	 */
	@Override
	public int compare(final CatalogItem alpha, final CatalogItem bravo) {
		int result;

		//
		result = compare(alpha, bravo, this.criteria);

		//
		return result;
	}

	/**
	 * 
	 */
	public static int compare(final CatalogItem alpha, final CatalogItem bravo, final Criteria criteria) {
		int result;

		switch (criteria) {
			case AUTHOR: {
				//
				String alphaValue;
				if (alpha == null) {
					//
					alphaValue = null;
				} else {
					//
					alphaValue = alpha.getAuthor();
				}

				//
				String bravoValue;
				if (bravo == null) {
					//
					bravoValue = null;
				} else {
					//
					bravoValue = bravo.getAuthor();
				}

				//
				result = PuckUtils.compare(alphaValue, bravoValue);
			}
			break;

			case CONTRIBUTOR: {
				//
				String alphaValue;
				if (alpha == null) {
					//
					alphaValue = null;
				} else {
					//
					alphaValue = alpha.getContributor();
				}

				//
				String bravoValue;
				if (bravo == null) {
					//
					bravoValue = null;
				} else {
					//
					bravoValue = bravo.getContributor();
				}

				//
				result = PuckUtils.compare(alphaValue, bravoValue);
			}
			break;

			case ID: {
				//
				Long alphaValue;
				if (alpha == null) {
					//
					alphaValue = null;
				} else {
					//
					alphaValue = alpha.getId();
				}

				//
				Long bravoValue;
				if (bravo == null) {
					//
					bravoValue = null;
				} else {
					//
					bravoValue = bravo.getId();
				}

				//
				result = MathUtils.compare(alphaValue, bravoValue);
			}
			break;

			case NAME: {
				//
				String alphaValue;
				if (alpha == null) {
					//
					alphaValue = null;
				} else {
					//
					alphaValue = alpha.getName();
				}

				//
				String bravoValue;
				if (bravo == null) {
					//
					bravoValue = null;
				} else {
					//
					bravoValue = bravo.getName();
				}

				//
				result = PuckUtils.compare(alphaValue, bravoValue);
			}
			break;

			default:
				throw new IllegalStateException("Unknown criteria [" + criteria + "]");
		}

		//
		return result;
	}
}
