package org.tip.puck.alliancenets;

import java.io.BufferedWriter;
import java.io.FileWriter;

import org.tip.puck.evo.EvoGen;
import org.tip.puck.evo.EvoGenCallbacks;
import org.tip.puck.evo.Generator;

/**
 * @author Telmo Menezes
 * @author TIP
 */
public class EvoAllianceGenNoCLI implements EvoGenCallbacks {
	private AllianceGen gen;
	private TopologicalIndices targIndices;
	private int targNodeCount;
	private int targEdgeCount;
	private int nodeCount;
	private int edgeCount;
	private int bestCount;

	/**
	 * 
	 * @param targNet
	 * @param outDir
	 */
	public EvoAllianceGenNoCLI(final AllianceNet targNet) {
		targIndices = genIndices(targNet);

		nodeCount = targNet.getNodeCount();
		edgeCount = targNet.getEdgeCount();
		targNodeCount = nodeCount;
		targEdgeCount = edgeCount;

		gen = new AllianceGen(nodeCount, edgeCount, targIndices);

		bestCount = 0;
	}

	/**
	 * 
	 */
	@Override
	public Generator baseGenerator() {
		return gen;
	}

	/**
	 * 
	 */
	@Override
	public double computeFitness(final Generator gen) {
		AllianceGen allGen = (AllianceGen) gen;
		allGen.run();
		AllianceNet allianceNet = allGen.getNet();
		TopologicalIndices indices = new TopologicalIndices(allianceNet);
		allGen.setIndices(indices);

		double dist = indices.distance(targIndices);

		if (Double.isNaN(dist)) {
			return Double.POSITIVE_INFINITY;
		} else {
			return dist;
		}
	}

	/**
	 * 
	 * @param allianceNet
	 * @return
	 */
	private TopologicalIndices genIndices(final AllianceNet allianceNet) {
		return new TopologicalIndices(allianceNet);
	}

	/**
	 * 
	 */
	@Override
	public String infoString() {
		String str = "target net node count: " + targNodeCount + "\n";
		str += "target net edge count: " + targEdgeCount + "\n";
		str += "generated nets node count: " + nodeCount + "\n";
		str += "generated nets edge count: " + edgeCount + "\n";

		return str;
	}

	/**
	 * 
	 */
	@Override
	public void onGeneration(final EvoGen evo) {
		AllianceGen bestGen = (AllianceGen)evo.getBestGenerator();
        
		System.out.println(targIndices);
		
        System.out.println("" + evo.getCurgen() + ","
                + evo.getBestFitness() + ","
                + evo.getBestGenFitness() + ","
                + bestGen.getIndices().getEndogamousPercentage() + ","
                + bestGen.getIndices().getNetworkConcentration() + ","
                + bestGen.getIndices().getEndogamicNetworkConcentration() + ","
                + bestGen.getIndices().getNetworkSymmetry() + ","
                + bestGen.getIndices().getParallels() + ","
                + bestGen.getIndices().getCrosses() + ","
                + bestGen.getIndices().getLoopTri() + ","
                + bestGen.getIndices().getTranTri() + ","
                + evo.getBestGenerator().genotypeSize() + ","
                + evo.getMeanGenoSize() + ","
                + evo.getGenTime() + ","
                + evo.getSimTime() + ","
                + evo.getFitTime() + "\n");
	}

	/**
	 * 
	 */
	@Override
	public void onNewBest(final EvoGen evo) {
		Generator bestGen = evo.getBestGenerator();
		bestCount++;
	}
}