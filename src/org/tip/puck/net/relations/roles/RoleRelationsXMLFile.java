package org.tip.puck.net.relations.roles;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;
import javax.xml.stream.XMLStreamException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.net.AlterAge;
import org.tip.puck.net.Gender;
import org.tip.puck.net.KinType;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.Role;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Partition;
import org.tip.puck.report.Report;
import org.tip.puck.util.Value;
import org.tip.puckgui.PuckGUI;

import fr.devinsy.util.xml.XMLBadFormatException;
import fr.devinsy.util.xml.XMLReader;
import fr.devinsy.util.xml.XMLTag;
import fr.devinsy.util.xml.XMLTag.TagType;
import fr.devinsy.util.xml.XMLWriter;

public class RoleRelationsXMLFile {
	
	public static final String DEFAULT_CHARSET_NAME = "UTF-8";
	public static final int MAX_LINE_SIZE = 1024;;
	public static final String XSD_FILE = "/org/tip/puck/io/puc/puck-1.3.xsd";
	public static final String PUCK_FORMAT_VERSION = "1.3";
	private static final Logger logger = LoggerFactory.getLogger(RoleRelationsXMLFile.class);

	/**
	 * Saves a report in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 * @throws IOException 
	 */
	public static Report save(final File file, final RelationModel source) throws PuckException, IOException {
		Report result;
		
		result = new Report("Export log");
		
		RoleDefinitions definitions = RoleRelationMaker.createRoleDefinitions(source.getRoleRelations(),source.getRules(),result);
		RoleRelationMaker.compareDefinitions(source.getRoleRelations().getOriginalDefinitions(),definitions,source.getRoleRelations(),result);

		for (Gender egoGender : Gender.valuesNotUnknown()){
			
			File genderedFile = new File(file.getCanonicalPath().substring(0, file.getCanonicalPath().indexOf(".term.xml"))+"_"+egoGender.toGedChar()+".term.xml");
			
			XMLWriter out = null;
			try {
				out = new XMLWriter(new OutputStreamWriter(new FileOutputStream(genderedFile), "UTF-8"));
				write(out,source,genderedFile.getName(),egoGender);

			} catch (UnsupportedEncodingException exception) {
				throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + genderedFile + "]");
			} catch (FileNotFoundException exception) {
				throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + genderedFile + "]");
			} finally {
				if (out != null) {
					out.close();
				}
			}
		}
		
		//
		return result;
	}
	
	/**
	 * Saves a report in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
/*	public static Report write(final PrintWriter out, final RelationModel source) throws PuckException {
		Report result;
		
		result = new Report("Export log");
		
		RoleDefinitions definitions = RoleRelationMaker.createRoleDefinitions(source.getRoleRelations(),source.getRules(),result);
		RoleRelationMaker.compareDefinitions(source.getRoleRelations().getOriginalDefinitions(),definitions,source.getRoleRelations(),result);
			
		for (RoleDefinition definition: definitions.toSortedList()){
			out.println(definition.toString());
		}
		//
		return result;
	}*/
	

	
	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 * @throws XMLBadFormatException 
	 */
	public static String load(final File file, final String charsetName, final List<RoleActorPair> list) throws PuckException, XMLBadFormatException {
		String result;

		result = null;
		XMLReader in = null;
		try {
			in = new XMLReader(new InputStreamReader(new FileInputStream(file), charsetName));
			result = read(in,list);

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
			}
		}

		//
		return result;
	}
	
	public static String read(final XMLReader in, List<RoleActorPair> list) throws PuckException, XMLStreamException, XMLBadFormatException {
		String result;

		in.readXMLHeader();
		
		in.readStartTag("KintermMap");
		in.readContentTag("Name").getContent();

		Map<String,List<String>> variables = readVariables(in);
		result = readKinTerms(variables, in, list);
		
		//
		return result;
	}
	
	public static Map<String,List<String>> readVariables(final XMLReader in) throws XMLStreamException, XMLBadFormatException, PuckException {
		Map<String,List<String>> result;
		
		result = new HashMap<String,List<String>>();
		
		in.readStartTag("VariablesFactory");
		
		XMLTag variables = in.readListTag("Variables");

		//
		if (variables.getType() != TagType.EMPTY) {
			
			while (in.hasNextStartTag("Variable")) {

				XMLTag variable = in.readStartTag("Variable");
				List<String> list = new ArrayList<String>();
				result.put(variable.attributes().getByLabel("Name").getValue(), list);

				XMLTag variableValueProtos = in.readListTag("VariableValueProtos");
				
				//
				if (variableValueProtos.getType() != TagType.EMPTY) {

					XMLTag tag = in.readTag();
					while (tag.getLabel().equals("VariableValueProto")) {
						list.add(tag.attributes().getByLabel("Value").getValue());
						tag = in.readTag();
					}

				}
				//
//				in.readEndTag("VariableValueProtos");
				
				XMLTag variableValues = in.readListTag("VariableValues");
				
				if (variableValues.getType() != TagType.EMPTY) {

					XMLTag tag = in.readTag();
					while (tag.getLabel().equals("VariableValue")) {
						list.add(tag.attributes().getByLabel("Value").getValue());
						tag = in.readTag();
					}
				}
				
				//
//				in.readEndTag("VariableValues");
				//
				in.readEndTag("Variable");
			}
		}
		//
		in.readEndTag("Variables");
		//
		in.readEndTag("VariablesFactory");
		//
		return result;
	}
	
	private static String getMetaRoleName(String idx){
		String result;
		
		result = null;
		
		if (idx.equals("-1")){
			result = "SIBLING";
		} else if (idx.equals("0")){
			result = "PARENT";
		} else if (idx.equals("1")){
			result = "CHILD";
		} else if (idx.equals("4")){
			result = "SPOUSE";
		}
		//
		return result;
	}
	
	private static Integer getOrientation(KinType kinType){
		Integer result;
		
		result = -1;
		
		if (kinType!=null){
			switch (kinType){
			case SIBLING:
				result = -1;
				break;
			case PARENT:
				result = 0;
				break;
			case CHILD:
				result = 1;
				break;
			case SPOUSE:
				result = 4;
				break;
			}
		}
		
		//
		return result;
	}
	
	private static RoleActor updateRoleActor (RoleActor actor, Map<String,MetaRole> metaRoles, String selfName, Gender egoGender, Gender alterGender){
		RoleActor result;
		
		result = actor.clone();
		
		MetaRole metaRole = metaRoles.get(actor.getRole().toString());
		if (metaRole!=null){
			result.setRole(metaRole);
		}

		result = result.withEgoGender(egoGender);
		if (alterGender!=null && !alterGender.isUnknown()){
			result = result.withAlterGender(alterGender);
		}
		result.setSelfName(selfName);
		
		//
		return result;
	}
	
	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws PuckException
	 * @throws Exception
	 */
	public static String readKinTerms(final Map<String,List<String>> variables, final XMLReader in, List<RoleActorPair> result) throws XMLStreamException, XMLBadFormatException, PuckException {
		String selfName;
		
		XMLTag list = in.readStartTag("Kinterms");
		Map<String,MetaRole> metaRoles = new HashMap<String,MetaRole>();
		
		List<String> genderTokens = variables.get("Sex");
		selfName = null;
		Gender egoGender = null;
		
		Map<String,Gender> genderByTerm = new HashMap<String,Gender>();

		//
		if (list.getType() != TagType.EMPTY) {

			XMLTag kinTerm = in.readTag();
			
			while (kinTerm.getLabel().equals(("Kinterm"))) {

				// <xs:element name="Kinterm" type="tns:Individual" />

				//
				
					// <xs:element name="name" type="xs:normalizedString" />
					String name = in.readNullableContentTag("Term").getContent();
					if (name == null) {
						name = "";
					}
					if (selfName==null){
						selfName = name;
					}
					Role selfTerm = new Role(name);

					// <xs:element name="gender" type="tns:Gender" />
					String genderToken = in.readContentTag("Sex").getContent();
					Gender gender;
					if (StringUtils.equals(genderToken, genderTokens.get(0))) {
						//
						gender = Gender.MALE;

					} else if (StringUtils.equals(genderToken, genderTokens.get(1))) {
						//
						gender = Gender.FEMALE;

					} else if (StringUtils.equals(genderToken, genderTokens.get(2))) {
						//
						gender = Gender.UNKNOWN;

					} else if (StringUtils.equals(genderToken, "N") || (StringUtils.equals(genderToken, "X"))) {
						//
						gender = Gender.UNKNOWN;

					} else if (StringUtils.equals(genderToken, "#")) {
						//
						gender = Gender.UNKNOWN;

					} else {
						//
						throw PuckExceptions.BAD_FILE_FORMAT.create("Unknown gender [" + genderToken + "]");
					}
					
					genderByTerm.put(name, gender);
					if (egoGender==null){
						egoGender = gender;
					}
					
					RoleActor self = new RoleActor(selfTerm,new MetaRole("SELF",Gender.UNKNOWN,gender,AlterAge.UNKNOWN),selfName);
					if (StringUtils.equals(genderToken, "#")){
						self.getRole().setCross(true);
					}

					String isGen = in.readContentTag("IsGen").getContent();
					String orientation = in.readContentTag("Orientation").getContent();
					
					if (isGen.equals("true")){
						metaRoles.put(name, new MetaRole(getMetaRoleName(orientation),Gender.UNKNOWN,gender,AlterAge.UNKNOWN));
					}
					
					in.readStartTag("Origin");
					in.readContentTag("x");
					in.readContentTag("y");
					in.readEndTag("Origin");
					in.readContentTag("Covered");
					in.readContentTag("Etc");
					in.readContentTag("CoveringTerm");
					in.readContentTag("CoveredTerms");
					
					XMLTag variablesList = in.readListTag("Variables");
					if (variablesList.getType() != TagType.EMPTY) {
						while (in.hasNextStartTag("Variable")) {
							in.readStartTag("Variable");
							XMLTag variableValues = in.readListTag("VariableValues");
							if (variableValues.getType() != TagType.EMPTY) {
								while (in.readTag().getLabel().equals("VariableValue")) {
								}
							}
//							in.readEndTag("VariableValues");
							in.readEndTag("Variable");
						}
					}
					in.readEndTag("Variables");
					
					XMLTag products = in.readListTag("Products");
					if (products.getType() != TagType.EMPTY) {
						XMLTag generator = in.readTag();
						while (generator.getLabel().equals("Generator")) {
							
							if (generator.getType() == TagType.START){
//								in.readStartTag("Generator");
								XMLTag product = in.readTag();
								while(product.getLabel().equals("Product")){
									Role alterTerm = new Role(product.getContent());
									RoleActor alter = new RoleActor(alterTerm,new MetaRole(generator.attributes().getByLabel("Name").getValue(),Gender.UNKNOWN,null,AlterAge.UNKNOWN),selfName);
									result.add(new RoleActorPair(self,alter));
									
									if (selfName==name && !alterTerm.getName().equals(alter.getRole().getName())){
										selfName = null;
										egoGender = null;
									}

									product = in.readTag();
								}
//								in.readEndTag("Generator");
							}
							generator = in.readTag();
							
						}
					}
//					in.readEndTag("Products");

				//
				in.readEndTag("Kinterm");
				kinTerm = in.readTag();
			}

			//
//			in.readEndTag("Kinterms");
		}
		
		for (RoleActorPair pair : result){
			
			RoleActor self = pair.getSelf();
			Gender alterGender = null;
			if (self.getRole().isCross() && self.getAlterGender().isUnknown()){
				alterGender = egoGender.invert();
				genderByTerm.put(self.getName(),alterGender);
			}
			pair.setSelf(updateRoleActor(self,metaRoles,selfName,egoGender,alterGender));
		}
		
		for (RoleActorPair pair : result){

			RoleActor alter = pair.getAlter();
			pair.setAlter(updateRoleActor(alter,metaRoles,selfName,egoGender,genderByTerm.get(alter.getName())));
		}
		
		//
		return selfName;
	}
	
	public static void write(final XMLWriter out, final RelationModel source, final String sourceFileName, final Gender egoGender) {
			
		out.writeXMLHeader();
		out.writeStartTag("KintermMap", "xmlns", "urn:schema:PUCK/" + PUCK_FORMAT_VERSION, "xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance", "version",
				"PUCK-" + PUCK_FORMAT_VERSION, "generator", "PUCK", "date", DatatypeConverter.printDateTime(new GregorianCalendar()), "filename",
				sourceFileName);

		out.writeTag("Name", source.getName());
		
		// Sterile code, not yet understood what it means in KAES
		out.writeStartTag("VariablesFactory");
		out.writeStartTag("Variables");
		out.writeStartTag("Variable","Name","Sex");
		out.writeStartTag("VariableValueProtos");
		for (Gender gender : Gender.valuesNotUnknown()){
			out.writeEmptyTag("VariableValueProto", "Value",gender.toGedChar()+"");
		}
		out.writeEndTag("VariableValueProtos");
		out.writeStartTag("VariableValues");
		out.writeEmptyTag("VariableValue", "Value","*");
		out.writeEndTag("VariableValues");
		out.writeEndTag("Variable");
		out.writeEndTag("Variables");
		out.writeEndTag("VariablesFactory");

		// <xs:element name="relations" type="tns:Relations" />
		writeRoleRelations(out, source, egoGender);

		out.writeEndTag("KintermMap");
		
	}


	/**
	 * 
	 * @param relations
	 * @return
	 */
	public static void writeRoleRelations(final XMLWriter out, final RelationModel model, final Gender egoGender) {

		RoleRelations relations = model.getRoleRelations();
		
		if ((relations == null) || (relations.isEmpty())) {
			// <xs:element name="relations" type="tns:Relations" />
			out.writeEmptyTag("Kinterms");

		} else {
			// <xs:element name="relations" type="tns:Relations" />
			out.writeStartTag("Kinterms");

			Map<RoleActor,RoleActors> kinTermMap = new RoleRelationWorker(model,PuckGUI.instance().getPreferences().getTerminologyMaxIterations()).getRoleRelationMap(egoGender);
			
			for (RoleActor self : kinTermMap.keySet()) {
				
					self = self.withEgoGender(egoGender);
					
					// <xs:element name="relation" type="tns:Relation" />
					out.writeStartTag("Kinterm");

					//
					{
						// <xs:element name="term" type="tns:token" />
						out.writeTag("Term", self.getName());
						
						// <xs:element name="sex" type="tns:token" />
						if (self.getRole().cross){
							out.writeTag("Sex", "#");
						} else {
							out.writeTag("Sex", self.getAlterGender().toGedChar()+"");
						}
						
						out.writeTag("IsGen", relations.getGeneratorTerms(self)!=null);
						out.writeTag("Orientation",getOrientation(relations.getElementaryKinType(self))+"");
						out.writeStartTag("Origin");
						out.writeTag("x", "0");
						out.writeTag("y", "0");
						out.writeEndTag("Origin");
						out.writeTag("Covered", false);
						out.writeTag("Etc", false);
						out.writeTag("CoveringTerm", "null");
						out.writeTag("CoveredTerms", "null");
						
						out.writeStartTag("Variables");
						out.writeStartTag("Variable", "Name","Sex");
						out.writeStartTag("VariableValues");
						for (Gender gender : Gender.valuesNotUnknown()){
							if (gender.specifies(self.getAlterGender())){
								out.writeEmptyTag("VariableValue", "Value", gender.toGedChar()+"");
							}
						}
						out.writeEndTag("VariableValues");
						out.writeEndTag("Variable");
						out.writeEndTag("Variables");

						out.writeStartTag("Products");
						Partition<RoleActor> byGenerators = new Partition<RoleActor>();
												
						for (RoleActor alter : kinTermMap.get(self)){
							for (Role generatorTerm : relations.getGeneratorTerms(alter)){
								byGenerators.put(alter, new Value(generatorTerm.toString()));
							}
						}
						
						for (Cluster<RoleActor> byGenerator : byGenerators.getClusters()){
							out.writeStartTag("Generator", "Name", byGenerator.getValue().stringValue());
							for (RoleActor product : byGenerator.getItems()){
								//
								out.writeTag("Product", product.getName());
							}
							//
							out.writeEndTag("Generator");
						}
						//
						out.writeEndTag("Products");
					}
					//
					out.writeEndTag("Kinterm");
			}
			//
			out.writeEndTag("Kinterms");
		}
	}






}
