package org.tip.puck.net.workers;

import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Gender;
import org.tip.puck.net.KinType;

public class MemoryCriteria {
	
	private int nrInformants;
	
	private double[][][][] memoryProbs;
	
	private double distanceFactor;
	private double maleAcceptance;
	private double femaleAcceptance;
	
	private FiliationType distanceType;
	private int maxDistance;
	private double distanceWeight;
	
	public double getDistanceWeight() {
		return distanceWeight;
	}

	public void setDistanceWeight(double distanceWeight) {
		this.distanceWeight = distanceWeight;
	}

	public int getMaxDistance() {
		return maxDistance;
	}

	public void setMaxDistance(int maxDistance) {
		this.maxDistance = maxDistance;
	}

	public FiliationType getDistanceType() {
		return distanceType;
	}

	public void setDistanceType(FiliationType distanceType) {
		this.distanceType = distanceType;
	}

	public MemoryCriteria(){

		memoryProbs = new double[2][2][3][2];
		
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				for (int k = 0; k < 3; k++) {
					for (int l = 0; l < 2; l++) {
						memoryProbs[i][j][k][l] = 1.;
					}
				}
			}
		}
		
		nrInformants = 100;
		distanceFactor = 1.;
		
		maleAcceptance = 1.;
		femaleAcceptance = 1.;
		
		maxDistance = 0;
		distanceWeight = 1.;
		
	}
	
	private int genderToInt(Gender gender) {
		switch(gender) {
		case MALE:
			return 0;
		case FEMALE:
			return 1;
		default:
			return -1;
		}
	}
	
	private int kinTypeToInt(KinType kinType) {
		switch(kinType) {
		case PARENT:
			return 0;
		case CHILD:
			return 1;
		case SPOUSE:
			return 2;
		default:
			return -1;
		}
	}
	
	private static Gender intToGender(int val) {
		switch(val) {
		case 0:
			return Gender.MALE;
		case 1:
			return Gender.FEMALE;
		default:
			return Gender.UNKNOWN;
		}
	}
	
	private static KinType intToKinType(int val) {
		switch(val) {
		case 0:
			return KinType.PARENT;
		case 1:
			return KinType.CHILD;
		case 2:
			return KinType.SPOUSE;
		default:
			return KinType.UNKNOWN;
		}
	}
	
	public static String describe(int ego, int alter, int ktype, int informant) {
		Gender egoGender = intToGender(ego);
		Gender alterGender = intToGender(alter);
		KinType kinType = intToKinType(ktype);
		Gender informantGender = intToGender(informant);
		
		String desc = "";
		switch(egoGender) {
		case MALE:
			desc += "man's ";
			break;
		case FEMALE:
			desc += "woman's ";
			break;
		default:
		}
		switch(kinType) {
		case PARENT:
			desc += "parent(";
			break;
		case CHILD:
			desc += "child(";
			break;
		case SPOUSE:
			desc += "spouse(";
			break;
		default:
		}
		switch(alterGender) {
		case MALE:
			desc += "male)";
			break;
		case FEMALE:
			desc += "female)";
			break;
		default:
		}
		switch(informantGender) {
		case MALE:
			desc += ": male informant";
			break;
		case FEMALE:
			desc += ": female informant";
			break;
		default:
		}
		
		return desc;
	}
	
	public double getMemory(Gender egoGender, Gender alterGender, KinType kinType, Gender informantGender) {
		int i = genderToInt(egoGender);
		int j = genderToInt(alterGender);
		int k = kinTypeToInt(kinType);
		int l = genderToInt(informantGender);
		
		return memoryProbs[i][j][k][l];
	}
	
	public double getMemory(int egoGender, int alterGender, int kinType, int informantGender) {
		return memoryProbs[egoGender][alterGender][kinType][informantGender];
	}
	
	public void setMemoryProb(Gender egoGender, Gender alterGender, KinType kinType, Gender informantGender, double prob) {
		int i1 = genderToInt(egoGender);
		int j1 = genderToInt(alterGender);
		int k1 = kinTypeToInt(kinType);
		int l1 = genderToInt(informantGender);
		
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				for (int k = 0; k < 3; k++) {
					for (int l = 0; l < 2; l++) {
						if (((i1 < 0) || (i == i1))
							&& ((j1 < 0) || (j == j1))
							&& ((k1 < 0) || (k == k1))
							&& ((l1 < 0) || (l == l1))) {
							
							memoryProbs[i][j][k][l] = prob;
						}
					}
				}
			}
		}
	}

	public double getDistanceFactor() {
		return distanceFactor;
	}

	public void setDistanceFactor(double distanceFactor) {
		this.distanceFactor = distanceFactor;
	}

	public int getNrInformants() {
		return nrInformants;
	}

	public void setNrInformants(int nrInformants) {
		this.nrInformants = nrInformants;
	}

	public double getMaleAcceptance() {
		return maleAcceptance;
	}

	public double getFemaleAcceptance() {
		return femaleAcceptance;
	}

	public void setMaleAcceptance(double maleAcceptance) {
		this.maleAcceptance = maleAcceptance;
	}

	public void setFemaleAcceptance(double femaleAcceptance) {
		this.femaleAcceptance = femaleAcceptance;
	}	
}
