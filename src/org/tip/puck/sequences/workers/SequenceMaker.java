package org.tip.puck.sequences.workers;

import org.tip.puck.PuckException;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.workers.ExpansionMode;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.EgoSequence;
import org.tip.puck.sequences.EgoSequences;
import org.tip.puck.sequences.Ordinal;
import org.tip.puck.sequences.Sequence;
import org.tip.puck.sequences.SequenceType;
import org.tip.puck.sequences.Sequences;

public class SequenceMaker {


	/**
	 * creates sequence with variable ego roles
	 * @param ego
	 * @return
	 */
	public static EgoSequence createSequence (Individual ego, SequenceCriteria criteria){
		EgoSequence result;
		
		result = new EgoSequence(ego);
//		result.setDateLabel(criteria.getDateLabel());
		
/*		for (Relation relation : ego.relations()){
			if ((relation.getAttributeValue("START_PLACE")==null || relation.getAttributeValue("END_PLACE")==null) && !(relation.getAttributeValue("START_PLACE")==null && relation.getAttributeValue("END_PLACE")==null)){
				continue;
			}
			result.putInOrder(relation);
		}*/
		
		result.putInOrder(ego.relations(), SequenceType.MOVES);

		
		//
		return result;
	}
	
	/**
	 * creates sequence with constant ego role
	 * @param ego
	 * @param criteria
	 * @return
	 */
	public static EgoSequence createPersonalEventSequence (Individual ego, Segmentation segmentation, SequenceCriteria criteria){
		EgoSequence result;
		
		String egoRoleName = criteria.getEgoRoleName();
		
		result = new EgoSequence(ego);
//		result.setEgoRoleName(egoRoleName);
//		result.setDateLabel(criteria.getDateLabel());
		
		Relations relations = new Relations();
		
		for (Relation relation : ego.relations()){
			if (segmentation.getCurrentRelations().contains(relation) && relation.getRoleNames(ego).contains(egoRoleName)){
				relations.add(relation);
			}
		}
		
/*		Collections.sort(list,new EventComparator(criteria));
		for (Relation event : relations){
			result.putInOrder(event);
		}*/
		
		result.putInOrder(relations, SequenceType.EVENTS);

		
//		result.setAlters();
		
		//
		return result;
	}
	
	public static EgoSequences createPersonalSequences (Segmentation segmentation, SequenceCriteria criteria){
		EgoSequences result;
		
		result = new EgoSequences();
		
		for (Individual individual : segmentation.getCurrentIndividuals().toSortedList()){
			result.add(createPersonalEventSequence(individual, segmentation, criteria));
		}
		//
		return result;
	}


	/**
	 * creates sequences including life events
	 * @param net
	 * @param segmentation
	 * @return
	 * @throws PuckException
	 */
	public static EgoSequences createBiographies (Net net, Segmentation segmentation, SequenceCriteria criteria) throws PuckException{
		EgoSequences result;
		
		result = new EgoSequences();
		
		net.getFamilyEvents(segmentation);
		
		for (Individual individual : segmentation.getCurrentIndividuals()){
			result.add(createSequence(individual, criteria));
		}
	
		net.removeFamilyEvents();
		
		//
		return result;
		
	}

	/**
	 * create sequence for multiple egos
	 * @param individual
	 * @param criteria
	 * @return
	 */
	public static EgoSequence createExtendedSequence (Individual individual, SequenceCriteria criteria){
		EgoSequence result;
		
		result = new EgoSequence(individual);
//		result.setDateLabel(criteria.getDateLabel());
		
		
		// Too comprehensive - certain roles (such as "OTHER" should be excluded)
		
		result.putInOrder(individual.relations(), SequenceType.EVENTS);
		
/*		for (Relation relation : individual.relations()){
			result.putInOrder(relation);
		}*/
		
		ExpansionMode expansionMode = criteria.getExpansionMode();
		FiliationType filiationType = criteria.getFiliationType();

		for (Individual relative : NetUtils.neighbors(individual, expansionMode, filiationType)){
			
/*			for (Relation relation : relative.relations()){
				result.putInOrder(relation);
			}*/
			
			result.putInOrder(relative.relations(), SequenceType.EVENTS);
		}
		
		//
		return result;
	}
	
	public static EgoSequences createExtendedBiographies (Net net, Segmentation segmentation, SequenceCriteria criteria) throws PuckException{
		EgoSequences result;
		
		result = new EgoSequences();
		
		net.getFamilyEvents(segmentation);
		
		for (Individual individual : segmentation.getCurrentIndividuals()){
			result.add(SequenceMaker.createExtendedSequence(individual,criteria));
		}
	
		net.removeFamilyEvents();

		//
		return result;
		
	}
	
	public static Sequences<Sequence<Relation>,Relation> createRelationSequences(Segmentation segmentation, SequenceCriteria criteria){
		Sequences<Sequence<Relation>,Relation> result;
		
		result = new Sequences<Sequence<Relation>,Relation>();
		
		int id = 0;

		for (Relation station : segmentation.getCurrentRelations().getByModelName(criteria.getRelationModelName())){
			
			String stringId = station.getAttributeValue(criteria.getLocalUnitLabel());
			
			if (stringId != null){

				id = Integer.parseInt(stringId);
				
			} else {
				
				id = 0;
			}
			
				Sequence<Relation> sequence = result.getById(id);
				
				if (sequence == null){
					sequence = new Sequence<Relation>(criteria.getLocalUnitLabel(), id);
					result.put(sequence);
				}
				
				String stringTime = station.getAttributeValue(criteria.getDateLabel());
				if (stringTime != null){
					
					int time = Integer.parseInt(stringTime);
					
					if (sequence.getStation(new Ordinal(time))==null){
						
						sequence.put(new Ordinal(time),station.ageFiltered(criteria.getMinAge(),time));
						
					} else {
						
						System.err.println("Ambigous local unit label: "+criteria.getLocalUnitLabel()+" "+sequence.getStation(new Ordinal(time))+" vs "+station);
					}
				}
			
		}
		//
		return result;
	}

	public static Sequence<Relations> createRelationSetSequence(Segmentation segmentation, SequenceCriteria criteria) throws PuckException{
		Sequence<Relations> result;
		
		result = new Sequence<Relations>(criteria.getLocalUnitLabel(),0);
		
		for (int time : criteria.getDates()){
			Relations relations = new Relations();
			relations.setId(time);
			result.put(new Ordinal(time), relations);
		}
	
		for (Relation relation : segmentation.getCurrentRelations().getByModelName(criteria.getRelationModelName()).getByAttribute(criteria.getLocalUnitLabel(), null)){
			
			Integer time = relation.getTime(criteria.getDateLabel());
						
			if (time!=null && result.getTimes().contains(new Ordinal(time))){
	
				result.getStation(new Ordinal(time)).put(relation); 
			}
		}
		
		
		//
		return result;
		
	}

	public static EgoSequence createPersonalStateSequence (Individual individual, Segmentation segmentation, SequenceCriteria sequenceCriteria){
		EgoSequence result;
		
		result = new EgoSequence(individual);
				
//		Ordinal previousTime = null;
		
		for (Ordinal time : Ordinal.getOrdinals(sequenceCriteria.getDates())){
			
			if (sequenceCriteria.getMaxAge()<1000 && IndividualValuator.ageAtYear(individual, time.getYear()) >= sequenceCriteria.getMaxAge()){

				continue;
			}

			if (sequenceCriteria.getMinAge()>0 && IndividualValuator.ageAtYear(individual, time.getYear()) < sequenceCriteria.getMinAge()){

				continue;
			}

			// Warning! supposes that there is only one relation by year... (relations with lower ID are preferred)
			Relation relation = individual.relations().getByModelName(sequenceCriteria.getRelationModelName()).getByTime(sequenceCriteria.getDateLabel(), time.getYear()).getFirst();
						
			if (relation != null && segmentation.getCurrentRelations().contains(relation)  && relation.getRoleNames(individual).contains(sequenceCriteria.getEgoRoleName())){
				
				result.put(time,relation);
				
				// Extrapolation of previous dates should be made in specific transformation operation not as part of analysis method
				
/*				if (previousTime!=null && result.getStation(previousTime)==null){
					Relation previousRelation = individual.relations().getByModelName(sequenceCriteria.getRelationModelName()).getPredecessors(relation, individual, sequenceCriteria.getEgoRoleName(), sequenceCriteria.getDateLabel(), sequenceCriteria.getStartDateLabel(), sequenceCriteria.getEndDateLabel(), previousTime.getYear()).getFirst();
					if (previousRelation!=null){
						result.put(previousTime, previousRelation);
					} 
				}*/
			}
//			previousTime = time;
		}
		//
		return result;
	}		



}
