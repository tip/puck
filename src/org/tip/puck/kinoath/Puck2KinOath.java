package org.tip.puck.kinoath;

import java.util.HashMap;

import nl.mpi.kinnate.kindata.DataTypes;
import nl.mpi.kinnate.kindata.EntityData;
import nl.mpi.kinnate.kindata.EntityData.SymbolType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;

/**
 * This class contains some static method to use KinOath in PUCK.
 * 
 * @author TIP
 */
public class Puck2KinOath {

	private static final Logger logger = LoggerFactory.getLogger(Puck2KinOath.class);

	/**
	 * This method transforms an individual list to a data list usable in
	 * KinOath framework.
	 * 
	 * @param source
	 * @return
	 */
	public static EntityData[] convert(final Individuals source) {

		EntityData[] result;

		long timer = org.joda.time.DateTimeUtils.currentTimeMillis();

		HashMap<Long, EntityData> entities = new HashMap<Long, EntityData>();

		// Create entities.
		for (Individual individual : source) {
			//
			SymbolType gender;
			switch (individual.getGender()) {
				case FEMALE:
					gender = SymbolType.circle;
				break;

				case MALE:
					gender = SymbolType.triangle;
				break;

				default:
					gender = SymbolType.square;
			}

			//
			EntityData entity = new EntityData(individual.getId(), new String[] { individual.getName() }, gender, false, null, null);

			//
			entities.put((long) individual.getId(), entity);
		}

		// Link entities.
		for (Individual individual : source) {
			//
			if (individual.getOriginFamily() != null) {
				//
				EntityData entity = entities.get((long) individual.getId());

				//
				for (Individual parent : individual.getParents()) {
					//
					EntityData parentEntity = entities.get((long) parent.getId());

					//
					if (parentEntity != null) {
						//
						entity.addRelatedNode(parentEntity, DataTypes.RelationType.ancestor, null, null, null, null);
					}
				}

				//
				for (Individual partner : individual.getPartners()) {
					//
					EntityData partnerEntity = entities.get((long) partner.getId());

					//
					if (partnerEntity != null) {
						//
						entity.addRelatedNode(partnerEntity, DataTypes.RelationType.union, null, null, null, null);
					}
				}

				//
				for (Individual child : individual.children()) {
					//
					EntityData childEntity = entities.get((long) child.getId());

					//
					if (childEntity != null) {
						//
						entity.addRelatedNode(childEntity, DataTypes.RelationType.descendant, null, null, null, null);
					}
				}
			}
		}

		// Build the result array.
		result = new EntityData[entities.size()];
		int count = 0;
		for (EntityData entity : entities.values()) {
			result[count] = entity;
			count += 1;
		}

		//
		logger.debug("...done (" + (org.joda.time.DateTimeUtils.currentTimeMillis() - timer) + " ms)");

		//
		return result;
	}
}
