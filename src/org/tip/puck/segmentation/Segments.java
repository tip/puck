package org.tip.puck.segmentation;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.partitions.PartitionCriteria;

/**
 * 
 * @author TIP
 */
public class Segments extends ArrayList<Segment> {

	private static final long serialVersionUID = -3414487104570655587L;

	/**
	 * 
	 */
	public Segments() {
		//
		super();
	}

	/**
	 * 
	 * @param individuals
	 * @param criteria
	 * @return
	 * @throws PuckException
	 */
	public Segment addSegment(final Segment source, final PartitionCriteria criteria) throws PuckException {
		Segment result;

		//
		if ((source == null) || (criteria == null) || (StringUtils.isBlank(criteria.getRelationModelName()))) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");

		} else {
			//
			if (criteria.getRelationModelName().equals(PartitionCriteria.RelationModelCanonicalNames.INDIVIDUAL.toString())) {
				//
				result = new IndividualSegment(source, criteria);

			} else if (criteria.getRelationModelName().equals(PartitionCriteria.RelationModelCanonicalNames.FAMILY.toString())) {
				//
				result = new FamilySegment(source, criteria);

			} else {
				//
				result = new RelationSegment(source, criteria);
			}

			//
			this.add(result);
		}

		//
		return result;
	}
}
