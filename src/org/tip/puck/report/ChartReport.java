package org.tip.puck.report;

import java.util.ArrayList;
import java.util.List;

import org.tip.puck.sequences.Ordinal;
import org.tip.puck.sequences.Sequenceable;

public class ChartReport extends Report {
	
	private List<ReportChart> charts;
	private List<ReportTable> tables;

	
	public ChartReport(String title){
		super(title);
		
		charts = new ArrayList<ReportChart>(20);
		tables = new ArrayList<ReportTable>(20);		

	}
	
	public void addChartWithTables(ReportChart chart, String title){
		
		if (chart != null) {
			
			chart.setTitle(title);
			charts.add(chart);
		
			ReportTable table = ReportTable.transpose(chart.createReportTableWithSum());
			table.setTitle(chart.getTitle());
			tables.add(table);
/*			
			if (!label.contains("EVENTS_") && !label.contains("RELATIONS")) {
				tables.add(ReportTable.normalize(table));
			}*/
		}
		
	}
	
	public void addChartsWithTables(Sequenceable<ReportChart> chartSequence, String title){
		
		for (Ordinal time : chartSequence.getTimes()){
			
			ReportChart chart = chartSequence.getStation(time);
			if (chart != null) {
				
				chart.setTitle(title+" "+time);
				charts.add(chart);
			
				ReportTable table = ReportTable.transpose(chart.createReportTableWithSum());
				table.setTitle(chart.getTitle());
				tables.add(table);
			}
		}
	}

	public void addChart(ReportChart chart) {
		charts.add(chart);
	}


	public void addTable(ReportTable table) {
		tables.add(table);
	}
	
	public void arrangeChartsAndTables(int width){
		
		int nr = Math.min(4, width);
		
		// Manage the number of chart by line.
		for (int chartIndex = 0; chartIndex < charts.size(); chartIndex++) {
			outputs().append(charts.get(chartIndex));
			if (chartIndex % nr == nr-1) {
				outputs().appendln();
			}
		}

		// Add chart tables.
		for (ReportTable table : tables) {
			outputs().appendln(table.getTitle());
			outputs().appendln(table);
		}
	}


	

}
