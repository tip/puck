package org.tip.puck.io.permutation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.io.ods.ODSBufferedReader;
import org.tip.puck.io.xls.XLSBufferedReader;

/**
 * This class represents a TXT File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class PermutationFile {
	private static final Logger logger = LoggerFactory.getLogger(PermutationFile.class);

	public static final Pattern LINE_PATTERN = Pattern.compile("(\\d+)\\s*[\\s,;|]\\s*(\\d+)");

	/**
	 * 
	 * @param pathname
	 * @return
	 * @throws PuckException
	 * @throws Exception
	 */
	public static PermutationTable load(final File file) throws PuckException {
		PermutationTable result;

		if (file == null) {
			result = new PermutationTable();
		} else if (!file.exists()) {
			throw PuckExceptions.FILE_NOT_FOUND.create("File=[" + file + "].");
		} else if (!file.isFile()) {
			throw PuckExceptions.NOT_A_FILE.create("File=[" + file + "].");
		} else if (file.getName().toLowerCase().endsWith(".txt")) {
			result = loadTXTFile(file);
		} else if (file.getName().toLowerCase().endsWith(".ods")) {
			result = loadODSFile(file);
		} else if (file.getName().toLowerCase().endsWith(".xls")) {
			result = loadXLSFile(file);
		} else {
			throw PuckExceptions.UNSUPPORTED_FILE_FORMAT.create("File=[" + file + "].");
		}

		//
		return result;
	}

	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static PermutationTable loadODSFile(final File file) throws PuckException {
		PermutationTable result;

		BufferedReader in = null;
		try {
			in = new ODSBufferedReader(file);
			result = read(in);

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} catch (Exception exception) {
			exception.printStackTrace();
			throw PuckExceptions.IO_ERROR.create("Opening file [" + file + "]");
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException exception) {
				logger.warn("Not managed error.");
				exception.printStackTrace();
			}
		}

		//
		return result;
	}

	/**
	 * Loads a TXT file into a permutation table.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static PermutationTable loadTXTFile(final File file) throws PuckException {
		PermutationTable result;

		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
			result = read(in);
		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException exception) {
				logger.warn("Not managed error.");
				exception.printStackTrace();
			}
		}

		//
		return result;
	}

	/**
	 * Loads a XLS file into a permutation table.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static PermutationTable loadXLSFile(final File file) throws PuckException {
		PermutationTable result;

		BufferedReader in = null;
		try {
			in = new XLSBufferedReader(file);
			result = read(in);

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create(exception, "Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create(exception, "Opening file [" + file + "]");
		} catch (IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Opening file [" + file + "]");
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException exception) {
				logger.warn("Not managed error.");
				exception.printStackTrace();
			}
		}

		//
		return result;
	}

	/**
	 * Reads a permutation table from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static PermutationTable read(final BufferedReader in) throws PuckException {
		PermutationTable result;

		try {
			result = new PermutationTable();

			//
			int lineCount = 0;
			boolean ended = false;
			while (!ended) {
				//
				String line = in.readLine();
				// logger.debug("line=[" + line + "]");

				if (StringUtils.isBlank(line)) {
					ended = true;
				} else {
					//
					Matcher matcher = LINE_PATTERN.matcher(line);

					//
					if ((matcher.find()) && (matcher.groupCount() == 2)) {
						//
						int source = Integer.parseInt(matcher.group(1));
						int target = Integer.parseInt(matcher.group(2));

						//
						result.add(source, target);
					} else {
						throw PuckExceptions.BAD_FILE_FORMAT.create("Bad line format: [" + line + "]");
					}
				}
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading line.");
		}

		logger.debug("Done.");

		//
		return result;
	}
}
