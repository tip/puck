package org.tip.puck.net.relations;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Populatable;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.Ordinal;
import org.tip.puck.util.NumberablesHashMap;

import fr.devinsy.util.StringList;

/**
 * The <code>Relations</code> class represents a relation collection.
 * 
 * @author TIP
 */
public class Relations extends NumberablesHashMap<Relation> implements Populatable {

	/**
	 * 
	 */
	public Relations() {
		super();
	}

	/**
	 * 
	 */
	public Relations(final int capacity) {
		super(capacity);
	}

	/**
	 * 
	 * @param source
	 */
	public Relations(final List<Relation> source) {
		super();

		add(source);
	}

	/**
	 * 
	 * @param source
	 */
	public Relations(final Relations source) {
		super();

		add(source);
	}

	/**
	 * 
	 * @return
	 */
	public int actorCount() {
		int result;

		result = 0;
		for (Relation relation : this) {
			result += relation.actors().size();
		}
		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public Relations add(final List<Relation> source) {
		Relations result;

		result = put(source);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public Relations add(final Relations source) {
		Relations result;

		result = put(source);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Actors getActors() {
		Actors result;

		result = new Actors();

		for (Relation relation : this) {
			result.addAll(relation.actors());
		}
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public StringList getAttributeLabels() {
		StringList result;

		//
		result = new StringList();

		//
		for (Relation relation : this) {
			for (String label : relation.attributes().labels()) {
				if (!result.contains(label)) {
					result.add(label);
				}
			}
		}

		//
		return result;
	}

	/***
	 * 
	 * @param ids
	 * @return
	 */
	public Relations getByIds(final Integer[] ids) {
		Relations result;

		if (ids == null) {
			result = new Relations();
		} else {
			result = new Relations(ids.length);
			for (Integer id : ids) {
				Relation relation = getById(id);
				if (relation != null) {
					result.add(relation);
				}
			}
		}

		//
		return result;
	}

	public Relations getByIndividual(final Individual individual) {
		Relations result;

		result = new Relations();
		for (Relation relation : individual.relations()) {
			if (contains(relation)) {
				result.add(relation);
			}
		}
		//
		return result;
	}

	/**
	 * 
	 * @param model
	 * @return
	 */
	public Relations getByModel(final RelationModel model) {
		Relations result;

		result = new Relations();
		for (Relation relation : this) {
			if (relation.getModel() == model) {
				result.add(relation);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param model
	 * @return
	 */
	public Relations getByModelName(final String name) {
		Relations result;

		//
		result = new Relations();

		//
		if (StringUtils.isNotBlank(name)) {
			for (Relation relation : this) {
				if (relation.getModel().getName().equals(name)) {
					result.add(relation);
				}
			}
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @param model
	 * @return
	 */
	public Relations getByPartialModelName(final String name) {
		Relations result;

		//
		result = new Relations();

		//
		if (StringUtils.isNotBlank(name)) {
			for (Relation relation : this) {
				if (StringUtils.containsIgnoreCase(relation.getModel().getName(), name)) {
					result.add(relation);
				}
			}
		}

		//
		return result;
	}


	/**
	 * 
	 * @param model
	 * @return
	 */
	public Relations getByModelNames(final StringList names) {
		Relations result;

		//
		result = new Relations();

		//
		for (String name : names) {
			//
			if (StringUtils.isNotBlank(name)) {
				//
				for (Relation relation : this) {
					//
					if (relation.getModel().getName().equals(name)) {
						//
						result.add(relation);
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param model
	 * @return
	 */
	public Relations getByName(final String name) {
		Relations result;

		//
		result = new Relations();

		//
		if (StringUtils.isNotBlank(name)) {
			for (Relation relation : this) {
				if (relation.getName().equals(name)) {
					result.add(relation);
				}
			}
		}

		//
		return result;
	}
	
	public Relations getPredecessors(final Relation relation, final Individual individual, final String roleName, final String dateLabel, final String startDateLabel, final String endDateLabel, final int limit){
		Relations result;
		
		result = new Relations();
		Actor actor = relation.actors().get(individual.getId(), roleName);
		
		// Extrapolate from date of arrival
		if (actor!=null){
			for (Relation otherRelation : this){
				Actor otherActor = otherRelation.actors().get(individual.getId(), roleName);
				if (otherActor!=null && otherActor.getAttributeValue(endDateLabel)!=null && otherActor.getAttributeValue(endDateLabel).equals(actor.getAttributeValue(startDateLabel))) {
					result.add(otherRelation);
				}
			}
		}
		
		// Extrapolate from last known date
		if (result.size()==0){
			Relation nextPrecedingRelation = null;
			Integer nextPrecedingYear = limit;
			Integer year = relation.getTime(dateLabel);
			for (Relation otherRelation : this){
				Integer otherYear = otherRelation.getTime(dateLabel);
				if (otherYear!=null && otherYear<year && (otherYear > nextPrecedingYear)){
					nextPrecedingYear = year;
					nextPrecedingRelation = otherRelation;
				}
			}
			if (nextPrecedingRelation!=null){
				result.add(nextPrecedingRelation);
			}
		}
		
		//
		return result;
	}
	
	public Relations getByAttribute (final String attributeLabel, final String attributeValue){
		Relations result;
		
		if (attributeLabel == null) {
			result = null;
		} else {
			result = new Relations();
			for (Relation relation : this) {
				if (relation.hasAttributeValue(attributeLabel)){
					if (attributeValue==null || (relation.getAttributeValue(attributeLabel).equals(attributeValue))){
						result.add(relation);
					}
				}
			}
		}
		//
		return result;
	}

	public Relations getByTime(final String dateLabel, final Integer time) {
		Relations result;

		if (time == null) {
			result = this;
		} else {
			result = new Relations();
			for (Relation relation : this) {
				if (relation.getTime(dateLabel)!=null && relation.getTime(dateLabel).equals(time)) {
					result.add(relation);
				}
			}
		}
		
		//
		return result;
	}

	/**
	 * This method searches for a relation from a subId and a model.
	 * 
	 * Relations can contains relations from different models. Each relation is
	 * defined by:
	 * <ul>
	 * <li>A id: it is different for each relation.
	 * <li>A typed id: it is different for each relation depending a model.
	 * </ul>
	 * 
	 * @param typedId
	 *            The typed id of the relation searched.
	 * 
	 * @param model
	 *            The model of the relation searched.
	 * 
	 * @return The relation corresponding to the model and the typed id.
	 */
	public Relation getByTypedId(final int typedId, final RelationModel model) {
		Relation result;

		boolean ended = false;
		Iterator<Relation> iterator = iterator();
		result = null;
		while (!ended) {
			//
			if (iterator.hasNext()) {
				//
				Relation relation = iterator.next();

				//
				if ((relation.getTypedId() == typedId) && (relation.getModel().equals(model))) {
					//
					ended = true;
					result = relation;
				}
			} else {
				//
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param typeId
	 * @param model
	 * @return
	 */
	public Relations getByTypedIds(final Integer[] typedIds, final String modelName) {
		Relations result;

		result = new Relations();

		for (Relation relation : this.getByModelName(modelName)) {
			for (Integer typedId : typedIds) {
				if (relation.getTypedId() == typedId) {
					result.add(relation);
				}
			}

		}
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int getFirstFreeTypedId() {
		int result;

		int[] ids = new int[this.size()];
		int relationIndex = 0;
		for (Relation relation : this) {
			ids[relationIndex] = relation.getTypedId();
			relationIndex += 1;
		}

		if (ids.length == 0) {
			result = 1;
		} else {
			Arrays.sort(ids);

			boolean ended = false;
			int index = 0;
			result = -1;
			while (!ended) {
				if (index < ids.length - 1) {
					if (ids[index] + 1 == ids[index + 1]) {
						index += 1;
					} else {
						ended = true;
						result = ids[index] + 1;
					}
				} else {
					ended = true;
					result = ids[index] + 1;
				}
			}
		}

		//
		return result;
	}
	
	public Individuals getAllIndividuals (List<Ordinal> times) {
		return getIndividuals();
	}

	public Individuals getIndividuals() {
		Individuals result;

		result = new Individuals();

		for (Relation relation : this) {
			for (Individual individual : relation.getIndividuals()) {
				if (!result.contains(individual)) {
					result.add(individual);
				}
			}
		}

		//
		return result;
	}
	
	public Individuals getIndividuals(Segmentation segmentation){
		Individuals result;
		
		result = new Individuals();
		
		for (Individual individual : getIndividuals()){
			if (segmentation.getCurrentIndividuals().contains(individual)){
				result.add(individual);
			}
		}
		
		//
		return result;

	}


	/**
	 * 
	 * @return
	 */
	public int getLastTypedId() {
		int result;

		result = 0;
		for (Relation relation : this.data.values()) {
			if (relation.getTypedId() > result) {
				result = relation.getTypedId();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public RelationModels getRelationModels() {
		RelationModels result;

		//
		result = new RelationModels();

		//
		for (Relation relation : this) {
			if (!result.contains(relation.getModel())) {
				result.add(relation.getModel());
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param strategy
	 * @return
	 */
	public int nextFreeTypedId(final IdStrategy strategy) {
		int result;

		if (strategy == null) {
			result = 0;
		} else {
			switch (strategy) {
				case FILL:
					result = getFirstFreeTypedId();
				break;

				case APPEND:
					result = getLastTypedId() + 1;
				break;

				case SIZE:
					result = this.size() + 1;
				break;

				default:
					result = 0;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public Relations put(final List<Relation> source) {
		Relations result;

		//
		if (source != null) {
			for (Relation relation : source) {
				this.add(relation);
			}
		}

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public Relations put(final Relations source) {
		Relations result;

		//
		if (source != null) {
			for (Relation relation : source) {
				this.add(relation);
			}
		}

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param relation
	 */
	public void remove(final Relation relation) {
		if (relation != null) {
			this.removeById(relation.getId());
		}
	}

	/**
	 * 
	 * @param relationModel
	 * @param sourceRole
	 */
	public void removeRelationRole(final RelationModel model, final Role role) {
		if ((model != null) && (role != null)) {
			//
			Relations relations = this.getByModel(model);
			for (Relation relation : relations) {
				Actors actors = relation.actors().getByRole(role);
				for (Actor actor : actors) {
					relation.actors().remove(actor);
					if (relation.actors().getById(actor.getId()) == null) {
						actor.getIndividual().relations().removeById(relation.getId());
					}
				}
			}

			//
			model.roles().remove(role);
		}
	}

	/**
	 * 
	 * @param pattern
	 * @return
	 */
	public List<Relation> searchByName(final String pattern) {
		List<Relation> result;

		Relations relations = new Relations();
		if (StringUtils.isNotBlank(pattern)) {
			//
			for (Relation relation : this) {
				if (relation.matches(pattern)) {
					relations.add(relation);
				}
			}
		}

		result = relations.toSortedList();

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Relation> toListSortedByTypeId() {
		List<Relation> result;

		result = toList();
		Collections.sort(result, new Comparator<Relation>() {
			@Override
			public int compare(final Relation relation1, final Relation relation2) {
				int result;

				result = relation1.getTypedId() - relation2.getTypedId();

				//
				return result;
			}
		});

		//
		return result;
	}
	
	public Relations getTransitiveClosure(RelationModel model){
		Relations result;
		
		result = new Relations();
		
		for (Relation relation : this){
			if (relation.actors()!=null && relation.actors().size()>0){
				int idx = relation.actors().toSortedList().get(0).getId();
				Relation transitiveRelation = result.getById(idx);
				if (transitiveRelation == null){
					transitiveRelation = new Relation(idx,idx,model,relation.getName());
					result.put(transitiveRelation);
				}
				for (Actor actor : relation.actors()){
					if (!transitiveRelation.hasActor(actor)){
						transitiveRelation.actors().add(actor);
					}
				}
			}
		}
		//
		return result;
	}
	
	public String toString (){
		return toSortedList().toString();
	}
	
	public Relations ageFiltered(int threshold, int time) {
		Relations result;
		
		if (threshold == 0) {
			
			result = this;
			
		} else {
			
			result = new Relations();
			
			for (Relation relation : this) {
				
				result.add(relation.ageFiltered(threshold,time));
			}
		}
		
		//
		return result;
	}

}
