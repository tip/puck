package org.tip.puck.graphs.random;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.Link;
import org.tip.puck.graphs.Node;
import org.tip.puck.graphs.Nodes;
import org.tip.puck.util.RandomUtils;
import fr.devinsy.util.StringList;
import org.tip.puck.util.ToolBox;

import umontreal.iro.lecuyer.randvar.NormalGen;
import umontreal.iro.lecuyer.randvar.ParetoGen;
import umontreal.iro.lecuyer.randvar.PowerGen;
import umontreal.iro.lecuyer.randvar.RandomVariateGen;
import umontreal.iro.lecuyer.rng.RandomStream;
import umontreal.iro.lecuyer.rng.WELL607;

/**
 * 
 * @author Klaus Hamberger
 * 
 * @param <E>
 */
public class RandomGraphMaker<E> {

	int arcWeightSum;
	int maxArcWeightSum;
	int nodeCount;
	double[] inertia;
	double outPreference;
	TreeMap<Double, Node<E>> nodeChoiceMap;
	Map<Node<E>, Double> egoWeightMap;
	Map<Node<E>, Double> alterWeightMap;
	Map<Node<E>,Set<Node<E>>> exhaustedInNeighbors;
	Map<Node<E>,Set<Node<E>>> exhaustedOutNeighbors;
	Set<Node<E>> exhaustedInNodes;
	Set<Node<E>> exhaustedOutNodes;
	Map<Node<E>,Set<Link<E>>>  exhaustedInLinks;
	Map<Node<E>,Set<Link<E>>>  exhaustedOutLinks;
	List<double[]> probas;
	DistributionType dist;
	Random randGen;
	
	StringList protocol;
	
	public RandomGraphMaker() {
	}

	public RandomGraphMaker(final RandomCriteria criteria) {

		maxArcWeightSum = criteria.getArcWeightSum();
		nodeCount = criteria.getNodeCount();
		outPreference = criteria.getOutPreference();
		dist = criteria.getDistributionType();

		protocol = new StringList();
		inertia = criteria.getInertia();
		randGen = new Random();

	}

	public Graph<E> createRandomGraph(final int nodeCount, final int arcCount) {
		Graph<E> result;

		result = new Graph<E>(nodeCount);
		for (int k = 0; k < arcCount; k++) {
			result.incArcWeight((int) (Math.random() * nodeCount + 1), (int) (Math.random() * nodeCount)+1);
		}

		//
		return result;
	}
	
	private boolean isExhausted(Node<E> node){
		boolean result;
		if (exhaustedInNodes==null || exhaustedOutNodes==null){
			result = false;
		} else {
			result = exhaustedInNodes.contains(node) && exhaustedOutNodes.contains(node);
		}
		//
		return result;
	}
	
	private void updateWeightMap (Graph<E> graph, Node<E> ego){

		//Update Weights
		alterWeightMap = new HashMap<Node<E>, Double>();
		for (Node<E> node : graph.getNodes()){
			if (isExhausted(node)){
				alterWeightMap.put(node, 0.);
			} else if (egoWeightMap==null){
				alterWeightMap.put(node, 1.);
			} else {
				alterWeightMap.put(node, egoWeightMap.get(node));
			}
		}
		Map<Integer,Nodes<E>> neighbors = ego.getNeighbors(graph.getNodes(), inertia.length);
		for (int distance = 0;distance<inertia.length;distance++){
			for (Node<E> node : neighbors.get(distance)){
				if (node!=ego || distance==0){
					alterWeightMap.put(node, alterWeightMap.get(node)*inertia[distance]);
				}
			}
		}

		// Update Probas
		double[] proba = new double[inertia.length];
		double maxValue = 0.;
		for (double value : alterWeightMap.values()){
			maxValue += value;
		}
		for (int distance = 0;distance<inertia.length;distance++){
			for (Node<E> node : neighbors.get(distance)){
				proba[distance] += alterWeightMap.get(node);
			}
			proba[distance]=proba[distance]/maxValue;
		}
		probas.add(proba);
	}
	
/*	private void updateChoiceMap (Graph<E> graph, Node<E> ego){
		
		Map <Node<E>, Double> preferenceMap = new HashMap<Node<E>, Double>();
		double[] proba = new double[inertia.length];
		
		for (Node<E> node : graph.getNodes()){
			preferenceMap.put(node, 1.);
		}
		
		Map<Integer,Nodes<E>> neighbors = ego.getNeighbors(graph.getNodes(), inertia.length);
		
		for (int distance = 0;distance<inertia.length;distance++){
			for (Node<E> node : neighbors.get(distance)){
				preferenceMap.put(node, preferenceMap.get(node)*inertia[distance]);
			}
		}
		
		nodeChoiceMap = RandomUtils.<Node<E>>getChoiceMap(preferenceMap);
		
		double maxValue = nodeChoiceMap.lastKey();
		
		for (int distance = 0;distance<inertia.length;distance++){
			for (Node<E> node : neighbors.get(distance)){
				proba[distance] += preferenceMap.get(node);
			}
			proba[distance]=proba[distance]/maxValue;
		}
		probas.add(proba);
	}*/
	
	public double[][] getProbaEvolution(){
		return ToolBox.toArray(probas);
	}
	
	/**
	 * generates a RandomGraph by Agent Behavior
	 * 
	 * @since 11-05-22 (modified 11-07-02; recoded for Puck 2.0 12-05-17)
	 */
	public Graph<E> createRandomGraphByAgentSimulation() {
		Graph<E> result;

		result = new Graph<E>(nodeCount);
		arcWeightSum = 0;
		probas = new ArrayList<double[]>();
		
		int factor = 1;
		
//		TreeMap<Double,Node<E>> distribution = getDistributionMap(result.getNodes(), dist, maxArcCount, factor);

		// Initialize Ego Distribution
		if (dist!=DistributionType.FREE){
			Map<Integer,Double> weightDistribution = getRandomDistribution(dist,nodeCount,maxArcWeightSum,factor);
			egoWeightMap = new HashMap<Node<E>,Double>();
			for (Node<E> node : result.getNodes()){
				egoWeightMap.put(node,weightDistribution.get(node.getId()-1));
			}
		}

		for (int k=0; k< maxArcWeightSum; k++){

//			Node<E> ego = draw(result.getNodes(), distribution);
//			updateChoiceMap(result, ego);
//			Node<E> alter = RandomUtils.draw(nodeChoiceMap);
			
			Node<E> ego;
			
			if (egoWeightMap==null){
				ego = RandomUtils.draw(result.getNodes().toList());
			} else {
				ego = RandomUtils.draw(egoWeightMap,randGen);
			}
			
			updateWeightMap(result,ego);
			
			Node<E> alter = RandomUtils.draw(alterWeightMap,randGen);

			if (alter != null) {
				// outgoing arc
				if (RandomUtils.event(outPreference)) {
					result.incArcWeight(ego, alter);
					// incoming arc
				} else {
					result.incArcWeight(alter, ego);
				}
				arcWeightSum++;
			}
		}
		
		//
		return result;
	}
	
	private void setExhaustedInNeighbors(Node<E> ego, Node<E> neighbor){
		if (exhaustedInNeighbors.get(ego)==null){
			exhaustedInNeighbors.put(ego,new HashSet<Node<E>>());
		}
		exhaustedInNeighbors.get(ego).add(neighbor);
	}
	
	private List<Node<E>> getUnexhaustedInNodes(Node<E> ego){
		List<Node<E>> result;
		
		result = new ArrayList<Node<E>>();
		Set<Node<E>> exhausted = exhaustedInNeighbors.get(ego);
		for (Node<E> neighbor : ego.getInNodes()){
			if (!exhausted.contains(neighbor)){
				result.add(neighbor);
			}
		}
		//
		return result;
	}
	
	private void setExhaustedOutNeighbors(Node<E> ego, Node<E> neighbor){
		if (exhaustedOutNeighbors.get(ego)==null){
			exhaustedOutNeighbors.put(ego,new HashSet<Node<E>>());
		}
		exhaustedOutNeighbors.get(ego).add(neighbor);
	}
	
	private List<Node<E>> getUnexhaustedOutNodes(Node<E> ego){
		List<Node<E>> result;
		
		result = new ArrayList<Node<E>>();
		Set<Node<E>> exhausted = exhaustedOutNeighbors.get(ego);
		for (Node<E> neighbor : ego.getOutNodes()){
			if (!exhausted.contains(neighbor)){
				result.add(neighbor);
			}
		}
		//
		return result;
	}
	
	private Map<Node<E>,Double> getUnexhaustedWeightedOutNodes(Node<E> ego, final Graph<E> source, final Graph<E> target){
		Map<Node<E>,Double> result;
		
		result = new HashMap<Node<E>,Double>();
		for (Node<E> neighbor : ego.getOutNodes()){
			double surplus = source.getArcWeight(ego.getId(), neighbor.getId()) - target.getArcWeight(ego.getId(), neighbor.getId());
			if (surplus>0){
				result.put(neighbor,surplus);
			}
		}
		//
		return result;
	}

	private Map<Node<E>,Double> getUnexhaustedWeightedInNodes(Node<E> ego, final Graph<E> source, final Graph<E> target){
		Map<Node<E>,Double> result;
		
		result = new HashMap<Node<E>,Double>();
		for (Node<E> neighbor : ego.getInNodes()){
			double surplus = source.getArcWeight(neighbor.getId(), ego.getId()) - target.getArcWeight(neighbor.getId(), ego.getId());
			if (surplus>0){
				result.put(neighbor,surplus);
			}
		}
		//
		return result;
	}
	

	/***
	 * generates a RandomGroupNet based on another RandomGroupNet (modelization
	 * of observer behaviour)
	 * 
	 * @param basicNet
	 *            the underlying RandomGroupNet (result of agent behaviour)
	 * @since 11-07-02
	 */
	public Graph<E> createRandomGraphByObserverSimulation(final Graph<E> source) {

		Graph<E> result;
		result = new Graph<E>(source.nodeCount());

		exhaustedOutNodes = new HashSet<Node<E>>();
		exhaustedInNodes = new HashSet<Node<E>>();
		exhaustedInNeighbors = new HashMap<Node<E>,Set<Node<E>>>();
		exhaustedOutNeighbors = new HashMap<Node<E>,Set<Node<E>>>();
		exhaustedInLinks = new HashMap<Node<E>,Set<Link<E>>>();
		exhaustedOutLinks = new HashMap<Node<E>,Set<Link<E>>>();

		for (Node<E> nodeModel : source.getNodes()){
			result.addNode(nodeModel.getId(), nodeModel.getReferent());
			exhaustedInNeighbors.put(nodeModel,new HashSet<Node<E>>());
			exhaustedOutNeighbors.put(nodeModel,new HashSet<Node<E>>());
			exhaustedInLinks.put(nodeModel,new HashSet<Link<E>>());
			exhaustedOutLinks.put(nodeModel,new HashSet<Link<E>>());
		}

		arcWeightSum = 0;
		probas = new ArrayList<double[]>();

		Node<E> egoModel = RandomUtils.draw(source.getNodes().toList());

		int egoId = egoModel.getId();
		Node<E> ego = result.getNode(egoId);
		
		StringList protocol = new StringList();
		
		while (arcWeightSum < maxArcWeightSum) {
			
			if (RandomUtils.event(outPreference)) {
				Node<E> alterModel = RandomUtils.draw(getUnexhaustedWeightedOutNodes(egoModel,source,result),randGen);
				if (alterModel != null) {
					int alterId = alterModel.getId();
					// if-else clause can be dropped, only links with positive surplus can be drawn - old code left only for test phase
					if (result.getArcWeight(egoId, alterId) < source.getArcWeight(egoId, alterId)) {
						result.incArcWeight(egoId, alterId);
						arcWeightSum++;
						protocol.appendln(egoId+" "+alterId);
					} else {
						setExhaustedOutNeighbors(egoModel, alterModel);
						protocol.appendln("exhausted link "+egoId+" "+alterId);
					}
				} else {
					exhaustedOutNodes.add(ego);
					protocol.appendln("exhausted out: "+ego.getId());
				}
			} else {
				Node<E> alterModel = RandomUtils.draw(getUnexhaustedWeightedInNodes(egoModel,source,result),randGen);
				if (alterModel != null) {
					int alterId = alterModel.getId();
					if (result.getArcWeight(alterId, egoId) < source.getArcWeight(alterId, egoId)) {
						result.incArcWeight(alterId, egoId);
						arcWeightSum++;
						protocol.appendln(alterId+" "+egoId);
					} else {
						setExhaustedInNeighbors(egoModel, alterModel);
						protocol.appendln("exhausted link "+egoId+" "+alterId);
					}
				} else {
					exhaustedInNodes.add(ego);
					protocol.appendln("exhausted in: "+ego.getId());
				}
			}

//			updateChoiceMap(result, ego);
//			ego = RandomUtils.draw(nodeChoiceMap);
			
			updateWeightMap(result, ego);
			ego = RandomUtils.draw(alterWeightMap,randGen);
			
			egoId = ego.getId();
			egoModel = source.getNode(egoId);
		}
		//
		return result;
	}

	public Graph<E> createRandomGraphByPermutation(final Graph<E> source) {
		Graph<E> result;

		result = new Graph<E>(source.nodeCount());
		result.addNodesWithId(source.getNodes());

		int n = source.nodeCount();

		// intialization (should be put outside the method so as not to be
		// repeated at each run...)
		ArrayList<Integer> rowIndex = new ArrayList<Integer>();
		ArrayList<Integer> columnIndex = new ArrayList<Integer>();
		double[] outCountdown = new double[n];
		double[] inCountdown = new double[n];

		for (int i = 0; i < n; i++) {
			Node<E> node = source.getNode(i+1);
			double outForce = node.getOutForce();
			double inForce = node.getInForce();

			if (outForce > 0) {
				rowIndex.add(i);
				outCountdown[i] = outForce;
			}
			if (inForce > 0) {
				columnIndex.add(i);
				inCountdown[i] = inForce;
			}
		}

		while (rowIndex.size() > 0 && columnIndex.size() > 0) {

			int a = (int) (Math.random() * rowIndex.size());
			int b = (int) (Math.random() * columnIndex.size());
			int i = rowIndex.get(a);
			int j = columnIndex.get(b);

			result.incArcWeight(i+1, j+1);

			outCountdown[i]--;
			inCountdown[j]--;
			if (outCountdown[i] == 0) {
				rowIndex.remove(a);
			}
			if (inCountdown[j] == 0) {
				columnIndex.remove(b);
			}
		}
		//
		return result;

	}

	public Graph<E> createRandomGraphByReshuffling(final int nodeCount, final int arcCount, final Map<int[],Double > map) {
		Graph<E> result;

		result = new Graph<E>(nodeCount);

		for (int k = 0; k < arcCount; k++) {
			int[] i = RandomUtils.draw(map,randGen);
			
//			double d = (int) (Math.random() * arcCount * arcCount);
//			int[] i = map.get(map.ceilingKey(d));
			result.incArcWeight(i[0], i[1]);
		}



		//
		return result;

	}
	
	private void addProbas(List<double[]> probasAgg){
		for (int i=0;i<maxArcWeightSum;i++){
			double[] probaAgg = probasAgg.get(i);
			double[] proba = probas.get(i);
			for (int j=0;j<inertia.length;j++) {
				probaAgg[j] += proba[j];
			}
		}
	}
	

	public List<Graph<E>> createRandomGraphsByAgentSimulation(final int runs) {
		List<Graph<E>> result;

		List<double[]> probasAgg = new ArrayList<double[]>();
		for (int i=0;i<maxArcWeightSum;i++){
			probasAgg.add(new double[inertia.length]);
		}

		result = new ArrayList<Graph<E>>();
		for (int run = 0; run < runs; run++) {
			result.add(createRandomGraphByAgentSimulation());
			addProbas(probasAgg);
		}

		// Normalize probabilities
		probas = probasAgg;
		for (int i=0;i<maxArcWeightSum;i++){
			double[] proba = probas.get(i);
			for (int j=0;j<inertia.length;j++) {
				proba[j] = proba[j]/runs;
			}
		}

		//
		return result;
	}

	public List<Graph<E>> createRandomGraphsByObserverSimulation(final Graph<E> source, final int runs) {
		List<Graph<E>> result;

		List<double[]> probasAgg = new ArrayList<double[]>();
		for (int i=0;i<maxArcWeightSum;i++){
			probasAgg.add(new double[inertia.length]);
		}

		result = new ArrayList<Graph<E>>();
		for (int run = 0; run < runs; run++) {
			result.add(createRandomGraphByObserverSimulation(source));
			addProbas(probasAgg);
		}
		
		// Normalize probabilities
		probas = probasAgg;
		for (int i=0;i<maxArcWeightSum;i++){
			double[] proba = probas.get(i);
			for (int j=0;j<inertia.length;j++) {
				proba[j] = proba[j]/runs;
			}
		}

		//
		return result;
	}

	public List<Graph<E>> createRandomGraphsByPermutation(final Graph<E> source, final int runs) {
		List<Graph<E>> result;

		result = new ArrayList<Graph<E>>();

		for (int run = 0; run < runs; run++) {
			result.add(createRandomGraphByPermutation(source));
		}

		//
		return result;
	}

	public List<Graph<E>> createRandomGraphsByRandomDistribution(final DistributionType dist, final int nodeCount, final int arcCount, final int factor,
			final int runs) {
		List<Graph<E>> result;

		result = new ArrayList<Graph<E>>();

		if (dist == DistributionType.FREE) {

			for (int run = 0; run < runs; run++) {
				result.add(createRandomGraph(nodeCount, arcCount));
			}

		} else {

			Map<Integer,Double> outForces = getRandomDistribution(dist, nodeCount, arcCount, factor);
			Map<Integer,Double> inForces = getRandomDistribution(dist, nodeCount, arcCount, factor);

			Map<int[], Double> map = getNodePairChoiceMap(outForces, inForces);
//			TreeMap<Double, int[]> map = getNodePairChoiceMap(outForces, inForces);

			for (int run = 0; run < runs; run++) {
				result.add(createRandomGraphByReshuffling(nodeCount, arcCount, map));
			}

		}

		//
		return result;
	}

	public List<Graph<E>> createRandomGraphsByReshuffling(final Graph<E> source, final int runs) {
		List<Graph<E>> result;

		result = new ArrayList<Graph<E>>();

		Map<int[], Double> map = getNodePairChoiceMap(source.getOutForces(), source.getInForces());

		for (int run = 0; run < runs; run++) {
			result.add(createRandomGraphByReshuffling(source.nodeCount(), (int)source.getArcWeightSum(), map));
		}
		
/*		double[] inForces = new double[source.nodeCount()];
		
		for (int i=0;i<source.nodeCount();i++){
			for (Graph<E> graph : result) {
				inForces[i] += graph.getInForce(i);
			}
		}
		for (int i=0;i<source.nodeCount();i++){
			inForces[i] = inForces[i]/runs;
		}
		for (int i=0;i<source.nodeCount();i++){
			System.out.println(source.getInForce(i)+" "+inForces[i]);
		}*/
		
		
/*		for (Node node : source.getNodes()){
			int i = node.getId();
			System.out.println(source.getOutForce(i)+" "+result.get(0).getOutForce(i));
		}*/
		
		//
		return result;
	}

	/**
	 * draws a randomNode
	 * 
	 * @return the random node
	 */
/*	private Node<E> drawNeighbor0(final Node<E> start, final Graph<E> graph) {
		Node<E> result;

		result = null;
		int limit = inertia.length;

		Map<Integer, Nodes<E>> neighbors = start.getNeighbors(graph.getNodes(), limit);

		for (int distance = 0; distance <= limit; distance++) {
			if (distance == limit || hasPreference(distance)) {
				Nodes<E> possiblePartners = neighbors.get(distance);
				if (possiblePartners.size() != 0) {
					result = RandomUtils.draw(possiblePartners.toList());
					// System.out.println(start.getId()+" "+result.getId()+" "+distance);
				} else {
					possiblePartners = getNeighborsBeyond(neighbors, distance);
					result = RandomUtils.draw(possiblePartners.toList());
					// formerly: continue;
					// altenatively: break (no Tolerance)
				}
				break;
			}
		}

		//
		return result;
	}*/

	/**
	 * draws a randomNode
	 * 
	 * @return the random node
	 */
/*	private Node<E> drawNeighbor1(final Node<E> start, final Graph<E> graph) {
		Node<E> result;

		result = null;
		int limit = inertia.length;

		for (int distance = 0; distance <= limit; distance++) {
			if (distance == limit || hasPreference(distance)) {
				Nodes<E> possiblePartners = start.getAllNeighbors(graph.getNodes(), distance);
				if (possiblePartners.size() != 0) {
					result = RandomUtils.draw(possiblePartners.toList());
					// System.out.println(start.getId()+" "+result.getId()+" "+distance);
				} else {
					possiblePartners = graph.getNodes();
					result = RandomUtils.draw(possiblePartners.toList());
					// formerly: continue;
					// altenatively: break (no Tolerance)
				}
				break;
			}
		}

		//
		return result;
	}*/

	/**
	 * draws a randomNode
	 * 
	 * @return the random node
	 */
/*	private Node<E> drawNeighbor2(final Node<E> ego, final Graph<E> graph) {
		Node<E> result = null;

		int trials = 5;

		while (result == null && trials > 0) {
			Node<E> alter = RandomUtils.draw(graph.getNodes().toList());
			// System.out.println(getPreference(ego,alter));
			if (RandomUtils.event(getPreference(ego, alter))) {
				result = alter;
			} else {
				trials--;
			}
		}
		//
		return result;
	}*/

	public int getArcCount() {
		return arcWeightSum;
	}

/*	private double getPreference(final Node<E> ego, final Node<E> alter) {
		double result;

		result = 1.;
		int limit = inertia.length;
		boolean[] status = ego.getNeighborStatus(alter, limit);

		for (int i = 0; i < limit; i++) {
			if (status[i]) {
				result = +inertia[i];
			} else {
				result = +0.5;
			}
		}
		result = result / new Double(limit);
		//
		return result;
	}*/

	/**
	 * a shortcut for local propensities
	 * 
	 * @param i
	 *            the type of simulation (0 agent behaviour, 1 observer
	 *            behaviour)
	 * @param j
	 *            the degree of the neighborhood
	 * @return the local propensity
	 */
/*	private boolean hasPreference(final int i) {
		return RandomUtils.event(inertia[i]);
	}*/


	private static <E> Nodes<E> getNeighborsBeyond(final Map<Integer, Nodes<E>> neighbors, final int lowerLimit) {
		Nodes<E> result;

		//
		result = new Nodes<E>();

		for (int distance : neighbors.keySet()) {
			if (distance > lowerLimit) {
				result.addAll(neighbors.get(distance));
			}
		}

		//
		return result;
	}

/*	private static <E> TreeMap<Double, int[]> getNodePairChoiceMap(final Map<Integer,Double> outForces, final Map<Integer,Double> inForces) {
		TreeMap<Double, int[]> result;

		result = new TreeMap<Double, int[]>();

		// Matrix matrix = source.getSquareMatrix().getMatrix();

		double t = 0;
		for (int outId : outForces.keySet()) {
			for (int inId : inForces.keySet()) {
				double d = outForces.get(outId) * inForces.get(inId);
				// int d = matrix.getRowSum(i)*matrix.getColSum(j);
				if (d != 0) {
					t = t + d;
					result.put(t, new int[] {outId, inId});
				}
			}
		}

		//
		return result;

	}*/
	
	private static <E> Map<int[], Double> getNodePairChoiceMap(final Map<Integer,Double> outForces, final Map<Integer,Double> inForces) {
		Map<int[], Double> result;

		result = new HashMap<int[], Double>();

		for (int outId : outForces.keySet()) {
			for (int inId : inForces.keySet()) {
				result.put(new int[] {outId, inId},outForces.get(outId) * inForces.get(inId));
			}
		}
		//
		return result;

	}
	

	private static <E> Map<Integer,Double> getRandomDistribution(final DistributionType dist, final int nodeCount, final int arcCount, final int factor) {
		Map<Integer,Double> result;

		result = new TreeMap<Integer,Double>();
		
		if (dist == DistributionType.FREE){
			result = null;
		} else if (dist == DistributionType.BERNOULLI) {
			for (int k = 0; k < arcCount; k++) {
				int i = (int) (nodeCount * Math.random());
				Double value = result.get(i);
				if (value==null){
					value = 0.;
				}
				result.put(i,value+1.);
			}
		} else if (dist == DistributionType.NORMAL) {
			double mu = new Double(arcCount)/new Double(nodeCount);
			
			
			for (int i = 0; i < nodeCount; i++) {
				result.put(i, getNormalGenerator(mu, 1).nextDouble());
			}
		} else {
			for (int i = 0; i < nodeCount; i++) {
				result.put(i, getPowerGenerator(dist, nodeCount, arcCount, factor).nextDouble());
			}
		}

		//
		return result;
	}

	/**
	 * sets the random variate generator
	 * 
	 * @param dist
	 *            the distribution type
	 * @param nodeCount
	 *            the number of vertices
	 * @param factor
	 *            the power law exponent
	 * @since 10-08-08
	 */
	private static RandomVariateGen getPowerGenerator(final DistributionType dist, final int nodeCount, final int arcCount, final int factor) {
		RandomVariateGen rvg = null;

		RandomStream s = new WELL607();
		switch (dist) {
			case PARETO: {
				double b = arcCount;
//				double b = (arcCount / nodeCount) * (factor + 1) / factor;
				rvg = new ParetoGen(s, factor);
			}
			case POWER: {
				rvg = new PowerGen(s, factor);
			}
		}
		//
		return rvg;
	}
	
	private static RandomVariateGen getNormalGenerator(double mu, double sigma) {
		RandomVariateGen rvg = null;

		RandomStream s = new WELL607();

		rvg = new NormalGen(s, mu, sigma);

		//
		return rvg;
	}

/*	public static <E> TreeMap<Double,Node<E>> getDistributionMap (Nodes<E> nodes, DistributionType dist, int arcCount, int factor){
		TreeMap<Double,Node<E>> result;
		
		if (dist==DistributionType.FREE){
			result = null;
		} else {
			result = new TreeMap<Double,Node<E>>();

			Map<Integer,Double> weightDistribution = getRandomDistribution(dist,nodes.size(),arcCount,factor);
			
			double maxValue = 0.;
			for (Integer i : weightDistribution.keySet()){
				if (weightDistribution.get(i)>0){
					maxValue += weightDistribution.get(i);
					result.put(maxValue,nodes.get(i+1));
				}
			}
		}
		//
		return result;
	}

	public static <E> Node<E> draw (Nodes<E> nodes, TreeMap<Double,Node<E>> distributionMap){
		Node<E> result;
		
		
		if (distributionMap==null){
			result = RandomUtils.draw(nodes.toList());
		} else {
			result = RandomUtils.draw(distributionMap);
		}
		
		//
		return result;
		
	}*/

/*	public Node<E> draw (Nodes<E> nodes, Map<Integer, Double> weightMap){
		Node<E> result;
		
		
		if (weightMap==null){
			result = RandomUtils.draw(nodes.toList());
		} else {
			Integer i = RandomUtils.draw(weightMap,randGen); 
			result = nodes.get(i+1);
		}
		
		//
		return result;
		
	}*/
	


}
