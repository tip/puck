package org.tip.puck.report;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author TIP
 */
public class HeaderLine {

	private String legend;
	private List<String> headers;

	/**
	 * 
	 */
	public HeaderLine() {
		this.legend = null;
		this.headers = new ArrayList<String>();
	}

	public String getHeader(final int columnIndex) {
		String result;

		if (columnIndex < this.headers.size()) {
			if (StringUtils.isBlank(this.headers.get(columnIndex))) {
				result = "null";
			} else {
				result = this.headers.get(columnIndex);
			}
		} else {
			result = String.valueOf(columnIndex + 1);
		}

		//
		return result;
	}

	public List<String> getHeaders() {
		return headers;
	}

	public String getLegend() {
		return legend;
	}

	/**
	 * 
	 * @param label
	 * @param columnIndex
	 */
	public void setHeader(final String label, final int columnIndex) {
		//
		while (columnIndex >= headers.size()) {
			this.headers.add(null);
		}

		//
		this.headers.set(columnIndex, label);
	}

	public void setLegend(final String label) {
		this.legend = label;
	}

}
