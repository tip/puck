/**
 * 
 */
package org.tip.puck.generators;

import java.util.HashMap;
import java.util.Map;

import org.tip.puck.net.Family;
import org.tip.puck.net.Individual;

/**
 * @author Telmo Menezes
 *
 */
public class PGraphNode {
	
	private Individual wife;
	private Individual husband;
	private PGraphNode wifeLink;
	private PGraphNode husbandLink;
	
	private Family family;
	
	private Map<PGraphNode, Integer> descendants;
	private Map<PGraphNode, Integer> originalDescendants;
	private int outDegree;
	private int outDegreeF;
	private int outDegreeM;
	
	public PGraphNode(Individual wife, Individual husband,
			PGraphNode wifeLink, PGraphNode husbandLink) {
		this.wife = wife;
		this.husband = husband;
		this.wifeLink = wifeLink;
		this.husbandLink = husbandLink;
		
		descendants = new HashMap<PGraphNode, Integer>();
		outDegree = 0;
	}
	
	public PGraphNode(Individual wife, Individual husband) {
		this(wife, husband, null, null);
	}
	
	public Family getFamily() {
		return family;
	}

	public PGraphNode(Family family){
		this(family.getWife(),family.getHusband());
		this.family = family;
	}
	
	public PGraphNode(Individual individual){
		this(null,null);
		if (individual.isFemale()) {
			wife = individual;
		} else {
			husband = individual;
		}
	}
	
	public PGraphNode(PGraphNode m) {
		this(m.wife, m.husband, m.wifeLink, m.husbandLink);
	}
	
	public void addDescendant(PGraphNode origin, int distance) {
		// Recursive step
		if (wifeLink != null) {
			wifeLink.addDescendant(origin, distance + 1);
		}
		if (husbandLink != null) {
			husbandLink.addDescendant(origin, distance + 1);
		}
		
		// A marriage is not its own descendant
		if (this == origin) {
			return;
		}
		
		// Register descendants and update distances
		if (descendants.containsKey(origin)) {
			Integer curDistance = descendants.get(origin);
			if (curDistance > distance) {
				descendants.put(origin, distance);
			}
		}
		else {
			descendants.put(origin, distance);
		}
	}
	
	public void clearDescendants() {
		descendants.clear();
	}
	
	public void storeOriginalDescendants() {
		originalDescendants = new HashMap<PGraphNode, Integer>(descendants);
	}
	
	public int generationalDistance(PGraphNode m) {
		if (this == m) {
			return 0;
		}
		else if (descendants.containsKey(m)) {
			return descendants.get(m);
		}
		else {
			return -1;
		}
	}
	
	public int originalGenerationalDistance(PGraphNode m) {
		if (this == m) {
			return 0;
		}
		else if (originalDescendants.containsKey(m)) {
			return originalDescendants.get(m);
		}
		else {
			return -1;
		}
	}
	
	public boolean hasDescendant(PGraphNode m) {
		return descendants.containsKey(m);
	}
	
	public double distance(PGraphNode m) {
		double dist = 0;
		if (wifeLink != m.wifeLink) {
			dist += 0.5;
		}
		if (husbandLink != m.husbandLink) {
			dist += 0.5;
		}
		
		return dist;
	}
	
	public void incOutDegree() {
		outDegree += 1;
	}
	
	public int numberOfDescendants() {
		return descendants.size();
	}
	
	public String hashKey() {
		String wifeStr = "?";
		String husbandStr = "?";
		if (wife != null) {
			wifeStr = "" + wife.getId();
		}
		if (husband != null) {
			husbandStr = "" + husband.getId();
		}
		return wifeStr + " [+] " + husbandStr;
	}
	
	@Override
	public String toString() {
		String wifeStr = "?";
		String husbandStr = "?";
		if (wife != null) {
			wifeStr = "" + wife.getId();
		}
		if (husband != null) {
			husbandStr = "" + husband.getId();
		}
		return wifeStr + " [+] " + husbandStr;
	}
	
	public Individual getWife() {
		return wife;
	}
	
	public void setWife(Individual wife) {
		this.wife = wife;
	}
	
	public Individual getHusband() {
		return husband;
	}
	
	public void setHusband(Individual husband) {
		this.husband = husband;
	}

	public PGraphNode getWifeLink() {
		return wifeLink;
	}

	public void setWifeLink(PGraphNode wifeLink) {
		this.wifeLink = wifeLink;
	}

	public PGraphNode getHusbandLink() {
		return husbandLink;
	}

	public void setHusbandLink(PGraphNode husbandLink) {
		this.husbandLink = husbandLink;
	}

	public Map<PGraphNode, Integer> getDescendants() {
		return descendants;
	}

	public int getOutDegree() {
		return outDegree;
	}

	public int getOutDegreeF() {
		return outDegreeF;
	}

	public void incOutDegreeF() {
		outDegreeF++;
	}

	public int getOutDegreeM() {
		return outDegreeM;
	}

	public void incOutDegreeM() {
		outDegreeM++;
	}
}
