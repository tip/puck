package org.tip.puckgui.views.mas;

import org.tip.puck.net.Gender;

/**
 * 
 * @author TIP
 */
public class NormalAgeWeightFactor implements WeightFactor {

	private final Type type = Type.NORMAL_AGE;
	private Gender gender;
	private double mean;
	private double stdev;

	/**
	 * 
	 */
	public NormalAgeWeightFactor() {
		this.gender = Gender.UNKNOWN;
		this.mean = 25;
		this.stdev = 5;
	}

	public Gender getGender() {
		return gender;
	}

	public double getMean() {
		return mean;
	}

	public double getStdev() {
		return stdev;
	}

	@Override
	public Type getType() {
		return type;
	}

	public void setGender(final Gender gender) {
		this.gender = gender;
	}

	public void setMean(final double mean) {
		this.mean = mean;
	}

	public void setStdev(final double stdev) {
		this.stdev = stdev;
	}
}
