package org.tip.puckgui;

import java.util.Locale;

/**
 * 
 * @author TIP
 * 
 */
public enum Language {
	DEUTSCH(Locale.GERMAN),
	ENGLISH(Locale.ENGLISH),
	FRENCH(Locale.FRENCH),
	ITALIAN(Locale.ITALIAN),
	JAPANESE(Locale.JAPANESE);

	private Locale locale;

	/**
	 * 
	 * @param locale
	 */
	private Language(final Locale locale) {
		this.locale = locale;
	}

	/**
	 * 
	 * @return
	 */
	public Locale locale() {
		return this.locale;
	}
}
