package org.tip.puck.kinoath;

import java.util.Comparator;

import org.tip.puck.util.MathUtils;

/**
 * 
 * @author TIP
 */
public class IndividualGroupComparator implements Comparator<IndividualGroup> {

	public enum Sorting {
		ID,
		SIZE,
		REVERSED_SIZE
	}

	private Sorting sorting;

	/**
	 * 
	 * @param sorting
	 */
	public IndividualGroupComparator(final Sorting sorting) {
		//
		this.sorting = sorting;
	}

	/**
	 * 
	 */
	@Override
	public int compare(final IndividualGroup alpha, final IndividualGroup bravo) {
		int result;

		result = compare(alpha, bravo, this.sorting);

		//
		return result;
	}

	/**
	 * 
	 */
	public static int compare(final IndividualGroup alpha, final IndividualGroup bravo, final Sorting sorting) {
		int result;

		//
		if (sorting == null) {
			//
			result = 0;

		} else {
			//
			switch (sorting) {
				case ID:
					result = MathUtils.compare(getId(alpha), getId(bravo));
				break;

				case REVERSED_SIZE:
					result = -1 * MathUtils.compare(getSize(alpha), getSize(bravo));
				break;

				case SIZE:
				default:
					result = MathUtils.compare(getSize(alpha), getSize(bravo));
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Long getId(final IndividualGroup source) {
		Long result;

		//
		if (source == null) {
			//
			result = null;

		} else {
			//
			result = source.getLongId();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Integer getSize(final IndividualGroup source) {
		Integer result;

		//
		if (source == null) {
			//
			result = null;

		} else {
			//
			result = source.size();
		}

		//
		return result;
	}
}
