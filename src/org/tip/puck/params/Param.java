package org.tip.puck.params;

/**
 * @author Telmo Menezes
 *
 */
public class Param {
    private double minValue;
    private double maxValue;
    private double value;
    private ParamType paramType;
    
    public Param(ParamType paramType, double value, double minValue, double maxValue) {
        this.paramType = paramType;
        this.value = value;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }
    
    public Param(double value) {
        this.paramType = ParamType.FIXED;
        this.value = value;
        this.minValue = Double.NEGATIVE_INFINITY;
        this.maxValue = Double.POSITIVE_INFINITY;
    }
    
    public Param(double minValue, double maxValue) {
        this.paramType = ParamType.VARIABLE;
        this.value = 0;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getMinValue() {
        return minValue;
    }

    public double getMaxValue() {
        return maxValue;
    }

    public ParamType getParamType() {
        return paramType;
    }
}
