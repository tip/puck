package org.tip.puck.mas;

import java.io.BufferedReader;
import java.io.FileReader;

import org.tip.puck.net.Gender;
import org.tip.puck.net.KinType;
import org.tip.puck.net.workers.MemoryCriteria;


public class MASConfig {
	private MAS mas;
	private MemoryCriteria memoryCriteria;
	
	public void fromFile(String filePath) {
		try {
			StringBuffer fileData = new StringBuffer();
			BufferedReader reader = new BufferedReader(new FileReader(filePath));
			char[] buf = new char[1024];
			int numRead=0;
			while((numRead=reader.read(buf)) != -1){
				String readData = String.valueOf(buf, 0, numRead);
				fileData.append(readData);
			}
			reader.close();
			String str = fileData.toString();
			fromString(str);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
    }
	
	public void fromString(String config) {
		parse(config);
	}
	
	public MASConfig() {
		mas = new MAS();
		memoryCriteria = new MemoryCriteria();
	}
	
	private void parse(String config) {
		String[] lines = config.split("\n");
		
		for (String line : lines) {
			parseLine(line.trim());
		}
	}
	
	private void parseLine(String line) {
		if (line.equals("")) {
			return;
		}
		
		// comment lines start with a #
		if (line.charAt(0) == '#') {
			return;
		}
		
		String[] parts = line.split(" ");
		String command = parts[0].toLowerCase();
		String arg = parts[1];
		for (int i = 2; i < parts.length; i++) {
			arg += " " + parts[i];
		}
		
		if (command.equals("factor")) {
			addFactor(arg);
		}
		else if (command.equals("years")) {
			mas.setYears(Integer.parseInt(arg));
		}
		else if (command.equals("initialpopulation")) {
			mas.setInitialPopulation(Integer.parseInt(arg));
		}
		else if (command.equals("fertilityrate")) {
			mas.setFertilityRate(Double.parseDouble(arg));
		}
		else if (command.equals("maxage")) {
			mas.setMaxAge(Integer.parseInt(arg));
		}
		else if (command.equals("samples")) {
			mas.setSamples(Integer.parseInt(arg));
		}
		else if (command.equals("memoryprob")) {
			addMemoryProb(arg);
		}
		else if (command.equals("nrinformants")) {
			memoryCriteria.setNrInformants(Integer.parseInt(arg));
		}
		else if (command.equals("distancefactor")) {
			memoryCriteria.setDistanceFactor(Double.parseDouble(arg));
		}
		else if (command.equals("maleacceptance")) {
			memoryCriteria.setMaleAcceptance(Double.parseDouble(arg));
		}
		else if (command.equals("femaleacceptance")) {
			memoryCriteria.setFemaleAcceptance(Double.parseDouble(arg));
		}
	}
	
	private Gender str2gender(String str) {
		Gender gender = Gender.UNKNOWN;
		if (str.equals("male")) {
			gender = Gender.MALE;
		}
		else if (str.equals("female")) {
			gender = Gender.FEMALE;
		}
		
		return gender;
	}
	
	private void addFactor(String arg) {
		String[] parts = arg.split("\\(");
		String factorName = parts[0].toLowerCase();
		String paramStr = parts[1].replace(')', ' ').trim();
		String[] params = paramStr.split(",");
		for (int i = 0; i < params.length; i++) {
			params[i] = params[i].trim().toLowerCase();
		}
		
		WeightFactor factor = null;
		
		if (factorName.equals("normalage")) {
			Gender gender = str2gender(params[0]);
			double mean = Double.parseDouble(params[1]);
			double stdev = Double.parseDouble(params[2]);
			factor = new WFNormalAge(gender, mean, stdev);
		}
		else if (factorName.equals("normaldifferenceage")) {
			double mean = Double.parseDouble(params[0]);
			double stdev = Double.parseDouble(params[1]);
			factor = new WFNormalAgeDifference(mean, stdev);
		}
		else if (factorName.equals("divorce")) {
			double prob = Double.parseDouble(params[0]);
			factor = new WFDivorce(prob);
		}
		else if (factorName.equals("divorce2")) {
			double weightDivorceMother = Double.parseDouble(params[0]);
			double weightDivorceFather = Double.parseDouble(params[1]);
			double weightDivorceBoth = Double.parseDouble(params[2]);
			factor = new WFDivorce2(weightDivorceMother, weightDivorceFather, weightDivorceBoth);
		}
		else if (factorName.equals("marriage")) {
			double weightCurrentMarriage = Double.parseDouble(params[0]);
			double weightPreviousMarriage = Double.parseDouble(params[1]);
			factor = new WFMarriage(weightCurrentMarriage, weightPreviousMarriage);
		}
		else if (factorName.equals("cousins")) {
			double cousins1 = Double.parseDouble(params[0]);
			double cousins2 = Double.parseDouble(params[1]);
			factor = new WFCousins(cousins1, cousins2);
		}
		else if (factorName.equals("agnaticcousins")) {
			double cousins1 = Double.parseDouble(params[0]);
			double cousins2 = Double.parseDouble(params[1]);
			factor = new WFAgnaticCousins(cousins1, cousins2);
		}
		else if (factorName.equals("uterinecousins")) {
			double cousins1 = Double.parseDouble(params[0]);
			double cousins2 = Double.parseDouble(params[1]);
			factor = new WFUterineCousins(cousins1, cousins2);
		}
		else if (factorName.equals("pregnancy")) {
			double weight = Double.parseDouble(params[0]);
			factor = new WFPregnancy(weight);
		}
		else if (factorName.equals("children")) {
			double c0 = Double.parseDouble(params[0]);
			double c1 = Double.parseDouble(params[1]);
			double c2 = Double.parseDouble(params[2]);
			double c3 = Double.parseDouble(params[3]);
			double c4 = Double.parseDouble(params[4]);
			factor = new WFChildren(c0, c1, c2, c3, c4);
		}
		else if (factorName.equals("childrenfather")) {
			double c0 = Double.parseDouble(params[0]);
			double c1 = Double.parseDouble(params[1]);
			double c2 = Double.parseDouble(params[2]);
			double c3 = Double.parseDouble(params[3]);
			double c4 = Double.parseDouble(params[4]);
			factor = new WFChildrenFather(c0, c1, c2, c3, c4);
		}
		else if (factorName.equals("childrenmother")) {
			double c0 = Double.parseDouble(params[0]);
			double c1 = Double.parseDouble(params[1]);
			double c2 = Double.parseDouble(params[2]);
			double c3 = Double.parseDouble(params[3]);
			double c4 = Double.parseDouble(params[4]);
			factor = new WFChildrenMother(c0, c1, c2, c3, c4);
		}
		else if (factorName.equals("childrencouple")) {
			double c0 = Double.parseDouble(params[0]);
			double c1 = Double.parseDouble(params[1]);
			double c2 = Double.parseDouble(params[2]);
			double c3 = Double.parseDouble(params[3]);
			double c4 = Double.parseDouble(params[4]);
			factor = new WFChildrenCouple(c0, c1, c2, c3, c4);
		}
		
		if (factor != null) {
			mas.addFactor(factor);
		}
	}
	
	private Gender strToGender(String str) {
		if (str.equals("male")) {
			return Gender.MALE;
		}
		else if (str.equals("female")) {
			return Gender.FEMALE;
		}
		
		return Gender.UNKNOWN;
	}
	
	private KinType strToKinType(String str) {
		if (str.equals("parent")) {
			return KinType.PARENT;
		}
		else if (str.equals("child")) {
			return KinType.CHILD;
		}
		
		return KinType.SPOUSE;
	}
	
	private void addMemoryProb(String arg) {
		String[] parts = arg.split(" ");
		String strEgo = parts[0].toLowerCase();
		String strAlter = parts[1].toLowerCase();
		String strKinType = parts[2].toLowerCase();
		String strInformant = parts[3].toLowerCase();
		String strProb = parts[4];
		
		Gender ego = strToGender(strEgo);
		Gender alter = strToGender(strAlter);
		KinType kinType = strToKinType(strKinType);
		Gender informant = strToGender(strInformant);
		double prob = Double.parseDouble(strProb);
		
		memoryCriteria.setMemoryProb(ego, alter, kinType, informant, prob);
	}

	public MAS getMas() {
		return mas;
	}

	public MemoryCriteria getMemoryCriteria() {
		return memoryCriteria;
	}
}
