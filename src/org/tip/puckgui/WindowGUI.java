package org.tip.puckgui;

import java.util.Locale;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.tip.puck.report.Report;

/**
 * 
 * WindowGUI defines default window functionalities.
 * 
 * @author TIP
 */
public interface WindowGUI {
	/**
	 * 
	 * @param report
	 */
	public void addRawTab(final String tabTitle, final JPanel panel);

	/**
	 * 
	 * @param report
	 */
	public void addReportTab(Report report);

	/**
	 * 
	 * @param tabTitle
	 * @param panel
	 */
	public void addReportTab(final String tabTitle, final JPanel panel);

	/**
	 * 
	 */
	public void closeCurrentTab();

	/**
	 * 
	 * @return
	 */
	public int getId();

	/**
	 * 
	 * @return
	 */
	public JFrame getJFrame();

	/**
	 * 
	 * @return
	 */
	public String getTitle();

	/**
	 * 
	 * @return
	 */
	public boolean isChanged();

	/**
	 * 
	 * @param changed
	 */
	public void setChanged(final boolean changed);

	/**
	 * Place the window on top.
	 */
	public void toFront();

	/**
	 * 
	 * @param locale
	 */
	public void updateLocale(final Locale locale);
}
