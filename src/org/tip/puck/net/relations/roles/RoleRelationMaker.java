package org.tip.puck.net.relations.roles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.net.AlterAge;
import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.KinType;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.relations.Roles;
import org.tip.puck.net.relations.roles.RoleActorPair.Adjustable;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.report.Report;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.statistics.StatisticsWorker;

import fr.devinsy.util.StringList;

public class RoleRelationMaker {
	
	public static enum RoleRelationRule {
		
		CHILD_CHILD_SIBLING("CHILD","CHILD","SIBLING"),
		SIBLING_SIBLING_SIBLING("SIBLING","SIBLING","SIBLING"),
		SPOUSE_CHILD_PARENT("SPOUSE","CHILD","PARENT"),
		CHILD_SPOUSE_CHILD("CHILD","SPOUSE","CHILD"),
		SIBLING_PARENT_CHILD("SIBLING","PARENT","CHILD");
		
		String alterRoleName;
		String mediusRoleName;
		String mediusAlterRoleName;
		
		private RoleRelationRule(String alterRoleName,String mediusRoleName, String mediusAlterRoleName) {
			
			this.alterRoleName = alterRoleName;
			this.mediusRoleName = mediusRoleName;
			this.mediusAlterRoleName = mediusAlterRoleName;
		}

		public String getAlterRoleName() {
			return alterRoleName;
		}

		public String getMediusRoleName() {
			return mediusRoleName;
		}

		public String getMediusAlterRoleName() {
			return mediusAlterRoleName;
		}

	}
	
	public static String getName(String fileName){
		String result;
		
		int idx =  fileName.indexOf(".term");
		if (idx<0){
			result = fileName;
		} else {
			result = fileName.substring(0, idx);
		}
		return result;
	}
	
	public static List<RoleRelationRule> getDefaultRules(){
		List<RoleRelationRule> result;
		
		result = new ArrayList<RoleRelationRule>();
		
		for (RoleRelationRule rule : RoleRelationRule.values()){
			if (rule!=RoleRelationRule.SIBLING_PARENT_CHILD){
				result.add(rule);
			}
		}
		
		//
		return result;
	}
	
	private static final Logger logger = LoggerFactory.getLogger(RoleRelationMaker.class);

	private static void compose(final RoleDefinitions definitions, final Individual self, final Queue<Individual> queue,
			final List<String> egoNeutral, Individuals visited, String egoName) {
		
		Gender egoGender = Gender.valueOf(self.getAttributeValue("EGOGENDER"));
			
		for (KinType kinType : KinType.basicTypesWithSiblings()){
			for (Individual alter : self.getKin(kinType)){
				
				AlterAge alterAge = alter.getRelativeAge(self);
				alter.setAttribute("EGOGENDER", egoGender.toString());

				definitions.add(new RoleDefinition(self.getName(),alter.getName(),kinType.toString(),egoGender,alter.getGender(),alterAge,egoName));

				if (!visited.contains(alter)){
					queue.add(alter);
				}
			}
		}
		visited.add(self);

	}

	public static RelationModel create(final Net net, final Report report) throws PuckException {
		RelationModel result;

		result = new RelationModel(net.getLabel());
		createRoleRelationsFromGenealogy(result, net, report);
		
		//
		return result;
	}
	
	public static RelationModel createEtic(final String name, final String selfName, final StringList roleDefinitionsList, final List<RoleRelationRule> rules, Report report) throws PuckException {
		RelationModel result;

		result = new RelationModel(getName(name),rules);
		createRoleRelationsEticFormat(result, selfName, roleDefinitionsList, report);

		//
		return result;
	}
	
	public static RelationModel createFromMap (final String name, final List<RoleActorPair> actorPairs, Report report) throws PuckException{
		RelationModel result;
		
		result = new RelationModel(getName(name));
		createRoleRelationsMapFormat(result, actorPairs, report);

		//
		return result;
	}

	public static RelationModel createStandard(final String name, final String selfName, final StringList roleDefinitionsList, final List<RoleRelationRule> rules, Report report) throws PuckException {
		RelationModel result;

		result = new RelationModel(getName(name),rules);
		createRoleRelationsStandardFormat(result, selfName, roleDefinitionsList, report);
		
		//
		return result;
	}
	
	public static RelationModel createEmic(final String name, final String selfName, final StringList roleDefinitionsList, final List<RoleRelationRule> rules, Report report) throws PuckException {
		RelationModel result;

		result = new RelationModel(getName(name),rules);
		
		createRoleRelationsEmicFormat(result, selfName, roleDefinitionsList, report);
		
		//
		return result;
	}
	
	public static RoleRelations createRoleRelationsFromGenealogy(final RelationModel model, final Net net, final Report report) throws PuckException {
		RoleRelations result;
		
		result = new RoleRelations();
		String selfName = net.individuals().getFirst().getName();
		result.setSelfName(selfName);
		model.setRoleRelations(result);
		Queue<Individual> queue = new LinkedList<Individual>();

		Individual maleEgo = null;
		Individual femaleEgo = null;

		// Determine egos
		for (Individual indi : net.individuals()) {
			if (indi.getName().equalsIgnoreCase(result.getSelfName())) {
				indi.setAttribute("EGOGENDER", indi.getGender().toString());
				indi.setAttribute("GENERATION", "0");
				queue.add(indi);
				
				if (indi.isMale()) {
					maleEgo = indi;
				} else if (indi.isFemale()) {
					femaleEgo = indi;
				}
				if (maleEgo != null && femaleEgo != null) {
					result.setEgoGenderDistinction(true);
					break;
				}
			}
		}

		List<String> egoNeutral = new ArrayList<String>();

		if ((maleEgo != null && maleEgo.isSterile()) || (femaleEgo != null && femaleEgo.isSterile()) || !result.isEgoGenderDistinction()) {
			egoNeutral.add("PARENT");
		}
		if ((maleEgo != null && maleEgo.isOrphan()) || (femaleEgo != null && femaleEgo.isOrphan()) || !result.isEgoGenderDistinction()) {
			egoNeutral.add("CHILD");
		}
		if ((maleEgo != null && maleEgo.isSingle()) || (femaleEgo != null && femaleEgo.isSingle()) || !result.isEgoGenderDistinction()) {
			egoNeutral.add("SPOUSE");
		}
		if ((maleEgo != null && maleEgo.isUnique()) || (femaleEgo != null && femaleEgo.isUnique()) || !result.isEgoGenderDistinction()) {
			egoNeutral.add("SIBLING");
		}

		RoleDefinitions definitions = new RoleDefinitions();
		Individuals visited = new Individuals();

		while (!queue.isEmpty()) {
			Individual self = queue.poll();
			compose(definitions, self, queue, egoNeutral,visited,selfName);
		}
		
		result.updateGenderConfigs();
		result.heteroMarriage = StatisticsWorker.hasSameGenderMarriages(net);
		result = createRoleRelations(definitions, selfName, model.getRules(), report);	
		
		model.setRoleRelations(result);
		
		for (RoleActor actor : result.getSortedSelfs()){
//			System.out.println(actor+"\t"+result.getSelfRelation(actor));
		}
		
		//
		return result;
	}

	
	private static String[] splitLastValid(String item){
		String[] result;
		
		result = new String[2];
		
		for (int idx=item.length()-1;idx>-1;idx--){
			String lastItem = item.substring(idx);
			if (KinType.valueOfLetter(lastItem)!=null){
				if (idx>0 && AlterAge.valueOfLetter(item.charAt(idx-1))!=null){
					idx= idx-1;
				}
				result[0] = item.substring(0, idx);
				result[1] = item.substring(idx);
				break;
			}
		}
		//
		return result;
	}
		
	private static MetaRole getMetaRoleFromString(String item){
		MetaRole result;

		result = new MetaRole(null);
		
		if (item.charAt(0)=='e' || item.charAt(0)=='y'){
			result.setAlterAge(AlterAge.valueOfLetter(item.charAt(0)));
			item = item.substring(1);
		}
		
		if (item!=null){
			
			KinType type = KinType.valueOfLetter(item);
			if (type!=null){
				result.setName(type.ungendered()+"");
				result.setAlterGender(type.getGender());
			}
		}
		
		//
		return result;
	}
	
	public static RoleRelations createRoleRelationsEticFormat(final RelationModel model, final String selfName, final StringList rolePositionList, final Report report) throws PuckException {
		RoleRelations result;
		
		int maxIterations = 5;

		result = new RoleRelations();
		result.selfName = selfName;
		
		RoleDefinitions definitions = new RoleDefinitions();
		Queue<RoleDefinition> queue = new LinkedList<RoleDefinition>();
						
		for (String rolePositionLine : rolePositionList) {
			
			String[] items = rolePositionLine.split("\t");
			
			Role alterTerm = null;
			Role selfTerm = null;
			MetaRole metaRole = null;
			
			for (int idx = 0; idx < items.length; idx++) {
				
				String item = items[idx];

				if (StringUtils.isEmpty(item)) {
					continue;
				}
				
				switch (idx) {
				case 0:
					alterTerm = new Role(item);
				break;
				case 1:
					String[] terms = splitLastValid(item);
					if (!StringUtils.isBlank(terms[0])){
						selfTerm = new Role(terms[0]);
					} else {
						selfTerm = new Role(selfName);
					}
					metaRole= getMetaRoleFromString(terms[1]);
				break;
				case 2:
					metaRole.setEgoGender(Gender.valueOf(item.charAt(0)));
				break;
				}
			}

			RoleActor alter = new RoleActor(alterTerm,metaRole,selfName);
			RoleDefinition definition = new RoleDefinition(selfTerm,alter); 
			definition.setLetters(items[1]);
			
			if (selfTerm.hasName(selfName)){
				definitions.add(definition);
			} else {
				queue.add(definition);
			}
		}
		
		Map<String,Integer> count = new HashMap<String,Integer>();
		for (RoleDefinition definition : queue){
			count.put(definition.getSelfTerm().getName(), 0);
		}
		
		while (!queue.isEmpty()){

			RoleDefinition definition = queue.poll();
			
			for (RoleDefinition selfDefinition : definitions.get(definition.getSelfTerm().getName(), definition.getEgoGender())){
				if (selfDefinition!=null){
					definition.getSelfTerm().setName(selfDefinition.getAlterTerm().getName());
					definitions.add(definition);
				} else if (count.get(definition.getSelfTerm().getName())<=maxIterations){
					count.put(definition.getSelfTerm().getName(), count.get(definition.getSelfTerm().getName())+1);
					queue.add(definition);
				} else {
					logger.warn("No medius term defined for "+definition);
				}
			}
		}
		
		result= createRoleRelations(definitions, selfName, model.getRules(), report);
		
		//
		model.setRoleRelations(result);
		
		logger.debug("Model "+model+" created");
		//
		return result;
	}
	
	public static RoleRelations createRoleRelationsEmicFormat1(final RelationModel model, final String selfName, final StringList roleRelationsList, Report report) throws PuckException {
		RoleRelations result;

		RoleDefinitions definitions = new RoleDefinitions();
		
		Map<String,RoleActor> elementaryTerms = new HashMap<String,RoleActor>();
		
		// Set elementary Terms
		for (String roleRelationLine : roleRelationsList) {
			
			String[] items = roleRelationLine.split("\t");

			if (!StringUtils.isEmpty(items[1])) {
				
				Role alterTerm = null;
				Role selfTerm = new Role(selfName);
				String metaRoleName = KinType.SELF.toString();
				Gender egoGender = Gender.UNKNOWN;
				Gender alterGender = Gender.UNKNOWN;
				AlterAge alterAge = AlterAge.UNKNOWN;
							
				for (int idx = 0; idx < items.length; idx++) {

					String item = items[idx];
					if (StringUtils.isEmpty(item)) {
						continue;
					}

					switch (idx) {
						case 0:
							alterTerm = model.role(item);
						break;
						case 1:
							metaRoleName = item;
						break;
						case 5:
							alterGender = Gender.valueOf(item);
						break;
						case 6:
							alterAge = AlterAge.valueOf(item);
						break;
						case 7:
							egoGender = Gender.valueOf(item);
						break;
					}
				}
				
				RoleActor alter = new RoleActor(alterTerm,new MetaRole(metaRoleName,egoGender,alterGender,alterAge),selfName);
				definitions.add(new RoleDefinition(selfTerm,alter));
				elementaryTerms.put(alterTerm.toString(), alter);
			}
		}
		
		// Set inverse elementary Terms
		for (String roleRelationLine : roleRelationsList) {
			
			String[] items = roleRelationLine.split("\t");

			if (!StringUtils.isEmpty(items[2])) {
				
				Role alterTerm = null;
				Role selfTerm = new Role(selfName);
				String metaRoleName = KinType.SELF.toString();
				Gender egoGender = Gender.UNKNOWN;
				Gender alterGender = Gender.UNKNOWN;
				AlterAge alterAge = AlterAge.UNKNOWN;
							
				for (int idx = 0; idx < items.length; idx++) {

					String item = items[idx];
					if (StringUtils.isEmpty(item)) {
						continue;
					}

					switch (idx) {
						case 0:
							alterTerm = model.role(item);
						break;
						case 2:
							RoleActor inverseAlter = elementaryTerms.get(item);
							if (inverseAlter!=null){

								metaRoleName = MetaRole.invertName(inverseAlter.getRole().getName());
								egoGender = inverseAlter.getEgoGender();

							} else {
								
								throw PuckExceptions.INVALID_PARAMETER.create((Exception)null, "Non-elementary reference term: "+item);

							}
							
						break;
						case 5:
							alterGender = Gender.valueOf(item);
						break;
						case 6:
							alterAge = AlterAge.valueOf(item);
						break;
						case 7:
							egoGender = Gender.valueOf(item);
						break;
					}
				}
				
				RoleActor alter = new RoleActor(alterTerm,new MetaRole(metaRoleName,egoGender,alterGender,alterAge),selfName);
				definitions.add(new RoleDefinition(selfTerm,alter));
				elementaryTerms.put(alterTerm.toString(), alter);
			}
		}
		
//		StringList errorList = new StringList();
		
		// Set other terms
		for (String roleRelationLine : roleRelationsList) {
			
			String[] items = roleRelationLine.split("\t");

			if (StringUtils.isEmpty(items[1]) && StringUtils.isEmpty(items[2])) {
				
				Role alterTerm = null;
				Role selfTerm = new Role(selfName);
				String metaRoleName = KinType.SELF.toString();
				Gender egoGender = Gender.UNKNOWN;
				Gender alterGender = Gender.UNKNOWN;
				AlterAge alterAge = AlterAge.UNKNOWN;
							
				for (int idx = 0; idx < items.length; idx++) {

					String item = items[idx];
					if (StringUtils.isEmpty(item)) {
						continue;
					}

					switch (idx) {
						case 0:
							alterTerm = model.role(item);
						break;
						case 3:
							selfTerm = model.role(item);
						break;
						case 4:
							RoleActor generator = elementaryTerms.get(item);
							if (generator!=null){
								metaRoleName = generator.getRole().getName();
								alterGender = generator.getAlterGender();
								egoGender = generator.getEgoGender();
								alterAge = generator.getAlterAge();
							} else {
								
								throw PuckExceptions.INVALID_PARAMETER.create((Exception)null, "Non-elementary generator term: "+item);
							}
						break;
						case 5:
							alterGender = Gender.valueOf(item);
						break;
						case 6:
							alterAge = AlterAge.valueOf(item);
						break;
						case 7:
							egoGender = Gender.valueOf(item);
						break;
					}
				}
				
				RoleActor alter = new RoleActor(alterTerm,new MetaRole(metaRoleName,egoGender,alterGender,alterAge),selfName);
				definitions.add(new RoleDefinition(selfTerm,alter));
			}

		}

		
/*		if (!errorList.isEmpty()){
			report.outputs().appendln(errorList.size()/2+" importation errors");
			report.outputs().appendln();
			report.outputs().appendln(errorList);
			report.outputs().appendln();
		}*/

		result= createRoleRelations(definitions, selfName, model.getRules(), report);		
		//
		model.setRoleRelations(result);
		
		logger.debug("Model "+model+" created");
		//
		return result;
	}
	
	
	public static RoleRelations createRoleRelationsEmicFormat(final RelationModel model, final String selfName, final StringList roleRelationsList, Report report) throws PuckException {
		RoleRelations result;

		RoleDefinitions definitions = new RoleDefinitions();
		RoleDefinitions composedDefinitions = new RoleDefinitions();
		RoleDefinitions inverseDefinitions = new RoleDefinitions();
		
		Map<String,RoleActor> elementaryTerms = new HashMap<String,RoleActor>();
		
		// Set elementary Terms
		for (String roleRelationLine : roleRelationsList) {
			
			String[] items = roleRelationLine.split("\t");

			if (!StringUtils.isEmpty(items[1])) {
				
				Role alterTerm = null;
				Role selfTerm = new Role(selfName);
				String metaRoleName = KinType.SELF.toString();
				Gender egoGender = Gender.UNKNOWN;
				Gender alterGender = Gender.UNKNOWN;
				AlterAge alterAge = AlterAge.UNKNOWN;
							
				for (int idx = 0; idx < items.length; idx++) {

					String item = items[idx];
					if (StringUtils.isEmpty(item)) {
						continue;
					}

					switch (idx) {
						case 0:
							alterTerm = model.role(item);
						break;
						case 1:
							metaRoleName = item;
						break;
						case 5:
							alterGender = Gender.valueOf(item);
						break;
						case 6:
							alterAge = AlterAge.valueOf(item);
						break;
						case 7:
							egoGender = Gender.valueOf(item);
						break;
					}
				}
				
				RoleActor alter = new RoleActor(alterTerm,new MetaRole(metaRoleName,egoGender,alterGender,alterAge),selfName);
				definitions.add(new RoleDefinition(selfTerm,alter));
				elementaryTerms.put(alterTerm.toString(), alter);
			}
		}
		
		// Set inverse elementary Terms
		for (String roleRelationLine : roleRelationsList) {
			
			String[] items = roleRelationLine.split("\t");

			if (!StringUtils.isEmpty(items[2])) {
				
				Role alterTerm = null;
				Role selfTerm = new Role(selfName);
				String metaRoleName = KinType.SELF.toString();
				Gender egoGender = Gender.UNKNOWN;
				Gender alterGender = Gender.UNKNOWN;
				AlterAge alterAge = AlterAge.UNKNOWN;
				String inverseTerm = null;
							
				for (int idx = 0; idx < items.length; idx++) {

					String item = items[idx];
					if (StringUtils.isEmpty(item)) {
						continue;
					}

					switch (idx) {
						case 0:
							alterTerm = model.role(item);
						break;
						case 2:
							RoleActor inverseAlter = elementaryTerms.get(item);
							if (inverseAlter!=null){

								metaRoleName = MetaRole.invertName(inverseAlter.getRole().getName());
								egoGender = inverseAlter.getEgoGender();

							} else {
								
								metaRoleName = null;
								inverseTerm = item;
//								throw PuckExceptions.INVALID_PARAMETER.create((Exception)null, "Non-elementary reference term: "+item);

							}
							
						break;
						case 5:
							alterGender = Gender.valueOf(item);
						break;
						case 6:
							alterAge = AlterAge.valueOf(item);
						break;
						case 7:
							egoGender = Gender.valueOf(item);
						break;
					}
				}
				
				RoleActor alter = new RoleActor(alterTerm,new MetaRole(metaRoleName,egoGender,alterGender,alterAge),selfName);

				if (metaRoleName!=null){
					
					definitions.add(new RoleDefinition(selfTerm,alter));
					elementaryTerms.put(alterTerm.toString(), alter);
					
				} else {
					
					alter.setAttribute("INVERSE", inverseTerm);
					inverseDefinitions.add(new RoleDefinition(selfTerm,alter));
				}
			}
		}
		
//		StringList errorList = new StringList();
		
		// Set other terms
		for (String roleRelationLine : roleRelationsList) {
			
			String[] items = roleRelationLine.split("\t");

			if (StringUtils.isEmpty(items[1]) && StringUtils.isEmpty(items[2])) {
				
				Role alterTerm = null;
				Role selfTerm = new Role(selfName);
				String metaRoleName = KinType.SELF.toString();
				Gender egoGender = Gender.UNKNOWN;
				Gender alterGender = Gender.UNKNOWN;
				AlterAge alterAge = AlterAge.UNKNOWN;
				String generatorTerm = null;
							
				for (int idx = 0; idx < items.length; idx++) {

					String item = items[idx];
					if (StringUtils.isEmpty(item)) {
						continue;
					}

					switch (idx) {
						case 0:
							alterTerm = model.role(item);
						break;
						case 3:
							selfTerm = model.role(item);
						break;
						case 4:
							RoleActor generator = elementaryTerms.get(item);
							if (generator!=null){
								metaRoleName = generator.getRole().getName();
								alterGender = generator.getAlterGender();
								egoGender = generator.getEgoGender();
								alterAge = generator.getAlterAge();
							} else {
								
								metaRoleName = null;
								generatorTerm = item;
//								throw PuckExceptions.INVALID_PARAMETER.create((Exception)null, "Non-elementary generator term: "+item);
							}
						break;
						case 5:
							alterGender = Gender.valueOf(item);
						break;
						case 6:
							alterAge = AlterAge.valueOf(item);
						break;
						case 7:
							egoGender = Gender.valueOf(item);
						break;
					}
				}
				
				RoleActor alter = new RoleActor(alterTerm,new MetaRole(metaRoleName,egoGender,alterGender,alterAge),selfName);
				
				if (metaRoleName!=null){
					definitions.add(new RoleDefinition(selfTerm,alter));
				} else {
					alter.setAttribute("GENERATOR", generatorTerm);
					composedDefinitions.add(new RoleDefinition(selfTerm,alter));
				}
			}

		}

		StringList resolved = new StringList();
		StringList unresolved = new StringList();
		int resolvedCount = 0;
		int unresolvedCount = 0;
		
		for (RoleDefinition protoDefinition : inverseDefinitions){
			Role inverseTerm = new Role(protoDefinition.getAlter().getAttributeValue("INVERSE"));
			StringList subReport = new StringList();
			RoleDefinition definition = definitions.reinvert(protoDefinition.getAlter(), inverseTerm, elementaryTerms,subReport);
			if (definition!=null){
				definitions.add(definition);
				resolved.appendln("Resolved definition: "+protoDefinition.getAlterTerm()+" as inverse of "+inverseTerm);
				resolved.appendln(subReport);
				resolvedCount++;
			} else {
				resolved.appendln("Unresolved definition: "+protoDefinition.getAlterTerm()+" as inverse of "+inverseTerm);
				unresolved.appendln(subReport);
				unresolvedCount++;
			}
		}
		
		for (RoleDefinition protoDefinition : composedDefinitions){
			Role generatorTerm = new Role(protoDefinition.getAlter().getAttributeValue("GENERATOR"));
			StringList subReport = new StringList();
			RoleDefinition definition = definitions.recompose(protoDefinition.getSelfTerm(), protoDefinition.getAlterTerm(), generatorTerm, elementaryTerms,subReport);
			if (definition!=null){
				definitions.add(definition);
				resolved.appendln("Resolved definition: "+protoDefinition.getAlterTerm()+" as "+generatorTerm +" of  "+protoDefinition.getSelfTerm());
				resolved.appendln(subReport);
				resolvedCount++;
			} else {
				unresolved.appendln("Unresolved definition: "+protoDefinition.getAlterTerm()+" as "+generatorTerm +" of  "+protoDefinition.getSelfTerm());
				unresolved.appendln(subReport);
				unresolvedCount++;
			}
		}
		
		if (resolvedCount>0){
			report.outputs().appendln(resolvedCount+" resolved definitions");
			report.outputs().appendln();
			report.outputs().appendln(resolved);
		}
		if (unresolvedCount>0){
			report.outputs().appendln(unresolvedCount+" unresolved definitions");
			report.outputs().appendln();
			report.outputs().appendln(unresolved);
		}
		
		result= createRoleRelations(definitions, selfName, model.getRules(), report);		
		//
		model.setRoleRelations(result);
		
		logger.debug("Model "+model+" created");
		//
		return result;
	}
	
	public static RoleRelations createRoleRelationsMapFormat(final RelationModel model, final List<RoleActorPair> actorPairs, final Report report) throws PuckException {
		RoleRelations result;

		String selfName = actorPairs.get(0).getSelf().getSelfName();
		
		result = createRoleRelations(actorPairs,  selfName, report);	
		
		//
		model.setRoleRelations(result);
		
		for (RoleActor actor : result.getSortedSelfs()){
//			System.out.println(actor+"\t"+result.getSelfRelation(actor));
		}
		
		logger.debug("Model "+model+" created");
		//
		return result;

		
	}

	
	
	
	public static RoleRelations createRoleRelationsStandardFormat(final RelationModel model, final String selfName, final StringList roleRelationsList, final Report report) throws PuckException {
		RoleRelations result;

		RoleDefinitions definitions = new RoleDefinitions();
		RoleDefinitions firstDefinitions = new RoleDefinitions();
		RoleDefinitions secondDefinitions = new RoleDefinitions();
		
		for (String roleRelationLine : roleRelationsList) {
			
			String[] items = roleRelationLine.split("\t");

			Role alterTerm = null;
			Role selfTerm = new Role(selfName);
			String metaRoleName = KinType.SELF.toString();
			Gender egoGender = Gender.UNKNOWN;
			Gender alterGender = Gender.UNKNOWN;
			AlterAge alterAge = AlterAge.UNKNOWN;
			
			for (int idx = 0; idx < items.length; idx++) {

				String item = items[idx];
				if (StringUtils.isEmpty(item)) {
					continue;
				}

				switch (idx) {
					case 0:
						alterTerm = model.role(item);
					break;
					case 1:
						selfTerm = model.role(item);
					break;
					case 2:
						metaRoleName = item;
					break;
					case 3:
						egoGender = Gender.valueOf(item);
					break;
					case 4:
						alterGender = Gender.valueOf(item);
					break;
					case 5:
						alterAge = AlterAge.valueOf(item);
					break;
				}
			}
			
			RoleActor alter = new RoleActor(alterTerm,new MetaRole(metaRoleName,egoGender,alterGender,alterAge),selfName);

			RoleDefinition definition = new RoleDefinition(selfTerm,alter);
			if (selfTerm.hasName(selfName) || alter.hasName(selfName)){
				firstDefinitions.add(definition);
			} else {
				secondDefinitions.add(definition);
			}
		}
		
		definitions.addAll(firstDefinitions);
		definitions.addAll(secondDefinitions);
		
		result = createRoleRelations(definitions, selfName, model.getRules(), report);	
		
		//
		model.setRoleRelations(result);
		
		for (RoleActor actor : result.getSortedSelfs()){
//			System.out.println(actor+"\t"+result.getSelfRelation(actor));
		}
		
		logger.debug("Model "+model+" created");
		//
		return result;
	}	
	
	
	private static RoleRelations createRoleRelations(final List<RoleActorPair> actorPairs, final String selfName, final Report report) throws PuckException{
		RoleRelations result;
		
		result = new RoleRelations();
		result.selfName = selfName;
		result.heteroMarriage = true;

		for (RoleActorPair pair : actorPairs){
			
			result.updateGenderConfigs(pair.getSelf());
			result.updateGenderConfigs(pair.getAlter());
			if (result.heteroMarriage() && pair.getAlter().isSpouse() && pair.getSelf().getAlterGender()==pair.getAlter().getAlterGender()){
				result.heteroMarriage = false;
			}
		}
		
		StringList subReport = new StringList();
		subReport.appendln("Read role definitions");
		subReport.appendln();

		for (RoleActorPair pair : actorPairs){
			
			// No reciprocation in kinterm map format, since relations are already coded complete, and reciprocation would render the gender configurations ambiguous
			
			putRoleRelation(result,new RoleActorPair(result,pair.getSelf(),pair.getAlter(),Adjustable.SELF),false,subReport);
		}
		
		//
		return result;
	}

	
	private static RoleRelations createRoleRelations(final RoleDefinitions definitions, final String selfName, final List<RoleRelationRule> rules, final Report report) throws PuckException{
		RoleRelations result;
		
		result = new RoleRelations();
		result.selfName = selfName;
		
		result.setOriginalDefinitions(definitions);
		result.updateGenderConfigs(definitions);
		
		StringList subReport = new StringList();
		subReport.appendln("Read role definitions");
		subReport.appendln();
		
		for (RoleDefinition definition : definitions.toSortedList()){
			
			if (!definition.getSelfTerm().hasName(selfName) && result.getGenderConfigs().get(definition.getSelfTerm())==null){
				
				throw PuckExceptions.INVALID_PARAMETER.create((Exception)null, "Undefined reference term: "+definition.getSelfTerm()+"\t in "+definition);

			} else if (!definition.getAlter().hasName(selfName) && result.getGenderConfigs().get(definition.getAlterTerm())==null){
				
				throw PuckExceptions.INVALID_PARAMETER.create((Exception)null, "Undefined alter term: "+definition.getAlterTerm()+"\t in "+definition);
				
			}
			
			// Adjustability of the medius terms (without specified gender)
			putRoleRelation(result,definition.getPair(result, Adjustable.SELF), true, subReport);
			
		}
		
		logger.debug("Definitions read");
		//
		
		if (rules!=null){
			
			Collections.sort(rules);
			for (RoleRelationRule rule : rules){
				
				composeRelations(result,rule.getAlterRoleName(),rule.getMediusRoleName(),rule.getMediusAlterRoleName(),subReport);
			}
			
					
	/*		composeRelations(result,"CHILD","CHILD","SIBLING",subReport);
			composeRelations(result,"SIBLING","SIBLING","SIBLING",subReport);
			composeRelations(result,"SPOUSE","CHILD","PARENT",subReport);
			composeRelations(result,"CHILD","SPOUSE","CHILD",subReport);*/
//			composeRelations(result,"SIBLING","PARENT","CHILD",genderConfigs,report);  // Not valid - cf. jen and tse are spouses not siblings depite shared parents
		}
		
		// Configuration report
		report.outputs().appendln("Configurating definitions");
		report.outputs().appendln();
		for (String basicDefinition: result.getBasicRoleDefinitions().toSortedStringList()){
			report.outputs().appendln(basicDefinition);
		}
		report.outputs().appendln();
		report.outputs().append(subReport);
		
		//
		return result;
	}


/*	private static boolean removeRoleDefinition(RoleRelations relations, RoleActor self, RoleActor alter, boolean withReciprocal, RoleDefinitions immortals, StringList report){
		boolean result;
		
		result = false;
		
		if (!immortals.contains(new RoleDefinition(self.getIndividual(),alter))){
			
			RoleRelation relation = relations.getSelfRelation(self);
			
			if (relation.getActors().remove(alter)){
				report.appendln("Removed"+reciprocal(withReciprocal)+"role:\t"+alter.getIndividual()+"\t"+self.getIndividual()+"\t"+alter.getRole());
			}

			// True also if the item has already been removed before, needed for enclosing method (do not identify with the return of remove!)
			result = true;
				
			if (withReciprocal && !self.equals(alter)){
				
				removeRoleDefinition(relations,alter,self.asReciprocalOf(alter),false,immortals,report);
			}
		}
		//
		return result;
	}*/
	
	private static boolean removeRoleDefinition (RoleRelations relations, RoleActorPair pair, boolean withReciprocal, RoleDefinitions immortals, StringList report){
		boolean result;
		
		result = false;

		if (pair.isValid()) {
			
			RoleActor self = pair.getSelf();
			RoleActor alter = pair.getAlter();
			RoleRelation relation = relations.getSelfRelation(self);

			// Do not removed actors indispensable for gender configuration
			if (immortals==null || !immortals.contains(new RoleDefinition(self.getIndividual(),alter))){
							
				if (relation!=null && relation.hasActor(alter)){
										
					if (relation.getActors().remove(alter)){
						report.appendln("Removed"+reciprocal(withReciprocal)+"role:\t"+alter.getIndividual()+"\t"+self.getIndividual()+"\t"+alter.getRole());
					}
					// True also if the item has already been removed before, needed for enclosing method (do not identify with the return of remove!)
					result = true;
				}
				
				if (withReciprocal && !self.equalsAbsolute(alter)) {
					
					removeRoleDefinition(relations,pair.reciprocal(relations),false, immortals, report);
				}
			}
		}
		
		//
		return result;
	}
	
	private static boolean neutralizeEgoGender (RoleRelations relations, RoleActorPair pair, boolean withReciprocal, StringList report){
		boolean result;
		
		result = false;
		
		if (pair.isValid()){
			
			RoleActor self = pair.getSelf();
			RoleActor alter = pair.getAlter();
			
			if (!self.getEgoGender().isUnknown()) {
				
				RoleRelation egoNeutralSelfRelation = relations.getSelfRelation(self.egoGenderNeutral());
				
				if (egoNeutralSelfRelation!=null && egoNeutralSelfRelation.hasActor(alter.egoGenderNeutral())){
					
					result = true;

				} else {
					
					if (removeRoleDefinition(relations,new RoleActorPair(relations, self.egoGenderComplement(),alter.egoGenderComplement(),pair.getAdjustable()),false,null, report)) {

						putRoleRelation(relations,new RoleActorPair(relations,self.egoGenderNeutral(),alter.egoGenderNeutral(),pair.getAdjustable()),withReciprocal,report);
						result = true;
					}
				}

			} else {
				
				for (Gender egoGender : Gender.valuesNotUnknown()){

					removeRoleDefinition(relations,new RoleActorPair(relations,self.withEgoGender(egoGender),alter.withEgoGender(egoGender),pair.getAdjustable()),false,null,report);
					
				}
			}
		}
		//
		return result;
	}
	
	private static String reciprocal (boolean withReciprocal){
		String result;
		
		result = " ";
		if (!withReciprocal){
			result = " reciprocal ";
		}
		//
		return result;
	}
	
	private static void putRoleRelation(RoleRelations relations, RoleActorPair pair, boolean withReciprocal, StringList report){
				
		if (pair.isValid()){
			
			RoleActor self = pair.getSelf();
			RoleActor alter = pair.getAlter();
			
			if (!neutralizeEgoGender(relations,pair,withReciprocal,report)){

				RoleRelation relation = relations.getSelfRelation(self);

				// Store new self relation if required 
				
				if (relation==null){
								
					relation = new RoleRelation(relations.size()+1);
					relation.getActors().addNonRedundant(self);
					relations.addAndUpdate(relation);
				}
				
				if (relation.getActors().addNonRedundant(alter)){

					if (relations.updateGenderConfigs(alter)){
						report.appendln("Updated configuration\t"+alter+"\t"+relations.getGenderConfigs().get(alter.getIndividual()));
					}

					report.appendln("Defined"+reciprocal(withReciprocal)+"role:\t"+alter.getIndividual()+"\t"+self.getIndividual()+"\t"+alter.getRole());

					if (withReciprocal){
						
						putRoleRelation(relations,pair.reciprocal(relations),false,report);
					}		
				}
			}
		}
	}

	private static boolean isEgoLinked (RoleRelations relations, RoleActor actor, Map<Role,Roles> genderConfigs){
		boolean result;
		
		result = false;
		
		RoleRelation selfRelation = relations.getSelfRelation(actor);
		
		if (selfRelation!=null){
			
			// Check for direct child or spouse links to ego
			for (RoleActor alter : selfRelation.getActorsByRoleNameWithUnspecific(null, relations)){
				if (alter.hasSelfName()){
					result = true;
					break;
				}
			}
		}

		//
		return result;
	}

	
	/**
	 * Check for auto-sibling property (classificatory property)
	 * @param relations
	 * @param actor
	 * @return
	 */
	private static boolean isClassificatory(RoleRelations relations, RoleActor actor){
		boolean result;
		
		result = false;
		
		for (RoleActor alter : actor.getActorsByRoleNameWithUnspecific("SIBLING", relations)){
			if (alter.getName().equals(actor.getName())){
				result = true;
				break;
			}
		}
		//
		return result;
	}
	
	private static boolean isIndividualizable (RoleRelations relations, RoleActor actor){
		boolean result;
		
		result = actor.isPersonal(relations) && !isClassificatory(relations,actor);
		//
		return result;
	}
	
	private static void composeRelations (RoleRelations relations, String alterRoleName, String mediusRoleName, String mediusAlterRoleName, StringList report) {
		
		report.appendln("Added "+alterRoleName+" links (mediated as "+mediusAlterRoleName+" of "+mediusRoleName+")");
		report.appendln();
				
		for (RoleActor medius : relations.getSortedSelfs()){
						
			StringList subReport = new StringList();

			RoleRelation mediusRelation = relations.getSelfRelation(medius);
			List<RoleActor> alters = mediusRelation.getActorsByRoleNameWithUnspecific(mediusAlterRoleName,relations).toListSortedByDistances(relations);
			List<RoleActor> selfs = mediusRelation.getActorsByRoleNameWithUnspecific(MetaRole.invertName(mediusRoleName),relations).toListSortedByDistances(relations);

			for (RoleActor self : selfs){
				
				for (RoleActor alter : alters){
					
					if (isComposable(relations,medius,alter,self,alterRoleName,mediusRoleName,mediusAlterRoleName)){								
							
						putRoleRelation(relations,new RoleActorPair(relations,self,alter.cloneAs(alterRoleName),Adjustable.SELF),true,subReport);
					}
				}
			}
			if (!subReport.isEmpty()){
				report.appendln("Added "+alterRoleName+" link between "+MetaRole.invertName(mediusRoleName)+" and "+mediusAlterRoleName+" of "+medius);
				report.append(subReport);
				report.appendln();
			}
		}
		logger.debug("Relations composed: "+alterRoleName+"-"+mediusRoleName+"-"+mediusAlterRoleName);
	}
	
	private static RoleActors getCommonParents(RoleRelations relations, RoleActor self, RoleActor alter, RoleActor medius){
		RoleActors result;
		
		result = new RoleActors();
		
		for (RoleActor parent : relations.getSortedActors()){
			
			RoleActors children = parent.getActorsByRoleNameWithUnspecific("CHILD", relations);

			if (children.containsAbsolute(self) && children.containsAbsolute(alter) && children.containsAbsolute(medius)){
				
				result.addNew(parent);
			}
		}
		//
		return result;
	}

	
	private static boolean haveCommonParent(RoleRelations relations, RoleActor self, RoleActor alter, RoleActor medius){
		boolean result;
		
		result = false;
		
		for (RoleRelation relation : relations){
			
			RoleActors children = relation.getActorsByRoleNameWithUnspecific("CHILD", relations);

			if (children.containsAbsolute(self) && children.containsAbsolute(alter) && children.containsAbsolute(medius)){
				
				result = true;
				break;
			}
		}
		//
		return result;
	}
	
	private static boolean isComposable(RoleRelations relations, RoleActor medius, RoleActor alter, RoleActor self, String alterRoleName, String mediusRoleName, String mediusAlterRoleName){
		boolean result;

		result = true;
		
		// Mere match and even equality of ego genders is not enough, as men and women may have partly different and partly shared systems - see Watchi feminine teknonymy

		if (relations.getEgoGenderScope(self)!=relations.getEgoGenderScope(alter)){

			result = false;
				
		// No composition of primary relations
		} else if (self.hasSelfName() || alter.hasSelfName()){
			
			result = false;
		
		// No automatic automarriage
		} else if (alterRoleName.equals("SPOUSE") && self.equalsAbsolute(alter)){
				
			result = false;
			
		// Conform spouse gender configuration
		} else if (alterRoleName.equals("SPOUSE") && relations.heteroMarriage() && self.getAlterGender()!=alter.getAlterGender().invert()){
				
			result = false;
				
		// Extension of links to sibling terms  only if classificatory	and at least 1 shared parent
		} else if (mediusAlterRoleName.equals("SIBLING") && mediusRoleName.equals("SIBLING")){
			
			result = haveCommonParent(relations,self,alter,medius) && !isIndividualizable(relations,alter) && !isIndividualizable(relations,self);
			
		} else {
			
			RoleRelation mediusRelation = relations.getSelfRelation(medius);
									
			// Checks uniqueness of self for medius (possibility of define self-alter link)
			for (RoleActor otherSelf : mediusRelation.getActorsByRoleNameWithUnspecific(MetaRole.invertName(mediusRoleName), relations)){
				if (!otherSelf.matchesAbsolute(self) && otherSelf.getEgoGender().matchs(self.getEgoGender()) && otherSelf.getAlterGender().matchs(self.getAlterGender())){
					result = false;
					break;
				}
			}			

			if (result){

				RoleRelation alterRelation = relations.getSelfRelation(alter);
				
				if (alterRelation!=null){
					
					// Checks uniqueness of self for alter (necessity to define self-alter link)
					for (RoleActor otherSelf : alterRelation.getActorsByRoleNameWithUnspecific(MetaRole.invertName(alterRoleName),relations)){
						if (!otherSelf.matchesAbsolute(self) && otherSelf.getEgoGender().matchs(self.getEgoGender()) && otherSelf.getAlterGender().matchs(self.getAlterGender())){
							result = false;
							break;
						}
					}
				}
			}
		}
		//
		return result;
	}

	private static void fixCommonParent (RoleRelations relations, RoleActor self, RoleActor alter, RoleActor medius, RoleDefinitions immortals){
		
		RoleDefinitions minimalDefinitions = null;
		
		RoleActor selfChild = self.cloneAs("CHILD");
		RoleActor alterChild = alter.cloneAs("CHILD");
		RoleActor mediusChild = medius.cloneAs("CHILD");
		
		for (RoleActor parent : getCommonParents(relations,self,alter,medius).toListSortedByDistances(relations)){
			
			RoleDefinitions missingDefinitions = new RoleDefinitions();
			Role parentTerm = parent.getIndividual();
			
			boolean hasSelf = false;
			boolean hasAlter = false;
			boolean hasMedius = false;
			
			for (RoleDefinition definition : immortals.getBySelfTerm(parentTerm).toSortedList()){
				
				if (definition.getAlter().equals(selfChild)){
					hasSelf = true;
				}
				if (definition.getAlter().equals(alterChild)){
					hasAlter = true;
				}
				if (definition.getAlter().equals(mediusChild)){
					hasMedius = true;
				}
			}
			
			if (!hasSelf){
				missingDefinitions.add(relations.getStandardRoleDefinition(parent,selfChild));
			}
			if (!hasAlter){
				missingDefinitions.add(relations.getStandardRoleDefinition(parent,alterChild));
			}
			if (!hasMedius){
				missingDefinitions.add(relations.getStandardRoleDefinition(parent,mediusChild));
			}
			
			if (minimalDefinitions==null || missingDefinitions.size()<minimalDefinitions.size()){
				
				minimalDefinitions = missingDefinitions;
				
				if (minimalDefinitions.size()==0){
					break;
				}
			}
		}
		
		for (RoleDefinition minimalDefinition : minimalDefinitions.toSortedList()){
			immortals.addNew(minimalDefinition);
		}
		
		immortals.addAll(minimalDefinitions);
	}
	
	private static void decomposeRelations(RoleRelations relations,String alterRoleName, String mediusRoleName, String mediusAlterRoleName, RoleDefinitions immortals, Report report){
			
		report.outputs().appendln("Removed "+alterRoleName+" links (mediated as "+mediusAlterRoleName+" of "+mediusRoleName+")");
		report.outputs().appendln();
				
		RoleDefinitions tempImmortals = new RoleDefinitions();
					
		for (RoleActor medius : relations.getSortedSelfs()){
			
			StringList subReport = new StringList();

			RoleRelation mediusRelation = relations.getSelfRelation(medius);
			List<RoleActor> alters = mediusRelation.getActorsByRoleNameWithUnspecific(mediusAlterRoleName,relations).toListSortedByDistances(relations);
			List<RoleActor> selfs = mediusRelation.getActorsByRoleNameWithUnspecific(MetaRole.invertName(mediusRoleName),relations).toListSortedByDistances(relations);
			
			for (RoleActor self : selfs){
				
				if (!medius.matchesAbsolute(self)){

					for (RoleActor alter : alters){
						
						if (!medius.matchesAbsolute(alter)){
							
							RoleActorPair pair = new RoleActorPair(relations,self,alter.cloneAs(alterRoleName),Adjustable.NONE);

							if (pair.isValid()){
								
								RoleActor adjustedSelf = pair.getSelf();
								RoleActor adjustedAlter = pair.getAlter();
								RoleRelation selfRelation = relations.getSelfRelation(adjustedSelf);
								
								if (selfRelation!=null && selfRelation.hasActor(adjustedAlter)){
																		
									if (isComposable(relations,medius,adjustedAlter,self,alterRoleName,mediusRoleName,mediusAlterRoleName)){

										if (!immortals.contains(new RoleDefinition(self.getIndividual(),adjustedAlter)) && !tempImmortals.contains(new RoleDefinition(self.getIndividual(),adjustedAlter))){

											if (alterRoleName.equals("SIBLING") && mediusRoleName.equals("SIBLING") && mediusAlterRoleName.equals("SIBLING")){
												
												// Autosibling links for personal links has to be coded explicitly (circular condition for composability check)
												if (self.isPersonal(relations) && self.matchesAbsolute(adjustedAlter)){
													continue;
												}
												
												// Fix parent relation (not to be removed in next step, since necessary for siblingship verification)
												fixCommonParent(relations,self,alter,medius,immortals);
											}
											
											tempImmortals.addNew(new RoleDefinition(medius.getIndividual(),self));
											tempImmortals.addNew(new RoleDefinition(medius.getIndividual(),alter));
											tempImmortals.addNew(new RoleDefinition(self.getIndividual(),medius.asReciprocalOf(self,relations)));
											tempImmortals.addNew(new RoleDefinition(alter.getIndividual(),medius.asReciprocalOf(alter,relations)));

											removeRoleDefinition(relations,pair,true,immortals,subReport);
										}
									}
								}
								
							}
							
						}
					}
				}
			}
			if (!subReport.isEmpty()){
				report.outputs().appendln("Removed "+alterRoleName+" link between "+MetaRole.invertName(mediusRoleName)+" and "+mediusAlterRoleName+" of "+medius);
				report.outputs().append(subReport);
				report.outputs().appendln();
			}
		}
		report.outputs().appendln();
	}
	
	public static MetaRole getUniqueGenderConfig (Role role, Roles genderConfig){
		MetaRole result;
		
		if (genderConfig==null){
			
			result = new MetaRole(null,Gender.UNKNOWN,Gender.UNKNOWN,AlterAge.UNKNOWN);
			
		} else {
			
			result = null;
			
			for (Role metaRole : genderConfig){
				MetaRole absolute = ((MetaRole)metaRole).absolute();

				if (result == null){
					result = absolute;
				} else if (!result.equals(absolute)){
					if (result.isCross() && absolute.isCross()){
						result.setEgoGender(null);
						result.setAlterGender(null);
						result.setCross(true);
					} else {
						result = null;
						break;
					}
				}
			}
		}
		
		//
		return result;
	}
	
	public static Map<Role,MetaRole> getUniqueGenderConfigs(RoleRelations relations){
		Map<Role,MetaRole> result;
		
		result = new HashMap<Role,MetaRole>();
		 Map<Role,Roles> genderConfigs = relations.getGenderConfigs();
		
		for (Role role : relations.getRoles()){
			result.put(role, getUniqueGenderConfig(role, genderConfigs.get(role)));
		}
		
		//
		return result;
		
	}
	
	public static void compareDefinitions (RoleDefinitions alpha, RoleDefinitions beta, RoleRelations relations, Report report){
		
		if (alpha!=null && beta!=null){
			
			StringList maintained = new StringList();
			StringList removed = new StringList();
			StringList added = new StringList();
			StringList inverted = new StringList();
			
			
			for (RoleDefinition definition : alpha){
				if (!beta.contains(definition)){
					if (beta.contains(definition.inverse(relations))){
						inverted.appendln(definition.toString());
					} else {
						removed.appendln(definition.toString());
					}
				} else {
					maintained.appendln(definition.toString());
				}
			}
			
			for (RoleDefinition definition : beta){
				if (!alpha.contains(definition) && !alpha.contains(definition.inverse(relations))){
					added.appendln(definition.toString());
				} 
			}
			
			report.outputs().appendln("Comparison with original definitions:");
			report.outputs().appendln();
			report.outputs().appendln(maintained.size()/2+" definitions maintained");
			report.outputs().append(maintained);
			report.outputs().appendln();
			report.outputs().appendln(removed.size()/2+" definitions removed");
			report.outputs().append(removed);
			report.outputs().appendln();
			report.outputs().appendln(added.size()/2+" definitions added");
			report.outputs().append(added);
			report.outputs().appendln();
			report.outputs().appendln(inverted.size()/2+" definitions inverted");
			report.outputs().append(inverted);
			report.outputs().appendln();

		}
		
	}
	

	public static RoleDefinitions createRoleDefinitions (final RoleRelations source, List<RoleRelationRule> rules, final Report report){
		RoleDefinitions result;
		
		result = new RoleDefinitions();
		RoleRelations relations = source.clone();
		
		RoleDefinitions basicDefinitions = relations.getBasicRoleDefinitions();
		
		report.outputs().appendln("Configurating definitions");
		report.outputs().appendln();
		for (String basicDefinition: basicDefinitions.toSortedStringList()){
			report.outputs().appendln(basicDefinition);
		}
		report.outputs().appendln();
		
		if (rules == null){
			rules = getDefaultRules();
		}
		Collections.sort(rules, Collections.reverseOrder());
		for (RoleRelationRule rule : rules){
			
			decomposeRelations(relations,rule.getAlterRoleName(),rule.getMediusRoleName(),rule.getMediusAlterRoleName(),basicDefinitions,report);
			
		}
		
/*		decomposeRelations(relations,"CHILD","SPOUSE","CHILD",basicDefinitions,report);
		decomposeRelations(relations,"SPOUSE","CHILD","PARENT",basicDefinitions,report);
		decomposeRelations(relations,"SIBLING","SIBLING","SIBLING",basicDefinitions,report);
		decomposeRelations(relations,"CHILD","CHILD","SIBLING",basicDefinitions,report); */
					
		
		removeReciprocalRelations(relations,basicDefinitions,report);
		
		for (RoleRelation relation : relations.toSortedList()){
			for (RoleActor actor : relation.getActors().toListSortedByRoles()){
				if (!actor.isSelf()){
					if (!actor.getRole().isCross()){
						result.put(new RoleDefinition(relation.getSelfTerm(),actor));
					} else {
						for (RoleActor crossActor : actor.crossExtension()){
							result.put(new RoleDefinition(relation.getSelfTerm(),crossActor));
						}
					}
				}
			}
		}
		
		result = result.compress();
		
		for (RoleActor actor : relations.getSortedSelfs()){
//			System.out.println(actor+"\t"+relations.getSelfRelation(actor));
		}
		//
		return result;
	}
	
	private static void removeReciprocalRelations(RoleRelations relations, RoleDefinitions immortals,  Report report){
					
		report.outputs().appendln("Remove reciprocal relations");
		report.outputs().appendln();
		
		StringList subReport = new StringList();
		
		for (RoleActor self : relations.getSortedSelfs()){
			
			for (RoleActor alter : relations.getSelfRelation(self).getActors().toListSortedByDistances(relations)){
									
				if (!alter.equalsAbsolute(self)) {
					
					RoleActorPair pair = new RoleActorPair(relations,self,alter,Adjustable.NONE);
					RoleActorPair reciprocalPair = pair.reciprocal(relations);
					
//					RoleActor reciprocal = self.asReciprocalOf(alter);
											
					// Remove one of the reciprocal actors unless necessary for gender configurations
					boolean removed = false;
					
					if (pair.isStandardPosition(relations)) {

						removed = removeRoleDefinition(relations,reciprocalPair,false,immortals,subReport);
					} 
					
					if (!removed){
						
						removed = removeRoleDefinition(relations,pair,false,immortals,subReport);
						immortals.addNew(reciprocalPair.getRoleDefinition());

					}
				}
			}
		}
		if (!subReport.isEmpty()){
			report.outputs().append(subReport);
			report.outputs().appendln();
		}

	}

	/***
	 * under construction...
	 * @param net
	 * @param segmentation
	 * @param model
	 * @param maxIterations
	 */
	public static void applyModel (Net net, Segmentation segmentation, RelationModel model, int maxIterations){
		
		maxIterations = 1;
		
		RoleRelations roleRelations = model.getRoleRelations();
		
		for (Individual ego : segmentation.getCurrentIndividuals()){
			
			Map<Individual,Integer> distances = new HashMap<Individual,Integer>();
			distances.put(ego, 0);
			
			Queue<ExtendedActor> queue = new LinkedList<ExtendedActor>();
			
			Relation relation = net.createRelation(ego.getId(), model.getRoleRelations().getSelfName()+" = "+ego, model);
			RoleActor self = model.getRoleRelations().getSelf(ego.getGender());
			
			relation.addActor(ego, self.getIndividual());
			queue.add(new ExtendedActor(ego,self));

			while (!queue.isEmpty()){
				
				expand(relation,roleRelations,maxIterations,queue,distances);
			}
		}
	}
	
	private static void expand (Relation relation, RoleRelations roleRelations, int maxIterations, Queue<ExtendedActor> queue, Map<Individual,Integer> distances){
		
		ExtendedActor extendedActor = queue.poll();
		Individual self = extendedActor.getIndividual();
		RoleActor selfActor = extendedActor.getRoleActor();
		Integer distance = distances.get(self);
		
		for (KinType kinType : KinType.basicTypesWithSiblings()){
	
			for (Gender alterGender : Gender.valuesNotUnknown()){
	
				Individuals alters = self.getKin(kinType,alterGender);
				RoleActors alterActors = roleRelations.getAlters(selfActor, kinType, alterGender);
								
				for (Individual alter : alters){
					
					if (distances.get(alter)==null){
					
						distances.put(alter,distance+1);

						for (RoleActor alterActor : alterActors){
							
							if (!isEgoLinked(roleRelations,alterActor,roleRelations.getGenderConfigs()) || distance==0){
								
								if (relation.addNewActor(alter, alterActor.getIndividual()) && distance<maxIterations){

									alterActor = alterActor.withEgoGender(selfActor.getEgoGender());
									queue.add(new ExtendedActor(alter,alterActor));
								}
							}
						}
					}
				}
			}
		}
	}
	
	private static boolean noSameSexSpouse(Individual selfIndi, Gender alterGender, KinType kinType){
		return !kinType.equals(KinType.SPOUSE) || selfIndi.getGender().invert()==alterGender;
	}
	
	private static boolean noDoubleParent(Individual selfIndi, Gender alterGender, KinType kinType){
		return !kinType.equals(KinType.PARENT) || selfIndi.getParent(alterGender)==null;
	}

	private static void expand (Net net, RoleRelations relations, Queue<ExtendedActor> queue, int maxIterations, Map<Individual,Integer> distances) throws PuckException{
		
		ExtendedActor extendedActor = queue.poll();
		Individual selfIndi = extendedActor.getIndividual();
		RoleActor self = extendedActor.getRoleActor();
		int distance = distances.get(selfIndi);
		
		for (KinType kinType : KinType.basicTypesWithSiblings()){
			
			for (Gender alterGender : Gender.valuesNotUnknown()){
								
				if (noSameSexSpouse(selfIndi,alterGender,kinType) && noDoubleParent(selfIndi,alterGender,kinType)){

					for (RoleActor alter : relations.getAlters(self, kinType,alterGender)) {
				
						if (!alter.hasSelfName() && (self.hasSelfName() || !alter.isPersonal(relations))){
						
							Individuals potentialKin =  selfIndi.getPotentialKin(kinType);
							Individual alterIndi = potentialKin.getFirstCorresponding(alter.getName(),alterGender,self.getEgoGender());
				
							if (alterIndi==null){
								
								if (kinType==KinType.PARENT && potentialKin.hasGender(alterGender)){
									// Parent already exists, no new parent will be created and no new relation set
									continue;
								}
								alterIndi = net.createIndividual(alter.getName(), alterGender);
								alterIndi.setAttribute("EGOGENDER", alter.getEgoGender()+"");
							}

							NetUtils.setKin(net, alterIndi, selfIndi, kinType);
							
							if (distance<maxIterations && !distances.containsKey(alterIndi)){
								
								alter = alter.withEgoGender(self.getEgoGender());
								distances.put(alterIndi, distance+1);
								queue.add(new ExtendedActor(alterIndi,alter));
								
							}
						}
					}
				}
			}
		}
	}

	static boolean concatenable(RoleActor alpha, RoleActor beta){
		boolean result;
		
		if (alpha==null){
			
			result = true;
			
		} else {

			result = !(alpha.isSibling() && beta.isParent()) &&
					!(alpha.isChild() && beta.isSibling()) &&
					!(alpha.isSibling() && beta.isSibling()) &&
					!(alpha.isChild() && beta.isParent()) && 
					!(alpha.isParent() && beta.isChild()) &&
					!(alpha.isSpouse() && beta.isSpouse());
		}

		//
		return result;
	}
	
	public static Net createNet (RelationModel model, int maxIterations) throws PuckException{
		Net result;
		
		RoleRelations relations = model.getRoleRelations();
		
		result = new Net();
		result.setLabel(model.getName());
		result.relationModels().add(new RelationModel("SIBLING"));
		
		for (Gender egoGender : Gender.valuesNotUnknown()){
			
			RoleActor self = model.getRoleRelations().getSelf(egoGender);
			Individual ego = result.createIndividual(model.getRoleRelations().getSelfName(), egoGender);
			ego.setAttribute("EGOGENDER", egoGender+"");
			
			Map<Individual,Integer> distances = new HashMap<Individual,Integer>();
			distances.put(ego, 0);
			
			Queue<ExtendedActor> queue = new LinkedList<ExtendedActor>();
			queue.add(new ExtendedActor(ego,self));

			while (!queue.isEmpty()){
				
				expand(result, relations,queue,maxIterations,distances);
			}
		}
		
		//Link siblings to parents
		for (Individual ego : result.individuals().toSortedList()){
			
			for (Individual sibling : ego.getRelated("SIBLING")){
				
				RoleActor siblingActor = new RoleActor(new Role(sibling.getName()),new MetaRole("SIBLING",Gender.valueOf(sibling.getAttributeValue("EGOGENDER")),sibling.getGender(),AlterAge.UNKNOWN),relations.getSelfName());
				
				for (Individual parent : ego.getParents()){
					if (sibling.getParent(parent.getGender())==null){
						for (RoleActor siblingsParentActor : siblingActor.getActorsByRoleNameWithUnspecific("PARENT", relations)){
							if (siblingsParentActor.getAlterGender()==parent.getGender() && siblingsParentActor.getName().equals(parent.getName())){
								NetUtils.setKin(result, parent, sibling, KinType.PARENT);
								break;
							}
						}
					}
				}
			}
		}

		//Fuse families
		for (Individual ego : result.individuals().toSortedList()){

			if (ego.spouses().size()==1){
				Individual spouse = ego.spouses().getFirst();
				Family mainFamily = result.families().getBySpouses(ego, spouse);
				for (Family otherFamily : ego.getPersonalFamilies()){
					if (!otherFamily.equals(mainFamily)){
						for (Individual child: otherFamily.getChildren()){
							
							RoleActor childActor = new RoleActor(new Role(child.getName()),new MetaRole("CHILD",Gender.valueOf(child.getAttributeValue("EGOGENDER")),child.getGender(),AlterAge.UNKNOWN),relations.getSelfName());

							for (RoleActor childsParentActor : childActor.getActorsByRoleNameWithUnspecific("PARENT", relations)){
								if (childsParentActor.getAlterGender()==spouse.getGender() && childsParentActor.getName().equals(spouse.getName())){
									mainFamily.getChildren().add(child);
									child.setOriginFamily(mainFamily);
									break;
								}
							}
						}
					}
				}
			}
		}
		
		// Cleanup

		// Remove isolate siblings 
		for (Individual isolate : result.individuals().toSortedList()){
			if (isolate.isIsolate()){
				for (Relation relation: isolate.relations().toList()){
					for (Individual sibling : relation.getIndividuals()){
						sibling.relations().remove(relation);
					}
					result.remove(relation);
				}
				result.individuals().removeById(isolate.getId());
			}
		}

		// Marry coparents
		for (Family family : result.families().toSortedList()){
			
			if (!family.hasMarried()){
				family.setMarried();
			}
		}
					
		//
		return result;
	}

}
