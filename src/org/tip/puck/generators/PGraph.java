/**
 * 
 */
package org.tip.puck.generators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.statistics.StatisticsWorker;
import org.tip.puck.util.RandomGenerator;

/**
 * @author Telmo Menezes
 * @author Klaus Hamberger
 * 
 */
public class PGraph {
	private Map<String, PGraphNode> nodes;
	private Map<Individual, Set<PGraphNode>> marriagesByIndividual;
	private Vector<PGraphLink> linksVector;

	private int totalLinks;
	
	private SiblingMarriageDistribution smDistrib;
	
	private int getProbableNumberOfSpouses(Gender gender, int k){
		if (gender == Gender.UNKNOWN){
			return 0;
		} else {
			return smDistrib.howManySpouses(gender, k);
		}
	}
	
	public Net toNet2 () {
		Net result = new Net();
		
		Map<PGraphNode, Set<PGraphNode>> brotherhood = new HashMap<PGraphNode, Set<PGraphNode>>();
		Map<PGraphNode, Set<PGraphNode>> sisterhood = new HashMap<PGraphNode, Set<PGraphNode>>();
		
		for (PGraphLink link: linksVector) {
			if (link.isWifeLink()) {
				if (!sisterhood.containsKey(link.getOrigin())) {
					sisterhood.put(link.getOrigin(), new HashSet<PGraphNode>());
				}
				sisterhood.get(link.getOrigin()).add(link.getTarget());
			}
			else {
				if (!brotherhood.containsKey(link.getOrigin())) {
					brotherhood.put(link.getOrigin(), new HashSet<PGraphNode>());
				}
				brotherhood.get(link.getOrigin()).add(link.getTarget());
			}
		}
		
		return result;
	}
	
	public Net toNet (){
		Net result;
		
		Map<PGraphNode,Family> index = new HashMap<PGraphNode,Family>();
		
		result = new Net();
		
		//extract PersonalFamilies
		int id = 1;
		for (PGraphNode node: nodes.values()){
			Individual husband = null;
			Individual wife = null;
			Family family = null;
			if (node.getHusband()!=null){
//				husband = result.individuals().getById(node.getHusband().getId());
//				if (husband==null){
					husband = node.getHusband().clone();
//					husband.setId(id);
					node.setHusband(husband);
					result.individuals().add(husband);
					id++;
//				}
			}
			if (node.getWife()!=null){
//				wife = result.individuals().getById(node.getWife().getId());
//				if (wife==null){
					wife = node.getWife().clone();
//					wife.setId(id);
					node.setWife(wife);
					result.individuals().add(wife);
					id++;
//				}
			}
			if (node.getFamily()!=null){
				family = new Family(node.getFamily().getId(),husband,wife);
				if (husband!=null) {
					husband.addPersonalFamily(family);
				}
				if (wife!=null) {
					wife.addPersonalFamily(family);
				}
				if (node.getFamily().hasMarried()) {
					family.setMarried(true);
				}
				result.families().add(family);
				index.put(node, family);
			}
		}
		
		
		
		//extract OriginFamilies
		for (PGraphNode node: nodes.values()){
			Individual husband = node.getHusband();
//			if (node.getHusband()!=null){
//				husband = result.get(node.getHusband().getId());
//			}
			Individual wife =node.getWife();
//			if (node.getWife()!=null){
//				wife = result.get(node.getWife().getId());
//			}
			Family husbandsParents = index.get(node.getHusbandLink());
			Family wifesParents = index.get(node.getWifeLink());
			
			if (husband!=null && husbandsParents!=null) {
				husband.setOriginFamily(husbandsParents);
				husbandsParents.getChildren().add(husband);
			}
			if (wife!=null && wifesParents!=null) {
				wife.setOriginFamily(wifesParents);
				wifesParents.getChildren().add(wife);
			}
		}
		
		//fuse same-sex Siblings
		for (Individual individual : result.individuals()){
			int additionalSpouses = getProbableNumberOfSpouses(individual.getGender(), monogamousSameSexSiblings(individual).size());
			while (additionalSpouses > 1 && additionalSpouses > monogamousSameSexSiblings(individual).size()) {
				Individual sibling = randomDraw(monogamousSameSexSiblings(individual));
				Family additionalFamily = sibling.getPersonalFamilies().toList().get(0);
				if (individual.isMale()){
					additionalFamily.setHusband(individual);
				} else if (individual.isFemale()){
					additionalFamily.setWife(individual);
				}
				individual.addPersonalFamily(additionalFamily);
				result.individuals().removeById(sibling.getId());
			}
		}
		
		//
		return result;
	}
	
	private static Individual randomDraw (Individuals individuals){
		int randomIndex = (int)(Math.random()*individuals.size());
		return individuals.toList().get(randomIndex);
	}
	
	public static Individuals monogamousSameSexSiblings(Individual individual) {
		Individuals result;
		
		result = new Individuals();
		Family family = individual.getOriginFamily();
		if (family != null) {
		    for (Individual sibling : family.getChildren()){
		        if (sibling!=individual && sibling.getGender()==individual.getGender() && sibling.spouses().size()==1){
		            result.add(sibling);
		        }
		    }
		}
		//
		return result;
	}
	
	private PGraphNode getPGraphNode (Family family){
		return nodes.get(new PGraphNode(family).hashKey());
	}

	public PGraph(final Net net) {

		smDistrib = new SiblingMarriageDistribution(net);
		totalLinks = 0;
		nodes = new HashMap<String, PGraphNode>();

		// Put couple nodes
		for (Family family : net.families()) {
			PGraphNode node = new PGraphNode(family);  
			nodes.put(node.hashKey(), node);
		}
		
		// Put single individual nodes
		for (Individual individual : net.individuals()){
			if (individual.getPersonalFamilies().size()==0){
				PGraphNode node = new PGraphNode(individual);
				nodes.put(node.hashKey(), node);
			}
		}
		
		// Extract links 
		for (PGraphNode node : nodes.values()){
			Individual husband = node.getHusband();
			Individual wife = node.getWife();
			if (husband!=null && husband.getOriginFamily()!=null){
				PGraphNode husbandsParents = getPGraphNode(husband.getOriginFamily());
				node.setHusbandLink(husbandsParents);
				totalLinks++;
			}
			if (wife!=null && wife.getOriginFamily()!=null){
				PGraphNode wifesParents = getPGraphNode(wife.getOriginFamily());
				node.setWifeLink(wifesParents);
				totalLinks++;
			}
		}

		// create links vector
		linksVector = new Vector<PGraphLink>();
		for (Map.Entry<String, PGraphNode> entry : nodes.entrySet()) {
			PGraphNode marriage = entry.getValue();
			PGraphNode wifeLink = marriage.getWifeLink();
			PGraphNode husbandLink = marriage.getHusbandLink();
			if (wifeLink != null) {
				PGraphLink link = new PGraphLink(wifeLink, marriage, true);
				linksVector.add(link);
			}
			if (husbandLink != null) {
				PGraphLink link = new PGraphLink(husbandLink, marriage, false);
				linksVector.add(link);
			}
		}

		// compute out degrees and ancestors
		computeOutDegrees();
		computeAncestors();
		
	}
	
	public PGraph(final PGraph pg) {
	    smDistrib = pg.smDistrib;
		totalLinks = pg.totalLinks;

		// copy nodes
		nodes = new HashMap<String, PGraphNode>();
		for (Map.Entry<String, PGraphNode> entry : pg.nodes.entrySet()) {
			String key = entry.getKey();
			PGraphNode node = new PGraphNode(entry.getValue());
			nodes.put(key, node);
		}

		// create links vector
		linksVector = new Vector<PGraphLink>();
		for (Map.Entry<String, PGraphNode> entry : nodes.entrySet()) {
			PGraphNode marriage = entry.getValue();
			PGraphNode wifeLink = marriage.getWifeLink();
			PGraphNode husbandLink = marriage.getHusbandLink();
			if (wifeLink != null) {
				PGraphLink link = new PGraphLink(wifeLink, marriage, true);
				linksVector.add(link);
			}
			if (husbandLink != null) {
				PGraphLink link = new PGraphLink(husbandLink, marriage, false);
				linksVector.add(link);
			}
		}

		// compute out degrees and ancestors
		computeOutDegrees();
		computeAncestors();
	}

	private boolean addLink(final PGraphLink link, final int maxGenDist) {
		PGraphNode orig = link.getOrigin();
		PGraphNode targ = link.getTarget();

		// marriages cannot link to themselves
		if (orig == targ) {
			// System.out.println("#1: self-link");
			return false;
		}

		// is origin a descendant of target?
		if (targ.hasDescendant(orig)) {
			// System.out.println("#2: orig is descendant of target");
			return false;
		}

		// is original generational distance within limit?
		int dist = orig.originalGenerationalDistance(targ);
		if ((dist < 0) || (dist > maxGenDist)) {
			//System.out.println("#3: originl generational distance not within limit " + dist);
			return false;
		}

		// set link on target
		if (link.isWifeLink()) {
			link.getTarget().setWifeLink(link.getOrigin());
		} else {
			link.getTarget().setHusbandLink(link.getOrigin());
		}

		// recompute ancestors
		clearAncestors();
		computeAncestors();

		return true;
	}

	public float averageNumberOfDescendants() {
		float avg = 0;

		for (Map.Entry<String, PGraphNode> entry : nodes.entrySet()) {
			PGraphNode marriage = entry.getValue();
			avg += marriage.numberOfDescendants();
		}

		avg /= numberOfMarriages();

		return avg;
	}
	
	public float maxNumberOfDescendants() {
		float max = 0;
		
		for (Map.Entry<String, PGraphNode> entry : nodes.entrySet()) {
			PGraphNode marriage = entry.getValue();
			float descs = marriage.numberOfDescendants();
			if (descs > max) {
				max = descs;
			}
		}

		return max;
	}

	private void clearAncestors() {
		for (Map.Entry<String, PGraphNode> entry : nodes.entrySet()) {
			PGraphNode marriage = entry.getValue();
			marriage.clearDescendants();
		}
	}

	private void computeAncestors() {
		for (Map.Entry<String, PGraphNode> entry : nodes.entrySet()) {
			PGraphNode marriage = entry.getValue();
			marriage.addDescendant(marriage, 0);
		}
	}

	private void computeOutDegrees() {
		for (Map.Entry<String, PGraphNode> entry : nodes.entrySet()) {
			PGraphNode marriage = entry.getValue();
			PGraphNode wifeLink = marriage.getWifeLink();
			PGraphNode husbandLink = marriage.getHusbandLink();
			if (wifeLink != null) {
				wifeLink.incOutDegree();
				wifeLink.incOutDegreeF();
			}
			if (husbandLink != null) {
				husbandLink.incOutDegree();
				husbandLink.incOutDegreeM();
			}
		}
	}

	public Map<String, PGraphNode> getMarriages() {
		return nodes;
	}

	public Map<Individual, Set<PGraphNode>> getMarriagesByIndividual() {
		return marriagesByIndividual;
	}

	public int getTotalLinks() {
		return totalLinks;
	}

	public int numberOfMarriages() {
		return nodes.size();
	}

	private void removeLink(final PGraphLink link) {
		if (link.isWifeLink()) {
			link.getTarget().setWifeLink(null);
		} else {
			link.getTarget().setWifeLink(null);
		}
	}

	public void storeOriginalDescendants() {
		for (Map.Entry<String, PGraphNode> entry : nodes.entrySet()) {
			PGraphNode marriage = entry.getValue();
			marriage.storeOriginalDescendants();
		}
	}

	public boolean switchLinks(final int k, final int maxGenDist) {
		// find list of links to shuffle, no repetitions, same gender target
		Set<PGraphLink> linkSet = new HashSet<PGraphLink>();
		PGraphLink link = linksVector.get(RandomGenerator.instance().random.nextInt(linksVector.size()));
		linkSet.add(link);
		boolean gender = link.isWifeLink();
		while (linkSet.size() < k) {
			link = linksVector.get(RandomGenerator.instance().random.nextInt(linksVector.size()));
			if (link.isWifeLink() == gender) {
				linkSet.add(link);
			}
		}
		List<PGraphLink> links = new ArrayList<PGraphLink>(linkSet);

		// create a perfect shuffle of the previous list
		List<PGraphLink> links2 = new ArrayList<PGraphLink>(links);
		boolean perfectShuffle = false;
		while (!perfectShuffle) {
			perfectShuffle = true;
			for (int i = 0; i < k; i++) {
				if (links.get(i) == links2.get(i)) {
					perfectShuffle = false;
				}
			}
			Collections.shuffle(links2);
		}

		// create new links list
		List<PGraphLink> newLinks = new ArrayList<PGraphLink>();
		for (int i = 0; i < k; i++) {
			PGraphLink newLink = new PGraphLink(links.get(i).getOrigin(), links2.get(i).getTarget(), links.get(i).isWifeLink());
			newLinks.add(newLink);
		}

		// remove old links
		for (PGraphLink oldLink : links) {
			removeLink(oldLink);
		}

		// recompute ancestors
		clearAncestors();
		computeAncestors();

		// try to add new links
		boolean fail = false;
		for (PGraphLink newLink : newLinks) {
			if (!addLink(newLink, maxGenDist)) {
				fail = true;
				break;
			}
		}

		// revert changes in case of failure
		if (fail) {
			for (PGraphLink newLink : newLinks) {
				removeLink(newLink);
			}
			for (PGraphLink oldLink : links) {
				addLink(oldLink, maxGenDist);
			}
		}
		else {
			// update link objects
			for (int i = 0; i < k; i++) {
				links.get(i).setTarget(links2.get(i).getTarget());
				links.get(i).setShuffled(true);
			}
		}

		return !fail;
	}
	
	public double distance(PGraph pg) {
		double dist = 0;
		double count = 0;
		for (Map.Entry<String, PGraphNode> entry : nodes.entrySet()) {
			String key = entry.getKey();
			PGraphNode m1 = entry.getValue();
			PGraphNode m2 = pg.getMarriages().get(key);
			dist += m1.distance(m2);
			count += 1.0;
		}
		
		return dist / count;
	}
	
	public double percentageShuffledLinks() {
		double shuffled = 0;
		double total = 0;
		for (PGraphLink link : linksVector) {
			if (link.isShuffled()) {
				shuffled += 1.0;
			}
			total += 1.0;
		}
		
		return (shuffled / total) * 100.0;
	}

	@Override
	public String toString() {
		String str = "Number of marriages: " + numberOfMarriages() + "\n";
		str += "Number of links: " + getTotalLinks() + "\n";
		str += "Average number of descendants: " + averageNumberOfDescendants() + "\n";
		str += "Max number of descendants: " + maxNumberOfDescendants() + "\n";
		return str;
	}
}
