package oldcore.trash;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;


import org.tip.puck.net.Net;


/**
 * The class of randomly generated kinship networks (by fieldwork simulation)
 * @author Klaus Hamberger
 * @since Version 0.5
 */
@SuppressWarnings("serial")
public class RandomNet extends Net {
	
	/**
	 * the array of average marriage ages for given gender (0 male, 1 female)
	 */
	final private int[] age;// = {i(10),i(11)};
	/**
	 * the array of age differences with given parents (0 father, 1 mother, 2 spouse)
	 */
	final private int[] dist;// = {i(12),i(14),i(16),i(3)};
	/**
	 * the proportion of males
	 */
	final private double mal;// = d(2); 
	/**
	 * the array of rates of married individuals of given gender (0 male, 1 female)
	 */
	final private double[] mar;// = {d(6),d(7)};
	/**
	 * the array of genealogical recall rates for given parent type (0 father, 1 mother, 2 spouse) and ego gender (0 male, 1 female)
	 */
	final private double[][] mem;// = {{d(18),d(19),d(20)},{d(21),d(22),d(23)}};
	/**
	 * the population size
	 * <p> a mare shortcut for setting the pop variable
	 */
	final private int n;// = i(0);
	/**
	 * an array containing population size, number of interviews and (genealogical or marriage) horizon
	 */
	final private int[] pop;// = {n,prod(n,d(1)),prod(n,d(5))};
	/**
	 * the array of rates of remarried individuals of given gender (0 male, 1 female)
	 */
	final private double[] remar;// = {d(8),d(9)};
	/**
	 * the array of tolerance spans for age difference and age (0 diff father-child, 1 diff mother-child, 2 diff husband-wife, 3 age)
	 */
	final private int[] tol;// = {i(13),i(15),i(17),i(4)}; 



	
	
	//Round!
	/**
	 * gets the product of an integer with a double
	 * @param i the integer
	 * @param d the double
	 * @return the product
	 */
	private static int prod (int i, double d){
		return new Double(i*d).intValue();
	}
	
	
	/**
	 * constructs a RandomNet
	 * @param str the name of the RandomNet
	 * @see gui.screens.StartScreen#open(String, int)
	 * @throws IOException
	 */
	public RandomNet (String str) throws IOException {
		Date date = new Date();
		RandomVertex myself = new RandomVertex();
		int interviews = 0;
		while (interviews < getNrInterviews()) {
			interviews = interviews+1;
		    RandomVertex v = myself.find(3,getPop(),1.0);
		    getProtocol().add(headline(2)+" Nr. "+interviews+" "+headline(3)+" "+v.dsignature());
		    v.tell(1.0,"  ");
		}
	}

	/**
	 * gets the age difference with a given parent
	 * @param i the type of parent (0 father, 1 mother, 2 spouse)
	 * @return the age difference
	 * @see elements.nodes.RandomVertex#idealBirthyear(int)
	 */
	public int getDist(int i){
		return dist[i];
	}
	
	/**
	 * gets the genealogical recall rate for given parent type and ego gender
	 * @param i the type of parent (0 father, 1 mother, 2 spouse)
	 * @param j the gender of ego (0 male, 1 female)
	 * @return the genealogical recall rate
	 * @see elements.nodes.RandomVertex#setRelative(RandomVertex,int,double,String)
	 */
	public double getGenMem (int i, int j){
		return mem[i][j];
	}
	
	/**
	 * gets the (matrimonial or kinship) horizon of an individual (the number of potential parents and marriage partners)
	 * @return the (matrimonial or kinship) horizon 
	 */
	public int getHor(){
		return pop[2];
	}
	
	/**
	 * gets the proportion of males
	 * @return the proportion of males
	 * @see elements.nodes.RandomVertex#idealGender(int)
	 */
	public double getMaleProp(){
		return mal;
	}

	/**
	 * gets the average marriage age for given gender
	 * @param g the gender index (0 male, 1 female)
	 * @return the average marriage age
	 * @see elements.nodes.RandomVertex#accepts(RandomVertex,int)
	 */
	public int getMarriageAge (int g){
		return age[g];
	}
	
	/**
	 * gets the rate of married individuals of given gender
	 * @param g the gender index (0 male, 1 female)
	 * @return the rate of married individuals
	 * @see elements.nodes.RandomVertex#wantsSpouse()
	 */
	public double getMarriageRate (int g){
		return mar[g];
	}
	
	/**
	 * gets the number of Interviews in the virtual fieldwork
	 * @return the number of interview
	 */
	private int getNrInterviews (){
		return pop[1];
	}
	
	/**
	 * gets the population size
	 * @return the population size
	 */
	private int getPop(){
		return pop[0];
	}
	
	/**
	 * gets the rate of remarried individuals of given gender
	 * @param g the gender index (0 male, 1 female)
	 * @return the rate of remarried individuals
	 * @see elements.nodes.RandomVertex#wantsSpouse()
	 */
	public double getRemarriageRate (int g){
		return remar[g];
	}
	
	/**
	 * gets the tolerance span for age difference and age
	 * @param i a key (0 diff father-child, 1 diff mother-child, 2 diff husband-wife, 3 age)
	 * @return the tolerance span for age difference and age
	 * @see elements.nodes.RandomVertex#accepts(RandomVertex,int)
	 * @see elements.nodes.RandomVertex#newFriend(int)
	 */
	public int getTolerance (int i){
		return tol[i];
	}	

}
