package org.tip.puck.io.ods;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import jxl.read.biff.BiffException;

import org.apache.commons.lang3.StringUtils;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents a TXT File reader and writer.
 * 
 * @author TIP
 */
public class ODSBufferedReader extends BufferedReader {

	private static final Logger logger = LoggerFactory.getLogger(ODSBufferedReader.class);

	private SpreadSheet workbook;
	private int currentSheet;
	private int currentRow;
	private int savedSheet;
	private int savedRow;

	/**
	 * @throws Exception
	 * @throws IOException
	 * @throws BiffException
	 * 
	 */
	public ODSBufferedReader(final File file) throws Exception {
		super(new InputStreamReader(new FileInputStream(file)));
		try {
			this.workbook = SpreadSheet.createFromFile(file);
			this.currentSheet = 0;
			this.currentRow = 0;
		} catch (Exception exception) {
			throw new Exception("JOpenDocumnent error.", exception);
		}
	}

	/**
	 * 
	 * @throws IOException
	 */
	@Override
	public void close() throws IOException {
		super.close();
	}

	/**
	 * 
	 */
	@Override
	public void mark(final int size) {
		this.savedSheet = this.currentSheet;
		this.savedRow = this.currentRow;
	}

	/**
	 * 
	 */
	@Override
	public boolean markSupported() {
		boolean result;

		result = true;

		//
		return result;
	}

	/**
	 * @throws IOException
	 * 
	 */
	@Override
	public String readLine() throws IOException {
		String result;

		//
		boolean ended = false;
		result = null;
		while (!ended) {
			if (this.currentSheet < this.workbook.getSheetCount()) {

				Sheet sheet = this.workbook.getSheet(this.currentSheet);

				if (this.currentRow < sheet.getRowCount()) {
					//
					ended = true;
					result = toTXTLine(sheet, this.currentRow);
					this.currentRow += 1;
				} else {
					this.currentSheet += 1;
					this.currentRow = 0;
				}
			} else {
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public void reset() {
		this.currentSheet = this.savedSheet;
		this.currentRow = this.savedRow;
	}

	/**
	 * 
	 * @param cell
	 * @return
	 */
	public static String toTXTLine(final Sheet sheet, final int currentRow) {
		String result;

		//
		StringBuffer buffer = new StringBuffer(1024);
		for (int cellIndex = 0; cellIndex < sheet.getColumnCount(); cellIndex++) {
			if (buffer.length() != 0) {
				buffer.append("\t");
			}

			String cellContent;
			if (sheet.getValueAt(cellIndex, currentRow) == null) {
				//
				cellContent = "";

			} else {
				//
				cellContent = sheet.getValueAt(cellIndex, currentRow).toString();
			}

			// Remove special character from the cell.
			if (StringUtils.isNotBlank(cellContent)) {
				//
				cellContent = cellContent.replaceAll("[\r\n\t]+", "");

				// In some cases, ODS cell contains id as double (132.0).
				// It is required to clean it for IURTXTFile.
				if (cellContent.endsWith(".0")) {
					//
					cellContent = cellContent.substring(0, cellContent.length() - 2);
				}
			}

			buffer.append(cellContent);
		}
		result = buffer.toString();

		//
		return result;
	}
}
