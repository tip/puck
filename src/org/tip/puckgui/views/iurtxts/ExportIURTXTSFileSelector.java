package org.tip.puckgui.views.iurtxts;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puckgui.views.ConfirmOverwriteDialog;

/**
 * 
 * @author cpm
 */
public class ExportIURTXTSFileSelector extends JFileChooser {

	private static final long serialVersionUID = 3969592136506847881L;

	private static final Logger logger = LoggerFactory.getLogger(ExportIURTXTSFileSelector.class);

	private File sourceFile;

	/**
	 * 
	 */
	public ExportIURTXTSFileSelector(final File sourceFile) {
		super();

		this.sourceFile = sourceFile;

		//
		File file;
		if ((sourceFile == null) || (StringUtils.isBlank(sourceFile.getAbsolutePath()))) {
			//
			file = null;

		} else {
			//
			file = sourceFile.getParentFile();
		}
		setCurrentDirectory(file);

		//
		setDialogTitle("Export to IURTXTS format");
		setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		setAcceptAllFileFilterUsed(false);
		setApproveButtonText("Export");
		setDialogType(CUSTOM_DIALOG);
	}

	/**
	 * 
	 */
	@Override
	public void approveSelection() {
		//
		File targetFile = getSelectedFile();

		logger.debug("getCurrentDirectory(): {}", getCurrentDirectory());
		logger.debug("selectedFile={}", targetFile);

		//
		File oneFile = new File(targetFile, FilenameUtils.getBaseName(this.sourceFile.getName()) + "-01-individuals.iurs.txt");

		logger.debug("oneFile={}", oneFile);

		if (oneFile.exists()) {
			//
			if (ConfirmOverwriteDialog.showDialog(null)) {
				//
				logger.debug("Overwrite");
				super.approveSelection();

			} else {
				//
				logger.debug("Cancel overwrite");
			}
		} else {
			//
			super.approveSelection();
		}
	}

	/**
	 * 
	 */
	@Override
	public void cancelSelection() {
		//
		logger.debug("Cancel selection.");
		super.cancelSelection();
	}

	/**
	 * 
	 */
	@Override
	public void setSelectedFile(final File file) {
		//
		super.setSelectedFile(file);

		System.out.println("==== SET SELECTED FILE=================");
		System.out.println("SELECED FILE " + file);
	}

	/**
	 * 
	 */
	@Override
	public void setVisible(final boolean visible) {
		//
		super.setVisible(visible);

		if (!visible) {
			//
			resetChoosableFileFilters();
		}
	}

	/**
	 * This method is the main one of the selector.
	 * 
	 * @param parent
	 * @return
	 */
	public static File showSelectorDialog(final Component parent, final File targetFile) {
		File result;

		//
		ExportIURTXTSFileSelector selector = new ExportIURTXTSFileSelector(targetFile);

		//
		if (selector.showDialog(parent, null) == JFileChooser.APPROVE_OPTION) {
			//
			logger.debug("getCurrentDirectory(): {}", selector.getCurrentDirectory());
			logger.debug("getSelectedFile() : {}", selector.getSelectedFile());
			result = selector.getSelectedFile();

		} else {
			//
			result = null;
		}

		//
		return result;
	}
}
