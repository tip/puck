package org.tip.puck.geo.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.geo.Coordinate2;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.geo.Geography;
import org.tip.puck.geo.Geography;
import org.tip.puck.geo.Place;
import org.tip.puck.geo.Place;
import org.tip.puck.geo.Places;
import org.tip.puck.io.iur.IURTXTFamilyLine;
import org.tip.puck.io.iur.IURTXTIndividualLine;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Attributes;
import org.tip.puck.util.LogHelper;

import com.vividsolutions.jts.geom.Coordinate;

import fr.devinsy.util.StringList;

/**
 * This class represents a TXT File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class GEOTXTFile {
	private static final Logger logger = LoggerFactory.getLogger(GEOTXTFile.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	public static final int MAX_LINE_SIZE = 2048;

	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Geography load(final File file) throws PuckException {
		Geography result;

		result = load(file, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Geography load(final File file, final String charsetName) throws PuckException {
		Geography result;

		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), charsetName));
			result = readGeography(in);

			result.setLabel(file.getName());

		} catch (UnsupportedEncodingException exception) {
			//
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");

		} catch (FileNotFoundException exception) {
			//
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");

		} finally {
			//
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}

	/**
	 * Reads a net from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static Geography readGeography(final BufferedReader in) throws PuckException {
		Geography result;

		try {
			// Check data type.
			in.mark(MAX_LINE_SIZE);
			GEOTXTLabelsLine titleLine = readLabelsLine(in);

			// logger.debug("Labels=" + LogHelper.toString(labelsLine));

			if ((titleLine == null) || (titleLine.size() != 1) || (!titleLine.get(0).equalsIgnoreCase("GEOGRAPHY"))) {
				//
				in.reset();
				result = null;

			} else {
				logger.debug("Geography loading...");

				//
				result = new Geography();

				//
				readHeaderAttributes(result.attributes(), in);
				
				GEOTXTLabelsLine labelsLine = readLabelsLine(in);
				
				if (labelsLine.contains("CONTINENT")){
					
					readPlaces2(result,in, labelsLine);
					
				} else {
					
					readPlaces(result, in, labelsLine);
				}
				
			}
		} catch (final IOException exception) {
			//
			throw PuckExceptions.IO_ERROR.create(exception, "Reading labels line.");
		}

		//
		return result;
	}

	/**
	 * Reads a net from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static void readHeaderAttributes(final Attributes target, final BufferedReader in) throws PuckException {
		logger.debug("Read header attributes.");

		try {

			boolean ended = false;
			while (!ended) {
				//
				in.mark(MAX_LINE_SIZE);
				String line = readNotEmptyLine(in);

				if (line == null) {
					//
					ended = true;

				} else {
					//
					String[] tokens = line.split("\\t");

					if (tokens.length > 2) {
						//
						ended = true;
						in.reset();

					} else if (StringUtils.isNotBlank(tokens[0])) {
						//
						target.put(tokens[0], tokens[1]);
					}
				}
			}
		} catch (final IOException exception) {
			//
			throw PuckExceptions.IO_ERROR.create(exception, "Reading labels line.");
		}
	}

	/**
	 * Reads a line of labels from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of labels or null.
	 * 
	 * @throws PuckException
	 */
	public static GEOTXTLabelsLine readLabelsLine(final BufferedReader in) throws PuckException {
		GEOTXTLabelsLine result;

		//
		String line = readNotEmptyLine(in);

		if (line == null) {
			//
			result = null;
		} else {
			//
			String[] tokens = line.split("\\t");

			//
			result = new GEOTXTLabelsLine();
			for (String token : tokens) {
				result.add(token);
			}
		}

		//
		return result;
	}

	/**
	 * Reads a not empty line from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a not empty line or null.
	 * 
	 * @throws PuckException
	 */
	public static String readNotEmptyLine(final BufferedReader in) throws PuckException {
		String result;

		try {
			boolean ended = false;
			result = null;
			while (!ended) {
				//
				String line = in.readLine();

				if (line == null) {
					ended = true;
					result = null;
				} else if (StringUtils.isNotBlank(line)) {
					ended = true;
					result = line;
				}
			}
		} catch (final IOException exception) {
			//
			throw PuckExceptions.IO_ERROR.create(exception, "Reading line.");
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @return
	 * @throws PuckException
	 */
	public static void readPlaces(final Geography target, final BufferedReader in, GEOTXTLabelsLine labelsLine) throws PuckException {

		//
		if (labelsLine != null) {

			boolean ended = false;
			while (!ended) {
				//
				GEOTXTLabelsLine valuesLine = readLabelsLine(in);

				if (valuesLine == null) {
					//
					ended = true;
				} else {
					//
					Place place = new Place(null);

					//
					for (int valueCount = 0; valueCount < valuesLine.size(); valueCount++) {
						//
						String label = labelsLine.get(valueCount);
						String value = valuesLine.get(valueCount);

						//
						if ((StringUtils.equalsIgnoreCase(label, "TOPONYM")) || (StringUtils.equalsIgnoreCase(label, "NAME"))) {
							//
							place.setToponym(value);

						} else if (StringUtils.equalsIgnoreCase(label, "ALTERNATES")) {
							//
							for (String homonym : value.split("[;]")) {
								//
								if (StringUtils.isNotBlank(homonym)) {

									target.addHomonym(place, homonym);
									place.getHomonyms().add(homonym);
								}
							}

						} else if (StringUtils.equalsIgnoreCase(label, "LATITUDE")) {
							//
							if (Coordinate2.isCoordinateValue(value)) {
								//
								try {
									//
									place.setLatitude(Double.parseDouble(value));

								} catch (Exception exception) {
									//
									System.out.println(exception.getMessage());
									throw PuckExceptions.IO_ERROR.create(exception, "Latitude format error [" + value + "]");
								}
							}

						} else if (StringUtils.equalsIgnoreCase(label, "LONGITUDE")) {
							//
							if (Coordinate2.isCoordinateValue(value)) {
								//
								try {
									//
									place.setLongitude(Double.parseDouble(value));

								} catch (Exception exception) {
									//
									throw PuckExceptions.IO_ERROR.create(exception, "Latitude format error [" + value + "]");
								}
							}

						} else if (StringUtils.equalsIgnoreCase(label, "ELEVATION")) {
							//
							if (Coordinate2.isCoordinateValue(value)) {
								//
								try {
									//
									place.setElevation(Double.parseDouble(value));

								} catch (Exception exception) {
									//
									throw PuckExceptions.IO_ERROR.create(exception, "elevation format error [" + value + "]");
								}
							}

						} else if (StringUtils.equalsIgnoreCase(label, "LEVEL")) {
							//
							if (StringUtils.isNotBlank(value)) {
								//
								try {
									//
									place.setGeoLevel(GeoLevel.valueOf(value.toUpperCase()));

								} catch (NumberFormatException exception) {
									//
									throw PuckExceptions.IO_ERROR.create(exception, "Level format error [{}].", value);
								}
							}

						} else if (StringUtils.equalsIgnoreCase(label, "ABOVE")) {
							//
							if (StringUtils.isNotBlank(value)) {
								//
								place.setSuperiorToponym(value);
							}

						} else if (StringUtils.equalsIgnoreCase(label, "EXTRADATA")) {
							//
							if (StringUtils.isNotBlank(value)) {
								//
								place.setExtraData(value);
							}

						} else if (StringUtils.equalsIgnoreCase(label, "COMMENT")) {
							//
							if (StringUtils.isNotBlank(value)) {
								//
								place.setComment(value);
							}
						} else {

							logger.debug("UNKNOWN LABEL [{}]", label);
						}
					}

					//
					if (StringUtils.isNotBlank(place.getToponym())) {
						//
						target.addPlace(place);
					}
				}
			}
		}
		
		for (Place place : target.getPlaces()){
			if (place.getSuperiorToponym()!=null){
				place.setSuperiorPlace(target.get(place.getSuperiorToponym()));
			}
		}

	}
	
	public static void readPlaces2(final Geography target, final BufferedReader in, GEOTXTLabelsLine labelsLine) throws PuckException {
		
		Place world = new Place(GeoLevel.INTERCONTINENTAL,"World",null);
		target.addPlace(world);
					
		boolean ended = false;
		while (!ended) {
			//
			try {
				//
				in.mark(MAX_LINE_SIZE);
				GEOTXTLabelsLine valuesLine = readLabelsLine(in);

				if (valuesLine == null) {
					//
					ended = true;
					
				} else {
					
					Place place = null;
					Place supPlace = world;
					
					for (int valueCount = 0; valueCount < 7; valueCount++) {
						//
						GeoLevel level = GeoLevel.valueOf(labelsLine.get(valueCount));
						String shortName = valuesLine.get(valueCount);
						String toponym = Place.getToponym(shortName, level, supPlace);
																		
						if (!StringUtils.isBlank(shortName)){
							place = target.get(level,toponym);
							if (place == null){
								place = new Place(level,shortName,supPlace);
								target.addPlace(place);
							}
							supPlace = place;
							place = null;
						}
					}
					
					target.addHomonym(supPlace, valuesLine.get(7));
					target.addPlace(supPlace);
					
//					target.put(tokens[7], supPlace);
										
					if (valuesLine.size()==10){
						try {
							supPlace.setCoordinate2(new Coordinate2(Double.parseDouble(valuesLine.get(9)), Double.parseDouble(valuesLine.get(8))));
						} catch (NumberFormatException e) {
							throw PuckExceptions.BAD_FILE_FORMAT.create(e, valuesLine.get(7)+" Latitude "+valuesLine.get(8)+" Longitude"+valuesLine.get(9));
						}
					}
					supPlace = world;
				}
			} catch (final IOException exception) {
				throw PuckExceptions.IO_ERROR.create(exception, "Reading individual line ");
			}
		}
		//
//		target.updateDistricts();
		//
	}



/*	public static void save(final File file, final Geography source) throws PuckException {

		PrintWriter out = null;
		try {
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));

			writeGeography(out, source);

		} catch (UnsupportedEncodingException exception) {

			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");

		} catch (FileNotFoundException exception) {

			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");

		} finally {
			//
			IOUtils.closeQuietly(out);
		}
	}*/


	public static void save(final File file, final Geography source) throws PuckException {

		PrintWriter out = null;
		try {
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));

			writeGeography(out, source);

		} catch (UnsupportedEncodingException exception) {

			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");

		} catch (FileNotFoundException exception) {

			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");

		} finally {
			//
			IOUtils.closeQuietly(out);
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final GEOTXTLabelsLine source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			StringList buffer = new StringList();
			buffer.append("[");
			for (String label : source) {
				buffer.append(label);
				buffer.append(",");
			}
			buffer.removeLast();
			buffer.append("]");

			//
			result = buffer.toString();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static String toString(final IURTXTFamilyLine source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			result = String.format("[id=%d][status=%c][fatherId=%d][motherId=%d][childIds=%s][attributeValues=%s]", source.getId(), source.getStatus(),
					source.getFatherId(), source.getMotherId(), source.getChildIds(), LogHelper.toString(source.attributeValues()));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static String toString(final IURTXTIndividualLine source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			result = String.format("[id=%d][name=%s][gender=%c][attributeValues=%s]", source.getId(), source.getName(), source.getGender(),
					LogHelper.toString(source.attributeValues()));
		}

		//
		return result;
	}


/*	public static void writeGeography(final PrintWriter out, final Geography source) {
		//
		out.println("GEOGRAPHY");
		out.println(" ");

		if (source != null) {

			StringList labelLine = new StringList();
			labelLine.add("TOPONYM");
			labelLine.add("ALTERNATES");
			labelLine.add("LATITUDE");
			labelLine.add("LONGITUDE");
			labelLine.add("ELEVATION");
			labelLine.add("LEVEL");
			labelLine.add("ABOVE");
			labelLine.add("EXTRADATA");
			labelLine.add("COMMENT");

			out.println(labelLine.toStringSeparatedBy("\t"));

			StringList valueLines = new StringList();
			Map<String, StringList> homonymLists = source.homonymLists();

			for (Place place : source.getPlaces()) {

				String sup = "";
				if (place.getSup() != null) {
					sup = place.getSup().getToponym();
				}

				String homonyms = "";
				StringList homonymList = homonymLists.get(place.getId());
				if (homonymList != null) {
					for (String homonym : homonymList) {
						if (homonyms.length() != 0) {
							homonyms += ";";
						}
						homonyms += homonym;
					}
				}

				String latitude = "";
				String longitude = "";

				Coordinate coordinate = place.getCoordinate();
				if (coordinate != null) {
					longitude = new Double(coordinate.x).toString();
					latitude = new Double(coordinate.y).toString();
				}

				valueLines
						.append(place.getToponym() + "\t" + homonyms + "\t" + latitude + "\t" + longitude + "\t\t" + place.getLevel() + "\t" + sup + "\t\t\t");
			}
			valueLines.sort();

			for (String valueLine : valueLines) {
				out.println(valueLine);
			}

		}
	}*/

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 * @throws PuckException
	 */
	public static void writeGeography(final PrintWriter out, final Geography source) {

		if (source != null) {
			//
			out.println("GEOGRAPHY");
			out.println(" ");

			writeHeaderAttributes(out, source.attributes());

			writePlaces(out, source.getPlaces());
		}
	}

	/**
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 */
	public static void writeHeaderAttributes(final PrintWriter out, final Attributes source) {

		if ((source != null) && (!source.isEmpty())) {

			for (Attribute attribute : source) {

				out.print(attribute.getLabel());
				out.print("\t");
				out.println(attribute.getValue());
			}

			out.println(" ");
		}
	}

	/**
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 */
	public static void writePlaces(final PrintWriter out, final Places source) {

		if ((source != null) && (!source.isEmpty())) {

			StringList labelLine = new StringList();
			labelLine.add("TOPONYM");
			labelLine.add("ALTERNATES");
			labelLine.add("LATITUDE");
			labelLine.add("LONGITUDE");
			labelLine.add("ELEVATION");
			labelLine.add("LEVEL");
			labelLine.add("ABOVE");
			labelLine.add("EXTRADATA");
			labelLine.add("COMMENT");

			out.println(labelLine.toStringSeparatedBy("\t"));

			for (Place place : source) {

				StringList valueLine = new StringList();

				valueLine.append(place.getToponym());
				valueLine.append(place.getHomonyms().toStringSeparatedBy(";"));

				if (place.getLatitude() == null) {
					valueLine.append("");
				} else {
					valueLine.append(Double.toString(place.getLatitude()));
				}

				if (place.getLongitude() == null) {
					valueLine.append("");
				} else {
					valueLine.append(Double.toString(place.getLongitude()));
				}

				if (place.getElevation() == null) {
					valueLine.append("");
				} else {
					valueLine.append(Double.toString(place.getElevation()));
				}

				if (place.getGeoLevel() == null) {
					valueLine.append("");
				} else {
					valueLine.append(place.getGeoLevel().toString());
				}

				if (place.getSuperiorPlace() == null) {
					valueLine.append("");
				} else {
					valueLine.append(place.getSuperiorPlace().toString());
				}

				valueLine.append(StringUtils.defaultString(place.getExtraData()));
				valueLine.append(StringUtils.defaultString(place.getComment()));

				out.println(valueLine.toStringSeparatedBy("\t"));
			}

			out.println();
		}
	}
}
