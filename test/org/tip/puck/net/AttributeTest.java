package org.tip.puck.net;

import org.fest.assertions.Assertions;
import org.junit.Test;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class AttributeTest {
	/**
     */
	@Test
	public void testHasEqualValue00() {

		Assertions.assertThat(new Attribute(null, null).hasEqualValue(new Attribute(null, null))).isTrue();
		Assertions.assertThat(new Attribute(null, null).hasEqualValue(new Attribute(null, "victor"))).isFalse();
		Assertions.assertThat(new Attribute(null, null).hasEqualValue(new Attribute("lima", null))).isFalse();
		Assertions.assertThat(new Attribute(null, null).hasEqualValue(new Attribute("lima", "victor"))).isFalse();

		Assertions.assertThat(new Attribute(null, "victor").hasEqualValue(new Attribute(null, null))).isFalse();
		Assertions.assertThat(new Attribute(null, "victor").hasEqualValue(new Attribute(null, "victor"))).isTrue();
		Assertions.assertThat(new Attribute(null, "victor").hasEqualValue(new Attribute(null, "VICTOR"))).isFalse();
		Assertions.assertThat(new Attribute(null, "victor").hasEqualValue(new Attribute("lima", null))).isFalse();
		Assertions.assertThat(new Attribute(null, "victor").hasEqualValue(new Attribute("lima", "victor"))).isFalse();
		Assertions.assertThat(new Attribute(null, "victor").hasEqualValue(new Attribute("LIMA", "VICTOR"))).isFalse();

		Assertions.assertThat(new Attribute("lima", null).hasEqualValue(new Attribute(null, null))).isFalse();
		Assertions.assertThat(new Attribute("lima", null).hasEqualValue(new Attribute(null, "victor"))).isFalse();
		Assertions.assertThat(new Attribute("lima", null).hasEqualValue(new Attribute("lima", null))).isTrue();
		Assertions.assertThat(new Attribute("lima", null).hasEqualValue(new Attribute("LIMA", null))).isFalse();
		Assertions.assertThat(new Attribute("lima", null).hasEqualValue(new Attribute("lima", "victor"))).isFalse();
		Assertions.assertThat(new Attribute("lima", null).hasEqualValue(new Attribute("LIMA", "VICTOR"))).isFalse();

		Assertions.assertThat(new Attribute("lima", "victor").hasEqualValue(new Attribute(null, null))).isFalse();
		Assertions.assertThat(new Attribute("lima", "victor").hasEqualValue(new Attribute(null, "victor"))).isFalse();
		Assertions.assertThat(new Attribute("lima", "victor").hasEqualValue(new Attribute(null, "VICTOR"))).isFalse();
		Assertions.assertThat(new Attribute("lima", "victor").hasEqualValue(new Attribute("lima", null))).isFalse();
		Assertions.assertThat(new Attribute("lima", "victor").hasEqualValue(new Attribute("LIMA", null))).isFalse();
		Assertions.assertThat(new Attribute("lima", "victor").hasEqualValue(new Attribute("lima", "victor"))).isTrue();
		Assertions.assertThat(new Attribute("lima", "victor").hasEqualValue(new Attribute("lima", "VICTOR"))).isFalse();
		Assertions.assertThat(new Attribute("lima", "victor").hasEqualValue(new Attribute("LIMA", "victor"))).isFalse();
		Assertions.assertThat(new Attribute("lima", "victor").hasEqualValue(new Attribute("LIMA", "VICTOR"))).isFalse();
	}
}
