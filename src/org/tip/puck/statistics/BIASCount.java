package org.tip.puck.statistics;

/**
 * 
 * @author TIP
 */
public class BIASCount extends FiliationCount {

	private double coo;

	/**
	 * 
	 */
	public BIASCount() {
		super();
	}

	/**
	 * 
	 */
	public BIASCount(final double coo, final double agnaticValue, final double uterineValue, final double cognaticValue) {
		super(agnaticValue, uterineValue, cognaticValue);
		this.coo = coo;
	}

	/**
	 * 
	 * @return
	 */
	public BIASCount add(final BIASCount source) {
		BIASCount result;

		this.coo += source.getCoo();
		super.add(source);

		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double addCoo(final double value) {
		double result;

		this.coo += value;
		result = this.coo;

		//
		return result;
	}

	public double getCoo() {
		return coo;
	}

	public void setCoo(final double coo) {
		this.coo = coo;
	}
}
