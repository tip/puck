package org.tip.puck.sequences;

import java.util.ArrayList;
import java.util.List;

import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individualizable;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Populatable;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.workers.SequenceMaker;
import org.tip.puck.sequences.workers.SequenceCriteria;
import org.tip.puck.util.NumberablesHashMap;

public class Sequences<S extends Sequenceable<E>,E> extends NumberablesHashMap<S> implements Sequenceables<S,E>{
	
	String label;

/*	String dateLabel;
	String idLabel;
	String relationModelName;
	int[] maxDepths;
	String chainClassification;
	List<String> relationModelNames;*/
	
	public Sequences(){
		
/*		relationModelName = criteria.getRelationModelName();
		dateLabel = criteria.getDateLabel();
		idLabel = criteria.getLocalUnitLabel();
		maxDepths = ToolBox.stringsToInts(criteria.getPattern());
		chainClassification = criteria.getChainClassification();
		relationModelNames = criteria.getRelationModelNames();*/

	}
	
	
	public String getLabel() {
		return label;
	}
	
	
	public boolean isPopulatable(){
		boolean result;
		
		result = false;
		
		for (S sequence : this){

			for (E station : sequence.getStations().values()){
				
				if (station != null){

					if (station instanceof Populatable) {
						result = true;
					}
					//
					return result;
				}
			}
		}
		//
		return result;
	}
	
	
	public Sequences(List<S> sequences){
		for (S sequence : sequences){
			add(sequence);
		}
	}
	
	public Individuals getIndividuals(Segmentation segmentation) throws PuckException{
		Individuals result;
		
		result = new Individuals();
		
		for (S sequence : this){
			
			if (sequence instanceof Populatable){

				for (Individual individual : ((Populatable)sequence).getIndividuals()){
					if (segmentation.getCurrentIndividuals().contains(individual)){
						result.add(individual);
					}
				}
				
			} else {
				
				throw PuckExceptions.INVALID_PARAMETER.create(sequence+" is not populatable");
			}
		}
		//
		return result;
	}
	
	
	public Individuals getIndividuals() throws PuckException{
		Individuals result;
		
		result = new Individuals();
		
		for (S sequence : this){

			if (sequence instanceof Populatable){

				for (Individual individual : ((Populatable)sequence).getIndividuals()){
					result.add(individual);
				}
				
			} else {
				
				throw PuckExceptions.INVALID_PARAMETER.create(sequence+" is not populatable");
			}
		}

		//
		return result;
	}

	
	public EgoSequences getIndividualStateSequences(Segmentation segmentation, SequenceCriteria criteria) throws PuckException{
		EgoSequences result;
		
		result = new EgoSequences();
		
		for (Individual member : getIndividuals(segmentation).toSortedList()){
			
			result.put(SequenceMaker.createPersonalStateSequence(member, segmentation, criteria));
		}
		//
		return result;
	}
	
	public List<E> getStations (Ordinal time){
		List<E> result;
		
		result = new ArrayList<E>();
		
		for (S sequence : this){
			result.add(sequence.getStation(time));
		}
		//
		return result;
	}

	
	public void addRenumbered(S sequence){
		sequence.setId(size());
		add(sequence);
	}


	public Individuals getAllIndividuals(List<Ordinal> times) throws PuckException {
		Individuals result;
		
		result = new Individuals();
		
		for (S sequence : this){

			if (sequence instanceof Populatable){

				for (Individual individual : ((Populatable)sequence).getAllIndividuals(times)){
					result.add(individual);
				}
				
			} else {
				
				throw PuckExceptions.INVALID_PARAMETER.create(sequence+" is not populatable");
			}
		}
		//
		return result;
	}
	
	
	



}
