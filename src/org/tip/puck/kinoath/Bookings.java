package org.tip.puck.kinoath;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;

/**
 * This class defines a booking list with index of all individuals booked. A pop
 * method allows to exit booking. Even if all booking are popped, booked
 * individuals are again knowable because of the special booked index.
 * 
 * 
 * @author TIP
 */
public class Bookings extends ArrayList<Booking> {

	private static final long serialVersionUID = -6652278468059678661L;
	private static final Logger logger = LoggerFactory.getLogger(Bookings.class);
	private Individuals booked;

	/**
	 * Initializes an newly created {@code Bookings} object so that it
	 * represents an empty booking list with an empty booked individual list.
	 */
	public Bookings() {
		super();

		this.booked = new Individuals();
	}

	/**
	 * Add a new booking to the booking list and sign then individual of the
	 * booking in the booked individual list.
	 * 
	 * If individual of the booking already is booked then nothing done: no
	 * booking add.
	 */
	@Override
	public boolean add(final Booking booking) {
		boolean result;

		if (isBooked(booking.getIndividual())) {
			//
			result = false;

		} else {
			//
			result = super.add(booking);

			this.booked.add(booking.getIndividual());
		}

		//
		return result;
	}

	/**
	 * Returns the individual booked list.
	 * 
	 * @return
	 */
	public Individuals booked() {
		Individuals result;

		result = this.booked;

		//
		return result;
	}

	/**
	 * Returns true if the individual already is booked.
	 * 
	 * @param individual
	 * @return
	 */
	public boolean isBooked(final Individual individual) {
		boolean result;

		result = this.booked.contains(individual);

		//
		return result;
	}

	/**
	 * Returns the last booking of the list and remove it from the list.
	 * 
	 * @return
	 */
	public Booking pop() {
		Booking result;

		result = remove(size() - 1);

		//
		return result;
	}
}
