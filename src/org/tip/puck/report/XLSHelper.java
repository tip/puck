package org.tip.puck.report;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import jxl.write.Label;
import jxl.write.WritableImage;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents a report File writer.
 * 
 * @author TIP
 */
public class XLSHelper {

	private static final Logger logger = LoggerFactory.getLogger(XLSHelper.class);

	private WritableWorkbook workbook;
	private int currentSheet;
	private int currentRow;
	private int currentColumn;

	/**
	 * 
	 */
	public XLSHelper(final WritableWorkbook source) {

		this.workbook = source;
		this.currentSheet = -1;
		this.currentRow = 0;
		this.currentColumn = 0;
	}

	/**
	 * 
	 * @param source
	 * @throws IndexOutOfBoundsException
	 * @throws WriteException
	 * @throws RowsExceededException
	 */
	public XLSHelper append(final long source) throws RowsExceededException, WriteException, IndexOutOfBoundsException {
		//
		this.workbook.getSheet(this.currentSheet).addCell(new Label(this.currentColumn, this.currentRow, String.valueOf(source)));
		this.currentColumn += 1;

		//
		return this;
	}

	/**
	 * 
	 * @param source
	 * @throws IndexOutOfBoundsException
	 * @throws WriteException
	 * @throws RowsExceededException
	 */
	public XLSHelper append(final String source) throws RowsExceededException, WriteException, IndexOutOfBoundsException {
		//
		if (source == null) {
			this.currentColumn += 1;
		} else {
			String lines[] = source.split("\n");
			for (String line : lines) {
				String tokens[] = line.split("\t");
				for (String token : tokens) {
					this.workbook.getSheet(this.currentSheet).addCell(new Label(this.currentColumn, this.currentRow, token));
					this.currentColumn += 1;
				}
				if (lines.length > 1) {
					this.currentRow += 1;
					this.currentColumn = 0;
				}
			}
		}

		//
		return this;
	}

	/**
	 * 
	 */
	public XLSHelper appendln() {
		this.currentRow += 1;
		this.currentColumn = 0;

		//
		return this;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws RowsExceededException
	 * @throws WriteException
	 * @throws IndexOutOfBoundsException
	 */
	public XLSHelper appendln(final BufferedImage source) throws RowsExceededException, WriteException, IndexOutOfBoundsException {

		try {
			int columnSize = 5;
			int rowSize = (int) Math.round(columnSize * 4 / 1.6);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(source, "PNG", baos);

			WritableImage image = new WritableImage(1, this.currentRow, columnSize, rowSize, baos.toByteArray());

			this.workbook.getSheet(this.currentSheet).addImage(image);

			this.currentRow += rowSize;
		} catch (IOException e) {
			e.printStackTrace();

			appendln("Sorry, error including image.");
		}

		//
		return this;
	}

	/**
	 * 
	 * @param source
	 * @throws RowsExceededException
	 * @throws WriteException
	 * @throws IndexOutOfBoundsException
	 */
	public XLSHelper appendln(final long source) throws RowsExceededException, WriteException, IndexOutOfBoundsException {
		//
		append(source);
		appendln();

		//
		return this;
	}

	/**
	 * 
	 * @param source
	 * @throws RowsExceededException
	 * @throws WriteException
	 * @throws IndexOutOfBoundsException
	 */
	public XLSHelper appendln(final String source) throws RowsExceededException, WriteException, IndexOutOfBoundsException {
		//
		append(source);
		appendln();

		//
		return this;
	}

	/**
	 * 
	 */
	public void newPage() {

		newPage(String.valueOf(this.currentSheet));
	}

	/**
	 * 
	 */
	public void newPage(final String name) {

		this.currentSheet += 1;
		this.workbook.createSheet(name, this.currentSheet);
		this.currentRow = 0;
		this.currentColumn = 0;
	}
}
