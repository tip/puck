package org.tip.puck.statistics;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckManager;
import org.tip.puck.census.chains.Chain;
import org.tip.puck.census.workers.CensusCriteria;
import org.tip.puck.census.workers.CensusReporter;
import org.tip.puck.census.workers.CircuitFinder;
import org.tip.puck.geo.Geography;
import org.tip.puck.geo.Place;
import org.tip.puck.geo.Places;
import org.tip.puck.graphs.Node;
import org.tip.puck.io.paj.PAJFile;
import org.tip.puck.net.Family;
import org.tip.puck.net.FamilyComparator;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.IndividualComparator;
import org.tip.puck.net.IndividualComparator.Sorting;
import org.tip.puck.net.Individualizable;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.KinType;
import org.tip.puck.net.Net;
import org.tip.puck.net.workers.AttributeDescriptor;
import org.tip.puck.net.workers.AttributeDescriptors;
import org.tip.puck.net.workers.AttributeValueDescriptor;
import org.tip.puck.net.workers.AttributeValueDescriptorList;
import org.tip.puck.net.workers.AttributeValueDescriptors;
import org.tip.puck.net.workers.AttributeWorker;
import org.tip.puck.net.workers.ControlReporter;
import org.tip.puck.net.workers.FamilyValuator;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Clusters;
import org.tip.puck.partitions.MultiPartition;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionCriteria.CumulationType;
import org.tip.puck.partitions.PartitionCriteria.SizeFilter;
import org.tip.puck.partitions.PartitionCriteria.ValueFilter;
import org.tip.puck.partitions.PartitionMaker;
import org.tip.puck.report.Report;
import org.tip.puck.report.ReportAttributes;
import org.tip.puck.report.ReportChart;
import org.tip.puck.report.ReportChart.GraphType;
import org.tip.puck.report.ReportChart.LogarithmType;
import org.tip.puck.report.ReportRawData;
import org.tip.puck.report.ReportTable;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.statistics.StatisticsWorker.Indicator;
import org.tip.puck.util.Chronometer;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.PuckUtils;
import org.tip.puck.util.ToolBox;
import org.tip.puck.util.Trafo;
import org.tip.puck.util.Value;
import org.tip.puck.util.Values;
import org.tip.puck.workers.NodeReferentValuator;

import fr.devinsy.util.StringList;

/**
 * 
 * 
 * @author TIP
 */
public class StatisticsReporter {

	private static final Logger logger = LoggerFactory.getLogger(StatisticsReporter.class);

	/**
	 * 
	 * @param partitionSequences
	 * @return
	 * @throws PuckException
	 */
	public static <E> ReportChart createArrayChart(final String title, final double[][] array, final String[] lineTitles) {
		ReportChart result;

		// == Build Report Chart.
		result = new ReportChart(title, GraphType.LINES);

		//
		{
			for (int j = 0; j < array[0].length; j++) {
				result.setLineTitle(lineTitles[j], j);

				for (int i = 0; i < array.length; i++) {
					result.setHeader(i + "", i);
					result.setValue(array[i][j], j, i);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 * @throws PuckException
	 */
	public static StringList createAttributeDescriptorGroupedList(final AttributeDescriptors source) {
		StringList result;

		if (source == null) {
			//
			result = null;

		} else {
			//
			List<String> relationModelNames = source.relationModelNames();

			//
			result = new StringList(relationModelNames.size() + 3 + 1);

			//
			for (AttributeDescriptor.Scope scope : AttributeDescriptor.CanonicalScopes) {
				//
				AttributeDescriptors descriptors = source.findByScope(scope);

				//
				result.append(scope.toString()).append(" (").append(descriptors.size()).append("): ");

				//
				StringList labelList = new StringList(descriptors.size());
				labelList.addAll(descriptors.labelsSorted());
				String labels = labelList.toStringWithFrenchCommas();
				result.appendln(labels);
			}

			//
			for (String name : relationModelNames) {
				//
				AttributeDescriptors descriptors = source.findByRelationModelName(name);

				//
				result.append(name).append(" (").append(descriptors.size()).append("): ");

				//
				StringList labelList = new StringList(descriptors.size());
				labelList.addAll(descriptors.labelsSorted());
				String labels = labelList.toStringWithFrenchCommas();
				result.appendln(labels);
			}

			//
			{
				AttributeDescriptor.Scope scope = AttributeDescriptor.Scope.ACTORS;
				AttributeDescriptors descriptors = source.findByScope(scope);

				result.append(scope.toString()).append(" (").append(descriptors.size()).append("): ");

				StringList labelList = new StringList(descriptors.size());
				labelList.addAll(descriptors.labelsSorted());
				String labels = labelList.toStringWithFrenchCommas();
				result.appendln(labels);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 * @throws PuckException
	 */
	public static ReportTable createAttributeDescriptorGroupedTable(final AttributeDescriptors source) {
		ReportTable result;

		if (source == null) {
			//
			result = null;
		} else {
			//
			List<String> relationModelNames = source.relationModelNames();

			//
			result = new ReportTable(relationModelNames.size() + 3 + 1, 3);
			result.setTitle("Descriptors");

			//
			result.set(0, 0, "Scope");
			result.set(0, 1, "Count");
			result.set(0, 2, "Labels");

			//
			int index = 1;
			for (AttributeDescriptor.Scope scope : AttributeDescriptor.CanonicalScopes) {
				//
				AttributeDescriptors descriptors = source.findByScope(scope);

				//
				result.set(index, 0, scope.toString());
				result.set(index, 1, descriptors.size());

				//
				StringList labelList = new StringList(descriptors.size());
				labelList.addAll(descriptors.labelsSorted());
				String labels = labelList.toStringWithFrenchCommas();
				result.set(index, 2, labels);

				//
				index += 1;
			}

			//
			for (String name : relationModelNames) {
				//
				AttributeDescriptors descriptors = source.findByRelationModelName(name);

				//
				result.set(index, 0, name);
				result.set(index, 1, descriptors.size());

				//
				StringList labelList = new StringList(descriptors.size());
				labelList.addAll(descriptors.labelsSorted());
				String labels = labelList.toStringWithFrenchCommas();
				result.set(index, 2, labels);

				//
				index += 1;
			}

		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 * @throws PuckException
	 */
	public static ReportTable createAttributeDescriptorTable(final AttributeDescriptors source) {
		ReportTable result;

		if (source == null) {
			//
			result = null;
		} else {
			//
			result = new ReportTable(source.size() + 1, 11);
			result.setTitle("Descriptors");

			//
			result.set(0, 0, "Scope");
			result.set(0, 1, "Label");
			result.set(0, 2, "Not set");
			result.set(0, 3, " % ");
			result.set(0, 4, "Set blank");
			result.set(0, 5, " % ");
			result.set(0, 6, "Filled");
			result.set(0, 7, " % ");
			result.set(0, 8, "Set");
			result.set(0, 9, "%");
			result.set(0, 10, "Theorical max.");

			//
			int index = 1;
			for (AttributeDescriptor descriptor : source) {
				//
				result.set(index, 0, descriptor.getRelationName());
				result.set(index, 1, descriptor.getLabel());
				result.set(index, 2, String.valueOf(descriptor.getCountOfNotSet()));
				result.set(index, 3, descriptor.getCovegareOfNotSet());
				result.set(index, 4, String.valueOf(descriptor.getCountOfBlank()));
				result.set(index, 5, descriptor.getCoverageOfBlank());
				result.set(index, 6, String.valueOf(descriptor.getCountOfFilled()));
				result.set(index, 7, descriptor.getCoverageOfFilled());
				result.set(index, 8, String.valueOf(descriptor.getCountOfSet()));
				result.set(index, 9, descriptor.getCoverageOfSet());
				result.set(index, 10, String.valueOf(descriptor.getMax()));

				//
				index += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param partitionCriteria
	 * @param splitCriteria
	 * @return
	 * @throws PuckException
	 */
	public static <E> ReportChart createChart(final String title, final Map<String, Map<String, Double>> map) throws PuckException {
		ReportChart result;

		if (map == null) {
			result = null;
		} else {
			//
			result = new ReportChart(title, GraphType.STACKED_BARS);

			//
			int clusterIndex = 0;
			HashMap<String, Integer> clusterValueToLineIndex = new HashMap<String, Integer>();

			for (String clusterLabel : map.keySet()) {
				//
				result.setHeader(clusterLabel, clusterIndex);

				for (String splitClusterLabel : map.get(clusterLabel).keySet()) {

					Integer lineIndex = clusterValueToLineIndex.get(splitClusterLabel);

					if (lineIndex == null) {
						lineIndex = clusterValueToLineIndex.size();

						clusterValueToLineIndex.put(splitClusterLabel, lineIndex);
						result.setLineTitle(splitClusterLabel, lineIndex);
					}
					result.addValue(map.get(clusterLabel).get(splitClusterLabel), lineIndex);

				}
				//
				clusterIndex += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static ReportChart createCompletenessChart(final FiliationCounts counts) {
		ReportChart result;

		//
		result = new ReportChart("Genealogical Completeness", GraphType.LINES);
		result.setHeadersLegend("Generational Level");
		result.setLinesLegend("% ascendants");
		result.setVerticalMax(100.0);
		result.setIntegerHorizontalUnit(true);

		//
		result.setLineTitle("Overall", 0);
		result.setLineTitle("Agnatic", 1);
		result.setLineTitle("Uterine", 2);

		//
		for (int ascendingIndex = 1; ascendingIndex < counts.size(); ascendingIndex++) {
			FiliationCount count = counts.get(ascendingIndex);
			if (count.isPositive()) {
				result.addValue(ascendingIndex, count.getCognatic(), 0);
				result.addValue(ascendingIndex, count.getAgnatic(), 1);
				result.addValue(ascendingIndex, count.getUterine(), 2);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param individuals
	 * @return
	 * @throws PuckException
	 */
	public static ReportChart createComponentsChart(final Individuals individuals, final int resolution) throws PuckException {
		ReportChart result;

		//
		FiliationCounts counts = StatisticsWorker.components(individuals, resolution);

		//
		result = new ReportChart("Components", GraphType.LINES);
		result.setLogarithmType(LogarithmType.HORIZONTAL);
		result.setHeadersLegend("% Individuals");
		result.setLinesLegend("% Components");

		//
		result.setLineTitle("Agnatic", 0);
		result.setLineTitle("Uterine", 1);

		//
		for (int ascendingIndex = 0; ascendingIndex < resolution; ascendingIndex++) {
			result.addValue(MathUtils.percent(ascendingIndex, resolution), counts.get(ascendingIndex).getAgnatic(), 0);
			result.addValue(MathUtils.percent(ascendingIndex, resolution), counts.get(ascendingIndex).getUterine(), 1);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param counts
	 * @return
	 */
	public static <E> ReportChart createDiversityChart(final Partition<Node<E>>[] source, final int maxPositions) {
		ReportChart result;

		//
		result = new ReportChart("Diversity_" + source[2].getLabel(), GraphType.LINES);
		result.setHeadersLegend("Depth");
		result.setLinesLegend("Diversity");
		result.setVerticalMax(100.0);
		result.setIntegerHorizontalUnit(true);

		//
		result.setLineTitle("All itineraries", 0);
		result.setLineTitle("Male itineraries", 1);
		result.setLineTitle("Female itineraries", 2);

		//
		for (int gender = 0; gender < 2; gender++) {
			List<Cluster<Node<E>>> clusterList = source[gender].getClusters().toListSortedByValue();
			for (int i = 1; i <= clusterList.size(); i++) {
				Cluster<Node<E>> cluster = clusterList.get(i - 1);
				if (cluster != null) {
					int value = cluster.getValue().intValue();
					double nodeSizeSum = 0.;
					for (Node<E> node : cluster.getItems()) {
						Value nodeSize = new NodeReferentValuator<E>().get(node, "SIZE", null);
						if (nodeSize != null) {
							nodeSizeSum += nodeSize.intValue();
						} else {
							System.err.println("Null size value for " + node + " " + node.getReferent());
						}
					}
					double maxSize = Math.min(Math.pow(maxPositions, value), nodeSizeSum);
					double diversity = MathUtils.percent(cluster.size() - 1, maxSize - 1);
					result.setHeader(MathUtils.toString(value), value);
					result.addValue(value, diversity, 0);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param counts
	 * @return
	 */
	public static ReportChart createGenderBIASNetWeightChart(final BIASCounts counts) {
		ReportChart result;

		//
		result = new ReportChart("Gender BIAS (net weight)", GraphType.LINES);
		result.setHeadersLegend("Generational Level");
		result.setLinesLegend("% Individuals");
		result.setVerticalMax(100.0);
		result.setIntegerHorizontalUnit(true);

		//
		result.setLineTitle("Only Uterine Ancestor Known", 0);
		result.setLineTitle("Only Agnatic Ancestor Known", 1);

		//
		for (int countIndex = 1; countIndex < counts.size(); countIndex++) {
			BIASCount count = counts.get(countIndex);
			if (count.isPositive()) {
				result.setHeader(MathUtils.toString(countIndex), countIndex);
				result.addValue(countIndex, count.getUterine(), 0);
				result.addValue(countIndex, count.getAgnatic(), 1);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 * @throws PuckException
	 */
	public static ReportTable createGenderBIASNetWeightTable(final BIASCounts counts) throws PuckException {
		ReportTable result;

		//
		result = new ReportTable(counts.size(), 5);
		result.setTitle("Gender BIAS (net weight)");

		//
		result.set(0, 0, " ");
		result.set(0, 1, "A or U");
		result.set(0, 2, "U not A");
		result.set(0, 3, "A not U");
		result.set(0, 4, "Diff");

		//
		for (int countIndex = 1; countIndex < counts.size(); countIndex++) {
			//
			BIASCount count = counts.get(countIndex);

			// Extract from Net.java Puck 1.0:
			// i
			// c
			// bias[0][i]
			// bias[1][i]
			// Math.round(100.*(bias[0][i]-bias[1][i]))/100.
			result.set(countIndex, 0, countIndex);
			result.set(countIndex, 1, count.getCoo());
			result.set(countIndex, 2, count.getUterine());
			result.set(countIndex, 3, count.getAgnatic());
			result.set(countIndex, 4, MathUtils.roundHundredth(count.getUterine() - count.getAgnatic()));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param counts
	 * @return
	 */
	public static ReportChart createGenderBIASWeightChart(final BIASCounts counts) {
		ReportChart result;

		//
		result = new ReportChart("Gender BIAS (weight)", GraphType.LINES);
		result.setHeadersLegend("Generational Level");
		result.setLinesLegend("% Individuals");
		result.setVerticalMax(100.0);
		result.setIntegerHorizontalUnit(true);

		//
		result.setLineTitle("Uterine Ancestor Known", 0);
		result.setLineTitle("Agnatic Ancestor Known", 1);
		result.setLineTitle("Agn. and Ut. Ancestor Known", 2);

		//
		for (int countIndex = 1; countIndex < counts.size(); countIndex++) {
			BIASCount count = counts.get(countIndex);
			if (count.isPositive()) {
				result.setHeader(MathUtils.toString(countIndex), countIndex);
				result.addValue(countIndex, count.getUterine(), 0);
				result.addValue(countIndex, count.getAgnatic(), 1);
				result.addValue(countIndex, count.getCognatic(), 2);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 * @throws PuckException
	 */
	public static ReportTable createGenderBIASWeightTable(final BIASCounts counts) throws PuckException {
		ReportTable result;

		//
		result = new ReportTable(counts.size(), 6);
		result.setTitle("Gender BIAS (weight)");

		//
		result.set(0, 0, " ");
		result.set(0, 1, "A or U");
		result.set(0, 2, "U");
		result.set(0, 3, "A");
		result.set(0, 4, "A and U");
		result.set(0, 5, "Diff");

		//
		for (int countIndex = 1; countIndex < counts.size(); countIndex++) {
			//
			BIASCount count = counts.get(countIndex);

			// Extract from Net.java Puck 1.0:
			// i
			// c
			// bias[0][i]
			// bias[1][i]
			// bias[2][i]
			// Math.round(100.*(bias[0][i]-bias[1][i]))/100.)
			result.set(countIndex, 0, countIndex);
			result.set(countIndex, 1, count.getCoo());
			result.set(countIndex, 2, count.getUterine());
			result.set(countIndex, 3, count.getAgnatic());
			result.set(countIndex, 4, count.getCognatic());
			result.set(countIndex, 5, MathUtils.roundHundredth(count.getUterine() - count.getAgnatic()));
		}

		//
		return result;
	}

	public static <E> ReportChart createMapChart(final Map<Value, Double[]> map, final int idx, final String label) throws PuckException {
		ReportChart result;

		if (map == null) {
			result = null;
		} else {
			//
			result = new ReportChart(label, GraphType.STACKED_BARS);

			//
			int index = 0;
			for (Value key : map.keySet()) {
				//
				result.setHeader(key.toString(), index);

				//
				result.addValue(map.get(key)[idx], 0);

				//
				index += 1;
			}
		}

		//
		return result;
	}

	public static <E> ReportChart createMapChart(final Map<Value, Double[]> map, final String label, final String[] splitLabels, final GraphType graphType)
			throws PuckException {
		ReportChart result;

		if (map == null) {
			result = null;
		} else {
			//
			result = new ReportChart(label, graphType);
			result.setIntegerHorizontalUnit(true);

			//
			int colIndex = 0;
			HashMap<String, Integer> clusterValueToLineIndex = new HashMap<String, Integer>();

			for (Value key : map.keySet()) {

				String header = null;
				if (key != null) {
					header = key.toString();
				}
				//
				result.setHeader(header, colIndex);
				for (int i = 0; i < splitLabels.length; i++) {
					String genderLabel = splitLabels[i];

					//
					Integer lineIndex = clusterValueToLineIndex.get(genderLabel);

					if (lineIndex == null) {
						lineIndex = clusterValueToLineIndex.size();

						clusterValueToLineIndex.put(genderLabel, lineIndex);
						result.setLineTitle(genderLabel, lineIndex);
					}

					//
					result.setValue(map.get(key)[i], lineIndex, colIndex);
				}

				//
				colIndex += 1;
			}
		}
		//
		return result;
	}

	/**
	 * 
	 * @param partitionCriteria
	 * @param splitCriteria
	 * @return
	 * @throws PuckException
	 */
	public static ReportChart createMeanClusterValuesChart(final String criteriaString, final Map<Cluster<Individual>, Double> source) throws PuckException {
		ReportChart result;

		if ((criteriaString == null) || (source == null)) {
			result = null;
		} else {
			result = new ReportChart("Mean Cluster Values (" + criteriaString + ")", GraphType.SCATTER);

			result.setHeadersLegend("Size");
			result.setLinesLegend("Mean Values");

			result.setLineTitle("Clusters", 0);
			int columnIndex = 0;
			for (Cluster<Individual> cluster : source.keySet()) {

				result.setHeader(cluster.getLabel(), columnIndex);
				result.addValue(cluster.size(), source.get(cluster), 0);

				columnIndex += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 * @throws PuckException
	 */
	public static ReportTable createMeanClusterValuesTable(final String criteriaString, final Clusters<Individual> source,
			final Map<Cluster<Individual>, Double> means) throws PuckException {
		ReportTable result;

		if ((criteriaString == null) || (source == null) || (means == null)) {
			result = null;
		} else {
			result = new ReportTable(source.size() + 1, 4);
			result.setTitle("Mean Cluster Values (" + criteriaString + ")");

			result.set(0, 0, "Cluster");
			result.set(0, 1, "Name");
			result.set(0, 2, "Mean Value");
			result.set(0, 3, "Size");

			int rowIndex = 1;
			for (Cluster<Individual> cluster : source.toListSortedByDescendingSize()) {

				result.set(rowIndex, 0, rowIndex);
				result.set(rowIndex, 1, cluster.getLabel());
				result.set(rowIndex, 2, MathUtils.toString(means.get(cluster)));
				result.set(rowIndex, 3, MathUtils.toString(cluster.size()));

				rowIndex += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param partitions
	 * @return
	 * @throws PuckException
	 */
	public static <E> ReportChart createMultiPartitionChart(final MultiPartition<E> partitions) throws PuckException {
		ReportChart result;

		// == Build Report Chart.
		result = new ReportChart(partitions.getLabel(), GraphType.STACKED_BARS);

		//
		{
			int columnIndex = 0;
			for (Value columnValue : partitions.sortedColValues()) {
				result.setLineTitle(columnValue.toString(), columnIndex);

				int rowIndex = 0;
				for (Value rowValue : partitions.rowValuesSortedBySize()) {
					result.setHeader(rowValue.toString(), rowIndex);
					result.setValue(partitions.frequency(rowValue, columnValue), columnIndex, rowIndex);
					rowIndex += 1;
				}

				columnIndex += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param partitionCriteria
	 * @param splitCriteria
	 * @return
	 * @throws PuckException
	 */
	public static <E> ReportChart createPartitionChart(final Partition<E> partition) throws PuckException {
		ReportChart result;

		if (partition == null) {
			result = null;
		} else {
			//
			result = new ReportChart(partition.getLabel(), GraphType.STACKED_BARS);

			//
			int clusterIndex = 0;
			for (Cluster<E> cluster : partition.getClusters().toListSortedByValue()) {
				//
				result.setHeader(cluster.getLabel(), clusterIndex);

				//
				result.addValue(cluster.size(), 0);

				//
				clusterIndex += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param partitionCriteria
	 * @param splitCriteria
	 * @return
	 * @throws PuckException
	 */
	public static <E> ReportChart createPartitionChart(final Partition<E> source, final PartitionCriteria partitionCriteria,
			final PartitionCriteria splitCriteria, final Geography geography) throws PuckException {
		ReportChart result;

		if ((source == null) || (partitionCriteria == null) || (!partitionCriteria.isValid())) {

			result = null;
		} else {
			//
			Partition<E> partition = source;

			//
			if ((partitionCriteria.getSizeFilter() != null) && (partitionCriteria.getSizeFilter() != SizeFilter.NONE)) {
				partition = PartitionMaker.createCleaned(partition, partitionCriteria.getSizeFilter());
			}
			if ((partitionCriteria.getValueFilter() != null) && (partitionCriteria.getValueFilter() != ValueFilter.NONE)) {
				partition = PartitionMaker.createCleaned(partition, partitionCriteria.getValueFilter());
			}

			//
			result = new ReportChart(partition.getLabel() + " " + partitionCriteria.toShortString(), GraphType.STACKED_BARS);

			//
			if (partitionCriteria.getCumulationType() == CumulationType.ASCENDANT) {
				//
				int clusterIndex = 0;
				int cumulation = 0;
				for (Cluster<E> cluster : partition.getClusters().toListSortedByValue()) {
					//
					result.setHeader(cluster.getLabel(), clusterIndex);

					//
					cumulation += cluster.size();
					result.addValue(cumulation, 0);

					//
					clusterIndex += 1;
				}
			} else if (partitionCriteria.getCumulationType() == CumulationType.DESCENDANT) {
				//
				int clusterIndex = partition.size() - 1;
				int cumulation = 0;
				List<Cluster<E>> clusters = partition.getClusters().toListSortedByValue();
				Collections.reverse(clusters);
				for (Cluster<E> cluster : clusters) {
					//
					result.setHeader(cluster.getLabel(), clusterIndex);

					//
					cumulation += cluster.size();
					result.setValue(cumulation, 0, clusterIndex);

					//
					clusterIndex -= 1;
				}
			} else if ((splitCriteria != null) && (splitCriteria.isValid())) {

				if (splitCriteria.getLabel().equals("GENDER")) {

					splitCriteria.setValues(new Values(Gender.getChartValueList()));
				}

				//
				int clusterIndex = 0;
				HashMap<String, Integer> clusterValueToLineIndex = new HashMap<String, Integer>();
				for (Cluster<E> cluster : partition.getClusters().toListSortedByValue()) {
										
					//
					Partition<E> split = PartitionMaker.create("split", cluster, splitCriteria, geography);
					// logger.debug("split cluster size=" + split.size());
					//
					result.setHeader(cluster.getLabel(), clusterIndex);
					for (Cluster<E> splitCluster : split.getClusters().toListSortedByValue()) {
						
						String splitClusterLabel;
						if (splitCluster.getLabel() == null) {
							splitClusterLabel = "null";
						} else {
							splitClusterLabel = splitCluster.getLabel();
						}

						//
						Integer lineIndex = clusterValueToLineIndex.get(splitClusterLabel);

						if (lineIndex == null) {
							lineIndex = clusterValueToLineIndex.size();

							clusterValueToLineIndex.put(splitClusterLabel, lineIndex);
							result.setLineTitle(splitClusterLabel, lineIndex);
						}

						//
						result.setValue(splitCluster.size(), lineIndex, clusterIndex);
					}

					//
					clusterIndex += 1;
				}
			} else {
				//
				int clusterIndex = 0;
				for (Cluster<E> cluster : partition.getClusters().toListSortedByValue()) {
					//
					result.setHeader(cluster.getLabel(), clusterIndex);
					
					//
					result.addValue(cluster.size(), 0);

					//
					clusterIndex += 1;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param partitionCriteria
	 * @param splitCriteria
	 * @return
	 * @throws PuckException
	 */
	public static <E> ReportChart createPartitionChartBySize(final Partition<E> partition, final double threshold) throws PuckException {
		ReportChart result;

		if (partition == null) {
			result = null;
		} else {
			//
			result = new ReportChart(partition.getLabel(), GraphType.STACKED_BARS);

			double minSize = partition.itemsCount() * threshold;
			//
			int clusterIndex = 0;
			for (Cluster<E> cluster : partition.getClusters().toListSortedByDescendingSize()) {
				//
				result.setHeader(cluster.getLabel(), clusterIndex);

				if (cluster.size() > minSize) {
					//
					result.addValue(cluster.size(), 0);

					//
					clusterIndex += 1;
				} else {
					break;
				}

			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static <E> ReportChart createRamificationChart(final Partition<Node<Cluster<E>>>[] partition, final int[][] totalSums, final String label) {
		ReportChart result;

		//
		result = new ReportChart("Sequence Tree Ramification " + label, GraphType.LINES);
		result.setHeadersLegend("Steps (events)");
		result.setLinesLegend("Concentration");
		result.setVerticalMax(100.0);
		result.setIntegerHorizontalUnit(true);

		//
		result.setLineTitle("Male", 0);
		result.setLineTitle("Female", 1);
		result.setLineTitle("Overall", 2);

		//
		for (int gender = 0; gender < 3; gender++) {

			Value maxValue = partition[gender].maxValue();
			if (maxValue != null) {

				for (int ascendingIndex = 0; ascendingIndex < maxValue.intValue(); ascendingIndex++) {
					Cluster<Node<Cluster<E>>> cluster = partition[gender].getCluster(new Value(ascendingIndex));
					List<Double> nodeSizes = new ArrayList<Double>();
					for (Node<Cluster<E>> node : cluster.getItems()) {
						Cluster<E> referent = node.getReferent();
						if (referent != null) {
							nodeSizes.add(new Double(referent.size()));
						}
					}
					result.addValue(ascendingIndex, MathUtils.herfindahl(nodeSizes, totalSums[ascendingIndex][gender]), gender);
				}

			}
		}
		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 * @throws PuckException
	 */
	public static ReportChart createSibsetDistributionChart(final FiliationCounts counts) throws PuckException {
		ReportChart result;

		//
		result = new ReportChart("Sibset Distribution", GraphType.LINES);
		result.setHeadersLegend("Size");
		result.setLinesLegend("Number");
		result.setIntegerHorizontalUnit(true);

		//
		result.setLineTitle("Uterine", 0);
		result.setLineTitle("Agnatic", 1);

		//
		for (int countIndex = 2; countIndex < counts.size(); countIndex++) {
			result.setHeader(MathUtils.toString(countIndex), countIndex);
			result.addValue(countIndex, counts.get(countIndex).getUterine(), 0);
			result.addValue(countIndex, counts.get(countIndex).getAgnatic(), 1);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 * @throws PuckException
	 */
	public static ReportTable createSibsetDistributionTable(final FiliationCounts counts) throws PuckException {
		ReportTable result;

		//
		result = new ReportTable(counts.size() + 1, 5);
		result.setTitle("Sibset Distribution");

		//
		result.set(0, 0, " ");
		result.set(0, 1, "Uterine");
		result.set(0, 2, "Agnatic");
		result.set(0, 3, "% Agnatic");
		result.set(0, 4, "% Uterine");

		// TODO 2 ?

		//
		int agnaticSum = counts.agnaticSum();
		int uterineSum = counts.uterineSum();
		for (int countIndex = 0; countIndex < counts.size(); countIndex++) {
			result.set(countIndex + 1, 0, countIndex);
			result.set(countIndex + 1, 1, MathUtils.toString(counts.get(countIndex).getUterine()));
			result.set(countIndex + 1, 2, MathUtils.toString(counts.get(countIndex).getAgnatic()));
			result.set(countIndex + 1, 3, MathUtils.percent(counts.get(countIndex).getUterine(), uterineSum));
			result.set(countIndex + 1, 4, MathUtils.percent(counts.get(countIndex).getAgnatic(), agnaticSum));
		}

		//
		return result;
	}

	public static <E extends Individualizable> ReportChart createSurvivalChart(final Partition<E> source, final PartitionCriteria splitCriteria, final Geography geography)
			throws PuckException {
		ReportChart result;

		if ((source == null) || (splitCriteria == null) || (!splitCriteria.isValid())) {
			result = null;
		} else {
			//
			Partition<Individual> partition = PartitionMaker.individualize(source, new PartitionCriteria(source.getLabel()));
			Partition<Individual> splitPartition = PartitionMaker.create("split", new Individuals(partition.getItemsAsList()), splitCriteria, geography);

			//
			result = new ReportChart(partition.getLabel() + " Survival Curves", GraphType.LINES);
			result.setIntegerHorizontalUnit(true);
			result.setVerticalMax(100.);

			//
			// int clusterIndex = 0;
			HashMap<String, Integer> clusterValueToLineIndex = new HashMap<String, Integer>();

			int nrClusters = partition.maxValue().intValue();

			// partition.getClusters().size();
			int nrLines = splitPartition.getClusters().size() + 1;

			double[][] values = new double[nrClusters][nrLines];

			for (Cluster<Individual> cluster : partition.getClusters().toListSortedByValue()) {

				//
				Partition<Individual> split = PartitionMaker.create("split", new Individuals(cluster.getItems()), splitCriteria, geography);
				// logger.debug("split cluster size=" + split.size());
				//

				int clusterIndex = -1;

				if ((cluster.getValue() != null) && (cluster.getValue().intValue() > -1)) {

					clusterIndex = cluster.getValue().intValue();
					result.setHeader(cluster.getLabel(), clusterIndex);

				}

				for (Cluster<Individual> splitCluster : split.getClusters().toListSortedByValue()) {
					String splitClusterLabel;
					if (splitCluster.getLabel() == null) {
						splitClusterLabel = "Null";
					} else {
						splitClusterLabel = splitCluster.getLabel();
					}

					//
					Integer lineIndex = clusterValueToLineIndex.get(splitClusterLabel);

					if (lineIndex == null) {
						lineIndex = clusterValueToLineIndex.size();

						clusterValueToLineIndex.put(splitClusterLabel, lineIndex);
						result.setLineTitle(splitClusterLabel, lineIndex);
					}

					// Count and Cumulate (Split)
					for (int index = 0; index < clusterIndex; index++) {
						values[index][lineIndex] += splitCluster.size();
					}

					if (clusterIndex < 0) {
						for (int index = 0; index < nrClusters; index++) {
							values[index][lineIndex] += splitCluster.size();
						}
					}

				}

				// Count and Cumulate (Overall)
				for (int index = 0; index < clusterIndex; index++) {
					values[index][nrLines - 1] += cluster.size();
				}

				if (clusterIndex < 0) {
					for (int index = 0; index < nrClusters; index++) {
						values[index][nrLines - 1] += cluster.size();
					}
				}

				//
				// clusterIndex += 1;
			}

			result.setLineTitle("Total", nrLines - 1);

			// Normalize and Visualize

			double[] sum = new double[nrLines + 1];
			for (int lineIndex = 0; lineIndex < nrLines; lineIndex++) {
				sum[lineIndex] += values[0][lineIndex];
			}
			// sum[nrLines - 1] = values[0][nrLines - 1];

			for (int clusterIndex = 0; clusterIndex < nrClusters; clusterIndex++) {
				for (int lineIndex = 0; lineIndex < nrLines; lineIndex++) {
					values[clusterIndex][lineIndex] = MathUtils.percent(values[clusterIndex][lineIndex], sum[lineIndex]);
					result.setValue(values[clusterIndex][lineIndex], lineIndex, clusterIndex);
				}
			}
		}

		//
		return result;

	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Report reportAttributeAndValueStatistics(final Segmentation source) {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Attributes");
		result.setOrigin("Statistics reporter");

		if (source == null) {
			//
			result.outputs().appendln("Blank source.");

		} else {
			//
			result.setTarget(source.getLabel());
			result.setInputComment("Net: " + source.getLabel());

			//
			AttributeDescriptors descriptors = AttributeWorker.getExogenousAttributeDescriptors(source, null).sort();

			//
			result.outputs().appendln("Label count: " + descriptors.size());
			result.outputs().appendln("Attribute count: " + descriptors.total());

			//
			result.outputs().appendln();
			StringList groupedList = createAttributeDescriptorGroupedList(descriptors);
			result.outputs().appendln(groupedList);

			//
			ReportTable table = createAttributeDescriptorTable(descriptors);
			result.outputs().appendln(table);

			//
			for (String label : descriptors.labels().sort()) {
				AttributeValueDescriptorList valueDescriptors = AttributeWorker.getExogenousAttributeValueDescriptors(source, label).toList().sortByValue();
				logger.debug("value descriptor count={}", valueDescriptors.size());

				result.outputs().appendln(label + ": " + valueDescriptors.size() + " different values");
				result.outputs().appendln(valueDescriptors.total() + " occurences");
				//
				ReportTable valueTable = new ReportTable(valueDescriptors.size() + 1, 3);
				//
				valueTable.set(0, 0, "ID");
				valueTable.set(0, 1, "Value");
				valueTable.set(0, 2, "Count");

				//
				int index = 1;
				for (AttributeValueDescriptor valueDescriptor : valueDescriptors) {
					//
					valueTable.set(index, 0, index);
					valueTable.set(index, 1, valueDescriptor.getValue());
					valueTable.set(index, 2, valueDescriptor.getCount());

					//
					index += 1;
				}
				result.outputs().appendln(valueTable);
			}
			result.outputs().appendln();
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Report reportAttributeStatistics(final Net source) {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Attributes");
		result.setOrigin("Statistics reporter");

		if (source == null) {
			//
			result.outputs().appendln("Blank source.");

		} else {
			//
			result.setTarget(source.getLabel());
			result.setInputComment("Net: " + source.getLabel());

			//
			AttributeDescriptors descriptors = AttributeWorker.getExogenousAttributeDescriptors(source, null).sort();

			//
			result.outputs().appendln("Label count: " + descriptors.size());
			result.outputs().appendln("Attribute count: " + descriptors.total());

			//
			result.outputs().appendln();
			StringList groupedList = createAttributeDescriptorGroupedList(descriptors);
			result.outputs().appendln(groupedList);

			//
			// ReportTable table =
			// createAttributeDescriptorGroupedTable(descriptors);
			// result.outputs().appendln(table);

			//
			ReportTable table = createAttributeDescriptorTable(descriptors);
			result.outputs().appendln(table);
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Report reportAttributeStatistics(final Segmentation source) {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Attribute statitics.");
		result.setOrigin("Statistics reporter");

		if (source == null) {
			//
			result.outputs().appendln("Blank source.");

		} else {
			//
			result.setTarget(source.getLabel());
			result.setInputComment("Segmentation: " + source.getSummary());

			//
			AttributeDescriptors descriptors = AttributeWorker.getExogenousAttributeDescriptors(source, null);

			//
			result.outputs().appendln("Label count: " + descriptors.size());
			result.outputs().appendln("Attribute count: " + descriptors.total());

			//
			result.outputs().appendln();
			StringList groupedList = createAttributeDescriptorGroupedList(descriptors);
			result.outputs().appendln(groupedList);

			//
			// ReportTable table =
			// createAttributeDescriptorGroupedTable(descriptors);
			// result.outputs().appendln(table);

			//
			ReportTable table = createAttributeDescriptorTable(descriptors);
			result.outputs().appendln(table);
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
/*	public static Report reportAttributeValueStatistics(final Net source) {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Attribute value statitics.");
		result.setOrigin("Statistics reporter");

		if (source == null) {
			//
			result.outputs().appendln("Blank source.");

		} else {
			//
			result.setTarget(source.getLabel());
			result.setInputComment("Net: " + source.getLabel());

			//
			AttributeValueDescriptorList valueDescriptors = AttributeWorker.getExogenousAttributeValueDescriptors(source).toList().sortByCount().reverse();
			logger.debug("value descriptor count={}", valueDescriptors.size());

			result.outputs().appendln("Value count: " + valueDescriptors.size());
			result.outputs().appendln("Attribute value count: " + valueDescriptors.total());

			//
			ReportTable valueTable = new ReportTable(valueDescriptors.size() + 1, 3);
			result.setTitle("Value Descriptors");

			//
			valueTable.set(0, 0, "ID");
			valueTable.set(0, 1, "Value");
			valueTable.set(0, 2, "Count");

			//
			int index = 1;
			for (AttributeValueDescriptor valueDescriptor : valueDescriptors) {
				//
				valueTable.set(index, 0, index);
				valueTable.set(index, 1, valueDescriptor.getValue());
				valueTable.set(index, 2, valueDescriptor.getCount());

				//
				index += 1;
			}

			result.outputs().appendln(valueTable);
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}*/

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Report reportAttributeValueStatistics(final Segmentation source) {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Attribute statitics.");
		result.setOrigin("Statistics reporter");

		if (source == null) {
			//
			result.outputs().appendln("Blank source.");

		} else {
			//
			result.setTarget(source.getLabel());
			result.setInputComment("Segmentation: " + source.getSummary());

			//
			result.setTarget(source.getLabel());
			result.setInputComment("Net: " + source.getLabel());

			//
			AttributeValueDescriptorList valueDescriptors = AttributeWorker.getExogenousAttributeValueDescriptors(source).toList().sortByCount().reverse();
			logger.debug("value descriptor count={}", valueDescriptors.size());

			result.outputs().appendln("Value count: " + valueDescriptors.size());
			result.outputs().appendln("Attribute value count: " + valueDescriptors.total());

			//
			ReportTable valueTable = new ReportTable(valueDescriptors.size() + 1, 3);
			result.setTitle("Value Descriptors");

			//
			valueTable.set(0, 0, "ID");
			valueTable.set(0, 1, "Value");
			valueTable.set(0, 2, "Count");

			//
			int index = 1;
			for (AttributeValueDescriptor valueDescriptor : valueDescriptors) {
				//
				valueTable.set(index, 0, index);
				valueTable.set(index, 1, valueDescriptor.getValue());
				valueTable.set(index, 2, valueDescriptor.getCount());

				//
				index += 1;
			}

			result.outputs().appendln(valueTable);
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportBasicInformation(final Net net, final Segmentation source) throws PuckException {
		Report result;

		//
		Chronometer chrono = new Chronometer();
		result = new Report();

		//
		result.setTitle("Basics");
		result.setOrigin("Statistics reporter");
		result.setTarget(net.getLabel());

		//
		if (source.isOn()) {
			result.setInputComment("Segmentation:\n" + source.getSummary());
		}

		//
		StatisticsWorker worker = new StatisticsWorker(net);
		ReportAttributes items = new ReportAttributes();

		for (StatisticsWorker.Indicator indicator : StatisticsWorker.Indicator.values()) {
			if (indicator.equals(Indicator.MEAN_DEPTH) || indicator.equals(Indicator.MEAN_SPOUSE_DISTANCE_GEN)) {
				continue;
			}
			worker.addItem(items, indicator);
		}

		result.outputs().append(items);
		result.outputs().appendln();

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportCompleteness(final Segmentation source) throws PuckException {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Completeness statistics about a corpus.");
		result.setOrigin("Statistics reporter");
		result.setTarget(source.getLabel());

		//
		if (source.isOn()) {
			result.setInputComment(source.getSummary());
		}

		//
		FiliationCounts counts = StatisticsWorker.completeness(source.getCurrentIndividuals(), 10);
		ReportChart reportChart = createCompletenessChart(counts);

		result.outputs().append(reportChart);
		result.outputs().appendln();

		//
		result.outputs().append(ReportTable.transpose(reportChart.createReportTableWithSum()));
		result.outputs().appendln();

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportFamiliesByHusband(final Segmentation segmentation) {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Family Spouse Ids (by Husband).");
		result.setOrigin("Statistics reporter");
		result.setTarget(segmentation.getLabel());

		//
		List<String> strings = new ArrayList<String>();
		for (Family family : segmentation.getCurrentFamilies().toSortedList(FamilyComparator.Sorting.HUSBAND)) {
			//
			int husbandId;
			String husbandName;

			if (family.getHusband() == null) {
				husbandId = 0;
				husbandName = "";
			} else {
				husbandId = family.getHusband().getId();
				husbandName = family.getHusband().getName();
			}

			//
			int wifeId;
			String wifeName;

			if (family.getWife() == null) {
				wifeId = 0;
				wifeName = "";
			} else {
				wifeId = family.getWife().getId();
				wifeName = family.getWife().getName();
			}

			//
			String string = husbandId + "\t" + husbandName + "\t" + wifeId + "\t" + wifeName + "\t" + family.getId();

			//
			strings.add(string);
		}

		result.outputs().appendln("Husband\tWife\tFamily");
		for (String string : strings) {
			result.outputs().appendln(string);
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportFamiliesByWife(final Segmentation segmentation) {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Family Spouse Ids (by Wife).");
		result.setOrigin("Statistics reporter");
		result.setTarget(segmentation.getLabel());

		//
		List<String> strings = new ArrayList<String>();
		for (Family family : segmentation.getCurrentFamilies().toSortedList(FamilyComparator.Sorting.WIFE)) {
			//
			int husbandId;
			String husbandName;

			if (family.getHusband() == null) {
				husbandId = 0;
				husbandName = "";
			} else {
				husbandId = family.getHusband().getId();
				husbandName = family.getHusband().getName();
			}

			//
			int wifeId;
			String wifeName;

			if (family.getWife() == null) {
				wifeId = 0;
				wifeName = "";
			} else {
				wifeId = family.getWife().getId();
				wifeName = family.getWife().getName();
			}

			//
			String string = wifeId + "\t" + wifeName + "\t" + husbandId + "\t" + husbandName + "\t" + family.getId();

			//
			strings.add(string);
		}

		result.outputs().appendln("Wife\tHusband\tFamily");
		for (String string : strings) {
			result.outputs().appendln(string);
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Report reportGeographyStatistics(final Net source) {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Geography Statistics");
		result.setOrigin("Statistics reporter");

		if ((source == null) || (source.getGeography() == null)) {
			//
			result.outputs().appendln("Blank source.");

		} else {
			//
			Geography geography = source.getGeography();

			//
			result.setTarget(source.getLabel());

			//
			result.outputs().appendln("* Corpus Geography Statistics");
			result.outputs().appendln("Places:\t\t" + geography.countOfPlaces());
			result.outputs().appendln("Homonyms:\t" + geography.countOfHomonyms());
			result.outputs().appendln("Toponyms:\t\t" + geography.countOfToponyms() + " (main + alternates)");

			{
				int conflictedCount = geography.countOfConflictedPlaces();
				int nameCount = geography.countOfToponyms();
				String conflictedData = String.format("%d / %d names (%s)", conflictedCount, nameCount,
						ToolBox.buildReadablePercentage(conflictedCount, nameCount));
				result.outputs().appendln("Conflicted places:\t" + conflictedData);
			}

			{
				int ungeocodedCount = geography.countOfUngeocodedPlaces();
				int placeCount = geography.countOfPlaces();
				String ungeocodedData = String.format("%d / %d places (%s)", ungeocodedCount, placeCount,
						ToolBox.buildReadablePercentage(ungeocodedCount, placeCount));
				result.outputs().appendln("Ungeocoded places:\t" + ungeocodedData);
			}

			{
				int blankCount = geography.getBlankPlaces().size();
				int placeCount = geography.countOfPlaces();
				String blankData = String.format("%d / %d places (%s)", blankCount, placeCount, ToolBox.buildReadablePercentage(blankCount, placeCount));
				result.outputs().appendln("Blank places:\t" + blankData);
			}

			{
				AttributeValueDescriptors valueDescriptors = AttributeWorker.getExogenousAttributeValueDescriptors(source, ".*_PLAC.*");
				Places places = new Places(geography.getPlaces().size());
				for (Place place : geography.getPlaces()) {

					if (valueDescriptors.getCountOf(place.getToponyms()) == 0) {

						places.add(place);
					}
				}

				int unusedCount = places.size();
				int placeCount = geography.countOfPlaces();
				String unusedData = String.format("%d / %d places (%s)", unusedCount, placeCount, ToolBox.buildReadablePercentage(unusedCount, placeCount));
				result.outputs().appendln("Unused places:\t" + unusedData);
			}

			result.outputs().appendln("Status:\t\t" + geography.getStatus().toString());
			result.outputs().appendln();

			//
			{
				result.outputs().appendln("* Missing Places");
				AttributeValueDescriptors valueDescriptors = AttributeWorker.getExogenousAttributeValueDescriptors(source, ".*_PLAC.*");

				AttributeValueDescriptors target = new AttributeValueDescriptors();
				for (AttributeValueDescriptor valueDescriptor : valueDescriptors) {

					if (geography.searchByToponym(valueDescriptor.getValue()) == null) {

						target.put(valueDescriptor);
					}
				}

				if (target.isEmpty()) {
					result.outputs().appendln("No attribute place is missing in geography.");
				} else {
					result.outputs().appendln("Place attribute missing in geography:\t" + target.size());

					ReportTable table = new ReportTable(target.size() + 1, 3);
					table.set(0, 0, "");
					table.set(0, 1, "Attribute value");
					table.set(0, 2, "Count");
					int index = 1;
					for (AttributeValueDescriptor valueDescriptor : target) {

						table.set(index, 0, index);
						table.set(index, 1, valueDescriptor.getValue());
						table.set(index, 2, valueDescriptor.getCount());

						index += 1;
					}

					result.outputs().appendln(table);
				}
			}

			//
			{
				result.outputs().appendln("* Corpus Place Attributes – Corpus Geography statistics");
				result.outputs().appendln("Coming soon…");
			}
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportGraphicsStatistics(final Segmentation segmentation, final StatisticsCriteria criteria, final File sourceFile)
			throws PuckException {
		Report result;

		if ((segmentation == null) || (criteria == null)) {
			result = null;
		} else {
			//
			Chronometer chrono = new Chronometer();

			//
			result = new Report();
			result.setTitle("Statistics");
			result.setOrigin("Statistics reporter");
			result.setTarget(segmentation.getLabel());

			//
			if (segmentation.isOn()) {
				result.setInputComment("Segmentation:\n" + segmentation.getSummary());
			}

			//
			List<ReportChart> charts = new ArrayList<ReportChart>(20);
			List<ReportTable> tables = new ArrayList<ReportTable>(20);

			//
			if (criteria.isGenderBIASWeight()) {
				//
				BIASCounts counts = StatisticsWorker.biasWeights(segmentation.getCurrentIndividuals());
				charts.add(createGenderBIASWeightChart(counts));
				tables.add(createGenderBIASWeightTable(counts));
			}

			//
			if (criteria.isGenderBIASNetWeight()) {
				//
				BIASCounts counts = StatisticsWorker.biasNetWeights(segmentation.getCurrentIndividuals());
				charts.add(createGenderBIASNetWeightChart(counts));
				tables.add(createGenderBIASNetWeightTable(counts));
			}

			//
			if (criteria.isComponents()) {
				//
				charts.add(createComponentsChart(segmentation.getCurrentIndividuals(), 1000));
			}

			//
			if (criteria.isGenealogicalCompleteness()) {
				//
				FiliationCounts counts = StatisticsWorker.completeness(segmentation.getCurrentIndividuals(), 10);
				ReportChart chart = createCompletenessChart(counts);
				charts.add(chart);
				tables.add(ReportTable.transpose(chart.createReportTable()));
			}

			//
			if (criteria.isSibsetDistribution()) {
				//
				FiliationCounts counts = StatisticsWorker.sibsetDistribution(segmentation.getCurrentIndividuals());
				charts.add(createSibsetDistributionChart(counts));
				tables.add(createSibsetDistributionTable(counts));
			}

			//
			if (criteria.isFourCousinMarriages()) {
				Partition<Chain> partition = CircuitFinder.createFirstCousinMarriages(segmentation);
				ReportChart chart = createPartitionChart(partition);
				if (chart != null) {
					//
					charts.add(chart);
					tables.add(ReportTable.transpose(chart.createReportTableWithSum()));
				}
			}

			//
			if (criteria.isAncestorTypes()) {
				MultiPartition<Chain> partition = CircuitFinder.createAncestorChains(segmentation, criteria.getAncestorTypesDegree());
				ReportChart chart = createMultiPartitionChart(partition);
				if (chart != null) {
					//
					charts.add(chart);
					tables.add(ReportTable.transpose(chart.createReportTableWithSum()));
				}
			}

			//
			if (criteria.isConsanguineChains()) {
				double[][] consCount = StatisticsWorker.countConsanguinePairs(segmentation, criteria.getConsanguineDegree());
				ReportChart chart = createArrayChart("Consanguines per person", consCount, new String[] { "HH", "FF", "HF", "FH" });
				if (chart != null) {
					chart.setHeadersLegend("Canonic Degree");
					chart.setLinesLegend("Consanguines");
					// chart.setVerticalMax(100.0);
					chart.setIntegerHorizontalUnit(true);

					//
					charts.add(chart);
					tables.add(ReportTable.transpose(chart.createReportTable()));
				}
			}

			// Compute charts and tables.
			StringList pajekBuffer = new StringList();
			for (PartitionCriteria partitionCriteria : criteria.getPartitionCriteriaList()) {
				//
				if (partitionCriteria.isValid()) {
					Partition<Individual> partition = PartitionMaker.create(segmentation.getLabel(), segmentation.getCurrentIndividuals(), partitionCriteria, segmentation.getGeography());

					ReportChart chart = createPartitionChart(partition, partitionCriteria, criteria.getSplitCriteria(), segmentation.getGeography());
					if (chart != null) {
						//
						charts.add(chart);
						tables.add(ReportTable.transpose(chart.createReportTableWithSum()));

						// Create Mean Cluster Values chart.
						if ((criteria.isMeanClusterValues()) && (criteria.getSplitCriteria() != null)) {
							try {
								Map<Cluster<Individual>, Double> means = StatisticsWorker.meanClusterValues(partition.getClusters(),
										criteria.getSplitCriteria(), segmentation.getGeography());

								String criteriaString = partitionCriteria.toShortString() + "/" + criteria.getSplitCriteria().getLabel();

								ReportChart meanChart = createMeanClusterValuesChart(criteriaString, means);
								charts.add(meanChart);

								tables.add(createMeanClusterValuesTable(criteriaString, partition.getClusters(), means));
							} catch (ClassCastException exception) {
								logger.debug("ClassCastException => No mean cluster value.");
								// If mean can't be calculated, do not build
								// chart
								// and table.
							}
						}
					}

					//
					partition.setLabel(partition.getLabel() + "_" + partitionCriteria.getLabel());
					pajekBuffer.addAll(PuckUtils.writePajekPartition(partition, new IndividualComparator(Sorting.ID), null));
				}
			}

			// Manage the number of chart by line.
			for (int chartIndex = 0; chartIndex < charts.size(); chartIndex++) {
				result.outputs().append(charts.get(chartIndex));
				if (chartIndex % 4 == 3) {
					result.outputs().appendln();
				}
			}

			// Add chart tables.
			for (ReportTable table : tables) {
				result.outputs().appendln(table.getTitle());
				result.outputs().appendln(table);
			}

			//
			if (pajekBuffer.length() != 0) {

				//
				File targetFile = ToolBox.setExtension(ToolBox.addToName(sourceFile, "-Partitions"), ".paj");
				ReportRawData rawData = new ReportRawData("Export Partitions to Pajek", "Pajek", "paj", targetFile);
				rawData.setData(PAJFile.convertToMicrosoftEndOfLine(pajekBuffer.toString()));

				//
				result.outputs().appendln("Partitions");
				result.outputs().append(rawData);
			}

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param segmentation
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportHomonyms(final Segmentation segmentation, final int nrParts, final int minCount) throws PuckException {
		Report result;

		Chronometer chrono = new Chronometer();

		result = new Report("Homonyms.");
		result.setOrigin("Statistics reporter");
		result.setTarget(segmentation.getLabel());

		Report simpleReport = new Report("List");
		Report detailedReport = new Report("Details");
		Report relativesReport = new Report("Relatives");

		Map<String, Individuals> ids = new TreeMap<String, Individuals>();
		Map<String, Integer> count = new TreeMap<String, Integer>();

		for (Individual individual : segmentation.getCurrentIndividuals()) {
			String name = individual.getNamePart(nrParts);
			Individuals id = ids.get(name);
			if (id == null) {
				id = new Individuals();
				ids.put(name, id);
				count.put(name, 1);
			} else {
				count.put(name, count.get(name) + 1);
			}
			id.put(individual);
		}

		String simpleLabelsLine = "Name\tIds";
		String detailedLabelsLine = "Name\tIds";
		String relativesLabelsLine = "Name\tIds";
		simpleReport.outputs().appendln(simpleLabelsLine);

		StringList labels = IndividualValuator.getAttributeLabels(segmentation.getCurrentIndividuals());
		for (String label : labels) {
			detailedLabelsLine += "\t" + label;
		}
		for (KinType kinType : KinType.basicTypes()) {
			relativesLabelsLine += "\t" + kinType;
		}
		detailedReport.outputs().appendln(detailedLabelsLine);
		relativesReport.outputs().appendln(relativesLabelsLine);

		for (String name : ids.keySet()) {
			if (count.get(name) >= minCount) {
				String simpleValuesLine = name + "\t";
				Individuals indis = ids.get(name);
				detailedReport.outputs().appendln(simpleValuesLine);
				relativesReport.outputs().appendln(simpleValuesLine);

				for (Individual indi : indis.toSortedList()) {
					simpleValuesLine += indi.getId() + ";";
					String detailedValuesLine = "\t" + indi.getId();
					String relativesValuesLine = "\t" + indi.getId();
					for (String label : labels) {
						detailedValuesLine += "\t" + Trafo.asNonNull(indi.getAttributeValue(label));
					}
					for (KinType kinType : KinType.basicTypes()) {
						relativesValuesLine += "\t" + indi.getKin(kinType);
					}
					detailedReport.outputs().appendln(detailedValuesLine);
					relativesReport.outputs().appendln(relativesValuesLine);
				}
				simpleReport.outputs().appendln(simpleValuesLine);
			}
		}

		result.outputs().append(simpleReport);
		result.outputs().append(detailedReport);
		result.outputs().append(relativesReport);
		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/*
	 * 
	 */
	public static Report reportIndividuals(final Segmentation segmentation, final List<Sorting> sorting, final List<String> details) {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Individual List.");
		result.setOrigin("Statistics reporter");
		result.setTarget(segmentation.getLabel());

		result.inputs().add("Sorting", sorting.toString());

		for (Individual individual : segmentation.getCurrentIndividuals().toSortedList(sorting)) {
			String signature = null;

			String firstname = individual.getFirstName();
			if (firstname == null) {
				firstname = "-";
			}

			String lastname = individual.getLastName();
			if (lastname == null) {
				lastname = "-";
			}

			switch (sorting.get(0)) {
				case ID:
					signature = individual.getId() + "\t" + firstname + "\t" + lastname;
				break;
				case FIRSTN:
					signature = firstname + "\t" + lastname + "\t" + individual.getId();
				break;
				case LASTN:
					signature = lastname + "\t" + firstname + "\t" + individual.getId();
			}

			if (details != null) {
				signature += "\t";
				for (String label : details) {
					if (label.equals("PARENTS")) {
						signature += individual.getFather() + "\t" + individual.getMother() + "\t";
					} else if (label.equals("SPOUSES")) {
						for (Individual spouse : individual.spouses()) {
							signature += spouse + " ";
						}
						signature += "\t";
					} else if (label.equals("PARTNERS")) {
						for (Individual spouse : individual.getPartners()) {
							signature += spouse + " ";
						}
						signature += "\t";
					} else if (label.equals("CHILDREN")) {
						for (Individual child : individual.children()) {
							signature += child + " ";
						}
						signature += "\t";
					} else if (label.equals("MARR_DATE")) {
						for (Family family : individual.getPersonalFamilies()) {
							signature += FamilyValuator.get(family, label, segmentation.getGeography()) + "\t";
						}
					} else if (label.equals("MARR_PLACE")) {
						for (Family family : individual.getPersonalFamilies()) {
							signature += FamilyValuator.get(family, label, segmentation.getGeography()) + "\t";
						}
					} else {
						signature += IndividualValuator.get(individual, label, segmentation.getGeography()) + "\t";
					}
				}
			}

			if (signature != null) {
				result.outputs().append(signature);
				result.outputs().appendln();
			}
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * 
	 * @param segmentation
	 * @param partitionLabel
	 * @param sorting
	 * @return
	 * @throws PuckException
	 */
	public static Report reportIndividuals(final Segmentation segmentation, final String partitionLabel, final Sorting sorting) throws PuckException {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Individual List.");
		result.setOrigin("Statistics reporter");
		result.setTarget(segmentation.getLabel());

		result.inputs().add("Partition", partitionLabel);
		result.inputs().add("Sorting", sorting.toString());

		Partition<Individual> partition = PartitionMaker.createRaw(partitionLabel, segmentation.getCurrentIndividuals(), segmentation.getGeography());

		for (Cluster<Individual> cluster : partition.getClusters().toListSortedByValue()) {
			if (cluster == null || cluster.getValue() == null) {
				continue;
			}
			result.outputs().append(cluster.getValue().toString() + "\t(" + cluster.count() + ")");
			result.outputs().appendln();

			for (Individual individual : new Individuals(cluster.getItems()).toSortedList(sorting)) {
				String signature = null;

				switch (sorting) {
					case ID:
						signature = individual.getId() + "\t" + individual.getFirstName() + "\t" + individual.getLastName();
					break;
					case FIRSTN:
						signature = individual.getFirstName() + "\t" + individual.getLastName() + "\t" + individual.getId();
					break;
					case LASTN:
						signature = individual.getLastName() + "\t" + individual.getFirstName() + "\t" + individual.getId();
				}

				if (signature != null) {
					result.outputs().append(signature);
					result.outputs().appendln();
				}
			}
			result.outputs().appendln();
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}	/**
	 * 
	 * @param segmentation
	 * @param firstPartitionLabel
	 * @param sorting
	 * @return
	 * @throws PuckException
	 */
	public static Report reportAttributeValueStatisticsByCluster(final Segmentation segmentation, final String firstPartitionLabel, final String secondPartitionLabel) throws PuckException {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Individual List.");
		result.setOrigin("Statistics reporter");
		result.setTarget(segmentation.getLabel());

		result.inputs().add("Partition", firstPartitionLabel);
		result.inputs().add("Attribute Label", secondPartitionLabel);

		Partition<Individual> partition = PartitionMaker.createRaw(firstPartitionLabel, segmentation.getCurrentIndividuals(), segmentation.getGeography());

		for (Cluster<Individual> cluster : partition.getClusters().toListSortedByValue()) {
			if (cluster == null || cluster.getValue() == null) {
				continue;
			}
			result.outputs().append(cluster.getValue().toString() + "\t(" + cluster.count() + ")");
			result.outputs().appendln();
			
			Partition<Individual> secondPartition = PartitionMaker.createRaw(secondPartitionLabel, new Individuals(cluster.getItems()), segmentation.getGeography());

			AttributeValueDescriptorList valueDescriptors = AttributeWorker.getAttributeValueDescriptors(secondPartition).toList().sortByValue();
			logger.debug("value descriptor count={}", valueDescriptors.size());

			result.outputs().appendln(secondPartitionLabel + ": " + valueDescriptors.size() + " different values");
			result.outputs().appendln(valueDescriptors.total() + " occurences");
			//
			ReportTable valueTable = new ReportTable(valueDescriptors.size() + 1, 3);
			//
			valueTable.set(0, 0, "ID");
			valueTable.set(0, 1, "Value");
			valueTable.set(0, 2, "Count");

			//
			int index = 1;
			for (AttributeValueDescriptor valueDescriptor : valueDescriptors) {
				//
				valueTable.set(index, 0, index);
				valueTable.set(index, 1, valueDescriptor.getValue());
				valueTable.set(index, 2, valueDescriptor.getCount());

				//
				index += 1;
			}
			result.outputs().appendln(valueTable);
			
			result.outputs().appendln();
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	public static Report reportNameSeries(final Net net) {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Name series.");
		result.setOrigin("Statistics reporter");
		result.setTarget(net.getLabel());

		result.outputs().appendln("Id\tName\tName_F_att\tName_F_real\tName_FF_att\tName_FF_real\tName_FFF_att\tName_FFF_real");

		for (Individual indi : net.individuals()) {
			String line = indi.getId() + "\t" + indi.getName();
			String fnameAtt = Trafo.asNonNull(indi.getAttributeValue("NAME_F"));
			String ffnameAtt = Trafo.asNonNull(indi.getAttributeValue("NAME_FF"));
			String fffnameAtt = Trafo.asNonNull(indi.getAttributeValue("NAME_FFF"));

			String fname = "";
			String ffname = "";
			String fffname = "";
			if (indi.getFather() != null) {
				fname = indi.getFather().getName();
				if (indi.getFather().getFather() != null) {
					ffname = indi.getFather().getFather().getName();
					if (indi.getFather().getFather().getFather() != null) {
						fffname = indi.getFather().getFather().getFather().getName();
					}
				}
			}

			line += "\t" + fnameAtt + "\t" + fname + "\t" + ffnameAtt + "\t" + ffname + "\t" + fffnameAtt + "\t" + fffname;
			result.outputs().appendln(line);
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	public static Report reportQuickReview(final Net net, final Segmentation source, final File file) throws PuckException {
		Report result;

		//
		Chronometer chrono = new Chronometer();
		result = new Report("Quick Review");

		//
		result.setTitle("Quick Review");
		result.setOrigin("Statistics reporter");
		result.setTarget(net.getLabel());

		Report controls = ControlReporter.reportControls(source, ResourceBundle.getBundle("org.tip.puckgui.messages"), ControlReporter.getDefaultControls());
		Report basics = reportBasicInformation(net, source);
		Report attributes = reportAttributeAndValueStatistics(source);
		Report statistics = StatisticsReporter.reportGraphicsStatistics(source, StatisticsCriteria.getDefaultCriteria(), file);
		CensusCriteria censusCriteria = CensusCriteria.getDefaultCriteria();
		censusCriteria.setClosingRelation("PARTN");
		Report census = CensusReporter.reportShortCensus(source, censusCriteria);

		result.outputs().append(controls);
		result.outputs().append(basics);
		result.outputs().append(attributes);
		result.outputs().append(statistics);
		result.outputs().append(census);

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	public static Report reportSynopsis(final String directory) {
		Report result;

		//
		Chronometer chrono = new Chronometer();
		result = new Report();

		//
		result.setTitle("Basic statistics about a corpus");
		result.setOrigin("Statistics reporter");

		StringList listStatistics = new StringList();
		String headline = "General statistics";
		for (StatisticsWorker.Indicator indicator : StatisticsWorker.Indicator.values()) {
			headline += "\t" + indicator.toString();
		}
		result.outputs().appendln(headline);

		StringList listBias = new StringList();
		listBias.appendln("Gender bias\tA1\tA2\tA3\tA4\tA5\tU1\tU2\tU3\tU4\tU5");

		StringList listCensus = new StringList();
		listCensus.appendln("First cousin marriages\tParPat\tCross\tParMat");

		String pattern = "XX(X)XX";
		String classificationType = "LINE";

		File folder = new File(directory);
		for (File file : folder.listFiles()) {
			try {
				Net net = PuckManager.loadNet(file);
				listStatistics.appendln(StatisticsWorker.getValueString(net));
				listBias.appendln(StatisticsWorker.listBiasWeights(net));
				listCensus.appendln(StatisticsWorker.listCircuitFrequencies(new Segmentation(net), classificationType, pattern));
			} catch (PuckException e) {
				System.err.println("Not a corpus file: " + file.getName());
			}
		}
		result.outputs().append(listStatistics);
		result.outputs().appendln();
		result.outputs().append(listBias);
		result.outputs().appendln();
		result.outputs().append(listCensus);
		result.outputs().appendln();

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	public static Report reportTerms(final Segmentation segmentation) {
		Report result;

		//
		Chronometer chrono = new Chronometer();
		result = new Report();

		//
		result.setTitle("Kin term list");
		result.setOrigin("Statistics reporter");

		Map<String, String> terms = new TreeMap<String, String>();

		for (Individual individual : segmentation.getCurrentIndividuals().toSortedList()) {
			String term = individual.getName();
			if ((StringUtils.isNotEmpty(term)) && (term.charAt(0) != '[')) {
				String type = individual.getAttributeValue("Type");
				String types = terms.get(term);
				if (types == null) {
					types = type;
				} else {
					types += ";" + type;
				}
				terms.put(term, types);
			}
		}

		for (String term : terms.keySet()) {
			result.outputs().appendln(term + "\t" + terms.get(term));
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report reportUnionHomonyms(final Net net, final int minCount) {
		Report result;

		Chronometer chrono = new Chronometer();
		result = new Report();
		result.setTitle("Homonyms.");
		result.setOrigin("Statistics reporter");
		result.setTarget(net.getLabel());

		Map<String, String> ids = new TreeMap<String, String>();
		Map<String, Integer> count = new TreeMap<String, Integer>();

		for (Family family : net.families()) {
			if (family.getHusband() != null && family.getWife() != null) {
				String name = family.toNameString();
				String id = ids.get(name);
				if (id == null) {
					ids.put(name, family.getId() + "");
					count.put(name, 1);
				} else {
					ids.put(name, id + ";" + family.getId());
					count.put(name, count.get(name) + 1);
				}
			}
		}

		result.outputs().appendln("Name\tIds");
		for (String name : ids.keySet()) {
			if (count.get(name) >= minCount) {
				result.outputs().appendln(name + "\t" + ids.get(name));
			}
		}
		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

}
