package org.tip.puck.alliancenets;

import java.util.HashSet;
import java.util.Set;


/**
 * @author Telmo Menezes
 *
 */
public class Group implements Cloneable {
    private int id;
    private Set<Alliance> inEdges;
    private Set<Alliance> outEdges;
    private int inDegree;
    private int outDegree;
    private int birth;

    // pageranks
    private double prIn;
    private double prInLast;
    private double prOut;
    private double prOutLast;

    // for generators
    private double genweight;

    // Auxiliary flag for algorithms that need to know if this node was already
    // visited
    private boolean flag;

    public Group(int id) {
        this.id = id;
        inDegree = 0;
        outDegree = 0;
        birth = -1;
        inEdges = new HashSet<Alliance>();
        outEdges = new HashSet<Alliance>();
    }
    
    @Override
    public Group clone()
    {
        Group clonedNode = new Group(id);
        return clonedNode;
    }

    public int getId() {
        return id;
    }

    public Set<Alliance> getInEdges() {
        return inEdges;
    }
    
    public Set<Alliance> getOutEdges() {
        return outEdges;
    }

    public int getInDegree() {
        return inDegree;
    }

    public int getOutDegree() {
        return outDegree;
    }

    public int getBirth() {
        return birth;
    }

    public void setBirth(int birth) {
        this.birth = birth;
    }

    public double getPrIn() {
        return prIn;
    }

    public void setPrIn(double prIn) {
        this.prIn = prIn;
    }

    double getPrInLast() {
        return prInLast;
    }

    void setPrInLast(double prInLast) {
        this.prInLast = prInLast;
    }

    public double getPrOut() {
        return prOut;
    }

    public void setPrOut(double prOut) {
        this.prOut = prOut;
    }

    double getPrOutLast() {
        return prOutLast;
    }

    void setPrOutLast(double prOutLast) {
        this.prOutLast = prOutLast;
    }

    public double getGenweight() {
        return genweight;
    }

    public void setGenweight(double genweight) {
        this.genweight = genweight;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
    
    void addInEdge(Alliance alliance) {
        inEdges.add(alliance);
        inDegree++;
    }
    
    void addOutEdge(Alliance alliance) {
        outEdges.add(alliance);
        outDegree++;
    }
    
    public double getTotalInputWeight() {
        double total = 0;
        for (Alliance alliance : inEdges) {
            total += alliance.getWeight();
        }
        return total;
    }
    
    public double getTotalOutputWeight() {
        double total = 0;
        for (Alliance alliance : outEdges) {
            total += alliance.getWeight();
        }
        return total;
    }
    
    public void removeInput(Alliance alliance) {
        inEdges.remove(alliance);
        inDegree--;
    }
    
    public void removeOutput(Alliance alliance) {
        outEdges.remove(alliance);
        outDegree--;
    }
}