package org.tip.puckgui.views;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puckgui.PuckGUI;
import org.tip.puckgui.util.GenericFileFilter;

/**
 * 
 * @author cpm
 */
public class ImportParamaterizedRelationModelFileSelector extends JFileChooser {

	private static final long serialVersionUID = 6088943912278928589L;
	private static final Logger logger = LoggerFactory.getLogger(ImportParamaterizedRelationModelFileSelector.class);

	/**
	 * 
	 */
	public ImportParamaterizedRelationModelFileSelector(final File sourceDirectory) {
		super();

		//
		File targetDirectory;
		if (sourceDirectory == null) {
			String preferenceDirectory = PuckGUI.instance().getPreferences().getTerminologyLastDirectory();
			if (StringUtils.isBlank(preferenceDirectory)) {
				targetDirectory = null;
			} else {
				targetDirectory = new File(preferenceDirectory);
			}
		} else {
			targetDirectory = sourceDirectory;
		}

		//
		setSelectedFile(targetDirectory);
		setDialogTitle("Import Parameterized Relation Model");
		setFileSelectionMode(JFileChooser.FILES_ONLY);
		setAcceptAllFileFilterUsed(false);
		setApproveButtonText("Import");
		setDialogType(CUSTOM_DIALOG);

		//
		GenericFileFilter defaultFileFilter = new GenericFileFilter("All Terms format files (Standard, ETIC, EMIC)", "term.ods", "term.txt", "term.xls",
				"etic.ods", "etic.txt", "etic.xls", "emic.ods", "emic.txt", "emic.xls");
		addChoosableFileFilter(defaultFileFilter);
		addChoosableFileFilter(new GenericFileFilter("Standard Terms format files (*.term.ods, *.term.txt, *.term.xls)", "term.ods", "term.txt", "term.xls"));
		addChoosableFileFilter(new GenericFileFilter("ETIC Terms files (*.etic.ods, *.etic.txt, *.etic.xls)", "etic.ods", "etic.txt", "etic.xls"));
		addChoosableFileFilter(new GenericFileFilter("EMIC Terms files (*.emic.ods, *.emic.txt, *.emic.xls)", "emic.ods", "emic.txt", "emic.xls"));
		setFileFilter(defaultFileFilter);
	}

	/**
	 * 
	 */
	@Override
	public void cancelSelection() {
		//
		logger.debug("Cancel selection.");
		super.cancelSelection();
	}

	/**
	 * 
	 */
	@Override
	public void setSelectedFile(final File file) {
		//
		super.setSelectedFile(file);

		System.out.println("==== SET SELECTED FILE=================");
		System.out.println("SELECED FILE " + file);
	}

	/**
	 * This method is the main one of the selector.
	 * 
	 * @param parent
	 * @return
	 */
	public static File showSelectorDialog(final Component parent, final File targetFile) {
		File result;

		//
		ImportParamaterizedRelationModelFileSelector selector = new ImportParamaterizedRelationModelFileSelector(targetFile);

		//
		if (selector.showDialog(parent, null) == JFileChooser.APPROVE_OPTION) {

			logger.debug("getCurrentDirectory(): {}", selector.getCurrentDirectory());
			logger.debug("getSelectedFile() : {}", selector.getSelectedFile());
			result = selector.getSelectedFile();
			PuckGUI.instance().getPreferences().setTerminologyLastDirectory(result);

		} else {

			result = null;
		}

		//
		return result;
	}
}
