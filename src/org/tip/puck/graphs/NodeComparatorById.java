package org.tip.puck.graphs;

import java.util.Comparator;

/**
 * 
 * @author TIP
 * 
 */
public class NodeComparatorById<E> implements Comparator<Node<E>> {
	/**
	 * 
	 */
	@Override
	public int compare(final Node<E> alpha, final Node<E> bravo) {
		int result;

		//
		Integer aphaId;
		if (alpha == null) {
			aphaId = null;
		} else {
			aphaId = alpha.getId();
		}

		//
		Integer bravoId;
		if (bravo == null) {
			bravoId = null;
		} else {
			bravoId = bravo.getId();
		}

		//
		if ((aphaId == null) && (bravoId == null)) {
			result = 0;
		} else if (aphaId == null) {
			result = -1;
		} else if (bravoId == null) {
			result = +1;
		} else {
			result = aphaId.compareTo(bravoId);
		}

		//
		return result;
	}
}
