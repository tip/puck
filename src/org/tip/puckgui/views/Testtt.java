package org.tip.puckgui.views;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.tip.puckgui.util.AutoComboBox;
import org.tip.puckgui.util.AutoTextField;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

public class Testtt extends JFrame {

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public Testtt() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		List<String> strings = new ArrayList<String>();
		strings.add("alpha");
		strings.add("bravo");
		strings.add("braille");
		strings.add("charlie");
		contentPane.setLayout(new FormLayout(new ColumnSpec[] { ColumnSpec.decode("159px"), ColumnSpec.decode("123px:grow"), }, new RowSpec[] {
				FormFactory.UNRELATED_GAP_ROWSPEC, RowSpec.decode("20px"), FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));
		AutoComboBox autoComboBox = new AutoComboBox(strings);
		autoComboBox.setStrict(false);
		contentPane.add(autoComboBox, "2, 2, left, top");

		AutoTextField autoTextField = new AutoTextField(strings);
		autoTextField.setStrict(false);
		autoTextField.setText("");
		contentPane.add(autoTextField, "2, 6, fill, default");

		AutoComboBox autoComboBox_1 = new AutoComboBox((List) null);
		contentPane.add(autoComboBox_1, "2, 12, fill, default");

		AutoComboBox autoComboBox_2 = new AutoComboBox((List) null);
		contentPane.add(autoComboBox_2, "2, 14, fill, default");

		AutoTextField autoTextField_1 = new AutoTextField((List) null);
		contentPane.add(autoTextField_1, "2, 16, fill, default");
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					Testtt frame = new Testtt();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
