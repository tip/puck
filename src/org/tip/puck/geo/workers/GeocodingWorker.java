package org.tip.puck.geo.workers;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.geo.Geography;
import org.tip.puck.geo.Place;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.Link;
import org.tip.puck.graphs.Node;
import org.tip.puck.net.workers.AttributeValueDescriptor;
import org.tip.puck.net.workers.AttributeValueDescriptors;

import fr.devinsy.util.StringSet;

/**
 * 
 * @author TIP
 */
public class GeocodingWorker {

	/**
	 * 
	 */
	private GeocodingWorker() {
	}

	/**
	 * This methods census all value of attribute contains the
	 * 
	 * @param geography
	 * @param toponyms
	 * @param weightStep
	 * @return
	 */
	public static Graph<Place> geocodeAttributeValueDescriptors(final Geography geography, final AttributeValueDescriptors valueDescriptors,
			final double weightFactor) {
		Graph<Place> result;

		result = new Graph<Place>();

		if ((geography != null) && (valueDescriptors != null)) {
			//
			for (AttributeValueDescriptor valueDescriptor : valueDescriptors) {
				//
				Place place = geography.get(valueDescriptor.getValue());

				if ((place != null) && (place.isGeocoded())) {

					Node<Place> node = result.addNode(place);

					node.incWeight(valueDescriptor.getCount() * weightFactor);
				}
			}
		}

		//
		return result;
	}

	/**
	 * This methods census all value of attribute contains the
	 * 
	 * @param geography
	 * @param toponyms
	 * @param weightStep
	 * @return
	 */
	public static <E> Graph<Place> geocodeGraph(final Geography geography, final Graph<E> source) {
		Graph<Place> result;

		if (source == null) {
			//
			result = null;

		} else {
			result = new Graph<Place>();
			result.setLabel(source.getLabel());

			if (geography != null) {
				//
				for (Node<E> sourceNode : source.getNodes()) {
					//
					String sourceNodeLabel = sourceNode.getLabel();

					//
					if (StringUtils.isNotBlank(sourceNodeLabel)) {
						//
						Place place = geography.get(sourceNodeLabel);

						if ((place != null) && (place.isGeocoded())) {
							//
							result.addNode(sourceNode.getId(), place);
						}
					}
				}

				//
				for (Node<Place> targetNode : result.getNodes()) {
					//
					Node<E> sourceNode = source.getNodes().get(targetNode.getId());

					for (Link<E> link : sourceNode.getLinks()) {
						//
						if ((result.getNodes().get(link.getTargetId()) != null) && (result.getNodes().get(link.getSourceId()) != null)) {
							//
							if (link.isArc()) {
								//
								result.addArc(link.getSourceId(), link.getTargetId(), link.getWeight());

							} else {
								//
								result.addEdge(link.getSourceId(), link.getTargetId(), link.getWeight());
							}
						}
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * This methods census all value of attribute contains the
	 * 
	 * @param geography
	 * @param toponyms
	 * @param weightStep
	 * @return
	 */
	public static Graph<Place> geocodeToponyms(final Geography geography, final StringSet toponyms) {
		Graph<Place> result;

		result = new Graph<Place>();

		if ((geography != null) && (toponyms != null)) {
			//
			for (String toponym : toponyms) {
				//
				Place place = geography.get(toponym);

				if ((place != null) && (place.isGeocoded())) {
					result.addNode(place);
				}
			}
		}

		//
		return result;
	}

	/**
	 * This methods census all value of attribute contains the
	 * 
	 * @param geography
	 * @param toponyms
	 * @param weightStep
	 * @return
	 * @Deprecated old work
	 */
	@Deprecated
	public static Graph<Place> geocodeToponymStrings(final Geography geography, final StringSet toponyms, final double weightStep) {
		Graph<Place> result;

		result = new Graph<Place>();

		if ((geography != null) && (toponyms != null)) {
			//
			for (String toponym : toponyms) {
				//
				List<Node<Place>> nodes = result.getNodesByLabel(toponym).toList();

				if (nodes.isEmpty()) {
					Place place = geography.get(toponym);

					if ((place != null) && (place.isGeocoded())) {
						Node<Place> node = result.addNode(place);
						node.incWeight(weightStep);
					}
				} else {
					Node<Place> node = nodes.get(0);
					node.incWeight(weightStep);
				}
			}
		}

		//
		return result;
	}

}
