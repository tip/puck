package org.tip.puck.statistics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author TIP
 */
public class NumberedFiliationCountLists extends HashMap<Integer, FiliationCounts> implements Iterable<FiliationCounts> {

	private static final long serialVersionUID = 388022876611398486L;

	/**
	 *
	 */
	public NumberedFiliationCountLists() {
		super();
	}

	/**
	 *
	 */
	public NumberedFiliationCountLists(final int initialCapacity) {
		super(initialCapacity);
	}

	/**
	 * 
	 */
	@Override
	public Iterator<FiliationCounts> iterator() {
		Iterator<FiliationCounts> result;

		result = super.values().iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<FiliationCounts> toList() {
		List<FiliationCounts> result;

		result = new ArrayList<FiliationCounts>(values());

		//
		return result;
	}
}
