package org.tip.puckgui;

import java.io.File;

import org.tip.puck.census.workers.CensusCriteria;
import org.tip.puck.graphs.random.RandomCriteria;
import org.tip.puck.net.workers.FileBatchConverterCriteria;
import org.tip.puck.partitions.graphs.RandomAllianceNetworkByAgentSimulationVariationsCriteria;
import org.tip.puck.partitions.graphs.RandomAllianceNetworkByRandomDistributionCriteria;
import org.tip.puck.partitions.graphs.VirtualFieldworkVariationsCriteria;
import org.tip.puck.sequences.workers.SequenceCriteria;
import org.tip.puckgui.views.GenerateRulesCriteria;
import org.tip.puckgui.views.RandomCorpusCriteria;
import org.tip.puckgui.views.RandomPermutationCriteria;
import org.tip.puckgui.views.ReshufflingCriteria;

/**
 * 
 * @author TIP
 * 
 */
public class Preferences {

	public static final String DEFAULT_TERMINOLOGY_SELFNAME = "[self]";
	public static final int DEFAULT_TERMINOLOGY_MAXITERATIONS = 2;

	private Language language;
	private boolean autoLoadLastFile;
	private CensusCriteria censusCriteria;
	private CensusCriteria differentialCensusCriteria;
	private RandomCriteria agentSimulationCriteria;
	private RandomCriteria virtualFieldworkCriteria;
	private RandomAllianceNetworkByAgentSimulationVariationsCriteria agentSimulationVariationsCriteria;
	private VirtualFieldworkVariationsCriteria virtualFieldworkVariationsCriteria;
	private RandomPermutationCriteria randomPermutationCriteria;
	private ReshufflingCriteria reshufflingCriteria;
	private RandomAllianceNetworkByRandomDistributionCriteria randomDistributionCriteria;
	private GenerateRulesCriteria generateRulesCriteria;
	private RandomCorpusCriteria randomCorpusCriteria;
	private InputSettings inputSettings;
	private SequenceCriteria spaceTimeAnalysisCriteria;
	private String flatdb4geonamesDirectory;
	private String terminologySelfName;
	private int terminologyMaxIterations;
	private String terminologyLastDirectory;
	private FileBatchConverterCriteria fileBatchConverterCriteria;

	/**
	 * Sets the default preferences.
	 */
	public Preferences() {
		this.language = Language.ENGLISH;
		this.autoLoadLastFile = false;
		this.censusCriteria = new CensusCriteria();
		this.differentialCensusCriteria = new CensusCriteria();
		this.agentSimulationCriteria = new RandomCriteria();
		this.virtualFieldworkCriteria = new RandomCriteria();
		this.agentSimulationVariationsCriteria = new RandomAllianceNetworkByAgentSimulationVariationsCriteria();
		this.virtualFieldworkVariationsCriteria = new VirtualFieldworkVariationsCriteria();
		this.randomPermutationCriteria = new RandomPermutationCriteria();
		this.reshufflingCriteria = new ReshufflingCriteria();
		this.randomDistributionCriteria = new RandomAllianceNetworkByRandomDistributionCriteria();
		this.generateRulesCriteria = new GenerateRulesCriteria();
		this.randomCorpusCriteria = new RandomCorpusCriteria();
		this.inputSettings = new InputSettings();
		this.spaceTimeAnalysisCriteria = new SequenceCriteria();
		this.flatdb4geonamesDirectory = null;
		this.terminologySelfName = DEFAULT_TERMINOLOGY_SELFNAME;
		this.terminologyMaxIterations = DEFAULT_TERMINOLOGY_MAXITERATIONS;
		this.fileBatchConverterCriteria = new FileBatchConverterCriteria();
	}

	public RandomCriteria getAgentSimulationCriteria() {
		return this.agentSimulationCriteria;
	}

	public RandomAllianceNetworkByAgentSimulationVariationsCriteria getAgentSimulationVariationsCriteria() {
		return this.agentSimulationVariationsCriteria;
	}

	public CensusCriteria getCensusCriteria() {
		return this.censusCriteria;
	}

	public CensusCriteria getDifferentialCensusCriteria() {
		return this.differentialCensusCriteria;
	}

	public FileBatchConverterCriteria getFileBatchConverterCriteria() {
		return this.fileBatchConverterCriteria;
	}

	public String getFlatDB4GeonamesDirectory() {
		return this.flatdb4geonamesDirectory;
	}

	public GenerateRulesCriteria getGenerateRulesCriteria() {
		return this.generateRulesCriteria;
	}

	public InputSettings getInputSettings() {
		return this.inputSettings;
	}

	public Language getLanguage() {
		return this.language;
	}

	public RandomCorpusCriteria getRandomCorpusMASCriteria() {
		return this.randomCorpusCriteria;
	}

	public RandomAllianceNetworkByRandomDistributionCriteria getRandomDistributionCriteria() {
		return this.randomDistributionCriteria;
	}

	public RandomPermutationCriteria getRandomPermutationCriteria() {
		return this.randomPermutationCriteria;
	}

	public ReshufflingCriteria getReshufflingCriteria() {
		return this.reshufflingCriteria;
	}

	public SequenceCriteria getSpaceTimeAnalysisCriteria() {
		return this.spaceTimeAnalysisCriteria;
	}

	public String getTerminologyLastDirectory() {
		return this.terminologyLastDirectory;
	}

	public int getTerminologyMaxIterations() {
		return this.terminologyMaxIterations;
	}

	public String getTerminologySelfName() {
		return this.terminologySelfName;
	}

	public RandomCriteria getVirtualFieldworkCriteria() {
		return this.virtualFieldworkCriteria;
	}

	public VirtualFieldworkVariationsCriteria getVirtualFieldworkVariationsCriteria() {
		return this.virtualFieldworkVariationsCriteria;
	}

	public boolean isAutoLoadLastFile() {
		return this.autoLoadLastFile;
	}

	public void setAgentSimulationCriteria(final RandomCriteria agentSimulationCriteria) {
		this.agentSimulationCriteria = agentSimulationCriteria;
	}

	public void setAgentSimulationVariationsCriteria(final RandomAllianceNetworkByAgentSimulationVariationsCriteria agentSimulationVariationsCriteria) {
		this.agentSimulationVariationsCriteria = agentSimulationVariationsCriteria;
	}

	public void setAutoLoadLastFile(final boolean autoLoadLastFile) {
		this.autoLoadLastFile = autoLoadLastFile;
	}

	public void setCensusCriteria(final CensusCriteria censusCriteria) {
		this.censusCriteria = censusCriteria;
	}

	public void setDifferentialCensusCriteria(final CensusCriteria differentialCensusCriteria) {
		this.differentialCensusCriteria = differentialCensusCriteria;
	}

	public void setFileBatchConverterCriteria(final FileBatchConverterCriteria fileBatchConverterCriteria) {
		this.fileBatchConverterCriteria = fileBatchConverterCriteria;
	}

	public void setFlatDB4GeoNamesDirectory(final String flatdb4geonamesDirectory) {
		this.flatdb4geonamesDirectory = flatdb4geonamesDirectory;
	}

	public void setGenerateRulesCriteria(final GenerateRulesCriteria generateRulesCriteria) {
		this.generateRulesCriteria = generateRulesCriteria;
	}

	public void setInputSettings(final InputSettings inputSettings) {
		this.inputSettings = inputSettings;
	}

	public void setLanguage(final Language language) {
		this.language = language;
	}

	public void setRandomCorpusCriteria(final RandomCorpusCriteria randomCorpusCriteria) {
		this.randomCorpusCriteria = randomCorpusCriteria;
	}

	public void setRandomDistributionCriteria(final RandomAllianceNetworkByRandomDistributionCriteria randomDistributionCriteria) {
		this.randomDistributionCriteria = randomDistributionCriteria;
	}

	public void setRandomPermutationCriteria(final RandomPermutationCriteria randomPermutationCriteria) {
		this.randomPermutationCriteria = randomPermutationCriteria;
	}

	public void setReshufflingCriteria(final ReshufflingCriteria reshufflingCriteria) {
		this.reshufflingCriteria = reshufflingCriteria;
	}

	public void setSpaceTimeAnalysisCriteria(final SequenceCriteria spaceTimeAnalysisCriteria) {
		this.spaceTimeAnalysisCriteria = spaceTimeAnalysisCriteria;
	}

	public void setTerminologyLastDirectory(final File value) {
		if (value != null) {
			setTerminologyLastDirectory(value.getAbsolutePath());
		}
	}

	public void setTerminologyLastDirectory(final String terminologyLastDirectory) {
		this.terminologyLastDirectory = terminologyLastDirectory;
	}

	public void setTerminologyMaxIterations(final Integer value) {
		if (value != null) {
			this.terminologyMaxIterations = value;
		}
	}

	public void setTerminologySelfName(final String value) {
		if (value != null) {
			this.terminologySelfName = value;
		}
	}

	public void setVirtualFieldworkCriteria(final RandomCriteria virtualFieldworkCriteria) {
		this.virtualFieldworkCriteria = virtualFieldworkCriteria;
	}

	public void setVirtualFieldworkVariationsCriteria(final VirtualFieldworkVariationsCriteria virtualFieldworkVariationsCriteria) {
		this.virtualFieldworkVariationsCriteria = virtualFieldworkVariationsCriteria;
	}
}
