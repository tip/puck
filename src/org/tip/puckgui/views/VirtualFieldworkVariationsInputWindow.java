package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.graphs.random.RandomGraphReporter;
import org.tip.puck.partitions.graphs.VirtualFieldworkVariationsCriteria;
import org.tip.puck.report.Report;
import org.tip.puckgui.GroupNetGUI;
import org.tip.puckgui.PuckGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class VirtualFieldworkVariationsInputWindow extends JFrame {

	private static final long serialVersionUID = -4381622113886305530L;
	private static final Logger logger = LoggerFactory.getLogger(VirtualFieldworkVariationsInputWindow.class);

	private JFrame thisJFrame;
	private JPanel contentPane;
	private JButton btnRestoreDefaults;
	private JCheckBox chckbxNumberOfLoops;
	private JCheckBox chckbxNumberOfCircuits;
	private JCheckBox chckbxNumberOfTriangles;
	private JCheckBox chckbxConcentration;
	private JCheckBox chckbxStrengthConcentration;
	private JCheckBox chckbxWeightDistribution;
	private JCheckBox chckbxStrengthDistribution;
	private JSpinner spnrFractionOfNodes;
	private JSpinner spnrFractionOfArcs;
	private JSpinner spnrNumberOfRuns;
	private final ButtonGroup buttonGroupVariable1 = new ButtonGroup();
	private final ButtonGroup buttonGroupVariable2 = new ButtonGroup();
	private JRadioButton rdbtnVariable1Index1;
	private JRadioButton rdbtnVariable1Index2;
	private JRadioButton rdbtnVariable2Index1;
	private JRadioButton rdbtnVariable2Index2;
	private JCheckBox chckbxExtractRepresentative;
	private JSpinner spnrVariable1IntervalFactor;
	private JSpinner spnrVariable2IntervalFactor;
	private JSpinner spnrOutPreference;
	private JSpinner spnrVariable1NumberOfIntervals;
	private JSpinner spnrVariable2NumberOfIntervals;
	private JCheckBox chckbxSymmetry;

	/**
	 * Create the frame.
	 */
	public VirtualFieldworkVariationsInputWindow(final GroupNetGUI groupNetGUI) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(FooReporterInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		setTitle("Virtual Fieldwork Variations");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 518, 500);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});

		this.btnRestoreDefaults = new JButton("Restore Defaults");
		this.btnRestoreDefaults.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Restore Defaults.
				setCriteria(new VirtualFieldworkVariationsCriteria());
			}
		});
		buttonPanel.add(this.btnRestoreDefaults);
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		getRootPane().setDefaultButton(btnLaunch);
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					//
					VirtualFieldworkVariationsCriteria criteria = getCriteria();

					//
					PuckGUI.instance().getPreferences().setVirtualFieldworkVariationsCriteria(criteria);

					//
					Report report = RandomGraphReporter.reportVirtualFieldworkVariations(groupNetGUI.getGroupNet(), criteria);

					//
					groupNetGUI.addReportTab(report);

					//
					dispose();

				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(VirtualFieldworkVariationsInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);

		JPanel panel_targets = new JPanel();
		panel_targets.setBorder(new TitledBorder(null, "Targets", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.contentPane.add(panel_targets, BorderLayout.NORTH);
		panel_targets.setLayout(new BoxLayout(panel_targets, BoxLayout.X_AXIS));

		JPanel panel_1 = new JPanel();
		panel_targets.add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));

		this.chckbxNumberOfLoops = new JCheckBox("Number of loops");
		panel_1.add(this.chckbxNumberOfLoops);

		this.chckbxNumberOfCircuits = new JCheckBox("Number of circuits");
		panel_1.add(this.chckbxNumberOfCircuits);

		this.chckbxNumberOfTriangles = new JCheckBox("Number of triangles");
		panel_1.add(this.chckbxNumberOfTriangles);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		panel_targets.add(horizontalStrut);

		JPanel panel = new JPanel();
		panel_targets.add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		this.chckbxConcentration = new JCheckBox("Concentration");
		panel.add(this.chckbxConcentration);

		this.chckbxWeightDistribution = new JCheckBox("Weight distribution");
		panel.add(this.chckbxWeightDistribution);

		this.chckbxStrengthDistribution = new JCheckBox("Strength distribution");
		panel.add(this.chckbxStrengthDistribution);

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		panel_targets.add(horizontalStrut_2);

		JPanel panel_7 = new JPanel();
		panel_targets.add(panel_7);
		panel_7.setLayout(new BoxLayout(panel_7, BoxLayout.Y_AXIS));

		this.chckbxSymmetry = new JCheckBox("Symmetry");
		panel_7.add(this.chckbxSymmetry);

		this.chckbxStrengthConcentration = new JCheckBox("Strength Concentration");
		panel_7.add(this.chckbxStrengthConcentration);

		Component verticalGlue = Box.createVerticalGlue();
		panel_7.add(verticalGlue);

		JPanel panel_parameters = new JPanel();
		panel_parameters.setBorder(new TitledBorder(null, "Parameters", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.contentPane.add(panel_parameters, BorderLayout.CENTER);
		panel_parameters.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("100dlu"), FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), },
				new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblNumberOfNodes = new JLabel("Fraction of nodes:");
		panel_parameters.add(lblNumberOfNodes, "2, 2");

		this.spnrFractionOfNodes = new JSpinner();
		this.spnrFractionOfNodes.setModel(new SpinnerNumberModel(new Double(1), new Double(0.0), new Double(1.0), new Double(0.05)));
		((JSpinner.NumberEditor) this.spnrFractionOfNodes.getEditor()).getFormat().setMinimumFractionDigits(5);
		panel_parameters.add(this.spnrFractionOfNodes, "4, 2");

		JLabel lblNumberOfArcs = new JLabel("Fraction of arcs:");
		panel_parameters.add(lblNumberOfArcs, "2, 4");

		this.spnrFractionOfArcs = new JSpinner();
		this.spnrFractionOfArcs.setModel(new SpinnerNumberModel(new Double(0.1), new Double(0.0), new Double(1.0), new Double(0.05)));
		((JSpinner.NumberEditor) this.spnrFractionOfArcs.getEditor()).getFormat().setMinimumFractionDigits(5);
		panel_parameters.add(this.spnrFractionOfArcs, "4, 4");

		JLabel lblOutPreference = new JLabel("Out preference:");
		panel_parameters.add(lblOutPreference, "2, 6, right, default");

		this.spnrOutPreference = new JSpinner();
		this.spnrOutPreference.setModel(new SpinnerNumberModel(new Double(0.5), new Double(0.1), null, new Double(0.1)));
		((JSpinner.NumberEditor) this.spnrOutPreference.getEditor()).getFormat().setMinimumFractionDigits(1);
		panel_parameters.add(this.spnrOutPreference, "4, 6");

		JLabel lblNumberOfRuns = new JLabel("Number of runs:");
		panel_parameters.add(lblNumberOfRuns, "2, 8");

		this.spnrNumberOfRuns = new JSpinner();
		this.spnrNumberOfRuns.setModel(new SpinnerNumberModel(new Integer(100), new Integer(1), null, new Integer(1)));
		panel_parameters.add(this.spnrNumberOfRuns, "4, 8");

		JPanel panel_2 = new JPanel();
		panel_parameters.add(panel_2, "2, 10, 5, 1, fill, fill");
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.X_AXIS));

		JPanel panel_firstVariable = new JPanel();
		panel_firstVariable.setBorder(new TitledBorder(null, "First variable", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.add(panel_firstVariable);
		panel_firstVariable.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblIndex = new JLabel("Index:");
		panel_firstVariable.add(lblIndex, "2, 2, right, default");

		JPanel panel_3 = new JPanel();
		panel_firstVariable.add(panel_3, "4, 2, fill, fill");
		panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.Y_AXIS));

		this.rdbtnVariable1Index1 = new JRadioButton("variable 1");
		this.buttonGroupVariable1.add(this.rdbtnVariable1Index1);
		this.rdbtnVariable1Index1.setSelected(true);
		panel_3.add(this.rdbtnVariable1Index1);

		this.rdbtnVariable1Index2 = new JRadioButton("variable 2");
		this.buttonGroupVariable1.add(this.rdbtnVariable1Index2);
		panel_3.add(this.rdbtnVariable1Index2);

		JPanel panel_5 = new JPanel();
		panel_firstVariable.add(panel_5, "2, 4, right, fill");
		panel_5.setLayout(new BoxLayout(panel_5, BoxLayout.Y_AXIS));

		JLabel lblNumberOf = new JLabel("Number of");
		panel_5.add(lblNumberOf);

		JLabel lblIntervals = new JLabel("intervals:");
		panel_5.add(lblIntervals);

		this.spnrVariable1NumberOfIntervals = new JSpinner();
		this.spnrVariable1NumberOfIntervals.setModel(new SpinnerNumberModel(new Integer(5), new Integer(0), null, new Integer(1)));
		panel_firstVariable.add(this.spnrVariable1NumberOfIntervals, "4, 4");

		JLabel lblIntervalFactor = new JLabel("Interval factor:");
		panel_firstVariable.add(lblIntervalFactor, "2, 6, right, default");

		this.spnrVariable1IntervalFactor = new JSpinner();
		this.spnrVariable1IntervalFactor.setModel(new SpinnerNumberModel(new Integer(10), new Integer(1), null, new Integer(1)));
		panel_firstVariable.add(this.spnrVariable1IntervalFactor, "4, 6");

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		panel_2.add(horizontalStrut_1);

		JPanel panel_secondVariable = new JPanel();
		panel_secondVariable.setBorder(new TitledBorder(null, "Second variable", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.add(panel_secondVariable);
		panel_secondVariable.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblIndex_1 = new JLabel("Index:");
		panel_secondVariable.add(lblIndex_1, "2, 2, right, default");

		JPanel panel_4 = new JPanel();
		panel_secondVariable.add(panel_4, "4, 2, fill, fill");
		panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.Y_AXIS));

		this.rdbtnVariable2Index1 = new JRadioButton("variable 1");
		this.buttonGroupVariable2.add(this.rdbtnVariable2Index1);
		panel_4.add(this.rdbtnVariable2Index1);

		this.rdbtnVariable2Index2 = new JRadioButton("variable 2");
		this.buttonGroupVariable2.add(this.rdbtnVariable2Index2);
		this.rdbtnVariable2Index2.setSelected(true);
		panel_4.add(this.rdbtnVariable2Index2);

		JPanel panel_6 = new JPanel();
		panel_secondVariable.add(panel_6, "2, 4, right, fill");
		panel_6.setLayout(new BoxLayout(panel_6, BoxLayout.Y_AXIS));

		JLabel lblNumberOf_1 = new JLabel("Number of");
		panel_6.add(lblNumberOf_1);

		JLabel lblIntervals_1 = new JLabel("intervals:");
		panel_6.add(lblIntervals_1);

		this.spnrVariable2NumberOfIntervals = new JSpinner();
		this.spnrVariable2NumberOfIntervals.setModel(new SpinnerNumberModel(new Integer(5), new Integer(0), null, new Integer(1)));
		panel_secondVariable.add(this.spnrVariable2NumberOfIntervals, "4, 4");

		JLabel lblIntervalFactor_1 = new JLabel("Interval factor:");
		panel_secondVariable.add(lblIntervalFactor_1, "2, 6, right, default");

		this.spnrVariable2IntervalFactor = new JSpinner();
		this.spnrVariable2IntervalFactor.setModel(new SpinnerNumberModel(new Integer(10), new Integer(1), null, new Integer(1)));
		panel_secondVariable.add(this.spnrVariable2IntervalFactor, "4, 6");

		this.chckbxExtractRepresentative = new JCheckBox("Extract a representative network");
		panel_parameters.add(this.chckbxExtractRepresentative, "2, 10, 5, 1");

		// //////////////////
		setCriteria(PuckGUI.instance().getPreferences().getVirtualFieldworkVariationsCriteria());
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public VirtualFieldworkVariationsCriteria getCriteria() throws PuckException {
		VirtualFieldworkVariationsCriteria result;

		//
		result = new VirtualFieldworkVariationsCriteria();

		//
		result.setNumberOfLoopsChecked(this.chckbxNumberOfLoops.isSelected());
		result.setNumberOfCircuitsChecked(this.chckbxNumberOfCircuits.isSelected());
		result.setNumberOfTrianglesChecked(this.chckbxNumberOfTriangles.isSelected());
		result.setConcentrationChecked(this.chckbxConcentration.isSelected());
		result.setStrengthConcentrationChecked(this.chckbxStrengthConcentration.isSelected());
		result.setWeightDistributionChecked(this.chckbxWeightDistribution.isSelected());
		result.setStrengthDistributionChecked(this.chckbxStrengthDistribution.isSelected());
		result.setSymmetryChecked(this.chckbxSymmetry.isSelected());

		//
		result.setFractionOfNodes((Double) this.spnrFractionOfNodes.getValue());
		result.setFractionOfArcs((Double) this.spnrFractionOfArcs.getValue());
		result.setOutPreference((Double) this.spnrOutPreference.getValue());
		result.setNumberOfRuns((Integer) this.spnrNumberOfRuns.getValue());

		//
		int index1;
		if (this.rdbtnVariable1Index1.isSelected()) {
			index1 = 0;
		} else if (this.rdbtnVariable1Index2.isSelected()) {
			index1 = 1;
		} else {
			throw PuckExceptions.INVALID_PARAMETER.create("Bad value");
		}
		result.setFirstVariableIndex(index1);

		result.setFirstVariableIntervalFactor((Integer) this.spnrVariable1IntervalFactor.getValue());

		//
		int index2;
		if (this.rdbtnVariable2Index1.isSelected()) {
			index2 = 0;
		} else if (this.rdbtnVariable2Index2.isSelected()) {
			index2 = 1;
		} else {
			throw PuckExceptions.INVALID_PARAMETER.create("Bad value");
		}
		result.setSecondVariableIndex(index2);

		result.setSecondVariableIntervalFactor((Integer) this.spnrVariable2IntervalFactor.getValue());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public void setCriteria(final VirtualFieldworkVariationsCriteria source) {
		//
		if (source != null) {
			//
			this.chckbxNumberOfLoops.setSelected(source.isNumberOfLoopsChecked());
			this.chckbxNumberOfCircuits.setSelected(source.isNumberOfCircuitsChecked());
			this.chckbxNumberOfTriangles.setSelected(source.isNumberOfTrianglesChecked());
			this.chckbxConcentration.setSelected(source.isConcentrationChecked());
			this.chckbxWeightDistribution.setSelected(source.isWeightDistributionChecked());
			this.chckbxStrengthDistribution.setSelected(source.isStrengthDistributionChecked());
			this.chckbxSymmetry.setSelected(source.isSymmetryChecked());

			//
			this.spnrFractionOfNodes.setValue(source.getFractionOfNodes());
			this.spnrFractionOfArcs.setValue(source.getArcWeightFraction());
			this.spnrOutPreference.setValue(source.getOutPreference());
			this.spnrNumberOfRuns.setValue(source.getNumberOfRuns());

			//
			this.rdbtnVariable1Index1.setSelected(false);
			this.rdbtnVariable1Index2.setSelected(false);
			switch (source.getFirstVariableIndex()) {
				case 0:
					this.rdbtnVariable1Index1.setSelected(true);
				break;
				case 1:
					this.rdbtnVariable1Index2.setSelected(true);
				break;
			}

			//
			this.spnrVariable1NumberOfIntervals.setValue(source.getFirstVariableNumberOfIntervals());
			this.spnrVariable1IntervalFactor.setValue(source.getFirstVariableIntervalFactor());

			//
			this.rdbtnVariable2Index1.setSelected(false);
			this.rdbtnVariable2Index2.setSelected(false);
			switch (source.getSecondVariableIndex()) {
				case 0:
					this.rdbtnVariable2Index1.setSelected(true);
				break;
				case 1:
					this.rdbtnVariable2Index2.setSelected(true);
				break;
			}

			//
			this.spnrVariable2NumberOfIntervals.setValue(source.getSecondVariableNumberOfIntervals());
			this.spnrVariable2IntervalFactor.setValue(source.getSecondVariableIntervalFactor());
		}
	}
}
