package org.tip.puck.util;

import java.util.Iterator;

/**
 * 
 * 
 * @author TIP
 */
public interface Numberables<NumberableType extends Numberable> extends Iterable<NumberableType> {

	/**
	 * 
	 */
	void clear();

	/**
	 * 
	 * @param id
	 * @return
	 */
	boolean containsId(final int id);

	/**
	 * 
	 * @param id
	 * @return
	 */
	NumberableType getById(final int id);

	/**
	 * 
	 * @return
	 */
	Integer[] getIds();

	/**
	 * 
	 * @return
	 */
	boolean isEmpty();

	/**
	 * This methods returns an iterator of numberable elements.
	 */
	@Override
	Iterator<NumberableType> iterator();

	/**
	 * 
	 */
	void put(final NumberableType object);

	/**
	 * 
	 * @param id
	 */
	void removeById(final int id);

	/**
	 * 
	 * @return
	 */
	int size();

}
