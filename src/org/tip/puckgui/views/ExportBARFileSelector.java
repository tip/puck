package org.tip.puckgui.views;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.util.ToolBox;
import org.tip.puckgui.util.GenericFileFilter;

/**
 * 
 * @author cpm
 */
public class ExportBARFileSelector extends JFileChooser {

	private static final long serialVersionUID = -8776305919634043940L;

	private static final Logger logger = LoggerFactory.getLogger(ExportBARFileSelector.class);

	/**
	 * 
	 */
	public ExportBARFileSelector(final File targetFile) {
		super();

		//
		File file;
		if ((targetFile == null) || (StringUtils.isBlank(targetFile.getAbsolutePath()))) {
			//
			file = null;

		} else {
			//
			file = ToolBox.setExtension(targetFile, ".txt");
		}

		//
		setSelectedFile(file);
		setDialogTitle("Export to BAR format");
		setFileSelectionMode(JFileChooser.FILES_ONLY);
		setAcceptAllFileFilterUsed(false);
		setApproveButtonText("Export");
		setDialogType(CUSTOM_DIALOG);

		//
		GenericFileFilter defaultFileFilter = new GenericFileFilter("BAR format files (*.ods, *.txt, *.xls)", "ods", "txt", "xls");
		addChoosableFileFilter(defaultFileFilter);
		addChoosableFileFilter(new GenericFileFilter("OpenDocumentFormat Spreadsheet (*.ods)", "ods"));
		addChoosableFileFilter(new GenericFileFilter("Text (*.txt)", "txt"));
		addChoosableFileFilter(new GenericFileFilter("Microsoft Excel (*.xls)", "xls"));
		setFileFilter(defaultFileFilter);
	}

	/**
	 * 
	 */
	@Override
	public void approveSelection() {
		//
		File targetFile = getSelectedFile();

		logger.debug("getCurrentDirectory(): {}", getCurrentDirectory());
		logger.debug("selectedFile={}", targetFile);

		if (StringUtils.isBlank(ToolBox.getExtension(targetFile))) {
			targetFile = new File(targetFile.getAbsolutePath() + ".txt");
		}

		//
		if (!StringUtils.endsWithAny(ToolBox.getExtension(targetFile), "ods", "txt", "xls")) {
			//
			targetFile = new File(targetFile.getAbsolutePath() + ".txt");
			setSelectedFile(targetFile);
		}

		//
		if (targetFile.exists()) {
			//
			if (ConfirmOverwriteDialog.showDialog(null)) {
				//
				logger.debug("Overwrite");
				super.approveSelection();

			} else {
				//
				logger.debug("Cancel overwrite");
			}
		} else {
			//
			super.approveSelection();
		}
	}

	/**
	 * 
	 */
	@Override
	public void cancelSelection() {
		//
		logger.debug("Cancel selection.");
		super.cancelSelection();
	}

	/**
	 * 
	 */
	@Override
	public void setSelectedFile(final File file) {
		//
		super.setSelectedFile(file);

		System.out.println("==== SET SELECTED FILE=================");
		System.out.println("SELECED FILE " + file);
	}

	/**
	 * 
	 */
	@Override
	public void setVisible(final boolean visible) {
		//
		super.setVisible(visible);

		if (!visible) {
			//
			resetChoosableFileFilters();
		}
	}

	/**
	 * This method is the main one of the selector.
	 * 
	 * @param parent
	 * @return
	 */
	public static File showSelectorDialog(final Component parent, final File targetFile) {
		File result;

		//
		ExportBARFileSelector selector = new ExportBARFileSelector(targetFile);

		//
		if (selector.showDialog(parent, null) == JFileChooser.APPROVE_OPTION) {
			//
			logger.debug("getCurrentDirectory(): {}", selector.getCurrentDirectory());
			logger.debug("getSelectedFile() : {}", selector.getSelectedFile());
			result = selector.getSelectedFile();

		} else {
			//
			result = null;
		}

		//
		return result;
	}
}
