package oldcore.trash;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.Gender;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.relations.Roles;
import org.tip.puck.util.PuckUtils;

public class RelationModelStatistics {
	
/*	Roles roles;
	RoleDefinitions roleDefinitions;
	Graph<Role> graph;*/
	
	public RelationModelStatistics (RelationModel model){
		
/*		graph = RelationModelMaker.relationModelGraph(model);
		roles = model.roles();
		roleDefinitions = model.roleDefinitions(); //.neutralize();*/

	}
	
/*	public List<Role> sortedRoles() {
		List<Role> result;
		
		result = roles;
		
		Collections.sort(result);
		
		return result;
	}



	public int termCount(){
		int result;
		
		result = roles.size();
		
		return result;
	}
	
	public List<Integer> generations(Role role){
		return generations(role, 0, 3, new Roles());
	}
	
	public Map<Role,List<Gender>> genderMap(){
		Map<Role,List<Gender>> result;
		
		result = new HashMap<Role,List<Gender>>();
		
		for (Role role : roles){
			result.put(role, genders(role));
		}
		
		//
		return result;
	}
	
	public List<Gender> genders(Role role){
		List<Gender> result;
		
		result = genders(role, 0, 3);
		
		//
		return result;
	}
		
	public List<Gender> genders (Role role, int iterations, int maxIterations){
		List<Gender> result;

		result = new ArrayList<Gender>();
		
		RoleDefinitions definitions = roleDefinitions.getDefinitions(role);
		
		for (RoleDefinition definition : definitions){
			if (!definition.alterGender().isUnknown()){
				if (!result.contains(definition.alterGender())){
					result.add(definition.alterGender());
				}
			} else if (definition.composition()!=null && iterations<maxIterations){
				if (roleDefinitions.isSpouseRole(definition.composition().get(1))){
					for (Gender gender : genders(definition.composition().get(0),iterations+1,maxIterations)){
						if (!result.contains(gender.invert())){
							result.add(gender.invert());
						}
					}
				} else {
					for (Gender gender : genders(definition.composition().get(1),iterations+1,maxIterations)){
						if (!result.contains(gender)){
							result.add(gender);
						}
					}
				}
			}
			
/*			if (definition.inversion()!=null){
				for (RoleDefinition inverseDefinition : roleDefinitions.getDefinitions(definition.inversion())){
					if (!result.contains(inverseDefinition.egoGender())){
						result.add(inverseDefinition.egoGender());
					}
				}
			}
		}
		//
		return result;
		
	}
	
	public List<Integer> generations(Role role, int iterations, int maxIterations, Roles visitedRoles){
		List<Integer> result;
		
		result = new ArrayList<Integer>();

		if (iterations<=maxIterations && !visitedRoles.contains(role)){
		
			RoleDefinitions definitions = roleDefinitions.getNonRecursiveDefinitions(role);
			
			for (RoleDefinition definition : definitions){
				
				if (definition.primary() == Primary.PARENT){
					if (!result.contains(1)){
						result.add(1);
					}
				} else if (definition.primary() == Primary.SIBLING || definition.primary() == Primary.SPOUSE){
					if (!result.contains(0)){
						result.add(0);
					}
				} 
				if (definition.inversion() != null){
					for (int inverseGeneration : generations(definition.inversion())){
						if (!result.contains(-inverseGeneration)){
							result.add(-inverseGeneration);
						}
					}
				} 
				
				if (definition.composition()!= null){
//					visitedRoles.add(role);
					for (int firstGeneration : generations(definition.composition().get(0),iterations+1, maxIterations, visitedRoles)){
						for (int secondGeneration : generations(definition.composition().get(1),iterations+1, maxIterations, visitedRoles)){
							if (!result.contains(firstGeneration+secondGeneration)){
//								if (role.getName().equals("kamuru")) System.out.println(definition.composition().get(0)+" "+definition.composition().get(1)+" "+firstGeneration+" "+secondGeneration);
								result.add(firstGeneration+secondGeneration);
							}
						}
					}
//					visitedRoles.remove(role);
				}
			}
		}
	
		Collections.sort(result);
		//
		return result;
		
	}
	
	public List<Integer> generations(Role role, Roles preDefined){
		List<Integer> result;
		
		result = new ArrayList<Integer>();
		
		RoleDefinitions definitions = roleDefinitions.getNonRecursiveDefinitions(role);
		
		for (RoleDefinition definition : definitions){
			if (definition.primary() == Primary.PARENT){
				if (!result.contains(1)){
					result.add(1);
				}
			} else if (definition.primary() == Primary.SIBLING || definition.primary() == Primary.SPOUSE){
				if (!result.contains(0)){
					result.add(0);
				}
			} 
			if (definition.inversion() != null){
				for (int inverseGeneration : generations(definition.inversion())){
					if (!result.contains(-inverseGeneration)){
						result.add(-inverseGeneration);
					}
				}
			} 
			
			if (definition.composition()!= null && !definition.composition().contains(role)){
				boolean recursive = false;
				for (Role preRole : preDefined){
					if (definition.composition().contains(preRole)){
						recursive = true;
						break;
					}
				}
				if (!recursive){
					preDefined.add(role);
					for (int firstGeneration : generations(definition.composition().get(0),preDefined)){
						for (int secondGeneration : generations(definition.composition().get(1),preDefined)){
							if (!result.contains(firstGeneration+secondGeneration)){
								result.add(firstGeneration+secondGeneration);
							}
						}
					}
					preDefined.remove(role);
				}
			}
		}
		Collections.sort(result);
		//
		return result;
		
	}
	
	public List<String> links (Role role, int maxIterations){
		return links (role, 0, maxIterations);
	}
	
	public List<String> links (Role role, int iterations, int maxIterations){
		List<String> result;
		
		result = new ArrayList<String>();
		
		if (iterations>maxIterations){
			return result;
		}

		RoleDefinitions definitions = roleDefinitions.getDefinitions(role);
		
		for (RoleDefinition definition : definitions){
			
			Gender egoGender = definition.egoGender();
			
			String gender = "";
			if (!definition.egoGender().isUnknown()){
				gender = egoGender.toSymbol()+"";
			} 
			
			if (definition.primary()!=null){
				String ps = definition.getPrimaryAsString();
				
				if (StringUtils.isNotBlank(ps) && !result.contains(ps)){
					result.add(ps);
				}
			} 
			
			if (definition.inversion()!=null){
				String ps = definition.getInversePrimaryAsString(roleDefinitions);
				if (StringUtils.isNotBlank(ps) && !result.contains(ps)){
					result.add(ps);
				}
			}
			if (definition.composition()!= null){

				for (String firstLink : links(definition.composition().get(0),iterations+1, maxIterations)){
					
					char firstGender = firstLink.charAt(0);
					
					if (Gender.isGenderSymbol(firstGender)){
						firstLink = firstLink.substring(1);
						if (!egoGender.isUnknown() && firstGender!=egoGender.toSymbol()){
							continue;
						}
					}
					
					if ((firstLink.charAt(0)=='H' && egoGender.isMale()) || (firstLink.charAt(0)=='W' && egoGender.isFemale())){
							// No homosexual marriage for terminology generation (optionalize!)
							continue;
					}
					
					char lastLetter = firstLink.charAt(firstLink.length()-1);
					
					for (String secondLink : links(definition.composition().get(1),maxIterations, maxIterations)){
						
						char secondGender = secondLink.charAt(0);
						if (Gender.isGenderSymbol(secondGender)){
							if (lastLetter=='a' || lastLetter=='b' || lastLetter=='h' || lastLetter=='p' || Gender.matchesChar(lastLetter, secondGender)){
								secondLink = secondLink.substring(1);
							} else {
								System.err.println("Gender mismatch "+firstLink+" "+secondLink+" "+definition);
							}
						} else if (Gender.sameSexChar(lastLetter, secondGender)){
							// No homosexual marriage for terminology generation (optionalize!)
							continue;
						}

//						if (role.getName().equals("waputyu")) System.out.println(gender+" "+firstLink+" "+secondLink+" "+definition.composition().get(0)+" "+definition.composition().get(1)+" "+egoGender);

						String link = firstLink+secondLink;
						if (firstLink.charAt(0)!='H' && firstLink.charAt(0)!='W'){
							link = gender+link;
						}
						
						link = link.replaceAll("eBeB", "eB").replaceAll("yBeB", "eB").replaceAll("eZeB", "eB").replaceAll("yZeB", "eB").replaceAll("eSbeB", "eB").replaceAll("ySbeB", "eB").replaceAll("eBeZ", "eZ").replaceAll("yBeZ", "eZ").replaceAll("eZeZ", "eZ").replaceAll("yZeZ", "eZ").replaceAll("eSbeZ", "eZ").replaceAll("ySbeZ", "eZ").replaceAll("eByB", "yB").replaceAll("yByB", "yB").replaceAll("eZyB", "yB").replaceAll("yZyB", "yB").replaceAll("eSbyB", "yB").replaceAll("ySbyB", "yB").replaceAll("eByZ", "yZ").replaceAll("yByZ", "yZ").replaceAll("eZyZ", "yZ").replaceAll("yZyZ", "yZ").replaceAll("eSbyZ", "yZ").replaceAll("ySbyZ", "yZ").replaceAll("eBeSb", "eSb").replaceAll("yBeSb", "eSb").replaceAll("eZeSb", "eSb").replaceAll("yZeSb", "eSb").replaceAll("eSbeSb", "eSb").replaceAll("ySbeSb", "eSb").replaceAll("eBySb", "ySb").replaceAll("yBySb", "ySb").replaceAll("eZySb", "ySb").replaceAll("yZySb", "ySb").replaceAll("eSbySb", "ySb").replaceAll("ySbySb", "ySb");
						link = link.replaceAll("BB", "B").replaceAll("SbB", "B").replaceAll("ZB", "B").replaceAll("BZ", "Z").replaceAll("SbZ", "Z").replaceAll("ZZ", "Z").replaceAll("BSb", "Sb").replaceAll("SbSb", "Sb").replaceAll("ZSb", "Sb");
						link = link.replaceAll("eBF", "F").replaceAll("eSbF", "F").replaceAll("eZF", "F").replaceAll("eBM", "M").replaceAll("eSbM", "M").replaceAll("eZM", "M").replaceAll("yBF", "F").replaceAll("ySbF", "F").replaceAll("yZF", "F").replaceAll("yBM", "M").replaceAll("ySbM", "M").replaceAll("yZM", "M");
						link = link.replaceAll("BF", "F").replaceAll("SbF", "F").replaceAll("ZF", "F").replaceAll("BM", "M").replaceAll("SbM", "M").replaceAll("ZM", "M");
						link = link.replaceAll("SeB", "S").replaceAll("DeB", "S").replaceAll("CheB", "S").replaceAll("SeZ", "D").replaceAll("DeZ", "D").replaceAll("CheZ", "D").replaceAll("SeSb", "Ch").replaceAll("DeSb", "Ch").replaceAll("CheSb", "Ch");
						link = link.replaceAll("SyB", "S").replaceAll("DyB", "S").replaceAll("ChyB", "S").replaceAll("SyZ", "D").replaceAll("DyZ", "D").replaceAll("ChyZ", "D").replaceAll("SySb", "Ch").replaceAll("DySb", "Ch").replaceAll("ChySb", "Ch");
						link = link.replaceAll("SB", "S").replaceAll("DB", "S").replaceAll("ChB", "S").replaceAll("SZ", "D").replaceAll("DZ", "D").replaceAll("ChZ", "D").replaceAll("SSb", "Ch").replaceAll("DSb", "Ch").replaceAll("ChSb", "Ch");

						link = link.replaceAll("ZM", "M").replaceAll("BM","M").replaceAll("ZF", "F").replaceAll("BF","F");
						link = link.replaceAll("DF", "H").replaceAll("SF", "H").replaceAll("DM", "W").replaceAll("SM", "W");

						// Remove homosexual marriages
						String[] sameSexUnions = new String[]{"MW","DW","ZW","WW","FH","SH","BH","HH"};
						boolean containsSameSexUnions = false;
						for (String sameSexUnion : sameSexUnions){
							if (link.contains(sameSexUnion)){
								containsSameSexUnions = true;
								break;
							}
						}
						
						if (!result.contains(link) && !containsSameSexUnions){
							result.add(link);
						}
					}
				}
			}
		}
		
		
		
		//Clear sibling links
		for (String link : new ArrayList<String>(result)){
			String reducedLink = link.replaceAll("MS", "B").replaceAll("FS", "B").replaceAll("MD", "Z").replaceAll("FD", "Z").replaceAll("FCh", "Sb").replaceAll("MCh", "Sb");
			if (result.contains(reducedLink) && !link.equals(reducedLink)){
				result.remove(link);
			} else {
				for (String otherLink : new ArrayList<String>(result)){
					String reducedOtherLink = otherLink.replaceAll("MS", "B").replaceAll("FS", "B").replaceAll("MD", "Z").replaceAll("FD", "Z").replaceAll("FCh", "Sb").replaceAll("MCh", "Sb");
					if (!link.equals(otherLink) && reducedLink.equals(reducedOtherLink)){
						result.remove(otherLink);
						if (!result.contains(reducedLink)){
							result.add(reducedLink);
						}
					}
				}
			}
			
			if (reducedLink.equals("Sb") && PuckUtils.containsStrings(result, "B;Z;eB;yB;eZ;yZ") && !link.equals(reducedLink)){
				result.remove(link);
			}
			if (reducedLink.equals("B") && PuckUtils.containsStrings(result, "eB;yB") && !link.equals(reducedLink)){
				result.remove(link);
			}
			if (reducedLink.equals("Z") && PuckUtils.containsStrings(result, "eZ;yZ") && !link.equals(reducedLink)){
				result.remove(link);
			}
			reducedLink = reducedLink.replaceAll("yB", "B").replaceAll("eB", "B").replaceAll("yZ", "Z").replaceAll("eZ", "Z").replaceAll("ySb", "Sb").replaceAll("eSb", "Sb");
			if (result.contains(reducedLink) && !link.equals(reducedLink)){
				result.remove(link);
			}
		}
		
		// Neutralize
		for (String link : new ArrayList<String>(result)){
			if (Gender.isGenderSymbol(link.charAt(0))){
				if (result.contains(link.substring(1))){
					result.remove(link);
				} else if (result.contains(Gender.inverseGenderSymbol(link.charAt(0))+link.substring(1))){
					result.remove(link);
					result.add(link.substring(1));
				}
			}
		}
		
		Collections.sort(result);
		//
		return result;
	}
	
	private boolean isParent(Role role){
		boolean result;
		
		result = false;
		
		for (RoleDefinition definition : roleDefinitions){
			if (definition.role().equals(role) && definition.primary()==Primary.PARENT){
				result = true;
				break;
			}
		}
		
		//
		return result;
	}
	
	private boolean isChild(Role role){
		boolean result;
		
		result = false;
		
		for (RoleDefinition definition : roleDefinitions){
			if (definition.role().equals(role) && isParent(definition.inversion())){
				result = true;
				break;
			}
		}
		
		//
		return result;
	}
	
	private boolean isSibling(Role role){
		boolean result;
		
		result = false;
		
		for (RoleDefinition definition : roleDefinitions){
			if (definition.role().equals(role) && definition.primary()==Primary.SIBLING){
				result = true;
				break;
			}
		}
		
		//
		return result;
	}
	
	public boolean hasSameGender(Role alpha, Role beta){
		boolean result;
		
		result = false;
		
		for (Gender gender : genders(alpha)){
			if (genders(beta).contains(gender)){
				result = true;
				break;
			}
		}
		//
		return result;
	}
	
	private boolean isAscendingParallel(Role role){
		boolean result;
		
		result = false;
		
		for (RoleDefinition definition : roleDefinitions){
			if (definition.role().equals(role) && definition.composition()!=null){
				
				Role first = definition.composition().get(0);
				Role second = definition.composition().get(1);

				if (isParent(first) && isSibling(second) && hasSameGender(first,second)) 
				
				result = true;
				break;
			}
		}
		
		//
		return result;
	}
	
	private boolean isAscendingCross(Role role){
		boolean result;
		
		result = false;
		
		for (RoleDefinition definition : roleDefinitions){
			if (definition.role().equals(role) && definition.composition()!=null){
				
				Role first = definition.composition().get(0);
				Role second = definition.composition().get(1);

				if (isParent(first) && isSibling(second) && !hasSameGender(first,second)) {
					result = true;
					break;
				}
				
			}
		}
		
		//
		return result;
	}
	
	private boolean isCollateralParallel(Role role){
		boolean result;
		
		result = false;
		
		for (RoleDefinition definition : roleDefinitions){
			if (definition.role().equals(role) && definition.composition()!=null){
				
				Role first = definition.composition().get(0);
				Role second = definition.composition().get(1);

				if (isAscendingParallel(first) && isChild(second)) {
					result = true;
					break;
				}
			}
		}
		
		//
		return result;
	}

	private boolean isCollateralCross(Role role){
		boolean result;
		
		result = false;
		
		for (RoleDefinition definition : roleDefinitions){
			if (definition.role().equals(role) && definition.composition()!=null){
				
				Role first = definition.composition().get(0);
				Role second = definition.composition().get(1);

				if (isAscendingCross(first) && isChild(second)) 
				
				result = true;
				break;
			}
		}
		
		//
		return result;
	}
	

	public String cousinClassification (){
		String result;
		
		result = null;
		
		boolean lineal = false;
		boolean merging = false;
		
		for (Role role : roles){
			if (isCollateralParallel(role) && isSibling(role)){
				merging = true;
			}
			if (isCollateralParallel(role) && isCollateralCross(role)){
				lineal = true;
			}
		}
		
		if (lineal){
			if (merging){
				result = "GENERATIONAL";
			} else {
				result = "LINEAL";
			}
		} else {
			if (merging){
				result = "BIFURCATE-MERGING";
			} else {
				result = "BIFURCATE-COLLATERAL";
			}
		}
		//
		return result;
	}
	
	public Roles undefinedRoles(){
		Roles result;
		
		result = new Roles();
		
		for (Role role : roles){
			if (roleDefinitions.getDefinitions(role).size()==0){
				if (!result.contains(role)){
					result.add(role);
				}
			}
		}
		
		Collections.sort(result);
		
		//
		return result;
	}
	
	public Roles uncomposableRoles(){
		Roles result;
		
		result = roleDefinitions.getUncomposableRoles();
		
		Collections.sort(result);
		
		//
		return result;
	}
	
	public RoleDefinitions recursiveDefinitions(){
		
		return roleDefinitions.recursiveDefinitions();
	}
	
	public List<String> nullFactors(){
		List<String> result;
		
		result = new ArrayList<String>();
		
		for (Role role : roles){
			for (RoleDefinition definition : roleDefinitions.getDefinitions(role)){
				if (RoleDefinitions.isNullOrHasNullFactor(definition.composition())){
					if (!result.contains(definition.composition())){
						if (definition.composition()!=null){
							result.add(role+":\t"+definition.composition().get(0)+"\t"+definition.composition().get(1));
						}
					}
				}
			}
		}
		
		Collections.sort(result);
		
		//
		return result;
	}*/
	
	
	
	



}
