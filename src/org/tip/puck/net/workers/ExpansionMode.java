package org.tip.puck.net.workers;

import fr.devinsy.util.StringList;

/**
 * The {@code SnowballCriteria} enum represents a value list of Expansion Mode.
 * 
 * @author TIP
 */
public enum ExpansionMode {
	NONE,
	ALL,
	RELATED,
	KIN,
	PARENT,
	CHILD,
	SPOUSE;

	/**
	 * 
	 * @return
	 */
	public static StringList getActiveLabels() {
		StringList result;

		//
		result = new StringList();

		//
		for (ExpansionMode mode : ExpansionMode.values()) {
			//
			if (mode != NONE) {
				//
				result.add(mode.toString());
			}
		}

		//
		return result;
	}
}
