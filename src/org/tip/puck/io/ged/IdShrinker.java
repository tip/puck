package org.tip.puck.io.ged;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;

import fr.devinsy.util.FileTools;
import fr.devinsy.util.StringList;

/**
 * This class represents a GED File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class IdShrinker {

	private static final Logger logger = LoggerFactory.getLogger(IdShrinker.class);
	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	public static final Pattern ID_PATTERN = Pattern.compile("^(.*@)((\\D*)(\\d+)(\\D*))(@.*)$");

	/**
	 * 
	 * @param source
	 * @return
	 * @throws IOException
	 */
	public static boolean containsLongId(final File source) throws IOException {
		boolean result;

		//
		if ((source == null) || (!source.getCanonicalPath().toLowerCase().endsWith(".ged"))) {
			//
			result = false;

		} else {
			//
			StringList lines = FileTools.loadToStringList(source);

			boolean ended = false;
			Iterator<String> iterator = lines.iterator();
			result = false;
			while (!ended) {
				if (iterator.hasNext()) {
					//
					String line = iterator.next();

					//
					if (line.matches("^.*@[IFif][a-zA-Z]*\\d{9,}@.*$")) {
						ended = true;
						result = true;
					}
				} else {
					ended = true;
					result = false;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Loads a GED file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 * @throws IOException
	 */
	public static File shrink(final File file) throws PuckException, IOException {
		File result;

		//
		StringList sourceLines = FileTools.loadToStringList(file);
		StringList targetLines = new StringList(sourceLines.size());

		//
		HashMap<String, String> map = new HashMap<String, String>();

		//
		int lastId = 0;
		int lastIndividualId = 0;
		int lastFamilyId = 0;
		for (String sourceLine : sourceLines) {
			//
			Matcher matcher = ID_PATTERN.matcher(sourceLine);

			//
			if (matcher.find()) {
				// for (int tokenIndex = 0; tokenIndex <= matcher.groupCount();
				// tokenIndex++) {
				// logger.debug(tokenIndex + " " + matcher.group(tokenIndex));
				// }

				//
				String sourceId = matcher.group(2);
				String prefix = matcher.group(3);
				String value = matcher.group(4);
				String suffix = matcher.group(5);

				//
				// logger.debug("[sourceId={}][prefix={}][value={}][suffix={}]",
				// sourceId, prefix, value, suffix);

				//
				String targetId = map.get(sourceId);
				if (targetId == null) {
					//
					if ((prefix.length() == 0) || (prefix.toUpperCase().startsWith("F")) || (prefix.toUpperCase().startsWith("I"))) {

						if (prefix.toUpperCase().startsWith("F")) {
							lastFamilyId += 1;
							targetId = prefix + lastFamilyId + suffix;
						} else if (prefix.toUpperCase().startsWith("I")) {
							lastIndividualId += 1;
							targetId = prefix + lastIndividualId + suffix;

						} else {
							lastId += 1;
							targetId = prefix + lastId + suffix;
						}
					} else {
						//
						targetId = sourceId;
					}

					//
					map.put(sourceId, targetId);
				}

				//
				String targetLine = matcher.group(1) + targetId + matcher.group(6);

				// if (!sourceLine.equals(targetLine)) {
				// logger.debug("DIFFERENT [{}][{}]", sourceLine, targetLine);
				// }

				//
				targetLines.append(targetLine);

				// logger.debug("[{}]=>[{}]", sourceLine, targetLine);
			} else {
				targetLines.append(sourceLine);
			}
		}

		//
		result = FileTools.addToName(file, "-shrinked_ids-" + new Date().getTime());
		FileTools.save(result, targetLines);

		//
		return result;
	}
}
