package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.graphs.random.DistributionType;
import org.tip.puck.graphs.random.RandomGraphReporter;
import org.tip.puck.partitions.graphs.RandomAllianceNetworkByAgentSimulationVariationsCriteria;
import org.tip.puck.report.Report;
import org.tip.puckgui.PuckGUI;
import org.tip.puckgui.WindowGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class RandomAllianceNetworkByAgentSimulationVariationsInputWindow extends JFrame {

	private static final long serialVersionUID = 7851329582938966248L;
	private static final Logger logger = LoggerFactory.getLogger(RandomAllianceNetworkByAgentSimulationVariationsInputWindow.class);

	private JFrame thisJFrame;
	private JPanel contentPane;
	private JButton btnRestoreDefaults;
	private JCheckBox chckbxNumberOfLoops;
	private JCheckBox chckbxNumberOfCircuits;
	private JCheckBox chckbxNumberOfTriangles;
	private JCheckBox chckbxConcentration;
	private JCheckBox chckbxWeightDistribution;
	private JCheckBox chckbxStrengthDistribution;
	private JSpinner spnrNumberOfNodes;
	private JSpinner spnrNumberOfArcs;
	private JSpinner spnrNumberOfRuns;
	private final ButtonGroup buttonGroupVariable1 = new ButtonGroup();
	private final ButtonGroup buttonGroupVariable2 = new ButtonGroup();
	private JRadioButton rdbtnVariable1Index1;
	private JRadioButton rdbtnVariable1Index2;
	private JRadioButton rdbtnVariable1Index3;
	private JRadioButton rdbtnVariable2Index1;
	private JRadioButton rdbtnVariable2Index2;
	private JRadioButton rdbtnVariable2Index3;
	private JSpinner spnrVariable1IntervalFactor;
	private JSpinner spnrVariable2IntervalFactor;
	private JSpinner spnrOutPreference;
	private JSpinner spnrVariable1NumberOfIntervals;
	private JSpinner spnrVariable2NumberOfIntervals;
	private JCheckBox chckbxSymmetry;
	private JComboBox cbBoxDistribution;

	/**
	 * Create the frame.
	 */
	public RandomAllianceNetworkByAgentSimulationVariationsInputWindow(final WindowGUI netGUI) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(
				RandomAllianceNetworkByAgentSimulationVariationsInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		setTitle("Random Alliance Network Variations (by agent simulation)");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 465, 535);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});

		this.btnRestoreDefaults = new JButton("Restore Defaults");
		this.btnRestoreDefaults.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Restore Defaults.
				setCriteria(new RandomAllianceNetworkByAgentSimulationVariationsCriteria());
			}
		});
		buttonPanel.add(this.btnRestoreDefaults);
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		getRootPane().setDefaultButton(btnLaunch);
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					//
					RandomAllianceNetworkByAgentSimulationVariationsCriteria criteria = getCriteria();

					//
					PuckGUI.instance().getPreferences().setAgentSimulationVariationsCriteria(criteria);

					//
					Report report = RandomGraphReporter.reportRandomAllianceNetworkByAgentSimulationVariations(criteria);

					//
					netGUI.addReportTab(report);

					//
					dispose();

				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(RandomAllianceNetworkByAgentSimulationVariationsInputWindow.this.thisJFrame, message, title,
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);

		JPanel panel_targets = new JPanel();
		panel_targets.setBorder(new TitledBorder(null, "Targets", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.contentPane.add(panel_targets, BorderLayout.NORTH);
		panel_targets.setLayout(new BoxLayout(panel_targets, BoxLayout.X_AXIS));

		JPanel panel_1 = new JPanel();
		panel_targets.add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));

		this.chckbxNumberOfLoops = new JCheckBox("Number of loops");
		panel_1.add(this.chckbxNumberOfLoops);

		this.chckbxNumberOfCircuits = new JCheckBox("Number of circuits");
		panel_1.add(this.chckbxNumberOfCircuits);

		this.chckbxNumberOfTriangles = new JCheckBox("Number of triangles");
		panel_1.add(this.chckbxNumberOfTriangles);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		panel_targets.add(horizontalStrut);

		JPanel panel = new JPanel();
		panel_targets.add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		this.chckbxConcentration = new JCheckBox("Concentration");
		panel.add(this.chckbxConcentration);

		this.chckbxWeightDistribution = new JCheckBox("Weight distribution");
		panel.add(this.chckbxWeightDistribution);

		this.chckbxStrengthDistribution = new JCheckBox("Strength distribution");
		panel.add(this.chckbxStrengthDistribution);

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		panel_targets.add(horizontalStrut_2);

		JPanel panel_7 = new JPanel();
		panel_targets.add(panel_7);
		panel_7.setLayout(new BoxLayout(panel_7, BoxLayout.Y_AXIS));

		this.chckbxSymmetry = new JCheckBox("Symmetry");
		panel_7.add(this.chckbxSymmetry);

		Component verticalGlue = Box.createVerticalGlue();
		panel_7.add(verticalGlue);

		JPanel panel_parameters = new JPanel();
		panel_parameters.setBorder(new TitledBorder(null, "Parameters", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.contentPane.add(panel_parameters, BorderLayout.CENTER);
		panel_parameters.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblNumberOfNodes = new JLabel("Number of nodes:");
		panel_parameters.add(lblNumberOfNodes, "2, 2, right, default");

		this.spnrNumberOfNodes = new JSpinner();
		this.spnrNumberOfNodes.setModel(new SpinnerNumberModel(new Integer(100), new Integer(1), null, new Integer(1)));
		panel_parameters.add(this.spnrNumberOfNodes, "4, 2");

		JLabel lblNumberOfArcs = new JLabel("Number of arcs:");
		panel_parameters.add(lblNumberOfArcs, "2, 4, right, default");

		this.spnrNumberOfArcs = new JSpinner();
		this.spnrNumberOfArcs.setModel(new SpinnerNumberModel(new Integer(1000), new Integer(1), null, new Integer(1)));
		panel_parameters.add(this.spnrNumberOfArcs, "4, 4");

		JLabel lblOutPreference = new JLabel("Out preference:");
		lblOutPreference.setHorizontalAlignment(SwingConstants.LEFT);
		panel_parameters.add(lblOutPreference, "2, 6, right, default");

		this.spnrOutPreference = new JSpinner();
		this.spnrOutPreference.setModel(new SpinnerNumberModel(new Double(0.5), new Double(0.1), null, new Double(0.1)));
		((JSpinner.NumberEditor) this.spnrOutPreference.getEditor()).getFormat().setMinimumFractionDigits(1);
		panel_parameters.add(this.spnrOutPreference, "4, 6");

		JLabel lblEgoDistribution = new JLabel("Ego Distribution:");
		panel_parameters.add(lblEgoDistribution, "2, 8, right, default");

		this.cbBoxDistribution = new JComboBox();
		this.cbBoxDistribution.setModel(new DefaultComboBoxModel(DistributionType.values()));
		panel_parameters.add(this.cbBoxDistribution, "4, 8, fill, default");

		JLabel lblNumberOfRuns = new JLabel("Number of runs:");
		panel_parameters.add(lblNumberOfRuns, "2, 10, right, default");

		this.spnrNumberOfRuns = new JSpinner();
		this.spnrNumberOfRuns.setModel(new SpinnerNumberModel(new Integer(100), new Integer(1), null, new Integer(1)));
		panel_parameters.add(this.spnrNumberOfRuns, "4, 10");

		JPanel panel_firstVariable = new JPanel();
		panel_parameters.add(panel_firstVariable, "2, 12");
		panel_firstVariable.setBorder(new TitledBorder(null, "First variable", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_firstVariable.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblIndex = new JLabel("Index:");
		panel_firstVariable.add(lblIndex, "2, 2, right, default");

		JPanel panel_3 = new JPanel();
		panel_firstVariable.add(panel_3, "4, 2, fill, fill");
		panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.Y_AXIS));

		this.rdbtnVariable1Index1 = new JRadioButton("endogamy");
		this.buttonGroupVariable1.add(this.rdbtnVariable1Index1);
		this.rdbtnVariable1Index1.setSelected(true);
		panel_3.add(this.rdbtnVariable1Index1);

		this.rdbtnVariable1Index2 = new JRadioButton("redoubling");
		this.buttonGroupVariable1.add(this.rdbtnVariable1Index2);
		panel_3.add(this.rdbtnVariable1Index2);

		this.rdbtnVariable1Index3 = new JRadioButton("transitivity");
		this.buttonGroupVariable1.add(this.rdbtnVariable1Index3);
		panel_3.add(this.rdbtnVariable1Index3);

		JPanel panel_5 = new JPanel();
		panel_firstVariable.add(panel_5, "2, 4, right, fill");
		panel_5.setLayout(new BoxLayout(panel_5, BoxLayout.Y_AXIS));

		JLabel lblNumberOf = new JLabel("Number of");
		panel_5.add(lblNumberOf);

		JLabel lblIntervals = new JLabel("intervals:");
		panel_5.add(lblIntervals);

		this.spnrVariable1NumberOfIntervals = new JSpinner();
		this.spnrVariable1NumberOfIntervals.setModel(new SpinnerNumberModel(new Integer(5), new Integer(0), null, new Integer(1)));
		panel_firstVariable.add(this.spnrVariable1NumberOfIntervals, "4, 4");

		JLabel lblIntervalFactor = new JLabel("Interval factor:");
		panel_firstVariable.add(lblIntervalFactor, "2, 6, right, default");

		this.spnrVariable1IntervalFactor = new JSpinner();
		this.spnrVariable1IntervalFactor.setModel(new SpinnerNumberModel(new Integer(2), new Integer(1), null, new Integer(1)));
		panel_firstVariable.add(this.spnrVariable1IntervalFactor, "4, 6");

		JPanel panel_secondVariable = new JPanel();
		panel_parameters.add(panel_secondVariable, "4, 12");
		panel_secondVariable.setBorder(new TitledBorder(null, "Second variable", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_secondVariable.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblIndex_1 = new JLabel("Index:");
		panel_secondVariable.add(lblIndex_1, "2, 2, right, default");

		JPanel panel_4 = new JPanel();
		panel_secondVariable.add(panel_4, "4, 2, fill, fill");
		panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.Y_AXIS));

		this.rdbtnVariable2Index1 = new JRadioButton("endogamy");
		this.buttonGroupVariable2.add(this.rdbtnVariable2Index1);
		panel_4.add(this.rdbtnVariable2Index1);

		this.rdbtnVariable2Index2 = new JRadioButton("redoubling");
		this.buttonGroupVariable2.add(this.rdbtnVariable2Index2);
		this.rdbtnVariable2Index2.setSelected(true);
		panel_4.add(this.rdbtnVariable2Index2);

		this.rdbtnVariable2Index3 = new JRadioButton("transitivity");
		this.buttonGroupVariable2.add(this.rdbtnVariable2Index3);
		panel_4.add(this.rdbtnVariable2Index3);

		JPanel panel_6 = new JPanel();
		panel_secondVariable.add(panel_6, "2, 4, right, fill");
		panel_6.setLayout(new BoxLayout(panel_6, BoxLayout.Y_AXIS));

		JLabel lblNumberOf_1 = new JLabel("Number of");
		panel_6.add(lblNumberOf_1);

		JLabel lblIntervals_1 = new JLabel("intervals:");
		panel_6.add(lblIntervals_1);

		this.spnrVariable2NumberOfIntervals = new JSpinner();
		this.spnrVariable2NumberOfIntervals.setModel(new SpinnerNumberModel(new Integer(5), new Integer(0), null, new Integer(1)));
		panel_secondVariable.add(this.spnrVariable2NumberOfIntervals, "4, 4");

		JLabel lblIntervalFactor_1 = new JLabel("Interval factor:");
		panel_secondVariable.add(lblIntervalFactor_1, "2, 6, right, default");

		this.spnrVariable2IntervalFactor = new JSpinner();
		this.spnrVariable2IntervalFactor.setModel(new SpinnerNumberModel(new Integer(2), new Integer(1), null, new Integer(1)));
		panel_secondVariable.add(this.spnrVariable2IntervalFactor, "4, 6");

		// //////////////////
		setCriteria(PuckGUI.instance().getPreferences().getAgentSimulationVariationsCriteria());
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public RandomAllianceNetworkByAgentSimulationVariationsCriteria getCriteria() throws PuckException {
		RandomAllianceNetworkByAgentSimulationVariationsCriteria result;

		//
		result = new RandomAllianceNetworkByAgentSimulationVariationsCriteria();

		//
		result.setNumberOfLoopsChecked(this.chckbxNumberOfLoops.isSelected());
		result.setNumberOfCircuitsChecked(this.chckbxNumberOfCircuits.isSelected());
		result.setNumberOfTrianglesChecked(this.chckbxNumberOfTriangles.isSelected());
		result.setConcentrationChecked(this.chckbxConcentration.isSelected());
		result.setWeightDistributionChecked(this.chckbxWeightDistribution.isSelected());
		result.setStrengthDistributionChecked(this.chckbxStrengthDistribution.isSelected());
		result.setSymmetryChecked(this.chckbxSymmetry.isSelected());
		result.setDistributionType((DistributionType) this.cbBoxDistribution.getSelectedItem());

		//
		result.setNumberOfNodes((Integer) this.spnrNumberOfNodes.getValue());
		result.setArcWeightSum((Integer) this.spnrNumberOfArcs.getValue());
		result.setOutPreference((Double) this.spnrOutPreference.getValue());
		result.setNumberOfRuns((Integer) this.spnrNumberOfRuns.getValue());

		//
		int index1;
		if (this.rdbtnVariable1Index1.isSelected()) {
			index1 = 0;
		} else if (this.rdbtnVariable1Index2.isSelected()) {
			index1 = 1;
		} else if (this.rdbtnVariable1Index3.isSelected()) {
			index1 = 2;
		} else {
			throw PuckExceptions.INVALID_PARAMETER.create("Bad value");
		}
		result.setFirstVariableIndex(index1);

		result.setFirstVariableNumberOfIntervals((Integer) this.spnrVariable1NumberOfIntervals.getValue());
		result.setFirstVariableIntervalFactor((Integer) this.spnrVariable1IntervalFactor.getValue());

		result.setFirstVariableInitialValue();

		//
		int index2;
		if (this.rdbtnVariable2Index1.isSelected()) {
			index2 = 0;
		} else if (this.rdbtnVariable2Index2.isSelected()) {
			index2 = 1;
		} else if (this.rdbtnVariable2Index3.isSelected()) {
			index2 = 2;
		} else {
			throw PuckExceptions.INVALID_PARAMETER.create("Bad value");
		}
		result.setSecondVariableIndex(index2);
		result.setSecondVariableNumberOfIntervals((Integer) this.spnrVariable2NumberOfIntervals.getValue());
		result.setSecondVariableIntervalFactor((Integer) this.spnrVariable2IntervalFactor.getValue());

		result.setSecondVariableInitialValue();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public void setCriteria(final RandomAllianceNetworkByAgentSimulationVariationsCriteria source) {
		//
		if (source != null) {
			//
			this.chckbxNumberOfLoops.setSelected(source.isNumberOfLoopsChecked());
			this.chckbxNumberOfCircuits.setSelected(source.isNumberOfCircuitsChecked());
			this.chckbxNumberOfTriangles.setSelected(source.isNumberOfTrianglesChecked());
			this.chckbxConcentration.setSelected(source.isConcentrationChecked());
			this.chckbxWeightDistribution.setSelected(source.isWeightDistributionChecked());
			this.chckbxStrengthDistribution.setSelected(source.isStrengthDistributionChecked());
			this.chckbxSymmetry.setSelected(source.isSymmetryChecked());
			this.cbBoxDistribution.setSelectedItem(source.getDistributionType());

			//
			this.spnrNumberOfNodes.setValue(source.getNumberOfNodes());
			this.spnrNumberOfArcs.setValue(source.getArcWeightSum());
			this.spnrOutPreference.setValue(source.getOutPreference());
			this.spnrNumberOfRuns.setValue(source.getNumberOfRuns());

			//
			this.rdbtnVariable1Index1.setSelected(false);
			this.rdbtnVariable1Index2.setSelected(false);
			this.rdbtnVariable1Index3.setSelected(false);
			switch (source.getFirstVariableIndex()) {
				case 0:
					this.rdbtnVariable1Index1.setSelected(true);
				break;
				case 1:
					this.rdbtnVariable1Index2.setSelected(true);
				break;
				case 2:
					this.rdbtnVariable1Index3.setSelected(true);
				break;
			}

			//
			this.spnrVariable1NumberOfIntervals.setValue(source.getFirstVariableNumberOfIntervals());
			this.spnrVariable1IntervalFactor.setValue(source.getFirstVariableIntervalFactor());

			//
			this.rdbtnVariable2Index1.setSelected(false);
			this.rdbtnVariable2Index2.setSelected(false);
			this.rdbtnVariable2Index3.setSelected(false);
			switch (source.getSecondVariableIndex()) {
				case 0:
					this.rdbtnVariable2Index1.setSelected(true);
				break;
				case 1:
					this.rdbtnVariable2Index2.setSelected(true);
				break;
				case 2:
					this.rdbtnVariable2Index3.setSelected(true);
				break;
			}

			//
			this.spnrVariable2NumberOfIntervals.setValue(source.getSecondVariableNumberOfIntervals());
			this.spnrVariable2IntervalFactor.setValue(source.getSecondVariableIntervalFactor());
		}
	}
}
