package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.workers.AttributeDescriptor;
import org.tip.puck.net.workers.AttributeDescriptors;
import org.tip.puck.net.workers.AttributeReplaceValueCriteria;
import org.tip.puck.net.workers.AttributeWorker;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class AttributeReplaceValueInputDialog extends JDialog {

	private static final long serialVersionUID = 2216201299943308913L;
	private final JPanel contentPanel = new JPanel();
	private AttributeReplaceValueCriteria dialogCriteria;
	private static AttributeReplaceValueCriteria lastCriteria = new AttributeReplaceValueCriteria();
	private JComboBox cmbbxTarget;
	private JComboBox cmbbxLabel;
	private JTextField txtfldValue;
	private JCheckBox chckbxIgnoreCase;
	private JTextField txtfldNewValue;

	/**
	 * Create the dialog.
	 */
	public AttributeReplaceValueInputDialog(final List<String> relationModelNames, final AttributeDescriptors attributeDescriptors) {
		super();

		//
		List<String> targetLabels = new ArrayList<String>();
		targetLabels.add("ALL");
		targetLabels.add("CORPUS");
		targetLabels.add("INDIVIDUALS");
		targetLabels.add("FAMILIES");
		targetLabels.add("RELATIONS");
		targetLabels.add("ACTORS");
		if (relationModelNames != null) {
			for (String name : relationModelNames) {
				targetLabels.add(name);
			}
		}

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Replace Attribute Value Input");
		setIconImage(Toolkit.getDefaultToolkit().getImage(AttributeReplaceValueInputDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				AttributeReplaceValueInputDialog.this.dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 420, 224);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));
		{
			JLabel lblTarget = new JLabel("Target:");
			this.contentPanel.add(lblTarget, "2, 2, right, default");
		}
		{
			this.cmbbxTarget = new JComboBox(targetLabels.toArray());
			this.cmbbxTarget.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(final ItemEvent event) {
					// Update label list.
					//
					if (event.getStateChange() == ItemEvent.SELECTED) {
						//
						List<String> labels;
						switch (AttributeReplaceValueInputDialog.this.cmbbxTarget.getSelectedIndex()) {
							case 0:
								labels = attributeDescriptors.labels();
							break;

							case 1:
								labels = attributeDescriptors.findByScope(AttributeDescriptor.Scope.CORPUS).labels();
							break;

							case 2:
								labels = attributeDescriptors.findByScope(AttributeDescriptor.Scope.INDIVIDUALS).labels();
							break;

							case 3:
								labels = attributeDescriptors.findByScope(AttributeDescriptor.Scope.FAMILIES).labels();
							break;

							case 4:
								labels = attributeDescriptors.findByScope(AttributeDescriptor.Scope.RELATION).labels();
							break;

							case 5:
								labels = attributeDescriptors.findByScope(AttributeDescriptor.Scope.ACTORS).labels();
							break;

							default:
								labels = attributeDescriptors.findByRelationModelName(
										(String) AttributeReplaceValueInputDialog.this.cmbbxTarget.getSelectedItem()).labels();
							break;
						}

						//
						Collections.sort(labels);

						//
						AttributeReplaceValueInputDialog.this.cmbbxLabel.setModel(new DefaultComboBoxModel(labels.toArray()));
						AttributeReplaceValueInputDialog.this.cmbbxLabel.setSelectedIndex(-1);
					}
				}
			});
			this.contentPanel.add(this.cmbbxTarget, "4, 2, fill, default");
		}
		{
			JLabel lblLabel = new JLabel("Label:");
			this.contentPanel.add(lblLabel, "2, 4, right, default");
		}
		{
			this.cmbbxLabel = new JComboBox(attributeDescriptors.labelsSorted().toArray());
			this.cmbbxLabel.setEditable(true);
			this.contentPanel.add(this.cmbbxLabel, "4, 4, fill, default");
		}
		{
			JLabel lblNewLabel = new JLabel("Value:");
			this.contentPanel.add(lblNewLabel, "2, 6, right, default");
		}
		{
			this.txtfldValue = new JTextField();
			this.contentPanel.add(this.txtfldValue, "4, 6");
			this.txtfldValue.setColumns(10);
		}
		{
			this.chckbxIgnoreCase = new JCheckBox("ignore case");
			this.contentPanel.add(this.chckbxIgnoreCase, "4, 8");
		}
		{
			JLabel lblNewValue = new JLabel("New value:");
			this.contentPanel.add(lblNewValue, "2, 10, right, default");
		}
		{
			this.txtfldNewValue = new JTextField();
			this.txtfldNewValue.setColumns(10);
			this.contentPanel.add(this.txtfldNewValue, "4, 10, fill, default");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						AttributeReplaceValueInputDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("Replace value");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						AttributeReplaceValueCriteria criteria = getCriteria();
						if (StringUtils.isBlank(criteria.getLabel())) {
							//
							String title = "Invalid input";
							String message = "Please, enter a label not empty.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						} else {
							//
							lastCriteria = criteria;
							AttributeReplaceValueInputDialog.this.dialogCriteria = criteria;

							//
							setVisible(false);
						}

					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}

		// ////////////////////////
		setCriteria(lastCriteria);
	}

	/**
	 * 
	 * @return
	 */
	public AttributeReplaceValueCriteria getCriteria() {
		AttributeReplaceValueCriteria result;

		result = new AttributeReplaceValueCriteria();

		//
		AttributeWorker.Scope scope;
		String optionalName;
		switch (this.cmbbxTarget.getSelectedIndex()) {
			case 0:
				scope = AttributeWorker.Scope.ALL;
				optionalName = null;
			break;

			case 1:
				scope = AttributeWorker.Scope.CORPUS;
				optionalName = null;
			break;

			case 2:
				scope = AttributeWorker.Scope.INDIVIDUALS;
				optionalName = null;
			break;

			case 3:
				scope = AttributeWorker.Scope.FAMILIES;
				optionalName = null;
			break;

			case 4:
				scope = AttributeWorker.Scope.RELATIONS;
				optionalName = null;
			break;

			case 5:
				scope = AttributeWorker.Scope.ACTORS;
				optionalName = null;
			break;

			default:
				scope = AttributeWorker.Scope.RELATION;
				optionalName = (String) this.cmbbxTarget.getSelectedItem();
			break;
		}
		result.setScope(scope);
		result.setOptionalRelationName(optionalName);

		//
		result.setLabel((String) this.cmbbxLabel.getSelectedItem());

		//
		result.setTargetValue(this.txtfldValue.getText());

		//
		result.setNewValue(this.txtfldNewValue.getText());

		//
		if (this.chckbxIgnoreCase.isSelected()) {
			//
			result.setCaseOption(AttributeWorker.CaseOption.IGNORE_CASE);

		} else {
			//
			result.setCaseOption(AttributeWorker.CaseOption.CASE_SENSITIVE);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public AttributeReplaceValueCriteria getDialogCriteria() {
		AttributeReplaceValueCriteria result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final AttributeReplaceValueCriteria source) {
		//
		if (source != null) {

			//
			switch (source.getScope()) {
				case ALL:
					this.cmbbxTarget.setSelectedIndex(0);
				break;

				case CORPUS:
					this.cmbbxTarget.setSelectedIndex(1);
				break;

				case INDIVIDUALS:
					this.cmbbxTarget.setSelectedIndex(2);
				break;

				case FAMILIES:
					this.cmbbxTarget.setSelectedIndex(3);
				break;

				case RELATIONS:
					this.cmbbxTarget.setSelectedIndex(4);
				break;

				case ACTORS:
					this.cmbbxTarget.setSelectedIndex(5);
				break;

				default:
					this.cmbbxTarget.setSelectedItem(source.getOptionalRelationName());
				break;
			}

			//
			this.cmbbxLabel.setSelectedItem(source.getLabel());

			//
			this.txtfldValue.setText(source.getTargetValue());

			//
			this.txtfldNewValue.setText(source.getNewValue());

			//
			if (source.isCaseSensitive()) {
				//
				this.chckbxIgnoreCase.setSelected(false);

			} else {
				//
				this.chckbxIgnoreCase.setSelected(true);
			}
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		/* AttributeFilter criteria = */showDialog(null, null);
	}

	/**
	 * Launch the application.
	 */
	public static AttributeReplaceValueCriteria showDialog(final List<String> relationModelNames, final AttributeDescriptors attributeDescriptors) {
		AttributeReplaceValueCriteria result;

		//
		AttributeReplaceValueInputDialog dialog = new AttributeReplaceValueInputDialog(relationModelNames, attributeDescriptors);
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
