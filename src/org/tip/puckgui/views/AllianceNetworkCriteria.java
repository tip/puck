package org.tip.puckgui.views;

import org.tip.puck.partitions.graphs.ClusterNetworkUtils.AllianceType;
import org.tip.puck.partitions.graphs.ClusterNetworkUtils.LineType;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class AllianceNetworkCriteria {

	private String label;
	private AllianceType allianceType;
	private LineType lineType;
	private int minimalDegree;
	private int minimalNodeStrength;
	private int minimalLinkWeight;
	private StringList partitionLabels;

	/**
	 * 
	 */
	public AllianceNetworkCriteria() {
		this.label = "";
		this.allianceType = AllianceType.WIFE_HUSBAND;
		this.lineType = LineType.WEIGHTED_ARC;
		this.minimalDegree = 0;
		this.minimalNodeStrength = 0;
		this.minimalLinkWeight = 0;
		this.partitionLabels = new StringList();
	}

	public AllianceType getAllianceType() {
		return allianceType;
	}

	public String getLabel() {
		return label;
	}

	public LineType getLineType() {
		return lineType;
	}

	public int getMinimalDegree() {
		return minimalDegree;
	}

	public int getMinimalLinkWeight() {
		return minimalLinkWeight;
	}

	public int getMinimalNodeStrength() {
		return minimalNodeStrength;
	}

	public boolean isWeightedLinks() {
		boolean result;

		if (this.lineType == LineType.WEIGHTED_ARC) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	public StringList partitionLabel() {
		return this.partitionLabels;
	}

	public void setAllianceType(final AllianceType allianceType) {
		this.allianceType = allianceType;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public void setLineType(final LineType lineType) {
		this.lineType = lineType;
	}

	public void setMinimalDegree(final int minimalDegree) {
		this.minimalDegree = minimalDegree;
	}

	public void setMinimalLinkWeight(final int minimalLinkWeight) {
		this.minimalLinkWeight = minimalLinkWeight;
	}

	public void setMinimalNodeStrength(final int minimalNodeStrength) {
		this.minimalNodeStrength = minimalNodeStrength;
	}

}
