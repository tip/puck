package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.census.chains.Chain;
import org.tip.puck.census.chains.ChainMaker;
import org.tip.puck.census.chains.Notation;
import org.tip.puck.census.workers.ChainValuator;
import org.tip.puck.census.workers.ChainValuator.ChainProperty;
import org.tip.puck.census.workers.SiblingMode;
import org.tip.puck.census.workers.SymmetryType;
import org.tip.puck.net.FiliationType;
import org.tip.puck.report.Report;
import org.tip.puckgui.NetGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class CalculatorWindow {
	private enum Calculation {
		SWITCHNOTATION,
		STANDARD,
		CLOSE,
		REFLECT,
		ROTATE,
		COMPOSE,
		INSERT
	}

	private static final Logger logger = LoggerFactory.getLogger(CalculatorWindow.class);
	private JFrame frmCalculator;
	private JTextField txtfld1;
	private JTextField txtfld2;
	private JTextField txtfld3;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton rdbtn1;
	private JRadioButton rdbtn2;
	private JRadioButton rdbtn3;

	private NetGUI netGUI;

	/**
	 * Create the application.
	 */
	public CalculatorWindow(final NetGUI netGUI) {
		this.netGUI = netGUI;
		initialize();
	}

	private String calculate(final String str1, final String str2, final Calculation calculation) {
		String result;

		Notation notation = Chain.notation(str1);

		Chain chain1 = ChainMaker.fromString(str1);
		Chain chain2 = ChainMaker.fromString(str2);

		Chain chain = null;

		switch (calculation) {
			case SWITCHNOTATION:
				chain = chain1;
				if (notation == Notation.CLASSIC_GENDERED) {
					notation = Notation.POSITIONAL;
				} else if (notation == Notation.POSITIONAL) {
					notation = Notation.CLASSIC_GENDERED;
				}
			break;
			case STANDARD:
				if (chain1.isHetero()) {
					chain1.setSymmetry(SymmetryType.PERMUTABLE);
				} else {
					chain1.setSymmetry(SymmetryType.INVERTIBLE);
				}
				chain = chain1.standard();
			break;
			case CLOSE:
			// chain = chain1.close();
			break;
			case REFLECT:
				chain = chain1.reflect();
			break;
			case ROTATE:
				chain = chain1.rotate();
			break;
			case COMPOSE:
				chain = ChainMaker.compose(chain1, chain2);
			break;
			case INSERT:
				chain = ChainMaker.insert(chain1, chain2);
			break;
		}

		if (chain != null) {
			result = chain.signature(notation);
		} else {
			result = "";
		}

		//
		return result;
	}

	/**
	 * s
	 * 
	 * @return
	 */
	public JFrame getJFrame() {
		JFrame result;

		result = this.frmCalculator;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public String getSelectedInput() {
		String result;

		if (this.rdbtn1.isSelected()) {
			result = this.txtfld1.getText();
		} else if (this.rdbtn2.isSelected()) {
			result = this.txtfld2.getText();
		} else {
			result = this.txtfld3.getText();
		}

		//
		return result;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.frmCalculator = new JFrame();
		this.frmCalculator.setTitle("Calculator");
		this.frmCalculator.setBounds(100, 100, 347, 274);
		this.frmCalculator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel_1 = new JPanel();
		this.frmCalculator.getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));

		JPanel panel = new JPanel();
		panel_1.add(panel);
		panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, },
				new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		this.rdbtn1 = new JRadioButton("");
		this.buttonGroup.add(this.rdbtn1);
		this.rdbtn1.setSelected(true);
		panel.add(this.rdbtn1, "2, 4");

		JLabel label = new JLabel("1");
		panel.add(label, "4, 4, right, default");

		this.txtfld1 = new JTextField();
		panel.add(this.txtfld1, "6, 4, fill, default");
		this.txtfld1.setColumns(10);

		this.rdbtn2 = new JRadioButton("");
		this.buttonGroup.add(this.rdbtn2);
		panel.add(this.rdbtn2, "2, 6");

		JLabel label_1 = new JLabel("2");
		panel.add(label_1, "4, 6, right, default");

		this.txtfld2 = new JTextField();
		panel.add(this.txtfld2, "6, 6, fill, default");
		this.txtfld2.setColumns(10);

		this.rdbtn3 = new JRadioButton("");
		this.buttonGroup.add(this.rdbtn3);
		panel.add(this.rdbtn3, "2, 8");

		JLabel label_2 = new JLabel("3");
		panel.add(label_2, "4, 8, right, default");

		this.txtfld3 = new JTextField();
		panel.add(this.txtfld3, "6, 8, fill, default");
		this.txtfld3.setColumns(10);

		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2);
		panel_2.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.GLUE_COLSPEC, ColumnSpec.decode("50dlu"), FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50dlu"), FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("50dlu"), FormFactory.GLUE_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JButton btnCanonic = new JButton("Standard");
		btnCanonic.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					String str = getSelectedInput();
					setSelectedInput(calculate(str, null, Calculation.STANDARD));
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(CalculatorWindow.this.frmCalculator, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panel_2.add(btnCanonic, "2, 2");

		JButton btnReflect = new JButton("Reflect");
		btnReflect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {

				try {
					String str = getSelectedInput();
					setSelectedInput(calculate(str, null, Calculation.REFLECT));
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(CalculatorWindow.this.frmCalculator, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panel_2.add(btnReflect, "4, 2");

		JButton btnRotate = new JButton("Rotate");
		btnRotate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					String str = getSelectedInput();
					setSelectedInput(calculate(str, null, Calculation.ROTATE));
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(CalculatorWindow.this.frmCalculator, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panel_2.add(btnRotate, "6, 2");

		JButton btnCompose = new JButton("Compose");
		btnCompose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					String str1 = CalculatorWindow.this.txtfld1.getText();
					String str2 = CalculatorWindow.this.txtfld2.getText();
					CalculatorWindow.this.txtfld3.setText(calculate(str1, str2, Calculation.COMPOSE));
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(CalculatorWindow.this.frmCalculator, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panel_2.add(btnCompose, "4, 4");

		JButton btnInsert = new JButton("Insert");
		btnInsert.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				try {
					String str1 = CalculatorWindow.this.txtfld1.getText();
					String str2 = CalculatorWindow.this.txtfld2.getText();
					CalculatorWindow.this.txtfld3.setText(calculate(str1, str2, Calculation.INSERT));
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(CalculatorWindow.this.frmCalculator, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panel_2.add(btnInsert, "6, 4");

		JButton btnCombine = new JButton("Closure");
		btnCombine.setEnabled(false);
		btnCombine.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {

				try {
					String str = getSelectedInput();
					setSelectedInput(calculate(str, null, Calculation.CLOSE));
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(CalculatorWindow.this.frmCalculator, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panel_2.add(btnCombine, "2, 4");

		JButton btnVary = new JButton("Develop");
		btnVary.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					Report report = new Report();
					report.inputs().add("Chain Development", getSelectedInput());
					List<Chain> chains = new ArrayList<Chain>();
					ChainMaker.develop(chains, getSelectedInput(), SiblingMode.FULL, true, FiliationType.COGNATIC);
					for (Chain chain : chains) {
						report.outputs().appendln(chain.signature(Notation.POSITIONAL) + "\t" + chain.signature(Notation.CLASSIC_GENDERED));
					}

					CalculatorWindow.this.netGUI.addReportTab(report);
					CalculatorWindow.this.frmCalculator.dispose();
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(CalculatorWindow.this.frmCalculator, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panel_2.add(btnVary, "4, 6");

		JButton btnAnalyze = new JButton("Analyze");
		btnAnalyze.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				// Launch.
				try {
					Chain chain = ChainMaker.fromString(getSelectedInput());

					Report report = new Report();
					report.inputs().add("Chain Analysis", getSelectedInput());
					for (ChainProperty property : ChainProperty.values()) {
						report.outputs().appendln(property + "\t" + ChainValuator.get(chain, property.toString()));
					}
					List<Chain> permutations = ChainMaker.getPermutations(chain);
					report.outputs().appendln(permutations.size() + " permutations:");
					for (Chain permutation : permutations) {
						report.outputs().appendln("\t" + permutation.signature(Notation.POSITIONAL) + "\t" + permutation.signature(Notation.CLASSIC_GENDERED));
					}

					CalculatorWindow.this.netGUI.addReportTab(report);
					CalculatorWindow.this.frmCalculator.dispose();
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(CalculatorWindow.this.frmCalculator, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JButton btnSwitch = new JButton("Switch");
		btnSwitch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				String str = getSelectedInput();
				setSelectedInput(calculate(str, null, Calculation.SWITCHNOTATION));
			}
		});
		panel_2.add(btnSwitch, "2, 6");
		panel_2.add(btnAnalyze, "6, 6");
	}

	/**
	 * 
	 * @return
	 */
	public void setSelectedInput(final String value) {
		if (this.rdbtn1.isSelected()) {
			this.txtfld1.setText(value);
		} else if (this.rdbtn2.isSelected()) {
			this.txtfld2.setText(value);
		} else {
			this.txtfld3.setText(value);
		}
	}
}
