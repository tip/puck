package org.tip.puck.partitions;

import java.util.ArrayList;

/**
 * 
 * @author TIP
 */
public class Partitions<E> extends ArrayList<Partition<E>> {

	private static final long serialVersionUID = -528838735828314899L;

	/**
	 * 
	 */
	public Partitions() {
		super();
	}

	/**
	 * 
	 * @param capacity
	 */
	public Partitions(final int capacity) {
		super(capacity);
	}

}
