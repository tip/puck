package org.tip.puck.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * 
 * @author TIP
 */
public class NumberablesHashMap<NumberableType extends Numberable> implements Numberables<NumberableType>, Comparable<NumberablesHashMap<NumberableType>> {

	public enum IdStrategy {
		FILL,
		APPEND,
		SIZE
	};

	protected HashMap<Integer, NumberableType> data;
	
	int id;

	/**
	 * 
	 */
	public NumberablesHashMap() {
		this.data = new HashMap<Integer, NumberableType>();
	}

	/**
	 * 
	 */
	public NumberablesHashMap(final int initialCapacity) {
		this.data = new HashMap<Integer, NumberableType>(initialCapacity);
	}
	
	public void addNew(final NumberableType numberableObject){
		if (!contains(numberableObject)){
			put(numberableObject);
		}
	}

	/**
	 * 
	 */
	public void add(final NumberableType numberableObject) {
		put(numberableObject);
	}

	/**
	 * 
	 */
	@Override
	public void clear() {
		this.data.clear();
	}

	/***
	 * 
	 * @param numberableObject
	 * @return
	 */
	public boolean contains(final NumberableType numberableObject) {
		boolean result;

		result = data.containsValue(numberableObject);

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public boolean containsId(final int id) {
		boolean result;

		result = this.data.containsKey(id);

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public NumberableType getById(final int id) {
		NumberableType result;

		result = this.data.get(id);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int getFirstFreeId() {
		int result;

		Integer[] ids = this.getIds();

		if (ids.length == 0) {
			result = 1;
		} else {

			Arrays.sort(ids);

			boolean ended = false;
			int index = 0;
			result = -1;
			while (!ended) {
				if (index < ids.length - 1) {
					if (ids[index] + 1 == ids[index + 1]) {
						index += 1;
					} else {
						ended = true;
						result = ids[index] + 1;
					}
				} else {
					ended = true;
					result = ids[index] + 1;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int getFirstId() {
		int result;

		result = Integer.MAX_VALUE;
		for (Integer id : data.keySet()) {
			if (id < result) {
				result = id;
			}
		}

		if (result == Integer.MAX_VALUE) {
			result = 0;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public Integer[] getIds() {
		Integer[] result;

		result = new Integer[this.data.keySet().size()];
		this.data.keySet().toArray(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int getLastId() {
		int result;

		result = 0;
		for (Integer id : data.keySet()) {
			if (id > result) {
				result = id;
			}
		}

		//
		return result;
	}
	
	public NumberableType getLast(){
		NumberableType result;
		
		result = null;
		for (NumberableType item: data.values()){
			result = item;
		}
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Integer> getSparseIds() {
		List<Integer> result;

		//
		result = new ArrayList<Integer>();

		//
		Integer[] ids = this.getIds();
		Arrays.sort(ids);

		//
		for (int index = 1; index < ids.length; index++) {
			if (ids[index] - 1 != ids[index - 1]) {
				result.add(index);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public boolean isEmpty() {
		boolean result;

		result = this.data.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<NumberableType> iterator() {
		Iterator<NumberableType> result;

		result = this.data.values().iterator();
		// result = toSortedList().iterator();

		//
		return result;
	}

	/**
	 * 
	 * @param strategy
	 * @return
	 */
	public int nextFreeId(final IdStrategy strategy) {
		int result;

		if (strategy == null) {
			result = 0;
		} else {
			switch (strategy) {
				case FILL:
					result = getFirstFreeId();
				break;

				case APPEND:
					result = getLastId() + 1;
				break;

				case SIZE:
					result = this.size() + 1;
				break;

				default:
					result = 0;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public void put(final NumberableType numberableObject) {
		if (numberableObject != null) {
			this.data.put(numberableObject.getId(), numberableObject);
		}
	}


	/**
	 * 
	 * @param id
	 */
	@Override
	public void removeById(final int id) {
		this.data.remove(id);
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public int size() {
		int result;

		result = this.data.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<NumberableType> toList() {
		List<NumberableType> result;

		result = new ArrayList<NumberableType>(this.data.values());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<NumberableType> toSortedList() {
		List<NumberableType> result;

		result = toList();
		Collections.sort(result, new NumberableComparator());

		//
		return result;
	}
	
	public String toStringAsIdList() {
		String result;
		
		result = "";
		
		for (Numberable numberable : toSortedList()){
			result += numberable.getId()+" ";
		}
		
		//
		return result;
	}
	
	
	public NumberableType getFirst(){
		NumberableType result;
		
		result = null;
		for (NumberableType item: toSortedList()){
			result = item;
			break;
		}
		
		if (data.size()>1){
			System.err.print("Warning : multiple items "+toSortedList());
		}
		//
		return result;
	}
	
	public int compareTo(NumberablesHashMap<NumberableType> other) {
		return getIds().toString().compareTo(other.getIds().toString());
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String hashKey() {
		return id + "";
	}
}
