/**
 * Copyright 2013-2022 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.tip.puck.stag;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.ResourceBundle;

import nl.mpi.kinnate.kindata.EntityData;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.census.chains.Chain;
import org.tip.puck.census.workers.CensusCriteria;
import org.tip.puck.census.workers.CircuitFinder;
import org.tip.puck.census.workers.CircuitType;
import org.tip.puck.census.workers.RestrictionType;
import org.tip.puck.census.workers.SiblingMode;
import org.tip.puck.census.workers.SymmetryType;
import org.tip.puck.kinoath.KinOathDiagram;
import org.tip.puck.kinoath.Puck2KinOath;
import org.tip.puck.kinoath.io.KinOathFile;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.roles.RoleRelationMaker;
import org.tip.puck.net.relations.roles.RoleRelationWorker;
import org.tip.puck.net.relations.workers.RelationModelReporter;
import org.tip.puck.net.workers.AttributeDescriptors;
import org.tip.puck.net.workers.AttributeWorker;
import org.tip.puck.net.workers.ControlReporter;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.net.workers.ControlReporter.ControlType;
import org.tip.puck.net.workers.IndividualValuator.EndogenousLabel;
import org.tip.puck.partitions.MultiPartition;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionCriteria.PartitionType;
import org.tip.puck.partitions.PartitionMaker;
import org.tip.puck.report.Report;
import org.tip.puck.report.ReportChart;
import org.tip.puck.report.ReportChartMaker;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.statistics.BIASCounts;
import org.tip.puck.statistics.FiliationCounts;
import org.tip.puck.statistics.GenderedDouble;
import org.tip.puck.statistics.GenderedInt;
import org.tip.puck.statistics.StatisticsReporter;
import org.tip.puck.statistics.StatisticsWorker;
import org.tip.puck.util.Chronometer;
import org.tip.puck.util.IntWithMax;
import org.tip.puck.util.MathUtils;

import fr.devinsy.util.StringList;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Stag
{
	private static final Logger logger = LoggerFactory.getLogger(Stag.class);

	public static final int GENEALOGY_GRAPHIC_WIDTH = 400;
	public static final int GENEALOGY_GRAPHIC_HEIGHT = 240;

	public static final String CROSS_FIRST_COUSIN_MARRIAGE_PATTERN = "XF(X)HX";
	public static final String LEVIRATE_MARRIAGE_PATTERN = "X(X)X.F";
	public static final String SORORATE_MARRIAGE_PATTERN = "X(X)X.H";
	public static final String DOUBLE_OR_EXCHANGE_MARRIAGE_PATTERN = "X(X)X.X(X)X";
	public static final String NIECE_NEPHEW_MARRIAGE_PATTERN = "X(X)XX";
	public static final String PARALLEL_FIRST_COUSIN_MARRIAGE_PATTERN = "XH(X)HX XF(X)FX";
	public static final String FIRST_COUSIN_MARRIAGE_PATTERN = "XX(X)XX";
	public static final String DOUBLE_MARRIAGE_PATTERN = "H(X)H.F(X)F";
	public static final String EXCHANGE_MARRIAGE_PATTERN = "H(X)F.H(X)F";


	/**
	 * @throws PuckException
	 * 
	 */
	public static CountAndRate analyzeCircuitCensus(final String pattern, final Net source) throws PuckException
	{
		CountAndRate result;

		//
		CensusCriteria criteria = new CensusCriteria();

		criteria.setPattern(pattern);
		criteria.setClosingRelation("SPOUSE");
		criteria.setChainClassification("SIMPLE");
		criteria.setCircuitType(CircuitType.CIRCUIT);
		criteria.setFiliationType(FiliationType.COGNATIC);
		criteria.setRestrictionType(RestrictionType.NONE);
		criteria.setSiblingMode(SiblingMode.FULL);
		criteria.setSymmetryType(SymmetryType.PERMUTABLE);

		//
		CircuitFinder finder = new CircuitFinder(new Segmentation(source), criteria);

		//
		finder.findCircuits();

		//
		finder.count();

		long count = finder.getCircuitCoupleCount();
		double density = finder.getCircuitCoupleDensity();

		result = new CountAndRate(count, density);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param source
	 */
	public static AttributeDescriptors buildAttributeDescriptorStatistics(final Net source)
	{
		AttributeDescriptors result;

		try
		{
			result = AttributeWorker.getExogenousAttributeDescriptors(source, null).sort();
		}
		catch (Exception exception)
		{
			logger.warn("ERROR IN STAG REFRESHING", exception);
			result = new AttributeDescriptors();
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param source
	 */
	public static Properties buildBasicStatistics(final Net source)
	{
		Properties result;

		result = new Properties();

		try
		{
			int errorCount = 0;

			if (source != null)
			{
				Chronometer chrono = new Chronometer().start();

				//
				result.put(StatisticTag.NUM_OF_INDIVIDUALS.name(), source.individuals().size());
				
				//
				GenderedInt genders = StatisticsWorker.genderDistribution(source.individuals());
				result.put(StatisticTag.NUM_OF_MEN.name(), genders.getMenValue());
				result.put(StatisticTag.NUM_OF_WOMEN.name(), genders.getWomenValue());
				result.put(StatisticTag.NUM_OF_UNKNOWN.name(), genders.getUnknownValue());

				//
				GenderedDouble percentages = StatisticsWorker.genderPercentageDistribution(genders);
				result.put(StatisticTag.RATE_OF_MEN.name(), percentages.getMenValue());
				result.put(StatisticTag.RATE_OF_WOMEN.name(), percentages.getWomenValue());
				result.put(StatisticTag.RATE_OF_UNKNOWN.name(), percentages.getUnknownValue());

				//
				result.put(StatisticTag.NUM_OF_UNIONS.name(), StatisticsWorker.numberOfPartnerships(source.families()));
				int marriageCount = StatisticsWorker.numberOfMarriages(source.families());
				result.put(StatisticTag.NUM_OF_MARRIAGES.name(), marriageCount);
				result.put(StatisticTag.DENSITY_OF_MARRIAGES.name(), StatisticsWorker.densityOfMarriages(marriageCount, source.individuals().size()));

				result.put(StatisticTag.NUM_OF_NON_SINGLE_MEN.name(), StatisticsWorker.numberOfNotSingles(source.individuals(), Gender.MALE));
				result.put(StatisticTag.NUM_OF_NON_SINGLE_WOMEN.name(), StatisticsWorker.numberOfNotSingles(source.individuals(), Gender.FEMALE));

				result.put(StatisticTag.NUM_OF_RELATION_MODELS.name(), source.relationModels().size());

				result.put(StatisticTag.NUM_OF_RELATIONS.name(), source.relations().size());

				int filiationCount = StatisticsWorker.numberOfFiliationTies(source.families());
				result.put(StatisticTag.NUM_OF_PARENT_CHILD_TIES.name(), filiationCount);
				result.put(StatisticTag.DENSITY_OF_FILIATION.name(), StatisticsWorker.densityOfFiliations(filiationCount, source.individuals().size()));

				int numberOfFertileMarriages = StatisticsWorker.numberOfFertileMarriages(source.families());
				result.put(StatisticTag.NUM_OF_FERTILE_MARRIAGES.name(), numberOfFertileMarriages);
				result.put(StatisticTag.RATE_OF_FERTILE_MARRIAGES.name(), MathUtils.percent(numberOfFertileMarriages, marriageCount));

				result.put(StatisticTag.NUM_OF_COWIFE_RELATIONS.name(), StatisticsWorker.numberOfCoSpouseRelations(source.individuals(), Gender.FEMALE));
				result.put(StatisticTag.NUM_OF_COHUSBAND_RELATIONS.name(), StatisticsWorker.numberOfCoSpouseRelations(source.individuals(), Gender.MALE));

				//
				IntWithMax component = null;
				try
				{
					component = StatisticsWorker.numberOfComponents(source.individuals());
					result.put(StatisticTag.NUM_OF_COMPONENTS.name(), component.value());
					result.put(StatisticTag.MAX_OF_COMPONENTS.name(), component.max());
				}
				catch (Exception exception)
				{
					logger.warn("Error on components.", exception);
					errorCount += 1;
				}

				//
				try
				{
					Partition<Individual> agnaticPartition = PartitionMaker.createRaw("PATRIC partition", source.individuals(), "PATRIC",null);

					result.put(StatisticTag.MEAN_OF_COMPONENTS_SHARE_AGNATIC.name(), agnaticPartition.meanShare());
					result.put(StatisticTag.MEAN_OF_COMPONENTS_SHARE_AGNATIC_WO_SINGLETON.name(), agnaticPartition.meanShare(2));
					result.put(StatisticTag.MAX_OF_COMPONENTS_SHARE_AGNATIC.name(), agnaticPartition.maxShare());
				}
				catch (PuckException exception)
				{
					logger.warn("Error on PartitionMaker.", exception);
					errorCount += 1;
				}

				try
				{
					Partition<Individual> uterinePartition = PartitionMaker.createRaw("MATRIC partition", source.individuals(), "MATRIC",null);

					result.put(StatisticTag.MEAN_OF_COMPONENTS_SHARE_UTERINE.name(), uterinePartition.meanShare());
					result.put(StatisticTag.MEAN_OF_COMPONENTS_SHARE_UTERINE_WO_SINGLETON.name(), uterinePartition.meanShare(2));
					result.put(StatisticTag.MAX_OF_COMPONENTS_SHARE_UTERINE.name(), uterinePartition.maxShare());
				}
				catch (PuckException exception)
				{
					logger.warn("Error on PartitionMaker.", exception);
					errorCount += 1;
				}

				result.put(StatisticTag.NUM_OF_ELEMENTARY_CYCLES.name(), StatisticsWorker.cyclomatic(marriageCount, filiationCount, source.individuals().size(), component.value()));

				try
				{
					Chronometer chronoBis = new Chronometer();
					result.put(StatisticTag.DEPTH.name(), StatisticsWorker.depth(source.individuals()));
					result.put(StatisticTag.DEPTH_MEAN.name(), StatisticsWorker.meanDepth(source.individuals()));

					//logger.debug("depth time: {}s", chronoBis.step().interval() / 1000);
				}
				catch (Exception exception)
				{
					logger.warn("Error on depth.", exception);
					errorCount += 1;
				}

				result.put(StatisticTag.MEAN_SPOUSE_OF_MEN.name(), StatisticsWorker.meanNumberOfSpouses(source.individuals(), Gender.MALE));
				result.put(StatisticTag.MEAN_SPOUSE_OF_WOMEN.name(), StatisticsWorker.meanNumberOfSpouses(source.individuals(), Gender.FEMALE));

				result.put(StatisticTag.MEAN_AGNATIC_FRATRY_SIZE.name(), StatisticsWorker.meanSibsetSize(source.individuals(), Gender.MALE));
				result.put(StatisticTag.MEAN_UTERINE_FRATRY_SIZE.name(), StatisticsWorker.meanSibsetSize(source.individuals(), Gender.FEMALE));
				result.put(StatisticTag.MEAN_CHILDREN_PER_FERTILE_COUPLE.name(), StatisticsWorker.meanSibsetSize(source.families()));

				logger.debug("Total Time: {}s", chrono.stop().interval() / 1000);

				//
				result.put(StatisticTag.BASIC_GSTATISTICS_ERROR.name(), errorCount);
			}
		}
		catch (Exception exception)
		{
			logger.warn("ERROR IN basic statistics building.", exception);
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public static Properties buildBasicStatistics(final RelationModel source)
	{
		Properties result;

		result = new Properties();

		try
		{
			int errorCount = 0;

			if (source != null)
			{
				RoleRelationWorker worker = new RoleRelationWorker(source, 2);

				result.put(StatisticTag.SELF_NAME.name(), worker.getRelations().getSelfName());
				result.put(StatisticTag.NUM_OF_TERMS.name(), worker.getTerms().size());
				result.put(StatisticTag.NUM_OF_FEMALES.name(), worker.getTermsByGender(Gender.FEMALE).size());
				result.put(StatisticTag.NUM_OF_FEMALES_EXCLUSIVE.name(), worker.getTermsByExclusiveGender(Gender.FEMALE).size());
				result.put(StatisticTag.NUM_OF_FEMALES_SPEAKER.name(), worker.getTermsByEgoGender(Gender.FEMALE).size());
				result.put(StatisticTag.NUM_OF_FEMALES_SPEAKER_EXCLUSIVE.name(), worker.getTermsByExclusiveEgoGender(Gender.FEMALE).size());

				result.put(StatisticTag.NUM_OF_MALES.name(), worker.getTermsByGender(Gender.MALE).size());
				result.put(StatisticTag.NUM_OF_MALES_EXCLUSIVE.name(), worker.getTermsByExclusiveGender(Gender.MALE).size());
				result.put(StatisticTag.NUM_OF_MALES_SPEAKER.name(), worker.getTermsByEgoGender(Gender.MALE).size());
				result.put(StatisticTag.NUM_OF_MALES_SPEAKER_EXCLUSIVE.name(), worker.getTermsByExclusiveEgoGender(Gender.MALE).size());

				result.put(StatisticTag.GENERATION_PATTERN_FEMALE.name(), worker.getGenerationPatterns()[1]);
				result.put(StatisticTag.GENERATION_PATTERN_MALE.name(), worker.getGenerationPatterns()[0]);
				result.put(StatisticTag.GENERATION_PATTERN_TOTAL.name(), worker.getGenerationPatterns()[3]);

				result.put(StatisticTag.NUM_OF_AUTORECIPROCAL_TERMS.name(), worker.getAutoReciprocalRoles().size());
				result.put(StatisticTag.RECURSIVE_TERMS.name(), worker.getRecursiveRolePattern());
				result.put(StatisticTag.ASCENDANT_CLASSIFICATION.name(), worker.getCollateralClassification()[0].name());
				result.put(StatisticTag.COUSIN_CLASSIFICATION.name(), worker.getCollateralClassification()[1].name());

				result.put(StatisticTag.KIN_TERM_NETWORK_FEMALE_COMPONENTS.name(), worker.getGraphStatistics(Gender.FEMALE, "NRCOMPONENTS").intValue());
				result.put(StatisticTag.KIN_TERM_NETWORK_FEMALE_CONCENTRATION.name(), worker.getGraphStatistics(Gender.FEMALE, "CONCENTRATION").doubleValue());
				result.put(StatisticTag.KIN_TERM_NETWORK_FEMALE_SPEAKER_DENSITY.name(), worker.getGraphStatistics(Gender.FEMALE, "DENSITY").doubleValue());
				result.put(StatisticTag.KIN_TERM_NETWORK_FEMALE_SPEAKER_MEAN_CLUSTERING_COEFFICIENT.name(), worker.getGraphStatistics(Gender.FEMALE, "MEANCLUSTERINGCOEFF").doubleValue());
				result.put(StatisticTag.KIN_TERM_NETWORK_FEMALE_SPEAKER_MEAN_DEGREE.name(), worker.getGraphStatistics(Gender.FEMALE, "MEANDEGREE").doubleValue());

				result.put(StatisticTag.KIN_TERM_NETWORK_MALE_COMPONENTS.name(), worker.getGraphStatistics(Gender.MALE, "NRCOMPONENTS").intValue());
				result.put(StatisticTag.KIN_TERM_NETWORK_MALE_CONCENTRATION.name(), worker.getGraphStatistics(Gender.MALE, "CONCENTRATION").doubleValue());
				result.put(StatisticTag.KIN_TERM_NETWORK_MALE_SPEAKER_DENSITY.name(), worker.getGraphStatistics(Gender.MALE, "DENSITY").doubleValue());
				result.put(StatisticTag.KIN_TERM_NETWORK_MALE_SPEAKER_MEAN_CLUSTERING_COEFFICIENT.name(), worker.getGraphStatistics(Gender.MALE, "MEANCLUSTERINGCOEFF").doubleValue());
				result.put(StatisticTag.KIN_TERM_NETWORK_MALE_SPEAKER_MEAN_DEGREE.name(), worker.getGraphStatistics(Gender.MALE, "MEANDEGREE").doubleValue());

				//
				result.put(StatisticTag.BASIC_TSTATISTICS_ERROR.name(), errorCount);
			}
		}
		catch (Exception exception)
		{
			logger.warn("ERROR IN STAG REFRESHING", exception);
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param source
	 */
	public static Properties buildCircuitStatistics(final Net source)
	{
		Properties result;

		result = new Properties();

		try
		{
			int errorCount = 0;

			if (source != null)
			{
				Chronometer chrono = new Chronometer().start();

				//
				try
				{
					CountAndRate data = analyzeCircuitCensus(CROSS_FIRST_COUSIN_MARRIAGE_PATTERN, source);

					result.put(StatisticTag.NUM_OF_CROSS_FIRST_COUSIN_MARRIAGES.name(), data.getCount());
					result.put(StatisticTag.RATE_OF_CROSS_FIRST_COUSIN_MARRIAGES.name(), data.getRate());

					//logger.debug("step 08: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
				}
				catch (Exception exception)
				{
					logger.error("Error on cross first cousin marriages.", exception);
					errorCount += 1;
				}

				//
				try
				{
					chrono.reset();
					CountAndRate data = analyzeCircuitCensus(LEVIRATE_MARRIAGE_PATTERN, source);

					result.put(StatisticTag.NUM_OF_LEVIRATE_MARRIAGES.name(), data.getCount());
					result.put(StatisticTag.RATE_OF_LEVIRATE_MARRIAGES.name(), data.getRate());

					//logger.debug("step 09: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
				}
				catch (Exception exception)
				{
					logger.error("Error on levirate marriages.", exception);
					errorCount += 1;
				}

				//
				try
				{
					chrono.reset();
					CountAndRate data = analyzeCircuitCensus(SORORATE_MARRIAGE_PATTERN, source);

					result.put(StatisticTag.NUM_OF_SORORATE_MARRIAGES.name(), data.getCount());
					result.put(StatisticTag.RATE_OF_SORORATE_MARRIAGES.name(), data.getRate());

					//logger.debug("step 10: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
				}
				catch (Exception exception)
				{
					logger.error("Error on sororate marriages.", exception);
					errorCount += 1;
				}

				//
				try
				{
					chrono.reset();
					CountAndRate data = analyzeCircuitCensus(DOUBLE_OR_EXCHANGE_MARRIAGE_PATTERN, source);

					result.put(StatisticTag.NUM_OF_DOUBLE_OR_EXCHANGE_MARRIAGES.name(), data.getCount());
					result.put(StatisticTag.RATE_OF_DOUBLE_OR_EXCHANGE_MARRIAGES.name(), data.getRate());

					// logger.debug("step 11: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
				}
				catch (Exception exception)
				{
					logger.error("Error on double or exchange marriages.", exception);
					errorCount += 1;
				}

				//
				try
				{
					chrono.reset();
					CountAndRate data = analyzeCircuitCensus(NIECE_NEPHEW_MARRIAGE_PATTERN, source);

					result.put(StatisticTag.NUM_OF_NIECE_NEPHEW_MARRIAGES.name(), data.getCount());
					result.put(StatisticTag.RATE_OF_NIECE_NEPHEW_MARRIAGES.name(), data.getRate());

					// logger.debug("step 12: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
				}
				catch (Exception exception)
				{
					logger.error("Error on Niece/Nephew marriages.", exception);
					errorCount += 1;
				}

				//
				try
				{
					chrono.reset();
					CountAndRate data = analyzeCircuitCensus(PARALLEL_FIRST_COUSIN_MARRIAGE_PATTERN, source);

					result.put(StatisticTag.NUM_OF_PARALLEL_FIRST_COUSIN_MARRIAGES.name(), data.getCount());
					result.put(StatisticTag.RATE_OF_PARALLEL_FIRST_COUSIN_MARRIAGES.name(), data.getRate());

					//logger.debug("step 13: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
				}
				catch (Exception exception)
				{
					logger.error("Error on parallel first cousin marriages.", exception);
					errorCount += 1;
				}

				//
				try
				{
					chrono.reset();
					CountAndRate data = analyzeCircuitCensus(FIRST_COUSIN_MARRIAGE_PATTERN, source);

					result.put(StatisticTag.NUM_OF_FIRST_COUSIN_MARRIAGES.name(), data.getCount());
					result.put(StatisticTag.RATE_OF_FIRST_COUSIN_MARRIAGES.name(), data.getRate());

					//logger.debug("step 14: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
				}
				catch (Exception exception)
				{
					logger.error("Error on first cousin marriages.", exception);
					errorCount += 1;
				}

				//
				try
				{
					chrono.reset();
					CountAndRate data = analyzeCircuitCensus(DOUBLE_MARRIAGE_PATTERN, source);

					result.put(StatisticTag.NUM_OF_DOUBLE_MARRIAGES.name(), data.getCount());
					result.put(StatisticTag.RATE_OF_DOUBLE_MARRIAGES.name(), data.getRate());

					//logger.debug("step 15: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
				}
				catch (Exception exception)
				{
					logger.error("Error on double marriages.", exception);
					errorCount += 1;
				}

				//
				try
				{
					chrono.reset();
					CountAndRate data = analyzeCircuitCensus(EXCHANGE_MARRIAGE_PATTERN, source);

					result.put(StatisticTag.NUM_OF_EXCHANGE_MARRIAGES.name(), data.getCount());
					result.put(StatisticTag.RATE_OF_EXCHANGE_MARRIAGES.name(), data.getRate());

					//logger.debug("step 16: {}s [{}]", chrono.step().interval() / 1000, source.getLabel());
				}
				catch (Exception exception)
				{
					logger.error("Error on exchange marriages.", exception);
					errorCount += 1;
				}

				//
				result.put(StatisticTag.BASIC_GSTATISTICS_ERROR.name(), errorCount);
			}
		}
		catch (Exception exception)
		{
			logger.warn("ERROR IN basic statistics building.", exception);
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param source
	 */
	public static Properties buildControlStatistics(final Net source)
	{
		Properties result;

		result = new Properties();

		try
		{
			for (ControlReporter.ControlType type : ControlReporter.ControlType.values())
			{
				Report report = ControlReporter.reportControls(source, (ResourceBundle) null, type);
				if (report != null)
				{
					result.put(type.name(), report.status());
				}
			}
		}
		catch (Exception exception)
		{
			System.out.println("ERRRORRRR");
			exception.printStackTrace();
			logger.error("Error controls.", exception);
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param source
	 */
	public static Properties buildControlStatistics(final RelationModel source)
	{
		Properties result;

		result = new Properties();

		try
		{
			for (RelationModelReporter.ControlType type : RelationModelReporter.ControlType.values())
			{
				Report report = RelationModelReporter.reportControl(source, type);
				if (report != null)
				{
					result.put(type.name(), report.status());
				}
			}
		}
		catch (Exception exception)
		{
			logger.error("Error controls.", exception);
		}

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param source
	 */
	public static Properties buildGeographyStatistics(final Net source)
	{
		Properties result;

		result = new Properties();

		try
		{
			if (source != null)
			{
				result.put(StatisticTag.GEOGRAPHY_AVAILABLE.name(), (source.getGeography() != null));
			}
		}
		catch (Exception exception)
		{
			logger.warn("ERROR IN geography statistics building.", exception);
		}

		logger.debug("Done.");

		//
		return result;
	}
}
