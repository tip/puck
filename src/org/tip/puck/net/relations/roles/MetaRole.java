package org.tip.puck.net.relations.roles;

import org.tip.puck.graphs.Link.LinkType;
import org.tip.puck.net.AlterAge;
import org.tip.puck.net.Gender;
import org.tip.puck.net.KinType;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.relations.Roles;


/**
 * 
 * @author TIP
 */
public class MetaRole extends Role {
	

	Gender egoGender;
	Gender alterGender;
	AlterAge alterAge;
	boolean cross;
	
	
	public MetaRole (String name){
		super(name);
		this.egoGender = Gender.UNKNOWN;
		this.alterGender = Gender.UNKNOWN;
		this.alterAge= AlterAge.UNKNOWN;
	}
	
	public MetaRole (String name, Gender egoGender, Gender alterGender, AlterAge alterAge){
		super(name);
		this.egoGender = egoGender;
		this.alterGender = alterGender;
		this.alterAge = alterAge;
	}
	
	public MetaRole(String ageLetter, String kinLetter, String egoGenderLetter){
		this.egoGender = Gender.valueOf(egoGenderLetter);
		
	}
	
	public MetaRole clone(){
		MetaRole result;
		
		result = new MetaRole(this.name,this.egoGender,this.alterGender,this.alterAge);
		result.setCross(cross);
		
		//
		return result;
	}

	
	AlterAge invertAge(){
		AlterAge result;
		
		if (alterAge==null){
			result = null;
		} else {
			result = alterAge.invert();
		}
		//
		return result;
	}
	
	boolean isCross(){
		boolean result;
		
		result = cross || (!egoGender.isUnknown() && egoGender.invert() == alterGender);
		
		//
		return result;
	}
	
	boolean isParallel(){
		boolean result;
		
		result = (!egoGender.isUnknown() && egoGender == alterGender);
		
		//
		return result;
	}
	
	String invertName(){
		String result;
		
		result = getKinType().inverse().toString();
		//
		return result;
	}
	
	static String invertName(String name){
		return new MetaRole(name).invertName();
	}
	
	/**
	 * 
	 */
	@Override
	public boolean equals(final Object role) {
		boolean result;
		
		result = role!=null && 
				(this.name == null ? ((MetaRole) role).name == null : this.name.equals(((MetaRole) role).name)) && 
				this.egoGender == ((MetaRole) role).egoGender && 
				this.alterGender == ((MetaRole) role).alterGender && 
				this.alterAge == ((MetaRole)role).alterAge  &&
				this.isCross() == ((MetaRole)role).isCross();
		//
		return result;
	}
	
	public int compareTo (Role other){
		int result;
		
		result = 0;

		if (other!=null){
			
			if (name!=null && ((MetaRole)other).name == null){
				
				result = -1;
				
			} else if (name==null && ((MetaRole)other).name != null){
				
				result = 1;
				
			} else {

				if (name != null && ((MetaRole)other).name != null){
					
					result = getKinType().compareTo(((MetaRole)other).getKinType());
				}
				
				if (result == 0){
					
					result = alterGender.compareToUnknownFirst(((MetaRole)other).alterGender);

					if (result == 0){
						
						result = egoGender.compareToUnknownFirst(((MetaRole)other).egoGender);
						
						if (result == 0){
							
							result = alterAge.compareTo(((MetaRole)other).alterAge);
							
							if (result == 0){
								
								if (isCross() && !((MetaRole)other).isCross()){
									
									result = -1;
									
								} else if (!isCross() && ((MetaRole)other).isCross()){
									
									result = +1;
								}
							}				 

						}		
					}
				}
			}
		}
		
		//
		return result;
	}
	
	
	public String toString(){
		String result;
		
		result = name;
		
		if (result == null){
			result = "-";
		}
		
		String suffix = "";
		String prefix = "";
		
		if (egoGender!=null && !egoGender.isUnknown()){
			suffix += " "+egoGender.toGedChar()+".S.";
		} else if (cross){
			suffix += " #S.";
		}
		if (alterAge!=null && !alterAge.isUnknown()){
			prefix += alterAge+" ";
		}
		if (alterGender!=null && !alterGender.isUnknown()){
			prefix += alterGender+" ";
		}
		
		result = prefix+result+suffix;
		//
		return result;
	}
	
	public String toStandardString(){
		String result;
		
		result = "";
		
		switch (getKinType()){
		case PARENT:
			switch (alterGender){
			case MALE:
				result = "Father";
				break;
			case FEMALE:
				result = "Mother";
				break;
			case UNKNOWN:
				result = "Parent";
				break;
			}
			break;
		case CHILD:
			switch (alterGender){
			case MALE:
				result = "Son";
				break;
			case FEMALE:
				result = "Daughter";
				break;
			case UNKNOWN:
				result = "Child";
				break;
			}
			break;
		case SIBLING:
			switch (alterGender){
			case MALE:
				result = "Brother";
				break;
			case FEMALE:
				result = "Sister";
				break;
			case UNKNOWN:
				result = "Sibling";
				break;
			}
			break;
		case SPOUSE:
			switch (alterGender){
			case MALE:
				result = "Husband";
				break;
			case FEMALE:
				result = "Wife";
				break;
			case UNKNOWN:
				result = "Spouse";
				break;
			}
			break;
		case SELF:
			break;
		}
		
		if (!alterAge.isUnknown()){
			result = alterAge.toLetter()+result;
		}
				
		//
		return result;
	}
	
	public String toLetter(){
		String result;
		
		result = "";
		
		switch (getKinType()){
		case PARENT:
			switch (alterGender){
			case MALE:
				result = "F";
				break;
			case FEMALE:
				result = "M";
				break;
			case UNKNOWN:
				result = "Pa";
				break;
			}
			break;
		case CHILD:
			switch (alterGender){
			case MALE:
				result = "S";
				break;
			case FEMALE:
				result = "D";
				break;
			case UNKNOWN:
				result = "Ch";
				break;
			}
			break;
		case SIBLING:
			switch (alterGender){
			case MALE:
				result = "B";
				break;
			case FEMALE:
				result = "Z";
				break;
			case UNKNOWN:
				result = "Sb";
				break;
			}
			break;
		case SPOUSE:
			switch (alterGender){
			case MALE:
				result = "H";
				break;
			case FEMALE:
				result = "W";
				break;
			case UNKNOWN:
				result = "Sp";
				break;
			}
			break;
		case SELF:
			break;
		}
		
		if (!alterAge.isUnknown()){
			result = alterAge.toLetter()+result;
		}
				
		//
		return result;
	}
	
	
	
	boolean specifies (MetaRole role){
		boolean result;
		
		result = name.equals(role.name) &&
			(alterGender.specifies(role.alterGender)) &&
			(egoGender.specifies(role.egoGender)) &&
			(alterAge.matches(role.alterAge)) &&
			(isCross() || !role.isCross());

		//
		return result;
	}
	
	boolean matches (MetaRole role){
		boolean result;
		
		result = name.equals(role.name) &&
			(alterGender.matchs(role.alterGender)) &&
			(egoGender.matchs(role.egoGender)) &&
			(alterAge.matches(role.alterAge));
		//
		return result;
	}

	boolean specifiesAbsolute (MetaRole role){
		boolean result;
		
		result = 
				(alterGender.specifies(role.alterGender)) &&
				(egoGender.specifies(role.egoGender)) &&
				(alterAge.matches(role.alterAge)) &&
				(isCross() || !role.isCross());

		//
		return result;
	}

	boolean matchesAbsolute (MetaRole role){
		boolean result;
		
		result = 
				(alterGender.matchs(role.alterGender)) &&
				(egoGender.matchs(role.egoGender)) &&
				(alterAge.matches(role.alterAge));
		//
		return result;
	}

	Gender getEgoGender() {
		return egoGender;
	}

	Gender getAlterGender() {
		return alterGender;
	}

	AlterAge getAlterAge() {
		return alterAge;
	}

	void setAlterAge(AlterAge alterAge) {
		this.alterAge = alterAge;
	}

	void setEgoGender(Gender egoGender) {
		this.egoGender = egoGender;
	}

	void setAlterGender(Gender alterGender) {
		this.alterGender = alterGender;
	}
	
	LinkType getLinkType(){
		LinkType result;
		
		if (getKinType()==getKinType().inverse()){
			result = LinkType.EDGE;
		} else {
			result = LinkType.ARC;
		}
		//
		return result;
	}
	
	KinType getKinType (){
		KinType result;
		
		if (name==null){
			result = null;
		} else {
			result = KinType.valueOf(name);
		}
		
		return result;
	}
	
	MetaRole absolute(){
		MetaRole result;
		
		result = clone();
		result.setName(null);
		
		//
		return result;
	}
	
	boolean equalsAbsolute(MetaRole other){
		boolean result;
		
		result = this.absolute().equals(other.absolute());
		
		//
		return result;
	}
	
	boolean containsAbsolute(Roles roles, MetaRole role){
		boolean result;
		
		result = false;
		
		for (Role otherRole : roles){
			if (((MetaRole)otherRole).equalsAbsolute(role)){
				result = true;
				break;
			}
		}
		//
		return result;
	}
	
	boolean isParent(){
		return getKinType()==KinType.PARENT;
	}
	
	boolean isChild(){
		return getKinType()==KinType.CHILD;
	}
	
	boolean isSpouse(){
		return getKinType()==KinType.SPOUSE;
	}
	
	boolean isSibling(){
		return getKinType()==KinType.SIBLING;
	}
	
	boolean isSelf(){
		return getKinType()==KinType.SELF;
	}

	void setCross(boolean cross) {
		this.cross = cross;
	}
	
	MetaRole egoGenderNeutral(){
		MetaRole result;
		
		result = clone();
		result.setEgoGender(Gender.UNKNOWN);
		
		//
		return result;
	}
	
	MetaRole alterGenderNeutral(){
		MetaRole result;
		
		result = clone();
		result.setAlterGender(Gender.UNKNOWN);
		
		//
		return result;
	}
	
	MetaRole alterAgeNeutral(){
		MetaRole result;
		
		result = clone();
		result.setAlterAge(AlterAge.UNKNOWN);
		
		//
		return result;
	}
	
	
	MetaRole crossGenderComplement(){
		MetaRole result;
		
		if (egoGender.isUnknown() || alterGender.isUnknown() || !isCross()){
			result = null;
		} else {
			result = clone();
			result.setEgoGender(alterGender);
			result.setEgoGender(egoGender);
		}
		
		//
		return result;
	}


	MetaRole egoGenderComplement(){
		MetaRole result;
		
		if (egoGender.isUnknown()){
			result = null;
		} else {
			result = clone();
			result.setEgoGender(egoGender.invert());
		}
		
		//
		return result;
	}
	
	MetaRole alterGenderComplement(){
		MetaRole result;
		
		if (alterGender.isUnknown()){
			result = null;
		} else {
			result = clone();
			result.setAlterGender(alterGender.invert());
		}
		
		//
		return result;
	}
	
	MetaRole alterAgeComplement(){
		MetaRole result;
		
		if (alterAge.isUnknown()){
			result = null;
		} else {
			result = clone();
			result.setAlterAge(alterAge.invert());
		}
		
		//
		return result;
	}
	


	

	
	

}
