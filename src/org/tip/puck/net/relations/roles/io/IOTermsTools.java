package org.tip.puck.net.relations.roles.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Attributes;
import org.tip.puck.net.relations.roles.RoleRelationMaker.RoleRelationRule;
import org.tip.puck.net.relations.roles.RoleRelationRules;
import org.tip.puckgui.Preferences;

import fr.devinsy.util.StringList;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class IOTermsTools {

	private static Logger logger = LoggerFactory.getLogger(IOTermsTools.class);
	public static final String DEFAULT_CHARSET_NAME = "UTF-8";
	public static final int MAX_LINE_SIZE = 2048;

	/**
	 * 
	 */
	private IOTermsTools() {
	}

	/**
	 * @param attributes
	 * @return
	 * @throws PuckException
	 */
	public static RoleRelationRules extractRelationRoleRules(final Attributes attributes) throws PuckException {
		RoleRelationRules result;

		result = new RoleRelationRules();

		Attributes rulesFields = attributes.searchByLabel("[Rr][Oo][Ll][Ee][_ ]?[Rr][Ee][Ll][Aa][Tt][Ii][Oo][Nn][_ ]?[Rr][Uu][Ll][Ee][Ss]");
		logger.debug("rulesFields count=[{}]", rulesFields.size());
		if (!rulesFields.isEmpty()) {

			Attribute rulesField = rulesFields.toList().get(0);

			if (StringUtils.isNotBlank(rulesField.getValue())) {

				String[] labels = rulesField.getValue().split("[,;]");
				for (String label : labels) {
					if (StringUtils.isNotBlank(label)) {
						RoleRelationRule rule = RoleRelationRule.valueOf(label.trim());
						logger.debug("rule valueof=[{}]", rule);
						if (rule == null) {
							throw PuckExceptions.BAD_FILE_FORMAT.create("Unknown rule label [" + label + "]");
						} else {
							result.add(rule);
						}
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param attributes
	 * @return
	 */
	public static String extractSelfName(final Attributes attributes) {
		String result;

		Attributes selfNameAttributes = attributes.searchByLabel("[Ss][Ee][Ll][Ff][_ ]?[Nn][Aa][Mm][Ee]");
		if (selfNameAttributes.isEmpty()) {

			result = Preferences.DEFAULT_TERMINOLOGY_SELFNAME;

		} else {

			Attribute selfNameAttribute = selfNameAttributes.toList().get(0);

			if (StringUtils.isBlank(selfNameAttribute.getValue())) {

				result = Preferences.DEFAULT_TERMINOLOGY_SELFNAME;

			} else {

				result = selfNameAttribute.getValue();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param fileName
	 * @return
	 */
	public static String getBaseName(final File source, final String extension) {
		String result;

		if (source == null) {
			throw new IllegalArgumentException("Null parameter.");
		} else if (!source.isFile()) {
			throw new IllegalArgumentException("Parameter is not a file.");
		} else {
			String fileName = source.getName();

			int extensionStartIndex = fileName.lastIndexOf(extension);
			if (extensionStartIndex == -1) {
				result = fileName;
			} else {
				result = fileName.substring(0, extensionStartIndex);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws PuckException
	 */
	public static void readAttributesVertically(final Attributes target, final BufferedReader in) throws PuckException {

		try {
			//
			in.mark(MAX_LINE_SIZE);
			String line = readNotEmptyLine(in);

			if (line != null) {

				if (line.split("\\\t").length == 2) {

					boolean ended = false;
					while (!ended) {

						line = in.readLine();
						if (StringUtils.isBlank(line)) {

							ended = true;

						} else {

							String[] tokens = line.split("\\\t");

							String label = tokens[0];
							String value = tokens[1];

							//
							if (StringUtils.isNotBlank(label)) {
								target.put(label, value);
								logger.debug("add attribute [{}][{}]", label, value);
							}
						}
					}

				} else {

					in.reset();
				}
			}

		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading attribute lines.");
		}
	}

	/**
	 * 
	 * @param in
	 * @return
	 * @throws PuckException
	 */
	public static StringList readLines(final BufferedReader in) throws PuckException {
		StringList result;

		result = new StringList();

		boolean ended = false;
		while (!ended) {
			//
			String source = readNotEmptyLine(in);
			if (source == null) {
				ended = true;
			} else {
				result.append(source);
			}
		}
		//
		return result;
	}

	/**
	 * 
	 * @param in
	 * @return
	 * @throws PuckException
	 */
	public static String readNotEmptyLine(final BufferedReader in) throws PuckException {
		String result;

		try {
			boolean ended = false;
			result = null;
			while (!ended) {
				//
				String line = in.readLine();

				if (line == null) {
					ended = true;
					result = null;
				} else if (StringUtils.isNotBlank(line)) {
					ended = true;
					result = line;
				}
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading line.");
		}

		//
		return result;
	}

	/**
	 * 
	 * @param in
	 * @return
	 * @throws PuckException
	 */
	public static StringList readTermRuleLines(final BufferedReader in) throws PuckException {
		StringList result;

		try {

			result = new StringList();

			// Read optional header line.
			in.mark(MAX_LINE_SIZE);
			String source = readNotEmptyLine(in);
			if (source != null) {
				if (source.toUpperCase().startsWith("TERM")) {

					logger.debug("header line detected");
				} else {

					in.reset();
					logger.debug("header line missing (optional)");
				}
			}

			// Read lines.
			result = readLines(in);

		} catch (IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading attribute lines.");
		}

		//
		return result;
	}
}
