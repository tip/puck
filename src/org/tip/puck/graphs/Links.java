package org.tip.puck.graphs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.graphs.Link.LinkType;

/**
 * 
 * @author TIP
 */
public class Links<E> extends ArrayList<Link<E>> implements Iterable<Link<E>> {

	private static final long serialVersionUID = -7834919205208300074L;

	/**
	 *
	 */
	public Links() {
		super();
	}
	
	public Links (List<Link<E>> source){
		
		for (Link<E> link : source){
			this.add(link);
		}
	}

	/**
	 *
	 */
	public Links(final int initialCapacity) {
		super(initialCapacity);
	}

	/**
	 * 
	 * @return
	 */
	public double averagePositiveWeight() {
		double result;

		//
		double sum = 0;

		//
		int count = 0;
		for (Link<E> link : this) {
			if ((link != null) && (link.getWeight() > 0)) {
				count += 1;
				sum += link.getWeight();
			}
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum / count;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double averageWeight() {
		double result;

		if (size() == 0) {
			result = 0;
		} else {
			result = sumWeight() / size();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetNode
	 * @return
	 */
	public Link<E> getArc(final Node<E> sourceNode, final Node<E> targetNode) {
		Link<E> result;

		boolean ended = false;
		result = null;
		Iterator<Link<E>> iterator = this.iterator();
		while (!ended) {
			if (iterator.hasNext()) {
				Link<E> link = iterator.next();
				if ((link.getType() == LinkType.ARC) && (link.getSourceNode() == sourceNode) && (link.getTargetNode() == targetNode)) {
					ended = true;
					result = link;
				}
			} else {
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Node<E>> getArcs() {
		List<Node<E>> result;

		//
		result = new ArrayList<Node<E>>();
		for (Link<E> link : this) {
			if (link.isArc()) {
				result.add(link.getSourceNode());
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetNode
	 * @return
	 */
	public double getArcWeight(final Node<E> sourceNode, final Node<E> targetNode) {
		double result;

		Link<E> link = this.getArc(sourceNode, targetNode);
		if (link == null) {
			result = 0;
		} else {
			result = link.getWeight();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Links<E> getByTag(final String pattern) {
		Links<E> result;

		result = new Links<E>();
		for (Link<E> link : this) {
			for (String tag : link.getTags()){
				if (StringUtils.equals(tag, pattern)) {
					result.add(link);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetNode
	 * @return
	 */
	public Link<E> getEdge(final Node<E> sourceNode, final Node<E> targetNode) {
		Link<E> result;

		boolean ended = false;
		result = null;
		Iterator<Link<E>> iterator = this.iterator();
		while (!ended) {
			if (iterator.hasNext()) {
				Link<E> link = iterator.next();
				if ((link.getType() == LinkType.EDGE) && (link.getSourceNode() == sourceNode) && (link.getTargetNode() == targetNode)) {
					ended = true;
					result = link;
				}
			} else {
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Node<E>> getEdges() {
		List<Node<E>> result;

		//
		result = new ArrayList<Node<E>>();
		for (Link<E> link : this) {
			if (link.isEdge()) {
				result.add(link.getSourceNode());
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetNode
	 * @return
	 */
	public double getEdgeWeight(final Node<E> sourceNode, final Node<E> targetNode) {
		double result;

		Link<E> link = this.getEdge(sourceNode, targetNode);
		if (link == null) {
			result = 0;
		} else {
			result = link.getWeight();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetNode
	 * @return
	 */
	public Link<E> getLink(final Node<E> sourceNode, final Node<E> targetNode) {
		Link<E> result;

		boolean ended = false;
		result = null;
		Iterator<Link<E>> iterator = this.iterator();
		while (!ended) {
			if (iterator.hasNext()) {
				Link<E> link = iterator.next();
				if ((link.getSourceNode() == sourceNode) && (link.getTargetNode() == targetNode)) {
					ended = true;
					result = link;
				}
			} else {
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetNode
	 * @return
	 */
	public double getLinkWeight(final Node<E> sourceNode, final Node<E> targetNode) {
		double result;

		Link<E> link = this.getLink(sourceNode, targetNode);
		if (link == null) {
			result = 0;
		} else {
			result = link.getWeight();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Node<E>> getSourceNodes() {
		List<Node<E>> result;

		//
		result = new ArrayList<Node<E>>();
		for (Link<E> link : this) {
			result.add(link.getSourceNode());
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Node<E>> getSourceNodesSortedByLabel() {
		List<Node<E>> result;

		//
		result = getSourceNodes();
		Collections.sort(result, new NodeComparatorByLabel<E>());

		//
		return result;
	}

	/**
	 * TODO Improve result.contains.
	 * 
	 * @return
	 */
	public List<String> getTags() {
		List<String> result;

		result = new ArrayList<String>();
		for (Link<E> link : this) {
			for (String tag : link.getTags()){
				if ((StringUtils.isNotBlank(tag)) && (!result.contains(tag))) {
					result.add(tag);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Node<E>> getTargetNodes() {
		List<Node<E>> result;

		//
		result = new ArrayList<Node<E>>();
		for (Link<E> link : this) {
			result.add(link.getTargetNode());
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Node<E>> getTargetNodesSortedByLabel() {
		List<Node<E>> result;

		//
		result = getTargetNodes();
		Collections.sort(result, new NodeComparatorByLabel<E>());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty() {
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double maxWeight() {
		double result;

		if (this.size() == 0) {
			result = 0;
		} else {
			result = Double.MIN_VALUE;
			for (Link<E> link : this) {
				if ((link != null) && (link.getWeight() > result)) {
					result = link.getWeight();
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double minWeight() {
		double result;

		if (this.size() == 0) {
			result = 0;
		} else {
			result = Double.MAX_VALUE;
			for (Link<E> link : this) {
				if ((link != null) && (link.getWeight() < result)) {
					result = link.getWeight();
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double sumWeight() {
		double result;

		result = 0;
		for (Link<E> link : this) {
			if (link != null) {
				result += link.getWeight();
			}
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Link<E>> toList() {
		List<Link<E>> result;

		result = new ArrayList<Link<E>>(this);

		//
		return result;
	}
	
	public String toString(){
		return toList().toString();
	}


}
