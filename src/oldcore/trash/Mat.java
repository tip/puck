package oldcore.trash;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * This static class contains a number of static methods for mathematical operations that are used by more than one class
 * @author Klaus Hamberger
 * @since Version 0.6
 *
 */
public class Mat {
	
	/**
	 * compares two integers
	 * @param a the first integer
	 * @param b the second integer
	 * @return 1 if the i > j, -1 if j > i, 0 if i = j
	 */
/*	public static int compare (int a, int b){
		return new Integer(a).compareTo(b);
//		if (a==b) return 0;
//		return sign(a-b);
	}*/
	
	/**
	 * compares the absolute value of two integers
	 * @param a the first integer
	 * @param b the second integer
	 * @return 1 if the |i| > |j|, -1 if |j| > |i|, 0 if |i| = |j|
	 */
/*	public static int compareAbs (int a, int b) {
//		if (Math.abs(a)==Math.abs(b)) return compare(a,b);
		return compare(Math.abs(a),Math.abs(b));*/
/*	      if (a==b) return 0;
	      if (Math.abs(a)==Math.abs(b)) return (a/Math.abs(a));  
	      return (Math.abs(a)-Math.abs(b))/Math.abs(Math.abs(a)-Math.abs(b));*/
	   }

	/**
	 * checks whether an integer differs from a nonnegative second integer
	 * <p> used for the unilinear filter criterion
	 * @param i the integer to be checked
	 * @param g the integer to be compared
	 * @return true if the second integer is nonnegative and differs from the first
	 */
/*	public static boolean noMatch (int i, int g){
		return g>-1 && i!=g;
	}*/
	
	

	/**
	 * returns an integer as a percentage of another integer, with a given number of decimal positions
	 * @param a the first integer
	 * @param b the second integer
	 * @param dec the number of decimal positions
	 * @return the first integer as a percentage of the second
	 */
/*	public static double percent (int a, int b, int dec) {
		  if (b==0) return 0.;
	      return percent (new Double(a).doubleValue(),new Double(b).doubleValue(),dec);
	   }*/

	/**
	 * returns the sign of an integer
	 * @return the sign of the integer (1 positive, -1 negative, 0 zero)
	 */
/*	private static int sign (int k){
		return k/Math.abs(k);
	}*/
	
	/**
	 * gets a random permutation of n integers
	 * http://www.cs.princeton.edu/introcs/14array/Permutation.java.html
	 * @return a random permutation of n integers
	 */
/*	public static int[] randomPermutation1 (int n){
		int[] a = new int[n];
		for (int i = 0; i<n; i++){
	         a[i] = i;
		}
		for (int i = 0; i < n; i++) {
	         int r = (int)(Math.random()*(i+1));  
	         int swap = a[r];
	         a[r] = a[i];
	         a[i] = swap;
		}
		return a;
	}*/
	
	/**
	 * gets a random permutation of n integers
	 * @return a random permutation of n integers
	 */
/*	public static int[] randomPermutation (int n){
		List<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i<n; i++){
	         list.add(i);
		}
		return permute(list);
	}*/
	
	/**
	 * permutes an integer list and returns the result as an array
	 * @param a the array of permuted integers
	 * @param list the 
	 */
/*	private static int[] permute(List<Integer> list){
		int[] a = new int[list.size()];
		for (int i = a.length-1; i >= 0; i--) {
	         int r = (int)(Math.random()*(i+1));  
	         a[i] = list.get(r);
	         list.remove(r);
		}
		return a;
	}*/
	
	
/*	public static int[] randomPermutation (int[] p){
		int n = p.length;
		int[] a = new int[n];
		Map<Integer,List<Integer>> lists = new TreeMap<Integer,List<Integer>>();
		for (int i = 0; i<n; i++){
	         a[i] = i;
	         int k = p[i];
	         if (lists.get(k)==null) lists.put(k,new ArrayList<Integer>());
	         lists.get(k).add(i);
		}
		int m = lists.size();
		int[][] perm = new int[m][];
		int j = 0;
		for (List<Integer> list : lists.values()){
			perm[j] = permute(list);
			j++;
		}
		int[] index = new int[m];
		for (int i=0;i<n;i++){
			int k = p[i];
			a[i]=perm[k][index[k]];
			index[k]++;
		}
		return a;
	}*/
	
	
	/**
	 * returns the square of an integer
	 * @param i the integer
	 * @return the square of the integer
	 */
/*	public static int pow2 (int i){
		return new Double(Math.pow(2,i)).intValue();
	}*/


	//More general use? 
	/**
	 * returns the number of possible matchings for a given number of elements
	 * @param n the number of elements
	 * @return the number of possible matchings
	 * @see partitions.OldNodePartition#relationStatistics()
	 */
/*	public static int rel(int n){
		return n*(n-1)/2;
	}*/

	/**
	 * returns a binary random value (0 vs 1) with a probability of p
	 * @param p the probability of event 0
	 * @return the random value 0 or 1
	 */
/*	public static int alea (double p) {
		if (event(p)) return 0;
		return 1;
	}*/

	/**
	 * returns a boolean random value (occurence or non-occurence of an event) with a probability of p 
	 * @param p the probability of the event 
	 * @return true if the random event occurs
	 */
/*	public static boolean event (double p) {
		return (Math.random() <= p);
	}*/

	/**
	 * checks whether an integer is beyond a given horizon around an expected value
	 * @param x the integer to be checked
	 * @param y the expected value 
	 * @param var the tolerance interval
	 * @return true if the integer is out of bounds 
	 */
/*	public static boolean outOfBounds (int x, int y, int var) {
		return ((x<(y-var)) || (x>=(y+var)));
	}*/

	/**
	 * generates a random number within given bounds
	 * @param min the lower bound
	 * @param max the upper bound
	 * @return a random number between min and max
	 * @since last modified 10-05-14
	 */
/*	public static int randomNumber (int min, int max) {
		return min+(int)(Math.random()*(max-min));
//		return min+(new Double(Math.floor(Math.random()*(max-min))).intValue());
	}*/
	
	/**
	 * generates a random number with given upper bound
	 * @param max the upper bound
	 * @return a random number between zero and max
	 * @since 10-05-14
	 */
/*	public static int randomNumber(int max){
		return randomNumber(0,max);
	}*/

	/**
	 * augments the values of an array by those of another array
	 * @param a the first array
	 * @param b the second array
	 * @since 10-08-16
	 */
/*	public static void addArray (double[]a, double[]b){
		for (int i=0;i<a.length;i++){
			a[i] = a[i]+b[i];
		}
	}*/

	//!
	/**
	 * augments the values of a matrix by those of another matrix
	 * @param A the first matrix
	 * @param B the second matrix
	 * @since 10-08-16
	 */
/*!	public static void addMatrix(Matrix A, Matrix B){
		for (int i=0;i<A.getRowDim();i++){
			for (int j=0;j<A.getRowDim();j++){
				A.set(i,j,A.get(i,j)+B.get(i,j));
			}
		}
	}*/

	/**
	 * divides the values of an array by an integer
	 * @param a the array
	 * @param n the integer
	 * @since 10-08-16
	 */
/*	public static void divArray (double[]a, int n){
		for (int i=0;i<a.length;i++){
			a[i] = a[i]/n;
		}
	}*/

	//!
	/**
	 * divides the values of a matrix by an integer
	 * @param A the matrix
	 * @param n the integer
	 * @since 10-08-16
	 */
/*	public static void divMatrix(Matrix A, int n){
		for (int i=0;i<A.getRowDim();i++){
			for (int j=0;j<A.getRowDim();j++){
				A.set(i,j,A.get(i,j)/n);
			}
		}
	}*/
	
}
