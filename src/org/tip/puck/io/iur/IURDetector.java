package org.tip.puck.io.iur;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckManager;
import org.tip.puck.io.bar.BARTXTFile;
import org.tip.puck.io.ods.ODSBufferedReader;
import org.tip.puck.io.xls.XLSBufferedReader;

/**
 * This class represents a TXT File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class IURDetector {

	private static final Logger logger = LoggerFactory.getLogger(IURDetector.class);

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static boolean assertIsBlankOrChildIds(final String source) {
		boolean result;

		if (StringUtils.isBlank(source)) {
			//
			result = true;

		} else if (source.trim().matches("^\\d+[\\d .,;]*")) {
			//
			result = true;

		} else {
			//
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static boolean assertIsBlankOrID(final String source) {
		boolean result;

		if (StringUtils.isBlank(source) || (NumberUtils.isNumber(source))) {
			//
			result = true;

		} else {
			//
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param block
	 * @return
	 */
	public static boolean assertIsFamilyLine(final String source) {
		boolean result;

		if (StringUtils.isBlank(source)) {
			//
			result = false;

		} else {
			//
			String[] tokens = source.split("\\t");

			//
			if (tokens.length == 0) {
				//
				result = false;

			} else if ((tokens.length > 0) && (!NumberUtils.isNumber(tokens[0]))) {
				//
				result = false;

			} else if ((tokens.length > 1) && (!assertIsUnionStatus(tokens[1]))) {
				//
				result = false;

			} else if ((tokens.length > 2) && (!assertIsBlankOrID(tokens[2]))) {
				//
				result = false;

			} else if ((tokens.length > 3) && (!assertIsBlankOrID(tokens[3]))) {
				//
				result = false;

			} else if ((tokens.length > 4) && (!assertIsBlankOrChildIds(tokens[4]))) {
				//
				result = false;

			} else {
				//
				result = true;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static boolean assertIsGender(final String source) {
		boolean result;

		if (StringUtils.isBlank(source)) {
			//
			result = true;

		} else {
			//
			String value = source.trim();

			//
			if (value.length() != 1) {
				//
				result = false;

			} else {
				//
				char unionKey = value.charAt(0);

				//
				switch (unionKey) {
					case 'm':
					case 'M':
					case 'h':
					case 'H':
					case 'f':
					case 'F':
					case 'X':
					case 'x':
						result = true;
					break;

					default:
						result = false;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param block
	 * @return
	 */
	public static boolean assertIsIndividualLine(final String source, final int labelCount) {
		boolean result;

		if (StringUtils.isBlank(source)) {
			//
			result = false;

		} else {
			//
			String[] tokens = source.split("\\t");

			//
			if (tokens.length == 0) {
				//
				result = false;

			} else if (tokens.length > labelCount) {
				//
				result = false;

			} else if ((tokens.length > 0) && (!NumberUtils.isNumber(tokens[0]))) {
				//
				result = false;

			} else if ((tokens.length > 2) && (!assertIsGender(tokens[2]))) {
				//
				result = false;

			} else {
				//
				result = true;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static boolean assertIsUnionStatus(final String source) {
		boolean result;

		if (StringUtils.isBlank(source)) {
			//
			result = true;

		} else {
			//
			String value = source.trim();

			//
			if (value.length() != 1) {
				//
				result = false;

			} else {
				//
				char unionKey = value.charAt(0);

				//
				switch (unionKey) {
					case 'M':
					case 'U':
					case 'D':
					case 'm':
					case 'u':
					case 'd':
						result = true;
					break;

					default:
						result = false;
				}
			}
		}

		//
		return result;
	}

	/**
	 * This method checks if a stream is a IUR one.
	 * 
	 * @param in
	 *            stream to check.
	 * 
	 * @return true if the stream is IUR one.
	 * @throws Exception
	 */
	public static boolean isIUR(final BufferedReader in) throws Exception {
		boolean result;

		//
		if (in == null) {
			//
			result = false;

		} else {

			//
			IURTXTFile.readCorpusAttributes(new org.tip.puck.net.Attributes(), in);

			//
			IURTXTLabelsLine labelsLine = IURTXTFile.readLabelsLine(in);

			// logger.debug("Individual labels line=" + labelsLine);

			//
			int individualLabelCount;
			if (labelsLine == null) {
				individualLabelCount = 3;
			} else {
				individualLabelCount = labelsLine.size();
			}

			//
			{
				boolean ended = false;
				result = false;
				while (!ended) {
					//
					in.mark(BARTXTFile.MAX_LINE_SIZE);
					String line = in.readLine();

					// logger.debug("source = " + line);

					if (StringUtils.isBlank(line)) {
						//
						ended = true;
						result = true;

					} else if (line.matches("^\\D.*$")) {
						//
						ended = true;
						in.reset();
						result = true;

					} else {
						//
						if (!assertIsIndividualLine(line, individualLabelCount)) {
							//
							ended = true;

							//
							logger.debug("Detected not individual line => NOT IUR");
							logger.debug("line=" + line);
							result = false;
						}
					}
				}
			}

			//
			if (result) {
				//
				IURTXTLabelsLine familyLabels = IURTXTFile.readLabelsLine(in);

				if (familyLabels == null) {
					//
					result = false;

				} else {
					//
					boolean ended = false;
					while (!ended) {
						//
						in.mark(BARTXTFile.MAX_LINE_SIZE);
						String line = in.readLine();

						// logger.debug("source = " + line);

						if (StringUtils.isBlank(line)) {
							//
							ended = true;
							result = true;

						} else if (line.matches("^\\D.*$")) {
							//
							ended = true;
							in.reset();
							result = true;

						} else {
							//
							if (!assertIsFamilyLine(line)) {
								//
								ended = true;

								//
								logger.debug("Detected not family line => NOT IUR");
								logger.debug("line=" + line);
								result = false;
							}
						}
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * This method checks if a stream is a BAR one.
	 * 
	 * @param in
	 *            stream to check.
	 * 
	 * @return true if the stream is BAR one.
	 */
	public static boolean isIUR(final File source) {
		boolean result;

		result = isIUR(source, PuckManager.DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * This method checks if a file is a IUR one.
	 * 
	 * @param file
	 *            file to check.
	 * 
	 * @return true if the file is IUR one.
	 */
	public static boolean isIUR(final File source, final String charsetName) {
		boolean result;
		
		//
		if (source == null) {
			//
			result = false;

		} else if (source.getName().matches("^.*\\.[Ii][Uu][Rr]\\.(ods|ODS|txt|TXT|xls|XLS)$")) {
			//
			result = true;

		} else if (source.getName().matches("^.*\\.[Bb][Aa][Rr]\\.(ods|ODS|txt|TXT|xls|XLS)$")) {
			//
			result = false;

		} else {
			//
			BufferedReader in = null;
			try {
				//
				String fileName = source.getName().toLowerCase();

				//
				if (fileName.endsWith(".ods")) {
					//
					in = new ODSBufferedReader(source);

				} else if (fileName.endsWith(".txt")) {
					//
					in = new BufferedReader(new InputStreamReader(new FileInputStream(source), charsetName));

				} else if (fileName.endsWith(".xls")) {
					//
					in = new XLSBufferedReader(source);

				} else {
					//
					in = null;
				}

				//
				result = isIUR(in);

			} catch (Exception exception) {
				//
				exception.printStackTrace();
				result = false;

			} finally {
				//
				IOUtils.closeQuietly(in);
			}
		}

		//
		return result;
	}
}
