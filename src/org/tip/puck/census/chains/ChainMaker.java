package org.tip.puck.census.chains;

import java.util.ArrayList;
import java.util.List;

import org.opengis.geometry.primitive.Ring;
import org.tip.puck.census.workers.SiblingMode;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.util.Value;

import oldcore.trash.OldRing;
import oldcore.trash.RingGroupMap;

public class ChainMaker {

	
	/**
	 * adjusts the gender of a vertex to that of a spouse vertex
	 * <p> only used for abstract ring composition
	 * @param v the spouse vertex 
	 * @see chains.Ring#compose(Ring)
	 */
	public static void assimilateGender(Individual ego, Individual alter){
		if (ego.getGender()!=alter.getGender()) {
			if (ego.getGender().isUnknown()) {
				ego.setGender(alter.getGender());
			}
			if (alter.getGender().isUnknown()) {
				alter.setGender(ego.getGender());
			}
		}
	}
	
	public static Chain createMarriage (Individual ego, Individual alter){
		 Chain result;
		 
		 result = new Chain(ego);
		 result.add(alter,0);
		 result.subchains = new ArrayList<Chain>();
		 result.subchains.add(new Chain(ego));
		 result.subchains.add(new Chain(ego));
		 result.subchains.add(new Chain(alter));
		 result.subchains.add(new Chain(alter));
		 
		 //
		 return result;
	}
	
	/**
	 * combines a chain with another chain
	 * @param chain2 the ring to be combined with the current ring
	 * @return the ring resulting from the ring combination
	 */
/*	public static Chain combine (Chain chain1, Chain chain2) {
		Chain result;
		
		result = compose(chain1, chain2.reflect()).zip();
		
		if (result.size()<2) return null;
		if (result.gender(0).isFemale()) result = result.reflect();
		return result;
	}*/

	/**
	 * composes a new ring from the current ring and another ring after having identified same-sex vertices 
	 * @param rightChain the ring to be composed with the current ring
	 * @return the ring resulting from the composition	 * 
	 */
	public static Chain compose (Chain chain1, Chain chain2){
		Chain result;
		
		Chain leftChain = chain1.clone();
		Chain rightChain = chain2.clone();
		
			assimilateGender(leftChain.getLast(),rightChain.getFirst());
			if (leftChain.getLast().getGender()!=rightChain.getFirst().getGender()) {
				result = ChainMaker.concatenateWithMarriage(leftChain,rightChain);
			} else  if (ChainMaker.continues(leftChain,rightChain)) {
				rightChain.remove(0);
//				chain2.truncate(false);
				if (leftChain.dim()==0){
					result = ChainMaker.concatenateWithMarriage(leftChain,rightChain);
				} else {
					result = ChainMaker.concatenate(leftChain,rightChain);
				}
			} else {
				leftChain.removeLast();
				rightChain.truncate(true);
				result = compose(leftChain,rightChain);
			}
		
		//
		return result;
	}
	


	//harmonize with concatenate (List,int)
	/**
	 * constructs a chain by concatenation of two chains, joint by a marraige link
	 * @param chain1 the first chain
	 * @param chain2 the second chain
	 * @see OldRing#compose(OldRing)
	 */
	public static Chain concatenateWithMarriage (Chain chain1, Chain chain2){
		Chain result = chain1.clone();
		
		if (result.subchains!=null){
			result.subchains.addAll(chain2.subchains);
		}
		
		result.addAllWithMarriage(chain2);
		result.setPivots(chain1);
		
		return result;
	}
	
	/**
	 * composes a new ring from the current ring and another ring
	 * @param chain2 the ring to be composed with the current ring
	 * @return the ring resulting from the composition
	 * @see OldRing#expand(int, int, NumberChain, RingGroupMap, int)
	 */
	static Chain concatenate (Chain chain1, Chain chain2){
		Chain result = chain1.clone(); 
	
		// Here no identity control...
/*		for (int i=0;i<chain2.size();i++){
			if (result.contains(chain2.get(i),result.size())) {
				return null;
			}
			result.add(chain2.get(i),chain2.dir(i)); 
		}*/
		
		result.addAll(chain2);
		
		// Attention not correct!
//		result.subchains.addAll(chain2.subchains);
		
		result.setPivots(chain1);
		
		//
		return result;
	}

	

	/**
	 * concatenates several consanguineous chains
	 * @param chains
	 * @param dmax
	 * @return
	 */
	public static Chain concatenate(List<Chain> chains, int dmax){
		
		if (chains.size()==1) {
			return chains.get(0);
		}
		
		Chain chain = new Chain();
//		c.order = chains.size();
		chain.subchains = new ArrayList<Chain>();
		
		for (int i=0;i<chains.size();i++){
			Chain next = chains.get(i);
			if (next.depth()>dmax || chain.hasElementsInCommon(next)) {
				return null;
			}
			chain.addAllWithMarriage(next);
			for (Chain subchain : next.subchains){
				chain.subchains.add(subchain);
			}
		}
		return chain;
	}



	/**
	 * concatenates two linear chains
	 * @param left
	 * @param right
	 * @return
	 */
	public static Chain concatenateInv (Chain left, Chain right){
		Chain result = new Chain();
		
		result.subchains = new ArrayList<Chain>();
		result.subchains.add(left.clone());
		result.subchains.add(right.clone());
		
		result.addAll(left);
		result.addInv(right);
		
//		result.depth = Math.max(left.size()-1, right.size()-1);
//		result.order = 1;
		if (left.size()>1 && right.size()>1) {
			result.addApex(left.length());
		}
		
		//
		return result;
	}

	// attention order modification!
	/**
	 * checks whether a ring continues the current ring
	 * @param chain2 the ring to be checked
	 * @return true if the ring continues the current ring
	 * @see OldRing#compose(OldRing)
	 */
	private static boolean continues (Chain chain1, Chain chain2){
		boolean result;
		
		if (chain1.lastDir()==0) {
			result = false;
		} else if (chain1.length()==0 || chain2.length()==0) {
			result = true;
		} else if (chain1.lastDir()==1) {
			if (chain2.dir(1)==-1) {
				//not modify in this method
//!				chain1.order--; 
			}
			result = true;
		} else if (chain1.lastDir()==chain2.dir(1)) {
			result = true;
		} else {
		   result = false;
		}
		
		//
		return result;
	}

	/**
	 * constructs a path from its characteristic number <p> 
	 * used for constructing rings from vectors and strings 
	 * @param k	the characteristic number of the path
	 * @see	Ring#readVector()
	 */
	public static Chain fromNumber (int k) {
		Chain result = new Chain();
	
		int i = 0;
		int a = 2;   
		int c = 1;
	
		if (k < 1) {
			c = -1;
			k = -k;
		}
		while (k > 0) {
			int b = a-k%a;
			result.add(new Individual(i+1,"",Gender.valueOf((2*b/a)-1)));
			a = 2*a;
			k = k-b;
			i = i+1;
		}
		if (c==-1) result.getLast().setGender(Gender.UNKNOWN);
		
		//
		return result;
	}
	
	

	/**
	 * constructs a ring model from its string notation
	 * @param kinString the ring in string notation
	 */
	public static Chain fromString (String kinString) {
		Chain result;
		
		result = null;
		
		if (kinString!=null && kinString.charAt(0)!='<') {
			
			if (Chain.notation(kinString)==Notation.VECTOR) {
				
				result = ChainMaker.fromVector(getVector(kinString));
				
			} else {
				
				result = new Chain();
				kinString = ChainMaker.standardize(kinString);

				int k = 1;
				int n = 0;
				int d = 0;
//				result.order=0;
				for (int i=0;i<kinString.length();i++) {
					
					try {
						Gender gender = Gender.valueOf(Integer.parseInt(kinString.substring(i,i+1)));
						
						if (gender == Gender.UNKNOWN){
							result.add(new Couple(k),d);
						} else {
							result.add(new Individual(k,"",gender),d);
						}
//						result.directions.add(d);
						k = k+1;
						if (d==0) d=1;
						
					} catch (NumberFormatException nfe) {
						
						n = n+1;
						d = -(n%2);
	/*					if (d==0) {
							result.order++;
							result.setPivot(result.size());
						}*/
					}
				}
//				result.pivots.remove(result.pivots.size()-1);  // cut
				result.getSubchains();
			}
		}
						
		//
		return result;
	}
	
	public static void develop(List<Chain> models, final String str, final SiblingMode sib, final boolean crossSex, final FiliationType line){
		develop(models, transform(str), 0, sib, crossSex, line);
	}
	

	
	/**
	 * FIXME Refined crossSex condition to add
	 * @param models
	 * @param str
	 * @param k
	 */
	public static void develop(List<Chain> models, final String str, final int k, final SiblingMode sib, final boolean crossSex, final FiliationType line) {
		if (isWellFormed(str)) {
			for (int i = k; i < str.length(); i++) {
				if (str.charAt(i) == 'X') {
					develop(models, fill(str, "X", "H", line), i, sib, crossSex, line);
					develop(models, fill(str, "X", "F", line), i, sib, crossSex, line);
					return;
				}
				if (str.charAt(i) == 'Y') {
					if (sib != SiblingMode.ALL) {
						develop(models, fill(str, "Y", "X", line), k, sib, crossSex, line);
					}
					if (sib != SiblingMode.NONE) {
						develop(models, fill(str, "Y", "", line), k, sib, crossSex, line);
					}
					return;
				}
			}
			
			// introduce refined homosexuality definition (inner or outer)
			if (!containsHomosexualMarriages(crossSex, str)) {
				Chain model = ChainMaker.fromString(str);
//				model.setSymmetry(symmetry);
				models.add(model.standard());
			}
		}
	}
		
		/**
		 * replaces chain formula variables
		 * 
		 * @param str
		 *            the Chain formula (in positional notation)
		 * @param x
		 *            the variable to be replaced
		 * @param y
		 *            the letter to replace it
		 * @return the new Chain formula
		 * @see groups.RingGroup#develop(boolean, String, int, int)
		 */
		private static String fill(final String str, final String x, final String y, FiliationType line) {
			if (line == FiliationType.COGNATIC || x.equals("Y")
					|| (line == FiliationType.AGNATIC && y.equals("H") || (line == FiliationType.UTERINE && y.equals("F")))) {
				return str.replaceFirst(x, y);
			}
			int i = str.indexOf(x);
			if (i == 0 || i == str.length() - 1 || str.charAt(i + 1) == '.' || str.charAt(i - 1) == '.') {
				return str.replaceFirst(x, y);
			}
			return null;
		}	



	/**
	 * constructs a chain from its characteristic vector
	 * @param v the characteristic vector of the ring
	 */
	public static Chain fromVector (Vector vector) {
		Chain result = new Chain();
		
		result.vector = vector;
		for (int i=0;i<vector.getNumbers().length;i++){
			if (i%2==0) continue;
			result.addAllWithMarriage(concatenateInv(fromNumber(vector.getNumbers()[i-1]),fromNumber(vector.getNumbers()[i])));
		}
		
		//
		return result;
	}

	/**
	 * sets the characteristic vector according to a string representation
	 * @param str the string representation of the vector
	 * @see OldRing#Ring(String)
	 */
	private static Vector getVector (String str) {
		Vector result;
		
		int[] numbers;
		
		String[] vec = str.split("\\ "); 
		numbers = new int[vec.length];
		for (int i=0;i<vec.length;i++){
			numbers[i]=Integer.parseInt(vec[i]);
		}
		
		result = new Vector(numbers);
		
		//
		return result;
	}

	
	public static int getOrder(Value key){
		int result;
		
		result = 0;
		
		if (key.isVector()){
			
			result = key.vectorValue().order();
			
		} else if (key.isString()) {
			
			result = ChainMaker.fromString(key.stringValue()).getOrder();
//			result = ChainMaker.getVector(key.stringValue()).order();
		}
		
		//
		return result;
	}
	
	/**
	 * inserts a ring into this ring at ego's parent position and returns the resulting ring<p>
	 * ego of ring r is parent of ego of this ring
	 * @param innerChain the ring to be inserted
	 * @return the resulting ring
	 */
	public static Chain insert (Chain outerChain, Chain innerChain) { // Line 2: Marriage of ego's father (H) or mother (F))
		Chain result;
		
		if (outerChain.length()==0 || outerChain.dir(1)!=1) {
			result = outerChain;
		} else {
//			if (outerChain.gender(1).isMale()) {
//				innerChain = innerChain.reflect();
//			}
			if (outerChain.get(1).getGender()!=innerChain.getLast().getGender()){
				innerChain = innerChain.reflect();
			}
			innerChain.setDir(0,1);
			Chain chain = concatenate(new Chain(outerChain.getFirst(),1),innerChain);
			outerChain.truncate(true);
			result = compose(chain,outerChain);
		}
		//
		return result;
	}

	/**
	 * translates ring characters into gender numbers and stops
	 * @param c the character in positional or standard notation
	 * @param key the key indicating type of the ring (1 positional, 2 standard)
	 * @return a string consisting of the gender number and eventually a marriage stop
	 * @see OldRing#standardize(String)
	 */
	private static String standardize (char c, Notation key) {
		switch (key){
		case POSITIONAL:
	         if (c=='H') return "0";
	         if (c=='F') return "1";
	         if (c=='X') return "2";
	         if ((c==')') || (c=='.')) return ".";
	         break;
		case CLASSIC:
	         if ((c=='F') || (c=='S')) return "0";
	         if ((c=='M') || (c=='D')) return "1";
	         if (c=='X') return "2";
	         if (c=='H') return ".0";
	         if (c=='W') return ".1";
	         if (c=='.') return ".";
	         break;
		case CLASSIC_GENDERED:
	         if ((c=='F') || (c=='S') || (c==Gender.MALE.toSymbol())) return "0";
	         if ((c=='M') || (c=='D') || (c==Gender.FEMALE.toSymbol())) return "1";
	         if (c=='X' || (c==Gender.UNKNOWN.toSymbol())) return "2";
	         if (c=='H') return ".0";
	         if (c=='W') return ".1";
	         if (c=='.') return ".";
	         break;
		}
		return "";
	}

	/**
	 * translates standard or numeric ring notation into basic notation
	 * @param str the ring in original notation
	 * @return the ring in basic notation
	 * @see OldRing#Ring(String)
	 */
	private static String standardize (String str) {
		String result;
		
	    result = "";
		Notation notation = Chain.notation(str);
	
	      if (notation == Notation.CLASSIC_GENDERED) {
    		  if (str.charAt(1)==' '){
    			  if (str.charAt(0)=='H'){
    				  result = result + 0;
    			  } else if (str.charAt(0)=='F'){
    				  result = result + 1; 
    			  } else {
    				  result = result + 2;
    			  }
	    		  str = str.substring(2);
    		  } else {
	    		  result = standardize(str.charAt(0),Notation.CLASSIC_GENDERED);
	    		  str = str.substring(1);
    		  }
	         str = str.replaceAll("MH","M.H").replaceAll("FW","F.W").replace("MW","M.W").replace("FH","F.H");
	         str = str.replaceAll("M ","H ").replaceAll("F ","W ");
	         str = str.replaceAll("B","X.S").replaceAll("Z","X.D");
	         char c = str.charAt(str.length()-1);
	         if (c!='D' && c!='S') str = str+".";
	      } 
	
	      for (int i=0;i<str.length();i++) {
	         char c = str.charAt(i);
	         if (notation == Notation.POSITIONAL) {
	            if (c==')' && str.charAt(i-1)=='(') { 
	            	result = result + "2";
	            }
	            if (c=='.' && (i==1 || str.charAt(i-2) =='.')) { 
	            	result = result + ".";
	            }
	         } else if (notation == Notation.CLASSIC_GENDERED) {
	        	char d = ' ';
	        	if (i>0){
	        		d = str.charAt(i-1);
	        	}
	            if ((c=='H' || c=='W') && (i==0 || d=='H' || d=='W')) { 
	            	result = result + ".";
	            } 
	            if ((c=='S' || c=='D') && (i==0 || d!='S' && d!='D' && d!='.')) { 
	            	result = result + ".";
	            }
	         }
	         result = result+standardize(c,notation);
//	         System.out.println(i+" "+str+" "+c+" "+standardize(c,notation)+" "+result);
	      }
	      
	      if ((notation == Notation.POSITIONAL) && (str.charAt(str.length()-2))=='.') {
	    	  result = result+"..";      
//	      } else if (notation == Notation.CLASSIC_GENDERED) {
//	    	  result = result+".";      
	      }
	      result = result+".";
	      
//	      System.out.println(result);
	      return result;
	   }

	/**
	 * checks whether a ring schema contains homosexual marriages
	 * 
	 * @param crossSex
	 *            true if ego and alter have to be of different sex
	 * @param str
	 *            the ring schema
	 * @return true if the ring schema contains homosexual marriages
	 */
	public static boolean containsHomosexualMarriages(boolean crossSex, final String str) {
		String s = str.replaceAll("\\(", "").replaceAll("\\)", "");
		if (s.indexOf("H.H") > -1 || s.indexOf("F.F") > -1) {
			return true; // no homosexual "inner" marriages - parametrize!
		}
		if (crossSex && str.charAt(0) == str.charAt(str.length() - 1)) {
			return true; // no homosexual "outer" relations
		}
		return false;
	}

	/**
	 * checks whether a VertexChain formula (in positional notation) is not well
	 * formed
	 * 
	 * @param str
	 *            the formula
	 * @return true if it is not well formed
	 * @see groups.RingGroup#develop(boolean, String, int, int)
	 */
	public static boolean isWellFormed(final String str) {
		boolean result;
		
		if (str == null) {
			result = false;
		} else {
			int i = str.indexOf("()");
			if (i == 0 || i == str.length() - 2) {
				result = false;
			} else if (str.indexOf(".()") > -1 || str.indexOf("().") > -1) {
				result = false;
			} else {
				result = true;
			}
		}
		//
		return result;
	}
	
	public static List<Chain> getPermutations(Chain chain){
		List<Chain> result;
		
		result = new ArrayList<Chain>();
		List<String> signatures = new ArrayList<String>();
		
		for (int i=0;i<2*chain.dim();i++){
			Chain newChain = chain.transform(i);
			String signature = newChain.signature(Notation.POSITIONAL);
			if (!signatures.contains(signature)){
				result.add(newChain);
				signatures.add(signature);
			}
		}
		//
		return result;
	}
	
	/**
	 * replaces the ith character of a String by another character
	 * 
	 * @param source
	 *            the string
	 * @param position
	 *            the position of the character
	 * @param character
	 *            the replacing character
	 * @return the string with the character replaced
	 * @see groups.RingGroup#transform(String)
	 */
	private static String replaceCharAt(final String source, final int position, final char character) {
		String result;
		
		result = source.substring(0, position) + character + source.substring(position + 1);
		
		//
		return result;
	}	
	

	/**
	 * transforms the Chain formula
	 * <p>
	 * an intermediary step in the process of Chain formula development
	 * 
	 * @param chain
	 *            the formula to be transformed
	 * @return the transformed formula
	 * @see groups.RingGroup#RingGroup(boolean, String, int, int, int)
	 */
	public static String transform(String chain) {
		String result;
		
		result = chain;
		
		result = result.replaceAll("\\(\\)", "\\(X\\)");
		
		int n = result.length() - 3;
		
		for (int i = 1; i < n; i++) {
			
			if (result.charAt(i) == '(' && result.charAt(i - 1) != '.' && result.charAt(i - 1) != 'H' && result.charAt(i - 1) != 'F') {
				
				result = replaceCharAt(result, i + 1, 'Y');
			}
		}
		//
		return result;
	}

}
