package org.tip.puckgui.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.report.ReportRawData;
import org.tip.puck.util.ToolBox;
import org.tip.puckgui.util.GenericFileFilter;

/**
 * 
 * @author TIP
 */
public class SaveReportRawDataButton extends JButton {
	private static final long serialVersionUID = -944539061378508158L;

	/**
	 * 
	 * @param title
	 */
	public SaveReportRawDataButton(final JFrame window, final ReportRawData rawData) {
		super();
		setText(String.format("%s (%d bytes)", rawData.getTitle(), rawData.getLength()));

		this.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Save As.
				try {
					File targetFile = rawData.getDefaultFile();
					boolean ended = false;
					while (!ended) {
						JFileChooser chooser = new JFileChooser();
						chooser.setSelectedFile(targetFile);

						chooser.setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.saveAsFileChooser.text"));
						chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
						chooser.setAcceptAllFileFilterUsed(false);
						GenericFileFilter defaultFileFilter = new GenericFileFilter(String.format("%s (*.%s)", rawData.getFormat(), rawData.getExtension()),
								rawData.getExtension());
						chooser.addChoosableFileFilter(defaultFileFilter);
						chooser.setFileFilter(defaultFileFilter);

						//
						if (chooser.showSaveDialog(window) == JFileChooser.APPROVE_OPTION) {
							System.out.println("getCurrentDirectory(): " + chooser.getCurrentDirectory());
							System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
							targetFile = chooser.getSelectedFile();

							boolean doSave;
							if (targetFile.exists()) {
								// Manage confirmation dialog.
								String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.title");
								String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.text");

								int response = JOptionPane.showConfirmDialog(window, message, title, JOptionPane.YES_NO_CANCEL_OPTION);

								if (response == JOptionPane.YES_OPTION) {
									doSave = true;
									ended = true;
								} else if (response == JOptionPane.NO_OPTION) {
									doSave = false;
									ended = false;
								} else {
									doSave = false;
									ended = true;
								}
							} else {
								doSave = true;
								ended = true;
							}

							if (doSave) {
								ToolBox.save(targetFile, rawData.getData());
							}

						} else {
							// Nothing to do.
							System.out.println("No Selection ");
							ended = true;
						}
					}
				} catch (final PuckException exception) {
					//
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.title");

					//
					String message;
					switch (PuckExceptions.valueOf(exception.getCode())) {
						case UNSUPPORTED_FILE_FORMAT:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedFileFormat");
						break;
						case NOT_A_FILE:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.notAFile");
						break;
						case UNSUPPORTED_ENCODING:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.unsupportedEncoding");
						break;
						default:
							message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileError.default");
					}

					//
					JOptionPane.showMessageDialog(window, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}
}
