package org.tip.puck.visualization;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.gephi.data.attributes.api.AttributeController;
import org.gephi.data.attributes.api.AttributeModel;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphFactory;
import org.gephi.graph.api.GraphModel;
import org.gephi.project.api.Workspace;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.Link;
import org.tip.puck.graphs.Node;
import org.tip.puck.visualization.style.attributes.AttributeMap;
import org.tip.puck.visualization.style.attributes.GraphStyle;

/**
 * Graph Importer class.
 * Imports a Graph<E> object into Gephi.
 *
 * @date Jun 1, 2012
 * @author Edoardo Savoia
 */
public class GraphImporter {

    private final GraphModel graphModel;
    private final AttributeModel attributeModel;
    private final GraphStyle graphStyle;
    private final GraphFactory factory;
    private final org.gephi.graph.api.Graph graph;
    private Map<Integer,org.gephi.graph.api.Node> nodesMap;

    public GraphImporter(final Workspace workspace, final GraphStyle graphStyle) {
	graphModel = Lookup.getDefault().lookup(GraphController.class).getModel(workspace);
	AttributeController attributeController = Lookup.getDefault().lookup(AttributeController.class);
	attributeModel = attributeController.getModel(workspace);
	// end with workspace

	this.graphStyle = graphStyle;

	switch (this.graphStyle) {
	    case directed:
		graph = graphModel.getDirectedGraph();
		break;
	    case undirected:
		graph = graphModel.getUndirectedGraph();
		break;
	    case mixed:
		graph = graphModel.getMixedGraph();
		break;
	    default:
		graph = null;
		break;
	}
	factory = graphModel.factory();
    }

    /*
     * Main method
     * Imports a Graph<E> object into Gephi given an AttibuteMap
     */
    public <E> void importGraph(Graph<E> graph, final AttributeMap attributeMap) throws Exception {
	attributeMap.registerAttributes(attributeModel);

	nodesMap = new HashMap<Integer, org.gephi.graph.api.Node>(graph.nodeCount());

	for (Iterator<Node<E>> it = graph.getNodes().iterator(); it.hasNext();) {
	    Node<E> node = it.next();
	    int nodeId = node.getId();
	    org.gephi.graph.api.Node newNode = addNode(nodeId);
	    // use attribute mapping to set node's attributes
	    attributeMap.setAttributes(newNode, node);
	}
	for (Node<E> node : graph.getNodes()) {

	    if (graphStyle == GraphStyle.directed || graphStyle == GraphStyle.mixed) {
		// graph.addArc(father, child, 1);
		List<Link<E>> arcs = node.getOutArcs().getLinks();
		for (Link<E> arc : arcs) {
		    int uidSource = arc.getSourceNode().getId();
		    int uidTarget = arc.getTargetNode().getId();
		    Edge gephiEdge = null;
		    try {
			gephiEdge = addEdge(uidSource, uidTarget, true);
		    } catch (Exception exception) {
			Exceptions.printStackTrace(exception);
		    }
		    attributeMap.setAttributes(gephiEdge, arc);
		}
	    }

	    if (graphStyle == GraphStyle.undirected || graphStyle == GraphStyle.mixed) {
		// graph.addEdge(father, mother, 1);
		List<Link<E>> edges = node.getInferiorEdges().getLinks();
		for (Link<E> edge : edges) {
		    int uidSource = edge.getSourceNode().getId();
		    int uidTarget = edge.getTargetNode().getId();
		    
		    Edge gephiEdge = null;
		    try {
			gephiEdge = addEdge(uidSource, uidTarget, false);
		    } catch (Exception exception) {
			System.out.println(exception.getMessage());
		    }
		    attributeMap.setAttributes(gephiEdge, edge);
		}
	    }
	}
	nodesMap.clear();
	nodesMap = null;
    }

    /*
     * Create a new Gephi node
     */
    public final org.gephi.graph.api.Node addNode(int uid) throws Exception {
	// String uidString = String.valueOf(uid);

	org.gephi.graph.api.Node newNode = factory.newNode();

	if (newNode == null) {
	    throw new Exception("newNode == null");
	}
	if (!graph.addNode(newNode)) {
	    throw new Exception("newNode not added");
	}
	org.gephi.graph.api.Node old = nodesMap.put(uid, newNode);
	if (old != null) {
	    System.out.println("Possible duplicate node (nodesMap contains "+uid+")");
	}
	return newNode;
    }

    /*
     * Create a new Gephi edge
     */
    public final Edge addEdge(int uidSource, int uidTarget, boolean directed) throws Exception {
	// String sourceString = String.valueOf(uidSource);
	// String targetString = String.valueOf(uidTarget);

	// org.gephi.graph.api.Node source = graph.getNode(sourceString);
	org.gephi.graph.api.Node source = nodesMap.get(uidSource);
	if (source == null) {
	    throw new Exception("source == null");
	}
	// org.gephi.graph.api.Node target = graph.getNode(targetString);
	org.gephi.graph.api.Node target = nodesMap.get(uidTarget);
	if (target == null) {
	    throw new Exception("target == null");
	}

	Edge newEdge = factory.newEdge(source, target, 1f, directed);
	if (newEdge == null) {
	    throw new Exception("newEdge == null");
	}

	if (!graph.contains(newEdge)) {
	    graph.addEdge(newEdge);
	    
	    if (!graph.contains(newEdge)){
		throw new Exception("newEdge not added");
	    }   
	}
	
	return newEdge;
    }
}
