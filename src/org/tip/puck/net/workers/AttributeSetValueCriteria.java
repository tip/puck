package org.tip.puck.net.workers;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author TIP
 */
public class AttributeSetValueCriteria {

	private AttributeWorker.Scope scope;
	private String optionalRelationName;
	private String label;
	private String targetValue;

	/**
	 * 
	 */
	public AttributeSetValueCriteria() {
		this.scope = AttributeWorker.Scope.NONE;
		this.optionalRelationName = null;
		this.label = null;
		this.targetValue = null;
	}

	public String getLabel() {
		return this.label;
	}

	public String getOptionalRelationName() {
		return this.optionalRelationName;
	}

	public AttributeWorker.Scope getScope() {
		return this.scope;
	}

	public String getTargetValue() {
		return this.targetValue;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public void setOptionalRelationName(final String optionalRelationName) {
		this.optionalRelationName = optionalRelationName;
	}

	public void setScope(final AttributeWorker.Scope scope) {
		this.scope = scope;
	}

	public void setTargetValue(final String value) {
		this.targetValue = value;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public static boolean isNotValid(final AttributeSetValueCriteria criteria) {
		boolean result;

		result = !isValid(criteria);

		//
		return result;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public static boolean isValid(final AttributeSetValueCriteria criteria) {
		boolean result;

		if ((criteria == null) || (criteria.getScope() == null) || (criteria.getScope() == AttributeWorker.Scope.NONE)
				|| (StringUtils.isBlank(criteria.getLabel()))) {
			//
			result = false;

		} else {
			//
			result = true;
		}

		//
		return result;
	}
}
