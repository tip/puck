package org.tip.puck.visualization.layouts.hierarchical.datastructs.partition;

import java.util.LinkedList;
import java.util.List;

/**
 * Implementation of disjoint-set data structure, providing merge(merge)/find
 * operations, in order to be able to track whether two elements belong in the
 * same set or not, and to efficiently merge two sets.
 *
 * @date Jul 11, 2012
 * @author Edoardo Savoia
 */
public class PartitionItem<E> {
    private PartitionItem<E> parent;
    private PartitionItem<E> child;
    private int rank;
    private final E value;

    /**
     * Creates a partition of a singleton set, containing only itself. The
     * created partition is permanently associated with the specified value,
     * which can be accessed by {@linkplain #value()}.
     *
     * @param <E>   the type of the user-defined value
     * @param value the value to associate with the created partition
     * @return the created partition
     */
    protected static <E> PartitionItem<E> singleton(E value) {
	return new PartitionItem<E>(value);
    }

    private PartitionItem(E value) {
	this.value = value;
	this.parent = this;
	this.child = null;
	this.rank = 0;
    }

    /**
     * Returns the value associated with this partition upon creation.
     *
     * @return the value associated with this partition upon creation
     */
    public E value() {
	return value;
    }

    /**
     * Merges this partition with another one. This has the following
     * implications: <ul> <li>{@code this.find() == other.find()} will be
     * true, forever <li>{@code Partition.areMerged(this, other)} will
     * return true, forever </ul>
     *
     * @param other the partition to merge this partition with
     * @return a partition representing the merged partitions. It will be
     *                either equal to either
     *      {@code this.find()} or {@code other.find()}, i.e. one representative
     * of the partitions which will be elected to represent the union
     */
    protected PartitionItem<E> merge(PartitionItem<E> other) {
	PartitionItem<E> root1 = other.find();
	PartitionItem<E> root2 = find();
	// assume:
	// root1 != root2
	if (root1.rank < root2.rank) {
	    root1.parent = root2;
	    getLastChild(root2).child = root1;
	    return root2;
	}
	else if (root1.rank > root2.rank) {
	    root2.parent = root1;
	    getLastChild(root1).child = root2;
	    return root1;
	}
	else {
	    root2.parent = root1;
	    getLastChild(root1).child = root2;
	    root1.rank++;
	    return root1;
	}
    }
    
    protected PartitionItem<E> append(PartitionItem<E> other) {
	PartitionItem<E> otherRoot = other.find();
	otherRoot.parent = this;
	getLastChild(this).child = otherRoot;
	return this;
    }

    protected List<E> values() {
	List<E> result = new LinkedList<E>();
	result.add(value);
	PartitionItem<E> tmp = this.child;
	while (tmp != null) {
	    /*
	    int parentIndex = result.lastIndexOf(tmp.parent);
	    if (parentIndex != -1) {
		result.add(parentIndex+1,tmp.value());
	    } else {
		result.add(tmp.value());
	    }
	    */
	    result.add(tmp.value());
	    System.out.println("parent has been already added? "+result.contains(tmp.parent.value));
	    tmp = tmp.child;
	}
	return result;
    }

    protected PartitionItem<E> getParent() {
	return parent;
    }

    protected PartitionItem<E> getChild() {
	return child;
    }

    protected int getRank() {
	return rank;
    }

    public boolean isSingleton(){
	return this.equals(parent) && child == null && rank == 0;
    }

    /**
     * Returns the unique representative of the set that this element
     * belongs to. The returned instance can be compared with the
     * representative of another element, to check whether the two elements
     * belong to the same set (when the two representatives will be
     * identical)
     *
     * @return the unique representative of the set that this partition
     *                belongs to
     */
    public PartitionItem<E> find() {
	PartitionItem<E> current = this;
	while (current.parent != current) {
	    current = current.parent;
	}
	return current;
    }

    private static <E> PartitionItem<E> getLastChild(PartitionItem<E> partition) {
	PartitionItem<E> tmpPartition = partition;
	while (tmpPartition.child != null) {
	    tmpPartition = tmpPartition.child;
	}
	return tmpPartition;
    }

    public static <E> List<E> toValues(PartitionItem<E> partition) {
	PartitionItem<E> rep = partition.find();
	PartitionItem<E> tmp = rep.child;
	List<E> retSet = new LinkedList<E>();
	retSet.add(rep.value);
	while (tmp != null) {
	    retSet.add(tmp.value());
	    tmp = tmp.child;
	}
	return retSet;
    }

}
