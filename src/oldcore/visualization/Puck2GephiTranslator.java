package oldcore.visualization;


import org.gephi.graph.api.DirectedGraph;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.Node;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;


public class Puck2GephiTranslator {
	
	Puck2GephiTranslator(){}
	
	/*
	 * First step in plotting a geneological corpus : translating Net into Graph.
	 * @param net - Network representation in Puck.
	 * @param grphMod - see in Gephi.
	 * @param diGrph - see in Gephi.
	 * @return - no return, it's the diGraph that is modified. 
	 * */
	public void translate(Net net, GraphModel grphMod){
		
		DirectedGraph diGrph = grphMod.getDirectedGraph();
		
        //Create nodes
        //Append as a Directed Graph
		for(Individual v : net.individuals()){
			Node n = grphMod.factory().newNode(new Integer(v.getId()).toString());
			n.getNodeData().setLabel(v.getName());
			if(v.isMale()) n.getNodeData().getAttributes().setValue("gender", "male");
			else n.getNodeData().getAttributes().setValue("gender", "female");
			diGrph.addNode(n);
		}
		//Create edges
		for(Individual v : net.individuals()){
			Node ego = diGrph.getNode(new Integer(v.getId()).toString());		
			//to children
			Individuals ch = v.children();
			if(ch!=null && ch.size()>0){
				for(Individual vv : ch){
					Node child = diGrph.getNode(new Integer(vv.getId()).toString());
					Edge e = grphMod.factory().newEdge(ego, child);
					e.getEdgeData().getAttributes().setValue("tie type", "parental");
					diGrph.addEdge(e);
				}
			}
			//to spouses
			Individuals sp = v.spouses();
			if(sp!=null && sp.size()>0){
				for(Individual vv : sp){
					Node spouse = diGrph.getNode(new Integer(vv.getId()).toString());
					Edge e = grphMod.factory().newEdge(ego, spouse);
					e.getEdgeData().getAttributes().setValue("tie type", "conjugal");
					diGrph.addEdge(e);
				}
			}			
		}
	}

}
