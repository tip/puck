package org.tip.puck.io.tip;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.io.tip.TIPLine.Relation;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.net.workers.NetUtils;

/**
 * This class represents a TXT File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class TIPFile {

	private static final Logger logger = LoggerFactory.getLogger(TIPFile.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	/**
	 * Loads a TIP file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file) throws PuckException {
		Net result;

		result = load(file, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Loads a TIP file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file, final String charsetName) throws PuckException {
		Net result;

		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), charsetName));
			result = read(in);

			result.setLabel(file.getName());

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException exception) {
				logger.warn("Not managed error.");
				exception.printStackTrace();
			}
		}

		//
		return result;
	}

	/**
	 * Reads a net from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static Net read(final BufferedReader in) throws PuckException {
		Net result;

		result = new Net();

		// Read.
		logger.debug("Read first block.");
		boolean ended = false;
		while (!ended) {

			//
			TIPLine source = readTIPLine(in);
			// logger.debug("source = " + LogHelper.toString(source));

			if (source == null) {
				ended = true;
			} else {

				switch (source.lineType()) {
					case NET_INFO:
						result.attributes().put(source.propertyLabel(), source.propertyValue());
					break;

					case IDENTITY: {
						Individual individual = result.get(source.individualId());
						if (individual == null) {
							individual = new Individual(source.individualId());
							result.individuals().put(individual);
						}
						individual.setGender(source.gender());
						individual.setName(source.name());
					}
					break;

					case KINSHIP: {
						switch (source.relation()) {
							case FATHER:
								NetUtils.setFatherRelation(result, source.alterId(), source.individualId());
							break;

							case MOTHER:
								NetUtils.setMotherRelation(result, source.alterId(), source.individualId());
							break;

							case SPOUSE:
								NetUtils.setSpouseRelationAndFixRoles(result, source.individualId(), source.alterId());
							break;
						}

					}
					break;

					case PROPERTY:
						NetUtils.setAttribute(result, source.individualId(), source.propertyLabel(), source.propertyValue());
						NetUtils.setAttribute(result, source.individualId(), source.propertyLabel() + "_PLACE", source.propertyPlace());
						NetUtils.setAttribute(result, source.individualId(), source.propertyLabel() + "_DATE", source.propertyDate());
						if (source.alterId() != 0) {
							NetUtils.setAttribute(result, source.individualId(), source.propertyLabel() + "_ALTERID", String.valueOf(source.alterId()));
						}
					break;
				}
			}
		}

		//
		result = NetUtils.buildCleanedNet(result);

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * Reads a line of attributes from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of attributes or null if no more.
	 * 
	 * @throws PuckException
	 */
	public static TIPLine readTIPLine(final BufferedReader in) throws PuckException {
		TIPLine result;

		try {
			//
			String line = in.readLine();
			// logger.debug("line=[" + line + "]");

			if (StringUtils.isBlank(line)) {
				result = null;
			} else {
				String[] tokens = line.split("\\t");

				if (tokens[0].startsWith("*")) {
					result = new TIPLine(tokens[0].substring(1, tokens[0].indexOf(' ')), tokens[0].substring(tokens[0].indexOf(' ')));
				} else if (tokens[0].equals("0")) {
					result = new TIPLine(Integer.parseInt(tokens[1]), Gender.valueOf(tokens[2].charAt(0)), tokens[3]);
				} else if (tokens[0].equals("1")) {
					result = new TIPLine(Integer.parseInt(tokens[1]), Integer.parseInt(tokens[2]), TIPLine.Relation.values()[Integer.parseInt(tokens[3])]);
				} else if (tokens[0].equals("2")) {
					result = new TIPLine(Integer.parseInt(tokens[1]), tokens[2], tokens[3], tokens[4], tokens[5], Integer.parseInt(tokens[6]));
				} else {
					logger.warn("Ignoring unknown line type [" + tokens[0] + "]");
					result = readTIPLine(in);
				}
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading line.");
		}

		//
		return result;
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File file, final Net source) throws PuckException {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));

			write(out, source);

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param net
	 *            Source.
	 */
	public static void write(final PrintWriter out, final Net net) {

		// Write first block labels.
		logger.debug("Write first block labels.");
		for (Attribute attribute : net.attributes().toList()) {
			write(out, new TIPLine(attribute.getLabel(), attribute.getValue()));
		}

		// Write first block data.
		for (Individual individual : net.individuals().toSortedList()) {
			write(out, new TIPLine(individual.getId(), individual.getGender(), individual.getName()));
			if (individual.getFather() != null) {
				write(out, new TIPLine(individual.getId(), individual.getFather().getId(), Relation.FATHER));
			}
			if (individual.getMother() != null) {
				write(out, new TIPLine(individual.getId(), individual.getMother().getId(), Relation.MOTHER));
			}

			for (Individual spouse : individual.getPartners()) {
				write(out, new TIPLine(individual.getId(), spouse.getId(), Relation.SPOUSE));
			}

			// TODO ATTRIBUTES.
		}
		out.println();

	}

	/**
	 * Write a line of attributes.
	 * 
	 * @param out
	 *            Target.
	 * @param source
	 *            Source.
	 */
	public static void write(final PrintWriter out, final TIPLine source) {
		if (source != null) {
			switch (source.lineType()) {
				case NET_INFO:
					out.println(String.format("*%s %s", source.propertyLabel(), source.propertyValue()));
				break;

				case IDENTITY:
					out.println(String.format("0\t%d\t%d\t%s", source.individualId(), source.gender().toInt(), source.name()));
				break;

				case KINSHIP:
					out.println(String.format("1\t%d\t%d\t%d", source.individualId(), source.alterId(), source.relation().ordinal()));
				break;

				case PROPERTY:
					out.println(String.format("2\t%d\t%s\t%s\t%s\t%s\t%d", source.individualId(), source.propertyLabel(), source.propertyValue(),
							source.propertyDate(), source.propertyPlace(), source.alterId()));
				break;
			}
		}
	}
}
