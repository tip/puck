package org.tip.puck.census.chains;

import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;

public class Couple extends Individual {
	
	Individual first;
	Individual second;
	
	public Couple(){
		super(0,"",Gender.MALE);
	}
	
	public Couple(int id, String label){
		super(id,label,Gender.MALE);
	}
	
	public Couple (Individual first, Individual second){
		super(0,"",Gender.MALE);
		this.first = first;
		this.second = second;
	}
	
	public Couple (int k){
		super(k,"",Gender.UNKNOWN);
	}
	
	@Override
	public boolean equals(final Object obj) {
		boolean result;

		result = obj != null && getId() == ((Individual) obj).getId() && toString().equals(((Individual) obj).toString());

		//
		return result;
	}


	
	/**
	 * 
	 * @return
	 */
	public Individuals individuals() {
		Individuals result = new Individuals();
		
		result.add(first);
		result.add(second);
		
		//
		return result;
	}
	
	public int getFirstId (){
		if (first==null) return 0;
		else return first.getId();
	}
	
	public int getSecondId (){
		if (second==null) return 0;
		else return second.getId();
	}

	public String signature (){
		if (first==null && second==null) return "";
		return getFirstId()+" "+getSecondId();
	}
	
	public String toString(){
		return signature();
	}

}
