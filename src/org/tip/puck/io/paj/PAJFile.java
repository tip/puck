package org.tip.puck.io.paj;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.GraphType;
import org.tip.puck.io.paj.PAJLine.LineType;
import org.tip.puck.io.paj.PAJLine.Shape;
import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.util.LogHelper;
import org.tip.puck.util.Numberable;
import org.tip.puck.util.PuckUtils;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class PAJFile {

	private static final Logger logger = LoggerFactory.getLogger(PAJFile.class);;
	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String convertToMicrosoftEndOfLine(final String source) {
		String result;

		result = source.replaceAll("\n", "\r\n");

		//
		return result;
	}
	
	/**
	 * 
	 * TODO optimize with StrintListWriter.
	 * 
	 * @param source
	 * @param targetFileName
	 * @param graphType
	 * @param partitonLabels
	 * @throws PuckException
	 */
	public static <E> void exportToPajek(final StringList source, final String fileName, final List<String> partitionLabels) throws PuckException {
		PrintWriter out = null;
		try {
			//
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(new File(fileName)), "UTF-8"));

			//
			String pajekString = source.toString();

			//
			pajekString = convertToMicrosoftEndOfLine(pajekString);

			//
			out.print(pajekString);

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + fileName + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + fileName + "]");
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}


	/**
	 * 
	 * TODO optimize with StrintListWriter.
	 * 
	 * @param source
	 * @param targetFileName
	 * @param graphType
	 * @param partitonLabels
	 * @throws PuckException
	 */
	public static <E> void exportToPajek(final Graph<E> source, final String fileName, final List<String> partitionLabels) throws PuckException {
		PrintWriter out = null;
		try {
			//
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(new File(fileName)), "UTF-8"));

			//
			String pajekString = PuckUtils.writePajekNetwork(source, partitionLabels).toString();

			//
			pajekString = convertToMicrosoftEndOfLine(pajekString);

			//
			out.print(pajekString);

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + fileName + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + fileName + "]");
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * TODO mutualize code with exportToPaje(Graph).
	 * 
	 * @param source
	 * @param targetFileName
	 * @param graphType
	 * @param partitonLabels
	 * @throws PuckException
	 */
	public static void exportToPajek(final Net source, final String fileName, final GraphType graphType, final List<String> partitionLabels)
			throws PuckException {
		PrintWriter out = null;
		try {
			//
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(new File(fileName)), "UTF-8"));

			//
			Segmentation segmentation = new Segmentation(source);

			//
			String pajekString;
			switch (graphType) {
				case OreGraph: {
					Graph<Individual> graph = NetUtils.createOreGraph(segmentation);
					pajekString = PuckUtils.writePajekNetwork(graph, partitionLabels).toString();
				}
				break;
				case PGraph: {
					Graph<Family> graph = NetUtils.createPGraph(segmentation);
					pajekString = PuckUtils.writePajekNetwork(graph, partitionLabels).toString();
				}
				break;
				case TipGraph: {
					Graph<Individual> graph = NetUtils.createTipGraph(segmentation);
					pajekString = PuckUtils.writePajekNetwork(graph, partitionLabels).toString();
				}
				break;
				case BinaryRelationGraph: {
					Graph<Individual> graph = NetUtils.createBinaryRelationGraph(segmentation);
					pajekString = PuckUtils.writePajekNetwork(graph, partitionLabels).toString();
				}
				break;
				case BimodalRelationGraph: {
					Graph<Numberable> graph = NetUtils.createBimodalRelationGraph(segmentation);
					partitionLabels.add("Mode");
					partitionLabels.add("Type");
					pajekString = PuckUtils.writePajekNetwork(graph, partitionLabels).toString();
				}
				break;
				default:
					throw PuckExceptions.INVALID_PARAMETER.create("Unknown GraphType: [" + graphType + "]");
			}

			pajekString = convertToMicrosoftEndOfLine(pajekString);

			out.print(pajekString);

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + fileName + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + fileName + "]");
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * Loads a PAJ file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file) throws PuckException {
		Net result;

		result = load(file, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Loads a PAJ file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file, final String charsetName) throws PuckException {
		Net result;

		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), charsetName));
			result = read(in);

			result.setLabel(file.getName());

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException exception) {
				logger.warn("Not managed error.");
				exception.printStackTrace();
			}
		}

		//
		return result;
	}

	/**
	 * Reads a net from a BufferedReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws PuckException
	 */
	public static Net read(final BufferedReader in) throws PuckException {
		Net result;

		result = new Net();

		// Read.
		boolean ended = false;
		Boolean doPermute = null;
		LineType currentHeader = null;
		Integer currentRelationCode = null;
		String currentAttribute = null;
		int currentCardinal = 0;
		int currentDataCount = 0;
		HashMap<Integer, Integer> idTable = new HashMap<Integer, Integer>();
		while (!ended) {
			//
			PAJLine source = readPAJLine(in);
			// logger.debug("source = " + LogHelper.toString(source));

			if (source == null) {
				ended = true;
			} else {
				//
				switch (source.getType()) {
					case NETWORK_HEADER:
						currentHeader = LineType.NETWORK_HEADER;
						currentRelationCode = null;
						result.setLabel(source.getLabel());
					break;

					case PARTITION_HEADER:
						currentHeader = LineType.PARTITION_HEADER;
						currentRelationCode = null;
						currentAttribute = source.getLabel();
					break;

					case VECTOR_HEADER:
						currentHeader = LineType.VECTOR_HEADER;
						currentRelationCode = null;
						currentAttribute = source.getLabel();
					break;

					case VERTICES_HEADER:
						currentHeader = LineType.VERTICES_HEADER;
						currentRelationCode = null;
						currentCardinal = source.getCardinal();
						currentDataCount = 0;
					break;

					case ARCS_HEADER:
						currentHeader = LineType.ARCS_HEADER;
						currentRelationCode = source.getRelationCode();
					break;

					case EDGE_HEADER:
						currentHeader = LineType.EDGE_HEADER;
						currentRelationCode = null;
					break;

					case ARC_EDGE_DATA: {
						if (currentHeader == LineType.ARCS_HEADER) {
							if (currentRelationCode == null) {
								Individual parent = result.individuals().getById(idTable.get(source.getSourceId()));
								if (parent.isFemale()) {
									NetUtils.setMotherRelation(result, idTable.get(source.getSourceId()), idTable.get(source.getTargetId()));
								} else {
									NetUtils.setFatherRelation(result, idTable.get(source.getSourceId()), idTable.get(source.getTargetId()));
								}
							} else {
								switch (currentRelationCode) {
									case 1:
										NetUtils.setSpouseRelationAndFixRoles(result, idTable.get(source.getSourceId()), idTable.get(source.getTargetId()));
									break;

									case 2:
									case 3:
									case 6:
										NetUtils.setMotherRelation(result, idTable.get(source.getSourceId()), idTable.get(source.getTargetId()));
									break;

									case 4:
									case 5:
									case 7:
										NetUtils.setFatherRelation(result, idTable.get(source.getSourceId()), idTable.get(source.getTargetId()));
									break;
									default:
										logger.warn("LINE IGNORED: [" + LogHelper.toString(source) + "]");
								}
							}
						} else if (currentHeader == LineType.EDGE_HEADER) {
							NetUtils.setSpouseRelationAndFixRoles(result, idTable.get(source.getSourceId()), idTable.get(source.getTargetId()));
						} else {
							logger.warn("LINE IGNORED: [" + LogHelper.toString(source) + "]");
						}
					}
					break;

					case VERTICE_LONG_DATA: {
						// Sometimes, some names contain "(1932)" but as not all
						// contains it, we have to ignore it.
						if (doPermute == null) {
							if (source.getTargetId() == null) {
								doPermute = false;
							} else {
								doPermute = true;
							}
						}

						if ((doPermute) && (source.getTargetId() == null)) {
							throw PuckExceptions.BAD_FILE_FORMAT.create("Waiting for numbered names: " + source);
						} else {
							//
							if (!doPermute) {
								source.setTargetId(source.getSourceId());
							}

							//
							Individual individual = result.get(source.getTargetId());
							if (individual == null) {

								idTable.put(source.getSourceId(), source.getTargetId());
								individual = new Individual(source.getTargetId());
								result.individuals().put(individual);
							}

							//
							individual.setName(source.getName());

							//
							switch (source.getShape()) {
								case CIRCLE:
								case ELLIPSE:
									individual.setGender(Gender.FEMALE);
								break;

								case TRIANGLE:
									individual.setGender(Gender.MALE);
								break;

								case DIAMOND:
								case SQUARE:
								case BOX:
								default:
									individual.setGender(Gender.UNKNOWN);
								break;
							}
						}
					}
					break;

					case VERTICE_SHORT_DATA:
						currentDataCount += 1;
						if (currentCardinal == result.size()) {
							result.individuals().getById(idTable.get(currentDataCount)).attributes().put(currentAttribute, source.getValue());
						}
					break;
				}
			}
		}

		//
		result = NetUtils.buildCleanedNet(result);

		logger.debug("Done.");

		//
		return result;
	}

	/**
	 * Reads a line of attributes from a BufferedReader.
	 * 
	 * @param in
	 *            The source of reading.
	 * 
	 * @return a line of attributes or null if no more.
	 * 
	 * @throws PuckException
	 */
	public static PAJLine readPAJLine(final BufferedReader in) throws PuckException {
		PAJLine result;

		try {
			//
			String line = in.readLine();
			// logger.debug("line=[" + line + "]");

			if (line == null) {
				result = null;
			} else if (StringUtils.isBlank(line)) {
				result = PAJLine.createEmptyLine();
			} else if (line.charAt(0) != '*') {
				if (NumberUtils.isNumber(line)) {
					//
					result = PAJLine.createVertice(line);
				} else {
					Matcher verticeMatcher = PAJLine.VERTICE_LONG_DATA_PATTERN.matcher(line);
					if (verticeMatcher.find()) {
						//
						if (verticeMatcher.groupCount() == 7) {
							//
							int sourceId = Integer.parseInt(verticeMatcher.group(1));

							//
							Integer targetId;
							String name;
							Matcher nameMatcher = PAJLine.NAME_PATTERN.matcher(verticeMatcher.group(2));
							if ((nameMatcher.find()) && (nameMatcher.groupCount() == 3)) {
								targetId = Integer.parseInt(nameMatcher.group(3));
								name = nameMatcher.group(1);
							} else {
								targetId = null;
								name = verticeMatcher.group(2);
							}

							//
							Shape shape;
							if (verticeMatcher.group(7) == null) {
								shape = Shape.BOX;
							} else {
								shape = PAJLine.Shape.valueOf(verticeMatcher.group(7).toUpperCase());
							}

							//
							result = PAJLine.createVertice(sourceId, targetId, name, shape);
						} else {
							throw PuckExceptions.BAD_FILE_FORMAT.create("Vertice data bad format: " + line);
						}
					} else {
						Matcher arcMatcher = PAJLine.ARC_EDGE_DATA_PATTERN.matcher(line);
						if ((arcMatcher.find()) && (arcMatcher.groupCount() == 3)) {
							//
							result = PAJLine.createArcEdgeData(Integer.parseInt(arcMatcher.group(1)), Integer.parseInt(arcMatcher.group(2)),
									Integer.parseInt(arcMatcher.group(3)));
						} else {
							throw PuckExceptions.BAD_FILE_FORMAT.create("Bad data format: " + line);
						}
					}
				}
			} else if ((line.startsWith("*Network ")) || (line.startsWith("*network "))) {
				//
				result = PAJLine.createNetworkHeader(line.substring(line.indexOf(' ') + 1));
			} else if ((line.startsWith("*Partition ")) || (line.startsWith("*partition "))) {
				//
				result = PAJLine.createPartitionHeader(line.substring(line.indexOf(' ') + 1));
			} else if ((line.startsWith("*Vector ")) || (line.startsWith("*Vector "))) {
				//
				result = PAJLine.createPartitionHeader(line.substring(line.indexOf(' ') + 1));
			} else if ((line.trim().equals("*Arcs")) || (line.trim().equals("*arcs"))) {
				result = PAJLine.createArcsHeader();
			} else if ((line.startsWith("*Arcs")) || (line.startsWith("*arcs"))) {
				//
				Matcher matcher = PAJLine.ARCS_HEADER_PATTERN.matcher(line);
				if ((matcher.find()) && (matcher.groupCount() == 2)) {
					result = PAJLine.createArcsHeader(Integer.parseInt(matcher.group(1)), matcher.group(2));
				} else {
					//
					throw PuckExceptions.BAD_FILE_FORMAT.create("Arcs header bad format: [" + line + "]");
				}
			} else if ((line.startsWith("*Edges")) || (line.startsWith("*edges"))) {
				//
				result = PAJLine.createEdgeHeader();
			} else if ((line.startsWith("*Vertices ")) || (line.startsWith("*vertices "))) {
				//
				result = PAJLine.createVerticesHeader(Integer.parseInt(line.substring(line.indexOf(' ') + 1)));
			} else {
				throw PuckExceptions.BAD_FILE_FORMAT.create("Unknow line type: " + line);
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading line.");
		}

		//
		return result;
	}
}
