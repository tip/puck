package org.tip.puck.net.relations.roles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.AlterAge;
import org.tip.puck.net.Gender;
import org.tip.puck.net.KinType;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.relations.Roles;
import org.tip.puck.net.relations.roles.RoleActorPair.Adjustable;
import org.tip.puck.util.NumberablesHashMap;

public class RoleRelations extends NumberablesHashMap<RoleRelation> {
	
	boolean egoGenderDistinction;
	Boolean heteroMarriage;
	RoleDefinitions originalDefinitions;
	
	Map<Role,Roles> genderConfigs;
	Map<Role,Integer> selfDistances;
	
	RoleActors selfs;
	RoleActors elementaryAlters;
	String selfName;
	
	private static final Logger logger = LoggerFactory.getLogger(RoleRelations.class);

	
	public RoleRelations(){
		
		genderConfigs = new HashMap<Role,Roles>();
		selfs = new RoleActors();
	}
	
	public RoleRelations clone(){
		RoleRelations result;
		
		result = new RoleRelations();
		result.originalDefinitions = originalDefinitions;
		result.heteroMarriage = heteroMarriage;
		result.selfs = selfs;
		result.selfName = selfName;
		
		// Not sorted, in order to detect eventual effects of storage order on the result...
		for (RoleRelation relation : this){
			result.put(relation.clone());
		}
		result.updateGenderConfigs();
		
		//
		return result;
	}
	
	public RoleActors getAlters (RoleActor self, KinType kinType, Gender alterGender){
		RoleActors result;
		
		result = new RoleActors();
		
		for (RoleActor alter: self.getActorsByRoleNameWithUnspecific(kinType.toString(),this)){

			if (alterGender==null || alterGender.matchs(alter.getAlterGender())) {

				result.add(alter);
			}
		}
		//
		return result;
	}
	
	
	RoleDefinitions getDefinitions(){
		RoleDefinitions result;
		
		result = new RoleDefinitions();
		
		for (RoleRelation relation : this){
			for (RoleActor actor : relation.getActors()){
				if (!actor.isSelf()){
					result.add(new RoleDefinition(relation.getSelfTerm(),actor));
				}
			}
		}
		
		//
		return result;
	}
	
	boolean hasElementaryName(RoleActor actor){
		boolean result;
		
		result = false;
		
		for (RoleActor elementaryAlter : getElementaryAlters()){
			
			if (actor.hasName(elementaryAlter.getName())){
				result = true;
				break;
			}
		}
		//
		return result;
	}
	
	public RoleActors getElementaryAlters(){
		
		if (elementaryAlters == null) {

			elementaryAlters = new RoleActors();
			
			for (Gender egoGender : Gender.values()){
				
				RoleActor self = getSelf(egoGender);
				RoleRelation relation = getSelfRelation(self);
				
				if (relation!=null){
					
					for (RoleActor alter : relation.getActors()){
						
						if (!alter.hasSelfName()){
							
							elementaryAlters.addNew(alter.egoGenderNeutral());
						}
					}
				}
			}		
		}
		//
		return elementaryAlters;
	}
	
	public Roles getGeneratorTerms (RoleActor actor){
		Roles result;
		
		result = new Roles();
		
		for (RoleActor elementaryAlter : getElementaryAlters()){
			
			if (actor.getRole().matches(elementaryAlter.getRole())){
				result.add(elementaryAlter.getIndividual());
			}
		}
		
		//
		return result;
	}
	
	public KinType getElementaryKinType (RoleActor actor){
		KinType result;
		
		result = null;
		
		for (RoleActor elementaryAlter : getElementaryAlters()){
			
			if (actor.hasName(elementaryAlter.getName())){
				result = elementaryAlter.getRole().getKinType();
				break;
			}
		}
		//
		return result;
	}

	
	public Map<Role,Integer> getDistances(){
		
		if (selfDistances==null){
			
			selfDistances = new HashMap<Role,Integer>();
			Queue<RoleActor> queue = new LinkedList<RoleActor>();
			RoleActors visited = new RoleActors();

			for (Gender egoGender : Gender.values()){
				RoleActor self = getSelf(egoGender);
				queue.add(self);
				selfDistances.put(self.getIndividual(), 0);
			}		
			
			while (!queue.isEmpty()){
				RoleActor self = queue.poll();
				visited.add(self);
				int selfDistance = selfDistances.get(self.getIndividual());
				for (KinType kinType : KinType.basicTypes()){
					for (RoleActor actor : self.getActorsByRoleNameWithUnspecific(kinType.toString(), this)){
						if (!actor.isSelf()){
							RoleActor alter = actor.asSelf(this);
							if (selfDistances.get(alter.getIndividual())==null && !visited.contains(alter)){
								queue.add(alter);
								selfDistances.put(alter.getIndividual(), selfDistance+1);
							}
						}
					}
				}
			}
		}
		//
		return selfDistances;
	}

	
	public RoleDefinition getStandardRoleDefinition (RoleActor alpha, RoleActor beta){
		
		return new RoleActorPair(this,alpha,beta,Adjustable.NONE).getStandardRoleDefinition(this);
	}
	
	
	/**
	 * gets the definitions used to fix the gender configurations of the terms, using the medius term closest to self
	 * @return
	 */
	public RoleDefinitions getBasicRoleDefinitions(){
		RoleDefinitions result;
		
		result = new RoleDefinitions();
		
		getDistances();
		Map<Role,MetaRole> uniqueGenderConfigs = RoleRelationMaker.getUniqueGenderConfigs(this);

		// Store definitions necessary for gender and age configuration
		
		for (Role role : getRoles().toSortedList()){
			
			if (!role.hasName(selfName)){
								
				MetaRole uniqueGenderConfig = uniqueGenderConfigs.get(role);
				RoleDefinition basicDefinition = null;
				
				for (Role metaRole : genderConfigs.get(role).toSortedList()){
										
					RoleActor actor = new RoleActor(role,(MetaRole)metaRole,selfName);
					
					Role closestSelfTerm = getAlters(actor, ((MetaRole)metaRole).getKinType().inverse(), null).toListSortedByDistances(this).get(0).getIndividual();
					
					RoleDefinition definition = new RoleDefinition(closestSelfTerm,actor);

					if (uniqueGenderConfig==null){
						
						result.addNew(definition);
						
					} else {
						
						if (definition.compareTo(basicDefinition, this)<0){
							basicDefinition = definition;
						}
					}
				}
				
				if (uniqueGenderConfig!=null){
					
					// Cross gender configuration > two different basic definitions needed
					if (uniqueGenderConfig.isCross()){

						for (RoleActor actor : basicDefinition.getAlter().crossExtension()){
							
							if (!result.hasAlterWithGender(basicDefinition.getAlterTerm(), actor.getAlterGender())){
								result.addNew(new RoleDefinition(basicDefinition.getSelfTerm(),actor));
							}
						}
					
					// Unique gender configuration > only one (most general) basic definition needed
					} else if (!result.hasAlter(basicDefinition.getAlterTerm())){
						if (basicDefinition.getRole().equalsAbsolute(uniqueGenderConfig)){
							result.addNew(basicDefinition);
						}
					}
				}
			}
		}
		
		//
		return result;
	}
	
	
	public boolean heteroMarriage(){
		//
		return heteroMarriage;
	}
	
	public List<RoleRelation> toSortedList(){
		List<RoleRelation> result;
		
		result = toList();
		Collections.sort(result);
		
		//
		return result;
	}
	
	public void updateGenderConfigs(){
		
		// Update gender configurations
		
		for (RoleActor actor : getActors()){
			updateGenderConfigs(actor);
		}
	}

	
	public void updateGenderConfigs(RoleDefinitions definitions){
		
		// Set marriage configuration
		
		setHeteroMarriage(definitions);
		
		// Update alter configurations
		
		for (RoleDefinition definition : definitions.toSortedList()){

			updateGenderConfigs(definition.getAlter());
			if (definition.getSelfTerm().hasName(selfName)){
				updateGenderConfigs(definition.getSelf());
			}
		}
	}
	
	private void setHeteroMarriage (RoleDefinitions definitions){
		
		heteroMarriage = true;
		
		for (RoleDefinition definition : definitions){
			
			if (definition.getSelfTerm().hasName(selfName) && (definition.getAlter().isSpouse() && !definition.getAlter().getRole().isCross())){
						
				heteroMarriage = false;
				break;
			}
		}
	}
	
	public boolean updateGenderConfigs(RoleActor actor){
		boolean result;
		
		if (actor.isSelf() || actor.hasName(selfName)){
			
			result = false;
			
		} else {
			
			result = true;
			boolean addable = true;

			Roles alterGenderConfigs = genderConfigs.get(actor.getIndividual());
			if (alterGenderConfigs == null){
				alterGenderConfigs = new Roles();
				genderConfigs.put(actor.getIndividual(), alterGenderConfigs);
			}
			MetaRole metaRole = actor.getRole().clone();
			
			for (Role otherRole : alterGenderConfigs.toSortedList()){
				
				MetaRole otherMetaRole = (MetaRole)otherRole;
							
				if (metaRole.specifies(otherMetaRole)){
					
					result = false;
					addable = false;
					
					break;
					
				} else if (otherMetaRole.specifies(metaRole)){
					
					alterGenderConfigs.remove(otherMetaRole);
					
				} else if (metaRole.getName().equals(otherMetaRole.getName())){

					if (metaRole.equals(otherMetaRole.crossGenderComplement())){
						
						otherMetaRole.setEgoGender(Gender.UNKNOWN);
						otherMetaRole.setAlterGender(Gender.UNKNOWN);
						otherMetaRole.setCross(true);
						addable = false;
						
					} else if (metaRole.equals(otherMetaRole.egoGenderComplement())){
						
						otherMetaRole.setEgoGender(Gender.UNKNOWN);
						addable = false;
						
					} else if (metaRole.equals(otherMetaRole.alterGenderComplement())){
							
						otherMetaRole.setAlterGender(Gender.UNKNOWN);
						addable = false;
						
					} else if (metaRole.equals(otherMetaRole.alterAgeComplement())){
						
						otherMetaRole.setAlterAge(AlterAge.UNKNOWN);
						addable = false;
						
					} else if (metaRole.getEgoGender()==otherMetaRole.getEgoGender() && metaRole.getAlterGender()==otherMetaRole.getAlterGender() && !metaRole.isCross() && otherMetaRole.isCross()){
						
						otherMetaRole.setCross(false);
						addable = false;
					
					} else if (metaRole.egoGenderComplement()!= null && metaRole.egoGenderComplement().specifies(otherMetaRole)){
						
						metaRole.setEgoGender(Gender.UNKNOWN);
						
					} else if (metaRole.alterGenderComplement()!= null && metaRole.alterGenderComplement().specifies(otherMetaRole)){
						
						metaRole.setAlterGender(Gender.UNKNOWN);
						
					} else if (metaRole.alterAgeComplement()!= null && metaRole.alterAgeComplement().specifies(otherMetaRole)){
						
						metaRole.setAlterAge(AlterAge.UNKNOWN);
					}
					
					if (!addable) {
						
						cleanUpGenderConfigs(otherMetaRole,alterGenderConfigs);
					}
				}
			}
			
			if (addable){
				
				alterGenderConfigs.add(metaRole);
				cleanUpGenderConfigs(metaRole,alterGenderConfigs);
			}
			
			if (result) {
				logger.debug("Updated gender configuration:\t"+actor+":\t"+alterGenderConfigs);
			}
			
		}
		//
		return result;
		
	}
	
	private static boolean cleanUpGenderConfigs(MetaRole role, Roles roles){
		boolean result;
		
		result = false;
		
		
		for (Role otherRole : roles.toSortedList()){
			
			MetaRole otherMetaRole = (MetaRole)otherRole;

			if (otherMetaRole.equals(role) || !otherMetaRole.getName().equals(role.getName())){
				continue;
			} else if (otherMetaRole.specifies(role)){
				roles.remove(otherMetaRole);
				result = true;
			} else if (otherMetaRole.equals(role.crossGenderComplement())){
				roles.remove(otherMetaRole);
				role.setEgoGender(Gender.UNKNOWN);
				role.setAlterGender(Gender.UNKNOWN);
				role.setCross(true);
				result = true;
			} else if (otherMetaRole.equals(role.egoGenderComplement())){
				roles.remove(otherMetaRole);
				role.setEgoGender(Gender.UNKNOWN);
				result = true;
			} else if (otherMetaRole.equals(role.alterGenderComplement())){
				roles.remove(otherMetaRole);
				role.setAlterGender(Gender.UNKNOWN);
				result = true;
			} else if (otherMetaRole.equals(role.alterAgeComplement())){
				roles.remove(otherMetaRole);
				role.setAlterAge(AlterAge.UNKNOWN);
				result = true;
			} else if (otherMetaRole.getEgoGender()==role.getEgoGender() && otherMetaRole.getAlterGender()==role.getAlterGender() && otherMetaRole.isCross() && !role.isCross()){
				roles.remove(otherMetaRole);
				result = true;
			}
		}
		//
		return result;
	}

	public List<RoleRelation> toSortedListReverse(){
		List<RoleRelation> result;
		
		result = toList();
		Collections.sort(result,Collections.reverseOrder());
		
		//
		return result;
	}
	
	public Roles getRoles(){
		Roles result;
		
		result = new Roles();
		
		for (RoleRelation relation: this){
			Role role = relation.getSelfTerm();
			if (!role.hasName(selfName) && !result.contains(role)){
				result.add(role);
			}
		}
		//
		return result;
	}
	

	
/*	public Roles getRolesWithGender(Gender egoGender, Gender alterGender){
		Roles result;
		
		result = new Roles();
		
		for (Role role : getRoles()){
			if (genderConfigs.get(role)!=null){
				for (Role metaRole : genderConfigs.get(role)){
					if (((MetaRole)metaRole).getEgoGender().matchs(egoGender) && ((MetaRole)metaRole).getAlterGender().matchs(alterGender)){
						result.add(role);
						break;
					}
				}
			}
		}
		//
		return result;
	}
	
	public Gender[] getUniqueGenders (Role role){
		Gender[] result;
		
		Gender roleEgoGender = null;
		Gender roleAlterGender = null;
		
		if (genderConfigs.get(role)!=null){
			
			for (Role metaRole : genderConfigs.get(role)){
				
				if (roleEgoGender == null){
					roleEgoGender = ((MetaRole)metaRole).getEgoGender();
				} else if (roleEgoGender!=((MetaRole)metaRole).getEgoGender()){
					roleEgoGender = Gender.UNKNOWN;
					break;
				}
			}

			for (Role metaRole : genderConfigs.get(role)){
				
				if (roleAlterGender == null){
					roleAlterGender = ((MetaRole)metaRole).getAlterGender();
				} else if (roleAlterGender!=((MetaRole)metaRole).getAlterGender()){
					roleAlterGender = Gender.UNKNOWN;
					break;
				}
			}
		}
		
		result = new Gender[]{roleEgoGender,roleAlterGender};
		//
		return result;
	}
	
	public Roles getRolesWithExclusiveGender(Gender egoGender, Gender alterGender){
		Roles result;
		
		result = new Roles();
		
		for (Role role : getRoles()){
			
			Gender[] uniqueGenders = getUniqueGenders(role);
			
			if ((egoGender==null || egoGender==uniqueGenders[0]) && (alterGender==null || alterGender == uniqueGenders[1])){
				result.add(role);
			}
		}
		//
		return result;
	}*/


	public List<RoleActors> adjustChain(RoleActors chain, List<RoleActors> startChains){
		List<RoleActors> result;
		
		result = startChains;
		
		for (RoleActor actor : chain){
			
			List<RoleActors> endChains = new ArrayList<RoleActors>();
				
			for (RoleActors startChain : result){
					
				RoleActor self = startChain.getLast();
				
				for (RoleActor alter : getAlters(self, actor.getRole().getKinType(), actor.getAlterGender())){
						
					if (RoleRelationMaker.concatenable(self,alter)){
						
						RoleActors endChain = startChain.clone();
						endChain.add(alter);
						endChains.add(endChain);
					}
				}
			}
			result = endChains;
		}
		//
		return result;
	}
	
	public RoleRelations getBySelf (RoleActor self){
		RoleRelations result;
		
		result = new RoleRelations();
		
		for (RoleRelation roleRelation : this){
			
			if (roleRelation.getSelf().matchesAbsolute(self)){
				result.add(roleRelation);
			}
		}
		
		//
		return result;
	}
	
	public RoleActors getSortedActors (){
		RoleActors result;
		
		result = new RoleActors();
		
		for (RoleRelation relation : this){
			for (RoleActor actor : relation.getActors()){
				result.addNew(actor);
			}
		}
		
		Collections.sort(result, new RoleActorComparator(this));
		//
		return result;
	}

	
	public RoleRelation getSelfRelation (RoleActor actor){
		RoleRelation result;
		
		result = null;
		
		RoleActor self = actor.asSelf(this);
		
		for (RoleRelation roleRelation : this){
			if (roleRelation.hasActor(self)){
				result = roleRelation;
				break;
			}
		}
		//
		return result;
	}
	
	public RoleActors getActors (){
		RoleActors result;
		
		result = new RoleActors();
		
		for (RoleRelation relation : this){
			for (RoleActor actor : relation.getActors()){
				result.addNew(actor);
			}
		}
		//
		return result;
	}
	
	public void addAndUpdate (final RoleRelation relation){
		
		add(relation);
		selfs.addNew(relation.getSelf());
	}
	
	public List<RoleActor> getSortedSelfs (){

/*		if (selfs==null){

			selfs = new RoleActors();
			
			for (RoleRelation relation : this){
				selfs.addNew(relation.getSelf());
			}
			
			Collections.sort(selfs,new RoleActorComparator(this));
		}*/
		
		//
		return selfs.toListSortedByDistances(this);
	}
	
	
	public Roles roles (){
		Roles result;
		
		result = new Roles();
		
		for (RoleRelation relation : this){
			for (Role role : relation.getRoles()){
				result.addNew(role);
			}
		}
		//
		return result;
	}
	
	public static boolean containsSpecific (Roles roles, MetaRole role){
		boolean result;
		
		result = false;
		
		for (Role otherRole : roles){
			if (!role.equals(otherRole) && ((MetaRole)otherRole).specifies(role)){
				result = true;
				break;
			}
		}
		//
		return result;
	}
	
	
	private static Roles getRoles(KinType kinType, Boolean[] config){
		Roles result;
		
		Roles egoComplements = new Roles();
		if (config[0]){
			for (Gender egoGender : Gender.valuesNotUnknown()){
				egoComplements.add(new MetaRole(kinType.toString(),egoGender,null,null));
			}
		} else {
			egoComplements.add(new MetaRole(kinType.toString(),Gender.UNKNOWN,null,null));
		}
		
		Roles alterComplements = new Roles();
		for (Role role : egoComplements){
			Gender egoGender = ((MetaRole)role).egoGender;
			if (config[1]){
				for (Gender alterGender : Gender.valuesNotUnknown()){
					alterComplements.add(new MetaRole(kinType.toString(),egoGender,alterGender,null));
				}
			} else {
				alterComplements.add(new MetaRole(kinType.toString(),egoGender,Gender.UNKNOWN,null));
			}
		}			
			
		result = new Roles();
		for (Role role: alterComplements){
			Gender egoGender = ((MetaRole)role).egoGender;
			Gender alterGender = ((MetaRole)role).alterGender;
			if (config[2]){
				for (AlterAge alterAge : AlterAge.valuesNotUnknown()){
					result.add(new MetaRole(kinType.toString(),egoGender,alterGender,alterAge));
				}
			} else {
				result.add(new MetaRole(kinType.toString(),egoGender,alterGender,AlterAge.UNKNOWN));
			}
		}
				
		//
		return result;
	}
	
	public Roles getGenerators (){
		Roles result;
		
		Map<KinType,Boolean[]> configs = new TreeMap<KinType,Boolean[]>();
		for (KinType kinType : KinType.basicTypesWithSiblings()){
			configs.put(kinType, new Boolean[]{false,false,false});
		}
		
		for (Role role : roles()){
			MetaRole metaRole = (MetaRole)role;
			Boolean[] config = configs.get(metaRole.getKinType());
			if (config!=null){
				if (!metaRole.getEgoGender().isUnknown()){
					config[0]=true;
				}
				if (!metaRole.getAlterGender().isUnknown()){
					config[1]=true;
				}
				if (!metaRole.getAlterAge().isUnknown()){
					config[2]=true;
				}
			}
		}
		
		result = new Roles();
		for (KinType kinType : configs.keySet()){
			result.addAll(getRoles(kinType,configs.get(kinType)));
		}
		
		//
		return result;
	}
	
	public RoleRelations getByRole(Role role){
		RoleRelations result;
		
		result = new RoleRelations();
		
		for (RoleRelation relation : this){
			for (RoleActor actor : relation.getActors()){
				if (actor.getIndividual().equals(role)){
					result.add(relation);
					break;
				}
			}
		}
		
		//
		return result;
	}
	
	public Roles getEmicRoles(Role role, Gender egoGender, Gender alterGender, AlterAge alterAge){
		Roles result;
		
		result = new Roles();
		
		RoleRelation selfRelation = this.getSelfRelation(this.getSelf(egoGender));
				
		for (RoleActor actor : selfRelation.getActors()){
			
			if (actor.getRole().getName().equals(role.getName()) && actor.getRole().getEgoGender().matchs(egoGender) && actor.getRole().getAlterGender().matchs(alterGender) && actor.getRole().getAlterAge().matchs(alterAge)){

				result.add(actor.getIndividual());
			}
		}
		//
		return result;
	}
	
	
	public RoleRelations getBySelfRole(Role role, Gender egoGender){
		RoleRelations result;
		
		result = new RoleRelations();
		
		for (RoleRelation relation : this){
			RoleActor actor = relation.getSelf();
			if (actor.getIndividual().equals(role)){ 
				if (egoGender==null || egoGender == actor.getRole().getEgoGender()){
					result.add(relation);
				}
			}
		}
		//
		return result;
	}

	public boolean isEgoGenderDistinction() {
		return egoGenderDistinction;
	}

	public void setEgoGenderDistinction(boolean egoGenderDistinction) {
		this.egoGenderDistinction = egoGenderDistinction;
	}
	
	
	public static boolean isNullOrHasNullFactor(Roles composition){
		boolean result;
		
		result = false;
		if (composition==null){
			result = true;
		} else if (composition.size()<2 || composition.get(0)==null || composition.get(1)==null) {
			result = true;
		}
		//
		return result;
	}

	public String toString (){
		String result;
		
		result = this.toList().toString();
		
		//
		return result;
	}

	public Map<Role, Roles> getGenderConfigs() {
		return genderConfigs;
	}

	public void setGenderConfigs(Map<Role, Roles> genderConfigs) {
		this.genderConfigs = genderConfigs;
	}
	
	public boolean hasEgoGenderDistinction(){
		boolean result;
		
		result = false;
		for (RoleRelation relation : this){
			if (!relation.getSelf().getEgoGender().isUnknown()){
				result = true;
				break;
			}
		}
		//
		return result;
	}
	
	public boolean hasAlterGenderDistinction(){
		boolean result;
		
		result = false;
		for (RoleRelation relation : this){
			if (!relation.getSelf().getAlterGender().isUnknown()){
				result = true;
				break;
			}
		}
		//
		return result;
	}
	
	public boolean hasAlterAgeDistinction(){
		boolean result;
		
		result = false;
		for (RoleRelation relation : this){
			if (!relation.getSelf().getAlterAge().isUnknown()){
				result = true;
				break;
			}
		}
		//
		return result;
	}

	public RoleDefinitions getOriginalDefinitions() {
		return originalDefinitions;
	}

	public void setOriginalDefinitions(RoleDefinitions originalDefinitions) {
		this.originalDefinitions = originalDefinitions;
	}
	
	public Gender getEgoGenderScope(RoleActor actor){
		Gender result;
		
		result = null; 
		
		if (actor.hasSelfName()){
			
			result = Gender.UNKNOWN;
			
		} else {
			
			for (Role metaRole : genderConfigs.get(actor.getIndividual())){
				
				if (metaRole.getName().equals(actor.getRole().getName())){
					
					Gender egoGender = ((MetaRole)metaRole).getEgoGender();
					
					if (result==null){
						result = egoGender;
					} else if (result!=egoGender){
						result = Gender.UNKNOWN;
					}
					
					if (result.isUnknown()){
						break;
					}
				}
			}
		}
		
		//
		return result;
	}

	public String getSelfName() {
		return selfName;
	}

	public void setSelfName(String selfName) {
		this.selfName = selfName;
	}

	RoleActor getSelf(Gender gender){
		RoleActor result;
		
		result = new RoleActor(new Role(selfName),new MetaRole("SELF",gender,gender,AlterAge.UNKNOWN),selfName);

		//
		return result;
	}
	
	
	
	

}
