package oldcore.trash;

import java.util.Map;
import java.util.TreeMap;

import oldcore.random.RandomMatrix;

public class Matrix {

	/**
	 * an auxiliary method that calclulates the relinking statistics (total number of circuits, absolute and relative parallel bias) form the number of parallel and cross circuits
	 * @param rel the array of parallel (0) and cross (1) circuit frequencies
	 * @return the result array (0 total number of circuits, 1 absolute parallel bias, 2 relative parallel bias)
	 * @since 10-08-08
	 * @see matrices.Matrix#getRelinkings(int)
	 */
	public static double[] getRelinkingStatistics(double[] rel){
		double[] delta = new double[3];
		delta[0] = rel[0]+rel[1];
		delta[1] = rel[0]-rel[1];
		delta[2] = delta[1]/delta[0];
		return delta;
	}
	/**
	 * rounds a double to 2 decimals
	 * @param d the double
	 * @return the rounded double
	 */
	public static double rd(double d){
		return oldcore.trash.Mat.round(d,2);
	}
	/**
	 * the intermarriage array
	 */
	int[][] matrix;
	/**
	 * the array of row sums (numbers of wives given)
	 */
	int[] rows;
	/**
	 * the array of column sums (numbers of wives taken)
	 */
	int[] columns;
	/**
	 * the total sum of values
	 */
	int sum;
	
	/**
	 * a mapping from partition indices to indices of a superpartition
	 */
	protected TreeMap<Integer,Integer> index;
	
	/**
	 * a mapping from cell indices to upper segment bounds in the (0,t) interval, where segment size corresponds to row and column sum products
	 * needed for randomMatrix construction
	 */
	TreeMap<Integer,int[]> map;	
	
	/**
	 * an array of relinking statistics
	 * <p> 0 parallel circuits, 1 cross circuits, 2 marriages in parallel circuits, 3 marriages in cross circuits, 4 marriages in circuits
	 */
	double[] relinkings;	
	
	boolean isSymmetric;
	
	private int numberOfCircuits;
	private int numberOfParallelCircuits;
	private int numberOfCrossCircuits;
	
	private int numberOfCyclicTriangles;
	private int numberOfTransitiveTriangles;

	private double normalizedNumberOfCircuits;
	private double normalizedNumberOfParallelCircuits;
	private double normalizedNumberOfCrossCircuits;
	
	private double normalizedNumberOfCyclicTriangles;
	private double normalizedNumberOftransitiveTriangles;

	private double concentrationIndex;
	private double symmetryIndex;
	private double endogamicConcentrationIndex;
	private double endogamyIndex;
	private double strengthConcentrationIndex;
	private double endogamicStrengthConcentrationIndex;
	private double strengthSymmetryIndex;
	
	private double expectedNumberOfCircuits;
	private double expectedNumberOfParallelCircuits;
	private double expectedNumberOfCrossCircuits;
	private double expectedNormalizedNumberOfCircuits;
	private double expectedNormalizedNumberOfParallelCircuits;
	private double expectedNormalizedNumberOfCrossCircuits;
	
	private double expectedConcentrationIndex;
	private double expectedEndogamicConcentrationIndex;
	private double expectedSymmetryIndex;
	private double expectedEndogamyIndex;



	/**
	 * constructs an empty n x n matrix 
	 * @param n the number of rows and columns
	 * @since 10-05
	 */
	public Matrix(int n){
		matrix = new int[n][n];
		rows = new int[n];
		columns = new int[n];
	}

	/**
	 * constructs an empty m x n matrix 
	 */
	public Matrix(int m, int n){
		matrix = new int[m][n];
		rows = new int[m];
		columns = new int[n];
	}
	
	/**
	 * constructs a matrix by fusion of a system of submatrices
	 * @param n the dimension of the matrix
	 * @param matrices the submatrix system
	 */
	public Matrix (int n, Map<int[],Matrix> matrices){
		matrix = new int[n][n];
		rows = new int[n];
		columns = new int[n];
		for (Matrix mat : matrices.values()){
			int m = mat.getRowDim();
			for (int i=0;i<m;i++){
				int a = mat.getIndex(i);
				for (int j=0;j<m;j++){
					int b = mat.getIndex(j);
					augment(a, b, mat.get(i,j));
				}
			}
		}
	}	
	

	/**
	 * constructs a matrix by reducing another matrix
	 * <p> all groups (rows and columns) that cannnot be implied in any relinking structure are eliminated
	 * @param mat the original matrix
	 * @since 10-05
	 */
	public Matrix (Matrix mat){
		int n = 0;
		for (int i=0;i<mat.getRowDim();i++){
			if (!mat.cannotPass(i)) n++;
		}
		matrix = new int[n][n];
		rows = new int[n];
		columns = new int[n];
//		part = new int[n];
		int a=0;
		for (int i=0;i<mat.getRowDim();i++){
			if (mat.cannotPass(i)) continue;
//			if (mat.part!=null) part[a]=mat.part[i];
			int b = 0;
			for (int j=0;j<mat.getRowDim();j++){
				if (mat.cannotPass(j)) continue;
				int k = mat.matrix[i][j];
				matrix[a][b]=k;
				rows[a]=rows[a]+k;
				columns[b]=columns[b]+k;
				sum=sum+k;
				b++;
			}
			a++;
		}
	}
	

	/**
	 * augments a cell value by one
	 * @param i the row index of the cell
	 * @param j the column index of the cell
	 * @since 10-05-19
	 */
	protected void augment(int i, int j){
		matrix[i][j]++; 
		rows[i]++;
		columns[j]++;
		sum++;
		
	}	

	/**
	 * augments a cell value by a given value
	 * @param i the row index of the cell
	 * @param j the column index of the cell
	 * @since 10-05-19
	 */
	public void augment(int i, int j, int v){
		matrix[i][j]=matrix[i][j]+v; 
		rows[i]=rows[i]+v;
		columns[j]=columns[j]+v;
		sum=sum+v;
	}
	
	
	/**
	 * checks whether a group can form part of a possible relinking circuit
	 * @param i the index of the group
	 * @return true if the group cannot form part of a possible relinking circuit
	 * @since 10-05-18
	 */
	private boolean cannotPass(int i){
		if (rows[i]>0 && columns[i]>0) return false;
		if (rows[i]>1 || columns[i]>1) return false;
		return true;
	}
	
	/**
	 * returns the value of a cell
	 * @param i the row index of the cell
	 * @param j the column index of the cell
	 * @return the cell value
	 * @since 10-05
	 */
	public int get(int i, int j){
		return matrix[i][j];
	}
	
	/**
	 * gets the number of columns
	 * @return the number of columns
	 * @since 10-05-19
	 */
	public int getColDim(){
		return columns.length;
	}
	
	/**
	 * gets the ith column sum
	 * @param i the column index
	 * @return the ith column sum
	 * @since 11-05-21
	 */
	public int getColSum (int i){
		return columns[i];
	}
	
	/**
	 * gets the mean column sum
	 * @return the mean column sum
	 */
	public double getColSumMeans(){
		return new Double(sum)/new Double(getColDim());
	}
	

	/**
	 * gets the vector of column sums (numbers of wives taken)
	 * @return the vector of column sums
	 * @since 10-07-07
	 */
	public int[] getColSums(){
		return columns;
	}

	
	/**
	 * gets the variance of column sums
	 * @return the variance of column sums
	 */
	public double getColSumVariance(){
		double means = getColSumMeans();
		double var = 0.;
		for (int i=0;i<getColDim();i++){
			var = var+Math.pow(columns[i]-means,2);
		}
		return var/new Double(getRowDim());
	}
	
	public double getSymmetryIndex() {
		return symmetryIndex;
	}
	
	/**
	 * gets the superpartition index for a given index
	 * @param i the index
	 * @return the superpartition index
	 */
	protected int getIndex(int i){
		return index.get(i);
	}


	/**
	 * counts the relinkings and computes the relinking statistics 
	 * @return the array of relinkings 
	 * <p> 0 total circuits, 1 absolute parallel bias, 2 relative parallel bias, 3 parallel circuits, 4 cross circuits, 
	 * 5 marriages in parallel circuits, 6 marriages in cross circuits, 7 marriages in circuits
	 * @since 10-05-19, modif 10-05-20, 10-08-07
	 * @see OldMatrix#getRelinkings(int)
	 * @see gui.screens.MatrixSumulationScreen#getRelinkingStatistics(int,int)
	 */
	public double[] getRelinkings (){
		if (relinkings!=null) return relinkings;
		relinkings = new double[8];
//		double den = 0;
//		double num = 0;
		for (int i=0;i<getRowDim();i++) {
			for (int j=0;j<i;j++) {
				double v = get(i,j);
				double w = get(j,i);
				double x = v*(v-1)/2 + w*(w-1)/2;
//				double x = Math.max(0,v*(v-1)/2) + Math.max(0,w*(w-1)/2);
				double y = v*w;
//				num = num + Math.pow(v-w, 2)-v-w;
//				den = den + Math.pow(v+w, 2)-v-w;
				relinkings[3] = relinkings[3] + x;
				relinkings[4] = relinkings[4] + y;
				if (v>1) relinkings[5] = relinkings[5] +v;
				if (w>1) relinkings[5] = relinkings[5] +w;
				if (v>0 && w>0) relinkings[6] = relinkings[6]+v+w;
				if (v>1 || (v>0 && w>0)) relinkings[7]=relinkings[7]+v;
				if (w>1 || (v>0 && w>0)) relinkings[7]=relinkings[7]+w;
			}
		}
		relinkings[0] = relinkings[3]+relinkings[4];
		relinkings[1] = relinkings[3]-relinkings[4];
		relinkings[2] = relinkings[1]/relinkings[0];
		return relinkings;
	}
	
	/**
	 * gets the mean relinking frequencies for a set of randomly permuted arrays
	 * <p> 0 total circuits, 1 absolute parallel bias, 2 relative parallel bias, 3 parallel circuits, 4 cross circuits, 
	 * 5 marriages in parallel circuits, 6 marriages in cross circuits, 7 marriages in circuits
	 * @param runs the number of runs
	 * @return the result array of relinking frequencies (0 parallel, 1 cross)
	 * @since 10-05-19, modif 10-08-17
	 */
/*	double[] getRelinkings (int runs, boolean exact){
		if (runs==0) return getRelinkings();
		double[] n = new double[8];
		if (map==null) setMap();
//		if (t==0) setMap();
//		Matrix moy = new Matrix(getRowDim());
		for (int k=0;k<runs;k++){
			Matrix rm = permute(exact); 
			double[] r = rm.getRelinkings();
//			addMatrix(moy,rm);
			oldcore.calc.Mat.addArray(n,r);
			for (int i=0;i<m;i++){
				n[i]=n[i]+r[i];
			}
		}
		oldcore.calc.Mat.divArray(n,runs);
		for (int i=0;i<m;i++){
			n[i]=n[i]/runs;
		}
//		divMatrix(moy,runs);
		String s = "Permutation";
		if (!exact) s = "Shuffling";
		end(s+" ("+runs+" runs) finished");
		return n;
	}*/
	
	/**
	 * returns the theoretically expected number of circuits, as well as the absolute and relative parallel bias of the alliance network
	 * @return the result array (0 number of circuits, 1 absolute parallel bias, 2 relative parallel bias)
	 * @since 10-07-04, modif. 10-08-07
	 */
	double[] getRelinkingStatistics(){
//		if (runs>=0) return getRelinkingStatistics(getRelinkings(runs));
		double[] delta = new double[3];
		long[] k = new long[4];
		for (int i=0;i<getRowDim();i++){
			long x = rows[i];
			long y = columns[i];
			long x2 = (long)Math.pow(x,2);
			long y2 = (long)Math.pow(y,2);
			k[0]= k[0]+x2;
			k[1]= k[1]+y2;
			k[2]= k[2]+x*y;
			k[3]= k[3]+x2*y2;
		}
		k[2]=(long)Math.pow(k[2],2);
		long num = k[0]*k[1]-k[2];
		long den = k[0]*k[1]+k[2]-2*k[3];
		double a = new Double(sum-1)/(2*Math.pow(sum,3));
		delta[0] = a*den;
		delta[1] = a*num;
		delta[2] = delta[1]/delta[0];
		return delta;
	}
	
	/**
	 * gets the number of rows
	 * @return the number of rows
	 * @since 10-05-19
	 */
	public int getRowDim(){
		return rows.length;
	}
	
	/**
	 * gets the ith row sum
	 * @param i the row index
	 * @return the ith row sum
	 * @since 11-05-21
	 */
	public int getRowSum (int i){
		return rows[i];
	}	
	
	/**
	 * gets the mean row sum
	 * @return the mean row sum
	 */
	public double getRowSumMeans(){
		return new Double(sum)/new Double(getRowDim());
	}
	
	/**
	 * gets the vector of row sums (numbers of wives given)
	 * @return the vector of row sums
	 * @since 10-07-07
	 */
	public int[] getRowSums(){
		return rows;
	}

	/**
	 * gets the variance of row sums
	 * @return the variance of row sums
	 */
	public double getRowSumVariance(){
		double means = getRowSumMeans();
		double var = 0.;
		for (int i=0;i<getRowDim();i++){
			var = var+Math.pow(rows[i]-means,2);
		}
		return var/new Double(getRowDim());
	}
	
	/**
	 * gets the cell value sum
	 * @return the sum of cell values
	 * @since 10-05
	 */
	public int getSum(){
		return sum;
	}
	
	/**
	 * redistributes the arcs of the matrix, preserving line and columns sums (exact permutation) or with probabilities according to original line and column sums (shuffling)
	 * @param exactSums true if row and column sums are to be exactly preserved (permutation), false otherwise (shuffling)
	 * @return the permuted matrix matrix
	 * @see OldMatrix#getRelinkings(int)
	 */
	protected Matrix permute(boolean exact){
		return new RandomMatrix(this,exact);
	}
	
	
	protected void print(){
		for (int i=0;i<matrix.length;i++){
			String s="";
			for (int j=0;j<matrix.length;j++){
				s=s+matrix[i][j]+"\t";
			}
			System.out.println(s);
		}	
	}
	
	
	
	//not functional for the moment, should reduce the computational charge
	/**
	 * gets the reduced matrix
	 * <p> all groups (rows and columns) that cannnot be implied in any relinking structure are eliminated
	 * @since 10-05
	 */
	public Matrix reduce (){
		return new Matrix(this);
	}
	
	/**
	 * returns the relinking statistics of the network as a string
	 * @return the relinking statistics as string
	 * @since 10-07-04, modif. 10-08-07, 10-08-09, 10-08-17
	 * @see maps.Net#reportRelinkings(String, int)
	 */
/*	public String reportRelinkingStatistics(int runs){
		double[] d0 = getRelinkings();
		double[] d1 = getRelinkings(runs,false);
		double[] d2 = getRelinkings(runs,true);
		double[] d3 = getRelinkingStatistics();
//		double[] d0 = getRelinkingStatistics(getRelinkings());
//		double[] d1 = getRelinkingStatistics(getRelinkings(runs,false));
//		double[] d2 = getRelinkingStatistics(getRelinkings(runs,true));
//		double[] d3 = getRelinkingStatistics();
		String s = "";
		s = s+getRowDim()+"\t";
		s = s+getSum()+"\t";
		s = s+rd(getRowSumMeans())+"\t";
		s = s+rd(getRowSumVariance())+"\t";
		s = s+rd(getColSumVariance())+"\t";
		s = s+rd(d0[0])+"\t";
		s = s+rd(d1[0])+"\t";
		s = s+rd(d2[0])+"\t";
		s = s+rd(d3[0])+"\t";
		s = s+rd(d0[1])+"\t";
		s = s+rd(d1[1])+"\t";
		s = s+rd(d2[1])+"\t";
		s = s+rd(d3[1])+"\t";
		s = s+rd(100*d0[2])+"%\t";
		s = s+rd(100*d1[2])+"%\t";
		s = s+rd(100*d2[2])+"%\t";
		s = s+rd(100*d3[2])+"%\t";
		return s;
	}*/
	
	/**
	 * sets the value of matrix cell ij to v
	 * @param i the row index
	 * @param j the column index
	 * @param v the cell value
	 */
	public void set(int i, int j, int v){
		matrix[i][j]=v;
		if (isSymmetric) matrix[j][i]=v;
	}
	
	public int[] getRow (int i){
		return matrix[i];
	}

	
	
	/**
	 * sets the superpartition index for a given index
	 * @param i the index
	 * @param j the superpartition index
	 */
	protected void setIndex(int i, int j){
		if (index.get(i)==null) index.put(i, j);
	}


	/**
	 * maps cell indices to upper segment bounds in the (0,t) interval, where segment size corresponds to row and column sum products
	 */
	protected void setMap(){
		if (map!=null) return; 
		map = new TreeMap<Integer,int[]>();
		int t = 0;
		int k = 1;
		for (int i=0;i<getRowDim();i++){
			for (int j=0;j<getColDim();j++){
				int d = rows[i]*columns[j];
				if (d==0) continue;
				t=t+d;
				map.put(t, new int[]{i,j});
//				comment(i+"\t"+j+"\t"+t);
				k++;
			}
		}
	}
	
	void analyze (){
		
		double squareSum = 0.;
		double crossSum = 0.;
		double loopSum = 0.;
		double squareLoopSum = 0.;
		double sumSquare = Math.pow(sum,2);
		double sumDoubleSquare = Math.pow(sum,4);
		double squareOutStrengthSum = 0.;
		double squareInStrengthSum = 0.;
		double squareStrengthProductSum = 0.;
		double strengthProductSum = 0.;
		
		numberOfCyclicTriangles = 0;
		numberOfTransitiveTriangles = 0;
		
		for (int i=0;i<getRowDim();i++){
			loopSum = loopSum + get(i,i);
			squareLoopSum = squareLoopSum+Math.pow(get(i,i), 2);
			squareOutStrengthSum = squareOutStrengthSum + Math.pow(rows[i], 2);
			squareInStrengthSum = squareInStrengthSum + Math.pow(columns[i], 2);
			strengthProductSum = strengthProductSum +  rows[i]*columns[i];
			squareStrengthProductSum = squareStrengthProductSum +  Math.pow(rows[i]*columns[i], 2);
			for (int j=0;j<getColDim();j++){
				squareSum = squareSum+Math.pow(get(i,j), 2);
				crossSum = crossSum+get(i,j)*get(j,i);
				if (get(i,j)>0){
					for (int k=0; k<getRowDim();k++){
						int indirectPaths = get(i,j)*get(j,k);
						if (i<j && j<k) numberOfCyclicTriangles = numberOfCyclicTriangles + indirectPaths*get(k,i);
						if (i!=j && j!=k && k!=i) numberOfTransitiveTriangles = numberOfTransitiveTriangles + indirectPaths*get(i,k);
					}
				}
			}
		}
		
		concentrationIndex = squareSum/sumSquare;
		symmetryIndex = crossSum/squareSum;
		if (loopSum>0){
			endogamicConcentrationIndex = squareLoopSum/Math.pow(loopSum,2);
		} else {
			endogamicConcentrationIndex = 0.;
		}
		endogamyIndex = loopSum/sum;
		
		numberOfParallelCircuits = new Double(squareSum - sum - squareLoopSum + loopSum).intValue()/2;
		normalizedNumberOfParallelCircuits = (squareSum - sum - squareLoopSum + loopSum)/sumSquare;
		numberOfCrossCircuits = new Double(crossSum - squareLoopSum).intValue()/2;
		normalizedNumberOfCrossCircuits = (crossSum - squareLoopSum)/sumSquare;
		numberOfCircuits = numberOfParallelCircuits + numberOfCrossCircuits;
		normalizedNumberOfCircuits = normalizedNumberOfParallelCircuits + normalizedNumberOfCrossCircuits;
		
		
		
		strengthConcentrationIndex = squareInStrengthSum*squareOutStrengthSum/sumDoubleSquare;
		endogamicStrengthConcentrationIndex = squareStrengthProductSum/Math.pow(strengthProductSum,2);
		strengthSymmetryIndex = Math.pow(squareStrengthProductSum, 0.5)/strengthProductSum;
		
		double factor0 = new Double(1.0/sum);
		int factor1 = sum*(sum-1)/2;
		double factor2 = (1.0 - factor0);

		double factor3 = factor2*Math.pow(strengthProductSum,2)/sumDoubleSquare + factor0*strengthProductSum/sumSquare;
		double factor4 = factor2*squareStrengthProductSum/sumDoubleSquare + factor0*strengthProductSum/sumSquare;

		expectedEndogamyIndex = strengthProductSum/sumSquare;
		expectedConcentrationIndex = factor2*strengthConcentrationIndex + factor0;
		expectedSymmetryIndex = (factor3)/expectedConcentrationIndex;
		expectedEndogamicConcentrationIndex = factor4/factor3;
		
//		double mathematicalBeta = (concentrationIndex*(1 + symmetryIndex)) - (1 - endogamyIndex)/sum;
//		System.out.println (mathematicalBeta);
	

		
		double expectedRelativeNumberOfParallelCircuits = strengthConcentrationIndex - squareStrengthProductSum/sumDoubleSquare;
		double expectedRelativeNumberOfCrossCircuits = Math.pow(strengthProductSum,2)/sumDoubleSquare - squareStrengthProductSum/sumDoubleSquare;

		expectedNumberOfParallelCircuits = factor1*(expectedRelativeNumberOfParallelCircuits);
		expectedNumberOfCrossCircuits = factor1*(expectedRelativeNumberOfCrossCircuits);
		expectedNumberOfCircuits = expectedNumberOfParallelCircuits + expectedNumberOfCrossCircuits;

		expectedNormalizedNumberOfParallelCircuits = factor2*(expectedRelativeNumberOfParallelCircuits);
		expectedNormalizedNumberOfCrossCircuits = factor2*(expectedRelativeNumberOfCrossCircuits);
		expectedNormalizedNumberOfCircuits = expectedNormalizedNumberOfParallelCircuits + expectedNormalizedNumberOfCrossCircuits;
		

	}
	
	public int getNumberOfCyclicTriangles() {
		return numberOfCyclicTriangles;
	}
	public int getNumberOfTransitiveTriangles() {
		return numberOfTransitiveTriangles;
	}
	public double getExpectedConcentrationIndex() {
		return expectedConcentrationIndex;
	}
	public double getExpectedEndogamicConcentrationIndex() {
		return expectedEndogamicConcentrationIndex;
	}
	public double getExpectedSymmetryIndex() {
		return expectedSymmetryIndex;
	}
	public double getExpectedEndogamyIndex() {
		return expectedEndogamyIndex;
	}

	public double getStrengthSymmetryIndex() {
		return strengthSymmetryIndex;
	}
	public double getExpectedNumberOfCircuits() {
		return expectedNumberOfCircuits;
	}
	public double getExpectedNumberOfParallelCircuits() {
		return expectedNumberOfParallelCircuits;
	}
	public double getExpectedNumberOfCrossCircuits() {
		return expectedNumberOfCrossCircuits;
	}
	public double getExpectedNormalizedNumberOfCircuits() {
		return expectedNormalizedNumberOfCircuits;
	}
	public double getExpectedNormalizedNumberOfParallelCircuits() {
		return expectedNormalizedNumberOfParallelCircuits;
	}
	public double getExpectedNormalizedNumberOfCrossCircuits() {
		return expectedNormalizedNumberOfCrossCircuits;
	}

	public int getBalanceOfParallelVsCrossCircuits(){
		return numberOfParallelCircuits - numberOfCrossCircuits;
	}
	public double getExpectedBalanceOfParallelVsCrossCircuits(){
		return expectedNumberOfParallelCircuits - expectedNumberOfCrossCircuits;
	}
	public double getExpectedNormalizedBalanceOfParallelVsCrossCircuits(){
		return expectedNormalizedNumberOfParallelCircuits - expectedNormalizedNumberOfCrossCircuits;
	}
	public double getNormalizedBalanceOfParallelVsCrossCircuits(){
		return normalizedNumberOfParallelCircuits - normalizedNumberOfCrossCircuits;
	}

	public int getNumberOfCircuits() {
		return numberOfCircuits;
	}
	public int getMaximalNumberOfCircuits() {
		return new Double(Math.pow(sum, 2)).intValue()/2;
	}
	public int getNumberOfParallelCircuits() {
		return numberOfParallelCircuits;
	}
	public int getNumberOfCrossCircuits() {
		return numberOfCrossCircuits;
	}
	public double getNormalizedNumberOfCircuits() {
		return normalizedNumberOfCircuits;
	}
	public double getNormalizedNumberOfParallelCircuits() {
		return normalizedNumberOfParallelCircuits;
	}
	public double getNormalizedNumberOfCrossCircuits() {
		return normalizedNumberOfCrossCircuits;
	}
	public double getConcentrationIndex() {
		return concentrationIndex;
	}
	public double getEndogamicConcentrationIndex() {
		return endogamicConcentrationIndex;
	}
	public double getEndogamyIndex() {
		return endogamyIndex;
	}

	public double getStrengthConcentrationIndex() {
		return strengthConcentrationIndex;
	}
	public double getEndogamicStrengthConcentrationIndex() {
		return endogamicStrengthConcentrationIndex;
	}
	
	public double getSurplusOfCircuits(){
		return (numberOfCircuits - expectedNumberOfCircuits)/expectedNumberOfCircuits;
	}
	public double getSurplusOfParallelCircuits(){
		return (numberOfParallelCircuits - expectedNumberOfParallelCircuits)/expectedNumberOfParallelCircuits;
	}
	public double getSurplusOfCrossCircuits(){
		return (numberOfCrossCircuits - expectedNumberOfCrossCircuits)/expectedNumberOfCrossCircuits;
	}
	
	public double getMinimumConcentration(){
		return 1.0/Math.pow(getRowDim(),2);
	}

	

}

