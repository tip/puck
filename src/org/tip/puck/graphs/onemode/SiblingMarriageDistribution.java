package org.tip.puck.graphs.onemode;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.util.RandomGenerator;


/**
 * @author Telmo Menezes
 * 
 */
public class SiblingMarriageDistribution {
    private Map<Integer, Vector<Integer>> distribMapMale;
    private Map<Integer, Vector<Integer>> distribMapFemale;
    
    public SiblingMarriageDistribution(Net net) {
        // compute distribution maps
        distribMapMale = new HashMap<Integer, Vector<Integer>>();
        distribMapFemale = new HashMap<Integer, Vector<Integer>>();
        
        for (Individual ind : net.individuals()) {
            int s = GraphModeTransformer.monogamousSameSexSiblings(ind).size();
            int m = ind.spouses().size();
            int k = m + s;
            
            if (ind.getGender() == Gender.MALE) {
                if (distribMapMale.containsKey(k)) {
                    distribMapMale.get(k).add(m);
                }
                else {
                    Vector<Integer> vec = new Vector<Integer>();
                    vec.add(m);
                    distribMapMale.put(k, vec);
                }
            }
            else if (ind.getGender() == Gender.FEMALE) {
                if (distribMapFemale.containsKey(k)) {
                    distribMapFemale.get(k).add(m);
                }
                else {
                    Vector<Integer> vec = new Vector<Integer>();
                    vec.add(m);
                    distribMapFemale.put(k, vec);
                }
            } 
        }
    }
    
    private boolean isKeyCloser(int key, int bestKey, int k) {
        int deltaKey = key - k;
        if (deltaKey < 0) {
            deltaKey = -deltaKey;
        }
        int deltaBest = bestKey - k;
        if (deltaBest < 0) {
            deltaBest = -deltaBest;
        }
        return deltaKey < deltaBest;
    }
    
    public int howManySpouses(Gender gender, int k) {
        Vector<Integer> distrib = null;
        boolean aprox = false;
        int bestKey = -1;
        if (gender == Gender.MALE) {
            if (distribMapMale.containsKey(k)) {
                distrib = distribMapMale.get(k);
            }
            else {
                aprox = true;
                for (int key : distribMapMale.keySet()) {
                    if ((bestKey < 0) || (isKeyCloser(key, bestKey, k))) {
                        bestKey = key;
                    }
                }
                distrib = distribMapMale.get(bestKey);
            }
        }
        else if (gender == Gender.FEMALE) {
            if (distribMapFemale.containsKey(k)) {
                distrib = distribMapFemale.get(k);
            }
            else {
                aprox = true;
                for (int key : distribMapFemale.keySet()) {
                    if ((bestKey < 0) || (isKeyCloser(key, bestKey, k))) {
                        bestKey = key;
                    }
                }
                distrib = distribMapFemale.get(bestKey);
            }
        }
        
        int size = distrib.size();
        int index = RandomGenerator.instance().random.nextInt(size);
        int m = distrib.get(index);
        
        if (aprox) {
            if (bestKey < 0) {
                return 0;
            }
            double dm = (double)m;
            double dk = (double)k;
            double dbk = (double)bestKey;
            double dres = (dm / dbk) * dk;
            return (int)dres;
        }
        else {
            return m;
        }
    }
    
    @Override
    public String toString() {
        String str = "";
        
        str += "Male Distribution\n";
        for (Integer k : distribMapMale.keySet()) {
            Vector<Integer> distrib = distribMapMale.get(k);
            str += "\tk=" + k + "\n";
            for (Integer m : distrib) {
                str += "\t\tm=" + m + "; s=" + (k - m) + "\n";
            }
        }
        
        str += "\nFemale Distribution\n";
        for (Integer k : distribMapFemale.keySet()) {
            Vector<Integer> distrib = distribMapFemale.get(k);
            str += "\tk=" + k + "\n";
            for (Integer m : distrib) {
                str += "\t\tm=" + m + "; s=" + (k - m) + "\n";
            }
        }
        
        return str;
    }
}
