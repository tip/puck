package org.tip.puck.partitions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.tip.puck.util.Value;

/**
 * 
 * @author TIP
 */
public class Cluster<E> implements Comparable<Cluster<E>> {
	private Value value;
	private List<E> items;

	/**
	 * 
	 */
	public Cluster(final Value value) {
		this.value = value;
		this.items = new ArrayList<E>();
	}
	
	public Cluster<E> clone(){
		Cluster<E> result;
		
		result = new Cluster<E>(this.value);
		result.items.addAll(this.getItems());
		
		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int compareTo(final Cluster<E> other) {
		int result;

		result = new ClusterComparatorByValue().compare(this, other);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int count() {
		int result;

		result = this.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public E getFirstItem() {
		E result;

		if (items == null || items.size() == 0) {
			result = null;
		} else {
			result = items.get(0);
		}

		return result;
	}

	public List<E> getItems() {
		return items;
	}

	/**
	 * 
	 */
	public String getLabel() {
		String result;

		if (this.value == null) {
			result = null;
		} else {
			result = this.value.toString();
		}

		//
		return result;
	}

	public Value getValue() {
		return value;
	}

	/**
	 * 
	 */
	@Override
	public int hashCode() {
		int result;

		if (this.value == null) {
			result = 0;
		} else {
			result = this.value.hashCode();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		boolean result;

		if (this.size() == 0) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty() {
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * Checks whether the cluster has null.
	 * 
	 * @return true if the cluster has null, false otherwise.
	 */
	public boolean isNull() {
		boolean result;

		if (this.value == null || (this.getLabel() == null)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}
	
	/**
	 * Checks whether the cluster has null.
	 * 
	 * @return true if the cluster has null, false otherwise.
	 */
	public boolean isNullOrZero() {
		boolean result;

		if (this.value == null || (this.value.isNumber() && value.intValue() == 0) || (this.getLabel() == null)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}
	

	/**
	 * 
	 * @param item
	 */
	public boolean put(final E item) {
		boolean result;
		
		if (!items.contains(item)) {
			result = this.items.add(item);
		} else {
			result = false;
		}
		//
		return result;
	}
	
	public void remove(final E item){
		this.items.remove(item);
	}

	public void setValue(final Value value) {
		this.value = value;
	}

	/**
	 * 
	 * @return
	 */
	public int size() {
		int result;

		result = this.items.size();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		result = getLabel();

		//
		return result;
	}

	
	public String getItemsAsString(){
		String result;
		
		result = "";
		for (E item : getItems()){
			if (result.length()>0) {
				result += ";";
			}
			if (item!=null){
				result += item.toString();
			}
		}
		
		//
		return result;
	}

	public <T extends Comparable<T>> String getItemsAsSortedString(){
		String result;
		
		result = "";
		Collections.sort((List<T>)getItems());
		
		for (E item : getItems()){
			if (result.length()>0) {
				result += ";";
			}
			if (item!=null){
				result += item.toString();
			}
		}
		
		//
		return result;
	}
	
	public <T extends Comparable<T>> List<T> getItemsAsSortedList(){
		List<T> result;
		
		result = (List<T>)getItems();
		Collections.sort(result);
		
		//
		return result;
	}

}
