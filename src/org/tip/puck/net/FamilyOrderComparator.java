package org.tip.puck.net;

import java.util.Comparator;

import org.tip.puck.util.MathUtils;

/**
 * This class compares two families with one parent in common.
 * 
 * 
 * @author TIP
 * 
 */
public class FamilyOrderComparator implements Comparator<Family> {

	private Individual referent;

	/**
	 * 
	 * @param sorting
	 */
	public FamilyOrderComparator(final Individual referent) {
		//
		if (referent == null) {
			//
			throw new IllegalArgumentException("referent is null.");

		} else {
			//
			this.referent = referent;
		}
	}

	/**
	 * 
	 */
	@Override
	public int compare(final Family alpha, final Family bravo) {
		int result;

		result = compare(alpha, bravo, this.referent);

		//
		return result;
	}

	/**
	 * 
	 */
	public static int compare(final Family alpha, final Family bravo, final Individual referent) {
		int result;

		//
		Integer alphaValue;
		if (alpha == null) {
			//
			alphaValue = null;

		} else {
			//
			alphaValue = alpha.getUnionOrder(referent);
		}

		//
		Integer bravoValue;
		if (bravo == null) {
			//
			bravoValue = null;

		} else {
			//
			bravoValue = bravo.getUnionOrder(referent);
		}

		//
		result = MathUtils.compareOrder(alphaValue, bravoValue);

		//
		return result;
	}
}
