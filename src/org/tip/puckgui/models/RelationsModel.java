package org.tip.puckgui.models;

import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractListModel;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.Relations;

/**
 * 
 * @author TIP
 */
public class RelationsModel extends AbstractListModel {

	private static final long serialVersionUID = 6934494508049569439L;
	private Relations source;
	private List<Relation> delegate;
	private int lastSearchIndex;
	private String lastSearchPattern;

	/**
	 * 
	 */
	public RelationsModel(final Relations source) {
		//
		super();

		//
		this.source = source;
		if (source == null) {
			//
			this.delegate = new ArrayList<Relation>(0);

		} else {
			//
			this.delegate = this.source.toListSortedByTypeId();
		}

		//
		this.lastSearchIndex = -1;
		this.lastSearchPattern = null;
	}

	/**
	 * 
	 */
	@Override
	public Object getElementAt(final int index) {
		Relation result;

		result = this.delegate.get(index);

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getSize() {
		int result;

		result = this.delegate.size();

		//
		return result;
	}

	public Relations getSource() {
		return this.source;
	}

	/**
	 * 
	 * @param relation
	 * @return
	 */
	public int indexOf(final Relation relation) {
		int result;

		if (this.source == null) {
			//
			result = -1;

		} else {
			//
			boolean ended = false;
			result = -1;
			int index = 0;
			while (!ended) {
				//
				if (index < this.delegate.size()) {
					//
					if (this.delegate.get(index) == relation) {
						//
						ended = true;
						result = index;

					} else {
						//
						index += 1;
					}
				} else {
					//
					ended = true;
					result = -1;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param pattern
	 */
	public Relation nextSearched(final String pattern) {
		Relation result;

		if (StringUtils.isBlank(pattern)) {
			//
			result = null;

		} else if (NumberUtils.isDigits(pattern)) {
			// Search relation by id.
			int relationId = Integer.parseInt(pattern);
			result = this.source.getById(relationId);
			resetSearch();

		} else {
			// Search relation by name.
			List<Relation> foundRelations = this.source.searchByName(pattern);
			if (foundRelations.isEmpty()) {
				//
				resetSearch();
				result = null;

			} else if ((this.lastSearchPattern == null) || (!this.lastSearchPattern.equals(pattern))) {
				//
				this.lastSearchIndex = 0;
				this.lastSearchPattern = pattern;
				result = foundRelations.get(0);

			} else {
				//
				this.lastSearchIndex += 1;
				if (this.lastSearchIndex >= foundRelations.size()) {
					//
					this.lastSearchIndex = 0;
				}
				result = foundRelations.get(this.lastSearchIndex);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param pattern
	 */
	public int nextSearchedIndex(final String pattern) {
		int result;

		result = indexOf(nextSearched(pattern));

		//
		return result;
	}

	/**
	 * 
	 */
	public void resetSearch() {
		//
		this.lastSearchIndex = -1;
		this.lastSearchPattern = null;
	}

	/**
	 * 
	 * @param source
	 */
	public void setSource(final Relations source) {
		//
		resetSearch();
		fireIntervalRemoved(this, 0, this.delegate.size());
		this.source = source;
		this.delegate = this.source.toListSortedByTypeId();
		fireIntervalAdded(this, 0, this.delegate.size());
	}
}
