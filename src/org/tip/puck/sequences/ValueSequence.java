package org.tip.puck.sequences;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.tip.puck.PuckException;
import org.tip.puck.net.relations.workers.RelationValuator;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.workers.SequenceCriteria;
import org.tip.puck.util.Value;

/**
 * 
 * @author Klaus Hamberger
 *
 */
public class ValueSequence extends Sequence<Value> {
	
	int nrLoops;
	int nrDirectReturns;
	int nrCycles;
	Set<Value> support;  	
	Set<Value> centers;  	
	
	public ValueSequence(String idLabel, int id){
		super(idLabel,id);
	}
	
	public ValueSequence(EgoSequence source, Segmentation segmentation, String idLabel, Object parameter, SequenceCriteria criteria) throws PuckException{
		super(idLabel,source.getId());
		
		for (Ordinal time : source.getTimes()){
			put(time,RelationValuator.get(source.getStation(time), segmentation, source.getEgo(), idLabel, parameter, criteria));
		}
		//
		setProfile();
	}
	
	public void setProfile(){
				
		centers = new HashSet<Value>();
		support = new HashSet<Value>();
			
		int i=0;
		for (Value value : stations.values()){
				
			if (value!=null){
				support.add(value);
			}
			
			// Check return pattern
			Value previousValue = null;
			boolean closed = false;
						
			for (int  j=i-1;j>-1;j--){
				previousValue = stations.get(getTimes().get(j));
				closed = previousValue!=null && previousValue.equals(value);
				if (closed){
					centers.add(value);
					if (j == (i-1)){
						nrLoops++;
					} else if (j == (i-2)){
						nrDirectReturns++;
					} else {
						nrCycles++;
					}
					break;
				}
			}
			i++;
		}
	}
	
	
/*	public ValueSequence(List<Relation> events, Map<RelationClassificationType,Map<Relation,String>> map, RelationClassificationType type){
		
		profile = new ArrayList<String>();
		support = new HashSet<String>();
		centers = new HashSet<String>();
			
		int i=0;
		for (Relation event : events){
			if (RelationValuator.isBirth(event)){
				start = map.get(type).get(event);
			}
			
			if (!RelationValuator.isBirth(event) || type == RelationClassificationType.PLACE){
				
				String place = map.get(type).get(event);
				profile.add(place);
				support.add(place);
				
				// Check return pattern
				String previousPlace = null;
				boolean closed = false;
				
				for (int  j=i-1;j>-1;j--){
					previousPlace = profile.get(j);
					closed = previousPlace!=null && previousPlace.equals(place);
					if (closed){
						centers.add(place);
						if (j == (i-1)){
							nrLoops++;
						} else if (j == (i-2)){
							nrDirectReturns++;
						} else {
							nrCycles++;
						}
						break;
					}
				}
			}
			i++;
		}
	}*/
	
	public List<Value> getCentersAsList(){
		List<Value> result;
		
		result = new ArrayList<Value>(centers);
		Collections.sort(result);
		
		//
		return result;
	}
	
	public Value start(){
		Value result;

		if (stations.isEmpty()){
			result = null;
		} else {
			result = stations.get(getFirstTime());
		}
		
		//
		return result;
	}

	public List<Value> getCentersWithoutStartAsList(){
		List<Value> result;
		
		result = new ArrayList<Value>(centers);
		result.remove(start());
		Collections.sort(result);
		
		//
		return result;
	}

	public Set<Value> getSupport() {
		return support;
	}

	public List<Value> getSupportAsList(){
		List<Value> result;
		
		result = new ArrayList<Value>(support);
		Collections.sort(result);
		
		//
		return result;
	}

	public int getNrDirectReturns() {
		return nrDirectReturns;
	}

	public int getNrCycles() {
		return nrCycles;
	}

	public int getNrLoops() {
		return nrLoops;
	}
	
	public int getNrCenters(){
		return centers.size();
	}
	
	public int getNrStations(){
		return getSupport().size();
	}
	
	public int getNrCentersWithoutStart(){
		int result;
		
		result = getNrCenters();
		if (centers.contains(start())){
			result = result-1;
		}
		//
		return result;
	}
	
	public String getMovement (int i){
		String result;
		
		result = null;
		
		if (i==0){
			result = "ORIGIN";
		} else {
			Value here = stations.get(getTimes().get(i));
			
			if (here.toString().equals("UNKNOWN")){
				result = "UNKNOWN";
			} else {
				for (int j=i-1;j>-1;j--){
					Value there = stations.get(getTimes().get(j));
					if (there!=null && there.equals(here)){
						if (j==i-1){
							continue;
						} else if (j==i-2){
							result = "BACK";
						} else {
							result = "RETURN";
						}
						if (here.equals(start())){
							result += "ORIGIN";
						}
						break;
					}
				}
			}
		}
		
		if (result==null){
			result = "NEW";
		}
		
		//
		return result;

	}
	
	public ValueSequence getMovements (){
		ValueSequence result;
		
		result = new ValueSequence(idLabel+"_MOVEMENT",id);
		
		int i=0;
		for (Ordinal time : getTimes()){
			result.put(time,new Value(getMovement(i)));
			i++;
		}
		
		//
		return result;
	}

	
}
	
