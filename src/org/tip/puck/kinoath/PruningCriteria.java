package org.tip.puck.kinoath;

/**
 * This class defines criteria to prune a kin network browse.
 * 
 * @author TIP
 */
public class PruningCriteria {

	public enum AboveType {
		ALL,
		COLLATERALS
	};

	public enum UnderType {
		ALL,
		AFFINES
	};

	public static PruningCriteria DIRECT = new PruningCriteria(1, 1, 1, AboveType.COLLATERALS, UnderType.AFFINES);
	public static PruningCriteria GRAND = new PruningCriteria(2, 1, 2, AboveType.COLLATERALS, UnderType.AFFINES);
	public static PruningCriteria GREAT = new PruningCriteria(3, 1, 3, AboveType.COLLATERALS, UnderType.AFFINES);
	public static PruningCriteria FULL = new PruningCriteria(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, AboveType.ALL, UnderType.ALL);

	private int parentsDepth;
	private int partnersDepth;
	private int childrenDepth;
	private boolean collaterals;
	private boolean affines;

	/**
	 * 
	 */
	public PruningCriteria() {
		this(3, 1, 3, AboveType.COLLATERALS, UnderType.AFFINES);
	}

	/**
	 * 
	 */
	public PruningCriteria(final int parentsDepth, final int partnersDepth, final int childrenDepth, final AboveType aboveType, final UnderType underType) {
		this.parentsDepth = parentsDepth;
		this.partnersDepth = partnersDepth;
		this.childrenDepth = childrenDepth;
		this.collaterals = (aboveType == AboveType.COLLATERALS);
		this.affines = (underType == UnderType.AFFINES);
	}

	public int getChildrenDepth() {
		return this.childrenDepth;
	}

	public int getParentsDepth() {
		return this.parentsDepth;
	}

	public int getPartnersDepth() {
		return this.partnersDepth;
	}

	public boolean isAffines() {
		return this.affines;
	}

	public boolean isCollaterals() {
		return this.collaterals;
	}

	public void setAffines(final boolean affines) {
		this.affines = affines;
	}

	public void setChildrenDepth(final int childrenDepth) {
		this.childrenDepth = childrenDepth;
	}

	public void setCollaterals(final boolean collaterals) {
		this.collaterals = collaterals;
	}

	public void setParentsDepth(final int parentsDepth) {
		this.parentsDepth = parentsDepth;
	}

	public void setPartnersDepth(final int partnersDepth) {
		this.partnersDepth = partnersDepth;
	}
}
