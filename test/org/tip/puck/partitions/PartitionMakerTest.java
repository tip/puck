package org.tip.puck.partitions;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author TIP
 */
public class PartitionMakerTest {
	private static final Logger logger = LoggerFactory.getLogger(PartitionMakerTest.class);

	public static final String SIMPLE_FILE = "test/Resources/Chimane6.txt";
	public static final String COMPLEX_FILE = "test/Resources/Ebrei_08.txt";

	/**
     */
	@Test
	public void testGetIntervalsBySize01() throws Exception {

		logger.debug("========================== testGetIntervalsBySize01");

		Intervals result = PartitionMaker.getIntervalsBySize(null, null, null, null, null);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.isEmpty()).isTrue();
	}

	/**
     */
	@Test(expected = IllegalArgumentException.class)
	public void testGetIntervalsBySize02() throws Exception {

		logger.debug("========================== testGetIntervalsBySize02");

		double max = 2020;

		PartitionMaker.getIntervalsBySize(null, null, null, null, max);
	}

	/**
     */
	@Test(expected = IllegalArgumentException.class)
	public void testGetIntervalsBySize03() throws Exception {

		logger.debug("========================== testGetIntervalsBySize03");

		double min = 1880;

		PartitionMaker.getIntervalsBySize(null, null, null, min, null);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize04() throws Exception {

		logger.debug("========================== testGetIntervalsBySize04");

		double min = 1900;
		double max = 1920;

		Intervals result = PartitionMaker.getIntervalsBySize(null, null, null, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(20);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize05() throws Exception {

		logger.debug("========================== testGetIntervalsBySize05");

		double start = 1900;

		Intervals result = PartitionMaker.getIntervalsBySize(start, null, null, null, null);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(1);
	}

	/**
     */
	@Test(expected = IllegalArgumentException.class)
	public void testGetIntervalsBySize06() throws Exception {

		logger.debug("========================== testGetIntervalsBySize06");

		double start = 1900;
		double max = 2020;

		PartitionMaker.getIntervalsBySize(start, null, null, null, max);
	}

	/**
     */
	@Test(expected = IllegalArgumentException.class)
	public void testGetIntervalsBySize07() throws Exception {

		logger.debug("========================== testGetIntervalsBySize07");

		double min = 1880;
		double start = 1900;

		PartitionMaker.getIntervalsBySize(start, null, null, min, null);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize08a() throws Exception {

		logger.debug("========================== testGetIntervalsBySize08a");

		double start = 1910;
		double min = 1925;
		double max = 2020;

		Intervals result = PartitionMaker.getIntervalsBySize(start, null, null, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(110);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize08b() throws Exception {

		logger.debug("========================== testGetIntervalsBySize08b");

		double min = 1880;
		double start = 1910;
		double max = 2020;

		Intervals result = PartitionMaker.getIntervalsBySize(start, null, null, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(110 + 1);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize08c() throws Exception {

		logger.debug("========================== testGetIntervalsBySize08c");

		double start = 1910;
		double min = 1880;
		double max = 1885;

		Intervals result = PartitionMaker.getIntervalsBySize(start, null, null, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(1);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize09() throws Exception {

		logger.debug("========================== testGetIntervalsBySize09");

		double end = 1990;

		Intervals result = PartitionMaker.getIntervalsBySize(null, null, end, null, null);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(1);
	}

	/**
     */
	@Test(expected = IllegalArgumentException.class)
	public void testGetIntervalsBySize10() throws Exception {

		logger.debug("========================== testGetIntervalsBySize10");

		double end = 1990;
		double max = 2020;

		PartitionMaker.getIntervalsBySize(null, null, end, null, max);
	}

	/**
     */
	@Test(expected = IllegalArgumentException.class)
	public void testGetIntervalsBySize11() throws Exception {

		logger.debug("========================== testGetIntervalsBySize11");

		double end = 1910;
		double min = 2020;

		PartitionMaker.getIntervalsBySize(null, null, end, min, null);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize12a() throws Exception {

		logger.debug("========================== testGetIntervalsBySize12a");

		double min = 1880;
		double max = 1985;
		double end = 1990;

		Intervals result = PartitionMaker.getIntervalsBySize(null, null, end, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(110);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize12b() throws Exception {

		logger.debug("========================== testGetIntervalsBySize12b");

		double min = 1880;
		double end = 1990;
		double max = 2020;

		Intervals result = PartitionMaker.getIntervalsBySize(null, null, end, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(110 + 1);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize12c() throws Exception {

		logger.debug("========================== testGetIntervalsBySize12c");

		double end = 1910;
		double min = 2020;
		double max = 2030;

		Intervals result = PartitionMaker.getIntervalsBySize(null, null, end, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(1);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize13() throws Exception {

		logger.debug("========================== testGetIntervalsBySize13");

		double start = 1910;
		double end = 1990;

		Intervals result = PartitionMaker.getIntervalsBySize(start, null, end, null, null);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(80);
	}

	/**
     */
	@Test(expected = IllegalArgumentException.class)
	public void testGetIntervalsBySize14() throws Exception {

		logger.debug("========================== testGetIntervalsBySize14");

		double start = 1910;
		double end = 2020;
		double min = 2020;

		PartitionMaker.getIntervalsBySize(start, null, end, min, null);
	}

	/**
     */
	@Test(expected = IllegalArgumentException.class)
	public void testGetIntervalsBySize15() throws Exception {

		logger.debug("========================== testGetIntervalsBySize15");

		double start = 1910;
		double end = 2020;
		double min = 2020;

		PartitionMaker.getIntervalsBySize(start, null, end, min, null);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize16a() throws Exception {

		logger.debug("========================== testGetIntervalsBySize16a");

		double start = 1910;
		double min = 1925;
		double max = 1985;
		double end = 1990;

		Intervals result = PartitionMaker.getIntervalsBySize(start, null, end, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(80);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize16b() throws Exception {

		logger.debug("========================== testGetIntervalsBySize16b");

		double min = 1880;
		double start = 1910;
		double max = 1985;
		double end = 1990;

		Intervals result = PartitionMaker.getIntervalsBySize(start, null, end, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(1 + 80 + 0);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize16c() throws Exception {

		logger.debug("========================== testGetIntervalsBySize16c");

		double start = 1910;
		double min = 1925;
		double end = 1990;
		double max = 2020;

		Intervals result = PartitionMaker.getIntervalsBySize(start, null, end, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(0 + 80 + 1);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize16d() throws Exception {

		logger.debug("========================== testGetIntervalsBySize16d");

		double min = 1880;
		double start = 1910;
		double end = 1990;
		double max = 2020;

		Intervals result = PartitionMaker.getIntervalsBySize(start, null, end, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(1 + 80 + 1);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize17() throws Exception {

		logger.debug("========================== testGetIntervalsBySize17");

		double size = 10;

		Intervals result = PartitionMaker.getIntervalsBySize(null, size, null, null, null);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.isEmpty()).isTrue();
	}

	/**
     */
	@Test(expected = IllegalArgumentException.class)
	public void testGetIntervalsBySize18() throws Exception {

		logger.debug("========================== testGetIntervalsBySize18");

		double size = 10;
		double max = 2020;

		PartitionMaker.getIntervalsBySize(null, size, null, null, max);
	}

	/**
     */
	@Test(expected = IllegalArgumentException.class)
	public void testGetIntervalsBySize19() throws Exception {

		logger.debug("========================== testGetIntervalsBySize19");

		double size = 10;
		double min = 1880;

		PartitionMaker.getIntervalsBySize(null, size, null, min, null);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize20a() throws Exception {

		logger.debug("========================== testGetIntervalsBySize20a");

		double size = 10;
		double min = 1880;
		double max = 2020;

		Intervals result = PartitionMaker.getIntervalsBySize(null, size, null, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(14);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize20b() throws Exception {

		logger.debug("========================== testGetIntervalsBySize20b");

		double size = 10;
		double min = 2020;
		double max = 1880;

		Intervals result = PartitionMaker.getIntervalsBySize(null, size, null, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(14);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize20c() throws Exception {

		logger.debug("========================== testGetIntervalsBySize20c");

		double size = 10;
		double min = 1880;
		double max = 1880;

		Intervals result = PartitionMaker.getIntervalsBySize(null, size, null, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(1);

		Assertions.assertThat(result.get(0).getMin()).isEqualTo(1880);
		Assertions.assertThat(result.get(0).isMinIncluded()).isEqualTo(true);
		Assertions.assertThat(result.get(0).getMax()).isEqualTo(1880);
		Assertions.assertThat(result.get(0).isMaxIncluded()).isEqualTo(true);

	}

	/**
     */
	@Test
	public void testGetIntervalsBySize21() throws Exception {

		logger.debug("========================== testGetIntervalsBySize21");

		double size = 10;
		double start = 1910;

		Intervals result = PartitionMaker.getIntervalsBySize(start, size, null, null, null);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(1);
	}

	/**
     */
	@Test(expected = IllegalArgumentException.class)
	public void testGetIntervalsBySize22() throws Exception {

		logger.debug("========================== testGetIntervalsBySize22");

		double size = 10;
		double start = 1910;
		double max = 2020;

		PartitionMaker.getIntervalsBySize(start, size, null, null, max);
	}

	/**
     */
	@Test(expected = IllegalArgumentException.class)
	public void testGetIntervalsBySize23() throws Exception {

		logger.debug("========================== testGetIntervalsBySize23");

		double size = 10;
		double start = 1910;
		double min = 2020;

		PartitionMaker.getIntervalsBySize(start, size, null, min, null);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize24a() throws Exception {

		logger.debug("========================== testGetIntervalsBySize24a");

		double size = 10;
		double start = 1910;
		double min = 1925;
		double max = 2020;

		Intervals result = PartitionMaker.getIntervalsBySize(start, size, null, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(11);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize24b() throws Exception {

		logger.debug("========================== testGetIntervalsBySize24b");

		double size = 10;
		double min = 1880;
		double start = 1910;
		double max = 2020;

		Intervals result = PartitionMaker.getIntervalsBySize(start, size, null, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(1 + 11);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize25() throws Exception {

		logger.debug("========================== testGetIntervalsBySize25");

		double size = 10;
		double end = 1990;

		Intervals result = PartitionMaker.getIntervalsBySize(null, size, end, null, null);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(1);
		Assertions.assertThat(result.get(0).getMin()).isEqualTo(1980);
		Assertions.assertThat(result.get(0).isMinIncluded()).isEqualTo(true);
		Assertions.assertThat(result.get(0).getMax()).isEqualTo(1990);
		Assertions.assertThat(result.get(0).isMaxIncluded()).isEqualTo(false);
	}

	/**
     */
	@Test(expected = IllegalArgumentException.class)
	public void testGetIntervalsBySize26() throws Exception {

		logger.debug("========================== testGetIntervalsBySize26");

		double size = 10;
		double end = 1990;
		double max = 2020;

		Intervals result = PartitionMaker.getIntervalsBySize(null, size, end, null, max);
	}

	/**
     */
	@Test(expected = IllegalArgumentException.class)
	public void testGetIntervalsBySize27() throws Exception {

		logger.debug("========================== testGetIntervalsBySize27");

		double size = 10;
		double min = 1910;
		double end = 1990;

		Intervals result = PartitionMaker.getIntervalsBySize(null, size, end, min, null);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize28() throws Exception {

		logger.debug("========================== testGetIntervalsBySize28");

		double start = 1910;
		double size = 10;
		double end = 1990;
		double min = 1880;
		double max = 2020;

		Intervals result = PartitionMaker.getIntervalsBySize(start, size, end, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(10);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize29a() throws Exception {

		logger.debug("========================== testGetIntervalsBySize29a");

		double start = 1910;
		double size = 10;
		double end = 1990;

		Intervals result = PartitionMaker.getIntervalsBySize(start, size, end, null, null);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(8);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize29b() throws Exception {

		logger.debug("========================== testGetIntervalsBySize29b");

		double start = 1910;
		double size = 11;
		double end = 1910;

		Intervals result = PartitionMaker.getIntervalsBySize(start, size, end, null, null);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(1);
		Assertions.assertThat(result.get(0).getMin()).isEqualTo(1910);
		Assertions.assertThat(result.get(0).isMinIncluded()).isEqualTo(true);
		Assertions.assertThat(result.get(0).getMax()).isEqualTo(1910);
		Assertions.assertThat(result.get(0).isMaxIncluded()).isEqualTo(true);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize29c() throws Exception {

		logger.debug("========================== testGetIntervalsBySize29c");

		double start = 1910;
		double size = 10;
		double end = 1910;

		Intervals result = PartitionMaker.getIntervalsBySize(start, size, end, null, null);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(1);
		Assertions.assertThat(result.get(0).getMin()).isEqualTo(1910);
		Assertions.assertThat(result.get(0).isMinIncluded()).isEqualTo(true);
		Assertions.assertThat(result.get(0).getMax()).isEqualTo(1910);
		Assertions.assertThat(result.get(0).isMaxIncluded()).isEqualTo(true);
	}

	/**
     */
	@Test(expected = IllegalArgumentException.class)
	public void testGetIntervalsBySize30() throws Exception {

		logger.debug("========================== testGetIntervalsBySize30");

		double start = 1910;
		double size = 10;
		double end = 1990;
		double max = 2020;

		Intervals result = PartitionMaker.getIntervalsBySize(start, size, end, null, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(1);
	}

	/**
     */
	@Test(expected = IllegalArgumentException.class)
	public void testGetIntervalsBySize31() throws Exception {

		logger.debug("========================== testGetIntervalsBySize31");

		double start = 1910;
		double size = 10;
		double end = 1990;
		double min = 1880;

		Intervals result = PartitionMaker.getIntervalsBySize(start, size, end, min, null);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(1);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize32a() throws Exception {

		logger.debug("========================== testGetIntervalsBySize32a");

		double size = 10;
		double start = 1910;
		double min = 1925;
		double max = 1985;
		double end = 1990;

		Intervals result = PartitionMaker.getIntervalsBySize(start, size, end, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(8 + 0 + 0);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize32b() throws Exception {

		logger.debug("========================== testGetIntervalsBySize32b");

		double size = 10;
		double start = 1910;
		double min = 1925;
		double end = 1990;
		double max = 2020;

		Intervals result = PartitionMaker.getIntervalsBySize(start, size, end, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(0 + 8 + 1);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize32c() throws Exception {

		logger.debug("========================== testGetIntervalsBySize32c");

		double size = 10;
		double min = 1880;
		double start = 1910;
		double max = 1985;
		double end = 1990;

		Intervals result = PartitionMaker.getIntervalsBySize(start, size, end, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(1 + 8 + 0);
	}

	/**
     */
	@Test
	public void testGetIntervalsBySize32d() throws Exception {

		logger.debug("========================== testGetIntervalsBySize32d");

		double size = 10;
		double min = 1880;
		double start = 1910;
		double end = 1990;
		double max = 2020;

		Intervals result = PartitionMaker.getIntervalsBySize(start, size, end, min, max);

		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.size()).isEqualTo(1 + 8 + 1);
	}
}
