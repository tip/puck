package org.tip.puckgui.views.geographyEditor;

import java.awt.Component;
import java.io.File;
import java.util.ResourceBundle;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.util.ToolBox;
import org.tip.puckgui.util.GenericFileFilter;

/**
 * This class is based on the dialog type so it must set at first.
 * 
 * http://stackoverflow.com/questions/596429/adjust-selected-file-to-filefilter-
 * in-a-jfilechooser
 * 
 * @author cpm
 * 
 */
public class GeographyImportFileSelector extends JFileChooser {

	private static final long serialVersionUID = 2154330996732219526L;
	private static final Logger logger = LoggerFactory.getLogger(GeographyImportFileSelector.class);

	/**
	 * 
	 */
	public GeographyImportFileSelector(final File targetFile) {
		super();

		//
		setSelectedFile(targetFile);
		setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.saveAsFileChooser.text"));
		setFileSelectionMode(JFileChooser.FILES_ONLY);
		setAcceptAllFileFilterUsed(false);
		setApproveButtonText("Load");
		setDialogType(CUSTOM_DIALOG);

		//
		GenericFileFilter defaultFileFilter = new GenericFileFilter("Geographic files (*.csv, *.ods, *.txt, *.xls)", "csv", "ods", "txt", "xls");
		addChoosableFileFilter(defaultFileFilter);
		addChoosableFileFilter(new GenericFileFilter("Geographic OpenDocumentFormat Spreadsheet (*.ods)", "ods"));
		addChoosableFileFilter(new GenericFileFilter("Geographic Text (*.txt, *.csv)", "txt", ".csv"));
		addChoosableFileFilter(new GenericFileFilter("Geographic Microsoft Excel (*.xls)", "xls"));
		setFileFilter(defaultFileFilter);
	}

	/**
	 * 
	 */
	@Override
	public void approveSelection() {
		//
		File targetFile = getSelectedFile();

		logger.debug("getCurrentDirectory(): {}", getCurrentDirectory());
		logger.debug("selectedFile={}", targetFile);

		if (StringUtils.isBlank(ToolBox.getExtension(targetFile))) {
			targetFile = new File(targetFile.getAbsolutePath() + ".txt");
		}

		//
		if (!StringUtils.endsWithAny(ToolBox.getExtension(targetFile), "csv", "ods", "txt", "xls")) {
			//
			targetFile = new File(targetFile.getAbsolutePath() + ".geo.csv");
			setSelectedFile(targetFile);
		}

		if (!targetFile.exists()) {
			//
			String title = "Import geography file error";
			String message = "Please, select an exiting file.";

			//
			JOptionPane.showMessageDialog(this, message, title, JOptionPane.ERROR_MESSAGE);

		} else {
			//
			super.approveSelection();
		}
	}

	/**
	 * 
	 */
	@Override
	public void cancelSelection() {
		//
		logger.debug("Cancel selection.");
		super.cancelSelection();
	}

	/**
	 * 
	 */
	@Override
	public void setSelectedFile(final File file) {
		//
		super.setSelectedFile(file);
	}

	/**
	 * 
	 */
	@Override
	public void setVisible(final boolean visible) {
		//
		super.setVisible(visible);

		if (!visible) {
			//
			resetChoosableFileFilters();
		}
	}

	/**
	 * This method is the main one of the selector.
	 * 
	 * @param parent
	 * @return
	 */
	public static File showSelectorDialog(final Component parent, final File targetFile) {
		File result;

		//
		GeographyImportFileSelector selector = new GeographyImportFileSelector(targetFile);

		//
		if (selector.showDialog(parent, null) == JFileChooser.APPROVE_OPTION) {
			//
			logger.debug("getCurrentDirectory(): {}", selector.getCurrentDirectory());
			logger.debug("getSelectedFile() : {}", selector.getSelectedFile());
			result = selector.getSelectedFile();

		} else {
			//
			result = null;
		}

		//
		return result;
	}
}
