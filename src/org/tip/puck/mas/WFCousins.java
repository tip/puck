package org.tip.puck.mas;

public class WFCousins implements WeightFactor {
    private double wgtCousin1;
    private double wgtCousin2;
    
    public WFCousins(double wgtCousin1, double wgtCousin2) {
        this.wgtCousin1 = wgtCousin1;
        this.wgtCousin2 = wgtCousin2;
    }
    
    public String factorName(){
    	return "cousins";
    }
    
    @Override
    public double factor(Agent mother, Agent father) {
        if (father.hasCousin(mother, 1)) {
            return wgtCousin1;
        }
        else if (father.hasCousin(mother, 2)) {
            return wgtCousin2;
        }
        else {
            return 1.0;
        }
    }
    
    @Override
    public boolean appliesToMarriage() {return false;}
    
    @Override
    public boolean appliesToDivorce() {return false;}
}