package org.tip.puck.partitions.graphs;

import org.tip.puck.util.MathUtils;

/**
 * 
 * @author TIP
 */
public class VirtualFieldworkVariationsCriteria {

	private boolean numberOfLoopsChecked;
	private boolean numberOfCircuitsChecked;
	private boolean numberOfTrianglesChecked;
	private boolean concentrationChecked;
	private boolean strengthConcentrationChecked;
	private boolean weightDistributionChecked;
	private boolean strengthDistributionChecked;
	private boolean symmetryChecked;

	private int numberOfNodes;
	private int arcWeightSum;
	private double fractionOfNodes;
	private double arcWeightFraction;
	private double outPreference;
	private int numberOfRuns;
	private int firstVariableIndex;
	private double firstVariableInitialValue;

	private int firstVariableIntervalFactor;
	private int firstVariableNumberOfIntervals;
	private int secondVariableIndex;
	private double secondVariableInitialValue;

	private int secondVariableIntervalFactor;
	private int secondVariableNumberOfIntervals;

	/**
	 * 
	 */
	public VirtualFieldworkVariationsCriteria() {
		this.numberOfLoopsChecked = false;
		this.numberOfCircuitsChecked = false;
		this.numberOfTrianglesChecked = false;
		this.concentrationChecked = false;
		this.weightDistributionChecked = false;
		this.strengthDistributionChecked = false;
		this.symmetryChecked = false;

		this.fractionOfNodes = 1.0;
		this.arcWeightFraction = 0.1;
		this.outPreference = 0.5;
		this.numberOfRuns = 100;

		this.firstVariableIndex = 0;
		this.firstVariableInitialValue = 0.00001;
		this.firstVariableIntervalFactor = 10;
		this.firstVariableNumberOfIntervals = 5;

		this.secondVariableIndex = 1;
		this.secondVariableInitialValue = 0.00001;
		this.secondVariableIntervalFactor = 10;
		this.secondVariableNumberOfIntervals = 5;
	}

	public int getFirstVariableIndex() {
		return firstVariableIndex;
	}

	public double getFirstVariableInitialValue() {
		return firstVariableInitialValue;
	}

	public int getFirstVariableIntervalFactor() {
		return firstVariableIntervalFactor;
	}

	public int getFirstVariableNumberOfIntervals() {
		return firstVariableNumberOfIntervals;
	}

	public double getArcWeightFraction() {
		return arcWeightFraction;
	}

	public double getFractionOfNodes() {
		return fractionOfNodes;
	}

	public int getArcWeightSum() {
		return arcWeightSum;
	}

	public int getNumberOfNodes() {
		return numberOfNodes;
	}

	public int getNumberOfRuns() {
		return numberOfRuns;
	}

	public double getOutPreference() {
		return outPreference;
	}

	public int getSecondVariableIndex() {
		return secondVariableIndex;
	}

	public double getSecondVariableInitialValue() {
		return secondVariableInitialValue;
	}

	public int getSecondVariableIntervalFactor() {
		return secondVariableIntervalFactor;
	}

	public int getSecondVariableNumberOfIntervals() {
		return secondVariableNumberOfIntervals;
	}

	public boolean isConcentrationChecked() {
		return concentrationChecked;
	}

	public boolean isNumberOfCircuitsChecked() {
		return numberOfCircuitsChecked;
	}

	public boolean isNumberOfLoopsChecked() {
		return numberOfLoopsChecked;
	}

	public boolean isNumberOfTrianglesChecked() {
		return numberOfTrianglesChecked;
	}

	public boolean isStrengthDistributionChecked() {
		return strengthDistributionChecked;
	}

	public boolean isSymmetryChecked() {
		return symmetryChecked;
	}

	public boolean isWeightDistributionChecked() {
		return weightDistributionChecked;
	}

	/**
	 * 
	 * @param sourceArcWeightSum
	 */
	public void nominalizeValues(final int sourceNodeCount, final double sourceArcWeightSum) {
		this.numberOfNodes = (int) MathUtils.round(this.fractionOfNodes * sourceNodeCount, 0);
		this.arcWeightSum = (int) MathUtils.round(this.arcWeightFraction * sourceArcWeightSum, 0);
		// this.firstVariableInitialValue = new Double((this.numberOfNodes - 1))
		// / Math.pow(this.firstVariableIntervalFactor,
		// this.firstVariableNumberOfIntervals);
		// this.secondVariableInitialValue = new Double((this.numberOfNodes -
		// 1))
		// / Math.pow(this.secondVariableIntervalFactor,
		// this.secondVariableNumberOfIntervals);
		this.firstVariableInitialValue = 1. / Math.pow(this.firstVariableIntervalFactor, this.firstVariableNumberOfIntervals);
		this.secondVariableInitialValue = 1. / Math.pow(this.secondVariableIntervalFactor, this.secondVariableNumberOfIntervals);
	}

	public void setConcentrationChecked(final boolean concentrationChecked) {
		this.concentrationChecked = concentrationChecked;
	}

	public void setFirstVariableIndex(final int firstVariableIndex) {
		this.firstVariableIndex = firstVariableIndex;
	}

	public void setFirstVariableInitialValue(final double firstVariableInitialValue) {
		this.firstVariableInitialValue = firstVariableInitialValue;
	}

	public void setFirstVariableIntervalFactor(final int firstVariableIntervalFactor) {
		this.firstVariableIntervalFactor = firstVariableIntervalFactor;
	}

	public void setFirstVariableNumberOfIntervals(final int firstVariableNumberOfIntervals) {
		this.firstVariableNumberOfIntervals = firstVariableNumberOfIntervals;
	}

	public void setFractionOfArcs(final double fractionOfArcs) {
		this.arcWeightFraction = fractionOfArcs;
	}

	public void setFractionOfNodes(final double fractionOfNodes) {
		this.fractionOfNodes = fractionOfNodes;
	}

	public void setNumberOfCircuitsChecked(final boolean numberOfCircuitsChecked) {
		this.numberOfCircuitsChecked = numberOfCircuitsChecked;
	}

	public void setNumberOfLoopsChecked(final boolean numberOfLoopsChecked) {
		this.numberOfLoopsChecked = numberOfLoopsChecked;
	}

	public void setNumberOfRuns(final int numberOfRuns) {
		this.numberOfRuns = numberOfRuns;
	}

	public void setNumberOfTrianglesChecked(final boolean numberOfTrianglesChecked) {
		this.numberOfTrianglesChecked = numberOfTrianglesChecked;
	}

	public void setOutPreference(final double outs) {
		this.outPreference = outs;
	}

	public void setSecondVariableIndex(final int secondVariableIndex) {
		this.secondVariableIndex = secondVariableIndex;
	}

	public void setSecondVariableInitialValue(final double secondVariableInitialValue) {
		this.secondVariableInitialValue = secondVariableInitialValue;
	}

	public void setSecondVariableIntervalFactor(final int secondVariableIntervalFactor) {
		this.secondVariableIntervalFactor = secondVariableIntervalFactor;
	}

	public void setSecondVariableNumberOfIntervals(final int secondVariableNumberOfIntervals) {
		this.secondVariableNumberOfIntervals = secondVariableNumberOfIntervals;
	}

	public void setStrengthDistributionChecked(final boolean strengthDistributionChecked) {
		this.strengthDistributionChecked = strengthDistributionChecked;
	}

	public void setSymmetryChecked(final boolean symmetryChecked) {
		this.symmetryChecked = symmetryChecked;
	}

	public void setWeightDistributionChecked(final boolean weightDistributionChecked) {
		this.weightDistributionChecked = weightDistributionChecked;
	}

	public boolean isStrengthConcentrationChecked() {
		return strengthConcentrationChecked;
	}

	public void setStrengthConcentrationChecked(boolean strengthConcentrationChecked) {
		this.strengthConcentrationChecked = strengthConcentrationChecked;
	}
}
