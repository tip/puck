package oldcore.visualization;


public class PlotOptions {
	
	public static enum HeritageType {agnatic, uterine, cognatic};
	private HeritageType ht = HeritageType.cognatic;
	public void setHeritageType(HeritageType ht){this.ht = ht;}
	public HeritageType getHeritageType(){return ht;}
	
	private boolean equalizeGenerations = true;
	public void setEqualizeGenerations(boolean eq){this.equalizeGenerations = eq;}
	public boolean getEqualizeGenerations(){return equalizeGenerations;}
	
	private boolean parentalTiesCurved = false;
	public void setParentalTiesCurved(boolean c){this.parentalTiesCurved = c;}
	public boolean getParentalTiesCurved(){return parentalTiesCurved;}
	
	private boolean conjugalTiesCurved = false;
	public void setConjugalTiesCurved(boolean c){this.conjugalTiesCurved = c;}
	public boolean getConjugalTiesCurved(){return conjugalTiesCurved;}
	
	private boolean showNodeLabels = true;
	public void setShowNodeLabels(boolean s){this.showNodeLabels = s;}
	public boolean getShowNodeLabels(){return showNodeLabels;}
	
	private int nbrIterVertOrdering = 500;
	public void setNbrIterVertOrdering(int n){this.nbrIterVertOrdering = n;}
	public int getNbrIterVertOrdering(){return nbrIterVertOrdering;}
	
	
	private PlotOptions(){}
	
	public static PlotOptions getInstance(){
		if(ref == null) ref = new PlotOptions();
		return ref;
	}
	
	private static PlotOptions ref = null;
}
