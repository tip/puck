package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellEditor;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.net.Attributes;
import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.UnionStatus;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.workers.AttributeWorker;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.util.NumberablesHashMap.IdStrategy;
import org.tip.puck.util.ToolBox;
import org.tip.puckgui.InputSettings.CheckLevel;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.PuckGUI;
import org.tip.puckgui.models.IndividualChildrenModel;
import org.tip.puckgui.models.IndividualRelationsModel;
import org.tip.puckgui.models.IndividualSpousesModel;
import org.tip.puckgui.models.IndividualsCellRenderer;
import org.tip.puckgui.models.IndividualsModel;
import org.tip.puckgui.util.AutoComboBox;
import org.tip.puckgui.util.ComboBoxIds;
import org.tip.puckgui.views.kinoath.IndividualDiagramsPanel;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.StringSet;

/**
 * 
 * @author TIP
 */
public class IndividualsPanel extends JPanel {

	private static final long serialVersionUID = 2895878993462787728L;

	private static final Logger logger = LoggerFactory.getLogger(IndividualsPanel.class);

	int reportCounter = 0;
	private StringSet attributeTemplates;
	private JPanel thisJPanel;
	private JTextField txtfldSearchIndividual;
	private NetGUI netGUI;
	private UnionStatus defaultUnionStatus = UnionStatus.MARRIED;
	private JTable tableIndividualSpouses;
	private JTable tableIndividualChildren;
	private JLabel lblIndividualId;
	private JLabel lblIndividualGender;
	JLabel lblIndividualParent1Id;
	JLabel lblIndividualParent2Id;
	JLabel lblIndividualParent1Name;
	JLabel lblIndividualParent2Name;
	JList individualList;
	JScrollPane individualsScrollPane;
	private static ImageIcon mediumFemaleIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/female-32x32.png"));
	private static ImageIcon mediumMaleIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/male-32x32.png"));
	private static ImageIcon mediumUnknowIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/unknown-32x32.png"));
	private static ImageIcon smallUnmarriedIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/unmarried-x16.png"));
	private static ImageIcon smallMarriedIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/married-x16.png"));
	private static ImageIcon smallDivorcedIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/divorced-x16.png"));
	private static ImageIcon mediumUnmarriedIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/unmarried-x23.png"));
	private static ImageIcon mediumMarriedIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/married-x23.png"));
	private static ImageIcon mediumDivorcedIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/divorced-x23.png"));
	private static ImageIcon largeFemaleIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/female-64x64.png"));
	private static ImageIcon largeMaleIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/male-64x64.png"));
	private static ImageIcon largeUnknowIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/unknown-64x64.png"));
	private static ImageIcon insertIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/insert-x16.png"));
	private static ImageIcon appendIcon = new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/append-x16.png"));
	private JLabel lblIndividualSpouses;
	private JLabel lblChildren;
	private JLabel lblFamilyId;
	private JLabel lblFamilyStatus;
	private JLabel lblPosition;
	private JTable tableIndividualRelations;
	private JLabel lblRelations;
	private JLabel lblIndividualFirstName;
	private AutoComboBox cmbbxIndividualFirstName;
	private AutoComboBox cmbbxIndividualLastName;
	private AttributesPanel attributesPanel;
	private JLabel lblIndividualLastName;
	private JLabel lblFirstLastNameSeparator;
	private AutoComboBox cmbbxIndividualParent1;
	private AutoComboBox cmbbxIndividualParent2;
	private AutoComboBox cmbbxIndividualChildrenIds;
	private AutoComboBox cmbbxIndividualSpousesIds;
	private AutoComboBox cmbbxIndividualSpousesOrders;
	private AutoComboBox cmbbxIndividualChildrenOrders;
	private JButton btnAddPartner;
	private JButton btnAddChild;
	private JTextField txtFldIndividualId;
	private boolean doLostFocus = true;
	private JToggleButton tglbtnIndividualIdStrategy;
	private JToggleButton tglbtnFamilyIdStrategy;
	private JButton btnDefaultUnionStatus;

	/**
	 * Initialize the contents of the frame.
	 */
	public IndividualsPanel(final NetGUI guiManager) {

		//
		this.thisJPanel = this;
		this.netGUI = guiManager;
		this.attributeTemplates = new StringSet();
		updateAttributeTemplates();

		//
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		JSplitPane individualsSplit = new JSplitPane();
		add(individualsSplit);
		individualsSplit.setAlignmentX(Component.CENTER_ALIGNMENT);

		JPanel individualsPanel = new JPanel();
		individualsPanel.setMinimumSize(new Dimension(200, 10));
		individualsSplit.setLeftComponent(individualsPanel);
		individualsPanel.setLayout(new BoxLayout(individualsPanel, BoxLayout.Y_AXIS));

		this.individualsScrollPane = new JScrollPane();
		this.individualsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		individualsPanel.add(this.individualsScrollPane);

		this.individualList = new JList();
		this.individualList.setDoubleBuffered(true);
		this.individualList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(final ListSelectionEvent event) {
				logger.debug("event = " + event.getValueIsAdjusting() + " " + event.getFirstIndex() + " " + event.getLastIndex() + " "
						+ IndividualsPanel.this.individualList.getSelectedIndex());

				if (!event.getValueIsAdjusting()) {
					// Selected.
					if (IndividualsPanel.this.individualList.getSelectedIndex() != -1) {
						Individual individual = (Individual) ((JList) event.getSource()).getModel().getElementAt(
								IndividualsPanel.this.individualList.getSelectedIndex());
						updateIndividualIdentity(individual);
					}
				}
			}
		});
		this.individualList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		this.individualList.setCellRenderer(new IndividualsCellRenderer());
		this.individualList.setModel(new IndividualsModel(null));
		this.individualsScrollPane.setViewportView(this.individualList);

		JPopupMenu popupMenu_6 = new JPopupMenu();
		addPopup(this.individualList, popupMenu_6);

		JMenuItem mntmIndividualDiagram = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.mntmIndividualDiagram.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmIndividualDiagram.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Draw individual diagram.
				Individual currentIndividual = getSelectedIndividual();
				JPanel diagramPanel = new IndividualDiagramsPanel(IndividualsPanel.this.netGUI);
				((IndividualDiagramsPanel) diagramPanel).update(currentIndividual);
				IndividualsPanel.this.netGUI.addReportTab("Ind. diagrams", diagramPanel);
			}
		});
		popupMenu_6.add(mntmIndividualDiagram);

		JPanel individualPanel = new JPanel();
		individualsSplit.setRightComponent(individualPanel);
		individualPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		individualPanel.setAlignmentY(Component.TOP_ALIGNMENT);
		individualPanel.setLayout(new BoxLayout(individualPanel, BoxLayout.Y_AXIS));

		JPanel panelIndividualIdentity = new JPanel();
		individualPanel.add(panelIndividualIdentity);
		panelIndividualIdentity.setLayout(new BoxLayout(panelIndividualIdentity, BoxLayout.X_AXIS));

		Component horizontalStrut = Box.createHorizontalStrut(20);
		panelIndividualIdentity.add(horizontalStrut);

		JPanel panel_2 = new JPanel();
		panelIndividualIdentity.add(panel_2);
		panel_2.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, }));

		JPanel panel = new JPanel();
		panel_2.add(panel, "2, 2");
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		Component verticalStrut_9 = Box.createVerticalStrut(20);
		verticalStrut_9.setMaximumSize(new Dimension(20, 20));
		panel.add(verticalStrut_9);

		this.lblIndividualId = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.label.text_2"));
		this.lblIndividualId.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				//
				logger.debug("Individual id clicked");
				if (event.getButton() == MouseEvent.BUTTON1) {
					//
					setIndividualIdEditorOn();
				}
			}
		});

		this.lblIndividualId.setAlignmentX(Component.RIGHT_ALIGNMENT);
		panel.add(this.lblIndividualId);

		this.txtFldIndividualId = new JTextField();
		this.txtFldIndividualId.setFocusTraversalKeysEnabled(false);
		this.txtFldIndividualId.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent event) {
				// INDIVIDUAL ID KEYPRESSED.
				try {
					if (event.getKeyCode() == KeyEvent.VK_ESCAPE) {
						logger.debug("escape pressed");
						IndividualsPanel.this.txtFldIndividualId.transferFocus();
					} else if (event.getKeyCode() == KeyEvent.VK_ENTER) {
						logger.debug("enter pressed");
						save();
						IndividualsPanel.this.txtFldIndividualId.transferFocus();
					} else if (event.getKeyCode() == KeyEvent.VK_TAB) {
						if (event.isShiftDown()) {
							logger.debug("shift-tab pressed");
							save();
							IndividualsPanel.this.txtFldIndividualId.transferFocus();
						} else {
							logger.debug("tab pressed");
							save();
							IndividualsPanel.this.txtFldIndividualId.transferFocus();
							setFirstNameEditorOn();
						}
					}
				} catch (PuckException exception) {
					// Save failed.
					IndividualsPanel.this.txtFldIndividualId.requestFocus();
				}
			}

			public void save() throws PuckException {
				//
				String input = IndividualsPanel.this.txtFldIndividualId.getText();
				logger.debug("[lbl,cmb]=[" + IndividualsPanel.this.lblIndividualId.getText() + "," + input + "]");

				if ((StringUtils.isNotBlank(input)) && (!StringUtils.equals(IndividualsPanel.this.lblIndividualId.getText(), input))) {
					input = input.trim();
					if (NumberUtils.isDigits(input)) {
						//
						int targetId = Integer.parseInt(input);

						Individual individual = IndividualsPanel.this.netGUI.getNet().individuals().getById(targetId);
						if (individual == null) {
							//
							Individual currentIndividual = getSelectedIndividual();
							IndividualsPanel.this.netGUI.getNet().changeId(currentIndividual, targetId);
							IndividualsPanel.this.netGUI.setChanged(true);

							//
							IndividualsPanel.this.lblIndividualId.setVisible(true);
							IndividualsPanel.this.txtFldIndividualId.setVisible(false);

							//
							IndividualsPanel.this.netGUI.updateAll();

							//
							select(currentIndividual);
						} else {
							IndividualsPanel.this.doLostFocus = false;
							JOptionPane.showMessageDialog(IndividualsPanel.this.thisJPanel, "ID already in use.", "Invalid id", JOptionPane.ERROR_MESSAGE);
							IndividualsPanel.this.doLostFocus = true;
							throw PuckExceptions.INVALID_PARAMETER.create();
						}
					} else {
						IndividualsPanel.this.doLostFocus = false;
						JOptionPane.showMessageDialog(IndividualsPanel.this.thisJPanel, "Numeric value required.", "Invalid id", JOptionPane.ERROR_MESSAGE);
						IndividualsPanel.this.doLostFocus = true;
						throw PuckExceptions.INVALID_PARAMETER.create();
					}
				}
			}
		});

		this.txtFldIndividualId.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(final FocusEvent e) {
				logger.debug("FOCUS LOST INDIVIDUAL ID");

				//
				if (IndividualsPanel.this.doLostFocus) {
					IndividualsPanel.this.lblIndividualId.setVisible(true);
					IndividualsPanel.this.txtFldIndividualId.setVisible(false);
				}
			}
		});
		panel.add(this.txtFldIndividualId);
		this.txtFldIndividualId.setColumns(10);

		Component verticalStrut_8 = Box.createVerticalStrut(10);
		verticalStrut_8.setMaximumSize(new Dimension(20, 10));
		panel.add(verticalStrut_8);

		this.lblPosition = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.lblNewLabel.text")); //$NON-NLS-1$ //$NON-NLS-2$
		this.lblPosition.setAlignmentX(Component.RIGHT_ALIGNMENT);
		panel.add(this.lblPosition);

		this.lblIndividualGender = new JLabel("");
		panel_2.add(this.lblIndividualGender, "4, 2");
		this.lblIndividualGender.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Click gender.
				if (event.getButton() == MouseEvent.BUTTON1) {
					IndividualsPanel.this.lblIndividualGender.requestFocus();
					Individual currentIndividual = getSelectedIndividual();
					if (currentIndividual != null) {
						//
						switch (currentIndividual.getGender()) {
							case FEMALE:
								currentIndividual.setGender(Gender.MALE);
							break;
							case MALE:
								currentIndividual.setGender(Gender.UNKNOWN);
							break;
							case UNKNOWN:
								currentIndividual.setGender(Gender.FEMALE);
							break;
						}

						//
						updateIndividualIdentity(currentIndividual);

						//
						IndividualsPanel.this.netGUI.setChanged(true);
					}
				}
			}
		});
		this.lblIndividualGender.setFont(new Font("Dialog", Font.PLAIN, 24));
		this.lblIndividualGender.setIcon(new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/female-64x64.png")));

		this.lblIndividualFirstName = new JLabel("X");
		this.lblIndividualFirstName.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Individual first name clicked.
				logger.debug("First Name clicked");
				if (event.getButton() == MouseEvent.BUTTON1) {
					//
					setFirstNameEditorOn();
				}
			}
		});
		panel_2.add(this.lblIndividualFirstName, "6, 2");
		this.lblIndividualFirstName.setFont(new Font("Dialog", Font.PLAIN, 24));

		this.cmbbxIndividualFirstName = new AutoComboBox(null);
		this.cmbbxIndividualFirstName.setVisible(false);
		this.cmbbxIndividualFirstName.setStrict(false);
		this.cmbbxIndividualFirstName.setMaximumRowCount(10);
		this.cmbbxIndividualFirstName.getEditor().getEditorComponent().setFocusTraversalKeysEnabled(false);

		// Custom manually.
		this.cmbbxIndividualFirstName.getEditor().getEditorComponent().addFocusListener(new FocusListener() {
			@Override
			public void focusGained(final FocusEvent arg0) {
				logger.debug("FOCUS GAINED FIRST NAME");
			}

			@Override
			public void focusLost(final FocusEvent arg0) {
				logger.debug("FOCUS LOST FIRST NAME");

				//
				IndividualsPanel.this.cmbbxIndividualFirstName.setVisible(false);
				IndividualsPanel.this.lblIndividualFirstName.setVisible(true);

				// Clean.
				IndividualsPanel.this.cmbbxIndividualFirstName.setDataList(new ArrayList<String>());
			}
		});

		this.cmbbxIndividualFirstName.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent event) {
				// Cancel.
				if (event.getKeyCode() == KeyEvent.VK_ESCAPE) {
					logger.debug("escape pressed");

					if (StringUtils.isBlank(IndividualsPanel.this.lblIndividualFirstName.getText())) {
						IndividualsPanel.this.cmbbxIndividualFirstName.getEditor().setItem("?");
						save();
					}
					IndividualsPanel.this.cmbbxIndividualFirstName.getEditor().getEditorComponent().transferFocus();
				} else if (event.getKeyCode() == KeyEvent.VK_ENTER) {
					logger.debug("enter pressed");
					save();
					IndividualsPanel.this.cmbbxIndividualFirstName.getEditor().getEditorComponent().transferFocus();
				} else if (event.getKeyCode() == KeyEvent.VK_TAB) {
					if (event.isShiftDown()) {
						logger.debug("shift-tab pressed");
						save();
						IndividualsPanel.this.cmbbxIndividualFirstName.getEditor().getEditorComponent().transferFocus();
						setIndividualIdEditorOn();
					} else {
						logger.debug("tab pressed");
						save();
						IndividualsPanel.this.cmbbxIndividualFirstName.getEditor().getEditorComponent().transferFocus();
						setLastNameEditorOn();
					}
				}
			}

			public void save() {
				String input = (String) IndividualsPanel.this.cmbbxIndividualFirstName.getEditor().getItem();
				logger.debug("[lbl,cmb]=[" + IndividualsPanel.this.lblIndividualFirstName.getText() + "," + input + "]");

				if (!StringUtils.equals(IndividualsPanel.this.lblIndividualFirstName.getText(), input)) {
					//
					Individual individual = getIdentityIndividual();
					if (individual != null) {
						if (StringUtils.isBlank(input)) {
							IndividualsPanel.this.lblIndividualFirstName.setText("?");
							individual.setFirstName("?");
						} else {
							IndividualsPanel.this.lblIndividualFirstName.setText(input);
							individual.setFirstName(input);
						}

						//
						IndividualsPanel.this.netGUI.setChanged(true);
						IndividualsPanel.this.netGUI.updateAll();
					}
				}
			}
		});

		panel_2.add(this.cmbbxIndividualFirstName, "7, 2");

		this.lblFirstLastNameSeparator = new JLabel("/");
		this.lblFirstLastNameSeparator.setFont(new Font("Dialog", Font.PLAIN, 24));
		panel_2.add(this.lblFirstLastNameSeparator, "9, 2");

		this.lblIndividualLastName = new JLabel("Y");
		this.lblIndividualLastName.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Individual last name clicked.
				if (event.getButton() == MouseEvent.BUTTON1) {
					logger.debug("Last Name clicked");
					setLastNameEditorOn();
				}
			}
		});
		this.lblIndividualLastName.setFont(new Font("Dialog", Font.PLAIN, 24));
		panel_2.add(this.lblIndividualLastName, "11, 2, right, default");

		this.cmbbxIndividualLastName = new AutoComboBox(null);
		this.cmbbxIndividualLastName.setVisible(false);
		this.cmbbxIndividualLastName.setStrict(false);
		this.cmbbxIndividualLastName.setMaximumRowCount(10);
		this.cmbbxIndividualLastName.getEditor().getEditorComponent().setFocusTraversalKeysEnabled(false);

		// Custom manually.
		this.cmbbxIndividualLastName.getEditor().getEditorComponent().addFocusListener(new FocusListener() {
			@Override
			public void focusGained(final FocusEvent arg0) {
				logger.debug("FOCUS GAINED LAST NAME");
			}

			@Override
			public void focusLost(final FocusEvent arg0) {
				logger.debug("FOCUS LOST LAST NAME");

				//
				IndividualsPanel.this.cmbbxIndividualLastName.setVisible(false);
				IndividualsPanel.this.lblIndividualLastName.setVisible(true);

				// Clean.
				IndividualsPanel.this.cmbbxIndividualLastName.setDataList(new ArrayList<String>());
			}
		});

		this.cmbbxIndividualLastName.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent event) {
				// Cancel.
				if (event.getKeyCode() == KeyEvent.VK_ESCAPE) {
					logger.debug("escape pressed");

					IndividualsPanel.this.cmbbxIndividualLastName.getEditor().getEditorComponent().transferFocus();
				} else if (event.getKeyCode() == KeyEvent.VK_ENTER) {
					logger.debug("enter pressed");
					save();
					IndividualsPanel.this.cmbbxIndividualLastName.getEditor().getEditorComponent().transferFocus();
				} else if (event.getKeyCode() == KeyEvent.VK_TAB) {
					if (event.isShiftDown()) {
						logger.debug("shift-tab pressed");
						save();
						setFirstNameEditorOn();
					} else {
						logger.debug("tab pressed");
						save();
						IndividualsPanel.this.cmbbxIndividualLastName.getEditor().getEditorComponent().transferFocus();
						setParent1IdEditorOn();
					}
				}
			}

			public void save() {
				String input = (String) IndividualsPanel.this.cmbbxIndividualLastName.getEditor().getItem();
				logger.debug("[lbl,cmb]=[" + IndividualsPanel.this.lblIndividualLastName.getText() + "," + input + "]");

				if (!StringUtils.equals(input, IndividualsPanel.this.lblIndividualLastName.getText())) {
					if (NumberUtils.isDigits(IndividualsPanel.this.lblIndividualId.getText())) {
						Individual individual = IndividualsPanel.this.netGUI.getNet().individuals()
								.getById(Integer.parseInt(IndividualsPanel.this.lblIndividualId.getText()));
						individual.setLastName(input);
						IndividualsPanel.this.netGUI.setChanged(true);
						IndividualsPanel.this.netGUI.updateAll();
					}
				}
			}
		});

		panel_2.add(this.cmbbxIndividualLastName, "12, 2, fill, default");

		Component horizontalGlue_2 = Box.createHorizontalGlue();
		panelIndividualIdentity.add(horizontalGlue_2);

		JPanel panel_12 = new JPanel();
		panelIndividualIdentity.add(panel_12);
		panel_12.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.GLUE_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.DEFAULT_COLSPEC, ColumnSpec.decode("max(100dlu;default)"), }, new RowSpec[] { RowSpec.decode("8dlu"),
				FormFactory.DEFAULT_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, RowSpec.decode("1dlu"), }));

		this.cmbbxIndividualParent1 = new AutoComboBox(null);
		this.cmbbxIndividualParent1.setStrict(false);
		this.cmbbxIndividualParent1.setVisible(false);
		this.cmbbxIndividualParent1.setMaximumRowCount(10);
		this.cmbbxIndividualParent1.getEditor().getEditorComponent().setFocusTraversalKeysEnabled(false);

		// Custom manually.
		this.cmbbxIndividualParent1.getEditor().getEditorComponent().addFocusListener(new FocusListener() {
			@Override
			public void focusGained(final FocusEvent arg0) {
				logger.debug("FOCUS GAINED PARENT 1 ID");
			}

			@Override
			public void focusLost(final FocusEvent arg0) {
				logger.debug("FOCUS LOST PARENT 1 ID");

				if (IndividualsPanel.this.doLostFocus) {
					logger.debug("unfocus parent 1 ID");
					//
					IndividualsPanel.this.cmbbxIndividualParent1.setVisible(false);
					IndividualsPanel.this.lblIndividualParent1Id.setVisible(true);
					IndividualsPanel.this.lblIndividualParent1Name.setVisible(true);

					// Clean.
					IndividualsPanel.this.cmbbxIndividualParent1.setDataList(new ArrayList<String>());
				}
			}
		});

		this.cmbbxIndividualParent1.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent event) {
				try {
					// Cancel.
					if (event.getKeyCode() == KeyEvent.VK_ESCAPE) {
						logger.debug("escape pressed");

						IndividualsPanel.this.cmbbxIndividualParent1.getEditor().getEditorComponent().transferFocus();
					} else if (event.getKeyCode() == KeyEvent.VK_ENTER) {
						logger.debug("enter pressed");
						save();
						IndividualsPanel.this.cmbbxIndividualParent1.getEditor().getEditorComponent().transferFocus();
					} else if (event.getKeyCode() == KeyEvent.VK_TAB) {
						if (event.isShiftDown()) {
							logger.debug("shift-tab pressed");
							save();
							IndividualsPanel.this.cmbbxIndividualParent1.getEditor().getEditorComponent().transferFocus();
							setLastNameEditorOn();
						} else {
							logger.debug("tab pressed");
							save();
							IndividualsPanel.this.cmbbxIndividualParent1.getEditor().getEditorComponent().transferFocus();
							setParent2IdEditorOn();
						}
					}
				} catch (PuckException exception) {
					// In case of failed save.
					logger.debug("SAVE FAILED");
					IndividualsPanel.this.cmbbxIndividualParent1.getEditor().getEditorComponent().requestFocus();
				}
			}

			public void save() throws PuckException {
				String input = (String) IndividualsPanel.this.cmbbxIndividualParent1.getEditor().getItem();
				logger.debug("[lbl,cmb]=[" + IndividualsPanel.this.lblIndividualParent1Id.getText() + "," + input + "]");

				// In case of new partner created with id lesser than
				// the current individual, the current index
				// value becomes wrong. So we need to save the
				// previous one.
				Individual currentIndividual = IndividualsPanel.this.netGUI.selectedIndividual();

				//
				Individual newFather = ComboBoxIds.getOrCreateIndividualFromInput(IndividualsPanel.this.netGUI.getIndividualIdStrategy(),
						IndividualsPanel.this.netGUI.getNet(), input, Gender.MALE);

				if (newFather != currentIndividual.getFather()) {
					// Make controls.
					Individual ego = getIdentityIndividual();

					// Check undefinedRoles.
					String errorMessage = controlPartners(CheckLevel.ERROR, newFather, ego.getMother(), ego);
					if (errorMessage != null) {
						IndividualsPanel.this.doLostFocus = false;
						JOptionPane.showMessageDialog(IndividualsPanel.this.thisJPanel, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
						IndividualsPanel.this.doLostFocus = true;
						throw PuckExceptions.INVALID_PARAMETER.create();
					} else {
						//
						NetUtils.setKinFather(IndividualsPanel.this.netGUI.getNet(), newFather, ego);
						if (ego.getOriginFamily().numberOfParents() == 2) {
							ego.getOriginFamily().setUnionStatus(getDefaultUnionStatus());
						}
						IndividualsPanel.this.netGUI.updateAll();

						// In case of new partner created with id lesser than
						// the
						// current individual, the current index value becomes
						// wrong. So we need to set the previous one.
						IndividualsPanel.this.netGUI.selectIndividualsTab(currentIndividual);

						// Check warning.
						String warningMessage = controlPartners(CheckLevel.WARNING, newFather, ego.getMother(), ego);
						if (warningMessage != null) {
							JOptionPane.showMessageDialog(IndividualsPanel.this.thisJPanel, warningMessage, "Warning", JOptionPane.WARNING_MESSAGE);
						}
					}
				}
			}
		});

		panel_12.add(this.cmbbxIndividualParent1, "5, 2, 2, 1, fill, default");

		this.cmbbxIndividualParent2 = new AutoComboBox(null);
		this.cmbbxIndividualParent2.setStrict(false);
		this.cmbbxIndividualParent2.setVisible(false);
		this.cmbbxIndividualParent2.setMaximumRowCount(10);
		this.cmbbxIndividualParent2.getEditor().getEditorComponent().setFocusTraversalKeysEnabled(false);

		// Custom manually.
		this.cmbbxIndividualParent2.getEditor().getEditorComponent().addFocusListener(new FocusListener() {
			@Override
			public void focusGained(final FocusEvent arg0) {
				logger.debug("FOCUS GAINED PARENT 2 ID");
			}

			@Override
			public void focusLost(final FocusEvent arg0) {
				logger.debug("FOCUS LOST PARENT 2 ID");

				//
				if (IndividualsPanel.this.doLostFocus) {
					IndividualsPanel.this.cmbbxIndividualParent2.setVisible(false);
					IndividualsPanel.this.lblIndividualParent2Id.setVisible(true);
					IndividualsPanel.this.lblIndividualParent2Name.setVisible(true);

					// Clean.
					IndividualsPanel.this.cmbbxIndividualParent2.setDataList(new ArrayList<String>());
				}
			}
		});

		this.cmbbxIndividualParent2.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent event) {
				try {
					//
					if (event.getKeyCode() == KeyEvent.VK_ESCAPE) {
						logger.debug("escape pressed");
						IndividualsPanel.this.cmbbxIndividualParent2.getEditor().getEditorComponent().transferFocus();
					} else if (event.getKeyCode() == KeyEvent.VK_ENTER) {
						logger.debug("enter pressed");
						save();
						IndividualsPanel.this.cmbbxIndividualParent2.getEditor().getEditorComponent().transferFocus();
					} else if (event.getKeyCode() == KeyEvent.VK_TAB) {
						if (event.isShiftDown()) {
							logger.debug("shift-tab pressed");
							save();
							IndividualsPanel.this.cmbbxIndividualParent2.getEditor().getEditorComponent().transferFocus();
							setParent1IdEditorOn();
						} else {
							logger.debug("tab pressed");
							save();
							IndividualsPanel.this.cmbbxIndividualParent2.getEditor().getEditorComponent().transferFocus();
						}
					}
				} catch (PuckException exception) {
					// In case of failed save.
					logger.debug("SAVE FAILED");
					IndividualsPanel.this.cmbbxIndividualParent2.getEditor().getEditorComponent().requestFocus();
				}
			}

			public void save() throws PuckException {
				String input = (String) IndividualsPanel.this.cmbbxIndividualParent2.getEditor().getItem();
				logger.debug("[lbl,cmb]=[" + IndividualsPanel.this.lblIndividualParent2Id.getText() + "," + input + "]");

				// In case of new partner created with id lesser than
				// the current individual, the current index
				// value becomes wrong. So we need to save the
				// previous one.
				Individual currentIndividual = IndividualsPanel.this.netGUI.selectedIndividual();

				//
				Individual newMother = ComboBoxIds.getOrCreateIndividualFromInput(IndividualsPanel.this.netGUI.getIndividualIdStrategy(),
						IndividualsPanel.this.netGUI.getNet(), input, Gender.FEMALE);

				if (newMother != currentIndividual.getMother()) {
					// Make controls.
					Individual ego = getIdentityIndividual();

					// Check undefinedRoles.
					String errorMessage = controlPartners(CheckLevel.ERROR, ego.getFather(), newMother, ego);
					if (errorMessage != null) {
						IndividualsPanel.this.doLostFocus = false;
						JOptionPane.showMessageDialog(IndividualsPanel.this.thisJPanel, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
						IndividualsPanel.this.doLostFocus = true;
						throw PuckExceptions.INVALID_PARAMETER.create();
					} else {
						//
						NetUtils.setKinMother(IndividualsPanel.this.netGUI.getNet(), newMother, getIdentityIndividual());
						if (ego.getOriginFamily().numberOfParents() == 2) {
							ego.getOriginFamily().setUnionStatus(getDefaultUnionStatus());
						}
						IndividualsPanel.this.netGUI.updateAll();

						// In case of new partner created with id lesser than
						// the
						// current individual, the current index value becomes
						// wrong. So we need to set the previous one.
						IndividualsPanel.this.netGUI.selectIndividualsTab(currentIndividual);

						// Check warning.
						String warningMessage = controlPartners(CheckLevel.WARNING, ego.getFather(), newMother, ego);
						if (warningMessage != null) {
							JOptionPane.showMessageDialog(IndividualsPanel.this.thisJPanel, warningMessage, "Warning", JOptionPane.WARNING_MESSAGE);
						}
					}
				}
			}
		});

		this.lblFamilyId = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.lblFamilyid.text"));
		this.lblFamilyId.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Family Id clicked.
				if (event.getClickCount() == 2) {
					int selectedIndex = IndividualsPanel.this.individualList.getSelectedIndex();
					if (selectedIndex != -1) {
						Individual selected = (Individual) ((IndividualsModel) IndividualsPanel.this.individualList.getModel()).getElementAt(selectedIndex);
						if (selected != null) {
							if (selected.getOriginFamily() != null) {
								IndividualsPanel.this.netGUI.selectFamiliesTab(selected.getOriginFamily());
							}
						}
					}
				}
			}
		});
		panel_12.add(this.lblFamilyId, "2, 2, 1, 4");

		this.lblFamilyStatus = new JLabel("");
		panel_12.add(this.lblFamilyStatus, "4, 2, 1, 4");
		this.lblFamilyStatus.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Family Id clicked.
				if (event.getClickCount() == 2) {
					int selectedIndex = IndividualsPanel.this.individualList.getSelectedIndex();
					if (selectedIndex != -1) {
						Individual selected = (Individual) ((IndividualsModel) IndividualsPanel.this.individualList.getModel()).getElementAt(selectedIndex);
						if (selected != null) {
							if (selected.getOriginFamily() != null) {
								IndividualsPanel.this.netGUI.selectFamiliesTab(selected.getOriginFamily());
							}
						}
					}
				}
			}
		});
		this.lblFamilyStatus.setIcon(new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/unmarried-x23.png")));

		JPopupMenu popupMenu_4 = new JPopupMenu();
		addPopup(this.lblFamilyStatus, popupMenu_4);

		JMenuItem mntmCreate = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.mntmCreate.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmCreate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Create Origin Family.
				addOriginFamily();
			}
		});

		JMenuItem mntmBrowse = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.mntmBrowse.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmBrowse.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Browse origin family.
				int selectedIndex = IndividualsPanel.this.individualList.getSelectedIndex();
				if (selectedIndex != -1) {
					//
					Individual selected = (Individual) ((IndividualsModel) IndividualsPanel.this.individualList.getModel()).getElementAt(selectedIndex);
					if (selected != null) {
						//
						if (selected.getOriginFamily() != null) {
							//
							IndividualsPanel.this.netGUI.selectFamiliesTab(selected.getOriginFamily());
						}
					}
				}
			}
		});
		popupMenu_4.add(mntmBrowse);
		mntmCreate.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, InputEvent.CTRL_MASK));
		popupMenu_4.add(mntmCreate);

		this.lblIndividualParent1Id = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.lblIndividualParent1Id.text"));
		panel_12.add(this.lblIndividualParent1Id, "5, 3");
		this.lblIndividualParent1Id.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Individual parent 1 id clicked.
				logger.debug("Parent 1 id clicked");
				if (event.getButton() == MouseEvent.BUTTON1) {
					//
					setParent1IdEditorOn();
				}
			}
		});
		this.lblIndividualParent1Id.setHorizontalAlignment(SwingConstants.RIGHT);
		this.lblIndividualParent1Id.setAlignmentX(Component.RIGHT_ALIGNMENT);

		this.lblIndividualParent1Name = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.lblFather.text"));
		panel_12.add(this.lblIndividualParent1Name, "6, 3");
		this.lblIndividualParent1Name.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				try {
					// Parent 1 clicked.
					if (event.getClickCount() == 2) {
						//
						Individual selected = getIdentityIndividual();
						if ((selected != null) && (selected.getFather() != null)) {
							//
							IndividualsPanel.this.netGUI.changeSegmentationToCluster(selected.getFather());
						}
					}
				} catch (final PuckException exception) {
					//
					exception.printStackTrace();
				}
			}
		});
		this.lblIndividualParent1Name.setIcon(new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/male-32x32.png")));

		JPopupMenu popupMenu_2 = new JPopupMenu();
		addPopup(this.lblIndividualParent1Name, popupMenu_2);

		JMenuItem mntmDelete_2 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.mntmDelete_2.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmDelete_2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Delete parent 1.
				Individual ego = getIdentityIndividual();
				if (ego != null) {
					Family family = ego.getOriginFamily();

					if ((family != null) && (family.getFather() != null)) {
						//
						NetUtils.removeFather(family);

						try {
							IndividualsPanel.this.netGUI.getSegmentation().refresh();
						} catch (PuckException exception) {
							// TODO Auto-generated catch block
							exception.printStackTrace();
						}
						IndividualsPanel.this.netGUI.updateAll();
						IndividualsPanel.this.netGUI.setChanged(true);
					}
				}
			}
		});

		JMenuItem mntmSwapParent = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.mntmSwapParent.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmSwapParent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Swap Individual parents.
				Individual currentIndividual = IndividualsPanel.this.netGUI.selectedIndividual();
				if ((currentIndividual != null) && (currentIndividual.isNotOrphan())) {
					NetUtils.swapParents(currentIndividual.getOriginFamily());
					IndividualsPanel.this.netGUI.setChanged(true);
					IndividualsPanel.this.netGUI.updateAll();
					IndividualsPanel.this.netGUI.selectIndividualsTab(currentIndividual);
				}
			}
		});

		JMenuItem mntmBrowse_1 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.mntmBrowse_1.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmBrowse_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Browse parent1.
				try {
					Individual selected = getIdentityIndividual();
					if ((selected != null) && (selected.getFather() != null)) {
						//
						IndividualsPanel.this.netGUI.changeSegmentationToCluster(selected.getFather());
					}
				} catch (final PuckException exception) {
					//
					exception.printStackTrace();
				}
			}
		});
		popupMenu_2.add(mntmBrowse_1);

		JMenuItem mntmEdit = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.mntmEdit.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmEdit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Edit parent 1 id.
				setParent1IdEditorOn();
			}
		});
		popupMenu_2.add(mntmEdit);
		popupMenu_2.add(mntmSwapParent);

		JSeparator separator = new JSeparator();
		popupMenu_2.add(separator);
		popupMenu_2.add(mntmDelete_2);

		this.lblIndividualParent2Id = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.lblIndividualParent2Id.text"));
		this.lblIndividualParent2Id.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Individual parent 2 id clicked.
				logger.debug("Parent 2 id clicked");
				if (event.getButton() == MouseEvent.BUTTON1) {
					//
					setParent2IdEditorOn();
				}
			}
		});
		panel_12.add(this.lblIndividualParent2Id, "5, 4");
		this.lblIndividualParent2Id.setHorizontalAlignment(SwingConstants.RIGHT);
		this.lblIndividualParent2Id.setAlignmentX(Component.RIGHT_ALIGNMENT);

		this.lblIndividualParent2Name = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.lblMother.text"));
		panel_12.add(this.lblIndividualParent2Name, "6, 4");
		this.lblIndividualParent2Name.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Individual Parent 2 clicked.
				try {
					if (event.getClickCount() == 2) {
						Individual selected = getIdentityIndividual();
						if ((selected != null) && (selected.getMother() != null)) {
							IndividualsPanel.this.netGUI.changeSegmentationToCluster(selected.getMother());
						}
					}
				} catch (final PuckException exception) {
					exception.printStackTrace();
				}
			}
		});
		this.lblIndividualParent2Name.setIcon(new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/female-32x32.png")));

		JPopupMenu popupMenu_3 = new JPopupMenu();
		addPopup(this.lblIndividualParent2Name, popupMenu_3);

		JMenuItem mntmDelete_3 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.mntmDelete_3.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmDelete_3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Delete parent 2.
				Individual ego = getIdentityIndividual();
				if (ego != null) {
					Family family = ego.getOriginFamily();

					if ((family != null) && (family.getMother() != null)) {
						//
						NetUtils.removeMother(family);

						try {
							IndividualsPanel.this.netGUI.getSegmentation().refresh();
						} catch (PuckException exception) {
							// TODO Auto-generated catch block
							exception.printStackTrace();
						}
						IndividualsPanel.this.netGUI.updateAll();
						IndividualsPanel.this.netGUI.setChanged(true);
					}
				}
			}
		});

		JMenuItem mntmBrowse_2 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.mntmBrowse_2.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmBrowse_2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Browse parent 2.
				try {
					Individual selected = getIdentityIndividual();
					if ((selected != null) && (selected.getMother() != null)) {
						//
						IndividualsPanel.this.netGUI.changeSegmentationToCluster(selected.getMother());
					}
				} catch (final PuckException exception) {
					//
					exception.printStackTrace();
				}
			}
		});
		popupMenu_3.add(mntmBrowse_2);

		JMenuItem mntmEdit_1 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.mntmEdit_1.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmEdit_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Edit parent 2 id.
				setParent2IdEditorOn();
			}
		});
		popupMenu_3.add(mntmEdit_1);

		JMenuItem mntmSwapParents = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.mntmSwapParents.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmSwapParents.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Swap Individual parents.
				Individual currentIndividual = IndividualsPanel.this.netGUI.selectedIndividual();
				if ((currentIndividual != null) && (currentIndividual.isNotOrphan())) {
					NetUtils.swapParents(currentIndividual.getOriginFamily());
					IndividualsPanel.this.netGUI.setChanged(true);
					IndividualsPanel.this.netGUI.updateAll();
					IndividualsPanel.this.netGUI.selectIndividualsTab(currentIndividual);
				}
			}
		});
		popupMenu_3.add(mntmSwapParents);

		JSeparator separator_1 = new JSeparator();
		popupMenu_3.add(separator_1);
		popupMenu_3.add(mntmDelete_3);
		panel_12.add(this.cmbbxIndividualParent2, "5, 5, 2, 1, fill, default");

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		panelIndividualIdentity.add(horizontalStrut_1);

		JPanel panel_6 = new JPanel();
		individualPanel.add(panel_6);
		panel_6.setLayout(new BorderLayout(0, 0));

		JSplitPane splitPane_1 = new JSplitPane();
		panel_6.add(splitPane_1, BorderLayout.CENTER);
		splitPane_1.setOrientation(JSplitPane.VERTICAL_SPLIT);

		JSplitPane splitPane_2 = new JSplitPane();
		splitPane_2.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane_1.setRightComponent(splitPane_2);

		JPanel panelIndividualChildren = new JPanel();
		panelIndividualChildren.setPreferredSize(new Dimension(10, 150));
		panelIndividualChildren.setBorder(new EmptyBorder(0, 0, 0, 10));
		panelIndividualChildren.setMinimumSize(new Dimension(10, 100));
		splitPane_2.setLeftComponent(panelIndividualChildren);
		panelIndividualChildren.setLayout(new BoxLayout(panelIndividualChildren, BoxLayout.Y_AXIS));

		Component verticalStrut_1 = Box.createVerticalStrut(5);
		panelIndividualChildren.add(verticalStrut_1);

		JPanel panel_7 = new JPanel();
		panel_7.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelIndividualChildren.add(panel_7);
		panel_7.setLayout(new BoxLayout(panel_7, BoxLayout.X_AXIS));

		this.lblChildren = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.lblChildren.text"));
		panel_7.add(this.lblChildren);

		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		panel_7.add(horizontalStrut_4);

		this.btnAddChild = new JButton(" + ");
		this.btnAddChild.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Add Child (menu item).
				logger.debug("Add Child");
				addChild();
			}
		});
		this.btnAddChild.setBorder(new EmptyBorder(0, 0, 0, 0));
		panel_7.add(this.btnAddChild);

		Component horizontalStrut_5 = Box.createHorizontalStrut(5);
		panel_7.add(horizontalStrut_5);

		JLabel lblCtrlk = new JLabel("(Ctrl-K)");
		panel_7.add(lblCtrlk);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setAlignmentX(Component.LEFT_ALIGNMENT);
		scrollPane_2.setBackground(new Color(255, 255, 255));
		scrollPane_2.setPreferredSize(new Dimension(3, 200));
		panelIndividualChildren.add(scrollPane_2);

		this.tableIndividualChildren = new JTable();
		this.tableIndividualChildren.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Individual children clicked.
				try {
					if (event.getClickCount() == 2) {
						//
						int selectedIndex = IndividualsPanel.this.tableIndividualChildren.getSelectedRow();
						if (selectedIndex != -1) {
							switch (IndividualsPanel.this.tableIndividualChildren.getSelectedColumn()) {
								case 1:
								case 2:
								case 3: {
									Individual selected = ((IndividualChildrenModel) IndividualsPanel.this.tableIndividualChildren.getModel()).children().get(
											selectedIndex);
									if (selected != null) {
										IndividualsPanel.this.netGUI.changeSegmentationToCluster(selected);
									}
								}
								break;

								case 4: {
									Individual selected = ((IndividualChildrenModel) IndividualsPanel.this.tableIndividualChildren.getModel()).children().get(
											selectedIndex);
									if ((selected != null) && (selected.getOriginFamily() != null)) {
										//
										IndividualsPanel.this.netGUI.selectFamiliesTab(selected.getOriginFamily());
									}
								}
								break;

								case 5: {
									Individual selected = ((IndividualChildrenModel) IndividualsPanel.this.tableIndividualChildren.getModel()).children().get(
											selectedIndex);
									if ((selected != null) && (selected.getOtherParent(getIdentityIndividual()) != null)) {
										IndividualsPanel.this.netGUI.changeSegmentationToCluster(selected.getOtherParent(getIdentityIndividual()));
									}
								}
								break;

								default:
							}
						}
					}
				} catch (final PuckException exception) {
					exception.printStackTrace();
				}
			}
		});

		JPopupMenu popupMenu_1 = new JPopupMenu();
		addPopup(this.tableIndividualChildren, popupMenu_1);

		JMenuItem mntmDelete_1 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.mntmDelete_1.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmDelete_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Delete individual children.
				logger.debug("Delete " + ArrayUtils.toString(IndividualsPanel.this.tableIndividualChildren.getSelectedRows()));

				int[] selectedRowIds = IndividualsPanel.this.tableIndividualChildren.getSelectedRows();
				if (selectedRowIds.length != 0) {
					ArrayUtils.reverse(selectedRowIds);
					for (int rowIndex : selectedRowIds) {
						int childId = (Integer) IndividualsPanel.this.tableIndividualChildren.getModel().getValueAt(rowIndex, 0);
						IndividualsPanel.this.netGUI.getNet().removeChild(IndividualsPanel.this.netGUI.getNet().individuals().getById(childId));
					}

					//
					IndividualsPanel.this.netGUI.setChanged(true);
					IndividualsPanel.this.netGUI.updateAll();
				}
			}
		});

		JMenuItem mntmBrowseChild = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.mntmBrowseChild.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmBrowseChild.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Browse child.
				try {
					//
					int selectedIndex = IndividualsPanel.this.tableIndividualChildren.getSelectedRow();
					if (selectedIndex != -1) {
						//
						Individual selected = ((IndividualChildrenModel) IndividualsPanel.this.tableIndividualChildren.getModel()).children()
								.get(selectedIndex);

						if (selected != null) {
							//
							IndividualsPanel.this.netGUI.changeSegmentationToCluster(selected);
						}
					}
				} catch (final PuckException exception) {
					exception.printStackTrace();
				}
			}
		});
		popupMenu_1.add(mntmBrowseChild);

		JMenuItem mntmBrowseSibset = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.mntmBrowseSibset.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmBrowseSibset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Browse sibset.
				int selectedIndex = IndividualsPanel.this.tableIndividualChildren.getSelectedRow();
				if (selectedIndex != -1) {
					//
					Individual selected = ((IndividualChildrenModel) IndividualsPanel.this.tableIndividualChildren.getModel()).children().get(selectedIndex);
					if ((selected != null) && (selected.getOriginFamily() != null)) {
						//
						IndividualsPanel.this.netGUI.selectFamiliesTab(selected.getOriginFamily());
					}
				}
			}
		});
		popupMenu_1.add(mntmBrowseSibset);

		JMenuItem mntmBrowseOtherParent = new JMenuItem(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.mntmBrowseOtherParent.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmBrowseOtherParent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Browse other parent.
				try {
					//
					int selectedIndex = IndividualsPanel.this.tableIndividualChildren.getSelectedRow();
					if (selectedIndex != -1) {
						//
						Individual selected = ((IndividualChildrenModel) IndividualsPanel.this.tableIndividualChildren.getModel()).children()
								.get(selectedIndex);
						if ((selected != null) && (selected.getOtherParent(getIdentityIndividual()) != null)) {
							//
							IndividualsPanel.this.netGUI.changeSegmentationToCluster(selected.getOtherParent(getIdentityIndividual()));
						}
					}
				} catch (final PuckException exception) {
					//
					exception.printStackTrace();
				}
			}
		});
		popupMenu_1.add(mntmBrowseOtherParent);

		JSeparator separator_3 = new JSeparator();
		popupMenu_1.add(separator_3);
		popupMenu_1.add(mntmDelete_1);
		scrollPane_2.setViewportView(this.tableIndividualChildren);
		this.tableIndividualChildren.setBorder(new LineBorder(new Color(169, 169, 169)));
		this.tableIndividualChildren.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		this.tableIndividualChildren.setModel(new IndividualChildrenModel(this.netGUI, null));

		this.cmbbxIndividualChildrenIds = new AutoComboBox(null);
		this.cmbbxIndividualChildrenIds.getEditor().getEditorComponent().addFocusListener(new FocusListener() {
			@Override
			public void focusGained(final FocusEvent arg0) {
			}

			@Override
			public void focusLost(final FocusEvent arg0) {
				logger.debug("FOCUS LOST CHILDREN TABLE " + IndividualsPanel.this.tableIndividualChildren.isEditing());
				if (IndividualsPanel.this.tableIndividualChildren.isEditing()) {
					IndividualsPanel.this.tableIndividualChildren.getColumnModel().getColumn(0).getCellEditor().cancelCellEditing();
				}

				if (((IndividualChildrenModel) IndividualsPanel.this.tableIndividualChildren.getModel()).isNewEditionOn()) {
					((IndividualChildrenModel) IndividualsPanel.this.tableIndividualChildren.getModel()).escapeNewEdition();
				}
			}
		});
		this.cmbbxIndividualChildrenIds.setEditable(true);
		this.cmbbxIndividualChildrenIds.setStrict(false);
		this.cmbbxIndividualChildrenIds.setMaximumRowCount(10);
		TableCellEditor editor = new DefaultCellEditor(this.cmbbxIndividualChildrenIds);
		this.tableIndividualChildren.getColumnModel().getColumn(0).setCellEditor(editor);
		this.tableIndividualChildren.getColumnModel().getColumn(0).setMinWidth(10);
		this.tableIndividualChildren.getColumnModel().getColumn(0).setPreferredWidth(90);
		this.tableIndividualChildren.getColumnModel().getColumn(0).setMaxWidth(110);

		this.tableIndividualChildren.getColumnModel().getColumn(1).setMinWidth(10);
		this.tableIndividualChildren.getColumnModel().getColumn(1).setMaxWidth(30);
		this.tableIndividualChildren.getColumnModel().getColumn(1).setResizable(false);

		List<Integer> childrenOrderList = new ArrayList<Integer>(99);
		for (int order = 1; order < 100; order++) {
			childrenOrderList.add(order);
		}
		this.cmbbxIndividualChildrenOrders = new AutoComboBox(childrenOrderList);
		this.cmbbxIndividualChildrenOrders.getEditor().getEditorComponent().addFocusListener(new FocusListener() {
			@Override
			public void focusGained(final FocusEvent arg0) {
			}

			@Override
			public void focusLost(final FocusEvent arg0) {
				logger.debug("FOCUS LOST CHILDREN TABLE(2) " + IndividualsPanel.this.tableIndividualChildren.isEditing());
				if (IndividualsPanel.this.tableIndividualChildren.isEditing()) {
					IndividualsPanel.this.tableIndividualChildren.getColumnModel().getColumn(3).getCellEditor().cancelCellEditing();
				}

				if (((IndividualChildrenModel) IndividualsPanel.this.tableIndividualChildren.getModel()).isNewEditionOn()) {
					((IndividualChildrenModel) IndividualsPanel.this.tableIndividualChildren.getModel()).escapeNewEdition();
				}
			}
		});
		this.cmbbxIndividualChildrenOrders.setEditable(true);
		this.cmbbxIndividualChildrenOrders.setStrict(false);
		this.cmbbxIndividualChildrenOrders.setMaximumRowCount(10);
		TableCellEditor orderEditor = new DefaultCellEditor(this.cmbbxIndividualChildrenOrders);
		this.tableIndividualChildren.getColumnModel().getColumn(3).setCellEditor(orderEditor);
		this.tableIndividualChildren.getColumnModel().getColumn(3).setPreferredWidth(50);
		this.tableIndividualChildren.getColumnModel().getColumn(3).setMaxWidth(50);
		this.tableIndividualChildren.getColumnModel().getColumn(3).setResizable(false);

		this.tableIndividualChildren.getColumnModel().getColumn(4).setPreferredWidth(100);
		this.tableIndividualChildren.getColumnModel().getColumn(4).setMaxWidth(125);
		this.tableIndividualChildren.getColumnModel().getColumn(4).setResizable(false);

		Component verticalStrut_3 = Box.createVerticalStrut(5);
		panelIndividualChildren.add(verticalStrut_3);

		JSplitPane splitPane_3 = new JSplitPane();
		splitPane_3.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane_2.setRightComponent(splitPane_3);

		JPanel panel_1 = new JPanel();
		splitPane_3.setRightComponent(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));

		Component verticalStrut_10 = Box.createVerticalStrut(5);
		panel_1.add(verticalStrut_10);

		JPanel panel_8 = new JPanel();
		panel_8.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_1.add(panel_8);
		panel_8.setLayout(new BoxLayout(panel_8, BoxLayout.X_AXIS));

		this.lblRelations = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.lblRelations.text"));
		panel_8.add(this.lblRelations);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setPreferredSize(new Dimension(3, 100));
		scrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_1.add(scrollPane);

		this.tableIndividualRelations = new JTable();
		this.tableIndividualRelations.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Individual spouse clicked.
				if (event.getClickCount() == 2) {
					int selectedIndex = IndividualsPanel.this.tableIndividualRelations.getSelectedRow();
					Relation relation = ((IndividualRelationsModel) IndividualsPanel.this.tableIndividualRelations.getModel()).getDelegate().get(selectedIndex);
					// TODO
					IndividualsPanel.this.netGUI.selectRelationTab(relation);
				}
			}
		});

		JPopupMenu popupMenu_5 = new JPopupMenu();
		addPopup(this.tableIndividualRelations, popupMenu_5);

		JMenuItem mntmBrowse_4 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.mntmBrowse_4.text_1")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmBrowse_4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Browse relation.
				int selectedIndex = IndividualsPanel.this.tableIndividualRelations.getSelectedRow();
				if (selectedIndex != -1) {
					Relation relation = ((IndividualRelationsModel) IndividualsPanel.this.tableIndividualRelations.getModel()).getDelegate().get(selectedIndex);
					if (relation != null) {
						//
						IndividualsPanel.this.netGUI.selectRelationTab(relation);
					}
				}
			}
		});
		popupMenu_5.add(mntmBrowse_4);
		this.tableIndividualRelations.setModel(new IndividualRelationsModel(null));
		scrollPane.setViewportView(this.tableIndividualRelations);

		this.attributesPanel = new AttributesPanel(this.netGUI, (Attributes) null, this.attributeTemplates);
		splitPane_3.setLeftComponent(this.attributesPanel);
		this.tableIndividualRelations.getColumnModel().getColumn(0).setMinWidth(10);
		this.tableIndividualRelations.getColumnModel().getColumn(0).setPreferredWidth(90);
		this.tableIndividualRelations.getColumnModel().getColumn(0).setMaxWidth(110);

		JPanel panelIndividualSpouses = new JPanel();
		panelIndividualSpouses.setPreferredSize(new Dimension(10, 150));
		panelIndividualSpouses.setMinimumSize(new Dimension(10, 100));
		panelIndividualSpouses.setBorder(new EmptyBorder(0, 0, 0, 10));
		panelIndividualSpouses.setAlignmentY(Component.TOP_ALIGNMENT);
		panelIndividualSpouses.setAlignmentX(Component.LEFT_ALIGNMENT);
		splitPane_1.setLeftComponent(panelIndividualSpouses);
		panelIndividualSpouses.setLayout(new BoxLayout(panelIndividualSpouses, BoxLayout.Y_AXIS));

		JPanel panel_5 = new JPanel();
		panel_5.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelIndividualSpouses.add(panel_5);
		panel_5.setLayout(new BoxLayout(panel_5, BoxLayout.X_AXIS));

		this.lblIndividualSpouses = new JLabel(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.lblSpouses.text"));
		panel_5.add(this.lblIndividualSpouses);

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		panel_5.add(horizontalStrut_3);

		this.btnAddPartner = new JButton(" + ");
		this.btnAddPartner.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Add partner.
				logger.debug("Add partner");
				addPartner();
			}
		});
		this.btnAddPartner.setBorder(new EmptyBorder(0, 0, 0, 0));
		panel_5.add(this.btnAddPartner);

		Component horizontalStrut_2 = Box.createHorizontalStrut(5);
		panel_5.add(horizontalStrut_2);

		JLabel lblCtrlp = new JLabel("(Ctrl-P)");
		panel_5.add(lblCtrlp);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setPreferredSize(new Dimension(3, 200));
		scrollPane_1.setBackground(new Color(255, 255, 255));
		scrollPane_1.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelIndividualSpouses.add(scrollPane_1);

		this.tableIndividualSpouses = new JTable();
		this.tableIndividualSpouses.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				// Individual spouse clicked.
				try {
					if (event.getClickCount() == 1) {
						logger.debug("one click");
					} else if (event.getClickCount() == 2) {
						int selectedIndex = IndividualsPanel.this.tableIndividualSpouses.getSelectedRow();
						int selectedColumn = IndividualsPanel.this.tableIndividualSpouses.getSelectedColumn();
						logger.debug("[index, col]=[" + selectedIndex + "," + selectedColumn + "]");
						if ((selectedIndex != -1) && (selectedColumn != -1)) {
							if ((selectedColumn > 0) && (selectedColumn < 3)) {
								// Redirect to an individual.
								Individual selected = ((IndividualSpousesModel) IndividualsPanel.this.tableIndividualSpouses.getModel()).spouses().get(
										selectedIndex);
								if (selected != null) {
									IndividualsPanel.this.netGUI.changeSegmentationToCluster(selected);
								}
							} else if (selectedColumn >= 3) {
								// Redirect to a family.
								Family targetFamily = ((IndividualSpousesModel) IndividualsPanel.this.tableIndividualSpouses.getModel())
										.getFamily(selectedIndex);
								if (targetFamily != null) {
									IndividualsPanel.this.netGUI.selectFamiliesTab(targetFamily);
								}
							}
						}
					}
				} catch (final PuckException exception) {
					exception.printStackTrace();
				}
			}
		});

		JPopupMenu popupMenu = new JPopupMenu();
		addPopup(this.tableIndividualSpouses, popupMenu);

		JMenuItem mntmDelete = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.mntmDelete.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmDelete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Delete individual partners.
				logger.debug("Delete " + ArrayUtils.toString(IndividualsPanel.this.tableIndividualSpouses.getSelectedRows()));

				int[] selectedRowIds = IndividualsPanel.this.tableIndividualSpouses.getSelectedRows();
				if (selectedRowIds.length != 0) {
					ArrayUtils.reverse(selectedRowIds);
					for (int rowIndex : selectedRowIds) {
						Family family = ((IndividualSpousesModel) IndividualsPanel.this.tableIndividualSpouses.getModel()).getFamily(rowIndex);

						if (family.isSingleParent()) {
							NetUtils.removeSpouse(family, getSelectedIndividual());
							if (family.isEmpty()) {
								IndividualsPanel.this.netGUI.getNet().remove(family);
							}
						} else {
							Individual spouse = ((IndividualSpousesModel) IndividualsPanel.this.tableIndividualSpouses.getModel()).getSpouse(rowIndex);
							NetUtils.removeSpouse(family, spouse);
							if (family.isEmpty()) {
								IndividualsPanel.this.netGUI.getNet().remove(family);
							}
						}
					}

					//
					IndividualsPanel.this.netGUI.setChanged(true);
					IndividualsPanel.this.netGUI.updateAll();
				}
			}
		});

		JMenuItem mntmBrowse_3 = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.mntmBrowse_3.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmBrowse_3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Browse partner.
				try {
					int selectedIndex = IndividualsPanel.this.tableIndividualSpouses.getSelectedRow();
					if (selectedIndex != -1) {
						// Redirect to an individual.
						Individual selected = ((IndividualSpousesModel) IndividualsPanel.this.tableIndividualSpouses.getModel()).spouses().get(selectedIndex);
						if (selected != null) {
							//
							IndividualsPanel.this.netGUI.changeSegmentationToCluster(selected);
						}
					}
				} catch (final PuckException exception) {
					//
					exception.printStackTrace();
				}

			}
		});
		popupMenu.add(mntmBrowse_3);

		JMenuItem mntmBrowseFamily = new JMenuItem(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.mntmBrowseFamily.text")); //$NON-NLS-1$ //$NON-NLS-2$
		mntmBrowseFamily.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Browse family.
				int selectedIndex = IndividualsPanel.this.tableIndividualSpouses.getSelectedRow();
				if (selectedIndex != -1) {
					// Redirect to a family.
					Family targetFamily = ((IndividualSpousesModel) IndividualsPanel.this.tableIndividualSpouses.getModel()).getFamily(selectedIndex);
					if (targetFamily != null) {
						IndividualsPanel.this.netGUI.selectFamiliesTab(targetFamily);
					}
				}
			}
		});
		popupMenu.add(mntmBrowseFamily);

		JSeparator separator_2 = new JSeparator();
		popupMenu.add(separator_2);
		popupMenu.add(mntmDelete);
		scrollPane_1.setViewportView(this.tableIndividualSpouses);
		this.tableIndividualSpouses.setBorder(new LineBorder(new Color(169, 169, 169)));
		this.tableIndividualSpouses.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.tableIndividualSpouses.setModel(new IndividualSpousesModel(this.netGUI, null));
		this.tableIndividualSpouses.getTableHeader().setReorderingAllowed(false);

		//
		this.cmbbxIndividualSpousesIds = new AutoComboBox(null);
		this.cmbbxIndividualSpousesIds.getEditor().getEditorComponent().addFocusListener(new FocusListener() {
			@Override
			public void focusGained(final FocusEvent arg0) {
			}

			@Override
			public void focusLost(final FocusEvent arg0) {
				logger.debug("FOCUS LOST SPOUSES TABLE " + IndividualsPanel.this.tableIndividualSpouses.isEditing());
				if (IndividualsPanel.this.tableIndividualSpouses.isEditing()) {
					IndividualsPanel.this.tableIndividualSpouses.getColumnModel().getColumn(0).getCellEditor().cancelCellEditing();
				}

				if (((IndividualSpousesModel) IndividualsPanel.this.tableIndividualSpouses.getModel()).isNewEditionOn()) {
					((IndividualSpousesModel) IndividualsPanel.this.tableIndividualSpouses.getModel()).escapeNewEdition();
				}
			}
		});
		this.cmbbxIndividualSpousesIds.setEditable(true);
		this.cmbbxIndividualSpousesIds.setStrict(false);
		this.cmbbxIndividualSpousesIds.setMaximumRowCount(10);
		TableCellEditor editor2 = new DefaultCellEditor(this.cmbbxIndividualSpousesIds);
		this.tableIndividualSpouses.getColumnModel().getColumn(0).setCellEditor(editor2);
		this.tableIndividualSpouses.getColumnModel().getColumn(0).setMinWidth(10);
		this.tableIndividualSpouses.getColumnModel().getColumn(0).setPreferredWidth(90);
		this.tableIndividualSpouses.getColumnModel().getColumn(0).setMaxWidth(110);

		this.tableIndividualSpouses.getColumnModel().getColumn(1).setMinWidth(10);
		this.tableIndividualSpouses.getColumnModel().getColumn(1).setMaxWidth(30);
		this.tableIndividualSpouses.getColumnModel().getColumn(1).setResizable(false);

		List<Integer> spousesOrderList = new ArrayList<Integer>(99);
		for (int order = 1; order < 100; order++) {
			spousesOrderList.add(order);
		}
		this.cmbbxIndividualSpousesOrders = new AutoComboBox(spousesOrderList);
		this.cmbbxIndividualSpousesOrders.getEditor().getEditorComponent().addFocusListener(new FocusListener() {
			@Override
			public void focusGained(final FocusEvent arg0) {
			}

			@Override
			public void focusLost(final FocusEvent arg0) {
				logger.debug("FOCUS LOST CHILDREN TABLE(2) " + IndividualsPanel.this.tableIndividualSpouses.isEditing());
				if (IndividualsPanel.this.tableIndividualSpouses.isEditing()) {
					IndividualsPanel.this.tableIndividualSpouses.getColumnModel().getColumn(3).getCellEditor().cancelCellEditing();
				}

				if (((IndividualSpousesModel) IndividualsPanel.this.tableIndividualSpouses.getModel()).isNewEditionOn()) {
					((IndividualSpousesModel) IndividualsPanel.this.tableIndividualSpouses.getModel()).escapeNewEdition();
				}
			}
		});
		this.cmbbxIndividualSpousesOrders.setEditable(true);
		this.cmbbxIndividualSpousesOrders.setStrict(false);
		this.cmbbxIndividualSpousesOrders.setMaximumRowCount(10);
		TableCellEditor spousesOrderEditor = new DefaultCellEditor(this.cmbbxIndividualSpousesOrders);
		this.tableIndividualSpouses.getColumnModel().getColumn(3).setCellEditor(spousesOrderEditor);

		this.tableIndividualSpouses.getColumnModel().getColumn(3).setMinWidth(10);
		this.tableIndividualSpouses.getColumnModel().getColumn(3).setPreferredWidth(50);
		this.tableIndividualSpouses.getColumnModel().getColumn(3).setMaxWidth(50);

		this.tableIndividualSpouses.getColumnModel().getColumn(4).setMinWidth(10);
		this.tableIndividualSpouses.getColumnModel().getColumn(4).setMaxWidth(30);
		this.tableIndividualSpouses.getColumnModel().getColumn(4).setResizable(false);

		this.tableIndividualSpouses.getColumnModel().getColumn(5).setMinWidth(10);
		this.tableIndividualSpouses.getColumnModel().getColumn(5).setPreferredWidth(90);
		this.tableIndividualSpouses.getColumnModel().getColumn(5).setMaxWidth(110);

		Component verticalStrut_2 = Box.createVerticalStrut(5);
		panelIndividualSpouses.add(verticalStrut_2);
		this.tableIndividualChildren.getColumnModel().getColumn(3).setPreferredWidth(100);

		JPanel individualsButtonsPanel = new JPanel();
		add(individualsButtonsPanel);
		individualsButtonsPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		individualsButtonsPanel.setLayout(new BoxLayout(individualsButtonsPanel, BoxLayout.X_AXIS));

		JButton btnRemoveIndividual = new JButton("");
		btnRemoveIndividual.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Remove individual.
				Individual target = getSelectedIndividual();
				if (target != null) {
					//
					int response = JOptionPane.showConfirmDialog(IndividualsPanel.this.thisJPanel,
							"Do you really want to delete individual nr. " + target.getId() + "?", "Individual delete confirm", JOptionPane.YES_NO_OPTION);

					if (response == JOptionPane.YES_OPTION) {
						//
						int selectedIndex = IndividualsPanel.this.individualList.getSelectedIndex();
						IndividualsPanel.this.netGUI.getNet().remove(target);
						((IndividualsModel) IndividualsPanel.this.individualList.getModel()).setSource(IndividualsPanel.this.netGUI.getNet().individuals());
						if (IndividualsPanel.this.netGUI.getNet().individuals().size() != 0) {
							if (selectedIndex == IndividualsPanel.this.netGUI.getNet().individuals().size()) {
								selectedIndex -= 1;
							}
							IndividualsPanel.this.individualList.setSelectedIndex(selectedIndex);
						}
						IndividualsPanel.this.netGUI.updateAll();
						IndividualsPanel.this.netGUI.setChanged(true);
					}
				}
			}
		});
		btnRemoveIndividual.setToolTipText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnNewButton.toolTipText"));
		btnRemoveIndividual.setIcon(new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/remove.png")));
		individualsButtonsPanel.add(btnRemoveIndividual);

		JButton btnIndividualsPrevious = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnPrevious.text"));
		btnIndividualsPrevious.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Previous individual.
				int selectedIndex = IndividualsPanel.this.individualList.getSelectedIndex();
				if (selectedIndex == -1) {
					int size = IndividualsPanel.this.individualList.getModel().getSize();
					if (size != 0) {
						selectedIndex = size - 1;
						IndividualsPanel.this.individualList.setSelectedIndex(selectedIndex);
						IndividualsPanel.this.individualList.ensureIndexIsVisible(selectedIndex);
					}
				} else if (selectedIndex > 0) {
					selectedIndex -= 1;
					IndividualsPanel.this.individualList.setSelectedIndex(selectedIndex);
					IndividualsPanel.this.individualList.ensureIndexIsVisible(selectedIndex);
				}
			}
		});

		JButton btnAddIndividual = new JButton("");
		btnAddIndividual.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Add an individual.
				addIndividual();
			}
		});
		btnAddIndividual.setToolTipText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnNewButton_1.toolTipText"));
		btnAddIndividual.setIcon(new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/add.png")));
		individualsButtonsPanel.add(btnAddIndividual);

		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		individualsButtonsPanel.add(horizontalStrut_6);

		JButton btnSort = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.btnSort.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnSort.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Sort individual list.

				Individual selectedIndividual = getSelectedIndividual();
				((IndividualsModel) IndividualsPanel.this.individualList.getModel()).touchSorting();
				if (selectedIndividual != null) {
					//
					select(selectedIndividual);
				}
			}
		});
		individualsButtonsPanel.add(btnSort);

		Component horizontalGlue_4 = Box.createHorizontalGlue();
		individualsButtonsPanel.add(horizontalGlue_4);
		individualsButtonsPanel.add(btnIndividualsPrevious);

		JButton btnIndividualsNext = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnNext.text"));
		btnIndividualsNext.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Next individual.
				int selectedIndex = IndividualsPanel.this.individualList.getSelectedIndex();
				int size = IndividualsPanel.this.individualList.getModel().getSize();
				if (selectedIndex == -1) {
					if (size != 0) {
						selectedIndex = 0;
						IndividualsPanel.this.individualList.setSelectedIndex(selectedIndex);
						IndividualsPanel.this.individualList.ensureIndexIsVisible(selectedIndex);
					}
				} else if (selectedIndex < size - 1) {
					selectedIndex += 1;
					IndividualsPanel.this.individualList.setSelectedIndex(selectedIndex);
					IndividualsPanel.this.individualList.ensureIndexIsVisible(selectedIndex);
				}
			}
		});
		individualsButtonsPanel.add(btnIndividualsNext);

		Component horizontalGlue = Box.createHorizontalGlue();
		individualsButtonsPanel.add(horizontalGlue);

		JPanel panel_13 = new JPanel();
		individualsButtonsPanel.add(panel_13);
		panel_13.setLayout(new BoxLayout(panel_13, BoxLayout.X_AXIS));

		this.tglbtnIndividualIdStrategy = new JToggleButton(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.tglbtnIndividualIdStratgy.text")); //$NON-NLS-1$ //$NON-NLS-2$
		this.tglbtnIndividualIdStrategy.setToolTipText(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.tglbtnIndividualIdStratgy.toolTipText")); //$NON-NLS-1$ //$NON-NLS-2$
		this.tglbtnIndividualIdStrategy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Click individual id strategy.
				if (IndividualsPanel.this.tglbtnIndividualIdStrategy.isSelected()) {
					IndividualsPanel.this.tglbtnIndividualIdStrategy.setIcon(appendIcon);
				} else {
					IndividualsPanel.this.tglbtnIndividualIdStrategy.setIcon(insertIcon);
				}
			}
		});
		this.tglbtnIndividualIdStrategy.setIcon(insertIcon);
		panel_13.add(this.tglbtnIndividualIdStrategy);

		this.tglbtnFamilyIdStrategy = new JToggleButton(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.tglbtnFamilyIdStrategy.text")); //$NON-NLS-1$ //$NON-NLS-2$
		this.tglbtnFamilyIdStrategy.setEnabled(false);
		this.tglbtnFamilyIdStrategy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Click family id strategy.
				if (IndividualsPanel.this.tglbtnFamilyIdStrategy.isSelected()) {
					IndividualsPanel.this.tglbtnFamilyIdStrategy.setIcon(appendIcon);
				} else {
					IndividualsPanel.this.tglbtnFamilyIdStrategy.setIcon(insertIcon);
				}
			}
		});
		this.tglbtnFamilyIdStrategy.setToolTipText(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.tglbtnFamilyIdStrategy.toolTipText")); //$NON-NLS-1$ //$NON-NLS-2$
		this.tglbtnFamilyIdStrategy.setIcon(insertIcon);
		panel_13.add(this.tglbtnFamilyIdStrategy);

		this.btnDefaultUnionStatus = new JButton("");
		this.btnDefaultUnionStatus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Click default union status.
				switch (IndividualsPanel.this.defaultUnionStatus) {
					case UNMARRIED:
						IndividualsPanel.this.defaultUnionStatus = UnionStatus.MARRIED;
						IndividualsPanel.this.btnDefaultUnionStatus.setIcon(smallMarriedIcon);
					break;

					case MARRIED:
						IndividualsPanel.this.defaultUnionStatus = UnionStatus.DIVORCED;
						IndividualsPanel.this.btnDefaultUnionStatus.setIcon(smallDivorcedIcon);
					break;

					case DIVORCED:
						IndividualsPanel.this.defaultUnionStatus = UnionStatus.UNMARRIED;
						IndividualsPanel.this.btnDefaultUnionStatus.setIcon(smallUnmarriedIcon);
					break;

					default:
						IndividualsPanel.this.defaultUnionStatus = UnionStatus.UNMARRIED;
						IndividualsPanel.this.btnDefaultUnionStatus.setIcon(smallUnmarriedIcon);
				}
			}
		});
		this.btnDefaultUnionStatus.setToolTipText(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.btnDefaultUnionStatus.toolTipText")); //$NON-NLS-1$ //$NON-NLS-2$
		this.btnDefaultUnionStatus.setIcon(smallMarriedIcon);
		panel_13.add(this.btnDefaultUnionStatus);

		Component horizontalGlue_3 = Box.createHorizontalGlue();
		individualsButtonsPanel.add(horizontalGlue_3);

		JButton btnResetTemplatedAttributes = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.btnRta.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnResetTemplatedAttributes.setToolTipText(ResourceBundle
				.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.btnResetTemplatedAttributes.toolTipText")); //$NON-NLS-1$ //$NON-NLS-2$
		btnResetTemplatedAttributes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Reset Templated Attributes.
				updateAttributeTemplates();
			}
		});
		individualsButtonsPanel.add(btnResetTemplatedAttributes);

		Component horizontalGlue_1 = Box.createHorizontalGlue();
		individualsButtonsPanel.add(horizontalGlue_1);

		JLabel lblIndividualsSearch = new JLabel(" ");
		lblIndividualsSearch.setToolTipText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.lblSearch.text")); //$NON-NLS-1$ //$NON-NLS-2$
		lblIndividualsSearch.setIcon(new ImageIcon(MainWindow.class.getResource("/org/tip/puckgui/images/find.png")));
		individualsButtonsPanel.add(lblIndividualsSearch);
		lblIndividualsSearch.setMinimumSize(new Dimension(300, 15));

		this.txtfldSearchIndividual = new JTextField();
		this.txtfldSearchIndividual.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Search individual.
				String pattern = IndividualsPanel.this.txtfldSearchIndividual.getText();
				logger.debug("Search individual=[" + pattern + "]");
				if (StringUtils.isNotBlank(pattern)) {
					int index = ((IndividualsModel) IndividualsPanel.this.individualList.getModel()).nextSearchedIndividualIndex(pattern);
					if (index != -1) {
						IndividualsPanel.this.individualList.setSelectedIndex(index);
						IndividualsPanel.this.individualList.ensureIndexIsVisible(index);
					}
				}
			}
		});
		individualsButtonsPanel.add(this.txtfldSearchIndividual);
		this.txtfldSearchIndividual.setMaximumSize(new Dimension(500, 50));
		this.txtfldSearchIndividual.setText("");
		this.txtfldSearchIndividual.setColumns(15);
	}

	/**
	 * 
	 */
	public void addChild() {
		// Add child.
		if (((IndividualChildrenModel) this.tableIndividualChildren.getModel()).getSource() != null) {
			if (!((IndividualChildrenModel) this.tableIndividualChildren.getModel()).isNewEditionOn()) {
				//
				Family targetFamily = ((IndividualSpousesModel) this.tableIndividualSpouses.getModel()).getFamily(this.tableIndividualSpouses.getSelectedRow());

				//
				((IndividualChildrenModel) this.tableIndividualChildren.getModel()).setNewItem(targetFamily);

				//
				this.tableIndividualChildren.scrollRectToVisible(this.tableIndividualChildren.getCellRect(
						this.tableIndividualChildren.getModel().getRowCount() - 1, 0, true));
				this.tableIndividualChildren.setRowSelectionInterval(this.tableIndividualChildren.getModel().getRowCount() - 1, this.tableIndividualChildren
						.getModel().getRowCount() - 1);
				this.tableIndividualChildren.editCellAt(this.tableIndividualChildren.getModel().getRowCount() - 1, 0);
				this.tableIndividualChildren.getEditorComponent().requestFocus();
			}
		}
	}

	/**
	 * 
	 */
	public void addIndividual() {
		// Add an individual.
		try {
			//
			logger.debug("ID Strategy = " + getIndividualIdStrategy());
			Individual individual = this.netGUI.getNet().createIndividual(getIndividualIdStrategy(), "", Gender.UNKNOWN);

			this.netGUI.setChanged(true);
			this.netGUI.updateAll();
			this.netGUI.selectIndividualsTab(individual);

			setFirstNameEditorOn();

		} catch (final Exception exception) {
			// Show trace.
			exception.printStackTrace();

			//
			String title = "Error computerum est";
			String message = "Error occured during working: " + exception.getMessage();

			//
			JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * 
	 */
	public void addOriginFamily() {
		//
		if (((IndividualSpousesModel) this.tableIndividualSpouses.getModel()).getSource() != null) {
			if (!((IndividualSpousesModel) this.tableIndividualSpouses.getModel()).isNewEditionOn()) {

				//
				Individual currentIndividual = getSelectedIndividual();

				if ((currentIndividual != null) && (currentIndividual.getOriginFamily() == null)) {
					//
					Family family = this.netGUI.getNet().createFamily(null, null);
					family.setUnionStatus(getDefaultUnionStatus());

					//
					currentIndividual.setOriginFamily(family);
					family.getChildren().add(currentIndividual);

					//
					this.netGUI.updateAll();
				}
			}
		}
	}

	/**
	 * 
	 */
	public void addPartner() {
		//
		if (((IndividualSpousesModel) this.tableIndividualSpouses.getModel()).getSource() != null) {
			if (!((IndividualSpousesModel) this.tableIndividualSpouses.getModel()).isNewEditionOn()) {
				//
				((IndividualSpousesModel) this.tableIndividualSpouses.getModel()).setNewItem();

				//
				this.tableIndividualSpouses.scrollRectToVisible(this.tableIndividualSpouses.getCellRect(
						this.tableIndividualSpouses.getModel().getRowCount() - 1, 0, true));
				this.tableIndividualSpouses.setRowSelectionInterval(this.tableIndividualSpouses.getModel().getRowCount() - 1, this.tableIndividualSpouses
						.getModel().getRowCount() - 1);
				this.tableIndividualSpouses.editCellAt(this.tableIndividualSpouses.getModel().getRowCount() - 1, 0);
				this.tableIndividualSpouses.getEditorComponent().requestFocus();
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	public UnionStatus getDefaultUnionStatus() {
		UnionStatus result;

		result = this.defaultUnionStatus;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public IdStrategy getFamilyIdStrategy() {
		IdStrategy result;

		if (this.tglbtnFamilyIdStrategy.isSelected()) {
			result = IdStrategy.APPEND;
		} else {
			result = IdStrategy.FILL;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individual getIdentityIndividual() {
		Individual result;

		if (NumberUtils.isDigits(this.lblIndividualId.getText())) {
			result = this.netGUI.getNet().individuals().getById(Integer.parseInt(this.lblIndividualId.getText()));
		} else {
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individual getIdentityParent1() {
		Individual result;

		if (NumberUtils.isDigits(this.lblIndividualParent1Id.getText())) {
			result = this.netGUI.getNet().individuals().getById(Integer.parseInt(this.lblIndividualParent1Id.getText()));
		} else {
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individual getIdentityParent2() {
		Individual result;

		if (NumberUtils.isDigits(this.lblIndividualParent2Id.getText())) {
			result = this.netGUI.getNet().individuals().getById(Integer.parseInt(this.lblIndividualParent2Id.getText()));
		} else {
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public IdStrategy getIndividualIdStrategy() {
		IdStrategy result;

		if (this.tglbtnIndividualIdStrategy.isSelected()) {
			result = IdStrategy.APPEND;
		} else {
			result = IdStrategy.FILL;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individual getSelectedIndividual() {
		Individual result;

		if (this.individualList.getSelectedIndex() == -1) {
			result = null;
		} else {
			result = (Individual) ((IndividualsModel) this.individualList.getModel()).getElementAt(this.individualList.getSelectedIndex());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 */
	public void select(final Individual individual) {
		int individualIndex = ((IndividualsModel) this.individualList.getModel()).indexOf(individual);

		if ((individualIndex >= 0) && (individualIndex < ((IndividualsModel) this.individualList.getModel()).getSize())) {
			this.individualList.setSelectedIndex(individualIndex);
			this.individualList.ensureIndexIsVisible(individualIndex);
		} else if (((IndividualsModel) this.individualList.getModel()).getSize() != 0) {
			this.individualList.setSelectedIndex(0);
			this.individualList.ensureIndexIsVisible(0);
		} else {
			updateIndividualIdentity(null);
		}
	}

	/**
	 * 
	 * @param target
	 */
	public void selectByIndex(final int individualIndex) {

		if ((individualIndex >= 0) && (individualIndex < ((IndividualsModel) this.individualList.getModel()).getSize())) {
			this.individualList.setSelectedIndex(individualIndex);
			this.individualList.ensureIndexIsVisible(individualIndex);
		} else if (((IndividualsModel) this.individualList.getModel()).getSize() != 0) {
			this.individualList.setSelectedIndex(0);
			this.individualList.ensureIndexIsVisible(0);
		} else {
			updateIndividualIdentity(null);
		}
	}

	/**
	 * 
	 */
	private void setFirstNameEditorOn() {
		//
		List<String> firstNames = this.netGUI.getNet().individuals().firstNames();
		this.cmbbxIndividualFirstName.setDataList(firstNames);

		Individual currentIndividual = getIdentityIndividual();
		if (currentIndividual != null) {
			int firstNameIndex = ToolBox.indexOf(currentIndividual.getFirstName(), firstNames);
			this.cmbbxIndividualFirstName.setSelectedIndex(firstNameIndex);
		}

		//
		this.lblIndividualFirstName.setVisible(false);
		this.cmbbxIndividualFirstName.setVisible(true);
		this.cmbbxIndividualFirstName.requestFocus();
	}

	/**
	 * 
	 */
	public void setFocusOnFind() {
		this.txtfldSearchIndividual.requestFocus();
	}

	/**
	 * 
	 */
	private void setIndividualIdEditorOn() {
		//
		this.txtFldIndividualId.setText(this.lblIndividualId.getText());

		//
		this.lblIndividualId.setVisible(false);
		this.txtFldIndividualId.setVisible(true);

		//
		this.txtFldIndividualId.requestFocus();
	}

	/**
	 * 
	 */
	private void setLastNameEditorOn() {

		//
		List<String> lastNames = this.netGUI.getNet().individuals().lastNames();
		this.cmbbxIndividualLastName.setDataList(lastNames);

		//
		Individual currentIndividual = getIdentityIndividual();
		if (currentIndividual != null) {
			int lastNameIndex = ToolBox.indexOf(currentIndividual.getLastName(), lastNames);
			this.cmbbxIndividualLastName.setSelectedIndex(lastNameIndex);
		}

		//
		this.lblIndividualLastName.setVisible(false);
		this.cmbbxIndividualLastName.setVisible(true);
		this.cmbbxIndividualLastName.requestFocus();
	}

	/**
	 * 
	 */
	private void setParent1IdEditorOn() {
		//
		setParent1IdEditorOnWithoutFocus();

		//
		this.cmbbxIndividualParent1.requestFocus();
	}

	/**
	 * 
	 */
	private void setParent1IdEditorOnWithoutFocus() {
		//
		List<Individual> individuals = ((IndividualsModel) this.individualList.getModel()).toSortedList();
		List<String> items = new ArrayList<String>(individuals.size());
		for (Individual individual : individuals) {
			items.add(individual.getId() + " " + individual.getName());
		}
		this.cmbbxIndividualParent1.setDataList(items);

		//
		Individual currentParent = getIdentityParent1();
		if (currentParent == null) {
			this.cmbbxIndividualParent1.setSelectedIndex(-1);
		} else {
			int currentItemIndex = ToolBox.indexOf(currentParent.getId() + " " + currentParent.getName(), items);
			this.cmbbxIndividualParent1.setSelectedIndex(currentItemIndex);
		}

		//
		this.lblIndividualParent1Id.setVisible(false);
		this.lblIndividualParent1Name.setVisible(false);

		//
		this.cmbbxIndividualParent1.setVisible(true);
	}

	/**
	 * 
	 */
	private void setParent2IdEditorOn() {
		//
		List<Individual> individuals = ((IndividualsModel) this.individualList.getModel()).toSortedList();
		List<String> items = new ArrayList<String>(individuals.size());
		for (Individual individual : individuals) {
			items.add(individual.getId() + " " + individual.getName());
		}
		this.cmbbxIndividualParent2.setDataList(items);

		//
		Individual currentParent = getIdentityParent2();
		if (currentParent == null) {
			this.cmbbxIndividualParent2.setSelectedIndex(-1);
		} else {
			int currentItemIndex = ToolBox.indexOf(currentParent.getId() + " " + currentParent.getName(), items);
			this.cmbbxIndividualParent2.setSelectedIndex(currentItemIndex);
		}

		//
		this.lblIndividualParent2Id.setVisible(false);
		this.lblIndividualParent2Name.setVisible(false);

		//
		this.cmbbxIndividualParent2.setVisible(true);

		//
		this.cmbbxIndividualParent2.requestFocus();
	}

	/**
	 * 
	 */
	public void update() {
		//
		int selectedIndividual = this.individualList.getSelectedIndex();

		//
		((IndividualsModel) this.individualList.getModel()).setSource(this.netGUI.getCurrentIndividuals());

		//
		selectByIndex(selectedIndividual);
	}

	/**
	 * 
	 */
	public void update(final Individual individual) {
		//
		((IndividualsModel) this.individualList.getModel()).setSource(this.netGUI.getCurrentIndividuals());

		//
		select(individual);
	}

	/**
	 * 
	 */
	public void updateAttributeTemplates() {
		//
		this.attributeTemplates.clear();
		if ((this.netGUI != null) && (this.netGUI.getNet() != null)) {
			//
			List<String> templates = AttributeWorker.getExogenousAttributeDescriptors(this.netGUI.getNet().individuals(), null).labels();

			//
			this.attributeTemplates.addAll(templates);
		}
	}

	/**
	 * 
	 */
	public void updateIndividualIdentity() {
		//
		updateIndividualIdentity(getSelectedIndividual());
	}

	/**
	 * 
	 */
	public void updateIndividualIdentity(final Individual source) {

		if (source == null) {
			//
			logger.debug("updateIndividualIdentity(null)");

		} else {
			//
			logger.debug("updateIndividualIdentity(" + source.getId() + ")");
		}

		if (source == null) {
			//
			this.lblIndividualId.setText("---");
			this.lblIndividualId.setVisible(true);
			this.txtFldIndividualId.setVisible(false);
			this.lblIndividualGender.setIcon(largeUnknowIcon);
			this.lblIndividualFirstName.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.individual.unknown"));
			this.lblIndividualLastName.setText("");

			this.lblFirstLastNameSeparator.setVisible(false);
			this.lblPosition.setText("(--/--)");

			//
			this.lblFamilyId.setText("---");
			this.lblFamilyStatus.setIcon(mediumUnmarriedIcon);

			//
			this.lblIndividualParent1Id.setText("---");
			this.lblIndividualParent1Name.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.individual.unknown"));
			this.lblIndividualParent1Name.setIcon(mediumUnknowIcon);

			//
			this.lblIndividualParent2Id.setText("---");
			this.lblIndividualParent2Name.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.individual.unknown"));
			this.lblIndividualParent2Name.setIcon(mediumUnknowIcon);

			//
			this.lblIndividualSpouses.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.lblSpouses.text"));
			((IndividualSpousesModel) this.tableIndividualSpouses.getModel()).setSource(null);
			this.cmbbxIndividualSpousesIds.setDataList(new ArrayList<String>(0));

			//
			this.lblChildren.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.lblChildren.text"));
			((IndividualChildrenModel) this.tableIndividualChildren.getModel()).setSource(null);
			this.cmbbxIndividualChildrenIds.setDataList(new ArrayList<String>(0));

			//
			this.attributesPanel.setSource(null);

			//
			this.lblRelations.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.lblRelations.text"));
			((IndividualRelationsModel) this.tableIndividualRelations.getModel()).setSource(null);

			//
			this.btnAddPartner.setEnabled(false);
			this.btnAddChild.setEnabled(false);

		} else {
			//
			this.lblIndividualId.setText(String.valueOf(source.getId()));
			this.lblIndividualId.setVisible(true);
			this.txtFldIndividualId.setVisible(false);
			switch (source.getGender()) {
				case FEMALE:
					this.lblIndividualGender.setIcon(largeFemaleIcon);
				break;
				case MALE:
					this.lblIndividualGender.setIcon(largeMaleIcon);
				break;
				case UNKNOWN:
					this.lblIndividualGender.setIcon(largeUnknowIcon);
				break;
			}
			this.lblIndividualFirstName.setText(source.getFirstName());

			//
			String lastName = source.getLastName();
			if (StringUtils.isBlank(lastName)) {
				//
				this.lblFirstLastNameSeparator.setVisible(false);
				this.lblIndividualLastName.setText("      ");

			} else {
				//
				this.lblFirstLastNameSeparator.setVisible(true);
				this.lblIndividualLastName.setText(source.getLastName());
			}

			//
			this.lblPosition.setText("(" + (this.individualList.getSelectedIndex() + 1) + "/" + this.individualList.getModel().getSize() + ")");

			//
			if (source.getOriginFamily() == null) {
				//
				this.lblFamilyId.setText("---");
				this.lblFamilyStatus.setIcon(mediumUnmarriedIcon);

			} else {
				//
				this.lblFamilyId.setText(String.valueOf(source.getOriginFamily().getId()));
				switch (source.getOriginFamily().getUnionStatus()) {
					case UNMARRIED:
						this.lblFamilyStatus.setIcon(mediumUnmarriedIcon);
					break;
					case MARRIED:
						this.lblFamilyStatus.setIcon(mediumMarriedIcon);
					break;
					case DIVORCED:
						this.lblFamilyStatus.setIcon(mediumDivorcedIcon);
					break;
					default:
						this.lblFamilyStatus.setIcon(largeUnknowIcon);
				}
			}

			//
			if (source.getFather() == null) {
				//
				this.lblIndividualParent1Id.setText("---");
				this.lblIndividualParent1Name.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.individual.unknown"));
				this.lblIndividualParent1Name.setIcon(mediumUnknowIcon);

			} else {
				//
				this.lblIndividualParent1Id.setText(String.valueOf(source.getFather().getId()));
				this.lblIndividualParent1Name.setText(source.getFather().getName());
				switch (source.getFather().getGender()) {
					case FEMALE:
						this.lblIndividualParent1Name.setIcon(mediumFemaleIcon);
					break;
					case MALE:
						this.lblIndividualParent1Name.setIcon(mediumMaleIcon);
					break;
					case UNKNOWN:
						this.lblIndividualParent1Name.setIcon(mediumUnknowIcon);
					break;
				}
			}

			//
			if (source.getMother() == null) {
				//
				this.lblIndividualParent2Id.setText("---");
				this.lblIndividualParent2Name.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.individual.unknown"));
				this.lblIndividualParent2Name.setIcon(mediumUnknowIcon);

			} else {
				//
				this.lblIndividualParent2Id.setText(String.valueOf(source.getMother().getId()));
				this.lblIndividualParent2Name.setText(source.getMother().getName());
				switch (source.getMother().getGender()) {
					case FEMALE:
						this.lblIndividualParent2Name.setIcon(mediumFemaleIcon);
					break;
					case MALE:
						this.lblIndividualParent2Name.setIcon(mediumMaleIcon);
					break;
					case UNKNOWN:
						this.lblIndividualParent2Name.setIcon(mediumUnknowIcon);
					break;
				}
			}

			//
			((IndividualSpousesModel) this.tableIndividualSpouses.getModel()).setSource(source);
			this.lblIndividualSpouses.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.lblSpouses.text") + " ("
					+ source.getPersonalFamilies().size() + ")");

			// Update selectable ids for all panels.
			ComboBoxIds.instance().update(((IndividualsModel) this.individualList.getModel()).toSortedList());
			this.cmbbxIndividualSpousesIds.setDataList(ComboBoxIds.instance().items());
			this.cmbbxIndividualChildrenIds.setDataList(ComboBoxIds.instance().items());

			//
			((IndividualChildrenModel) this.tableIndividualChildren.getModel()).setSource(source);
			this.lblChildren.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.lblChildren.text") + " ("
					+ ((IndividualChildrenModel) this.tableIndividualChildren.getModel()).children().size() + ")");

			//
			this.attributesPanel.setSource(source.attributes());

			//
			this.lblRelations.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.lblRelations.text") + " ("
					+ source.relations().size() + ")");
			((IndividualRelationsModel) this.tableIndividualRelations.getModel()).setSource(source);

			//
			this.btnAddPartner.setEnabled(true);
			this.btnAddChild.setEnabled(true);
		}
	}

	/**
	 * 
	 * @param component
	 * @param popup
	 */
	private static void addPopup(final Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			@Override
			public void mouseReleased(final MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(final MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}

	/**
	 * 
	 * @param level
	 * @param father
	 * @param mother
	 * @param children
	 * @return
	 */
	public static String controlFixedPartners(final CheckLevel level, final Individual father, final Individual mother, final Individual... children) {
		String result;

		if (NetUtils.isRolesFixedByGender(father, mother)) {
			result = IndividualsPanel.controlPartners(level, father, mother, children);
		} else {
			result = IndividualsPanel.controlPartners(level, father, mother, children);
		}
		//
		return result;
	}

	/**
	 * 
	 * @param level
	 * @param father
	 * @param mother
	 * @param children
	 * @return
	 */
	public static String controlPartners(final CheckLevel level, final Individual father, final Individual mother, final Individual... children) {
		String result;

		if ((level == CheckLevel.ERROR) && (NetUtils.isSame(father, mother))) {
			result = "Same parent twice detected.";
		} else if ((PuckGUI.instance().getPreferences().getInputSettings().getSameSexSpouses() == level) && (NetUtils.isSameSex(father, mother))) {
			result = "Same sex spouses detected.";
		} else if ((PuckGUI.instance().getPreferences().getInputSettings().getFemaleFathersOrMaleMothers() == level)
				&& (NetUtils.isFemaleFatherOrMaleMother(father, mother))) {
			result = "Female Father or Male Mother or detected.";
		} else if ((PuckGUI.instance().getPreferences().getInputSettings().getParentChildMarriages() == level)
				&& (NetUtils.isParentChildMarriage(father, mother, children))) {
			result = "Parent-child marriages detected.";
		} else {
			result = null;
		}

		//
		return result;
	}
}
