package org.tip.puck.graphs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author TIP
 */
public class WeightedNodes<E> extends HashMap<Node<E>, Double> implements Iterable<Node<E>> {

	private static final long serialVersionUID = -5631405850137514711L;

	/**
	 *
	 */
	public WeightedNodes() {
		super();
	}

	/**
	 *
	 */
	public WeightedNodes(final int initialCapacity) {
		super(initialCapacity);
	}

	/**
	 * 
	 * @param targetNode
	 */
	public void add(final Node<E> targetNode) {
		this.put(targetNode, 0.0);
	}

	/**
	 * 
	 * @param targetNode
	 */
	public void add(final Node<E> targetNode, final double weight) {
		this.put(targetNode, weight);
	}

	/**
	 * 
	 * @param targetNode
	 */
	public void addWeight(final Node<E> targetNode, final double value) {
		//
		Double weight = this.get(targetNode);
		if (weight == null) {
			weight = 0.0;
		}

		//
		this.put(targetNode, weight + value);
	}

	/**
	 * 
	 * @return
	 */
	public double average() {
		double result;

		if (size() == 0) {
			result = 0;
		} else {
			result = sum() / size();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double averagePositive() {
		double result;

		//
		double sum = 0;

		//
		int count = 0;
		for (Double value : this.values()) {
			if ((value != null) && (value > 0)) {
				count += 1;
				sum += value;
			}
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum / count;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetNode
	 */
	public Double getWeight(final Node<E> targetNode) {
		Double result;

		//
		result = this.get(targetNode);

		//
		return result;
	}

	/**
	 * 
	 * @param targetNode
	 */
	public void incWeight(final Node<E> targetNode) {
		//
		Double weight = this.get(targetNode);
		if (weight == null) {
			weight = 0.0;
		}

		//
		this.put(targetNode, weight + 1);
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Node<E>> iterator() {
		Iterator<Node<E>> result;

		result = super.keySet().iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double max() {
		double result;

		if (this.size() == 0) {
			result = 0;
		} else {
			result = Double.MIN_VALUE;
			for (Double value : this.values()) {
				if ((value != null) && (value > result)) {
					result = value;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double min() {
		double result;

		if (this.size() == 0) {
			result = 0;
		} else {
			result = Double.MAX_VALUE;
			for (Double value : this.values()) {
				if ((value != null) && (value < result)) {
					result = value;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetNode
	 * @param value
	 */
	public void setWeight(final Node<E> targetNode, final double value) {
		//
		this.put(targetNode, value);
	}

	/**
	 * 
	 * @return
	 */
	public double sum() {
		double result;

		result = 0;
		for (Double value : this.values()) {
			if (value != null) {
				result += value;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Node<E>> toList() {
		List<Node<E>> result;

		result = new ArrayList<Node<E>>(this.keySet());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Node<E>> toListSortedByLabel() {
		List<Node<E>> result;

		result = toList();
		Collections.sort(result, new NodeComparatorByLabel<E>());

		//
		return result;
	}

}
