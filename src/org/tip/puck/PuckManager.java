package org.tip.puck;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.io.bar.BARODSFile;
import org.tip.puck.io.bar.BARTXTFile;
import org.tip.puck.io.bar.BARXLSFile;
import org.tip.puck.io.ged.GEDFile;
import org.tip.puck.io.iur.IURODSFile;
import org.tip.puck.io.iur.IURTXTFile;
import org.tip.puck.io.iur.IURXLSFile;
import org.tip.puck.io.ods.ODSFile;
import org.tip.puck.io.paj.PAJFile;
import org.tip.puck.io.permutation.PermutationFile;
import org.tip.puck.io.permutation.PermutationTable;
import org.tip.puck.io.pl.PLFile;
import org.tip.puck.io.puc.PUCFile;
import org.tip.puck.io.tip.TIPFile;
import org.tip.puck.io.txt.TXTFile;
import org.tip.puck.io.xls.XLSFile;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.roles.RoleRelationRules;
import org.tip.puck.net.relations.roles.RoleRelationsTXTFile;
import org.tip.puck.net.relations.roles.RoleRelationsXLSFile;
import org.tip.puck.net.relations.roles.RoleRelationsXMLFile;
import org.tip.puck.net.relations.roles.io.EMICFile;
import org.tip.puck.net.relations.roles.io.ETICFile;
import org.tip.puck.net.relations.roles.io.TermGenealogyFile;
import org.tip.puck.net.relations.roles.io.TermStandardFile;
import org.tip.puck.net.workers.UpdateWorker;
import org.tip.puck.net.workers.UpdateWorker.UpdateMode;
import org.tip.puck.report.Report;
import org.tip.puck.report.ReportTXTFile;
import org.tip.puck.report.ReportXLSFile;

/**
 * 
 * @author TIP
 * 
 */
public class PuckManager {

	private static final Logger logger = LoggerFactory.getLogger(PuckManager.class);

	public static String DEFAULT_CHARSET_NAME = "UTF-8";

	/**
	 * 
	 * @param target
	 * @param sourceFile
	 * @param concordanceFile
	 * @return
	 * @throws PuckException
	 */
	public static Net fuseNet(final Net target, final File sourceFile, final File concordanceFile, final Report errorReport) throws PuckException {
		Net result;

		if ((target == null) || (sourceFile == null)) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter.");

		} else {
			//
			Net source = PuckManager.loadNet(sourceFile);

			//
			PermutationTable permutations = PermutationFile.load(concordanceFile);

			//
			result = UpdateWorker.fuse(target, source, permutations, errorReport);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param pathname
	 * @return
	 * @throws PuckException
	 * @throws Exception
	 */
	public static Net loadNet(final File file) throws PuckException {
		Net result;

		result = loadNet(file, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * 
	 * @param pathname
	 * @return
	 * @throws PuckException
	 * @throws Exception
	 */
	public static Net loadNet(final File file, final String charsetName) throws PuckException {
		Net result;

		if (file == null) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("File=[" + file + "].");

		} else if (!file.exists()) {
			//
			throw PuckExceptions.FILE_NOT_FOUND.create("File=[" + file + "].");

		} else if (!file.isFile()) {
			//
			throw PuckExceptions.NOT_A_FILE.create("File=[" + file + "].");

		} else {
			String fileNameLowerCase = StringUtils.lowerCase(file.getName());

			if (fileNameLowerCase == null) {

				throw PuckExceptions.UNSUPPORTED_FILE_FORMAT.create("File=[" + file + "].");

			} else if (fileNameLowerCase.endsWith(".ged")) {
				//
				result = GEDFile.load(file, charsetName);

			} else if (fileNameLowerCase.endsWith(".iur.txt")) {

				result = IURTXTFile.load(file, charsetName);

			} else if (fileNameLowerCase.endsWith(".iur.ods")) {

				result = IURODSFile.load(file);

			} else if (fileNameLowerCase.endsWith(".iur.xls")) {

				result = IURXLSFile.load(file);

			} else if (fileNameLowerCase.endsWith(".bar.txt")) {

				result = BARTXTFile.load(file, charsetName);

			} else if (fileNameLowerCase.endsWith(".bar.ods")) {

				result = BARODSFile.load(file);

			} else if (fileNameLowerCase.endsWith(".bar.xls")) {

				result = BARXLSFile.load(file);

			} else if (fileNameLowerCase.endsWith(".ods")) {
				//
				result = ODSFile.load(file);

			} else if (fileNameLowerCase.endsWith(".paj")) {
				//
				result = PAJFile.load(file, charsetName);

			} else if (fileNameLowerCase.endsWith(".pl")) {
				//
				result = PLFile.load(file, charsetName);

			} else if (fileNameLowerCase.endsWith(".puc")) {
				//
				result = PUCFile.load(file);

			} else if (fileNameLowerCase.endsWith(".tip")) {
				//
				result = TIPFile.load(file, charsetName);

			} else if (fileNameLowerCase.endsWith(".txt")) {
				//
				result = TXTFile.load(file, charsetName);

			} else if (fileNameLowerCase.endsWith(".txt3")) {
				// In past, IUR format was named TXT version 3, TXT3.
				result = IURTXTFile.load(file, charsetName);

			} else if (fileNameLowerCase.endsWith(".xls")) {
				//
				result = XLSFile.load(file);

			} else {
				throw PuckExceptions.UNSUPPORTED_FILE_FORMAT.create("File=[" + file + "].");
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param pathname
	 * @return
	 * @throws PuckException
	 * @throws Exception
	 */
	public static RelationModel loadRelationModel(final File file) throws PuckException {
		RelationModel result;

		result = loadRelationModel(file, new Report("/dev/null"), null);

		//
		return result;
	}

	/**
	 * 
	 * @param pathname
	 * @return
	 * @throws PuckException
	 * @throws Exception
	 */
	public static RelationModel loadRelationModel(final File file, final Report report, final RoleRelationRules ruleConstraints) throws PuckException {
		RelationModel result;

		if (file == null) {

			throw PuckExceptions.INVALID_PARAMETER.create("File=[" + file + "].");

		} else if (!file.exists()) {

			throw PuckExceptions.FILE_NOT_FOUND.create("File=[" + file + "].");

		} else if (!file.isFile()) {

			throw PuckExceptions.NOT_A_FILE.create("File=[" + file + "].");

		} else {

			String name = StringUtils.lowerCase(file.getName());

			if (name == null) {
				//
				throw PuckExceptions.UNSUPPORTED_FILE_FORMAT.create("File=[" + file + "].");

			} else if (name.matches("^.+\\.term\\.(ods|txt|xls)$")) {
				//
				result = TermStandardFile.load(file, report, ruleConstraints);
				logger.debug("report [status={}][isEmpty={}]", report.status(), report.outputs().isEmpty());
				logger.debug("report output={}", report.outputs());

			} else if (name.matches("^.+\\.term\\.(puc|iur\\.ods|iur\\.txt|iur\\.xls)$")) {
				//
				result = TermGenealogyFile.load(file, report);
				logger.debug("report [status={}][isEmpty={}]", report.status(), report.outputs().isEmpty());
				logger.debug("report output={}", report.outputs());

			} else if (name.matches("^.+\\.etic\\.(ods|txt|xls)$")) {
				//
				result = ETICFile.load(file, report, ruleConstraints);
				logger.debug("report [status={}][isEmpty={}]", report.status(), report.outputs().isEmpty());
				logger.debug("report output={}", report.outputs());

			} else if (name.matches("^.+\\.emic\\.(ods|txt|xls)$")) {
				//
				result = EMICFile.load(file, report, ruleConstraints);
				logger.debug("report [status={}][isEmpty={}]", report.status(), report.outputs().isEmpty());
				logger.debug("report output={}", report.outputs());

			} else {
				throw PuckExceptions.UNSUPPORTED_FILE_FORMAT.create("File=[" + file + "].");
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static Net newEmptyNet() {
		Net result;

		result = null;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static Net newRandomNet() {
		Net result;

		result = null;

		//
		return result;
	}

	/**
	 * 
	 * @param pathname
	 * @param net
	 * @throws PuckException
	 */
	public static void saveNet(final File targetFile, final Net net) throws PuckException {

		try {
			//
			if (targetFile == null) {
				//
				throw PuckExceptions.INVALID_PARAMETER.create("File=[" + targetFile + "].");

			} else if (targetFile.isDirectory()) {
				//
				throw PuckExceptions.NOT_A_FILE.create("File=[" + targetFile + "].");

			} else {
				// Define temporary target.
				File temporaryFile = new File(targetFile.getAbsolutePath() + "-tmp");
				temporaryFile.delete();

				// Save.
				String fileNameLowerCase = StringUtils.lowerCase(targetFile.getName());
				if (fileNameLowerCase == null) {

					throw PuckExceptions.UNSUPPORTED_FILE_FORMAT.create("File=[" + targetFile + "].");

				} else if (fileNameLowerCase.endsWith(".ged")) {
					//
					GEDFile.save(temporaryFile, net);

				} else if (fileNameLowerCase.endsWith(".iur.ods")) {
					//
					IURODSFile.save(temporaryFile, net);
					// ODSWriter adds ".ods" extension systematically.
					temporaryFile = new File(temporaryFile.getAbsolutePath() + ".ods");

				} else if (fileNameLowerCase.endsWith(".iur.txt")) {
					//
					IURTXTFile.save(temporaryFile, net);

				} else if (fileNameLowerCase.endsWith(".iur.xls")) {
					//
					IURXLSFile.save(temporaryFile, net);

				} else if (fileNameLowerCase.endsWith(".bar.ods")) {
					//
					BARODSFile.save(temporaryFile, net);
					// ODSWriter adds ".ods" extension systematically.
					temporaryFile = new File(temporaryFile.getAbsolutePath() + ".ods");

				} else if (fileNameLowerCase.endsWith(".bar.txt")) {
					//
					BARTXTFile.save(temporaryFile, net);

				} else if (fileNameLowerCase.endsWith(".bar.xls")) {
					//
					IURXLSFile.save(temporaryFile, net);

				} else if (fileNameLowerCase.endsWith(".ods")) {
					//
					IURODSFile.save(temporaryFile, net);
					// ODSWriter adds ".ods" extension systematically.
					temporaryFile = new File(temporaryFile.getAbsolutePath() + ".ods");

				} else if (fileNameLowerCase.endsWith(".pl")) {
					//
					PLFile.save(temporaryFile, net);

				} else if (fileNameLowerCase.endsWith(".puc")) {
					//
					PUCFile.save(temporaryFile, net, "PUCK");

				} else if (fileNameLowerCase.endsWith(".tip")) {
					//
					TIPFile.save(temporaryFile, net);

				} else if (fileNameLowerCase.endsWith(".txt")) {
					//
					IURTXTFile.save(temporaryFile, net);

				} else if (fileNameLowerCase.endsWith(".xls")) {
					//
					IURXLSFile.save(temporaryFile, net);

				} else {

					throw PuckExceptions.UNSUPPORTED_FILE_FORMAT.create("File=[" + targetFile + "].");
				}

				// Backup existing file.
				File backupFile = new File(targetFile.getAbsolutePath() + ".bak");
				if (targetFile.exists()) {
					backupFile.delete();
					boolean succeed = targetFile.renameTo(backupFile);
					if (!succeed) {
						logger.warn("Backup failed for [" + targetFile + "][" + backupFile + "]");
					}
				}

				// Rename temporary file.
				boolean succeed = temporaryFile.renameTo(targetFile);
				if (!succeed) {
					logger.error("Error renaming temporary file from [" + temporaryFile + "] to [" + targetFile + "]: ");

					logger.error("First, retry after delay.");
					try {
						Thread.sleep(2000);
					} catch (InterruptedException exception) {
					}
					succeed = temporaryFile.renameTo(targetFile);

					logger.error("Rename retry result=" + succeed);
					if (!succeed) {
						logger.error("Second, try to restore bak file.");
						boolean succeedRestore = backupFile.renameTo(targetFile);
						logger.error("Bak file restore result=" + succeedRestore);
						logger.error("Last, throwing.");
						if (succeedRestore) {
							throw PuckExceptions.IO_ERROR.create("Can't rename temporary file=[" + temporaryFile + "].");
						} else {
							throw PuckExceptions.IO_ERROR
									.create("Several rename file failed. Please retry later.\nYour origninal file has now the following name=[" + backupFile
											+ "].");
						}
					}
				}
			}
		} catch (final SecurityException exception) {
			exception.printStackTrace();
			logger.error("Permissions denied: " + exception.getMessage());
			throw PuckExceptions.IO_ERROR.create("Permission denied: " + exception.getMessage(), exception);
		}
	}

	/**
	 * 
	 * @param pathname
	 * @param net
	 * @throws PuckException
	 * @throws IOException
	 */
	public static Report saveRelationModel(final File file, final RelationModel model) throws PuckException, IOException {
		Report result;

		if (file == null) {

			throw PuckExceptions.INVALID_PARAMETER.create("File=[" + file + "].");

		} else if (file.isDirectory()) {
			//
			throw PuckExceptions.NOT_A_FILE.create("File=[" + file + "].");

		} else if (file.getName().toLowerCase().endsWith(".txt")) {
			//
			result = RoleRelationsTXTFile.save(file, model);

		} else if (file.getName().toLowerCase().endsWith(".xls")) {
			//
			result = RoleRelationsXLSFile.save(file, model);

		} else if (file.getName().toLowerCase().endsWith(".xml")) {
			//
			result = RoleRelationsXMLFile.save(file, model);

		} else {
			//
			throw PuckExceptions.UNSUPPORTED_FILE_FORMAT.create("File=[" + file + "].");
		}
		//
		return result;
	}

	/**
	 * 
	 * @param pathname
	 * @param net
	 * @throws PuckException
	 */
	public static void saveReport(final File file, final Report net) throws PuckException {

		if (file == null) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("File=[" + file + "].");

		} else if (file.isDirectory()) {
			//
			throw PuckExceptions.NOT_A_FILE.create("File=[" + file + "].");

		} else if (file.getName().toLowerCase().endsWith(".txt")) {
			//
			ReportTXTFile.save(file, net);

		} else if (file.getName().toLowerCase().endsWith(".xls")) {
			//
			ReportXLSFile.save(file, net);

		} else {
			//
			throw PuckExceptions.UNSUPPORTED_FILE_FORMAT.create("File=[" + file + "].");
		}
	}

	/**
	 * 
	 * @param net
	 * @param file
	 * @throws PuckException
	 */
	public static Net updateNetAppending(final Net target, final File file, final Report errorReport) throws PuckException {
		Net result;

		if ((target == null) || (file == null)) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter.");

		} else {
			//
			Net source = PuckManager.loadNet(file);

			//
			result = UpdateWorker.update(target, source, errorReport, UpdateMode.APPEND);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @param file
	 * @throws PuckException
	 */
	public static Net updateNetOverwriting(final Net target, final File file, final Report errorReport) throws PuckException {
		Net result;

		if ((target == null) || (file == null)) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter.");

		} else {
			//
			Net source = PuckManager.loadNet(file);

			//
			result = UpdateWorker.update(target, source, errorReport, UpdateMode.OVERWRITE);
		}

		//
		return result;
	}
}
