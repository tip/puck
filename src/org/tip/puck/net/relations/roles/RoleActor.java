package org.tip.puck.net.relations.roles;

import org.tip.puck.net.AlterAge;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Attributes;
import org.tip.puck.net.Gender;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.relations.Roles;
import org.tip.puck.net.relations.roles.RoleActorPair.Adjustable;
import org.tip.puck.sequences.EgoSequence;
import org.tip.puck.util.Numberable;

// Transform into extension of Actor<E>

/**
 * 
 * @author TIP
 */
public class RoleActor implements Comparable<RoleActor>, Numberable {

	private Role individual;
	private MetaRole role;
	private int id;
	String selfName;
	
	private Attributes attributes;
	
	
	public RoleActor clone(){
		RoleActor result;
		
		result = new RoleActor (individual, role.clone(),selfName);
		
		//
		return result;
	}
	
	public RoleActor clone(int id){
		RoleActor result;
		
		result = clone();
		result.setId(id);
		
		//
		return result;
	}
	
	public RoleActor cloneAs(String roleName){
		RoleActor result;
		
		result = clone();
		result.getRole().setName(roleName);
		
		//
		return result;

	}
	
	/**
	 * 
	 * @param individual
	 * @param role
	 */
	public RoleActor(final Role individual, final MetaRole role, String selfName) {
		//
		this.individual = individual;
		this.selfName = selfName;
		this.individual.setSelfName(selfName);
		this.role = role;
		this.attributes = new Attributes();
	}
	
	/**
	 * 
	 */
	@Override
	public boolean equals(final Object actor) {
		return this.role.equals(((RoleActor) actor).role) && this.individual.equals(((RoleActor) actor).individual);

	}
	
	boolean specifies(final RoleActor other){
		boolean result;
		
		result = this.individual.equals(other.getIndividual()) && this.role.specifies(other.role);
		
		//
		return result;
	}

	boolean specifiesEgoGender(final RoleActor other){
		boolean result;
		
		result = this.individual.equals(other.getIndividual()) && this.role.getEgoGender().specifies(other.role.getEgoGender());
		
		//
		return result;
	}

	boolean specifiesAbsolute(final RoleActor other){
		boolean result;
		
		result = this.individual.equals(other.getIndividual()) && this.role.specifiesAbsolute(other.role);
		
		//
		return result;
	}
	
	boolean matchesAbsolute(final RoleActor other){
		boolean result;
		
		result = this.individual.equals(other.getIndividual()) && this.role.matchesAbsolute(other.role);
	
		//
		return result;
	}

	
	/**
	 * 
	 * @return
	 */
	public int getId() {
		return id;
	}

	public Role getIndividual() {
		return this.individual;
	}

	/**
	 * 
	 * @return
	 */
	public String getName() {
		String result;

		result = this.individual.getName();

		//
		return result;
	}

	public MetaRole getRole() {
		return this.role;
	}

	public void setIndividual(final Role individual) {
		this.individual = individual;
	}

	public void setRole(final MetaRole role) {
		this.role = role;
	}
	
	/**
	 * 
	 * @return
	 */
	public Attributes attributes() {
		Attributes result;

		result = this.attributes;

		//
		return result;
	}
	
	public void setAttribute(final String label, final String value) {
		this.attributes.put(label, value);
	}

	public String toString(){
		return individual+" ["+role+"]";
	}
	
	/**
	 * 
	 */
	public String getAttributeValue(final String label) {
		String result;

		Attribute attribute = this.attributes().get(label);
		if (attribute == null) {
			result = null;
		} else {
			result = attribute.getValue();
		}

		//
		return result;
	}
	
	public int compareTo (RoleActor other){
		int result;
		
		if (this.hasSelfName() && !other.hasSelfName()){
			result = -1;
		} else if (!this.hasSelfName() && other.hasSelfName()){
			result = 1;
		} else {
			result = this.individual.compareTo(other.individual);
			
			if (result == 0){
				result = this.role.compareTo(other.role);
			}
		}
		//
		return result;
	}
	
	public boolean equalsAbsolute (RoleActor other){
		boolean result;
		
		result = this.absolute().equals(other.absolute());
		
		//
		return result;
	}
	
	public RoleActor absolute(){
		RoleActor result;
		
		result = new RoleActor(individual,role.absolute(),selfName);
		
		//
		return result;
	}
	
	RoleActor withEgoGender(Gender egoGender){
		RoleActor result;
		
		if (!getEgoGender().isUnknown() || egoGender.isUnknown()) {
			
			result = this;
			
		} else {
			
			result = clone();
			
			result.getRole().setEgoGender(egoGender);
			
			if (role.isCross()){
				result.getRole().setAlterGender(egoGender.invert());
			}
			
			if (result.hasSelfName()){
				result.getRole().setAlterGender(egoGender);
			}
		}
		//
		return result;

	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String hashKey() {
		return toString();
	}
	
	public int hashCode(){
		return toString().hashCode();
	}
	

	boolean isHiddenCross(){
		boolean result;
		
		result = role.isCross() && getEgoGender().isUnknown() && getAlterGender().isUnknown();
		
		//
		return result;
	}
	
	boolean hasCorrectGender (RoleActor self, RoleRelations relations){
		boolean result;
		
		result = (
				!(hasSelfName() && getEgoGender()!=getAlterGender()) && 
				!(relations.heteroMarriage() && isSpouse() && !getAlterGender().matchs(self.getAlterGender().invert())));
		//
		return result;
		
	}
	
/*	boolean isValidFor (RoleActor self, RoleRelations relations){
		boolean result;
		
		result = (!isSpouse() || !relations.heteroMarriage() || getAlterGender().matchs(self.getAlterGender().invert()));
	
		//
		return result;
	}

	
	public boolean isValid(Map<Role,Roles> genderConfigs){
		boolean result;
		
		result = true;
		
		if (genderConfigs!=null && genderConfigs.get(individual)!=null){
			
			for (Role standardRole : genderConfigs.get(individual)){
				
				if (standardRole.getName().equals(role.getName())){
					
					result = false;
					
					if (role.specifies((MetaRole)standardRole)){
						result = true;
						break;
					}
					
				} 
			}
			
			boolean cross = isUniqueCross(genderConfigs.get(individual));
			
			if (cross && role.isCross()){							
				result = true;
			} else if (cross && !role.isCross()){
				result = false;
			}
		}
		
		//
		return result;
	}*/
	
	public Gender getEgoGender(){
		return role.getEgoGender();
	}
	
	public Gender getAlterGender(){
		return role.getAlterGender();
	}
	
	public AlterAge getAlterAge(){
		return role.getAlterAge();
	}
	
	RoleActor egoGenderNeutral(){
		RoleActor result;
		
		result = clone();
		result.getRole().setEgoGender(Gender.UNKNOWN);
		//
		return result;
	}
	
	RoleActor egoGenderComplement(){
		RoleActor result;
		
		MetaRole egoGenderComplement = role.egoGenderComplement();
		
		if (egoGenderComplement==null){
			
			result = null;
			
		} else {
			
			result = clone();
			result.setRole(egoGenderComplement);
		}
		//
		return result;
	}
	
	RoleActor asSiblingOf(RoleActor alter, RoleRelations relations){
		RoleActor result;

		result = clone();

		if (!alter.isPersonal(relations)){

			AlterAge alterAge = getUniqueAlterAge(relations.getGenderConfigs().get(alter.getIndividual()),"SIBLING",getEgoGender());

			if (!isPersonal(relations)){
				
				AlterAge selfAge = getUniqueAlterAge(relations.getGenderConfigs().get(result.getIndividual()),"SIBLING",getEgoGender());
				
				if (selfAge==alterAge.invert()){
				
					result.getRole().setAlterAge(selfAge);
					
				} else {
					
					result.getRole().setAlterAge(AlterAge.UNKNOWN);

				}

			} else {

				result.getRole().setAlterAge(alterAge.invert());
			}
		}
		//
		return result;
	}
	
/*	public RoleActor adjustedAsAlter (RoleActor self, RoleRelations relations){
		RoleActor result;
		
		result = clone();
				
		if (self.isHiddenCross()){
			
			result.setEgoGender(result.getAlterGender().invert());
			
		} else {
			
			result = result.withEgoGender(self.getEgoGender());
		}
		
		if (isUniqueCross(relations.getGenderConfigs().get(individual)) && result.getAlterGender().isUnknown()){
			
			result.setAlterGender(result.getEgoGender().invert());
		}
		
		result.setAlterAge(getUniqueAlterAge(relations.getGenderConfigs().get(individual),role.getName(),getEgoGender()));
		
		if (result.isSibling()){
			
			result = result.asSiblingOf(self, relations);
			
		} else if (result.isSpouse()){
			
			result = result.asSpouseOf(self,relations);
		}
		//
		return result;
	}
	
	public RoleActor adjustedAsSelf (RoleActor alter, RoleRelations relations){
		RoleActor result;
		
		result = clone();
		
		result = result.withEgoGender(alter.getEgoGender());

		if (alter.isSpouse()){
			
			result = result.asSpouseOf(alter,relations);
		}
		
		result = result.asSelf(relations);
		
		//
		return result;
	}*/
	
	RoleActor withAlterGender(RoleRelations relations) {
		RoleActor result;
		
		if (getAlterGender()!=null){
			
			result = this;
			
		} else {
			
			result = clone();
			
			if (hasSelfName()){
				
				result.setAlterGender(Gender.UNKNOWN);
				
			} else {
				
				Roles configs = relations.getGenderConfigs().get(individual);

				if (isUniqueCross(configs)){
			
					result.setAlterGender(getEgoGender().invert());

					if (getEgoGender().isUnknown()){
				
						result.getRole().setCross(true);
					}

				} else {
			
					result.setAlterGender(getUniqueAlterGender(configs, role.getName(), getEgoGender()));
				}
			}
		}
		//
		return result;
	}
	
	RoleActor withAlterGender (Gender alterGender){
		RoleActor result;
		
		result = clone();
		
		result.getRole().setAlterGender(alterGender);

		Gender egoGender = Gender.UNKNOWN;
		
		if (result.getRole().isCross()){
			egoGender = alterGender.invert();
		}
		
		if (result.hasSelfName()){
			egoGender = alterGender;
		}
		
		if (!egoGender.matchs(result.getEgoGender())){
			
			result = null;
			
		} else if (!egoGender.isUnknown()){
			
			result.getRole().setEgoGender(egoGender);
		}
		//
		return result;
	}
	
	/**
	 * Changes the role to self and extends the alter gender  according to the initial definitions (if no definitions are given,
	 * the alter gender is set unknown
	 * Needed at the initial importation where alter genders of reciprocal terms are not directly accessible
	 * @param self
	 * @param genderConfigs
	 * @return
	 */
	RoleActor asSelf(RoleRelations relations){
		RoleActor result;
		
		if (isSelf()){
			
			result = this;
			
		} else {

			result = cloneAs("SELF");
			result.setAlterAge(AlterAge.UNKNOWN);
			
			if (result.getAlterGender()==null){
				
				// The first two adjustments are redundant if embedded in coupled adjustment of self and alter
				if (hasSelfName()){
					
					result.setAlterGender(result.getEgoGender());
					
				} else if (result.getRole().isCross()){
					
					result.setAlterGender(result.getEgoGender());
					
				// Adjustment to general configurations
				} else {
					
					result = result.withAlterGender(relations);
				}
			}
		}
		
		//
		return result;
	}
	
	static boolean isUniqueCross (Roles configs){
		boolean cross;
		
		cross = true;
		
		for (Role standardRole : configs){
			
			if (!((MetaRole)standardRole).isCross()) {
				cross = false;
				break;
			}
		}
		//
		return cross;
	}
	
	static AlterAge getUniqueAlterAge (Roles configs, String name, Gender egoGender){
		AlterAge result;
				
		result = null;
		
		// Check for corresponding roles
		
		if (name!=null && configs!=null){
			
			for (Role role : configs){
				
				MetaRole metaRole = (MetaRole)role;
				
				if (metaRole.getName().equals(name) && egoGender.specifies(metaRole.getEgoGender())){
					
					if (result==null){
						
						result = metaRole.getAlterAge();
						
					} else if (!result.equals(metaRole.getAlterAge())){

						result = null;
						break;
					}
				}
			}
		}
		
		if (result == null && configs!=null){
			
			for (Role role : configs){
				MetaRole metaRole = (MetaRole)role;
				if (result==null){
					result = metaRole.getAlterAge();
				} else if (!result.equals(metaRole.getAlterAge())){
					result = null;
					break;
				} 
			}
		}
		
		if (result==null){
			result = AlterAge.UNKNOWN;
		}

		//
		return result;
	}
	
	static Gender getUniqueAlterGender (Roles configs, String name, Gender egoGender){
		Gender result;
				
		result = null;
				
		// Check for corresponding roles
		
		if (name!=null && configs!=null){
			
			for (Role role : configs){
				
				MetaRole metaRole = (MetaRole)role;
				
				if (metaRole.getName().equals(name) && egoGender.specifies(metaRole.getEgoGender())){
					
					if (result==null){
						
						result = metaRole.getAlterGender();
						
					} else if (!result.equals(metaRole.getAlterGender())){

						result = null;
						break;
					}
				}
			}
		}
		
		if (result == null && configs!=null){
			
			boolean cross = true;
			
			for (Role role : configs){
				MetaRole metaRole = (MetaRole)role;
				if (!metaRole.isCross()){
					cross = false;
				}
				if (result==null){
					result = metaRole.getAlterGender();
				} else if (!result.equals(metaRole.getAlterGender())){
					if (cross){
						result = egoGender.invert();
					} else {
						result = null;
						break;
					}
				} 
			}
		}
		
		if (result==null){
//			// Equivocal alter gender for different ego genders;
			result = Gender.UNKNOWN;
		}
			
		//
		return result;
	}
	
	RoleActor withAge (RoleRelations relations){
		RoleActor result;
		
		result = clone();
		result.setAlterAge(getUniqueAlterAge(relations.getGenderConfigs().get(individual),role.getName(),getEgoGender()));
		
		return result;
	}
	
	RoleActor withCrossness(RoleRelations relations){
		RoleActor result;
		
		if (hasSelfName() || getRole().isCross() || !isUniqueCross(relations.getGenderConfigs().get(individual))){
			
			result = this;
			
		} else {
			
			result = clone();
			
			if (!result.getEgoGender().isUnknown() && (result.getAlterGender()==null || result.getAlterGender().isUnknown())){
				
				result.setAlterGender(result.getEgoGender().invert());
				
			} else if (result.getAlterGender()!=null && !result.getAlterGender().isUnknown() && result.getEgoGender().isUnknown()){
				
				result.setEgoGender(result.getAlterGender().invert());
				
			} else if (result.getEgoGender().isUnknown() && result.getAlterGender()==null || result.getAlterGender().isUnknown()){
				
				result.getRole().setCross(true);
				
			} else {
				
				// Invalid cross gender configuration
//				System.err.println(" Invalid cross gender configuration "+this);
				result = null;
			}
		}
		//
		return result;
	}

	
	RoleActor asAlterOf (RoleActor self, RoleRelations relations){
		RoleActor result;
		
		result = new RoleActorPair(relations,self,this,Adjustable.NONE).getAlter();
		//
		return result;
	}
	
	RoleActor asReciprocalOf (RoleActor alter, RoleRelations relations){
		RoleActor result;

		result = cloneAs(alter.getRole().invertName()).asAlterOf(alter, relations);
		
		//
		return result;
	}

	
/*	public RoleActor getReciprocal (RoleActor self, RoleRelations relations){
		RoleActor result;

		result = self.cloneAs(getRole().invertName());
		
		//
		return result;
	}

	
	public RoleActor getReciprocal1 (RoleActor self, RoleRelations relations){
		RoleActor result;
				
		Map<Role,Roles> genderConfigs = relations.getGenderConfigs();
		
		Roles configs = relations.getGenderConfigs().get(self.getIndividual());
		String roleName = getRole().invertName();
		
		Gender egoGender = getEgoGender();
		Gender alterGender = null;
		AlterAge alterAge = null;

		
		if (self.hasSelfName()){
			
			alterGender = egoGender;
			alterAge = AlterAge.UNKNOWN;
						
		} else {
			
			if (isUniqueCross(configs)){
				
				alterGender = egoGender.invert();
				
			} else {
				
				alterGender = getUniqueAlterGender(configs, roleName, egoGender);
				
			}
		}
		
		if (alterAge==null){
			alterAge = getUniqueAlterAge(configs, roleName, egoGender);
		}
		
		//
		MetaRole inverseRole = new MetaRole(roleName,egoGender,alterGender,alterAge);
//		inverseRole.setCross(role.cross);
		
		result = new RoleActor(self.getIndividual(), inverseRole).adjustedAsAlter(this, relations);
		result.adjustCrossness(genderConfigs);

		//
		return result;
	}
	

	public boolean isPersonalParent(RoleRelations relations){
		boolean result;
		
		result = false;
		
		for (RoleActor alter : getActorsByRoleNameWithUnspecific("CHILD", relations)){
			if (alter.hasSelfName()){
				result = true;
				break;
			}
		}
		//
		return result;
	}*/
	
	/**
	 * Check for direct child or spouse links to ego
	 * @param relations
	 * @param actor
	 * @return
	 */
	boolean isPersonal(RoleRelations relations){
		boolean result;
		
		result = false;
		
		for (RoleActor alter : getActorsByRoleNameWithUnspecific(null, relations)){
			if (alter.hasSelfName() && (alter.isSelf() || alter.isSpouse() || alter.isChild())){
				result = true;
				break;
			}
		}
		//
		return result;
	}
	
	boolean hasSelfName(){
		return hasName(selfName);
	}

	public boolean isParent(){
		return role.isParent();
	}
	
	public boolean isChild(){
		return role.isChild();
	}
	
	public boolean isSpouse(){
		return role.isSpouse();
	}
	
	public boolean isSibling(){
		return role.isSibling();
	}
	
	public boolean isSelf(){
		return role.isSelf();
	}
	
	public RoleActors getActorsByRoleNameWithUnspecific(String roleName, RoleRelations relations){
		RoleActors result;
		
		result = new RoleActors();
				
		for (RoleRelation selfRelation : relations.getBySelf(this)){
			
			for (RoleActor actor : selfRelation.getActorsByRoleName(roleName)){
				
				result.addNew(actor.clone());
			}
		}

		//
		return result;
	}
	
	public void setEgoGender(Gender egoGender){
		
		role.setEgoGender(egoGender);
	}
	
	public void setAlterGender(Gender alterGender){
		
		role.setAlterGender(alterGender);
	}
	
	public void setAlterAge(AlterAge alterAge){
		
		role.setAlterAge(alterAge);
	}
	
	RoleActors crossExtension(){
		RoleActors result;
		
		result = new RoleActors();
		
		if (role.isCross()){
			
			if (!getAlterGender().isUnknown()){
				result.add(this);
			} else {
				result.add(this.withEgoGender(Gender.FEMALE));
				result.add(this.withEgoGender(Gender.MALE));
			}
			
		}
		//
		return result;
	}
	
	public boolean hasName (String name){
		return getName().equals(name);
	}
	
	boolean isFirstAlter(RoleRelation relation){
		
		return (relation!=null && relation.getActors().indexOf(this)==1);
	}

	public String getSelfName() {
		return selfName;
	}

	public void setSelfName(String selfName) {
		this.selfName = selfName;
	}
	
	
	
}
