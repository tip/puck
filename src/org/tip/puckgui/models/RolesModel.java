package org.tip.puckgui.models;

import java.util.ResourceBundle;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.relations.Roles;

/**
 * 
 * @author TIP
 */
public class RolesModel extends AbstractTableModel {

	private static final long serialVersionUID = 2218630487173690426L;

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.tip.puckgui.messages");

	protected Roles source;
	protected Roles delegation;

	/**
	 * 
	 */
	public RolesModel(final Roles source) {
		super();
		setSource(source);
	}

	/**
	 * 
	 */
	@Override
	public Class getColumnClass(final int columnIndex) {
		Class result;

		if (this.source == null) {
			result = String.class;
		} else {
			switch (columnIndex) {
				case 0:
					result = String.class;
				break;
				case 1:
					result = String.class;
				break;
				default:
					result = String.class;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getColumnCount() {
		int result;

		result = 2;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String getColumnName(final int columnIndex) {
		String result;

		switch (columnIndex) {
			case 0:
				result = "Role";
			break;
			case 1:
				result = "Cardinality";
			break;
			default:
				result = "";
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getRowCount() {
		int result;

		result = this.delegation.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Roles getSource() {
		Roles result;

		result = this.source;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Roles getTarget() {
		Roles result;

		result = this.delegation;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Object getValueAt(final int rowIndex, final int columnIndex) {
		Object result;

		if (this.delegation == null) {
			result = null;
		} else {
			switch (columnIndex) {
				case 0:
					result = this.delegation.get(rowIndex).getName();
				break;
				case 1:
					if (StringUtils.isBlank(this.delegation.get(rowIndex).getName())) {
						result = "";
					} else {
						result = this.delegation.get(rowIndex).getDefaultCardinality();
					}
				break;
				default:
					result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public boolean isCellEditable(final int rowIndex, final int columnIndex) {
		boolean result;

		result = true;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void setSource(final Roles source) {
		//
		this.source = source;
		if (source == null) {
			this.delegation = new Roles();
		} else {
			this.delegation = source;
		}

		//
		this.delegation.add(new Role("", 0));

		//
		fireTableDataChanged();
	}

	/**
	 * 
	 */
	@Override
	public void setValueAt(final Object value, final int rowIndex, final int columnIndex) {
		//
		Role currentRole = this.delegation.get(rowIndex);
		if (columnIndex == 0) {

			if (this.delegation.getByName((String) value) == null) {
				currentRole.setName((String) value);
			} else {
				JOptionPane.showMessageDialog(null, "Role already existing", "Input Error", JOptionPane.ERROR_MESSAGE);
			}
		} else if ((columnIndex == 1) && (NumberUtils.isDigits((String) value))) {
			currentRole.setDefaultCardinality(Integer.parseInt((String) value));
		}

		//
		if ((rowIndex == this.delegation.size() - 1) && (StringUtils.isNotBlank(this.delegation.get(rowIndex).getName()))) {
			//
			this.delegation.add(new Role("", 0));

			//
			fireTableDataChanged();
		}
	}
}
