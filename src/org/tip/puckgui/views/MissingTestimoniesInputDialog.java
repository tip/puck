package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.RelationModels;
import org.tip.puck.net.relations.Role;
import org.tip.puck.sequences.workers.MissingTestimoniesCriteria;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class MissingTestimoniesInputDialog extends JDialog {

	private static final long serialVersionUID = -6081828976427430274L;

	private static final Logger logger = LoggerFactory.getLogger(MissingTestimoniesInputDialog.class);

	private RelationModels relationModels;

	private final JPanel contentPanel = new JPanel();
	private MissingTestimoniesCriteria dialogCriteria;
	private static MissingTestimoniesCriteria lastCriteria = new MissingTestimoniesCriteria();
	private JComboBox cmbbxRelationModel;
	private JComboBox cmbbxEgoRole;
	private JButton okButton;

	/**
	 * Create the dialog.
	 */
	public MissingTestimoniesInputDialog(final RelationModels relationModels) {
		super();

		this.relationModels = relationModels;

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Missing Testimonies Input Dialog");
		setIconImage(Toolkit.getDefaultToolkit().getImage(MissingTestimoniesInputDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				MissingTestimoniesInputDialog.this.dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 420, 157);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("max(100dlu;default):grow"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));
		{
			JLabel lblRelationModel = new JLabel("Relation Model:");
			this.contentPanel.add(lblRelationModel, "2, 2, right, default");
		}
		{
			this.cmbbxRelationModel = new JComboBox(relationModels.nameList().toArray());
			this.cmbbxRelationModel.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(final ItemEvent event) {
					// Update label list.
					//
					if (event.getStateChange() == ItemEvent.SELECTED) {
						//
						setRelationModel(MissingTestimoniesInputDialog.this.cmbbxRelationModel.getSelectedIndex(), null);
					}
				}
			});
			this.contentPanel.add(this.cmbbxRelationModel, "4, 2, fill, default");
		}
		{
			JLabel lblEgoRole = new JLabel("Ego Role:");
			this.contentPanel.add(lblEgoRole, "2, 4, right, default");
		}
		{
			this.cmbbxEgoRole = new JComboBox();
			this.contentPanel.add(this.cmbbxEgoRole, "4, 4, fill, default");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						MissingTestimoniesInputDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				this.okButton = new JButton("Launch");
				this.okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						MissingTestimoniesCriteria criteria = getCriteria();

						if (criteria.getRelationModelName() == null) {
							//
							String title = "Invalid input";
							String message = "A relation model is required to launch.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

						} else if (criteria.getEgoRoleName() == null) {
							//
							String title = "Invalid input";
							String message = "An ego role is required to launch.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

						} else {
							//
							lastCriteria = criteria;
							MissingTestimoniesInputDialog.this.dialogCriteria = criteria;

							//
							setVisible(false);
						}
					}
				});
				this.okButton.setActionCommand("OK");
				buttonPane.add(this.okButton);
				getRootPane().setDefaultButton(this.okButton);
			}
		}

		// ////////////////////////
		setCriteria(lastCriteria);
	}

	/**
	 * 
	 * @return
	 */
	public MissingTestimoniesCriteria getCriteria() {
		MissingTestimoniesCriteria result;

		result = new MissingTestimoniesCriteria();

		//
		if (this.cmbbxRelationModel.getSelectedIndex() == -1) {
			//
			result.setRelationModelName(null);

		} else {
			//
			result.setRelationModelName((String) this.cmbbxRelationModel.getSelectedItem());
		}

		//
		if (this.cmbbxEgoRole.getSelectedIndex() == -1) {
			//
			result.setEgoRoleName(null);

		} else {
			//
			result.setEgoRoleName((String) this.cmbbxEgoRole.getSelectedItem());
		}

		logger.debug("criteria=" + result.toString());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public MissingTestimoniesCriteria getDialogCriteria() {
		MissingTestimoniesCriteria result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final MissingTestimoniesCriteria source) {
		//
		if (source != null) {
			//
			if (this.relationModels.isEmpty()) {
				//
				this.cmbbxRelationModel.setEnabled(false);
				this.cmbbxEgoRole.setEnabled(false);

			} else {
				//
				this.cmbbxRelationModel.setEnabled(true);
				this.cmbbxEgoRole.setEnabled(true);

				//
				RelationModel currentRelationModel = this.relationModels.getByName(source.getRelationModelName());
				if (currentRelationModel == null) {
					//
					currentRelationModel = this.relationModels.get(0);
				}

				//
				setRelationModel(this.relationModels.indexOf(currentRelationModel), source.getEgoRoleName());
			}
		}
	}

	/**
	 * 
	 * @param currentRelationModelIndex
	 */
	private void setRelationModel(final int currentRelationModelIndex, final String egoRoleName) {
		//
		RelationModel currentRelationModel = this.relationModels.get(currentRelationModelIndex);

		//
		if (currentRelationModel.roles().isEmpty()) {
			//
			this.cmbbxEgoRole.setEnabled(false);

		} else {
			//
			this.cmbbxEgoRole.setModel(new DefaultComboBoxModel(currentRelationModel.roles().toNameList().toArray()));

			//
			Role currentEgoRole = currentRelationModel.roles().getByName(egoRoleName);
			if (currentEgoRole == null) {
				//
				currentEgoRole = currentRelationModel.roles().get(0);
			}

			this.cmbbxEgoRole.setSelectedIndex(currentRelationModel.roles().indexOf(currentEgoRole));
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		/* AttributeFilter criteria = */showDialog(null);
	}

	/**
	 * Launch the application.
	 */
	public static MissingTestimoniesCriteria showDialog(final RelationModels relationModels) {
		MissingTestimoniesCriteria result;

		//
		MissingTestimoniesInputDialog dialog = new MissingTestimoniesInputDialog(relationModels);
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
