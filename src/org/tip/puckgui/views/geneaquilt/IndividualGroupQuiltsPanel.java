package org.tip.puckgui.views.geneaquilt;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.kinoath.IndividualGroup;
import org.tip.puck.kinoath.IndividualGroups;
import org.tip.puck.kinoath.KinOathDiagram;
import org.tip.puck.net.Families;
import org.tip.puck.net.Individual;
import org.tip.puck.net.workers.GeneaQuiltConvert;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.views.kinoath.IndividualGroupsCellRenderer;
import org.tip.puckgui.views.kinoath.IndividualGroupsModel;

import fr.inria.aviz.geneaquilt.gui.quiltview.GeneaQuiltPanel;
import fr.inria.aviz.geneaquilt.model.Network;

/**
 * 
 * @author TIP
 */
public class IndividualGroupQuiltsPanel extends JPanel {

	private static final long serialVersionUID = 6212151822603691885L;

	private static final Logger logger = LoggerFactory.getLogger(IndividualGroupQuiltsPanel.class);

	int reportCounter = 0;
	private JPanel thisJPanel;
	private NetGUI netGUI;
	JList individualGroupList;
	JScrollPane individualGroupsScrollPane;
	KinOathDiagram diagram;
	private GeneaQuiltPanel geneaQuiltPanel;
	private JSplitPane split;

	/**
	 * Initialize the contents of the frame.
	 */
	public IndividualGroupQuiltsPanel(final NetGUI guiManager, final IndividualGroups source) {

		//
		this.thisJPanel = this;
		this.netGUI = guiManager;

		//
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		this.split = new JSplitPane();
		add(this.split);
		this.split.setAlignmentX(Component.CENTER_ALIGNMENT);

		JPanel individualGroupsPanel = new JPanel();
		individualGroupsPanel.setMinimumSize(new Dimension(200, 10));
		this.split.setLeftComponent(individualGroupsPanel);
		individualGroupsPanel.setLayout(new BoxLayout(individualGroupsPanel, BoxLayout.Y_AXIS));

		this.individualGroupsScrollPane = new JScrollPane();
		this.individualGroupsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		individualGroupsPanel.add(this.individualGroupsScrollPane);

		this.individualGroupList = new JList();
		this.individualGroupList.setDoubleBuffered(true);
		this.individualGroupList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(final ListSelectionEvent event) {
				logger.debug("event = " + event.getValueIsAdjusting() + " " + event.getFirstIndex() + " " + event.getLastIndex() + " "
						+ IndividualGroupQuiltsPanel.this.individualGroupList.getSelectedIndex());

				if (!event.getValueIsAdjusting()) {
					// Selected.
					if (IndividualGroupQuiltsPanel.this.individualGroupList.getSelectedIndex() != -1) {
						//
						IndividualGroup group = (IndividualGroup) ((JList) event.getSource()).getModel().getElementAt(
								IndividualGroupQuiltsPanel.this.individualGroupList.getSelectedIndex());
						updateIndividualGroupDiagram(group);
					}
				}
			}
		});
		this.individualGroupList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		this.individualGroupList.setCellRenderer(new IndividualGroupsCellRenderer());
		this.individualGroupList.setModel(new IndividualGroupsModel(source));
		this.individualGroupsScrollPane.setViewportView(this.individualGroupList);

		this.geneaQuiltPanel = new GeneaQuiltPanel(null, (Network) null);
		this.split.setRightComponent(this.geneaQuiltPanel);

		JPanel buttonsPanel = new JPanel();
		add(buttonsPanel);
		buttonsPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));

		JButton btnGroupsPrevious = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnPrevious.text"));
		btnGroupsPrevious.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Previous individual group.
				int selectedIndex = IndividualGroupQuiltsPanel.this.individualGroupList.getSelectedIndex();
				if (selectedIndex == -1) {
					int size = IndividualGroupQuiltsPanel.this.individualGroupList.getModel().getSize();
					if (size != 0) {
						selectedIndex = size - 1;
						IndividualGroupQuiltsPanel.this.individualGroupList.setSelectedIndex(selectedIndex);
						IndividualGroupQuiltsPanel.this.individualGroupList.ensureIndexIsVisible(selectedIndex);
					}
				} else if (selectedIndex > 0) {
					selectedIndex -= 1;
					IndividualGroupQuiltsPanel.this.individualGroupList.setSelectedIndex(selectedIndex);
					IndividualGroupQuiltsPanel.this.individualGroupList.ensureIndexIsVisible(selectedIndex);
				}
			}
		});

		Component horizontalStrut_6 = Box.createHorizontalStrut(20);
		buttonsPanel.add(horizontalStrut_6);

		JButton btnClose = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualDiagramPanel.btnClose.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				//
				IndividualGroupQuiltsPanel.this.netGUI.closeCurrentTab();
			}
		});
		buttonsPanel.add(btnClose);

		Component horizontalGlue_1 = Box.createHorizontalGlue();
		buttonsPanel.add(horizontalGlue_1);

		JButton btnSort = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("IndividualsPanel.btnSort.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnSort.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Sort individual group list.

				IndividualGroup selectedGroup = getSelectedGroup();
				((IndividualGroupsModel) IndividualGroupQuiltsPanel.this.individualGroupList.getModel()).touchSorting();
				if (selectedGroup != null) {
					//
					select(selectedGroup);
				}
			}
		});
		buttonsPanel.add(btnSort);

		Component horizontalGlue_4 = Box.createHorizontalGlue();
		buttonsPanel.add(horizontalGlue_4);
		buttonsPanel.add(btnGroupsPrevious);

		JButton btnGroupNext = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.btnNext.text"));
		btnGroupNext.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent event) {
				// Next group.
				int selectedIndex = IndividualGroupQuiltsPanel.this.individualGroupList.getSelectedIndex();
				int size = IndividualGroupQuiltsPanel.this.individualGroupList.getModel().getSize();
				if (selectedIndex == -1) {
					if (size != 0) {
						selectedIndex = 0;
						IndividualGroupQuiltsPanel.this.individualGroupList.setSelectedIndex(selectedIndex);
						IndividualGroupQuiltsPanel.this.individualGroupList.ensureIndexIsVisible(selectedIndex);
					}
				} else if (selectedIndex < size - 1) {
					selectedIndex += 1;
					IndividualGroupQuiltsPanel.this.individualGroupList.setSelectedIndex(selectedIndex);
					IndividualGroupQuiltsPanel.this.individualGroupList.ensureIndexIsVisible(selectedIndex);
				}
			}
		});
		buttonsPanel.add(btnGroupNext);

		Component horizontalGlue = Box.createHorizontalGlue();
		buttonsPanel.add(horizontalGlue);

		JPanel panel_13 = new JPanel();
		buttonsPanel.add(panel_13);
		panel_13.setLayout(new BoxLayout(panel_13, BoxLayout.X_AXIS));

		// ////////////////////
		selectByIndex(1);
	}

	/**
	 * 
	 * @return
	 */
	public IndividualGroup getSelectedGroup() {
		IndividualGroup result;

		if (this.individualGroupList.getSelectedIndex() == -1) {
			result = null;
		} else {
			result = (IndividualGroup) ((IndividualGroupsModel) this.individualGroupList.getModel()).getElementAt(this.individualGroupList.getSelectedIndex());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 */
	public void select(final IndividualGroup group) {
		int groupIndex = ((IndividualGroupsModel) this.individualGroupList.getModel()).indexOf(group);

		if ((groupIndex >= 0) && (groupIndex < ((IndividualGroupsModel) this.individualGroupList.getModel()).getSize())) {
			//
			this.individualGroupList.setSelectedIndex(groupIndex);
			this.individualGroupList.ensureIndexIsVisible(groupIndex);

		} else if (((IndividualGroupsModel) this.individualGroupList.getModel()).getSize() != 0) {
			//
			this.individualGroupList.setSelectedIndex(0);
			this.individualGroupList.ensureIndexIsVisible(0);

		} else {
			//
			updateIndividualGroupDiagram(null);
		}
	}

	/**
	 * 
	 * @param target
	 */
	public void selectByIndex(final int groupIndex) {

		if ((groupIndex >= 0) && (groupIndex < ((IndividualGroupsModel) this.individualGroupList.getModel()).getSize())) {
			//
			this.individualGroupList.setSelectedIndex(groupIndex);
			this.individualGroupList.ensureIndexIsVisible(groupIndex);

		} else if (((IndividualGroupsModel) this.individualGroupList.getModel()).getSize() != 0) {
			//
			this.individualGroupList.setSelectedIndex(0);
			this.individualGroupList.ensureIndexIsVisible(0);

		} else {
			//
			updateIndividualGroupDiagram(null);
		}
	}

	/**
	 * 
	 */
	public void updateGroupDiagram() {
		//
		updateIndividualGroupDiagram(getSelectedGroup());
	}

	/**
	 * 
	 */
	public void updateIndividualGroupDiagram(final IndividualGroup source) {
		//
		if (source == null) {
			//
			logger.debug("updateIndividualGroupDiagram(null)");

		} else {
			//
			logger.debug("updateIndividualGroupDiagram(size=" + source.size() + ")");

			//
			this.split.remove(this.geneaQuiltPanel);

			Families families = new Families();
			for (Individual individual : source) {
				//
				families.add(individual.getOriginFamily());
				families.add(individual.getPersonalFamilies());
			}
			Network network = GeneaQuiltConvert.convertToGeneaQuilt(source, families);
			this.geneaQuiltPanel = new GeneaQuiltPanel(this, network);
			this.split.setRightComponent(this.geneaQuiltPanel);
		}
	}
}
