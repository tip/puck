package oldcore.visualization;


import oldcore.visualization.PlotOptions.HeritageType;

import org.gephi.graph.api.Edge;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.Node;
import org.gephi.ranking.api.NodeRanking;
import org.gephi.ranking.impl.AbstractRanking;

public class NbrChildrenRanking extends AbstractRanking<Node, Integer> implements NodeRanking<Integer> {
	
	HeritageType ht;
	
	NbrChildrenRanking(Graph graph, HeritageType ht){
		this.graph = graph;
		this.ht = ht;
	}

	@Override
	public String getName() {
		return "Number spouses ranking";
	}

	@Override
	public Class getType() {
		return Integer.class;
	}

	@Override
	public float normalize(Integer value) {
		return (float) ((value - minimum) / (float) (maximum - minimum));
	}

	@Override
	public Integer unNormalize(float normalizedValue) {
		return (int) (normalizedValue * (maximum - minimum)) + minimum;
	}

	@Override
	public Integer getValue(Node node) {
		if((ht == HeritageType.agnatic && node.getNodeData().getAttributes().getValue("gender").equals("female")) || 
				(ht == HeritageType.uterine && node.getNodeData().getAttributes().getValue("gender").equals("male")))
			return 0;
		int nbr_children = 0;
		for(Edge e : graph.getEdges(node)){
			if(e.getSource()==node && e.getEdgeData().getAttributes().getValue("tie type").equals("parental")) nbr_children++;
		}
		return nbr_children;
	}
}