package org.tip.puckgui.util;

/* From http://java.sun.com/docs/books/tutorial/index.html */

/*
 * Copyright (c) 2006 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * -Redistribution of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * -Redistribution in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * Neither the name of Sun Microsystems, Inc. or the names of contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. SUN MIDROSYSTEMS, INC. ("SUN") AND ITS
 * LICENSORS SHALL NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A
 * RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 * IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT
 * OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR
 * PUNITIVE DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY,
 * ARISING OUT OF THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF SUN HAS
 * BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * 
 * You acknowledge that this software is not designed, licensed or intended for
 * use in the design, construction, operation or maintenance of any nuclear
 * facility.
 */

import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 * Modified to accept null value in constructors.
 */
public class AutoTextField extends JTextField {

	/**
	 * 
	 */
	class AutoDocument extends PlainDocument {

		@Override
		public void insertString(final int i, final String s, final AttributeSet attributeset) throws BadLocationException {
			if (s == null || "".equals(s)) {
				return;
			}
			String s1 = getText(0, i);
			String s2 = getMatch(s1 + s);
			int j = (i + s.length()) - 1;
			if (isStrict && s2 == null) {
				s2 = getMatch(s1);
				j--;
			} else if (!isStrict && s2 == null) {
				super.insertString(i, s, attributeset);
				return;
			}
			if (autoComboBox != null && s2 != null) {
				autoComboBox.setSelectedValue(s2);
			}
			super.remove(0, getLength());
			super.insertString(0, s2, attributeset);
			setSelectionStart(j + 1);
			setSelectionEnd(getLength());
		}

		@Override
		public void remove(final int i, final int j) throws BadLocationException {
			int k = getSelectionStart();
			if (k > 0) {
				k--;
			}
			String s = getMatch(getText(0, k));
			if (!isStrict && s == null) {
				super.remove(i, j);
			} else {
				super.remove(0, getLength());
				super.insertString(0, s, null);
			}
			if (autoComboBox != null && s != null) {
				autoComboBox.setSelectedValue(s);
			}
			try {
				setSelectionStart(k);
				setSelectionEnd(getLength());
			} catch (Exception exception) {
			}
		}

		@Override
		public void replace(final int i, final int j, final String s, final AttributeSet attributeset) throws BadLocationException {
			super.remove(i, j);
			insertString(i, s, attributeset);
		}

	}

	private static final long serialVersionUID = -8060098505837569333L;
	@SuppressWarnings("unchecked")
	private List dataList;
	private boolean isCaseSensitive;
	private boolean isStrict;
	private AutoComboBox autoComboBox;

	/**
	 * 
	 * @param list
	 */
	@SuppressWarnings("unchecked")
	public AutoTextField(final List list) {
		setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
		isCaseSensitive = false;
		isStrict = true;
		autoComboBox = null;
		if (list == null) {
			dataList = new ArrayList<String>();
		} else {
			dataList = list;
		}
		init();
		return;
	}

	/**
	 * 
	 * @param list
	 * @param b
	 */
	@SuppressWarnings("unchecked")
	AutoTextField(final List list, final AutoComboBox b) {
		setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
		isCaseSensitive = false;
		isStrict = true;
		autoComboBox = null;
		if (list == null) {
			dataList = new ArrayList<String>();
		} else {
			dataList = list;
		}
		autoComboBox = b;
		init();
		return;
	}

	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List getDataList() {
		return dataList;
	}

	/**
	 * 
	 * @param s
	 * @return
	 */
	private String getMatch(final String s) {
		if (s.equals("")) {
			return s; // added
		}
		for (int i = 0; i < dataList.size(); i++) {
			String s1 = dataList.get(i).toString();
			if (s1 != null) {
				if (!isCaseSensitive && s1.toLowerCase().startsWith(s.toLowerCase())) {
					return s1;
				}
				if (isCaseSensitive && s1.startsWith(s)) {
					return s1;
				}
			}
		}

		return null;
	}

	/**
	 * 
	 */
	private void init() {
		setDocument(new AutoDocument());
		if (isStrict && dataList.size() > 0) {
			setText(dataList.get(0).toString());
		}
	}

	public boolean isCaseSensitive() {
		return isCaseSensitive;
	}

	public boolean isStrict() {
		return isStrict;
	}

	/**
	 * 
	 */
	@Override
	public void replaceSelection(final String s) {
		AutoDocument _lb = (AutoDocument) getDocument();
		if (_lb != null) {
			try {
				int i = Math.min(getCaret().getDot(), getCaret().getMark());
				int j = Math.max(getCaret().getDot(), getCaret().getMark());
				_lb.replace(i, j - i, s, null);
			} catch (Exception exception) {
			}
		}
	}

	public void setCaseSensitive(final boolean flag) {
		isCaseSensitive = flag;
	}

	/**
	 * 
	 * @param list
	 */
	@SuppressWarnings("unchecked")
	public void setDataList(final List list) {
		if (list == null) {
			throw new IllegalArgumentException("values can not be null");
		} else {
			dataList = list;
			return;
		}
	}

	/**
	 * 
	 * @param flag
	 */
	public void setStrict(final boolean flag) {
		isStrict = flag;
	}
}
