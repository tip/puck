package org.tip.puck.io.bar;

import java.util.ArrayList;
import java.util.List;

import org.tip.puck.io.bar.BARTXTFile.Format;

/**
 * This class represents a line of individual from a TXT file.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class BARTXTIndividualLine {
	public static final int NOID = 0;

	private int id;
	private String name;
	private char gender;
	private int fatherId;
	private int motherId;
	private List<Integer> spouseIds;

	private Format format;

	private int originFamilyId;
	private List<Integer> personalFamilyIds;

	/**
	 * 
	 */
	public BARTXTIndividualLine(final Format format) {
		this.format = format;
		this.gender = 'X';
		if (format == format.ONEMODE) {
			this.spouseIds = new ArrayList<Integer>();
		} else if (format == format.TWOMODE) {
			this.personalFamilyIds = new ArrayList<Integer>();
		}
	}

	public int fatherId() {
		return this.fatherId;
	}

	public char gender() {
		return this.gender;
	}

	public int id() {
		return this.id;
	}

	public int motherId() {
		return this.motherId;
	}

	public String name() {
		return this.name;
	}

	public int originFamilyId() {
		return this.originFamilyId;
	}

	public List<Integer> personalFamilyIds() {
		return this.personalFamilyIds;
	}

	public void setFatherId(final int fatherId) {
		this.fatherId = fatherId;
	}

	public void setGender(final char gender) {
		this.gender = gender;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public void setMotherId(final int motherId) {
		this.motherId = motherId;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setOriginFamilyId(final int originFamilyId) {
		this.originFamilyId = originFamilyId;
	}

	public List<Integer> spouseIds() {
		return this.spouseIds;
	}

}
