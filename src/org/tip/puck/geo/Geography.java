package org.tip.puck.geo;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.geo.tools.GeotoolsUtils;
import org.tip.puck.graphs.Graph;
import org.tip.puck.net.Attributes;
import org.tip.puck.partitions.Partition;
import org.tip.puck.util.Value;

import fr.devinsy.util.StringList;
import fr.devinsy.util.StringSet;

/**
 * 
 */
public class Geography {

	public enum Status {
		PERFECT,
		COMPLETE,
		WORKABLE,
		UNWORKABLE
	}

	private static final Logger logger = LoggerFactory.getLogger(Geography.class);

	private String label;
	private Places places;
	private IndexOfToponyms indexOfToponyms;
	private Attributes attributes;
	private Map<GeoLevel, Map<String, Place>> levels;
	private Map<String, String> homonyms;


	/**
	 * 
	 */
	public Geography() {
		//
		this.places = new Places();
		this.indexOfToponyms = new IndexOfToponyms();
		this.attributes = new Attributes();
		this.levels = new HashMap<GeoLevel, Map<String, Place>>();
		for (GeoLevel level : GeoLevel.values()) {
			this.levels.put(level, new HashMap<String, Place>());
		}
		this.homonyms = new HashMap<String,String>();
	}

	/**
	 * 
	 * @param place
	 */
	public Place addPlace(final Place place) {
		if (place != null && !places.contains(place)) {
			//
			this.places.add(place);

			//
			this.indexOfToponyms.index(place);
			this.levels.get(place.getGeoLevel()).put(place.getToponym(), place);

		}

		//
		return place;
	}
	
	public void addHomonym(final Place place, final String homonym){
		homonyms.put(homonym, place.getToponym());
		place.getHomonyms().add(homonym);

	}

	public Attributes attributes() {
		return this.attributes;
	}

	/**
	 * 
	 */
	public void clear() {
		this.label = "";
		this.attributes.clear();
		this.places.clear();
		this.indexOfToponyms.clear();
	}

	/**
	 * 
	 * @param toponym
	 * @return
	 */
	public boolean contains(final String toponym) {
		boolean result;

		if (toponym == null) {
			result = false;
		} else {
			if (get(toponym) == null) {
				result = false;
			} else {
				result = true;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int countOfConflictedPlaces() {
		int result;

		result = getConflictedPlaces().size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int countOfGeocodedPlaces() {
		int result;

		result = getGeocodedPlaces().size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int countOfHomonyms() {
		int result;

		result = 0;

		for (Place place : this.places) {
			//
			result += place.getHomonyms().size();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int countOfPlaces() {
		int result;

		result = this.places.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int countOfSingleHomonyms() {
		int result;

		result = getHomonyms().size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int countOfToponyms() {
		int result;

		result = this.indexOfToponyms.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int countOfUngeocodedPlaces() {
		int result;

		result = getUngeocodedPlaces().size();

		//
		return result;
	}

	/**
	 * 
	 * @param toponym
	 * @return
	 */
	public Place get(final String toponym) {
		Place result;

		if (toponym == null) {
			//
			result = null;

		} else {
			//
			result = this.indexOfToponyms.get(toponym);
		}

		//
		return result;
	}
	

	/**
	 * This method returns the list of lacking places.
	 * 
	 * A lacking place is a place with missing coordinates.
	 * 
	 * 
	 * @return
	 */
	public Places getBlankPlaces() {
		Places result;

		result = new Places();

		for (Place place : this.places) {
			//
			if ((StringUtils.isBlank(place.getComment())) && (place.getHomonyms().isEmpty()) && (place.getCoordinate2() == null)) {
				//
				result.add(place);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Places getConflictedPlaces() {
		Places result;

		Set<Place> conflictedPlaces = new HashSet<Place>();

		for (Place place : this.places) {
			//
			StringList names = new StringList(1 + place.getHomonyms().size());
			names.add(place.getToponym());
			names.addAll(place.getHomonyms());

			//
			for (String name : names) {
				//
				Place reference = this.indexOfToponyms.get(name);
				if ((reference != place) && (reference != null)) {
					//
					conflictedPlaces.add(reference);
					conflictedPlaces.add(place);
				}
			}
		}
		// logger.debug("conflicted1=" + conflictedPlaces.size());

		//
		result = new Places();
		for (Place place : conflictedPlaces) {
			//
			result.add(place);
		}
		// logger.debug("conflicted2=" + result.size());

		//
		return result;
	}

	/**
	 * This method returns the list of geocoded places.
	 * 
	 * A lacking place is a place with missing coordinates.
	 * 
	 * 
	 * @return
	 */
	public Places getGeocodedPlaces() {
		Places result;

		result = new Places();

		for (Place place : this.places) {
			//
			if (place.getCoordinate2() != null) {
				//
				result.add(place);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public StringList getHomonyms() {
		StringList result;

		StringSet names = new StringSet();

		for (Place place : this.places) {
			//
			names.addAll(place.getHomonyms());
		}

		result = names.toStringList();

		//
		return result;
	}

	public String getLabel() {
		return this.label;
	}

	/**
	 * 
	 * @return
	 */
	public StringSet getMainToponyms() {
		StringSet result;

		result = new StringSet();

		for (Place place : this.places) {
			//
			result.add(place.getToponym());
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Places getPlaces() {
		Places result;

		//
		result = new Places(this.places);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Status getStatus() {
		Status result;

		if (isLacking()) {
			//
			if (isConflicted()) {
				//
				result = Status.UNWORKABLE;

			} else if (countOfUngeocodedPlaces() == countOfPlaces()) {
				//
				result = Status.UNWORKABLE;

			} else {
				//
				result = Status.WORKABLE;
			}
		} else {
			//
			if (isConflicted()) {
				//
				result = Status.UNWORKABLE;

			} else if (isEmpty()) {
				//
				result = Status.UNWORKABLE;

			} else {
				//
				result = Status.COMPLETE;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public StringList getToponyms() {
		StringList result;

		result = new StringList();

		for (String toponym : this.indexOfToponyms.getToponyms()) {
			//
			result.add(toponym);
		}

		//
		return result;
	}

	/**
	 * This method returns the list of lacking places.
	 * 
	 * A lacking place is a place with missing coordinates.
	 * 
	 * 
	 * @return
	 */
	public Places getUngeocodedPlaces() {
		Places result;

		result = new Places();

		for (Place place : this.places) {
			//
			if (place.getCoordinate2() == null) {
				//
				result.add(place);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isConflicted() {
		boolean result;

		boolean ended = false;
		StringSet toponyms = new StringSet();
		Iterator<Place> iterator = this.places.iterator();
		result = true;
		while (!ended) {
			//
			if (iterator.hasNext()) {
				//
				Place place = iterator.next();

				//
				if (toponyms.contains(place.getToponym())) {
					//
					ended = true;
					result = true;

				} else {
					//
					boolean ended2 = false;
					Iterator<String> iterator2 = place.getHomonyms().iterator();
					while (!ended2) {
						//
						if (iterator2.hasNext()) {
							//
							String homonym = iterator2.next();

							if (toponyms.contains(homonym)) {
								//
								ended2 = true;
								ended = true;
								result = true;
							}
						} else {
							//
							ended2 = true;

							//
							toponyms.put(place.getToponym());
							toponyms.put(place.getHomonyms());
						}
					}
				}
			} else {
				//
				ended = true;
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		boolean result;

		result = this.places.isEmpty();

		//
		return result;
	}

	/**
	 * A geography is lacking when it contains ungeocoded place.
	 * 
	 * @return
	 */
	public boolean isLacking() {
		boolean result;

		result = false;

		boolean ended = false;
		Iterator<Place> iterator = this.places.iterator();
		while (!ended) {
			//
			if (iterator.hasNext()) {
				//
				Place place = iterator.next();

				if (place.getCoordinate2() == null) {
					//
					ended = true;
					result = true;
				}

			} else {
				//
				ended = true;
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotConflicted() {
		boolean result;

		result = !isConflicted();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotLacking() {
		boolean result;

		result = !isLacking();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isVoid() {
		boolean result;

		result = (this.places.isEmpty() && this.attributes.isEmpty());

		//
		return result;
	}

	/**
	 * 
	 */
	public void rebuildIndexes() {
		//
		this.indexOfToponyms.clear();
		this.indexOfToponyms.index(this.places);
	}

	/**
	 * 
	 * @param place
	 */
	public void removePlace(final Place place) {
		this.places.remove(place);
		rebuildIndexes();
	}

	/**
	 * 
	 * @param toponym
	 * @return
	 */
	public Place searchByToponym(final String toponym) {
		Place result;

		if (toponym == null) {
			//
			result = null;

		} else {
			//
			result = this.indexOfToponyms.get(toponym);
		}

		//
		return result;
	}

	public void setLabel(final String label) {
		this.label = label;
	}
	
	/**
	 * 
	 * @param level
	 * @param bitoponym
	 * @return
	 */
	public Place get(final GeoLevel level, final String toponym) {
		return this.levels.get(level).get(toponym);
	}
	
	
	/**
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public Place getCommonAncestor(final Place first, final Place second) {
		Place result;

		result = null;

		if (first == second) {
			result = first;
		} else if (first != null && second != null) {
					
			int comp = first.compareByLevel(second);
			if (comp == 0) {
				result = getCommonAncestor(first.getSuperiorPlace(), second.getSuperiorPlace());
			} else if (comp < 0) {
				result = getCommonAncestor(first, second.getSuperiorPlace());
			} else if (comp > 0) {
				result = getCommonAncestor(first.getSuperiorPlace(), second);
			}
		}
		//
		return result;
	}


	/**
	 * 
	 * @param homonym
	 * @return
	 */
	public Place getByHomonym(final String homonym) {
		Place result;
		
		if (homonym==null){
			result = null;
		} else {
			result = get(this.homonyms.get(homonym));
		}
		
		//
		return result;
	}
	
	public void putUncodedPlaces (Partition<Place> uncodedPlaces, String placeName){
		Place place = getByHomonym(placeName);
		if (place != null && place.getCoordinate2()==null) {
			uncodedPlaces.put(place,new Value(place.getGeoLevel()));
			Place sup = place.getSuperiorPlace();
			while (sup.getCoordinate2()==null){
				uncodedPlaces.put(sup, new Value (sup.getGeoLevel()));
				sup = sup.getSuperiorPlace();
			}
		}
	}
	
	/**
	 * @param geography
	 * @param event
	 * @return
	 */
	public GeoLevel getDistance (final String startName, final String endName){
		GeoLevel result;
		
		Place start = getByHomonym(startName);
		Place end = getByHomonym(endName);

		if (start != null && end != null){
			Place commonAncestor = getCommonAncestor(start, end);
			result = commonAncestor.getGeoLevel();
		} else {
			result = null;
		}
		
		//
		return result;
	}
	
	/**
	 * @param unit = 'M' is statute miles, 'K' is kilometers (default), 'N' is nautical miles  
	 * @param startName
	 * @param endName
	 * @return
	 */
	public Double getDistance (final String startName, final String endName, char unit){
		Double result;
		
		Place start = get(startName);
		Place end = get(endName);

		if (start != null && end != null){
			result = GeotoolsUtils.distance(start, end, unit);
		} else {
			result = null;
		}
		
		//
		return result;
	}
	
	
	/**
	 * 
	 * @param homonym
	 * @param placenames
	 * @return
	 */
	public Place getSuperiorPlace(final String homonym, final List<String> placenames) {
		Place result;

		if (getByHomonym(homonym) == null) {
			result = null;
		} else {
			result = getByHomonym(homonym).getSuperiorPlace(placenames);
		}

		//
		return result;
	}
	
	public String getToponym(final String homonym){
		String result;
		
		Place place = getByHomonym(homonym);
		if (place == null){
			result = null;
		} else {
			result = place.getToponym();
		}
		//
		return result;
	}
	
	public String getToponym(final String homonym, final String parameter){
		String result;
		
		Place place = getPlace(homonym, parameter);
		if (place == null){
			result = null;
		} else {
			result = place.getToponym();
		}
		//
		return result;
	}
	
	/**
	 * 
	 * @param homonym
	 * @param parameter
	 * @return
	 */
	public Place getPlace(final String homonym, final String parameter) {
		Place result;

		try {
			result = getPlace(homonym, GeoLevel.valueOf(parameter));
						
		} catch (IllegalArgumentException iae) {
			result = getPlaceByCenter(homonym, parameter);
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @param homonym
	 * @param level
	 * @return
	 */
	public Place getPlace(final String homonym, final GeoLevel level) {
		Place result;

		if (getByHomonym(homonym) == null) {
			result = null;
//			System.err.println("Place not known: " + homonym);
		} else {
			result = getByHomonym(homonym).atLevel(level);
			
		}

		//
		return result;
	}

	/**
	 * @param homonym
	 * @return
	 */
	private Place getPlaceByCenter(final String homonym, final String center) {
		Place result;
		
		result = null;

		if (homonym == null) {
			
			result = null;
			
		} else if (homonym.equals(center)) {
			
			result = getByHomonym(homonym);
			
		} else {
			
			Place centerPlace = getByHomonym(center);
			
			if (centerPlace != null) {

				GeoLevel centerLevel = getByHomonym(center).getGeoLevel();
				
				for (GeoLevel level : GeoLevel.values()) {
					Place preResult = getPlace(homonym, level);
					if (preResult == null){
						break;
					} else {
						result = preResult;
						if (level.ordinal()>centerLevel.ordinal() || level == GeoLevel.SUBQUARTER || (result != null && !result.equals(getPlace(center, level)))) {
							break;
						}
					}
				}
			}
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @param level
	 * @return
	 */
	public Graph<Place> graph(final GeoLevel level) {
		Graph<Place> result;

		result = new Graph<Place>();

		for (Place place : places(level)) {
			result.addNode(place);
		}
		//
		return result;

	}
	
	/**
	 * 
	 * @param level
	 * @return
	 */
	private Places places(final GeoLevel level) {
		Places result;

		result = new Places();

		Map<String, Place> placeMap = this.levels.get(level);
		if (placeMap != null) {
			for (Place place : placeMap.values()) {
				result.add(place);
			}
		}
		//
		return result;
	}

}
