package org.tip.puck.net.relations.roles.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.io.ods.ODSBufferedReader;
import org.tip.puck.io.xls.XLSBufferedReader;
import org.tip.puck.net.Attributes;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.roles.RoleRelationMaker;
import org.tip.puck.net.relations.roles.RoleRelationRules;
import org.tip.puck.report.Report;

import fr.devinsy.util.StringList;

/**
 * This class manages terminology file in format "ETIC".
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class ETICFile {

	private static final Logger logger = LoggerFactory.getLogger(ETICFile.class);

	public static String DEFAULT_CHARSET_NAME = "UTF-8";

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel load(final File source, final Report report, final RoleRelationRules ruleConstraints) throws PuckException {
		RelationModel result;

		if (source == null) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("File=[" + source + "].");

		} else if (!source.exists()) {
			//
			throw PuckExceptions.FILE_NOT_FOUND.create("File=[" + source + "].");

		} else if (!source.isFile()) {
			//
			throw PuckExceptions.NOT_A_FILE.create("File=[" + source + "].");

		} else {

			String name = StringUtils.lowerCase(source.getName());

			if (name.endsWith(".etic.ods")) {
				//
				result = loadFromEMICODSFile(source, report, ruleConstraints);

			} else if (name.endsWith(".etic.txt")) {
				//
				result = loadFromEMICTXTFile(source, report, ruleConstraints);

			} else if (name.endsWith(".etic.xls")) {
				//
				result = loadFromEMICXLSFile(source, report, ruleConstraints);

			} else {
				//
				throw PuckExceptions.UNSUPPORTED_FILE_FORMAT.create("File=[" + source + "].");
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @param report
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel loadFromEMICODSFile(final File file, final Report report, final RoleRelationRules ruleConstraints) throws PuckException {
		RelationModel result;

		BufferedReader in = null;
		try {
			in = new ODSBufferedReader(file);
			result = readFromETICTXTFile(in, report, ruleConstraints);
			result.setName(IOTermsTools.getBaseName(file, ".etic.ods"));

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} catch (Exception exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "\nOpening file [" + file + "]\n[" + exception.getMessage() + "]");
		} finally {
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @param charsetName
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel loadFromEMICTXTFile(final File file, final Report report, final RoleRelationRules ruleConstraints) throws PuckException {
		RelationModel result;

		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), DEFAULT_CHARSET_NAME));
			result = readFromETICTXTFile(in, report, ruleConstraints);
			result.setName(IOTermsTools.getBaseName(file, ".etic.txt"));

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} catch (Exception exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "\nOpening file [" + file + "]\n[" + exception.getMessage() + "]");
		} finally {
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @param report
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel loadFromEMICXLSFile(final File file, final Report report, final RoleRelationRules ruleConstraints) throws PuckException {
		RelationModel result;

		BufferedReader in = null;
		try {
			in = new XLSBufferedReader(file);
			result = readFromETICTXTFile(in, report, ruleConstraints);
			result.setName(IOTermsTools.getBaseName(file, ".etic.xls"));

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} catch (Exception exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "\nOpening file [" + file + "]\n[" + exception.getMessage() + "]");
		} finally {
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param in
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel readFromETICTXTFile(final BufferedReader in, final Report report, final RoleRelationRules ruleConstraints) throws PuckException {
		RelationModel result;

		//
		logger.debug("Read attributes.");
		Attributes attributes = new Attributes();
		IOTermsTools.readAttributesVertically(attributes, in);

		//
		StringList lines = IOTermsTools.readTermRuleLines(in);

		// Extract Self Name parameter.
		String selfName = IOTermsTools.extractSelfName(attributes);

		// Extract rule parameters.
		RoleRelationRules rules;
		if (ruleConstraints == null) {
			rules = IOTermsTools.extractRelationRoleRules(attributes);
		} else {
			rules = ruleConstraints;
		}

		//
		report.outputs().appendln("Source reading");
		report.outputs().appendln();
		report.outputs().append("Self Name:\t\t").appendln(selfName);
		report.outputs().append("Role Relation Rules:\t").appendln(rules);
		report.outputs().appendln();

		//
		result = RoleRelationMaker.createEtic("default", selfName, lines, rules, report);

		//
		return result;
	}
}
