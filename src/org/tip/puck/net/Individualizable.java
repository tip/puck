package org.tip.puck.net;

import org.tip.puck.util.Numberable;

public interface Individualizable extends Numberable {

	Individual getEgo();
	
}
