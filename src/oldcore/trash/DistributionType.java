package oldcore.trash;

/**
 * an enumeration of distribution types used for alliance matrix simulation
 * @author Klaus Hamberger
 * @since version 0.95, 10-08-08
 *
 */
public enum DistributionType {
	FREE(0),BERNOULLI(1),POWER_NONSTANDARD(2),POWER_STANDARD (3);
	
	/**
	 * the index of the type
	 */
	public final int id;
	
	/**
	 * gets the distribution type with a given index
	 * @param id the index
	 * @return the distribution type
	 * @see gui.screens.MatrixSimulationScreen#dist()
	 */
    public static DistributionType id(final int id) {
        for (DistributionType e : DistributionType.values()) {
            if (e.id == id) {
                return e;
            }
        }
        return null;
    }
	
	/**
	 * constructs a distribution type 
	 * @param id
	 */
	private DistributionType(int id){
		this.id = id;
	}
}


