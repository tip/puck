package org.tip.puck.geo.io;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.swing.JProgressBar;

import org.geonames.Toponym;
import org.geonames.ToponymSearchCriteria;
import org.geonames.ToponymSearchResult;
import org.geonames.WebService;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.tip.puck.geo.Place;
import org.tip.puck.geo.Place;
import org.tip.puck.geo.ToponymGeom;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.Node;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;

public class GeoNamesWebService {

	ArrayList<DefaultFeatureCollection> collectionsPointsAndLines = new ArrayList<DefaultFeatureCollection>();
	
	DefaultFeatureCollection featurePointsCollection;
	DefaultFeatureCollection featureLinesCollection;
	SimpleFeatureBuilder featureBuilder;

	GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory(null);

	SimpleFeatureType pointFeatureType;

	JProgressBar progressBar;
	
	public GeoNamesWebService(JProgressBar progressBar) {

		this.progressBar = progressBar;
		
		SimpleFeatureTypeBuilder b = new SimpleFeatureTypeBuilder();

		//set the name
		b.setName( "SCHEMA_POINT" );

		//add a geometry property
		b.setCRS( DefaultGeographicCRS.WGS84 ); // set crs first
		b.add("id_geonames", Integer.class);
		b.add("name", String.class);
		b.add("country", String.class);
		b.add("the_geom", Point.class ); // then add geometry

		//build the type
		this.pointFeatureType = b.buildFeatureType();

		featurePointsCollection = new DefaultFeatureCollection("internal",this.pointFeatureType);

		this.featureBuilder = new SimpleFeatureBuilder(this.pointFeatureType);

	}

	public DefaultFeatureCollection getFeaturePointsCollection() {
		return featurePointsCollection;
	}

	public void setFeaturePointsCollection(DefaultFeatureCollection featurePointsCollection) {
		this.featurePointsCollection = featurePointsCollection;
	}

	public SimpleFeatureBuilder getFeatureBuilder() {
		return featureBuilder;
	}

	public void setFeatureBuilder(SimpleFeatureBuilder featureBuilder) {
		this.featureBuilder = featureBuilder;
	}

	public GeometryFactory getGeometryFactory() {
		return geometryFactory;
	}

	public void setGeometryFactory(GeometryFactory geometryFactory) {
		this.geometryFactory = geometryFactory;
	}

	public SimpleFeatureType getPointFeatureType() {
		return pointFeatureType;
	}

	public void setPointFeatureType(SimpleFeatureType pointFeatureType) {
		this.pointFeatureType = pointFeatureType;
	}
	
	public ArrayList<DefaultFeatureCollection> getCoordForNodesPlaces(Graph<?> geoNetwork) {
		
		Graph<Place> graphPlaces = new Graph<Place>("GraphPlaces");
//		Graph<Cluster<Place>> graphClusterPlaces= new Graph<>("GraphClusterPlaces");
		
		HashSet<Place> placesList = new HashSet<Place>();
		List<Place> placesListDuplicate = new ArrayList<Place>();
		
		int nbTreatments = 0;
		int nbTreatmentsMax = geoNetwork.nodeCount();
		progressBar.setMaximum(nbTreatmentsMax);
		progressBar.setString("Processing ...");
		progressBar.setStringPainted(true);
		
		//Add nodes and build geometry
		for (Node<?> node : geoNetwork.getNodes()) {

			SimpleFeature f = getCoordForToponym(node.getLabel());
			Place p;
			if( f != null ) {

				this.featurePointsCollection.add(f);

				Geometry g = (Geometry) f.getAttribute( "the_geom" );
				p = new Place(node.getLabel(), g.getCoordinate());

			}
			else {
				p = new Place(node.getLabel());
				
			}
			graphPlaces.addNode(node.getId(),p);
			progressBar.setValue(nbTreatments++);
			
			//prepare for clustering
			placesList.add(p);				//hashset guarantee to avoid duplicate value
			placesListDuplicate.add(p);		//list conserve all values
			
		}
		collectionsPointsAndLines.add(featurePointsCollection);
		
		//Add links
		graphPlaces.addLinksByIds(geoNetwork.getLinks());

		BuildingGeoLinks linksFactory = new BuildingGeoLinks();
		this.featureLinesCollection = linksFactory.buildLinkGeometry(progressBar,graphPlaces);
		
		collectionsPointsAndLines.add(this.featureLinesCollection);
		
		return collectionsPointsAndLines;

	}

	public DefaultFeatureCollection getCoordForToponym(List<String> listToponymTexts) {

		for (String toponymText : listToponymTexts) {
			this.featurePointsCollection.add(getCoordForToponym(toponymText));
		}

		return featurePointsCollection;

	}

	public SimpleFeature getCoordForToponym(String text) {

		//Prepare connection to geonames webservice 
		WebService.setUserName("puck"); // puck user has been created on geonames.org

		if( text.indexOf("?") > 0 ) {
			//filtre des ?
			text = text.substring(0, text.indexOf("?"));
		}

		ToponymSearchCriteria searchCriteria = new ToponymSearchCriteria();
		searchCriteria.setQ(text);
		ToponymSearchResult searchResult = null;
		try {
			searchResult = WebService.search(searchCriteria);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Integer i = 0;
		for (Toponym iterToponym : searchResult.getToponyms()) {
			
			Coordinate pointCoord = new Coordinate(iterToponym.getLongitude(), iterToponym.getLatitude());
			Point point = this.geometryFactory.createPoint(pointCoord);

			ToponymGeom tpGeom = new ToponymGeom(iterToponym, point);

			SimpleFeature feature = SimpleFeatureBuilder.build( 
					this.pointFeatureType, new Object[]{tpGeom.getToponym().getGeoNameId(), tpGeom.getToponym().getName(), iterToponym.getCountryCode(), point}, null );
			i++;
			
			System.out.println("Geocode from geonames : " + iterToponym.getGeoNameId() + " " + iterToponym.getName()+" "+ iterToponym.getCountryName() + " " + iterToponym.getCountryCode() +" "+pointCoord);

			return feature;
		}
		return null;

	}

}