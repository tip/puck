/**
 * Copyright 2011 Christian P. MOMON (christian.momon@devinsy.fr).
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Adaptations by TIP.
 * 
 */
package org.tip.puck.util;

import java.util.List;

/**
 * 
 * @author Christian P. MOMON
 */
public class LogHelperCore {

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final int[] source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			StringBuffer buffer = new StringBuffer(192);

			buffer.append("#");
			buffer.append(source.length);
			buffer.append("{");
			int startSize = buffer.length();
			for (int value : source) {
				if (buffer.length() != startSize) {
					buffer.append(",");
				}
				buffer.append(value);
			}
			buffer.append("}");
			result = buffer.toString();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final List<?> source) {

		String result;

		if (source == null) {
			result = "[null]";
		} else {
			StringBuffer buffer = new StringBuffer(192);

			buffer.append("#");
			buffer.append(source.size());
			buffer.append("{");
			int startSize = buffer.length();
			for (Object subSource : source) {
				if (buffer.length() != startSize) {
					buffer.append(",");
				}

				if (subSource instanceof String) {
					buffer.append((String) subSource);
				} else if (subSource instanceof Integer) {
					buffer.append(subSource);
				} else {
					buffer.append("STU");
				}
			}
			buffer.append("}");
			result = buffer.toString();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final String[] source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			StringBuffer buffer = new StringBuffer(192);

			buffer.append("#");
			buffer.append(source.length);
			buffer.append("{");
			int startSize = buffer.length();
			for (String value : source) {
				if (buffer.length() != startSize) {
					buffer.append(",");
				}
				buffer.append(value);
			}
			buffer.append("}");
			result = buffer.toString();
		}

		//
		return result;
	}
}
