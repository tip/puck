package org.tip.puck.partitions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.tip.puck.util.Value;

/**
 * 
 * @author TIP
 */
public class Clusters<E> extends HashMap<Value, Cluster<E>> implements Iterable<Cluster<E>> {

	private static final long serialVersionUID = 908605489478085791L;

	/**
	 * 
	 */
	public Clusters() {
		super();
	}

	/**
	 * 
	 * @return
	 */
	public Collection<Value> getValues() {
		Collection<Value> result;

		result = this.keySet();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty() {
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNumeric() {
		boolean result;

		boolean ended = false;
		result = false;
		Iterator<Value> iterator = this.getValues().iterator();
		while (!ended) {
			if (iterator.hasNext()) {
				Value value = iterator.next();
				if (value != null && value.isNotNumber()) {
					ended = true;
					result = false;
				}
			} else {
				ended = true;
				result = true;
			}
		}

		//
		return result;
	}
	

	/**
	 * 
	 * @return
	 */
	public boolean isNumericOrNull() {
		boolean result;

		boolean ended = false;
		result = false;
		Iterator<Value> iterator = this.getValues().iterator();
		while (!ended) {
			if (iterator.hasNext()) {
				Value value = iterator.next();
				if ((value != null) && (value.isNotNumber())) {
					ended = true;
					result = false;
				}
			} else {
				ended = true;
				result = true;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Cluster<E>> iterator() {
		Iterator<Cluster<E>> result;

		result = this.values().iterator();

		//
		return result;
	}

	/**
	 * 
	 * @param cluster
	 * @return
	 */
	public Cluster<E> put(final Cluster<E> cluster) {
		Cluster<E> result;

		if (cluster != null) {
			put(cluster.getValue(), cluster);
		}

		result = cluster;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Cluster<E>> toList() {
		List<Cluster<E>> result;

		result = new ArrayList<Cluster<E>>(this.values());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Cluster<E>> toListSortedByDescendingSize() {
		List<Cluster<E>> result;

		result = toListSortedBySize();
		Collections.reverse(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Cluster<E>> toListSortedByDescendingValue() {
		List<Cluster<E>> result;

		result = toListSortedByValue();
		Collections.reverse(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Cluster<E>> toListSortedBySize() {
		List<Cluster<E>> result;

		result = toList();
		Collections.sort(result, new ClusterComparatorBySize());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Cluster<E>> toListSortedByValue() {
		List<Cluster<E>> result;

		result = toList();
		Collections.sort(result, new ClusterComparatorByValue());

		//
		return result;
	}


}
