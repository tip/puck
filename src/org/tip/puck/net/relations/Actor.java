package org.tip.puck.net.relations;

import org.tip.puck.net.Attribute;
import org.tip.puck.net.Attributes;
import org.tip.puck.net.Individual;

/**
 * 
 * @author TIP
 */
public class Actor implements Comparable<Actor> {

	private Individual individual;
	protected Role role;
	
	private Integer relationOrder; // Relation order for the individual in this role.
	private Attributes attributes;
	private Individual referent;

	/**
	 * 
	 * @param individual
	 * @param role
	 */
	public Actor(final Individual individual, final Role role) {
		//
		this.individual = individual;
		this.role = role;
		this.attributes = new Attributes();
	}

	/**
	 * 
	 */
	@Override
	public boolean equals(final Object actor) {
		return this.role.equals(((Actor) actor).role) && this.individual.equals(((Actor) actor).individual);

	}

	/**
	 * 
	 * @return
	 */
	public int getId() {
		int result;

		result = this.individual.getId();

		//
		return result;
	}

	public Individual getIndividual() {
		return this.individual;
	}

	/**
	 * 
	 * @return
	 */
	public String getName() {
		String result;

		result = this.individual.getName();

		//
		return result;
	}

	public Integer getRelationOrder() {
		return this.relationOrder;
	}

	public Role getRole() {
		return this.role;
	}

	public void setIndividual(final Individual individual) {
		this.individual = individual;
	}

	public void setRelationOrder(final Integer order) {
		this.relationOrder = order;
	}

	public void setRole(final Role role) {
		this.role = role;
	}
	
	/**
	 * 
	 * @return
	 */
	public Attributes attributes() {
		Attributes result;

		result = this.attributes;

		//
		return result;
	}
	
	public void setAttribute(final String label, final String value) {
		this.attributes.put(label, value);
	}
	
	public Individual getReferent() {
		return referent;
	}

	public void setReferent(Individual referent) {
		this.referent = referent;
	}

	public String toString(){
		return individual+" ["+role+"]";
	}
	
	/**
	 * 
	 */
	public String getAttributeValue(final String label) {
		String result;

		Attribute attribute = this.attributes().get(label);
		if (attribute == null) {
			result = null;
		} else {
			result = attribute.getValue();
		}

		//
		return result;
	}
	
	public int compareTo (Actor other){
		int result;
		
		result = this.individual.compareTo(other.individual);
		
		if (result == 0){
			result = this.role.compareTo(other.role);
		}
		
		//
		return result;
	}
	
	

}
