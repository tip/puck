package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.workers.AttributeDescriptor;
import org.tip.puck.net.workers.AttributeDescriptors;
import org.tip.puck.net.workers.AttributeRenameCriteria;
import org.tip.puck.net.workers.AttributeToRelationCriteria;
import org.tip.puck.net.workers.AttributeWorker;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class AttributeToRelationInputDialog extends JDialog {

	private static final long serialVersionUID = 6558093839789406987L;
	private final JPanel contentPanel = new JPanel();
	private AttributeToRelationCriteria dialogCriteria;
	private static AttributeToRelationCriteria lastCriteria = new AttributeToRelationCriteria();
	private JComboBox cmbbxTarget;
	private JComboBox cmbbxLabel;
	private JTextField txtfldNewName;
	private JCheckBox chckbxIgnoreCase;
	private JTextField txtFldEponymName;
	private JTextField txtFldDateSeparator;
	private JTextField txtRelationName;

	/**
	 * Create the dialog.
	 */
	public AttributeToRelationInputDialog(final List<String> relationModelNames, final AttributeDescriptors attributeDescriptors) {
		super();

		//
		List<String> targetLabels = new ArrayList<String>();
		targetLabels.add("ALL");
		targetLabels.add("CORPUS");
		targetLabels.add("INDIVIDUALS");
		targetLabels.add("FAMILIES");
		targetLabels.add("RELATIONS");
		if (relationModelNames != null) {
			for (String name : relationModelNames) {
				targetLabels.add(name);
			}
		}

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Attribute To Relation Input");
		setIconImage(Toolkit.getDefaultToolkit().getImage(AttributeToRelationInputDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				AttributeToRelationInputDialog.this.dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 420, 235);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,}));
		{
			JLabel lblTarget = new JLabel("Target:");
			this.contentPanel.add(lblTarget, "2, 2, right, default");
		}
		{
			this.cmbbxTarget = new JComboBox(targetLabels.toArray());
			this.cmbbxTarget.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(final ItemEvent event) {
					// Update label list.
					//
					if (event.getStateChange() == ItemEvent.SELECTED) {
						//
						List<String> labels;
						switch (AttributeToRelationInputDialog.this.cmbbxTarget.getSelectedIndex()) {
							case 0:
								labels = attributeDescriptors.labels();
							break;

							case 1:
								labels = attributeDescriptors.findByScope(AttributeDescriptor.Scope.CORPUS).labels();
							break;

							case 2:
								labels = attributeDescriptors.findByScope(AttributeDescriptor.Scope.INDIVIDUALS).labels();
							break;

							case 3:
								labels = attributeDescriptors.findByScope(AttributeDescriptor.Scope.FAMILIES).labels();
							break;

							case 4:
								labels = attributeDescriptors.findByScope(AttributeDescriptor.Scope.RELATION).labels();
							break;

							default:
								labels = attributeDescriptors.findByRelationModelName((String) AttributeToRelationInputDialog.this.cmbbxTarget.getSelectedItem())
										.labels();
							break;
						}

						//
						Collections.sort(labels);

						//
						AttributeToRelationInputDialog.this.cmbbxLabel.setModel(new DefaultComboBoxModel(labels.toArray()));
						AttributeToRelationInputDialog.this.cmbbxLabel.setSelectedIndex(-1);
					}
				}
			});
			this.contentPanel.add(this.cmbbxTarget, "4, 2, fill, default");
		}
		{
			JLabel lblLabel = new JLabel("Label:");
			this.contentPanel.add(lblLabel, "2, 4, right, default");
		}
		{
			this.cmbbxLabel = new JComboBox(attributeDescriptors.labelsSorted().toArray());
			this.cmbbxLabel.setEditable(true);
			this.contentPanel.add(this.cmbbxLabel, "4, 4, fill, default");
		}
		{
			JLabel lblRelationName = new JLabel("Relation Name:");
			contentPanel.add(lblRelationName, "2, 6, right, default");
		}
		{
			txtRelationName = new JTextField();
			contentPanel.add(txtRelationName, "4, 6, fill, default");
			txtRelationName.setColumns(10);
		}
		{
			JLabel lblNewLabel = new JLabel("Default Role:");
			this.contentPanel.add(lblNewLabel, "2, 8, right, default");
		}
		{
			this.txtfldNewName = new JTextField();
			this.contentPanel.add(this.txtfldNewName, "4, 8");
			this.txtfldNewName.setColumns(10);
		}
		{
			JLabel lblEponymRole = new JLabel("Eponym Role:");
			contentPanel.add(lblEponymRole, "2, 10, right, default");
		}
		{
			txtFldEponymName = new JTextField();
			contentPanel.add(txtFldEponymName, "4, 10, fill, default");
			txtFldEponymName.setColumns(10);
		}
		{
			JLabel lblDateSeparator = new JLabel("Date separator:");
			contentPanel.add(lblDateSeparator, "2, 12, right, default");
		}
		{
			txtFldDateSeparator = new JTextField();
			contentPanel.add(txtFldDateSeparator, "4, 12, fill, top");
			txtFldDateSeparator.setColumns(10);
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(null);
			this.contentPanel.add(panel, "4, 14, fill, center");
			panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.LABEL_COMPONENT_GAP_COLSPEC, ColumnSpec.decode("21px"),
					FormFactory.LABEL_COMPONENT_GAP_COLSPEC, ColumnSpec.decode("83px:grow"), }, new RowSpec[] { FormFactory.LINE_GAP_ROWSPEC,
					RowSpec.decode("21px"), FormFactory.DEFAULT_ROWSPEC, }));
			{
				this.chckbxIgnoreCase = new JCheckBox("");
				panel.add(this.chckbxIgnoreCase, "2, 2, left, top");
			}
			{
				JLabel lblIgnoreCase = new JLabel("ignore case");
				panel.add(lblIgnoreCase, "4, 2, left, center");
			}
			{
				JCheckBox chckbxTrim = new JCheckBox("");
				panel.add(chckbxTrim, "2, 3");
				chckbxTrim.setEnabled(false);
				chckbxTrim.setSelected(true);
			}
			{
				JLabel lblTrimLabelAnd = new JLabel("trim label and new label");
				panel.add(lblTrimLabelAnd, "4, 3");
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						AttributeToRelationInputDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("Convert");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						AttributeToRelationCriteria criteria = getCriteria();
						if (StringUtils.isBlank(criteria.getLabel())) {
							//
							String title = "Invalid input";
							String message = "Please, enter a label not empty.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						} else {
							//
							lastCriteria = criteria;
							AttributeToRelationInputDialog.this.dialogCriteria = criteria;

							//
							setVisible(false);
						}

					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}

		// ////////////////////////
		setCriteria(lastCriteria);
	}

	/**
	 * 
	 * @return
	 */
	public AttributeToRelationCriteria getCriteria() {
		AttributeToRelationCriteria result;

		result = new AttributeToRelationCriteria();

		//
		AttributeWorker.Scope scope;
		switch (this.cmbbxTarget.getSelectedIndex()) {
			case 0:
				scope = AttributeWorker.Scope.ALL;
			break;

			case 1:
				scope = AttributeWorker.Scope.CORPUS;
			break;

			case 2:
				scope = AttributeWorker.Scope.INDIVIDUALS;
			break;

			case 3:
				scope = AttributeWorker.Scope.FAMILIES;
			break;

			case 4:
				scope = AttributeWorker.Scope.RELATIONS;
			break;

			default:
				scope = AttributeWorker.Scope.RELATION;
				result.setSourceRelationName((String) this.cmbbxTarget.getSelectedItem());
			break;
		}
		
		result.setScope(scope);
		result.setTargetRelationName(this.txtRelationName.getText());

		//
		result.setLabel((String) this.cmbbxLabel.getSelectedItem());

		//
		result.setRoleName(this.txtfldNewName.getText());

		//
		result.setEponymRoleName(this.txtFldEponymName.getText());
		
		//
		result.setDateSeparator(this.txtFldDateSeparator.getText());

		//
		if (this.chckbxIgnoreCase.isSelected()) {
			//
			result.setCaseOption(AttributeWorker.CaseOption.IGNORE_CASE);

		} else {
			//
			result.setCaseOption(AttributeWorker.CaseOption.CASE_SENSITIVE);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public AttributeToRelationCriteria getDialogCriteria() {
		AttributeToRelationCriteria result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final AttributeToRelationCriteria source) {
		//
		if (source != null) {

			//
			switch (source.getScope()) {
				case ALL:
					this.cmbbxTarget.setSelectedIndex(0);
				break;

				case CORPUS:
					this.cmbbxTarget.setSelectedIndex(1);
				break;

				case INDIVIDUALS:
					this.cmbbxTarget.setSelectedIndex(2);
				break;

				case FAMILIES:
					this.cmbbxTarget.setSelectedIndex(3);
				break;

				case RELATIONS:
					this.cmbbxTarget.setSelectedIndex(4);
				break;

				default:
					this.cmbbxTarget.setSelectedItem(source.getSourceRelationName());
				break;
			}

			//
			this.cmbbxLabel.setSelectedItem(source.getLabel());

			//
			this.txtfldNewName.setText(source.getRoleName());

			//
			if (source.isCaseSensitive()) {
				//
				this.chckbxIgnoreCase.setSelected(false);

			} else {
				//
				this.chckbxIgnoreCase.setSelected(true);
			}
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		/* AttributeFilter criteria = */showDialog(null, null);
	}

	/**
	 * Launch the application.
	 */
	public static AttributeToRelationCriteria showDialog(final List<String> relationModelNames, final AttributeDescriptors attributeDescriptors) {
		AttributeToRelationCriteria result;

		//
		AttributeToRelationInputDialog dialog = new AttributeToRelationInputDialog(relationModelNames, attributeDescriptors);
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
