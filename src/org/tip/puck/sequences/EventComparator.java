package org.tip.puck.sequences;

import java.util.Comparator;

import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.sequences.workers.SequenceCriteria;

/**
 * 
 * @author Klaus Hamberger
 *
 */
public class EventComparator implements Comparator<Relation> {
	
	String dateLabel;
	String startPlaceLabel;
	String endPlaceLabel;
	
	public EventComparator(SequenceCriteria criteria){
		
		this.dateLabel = criteria.getDateLabel();
		this.startPlaceLabel = criteria.getStartPlaceLabel();
		this.endPlaceLabel = criteria.getEndPlaceLabel();
	}
	
	@Override
	public int compare(Relation event1, Relation event2) {
		int result;

		result = 0;
		
		if (event1.getId()!=event2.getId()){

			// Compare objective time order

				String date1 = ((Relation)event1).getAttributeValue(dateLabel);
				String date2 = ((Relation)event2).getAttributeValue(dateLabel);
				String startPlace1 = event1.getAttributeValue(startPlaceLabel);
				String endPlace1 = event1.getAttributeValue(endPlaceLabel);
				String startPlace2 = event2.getAttributeValue(startPlaceLabel);
				String endPlace2 = event2.getAttributeValue(endPlaceLabel);
				
				if (date1==null){
					System.err.println("Null date "+event1);
					date1="";
				}
				if (date2==null){
					System.err.println("Null date "+event2);
					date2="";
				}
				
				result = date1.compareTo(date2);
				
				// Birth events precede all others
				if (result == 0){
					if (startPlace1!=null && startPlace2==null){
						result = +1;
					} else if (startPlace2!=null && startPlace1 == null) {
						result = -1;
					}
					
				}
				
				if (result == 0){

					// Compare subjective time order
					for (Actor actor1 : event1.actors()){
						for (Actor actor2 : event2.actors()){
							if (actor1.getIndividual().equals(actor2.getIndividual())){
								String order1 = actor1.attributes().getValue("ORDER");
								String order2 = actor2.attributes().getValue("ORDER");
								if (order1!=null && order2!=null){
									int newResult = new Integer(order1).compareTo(new Integer(order2));
									if (newResult!=0){
										if (result==0){
											result = newResult;
										} else if (result!=newResult){
											System.err.println("Inconsistent Time Order "+event1+" for "+actor1);
										}
									}
								}
							}
						}
					}
				}
				
				if (result == 0) {

					if (endPlace1!=null && endPlace1.equals(startPlace2) && !(endPlace2!=null && endPlace2.equals(startPlace1))){
						result = -1;
					} else if (endPlace2!=null && endPlace2.equals(startPlace1) &&! (endPlace1!=null && endPlace1.equals(startPlace2))){
						result = 1;
					} else {
						result = new Integer(event1.getId()).compareTo(event2.getId());
					}

				}
			
		}
		//
		return result;
	}

}
