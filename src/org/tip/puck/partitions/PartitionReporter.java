package org.tip.puck.partitions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.geo.Geography;
import org.tip.puck.net.Individual;
import org.tip.puck.report.Report;
import org.tip.puck.report.ReportChart;
import org.tip.puck.report.ReportChart.GraphType;
import org.tip.puck.report.ReportTable;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.statistics.StatisticsCriteria;
import org.tip.puck.statistics.StatisticsReporter;
import org.tip.puck.util.Chronometer;
import org.tip.puck.util.MathUtils;

/**
 * 
 * @author TIP
 *
 */
public class PartitionReporter {
	
	private static final Logger logger = LoggerFactory.getLogger(PartitionReporter.class);

	/**
	 * 
	 * @param clusterSizeDistribution
	 * @param label
	 * @return
	 * @throws PuckException
	 */
	public static ReportChart createClusterDistributionChart(final Map<Integer, Integer> clusterSizeDistribution, final String label) throws PuckException {
		ReportChart result;

		if (clusterSizeDistribution == null) {
			result = null;
		} else {
			//
			result = new ReportChart(label, GraphType.STACKED_BARS);

			int clusterIndex = 0;

			for (Integer size : clusterSizeDistribution.keySet()) {
				//
				result.setHeader(size + "", clusterIndex);

				//
				result.addValue(clusterSizeDistribution.get(size), 0);

				//
				clusterIndex += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param segmentation
	 * @param criteria
	 * @return
	 * @throws PuckException
	 */
	public static <E> Report reportPartitionStatistics(final Segmentation segmentation, final StatisticsCriteria criteria) throws PuckException {
		Report result;

		if ((segmentation == null) || (criteria == null)) {
			throw PuckExceptions.INVALID_PARAMETER.create("Null parameter detected.");
		} else {
			result = new Report();
			Chronometer chrono = new Chronometer();

			result.setTitle("Basic partition statistics.");
			result.setOrigin("Partition reporter");
			result.setTarget(segmentation.getLabel());

			//
			if (segmentation.isOn()) {
				result.setInputComment("Segmentation:\n" + segmentation.getSummary());
			}

			//
			List<ReportChart> charts = new ArrayList<ReportChart>(20);
			List<ReportTable> sourceTables = new ArrayList<ReportTable>(20);
			Map<ReportTable, ReportTable> tables = new HashMap<ReportTable, ReportTable>(20);
			Map<ReportTable, Double> meanValues = new HashMap<ReportTable, Double>(20);
			Map<ReportTable, Double> medianValues = new HashMap<ReportTable, Double>(20);
			Map<ReportTable, Integer> nrClusters = new HashMap<ReportTable, Integer>(20);
			Map<ReportTable, Integer> nrItems = new HashMap<ReportTable, Integer>(20);

			// Compute charts and tables.
			for (PartitionCriteria partitionCriteria : criteria.getPartitionCriteriaList()) {

				if (StringUtils.isEmpty(partitionCriteria.getLabel())) {
					continue;
				}
				//
				Partition<Individual> partition = PartitionMaker.create(segmentation.getLabel(), segmentation.getCurrentIndividuals(), partitionCriteria, segmentation.getGeography());

				ReportChart sourceChart = StatisticsReporter.createPartitionChart(partition, partitionCriteria, criteria.getSplitCriteria(), segmentation.getGeography());
				ReportTable sourceTable = ReportTable.transpose(sourceChart.createReportTableWithSum());
				sourceTables.add(sourceTable);

				Map<Integer, Integer> clusterSizeDistribution = ReportTable.columnSizeDistribution(sourceTable);

				ReportChart chart = createClusterDistributionChart(clusterSizeDistribution, partition.getCriteriaLabel());
				if (chart != null) {
					//
					charts.add(chart);
					ReportTable table = ReportTable.transpose(chart.createReportTableWithSum());
					tables.put(sourceTable, table);
					meanValues.put(sourceTable, MathUtils.meanValue(clusterSizeDistribution));
					medianValues.put(sourceTable, MathUtils.medianValue(clusterSizeDistribution));
					nrClusters.put(sourceTable, partition.nonNullClusterCount());
					nrItems.put(sourceTable, partition.clusteredItemsCount());
				}

			}

			// Manage the number of chart by line.
			for (int chartIndex = 0; chartIndex < charts.size(); chartIndex++) {
				result.outputs().append(charts.get(chartIndex));
				if (chartIndex % 4 == 3) {
					result.outputs().appendln();
				}
			}

			// Add chart tables.
			for (ReportTable sourceTable : sourceTables) {
				result.outputs().appendln(sourceTable.getTitle());
				result.outputs().appendln();

				ReportTable table = tables.get(sourceTable);
				
				result.outputs().appendln("number of (nonnull) clusters =" + nrClusters.get(sourceTable));
				result.outputs().appendln("number of clustered items =" + nrItems.get(sourceTable));
				result.outputs().appendln("mean =" + meanValues.get(sourceTable));
				result.outputs().appendln("median =" + medianValues.get(sourceTable));
				result.outputs().appendln(table);
			}

			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}

}
