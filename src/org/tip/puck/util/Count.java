package org.tip.puck.util;

/**
 * 
 * @author TIP
 */
public class Count {

	private double value;
	private double min;
	private double max;

	/**
	 * 
	 */
	public Count() {
	}

	/**
	 * 
	 */
	public Count(final double value, final double min, final double max) {
		set(value, min, max);
	}

	/**
	 * 
	 * @return
	 */
	public double add(final double value) {
		double result;

		this.value += value;
		result = this.value;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double addMax(final double value) {
		double result;

		this.max += value;
		result = this.max;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double addMin(final double value) {
		double result;

		this.min += value;
		result = this.min;

		//
		return result;
	}

	public double get() {
		return this.value;
	}

	public double getMax() {
		return this.max;
	}

	public double getMin() {
		return this.min;
	}

	/**
	 * 
	 * @return
	 */
	public double inc() {
		double result;

		this.value += 1;
		result = this.value;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double incMax() {
		double result;

		this.max += 1;
		result = this.max;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double incMin() {
		double result;

		this.min += 1;
		result = this.min;

		//
		return result;
	}

	public void set(final double value) {
		this.value = value;
	}

	/**
	 * 
	 * @param agnaticValue
	 * @param uterineValue
	 * @param cognaticValue
	 * @return
	 */
	public Count set(final double value, final double min, final double max) {
		Count result;

		this.value = value;
		this.min = min;
		this.max = max;

		result = this;

		//
		return result;
	}

	public void setMax(final double value) {
		this.max = value;
	}

	public void setMin(final double value) {
		this.min = value;
	}

}
