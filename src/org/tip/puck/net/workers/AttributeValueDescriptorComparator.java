package org.tip.puck.net.workers;

import java.util.Comparator;

import org.tip.puck.util.MathUtils;

/**
 * 
 * @author TIP
 * 
 */
public class AttributeValueDescriptorComparator implements Comparator<AttributeValueDescriptor> {

	public enum Sorting {
		VALUE,
		COUNT
	}

	private Sorting sorting;

	/**
	 * 
	 * @param sorting
	 */
	public AttributeValueDescriptorComparator(final Sorting sorting) {
		this.sorting = sorting;
	}

	/**
	 * 
	 */
	@Override
	public int compare(final AttributeValueDescriptor alpha, final AttributeValueDescriptor bravo) {
		int result;

		result = compare(alpha, bravo, this.sorting);

		//
		return result;
	}

	/**
	 * 
	 */
	public static int compare(final AttributeValueDescriptor alpha, final AttributeValueDescriptor bravo, final Sorting sorting) {
		int result;

		//
		switch (sorting) {
			case VALUE:
			default:
				//
				result = compare(getValue(alpha), getValue(bravo));
			break;

			case COUNT:
				//
				result = MathUtils.compare(getCount(alpha), getCount(bravo));

				if (result == 0) {

					result = compare(alpha, bravo, Sorting.VALUE);
				}
			break;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static int compare(final String alpha, final String bravo) {
		int result;

		//
		if (alpha == null) {
			//
			result = -1;

		} else if (bravo == null) {
			//
			result = +1;

		} else {
			//
			result = alpha.compareTo(bravo);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Long getCount(final AttributeValueDescriptor source) {
		Long result;

		//
		if (source == null) {
			//
			result = null;

		} else {
			//
			result = source.getCount();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String getValue(final AttributeValueDescriptor source) {
		String result;

		//
		if (source == null) {
			//
			result = null;
		} else {
			//
			result = source.getValue();
		}

		//
		return result;
	}
}
