package org.tip.puck.visualization.layouts.sugiyama;

import org.gephi.graph.api.Node;
import org.gephi.graph.spi.LayoutData;

/**
 * @date Jun 25, 2012
 * @author Edoardo Savoia
 */
class NodeWrapper implements LayoutData, Comparable<NodeWrapper> {

    public static void initData(Node node, NodeWrapper wrapper) {
	NodeWrapper nodeData = (NodeWrapper) node.getNodeData().getLayoutData();
	if (nodeData == null) {
	    node.getNodeData().setLayoutData(wrapper);
	}
    }

    public static NodeWrapper getData(Node node) {
	return (NodeWrapper) node.getNodeData().getLayoutData();
    }

    public static void clean(Node node) {
	NodeWrapper nodeData = getData(node);
	if (nodeData != null) {
	    node.getNodeData().setLayoutData(null);
	}
    }

    /**
     * Sum value for edge Crosses.
     */
    private double edgeCrossesIndicator = 0;
    /**
     * Counter for additions to the edgeCrossesIndicator.
     */
    private int additions = 0;
    /**
     * Node's vertical level.
     */
    private int level = 0;
    /**
     * Current position in the grid.
     */
    private int gridPosition = 0;
    /**
     * Priority for movements.
     * Is used when moving node to its barycenter.
     */
    private int priority = 0;
    
    NodeWrapper(int level, double edgeCrossesIndicator) {
	this.level = level;
	this.edgeCrossesIndicator = edgeCrossesIndicator;
	additions++;
    }

    /**
     * Gets the average value for the edge crosses indicator.
     */
    double getEdgeCrossesIndicator() {
	if (additions == 0) {
	    return 0;
	}
	return edgeCrossesIndicator / additions;
    }

    /**
     * Adds a value to the edge crosses indicator.
     */
    void addToEdgeCrossesIndicator(double addValue) {
	edgeCrossesIndicator += addValue;
	additions++;
    }

    /**
     * Gets the level of the wrapped node.
     */
    int getLevel() {
	return level;
    }

    /**
     * Gets the grid position for the wrapped node.
     */
    int getGridPosition() {
	return gridPosition;
    }

    /**
     * Sets the grid position for the wrapped node.
     */
    void setGridPosition(int pos) {
	this.gridPosition = pos;
    }

    /**
     * Increments priority of this node wrapper.
     */
    void incrementPriority() {
	priority++;
    }

    /**
     * Gets the priority of this node wrapper.
     */
    int getPriority() {
	return priority;
    }

    @Override
    public int compareTo(NodeWrapper compare) {
	if (compare.getEdgeCrossesIndicator() == this.getEdgeCrossesIndicator()) {
	    return 0;
	}
	double compareValue = compare.getEdgeCrossesIndicator() - this.getEdgeCrossesIndicator();
	return (int) Math.round(compareValue);
    }
}
