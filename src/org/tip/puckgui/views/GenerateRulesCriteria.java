package org.tip.puckgui.views;

/**
 * 
 * @author TIP
 */
public class GenerateRulesCriteria {
	private int generationCount;

	/**
	 * 
	 */
	public GenerateRulesCriteria() {
		this.generationCount = 1000;
	}

	public int getGenerationCount() {
		return generationCount;
	}

	public void setGenerationCount(final int generationCount) {
		this.generationCount = generationCount;
	}
}
