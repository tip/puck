package org.tip.puck.visualization.style;

import org.gephi.project.api.Workspace;
import org.tip.puck.visualization.style.attributes.AttributeMap;
import org.tip.puck.visualization.style.attributes.GraphStyle;

/**
 * Style Interface.
 *
 * @date May 14, 2012
 * @author Edoardo Savoia
 */
public interface Style {

    /*
     * Returns the graph type (directed, undirected, mixed) needed by style
     */
    GraphStyle graphStyle();

    /*
     * Returns the attribute mapping from Graph<E> properties to Gephi's
     */
    AttributeMap styleMapping();

    /*
     * Styles the given workspace (containing the graph)
     */
    void applyStyle(Workspace workspace);

}
