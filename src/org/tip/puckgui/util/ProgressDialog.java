package org.tip.puckgui.util;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.util.Chronometer;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class ProgressDialog extends JDialog {

	private static final long serialVersionUID = -8257265677669480909L;

	private static Logger logger = LoggerFactory.getLogger(ProgressDialog.class);

	private final JPanel contentPanel = new JPanel();
	private Timer progressUpdater;
	private JProgressBar progressBar;
	private JLabel lblTimer;
	private JLabel lblCounter;
	private JLabel lblfilledPlaces;

	/**
	 * Create the dialog.
	 */
	public ProgressDialog(final ProgressStatus status) {

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModal(true);
		setTitle("Download GeoNames dump files");
		setBounds(100, 100, 523, 293);
		setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new BoxLayout(this.contentPanel, BoxLayout.Y_AXIS));
		{
			JPanel introPanel = new JPanel();
			this.contentPanel.add(introPanel);
			{
				JLabel lblDescription = new JLabel(status.getDescription());
				introPanel.add(lblDescription);
			}
		}
		{
			Component verticalStrut = Box.createVerticalStrut(10);
			this.contentPanel.add(verticalStrut);
		}
		{
			JPanel inputPanel = new JPanel();
			this.contentPanel.add(inputPanel);
			inputPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
					FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
					FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));
			{
				this.progressBar = new JProgressBar();
				this.progressBar.setEnabled(false);
				inputPanel.add(this.progressBar, "4, 2");
			}
			{
				this.lblCounter = new JLabel("- / - (  0 %)");
				inputPanel.add(this.lblCounter, "4, 4, center, default");
			}
			{
				this.lblfilledPlaces = new JLabel("Secondary count: -");
				inputPanel.add(this.lblfilledPlaces, "4, 6, center, default");
			}
			{
				this.lblTimer = new JLabel("00:00:00");
				inputPanel.add(this.lblTimer, "4, 9, center, default");
			}
		}

		// ///////////////////////////////////////////
		doWork();
	}

	/**
	 * 
	 */
	private void doWork() {

		final ProgressStatus fillStatus = new ProgressStatus();

		ActionListener taskPerformer = new ActionListener() {
			/**
			 * 
			 */
			@Override
			public void actionPerformed(final ActionEvent event) {
				refreshDisplay(fillStatus);
			}
		};
		this.progressUpdater = new Timer(1000, taskPerformer);
		this.progressUpdater.setRepeats(true);
		this.progressUpdater.start();
	}

	/**
	 * 
	 */
	private void refreshDisplay(final ProgressStatus source) {
		// Update.
		ProgressDialog.this.progressBar.setValue(source.getCurrentRate());

		String counterValue = String.format("%d / %d (%3d %%)", source.getCurrent(), source.getMax(), source.getCurrentRate());
		ProgressDialog.this.lblCounter.setText(counterValue);

		String filledCounter = String.format("Secondary count: %d", source.getExtra1());
		ProgressDialog.this.lblfilledPlaces.setText(filledCounter);

		String timerValue = Chronometer.toTimer(source.getChrono().stop().interval());
		ProgressDialog.this.lblTimer.setText(timerValue);
	}

	/**
	 * This method shows the dialog in center of the screen.
	 */
	public static void showDialog() {
		showDialog(null, null);
	}

	/**
	 * This method shows the dialog.
	 */
	public static void showDialog(final Component parent, final ProgressStatus status) {
		//
		ProgressDialog dialog = new ProgressDialog(status);
		dialog.pack();
		dialog.setLocationRelativeTo(parent);
		dialog.setVisible(true);
	}
}
