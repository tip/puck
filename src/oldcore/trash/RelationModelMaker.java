package oldcore.trash;

public class RelationModelMaker {
	
	

/*	private static void compose1(final RelationModel model, final Individual indi, final Gender egoGender, final Queue<Individual> queue,
			final List<Individual> visited, final List<String> egoNeutral) {

		RoleDefinitions roleDefinitions = model.roleDefinitions();

		if (!indi.getName().equalsIgnoreCase("ego") && roleDefinitions.getRoleByName(indi.getName()) == null) {
			System.out.println("role definition missing: " + indi+" for "+egoGender+" ego");
		}

		if (indi.getFather() != null && indi.getFather().getName() != null && !visited.contains(indi.getFather())) {
			indi.getFather().setAttribute("EGOGENDER", egoGender.toString());
			indi.getFather().setAttribute("GENERATION", (Integer.parseInt(indi.getAttributeValue("GENERATION")) + 1) + "");
			Gender childGender = indi.getGender();
			if (indi.getName().equalsIgnoreCase("Ego")) {
				if (egoNeutral.contains("PARENT")) {
					childGender = Gender.UNKNOWN;
				}
				roleDefinitions.addNew(new RoleDefinition(roleDefinitions.size(), model.role(indi.getFather()), Primary.PARENT, null, null, Gender.MALE, null,
						childGender));
			} else {
				for (Role fatherRole : roleDefinitions.getRoles(Primary.PARENT, Gender.MALE, null, childGender)) {
					Roles composition = new Roles();
					composition.add(roleDefinitions.getRoleByName(indi.getName()));
					composition.add(fatherRole);
					roleDefinitions.addNew(new RoleDefinition(roleDefinitions.size(), model.role(indi.getFather()), null, null, composition, Gender.MALE, null,
							egoGender));
				}
			}
			queue.add(indi.getFather());
			visited.add(indi.getFather());
		}

		if (indi.getMother() != null && indi.getMother().getName() != null && !visited.contains(indi.getMother())) {
			indi.getMother().setAttribute("EGOGENDER", egoGender.toString());
			indi.getMother().setAttribute("GENERATION", (Integer.parseInt(indi.getAttributeValue("GENERATION")) + 1) + "");
			Gender childGender = indi.getGender();
			if (indi.getName().equalsIgnoreCase("Ego")) {
				if (egoNeutral.contains("PARENT")) {
					childGender = Gender.UNKNOWN;
				}
				roleDefinitions.addNew(new RoleDefinition(roleDefinitions.size(), model.role(indi.getMother()), Primary.PARENT, null, null, Gender.FEMALE,
						null, childGender));
			} else {
				for (Role motherRole : roleDefinitions.getRoles(Primary.PARENT, Gender.FEMALE, null, childGender)) {
					Roles composition = new Roles();
					composition.add(roleDefinitions.getRoleByName(indi.getName()));
					composition.add(motherRole);
					roleDefinitions.addNew(new RoleDefinition(roleDefinitions.size(), model.role(indi.getMother()), null, null, composition, Gender.FEMALE,
							null, egoGender));
				}
			}
			queue.add(indi.getMother());
			visited.add(indi.getMother());
		}

		for (Individual sibling : indi.siblings()) {
			
			Gender siblingGender = indi.getGender();
			if (egoNeutral.contains("SIBLING")) {
				siblingGender = Gender.UNKNOWN;
			}

			if (sibling.getName() != null && !visited.contains(sibling)) {
				sibling.setAttribute("EGOGENDER", egoGender.toString());
				sibling.setAttribute("GENERATION", indi.getAttributeValue("GENERATION"));
				if (indi.getName().equalsIgnoreCase("Ego")) {
					roleDefinitions.addNew(new RoleDefinition(roleDefinitions.size(), model.role(sibling), Primary.SIBLING, null, null, sibling.getGender(),
							getAlterAge(sibling, indi), siblingGender));
				} else {
					for (Role siblingRole : roleDefinitions.getRoles(Primary.SIBLING, sibling.getGender(), getAlterAge(sibling, indi), siblingGender)) {
						Roles composition = new Roles();
						composition.add(roleDefinitions.getRoleByName(indi.getName()));
						composition.add(siblingRole);
						roleDefinitions.addNew(new RoleDefinition(roleDefinitions.size(), model.role(sibling), null, null, composition, sibling.getGender(),
								null, egoGender));
					}
				}
				queue.add(sibling);
				visited.add(sibling);
			}
		}

		for (Individual spouse : indi.getPartners()) {

			Gender spouseGender = indi.getGender();

			if (spouse.getName() != null && !visited.contains(spouse)) {
				spouse.setAttribute("EGOGENDER", egoGender.toString());
				spouse.setAttribute("GENERATION", indi.getAttributeValue("GENERATION"));
				if (indi.getName().equalsIgnoreCase("Ego")) {
					if (egoNeutral.contains("SPOUSE")) {
						spouseGender = Gender.UNKNOWN;
					}
					roleDefinitions.addNew(new RoleDefinition(roleDefinitions.size(), model.role(spouse), Primary.SPOUSE, null, null, spouse.getGender(), null,
							spouseGender));
				} else {
					for (Role spouseRole : roleDefinitions.getRoles(Primary.SPOUSE, spouse.getGender(), null, spouseGender)) {
						Roles composition = new Roles();
						composition.add(roleDefinitions.getRoleByName(indi.getName()));
						composition.add(spouseRole);
						roleDefinitions.addNew(new RoleDefinition(roleDefinitions.size(), model.role(spouse), null, null, composition, spouse.getGender(),
								null, egoGender));
					}
				}
				queue.add(spouse);
				visited.add(spouse);
			}
		}

		for (Individual child : indi.children()) {

			Gender parentGender = indi.getGender();

			if (child.getName() != null && !visited.contains(child)) {
				child.setAttribute("EGOGENDER", egoGender.toString());
				child.setAttribute("GENERATION", (Integer.parseInt(indi.getAttributeValue("GENERATION")) - 1) + "");

				if (indi.getName().equalsIgnoreCase("Ego")) {
					if (egoNeutral.contains("CHILD")) {
						parentGender = Gender.UNKNOWN;
					}
					Roles parentRoles = roleDefinitions.getRoles(Primary.PARENT, parentGender, null, child.getGender());
					for (Role role : parentRoles) {
						roleDefinitions.addNew(new RoleDefinition(roleDefinitions.size(), model.role(child), null, role, null, child.getGender(), null,
								parentGender));
					}
				} else {
					Roles parentRoles = roleDefinitions.getRoles(Primary.PARENT, parentGender, null, child.getGender());

					for (Role parentRole : parentRoles) {
						Role childRole = roleDefinitions.getInverseRole(parentRole, child.getGender(), parentGender);
						if (childRole != null) {
							Roles composition = new Roles();
							composition.add(roleDefinitions.getRoleByName(indi.getName()));
							composition.add(childRole);
							roleDefinitions.addNew(new RoleDefinition(roleDefinitions.size(), model.role(child), null, null, composition, child.getGender(),
									getAlterAge(child), egoGender));
						} else {
							System.out.println("Missing " + indi.getGender() + " (" + parentGender + ") " + " parentRole for " + child.getGender() + " child :"
									+ indi + " for " + child);
						}
					}
				}
				queue.add(child);
				visited.add(child);
			}
		}
		
	}

	private static void composeParents(final RelationModel model, final Individual indi, final Gender egoGender, final Queue<Individual> queue,
			final List<Individual> visited, final List<String> egoNeutral) {

		RoleDefinitions roleDefinitions = model.roleDefinitions();

		if (indi.getFather() != null && indi.getFather().getName() != null && !visited.contains(indi.getFather())) {
			indi.getFather().setAttribute("EGOGENDER", egoGender.toString());
			indi.getFather().setAttribute("GENERATION", (Integer.parseInt(indi.getAttributeValue("GENERATION")) + 1) + "");
			Gender childGender = indi.getGender();
			if (egoNeutral.contains("PARENT")) {
				childGender = Gender.UNKNOWN;
			}
			if (indi.getName().equalsIgnoreCase("Ego")) {
				roleDefinitions.addNew(new RoleDefinition(roleDefinitions.size(), model.role(indi.getFather()), Primary.PARENT, null, null, Gender.MALE, null,
						childGender));
			} else {
				for (Role fatherRole : roleDefinitions.getRoles(Primary.PARENT, Gender.MALE, null, childGender)) {
					Roles composition = new Roles();
					composition.add(roleDefinitions.getRoleByName(indi.getName()));
					composition.add(fatherRole);
					roleDefinitions.addNew(new RoleDefinition(roleDefinitions.size(), model.role(indi.getFather()), null, null, composition, null, null,
							egoGender));
				}
			}
			queue.add(indi.getFather());
			visited.add(indi.getFather());
		}

		if (indi.getMother() != null && indi.getMother().getName() != null && !visited.contains(indi.getMother())) {
			indi.getMother().setAttribute("EGOGENDER", egoGender.toString());
			indi.getMother().setAttribute("GENERATION", (Integer.parseInt(indi.getAttributeValue("GENERATION")) + 1) + "");
			Gender childGender = indi.getGender();
			if (egoNeutral.contains("PARENT")) {
				childGender = Gender.UNKNOWN;
			}
			if (indi.getName().equalsIgnoreCase("Ego")) {
				roleDefinitions.addNew(new RoleDefinition(roleDefinitions.size(), model.role(indi.getMother()), Primary.PARENT, null, null, Gender.FEMALE,
						null, childGender));
			} else {
				for (Role motherRole : roleDefinitions.getRoles(Primary.PARENT, Gender.FEMALE, null, childGender)) {
					Roles composition = new Roles();
					composition.add(roleDefinitions.getRoleByName(indi.getName()));
					composition.add(motherRole);
					roleDefinitions.addNew(new RoleDefinition(roleDefinitions.size(), model.role(indi.getMother()), null, null, composition, null, null,
							egoGender));
				}
			}
			queue.add(indi.getMother());
			visited.add(indi.getMother());
		}

	}

	public static RelationModel create(final Net net) {
		RelationModel result;

		result = new RelationModel(net.getLabel());
		createRoleRelations(result, net);

		//
		return result;
	}
	
	public static RelationModel createFromPositionList(final String name, final StringList roleDefinitionsList, Report report) {
		RelationModel result;

		result = new RelationModel(name);
		Report importReport = new Report("Import log");
		report.outputs().append(importReport);

		createRoleRelationsFromPositionList(result, roleDefinitionsList,importReport);

		//
		return result;
	}

	public static RelationModel create(final String name, final StringList roleDefinitionsList) {
		RelationModel result;

		result = new RelationModel(name);
		createRoleDefinitions(result, roleDefinitionsList);

		//
		return result;
	}

	public static RelationModel createNew(final String name, final StringList roleDefinitionsList, Report report) {
		RelationModel result;

		Report importReport = new Report("Import log");
		Report exportReport = new Report("Export log");
		report.outputs().append(importReport);
		report.outputs().append(exportReport);
		
		result = new RelationModel(name);
		createRoleRelations(result, roleDefinitionsList, importReport);
		
		createRoleDefinitions(result.getRoleRelations(),exportReport);

		//
		return result;
	}
	
	public static RoleRelations createRoleRelations(final RelationModel model, final Net net) {
		RoleRelations result;
		
		result = new RoleRelations();
		model.setRoleRelations(result);
		Queue<Individual> queue = new LinkedList<Individual>();

		Individual maleEgo = null;
		Individual femaleEgo = null;

		// Determine egos
		for (Individual indi : net.individuals()) {
			if (indi.getName().equalsIgnoreCase("[self]")) {
				indi.setAttribute("EGOGENDER", indi.getGender().toString());
				indi.setAttribute("GENERATION", "0");
				queue.add(indi);
				
				if (indi.isMale()) {
					maleEgo = indi;
				} else if (indi.isFemale()) {
					femaleEgo = indi;
				}
				if (maleEgo != null && femaleEgo != null) {
					result.setEgoGenderDistinction(true);
					break;
				}
			}
		}

		List<String> egoNeutral = new ArrayList<String>();

		if ((maleEgo != null && maleEgo.isSterile()) || (femaleEgo != null && femaleEgo.isSterile()) || !result.isEgoGenderDistinction()) {
			egoNeutral.add("PARENT");
		}
		if ((maleEgo != null && maleEgo.isOrphan()) || (femaleEgo != null && femaleEgo.isOrphan()) || !result.isEgoGenderDistinction()) {
			egoNeutral.add("CHILD");
		}
		if ((maleEgo != null && maleEgo.isSingle()) || (femaleEgo != null && femaleEgo.isSingle()) || !result.isEgoGenderDistinction()) {
			egoNeutral.add("SPOUSE");
		}
		if ((maleEgo != null && maleEgo.isUnique()) || (femaleEgo != null && femaleEgo.isUnique()) || !result.isEgoGenderDistinction()) {
			egoNeutral.add("SIBLING");
		}


		while (!queue.isEmpty()) {
			Individual indi = queue.poll();
			compose(result, indi, queue, egoNeutral);
		}
		
		//
		return result;
	}


	
	public static RoleDefinitions createRoleDefinitions1(final RelationModel model, final Net net) {
		RoleDefinitions result;
		
		result = new RoleDefinitions();
		model.setRoleDefinitions(result);

		Individual maleEgo = null;
		Individual femaleEgo = null;

		// Determine egos
		for (Individual indi : net.individuals()) {
			if (indi.getName().equalsIgnoreCase("Ego")) {
				indi.setAttribute("EGOGENDER", indi.getGender().toString());
				indi.setAttribute("GENERATION", "0");
				if (indi.isMale()) {
					maleEgo = indi;
				} else if (indi.isFemale()) {
					femaleEgo = indi;
				}
				if (maleEgo != null && femaleEgo != null) {
					result.setEgoGenderDistinction(true);
					break;
				}
			}
		}

		List<String> egoNeutral = new ArrayList<String>();

		if ((maleEgo != null && maleEgo.isSterile()) || (femaleEgo != null && femaleEgo.isSterile()) || !result.isEgoGenderDistinction()) {
			egoNeutral.add("CHILD");
		}
		if ((maleEgo != null && maleEgo.isOrphan()) || (femaleEgo != null && femaleEgo.isOrphan()) || !result.isEgoGenderDistinction()) {
			egoNeutral.add("PARENT");
		}
		if ((maleEgo != null && maleEgo.isSingle()) || (femaleEgo != null && femaleEgo.isSingle()) || !result.isEgoGenderDistinction()) {
			egoNeutral.add("SPOUSE");
		}
		if ((maleEgo != null && maleEgo.isUnique()) || (femaleEgo != null && femaleEgo.isUnique()) || !result.isEgoGenderDistinction()) {
			egoNeutral.add("SIBLING");
		}

		Queue<Individual> maleQueue = new LinkedList<Individual>();
		Queue<Individual> femaleQueue = new LinkedList<Individual>();
		List<Individual> visited = new ArrayList<Individual>();

		// Temporary: preliminary separate parent composition for male and
		// female ego to assure disponibility of child roles for all genders

		if (maleEgo != null) {
			maleQueue.add(maleEgo);
			visited.add(maleEgo);
			composeParents(model, maleEgo, Gender.MALE, maleQueue, visited, egoNeutral);
		}
		if (femaleEgo != null) {
			femaleQueue.add(femaleEgo);
			visited.add(femaleEgo);
			composeParents(model, femaleEgo, Gender.FEMALE, femaleQueue, visited, egoNeutral);
		}

		if (maleEgo != null) {
			compose(model, maleEgo, Gender.MALE, maleQueue, visited, egoNeutral);
			System.out.println("Male Ego environments complete: " + maleQueue);
		}

		if (femaleEgo != null) {
			compose(model, femaleEgo, Gender.FEMALE, femaleQueue, visited, egoNeutral);
			System.out.println("Female Ego environments complete: " + femaleQueue);
		}

		while (!maleQueue.isEmpty()) {
			Individual indi = maleQueue.remove();
			Gender egoGender = Gender.valueOf(indi.getAttributeValue("EGOGENDER"));
			compose(model, indi, egoGender, maleQueue, visited, egoNeutral);
		}
		
		while (!femaleQueue.isEmpty()) {
			Individual indi = femaleQueue.remove();
			Gender egoGender = Gender.valueOf(indi.getAttributeValue("EGOGENDER"));
			compose(model, indi, egoGender, femaleQueue, visited, egoNeutral);
		}
		
		result = result.clean();
		
		//
		return result;
	}

	public static RoleDefinitions createRoleDefinitionsFromPositionList1(final RelationModel model, final StringList rolePositionList) {
		RoleDefinitions result;

		result = new RoleDefinitions();
		
		Map<String,Roles> pseudoRoles = new TreeMap<String,Roles>();
		
		
		int id = 0;
		
		// Set primary terms
		
		for (String rolePositionLine : rolePositionList) {
			
			String[] items = rolePositionLine.split("\t");
			id++;
			
			RoleDefinition definition = new RoleDefinition(id);
			
			for (int idx = 0; idx < items.length; idx++) {
				
				String item = items[idx];
				if (StringUtils.isEmpty(item)) {
					continue;
				}
				
				switch (idx) {
				case 0:
					definition.setRole(model.role(item));
				break;
				case 1:
					if (Character.isLowerCase(item.charAt(item.length()-1))){
						definition.setOther(item);
						item = null;
					} else if (item.charAt(0)=='e'){
						definition.setAlterAge(AlterAge.ELDER);
						item = item.substring(1);
					} else if (item.charAt(0)=='y'){
						definition.setAlterAge(AlterAge.YOUNGER);
						item = item.substring(1);
					}
					if (item!=null && !RoleDefinitions.setPrimaryFromString(definition,item)){
						definition.setComposition();
						definition.composition().add(new Role(item));
						if (pseudoRoles.get(item)==null){
							pseudoRoles.put(item,new Roles());
						}
						pseudoRoles.get(item).add(definition.role());
						
						char firstLetter = item.charAt(0);
						if (definition.egoGender().isUnknown() && (firstLetter=='H' || firstLetter=='W')){
							definition.setEgoGender(Gender.valueOf(firstLetter).invert());
						}

					}
				break;
				case 2:
					definition.setEgoGender(Gender.valueOf(item.charAt(0)));
				break;
				}
			}
			
			result.addNew(definition);
		}
		
		// Set inversions of primary terms
		
		result.setInversePrimaryTerms();
		
		// Set other terms
		
		result = result.clean();
		for (RoleDefinition definition : result.toSortedList()){

			if (definition.composition()!=null && definition.composition().size()==1 && pseudoRoles.containsKey(definition.composition().get(0).getName())){
				
				String name = definition.composition().get(0).getName();
				String firstItem = name.substring(0, name.length()-1);
				String lastItem = name.substring(name.length()-1);
				
				Roles firstRoles = null;
				
				if (firstItem.length()==1){
					firstRoles = result.getPrimaryTerms(firstItem, definition.egoGender(), null, Gender.UNKNOWN);
				}
				
				if (firstRoles==null){
					firstRoles = pseudoRoles.get(firstItem);
				}
				
				if (firstRoles==null){
					firstRoles = new Roles();
					firstRoles.add(new Role("["+firstItem+"]"));
				}
				
				Gender egoGender = Gender.UNKNOWN;
				char genderLetter = firstItem.charAt(firstItem.length()-1);
				if (genderLetter=='S' || genderLetter=='F' || genderLetter=='H'  || genderLetter=='B' ){
					egoGender = Gender.MALE;
				} else if (genderLetter=='D' || genderLetter=='M' || genderLetter=='W'  || genderLetter=='Z' ){
					egoGender = Gender.FEMALE;
				}
				
				for (Role firstRole : firstRoles){

					definition.composition().set(0, firstRole);
					Roles lastRoles = result.getPrimaryTerms(lastItem,egoGender,definition.alterAge(), definition.alterGender());

					if (lastRoles.size()>0){
						result.removeById(definition.getId());
						for (Role lastRole : lastRoles) {
							RoleDefinition def1 = definition.clone();
							def1.setId(result.getLastId()+1);
							def1.composition().add(lastRole);
							result.addNew(def1);
						}
					}
				}
			}
		}
		
		result = result.clean();
		
		//
		model.setRoleDefinitions(result);
		return result;
	}
	
	private static void putRoleDefinition(RoleRelations result, RoleRelation roleRelation, Role alter, MetaRole alterRole, Map<Role,Roles> genderConfigs, boolean withReciprocal, StringList report){	

		RoleActor self = roleRelation.getSelf();
		RoleActor alterActor = new RoleActor(alter,alterRole);

		// Restrict gender scope
		
		Gender egoGender = self.getEgoGender();
		
		if (!egoGender.isUnknown()){
			if (alterRole.getEgoGender().isUnknown()){
				alterActor = alterActor.withEgoGender(egoGender);
			// Temporary controls, to be removed when code is stable
			} else if (alterRole.getEgoGender()!=egoGender) {
				System.err.println("Incompatible ego gender for "+self+" and "+alter);
			}
		} else if (alterRole.getEgoGender()!=egoGender){
			System.err.println("Gender for "+self+" restricted by "+alterActor);
		}
		
		// Put relation 
		
		if (alterActor.isValid(genderConfigs)){

			if (roleRelation.actors().addNew(alterActor)){
				
				String reciprocal = " ";
				if (!withReciprocal){
					reciprocal = " reciprocal ";
				}
				report.appendln("Defined"+reciprocal+"role:\t"+alter+"\t"+self+"\t"+alterRole);
			}
			
			if (withReciprocal){
				putReciprocalRoleDefinition(result,self,alterActor,genderConfigs,report);
			}		
		}
	}
	
	private static Gender getCommonEgoGender(Gender alpha, Gender beta){
		Gender result;
				
		if (alpha == null || beta == null) {
			
			result = null;

		} else if (alpha == beta || beta.isUnknown()) {
			
			result = alpha;
			
		} else if (alpha.isUnknown()){
			
			result = beta;
			
		} else {
			
			result = null;
		}
		//
		return result;
	}
	
	private static void restrictGenderScopes (RoleRelations result, Map<Role,Map<String,MetaRole>> genderConfigs, Report report) {
		
		for (RoleRelation alterRelation : result.toSortedList()){
			
			Role alter = alterRelation.getSelfRole();
			
			// Restrict egoGender scope for composed terms

			MetaRole genderConfig = getUniqueGenderConfig(null,genderConfigs.get(alter));
//			MetaRole genderConfig = getUniqueGenderConfig(null,result.getGenderConfiguration(alter));
			if (genderConfig!=null && !genderConfig.getEgoGender().isUnknown()){
				
				report.outputs().appendln();
				report.outputs().appendln("Rendered ego gender compatible with  "+alter);

				for (RoleActor actor : alterRelation.actors()){
					if (!actor.getRole().getName().equals("SELF")){
						if (actor.getEgoGender().isUnknown()){
							actor.getRole().setEgoGender(genderConfig.getEgoGender());
							report.outputs().appendln("Reduced scope of ego gender for "+actor);
						} else if (actor.getEgoGender()!=genderConfig.getEgoGender()) {
							System.err.println("Incompatible ego gender for "+alter+" and "+actor);
						}
					}
				}
			}
		}
	}
	

	
	private static RoleRelation putRelation (RoleRelations result, RoleActor self, Map<Role,Roles> genderConfigs){
		RoleRelation roleRelation;
		
		roleRelation = result.getAdjustedSelfRelation(self,genderConfigs);
		
		if (roleRelation==null){
			
			RoleActor adjustedSelf = getAdjustedSelfEquivalent(self,genderConfigs);
			
			if (adjustedSelf.isValid(genderConfigs)){
				roleRelation = new RoleRelation(result.size()+1);
				adjustedSelf.setId(roleRelation.getId());
				roleRelation.actors().addNew(adjustedSelf);
				result.add(roleRelation);
			}
		}
		
		//
		return roleRelation;
	}
	
	
	private static void closeSiblingTriangles (RoleRelations result, Map<Role,Roles> genderConfigs, Report report) {
		
		report.outputs().appendln("Create sibling relations between (non-individualized) full co-siblings");
		report.outputs().appendln();

		for (RoleRelation relation : result.toSortedList()){

			StringList subReport = new StringList();

//			RoleActor self = relation.getSelf();
			RoleActors siblings = relation.getActorsByRoleNameWithUnspecific("SIBLING",result);

			for (RoleActor sibling : siblings){
				
				for (RoleActor secondSibling : siblings){
										
					Gender egoGender = getCommonEgoGender(sibling.getEgoGender(),secondSibling.getEgoGender());
					
					if (egoGender!=null){

						RoleActor firstSibling = sibling.withEgoGender(egoGender);
						secondSibling = secondSibling.withEgoGender(egoGender);
						
						if (!isIndividualizable(result,sibling,genderConfigs) && !isIndividualizable(result,secondSibling,genderConfigs)){
														
							putRoleDefinition(result, firstSibling, secondSibling, genderConfigs, true,subReport);

//							RoleRelation siblingRelation = putRelation(result,oldSibling,genderConfigs);
//							putRoleDefinition(result,siblingRelation,otherSibling.getIndividual(),otherSibling.getRole(),genderConfigs,false,subReport);
						}
					}
				}
			}
			if (!subReport.isEmpty()){
				report.outputs().appendln("Transfer siblings to siblings of "+relation.getSelf());
				report.outputs().append(subReport);
				report.outputs().appendln();
			}
		}
	}
		

	
	
	private static void closeParentSiblingTriangles (RoleRelations result, Map<Role,Roles> genderConfigs, Report report) {
		
		report.outputs().appendln("Transfer parents to (half-orphan) siblings");
		report.outputs().appendln();

		for (RoleRelation relation : result.toSortedList()){

			StringList subReport = new StringList();

//			Role alter = relation.getSelfRole();

			RoleActors siblings = relation.getActorsByRoleNameWithUnspecific("SIBLING",result);
			RoleActors parents = relation.getActorsByRoleNameWithUnspecific("PARENT",result);

			for (RoleActor sibling : siblings){
								
				for (RoleActor parent : parents){
					
					Gender egoGender = getCommonEgoGender(sibling.getEgoGender(),parent.getEgoGender());
					
					if (egoGender!=null){
						
						RoleActor genderedSibling = sibling.withEgoGender(egoGender);
						RoleActor genderedParent = parent.withEgoGender(egoGender);
						
						if (isAdoptable(result,genderedSibling,genderedParent,genderConfigs)){
							
							putRoleDefinition(result,genderedSibling,genderedParent,genderConfigs,true,subReport);
//							RoleRelation siblingRelation = putRelation(result,sibling,genderConfigs);

//							MetaRole parentRole = new MetaRole(parent.getRole());
//							parentRole.setEgoGender(egoGender);
//							System.out.println("adoptable "+parent); // Reciprocable true or false?? 
//							putRoleDefinition(result,siblingRelation,parent.getIndividual(),parent.getRole(),genderConfigs,true,subReport);
						}
					}
				}
			}
			if (!subReport.isEmpty()){
				report.outputs().appendln("Transfer parents to siblings of "+relation.getSelf());
				report.outputs().append(subReport);
				report.outputs().appendln();
			}
		}
	}
	
	public static boolean isValidComposition(RoleRelations relations, RoleActor medius, RoleActor alter, RoleActor self, String alterLink, String mediusLink, String mediusAlterLink, Map<Role,Roles> genderConfigs){
		boolean result;
		
		result = false;
		
//		if (alterLink.equals("CHILD") && mediusLink.equals("SPOUSE") && mediusAlterLink.equals("CHILD")){
			result = isComposable(relations,medius,alter,self,genderConfigs);
//		}
		
		//
		return result;
	}
		
	
	private static void closeCoChildRelations (RoleRelations result, Map<Role,Roles> genderConfigs, Report report) {

		report.outputs().appendln("Create sibling relations between distinct full co-children");
		report.outputs().appendln();
		
		for (RoleRelation alterRelation : result.toSortedList()){

			StringList subReport = new StringList();

			Role alter = alterRelation.getSelfRole();

			RoleActors children = alterRelation.getActorsByRoleNameWithUnspecific("CHILD",result);
						
			for (RoleActor child : children){
				
				RoleRelation childRelation = putRelation(result,child,genderConfigs);
				
				for (RoleActor otherChild : children){
					
					Gender egoGender = getCommonEgoGender(child.getEgoGender(),otherChild.getEgoGender());
					if (egoGender!=null){
						if (child!=otherChild && haveSameAlters(result,child,otherChild,"PARENT",genderConfigs)){
							putRoleDefinition(result,childRelation,otherChild.getIndividual(), new MetaRole("SIBLING",egoGender,otherChild.getAlterGender(),null),genderConfigs,true,subReport);
						}
					}
				}
			}
			
			if (!subReport.isEmpty()){
				
				report.outputs().appendln("Create sibling relations between full co-children of "+alter);
				report.outputs().append(subReport);
				report.outputs().appendln();
			}
		}
	}
		
	private static void closeCoParentTriangles (RoleRelations result, Map<Role,Roles> genderConfigs, Report report) {

		report.outputs().appendln("Create spouse relation between cross sex full coparents");
		report.outputs().appendln();

		for (RoleRelation alterRelation : result.toSortedList()){
			
			StringList subReport = new StringList();

			RoleActors parents = alterRelation.getActorsByRoleNameWithUnspecific("PARENT",result);

			for (RoleActor parent : parents){
				
				for (RoleActor otherParent : parents){
					
					if (parent.getAlterGender()!=otherParent.getAlterGender()){
						
						Gender egoGender = getCommonEgoGender(parent.getEgoGender(),otherParent.getEgoGender());
						if (egoGender!=null){
							
							RoleActor firstParent = parent.withEgoGender(egoGender);
							RoleActor secondParent = otherParent.withEgoGender(egoGender);
							secondParent.getRole().setName("SPOUSE");
							
							if (haveSameAlters(result,firstParent,secondParent,"PARENT",genderConfigs)){
								putRoleDefinition(result,firstParent,secondParent,genderConfigs,true,subReport);
							}
						}
					}
				}
			}
			if (!subReport.isEmpty()){
				
				report.outputs().appendln("Create spouse relations between full co-parents of "+alterRelation.getSelf());
				report.outputs().append(subReport);
				report.outputs().appendln();
			}
		}
	}

	private static boolean haveSameAlters(RoleRelations roleRelations, RoleActor alpha, RoleActor beta, String alterRoleName, Map<Role,Roles> genderConfigs){
		boolean result;

		result = false;
		
		RoleRelation alphaRelation = roleRelations.getAdjustedSelfRelation(alpha,genderConfigs);
		RoleRelation betaRelation = roleRelations.getAdjustedSelfRelation(beta,genderConfigs);
		
		if (alphaRelation!=null && betaRelation!=null){
			
			Roles alphaAlters = alphaRelation.getActorsByRoleName(alterRoleName).getIndividuals();
			Roles betaAlters = betaRelation.getActorsByRoleName(alterRoleName).getIndividuals();
			
			if (alphaAlters.size()==betaAlters.size()){

				result = true;
				
				for (Role alphaAlter : alphaAlters){
					if (!betaAlters.contains(alphaAlter)){
						result = false;
						break;
					}
				}
			}
		}

		//
		return result;
	}

	
	private static Gender getUniqueEgoGender (Map<String,MetaRole> configs, Gender alterGender){
		Gender result;
		
		result = Gender.UNKNOWN;
		boolean cross = true;
		
		for (MetaRole value : configs.values()){
			if (!value.isCross()){
				cross = false;
			}
			if (result==null){
				if (value.getEgoGender().isUnknown() && cross){
					result = alterGender.invert();
				} else {
					result = value.getEgoGender();
				}
			} else if (!result.equals(value.getEgoGender())){
				if (!value.getEgoGender().isUnknown() || !cross){
					result = Gender.UNKNOWN;
					break;
				}
			} 
		}
		//
		return result;
	}
	
	
	private static Gender getUniqueAlterGender (Map<String,MetaRole> configs, String name, Gender egoGender){
		Gender result;
		
		result = Gender.UNKNOWN;
		boolean cross = true;
		
		if (name!=null && configs.get(name)!=null){
			
			result = configs.get(name).getAlterGender();
		
		} else {

			for (MetaRole value : configs.values()){
				if (!value.isCross()){
					cross = false;
				}
				if (result==null){
					if (value.getAlterGender().isUnknown() && cross){
						result = egoGender.invert();
					} else {
						result = value.getAlterGender();
					}
				} else if (!result.equals(value.getAlterGender())){
					if (!value.getAlterGender().isUnknown() || !cross){
						result = Gender.UNKNOWN;
						break;
					}
				} 
			}
		}
		//
		return result;
	}
	
	private static MetaRole getUniqueGenderConfig(String name, Map<String,MetaRole> alterGenders){
		MetaRole result;
		
		result = null;
		
		if (name!=null){
			
			result = alterGenders.get(name);
		}
		
		if (result == null){
			
			for (MetaRole value : alterGenders.values()){
				if (result==null){
					result = value;
				} else if (!result.equals(value)){
					result = null;
					break;
				} 
			}
		}
		//
		return result;
	}

	private static void addReciprocalRelations(RoleRelations result, Map<Role,Roles> genderConfigs, Report report){
		
		report.outputs().appendln();
		report.outputs().appendln("Add reciprocal roles");

		for (RoleRelation alterRelation : result.toSortedList()){
			
			RoleActor self = alterRelation.getSelf();
			
			for (RoleActor alter : alterRelation.actors().toSortedList()){
				
				putReciprocalRoleDefinition(result,self,alter,genderConfigs,report);
				
				MetaRole mediusRole = mediusActor.getRole();
				
				if (!mediusRole.getName().equals("SELF")){
					
					Gender egoGender = getEgoGender(alter,mediusActor,genderConfigs);
					Gender alterGender = null;
					
					if (alter.getName().equals("[self]")){
						
						alterGender = mediusRole.getEgoGender();
						
					} else {
						
						alterGender = getAlterGender(alter,mediusRole.invertName(),egoGender,genderConfigs);
					}
					//
					MetaRole alterRole = new MetaRole(mediusRole.invertName(),egoGender,alterGender,mediusRole.invertAge());
					putRoleDefinition(result,mediusActor,new RoleActor(alter, alterRole),genderConfigs,report);
				}
			}
		}
	}
	
	private static void putReciprocalRoleDefinition(RoleRelations result, RoleActor self, RoleActor alter, Map<Role,Roles> genderConfigs, StringList report){
		
		MetaRole alterRole = alter.getRole();

		Gender egoGender = getEgoGender(self.getIndividual(),alter,genderConfigs);
		Gender alterGender = null;
		
		if (self.getName().equals("[self]")){
			
			alterGender = alterRole.getEgoGender();
			
		} else {
			
			alterGender = getAlterGender(self.getIndividual(),alterRole.invertName(),egoGender,genderConfigs);
		}
		//
		MetaRole selfRole = new MetaRole(alterRole.invertName(),egoGender,alterGender,alterRole.invertAge());
		
		putRoleDefinition(result,alter,alter.getReciprocal(self.getIndividual(), genderConfigs),genderConfigs,false,report);
		putRoleDefinition(result,alter,new RoleActor(self.getIndividual(), selfRole),genderConfigs,false,report);

	}
	
	private static void removeReciprocalRoleDefinition(RoleRelations result, RoleActor self, RoleActor alter, Map<Role,Roles> genderConfigs, StringList report){
		
		MetaRole alterRole = alter.getRole();

		Gender egoGender = getEgoGender(self.getIndividual(),alter,genderConfigs);
		Gender alterGender = null;
		
		if (self.getName().equals("[self]")){
			
			alterGender = alterRole.getEgoGender();
			
		} else {
			
			alterGender = getAlterGender(self.getIndividual(),alterRole.invertName(),egoGender,genderConfigs);
		}
		//
		MetaRole selfRole = new MetaRole(alterRole.invertName(),egoGender,alterGender,alterRole.invertAge());
		
		removeRoleDefinition(result,alter,new RoleActor(self.getIndividual(), selfRole),genderConfigs,false,report);
		
		removeRoleDefinition(result,alter,alter.getReciprocal(self.getIndividual(), genderConfigs),genderConfigs,false,report);

	}
	
		
	public static RoleDefinitions createRoleDefinitions(final RelationModel model, final StringList roleDefinitionsList) {
		RoleDefinitions result;

		result = new RoleDefinitions();

		int id = 0;

		for (String roleDefinitionLine : roleDefinitionsList) {
			
			String[] items = roleDefinitionLine.split("\t");

			id++;
			Role role = null;
			Primary primary = null;
			Role inversion = null;
			Roles composition = new Roles();
			AlterAge alterAge = null;
			Gender alterGender = Gender.UNKNOWN;
			Gender egoGender = Gender.UNKNOWN;

			for (int idx = 0; idx < items.length; idx++) {

				String item = items[idx];
				if (StringUtils.isEmpty(item)) {
					continue;
				}

				switch (idx) {
					case 0:
						role = model.role(item);
					break;
					case 1:
						primary = Primary.valueOf(item);
					break;
					case 2:
						inversion = model.role(item);
					break;
					case 3:
						composition.add(model.role(item));
					break;
					case 4:
						composition.add(model.role(item));
					break;
					case 5:
						alterGender = Gender.valueOf(item);
					break;
					case 6:
						alterAge = AlterAge.valueOf(item);
					break;
					case 7:
						egoGender = Gender.valueOf(item);
					break;
				}
			}
			result.add(new RoleDefinition(id, role, primary, inversion, composition, alterGender, alterAge, egoGender));
		}

		result = result.clean();

		//
		model.setRoleDefinitions(result);
		return result;
	}
	
	private static boolean crossSex (Role ego, Role alter, Map<Role,List<Gender>> genders){
		boolean result;
		
		result = false;
				
		for (Gender egoGender : genders.get(ego)){
			for (Gender alterGender : genders.get(alter)){
				if (egoGender!=alterGender){
					result = true;
					break;
				}
			}
			if (result){
				break;
			}
		}
		//
		return result;
	}

	private static boolean sameSex (Role ego, Role alter, Map<Role,List<Gender>> genders){
		boolean result;
		
		result = false;
		
		for (Gender egoGender : genders.get(ego)){
			for (Gender alterGender : genders.get(alter)){
				if (egoGender==alterGender){
					result = true;
					break;
				}
			}
			if (result){
				break;
			}
		}
		//
		return result;
	}


	public static Graph<Role> relationModelGraph1(final RelationModel model) {
		Graph<Role> result;

		result = new Graph<Role>();
		result.setLabel(model.getName());

		Map<Role, LinkType> linkTypes = new HashMap<Role, LinkType>();
		Map<Role, Integer> weights = new HashMap<Role, Integer>();
		Map<Role, Roles> inversions = new HashMap<Role, Roles>();
		Map<Role, String> tags = new HashMap<Role, String>();
		
		RelationModelStatistics statistics = new RelationModelStatistics(model);
		Map<Role,List<Gender>> genderMap = statistics.genderMap();

		Role maleEgo = new Role("Male Ego");
		Role femaleEgo = new Role("Female Ego");
		
		Gender[] maleGenders = new Gender[]{Gender.MALE};
		Gender[] femaleGenders = new Gender[]{Gender.FEMALE};
		
		genderMap.put(maleEgo, Arrays.asList(maleGenders));
		genderMap.put(femaleEgo, Arrays.asList(femaleGenders));

		result.addNode(maleEgo);
		result.addNode(femaleEgo);

		for (Role role : model.roles()) {
			result.addNode(role);
		}

		// Set primary links
		
		for (RoleDefinition definition : model.roleDefinitions().toSortedList()) {
			if (definition.primary() != null) {
				
				Role alter = definition.role();
				Roles egos = new Roles();
				if (!definition.egoGender().isFemale()){
					egos.add(maleEgo);
				} 
				if (!definition.egoGender().isMale()){
					egos.add(femaleEgo);
				}
								
				switch (definition.primary()) {
					case PARENT:
						linkTypes.put(alter, LinkType.ARC);
						if (definition.alterGender().isMale()) {
							weights.put(alter, 1);
							tags.put(alter, "F");
						} else if (definition.alterGender().isFemale()) {
							weights.put(alter, -1);
							tags.put(alter, "M");
						} else {
							tags.put(alter, "Pa");
						}
					break;
					case SIBLING:
						linkTypes.put(alter, LinkType.EDGE);
						weights.put(alter, -1);
						if (definition.alterGender().isMale()) {
							tags.put(alter, "B");
						} else if (definition.alterGender().isFemale()) {
							tags.put(alter, "Z");
						} else {
							tags.put(alter, "Sb");
						}
					break;
					case SPOUSE:
						linkTypes.put(alter, LinkType.EDGE);
						weights.put(alter, 1);
						if (definition.alterGender().isMale()) {
							tags.put(alter, "H");
						} else if (definition.alterGender().isFemale()) {
							tags.put(alter, "W");
						} else {
							tags.put(alter, "Sp");
						}
					break;
				}
				
				for (Role ego : egos){
					if (definition.primary()==Primary.SPOUSE && !crossSex(ego,alter,genderMap)){
						continue;
					}
					result.addLink(ego, alter, linkTypes.get(alter), weights.get(alter));
				}
			}
		}
		
		// Set inverse primary links;
		
		for (RoleDefinition definition : model.roleDefinitions().toSortedList()) {
			if (definition.inversion() != null) {
				
				Role alter = definition.role();
				Roles egos = new Roles();
				if (!definition.egoGender().isFemale()){
					egos.add(maleEgo);
				} 
				if (!definition.egoGender().isMale()){
					egos.add(femaleEgo);
				}
				Role linkRole = definition.inversion();
				
				Roles inverseRoles = inversions.get(alter);
				if (inverseRoles == null){
					inverseRoles = new Roles();
					inversions.put(alter, inverseRoles);
				}
				inverseRoles.add(linkRole);

				for (Role ego : egos){
					if (genderMap.get(ego).size()==0 || sameSex(ego,linkRole,genderMap)){
						result.addLink(alter, ego, linkTypes.get(linkRole), weights.get(linkRole));
					}
				}
			}
		}

		// Set composite terms

		for (RoleDefinition definition : model.roleDefinitions().toSortedList()) {
			if (definition.composition() != null) {
				
				Role alter = definition.role();
				Role ego = definition.composition().get(0);
				Role linkRole = definition.composition().get(1);
				
				if (linkTypes.containsKey(linkRole)){
					if (linkTypes.get(linkRole)==LinkType.EDGE && weights.get(linkRole)==1 && !crossSex(ego,alter,genderMap)){
						continue;
					}
					result.addLink(ego, alter, linkTypes.get(linkRole), weights.get(linkRole));
				} else if (inversions.containsKey(linkRole)){
					Roles inverseLinkRoles = inversions.get(linkRole);
					for (Role inverseLinkRole : inverseLinkRoles){
						if (genderMap.get(ego).size()==0 || sameSex(ego,inverseLinkRole,genderMap)){
							result.addLink(alter, ego, linkTypes.get(inverseLinkRole), weights.get(inverseLinkRole));
						}
					}
				} else {
					System.err.println("Undefined link role "+ linkRole);
				}

			} else if (definition.primary()==null && definition.inversion()==null){
				
				System.err.println("No composition : "+definition);
			}

					for (Gender egoGender : model.roleDefinitions().getAlterGenders(definition.composition().get(0))) {
						for (Gender alterGender : model.roleDefinitions().getAlterGenders(definition.composition().get(1))) {
							if (definition.alterGender() != null && definition.alterGender().equals(alterGender)) {
								Role inverseRole = model.roleDefinitions().getInverseRole(definition.composition().get(1), egoGender, alterGender);
								System.out.println(role+" "+inverseRole);
								
								arcType = arcTypes.get(inverseRole);
								if (arcType.equals("FATHER")) {
									result.addArc(alterNode, egoNode, 1);
								} else if (arcType.equals("MOTHER")) {
									result.addArc(alterNode, egoNode, -1);
								}
							}
						}
					}
				}

				// System.out.println(definition+" "+alterNode+" "+egoNode+" "+arcType);

		
		}

		//
		return result;
	}*/


}
