package org.tip.puck.visualization.layouts.simpa;

import javax.swing.Icon;
import javax.swing.JPanel;
import org.gephi.layout.plugin.force.StepDisplacement;
import org.gephi.layout.spi.Layout;
import org.gephi.layout.spi.LayoutBuilder;
import org.gephi.layout.spi.LayoutUI;
import org.openide.util.NbBundle;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Edoardo Savoia
 */

@ServiceProvider(service = LayoutBuilder.class)
public class SimPaLayoutBuilderUpdated implements LayoutBuilder {

    SimPaLayoutUI ui = new SimPaLayoutUI();

    @Override
    public String getName() {
        return NbBundle.getMessage(SimPaLayoutBuilderUpdated.class, "SimPaLayoutBuilder.name");
    }

    @Override
    public LayoutUI getUI() {
        return ui;
    }

    @Override
    public SimPaLayoutUpdated buildLayout() {
        SimPaLayoutUpdated layout = new SimPaLayoutUpdated(this, new StepDisplacement(1f));
        return layout;
    }

    private class SimPaLayoutUI implements LayoutUI {

        @Override
        public String getDescription() {
            return NbBundle.getMessage(SimPaLayoutBuilderUpdated.class, "SimPaLayoutBuilder.description");
        }

        @Override
        public Icon getIcon() {
            return null;
        }

        @Override
        public JPanel getSimplePanel(Layout layout) {
            return null;
        }

        @Override
        public int getQualityRank() {
            return 3;
        }

        @Override
        public int getSpeedRank() {
            return 4;
        }

    }
}
