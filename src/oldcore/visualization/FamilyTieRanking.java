package oldcore.visualization;

import oldcore.visualization.PlotOptions.HeritageType;

import org.gephi.graph.api.Edge;
import org.gephi.graph.api.EdgeData;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.Node;
import org.gephi.ranking.api.EdgeRanking;
import org.gephi.ranking.impl.AbstractRanking;

public class FamilyTieRanking extends AbstractRanking<Edge, Integer> implements EdgeRanking<Integer>  {
	
	private HeritageType ht;

	public FamilyTieRanking(Graph graph, HeritageType ht){
		this.graph = graph;
		this.ht = ht;
	}
	
	@Override
	public String getName() {
		return "Context dependent family tie ranking";
	}

	@Override
	public Class getType() {
		return Integer.class;
	}

	@Override
	public float normalize(Integer value) {
		return (float) ((value - minimum) / (float) (maximum - minimum));
	}

	@Override
	public Integer unNormalize(float normalizedValue) {
		return (int) (normalizedValue * (maximum - minimum)) + minimum;
	}

	@Override
	public Integer getValue(Edge e) {
		EdgeData ed = e.getEdgeData();
		String tt = (String) ed.getAttributes().getValue("tie type");
		if(tt.equals("conjugal"))return new Integer(1);
		else {
			Node src = e.getSource();
			String srcGender = (String) src.getNodeData().getAttributes().getValue("gender");
			Node dst = e.getTarget();
			String dstGender = (String) dst.getNodeData().getAttributes().getValue("gender");
			if(ht == HeritageType.agnatic){
				if(srcGender.equals("male") && dstGender.equalsIgnoreCase("male")) return new Integer(2);
				else return new Integer(3);
			} else if(ht == HeritageType.uterine){
				if(srcGender.equals("female") && dstGender.equalsIgnoreCase("female")) return new Integer(2);
				else return new Integer(3);
			} else return new Integer(2);
		}
	}
}
