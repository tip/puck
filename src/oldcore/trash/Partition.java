package oldcore.trash;

import static oldcore.trash.Mat.percent;
import static oldcore.trash.Trafo.asInt;
import static oldcore.trash.Trafo.asString;
import static oldcore.trash.Trafo.isNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


import org.tip.puck.census.chains.Chain;
import org.tip.puck.census.chains.ChainFinder;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.util.StringUtils;
import org.tip.puck.util.StringUtils.isNull;




/**
 * This class is a standard implementation of the Partition interface
 * @author Klaus Hamberger
 * @since Version 0.6
 * @param <E> the type of nodes that compose the clusters of the partition
 */
@SuppressWarnings("serial")
public class Partition<E extends Clusterable> extends ArrayList<Cluster<E>> {

	/**
	 * renumbers the elements continuously
	 * @since 10-08-10
	 */
/*	public void renumber (Collection<E> nodes){
		int i=1;
		for (E v: nodes){
//			v.setCurrent(i);
			i++;
		}
	}*/
	
	
	/**
	 * the partition label
	 */
	protected String attributeLabel;

	
	String netLabel;


	/**
	 * the map of cluster values for vertices
	 * @since 10-08-15
	 */
	private Map<E,Object> values = new TreeMap<E,Object>();
	
/*	final class ClusterComparator implements Comparator<Object>{
		
		public int compare(Object o1, Object o2){
			if (o1==null && o2==null) {
				return 0;
			} else if (o2==null) {
				return 1;
			} else if (o1==null) {
				return -1;
			} else {
				if (o1 instanceof Cluster){
					return compare(((Cluster)o1).getVal(),((Cluster)o2).getVal());
				} else if (o1 instanceof int[]) {
					return new VectorComparator().compare((int[])o1,(int[])o2,true);
				} else {
					return ((Comparable)o1).compareTo((Comparable)o2);
				}
			}
		}


		
		
	}
	
	public void sort(){
		Collections.sort(this, new ClusterComparator());
	}*/
	
	
	private Map<Object, Cluster<E>> index = new TreeMap<Object, Cluster<E>>(new ClusterComparator());

	
	
	/**
	 * the cluster value or interval used for binarization
	 */
    private String bin;
	
    /**
     * the upper and lower bounds for periodization
     */
    private int[] bounds = {-1,-1};
    /**
     * a cumulation index (+1 ascending, -1 descending, 0 no cumulation)
     */
    private int cumul;
    /**
     * the degree of graduable partition commands
     */
	private int deg;
	/**
	 * the number of length of periods
	 */
    private int per;
    /**
     * the starting point of periodization
     */
    private String start;
	/**
	 * true if fixed period length is used for periodization
	 */
	private boolean fix = false;
    
	
/*    public Partition (){
		
	}*/
	
	/**
     * constructs a Partition from a vertex collection and an alternative partition command
     * @param c the Vertex cluster
     * @param cmd the partition command
     * @see partitions.OldNodePartition#partitionSystem(String)
     * @since modified 10-05-12, 10-08-10
     */
/*	public Partition (Collection<E> c, String cmd){ 
		initialize(cmd);
//		renumber(c);
		fill(c);
		if (c instanceof Cluster) attributeLabel = asString(((Cluster<E>)c).getVal());
    }*/
	
	
	public Partition (Map<E,Object> values){
		this.values = values;
		// make and fill clusters...
	}
	
	
	public boolean containsKey(Object key){
		return index.containsKey(key);
	}
	
	
	
	
	
	/**
	 * constructs a Partition from a network according to a partition command
	 * @param NodeMap the network to be partitioned
	 * @param cmd the partition command
	 * @see maps.AbstractNet#getPartition(String)
	 * @see maps.Net#bias(ArrayList, int)
	 */
/*	public Partition (Net net, String cmd){
//	    label = net.getLabel() + " ("+cmd+getSubtitle()+")"; 
//	    attributeLabel = net.getLabel() + " ("+cmd+")"; 
		netLabel = net.getLabel();
		initialize(cmd);
		Collection<E> temp = new ArrayList<E>();
		for (Clusterable v : net.individuals()){
			temp.add((E)v);
		}
	    fill(temp);
	    Collections.sort(this);
	}	*/
	

	
	/**
	 * constructs a NodePartition from another Partition
	 * @param p the original Partition
	 * @see partitions.OldNodePartition#regroup()
	 */
/*	private Partition (Partition<E> p){
		initialize(p.getAttributeLabel());
//		setNet(p.getNet());
		setValues(p.getValues());
		attributeLabel = p.getAttributeLabel(); 
	}*/	

	/**
	 * checks whether a parition necessitates recursive methods (inspection of the entire network)
	 * @return true if inspection of the entire network is necessary
	 * @see OldNodePartition.NodePartition#get(Individual)
	 * @see partition.VertexPartition#fill(Collection<Vertex>)
	 */
/*	private boolean callsNet (){
	    final String[] structurelabels = {"PATRIC","MATRIC","PATRID","MATRID","DEPTH","ORD"};
		String t = getAttributeLabel().split("\\s")[0];
		for (String s : structurelabels){
			if (s.equals(t)) return true;
		}
		return false;
	}*/
	
	public String getLabel (){
		return netLabel+" "+attributeLabel;
	}
	
	/**
	 * reads and modifies partition commands with number parts 
	 * @param cmd the partition command
	 * @return  the modified partition command
	 * @see partitions.OldNodePartition#initialize(String)
	 */
	private String checkIndexedLabel (String cmd){
		String[] indexedLabels = {"PEDG", "PROG", "SIBL"};
		for (String lb: indexedLabels) {
			if (cmd.indexOf(lb)==-1) continue;
			cmd = cmd.substring(cmd.indexOf(" ")+1);
			deg = Integer.parseInt(cmd.split(" ")[0]);
			try {
				return lb + cmd.substring(cmd.indexOf(" "));
			} catch (IndexOutOfBoundsException iob) {
				return lb;
			}
		}
		return cmd;
	}
	
	
	/**
	 * counts the membersByRelationId of all clusters of the partition
	 * @return the aggregate size of the clusters of the partition
	 * @see partitions.OldNodePartition#getDataset(String)
	 * @see partitions.OldNodePartition#report(List)
	 */
/*	int countAll(){
		int count = 0;
		for (Cluster<E> clu : this){
			if (!clu.isNullCluster()) count = count+clu.count();
		}
		return count;
	}*/

	/**
	 * fills the partition (distributes vertices from a list among appropriate clusters)
	 * @param list the vertex list
	 * @see partitions.OldNodePartition#VertexPartition(Cluster, String)
	 * @see partitions.OldNodePartition#VertexPartition(NodeMap, String)
	 */
	private void fill (Collection<E> list) {
//		if (getLabel().equals("GEN")) originNet.setGenerations();
//		if (callsNet()) originNet.getClusters(getLabel());
		for (E v : list){
			put(v); 
		}
//		Collections.sort(this);
	}
	
	/**
	 * gets the heading of the cluster to which a given element belongs 
	 * @param e the element
	 * @return the heading of the cluster to which the element belongs
	 * @see partitions.AbstractPartition#put(E)
	 * @since last modified 12-02-03
	 */
/*	public Object get (E v){
//		if (callsNet()) return v.getCluster();
//	    return v.getCluster(getLabel(),bin,deg);
		return v.getAttributeValue(getAttributeLabel());
	}*/
	
	/**
	 * gets the cluster with given value
	 * @param v the heading of the cluster
	 * @return the cluster
	 * @see partitions.AbstractPartition#put(E);
	 * @see partitions.AbstractPartition#put(E,Object);
	 * @see partitions.Partition#putAll(Cluster, Object)
	 */
/*	public Cluster<E> getCluster (Object v){
		if (v==null) return null;

		return index.get(v);
		for (Cluster clu : this){
			if (clu.hasHeading(v)) return clu;
		}
		return null;
	}*/
	
	/**
	 * returns the cluster of an element
	 * @param item
	 * @return
	 */
	public Cluster<E> getCluster(E item){
		return index.get(values.get(item));
	}
	
	public int getClusterIndex (E item){
		return indexOf(getCluster(item));
	}
	
	public Cluster<E> getClusterWithValue (Object value){
		return index.get(value);
	}
	
/*	public int getValueFrequency (Object value){
		int result;
		Cluster<E> cluster = index.get(value);
		if (cluster==null){
			result = 0;
		} else {
			result = cluster.size();
		}
		
		//
		return result;
		
	}*/
	
    
    /**
	 * gets the distribution of cluster sizes
	 * @return the distribution of cluster sizes
	 * @since 10/04/13
	 */
/*	public Map<Integer,Integer> getClusterSizes(){
		Map<Integer,Integer> map = new TreeMap<Integer,Integer>();
		for (Cluster<E> clu: this){
			if (StringUtils.isNull(clu.getVal()) || clu.getVal().equals(0)) continue;
			int i = clu.size();
			if (map.get(i)==null) map.put(i, 1);
			else map.put(i, map.get(i)+1);
		}
		return map;
	}*/
	
	/**
	 * gets the index of the cluster to which a vertex belongs
	 * @param item the vertex
	 * @return the index of the vertice's cluster
	 * @see io.write.AbstractWriter#writePartition(Partition)
	 * @see OldGroupNet.GroupNet#get(Individual,Partition)
	 */
/*    public int getIndex(E item) {
		Object value = values.get(item);
		for (int i=0;i<size();i++){
			if (get(i).hasValueEqualTo(value)) return i;
		}
		return 0;
	}*/	

	/**
	 * gets the label of the partition
	 * @return the partition label
	 */
    public String getAttributeLabel() {
    	return attributeLabel;
    }

    
	/**
	 * gets the cluster value of a given vertex
	 * @param item the vertex to be checked
	 * @return the cluster value of the vertex
	 * @see partitions.OldNodePartition#getIndex(Individual)
	 * @see partitions.OldNodePartition#getSubNet(Individual)
	 * @see maps.Net#bias(ArrayList, int)
	 */
    public Object getVal(E item){
		return values.get(item);
	}
	
	/**
	 * constructs a NodePartition from another Partition
	 * @param p the original Partition
	 * @see partitions.NodePartition#regroup()
	 */
/*	private Partition (Partition p){
		initialize(p.getLabel());
		setNet(p.getNet());
		setValues(p.getValues());
		label = p.getLabel(); 
	}*/
	
	/**
	 * gets the cluster value of a given vertex
	 * @param v the vertex to be checked
	 * @param withNull true if the null cluster shall be considered (with value 0)
	 * @return the cluster value of the vertex
	 * @see io.write.AbstractWriter#writePartition(Partition)
	 * @see partitions.OldNodePartition#getVal(Individual)
	 * @since modif 10-08-15
     */
	public Object getVal(E v, boolean withNull){
		Object val = getVal(v);
		if (withNull && val==null) val = 0;
		return val;
	}
	
	/**
	 * gets the value list of the partition
	 * @return the value list
	 * @see partitions.NodePartition#VertexPartition(Partition<Vertex>)
	 * @since modif 10-08-16
	 */
	public Map<E,Object> getValues() {
		return values;
	}
	
	/**
	 * initializes the partition (sets the label and creates the first cluster and the empty value list)
	 * @param cmd the partition command
	 * @see partitions.NodePartition#initialize(String);
	 * @since modif 10-08-16
	 */
/*	void initialize(String cmd){
		
		

		add(new Cluster<E>());
		attributeLabel = cmd;
		String[] b = cmd.split("=");
		cmd = b[0];
		if (b.length>1) bin = b[1];
		if (cmd.charAt(0)=='+') {
			cumul=1;
			cmd = cmd.substring(1).trim();
		} else if (cmd.charAt(0)=='-') {
			cumul=-1;
			cmd = cmd.substring(1).trim();
		}
		cmd = checkIndexedLabel(cmd.trim());
		String[] s = cmd.split(" ");
		int n = s.length;
		if ((n==1) || (s[0].equals("SIBL"))) {
			attributeLabel = cmd;
			return;
		}
		int i = 1;
		start = "";
		if (bin!=null) return; //str.indexOf('=')>-1
		while (i<n) {
			try {
	            if (s[i].charAt(0)=='*') {
	               fix = true;
	               s[i] = s[i].substring(1);
	            }
	            per = Integer.parseInt(s[i]);
	            for (int j=i+1;j<n;j++) {
	               start = start+s[j];
	            }               
	            cmd = "";
	            for (int j=0;j<i;j++) {
	               cmd = cmd+s[j]+" ";
	            }
	            attributeLabel = cmd.trim();
	            start = start.trim();
	            return;
	         } catch (NumberFormatException nfe) {
	        	if (i==n-1) attributeLabel = cmd.trim();
	            i++;
	         }
	      }
	}*/
	
	   /**
	 * gets the maximal Cluster
	 * @return the maximal Cluster
	 * @see RingGroupMap#decompose()
	 */
/*		Cluster<E> maxCluster () {
			Cluster<E> m = new Cluster<E>();
			for (Cluster<E> c : this) {
				if (c.size()>m.size()) m = c;
			}
			return m;
		}	*/
	
	/**
	 * 	
	 * @deprecated
	 */
	public int nrItems(){
		return values.size();
	}
	
	/**
	 * checks whether the partition has numeric cluster values
	 * @return true if the partition has numeric cluster values
	 * @see partitions.OldNodePartition#getExportMode()
	 * @see partitions.OldNodePartition#regroup()
	 * @since modif 10-08-15
	 */
/*	private boolean numeric (){
		try {
			for (Object obj : getValues().values()){
				if (isNull(obj)) continue;
				int i = asInt(obj);
				if (bounds[0]<0 || i<bounds[0]) bounds[0] = i;
				if (i>bounds[1]) bounds[1] = i;
			}
//			numeric=true;
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}*/
	
    /**
	 * puts a node into the appropriate cluster
	 * @param E the node
	 * @see partitions.OldNodePartition#put(Individual)
	 */
	void put (E e){
		Object value = e.getAttributeValue(attributeLabel);
		put (value, e);
	}

    
	/**
	 * puts a node into a cluster with given value 
	 * @param item the cluster element
	 * @param value the cluster value
	 */
	public void put(Object value, E item){

		values.put(item, value);
		if (value==null) {
			get(0).add(item);
			return;
		}
		
		Cluster<E> cluster = index.get(value);
		if (cluster==null) {
			cluster = new Cluster<E>(value,item);
			add(cluster);
			index.put(value, cluster);
		}
		if (!cluster.contains(item)) cluster.add(item);
	}    
	
/*	public void putCluster (Object value){
		Cluster<E> cluster = new Cluster<E>(value);
		add(cluster);
		index.put(value, cluster);
	}*/
    
	/**
	 * puts all nodes from a cluster into a cluster with given heading
	 * @param c the initial cluster
	 * @param v the heading of the new cluster
	 * @see partitions.OldNodePartition#regroup()
	 */
/*	public void putAll(Cluster<E> c, Object v){
		if (c.getVal()==null) get(0).addAll(c);
		else {
			try {
				index.get(v).addAll(c);
			} catch (NullPointerException npe) {
				add(new Cluster<E>(v,c));
			}
		}
	}    */
	


    /**
     * relationalizes a partition by defining an equivalence relation between any two vertices of the same cluster
     * @see maps.Net#relationalize()
     */
/*    public void relationalize () {
    	for (Cluster<E> cluster : this){
    		if (cluster.isNullCluster()) continue;
        	for (int i=0;i<cluster.size();i++){
        		Individual ego = (Individual)cluster.get(i);
        		for (int j=0;j<i;j++){
        			Individual alter = (Individual)cluster.get(j);
   					ego.addRelation("@"+attributeLabel,alter);
   					alter.addRelation("@"+attributeLabel,ego);
//   					ego.setAttribute(new Attribute("@"+attributeLabel,((Individual)alter).getId()+""));
//   					alter.setAttribute(new Attribute("@"+attributeLabel,((Individual)ego).getId()+""));
    			}
			}
		}
	}*/
    
	/**
     * periodizes the partition by regrouping clusters into larger clusters 
     * @return the periodized partition
     * @see maps.AbstractNet#getPartition(String)
     */
/*    public Partition<E> regroup (){
	    if (per==0 || !numeric()) return this;
		Partition<E> part = new Partition<E>(this);
		part.cumul = cumul;
		int span = 0;
		if (start.length()>0) span = bounds[1]-asInt(start);
		else {
			span = bounds[1]-bounds[0];
			start = bounds[0]+"";
		}
		if (!fix) per = Math.round(new Float(span/per+0.5));
		int i=0;
		int t=-1;
		Object current = bounds[0]+" - "+(asInt(start)-1);
		for (Cluster<E> clu : this) {
			int k = asInt(clu.getVal())-asInt(start);
			if (t>-1 && t<k) t=k;
			if ((t==-1 && k>=0) || (t>-1 && k>=t)){
				if (t==-1) t=0;
				int s = asInt(start)+t;
				if (t==-1) 	t=0; //else {
				t=t+per;
				if (i<size()-1) current = s+" - "+(asInt(start)+t-1);
				else current = s+" - "+bounds[1];
			}
			part.putAll(clu, current);
			replaceValues(clu.getVal(),current);
			if (t==-1) i++;
		}
		return part;
	}*/   
	
	/**
	 * replaces a cluster value by another
	 * @param a the first cluster value
	 * @param b the second cluster value
	 * @see partitions.OldNodePartition#regroup()
	 * @since modif 10-08-16
	 */
/*	private void replaceValues(Object a, Object b){
/*		for (int i=0;i<getValues().size();i++){
			try	{
				if (getValues().get(i).equals(a)) getValues().set(i,b);
			} catch (NullPointerException npe) {}
		}*/
/*		for (E v : getValues().keySet()){
			try	{
				if (getValues().get(v).equals(a)) getValues().put(v,b);
			} catch (NullPointerException npe) {}
		}
	}*/
	
	/**
	 * produces the list of all clusters of the partition and their sizes
	 * @param list the string list to be used
	 * @see gui.ShowMask#show(String{])
	 */
/*	public void report (List<String> statistics) {
        statistics.add(attributeLabel);statistics.add("\n");
        int i=0;
        int cum=0;
        int tot=0;
        if (cumul==-1) tot=countAll();
        for (Cluster<E> clu : this) {
           int count = clu.count()+cum;
           int cnt = count;
           if (cumul==-1 && !clu.isNullCluster()) cnt=tot-cum;
           statistics.add(i+"\t"+asString(clu.getVal())+"\t"+cnt);
           i++;
           if (cumul!=0 && !clu.isNullCluster()) cum=count;
        }
        statistics.add("\n");
     }*/



	/**
	 * sets the map of cluster values
	 * @param values the map of cluster values
	 * @since modif 10-08-15
	 */
	public void setValues(Map<E,Object> values) {
		this.values = values;
	}
	
    //check! 
	/**
	 * gets the share of a cluster (the percentage of nodes contained in the cluster)
	 * @param c the cluster
	 * @param k a multiplier
	 */
/*    public int share (Cluster<E> c, int k){                  
    	return k*c.count()/index.size();
    }*/
    
    /**
     * gets the mean share of a cluster, the share of the maximal cluster, and the marriage share of the maximal cluster
     * @return an array containing the three types of shares
     * @see maps.Net#getIdentityCard()
     */
    public double[] shares () {
		double[] shares = new double[3];
		int r = 0;
		Cluster<E> a = get(0);
		for (Cluster<E> c : this){
			int i = c.size();
			if (i>1) r++;
			if (i>a.size()) a=c;
		}
		int s = 0;
		for (E e: a){
			Individual v = (Individual)e;
			if (v.getGender().isMale()) {
				s = s+v.spouses().size();	
				continue;
			}
			if (v.isSingle()) continue;
			for (Individual w : v.spouses()){
			   if (!a.contains(w)) s++;
			}
		}
		
		int nrMarriages = 1; //
		
		shares[0] = percent(1,r);      // mean cluster share
		shares[1] = percent(a.size(), getValues().size());  // maximal cluster share
		shares[2] = percent(s,nrMarriages);  //marriage share of the maximal cluster
		return shares;
	}
    
	/**
	 * produces a partition of partitions (partitioning each cluster according to a new partition command)
	 * @param cmd the second (transversal) partition command
	 * @return the list of partitions
	 * @see partitions.OldNodePartition#getDataset(String)
	 * @since last modified 10-11-21 KH
	 */
/*    private List<Partition<E>> partitionSystem (String cmd){
		List<Partition<E>> parts = new ArrayList<Partition<E>>();
		for (Cluster<E> clu : this){
			parts.add(new Partition<E>(clu,cmd));
		}
		return parts;
	}*/   
    
	   /**
	    * splits a partition command into a label part and a degree part and returns the latter
	    * @param cmd the partition command
	    * @return the degree part of the partition command
	    */
		private int splitLabel (String[] cmd){
			String[] indexedLabels = {"PEDG", "PROG", "SIBL"};
			int deg = 0;
			for (String lb: indexedLabels) {
				if (cmd[0].indexOf(lb)==-1) continue;
				cmd[0] = cmd[0].substring(cmd[0].indexOf(" ")+1);
				deg = Integer.parseInt(cmd[0].split(" ")[0]);
				try {
					cmd[0] = lb + cmd[0].substring(cmd[0].indexOf(" ")).trim();
				} catch (IndexOutOfBoundsException iob) {
					cmd[0] = lb;
				}
			}
			return deg;
		}
		
	
	private static int length(List<String> s){
		int i=0;
		for (String t : s){
			i = i+t.length();
		}
		return i;
	}
	
	//should return the translation in the chosen language
	private String t (String str){
		return str;
	}
	

	private String hf = "\tHH\tFF\tHF\t"+t("density")+"\t";

	
    /**
     * a shortcut method that checks whether a category of the relational statistics has a reduced number of columns
     * @param k a first category index
     * @param i a second category index
     * @return true if the column number is reduced
     * @see partitions.OldNodePartition#relationStatistics()
     */
    private static boolean rcn(int k, int i){ //reduced column number
    	if (i==1) return (k ==10 || k==11);  //no gender-differentiation
    	if (i==2) return (k%2!=0 && (k<9 || k>12)); //no distance
    	return ((k>6 && k<12) || k==20); //no distance
    }
	
    /**
     * produces the intra-cluster relation statistics (the aggregate profiles of kinship relations between the vertices of each cluster)
     * @see gui.screens.StatsScreen#show(String[])
     */
/*	public ArrayList<String> relationStatistics (){
    	ArrayList<String> list = new ArrayList<String>();
    	final String[] t = 			{t("Individuals Partitions"),t("Relation Partitions"),t("Statistics"),t("Relation Statistics"),
				t("Cluster")+"\t"+t("size")+"\t"+t("men")+"\t"+t("women")+"\t"+t("relations")+"\t"+t("agnatic")+hf+t("distance")+"\t"+t("uterine")+hf+t("distance")+"\t"+t("consang")+hf+t("distance")+"\t"+t("cognatic")+hf+t("distance")+"\t"+t("bilateral")+hf+t("parent-child")+hf+t("marriage")+"\t"+t("density")+"\t"+t("co-marriage")+"\t"+t("density")+"\t"+t("affinal")+"-"+t("agnatic")+hf+t("distance")+"\t"+t("affinal")+"-"+t("uterine")+hf+t("distance")+"\t"+t("affinal")+"-"+t("consang")+hf+t("distance")+"\t"+t("affinal")+"-"+t("cognatic")+hf+t("distance")+"\t"+t("affinal")+"-"+t("bilateral")+hf,//+"\t"+t("density")+" (ag.)\t"+t("mean distance")+" (ag.)\t"+t("density")+" (ut.)\t"+t("mean distance")+" (ut.)\t"+t("density")+" (cog.)\t"+t("mean distance")+" (cog.)\t"+t("density")+t(" (bil.)\t"+t("density")+" ("+t("fil")+".)\t"+t("density")+" ("+t("marr")+".)\t"+t("density")+" ("+t("comarr")+".)"),
				t("mean"),t("Partitions"),t("Diagrams"),t("total"),t("Affiliation Statistics"),
				t("Cluster")+"\t"+t("size")+"\t"+t("men")+"\t"+t("women")+"\tEgo\tm\tw\tF\tm\tw\tM\tm\tw\tFF\tm\tw\tFM\tm\tw\tMF\tm\tw\tMM\tm\tw\tFFF\tm\tw\tMMM\tm\tw\tH/W\tm\tw\tFW\tm\tw\tMH\tm\tw\tFFW\tm\tw\tMMH\tm\tw\tSpF\tm\tw\tSpM\tm\tw",
				t("Relinking statistics")};
    	String s = "";
    	list.add(t[4]);
    	int z = 21; //nr of columns
    	double[] sum1 = new double[z];
    	int[] sum2 = new int[z];
    	int tot = 0;
    	int totm = 0;
    	int[] totrel = new int[4];
    	int[][] totr = new int[z][4];
    	for (Cluster clu : this){
    		if (clu.isNullCluster()) continue;
    		int n = clu.size();
    		int m = clu.size(Gender.MALE);
    		tot = tot+n;
    		totm = totm+m;
    		int[] rel = {Mat.rel(n),Mat.rel(m),Mat.rel(n-m),n*m};
    		for (int i=0;i<4;i++){
        		totrel[i] = totrel[i]+rel[i];
    		}
    		int[][] cd = new int[z][4];
    		if (clu.isNullCluster()) continue; //double??
    		for (Object e : clu){
    			Individual ego = (Individual)e; 
    			int[][] d = ChainFinder.relationStatistics(ego, clu);
    			int i = ego.getGender().toInt()+1;
    			for (int k = 0;k<z;k++){
    				int h = 0;
    				if (k>11) h=1;
    				if (rcn(k,1)) continue;
					cd[k][i] = cd[k][i] + d[k-h][0];
					cd[k][3] = cd[k][3] + d[k-h][1];
					cd[k][0] = cd[k][1]+cd[k][2]+cd[k][3];
    			}
    			cd[10][0] = cd[10][0]+d[10][1];
				cd[11][0] = cd[11][0]+d[10][0];
    		}
    		s = asString(clu.getVal())+"\t"+n+"\t"+m+"\t"+(n-m)+"\t"+rel[0]+"\t";
			for (int k = 0;k<z;k++){
				if (rcn(k,2)) continue;
				for (int i=0;i<4;i++){
					if (rcn(k,1) && i>0) continue; 
					s = s+cd[k][i]+"\t";
					totr[k][i]=totr[k][i]+cd[k][i];
				}
				double i = percent(cd[k][0],rel[0]); 
				s = s+i+"\t";
				sum1[k] = sum1[k]+i;
				sum2[k]++;
				if (rcn(k,3)) continue;
				i = percent(cd[k+1][0],cd[k][0]*100); 
				s = s+i+"\t";
				sum1[k+1] = sum1[k+1]+i;
				if (cd[k+1][0]>0) sum2[k+1]++;
			}*/
/*			for (int k = 0;k<10;k++){
				if (k%2==0 || k>=7) {
					double i = percent(cd[k][0],rel[0]); 
					s = s+i+"\t";
					sum1[k] = sum1[k]+i;
					sum2[k]++;
				} else {
					double i = percent(cd[k][0],cd[k-1][0]*100); 
					s = s+i+"\t";
					sum1[k] = sum1[k]+i;
					if (cd[k][0]>0) sum2[k]++;
				}
			}*/
/*			list.add(s);
    	}*/
/*    	s = t[5]+"\t"+percent(tot,size()*100)+"\t"+percent(totm,size()*100)+"\t"+percent(tot-totm,size()*100)+"\t"+"\t";
    	String s1 = t[8]+"\t"+tot+"\t"+totm+"\t"+(tot-totm)+"\t"+totrel[0]+"\t";
    	for (int k=0;k<z;k++){
			if (rcn(k,2)) continue;
			for (int i=0;i<4;i++){
				if (rcn(k,1) && i>0) continue; 
				int tr = totrel[i];
//				if (k==8) tr = totrel[3];  //mariage 
//				if (k==9) tr = totrel[2];  //comariage
    			s = s+percent(totr[k][i],tr)+"\t";
    			s1 = s1+totr[k][i]+"\t";
    		}
			s = s+percent(sum1[k],sum2[k]*100)+"\t";
			s1 = s1+"\t";
			if (rcn(k,3)) continue;
			s = s+percent(sum1[k+1],sum2[k+1]*100)+"\t";
			s1 = s1+"\t";
    	}
		for (int k = 0;k<10;k++){
			s = s+percent(sum1[k],sum2[k]*100)+"\t";
		}
		list.add(1,s);
		list.add(1,s1);*/
//		list.add(totrel[0]+"");
/*    	return list;
    }*/
	

	
	
/*	class ChainComparator implements Comparator<List<String>> {
		public int compare(List<String> s1, List<String> s2){
			if (s1.size()!=s2.size()) return new Integer(s1.size()).compareTo(s2.size());
			int t1 = length(s1); 
			int t2 = length(s2);
			if (t1!=t2) return new Integer(t1).compareTo(t2);
			for (int i=0;i<s1.size();i++){
				if (s1.get(i).length()!=s2.get(i).length()) return new Integer(s2.get(i).length()).compareTo(s1.get(i).length());
			}

			for (int i=0;i<s1.size();i++){
				int k1 = Chain.number(s1.get(i));
				int k2 = Chain.number(s2.get(i));
				if (k1!=k2) return new Integer(k1).compareTo(k2);
			}
			return 0;
		}
	}*/	

	
}

