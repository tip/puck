package org.tip.puck.io.deluz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.PuckManager;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.Role;
import org.tip.puck.net.workers.NetUtils;
import org.tip.puck.report.Report;

import cern.colt.Arrays;
import fr.devinsy.util.StringList;

public class DELUZFile {
	
	
	private static String getLabel (char letter){
		String result;
		
		switch (letter){
		case 'a':
			result = "TRIBE";
			break;
		case 'b':
			result = "VILLAGE";
			break;
		case 'c':
			result = "QUARTER";
			break;
		case 'd':
			result = "SEGMENT";
			break;
		case 'e':
			result = "GENEALOGY";
			break;
		case 'f':
			result = "PROHIBITIONS";
			break;
		case 'g':
			result = "NAMES";
			break;
		case 'h':
			result = "MOTTO";
			break;
		case 'i':
			result = "CULTS";
			break;
		case 'k':
			result = "NOTE";
			break;
		default:
			result = null;
		}
		
		//
		return result;
		
	}
	

	
	/**
	 * 
	 * @param result
	 * @param in
	 * @throws PuckException
	 */
	private static void read(final Relation relation, final BufferedReader in) throws PuckException {

		Integer id = null;
		String value = null;
		char labelLetter = 'x';

		try {
			boolean ended = false;
			while (!ended) {
				//
				String line = in.readLine();

				if (line == null) {
					ended = true;

				} else if (StringUtils.isNotBlank(line)) {
										
					if (id==null){
						id = Integer.parseInt(line.trim());
						relation.setId(id);
						relation.setTypedId(id);

					} else if (line.length()>1 && line.charAt(1)==')'){
						String label = getLabel(labelLetter);
						if (label!=null && StringUtils.isNotEmpty(value)){
							relation.setAttribute(label, value.trim());
						}
						labelLetter = line.charAt(0);
						value=line.substring(2);
						
					} else if (labelLetter=='e'){
						value+=line+"\n";
					} else {
						value+=line+" / ";
					}
				}
			}
		} catch (final IOException exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.IO_ERROR.create(exception, "Reading line.");
		}
		
		relation.setName(relation.getAttributeValue("TRIBE")+" "+relation.getAttributeValue("VILLAGE")+" "+relation.getAttributeValue("QUARTER")+" "+relation.getAttributeValue("SEGMENT")+" ");

	}
	
	private static void getGenealogy(Net net, Role defaultRole, Report report, StringList errors){
		
		for (Relation relation : net.relations()){

			String genealogy = relation.getAttributeValue("GENEALOGY");
			if (genealogy!=null){
				String indis = addGenealogy(net,relation,genealogy,defaultRole,errors);
				relation.setAttribute("GENEALOGY", indis);
//				report.outputs().appendln(relation.getId()+"\t"+indis);
				
			}
		}
	}
	
	private static String trim (String item){
		String result;
		
		result = item.replaceAll("= ", "=");
		while (result.contains("=-")){
			result = result.replaceAll("=-", "=");
		}

		//
		return result;
	}
	
	private static String addGenealogy(Net net, Relation relation, String genealogy, Role defaultRole, StringList errors){
		String result;
		
		Individuals individuals = new Individuals();
		result = "";
		
		String[] generations = genealogy.split("\\n");
		int row = 0;
		for (String generation : generations){
			String[] coupleStrings = trim(generation).replaceAll("-", "\n").split("\\n");
			result += row+": "+Arrays.toString(coupleStrings)+";";
			for (String coupleString : coupleStrings){
				String[] indiStrings = coupleString.replaceAll("=,", "=").split("=");
				Individuals couple = new Individuals();
				for (String indiString : indiStrings){
					if (!StringUtils.isEmpty(indiString.trim()) && !StringUtils.isEmpty(indiString.replaceAll("-", "").trim())){
						Individual individual = getIndividual(net,indiString,row);
						individual.setAttribute(relation.getModel().getName(), relation.getId()+"");
						relation.addActor(individual, defaultRole);
						individuals.add(individual);
						couple.add(individual);
					}
				}
				if (couple.size()>1){
					NetUtils.setKinSpouse(net, couple.getFirst(), couple.getLast());
				} else if (coupleString.contains("=")) {
					// Should not happen, can be deleted...
					errors.appendln("Segment "+relation.getId()+":\tCut-off marriage partner for "+couple.getFirst()+":\tLine "+generation);
				}
			}
			row++;
		}
		
		for (Individual child : individuals){
			String parentName = child.getAttributeValue("PARENT");
			int parentRow = Integer.parseInt(child.getAttributeValue("ROW"))-1;
			
			if (parentName!=null){
				boolean hasParent = false;
				for (Individual parent : individuals.searchByName(parentName)){
					if (Integer.parseInt(parent.getAttributeValue("ROW"))==parentRow){
						NetUtils.setKinParent(net, parent, child);
						hasParent = true;
					}
				}
				if (!hasParent){
					errors.appendln("Segment "+relation.getId()+":\tNo parent with name "+parentName+" found in generation "+parentRow+" for "+child);
				}
			}
		}
		//
		return result;
	}
	
	private static Individual getIndividual(Net net,String indiString, int row){
		Individual result;
		
		int column = 0;
		Gender gender = Gender.UNKNOWN;
		String name = null;
		String parent = null;
		boolean alive = false;
		
		while (indiString.charAt(0)=='-'){
			indiString = indiString.substring(1);
			column++;
		}
		
		if (indiString.contains(" bi ")){
			gender = Gender.MALE;
			indiString = indiString.replace(" bi ", ";");
		} else if (indiString.contains(" lu ")){
			gender = Gender.FEMALE;
			indiString = indiString.replace(" lu ", ";");
		}
		
		if (indiString.contains("#")){
			indiString = indiString.replaceAll("#", "");
			alive = true;
		}
		
		String[] parentChildPair = indiString.split(";");
		
		if (parentChildPair.length==1){
			name = parentChildPair[0].trim();
		} else if (parentChildPair.length==2){
			parent = parentChildPair[0].trim();
			name = parentChildPair[1].trim();
		}
		
		result = net.createIndividual(name, gender);
		result.setAttribute("ROW", row+"");
		result.setAttribute("COLUMN", column+"");
		if (parent!=null){
			result.setAttribute("PARENT", parent);
		}
		if (alive){
			result.setAttribute("ALIVE", "yes");
		}
		
		//
		return result;
	}

	
	
	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	private static Relation load(final File file, final RelationModel model) throws PuckException {
		Relation result;
		
		result = new Relation(model);
		
		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), PuckManager.DEFAULT_CHARSET_NAME));
			read(result,in);
			result.setAttribute("SOURCEFILE", file.getName());

		} catch (UnsupportedEncodingException exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");

		} catch (FileNotFoundException exception) {
			//
			exception.printStackTrace();
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");

		} finally {
			//
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}
	
	public static Net load(final File file, final String relationModelName, final String defaultRoleName, final Report report) throws PuckException{
		Net result;
		
		result = new Net();
		
			String directory = file.getAbsolutePath();
			File folder = new File(directory);
			RelationModel model = result.createRelationModel(relationModelName);
			Role defaultRole = new Role(defaultRoleName);
			
			for (File detailfile : folder.listFiles()) {
				try {
					Relation relation = DELUZFile.load(detailfile,model);
					result.relations().add(relation);
					
				} catch (PuckException ex) {
					System.err.println("Not a corpus file: " + detailfile.getName());
				}
			}
			
			report.outputs().appendln("Segment\tGenealogies");
			StringList errors = new StringList();
			
			getGenealogy(result,defaultRole,report,errors);
			
			if (!errors.isEmpty()){
				report.outputs().appendln();
				report.outputs().appendln("Importation errors:");
				report.outputs().appendln(errors);
			}
			
		//
		return result;
		
	}
}
