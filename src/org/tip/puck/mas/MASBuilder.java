package org.tip.puck.mas;

import org.tip.puck.net.Gender;
import org.tip.puck.params.Param;
import org.tip.puck.params.ParamException;
import org.tip.puck.params.Params;

/**
 * @author Telmo Menezes
 *
 */
public class MASBuilder {
    public static MAS fromParams(Params params) throws ParamException {
        //Param probSameMarriage = params.getParam("probSameMarriage");
        
        MAS mas = new MAS();
        
        // weight factor: normal age - mother
        if (params.paramExists("wfMotherNormalAgeMean")) {
            Param wfMotherNormalAgeMean = params.getParam("wfMotherNormalAgeMean");
            Param wfMotherNormalAgeStdev = params.getParam("wfMotherNormalAgeStdev");
            mas.addFactor(new WFNormalAge(Gender.FEMALE,
                    wfMotherNormalAgeMean.getValue(),
                    wfMotherNormalAgeStdev.getValue()));
        }
        
        // weight factor: normal age - father
        if (params.paramExists("wfFatherNormalAgeMean")) {
            Param wfFatherNormalAgeMean = params.getParam("wfFatherNormalAgeMean");
            Param wfFatherNormalAgeStdev = params.getParam("wfFatherNormalAgeStdev");
            mas.addFactor(new WFNormalAge(Gender.MALE,
                    wfFatherNormalAgeMean.getValue(),
                    wfFatherNormalAgeStdev.getValue()));
        }
        
        // weight factor: normal age difference
        if (params.paramExists("wfNormalAgeDifferenceMean")) {
            Param wfNormalAgeDifferenceMean = params.getParam("wfNormalAgeDifferenceMean");
            Param wfNormalAgeDifferenceStdev = params.getParam("wfNormalAgeDifferenceStdev");
            mas.addFactor(new WFNormalAgeDifference(
                    wfNormalAgeDifferenceMean.getValue(),
                    wfNormalAgeDifferenceStdev.getValue()));
        }
        
        // weight factor: divorce
        if (params.paramExists("wfDivorce")) {
            Param wfDivorceProb = params.getParam("wfDivorceProb");
            mas.addFactor(new WFDivorce(wfDivorceProb.getValue()));
        }
        
        mas.addFactor(new WFDivorce(0.01));
        
        return mas;
    }
}