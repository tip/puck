package org.tip.puck.util;

import java.util.Comparator;

/**
 * 
 * @author TIP
 * 
 */
public class NumberableComparator implements Comparator<Numberable> {
	/**
	 * 
	 */
	@Override
	public int compare(final Numberable alpha, final Numberable bravo) {
		int result;

		result = alpha.getId() - bravo.getId();

		//
		return result;
	}
}
