package org.tip.puck.kinoath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.Individuals;

/**
 * This class defines a individual list with an idea. The goal is to meet
 * individual who have a link in a kin network.
 * 
 * @author TIP
 */
public class IndividualGroup extends Individuals {

	private static final Logger logger = LoggerFactory.getLogger(IndividualGroup.class);

	private long longId;
	private String label;

	/**
	 * 
	 */
	public IndividualGroup() {
		//
		this(0, (String) null);
	}

	/**
	 * 
	 */
	public IndividualGroup(final IndividualGroup source) {
		//
		this(source.getLongId(), source.getLabel());

		this.add(source);
	}

	/**
	 * 
	 */
	public IndividualGroup(final long id, final String name) {
		//
		super();

		//
		this.longId = id;
		this.label = name;
	}

	/**
	 * 
	 */
	public IndividualGroup(final String name) {
		//
		this(0, name);
	}

	public long getLongId() {
		return this.longId;
	}

	public String getLabel() {
		return this.label;
	}

	public void setLongId(final long id) {
		this.longId = id;
	}

	public void setLabel(final String label) {
		this.label = label;
	}
}
