package org.tip.puck.net.workers;


/**
 * 
 * @author TIP
 */
public class GeoPlaceNetCriteria {

	private AttributeTypes scopes;
	private boolean weight;

	/**
	 * 
	 */
	public GeoPlaceNetCriteria() {
		this.scopes = new AttributeTypes();
		this.weight = true;
	}

	public AttributeTypes getScopes() {
		return this.scopes;
	}

	public boolean isWeight() {
		return this.weight;
	}

	public void setWeight(final boolean weight) {
		this.weight = weight;
	}
}
