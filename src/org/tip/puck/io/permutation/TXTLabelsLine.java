package org.tip.puck.io.permutation;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a line of labels from a TXT file.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class TXTLabelsLine {
	protected List<String> labels;

	/**
	 * 
	 */
	public TXTLabelsLine() {
		super();
		this.labels = new ArrayList<String>();
	}

	public List<String> labels() {
		return labels;
	}

}
