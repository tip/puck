package org.tip.puck.params;

/**
 * @author Telmo Menezes
 *
 */
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Params {
    private Map<String, Param> params;
    
    public Params() {
        params = new HashMap<String, Param>();
    }
    
    public void addParam(String name, Param param) {
        params.put(name, param);
    }
    
    public Param getParam(String name) throws ParamException {
        if (!paramExists(name)) {
            throw new ParamException("Param: '" + name + "' does not exist.");
        }
        return params.get(name);
    }
    
    public boolean paramExists(String name) {
        return params.containsKey(name);
    }
    
    public static Params load(String filePath) throws NumberFormatException, IOException {
        Params newParams = new Params();
        
        FileInputStream fstream = new FileInputStream(filePath);
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
            
        String strLine;
        while ((strLine = br.readLine()) != null)   {
            String[] tokens = strLine.split(" ");
            if (tokens.length == 2) {
                double value = Double.parseDouble(tokens[1]);
                Param param = new Param(value);
                newParams.addParam(tokens[0], param);
            }
            else if (tokens.length == 3) {
                double minValue = Double.parseDouble(tokens[1]);
                double maxValue = Double.parseDouble(tokens[2]);
                Param param = new Param(minValue, maxValue);
                newParams.addParam(tokens[0], param);
            }
        }
        in.close();
        
        return newParams;
    }
}