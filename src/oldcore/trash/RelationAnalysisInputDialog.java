package oldcore.trash;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.census.workers.ChainValuator.ChainProperty;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.RelationModels;
import org.tip.puck.net.workers.AttributeDescriptors;
import org.tip.puck.sequences.workers.SequenceCriteria;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.StringList;
import fr.devinsy.util.StringSet;
import javax.swing.ComboBoxModel;

/**
 * 
 * @author TIP
 */
public class RelationAnalysisInputDialog extends JDialog {

	private static final long serialVersionUID = -4483136985642437109L;

	private static final Logger logger = LoggerFactory.getLogger(RelationAnalysisInputDialog.class);

	private RelationModels relationModels;
	private AttributeDescriptors attributeDescriptors;

	private final JPanel contentPanel = new JPanel();
	private SequenceCriteria dialogCriteria;
	private static SequenceCriteria lastCriteria = new SequenceCriteria();
	private JComboBox cmbxRelationModel;
	private JComboBox cmbxDateLabel;
	private JComboBox cmbxLocalUnitLabel;
	private JButton okButton;
	private JPanel alterRelationModelPanel;
	private JTextField txtfldPattern;
	private JTextField txtfldAlterFilterAttributeLabel;
	private JTextField txtfldAlterFilterAttributeValue;
	private JComboBox cmbxChainClassification;
	private JComboBox cmbxDefautlReferentRoleName;
	private JComboBox cmbxAlterFilterRole;

	/**
	 * Create the dialog.
	 */
	public RelationAnalysisInputDialog(final RelationModels relationModels, final AttributeDescriptors attributeDescriptors) {
		super();

		this.relationModels = relationModels;
		this.attributeDescriptors = attributeDescriptors;

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Relation Analysis Input Dialog");
		setIconImage(Toolkit.getDefaultToolkit().getImage(RelationAnalysisInputDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				RelationAnalysisInputDialog.this.dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 521, 494);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(100dlu;default):grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("max(75dlu;default)"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,}));
		{
			JLabel lblRelationModel = new JLabel("Relation Model:");
			this.contentPanel.add(lblRelationModel, "2, 2, right, default");
		}
		{
			this.cmbxRelationModel = new JComboBox(relationModels.nameList().toArray());
			this.contentPanel.add(this.cmbxRelationModel, "4, 2, fill, default");
		}
		{
			JLabel lblPattern = new JLabel("Pattern:");
			this.contentPanel.add(lblPattern, "2, 4, right, default");
		}
		{
			this.txtfldPattern = new JTextField();
			this.contentPanel.add(this.txtfldPattern, "4, 4, fill, default");
			this.txtfldPattern.setColumns(10);
		}
		{
			JLabel lblChain = new JLabel("Chain Classification:");
			this.contentPanel.add(lblChain, "2, 6, right, default");
		}
		{
			this.cmbxChainClassification = new JComboBox();
			this.cmbxChainClassification.setModel(new DefaultComboBoxModel(ChainProperty.values()));
			this.contentPanel.add(this.cmbxChainClassification, "4, 6, fill, default");
		}
		{
			JLabel lblRelationModelNames = new JLabel("Alter Relation Models:");
			this.contentPanel.add(lblRelationModelNames, "2, 8, right, top");
		}
		{
			JScrollPane alterRolesScrollPane = new JScrollPane();
			alterRolesScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			this.contentPanel.add(alterRolesScrollPane, "4, 8, fill, fill");
			{
				this.alterRelationModelPanel = new JPanel();
				alterRolesScrollPane.setViewportView(this.alterRelationModelPanel);
				this.alterRelationModelPanel.setLayout(new BoxLayout(this.alterRelationModelPanel, BoxLayout.Y_AXIS));
			}
		}
		{
			JLabel lblAlterFilterRole = new JLabel("Alter Filter Role:");
			this.contentPanel.add(lblAlterFilterRole, "2, 10, right, default");
		}
		{
			this.cmbxAlterFilterRole = new JComboBox(relationModels.roleNames().sort().toArray());
			this.contentPanel.add(this.cmbxAlterFilterRole, "4, 10, fill, default");
		}
		{
			JLabel lblAlterFilterAttribute = new JLabel("Alter Filter Attribute Label:");
			this.contentPanel.add(lblAlterFilterAttribute, "2, 12, right, default");
		}
		{
			this.txtfldAlterFilterAttributeLabel = new JTextField();
			this.contentPanel.add(this.txtfldAlterFilterAttributeLabel, "4, 12, fill, default");
			this.txtfldAlterFilterAttributeLabel.setColumns(10);
		}
		{
			JLabel lblAlterFilterAttribute_1 = new JLabel("Alter Filter Attribute Value:");
			this.contentPanel.add(lblAlterFilterAttribute_1, "2, 14, right, default");
		}
		{
			this.txtfldAlterFilterAttributeValue = new JTextField();
			this.txtfldAlterFilterAttributeValue.setColumns(10);
			this.contentPanel.add(this.txtfldAlterFilterAttributeValue, "4, 14, fill, default");
		}
		{
			JLabel lblDefaultReferentRole = new JLabel("Default Referent Role:");
			this.contentPanel.add(lblDefaultReferentRole, "2, 16, right, default");
		}
		{
			this.cmbxDefautlReferentRoleName = new JComboBox(relationModels.roleNames().sort().toArray());
			this.contentPanel.add(this.cmbxDefautlReferentRoleName, "4, 16, fill, default");
		}
		{
			JLabel lblDateLabel = new JLabel("Date Label:");
			this.contentPanel.add(lblDateLabel, "2, 18, right, default");
		}
		{
			this.cmbxDateLabel = new JComboBox(new DefaultComboBoxModel(attributeDescriptors.labels().sort().toArray()));
			this.contentPanel.add(this.cmbxDateLabel, "4, 18, fill, default");
		}
		{
			JLabel lblLocalUnitLabel = new JLabel("Local Unit Label:");
			contentPanel.add(lblLocalUnitLabel, "2, 20, right, default");
		}
		{
			this.cmbxLocalUnitLabel = new JComboBox(new DefaultComboBoxModel(attributeDescriptors.labels().sort().toArray()));
			contentPanel.add(this.cmbxLocalUnitLabel, "4, 20, fill, default");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						RelationAnalysisInputDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				this.okButton = new JButton("Launch");
				this.okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						SequenceCriteria criteria = getCriteria();

						if (criteria.getRelationModelName() == null) {
							//
							String title = "Invalid input";
							String message = "A relation model is required to launch.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);

						} else if (criteria.getDateLabel() == null) {
							//
							String title = "Invalid input";
							String message = "A valid date label is required to launch.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						} else {
							//
							lastCriteria = criteria;
							RelationAnalysisInputDialog.this.dialogCriteria = criteria;

							//
							setVisible(false);
						}
					}
				});
				this.okButton.setActionCommand("OK");
				buttonPane.add(this.okButton);
				getRootPane().setDefaultButton(this.okButton);
			}
		}

		// ////////////////////////
		this.alterRelationModelPanel.removeAll();
		for (RelationModel model : relationModels) {
			//
			JCheckBox checkBox = new JCheckBox(model.getName());
			checkBox.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(final ItemEvent e) {
					//
					logger.debug("checkbox changed.");
					updateAlterRelationModel();
				}
			});

			this.alterRelationModelPanel.add(checkBox);
		}

		updateAlterRelationModel();

		setCriteria(lastCriteria);
	}

	/**
	 * 
	 * @return
	 */
	private StringList getAlterRelationModelNames() {
		StringList result;

		result = new StringList();

		for (Component component : this.alterRelationModelPanel.getComponents()) {
			//
			JCheckBox checkbox = (JCheckBox) component;

			if (checkbox.isSelected()) {
				//
				result.add(checkbox.getText());
			}
		}

		logger.debug("alterRelationModel checked={}", result.toStringWithCommas());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public SequenceCriteria getCriteria() {
		SequenceCriteria result;

		result = new SequenceCriteria();

		//
		if (this.cmbxRelationModel.getSelectedIndex() == -1) {
			//
			result.setRelationModelName(null);

		} else {
			//
			result.setRelationModelName((String) this.cmbxRelationModel.getSelectedItem());
		}

		//
		result.setPattern(this.txtfldPattern.getText());

		//
		result.setChainClassification(((ChainProperty) this.cmbxChainClassification.getSelectedItem()).name());

		//
		result.setRelationModelNames(getAlterRelationModelNames());

		//
		result.setAlterFilterRoleName((String) this.cmbxAlterFilterRole.getSelectedItem());
		result.setAlterAttributeLabel(this.txtfldAlterFilterAttributeLabel.getText());
		result.setAlterAttributeValue(this.txtfldAlterFilterAttributeValue.getText());

		result.setDefaultReferentRoleName((String) this.cmbxDefautlReferentRoleName.getSelectedItem());

		result.setDateLabel((String) this.cmbxDateLabel.getSelectedItem());
		result.setLocalUnitLabel((String) this.cmbxLocalUnitLabel.getSelectedItem());

		logger.debug("[relationModelName={}][chaineClassification={}][alterRelationModelNames={}]", result.getRelationModelName(),
				result.getChainClassification(), new StringList(result.getRelationModelNames()).toStringWithCommas());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public SequenceCriteria getDialogCriteria() {
		SequenceCriteria result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final SequenceCriteria source) {
		//
		if (source != null) {
			//
			if (this.relationModels.isEmpty()) {
				//
				this.cmbxRelationModel.setEnabled(false);
				this.txtfldPattern.setEnabled(false);
				this.cmbxChainClassification.setEnabled(false);
				this.cmbxDefautlReferentRoleName.setEnabled(false);
				this.alterRelationModelPanel.setEnabled(false);
				this.cmbxAlterFilterRole.setEnabled(false);
				this.txtfldAlterFilterAttributeLabel.setEnabled(false);
				this.txtfldAlterFilterAttributeValue.setEnabled(false);
				this.cmbxDateLabel.setEnabled(false);
				this.cmbxLocalUnitLabel.setEnabled(false);

			} else {
				//
				this.cmbxRelationModel.setEnabled(true);
				this.txtfldPattern.setEnabled(true);
				this.cmbxChainClassification.setEnabled(true);
				this.cmbxDefautlReferentRoleName.setEnabled(true);
				this.alterRelationModelPanel.setEnabled(true);
				this.cmbxAlterFilterRole.setEnabled(true);
				this.txtfldAlterFilterAttributeLabel.setEnabled(true);
				this.txtfldAlterFilterAttributeValue.setEnabled(true);
				this.cmbxDateLabel.setEnabled(true);
				this.cmbxLocalUnitLabel.setEnabled(true);

				//
				this.cmbxRelationModel.setSelectedItem(source.getRelationModelName());
				this.txtfldPattern.setText(source.getPattern());
				this.cmbxChainClassification.setSelectedItem(ChainProperty.valueOf(source.getChainClassification()));

				for (Component component : this.alterRelationModelPanel.getComponents()) {
					//
					JCheckBox checkbox = (JCheckBox) component;

					if (source.getRelationModelNames().contains(checkbox.getText())) {
						//
						checkbox.setSelected(true);
					}
				}

				this.txtfldAlterFilterAttributeLabel.setText(source.getAlterFilterAttributeLabel());
				this.txtfldAlterFilterAttributeValue.setText(source.getAlterFilterAttributeValue());

				this.cmbxAlterFilterRole.setSelectedItem(source.getAlterFilterRoleName());

				this.cmbxDefautlReferentRoleName.setSelectedItem(source.getDefaultReferentRoleName());
				this.cmbxDateLabel.setSelectedItem(source.getDateLabel());
				this.cmbxLocalUnitLabel.setSelectedItem(source.getLocalUnitLabel());
			}
		}
	}

	/**
	 * 
	 * @param currentRelationModelIndex
	 */
	private void updateAlterRelationModel() {
		//
		StringList currentRelationModelNames = getAlterRelationModelNames();

		StringSet roleNames = new StringSet();

		for (String relationModelName : currentRelationModelNames) {
			//
			roleNames.addAll(this.relationModels.getByName(relationModelName).roles().toNameList());
		}

		logger.debug("roleNames={}", roleNames.toStringList().toStringWithCommas());

		//
		StringList target = new StringList();
		target.add("ALL");
		target.addAll(roleNames.toStringList().sort());
		this.cmbxAlterFilterRole.setModel(new DefaultComboBoxModel(target.toArray()));
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		/* AttributeFilter criteria = */showDialog(null, null);
	}

	/**
	 * Launch the application.
	 */
	public static SequenceCriteria showDialog(final RelationModels relationModels, final AttributeDescriptors attributeDescriptors) {
		SequenceCriteria result;

		//
		RelationAnalysisInputDialog dialog = new RelationAnalysisInputDialog(relationModels, attributeDescriptors);
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
