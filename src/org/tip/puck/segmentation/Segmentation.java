package org.tip.puck.segmentation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.geo.Geography;
import org.tip.puck.net.Families;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.RelationModels;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.relations.workers.RelationValuator;
import org.tip.puck.net.workers.FamilyValuator;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionCriteria.RelationModelCanonicalNames;
import org.tip.puck.partitions.PartitionCriteriaList;
import org.tip.puck.util.Labels;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class Segmentation {

	private String label;
	private Segments segments;
	private int currentSegmentIndex;
	private Geography geography;

	/**
	 * 
	 * @param individuals
	 * @param families
	 * @param relations
	 */
	public Segmentation(final Individuals individuals, final Families families, final Relations relations, final Geography geography) {

		this.segments = new Segments();

		this.segments.add(new DefaultSegment(individuals, families, relations, geography));

		this.currentSegmentIndex = 0;
		
		this.geography = geography;
	}

	/**
	 * 
	 */
	public Segmentation(final Net source) {

		this.segments = new Segments();

		this.label = source.getLabel();

		this.segments.add(new DefaultSegment(source.individuals(), source.families(), source.relations(), source.getGeography()));

		this.currentSegmentIndex = 0;
		
		this.geography = source.getGeography();
	}

	/**
	 * This methods instanciates a segmentation from a net and one segmentation
	 * of this net.
	 * 
	 * @param source
	 *            The net where take individuals, families and relations.
	 * 
	 * @param reference
	 *            The segmentation to copy.
	 * 
	 * @throws PuckException
	 */
	public Segmentation(final Net source, final Segmentation reference) throws PuckException {

		this.segments = new Segments();

		this.label = source.getLabel();
		
		this.geography = source.getGeography();

		this.segments.add(new DefaultSegment(source.individuals(), source.families(), source.relations(), source.getGeography()));

		if (reference != null) {
			//
			for (int index = 1; index < reference.getSegments().size(); index++) {
				//
				Segment sourceSegment = reference.getSegments().get(index);

				//
				Segment targetSegment = addSegment(sourceSegment.getCriteria());
				targetSegment.selectCluster(sourceSegment.getCurrentClusterIndex());
			}

			//
			this.currentSegmentIndex = reference.getCurrentSegmentIndex();
		}
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 * @throws PuckException
	 */
	public Segment addSegment(final PartitionCriteria criteria) throws PuckException {
		Segment result;

		if ((criteria == null) || (StringUtils.isBlank(criteria.getRelationModelName()))) {
			//
			result = null;

		} else {
			//
			while (this.currentSegmentIndex != this.segments.size() - 1) {
				this.segments.remove(this.segments.size() - 1);
			}

			//
			result = this.segments.addSegment(this.getCurrentSegment(), criteria);
			this.currentSegmentIndex += 1;
		}
		
		result.setGeography(geography);

		//
		return result;
	}

	/**
	 * 
	 * @throws PuckException
	 */
	public void clear() throws PuckException {
		//
		this.currentSegmentIndex = 0;

		//
		clearBelow();
	}

	/**
	 * 
	 * @throws PuckException
	 */
	public void clearBelow() throws PuckException {
		//
		while (this.segments.size() - 1 > this.currentSegmentIndex) {
			this.segments.remove(this.segments.size() - 1);
		}
	}

	/**
	 * 
	 * @return
	 */
	public Families getAllFamilies() {
		Families result;

		result = this.segments.get(0).getCurrentFamilies();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individuals getAllIndividuals() {
		Individuals result;

		result = this.segments.get(0).getCurrentIndividuals();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Relations getAllRelations() {
		Relations result;

		result = this.segments.get(0).getCurrentRelations();

		//
		return result;
	}

	/**
	 * 
	 * Note: by default, the first criteria is null.
	 * 
	 * @return
	 */
	public PartitionCriteriaList getCriteriaList() {
		PartitionCriteriaList result;

		//
		result = new PartitionCriteriaList(size());

		//
		for (int index = 1; index < this.segments.size(); index++) {
			//
			Segment segment = this.segments.get(index);

			//
			if (segment != null) {
				result.add(segment.getCriteria());
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int getCurrentClusterIndex() {
		int result;

		result = this.segments.get(this.currentSegmentIndex).getCurrentClusterIndex();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> getCurrentClusterLabels() {
		List<String> result;

		//
		result = this.segments.get(this.currentSegmentIndex).getClusterLabels();

		//
		return result;
	}

	/***
	 * 
	 * @return
	 */
	public Families getCurrentFamilies() {
		Families result;

		result = this.segments.get(this.currentSegmentIndex).getCurrentFamilies();

		//
		return result;
	}

	/***
	 * 
	 * @return
	 */
	public Individuals getCurrentIndividuals() {
		Individuals result;

		result = this.segments.get(this.currentSegmentIndex).getCurrentIndividuals();

		//
		return result;
	}

	/***
	 * 
	 * @return
	 */
	public Relations getCurrentRelations() {
		Relations result;

		result = this.segments.get(this.currentSegmentIndex).getCurrentRelations();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Segment getCurrentSegment() {
		Segment result;

		result = this.segments.get(this.currentSegmentIndex);

		//
		return result;
	}

	public int getCurrentSegmentIndex() {
		return this.currentSegmentIndex;
	}

	public String getLabel() {
		return this.label;
	}

	/**
	 * 
	 * @return
	 */
	public Families getOutOfCurrentClusterFamilies() {
		Families result;

		result = this.segments.get(this.currentSegmentIndex).getOutOfCurrentClusterFamilies();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individuals getOutOfCurrentClusterIndividuals() {
		Individuals result;

		result = this.segments.get(this.currentSegmentIndex).getOutOfCurrentClusterIndividuals();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Relations getOutOfCurrentClusterRelations() {
		Relations result;

		result = this.segments.get(this.currentSegmentIndex).getOutOfCurrentClusterRelations();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public StringList getSegmentLabels() {
		StringList result;

		//
		result = new StringList();

		//
		result.add(this.segments.get(0).getLabel());
		for (int segmentIndex = 1; segmentIndex < this.segments.size(); segmentIndex++) {
			result.add(segmentIndex + " " + this.segments.get(segmentIndex).getLabel());
		}

		//
		return result;
	}

	public Segments getSegments() {
		return this.segments;
	}

	/**
	 * 
	 * @return
	 */
	public String getSummary() {
		String result;

		//
		StringList summary = new StringList();

		//
		summary.appendln(String.format("%s (%d)", this.segments.get(0).getLabel(), this.segments.get(0).getCurrentSize()));
		for (int segmentIndex = 1; segmentIndex <= this.currentSegmentIndex; segmentIndex++) {
			summary.appendln(String.format("%d %s (%d)", segmentIndex, this.segments.get(segmentIndex).getLabel(), this.segments.get(segmentIndex)
					.getCurrentSize()));
		}

		//
		result = summary.toString();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isAtTheBottom() {
		boolean result;

		if (this.currentSegmentIndex == this.segments.size() - 1) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isAtTheTop() {
		boolean result;

		if (this.currentSegmentIndex == 0) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isOn() {
		boolean result;

		if (this.currentSegmentIndex > 0) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * @throws PuckException
	 * 
	 */
	public void refresh() throws PuckException {
		//
		for (int stepIndex = 1; stepIndex < this.segments.size(); stepIndex++) {
			this.segments.get(stepIndex).refresh(this.segments.get(stepIndex - 1));
		}
	}

	/**
	 * 
	 * @param index
	 * @throws PuckException
	 */
	public void selectCluster(final Individual individual) throws PuckException {
		//
		if (individual != null) {
			this.segments.get(this.currentSegmentIndex).selectCluster(individual);
		}
	}

	/**
	 * 
	 * @param index
	 * @throws PuckException
	 */
	public void selectCluster(final int index) throws PuckException {

		this.segments.get(this.currentSegmentIndex).selectCluster(index);
	}

	/**
	 * 
	 * @param index
	 * @throws PuckException
	 */
	public void selectNextPartition() throws PuckException {

		selectPartition(this.currentSegmentIndex + 1);
	}

	/**
	 * 
	 * @param index
	 * @throws PuckException
	 */
	public void selectPartition(final int index) throws PuckException {

		if ((index < 0) || (index >= this.segments.size())) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Index out of bounds.");

		} else {
			//
			for (int stepIndex = this.currentSegmentIndex + 1; stepIndex <= index; stepIndex++) {

				this.segments.get(stepIndex).refresh(this.segments.get(stepIndex - 1));
			}

			//
			this.currentSegmentIndex = index;
		}
	}

	/**
	 * 
	 * @param index
	 * @throws PuckException
	 */
	public void selectPreviousPartition() throws PuckException {

		selectPartition(this.currentSegmentIndex - 1);
	}

	public void setLabel(final String label) {

		this.label = label;
	}

	/**
	 * 
	 * @return
	 */
	public int size() {
		int result;

		result = this.segments.size();

		//
		return result;
	}

	/**
	 * 
	 * @param individuals
	 * @param families
	 * @param relations
	 * @return
	 */
	public static List<Labels> buildModelLabels(final Individuals individuals, final Families families, final RelationModels relationModels,
			final Relations relations) {
		List<Labels> result;

		result = new ArrayList<Labels>();

		//
		Labels labels = new Labels(RelationModelCanonicalNames.INDIVIDUAL.toString());
		labels.addAll(IndividualValuator.getAttributeLabels(individuals));
		result.add(labels);

		//
		labels = new Labels(RelationModelCanonicalNames.FAMILY.toString());
		labels.addAll(FamilyValuator.getAttributeLabels(families));
		result.add(labels);

		//
		for (RelationModel relationModel : relationModels) {
			//
			labels = new Labels(relationModel.getName());
			labels.addAll(RelationValuator.getAttributeLabels(relations.getByModel(relationModel)));
			result.add(labels);
		}

		//
		return result;
	}
	
	public String toString(){
		String result;
		
		result = label+" "+currentSegmentIndex;
		
		if (segments!=null){
			result +=" "+segments.get(currentSegmentIndex);
		}
		//
		return result;
	}

	public Geography getGeography() {
		return geography;
	}
	
	
}
