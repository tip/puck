package org.tip.puckgui.models;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.table.AbstractTableModel;

import org.tip.puck.net.Attribute;
import org.tip.puck.net.Individual;

/**
 * 
 * @author TIP
 */
public class IndividualAttributesModel extends AbstractTableModel {

	private static final long serialVersionUID = 7323540004050267991L;

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("org.tip.puckgui.messages");

	protected Individual source;
	protected List<Attribute> attributes;

	/**
	 * 
	 */
	public IndividualAttributesModel(final Individual source) {
		super();
		this.source = source;
		if (source == null) {
			this.attributes = new ArrayList<Attribute>();
		} else {
			this.attributes = this.source.attributes().toSortedList();
		}
	}

	/**
	 * 
	 */
	@Override
	public Class getColumnClass(final int columnIndex) {
		Class result;

		if (this.source == null) {
			result = String.class;
		} else {
			switch (columnIndex) {
				case 0:
					result = String.class;
				break;
				case 1:
					result = String.class;
				break;
				default:
					result = String.class;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getColumnCount() {
		int result;

		result = 2;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String getColumnName(final int columnIndex) {
		String result;

		switch (columnIndex) {
			case 0:
				result = BUNDLE.getString("MainWindow.individual.label");
			break;
			case 1:
				result = BUNDLE.getString("MainWindow.individual.value");
			break;
			default:
				result = "";
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getRowCount() {
		int result;

		result = this.attributes.size();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Object getValueAt(final int rowIndex, final int columnIndex) {
		Object result;

		if (this.source == null) {
			result = null;
		} else {
			switch (columnIndex) {
				case 0:
					result = this.attributes.get(rowIndex).getLabel();
				break;
				case 1:
					result = this.attributes.get(rowIndex).getValue();
				break;
				default:
					result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void setSource(final Individual source) {
		this.source = source;
		if (source == null) {
			this.attributes = new ArrayList<Attribute>();
		} else {
			this.attributes = this.source.attributes().toSortedList();
		}
		fireTableDataChanged();
	}

	/**
	 * 
	 * @return
	 */
	public Individual source() {
		Individual result;

		result = this.source;

		//
		return result;
	}
}
