package org.tip.puck.partitions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.census.chains.Chain;
import org.tip.puck.census.chains.Chains;
import org.tip.puck.census.workers.ChainValuator;
import org.tip.puck.geo.Geography;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.Node;
import org.tip.puck.graphs.workers.NodeValuator;
import org.tip.puck.net.Families;
import org.tip.puck.net.Family;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individualizable;
import org.tip.puck.net.Individuals;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.relations.workers.RelationValuator;
import org.tip.puck.net.workers.FamilyValuator;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.partitions.Interval.EndpointStatus;
import org.tip.puck.partitions.PartitionCriteria.SizeFilter;
import org.tip.puck.partitions.PartitionCriteria.ValueFilter;
import org.tip.puck.sequences.Sequenceable;
import org.tip.puck.sequences.Sequenceables;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.Numberable;
import org.tip.puck.util.NumberedValues;
import org.tip.puck.util.Value;
import org.tip.puck.util.Values;
import org.tip.puck.workers.MetaValuator;

/**
 * Don't be afraid by the number of methods. In fact, each source type has only
 * one specific method: create(Type, PartitionCriteria). All other methods are
 * helpers, they contain only one line to call the specific method.
 * 
 * @author TIP
 */
public class PartitionMaker {
	private static final Logger logger = LoggerFactory.getLogger(PartitionMaker.class);

	private static final double MAX_INTERVALS = 10000;
	private static final double MIN_VALUE = Double.MIN_VALUE / 4;
	private static final double MAX_VALUE = Double.MAX_VALUE / 4;

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> create(final Net source, final PartitionCriteria criteria) throws PuckException {
		Partition<Individual> result;

		if ((source == null) || (criteria == null)) {
			result = null;
		} else {
			//
			result = create(source.getLabel() + " " + criteria.getLabel(), source.individuals(), criteria, source.getGeography());
		}

		//
		return result;
	}
	
	public static <E extends Numberable, T> Partition<E> createPseudoPartition (final Partition<E> source, final PartitionCriteria criteria) throws PuckException{
		Partition<E> result;
		
		if ((source == null) || (criteria == null)) {
			result = null;
		} else {
			
			//
			result = new Partition<E>();
			result.setLabel(source.getLabel());
			result.setCriteria(criteria);
			result.setPseudo(true);
			result.setOriginalItems(new HashSet<E>());
			
			int pseudoId = 1;
			
			for (Cluster<E> cluster : source.getClusters()){
				
				if (cluster.getValue()!=null){
					
					for (T partial : (List<T>)cluster.getValue().listValue()){
						
						for (E item : cluster.getItems()){
								
							result.put((E)item.clone(pseudoId),new Value(partial));
							result.getOriginalItems().add(item);
							
							pseudoId++;
						}
					}
				}
			}
		}

		//
		return result;
	}
	
	public static <E extends Numberable> Partition<E> create (final PartitionSequence<E> source, final PartitionCriteria criteria, final Geography geography) throws PuckException{
		Partition<E> result;
		
		if ((source == null) || (criteria == null)) {
			result = null;
		} else {
			
			//
			result = new Partition<E>();
			result.setLabel(source.getLabel());
			result.setCriteria(criteria);
			
			//
			for (E item : source.getItems()) {
				result.put(item, new MetaValuator<E>().get(item,criteria.getLabel(),geography));
			}
	
		}

		//
		return result;
	}
	

	
	public static <E extends Numberable> Partition<E> create (final Partition<E> source, final PartitionCriteria criteria) throws PuckException{
		Partition<E> result;
		
		if ((source == null) || (criteria == null)) {
			result = null;
		} else {
			
			//
			result = new Partition<E>();
			result.setLabel(source.getLabel());
			result.setCriteria(criteria);
			
			
			NumberedValues values = new NumberedValues(source);
			NumberedValues partitionValues = getPartitionValues(values, criteria);

			//
			for (E item : source.getItems()) {
				result.put(item, partitionValues.get(item.getId()));
			}
	
		}

		//
		return result;
	}
	
	public static <S extends Individualizable> Partition<Individual> individualize (final Partition<S> source, final PartitionCriteria criteria) throws PuckException{
		Partition<Individual> result;
		
		if ((source == null) || (criteria == null)) {
			result = null;
		} else {
			
			//
			result = new Partition<Individual>();
			result.setLabel(source.getLabel());
			result.setCriteria(criteria);
			
			
			NumberedValues values = new NumberedValues(source);
			NumberedValues partitionValues = getPartitionValues(values, criteria);

			//
			for (S item : source.getItems()) {
				result.put(item.getEgo(), partitionValues.get(item.getId()));
			}
	
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Family> create(final String title, final Families source, final PartitionCriteria criteria, final Geography geography) throws PuckException {
		Partition<Family> result;

		if ((source == null) || (criteria == null)) {
			result = null;
		} else {
			//
			result = new Partition<Family>();
			result.setLabel(title);
			result.setCriteria(criteria);

			//
			NumberedValues values = FamilyValuator.get(source, criteria.getLabel(), criteria.getLabelParameter(), geography);

			//
			NumberedValues partitionValues = getPartitionValues(values, criteria);

			//
			for (Family family : source) {
				result.put(family, partitionValues.get(family.getId()));
			}

		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> create(final String title, final Individuals source, final PartitionCriteria criteria, final Geography geography) throws PuckException {
		Partition<Individual> result;
		
		if ((source == null) || (criteria == null)) {
			result = null;
		} else {
			//
			result = new Partition<Individual>();
			result.setLabel(title);
			result.setCriteria(criteria);
									
			//
			// Check for dated vs Undated attributes
			if (criteria.getRelationModelName()!=null && criteria.getEgoRoleName()!=null && criteria.getTime()!=null){
				
				Relations datedAttributes = new Relations();
				for (Individual ego : source){
					datedAttributes.add(ego.relations(criteria.getRelationModelName(), criteria.getEgoRoleName(), criteria.getLabel(), "TIME", criteria.getTime()));
				}
				
				NumberedValues values = RelationValuator.getByIndividuals(datedAttributes, criteria.getLabel(), criteria.getLabelParameter(),geography);

				//
				NumberedValues partitionValues = getPartitionValues(values, criteria);

				//
				for (Individual individual : datedAttributes.getIndividuals()) {
					result.put(individual, partitionValues.get(individual.getId()));
				}		
				
			} else {
							
				NumberedValues values = IndividualValuator.get(source, criteria.getLabel(), criteria.getLabelParameter(), geography);
				//
				NumberedValues partitionValues = getPartitionValues(values, criteria);

				//
				for (Individual individual : source) {
					result.put(individual, partitionValues.get(individual.getId()));
				}
			}
						
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> create(final String title, final Individuals individuals, final Relations relations, final PartitionCriteria criteria, final Geography geography) throws PuckException {
		Partition<Individual> result;
				
		if ((individuals == null) || (relations == null) || (criteria == null)) {
			result = null;
		} else {
			//
			result = new Partition<Individual>();
			result.setLabel(title);
			result.setCriteria(criteria);
						
			NumberedValues values = null;
			
			if (RelationValuator.getAttributeLabels(relations).contains(criteria.getLabel())){
				
				values = RelationValuator.getByIndividuals(relations, criteria.getLabel(), criteria.getLabelParameter(),geography);
				
			} else if (IndividualValuator.getAttributeLabels(individuals).contains(criteria.getLabel())){
				
				values = IndividualValuator.get(individuals, criteria.getLabel(), criteria.getLabelParameter(),geography);
				
			} else {
				System.err.println("partition label *"+criteria.getLabel()+"* is not recognized");
			}

			//
			NumberedValues partitionValues = getPartitionValues(values, criteria);
			if (partitionValues!=null){
				for (Individual individual : individuals) {
					result.put(individual, partitionValues.get(individual.getId()));
				}
			}
		}

		//
		return result;
	}


	
	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
/*	public static Partition<Individual> createByIndividuals(final String title, final RelationSet source, final PartitionCriteria criteria) throws PuckException {
		Partition<Individual> result;
		
		if ((source == null) || (criteria == null)) {
			result = null;
		} else {
			//
			result = new Partition<Individual>();
			result.setLabel(title);
			result.setCriteria(criteria);
			
			NumberedValues values = null;
			
			if (RelationValuator.getAttributeLabels(source).contains(criteria.getLabel())){
				values = RelationValuator.getByIndividuals(source, criteria.getLabel(), criteria.getLabelParameter());
			} else if (IndividualValuator.getAttributeLabels(source.getIndividuals()).contains(criteria.getLabel())){
				values = IndividualValuator.get(source.getIndividuals(), criteria.getLabel(), criteria.getLabelParameter());
			} else {
				System.err.println("partition label "+criteria.getLabel()+" is not recognized");
			}

			//
			NumberedValues partitionValues = getPartitionValues(values, criteria);

			//
			for (Individual individual : source.getIndividuals()) {
				result.put(individual, partitionValues.get(individual.getId()));
			}

		}

		//
		return result;
	}*/

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static <E> Partition<Node<E>> create(final String title, final Graph<E> source, final PartitionCriteria criteria) throws PuckException {
		Partition<Node<E>> result;

		if ((source == null) || (criteria == null)) {
			result = null;
		} else {
			//
			result = new Partition<Node<E>>();
			result.setLabel(title);
			result.setCriteria(criteria);
			
			//
			Values values = NodeValuator.get(source, criteria.getLabel());

			//
			Values partitionValues = getPartitionValues(values, criteria);

			//
			List<Node<E>> nodes = source.getNodes().toListSortedById();
			for (int index = 0; index < source.nodeCount(); index++) {
				result.put(nodes.get(index), partitionValues.get(index));
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public static Partition<Value> create(final String title, final List<Value> values) {
		Partition<Value> result;

		result = new Partition<Value>();
		result.setLabel(title);

		for (Value value : values) {
			result.put(value, value);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Chain> create(final String title, final Partition<Chain> source, final PartitionCriteria criteria) throws PuckException {
		Partition<Chain> result;

		if ((source == null) || (criteria == null)) {
			result = null;
		} else {
			//
			result = new Partition<Chain>();
			result.setLabel(title);
			result.setCriteria(criteria);

			//
			for (Cluster<Chain> cluster : source.getClusters()) {
				result.putAll(cluster.getItems(), ChainValuator.get(cluster.getFirstItem(), criteria.getLabel(), criteria.getLabelParameter()));

			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static <E> Partition<E> create(final String title, final Cluster<E> source, final PartitionCriteria criteria, final Geography geography) throws PuckException {
		Partition<E> result;
		
		if ((source == null) || (criteria == null)) {
			result = null;
		} else {
			//
			result = new Partition<E>();
			result.setLabel(title);
			result.setCriteria(criteria);
			result.putClusters();
			
			E firstItem = source.getFirstItem();
						
			if (firstItem instanceof Chain){

				Chains map = new Chains((List<Chain>)source.getItems());
				NumberedValues values = ChainValuator.get((Chains)map, criteria.getLabel(), criteria.getLabelParameter());			
				NumberedValues partitionValues = getPartitionValues(values, criteria);
				for (Chain e : map) {
					result.put((E)e, partitionValues.get(e.getId()));
				}
				
				result.put(firstItem, ChainValuator.get((Chain)firstItem, criteria.getLabel(), criteria.getLabelParameter()));
			
			} else if (firstItem instanceof Individual){

				Individuals map = new Individuals((List<Individual>)source.getItems());
				NumberedValues values = IndividualValuator.get((Individuals)map, criteria.getLabel(), criteria.getLabelParameter(), geography);			
				NumberedValues partitionValues = getPartitionValues(values, criteria);
				for (Individual e : map) {
					result.put((E)e, partitionValues.get(e.getId()));
				}
				
			} else if (firstItem instanceof Family){
				
				Families map = new Families((List<Family>)source.getItems());
				NumberedValues values = FamilyValuator.get((Families)map, criteria.getLabel(), criteria.getLabelParameter(), geography);			
				NumberedValues partitionValues = getPartitionValues(values, criteria);
				for (Family e : map) {
					result.put((E)e, partitionValues.get(e.getId()));
				}
			
			} else if (firstItem instanceof Relation){
				
				Relations map = new Relations((List<Relation>)source.getItems());
				NumberedValues values = RelationValuator.get((Relations)map, criteria.getLabel(), criteria.getLabelParameter(), geography);			
				NumberedValues partitionValues = getPartitionValues(values, criteria);
				for (Relation e : map) {
					result.put((E)e, partitionValues.get(e.getId()));
				}
				
			} else if (firstItem instanceof Individualizable && IndividualValuator.getAttributeLabels(null).contains(criteria.getLabel())){
								
				Individuals map = new Individuals();
				for (E item : source.getItems()){
					map.put(((Individualizable)item).getEgo());
				}
				NumberedValues values = IndividualValuator.get((Individuals)map, criteria.getLabel(), criteria.getLabelParameter(), geography);			
				NumberedValues partitionValues = getPartitionValues(values, criteria);
				for (Individual e : map) {
					result.put((E)e, partitionValues.get(e.getId()));
				}
			}
		}
		//
		return result;
	}
	
	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Relation> create(final String title, final Relations source, final PartitionCriteria criteria, final Geography geography) throws PuckException {
		Partition<Relation> result;

		if ((source == null) || (criteria == null)) {
			result = null;
		} else {
			//
			result = new Partition<Relation>();
			result.setLabel(title);
			result.setCriteria(criteria);

			//
			NumberedValues values = RelationValuator.get(source, criteria.getLabel(), criteria.getLabelParameter(), geography);

			//
			NumberedValues partitionValues = getPartitionValues(values, criteria);

			//
			for (Relation relation : source) {
				result.put(relation, partitionValues.get(relation.getId()));
			}

		}

		//
		return result;
	}
	
	public static <S extends Sequenceable<E>,E extends Numberable> Partition<S> create(final String title, final Sequenceables <S,E> sequences, final NumberedValues values, final PartitionCriteria criteria) throws PuckException {
		Partition<S> result;

		if ((sequences == null) ||(values == null) || (criteria == null)) {
			result = null;
		} else {
			//
			result = new Partition<S>();
			result.setLabel(title);
			result.setCriteria(criteria);
			
			//
			NumberedValues partitionValues = getPartitionValues(values, criteria);

			//
			for (int id : values.keySet()) {

				result.put(sequences.getById(id), partitionValues.get(id));
				
			}
		}

	//
	return result;

	}

	/**
	 * 
	 * @param sequences
	 * @return
	 * @throws PuckException
	 */
/*	public static Partition<Individual> create(final String title, final Individuals individuals, final NumberedValues values, final PartitionCriteria criteria) throws PuckException {
		Partition<Individual> result;

		if ((individuals == null) ||(values == null) || (criteria == null)) {
			result = null;
		} else {
			//
			result = new Partition<Individual>();
			result.setLabel(title);
			result.setCriteria(criteria);

			//
			NumberedValues partitionValues = getPartitionValues(values, criteria);

			//
			for (int id : values.keySet()) {
				
				if (partitionValues.get(id)!=null && partitionValues.get(id).isList()){
					
					result.setPseudo(true);
					
					List list = partitionValues.get(id).listValue();
					if (list.size()==0){
						result.put(individuals.getById(id), new Value("-"));
					} else {
						for (Object partialValue : list){
							if (partialValue == null){
								result.put(individuals.getById(id), null);
							} else if (partialValue instanceof String){
								result.put(individuals.getById(id), new Value((String)partialValue));
							} else if (partialValue instanceof String[]){
									result.put(individuals.getById(id), new Value(Arrays.toString((String[])partialValue)));
							} else {
								result.put(individuals.getById(id), new Value(partialValue.toString()));
							}
							
						}
					}
					
				} else {
					
					result.put(individuals.getById(id), partitionValues.get(id));
					
				}
			}
		}

		//
		return result;
	}*/

	/**
	 * 
	 * @param sequences
	 * @return
	 * @throws PuckException
	 */
/*	public static Partition<Relation> create(final String title, final Relations individuals, final NumberedValues values, final PartitionCriteria criteria) throws PuckException {
		Partition<Relation> result;

		if ((individuals == null) ||(values == null) || (criteria == null)) {
			result = null;
		} else {
			//
			result = new Partition<Relation>();
			result.setLabel(title);
			result.setCriteria(criteria);

			//
			NumberedValues partitionValues = getPartitionValues(values, criteria);

			//
			for (int id : values.keySet()) {
				
				if (partitionValues.get(id)!=null && partitionValues.get(id).isList()){
					
					List list = partitionValues.get(id).listValue();
					if (list.size()==0){
						result.put(individuals.getById(id), new Value("-"));
					} else {
						for (Object partialValue : list){
							if (partialValue == null){
								result.put(individuals.getById(id), null);
							} else if (partialValue instanceof String){
								result.put(individuals.getById(id), new Value((String)partialValue));
							} else if (partialValue instanceof String[]){
									result.put(individuals.getById(id), new Value(Arrays.toString((String[])partialValue)));
							} else {
								result.put(individuals.getById(id), new Value(partialValue.toString()));
							}
							
						}
					}
					
				} else {
					
					result.put(individuals.getById(id), partitionValues.get(id));
					
				}
			}
		}

		//
		return result;
	}*/

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createBinarization(final Net source, final String label, final String pattern) throws PuckException {
		Partition<Individual> result;

		result = create(source, PartitionCriteria.createBinarization(label, null, pattern));

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createBinarization(final Net source, final String label, final String labelParameter, final String pattern)
			throws PuckException {
		Partition<Individual> result;

		result = create(source, PartitionCriteria.createBinarization(label, labelParameter, pattern));

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createBinarization(final String title, final Individuals source, final String label, final String pattern, final Geography geography)
			throws PuckException {
		Partition<Individual> result;

		result = create(title, source, PartitionCriteria.createBinarization(label, null, pattern),geography);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createBinarization(final String title, final Individuals source, final String label, final String labelParameter,
			final String pattern, final Geography geography) throws PuckException {
		Partition<Individual> result;

		result = create(title, source, PartitionCriteria.createBinarization(label, labelParameter, pattern), geography);

		//
		return result;
	}

	/**
	 * Copy a new partition removing cluster with filtered value.
	 * 
	 * @param source
	 *            The partition to copy.
	 * @param filter
	 *            The filter of value to remove.
	 * 
	 * @return A copy of the partition source without filtered value.
	 */
	public static <E> Partition<E> createCleaned(final Partition<E> source, final SizeFilter filter) {
		Partition<E> result;

		if (source == null) {
			result = null;
		} else if (filter == null) {
			result = createCopy(source);
		} else {
			switch (filter) {
				case NONE:
					result = createCopy(source);
				break;

				case TRIM: {
					//
					List<Cluster<E>> clusters = source.getClusters().toListSortedByValue();

					// Find the first cluster not empty.
					int startIndex = 0;
					{
						boolean ended = false;
						int index = 0;
						while (!ended) {
							if (index < clusters.size()) {
								Cluster<E> cluster = clusters.get(index);
								if (cluster.isNotEmpty()) {
									ended = true;
									startIndex = index;
								} else {
									index += 1;
								}
							} else {
								ended = true;
								startIndex = 0;
							}
						}
					}

					// Find the last cluster non empty.
					int endIndex = 0;
					{
						boolean ended = false;
						int index = clusters.size() - 1;
						while (!ended) {
							if (index >= 0) {
								Cluster<E> cluster = clusters.get(index);
								if (cluster.isNotEmpty()) {
									ended = true;
									endIndex = index;
								} else {
									index += 1;
								}
							} else {
								ended = true;
								endIndex = 0;
							}
						}
					}

					result = new Partition<E>(source.getLabel());
					for (int clusterIndex = startIndex; clusterIndex <= endIndex; clusterIndex++) {
						Cluster<E> cluster = clusters.get(clusterIndex);

						result.putAll(cluster.getItems(), cluster.getValue());
					}
				}
				break;

				case EMPTY:
					result = new Partition<E>(source.getLabel());
					for (Cluster<E> cluster : source.getClusters()) {
						if (cluster.isNotEmpty()) {
							result.putAll(cluster.getItems(), cluster.getValue());
						}
					}
				break;

				case HOLES:
					if (source.getClusters().isNumericOrNull()) {
						//
						result = new Partition<E>(source.getLabel());

						//
						List<Cluster<E>> clusters = source.getClusters().toListSortedByValue();

						//
						Cluster<E> maxCluster = Collections.max(clusters);
						if (maxCluster.getValue() == null) {
							result = createCopy(source);
						} else {
							//
							int count = 0;
							for (int clusterIndex = 0; clusterIndex < clusters.size(); clusterIndex++) {
								//
								Cluster<E> cluster = clusters.get(clusterIndex);

								//
								if (cluster.getValue() == null) {
									result.putAll(cluster.getItems(), cluster.getValue());
								} else {
									//
									int currentCount = cluster.getValue().intValue();

									//
									while (count < currentCount) {
										result.putCluster(new Value(count));
										count += 1;
									}

									//
									result.putAll(cluster.getItems(), cluster.getValue());
									count += 1;
								}
							}
						}
					} else {
						result = createCopy(source);
					}
				break;

				default:
					result = createCopy(source);
			}
		}

		//
		return result;
	}

	/**
	 * Copy a new partition removing cluster with filtered value.
	 * 
	 * @param source
	 *            The partition to copy.
	 * @param filter
	 *            The filter of value to remove.
	 * 
	 * @return A copy of the partition source without filtered value.
	 */
	public static <E> Partition<E> createCleaned(final Partition<E> source, final ValueFilter filter) {
		Partition<E> result;

		if (source == null) {
			result = null;
		} else if (filter == null) {
			result = createCopy(source);
		} else {
			switch (filter) {
				case NONE:
					result = createCopy(source);
				break;

				case NULL:
					result = new Partition<E>(source.getLabel());
					for (Cluster<E> cluster : source.getClusters()) {
						if (cluster.getValue() != null) {
							result.putAll(cluster.getItems(), cluster.getValue());
						}
					}
				break;

				case ZERO:
					result = new Partition<E>(source.getLabel());
					for (Cluster<E> cluster : source.getClusters()) {
						Value value = cluster.getValue();
						if ((value != null) && ((value.isNotNumber()) || (value.doubleValue() != 0))) {
							result.putAll(cluster.getItems(), cluster.getValue());
						}
					}
				break;

				default:
					result = createCopy(source);
			}
		}

		//
		return result;
	}

	/**
	 * Copy a new partition.
	 * 
	 * @param source
	 *            The partition to copy.
	 * 
	 * @return A copy of the partition source.
	 */
	public static <E> Partition<E> createCopy(final Partition<E> source) {
		Partition<E> result;

		if (source == null) {
			result = null;
		} else {
			result = new Partition<E>(source.getLabel());
			for (Cluster<E> cluster : source.getClusters()) {
				result.putAll(cluster.getItems(), cluster.getValue());
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createCountedGrouping(final Net source, final String label, final Double start, final Double count, final Double end)
			throws PuckException {
		Partition<Individual> result;

		result = create(source, PartitionCriteria.createCountedGrouping(label, null, start, count, end));

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createCountedGrouping(final Net source, final String label, final String labelParameter, final Double start,
			final Double count, final Double end) throws PuckException {
		Partition<Individual> result;

		result = create(source, PartitionCriteria.createCountedGrouping(label, labelParameter, start, count, end));

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createCountedGrouping(final String title, final Individuals source, final String label, final Geography geography, final Double start,
			final Double count, final Double end) throws PuckException {
		Partition<Individual> result;

		result = create(title, source, PartitionCriteria.createCountedGrouping(label, null, start, count, end), geography);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createCountedGrouping(final String title, final Individuals source, final String label, final String labelParameter,
			final Geography geography, final Double start, final Double count, final Double end) throws PuckException {
		Partition<Individual> result;

		result = create(title, source, PartitionCriteria.createCountedGrouping(label, labelParameter, start, count, end), geography);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createGrouping(final Net source, final String label, final Intervals intervals) throws PuckException {
		Partition<Individual> result;

		result = create(source, PartitionCriteria.createGrouping(label, null, intervals));

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createGrouping(final Net source, final String label, final String steps) throws PuckException {
		Partition<Individual> result;

		result = create(source, PartitionCriteria.createGrouping(label, null, getIntervals(steps)));

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createGrouping(final String title, final Individuals source, final String label, final Intervals intervals, final Geography geography)
			throws PuckException {
		Partition<Individual> result;

		result = create(title, source, PartitionCriteria.createGrouping(label, null, intervals), geography);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createGrouping(final String title, final Individuals source, final String label, final String steps, final Geography geography)
			throws PuckException {
		Partition<Individual> result;

		result = create(title, source, PartitionCriteria.createGrouping(label, null, getIntervals(steps)), geography);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createGrouping(final String title, final Individuals source, final String label, final String labelParameter,
			final Intervals intervals, final Geography geography) throws PuckException {
		Partition<Individual> result;

		result = create(title, source, PartitionCriteria.createGrouping(label, labelParameter, intervals), geography);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createGrouping(final String title, final Individuals source, final String label, final String labelParameter,
			final String steps, final Geography geography) throws PuckException {
		Partition<Individual> result;

		result = create(title, source, PartitionCriteria.createGrouping(label, labelParameter, getIntervals(steps)), geography);

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public static <E> Map<Value,Integer> getPartitionNumbersMap (final Partition<E> source) {
		Map<Value,Integer> result;

		if (source == null || source.isNumeric()) {
			result = null;
		} else {
			result = new HashMap<Value,Integer>();

			List<Cluster<E>> clusterList = source.getClusters().toListSortedByValue();
			for (int i = 1; i <= clusterList.size(); i++) {
				Cluster<E> cluster = clusterList.get(i - 1);
				if (cluster.getValue()==null){
					result.put(cluster.getValue(), 0);
				} else {
					result.put(cluster.getValue(), i);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static <E> Partition<E> createNumerized(final Partition<E> source, final Map<Value,Integer> partitionNumbersMap) {
		Partition<E> result;

		if (source == null) {
			result = null;
		} else {
			
			if (source.isNumeric()) {
				result = source;
			} else {
				result = new Partition<E>();
				result.setLabel(source.getLabel());

				List<Cluster<E>> clusterList = source.getClusters().toListSortedByValue();
				for (int i = 1; i <= clusterList.size(); i++) {
					Value intValue = new Value(0);
					Cluster<E> cluster = clusterList.get(i - 1);
					if (cluster.getValue()!=null){
						if (partitionNumbersMap!=null){
							if (partitionNumbersMap.get(cluster.getValue())!=null){
								intValue = new Value(partitionNumbersMap.get(cluster.getValue()));
							} else {
								intValue = new Value(0);
							}
						} else {
							intValue = new Value(i);
						}
					}
					result.putAll(cluster.getItems(), intValue);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createRaw(final Net source, final String label) throws PuckException {
		Partition<Individual> result;

		result = create(source, PartitionCriteria.createRaw(label, null));

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createRaw(final Net source, final String label, final String labelParameter) throws PuckException {
		Partition<Individual> result;

		result = create(source, PartitionCriteria.createRaw(label, labelParameter));

		//
		return result;
	}

	/**
	 * 
	 * @param title
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createRaw(final String title, final Individuals source, final Geography geography) throws PuckException {
		return create(title, source, PartitionCriteria.createRaw(title), geography);
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createRaw(final String title, final Individuals source, final String label, final Geography geography) throws PuckException {
		Partition<Individual> result;

		result = create(title, source, PartitionCriteria.createRaw(label, null), geography);

		//
		return result;
	}
	
	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Relation> createRaw(final String title, final Relations source, final String label, final Geography geography) throws PuckException {
		Partition<Relation> result;

		result = create(title, source, PartitionCriteria.createRaw(label, null), geography);

		//
		return result;
	}


	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createRaw(final String title, final Individuals source, final String label, final String labelParameter, final Geography geography)
			throws PuckException {
		Partition<Individual> result;

		result = create(title, source, PartitionCriteria.createRaw(label, labelParameter), geography);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Relation> createRaw(final String title, final Relations source, final String label, final String labelParameter, final Geography geography)
			throws PuckException {
		Partition<Relation> result;

		result = create(title, source, PartitionCriteria.createRaw(label, labelParameter), geography);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createSizedGrouping(final Net source, final String label, final Double start, final Double size, final Double end)
			throws PuckException {
		Partition<Individual> result;

		result = create(source, PartitionCriteria.createSizedGrouping(label, null, start, size, end));

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createSizedGrouping(final Net source, final String label, final String labelParameter, final Double start,
			final Double size, final Double end) throws PuckException {
		Partition<Individual> result;

		result = create(source, PartitionCriteria.createSizedGrouping(label, labelParameter, start, size, end));

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createSizedGrouping(final String title, final Individuals source, final String label, final Geography geography, final Double start,
			final Double size, final Double end) throws PuckException {
		Partition<Individual> result;

		result = create(title, source, PartitionCriteria.createSizedGrouping(label, null, start, size, end), geography);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Partition<Individual> createSizedGrouping(final String title, final Individuals source, final String label, final String labelParameter,
			final Geography geography, final Double start, final Double size, final Double end) throws PuckException {
		Partition<Individual> result;

		result = create(title, source, PartitionCriteria.createSizedGrouping(label, labelParameter, start, size, end), geography);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public static Value getBinarizedValue(final Value sourceValue, final String pattern) {
		Value result;

		//
		//
		if (sourceValue == null) {
			result = null;
		} else if (StringUtils.isEmpty(pattern)) {
			result = new Value("nonnull");
		} else {

			String targetPattern = pattern.replaceAll("\\*", ".*").replaceAll("\\+", "PLUS").replaceAll("\\-", "MINUS");
			String sourcePattern = sourceValue.stringValue().replaceAll("\\*", ".*").replaceAll("\\+", "PLUS").replaceAll("\\-", "MINUS");
			
			if (sourcePattern.matches(targetPattern)) {
				result = new Value(pattern);
			} else {
				result = null;
			}
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public static Value getPartializedValue(final Value sourceValue) {
		Value result;

		//
		if (sourceValue == null) {
			result = null;
		} else {
			List<String> list = new ArrayList<String>();
			String string = sourceValue.stringValue().replace("[", "").replace("]", "");
			
			String[] partializedString = string.split(",");
			String trunkString = "[";
			
			for (int i=0;i<partializedString.length;i++) {
				String partialString = trunkString+partializedString[i];
				trunkString = partialString+", ";
				list.add(partialString+"]");
			}
			//
			result = new Value(list);
		}

		//
		return result;
	}
	

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public static NumberedValues getBinarizedValues(final NumberedValues source, final String pattern) {
		NumberedValues result;

		if (source == null) {
			result = null;
		} else if (pattern == null) {
			result = source;
		} else {
			//
			result = new NumberedValues(source.size());
			for (Integer id : source.keySet()) {
				Value sourceValue = source.get(id);
				Value targetValue = getBinarizedValue(sourceValue, pattern);
				result.put(id, targetValue);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public static Values getBinarizedValues(final Values source, final String pattern) {
		Values result;

		if (source == null) {
			result = null;
		} else if (pattern == null) {
			result = source;
		} else {
			//
			result = new Values(source.size());
			for (Value sourceValue : source) {
				result.add(getBinarizedValue(sourceValue, pattern));
			}
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public static Values getPartializedValues(final Values source) {
		Values result;

		if (source == null) {
			result = null;
		} else {
			//
			result = new Values(source.size());
			for (Value sourceValue : source) {
				result.add(getPartializedValue(sourceValue));
			}
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public static NumberedValues getPartializedValues(final NumberedValues source) {
		NumberedValues result;

		if (source == null) {
			result = null;
		} else {
			//
			result = new NumberedValues(source.size());
			for (Integer id : source.keySet()) {
				Value sourceValue = source.get(id);
				Value targetValue = getPartializedValue(sourceValue);
				result.put(id, targetValue);
			}
		}

		//
		return result;
	}


	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public static Value getGroupingValue(final Value sourceValue, final Intervals intervals) {
		Value result;

		//
		if ((sourceValue == null) || (sourceValue.isNotNumber())) {
			result = null;
		} else {
			Interval interval = intervals.find(sourceValue.doubleValue());
			if (interval == null) {
				result = null;
			} else {
				result = new Value(interval);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public static NumberedValues getGroupingValues(final NumberedValues source, final Intervals intervals) {
		NumberedValues result;

		if (source == null) {
			result = null;
		} else if (intervals == null) {
			result = source;
		} else {
			//
			result = new NumberedValues(source.size());
			for (Integer id : source.keySet()) {
				Value sourceValue = source.get(id);
				Value targetValue = getGroupingValue(sourceValue, intervals);
				result.put(id, targetValue);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 * @throws PuckException
	 */
	public static Values getGroupingValues(final Values source, final Intervals intervals) {
		Values result;

		if (source == null) {
			result = null;
		} else if (intervals == null) {
			result = source;
		} else {
			//
			result = new Values(source.size());
			for (Value sourceValue : source) {
				result.add(getGroupingValue(sourceValue, intervals));
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param steps
	 * @return
	 */
	public static Intervals getIntervals(final String steps) {
		Intervals result;

		//
		result = new Intervals();

		//
		if (StringUtils.isNotBlank(steps)) {
			//
			List<Double> values = new ArrayList<Double>();
			String[] strings = steps.split("[\\s]+");
			for (String string : strings) {
				if (NumberUtils.isNumber(string)) {
					values.add(Double.parseDouble(string));
				}
			}

			//
			Collections.sort(values);

			//
			if (values.size() == 1) {
				Interval interval = new Interval(values.get(0), EndpointStatus.INCLUDED, values.get(0), EndpointStatus.INCLUDED);
				result.add(interval);
			} else if (values.size() > 1) {
				for (int index = 0; index < values.size() - 1; index++) {
					Interval interval;
					if (index == values.size() - 1) {
						interval = new Interval(values.get(index), EndpointStatus.INCLUDED, values.get(index + 1), EndpointStatus.INCLUDED);
					} else {
						interval = new Interval(values.get(index), EndpointStatus.INCLUDED, values.get(index + 1), EndpointStatus.EXCLUDED);
					}
					result.add(interval);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param start
	 * @param size
	 * @param end
	 * @return
	 * @throws PuckException
	 */
	public static Intervals getIntervalsByCount(final Double start, final Double size, final Double end) throws PuckException {
		Intervals result;

		result = getIntervalsByCount(start, size, end, MIN_VALUE, MAX_VALUE);

		//
		return result;
	}

	/**
	 * This method splits an interval to a count of intervals and add
	 * before/after intervals to contains all provided values.
	 * 
	 * If source interval is undefined then none interval will be generated.
	 * 
	 * It is required that min and max values are both null or both not null.
	 * Otherwise, parameters are considered as inconsistent and an exception is
	 * throw.
	 * 
	 * Default count value is set to 1.
	 * 
	 * Each parameter can take null value or non null value. So there is 2â�µ (32)
	 * cases:
	 * 
	 * <pre>
	 * index	start	count	end 	min 	max 	result
	 * 1	null	null	null	null	null	byCount(start, 1, end, min, max)
	 * 2	null	null	null	null	max 	byCount(start, 1, end, min, max)
	 * 3	null	null	null	min 	null	byCount(start, 1, end, min, max)
	 * 4	null	null	null	min 	max 	byCount(start, 1, end, min, max)
	 * 5	start	null	null	null	null	byCount(start, 1, end, min, max)
	 * 6	start	null	null	null	max 	byCount(start, 1, end, min, max)
	 * 7	start	null	null	min 	null	byCount(start, 1, end, min, max)
	 * 8	start	null	null	min 	max 	byCount(start, 1, end, min, max)
	 * 9	null	null	end 	null	null	byCount(start, 1, end, min, max)
	 * 10	null	null	end 	null	max 	byCount(start, 1, end, min, max)
	 * 11	null	null	end 	min 	null	byCount(start, 1, end, min, max)
	 * 12	null	null	end 	min 	max 	byCount(start, 1, end, min, max)
	 * 13	start	null	end 	null	null	byCount(start, 1, end, min, max)
	 * 14	start	null	end 	null	max 	byCount(start, 1, end, min, max)
	 * 15	start	null	end 	min 	null	byCount(start, 1, end, min, max)
	 * 16	start	null	end 	min 	max 	byCount(start, 1, end, min, max)
	 * 17	null	count	null	null	null	empty
	 * 18	null	count	null	null	max 	inconsistent min/max => error
	 * 19	null	count	null	min 	null	inconsistent min/max => error
	 * 20	null	count	null	min 	max 	bySize(start, size, max, min, max)
	 * 21	start	count	null	null	null	empty
	 * 22	start	count	null	null	max 	inconsistent min/max => error
	 * 23	start	count	null	min 	null	inconsistent min/max => error
	 * 24	start	count	null	min 	max 	bySize(start, size, end, min, max) + 1 opt.
	 * 25	null	count	end 	null	null	empty
	 * 26	null	count	end 	null	max 	inconsistent min/max => error
	 * 27	null	count	end 	min 	null	inconsistent min/max => error
	 * 28	null	count	end 	min 	max 	bySize(min, size, max/end) + 1 opt.
	 * 29	start	count	end 	null	null	bySize(start, size, end)
	 * 30	start	count	end 	null	max 	inconsistent min/max => error
	 * 31	start	count	end 	min 	null	inconsistent min/max => error
	 * 32	start	count	end 	min 	max 	bySize(start, size, end) + 2 opt.
	 * </pre>
	 * 
	 * 
	 * @param start
	 *            the start value of interval generation.
	 * @param count
	 *            the count of targeted intervals.
	 * @param end
	 *            the end value of interval generation.
	 * @param min
	 *            the minimum value provided to be placed in intervals.
	 * @param max
	 *            the maximum value provided to be placed in intervals.
	 * 
	 * @return an interval list.
	 * 
	 * @throws PuckException
	 *             if interval count in too large or if count value is zero.
	 */
	public static Intervals getIntervalsByCount(final Double start, final Double count, final Double end, final Double min, final Double max)
			throws PuckException {
		Intervals result;
		// logger.debug("start=" + start + ", count=" + count + ", end=" + end +
		// ", min=" + min + ", max=" + max);

		if (count == null) {
			//
			result = getIntervalsByCount(start, 1.0, end, min, max);

		} else if (count <= 0) {
			//
			throw new IllegalArgumentException("Illegal count value (zero or negative).");

		} else if ((min == null) && (max == null)) {
			//
			result = new Intervals();

		} else if ((min != null) && (max != null) && (min > max)) {
			//
			result = getIntervalsByCount(start, count, end, max, min);

		} else if ((start != null) && (end != null) && (start > end)) {
			//
			result = getIntervalsByCount(end, count, start, min, max);

		} else if ((min == null) && (max != null)) {
			//
			throw new IllegalArgumentException("Undefined min value is impossible if max value is defined.");

		} else if ((min != null) && (max == null)) {
			//
			throw new IllegalArgumentException("Undefined max value is impossible if min value is defined.");

		} else if ((start == null) && (min == null) && (end == null) && (max == null)) {
			//
			result = new Intervals();

		} else if ((start == null) && (min != null) && (end == null) && (max != null)) {
			//
			result = getIntervalsBySize(min, (max - min) / count, max);

		} else if ((start != null) && (end == null) && (min == null) && (max == null)) {
			//
			result = new Intervals();

		} else if ((start == null) && (end != null) && (min == null) && (max == null)) {
			//
			result = new Intervals();

		} else {
			//
			double targetStart;
			if (start == null) {
				//
				targetStart = min;

			} else {
				//
				targetStart = start;
			}

			//
			double targetEnd;
			if (end == null) {
				//
				targetEnd = max;

			} else {
				//
				targetEnd = end;
			}

			//
			double targetSize;
			if ((MathUtils.isDecimalInteger(targetStart)) && (MathUtils.isDecimalInteger(targetEnd))) {
				//
				targetSize = Math.abs(Math.ceil((targetEnd - targetStart) / count));

			} else {
				//
				targetSize = (targetEnd - targetStart) / count;
			}

			//
			result = getIntervalsBySize(targetStart, targetSize, targetEnd, min, max);
		}

		//
		return result;
	}

	/**
	 * This method splits an interval to smaller fixed size intervals.
	 * 
	 * @param start
	 *            the first value of the first interval.
	 * @param size
	 *            the size of each interval.
	 * @param end
	 *            the last value of the last interval.
	 * 
	 * @return an interval list.
	 */
	public static Intervals getIntervalsBySize(final double start, final double size, final double end) {
		Intervals result;

		// logger.debug("targetStart=" + targetStart + ", targetSize=" +
		// targetSize + ", targetEnd=" + targetEnd);

		//
		result = new Intervals();

		//
		if (start < end) {
			//
			for (double value = start; value < end; value += size) {
				//
				Interval interval;
				if (value + size < end) {
					//
					interval = new Interval(value, EndpointStatus.INCLUDED, value + size, EndpointStatus.EXCLUDED);

				} else {
					//
					interval = new Interval(value, EndpointStatus.INCLUDED, end, EndpointStatus.INCLUDED);
				}
				result.add(interval);
			}

		} else if (start > end) {
			//
			for (double value = end; value > start; value -= size) {
				//
				Interval interval;
				if (value - size > start) {
					//
					interval = new Interval(value - size, EndpointStatus.INCLUDED, value, EndpointStatus.EXCLUDED);

				} else {
					//
					interval = new Interval(start, EndpointStatus.INCLUDED, value, EndpointStatus.INCLUDED);
				}
				result.add(interval);
			}
			Collections.reverse(result);

		} else {
			//
			Interval interval = new Interval(start, EndpointStatus.INCLUDED, end, EndpointStatus.INCLUDED);
			result.add(interval);
		}

		//
		return result;
	}

	/**
	 * This method splits an interval to smaller fixed size intervals and add
	 * before/after intervals to contains all provided values.
	 * 
	 * If source interval is undefined then none interval will be generated.
	 * 
	 * It is required that min and max values are both null or both not null.
	 * Otherwise, parameters are considered as inconsistent and an exception is
	 * throw.
	 * 
	 * Default size value is set to 1.
	 * 
	 * If <code>min</code> value is lesser than <code>start</code> then an
	 * interval [min, start[ is appended.
	 * 
	 * If <code>max</code> value is greater than <code>end</code> then an
	 * interval ]end, start[ is appended.
	 * 
	 * 
	 * Each parameter can take null value or non null value. So there is 2â�µ (32)
	 * cases:
	 * 
	 * <pre>
	 * index	start	size	end 	min 	max 	result
	 * 1	null	null	null	null	null	bySize(start, 1, end, min, max)
	 * 2	null	null	null	null	max 	bySize(start, 1, end, min, max)
	 * 3	null	null	null	min 	null	bySize(start, 1, end, min, max)
	 * 4	null	null	null	min 	max 	bySize(start, 1, end, min, max)
	 * 5	start	null	null	null	null	bySize(start, 1, end, min, max)
	 * 6	start	null	null	null	max 	bySize(start, 1, end, min, max)
	 * 7	start	null	null	min 	null	bySize(start, 1, end, min, max)
	 * 8	start	null	null	min 	max 	bySize(start, 1, end, min, max)
	 * 9	null	null	end 	null	null	bySize(start, 1, end, min, max)
	 * 10	null	null	end 	null	max 	bySize(start, 1, end, min, max)
	 * 11	null	null	end 	min 	null	bySize(start, 1, end, min, max)
	 * 12	null	null	end 	min 	max 	bySize(start, 1, end, min, max)
	 * 13	start	null	end 	null	null	bySize(start, 1, end, min, max)
	 * 14	start	null	end 	null	max 	bySize(start, 1, end, min, max)
	 * 15	start	null	end 	min 	null	bySize(start, 1, end, min, max)
	 * 16	start	null	end 	min 	max 	bySize(start, 1, end, min, max)
	 * 17	null	size	null	null	null	empty
	 * 18	null	size	null	null	max 	inconsistent min/max => error
	 * 19	null	size	null	min 	null	inconsistent min/max => error
	 * 20	null	size	null	min 	max 	bySize(min, size, max)
	 * 21	start	size	null	null	null	1
	 * 22	start	size	null	null	max 	inconsistent min/max => error
	 * 23	start	size	null	min 	null	inconsistent min/max => error
	 * 24	start	size	null	min 	max 	bySize(start|min, size, end) + 1 opt.
	 * 25	null	size	end 	null	null	1
	 * 26	null	size	end 	null	max 	inconsistent min/max => error
	 * 27	null	size	end 	min 	null	inconsistent min/max => error
	 * 28	null	size	end 	min 	max 	bySize(min, size, max|end) + 1 opt.
	 * 29	start	size	end 	null	null	bySize(start, size, end)
	 * 30	start	size	end 	null	max 	inconsistent min/max => error
	 * 31	start	size	end 	min 	null	inconsistent min/max => error
	 * 32	start	size	end 	min 	max 	bySize(start, size, end) + 2 opt.
	 * </pre>
	 * 
	 * @param start
	 *            the start value of interval generation.
	 * @param size
	 *            the size of each interval.
	 * @param end
	 *            the end value of interval generation.
	 * @param min
	 *            the minimum value that must be placed in intervals.
	 * @param max
	 *            the maximum value that must be placed in intervals.
	 * 
	 * @return an interval list.
	 * 
	 * @throws PuckException
	 *             if interval count in too large or if size value is zero.
	 * 
	 */
	public static Intervals getIntervalsBySize(final Double start, final Double size, final Double end, final Double min, final Double max)
			throws PuckException {
		Intervals result;
		// logger.debug("start=" + start + ", size=" + size + ", end=" + end +
		// ", min=" + min + ", max=" + max);

		if (size == null) {
			//
			result = getIntervalsBySize(start, 1.0, end, min, max);

		} else if (size <= 0) {
			//
			throw new IllegalArgumentException("Illegal size value (negative or zero).");

		} else if ((min != null) && (max != null) && (min > max)) {
			//
			result = getIntervalsBySize(start, size, end, max, min);

		} else if ((start != null) && (end != null) && (start > end)) {
			//
			result = getIntervalsBySize(end, size, start, min, max);

		} else if ((min == null) && (max != null)) {
			//
			throw new IllegalArgumentException("Undefined min value is impossible if max value is defined.");

		} else if ((min != null) && (max == null)) {
			//
			throw new IllegalArgumentException("Undefined max value is impossible if min value is defined.");

		} else if ((start == null) && (min == null) && (end == null) && (max == null)) {
			//
			result = new Intervals();

		} else if ((start == null) && (min != null) && (end == null) && (max != null)) {
			//
			result = getIntervalsBySize(min, size, max);

		} else if ((start != null) && (end == null) && (min == null) && (max == null)) {
			//
			result = new Intervals();

			result.add(new Interval(start, EndpointStatus.INCLUDED, start + size, EndpointStatus.EXCLUDED));

		} else if ((start == null) && (end != null) && (min == null) && (max == null)) {
			//
			result = new Intervals();

			result.add(new Interval(end - size, EndpointStatus.INCLUDED, end, EndpointStatus.EXCLUDED));

		} else {
			// At this point, start and min can not both be null.
			double targetStart;
			if (start == null) {
				//
				targetStart = min;

			} else {
				//
				targetStart = start;
			}

			// At this point, end and max can not both be null.
			double targetEnd;
			if (end == null) {
				//
				targetEnd = max;

			} else {
				//
				targetEnd = end;
			}

			//
			double intervalCount = (targetEnd - targetStart) / size;

			//
			if (intervalCount > MAX_INTERVALS) {
				//
				throw PuckExceptions.OVERFLOW.create("Too numerous intervals: " + intervalCount + " > " + MAX_INTERVALS + ".");

			} else {
				// logger.debug("targetStart=" + targetStart + ", targetSize=" +
				// targetSize + ", targetEnd=" + targetEnd);

				//
				result = getIntervalsBySize(targetStart, size, targetEnd);

				//
				if ((min != null) && (start != null) && (min < start)) {
					//
					result.add(new Interval(min, EndpointStatus.INCLUDED, start, EndpointStatus.EXCLUDED));
				}

				//
				if ((max != null) && (end != null) && (max > end)) {
					//
					result.add(new Interval(end, EndpointStatus.EXCLUDED, max, EndpointStatus.INCLUDED));
				}
			}
		}

		// logger.debug("======" + result.toString());

		//
		return result;
	}
	
	/**
	 * 
	 * @param values
	 * @param criteria
	 * @return
	 * @throws PuckException
	 */
	public static NumberedValues getPartitionValues(final NumberedValues values, final PartitionCriteria criteria) throws PuckException {
		NumberedValues result;

		if (values == null) {
			result = null;
		} else if (criteria == null) {
			result = values;
		} else {
			//
			switch (criteria.getType()) {
				case RAW: {
					result = values;
				}
				break;

				case BINARIZATION: {
					result = getBinarizedValues(values, criteria.getPattern());
				}
				break;

				case FREE_GROUPING: {
					result = getGroupingValues(values, criteria.getIntervals());
				}
				break;

				case COUNTED_GROUPING: {
					Intervals intervals = getIntervalsByCount(criteria.getStart(), criteria.getCount(), criteria.getEnd(), values.min(), values.max());
					result = getGroupingValues(values, intervals);
				}
				break;

				case SIZED_GROUPING: {
					Intervals intervals = getIntervalsBySize(criteria.getStart(), criteria.getSize(), criteria.getEnd(), values.min(), values.max());
					result = getGroupingValues(values, intervals);
				}
				break;

				case PARTIALIZATION: {
					result = getPartializedValues(values);
				}
				break;

				default:
					result = new NumberedValues();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param valuesByHouses
	 * @param criteria
	 * @return
	 * @throws PuckException
	 */
	public static Values getPartitionValues(final Values source, final PartitionCriteria criteria) throws PuckException {
		Values result;

		if (source == null) {
			result = null;
		} else if (criteria == null) {
			result = source;
		} else {
			//
			switch (criteria.getType()) {
				case RAW: {
					result = source;
				}
				break;

				case BINARIZATION: {
					result = getBinarizedValues(source, criteria.getPattern());
				}
				break;

				case FREE_GROUPING: {
					result = getGroupingValues(source, criteria.getIntervals());
				}
				break;

				case COUNTED_GROUPING: {
					Intervals intervals = getIntervalsByCount(criteria.getStart(), criteria.getCount(), criteria.getEnd(), source.min(), source.max());
					result = getGroupingValues(source, intervals);
				}
				break;

				case SIZED_GROUPING: {
					Intervals intervals = getIntervalsBySize(criteria.getStart(), criteria.getSize(), criteria.getEnd(), source.min(), source.max());
					result = getGroupingValues(source, intervals);
				}
				break;

				case PARTIALIZATION: {
					result = getPartializedValues(source);
				}
				break;

				default:
					result = new Values();
			}
		}

		//
		return result;
	}

}
