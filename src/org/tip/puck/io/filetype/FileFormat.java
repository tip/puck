package org.tip.puck.io.filetype;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DRAFT
 * 
 * @author TIP
 * 
 */
public class FileFormat {

	public enum Format {
		DAT,
		BAR_ODS,
		BAR_TXT,
		BAR_XLS,
		IUR_ODS,
		IUR_TXT,
		IUR_XLS,
		GEDCOM,
		PAJEK,
		PROLOG,
		PUC,
		SELZ_ODS,
		SELZ_TXT,
		SELZ_XLS,
		TIP,
		UNKNOWN
	}

	private static final Logger logger = LoggerFactory.getLogger(FileFormat.class);

	private List<Format> formats;

	/**
	 * 
	 */
	public FileFormat() {
		this.formats = new ArrayList<Format>();
	}

	/**
	 * 
	 * @param format
	 */
	public void addFormat(final Format format) {
		if (format != null) {
			if (!this.formats.contains(format)) {
				this.formats.add(format);
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	public Format format() {
		Format result;

		switch (this.formats.size()) {
			case 0:
				result = Format.UNKNOWN;
			break;
			case 1:
				result = this.formats.get(0);
			break;
			default:
				throw new IllegalAccessError("Format is multiple.");
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Format> formats() {
		List<Format> result;

		result = this.formats;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isSingle() {
		boolean result;

		if (this.formats.size() == 1) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isUnknown() {
		boolean result;

		if (this.formats.isEmpty()) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param format
	 */
	public void setFormat(final Format format) {
		//
		this.formats.clear();

		//
		if ((format != null) || (format != Format.UNKNOWN)) {
			//
			this.formats.add(format);
		}
	}
}
