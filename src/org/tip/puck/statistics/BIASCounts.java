package org.tip.puck.statistics;

import java.util.ArrayList;

/**
 * 
 * @author TIP
 */
public class BIASCounts extends ArrayList<BIASCount> {

	private static final long serialVersionUID = 3076624104856435334L;

	/**
	 * 
	 */
	public BIASCounts() {
		super();
	}

	/**
	 * 
	 * @param capacity
	 */
	public BIASCounts(final int capacity) {
		super(capacity);
	}

	/**
	 * 
	 * @param capacity
	 */
	public BIASCounts(final int capacity, final int filledCapacity) {
		super(capacity);

		// Fill array.
		while (capacity >= this.size()) {
			this.add(new BIASCount());
		}
	}
	
	public void add (BIASCounts counts){
		for (int i=0;i<size();i++){
			get(i).add(counts.get(i));
		}
	}
	
	public void divide (double factor){
		for (BIASCount count : this){
			count.setAgnatic(count.getAgnatic()/factor);
			count.setUterine(count.getUterine()/factor);
			count.setCognatic(count.getCognatic()/factor);
			count.setCoo(count.getCoo()/factor);
		}
	}

	/**
	 * 
	 * @return
	 */
	public int agnaticSum() {
		int result;

		result = 0;
		for (BIASCount count : this) {
			result += count.getAgnatic();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int cognaticSum() {
		int result;

		result = 0;
		for (BIASCount count : this) {
			result += count.getCognatic();
		}

		//
		return result;
	}

	/**
	 * @return
	 * 
	 */
	@Override
	public BIASCount get(final int index) {
		BIASCount result;

		// Fill array.
		while (index >= this.size()) {
			this.add(new BIASCount());
		}

		//
		result = super.get(index);

		//
		return result;
	}

	/**
	 * @return
	 * 
	 */
	@Override
	public BIASCount set(final int index, final BIASCount value) {
		BIASCount result;

		// Fill array.
		while (index >= this.size()) {
			this.add(new BIASCount());
		}

		//
		result = super.set(index, value);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public BIASCount sum() {
		BIASCount result;

		result = new BIASCount();
		for (BIASCount count : this) {
			result.addCoo(count.getCoo());
			result.addAgnatic(count.getAgnatic());
			result.addCognatic(count.getCognatic());
			result.addUterine(count.getUterine());
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int uterineSum() {
		int result;

		result = 0;
		for (BIASCount count : this) {
			result += count.getUterine();
		}

		//
		return result;
	}
}
