package org.tip.puck.net.random;

import java.util.Map;
import java.util.Set;

import org.tip.puck.PuckException;
import org.tip.puck.census.workers.CensusCriteria;
import org.tip.puck.census.workers.CircuitFinder;
import org.tip.puck.matrix.MatrixStatistics.Indicator;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.net.workers.MemoryCriteria;
import org.tip.puck.report.Report;
import org.tip.puck.report.ReportChart;
import org.tip.puck.report.ReportChart.GraphType;
import org.tip.puck.report.ReportTable;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.statistics.BIASCount;
import org.tip.puck.statistics.BIASCounts;
import org.tip.puck.statistics.StatisticsReporter;
import org.tip.puck.statistics.StatisticsWorker;
import org.tip.puck.util.Chronometer;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.Value;
import org.tip.puckgui.views.RandomCorpusCriteria;
import org.tip.puckgui.views.mas.AgnaticCousinsWeightFactor;
import org.tip.puckgui.views.mas.Divorce2WeightFactor;
import org.tip.puckgui.views.mas.WeightFactor;

public class RandomNetReporter {
	
	public static Report ReportMASVariation () throws PuckException{
		
		Report result;

		//
		Chronometer chrono = new Chronometer();

		//
		result = new Report("Random Net Variation by observer");
		result.setOrigin("RandomNetReporter");
//		result.setTarget("");
		
		CensusCriteria censusCriteria = new CensusCriteria();
		censusCriteria.setPattern("XH(X)HX");
		
		int n1 = 6;
		int n2 = 6;
		int runs = 10;
		
		double agnaticCousinWeightInitialValue = 1.;
		double agnaticCousinWeightIntervalFactor = 10;
		double polygynyWeightInitialValue = 0.;
		double polygynyWeightIntervalFactor = 0.2;
		
		double[][] circuitCounts = new double[n1][n2];
		double[][] marriageCounts = new double[n1][n2];
		double[][] circuitPercentageCounts = new double[n1][n2];
		
		RandomCorpusCriteria criteria = new RandomCorpusCriteria();
		criteria.weightFactors().add(new AgnaticCousinsWeightFactor());
		((AgnaticCousinsWeightFactor)criteria.getWeightFactor(WeightFactor.Type.AGNATICCOUSINS)).setFirst(agnaticCousinWeightInitialValue);

		criteria.weightFactors().add(new Divorce2WeightFactor());
		((Divorce2WeightFactor)criteria.getWeightFactor(WeightFactor.Type.DIVORCE2)).setMaleProbability(polygynyWeightInitialValue);

		
		criteria.setMas(true);

		//
		result.inputs().add("Number of runs by combination", runs);

		for (int i = 0; i < n1; i++) {
			
			double agnaticCousinWeight = ((AgnaticCousinsWeightFactor)criteria.getWeightFactor(WeightFactor.Type.AGNATICCOUSINS)).getFirst();
			
			for (int j = 0; j < n2; j++) {
				
				double polygynyWeight = ((Divorce2WeightFactor)criteria.getWeightFactor(WeightFactor.Type.DIVORCE2)).getMaleProbability();
						
				System.out.println("Agnatic cousin preference weight "+agnaticCousinWeight+ "\tPolygyny Weight "+ polygynyWeight);

				double circuits = 0.;
				double marriages = 0.;
				double circuitPercentages = 0.;
				
				for (int run=0; run<runs; run++){
					RandomNetMaker randomNetMaker = new RandomNetMaker(criteria);
					Net net = randomNetMaker.createRandomMASNet();
					
					CircuitFinder finder = new CircuitFinder(new Segmentation(net),censusCriteria);
					finder.findCircuits();
					finder.count();
					circuits += finder.getNrCircuits(0);
					marriages += finder.getCouplesConsidered();
					circuitPercentages += MathUtils.percent(circuits, marriages);
				}
				
				circuits = circuits/runs;
				marriages = marriages/runs;
				circuitPercentages = circuitPercentages/runs;
				
				circuitCounts[i][j] = circuits;
				marriageCounts[i][j] = marriages;
				circuitPercentageCounts[i][j] = circuitPercentages;
				System.out.println(circuits);

				((Divorce2WeightFactor)criteria.getWeightFactor(WeightFactor.Type.DIVORCE2)).setMaleProbability(polygynyWeight + polygynyWeightIntervalFactor);
			}
			((AgnaticCousinsWeightFactor)criteria.getWeightFactor(WeightFactor.Type.AGNATICCOUSINS)).setFirst(agnaticCousinWeight * agnaticCousinWeightIntervalFactor);
			((Divorce2WeightFactor)criteria.getWeightFactor(WeightFactor.Type.DIVORCE2)).setMaleProbability(polygynyWeightInitialValue);
		}
		
		//
		result.outputs().appendln("Agnatic cousn marriages");
		//
		ReportChart chartCircuits = new ReportChart("Agnatic cousin marriages", GraphType.SURFACE);
		//
		double v1 = agnaticCousinWeightInitialValue;
		double v2 = polygynyWeightInitialValue;
		for (int j = 0; j < n2; j++) {
			chartCircuits.setHeader(String.valueOf(v2), j);
			v2 = v2 + polygynyWeightIntervalFactor;
		}

		for (int i = 0; i < n1; i++) {
			chartCircuits.setLineTitle(String.valueOf(v1), i);
			//
			for (int j = 0; j < n2; j++) {
				//
				chartCircuits.setValue(MathUtils.round(circuitCounts[i][j], 4), i, j);
			}
			v1 = v1 * agnaticCousinWeightIntervalFactor;
		}
		result.outputs().append(chartCircuits);

		//
		ReportTable tableCircuits = chartCircuits.createReportTable();
		tableCircuits.set(0, 0, "prefernce/polygyny");
		result.outputs().appendln(tableCircuits);
		result.outputs().appendln();
		//
		
		//
		result.outputs().appendln("Total marriages");
		//
		ReportChart chartMarriages = new ReportChart("Total marriages", GraphType.SURFACE);
		//
		v1 = agnaticCousinWeightInitialValue;
		v2 = polygynyWeightInitialValue;
		for (int j = 0; j < n2; j++) {
			chartMarriages.setHeader(String.valueOf(v2), j);
			v2 = v2 + polygynyWeightIntervalFactor;
		}

		for (int i = 0; i < n1; i++) {
			chartMarriages.setLineTitle(String.valueOf(v1), i);
			//
			for (int j = 0; j < n2; j++) {
				//
				chartMarriages.setValue(MathUtils.round(marriageCounts[i][j], 4), i, j);
			}
			v1 = v1 * agnaticCousinWeightIntervalFactor;
		}
		result.outputs().append(chartMarriages);

		//
		ReportTable tableMarriages = chartMarriages.createReportTable();
		tableMarriages.set(0, 0, "prefernce/polygyny");
		result.outputs().appendln(tableMarriages);
		result.outputs().appendln();
		//
		
		//
		result.outputs().appendln("Agnatic cousin marriages (%");
		//
		ReportChart chartCircuitPecentages = new ReportChart("Agnatic cousin marriages (%)", GraphType.SURFACE);
		//
		v1 = agnaticCousinWeightInitialValue;
		v2 = polygynyWeightInitialValue;
		for (int j = 0; j < n2; j++) {
			chartCircuitPecentages.setHeader(String.valueOf(v2), j);
			v2 = v2 + polygynyWeightIntervalFactor;
		}

		for (int i = 0; i < n1; i++) {
			chartCircuitPecentages.setLineTitle(String.valueOf(v1), i);
			//
			for (int j = 0; j < n2; j++) {
				//
				chartCircuitPecentages.setValue(MathUtils.round(circuitPercentageCounts[i][j], 4), i, j);
			}
			v1 = v1 * agnaticCousinWeightIntervalFactor;
		}
		result.outputs().append(chartCircuitPecentages);

		//
		ReportTable tableCircuitPercentages = chartCircuitPecentages.createReportTable();
		tableCircuitPercentages.set(0, 0, "prefernce/polygyny");
		result.outputs().appendln(tableCircuitPercentages);
		result.outputs().appendln();
		//
		
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;

		
	}
	
	/**
	 * 
	 * @param chartTitle
	 * @param chartType
	 * @param initialValue
	 * @param intervalFactor
	 * @param indicator
	 * @return
	 */
	public static ReportChart buildChartFromTable(final String chartTitle, final GraphType chartType, final double[][] source,
			final double initialValue, final double intervalFactor, final Indicator indicator) {
		ReportChart result;

		//
		result = new ReportChart(chartTitle, chartType);
		
		//
		int iMax = source.length;
		int jMax;
		if (source.length == 0) {
			jMax = 0;
		} else {
			jMax = source[0].length;
		}

		//
		double v1 = initialValue;
		for (int i = 0; i < iMax; i++) {
			result.setLineTitle(String.valueOf(MathUtils.round(v1, 8)), i);
			result.setHeader(String.valueOf(v1), i);

			//
			for (int j = 0; j < jMax; j++) {
				//
				result.setValue(MathUtils.round(source[i][j], 4), i, j);
			}

			// Increment first variable for next iteration step.
			v1 = v1 * intervalFactor;
		}

		//
		return result;
	}
	
	/**
	 * Synonym: reportRandomAllianceNetworkByObserverSimulationVariations.
	 * 
	 * @param criteria
	 * @return
	 * @throws PuckException 
	 */
	public static <E> Report reportVirtualFieldworkVariations(final Segmentation source) throws PuckException {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		//
		result = new Report("Random Net Variation by observer");
		result.setOrigin("RandomNetReporter");
		result.setTarget(source.getLabel());
		

		int n1 = 8;
		int n2 = 6;
		double[][] biasCounts = new double[n1][n2];
		Double[][][] agnaticClosureRates = new Double[n1][n2][3];
		Double[][][] uterineClosureRates = new Double[n1][n2][3];
		double[][][] differenceClosureRates = new double[n1][n2][3];
		double[][] meanInformation = new double[n1][n2];
		double[][] explorationRateIndi = new double[n1][n2];
		double[][] explorationRateUnions = new double[n1][n2];
		double[][] meanDegree = new double[n1][n2];
		MemoryCriteria criteria = new MemoryCriteria();
		
		
		double lossFactorInitialValue = 0.1;
		double distanceWeightInitialValue = 1;
		double lossFactorIntervalFactor = 0.1;
		double distanceWeightIntervalFactor = 10;
		FiliationType distanceType = FiliationType.VIRI;
		int runs = 100;
		int gen = 3;
		int maxDist = 6;
		
		criteria.setMaxDistance(maxDist);
		criteria.setDistanceType(distanceType);
		criteria.setDistanceFactor(lossFactorInitialValue);
		criteria.setDistanceWeight(distanceWeightInitialValue);
		
		Map<Individual,Set<Individual>> neighborSets = StatisticsWorker.neighborSets(source.getCurrentIndividuals(), criteria.getMaxDistance(), criteria.getDistanceType());

		int trueSize = source.getAllIndividuals().size();
		int trueMarriages = StatisticsWorker.numberOfMarriages(source.getAllFamilies());
		BIASCount trueBias = StatisticsWorker.biasWeights(source.getAllIndividuals()).get(gen);
		Double[] trueAgnaticClosureRates = CircuitFinder.getClosureRates(source, "LINE", FiliationType.AGNATIC, 0).get(new Value("AGNATIC"));
		Double[] trueUterineClosureRates = CircuitFinder.getClosureRates(source, "LINE", FiliationType.UTERINE, 0).get(new Value("UTERINE"));
		//
		result.inputs().add("Number of runs by combination", runs);
		result.inputs().add("Kin type", criteria.getDistanceType().toString());
		result.inputs().add("Maximal distance", criteria.getMaxDistance());

		for (int i = 0; i < n1; i++) {
			for (int j = 0; j < n2; j++) {
				System.out.println("loss Factor "+criteria.getDistanceFactor()+"\tdistance weight "+criteria.getDistanceWeight());
//				FieldWorker fieldworker = new FieldWorker(source, criteria, null);
				RandomNetExplorer randomNetExplorer = new RandomNetExplorer(source, criteria, null,neighborSets);
				randomNetExplorer.getStatistics(runs,gen);
				BIASCounts theseBiasCounts = randomNetExplorer.getBiasCounts();
				biasCounts[i][j] = theseBiasCounts.get(gen).getAgnatic() - theseBiasCounts.get(gen).getUterine();
				explorationRateIndi[i][j] = randomNetExplorer.getExplorationRateIndi();
				meanInformation[i][j] = randomNetExplorer.getMeanInformation();
				explorationRateUnions[i][j] = randomNetExplorer.getExplorationRateUnions();
				meanDegree[i][j] = randomNetExplorer.getMeanDegree();
				
				Map<Value,Double[]> theseClosureRates = randomNetExplorer.getClosureRates();
				
				Double[] closureRateAgnatic = new Double[]{0.,0.,0.};
				Double[] closureRateUterine = new Double[]{0.,0.,0.};
				
				if (theseClosureRates.get(new Value("AGNATIC"))!=null){
					closureRateAgnatic = theseClosureRates.get(new Value("AGNATIC"));
				}
				if (theseClosureRates.get(new Value("UTERINE"))!=null){
					closureRateUterine = theseClosureRates.get(new Value("UTERINE"));
				}
				
				agnaticClosureRates[i][j] = closureRateAgnatic;
				uterineClosureRates[i][j] = closureRateUterine;
				for (int k=0;k<3;k++){
					differenceClosureRates[i][j][k] = closureRateAgnatic[k] - closureRateUterine[k];
				}
				criteria.setDistanceWeight(criteria.getDistanceWeight() * distanceWeightIntervalFactor);
			}
			criteria.setDistanceFactor(criteria.getDistanceFactor() + lossFactorIntervalFactor);
			criteria.setDistanceWeight(distanceWeightInitialValue);
		}
		
		//
		result.outputs().appendln("Mean information");
		//
		ReportChart chart00 = new ReportChart("Mean information", GraphType.SURFACE);
		//
		double v1 = lossFactorInitialValue;
		double v2 = distanceWeightInitialValue;
		for (int j = 0; j < n2; j++) {
			chart00.setHeader(String.valueOf(v2), j);
			v2 = v2 * distanceWeightIntervalFactor;
		}

		for (int i = 0; i < n1; i++) {
			chart00.setLineTitle(String.valueOf(v1), i);
			//
			for (int j = 0; j < n2; j++) {
				//
				chart00.setValue(MathUtils.round(meanInformation[i][j], 4), i, j);
			}
			v1 = v1 + lossFactorIntervalFactor;
		}
		result.outputs().append(chart00);

		//
		ReportTable table00 = chart00.createReportTable();
		table00.set(0, 0, "loss/inertia");
		result.outputs().appendln(table00);
		result.outputs().appendln();
		//

		
		//
		result.outputs().appendln("Network exploration (individuals)");
		//
		ReportChart chart01 = new ReportChart("Network exploration (individuals)", GraphType.SURFACE);
		//
		v1 = lossFactorInitialValue;
		v2 = distanceWeightInitialValue;
		for (int j = 0; j < n2; j++) {
			chart01.setHeader(String.valueOf(v2), j);
			v2 = v2 * distanceWeightIntervalFactor;
		}

		for (int i = 0; i < n1; i++) {
			chart01.setLineTitle(String.valueOf(v1), i);
			//
			for (int j = 0; j < n2; j++) {
				//
				chart01.setValue(MathUtils.round(explorationRateIndi[i][j], 4), i, j);
			}
			v1 = v1 + lossFactorIntervalFactor;
		}
		result.outputs().append(chart01);

		//
		ReportTable table01 = chart01.createReportTable();
		table01.set(0, 0, "loss/inertia");
		result.outputs().appendln(table01);
		result.outputs().appendln();
		//
		
		//
		result.outputs().appendln("Network exploration (unions)");
		//
		ReportChart chart03 = new ReportChart("Network exploration (unions)", GraphType.SURFACE);
		//
		v1 = lossFactorInitialValue;
		v2 = distanceWeightInitialValue;
		for (int j = 0; j < n2; j++) {
			chart03.setHeader(String.valueOf(v2), j);
			v2 = v2 * distanceWeightIntervalFactor;
		}

		for (int i = 0; i < n1; i++) {
			chart03.setLineTitle(String.valueOf(v1), i);
			//
			for (int j = 0; j < n2; j++) {
				//
				chart03.setValue(MathUtils.round(explorationRateUnions[i][j], 4), i, j);
			}
			v1 = v1 + lossFactorIntervalFactor;
		}
		result.outputs().append(chart03);

		//
		ReportTable table03 = chart03.createReportTable();
		table03.set(0, 0, "loss/inertia");
		result.outputs().appendln(table03);
		result.outputs().appendln();
		//


		//
		result.outputs().appendln("Mean NrMarriages (true = "+MathUtils.percent(2*trueMarriages,100*trueSize)+")");
		//
		ReportChart chart02 = new ReportChart("Mean NrMarriages", GraphType.SURFACE);
		//
		v1 = lossFactorInitialValue;
		v2 = distanceWeightInitialValue;
		for (int j = 0; j < n2; j++) {
			chart02.setHeader(String.valueOf(v2), j);
			v2 = v2 * distanceWeightIntervalFactor;
		}

		for (int i = 0; i < n1; i++) {
			chart02.setLineTitle(String.valueOf(v1), i);
			//
			for (int j = 0; j < n2; j++) {
				//
				chart02.setValue(MathUtils.round(meanDegree[i][j], 4), i, j);
			}
			v1 = v1 + lossFactorIntervalFactor;
		}
		result.outputs().append(chart02);

		//
		ReportTable table02 = chart02.createReportTable();
		table02.set(0, 0, "loss/inertia");
		result.outputs().appendln(table02);
		result.outputs().appendln();
		//


		//
		result.outputs().appendln("Agnatic bias variation ("+gen+" generations)"+ "( true = "+MathUtils.percent(trueBias.getAgnatic()-trueBias.getUterine(),100)+")");
		//
		ReportChart chart = new ReportChart("Agnatic Bias Variation "+gen, GraphType.SURFACE);
		//
		v1 = lossFactorInitialValue;
		v2 = distanceWeightInitialValue;
		for (int j = 0; j < n2; j++) {
			chart.setHeader(String.valueOf(v2), j);
			v2 = v2 * distanceWeightIntervalFactor;
		}

		for (int i = 0; i < n1; i++) {
			chart.setLineTitle(String.valueOf(v1), i);
			//
			for (int j = 0; j < n2; j++) {
				//
				chart.setValue(MathUtils.round(biasCounts[i][j], 4), i, j);
			}
			v1 = v1 + lossFactorIntervalFactor;
		}
		result.outputs().append(chart);

		//
		ReportTable table = chart.createReportTable();
		table.set(0, 0, "loss/inertia");
		result.outputs().appendln(table);
		result.outputs().appendln();
		//
		
		
		//
		result.outputs().appendln("Agnatic Cousin Marriage ("+gen+" generations) (true ="+trueAgnaticClosureRates[0]+")");

		ReportChart chart2a = new ReportChart("Agnatic Cousin Marriage Variation "+gen, GraphType.SURFACE);

		//
		v1 = lossFactorInitialValue;
		v2 = distanceWeightInitialValue;
		for (int j = 0; j < n2; j++) {
			chart2a.setHeader(String.valueOf(v2), j);
			v2 = v2 * distanceWeightIntervalFactor;
		}

		for (int i = 0; i < n1; i++) {
			chart2a.setLineTitle(String.valueOf(v1), i);
			//
			for (int j = 0; j < n2; j++) {
				//
				chart2a.setValue(MathUtils.round(agnaticClosureRates[i][j][0], 4), i, j);
			}
			v1 = v1 + lossFactorIntervalFactor;
		}
		result.outputs().append(chart2a);

		//
		ReportTable table2a = chart2a.createReportTable();
		table2a.set(0, 0, "loss/inertia");
		result.outputs().appendln(table2a);
		result.outputs().appendln();

		//
		result.outputs().appendln("Agnatic Cousin Frequency ("+gen+" generations) (true ="+trueAgnaticClosureRates[1]+")");

		//
		ReportChart chart2b = new ReportChart("Agnatic Cousin Frequency Variation "+gen, GraphType.SURFACE);

		//
		v1 = lossFactorInitialValue;
		v2 = distanceWeightInitialValue;
		for (int j = 0; j < n2; j++) {
			chart2b.setHeader(String.valueOf(v2), j);
			v2 = v2 * distanceWeightIntervalFactor;
		}

		for (int i = 0; i < n1; i++) {
			chart2b.setLineTitle(String.valueOf(v1), i);
			//
			for (int j = 0; j < n2; j++) {
				//
				chart2b.setValue(MathUtils.round(agnaticClosureRates[i][j][1], 4), i, j);
			}
			v1 = v1 + lossFactorIntervalFactor;
		}
		result.outputs().append(chart2b);

		//
		ReportTable table2b = chart2b.createReportTable();
		table2b.set(0, 0, "loss/inertia");
		result.outputs().appendln(table2b);
		result.outputs().appendln();
		//


		result.outputs().appendln("Agnatic Closure ("+gen+" generations) (true ="+trueAgnaticClosureRates[2]+")");

		//
		ReportChart chart2c = new ReportChart("Agnatic Closure Variation "+gen, GraphType.SURFACE);

		//
		v1 = lossFactorInitialValue;
		v2 = distanceWeightInitialValue;
		for (int j = 0; j < n2; j++) {
			chart2c.setHeader(String.valueOf(v2), j);
			v2 = v2 * distanceWeightIntervalFactor;
		}

		for (int i = 0; i < n1; i++) {
			chart2c.setLineTitle(String.valueOf(v1), i);
			//
			for (int j = 0; j < n2; j++) {
				//
				chart2c.setValue(MathUtils.round(agnaticClosureRates[i][j][2], 4), i, j);
			}
			v1 = v1 + lossFactorIntervalFactor;
		}
		result.outputs().append(chart2c);

		//
		ReportTable table2c = chart2c.createReportTable();
		table2c.set(0, 0, "loss/inertia");
		result.outputs().appendln(table2c);
		result.outputs().appendln();
		//
		
		//
		result.outputs().appendln("Uterine Cousin Marriage ("+gen+" generations) (true ="+trueUterineClosureRates[0]+")");

		ReportChart chart4a = new ReportChart("Uterine Cousin Marriage Variation "+gen, GraphType.SURFACE);

		//
		v1 = lossFactorInitialValue;
		v2 = distanceWeightInitialValue;
		for (int j = 0; j < n2; j++) {
			chart4a.setHeader(String.valueOf(v2), j);
			v2 = v2 * distanceWeightIntervalFactor;
		}

		for (int i = 0; i < n1; i++) {
			chart4a.setLineTitle(String.valueOf(v1), i);
			//
			for (int j = 0; j < n2; j++) {
				//
				chart4a.setValue(MathUtils.round(uterineClosureRates[i][j][0], 4), i, j);
			}
			v1 = v1 + lossFactorIntervalFactor;
		}
		result.outputs().append(chart4a);

		//
		ReportTable table4a = chart4a.createReportTable();
		table4a.set(0, 0, "loss/inertia");
		result.outputs().appendln(table4a);
		result.outputs().appendln();
		
		//
		result.outputs().appendln("Uterine Cousin Frequency ("+gen+" generations) (true ="+trueUterineClosureRates[1]+")");
		//
		ReportChart chart4b = new ReportChart("Uterine Cousin Frequency Variation "+gen, GraphType.SURFACE);

		//
		v1 = lossFactorInitialValue;
		v2 = distanceWeightInitialValue;
		for (int j = 0; j < n2; j++) {
			chart4b.setHeader(String.valueOf(v2), j);
			v2 = v2 * distanceWeightIntervalFactor;
		}

		for (int i = 0; i < n1; i++) {
			chart4b.setLineTitle(String.valueOf(v1), i);
			//
			for (int j = 0; j < n2; j++) {
				//
				chart4b.setValue(MathUtils.round(uterineClosureRates[i][j][1], 4), i, j);
			}
			v1 = v1 + lossFactorIntervalFactor;
		}
		result.outputs().append(chart4b);

		//
		ReportTable table4b = chart4b.createReportTable();
		table4b.set(0, 0, "loss/inertia");
		result.outputs().appendln(table4b);
		result.outputs().appendln();
		//
		
		result.outputs().appendln("Uterine Closure ("+gen+" generations) (true ="+trueUterineClosureRates[2]+")");
		//
		ReportChart chart4c = new ReportChart("Uterine Closure Variation "+gen, GraphType.SURFACE);

		//
		v1 = lossFactorInitialValue;
		v2 = distanceWeightInitialValue;
		for (int j = 0; j < n2; j++) {
			chart4c.setHeader(String.valueOf(v2), j);
			v2 = v2 * distanceWeightIntervalFactor;
		}

		for (int i = 0; i < n1; i++) {
			chart4c.setLineTitle(String.valueOf(v1), i);
			//
			for (int j = 0; j < n2; j++) {
				//
				chart4c.setValue(MathUtils.round(uterineClosureRates[i][j][2], 4), i, j);
			}
			v1 = v1 + lossFactorIntervalFactor;
		}
		result.outputs().append(chart4c);

		//
		ReportTable table4c = chart4c.createReportTable();
		table4c.set(0, 0, "loss/inertia");
		result.outputs().appendln(table4c);
		result.outputs().appendln();
		//
		
		result.outputs().appendln("Agnatic-Uterine cousin marriage divergence ("+gen+" generations) (true ="+(trueAgnaticClosureRates[0]-trueUterineClosureRates[0])+")");

		ReportChart chart3a = new ReportChart("Agnatic-Uterine cousin marriage divergence "+gen, GraphType.SURFACE);

		//
		v1 = lossFactorInitialValue;
		v2 = distanceWeightInitialValue;
		for (int j = 0; j < n2; j++) {
			chart3a.setHeader(String.valueOf(v2), j);
			v2 = v2 * distanceWeightIntervalFactor;
		}

		for (int i = 0; i < n1; i++) {
			chart3a.setLineTitle(String.valueOf(v1), i);
			//
			for (int j = 0; j < n2; j++) {
				//
				chart3a.setValue(MathUtils.round(differenceClosureRates[i][j][0], 4), i, j);
			}
			v1 = v1 + lossFactorIntervalFactor;
		}
		result.outputs().append(chart3a);

		//
		ReportTable table3a = chart3a.createReportTable();
		table3a.set(0, 0, "loss/inertia");
		result.outputs().appendln(table3a);
		result.outputs().appendln();
		//


		//
		result.outputs().appendln("Agnatic-Uterine cousin frequency divergence ("+gen+" generations) (true ="+(trueAgnaticClosureRates[1]-trueUterineClosureRates[1])+")");

		ReportChart chart3b = new ReportChart("Agnatic-Uterine cousin frequency divergence "+gen, GraphType.SURFACE);

		//
		v1 = lossFactorInitialValue;
		v2 = distanceWeightInitialValue;
		for (int j = 0; j < n2; j++) {
			chart3b.setHeader(String.valueOf(v2), j);
			v2 = v2 * distanceWeightIntervalFactor;
		}

		for (int i = 0; i < n1; i++) {
			chart3b.setLineTitle(String.valueOf(v1), i);
			//
			for (int j = 0; j < n2; j++) {
				//
				chart3b.setValue(MathUtils.round(differenceClosureRates[i][j][1], 4), i, j);
			}
			v1 = v1 + lossFactorIntervalFactor;
		}
		result.outputs().append(chart3b);

		//
		ReportTable table3b = chart3b.createReportTable();
		table3b.set(0, 0, "loss/inertia");
		result.outputs().appendln(table3b);
		result.outputs().appendln();
		//
	
		//
		result.outputs().appendln("Agnatic-Uterine closure divergence ("+gen+" generations) (true ="+(trueAgnaticClosureRates[2]-trueUterineClosureRates[2])+")");
		//
		ReportChart chart3c = new ReportChart("Agnatic-Uterine closure divergence Variation "+gen, GraphType.SURFACE);

		//
		v1 = lossFactorInitialValue;
		v2 = distanceWeightInitialValue;
		for (int j = 0; j < n2; j++) {
			chart3c.setHeader(String.valueOf(v2), j);
			v2 = v2 * distanceWeightIntervalFactor;
		}

		for (int i = 0; i < n1; i++) {
			chart3c.setLineTitle(String.valueOf(v1), i);
			//
			for (int j = 0; j < n2; j++) {
				//
				chart3c.setValue(MathUtils.round(differenceClosureRates[i][j][2], 4), i, j);
			}
			v1 = v1 + lossFactorIntervalFactor;
		}
		result.outputs().append(chart3c);

		//
		ReportTable table3c = chart3c.createReportTable();
		table3c.set(0, 0, "loss/inertia");
		result.outputs().appendln(table3c);
		result.outputs().appendln();
		//
	

		
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	public static Report reportVirtualFieldwork (final RandomNetExplorer randomNetExplorer, final int runs, final int genMax) throws PuckException{
		Report result;
		
		if ((randomNetExplorer == null)) {
			result = null;
		} else {
			//
			Chronometer chrono = new Chronometer();
			
			//
			result = new Report();
			result.setTitle("Virtual fieldwork statistics.");
			result.setOrigin("Fieldworker");
//			result.setTarget(fieldWorker.getLabel());
			
			randomNetExplorer.getStatistics (runs,genMax);
			
			BIASCounts counts = randomNetExplorer.getBiasCounts();
			Map<Value,Double[]> closureRates = randomNetExplorer.getClosureRates();
					
			ReportChart chart = StatisticsReporter.createGenderBIASWeightChart(counts);
			ReportTable table = StatisticsReporter.createGenderBIASWeightTable(counts);
	
			result.outputs().append(chart);
			result.outputs().appendln(table.getTitle());
			result.outputs().appendln(table);
			
			ReportChart chart2 = StatisticsReporter.createMapChart(closureRates,0,"First cousin relations "+genMax);
			ReportTable table2 = chart2.createReportTable();
			result.outputs().append(chart2);
			result.outputs().appendln(table2.getTitle());
			result.outputs().appendln(table2);
			
			ReportChart chart3 = StatisticsReporter.createMapChart(closureRates,1,"First cousin marriages "+genMax);
			ReportTable table3 = chart3.createReportTable();
			result.outputs().append(chart3);
			result.outputs().appendln(table3.getTitle());
			result.outputs().appendln(table3);
			
			ReportChart chart4 = StatisticsReporter.createMapChart(closureRates,2,"First cousin closure Rates "+genMax);
			ReportTable table4 = chart4.createReportTable();
			result.outputs().append(chart4);
			result.outputs().appendln(table4.getTitle());
			result.outputs().appendln(table4);
			
			//
			result.setTimeSpent(chrono.stop().interval());
		}
		
		//
		return result;
	}
	


}
