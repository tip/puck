package org.tip.puck.mas;

import org.tip.puck.util.Distributions;


/**
 * @author Telmo Menezes
 *
 */
public class WFNormalAgeDifference implements WeightFactor {

    double mean;
    double stdev;
    
    public WFNormalAgeDifference(double mean, double stdev) {
        this.mean = mean;
        this.stdev = stdev;
    }
    
    public String factorName(){
    	return "normalagedifference";
    }
    
    @Override
    public double factor(Agent mother, Agent father) {
        double ageDiff = father.getAge() - mother.getAge();
        return Distributions.normal(ageDiff, mean, stdev);
    }
    
    @Override
    public boolean appliesToMarriage() {return false;}
    
    @Override
    public boolean appliesToDivorce() {return true;}
}
