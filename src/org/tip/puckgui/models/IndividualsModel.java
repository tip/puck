package org.tip.puckgui.models;

import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractListModel;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.tip.puck.net.Individual;
import org.tip.puck.net.IndividualComparator;
import org.tip.puck.net.Individuals;

/**
 * 
 * @author TIP
 */
public class IndividualsModel extends AbstractListModel {

	private static final long serialVersionUID = -7257098585401198278L;

	public enum Sorting {
		ID,
		FIRST_NAME,
		LAST_NAME
	}

	private Individuals source;
	private List<Individual> delegate;
	private int lastIndividualSearchIndex;
	private String lastIndividualSearchPattern;
	private Sorting sorting;

	/**
	 * 
	 */
	public IndividualsModel(final Individuals source) {
		//
		super();

		//
		this.source = source;
		if (source == null) {
			this.delegate = new ArrayList<Individual>(0);
		} else {
			this.delegate = this.source.toSortedList();
		}
		this.sorting = Sorting.ID;

		//
		this.lastIndividualSearchIndex = -1;
		this.lastIndividualSearchPattern = null;
	}

	/**
	 * 
	 */
	@Override
	public Object getElementAt(final int index) {
		Individual result;

		result = this.delegate.get(index);

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getSize() {
		int result;

		result = this.delegate.size();

		//
		return result;
	}

	public Sorting getSorting() {
		return this.sorting;
	}

	public Individuals getSource() {
		return this.source;
	}

	/**
	 * 
	 * @param individual
	 * @return
	 */
	public int indexOf(final Individual individual) {
		int result;

		if (this.source == null) {
			result = -1;
		} else {
			boolean ended = false;
			result = -1;
			int index = 0;
			while (!ended) {
				if (index < this.delegate.size()) {
					if (this.delegate.get(index) == individual) {
						ended = true;
						result = index;
					} else {
						index += 1;
					}
				} else {
					ended = true;
					result = -1;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param pattern
	 */
	public Individual nextSearchedIndividual(final String pattern) {
		Individual result;

		if (StringUtils.isBlank(pattern)) {
			result = null;
		} else if (NumberUtils.isDigits(pattern)) {
			// Search individual by id.
			int individualId = Integer.parseInt(pattern);
			result = this.source.getById(individualId);
			resetIndividualSearch();
		} else {
			// Search individual by name.
			List<Individual> foundIndividuals = this.source.searchByName(pattern).toSortedList();
			if (foundIndividuals.isEmpty()) {
				resetIndividualSearch();
				result = null;
			} else if ((this.lastIndividualSearchPattern == null) || (!this.lastIndividualSearchPattern.equals(pattern))) {
				this.lastIndividualSearchIndex = 0;
				this.lastIndividualSearchPattern = pattern;
				result = foundIndividuals.get(0);
			} else {
				this.lastIndividualSearchIndex += 1;
				if (this.lastIndividualSearchIndex >= foundIndividuals.size()) {
					this.lastIndividualSearchIndex = 0;
				}
				result = foundIndividuals.get(this.lastIndividualSearchIndex);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param pattern
	 */
	public int nextSearchedIndividualIndex(final String pattern) {
		int result;

		result = indexOf(nextSearchedIndividual(pattern));

		//
		return result;
	}

	/**
	 * 
	 */
	public void resetIndividualSearch() {
		this.lastIndividualSearchIndex = -1;
		this.lastIndividualSearchPattern = null;
	}

	/**
	 * 
	 * @param sorting
	 */
	public void setSorting(final Sorting sorting) {
		//
		if (sorting != null) {
			//
			this.sorting = sorting;
			setSource(this.source);
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void setSource(final Individuals source) {
		//
		resetIndividualSearch();
		fireIntervalRemoved(this, 0, this.delegate.size());
		this.source = source;
		switch (this.sorting) {
			case ID:
				this.delegate = this.source.toSortedList();
			break;
			case FIRST_NAME:
				this.delegate = this.source.toSortedList(IndividualComparator.Sorting.FIRSTN);
			break;
			case LAST_NAME:
				this.delegate = this.source.toSortedList(IndividualComparator.Sorting.LASTN);
			break;
		}

		fireIntervalAdded(this, 0, this.delegate.size());
	}

	/**
	 * 
	 * @return
	 */
	public List<Individual> toSortedList() {
		List<Individual> result;

		result = this.delegate;

		//
		return result;
	}

	/**
	 * 
	 */
	public void touchSorting() {
		//
		switch (this.sorting) {
			case ID:
				setSorting(Sorting.FIRST_NAME);
			break;

			case FIRST_NAME:
				setSorting(Sorting.LAST_NAME);
			break;

			case LAST_NAME:
				setSorting(Sorting.ID);
			break;
		}
	}
}
