package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import org.tip.puck.net.FiliationType;
import org.tip.puck.net.workers.DistanceAttributeCriteria;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class DistanceAttributeInputDialog extends JDialog {

	private static final long serialVersionUID = 8635062212006842312L;
	private final JPanel contentPanel = new JPanel();
	private DistanceAttributeCriteria dialogCriteria;
	private static DistanceAttributeCriteria lastCriteria = new DistanceAttributeCriteria();
	private JSpinner spnnrMaxDistance;
	private JRadioButton rdbtnUxori;
	private JRadioButton rdbtnViri;
	private JRadioButton rdbtnSpouse;
	private JRadioButton rdbtnIdentity;
	private JRadioButton rdbtnBilinear;
	private JRadioButton rdbtnUterine;
	private JRadioButton rdbtnAgnatic;
	private JRadioButton rdbtnCognatic;
	private final ButtonGroup buttonGroupExpansionMode = new ButtonGroup();
	private JTextPane txtpnThisFormsWill;
	private JPanel headerPanel;

	/**
	 * Create the dialog.
	 */
	public DistanceAttributeInputDialog() {
		super();

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Distance Attribute Input");
		setIconImage(Toolkit.getDefaultToolkit().getImage(DistanceAttributeInputDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				DistanceAttributeInputDialog.this.dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 290, 380);
		getContentPane().setLayout(new BorderLayout());
		{
			this.headerPanel = new JPanel();
			getContentPane().add(this.headerPanel, BorderLayout.NORTH);
			this.headerPanel.setLayout(new BoxLayout(this.headerPanel, BoxLayout.Y_AXIS));
			{
				this.txtpnThisFormsWill = new JTextPane();
				this.txtpnThisFormsWill.setBackground(UIManager.getColor("Panel.background"));
				this.headerPanel.add(this.txtpnThisFormsWill);
				this.txtpnThisFormsWill.setText("This forms will add a DIST attribute to any individuals.");
			}
		}
		this.contentPanel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));
		{
			JLabel lblTarget = new JLabel("FiliationType:");
			this.contentPanel.add(lblTarget, "2, 2, right, default");
		}
		{
			JPanel panel = new JPanel();
			this.contentPanel.add(panel, "4, 2, fill, fill");
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			{
				this.rdbtnCognatic = new JRadioButton("Cognatic");
				this.buttonGroupExpansionMode.add(this.rdbtnCognatic);
				panel.add(this.rdbtnCognatic);
			}
			{
				this.rdbtnAgnatic = new JRadioButton("Agnatic");
				this.buttonGroupExpansionMode.add(this.rdbtnAgnatic);
				panel.add(this.rdbtnAgnatic);
			}
			{
				this.rdbtnUterine = new JRadioButton("Uterine");
				this.buttonGroupExpansionMode.add(this.rdbtnUterine);
				panel.add(this.rdbtnUterine);
			}
			{
				this.rdbtnBilinear = new JRadioButton("Bilinear");
				this.buttonGroupExpansionMode.add(this.rdbtnBilinear);
				panel.add(this.rdbtnBilinear);
			}
			{
				this.rdbtnIdentity = new JRadioButton("Identity");
				this.buttonGroupExpansionMode.add(this.rdbtnIdentity);
				panel.add(this.rdbtnIdentity);
			}
			{
				this.rdbtnSpouse = new JRadioButton("Spouse");
				this.buttonGroupExpansionMode.add(this.rdbtnSpouse);
				panel.add(this.rdbtnSpouse);
			}
			{
				this.rdbtnViri = new JRadioButton("Viri");
				this.buttonGroupExpansionMode.add(this.rdbtnViri);
				panel.add(this.rdbtnViri);
			}
			{
				this.rdbtnUxori = new JRadioButton("Uxori");
				this.buttonGroupExpansionMode.add(this.rdbtnUxori);
				panel.add(this.rdbtnUxori);
			}
		}
		{
			JLabel lblLabel = new JLabel("Max Distance:");
			this.contentPanel.add(lblLabel, "2, 4, right, default");
		}
		{
			this.spnnrMaxDistance = new JSpinner();
			this.spnnrMaxDistance.setModel(new SpinnerNumberModel(new Integer(100), new Integer(0), null, new Integer(1)));
			this.contentPanel.add(this.spnnrMaxDistance, "4, 4");
		}
		{
			JLabel lblNo = new JLabel("0 = no limit");
			this.contentPanel.add(lblNo, "4, 6, center, default");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						DistanceAttributeInputDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("Generate");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						DistanceAttributeCriteria criteria = getCriteria();

						//
						if (DistanceAttributeCriteria.isValid(criteria)) {
							//
							lastCriteria = criteria;
							DistanceAttributeInputDialog.this.dialogCriteria = criteria;

							//
							setVisible(false);

						} else {
							//
							String title = "Invalid input";
							String message = "Please, check your input.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						}

					}
				});
				{
					Component horizontalStrut = Box.createHorizontalStrut(20);
					buttonPane.add(horizontalStrut);
				}
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}

		// ////////////////////////
		setCriteria(lastCriteria);
	}

	/**
	 * 
	 * @return
	 */
	public DistanceAttributeCriteria getCriteria() {
		DistanceAttributeCriteria result;

		result = new DistanceAttributeCriteria();

		//
		if (this.rdbtnUterine.isSelected()) {
			//
			result.setFiliationType(FiliationType.UTERINE);

		} else if (this.rdbtnAgnatic.isSelected()) {
			//
			result.setFiliationType(FiliationType.AGNATIC);

		} else if (this.rdbtnBilinear.isSelected()) {
			//
			result.setFiliationType(FiliationType.BILINEAR);

		} else if (this.rdbtnIdentity.isSelected()) {
			//
			result.setFiliationType(FiliationType.IDENTITY);

		} else if (this.rdbtnSpouse.isSelected()) {
			//
			result.setFiliationType(FiliationType.SPOUSE);

		} else if (this.rdbtnViri.isSelected()) {
			//
			result.setFiliationType(FiliationType.VIRI);

		} else if (this.rdbtnUxori.isSelected()) {
			//
			result.setFiliationType(FiliationType.UXORI);

		} else if (this.rdbtnCognatic.isSelected()) {
			//
			result.setFiliationType(FiliationType.COGNATIC);
		}

		//
		result.setMaxDistance((Integer) this.spnnrMaxDistance.getValue());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public DistanceAttributeCriteria getDialogCriteria() {
		DistanceAttributeCriteria result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final DistanceAttributeCriteria source) {
		//
		if (source != null) {

			//
			this.rdbtnAgnatic.setSelected(false);
			this.rdbtnUterine.setSelected(false);
			this.rdbtnBilinear.setSelected(false);
			this.rdbtnIdentity.setSelected(false);
			this.rdbtnSpouse.setSelected(false);
			this.rdbtnViri.setSelected(false);
			this.rdbtnUxori.setSelected(false);
			this.rdbtnCognatic.setSelected(false);

			//
			if (source.getFiliationType() == null) {
				//
				this.rdbtnCognatic.setSelected(true);

			} else {
				//
				switch (source.getFiliationType()) {
					case AGNATIC:
						this.rdbtnAgnatic.setSelected(true);
					break;

					case BILINEAR:
						this.rdbtnBilinear.setSelected(true);
					break;

					case COGNATIC:
						this.rdbtnCognatic.setSelected(true);
					break;

					case IDENTITY:
						this.rdbtnIdentity.setSelected(true);
					break;

					case SPOUSE:
						this.rdbtnSpouse.setSelected(true);
					break;

					case UTERINE:
						this.rdbtnUterine.setSelected(true);
					break;

					case UXORI:
						this.rdbtnUxori.setSelected(true);
					break;

					case VIRI:
						this.rdbtnViri.setSelected(true);
					break;

					default:
						this.rdbtnCognatic.setSelected(true);
				}
			}

			//
			this.spnnrMaxDistance.setValue(source.getMaxDistance());
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		/* DistanceAttributeCriteria criteria = */showDialog();
	}

	/**
	 * Launch the application.
	 */
	public static DistanceAttributeCriteria showDialog() {
		DistanceAttributeCriteria result;

		//
		DistanceAttributeInputDialog dialog = new DistanceAttributeInputDialog();
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
