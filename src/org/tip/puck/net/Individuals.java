package org.tip.puck.net;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.IndividualComparator.Sorting;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.sequences.Ordinal;
import org.tip.puck.util.NumberablesHashMap;

import fr.devinsy.util.StringList;

/**
 * The <code>Individuals</code> class represents an individual collection.
 * 
 * 
 * @author TIP
 */
public class Individuals extends NumberablesHashMap<Individual> implements Populatable{
	/**
	 * 
	 */
	public Individuals() {
		super();
	}

	/**
	 * 
	 */
	public Individuals(final Individuals source) {
		super();
		add(source);
	}
	
	/**
	 * 
	 */
	public Individuals(final int initialCapacity) {
		super(initialCapacity);
	}


	/**
	 * 
	 * @param source
	 */
	public Individuals(final List<Individual> source) {
		super();
		add(source);
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public Individuals add(final Individuals source) {
		Individuals result;

		result = put(source);

		//
		return result;
	}
	
	public Individual getFirstCorresponding (final String name, final Gender alterGender, final Gender egoGender){
		Individual result;
		
		result = null;
		
		for (Individual individual : toSortedList()){
			if (individual.getName().equals(name) && individual.getGender().matchs(alterGender) && Gender.valueOf(individual.getAttributeValue("EGOGENDER")).matchs(egoGender)){
				result = individual;
				break;
			}
		}
		//
		return result;
	}
	
	public boolean hasGender (Gender gender){
		boolean result;
		
		result = false;
		
		for (Individual individual : this){
			if (individual.getGender()==gender){
				result = true;
				break;
			}
		}
		//
		return result;
	}



	/**
	 * 
	 * @param source
	 * @return
	 */
	public Individuals add(final List<Individual> source) {
		Individuals result;

		result = put(source);

		//
		return result;
	}

	/**
	 * This method makes a shallow copy of the list.
	 * 
	 * @return
	 */
	public Individuals getIndividuals() {
		Individuals result;

		//
		result = new Individuals(this);

		//
		return result;
	}
	
	
	public Individuals getAllIndividuals (List<Ordinal> times) {
		return getIndividuals();
	}
	
	/**
	 * 
	 * @return
	 */
	public List<String> firstNames() {
		List<String> result;

		//
		HashMap<String, String> buffer = new HashMap<String, String>();
		for (Individual individual : this) {
			String firstName = individual.getFirstName();
			if (StringUtils.isNotBlank(firstName)) {
				buffer.put(firstName, null);
			}
		}

		//
		Set<String> firstNames = buffer.keySet();
		result = new ArrayList<String>(firstNames.size());
		for (String firstName : buffer.keySet()) {
			result.add(firstName);
		}

		//
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public StringList getAttributeLabels() {
		StringList result;

		//
		result = new StringList();

		//
		for (Individual individual : this) {
			for (String label : individual.attributes().labels()) {
				if (!result.contains(label)) {
					result.add(label);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param pattern
	 * @return
	 */
	public Individuals getByGender(final Gender pattern) {
		Individuals result;

		if (pattern == null) {
			result = new Individuals(this);
		} else {
			result = new Individuals();
			for (Individual individual : this) {
				if (individual.getGender() == pattern) {
					result.add(individual);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param pattern
	 * @return
	 */
	public Individuals getByGenderNear(final Gender pattern) {
		Individuals result;

		if (pattern == null) {
			result = new Individuals(this);
		} else {
			result = new Individuals();

			for (Individual individual : this) {
				if (individual.getGender().matchs(pattern)) {
					result.add(individual);
				}
			}
		}

		//
		return result;
	}

	/***
	 * 
	 * @param ids
	 * @return
	 */
	public Individuals getByIds(final Integer[] ids) {
		Individuals result;

		if (ids == null) {
			result = new Individuals();
		} else {
			result = new Individuals(ids.length);
			for (Integer id : ids) {
				Individual individual = getById(id);
				if (individual != null) {
					result.add(individual);
				}
			}
		}

		//
		return result;
	}

	/***
	 * 
	 * @param ids
	 * @return
	 */
	public Individuals getByIds(final List<Integer> ids) {
		Individuals result;

		if (ids == null) {
			result = new Individuals();
		} else {
			result = new Individuals(ids.size());
			for (Integer id : ids) {
				Individual individual = getById(id);
				if (individual != null) {
					result.add(individual);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param pattern
	 * @return
	 */
	public Individuals getFertiles() {
		Individuals result;

		result = new Individuals();
		for (Individual individual : this) {
			if (individual.isFertile()) {
				result.add(individual);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param pattern
	 * @return
	 */
	public Individuals getSteriles() {
		Individuals result;

		//
		result = new Individuals();
		for (Individual individual : this) {
			if (individual.isSterile()) {
				result.add(individual);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<String> lastNames() {
		List<String> result;

		//
		HashMap<String, String> buffer = new HashMap<String, String>();
		for (Individual individual : this) {
			String lastName = individual.getLastName();
			if (StringUtils.isNotBlank(lastName)) {
				buffer.put(lastName, null);
			}
		}

		//
		Set<String> lastNames = buffer.keySet();
		result = new ArrayList<String>(lastNames.size());
		for (String lastName : buffer.keySet()) {
			result.add(lastName);
		}

		//
		Collections.sort(result);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public Individuals put(final Individuals source) {
		Individuals result;

		//
		if (source != null) {
			for (Individual individual : source) {
				this.add(individual);
			}
		}

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public Individuals put(final List<Individual> source) {
		Individuals result;

		//
		if (source != null) {
			for (Individual individual : source) {
				this.add(individual);
			}
		}

		//
		result = this;

		//
		return result;
	}

	/**
	 * This method removes a collection of individuals.
	 * 
	 * @param source
	 *            Individuals to remove.
	 * 
	 * @return
	 */
	public Individuals remove(final Individuals source) {
		Individuals result;

		//
		for (Individual individual : source) {
			//
			removeById(individual.getId());
		}

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @param pattern
	 * @return individuals matching pattern.
	 */
	public Individuals searchByAttribute(final String label, final String pattern) {
		Individuals result;

		//
		result = new Individuals();

		//
		if ((StringUtils.isNotBlank(label)) && (StringUtils.isNotBlank(pattern))) {
			//
			for (Individual individual : this) {
				//
				Attribute attribute = individual.attributes().get(label);

				if ((attribute != null) && (attribute.getValue() != null) && (attribute.getValue().matches(pattern))) {
					//
					result.add(individual);
				}
			}
		}

		//
		return result;
	}

	/**
	 * This method searches by name ignoring case.
	 * 
	 * @param pattern
	 *            Filter of search.
	 * 
	 * @return individuals matching pattern.
	 */
	public Individuals searchByName(final String pattern) {
		Individuals result;

		result = new Individuals();

		if (StringUtils.isNotBlank(pattern)) {
			//
			String targetPattern = pattern.toLowerCase();

			for (Individual individual : this) {
				//
				if (individual.getName() != null && individual.getName().toLowerCase().contains(targetPattern)) {
					//
					result.add(individual);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @param label
	 * @param value
	 */
	public void setAttribute(final int id, final String label, final String value) {

		if ((label != null) && (value != null)) {
			//
			Individual individual = getById(id);
			if (individual == null) {
				individual = new Individual(id);
				this.put(individual);
			}

			//
			if ((label.equals("NAME")) && (StringUtils.isNotBlank(value)) && (!StringUtils.equals(value, "null"))) {
				individual.setName(value);
			}

			//
			individual.attributes().put(label, value);
		}
	}

	/**
	 * 
	 * @return
	 */
	public int size(final Gender pattern) {
		int result;

		if (pattern == null) {
			result = this.size();
		} else {
			result = 0;
			for (Individual individual : this) {
				if (individual.getGender() == pattern) {
					result += 1;
				}
			}
		}

		//
		return result;
	}
	
	public double genderRatio (final Gender gender){
		double result;
		
		result = new Double(size(gender))/new Double(size());
		
		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Individual[] toArray() {
		Individual[] result;

		result = new Individual[this.size()];

		int individualCount = 0;
		for (Individual individual : this) {
			result[individualCount] = individual;
			individualCount += 1;
		}

		//
		return result;
	}

	/**
	 * This method returns a list of individuals sorted by their birth order.
	 * 
	 * @return a individual list sorted by their birth order.
	 */
	public List<Individual> toListSortedByOrder() {
		List<Individual> result;

		result = toList();
		Collections.sort(result, new IndividualComparator(Sorting.BIRTH_ORDER, Sorting.ID));

		//
		return result;
	}

	/**
	 * This method returns a list of individuals sorted by their birth order.
	 * 
	 * @return a individual list sorted by their birth order.
	 */
	public List<Individual> toListSortedByBirthYearOrOrder() {
		List<Individual> result;

		result = toList();
		Collections.sort(result, new IndividualComparator(Sorting.BIRTH_YEAR_OR_ORDER, Sorting.ID));

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Individual> toSortedList(final List<Sorting> multiSorting) {
		List<Individual> result;

		result = toList();
		Collections.sort(result, new IndividualComparator(multiSorting));

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Individual> toSortedList(final Sorting... sortings) {
		List<Individual> result;

		result = toList();
		Collections.sort(result, new IndividualComparator(sortings));

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Individual> toSortedList(final Sorting sorting) {
		List<Individual> result;

		result = toList();
		Collections.sort(result, new IndividualComparator(sorting));

		//
		return result;
	}

	/**
	 * This method concatenates the name of individuals.
	 * 
	 * @return the name list of individuals separated by a space character.
	 */
	public String toStringAsNameList() {
		String result;

		StringList buffer = new StringList();

		for (Individual individual : toSortedList()) {
			//
			buffer.append(individual.getName());
		}

		result = buffer.toStringSeparatedBy(" ");

		//
		return result;
	}
	
	public String toString(){		
		return toSortedList().toString();
	}
	
	public Individuals getIndividuals(Segmentation segmentation){
		return segmentation.getCurrentIndividuals();
	}
	




}
