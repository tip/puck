package org.tip.puck;

import java.text.MessageFormat;

import org.tip.puck.util.ExceptionUtil;

/**
 * 
 * @author TIP
 */
public enum PuckExceptions {

	/** */
	FORMAT_CONFLICT(10, "BAR/IUR format conflict."),
	/** */
	OVERFLOW(9, "Limit exceeded."),
	/** */
	UNRESOLVED_KIN(8, "Unresolved kin."),
	/** */
	UNSUPPORTED_FILE_FORMAT(7, "Unsupported file format."),
	/** */
	NOT_A_FILE(6, "The target is not file."),
	/** */
	UNSUPPORTED_ENCODING(5, "Unsupported Encoding."),
	/** */
	FILE_NOT_FOUND(4, "File not found."),
	/** */
	IO_ERROR(3, "Input/Output error."),
	/** */
	BAD_FILE_FORMAT(2, "Bad file format."),
	/** */
	INVALID_PARAMETER(1, "Invalid parameter.");

	private int code;
	private String message;

	/**
	 * 
	 * @param code
	 * @param message
	 */
	private PuckExceptions(final int code, final String message) {
		this.code = code;
		this.message = message;
	}

	/**
	 * @return
	 */
	public PuckException create() {
		PuckException result;

		result = create((Exception) null, "");

		//
		return result;
	}

	/**
	 * @return
	 */
	public PuckException create(final Exception exception) {
		PuckException result;

		result = create(exception, "");

		//
		return result;
	}

	/**
	 * @return
	 */
	public PuckException create(final Exception exception, final String format, final Object... arguments) {
		PuckException result;

		String extendedMessage = MessageFormat.format(this.message + " " + format, arguments);

		// Instanciate exception.
		result = PuckException.getInstance(ExceptionUtil.getCaller(this.getClass().getName()), this.code, extendedMessage, exception);

		//
		return result;
	}

	/**
	 * @return
	 */
	public PuckException create(final String format, final Object... values) {
		PuckException result;

		result = create(null, format, values);

		//
		return result;
	}

	public int getCode() {
		return this.code;
	}

	public String getMessage() {
		return this.message;
	}

	public void setCode(final int code) {
		this.code = code;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

	/**
	 * 
	 * @param code
	 * @return
	 */
	public static PuckExceptions valueOf(final int code) {
		PuckExceptions result;

		boolean ended = false;
		result = null;
		int count = 0;
		while (!ended) {
			if (count < PuckExceptions.values().length) {
				PuckExceptions item = PuckExceptions.values()[count];

				if (item.getCode() == code) {
					ended = true;
					result = item;
				} else {
					count += 1;
				}
			} else {
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}
}
