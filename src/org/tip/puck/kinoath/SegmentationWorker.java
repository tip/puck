package org.tip.puck.kinoath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.segmentation.Segment;

/**
 * 
 * @author TIP
 */
public class SegmentationWorker {

	private static final Logger logger = LoggerFactory.getLogger(SegmentationWorker.class);

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static IndividualGroups convert(final Segment source) {
		IndividualGroups result;

		//
		result = new IndividualGroups();

		//
		if (source != null) {
			//
			int currentClusterIndex = source.getCurrentClusterIndex();
			logger.debug("Initial index=" + currentClusterIndex);
			logger.debug("Cluster count=" + source.getClusterCount());

			//
			try {
				//
				for (int clusterIndex = 0; clusterIndex < source.getClusterCount(); clusterIndex++) {
					//
					source.selectCluster(clusterIndex);

					IndividualGroup group = new IndividualGroup(result.size() + 1, source.getLabel());
					group.put(source.getCurrentIndividuals());

					result.add(group);
				}

				//
				source.selectOutOfPartitionCluster();

				IndividualGroup group = new IndividualGroup(result.size() + 1, source.getLabel());
				group.put(source.getCurrentIndividuals());

				result.add(group);

			} catch (PuckException exception) {
				//
				exception.printStackTrace();

			} finally {
				try {
					//
					source.selectCluster(currentClusterIndex);
					logger.debug("Initial index set.");

				} catch (PuckException exception) {
					//
					logger.debug("Very embarassing");
					exception.printStackTrace();
				}
			}
		}

		//
		return result;
	}
}
