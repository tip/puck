package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.net.workers.AttributeWorker.Scope;
import org.tip.puck.net.workers.IndividualValuator;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class ValuateExogenousAttributeDialog extends JDialog {

	private static final long serialVersionUID = 159505630124471904L;
	private static final Logger logger = LoggerFactory.getLogger(ValuateExogenousAttributeDialog.class);
	private final JPanel contentPanel = new JPanel();
	private ValuateExogenousAttributeCriteria dialogCriteria;
	private static ValuateExogenousAttributeCriteria lastCriteria = new ValuateExogenousAttributeCriteria();
	private JComboBox cmbbxTarget;
	private JComboBox cmbbxSourceLabel;
	private JTextField txtfldTargetLabel;
	private JTextField txtfldValuePrefix;

	/**
	 * Create the dialog.
	 */
	public ValuateExogenousAttributeDialog() {
		super();

		//
		List<String> targetLabels = new ArrayList<String>();
		targetLabels.add("INDIVIDUALS");

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Valuate Exogenous Attribute");
		setIconImage(Toolkit.getDefaultToolkit().getImage(ValuateExogenousAttributeDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				ValuateExogenousAttributeDialog.this.dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 366, 235);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));
		{
			JLabel lblScope = new JLabel("Scope:");
			this.contentPanel.add(lblScope, "2, 2, right, default");
		}
		{
			this.cmbbxTarget = new JComboBox(targetLabels.toArray());
			this.contentPanel.add(this.cmbbxTarget, "4, 2, fill, default");
		}
		{
			JLabel lblSourceLabel = new JLabel("Source Label:");
			this.contentPanel.add(lblSourceLabel, "2, 4, right, default");
		}
		{
			this.cmbbxSourceLabel = new JComboBox(IndividualValuator.EndogenousLabel.values());
			this.contentPanel.add(this.cmbbxSourceLabel, "4, 4, fill, default");
		}
		{
			JLabel lblTargetLabel = new JLabel("Target Label:");
			this.contentPanel.add(lblTargetLabel, "2, 6, right, default");
		}
		{
			this.txtfldTargetLabel = new JTextField();
			this.contentPanel.add(this.txtfldTargetLabel, "4, 6, fill, default");
			this.txtfldTargetLabel.setColumns(10);
		}
		{
			JLabel lblPrefixValue = new JLabel("Prefix value:");
			this.contentPanel.add(lblPrefixValue, "2, 8, right, default");
		}
		{
			this.txtfldValuePrefix = new JTextField();
			this.contentPanel.add(this.txtfldValuePrefix, "4, 8, fill, default");
			this.txtfldValuePrefix.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						ValuateExogenousAttributeDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						ValuateExogenousAttributeCriteria criteria = getCriteria();
						if (StringUtils.isBlank(criteria.getTargetLabel())) {
							//
							String title = "Invalid input";
							String message = "Please, enter a target label not empty.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						} else {
							//
							lastCriteria = criteria;
							ValuateExogenousAttributeDialog.this.dialogCriteria = criteria;

							//
							setVisible(false);
						}

					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}

		// ////////////////////////
		setCriteria(lastCriteria);
	}

	/**
	 * 
	 * @return
	 */
	public ValuateExogenousAttributeCriteria getCriteria() {
		ValuateExogenousAttributeCriteria result;

		result = new ValuateExogenousAttributeCriteria();

		//
		result.setScope(Scope.INDIVIDUALS);
		logger.debug("===> " + this.cmbbxSourceLabel.getSelectedItem());
		result.setSourceLabel(this.cmbbxSourceLabel.getSelectedItem().toString());
		result.setTargetLabel(this.txtfldTargetLabel.getText());
		result.setValuePrefix(this.txtfldValuePrefix.getText());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public ValuateExogenousAttributeCriteria getDialogCriteria() {
		ValuateExogenousAttributeCriteria result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final ValuateExogenousAttributeCriteria source) {
		//
		if (source != null) {

			//
			this.cmbbxSourceLabel.setSelectedItem(source.getSourceLabel());
			if (this.cmbbxSourceLabel.getSelectedIndex() == -1) {
				this.cmbbxSourceLabel.setSelectedIndex(0);
			}

			//
			if (source.getTargetLabel() != null) {
				this.txtfldTargetLabel.setText(source.getTargetLabel());
			}

			if (source.getValuePrefix() != null) {
				this.txtfldTargetLabel.setText(source.getValuePrefix());
			}
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		ValuateExogenousAttributeCriteria criteria = showDialog();
	}

	/**
	 * Launch the application.
	 */
	public static ValuateExogenousAttributeCriteria showDialog() {
		ValuateExogenousAttributeCriteria result;

		//
		ValuateExogenousAttributeDialog dialog = new ValuateExogenousAttributeDialog();
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
