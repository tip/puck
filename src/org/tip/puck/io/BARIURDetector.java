package org.tip.puck.io;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.io.bar.BARFile;
import org.tip.puck.io.iur.IURFile;

/**
 * This class represents a ODS File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class BARIURDetector {

	public enum Format {
		BAR,
		IUR,
		UNKNOWN,
		CONFLICTING
	}

	private static final Logger logger = LoggerFactory.getLogger(BARIURDetector.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	public static final int MAX_LINE_SIZE = 2048;

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static Format detectFormat(final File source) {
		Format result;

		//
		boolean isBAR = BARFile.isBAR(source);
		boolean isIUR = IURFile.isIUR(source);

		//
		if ((isBAR) && (isIUR)) {
			result = Format.CONFLICTING;
		} else if (isBAR) {
			result = Format.BAR;
		} else if (isIUR) {
			result = Format.IUR;
		} else {
			result = Format.UNKNOWN;
		}

		//
		logger.debug("BARIUR detector: [isBAR={}][isIUR={}][result={}]", isBAR, isIUR, result);

		//
		return result;
	}
}
