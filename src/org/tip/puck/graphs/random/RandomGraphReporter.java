package org.tip.puck.graphs.random;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.workers.GraphReporter;
import org.tip.puck.matrix.MatrixStatistics;
import org.tip.puck.matrix.MatrixStatistics.DistributionObject;
import org.tip.puck.matrix.MatrixStatistics.Indicator;
import org.tip.puck.matrix.MatrixStatistics.Mode;
import org.tip.puck.net.Individual;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.graphs.RandomAllianceNetworkByAgentSimulationVariationsCriteria;
import org.tip.puck.partitions.graphs.RandomAllianceNetworkByRandomDistributionCriteria;
import org.tip.puck.partitions.graphs.VirtualFieldworkVariationsCriteria;
import org.tip.puck.report.Report;
import org.tip.puck.report.ReportChart;
import org.tip.puck.report.ReportChart.GraphType;
import org.tip.puck.report.ReportChart.LogarithmType;
import org.tip.puck.report.ReportTable;
import org.tip.puck.statistics.StatisticsReporter;
import org.tip.puck.util.Chronometer;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.ToolBox;

/**
 * 
 * @author TIP
 */
public class RandomGraphReporter {

	private static final Logger logger = LoggerFactory.getLogger(RandomGraphReporter.class);

	/**
	 * 
	 * @param chartTitle
	 * @param chartType
	 * @param initialValue
	 * @param intervalFactor
	 * @param indicator
	 * @return
	 */
	public static ReportChart buildChartFromMatrixStatisticsTable(final String chartTitle, final GraphType chartType, final MatrixStatistics[][] source,
			final double initialValue, final double intervalFactor, final Indicator indicator, final Mode mode) {
		ReportChart result;

		//
		result = new ReportChart(chartTitle, chartType);

		//
		int iMax = source.length;
		int jMax;
		if (source.length == 0) {
			jMax = 0;
		} else {
			jMax = source[0].length;
		}

		//
		double v1 = initialValue;
		for (int i = 0; i < iMax; i++) {
			result.setLineTitle(String.valueOf(MathUtils.round(v1, 8)), i);
			result.setHeader(String.valueOf(v1), i);

			//
			for (int j = 0; j < jMax; j++) {
				//
				result.setValue(MathUtils.round(source[i][j].get(indicator, mode), 4), i, j);
			}

			// Increment first variable for next iteration step.
			v1 = v1 * intervalFactor;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param title
	 * @param legend
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static ReportChart createDistributionChart(final String title, final String legend, final Map<Double, Double> source) {
		ReportChart result;

		//
		result = new ReportChart(title, GraphType.LINES);
		result.setHeadersLegend("Distribution");
		result.setLinesLegend(legend);

		//
		result.setLineTitle(legend, 0);

		//
		Double[] keys = ToolBox.sort(source.keySet());
		for (int keyIndex = 0; keyIndex < keys.length; keyIndex++) {
			Double key = keys[keyIndex];
			result.setHeader(MathUtils.toString(key), keyIndex);
			result.addValue(key, source.get(key), 0);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param title
	 * @param legend
	 * @param source
	 * @return
	 */
	public static ReportTable createDistributionTable(final String title, final String legend, final Map<Double, Double> source) {
		ReportTable result;

		//
		result = new ReportTable(2, source.size() + 1);
		result.setTitle(title);

		//
		result.set(0, 0, " ");
		result.set(1, 0, legend);

		//
		Double[] keys = ToolBox.sort(source.keySet());
		for (int keyIndex = 0; keyIndex < keys.length; keyIndex++) {
			Double key = keys[keyIndex];
			result.set(0, keyIndex + 1, key);
			result.set(1, keyIndex + 1, MathUtils.round(source.get(key), 4));
		}

		//
		return result;
	}

	public static <E> MatrixStatistics createRandomGraphStatisticsByRandomDistribution(final Graph<E> source, final int runs) {
		MatrixStatistics result;

		RandomAllianceNetworkByRandomDistributionCriteria criteria = new RandomAllianceNetworkByRandomDistributionCriteria();

		criteria.setNumberOfNodes(source.nodeCount());
		criteria.setNumberOfArcs((int) source.getArcWeightSum());
		criteria.setDistributionType(DistributionType.FREE);
		criteria.setNumberOfRuns(runs);

		result = createRandomGraphStatisticsByRandomDistribution(criteria);

		//
		return result;

	}

	/**
	 * 
	 * @param criteria
	 * @param runs
	 * @return
	 */
	public static <E> MatrixStatistics createRandomGraphStatisticsByRandomDistribution(final RandomAllianceNetworkByRandomDistributionCriteria criteria) {
		MatrixStatistics result;

		// Foo initialization.
		RandomGraphMaker<E> randomGraphMaker = new RandomGraphMaker<E>(new RandomCriteria());

		//
		List<Graph<E>> graphs = randomGraphMaker.createRandomGraphsByRandomDistribution(criteria.getDistributionType(), criteria.getNumberOfNodes(),
				criteria.getNumberOfArcs(), criteria.getPowerFactor(), criteria.getNumberOfRuns());

		result = GraphReporter.getMatrixStatistics(graphs);

		//
		return result;
	}

	/**
	 * 
	 * @param criteria
	 * @param runs
	 * @return
	 */
	public static <E> MatrixStatistics createRandomGraphStatisticsByRandomPermutation(final Graph<E> source, final int runCount) {
		MatrixStatistics result;

		// Foo intialization.
		RandomGraphMaker<E> randomGraphMaker = new RandomGraphMaker<E>(new RandomCriteria());

		List<Graph<E>> graphs = randomGraphMaker.createRandomGraphsByPermutation(source, runCount);

		result = GraphReporter.getMatrixStatistics(graphs);

		//
		return result;
	}

	/**
	 * 
	 * @param criteria
	 * @param runs
	 * @return
	 */
	public static <E> MatrixStatistics createRandomGraphStatisticsByReshuffling(final Graph<E> source, final int runCount) {
		MatrixStatistics result;

		// Foo intialization.
		RandomGraphMaker<E> randomGraphMaker = new RandomGraphMaker<E>(new RandomCriteria());

		List<Graph<E>> graphs = randomGraphMaker.createRandomGraphsByReshuffling(source, runCount);

		result = GraphReporter.getMatrixStatistics(graphs);

		//
		return result;
	}

	/**
	 * 
	 * @param criteria
	 * @param runs
	 * @return
	 */
	public static <E> Report reportRandomAllianceNetworkByAgentSimulation(final RandomCriteria criteria, final MatrixStatistics stats,
			final double[][] probaEvolution) {
		Report result;

		Chronometer chrono = new Chronometer();

		//
		result = new Report("Agent-based generated Random Graph.");
		result.setOrigin("RandomGraphMaker.reportStatisticsRandomGraphByAgentSimulation()");
		result.setTarget("Statistics about Random Alliance Network by Agent simulation");

		//
		result.inputs().add("Inertia Pattern", criteria.getInertiaPattern());
		result.inputs().add("Number of Nodes", criteria.getNodeCount());
		result.inputs().add("Maximal Number of Arcs", criteria.getArcWeightSum());
		result.inputs().add("Maximal Number of Runs", criteria.getRunCount());
		result.inputs().add("Probability of Outward Direction", criteria.getOutPreference());
		result.inputs().add("Ego distribution", criteria.getDistributionType().toString());
		result.inputs().add("Show probability evolution", criteria.isShowProbabilityEvolution());

		//
		result.outputs().append(MatrixStatistics.statisticsRandom(stats, null));
		// result.outputs().append(source.reportValues(null));

		//
		Map<Double, Double> weightsDistribution = MatrixStatistics.getRandomDistribution(DistributionObject.WEIGHTS, null, criteria, 1, criteria.getRunCount());
		result.outputs().appendln("Weights Distributions");
		result.outputs().appendln(createDistributionChart("Weights Distributions", "Weights", weightsDistribution));
		result.outputs().appendln(createDistributionTable("Weights Distributions", "Weights", weightsDistribution));
		result.outputs().appendln();

		//
		Map<Double, Double> forcesDistribution = MatrixStatistics
				.getRandomDistribution(DistributionObject.STRENGTHS, null, criteria, 1, criteria.getRunCount());
		result.outputs().appendln("Forces Distributions");
		result.outputs().appendln(createDistributionChart("Forces Distributions", "Forces", forcesDistribution));
		result.outputs().appendln(createDistributionTable("Forces Distributions", "Forces", forcesDistribution));
		result.outputs().appendln();

		//
		if (criteria.isShowProbabilityEvolution()) {
			//
			result.outputs().appendln("Probability Evolution");
			ReportChart chart = StatisticsReporter.createArrayChart("Probability Evolution", probaEvolution,
					new String[] { "inertia0", "inertia1", "inertia2" });
			if (chart != null) {
				//
				result.outputs().appendln(chart);
				result.outputs().appendln(chart.createReportTable());
			}
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public static <E> Report reportRandomAllianceNetworkByAgentSimulationVariations(final RandomAllianceNetworkByAgentSimulationVariationsCriteria criteria) {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		//
		result = new Report("Random Graph Variation by agent");
		result.setOrigin("RandomGraphReporter.reportAgentStatistics()");
		result.setTarget("Random Alliance Network Statistics");

		//
		result.inputs().add("Number Of Loops", criteria.isNumberOfLoopsChecked());
		result.inputs().add("Number of circuits", criteria.isNumberOfCircuitsChecked());
		result.inputs().add("Number of triangles", criteria.isNumberOfTrianglesChecked());
		result.inputs().add("Concentration", criteria.isConcentrationChecked());
		result.inputs().add("weight distribution", criteria.isWeightDistributionChecked());
		result.inputs().add("Strength distributionChecked", criteria.isStrengthDistributionChecked());

		result.inputs().add("Number of Nodes", criteria.getNumberOfNodes());
		result.inputs().add("Number of arcs", criteria.getArcWeightSum());
		result.inputs().add("Out preference", criteria.getOutPreference());
		result.inputs().add("Number of runs", criteria.getNumberOfRuns());
		result.inputs().add("Ego Distribution", criteria.getDistributionType().toString());

		result.inputs().add("First variable index", criteria.getFirstVariableIndex());
		result.inputs().add("First variable initial value", criteria.getFirstVariableInitialValue());
		result.inputs().add("First variable interval factor", criteria.getFirstVariableIntervalFactor());
		result.inputs().add("First variable number of steps", criteria.getFirstVariableNumberOfIntervals());

		result.inputs().add("Second variable index", criteria.getSecondVariableIndex());
		result.inputs().add("Second variable initial value", criteria.getSecondVariableInitialValue());
		result.inputs().add("Second variable interval factor", criteria.getSecondVariableIntervalFactor());
		result.inputs().add("Second variable number of steps", criteria.getSecondVariableNumberOfIntervals());

		result.inputs().add("Variation of variable 1", "i" + criteria.getFirstVariableIndex());
		result.inputs().add("Variation of variable 2", "i" + criteria.getSecondVariableIndex());

		// ////////////////////////////////////////////////////////

		if ((criteria.isNumberOfLoopsChecked()) || (criteria.isNumberOfCircuitsChecked()) || (criteria.isNumberOfTrianglesChecked())
				|| (criteria.isConcentrationChecked()) || (criteria.isSymmetryChecked())) {

			// Compute a board of matrix statistics.
			// ========================================
			int n1 = 1 + 2 * criteria.getFirstVariableNumberOfIntervals();
			int n2 = 1 + 2 * criteria.getSecondVariableNumberOfIntervals();
			MatrixStatistics[][] statistics = new MatrixStatistics[n1][n2];

			RandomCriteria agentCriteria = new RandomCriteria();
			agentCriteria.setArcWeightSum(criteria.getArcWeightSum());
			agentCriteria.setNodeCount(criteria.getNumberOfNodes());
			agentCriteria.setDistributionType(criteria.getDistributionType());

			// Initialize the first variable.
			agentCriteria.getInertia()[criteria.getFirstVariableIndex()] = criteria.getFirstVariableInitialValue();

			// Travel the first variable.
			for (int i = 0; i < n1; i++) {
				// Initialize the second variable.
				agentCriteria.getInertia()[criteria.getSecondVariableIndex()] = criteria.getSecondVariableInitialValue();

				// Travel the second variable.
				for (int j = 0; j < n2; j++) {
					System.out.println(agentCriteria.getInertia()[criteria.getFirstVariableIndex()] + " "
							+ agentCriteria.getInertia()[criteria.getSecondVariableIndex()]);

					//
					RandomGraphMaker<E> randomGraphMaker = new RandomGraphMaker<E>(agentCriteria);

					//
					statistics[i][j] = GraphReporter.getMatrixStatistics(randomGraphMaker.createRandomGraphsByAgentSimulation(criteria.getNumberOfRuns()));

					//
					agentCriteria.getInertia()[criteria.getSecondVariableIndex()] = agentCriteria.getInertia()[criteria.getSecondVariableIndex()]
							* criteria.getSecondVariableIntervalFactor();
				}

				// Increment the first variable.
				agentCriteria.getInertia()[criteria.getFirstVariableIndex()] = agentCriteria.getInertia()[criteria.getFirstVariableIndex()]
						* criteria.getFirstVariableIntervalFactor();
			}

			// Compute statistics.
			// ==================

			//
			if (criteria.isNumberOfLoopsChecked()) {
				// Add title.
				result.outputs().appendln("Number of loops");

				ReportChart chart;

				//
				chart = buildChartFromMatrixStatisticsTable("Number of loops", GraphType.SURFACE, statistics, criteria.getFirstVariableInitialValue(),
						criteria.getFirstVariableIntervalFactor(), Indicator.LOOPS, Mode.SIMPLE);
				result.outputs().append(chart);

				//
				chart = buildChartFromMatrixStatisticsTable("Number of loops", GraphType.LINES, statistics, criteria.getFirstVariableInitialValue(),
						criteria.getFirstVariableIntervalFactor(), Indicator.LOOPS, Mode.SIMPLE);
				result.outputs().append(chart);

				//
				ReportTable table = chart.createReportTable();
				table.set(0, 0, "i" + criteria.getFirstVariableIndex() + "/i" + criteria.getSecondVariableIndex());
				result.outputs().appendln(table);
			}

			//
			if (criteria.isNumberOfCircuitsChecked()) {
				// Add title.
				result.outputs().appendln("Number of circuits");

				ReportChart chart;

				//
				chart = buildChartFromMatrixStatisticsTable("Number of circuits", GraphType.SURFACE, statistics, criteria.getFirstVariableInitialValue(),
						criteria.getFirstVariableIntervalFactor(), Indicator.DUAL_CIRCUITS, Mode.SIMPLE);
				result.outputs().append(chart);

				//
				chart = buildChartFromMatrixStatisticsTable("Number of circuits", GraphType.LINES, statistics, criteria.getFirstVariableInitialValue(),
						criteria.getFirstVariableIntervalFactor(), Indicator.DUAL_CIRCUITS, Mode.SIMPLE);
				result.outputs().append(chart);

				//
				ReportTable table = chart.createReportTable();
				table.set(0, 0, "i" + criteria.getFirstVariableIndex() + "/i" + criteria.getSecondVariableIndex());
				result.outputs().appendln(table);
			}

			//
			if (criteria.isNumberOfTrianglesChecked()) {
				//
				result.outputs().appendln("Number of triangles");

				ReportChart chart;

				//
				chart = buildChartFromMatrixStatisticsTable("Number of triangles", GraphType.SURFACE, statistics, criteria.getFirstVariableInitialValue(),
						criteria.getFirstVariableIntervalFactor(), Indicator.TRIANGLES, Mode.SIMPLE);
				result.outputs().append(chart);

				//
				chart = buildChartFromMatrixStatisticsTable("Number of triangles", GraphType.LINES, statistics, criteria.getFirstVariableInitialValue(),
						criteria.getFirstVariableIntervalFactor(), Indicator.TRIANGLES, Mode.SIMPLE);
				result.outputs().append(chart);

				//
				ReportTable table = chart.createReportTable();
				table.set(0, 0, "i" + criteria.getFirstVariableIndex() + "/i" + criteria.getSecondVariableIndex());
				result.outputs().appendln(table);
			}

			//
			if (criteria.isConcentrationChecked()) {
				//
				result.outputs().appendln("Concentration Index");

				ReportChart chart;

				//
				chart = buildChartFromMatrixStatisticsTable("Concentration Index", GraphType.SURFACE, statistics, criteria.getFirstVariableInitialValue(),
						criteria.getFirstVariableIntervalFactor(), Indicator.CONCENTRATION, Mode.SIMPLE);
				result.outputs().append(chart);

				//
				chart = buildChartFromMatrixStatisticsTable("Concentration Index", GraphType.LINES, statistics, criteria.getFirstVariableInitialValue(),
						criteria.getFirstVariableIntervalFactor(), Indicator.CONCENTRATION, Mode.SIMPLE);
				result.outputs().append(chart);

				//
				ReportTable table = chart.createReportTable();
				table.set(0, 0, "i" + criteria.getFirstVariableIndex() + "/i" + criteria.getSecondVariableIndex());
				result.outputs().appendln(table);
			}

			//
			if (criteria.isSymmetryChecked()) {
				//
				result.outputs().appendln("Symmetry Index");

				ReportChart chart;

				//
				chart = buildChartFromMatrixStatisticsTable("Symmetry Index", GraphType.SURFACE, statistics, criteria.getFirstVariableInitialValue(),
						criteria.getFirstVariableIntervalFactor(), Indicator.SYMMETRY, Mode.SIMPLE);
				result.outputs().append(chart);

				//
				chart = buildChartFromMatrixStatisticsTable("Symmetry Index", GraphType.LINES, statistics, criteria.getFirstVariableInitialValue(),
						criteria.getFirstVariableIntervalFactor(), Indicator.SYMMETRY, Mode.SIMPLE);
				result.outputs().append(chart);

				//
				ReportTable table = chart.createReportTable();
				table.set(0, 0, "i" + criteria.getFirstVariableIndex() + "/i" + criteria.getSecondVariableIndex());
				result.outputs().appendln(table);
			}

		}

		// ////////////////////////////////////////////////////////
		if ((criteria.isWeightDistributionChecked()) || (criteria.isStrengthDistributionChecked())) {
			//
			RandomCriteria agentCriteria = new RandomCriteria();
			agentCriteria.setArcWeightSum(criteria.getArcWeightSum());
			agentCriteria.setNodeCount(criteria.getNumberOfNodes());
			agentCriteria.getInertia()[criteria.getFirstVariableIndex()] = criteria.getFirstVariableInitialValue();

			int nrSeries = 11;

			/*
			 * Matrix[] matrix = new Matrix[nrSeries]; Map<Double, Double[]>
			 * weightDistributions = new TreeMap<Double, Double[]>();
			 * Map<Double, Double[]> forceDistributions = new TreeMap<Double,
			 * Double[]>();
			 * 
			 * for (int i = 0; i < nrSeries; i++) { for (int run = 0; run <
			 * criteria.getNumberOfRuns(); run++) { // RandomGraphMaker<E>
			 * randomGraphMaker = new RandomGraphMaker<E>(agentCriteria);
			 * 
			 * // Graph<E> randomGraph =
			 * randomGraphMaker.createRandomGraphByAgentSimulation();
			 * 
			 * // matrix[i] = randomGraph.getSquareMatrix().getMatrix();
			 * 
			 * Map<Double, Double> weightDistribution =
			 * MatrixStatistics.getDistributionOfWeights(randomGraph,interval1);
			 * for (Double key : weightDistribution.keySet()) { Double[] values
			 * = weightDistributions.get(key); if (values == null) { values =
			 * new Double[nrSeries]; } values[i] = weightDistribution.get(key);
			 * weightDistributions.put(key, values); }
			 * 
			 * Map<Double, Double> forceDistribution =
			 * MatrixStatistics.getDistributionOfForces(randomGraph, interval2);
			 * for (Double key : forceDistribution.keySet()) { Double[] values =
			 * forceDistributions.get(key); if (values == null) { values = new
			 * Double[nrSeries]; } values[i] = forceDistribution.get(key);
			 * forceDistributions.put(key, values); } }
			 * 
			 * // Increment the variable.
			 * agentCriteria.getInertia()[criteria.getFirstVariableIndex()] =
			 * agentCriteria.getInertia()[criteria.getFirstVariableIndex()]
			 * criteria.getFirstVariableIntervalFactor();; }
			 * 
			 * // for (int i = 0; i < nrSeries; i++) { for (Double key :
			 * weightDistributions.keySet()) { Double[] values =
			 * weightDistributions.get(key); if (values[i] != null) { values[i]
			 * = values[i] / criteria.getNumberOfRuns(); } } for (Double key :
			 * forceDistributions.keySet()) { Double[] values =
			 * forceDistributions.get(key); if (values[i] != null) { values[i] =
			 * values[i] / criteria.getNumberOfRuns(); } } }
			 */

			// Compute statistics.
			// ==================
			if (criteria.isWeightDistributionChecked()) {
				//
				Map<Double, Double[]> weightDistributions = MatrixStatistics.getRandomDistributions(DistributionObject.WEIGHTS, null, agentCriteria,
						criteria.getNumberOfRuns(), nrSeries, criteria.getFirstVariableIndex(), criteria.getFirstVariableIntervalFactor());

				//
				result.outputs().appendln("Weight distributions");

				{
					//
					ReportChart chart = new ReportChart("Weight distributions", GraphType.LINES);
					chart.setLogarithmType(LogarithmType.VERTICAL);

					//
					double v1 = criteria.getFirstVariableInitialValue();
					Double[] keys = ToolBox.sort(weightDistributions.keySet());
					for (int i = 0; i < nrSeries; i++) {
						chart.setLineTitle(String.valueOf(MathUtils.round(v1, 8)), i);
						// chart.setHeader(String.valueOf(v1), i);

						//
						for (int j = 0; j < keys.length; j++) {
							Double key = keys[j];
							//
							Double value = weightDistributions.get(key)[i];
							if ((value != null) && (value > 0)) {
								chart.addValue(key, value, i);
							}
						}

						// Increment first variable for next iteration step.
						v1 = v1 * criteria.getFirstVariableIntervalFactor();
					}

					//
					result.outputs().appendln(chart);
				}

				{
					//
					ReportTable table = new ReportTable(weightDistributions.size() + 1, nrSeries + 1);

					//
					table.set(0, 0, " ");
					Double v = criteria.getFirstVariableInitialValue();
					for (int i = 0; i < nrSeries; i++) {
						table.set(0, i + 1, String.valueOf(v));
						v = v * criteria.getFirstVariableIntervalFactor();
					}

					//
					Double[] keys = ToolBox.sort(weightDistributions.keySet());
					for (int j = 0; j < keys.length; j++) {
						Double key = keys[j];
						table.set(j + 1, 0, key);
						for (int i = 0; i < nrSeries; i++) {
							Double value = weightDistributions.get(key)[i];
							if (value == null) {
								table.set(j + 1, i + 1, " ");
							} else {
								table.set(j + 1, i + 1, MathUtils.round(value, 4));
							}
						}
					}

					//
					result.outputs().appendln(table);
				}

				//
				result.outputs().appendln();
			}

			//
			if (criteria.isStrengthDistributionChecked()) {
				//
				Map<Double, Double[]> forceDistributions = MatrixStatistics.getRandomDistributions(DistributionObject.STRENGTHS, null, agentCriteria,
						criteria.getNumberOfRuns(), nrSeries, criteria.getFirstVariableIndex(), criteria.getFirstVariableIntervalFactor());

				//
				result.outputs().appendln("Forces Distributions");

				{
					//
					ReportChart chart = new ReportChart("Forces Distributions", GraphType.LINES);

					//
					double v1 = criteria.getFirstVariableInitialValue();
					Double[] keys = ToolBox.sort(forceDistributions.keySet());
					for (int i = 0; i < nrSeries; i++) {
						chart.setLineTitle(String.valueOf(MathUtils.round(v1, 8)), i);
						// chart.setHeader(String.valueOf(v1), i);

						//
						for (int j = 0; j < keys.length; j++) {
							Double key = keys[j];
							//
							Double value = forceDistributions.get(key)[i];
							if ((value != null) && (value > 0)) {
								chart.addValue(key, value, i);
							}
						}

						// Increment first variable for next iteration step.
						v1 = v1 * criteria.getFirstVariableIntervalFactor();
					}

					//
					result.outputs().appendln(chart);
				}
				{
					//
					ReportTable table = new ReportTable(forceDistributions.size() + 1, nrSeries + 1);

					//
					table.set(0, 0, " ");
					Double v = criteria.getFirstVariableInitialValue();
					for (int i = 0; i < nrSeries; i++) {
						table.set(0, i + 1, String.valueOf(v));
						v = v * criteria.getFirstVariableIntervalFactor();
					}

					//
					Double[] keys = ToolBox.sort(forceDistributions.keySet());
					for (int j = 0; j < keys.length; j++) {
						Double key = keys[j];
						table.set(j + 1, 0, key);
						for (int i = 0; i < nrSeries; i++) {
							Double value = forceDistributions.get(key)[i];
							if (value == null) {
								table.set(j + 1, i + 1, " ");
							} else {
								table.set(j + 1, i + 1, MathUtils.round(value, 4));
							}
						}
					}

					//
					result.outputs().appendln(table);
				}
				result.outputs().appendln();
			}

			//
			result.inputs().add("Number of Nodes", criteria.getNumberOfNodes());
			result.inputs().add("Number of Arcs", criteria.getArcWeightSum());
			result.inputs().add("Variation of variable", "i" + criteria.getFirstVariableIndex());

		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * 
	 * @param criteria
	 * @param runs
	 * @return
	 */
	public static <E> Report reportRandomAllianceNetworkByRandomDistribution(final RandomAllianceNetworkByRandomDistributionCriteria criteria,
			final MatrixStatistics source) {
		Report result;

		Chronometer chrono = new Chronometer();

		//
		result = new Report("Random Distibution generated Random Graph.");
		result.setOrigin("RandomGraphMaker.reportRandomAllianceNetworkByRandomDistribution()");
		result.setTarget("Statistics about Random Alliance Network by Random Distribution");

		//
		result.inputs().add("Number of nodes", criteria.getNumberOfNodes());
		result.inputs().add("Number of arcs", criteria.getNumberOfArcs());
		result.inputs().add("Distribution type", criteria.getDistributionType().toString());
		result.inputs().add("Power factor", criteria.getPowerFactor());
		result.inputs().add("Number of runs", criteria.getNumberOfRuns());
		result.inputs().add("Extract representative", criteria.isExtractRepresentative());

		//
		result.outputs().append(MatrixStatistics.statisticsRandom(source, null));

		//
		Map<Double, Double> weightsDistribution = MatrixStatistics.getDistribution(DistributionObject.WEIGHTS, source.getGraph());
		result.outputs().appendln("Weights Distributions");
		result.outputs().appendln(createDistributionChart("Weights Distributions", "Weights", weightsDistribution));
		result.outputs().appendln(createDistributionTable("Weights Distributions", "Weights", weightsDistribution));
		result.outputs().appendln();

		//
		Map<Double, Double> forcesDistribution = MatrixStatistics.getDistribution(DistributionObject.STRENGTHS, source.getGraph());
		result.outputs().appendln("Forces Distributions");
		result.outputs().appendln(createDistributionChart("Forces Distributions", "Forces", forcesDistribution));
		result.outputs().appendln(createDistributionTable("Forces Distributions", "Forces", forcesDistribution));
		result.outputs().appendln();

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * 
	 * @param criteria
	 * @param runs
	 * @return
	 */
	public static <E> Report reportRandomAllianceNetworkByRandomPermutation(final int runCount, final MatrixStatistics randStats,
			final MatrixStatistics sourceStats) {
		Report result;

		Chronometer chrono = new Chronometer();

		//
		result = new Report("Random Distibution generated Random Permutation.");
		result.setOrigin("RandomGraphMaker.reportRandomAllianceNetworkByRandomPermutation()");
		result.setTarget("Statistics about Random Alliance Network by Random Permutation");

		//
		result.inputs().add("Number of runs", runCount);

		//
		result.outputs().append(MatrixStatistics.statisticsRandom(randStats, sourceStats));

		//
		Map<Double, Double> weightsDistribution = MatrixStatistics.getDistribution(DistributionObject.WEIGHTS, randStats.getGraph());
		result.outputs().appendln("Weights Distributions");
		result.outputs().appendln(createDistributionChart("Weights Distributions", "Weights", weightsDistribution));
		result.outputs().appendln(createDistributionTable("Weights Distributions", "Weights", weightsDistribution));
		result.outputs().appendln();

		//
		Map<Double, Double> forcesDistribution = MatrixStatistics.getDistribution(DistributionObject.STRENGTHS, randStats.getGraph());
		result.outputs().appendln("Forces Distributions");
		result.outputs().appendln(createDistributionChart("Forces Distributions", "Forces", forcesDistribution));
		result.outputs().appendln(createDistributionTable("Forces Distributions", "Forces", forcesDistribution));
		result.outputs().appendln();

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * 
	 * @param criteria
	 * @param runs
	 * @return
	 */
	public static <E> Report reportRandomAllianceNetworkByReshuffling(final int runCount, final MatrixStatistics randStats, final MatrixStatistics sourceStats) {
		Report result;

		Chronometer chrono = new Chronometer();

		//
		result = new Report("Random Distibution generated Reshuffling");
		result.setOrigin("RandomGraphMaker.reportRandomAllianceNetworkByReshuffling()");
		result.setTarget("Statistics about Random Alliance Network by Reshuffling");

		//
		result.inputs().add("Number of runs", runCount);

		//
		result.outputs().append(MatrixStatistics.statisticsRandom(randStats, sourceStats));

		//
		Map<Double, Double> weightsDistribution = MatrixStatistics.getDistribution(DistributionObject.WEIGHTS, randStats.getGraph());
		result.outputs().appendln("Weights Distributions");
		result.outputs().appendln(createDistributionChart("Weights Distributions", "Weights", weightsDistribution));
		result.outputs().appendln(createDistributionTable("Weights Distributions", "Weights", weightsDistribution));
		result.outputs().appendln();

		//
		Map<Double, Double> forcesDistribution = MatrixStatistics.getDistribution(DistributionObject.STRENGTHS, randStats.getGraph());
		result.outputs().appendln("Forces Distributions");
		result.outputs().appendln(createDistributionChart("Forces Distributions", "Forces", forcesDistribution));
		result.outputs().appendln(createDistributionTable("Forces Distributions", "Forces", forcesDistribution));
		result.outputs().appendln();

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * 
	 * @param criteria
	 * @param runs
	 * @return
	 */
	/*
	 * public static <E> Report reportStatisticsRandomGraphByReshuffling(final
	 * int runCount, final MatrixStatistics source) { Report result;
	 * 
	 * Chronometer chrono = new Chronometer();
	 * 
	 * // result = new Report("Random Distibution generated Reshuffling.");
	 * result
	 * .setOrigin("RandomGraphMaker.reportStatisticsRandomGraphByReshuffling()"
	 * );
	 * result.setTarget("Statistics about Random Alliance Network by Reshuffling"
	 * );
	 * 
	 * // result.inputs().add("Number of runs", runCount);
	 * 
	 * // result.outputs().append(source.reportValues(source));
	 * 
	 * // Map<Double, Double> weightsDistribution =
	 * MatrixStatistics.getDistribution(DistributionObject.WEIGHTS,
	 * source.getGraph()); StringList lines = new StringList();
	 * lines.appendln("Weights Distributions"); Double[] keys =
	 * ToolBox.sort(weightsDistribution.keySet()); for (Double key : keys) {
	 * lines.append(key).append("\t"); } lines.appendln(); for (Double key :
	 * keys) { lines.append(weightsDistribution.get(key)).append("\t"); }
	 * lines.appendln(); result.outputs().appendln(lines);
	 * 
	 * // Map<Double, Double> forcesDistribution =
	 * MatrixStatistics.getDistribution(DistributionObject.FORCES,
	 * source.getGraph()); lines = new StringList();
	 * lines.appendln("Forces Distributions"); keys =
	 * ToolBox.sort(forcesDistribution.keySet()); for (Double key : keys) {
	 * lines.append(key).append("\t"); } lines.appendln(); for (Double key :
	 * keys) { lines.append(forcesDistribution.get(key)).append("\t"); }
	 * lines.appendln(); result.outputs().appendln(lines);
	 * 
	 * // result.setTimeSpent(chrono.stop().interval());
	 * 
	 * // return result; }
	 */

	/**
	 * Similar with reportRandomAllianceNetworkByAgentSimulation. Synonym:
	 * reportRandomAllianceNetworkByObserverSimulation.
	 * 
	 * @param criteria
	 * @param runs
	 * @return
	 */
	public static <E> Report reportVirtualFieldwork(final RandomCriteria criteria, final MatrixStatistics randStats, final MatrixStatistics sourceStats,
			final double[][] probaEvolution) {
		Report result;

		Chronometer chrono = new Chronometer();

		//
		result = new Report("Observer-based generated Random Graph.");
		result.setOrigin("RandomGraphMaker.reportStatisticsRandomGraphByObserverSimulation()");
		result.setTarget("Statistics about Random Alliance Network by Observer simulation");

		//
		result.inputs().add("Inertia Pattern", criteria.getInertiaPattern());
		result.inputs().add("Number of Nodes", criteria.getNodeCount());
		result.inputs().add("Maximal Number of Arcs", criteria.getArcWeightSum());
		result.inputs().add("Maximal Number of Runs", criteria.getRunCount());
		result.inputs().add("Probability of Outward Direction", criteria.getOutPreference());
		result.inputs().add("Show probability evolution", criteria.isShowProbabilityEvolution());

		//
		result.outputs().append(MatrixStatistics.statisticsRandom(randStats, sourceStats));
		/*
				//
				Map<Double, Double> weightsDistribution = MatrixStatistics.getRandomDistribution(DistributionObject.WEIGHTS, randStats.getGraph(), criteria, 1,
						criteria.getRunCount());
				result.outputs().appendln("Weights Distributions");
				result.outputs().appendln(createDistributionChart("Weights Distributions", "Weights", weightsDistribution));
				result.outputs().appendln(createDistributionTable("Weights Distributions", "Weights", weightsDistribution));
				result.outputs().appendln();

				//
				Map<Double, Double> forcesDistribution = MatrixStatistics.getRandomDistribution(DistributionObject.FORCES, randStats.getGraph(), criteria, 1,
						criteria.getRunCount());
				result.outputs().appendln("Forces Distributions");
				result.outputs().appendln(createDistributionChart("Forces Distributions", "Forces", forcesDistribution));
				result.outputs().appendln(createDistributionTable("Forces Distributions", "Forces", forcesDistribution));
				result.outputs().appendln();

				*/
		//
		if (criteria.isShowProbabilityEvolution()) {
			//
			result.outputs().appendln("Probability Evolution");
			ReportChart chart = StatisticsReporter.createArrayChart("Probability Evolution", probaEvolution,
					new String[] { "inertia0", "inertia1", "inertia2" });
			if (chart != null) {
				//
				result.outputs().appendln(chart);
				result.outputs().appendln(chart.createReportTable());
			}
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

	/**
	 * Synonym: reportRandomAllianceNetworkByObserverSimulationVariations.
	 * 
	 * @param criteria
	 * @return
	 */
	public static <E> Report reportVirtualFieldworkVariations(final Graph<Cluster<Individual>> source, final VirtualFieldworkVariationsCriteria criteria) {
		Report result;

		//
		Chronometer chrono = new Chronometer();

		//
		result = new Report("Random Graph Variation by observer");
		result.setOrigin("RandomGraphReporter.reportObserverStatistics()");
		result.setTarget(source.getLabel());

		//
		result.inputs().add("Number Of Loops", criteria.isNumberOfLoopsChecked());
		result.inputs().add("Number of circuits", criteria.isNumberOfCircuitsChecked());
		result.inputs().add("Number of triangles", criteria.isNumberOfTrianglesChecked());
		result.inputs().add("Concentration", criteria.isConcentrationChecked());
		result.inputs().add("weight distribution", criteria.isWeightDistributionChecked());
		result.inputs().add("Strength distributionChecked", criteria.isStrengthDistributionChecked());

		// Some values ​​could not be calculated before. It will be done now.
		criteria.nominalizeValues(source.nodeCount(), source.getArcWeightSum());

		result.inputs().add("Fraction of Nodes", criteria.getFractionOfNodes());
		result.inputs().add("Number of Nodes", criteria.getFractionOfNodes());
		result.inputs().add("Fraction of arc weights", criteria.getArcWeightFraction());
		result.inputs().add("Sum of arc weights", criteria.getArcWeightSum());
		result.inputs().add("Out preference", criteria.getOutPreference());
		result.inputs().add("Number of runs", criteria.getNumberOfRuns());

		result.inputs().add("First variable index", criteria.getFirstVariableIndex());
		result.inputs().add("First variable initial value", criteria.getFirstVariableInitialValue());
		result.inputs().add("First variable interval factor", criteria.getFirstVariableIntervalFactor());

		result.inputs().add("Second variable index", criteria.getSecondVariableIndex());
		result.inputs().add("Second variable initial value", criteria.getSecondVariableInitialValue());
		result.inputs().add("Second variable interval factor", criteria.getSecondVariableIntervalFactor());

		result.inputs().add("Variation of variable 1", "i" + criteria.getFirstVariableIndex());
		result.inputs().add("Variation of variable 2", "i" + criteria.getSecondVariableIndex());

		// ////////////////////////////////////////////////////////
		if ((criteria.isNumberOfLoopsChecked()) || (criteria.isNumberOfCircuitsChecked()) || (criteria.isNumberOfTrianglesChecked())
				|| (criteria.isConcentrationChecked() || (criteria.isSymmetryChecked()))) {

			MatrixStatistics sourceStatistics = GraphReporter.getMatrixStatistics(source);

			int n1 = 1 + criteria.getFirstVariableNumberOfIntervals() * 2;
			int n2 = 1 + criteria.getSecondVariableNumberOfIntervals() * 2;

			MatrixStatistics[][] statistics = new MatrixStatistics[n1][n2];

			RandomCriteria observerCriteria = new RandomCriteria();
			observerCriteria.setArcWeightSum(criteria.getArcWeightSum());
			observerCriteria.setNodeCount(criteria.getNumberOfNodes());
			observerCriteria.getInertia()[criteria.getFirstVariableIndex()] = criteria.getFirstVariableInitialValue();
			observerCriteria.getInertia()[criteria.getSecondVariableIndex()] = criteria.getSecondVariableInitialValue();

			for (int i = 0; i < n1; i++) {
				for (int j = 0; j < n2; j++) {
					System.out.println(observerCriteria.getInertia()[criteria.getFirstVariableIndex()] + " "
							+ observerCriteria.getInertia()[criteria.getSecondVariableIndex()]);
					RandomGraphMaker<Cluster<Individual>> observerRandomGraphMaker = new RandomGraphMaker<Cluster<Individual>>(observerCriteria);
					statistics[i][j] = GraphReporter.getMatrixStatistics(observerRandomGraphMaker.createRandomGraphsByObserverSimulation(source,
							criteria.getNumberOfRuns()));
					statistics[i][j].compareValues(sourceStatistics);
					observerCriteria.getInertia()[criteria.getSecondVariableIndex()] = observerCriteria.getInertia()[criteria.getSecondVariableIndex()]
							* criteria.getSecondVariableIntervalFactor();
				}
				observerCriteria.getInertia()[criteria.getFirstVariableIndex()] = observerCriteria.getInertia()[criteria.getFirstVariableIndex()]
						* criteria.getFirstVariableIntervalFactor();
				observerCriteria.getInertia()[criteria.getSecondVariableIndex()] = criteria.getSecondVariableInitialValue();
			}

			// Compute statistics.
			// ==================

			System.out.println("arcs: " + criteria.getArcWeightSum() + "\tnodes:" + criteria.getNumberOfNodes());

			//
			if (criteria.isNumberOfLoopsChecked()) {
				// Add title.
				result.outputs().appendln("Loop Overestimation");

				ReportChart chart;

				//
				chart = buildChartFromMatrixStatisticsTable("Loop Overestimation", GraphType.SURFACE, statistics, criteria.getFirstVariableInitialValue(),
						criteria.getFirstVariableIntervalFactor(), Indicator.LOOPS, Mode.OVERESTIMATION);
				result.outputs().append(chart);

				//
				chart = buildChartFromMatrixStatisticsTable("Loop Overestimation", GraphType.LINES, statistics, criteria.getFirstVariableInitialValue(),
						criteria.getFirstVariableIntervalFactor(), Indicator.LOOPS, Mode.OVERESTIMATION);
				result.outputs().append(chart);

				//
				ReportTable table = chart.createReportTable();
				table.set(0, 0, "i" + criteria.getFirstVariableIndex() + "/i" + criteria.getSecondVariableIndex());
				result.outputs().appendln(table);

				//
				result.outputs().appendln("Expected Loops Overestimation");

				// **
				ReportChart chart1a;

				//
				chart1a = buildChartFromMatrixStatisticsTable("Expected Loops Overestimation", GraphType.SURFACE, statistics,
						criteria.getFirstVariableInitialValue(), criteria.getFirstVariableIntervalFactor(), Indicator.LOOPS, Mode.OVERESTIMATION_EXPECTED);
				result.outputs().append(chart1a);

				//
				chart1a = buildChartFromMatrixStatisticsTable("Expected Loops Overestimation", GraphType.LINES, statistics,
						criteria.getFirstVariableInitialValue(), criteria.getFirstVariableIntervalFactor(), Indicator.LOOPS, Mode.OVERESTIMATION_EXPECTED);
				result.outputs().append(chart1a);

				//
				ReportTable table1a = chart1a.createReportTable();
				table1a.set(0, 0, "i" + criteria.getFirstVariableIndex() + "/i" + criteria.getSecondVariableIndex());
				result.outputs().appendln(table1a);

				//
				result.outputs().appendln("Loop Divergence Overestimation");

				// **
				ReportChart chart1;

				//
				chart1 = buildChartFromMatrixStatisticsTable("Loop Divergence Overestimation", GraphType.SURFACE, statistics,
						criteria.getFirstVariableInitialValue(), criteria.getFirstVariableIntervalFactor(), Indicator.LOOPS, Mode.OVERESTIMATION_DIVERGENCE);
				result.outputs().append(chart1);

				//
				chart1 = buildChartFromMatrixStatisticsTable("Loop Divergence Overestimation", GraphType.LINES, statistics,
						criteria.getFirstVariableInitialValue(), criteria.getFirstVariableIntervalFactor(), Indicator.LOOPS, Mode.OVERESTIMATION_DIVERGENCE);
				result.outputs().append(chart1);

				//
				ReportTable table1 = chart1.createReportTable();
				table1.set(0, 0, "i" + criteria.getFirstVariableIndex() + "/i" + criteria.getSecondVariableIndex());
				result.outputs().appendln(table1);

				//

			}

			//
			if (criteria.isNumberOfCircuitsChecked()) {
				//
				result.outputs().appendln("Circuit Overestimation");

				ReportChart chart;

				//
				chart = buildChartFromMatrixStatisticsTable("Circuit Overestimation", GraphType.SURFACE, statistics, criteria.getFirstVariableInitialValue(),
						criteria.getFirstVariableIntervalFactor(), Indicator.DUAL_CIRCUITS, Mode.OVERESTIMATION);
				result.outputs().append(chart);

				//
				chart = buildChartFromMatrixStatisticsTable("Circuit Overestimation", GraphType.LINES, statistics, criteria.getFirstVariableInitialValue(),
						criteria.getFirstVariableIntervalFactor(), Indicator.DUAL_CIRCUITS, Mode.OVERESTIMATION);
				result.outputs().append(chart);

				//
				ReportTable table = chart.createReportTable();
				table.set(0, 0, "i" + criteria.getFirstVariableIndex() + "/i" + criteria.getSecondVariableIndex());
				result.outputs().appendln(table);

				//
				result.outputs().appendln("Expected Circuits Overestimation");

				// **
				ReportChart chart1a;

				//
				chart1a = buildChartFromMatrixStatisticsTable("Expected Circuits Overestimation", GraphType.SURFACE, statistics,
						criteria.getFirstVariableInitialValue(), criteria.getFirstVariableIntervalFactor(), Indicator.DUAL_CIRCUITS,
						Mode.OVERESTIMATION_EXPECTED);
				result.outputs().append(chart1a);

				//
				chart1a = buildChartFromMatrixStatisticsTable("Expected Circuits Overestimation", GraphType.LINES, statistics,
						criteria.getFirstVariableInitialValue(), criteria.getFirstVariableIntervalFactor(), Indicator.DUAL_CIRCUITS,
						Mode.OVERESTIMATION_EXPECTED);
				result.outputs().append(chart1a);

				//
				ReportTable table1a = chart1a.createReportTable();
				table1a.set(0, 0, "i" + criteria.getFirstVariableIndex() + "/i" + criteria.getSecondVariableIndex());
				result.outputs().appendln(table1a);

				//
				result.outputs().appendln("Circuit Divergence Overestimation");

				//
				ReportChart chart1;

				//
				chart1 = buildChartFromMatrixStatisticsTable("Circuit Divergence Overestimation", GraphType.SURFACE, statistics,
						criteria.getFirstVariableInitialValue(), criteria.getFirstVariableIntervalFactor(), Indicator.DUAL_CIRCUITS,
						Mode.OVERESTIMATION_DIVERGENCE);
				result.outputs().append(chart1);

				//
				chart1 = buildChartFromMatrixStatisticsTable("Circuit Divergence Overestimation", GraphType.LINES, statistics,
						criteria.getFirstVariableInitialValue(), criteria.getFirstVariableIntervalFactor(), Indicator.DUAL_CIRCUITS,
						Mode.OVERESTIMATION_DIVERGENCE);
				result.outputs().append(chart1);

				//
				ReportTable table1 = chart1.createReportTable();
				table1.set(0, 0, "i" + criteria.getFirstVariableIndex() + "/i" + criteria.getSecondVariableIndex());
				result.outputs().appendln(table1);

				//

			}

			//
			if (criteria.isNumberOfTrianglesChecked()) {
				//
				result.outputs().appendln("Triangle Overestimation");

				ReportChart chart;

				//
				chart = buildChartFromMatrixStatisticsTable("Triangle Overestimation", GraphType.SURFACE, statistics, criteria.getFirstVariableInitialValue(),
						criteria.getFirstVariableIntervalFactor(), Indicator.TRIANGLES, Mode.OVERESTIMATION);
				result.outputs().append(chart);

				//
				chart = buildChartFromMatrixStatisticsTable("Triangle Overestimation", GraphType.LINES, statistics, criteria.getFirstVariableInitialValue(),
						criteria.getFirstVariableIntervalFactor(), Indicator.TRIANGLES, Mode.OVERESTIMATION);
				result.outputs().append(chart);

				//
				ReportTable table = chart.createReportTable();
				table.set(0, 0, "i" + criteria.getFirstVariableIndex() + "/i" + criteria.getSecondVariableIndex());
				result.outputs().appendln(table);
			}

			//
			if (criteria.isConcentrationChecked()) {
				//
				result.outputs().appendln("Concentration Overestimation");

				ReportChart chart;

				//
				chart = buildChartFromMatrixStatisticsTable("Concentration Overestimation", GraphType.SURFACE, statistics,
						criteria.getFirstVariableInitialValue(), criteria.getFirstVariableIntervalFactor(), Indicator.CONCENTRATION, Mode.OVERESTIMATION);
				result.outputs().append(chart);

				//
				chart = buildChartFromMatrixStatisticsTable("Concentration Overestimation", GraphType.LINES, statistics,
						criteria.getFirstVariableInitialValue(), criteria.getFirstVariableIntervalFactor(), Indicator.CONCENTRATION, Mode.OVERESTIMATION);
				result.outputs().append(chart);

				//
				ReportTable table = chart.createReportTable();
				table.set(0, 0, "i" + criteria.getFirstVariableIndex() + "/i" + criteria.getSecondVariableIndex());
				result.outputs().appendln(table);
				//

				//
				result.outputs().appendln("Expected Concentration Overestimation");

				// **
				ReportChart chart1a;

				//
				chart1a = buildChartFromMatrixStatisticsTable("Expected Concentration Overestimation", GraphType.SURFACE, statistics,
						criteria.getFirstVariableInitialValue(), criteria.getFirstVariableIntervalFactor(), Indicator.CONCENTRATION,
						Mode.OVERESTIMATION_EXPECTED);
				result.outputs().append(chart1a);

				//
				chart1a = buildChartFromMatrixStatisticsTable("Expected Concentration Overestimation", GraphType.LINES, statistics,
						criteria.getFirstVariableInitialValue(), criteria.getFirstVariableIntervalFactor(), Indicator.CONCENTRATION,
						Mode.OVERESTIMATION_EXPECTED);
				result.outputs().append(chart1a);

				//
				ReportTable table1a = chart1a.createReportTable();
				table1a.set(0, 0, "i" + criteria.getFirstVariableIndex() + "/i" + criteria.getSecondVariableIndex());
				result.outputs().appendln(table1a);

				//
				result.outputs().appendln("Concentration Divergence Overestimation");

				ReportChart chart1;

				//
				chart1 = buildChartFromMatrixStatisticsTable("Concentration Divergence Overestimation", GraphType.SURFACE, statistics,
						criteria.getFirstVariableInitialValue(), criteria.getFirstVariableIntervalFactor(), Indicator.CONCENTRATION,
						Mode.OVERESTIMATION_DIVERGENCE);
				result.outputs().append(chart1);

				//
				chart1 = buildChartFromMatrixStatisticsTable("Concentration Divergence Overestimation", GraphType.LINES, statistics,
						criteria.getFirstVariableInitialValue(), criteria.getFirstVariableIntervalFactor(), Indicator.CONCENTRATION,
						Mode.OVERESTIMATION_DIVERGENCE);
				result.outputs().append(chart1);

				//
				ReportTable table1 = chart1.createReportTable();
				table1.set(0, 0, "i" + criteria.getFirstVariableIndex() + "/i" + criteria.getSecondVariableIndex());
				result.outputs().appendln(table1);
			}

			//
			if (criteria.isSymmetryChecked()) {
				//
				result.outputs().appendln("Symmetry Overestimation");

				ReportChart chart;

				//
				chart = buildChartFromMatrixStatisticsTable("Symmetry Overestimation", GraphType.SURFACE, statistics, criteria.getFirstVariableInitialValue(),
						criteria.getFirstVariableIntervalFactor(), Indicator.SYMMETRY, Mode.OVERESTIMATION);
				result.outputs().append(chart);

				//
				chart = buildChartFromMatrixStatisticsTable("Symmetry Overestimation", GraphType.LINES, statistics, criteria.getFirstVariableInitialValue(),
						criteria.getFirstVariableIntervalFactor(), Indicator.SYMMETRY, Mode.OVERESTIMATION);
				result.outputs().append(chart);

				//
				ReportTable table = chart.createReportTable();
				table.set(0, 0, "i" + criteria.getFirstVariableIndex() + "/i" + criteria.getSecondVariableIndex());
				result.outputs().appendln(table);
				//
				result.outputs().appendln("Symmetry Divergence Overestimation");

				ReportChart chart1;

				//
				chart1 = buildChartFromMatrixStatisticsTable("Symmetry Divergence Overestimation", GraphType.SURFACE, statistics,
						criteria.getFirstVariableInitialValue(), criteria.getFirstVariableIntervalFactor(), Indicator.SYMMETRY, Mode.OVERESTIMATION_DIVERGENCE);
				result.outputs().append(chart1);

				//
				chart1 = buildChartFromMatrixStatisticsTable("Symmetry Divergence Overestimation", GraphType.LINES, statistics,
						criteria.getFirstVariableInitialValue(), criteria.getFirstVariableIntervalFactor(), Indicator.SYMMETRY, Mode.OVERESTIMATION_DIVERGENCE);
				result.outputs().append(chart1);

				//
				ReportTable table1 = chart1.createReportTable();
				table1.set(0, 0, "i" + criteria.getFirstVariableIndex() + "/i" + criteria.getSecondVariableIndex());
				result.outputs().appendln(table1);
			}

			//
			if (criteria.isStrengthConcentrationChecked()) {
				//
				result.outputs().appendln("Strength Concentration Overestimation");

				ReportChart chart;

				//
				chart = buildChartFromMatrixStatisticsTable("Strength Concentration Overestimation", GraphType.SURFACE, statistics,
						criteria.getFirstVariableInitialValue(), criteria.getFirstVariableIntervalFactor(), Indicator.STRENGTH_CONCENTRATION,
						Mode.OVERESTIMATION);
				result.outputs().append(chart);

				//
				chart = buildChartFromMatrixStatisticsTable("Strength Concentration Overestimation", GraphType.LINES, statistics,
						criteria.getFirstVariableInitialValue(), criteria.getFirstVariableIntervalFactor(), Indicator.STRENGTH_CONCENTRATION,
						Mode.OVERESTIMATION);
				result.outputs().append(chart);

				//
				ReportTable table = chart.createReportTable();
				table.set(0, 0, "i" + criteria.getFirstVariableIndex() + "/i" + criteria.getSecondVariableIndex());
				result.outputs().appendln(table);
				//
			}

			//
			/*			if (criteria.isSymmetryChecked()) {
							//
							result.outputs().appendln("Symmetry Overestimation");

							{
								//
								ReportChart chart = new ReportChart("Symmetry Overestimation", GraphType.SURFACE);

								//
								double v1 = criteria.getFirstVariableInitialValue();
								for (int i = 0; i < n1; i++) {
									chart.setLineTitle(String.valueOf(Mat.round(v1, 8)), i);
									chart.setHeader(String.valueOf(v1), i);

									//
									for (int j = 0; j < n2; j++) {
										//
										chart.setValue(Mat.round(statistics[i][j].getOverestimationOfSymmetry(), 4), i, j);
									}

									// Increment first variable for next iteration step.
									v1 = v1 * criteria.getFirstVariableIntervalFactor();
								}
								result.outputs().append(chart);
							}

							//
							ReportChart chart = new ReportChart("Symmetry Overestimation", GraphType.LINES);

							//
							double v1 = criteria.getFirstVariableInitialValue();
							for (int i = 0; i < n1; i++) {
								chart.setLineTitle(String.valueOf(Mat.round(v1, 8)), i);
								chart.setHeader(String.valueOf(v1), i);

								//
								for (int j = 0; j < n2; j++) {
									//
									chart.setValue(Mat.round(statistics[i][j].getOverestimationOfSymmetry(), 4), i, j);
								}

								// Increment first variable for next iteration step.
								v1 = v1 * criteria.getFirstVariableIntervalFactor();
							}
							result.outputs().appendln(chart);

							//
							ReportTable table = chart.createReportTable();
							table.set(0, 0, "i" + criteria.getFirstVariableIndex() + "/i" + criteria.getSecondVariableIndex());
							result.outputs().appendln(table);
						}*/

		}

		// ////////////////////////////////////////////////////////
		if ((criteria.isWeightDistributionChecked()) || (criteria.isStrengthDistributionChecked())) {
			int nrSeries = 12;
			// Matrix[] matrix = new Matrix[nrSeries];

			/*
			 * Map<Double, Double[]> weightDistributions = new TreeMap<Double,
			 * Double[]>(); Map<Double, Double[]> forceDistributions = new
			 * TreeMap<Double, Double[]>();
			 * 
			 * for (int i = 0; i < n; i++) { for (int run = 0; run <
			 * criteria.getNumberOfRuns(); run++) {
			 * 
			 * Graph randomGraph = source; if (i == 0) { matrix[i] =
			 * source.getSquareMatrix().getMatrix(); } else {
			 * RandomGraphMaker<E> randomGraphMaker = new
			 * RandomGraphMaker<E>(observerCriteria); randomGraph =
			 * randomGraphMaker.createRandomGraphByAgentSimulation(); matrix[i]
			 * = randomGraph.getSquareMatrix().getMatrix(); }
			 * 
			 * Map<Double, Double> weightDistribution =
			 * MatrixStatistics.getDistributionOfWeights(randomGraph,interval1);
			 * for (Double key : weightDistribution.keySet()) { Double[] values
			 * = weightDistributions.get(key); if (values == null) { values =
			 * new Double[n]; } values[i] = weightDistribution.get(key);
			 * weightDistributions.put(key, values); }
			 * 
			 * Map<Double, Double> forceDistribution =
			 * MatrixStatistics.getDistributionOfForces(randomGraph,interval2);
			 * for (Double key : forceDistribution.keySet()) { Double[] values =
			 * forceDistributions.get(key); if (values == null) { values = new
			 * Double[n]; } values[i] = forceDistribution.get(key);
			 * forceDistributions.put(key, values); } }
			 * 
			 * if (i > 0) {
			 * observerCriteria.getInertia()[criteria.getFirstVariableIndex()] =
			 * observerCriteria.getInertia()[criteria.getFirstVariableIndex()]
			 * criteria.getFirstVariableIntervalFactor(); } }
			 * 
			 * for (int i = 0; i < n; i++) { for (Double key :
			 * weightDistributions.keySet()) { Double[] values =
			 * weightDistributions.get(key); if (values[i] != null) { values[i]
			 * = values[i] / criteria.getNumberOfRuns(); } } for (Double key :
			 * forceDistributions.keySet()) { Double[] values =
			 * forceDistributions.get(key); if (values[i] != null) { values[i] =
			 * values[i] / criteria.getNumberOfRuns(); } } }
			 */

			// Compute statistics.
			// ==================
			if (criteria.isWeightDistributionChecked()) {
				//
				RandomCriteria observerCriteria = new RandomCriteria();
				observerCriteria.setArcWeightSum(criteria.getArcWeightSum());
				observerCriteria.setNodeCount(criteria.getNumberOfNodes());
				observerCriteria.getInertia()[criteria.getFirstVariableIndex()] = criteria.getFirstVariableInitialValue();

				//
				Map<Double, Double[]> weightDistributions;

				weightDistributions = MatrixStatistics.getRandomDistributions(DistributionObject.WEIGHTS, source, observerCriteria, criteria.getNumberOfRuns(),
						nrSeries, criteria.getFirstVariableIndex(), criteria.getFirstVariableIntervalFactor());

				//
				result.outputs().appendln("Weight distributions");

				{
					//
					ReportChart chart = new ReportChart("Weight distributions", GraphType.LINES);
					chart.setLogarithmType(LogarithmType.VERTICAL);

					//
					double v1 = criteria.getFirstVariableInitialValue();
					Double[] keys = ToolBox.sort(weightDistributions.keySet());
					for (int i = 0; i < nrSeries; i++) {
						if (i == 0) {
							chart.setLineTitle("Orig.", i);
						} else {
							chart.setLineTitle(String.valueOf(MathUtils.round(v1, 8)), i);
						}

						//
						for (int j = 0; j < keys.length; j++) {
							Double key = keys[j];
							//
							Double value = weightDistributions.get(key)[i];
							if ((value != null) && (value > 0)) {
								chart.addValue(key, value, i);
							}
						}

						// Increment first variable for next iteration step.
						v1 = v1 * criteria.getFirstVariableIntervalFactor();
					}

					//
					result.outputs().appendln(chart);
				}

				{
					//
					ReportTable table = new ReportTable(weightDistributions.size() + 1, nrSeries + 2);

					//
					table.set(0, 0, " ");
					table.set(0, 1, "Orig.");

					Double v = criteria.getFirstVariableInitialValue();
					for (int i = 1; i < nrSeries; i++) {
						table.set(0, i + 2, String.valueOf(MathUtils.round(v, 8)));
						v = v * criteria.getFirstVariableIntervalFactor();
					}

					//
					Double[] keys = ToolBox.sort(weightDistributions.keySet());
					for (int j = 0; j < keys.length; j++) {
						Double key = keys[j];
						table.set(j + 1, 0, key);
						for (int i = 0; i < nrSeries; i++) {
							Double value = weightDistributions.get(key)[i];
							if (value == null) {
								table.set(j + 1, i + 1, " ");
							} else {
								table.set(j + 1, i + 1, MathUtils.round(value, 4));
							}
						}
					}

					//
					result.outputs().appendln(table);
				}

				//
				result.outputs().appendln();
			}

			//
			if (criteria.isStrengthDistributionChecked()) {
				//
				RandomCriteria observerCriteria = new RandomCriteria();
				observerCriteria.setArcWeightSum(criteria.getArcWeightSum());
				observerCriteria.setNodeCount(criteria.getNumberOfNodes());
				observerCriteria.getInertia()[criteria.getFirstVariableIndex()] = criteria.getFirstVariableInitialValue();

				//
				Map<Double, Double[]> strengthDistributions = MatrixStatistics.getRandomDistributions(DistributionObject.STRENGTHS, source, observerCriteria,
						criteria.getNumberOfRuns(), nrSeries, criteria.getFirstVariableIndex(), criteria.getFirstVariableIntervalFactor());

				//
				result.outputs().appendln("Force distributions");

				{
					//
					ReportChart chart = new ReportChart("Force distributions", GraphType.LINES);

					//
					double v1 = criteria.getFirstVariableInitialValue();
					Double[] keys = ToolBox.sort(strengthDistributions.keySet());
					for (int i = 0; i < nrSeries; i++) {
						if (i == 0) {
							chart.setLineTitle("Orig.", i);
						} else {
							chart.setLineTitle(String.valueOf(MathUtils.round(v1, 8)), i);
						}

						//
						for (int j = 0; j < keys.length; j++) {
							Double key = keys[j];
							//
							Double value = strengthDistributions.get(key)[i];
							if ((value != null) && (value > 0)) {
								chart.addValue(key, value, i);
							}
						}

						// Increment first variable for next iteration step.
						v1 = v1 * criteria.getFirstVariableIntervalFactor();
					}

					//
					result.outputs().appendln(chart);
				}

				{
					//
					ReportTable table = new ReportTable(strengthDistributions.size() + 1, nrSeries + 2);

					//
					table.set(0, 0, " ");
					table.set(0, 1, "Orig.");
					Double v = criteria.getFirstVariableInitialValue();
					for (int i = 1; i < nrSeries; i++) {
						table.set(0, i + 2, String.valueOf(MathUtils.round(v, 8)));
						v = v * criteria.getFirstVariableIntervalFactor();
					}

					//
					Double[] keys = ToolBox.sort(strengthDistributions.keySet());
					for (int j = 0; j < keys.length; j++) {
						Double key = keys[j];
						table.set(j + 1, 0, key);
						for (int i = 0; i < nrSeries; i++) {
							Double value = strengthDistributions.get(key)[i];
							if (value == null) {
								table.set(j + 1, i + 1, " ");
							} else {
								table.set(j + 1, i + 1, MathUtils.round(value, 4));
							}
						}
					}

					//
					result.outputs().appendln(table);
				}

				result.outputs().appendln();
			}
		}

		//
		result.setTimeSpent(chrono.stop().interval());

		//
		return result;
	}

}
