package org.tip.puckgui.views.geographyEditor;

import javax.swing.table.AbstractTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.FlatDB4GeoNames;
import org.tip.flatdb4geonames.model.GeoNamesLine;
import org.tip.flatdb4geonames.model.GeoNamesLines;
import org.tip.flatdb4geonames.model.index.Feature;

/**
 * 
 * @author TIP
 */
public class GeoNamesLineModel extends AbstractTableModel {

	private static final long serialVersionUID = 5317797871989491068L;

	private static final Logger logger = LoggerFactory.getLogger(GeoNamesLineModel.class);

	public static final int COLUMN_GEONAMEID = 0;
	public static final int COLUMN_TOPONYM = 1;
	public static final int COLUMN_ALTERNATE_NAMES = 2;
	public static final int COLUMN_LATITUDE = 3;
	public static final int COLUMN_LONGITUDE = 4;
	public static final int COLUMN_ELEVATION = 5;
	public static final int COLUMN_FEATURE = 6;
	public static final int COLUMN_ADMINCODES = 7;
	public static final int COLUMN_POPULATION = 8;
	public static final int COLUMN_TIMEZONE = 9;
	public static final int COLUMN_COUNT = 10;

	private GeoNamesLines source;
	private int lastSearchIndex;
	private String lastSearchPattern;

	/**
	 * 
	 */
	public GeoNamesLineModel(final GeoNamesLines source) {
		//
		super();

		//
		setSource(source);
	}

	/**
	 * 
	 */
	@Override
	public Class getColumnClass(final int columnIndex) {
		Class result;

		switch (columnIndex) {
			case COLUMN_GEONAMEID:
				result = Number.class;
			break;
			case COLUMN_TOPONYM:
				result = String.class;
			break;
			case COLUMN_ALTERNATE_NAMES:
				result = String.class;
			break;
			case COLUMN_LATITUDE:
				result = Double.class;
			break;
			case COLUMN_LONGITUDE:
				result = Double.class;
			break;
			case COLUMN_ELEVATION:
				result = Double.class;
			break;
			case COLUMN_FEATURE:
				result = String.class;
			break;
			case COLUMN_ADMINCODES:
				result = String.class;
			break;
			case COLUMN_POPULATION:
				result = Long.class;
			break;
			case COLUMN_TIMEZONE:
				result = String.class;
			break;
			default:
				result = String.class;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getColumnCount() {
		int result;

		result = COLUMN_COUNT;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public String getColumnName(final int columnIndex) {
		String result;

		switch (columnIndex) {
			case COLUMN_GEONAMEID:
				result = "GeoNameId";
			break;

			case COLUMN_TOPONYM:
				result = "Toponym";
			break;

			case COLUMN_ALTERNATE_NAMES:
				result = "Alternates Toponyms";
			break;

			case COLUMN_LATITUDE:
				result = "Latitude";
			break;

			case COLUMN_LONGITUDE:
				result = "Longitude";
			break;

			case COLUMN_ELEVATION:
				result = "Elevation";
			break;

			case COLUMN_FEATURE:
				result = "Feature";
			break;

			case COLUMN_ADMINCODES:
				result = "Admin";
			break;

			case COLUMN_POPULATION:
				result = "Population";
			break;

			case COLUMN_TIMEZONE:
				result = "TimeZone";
			break;

			default:
				result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getRowCount() {
		int result;

		if (this.source == null) {

			result = 0;

		} else {

			result = this.source.size();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public GeoNamesLines getSource() {
		GeoNamesLines result;

		result = this.source;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Object getValueAt(final int rowIndex, final int columnIndex) {
		Object result;

		GeoNamesLine line = this.source.get(rowIndex);

		switch (columnIndex) {
			case COLUMN_GEONAMEID:
				result = line.getGeoNameId();
			break;

			case COLUMN_TOPONYM:
				result = line.getName();
			break;

			case COLUMN_ALTERNATE_NAMES:
				result = line.getAlternateNames().toStringList().sort().toStringSeparatedBy(";");
			break;

			case COLUMN_LATITUDE:
				result = line.getLatitude();
			break;

			case COLUMN_LONGITUDE:
				result = line.getLongitude();
			break;

			case COLUMN_ELEVATION:
				result = line.getElevation();
			break;

			case COLUMN_FEATURE:
				if (line.getFeatureClass() == null) {

					result = null;

				} else {
					if (FlatDB4GeoNames.isOpened()) {

						result = FlatDB4GeoNames.instance().searchFeatureShortDescription(line.getFeatureCodePath());

					} else {

						result = Feature.convertToFeaturePath(line.getFeatureClass().getCode(), line.getFeatureCode());
					}

				}
			break;

			case COLUMN_ADMINCODES:
				result = line.getAdministrativeCodePath();
			break;

			case COLUMN_POPULATION:
				result = line.getPopulation();
			break;

			case COLUMN_TIMEZONE:
				result = line.getTimeZone();
			break;

			default:
				result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public int indexOf(final GeoNamesLine target) {
		int result;

		if (this.source == null) {
			//
			result = -1;

		} else {

			result = this.source.indexOf(target);
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public boolean isCellEditable(final int row, final int col) {
		boolean result;

		switch (col) {
			default:
				result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isSet() {
		boolean result;

		if (this.source == null) {

			result = false;
		} else {

			result = true;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param pattern
	 */
	public GeoNamesLine nextSearched(final String pattern) {
		GeoNamesLine result;

		result = null;

		//
		return result;
	}

	/**
	 * 
	 * @param pattern
	 */
	public int nextSearchedIndex(final String pattern) {
		int result;

		result = indexOf(nextSearched(pattern));

		//
		return result;
	}

	/**
	 * 
	 */
	public void resetSearch() {
		this.lastSearchIndex = -1;
		this.lastSearchPattern = null;
	}

	/**
	 * 
	 * @param source
	 */
	public void setSource(final GeoNamesLines source) {
		//
		resetSearch();

		// Set an empty JTable.
		if (this.source != null) {
			//
			fireTableRowsDeleted(0, this.source.size());
		}

		//
		if (source == null) {
			//
			this.source = new GeoNamesLines();

		} else {
			//
			this.source = source;
		}

		//
		fireTableDataChanged();
	}
}
