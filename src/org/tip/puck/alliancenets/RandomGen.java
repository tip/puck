package org.tip.puck.alliancenets;

import org.tip.puck.evo.Generator;
import org.tip.puck.util.RandomGenerator;


/**
 * @author Telmo Menezes
 *
 */
public class RandomGen extends Generator {
    // generated alliance net
    private AllianceNet allianceNet;
    
    // topological indices
    private TopologicalIndices indices;
    private TopologicalIndices targetIndices;
    
    public RandomGen(int nodeCount, int edgeCount, TopologicalIndices targetIndices) {
        super(nodeCount, edgeCount);
        
        this.targetIndices = targetIndices;
        indices = null;
    }

    @Override
    public void createProgSet() {
        progcount = 0;
        progset = null;
    }

    @Override
    public void run() {
        allianceNet = new AllianceNet(true);

        // create nodes
        Group[] nodeArray = new Group[nodeCount];

        for (int i = 0; i < nodeCount; i++) {
            nodeArray[i] = allianceNet.addNodeWithId(i);
        }

        // create edges
        int n = (int)targetIndices.getN();
        for (int i = 0; i < n; i++) {   
        	int origIndex = RandomGenerator.instance().random.nextInt(nodeCount);
        	int targIndex = RandomGenerator.instance().random.nextInt(nodeCount);
        	
            Group origNode = nodeArray[origIndex];
            Group targNode = nodeArray[targIndex];

            Alliance alliance = allianceNet.getEdge(origNode, targNode);
            if (alliance == null) {
                allianceNet.addEdge(origNode, targNode, i);
                alliance = allianceNet.getEdge(origNode, targNode);
            }

            alliance.setWeight(alliance.getWeight() + 1.0);
        }
    }
    
    public double distance(Generator generator) {
        return 0;
    }
    
    @Override
    public Generator clone() {
        return new RandomGen(nodeCount, edgeCount, targetIndices);
    }

    public TopologicalIndices getIndices() {
        return indices;
    }

    public void setIndices(TopologicalIndices indices) {
        this.indices = indices;
    }

    public TopologicalIndices getTargetIndices() {
        return targetIndices;
    }

    public void setTargetIndices(TopologicalIndices targetIndices) {
        this.targetIndices = targetIndices;
    }

    public AllianceNet getNet() {
        return allianceNet;
    }
}