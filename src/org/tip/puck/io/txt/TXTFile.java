package org.tip.puck.io.txt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.io.BARIURDetector;
import org.tip.puck.io.bar.BARTXTFile;
import org.tip.puck.io.iur.IURTXTFile;
import org.tip.puck.net.Net;
import org.tip.puck.util.PuckUtils;

import fr.devinsy.util.StringList;

/**
 * This class represents a TXT File reader and writer.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class TXTFile {

	private static final Logger logger = LoggerFactory.getLogger(TXTFile.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	public static final int MAX_LINE_SIZE = 2048;

	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file) throws PuckException {
		Net result;

		result = load(file, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static Net load(final File file, final String charsetName) throws PuckException {
		Net result;

		//
		BARIURDetector.Format format = BARIURDetector.detectFormat(file);

		//
		switch (format) {
			case BAR:
				result = BARTXTFile.load(file, DEFAULT_CHARSET_NAME);
			break;

			case IUR:
				result = IURTXTFile.load(file, DEFAULT_CHARSET_NAME);
			break;

			case CONFLICTING:
				throw PuckExceptions.FORMAT_CONFLICT.create();

			case UNKNOWN:
				throw PuckExceptions.BAD_FILE_FORMAT.create("Unknown format.");

			default:
				result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @return
	 */
	public static List<StringList> readLineBlocks(final File file, final String charsetName) {
		List<StringList> result;

		BufferedReader in = null;
		try {
			//
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), charsetName));
			result = PuckUtils.readLineBlocks(in);

		} catch (Exception exception) {
			//
			exception.printStackTrace();
			result = null;

		} finally {
			//
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @return
	 */
	public static StringList readLines(final File file, final String charsetName) {
		StringList result;

		BufferedReader in = null;
		try {
			//
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), charsetName));
			result = PuckUtils.readLines(in);

		} catch (Exception exception) {
			//
			exception.printStackTrace();
			result = null;

		} finally {
			//
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File file, final Net source) throws PuckException {

		IURTXTFile.save(file, source);
	}
}
