package org.tip.puck.visualization.style.implementations;

import org.gephi.data.attributes.api.AttributeModel;
import org.gephi.data.attributes.api.AttributeOrigin;
import org.gephi.data.attributes.api.AttributeTable;
import org.gephi.data.attributes.api.AttributeType;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.Node;
import org.gephi.layout.api.LayoutController;
import org.gephi.layout.plugin.force.StepDisplacement;
import org.gephi.layout.spi.Layout;
import org.gephi.preview.api.PreviewController;
import org.gephi.preview.api.PreviewModel;
import org.gephi.preview.api.PreviewProperty;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;
import org.tip.gephiplugins.genealogicalrenderers.DashedEdgeRenderer;
import org.tip.gephiplugins.genealogicalrenderers.NodeShapeRenderer;
import org.tip.gephiplugins.genealogicalrenderers.shape.plugins.DefaultShapes;
import org.tip.puck.graphs.Link;
import org.tip.puck.visualization.layouts.hierarchical.HierarchicalLayout;
import org.tip.puck.visualization.layouts.simpa.SimPaLayoutUpdated;
import org.tip.puck.visualization.layouts.sugiyama.SugiyamaLayout;
import org.tip.puck.visualization.style.Style;
import org.tip.puck.visualization.style.attributes.AttributeMap;
import org.tip.puck.visualization.style.attributes.GraphStyle;

/**
 * @date May 13, 2012
 *
 * @author Edoardo Savoia
 */
public class OreGraph implements Style {

    public static final String NODE_SHAPE_PROP = NodeShapeRenderer.BOUND_PROP_DEFAULT;
    public static final String NODE_DEFAULT_SHAPE = DefaultShapes.diamond.name();
    /*
     * Ore-graph: non-valued mixed graph (arcs and eges) with 3 types of
     * vertices
     *
     * Edges for marriages, Arcs for parent-child links (directed/undirected),
     *
     * Triangles for men, Circles for women, Diamonds for unknown sex (gender),
     *
     * Colors for group affiliation (done by layout),
     *
     * Layers according to generation (generation)
     *
     */
   
    private final AttributeMap map = new AttributeMap() {

	@Override
	public void setAttributes(Node gephiNode, org.tip.puck.graphs.Node<?> graphNode) {
	    String shape = graphNode.getTag();
	    if (shape.equalsIgnoreCase("ellipse")) {
		shape = "circle";
	    }
	    gephiNode.getNodeData().getAttributes().setValue(NODE_SHAPE_PROP.toLowerCase(), shape);
	}

	@Override
	public void setAttributes(Edge gephiEdge, Link<?> graphEdge) {
	}

	@Override
	public void registerAttributes(AttributeModel attributeModel) {
	    AttributeTable table = attributeModel.getNodeTable();
	    table.addColumn(NODE_SHAPE_PROP.toLowerCase(),
		    NODE_SHAPE_PROP,
		    AttributeType.STRING,
		    AttributeOrigin.DATA,
		    NODE_DEFAULT_SHAPE);
	}
    };

    @Override
    public GraphStyle graphStyle() {
	return GraphStyle.mixed;

    }

    @Override
    public AttributeMap styleMapping() {
	return map;
    }

    @Override
    public void applyStyle(Workspace workspace) {

	layout(workspace);
	
	GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getModel(workspace);
	
	
	PreviewModel previewModel = Lookup.getDefault().lookup(PreviewController.class).getModel(workspace);

	previewModel.getProperties().putValue(PreviewProperty.SHOW_NODE_LABELS, Boolean.FALSE);
	previewModel.getProperties().putValue(PreviewProperty.EDGE_CURVED, Boolean.FALSE);

	// Plugin config
	previewModel.getProperties().putValue(NodeShapeRenderer.ENABLE_SHAPES, Boolean.TRUE);
	previewModel.getProperties().putValue(NodeShapeRenderer.MINSIZE_PROP_NAME, 30f);

	PreviewController previewController = Lookup.getDefault().lookup(PreviewController.class);
	previewController.refreshPreview(workspace);

    }

    
    private void layout(Workspace workspace) {
	// Layout
	GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getModel(workspace);

	LayoutController lookup = Lookup.getDefault().lookup(LayoutController.class);
	
	
	// Layout buildLayout = new HierarchicalLayout(null);
	// SimPaLayoutUpdated buildLayout = new SimPaLayoutUpdated(null, new StepDisplacement(1f));
	Layout buildLayout = new SugiyamaLayout(null);
	buildLayout.setGraphModel(graphModel);
	buildLayout.resetPropertiesValues();
	buildLayout.initAlgo();
	buildLayout.goAlgo();
	buildLayout.endAlgo();

    }
}
