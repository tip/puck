package org.tip.puck.net.relations.roles.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.io.ods.ODSBufferedReader;
import org.tip.puck.io.xls.XLSBufferedReader;
import org.tip.puck.net.Attributes;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.roles.RoleRelationMaker;
import org.tip.puck.net.relations.roles.RoleRelationRules;
import org.tip.puck.report.Report;

import fr.devinsy.util.StringList;

/**
 * This class manages terminology file in format "Standard".
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class TermStandardFile {

	private static Logger logger = LoggerFactory.getLogger(TermStandardFile.class);

	public static String DEFAULT_CHARSET_NAME = "UTF-8";

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel load(final File source, final Report report, final RoleRelationRules ruleConstraints) throws PuckException {
		RelationModel result;

		if (source == null) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("File=[" + source + "].");

		} else if (!source.exists()) {
			//
			throw PuckExceptions.FILE_NOT_FOUND.create("File=[" + source + "].");

		} else if (!source.isFile()) {
			//
			throw PuckExceptions.NOT_A_FILE.create("File=[" + source + "].");

		} else {

			String name = StringUtils.lowerCase(source.getName());

			if (name.endsWith(".term.ods")) {
				//
				result = loadFromTermODSFile(source, report, ruleConstraints);

			} else if (name.endsWith(".term.txt")) {
				//
				result = loadFromTermTXTFile(source, report, ruleConstraints);

			} else if (name.endsWith(".term.xls")) {
				//
				result = loadFromTermXLSFile(source, report, ruleConstraints);

			} else {
				//
				throw PuckExceptions.UNSUPPORTED_FILE_FORMAT.create("File=[" + source + "].");
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @param report
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel loadFromTermODSFile(final File file, final Report report, final RoleRelationRules ruleConstraints) throws PuckException {
		RelationModel result;

		BufferedReader in = null;
		try {
			in = new ODSBufferedReader(file);
			result = readFromTermTXTFile(in, report, ruleConstraints);
			result.setName(IOTermsTools.getBaseName(file, ".term.ods"));

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} catch (Exception exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "\nOpening file [" + file + "]\n[" + exception.getMessage() + "]");
		} finally {
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @param charsetName
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel loadFromTermTXTFile(final File file, final Report report, final RoleRelationRules ruleConstraints) throws PuckException {
		RelationModel result;

		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), DEFAULT_CHARSET_NAME));
			result = readFromTermTXTFile(in, report, ruleConstraints);
			result.setName(IOTermsTools.getBaseName(file, ".term.txt"));

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "\nOpening file [" + file + "]\n[" + exception.getMessage() + "]");
		} catch (Exception exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "\nOpening file [" + file + "]\n[" + exception.getMessage() + "]");
		} finally {
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @param report
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel loadFromTermXLSFile(final File file, final Report report, final RoleRelationRules ruleConstraints) throws PuckException {
		RelationModel result;

		BufferedReader in = null;
		try {
			in = new XLSBufferedReader(file);
			result = readFromTermTXTFile(in, report, ruleConstraints);
			result.setName(IOTermsTools.getBaseName(file, ".term.xls"));

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} catch (Exception exception) {
			exception.printStackTrace();
			throw PuckExceptions.IO_ERROR.create(exception, "\nOpening file [" + file + "]\n[" + exception.getMessage() + "]");
		} finally {
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param in
	 * @return
	 * @throws PuckException
	 */
	public static RelationModel readFromTermTXTFile(final BufferedReader in, final Report report, final RoleRelationRules ruleConstraints) throws PuckException {
		RelationModel result;

		//
		logger.debug("Read attributes.");
		Attributes attributes = new Attributes();
		IOTermsTools.readAttributesVertically(attributes, in);

		// Extract Self Name parameter.
		String selfName = IOTermsTools.extractSelfName(attributes);
		logger.debug("selfName=[{}]", selfName);

		// Extract rule parameters.
		RoleRelationRules rules;
		if (ruleConstraints == null) {
			rules = IOTermsTools.extractRelationRoleRules(attributes);
		} else {
			rules = ruleConstraints;
		}
		logger.debug("rule=[{}]", rules);

		//
		StringList lines = IOTermsTools.readTermRuleLines(in);
		logger.debug("lines count=[{}]", lines.size());

		//
		report.outputs().appendln("Source reading");
		report.outputs().appendln();
		report.outputs().append("Self Name:\t\t").appendln(selfName);
		report.outputs().append("Role Relation Rules:\t").appendln(rules);
		report.outputs().appendln();

		//
		result = RoleRelationMaker.createStandard("default", selfName, lines, rules, report);

		//
		return result;
	}
}
