package org.tip.puck.report;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;

/**
 * This class represents a report File writer.
 * 
 * @author TIP
 */
public class ReportTXTFile {

	private static final Logger logger = LoggerFactory.getLogger(ReportTXTFile.class);

	/**
	 * Saves a report in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static void save(final File file, final Report source) throws PuckException {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));

			writeGeneral(out, source);
			write(out, source);

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * Writes a report in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param net
	 *            Source.
	 */
	public static void write(final PrintWriter out, final Report source) {
		//
		for (Object output : source.outputs()) {
			if (output != null) {
				if (output instanceof ReportAttributes) {
					//
					ReportAttributes attributes = (ReportAttributes) output;

					//
					out.println("Attribute table");
					for (ReportAttribute item : attributes) {
						out.println(item.label() + "\t" + item.value());
					}

					//
					out.println();

				} else if (output instanceof ReportChart) {
					//
					ReportChart chart = (ReportChart) output;

					//
					out.println("Chart: " + chart.getTitle());

					//
					out.println();

				} else if (output instanceof ReportTable) {
					//
					ReportTable table = (ReportTable) output;

					//
					for (int rowIndex = 0; rowIndex < table.getRowCount(); rowIndex++) {
						for (int columnIndex = 0; columnIndex < table.getColumnCount(); columnIndex++) {
							out.append("\t").append(table.getString(rowIndex, columnIndex));
						}
						out.println();
					}

					//
					out.println();

				} else if (output instanceof ReportRawData) {
					//
					ReportRawData rawData = (ReportRawData) output;

					//
					out.println("RawData:" + rawData.getTitle());
					out.println(rawData.getData());

					//
					out.println();

				} else if (output instanceof Report) {
					//
					Report report = (Report) output;

					//
					write(out, report);

					//
					out.println();

				} else {
					//
					out.println(output.toString());

					//
					out.println();
				}
			}
		}
	}

	/**
	 * Writes a report in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param net
	 *            Source.
	 */
	public static void writeGeneral(final PrintWriter out, final Report source) {
		//
		out.println("     General information");
		out.println("Title: " + source.title());
		out.println("Origin: " + source.origin());
		out.println("Date:" + ((new SimpleDateFormat("dd/MM/yyyy HH':'mm")).format(source.date().getTime())));
		out.println("Target: " + source.target());
		out.println("Time spent: " + source.timeSpent());
		out.println();

		//
		out.println("     Inputs");
		out.println("Input comment: " + source.inputComment());
		out.println("Input data:");
		for (ReportAttribute item : source.inputs()) {
			out.println(item.label() + "\t" + item.value());
		}
		out.println();
		out.println();
	}
}
