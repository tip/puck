package org.tip.puck.io.iur;

import java.util.ArrayList;

/**
 * This class represents a line of labels from a TXT file.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class IURTXTLabelsLine extends ArrayList<String> {

	private static final long serialVersionUID = 7413251537721884312L;

	/**
	 * 
	 */
	public IURTXTLabelsLine() {
		super();
	}
}
