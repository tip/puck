package org.tip.puck.net.workers;

import org.tip.puck.util.MathUtils;

/**
 * 
 * 
 * @author TIP
 */
public class AttributeDescriptor {

	public enum Scope {
		NONE,
		CORPUS,
		INDIVIDUALS,
		FAMILIES,
		RELATION,
		ACTORS
	}

	public static Scope[] CanonicalScopes = { Scope.CORPUS, Scope.INDIVIDUALS, Scope.FAMILIES };

	private Scope scope;
	private String optionalRelationName;
	private String label;
	private long countOfSet;
	private long countOfBlank;
	private long max; // Theoretical maximum count.

	/**
	 * 
	 * @param scope
	 * @param label
	 */
	public AttributeDescriptor(final Scope scope, final String label) {
		//
		if (scope == null) {
			this.scope = Scope.NONE;
		} else {
			this.scope = scope;
		}
		this.label = label;
		this.countOfSet = 0;
		this.countOfBlank = 0;
	}

	/**
	 * 
	 * @param scope
	 * @param optionalRelationName
	 * @param label
	 */
	public AttributeDescriptor(final String optionalRelationName, final String label) {
		//
		this.scope = Scope.RELATION;
		this.optionalRelationName = optionalRelationName;
		this.label = label;
		this.countOfSet = 0;
		this.countOfBlank = 0;
	}

	/**
	 * This method returns how many times this attribute is set blank (i.e. not
	 * null and containing no alphanumeric character.
	 * 
	 * @return
	 */
	public long getCountOfBlank() {
		return this.countOfBlank;
	}

	/**
	 * This method returns how many times this attribute is filled (i.e. not
	 * null and with at least one alphanumeric character.
	 * 
	 * @return
	 */
	public long getCountOfFilled() {
		long result;

		result = this.countOfSet - this.countOfBlank;

		//
		return result;
	}

	/**
	 * This method returns how many times this attribute is not set.
	 * 
	 * @return
	 */
	public long getCountOfNotSet() {
		long result;

		if (this.max < this.countOfSet) {
			result = this.countOfSet;
		} else {
			result = this.max - this.countOfSet;
		}

		//
		return result;
	}

	/**
	 * This method returns how many times this attribute is set (i.e. not null
	 * but filled or blank).
	 * 
	 * @return
	 */
	public long getCountOfSet() {
		return this.countOfSet;
	}

	/**
	 * This method returns coverage of this attribute is not set.
	 * 
	 * @return
	 */
	public double getCovegareOfNotSet() {
		double result;

		if (this.max < getCountOfNotSet()) {
			result = 100;
		} else {
			result = MathUtils.percent(getCountOfNotSet(), this.max);
		}

		//
		return result;
	}

	/**
	 * This method returns coverage of this attribute is set blank (i.e. not
	 * null and containing no alphanumeric character.
	 * 
	 * @return
	 */
	public double getCoverageOfBlank() {
		double result;

		if (this.max < getCountOfBlank()) {
			result = 100;
		} else {
			result = MathUtils.percent(getCountOfBlank(), this.max);
		}

		//
		return result;
	}

	/**
	 * This method returns coverage of this attribute is filled (i.e. not null
	 * and with at least one alphanumeric character.
	 * 
	 * @return
	 */
	public double getCoverageOfFilled() {
		double result;

		if (this.max < getCountOfFilled()) {
			result = 100;
		} else {
			result = MathUtils.percent(getCountOfFilled(), this.max);
		}

		//
		return result;
	}

	/**
	 * This method returns coverage of this attribute is set (i.e. not null but
	 * filled or blank).
	 * 
	 * @return
	 */
	public double getCoverageOfSet() {
		double result;

		if (this.max < getCountOfSet()) {
			result = 100;
		} else {
			result = MathUtils.percent(getCountOfSet(), this.max);
		}

		//
		return result;
	}

	public String getLabel() {
		return this.label;
	}

	public long getMax() {
		return this.max;
	}

	public String getOptionalRelationName() {
		return this.optionalRelationName;
	}

	/**
	 * 
	 * @return
	 */
	public String getRelationName() {
		String result;

		//
		switch (this.scope) {
			case CORPUS:
			case INDIVIDUALS:
			case FAMILIES:
				result = this.scope.name();
			break;
			case RELATION:
				result = this.optionalRelationName;
			break;
			case ACTORS:
				result = this.scope.name();
			break;
			case NONE:
			default:
				result = null;
		}

		//
		return result;
	}

	public Scope getScope() {
		return this.scope;
	}

	public void incCountOfBlank() {
		this.countOfBlank += 1;
	}

	public void incCountOfSet() {
		this.countOfSet += 1;
	}

	public void setCountOfBlank(final long countOfBlank) {
		this.countOfBlank = countOfBlank;
	}

	public void setCountOfSet(final long count) {
		this.countOfSet = count;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public void setMax(final long max) {
		this.max = max;
	}

	public void setOptionalRelationName(final String optionalRelationName) {
		this.optionalRelationName = optionalRelationName;
	}

	public void setScope(final Scope scope) {
		this.scope = scope;
	}
}
