package org.tip.puck.partitions.workers;

import org.tip.puck.geo.Geography;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.util.Value;
import org.tip.puck.workers.MetaValuator;

/**
 * 
 * @author TIP
 */
public class ClusterValuator<E> {

	public enum EndogenousLabel {
		SIZE,
	}

	/**
	 * 
	 * @param source
	 * @param label
	 * @return
	 */
	public static <E> Value get(final Cluster<E> source, final String label, final Geography geography) {
		Value result;

		//
		EndogenousLabel endogenousLabel;
		try {
			endogenousLabel = EndogenousLabel.valueOf(label.replace(" ", "_"));
		} catch (IllegalArgumentException exception) {
			endogenousLabel = null;
		}

		// for circuit intersection networks - perhaps dangerous to
		// generalize...
		// TODO
		if (endogenousLabel == null) {
			result = new MetaValuator<E>().get(source.getFirstItem(), label, geography);
		} else {
			switch (endogenousLabel) {
				case SIZE:
					result = new Value(source.count());
				break;
				default:
					result = null;
				break;
			}
		}
		//
		return result;
	}

}
