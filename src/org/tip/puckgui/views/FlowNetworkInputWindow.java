package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.geo.Place;
import org.tip.puck.geo.workers.GeocodingWorker;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.workers.GraphUtils;
import org.tip.puck.net.relations.workers.RelationValuator;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.partitions.graphs.ClusterNetworkReporter;
import org.tip.puck.partitions.graphs.ClusterNetworkUtils;
import org.tip.puck.partitions.graphs.ClusterNetworkUtils.LineType;
import org.tip.puck.report.Report;
import org.tip.puckgui.NetGUI;
import org.tip.puckgui.views.geo.MapPanel;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class FlowNetworkInputWindow extends JFrame {

	private static final long serialVersionUID = -3675976835293862683L;
	private static final Logger logger = LoggerFactory.getLogger(FlowNetworkInputWindow.class);

	private JFrame thisJFrame;
	private NetGUI netGUI;
	private JPanel contentPane;
	private static String lastSourceLabel;
	private static String lastTargetLabel;
	private static String lastRelationLabel;
	private static String lastLevel;
	private static int lastMinimalValue = 2;
	private JComboBox comboBoxSourceLabel;
	private JComboBox comboBoxTargetLabel;
	private JComboBox comboBoxRelation;
	private JComboBox comboBoxLevel;
	private JSpinner spinnerMinimalNumberOfLinks;
	private JCheckBox chckbxBuildCartogrpahy;

	/**
	 * Create the frame.
	 */
	public FlowNetworkInputWindow(final NetGUI netGUI) {
		//
		List<String> availableSourceLabels = IndividualValuator.getAttributeLabelSample(netGUI.getNet().individuals());
		availableSourceLabels.addAll(RelationValuator.getAttributeLabelSample(netGUI.getNet().relations()));
		List<String> availableTargetLabels = new ArrayList<String>(availableSourceLabels);
		List<String> availableRelations = netGUI.getNet().getRelationModelLabels();
		List<String> availableLevels = GeoLevel.toStringList();

		//
		if (lastSourceLabel == null) {
			availableSourceLabels.add(0, "");
		} else {
			availableSourceLabels.add(0, lastSourceLabel);
		}

		//
		if (lastTargetLabel == null) {
			availableTargetLabels.add(0, "");
		} else {
			availableTargetLabels.add(0, lastTargetLabel);
		}

		//
		if (lastRelationLabel == null) {
			availableRelations.add(0, "");
		} else {
			availableRelations.add(0, lastRelationLabel);
		}

		setIconImage(Toolkit.getDefaultToolkit().getImage(FooReporterInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		this.netGUI = netGUI;
		setTitle("Flow Network Inputs");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 340, 315);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		this.contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblSourceLabel = new JLabel("Source Label:");
		panel.add(lblSourceLabel, "2, 4, right, default");

		this.comboBoxSourceLabel = new JComboBox(availableSourceLabels.toArray());
		this.comboBoxSourceLabel.setSelectedIndex(0);
		this.comboBoxSourceLabel.setMaximumRowCount(12);
		this.comboBoxSourceLabel.setEditable(true);
		panel.add(this.comboBoxSourceLabel, "4, 4, fill, default");

		JLabel lblTargetLabel = new JLabel("Target Label:");
		panel.add(lblTargetLabel, "2, 6, right, default");

		this.comboBoxTargetLabel = new JComboBox(availableTargetLabels.toArray());
		this.comboBoxTargetLabel.setSelectedIndex(0);
		this.comboBoxTargetLabel.setMaximumRowCount(12);
		this.comboBoxTargetLabel.setEditable(true);
		panel.add(this.comboBoxTargetLabel, "4, 6, fill, default");

		JLabel lblRelationLabel = new JLabel("Relation Label:");
		panel.add(lblRelationLabel, "2, 8, right, default");

		this.comboBoxRelation = new JComboBox(availableRelations.toArray());
		this.comboBoxRelation.setSelectedIndex(0);
		this.comboBoxRelation.setMaximumRowCount(12);
		this.comboBoxRelation.setEditable(true);
		panel.add(this.comboBoxRelation, "4, 8, fill, default");

		JLabel lblLevel = new JLabel("Level:");
		panel.add(lblLevel, "2, 10, right, default");

		this.comboBoxLevel = new JComboBox(availableLevels.toArray());
		if (lastLevel == null) {
			this.comboBoxLevel.setSelectedIndex(availableLevels.size() - 1);
		} else {
			this.comboBoxLevel.setSelectedIndex(availableLevels.indexOf(lastLevel));
		}
		this.comboBoxLevel.setMaximumRowCount(12);
		panel.add(this.comboBoxLevel, "4, 10, fill, default");

		JLabel label = new JLabel("<html><div style=\"text-align:right\">Minimal number:<br/>of links</div></html>");
		panel.add(label, "2, 12");

		this.spinnerMinimalNumberOfLinks = new JSpinner();
		this.spinnerMinimalNumberOfLinks.setModel(new SpinnerNumberModel(new Integer(2), new Integer(0), null, new Integer(1)));
		panel.add(this.spinnerMinimalNumberOfLinks, "4, 12");

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});
		buttonPanel.add(btnCancel);

		JLabel lblBuildCartography = new JLabel("Build Cartography");
		panel.add(lblBuildCartography, "2, 14");

		this.chckbxBuildCartogrpahy = new JCheckBox("");
		panel.add(this.chckbxBuildCartogrpahy, "4, 14");

		JButton btnLaunch = new JButton("Launch");
		getRootPane().setDefaultButton(btnLaunch);
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					//
					String sourceLabel = (String) FlowNetworkInputWindow.this.comboBoxSourceLabel.getSelectedItem();
					String targetLabel = (String) FlowNetworkInputWindow.this.comboBoxTargetLabel.getSelectedItem();
					String relationLabel = (String) FlowNetworkInputWindow.this.comboBoxRelation.getSelectedItem();
					String level = (String) FlowNetworkInputWindow.this.comboBoxLevel.getSelectedItem();
					int minimalNumberOfLinks = (Integer) FlowNetworkInputWindow.this.spinnerMinimalNumberOfLinks.getValue();

					//
					lastSourceLabel = sourceLabel;
					lastTargetLabel = targetLabel;
					lastRelationLabel = relationLabel;
					lastLevel = level;
					lastMinimalValue = minimalNumberOfLinks;

					//
					if ((sourceLabel == null) || (targetLabel == null)) {
						//
						String title = "Bad input";
						String message = "Please, enter none empty input.";

						//
						JOptionPane.showMessageDialog(FlowNetworkInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
					} else {
						Report report = ClusterNetworkReporter.reportFlowNetwork(netGUI.getSegmentation(), netGUI.getFile(), relationLabel, sourceLabel,
								targetLabel, level, minimalNumberOfLinks);
						netGUI.addReportTab(report);

						// Add ask yes/no build cartography.
						if (FlowNetworkInputWindow.this.chckbxBuildCartogrpahy.isSelected()) {

							// Recompute Graph

							// Compute.
							Graph<?> graph;

							if (StringUtils.isBlank(relationLabel)) {
								graph = ClusterNetworkUtils.createFlowNetwork(netGUI.getSegmentation(), sourceLabel, targetLabel, LineType.WEIGHTED_ARC);
							} else {
								graph = ClusterNetworkUtils.createFlowNetwork(netGUI.getSegmentation(), relationLabel, sourceLabel, targetLabel, level,
										LineType.WEIGHTED_ARC);
							}

							logger.debug("Graph node count={}", graph.nodeCount());
							graph = GraphUtils.reduce(graph, minimalNumberOfLinks, 0, 0);

							logger.debug("Graph reduced node count={}", graph.nodeCount());
							Graph<Place> geocodedGraph = GeocodingWorker.geocodeGraph(netGUI.getNet().getGeography(), graph);
							logger.debug("Graph geocoded node count={}", geocodedGraph.nodeCount());

							String label = String.format("%s-%s", netGUI.getNet().getLabel(), "Flow Cartography");
							logger.debug("geocoded graph label=[{}]", label);
							geocodedGraph.setLabel(label);

							MapPanel cartographyMap = new MapPanel(geocodedGraph);
							netGUI.addTab("Flow Cartography", cartographyMap);

							/*
							// Config Geocode

							ConfigGeocodingDialog dialog = new ConfigGeocodingDialog(netGUI, graph);
							dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
							dialog.setVisible(true);

							// TO DO : activate Export Gis in main file menu
							// mntmExportGis.setEnabled(true);
							*/
						}

						dispose();
					}
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(FlowNetworkInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);

		// ////////////////////////

		//
		this.spinnerMinimalNumberOfLinks.setValue(lastMinimalValue);

	}
}