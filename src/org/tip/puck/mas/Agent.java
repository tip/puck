package org.tip.puck.mas;


import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import org.tip.puck.net.Family;
import org.tip.puck.net.FiliationType;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individuals;


/**
 * @author Telmo Menezes
 *
 */
public class Agent extends Individual {
	public static int MAX_COUSIN_DEGREE = 2;
	
    private int birth;
    private Family currentMarriage;
    private Set<Integer> partners;
    private MAS mas;
    private int ageOfMotherAtBirth;
    private int ageOfFatherAtBirth;
    private Vector<Set<Integer>> cousins;
    private Vector<Set<Integer>> agnaticCousins;
    private Vector<Set<Integer>> uterineCousins;
    private int timeOfLastChildBirth;
    private boolean alive;

    
    public Agent(MAS mas, final int id, final Gender gender, int curCycle, Family origFamily) {
        super(id, "" + id, gender);
        
        this.mas = mas;
        
        this.setOriginFamily(origFamily);
        
        birth = curCycle;
        ageOfMotherAtBirth = 0;
        ageOfFatherAtBirth = 0;
        if (getMother() != null) {
            ageOfMotherAtBirth = ((Agent)getMother()).getAge();
        }
        if (getFather() != null) {
            ageOfFatherAtBirth = ((Agent)getFather()).getAge();
        }
        
        currentMarriage = null;
        
        partners = new HashSet<Integer>();
        
        cousins = new Vector<Set<Integer>>();
        for (int i = 0; i < MAX_COUSIN_DEGREE; i ++) {
        	Set<Integer> cousinSet = new HashSet<Integer>();
        	cousins.add(cousinSet);
        }
        agnaticCousins = new Vector<Set<Integer>>();
        for (int i = 0; i < MAX_COUSIN_DEGREE; i ++) {
        	Set<Integer> cousinSet = new HashSet<Integer>();
        	agnaticCousins.add(cousinSet);
        }
        uterineCousins = new Vector<Set<Integer>>();
        for (int i = 0; i < MAX_COUSIN_DEGREE; i ++) {
        	Set<Integer> cousinSet = new HashSet<Integer>();
        	uterineCousins.add(cousinSet);
        }
        
        timeOfLastChildBirth = -1;
        
        alive = true;
    }
    
    public void die() {
    	alive = false;
    	cousins = null;
    	agnaticCousins = null;
    	uterineCousins = null;
    }
    
    public boolean isPartnerOf(Agent agent) {
        if (currentMarriage == null) {
            return false;
        }
        
        if (currentMarriage.getHusband().getId() == getId()) {
            return currentMarriage.getWife().getId() == agent.getId();
        }
        else if (currentMarriage.getWife().getId() == getId()) {
            return currentMarriage.getHusband().getId()	 == agent.getId();
        }
        
        return false;
    }
    
    public boolean wasPartnerOf(Agent agent) {
    	return partners.contains(agent.getId());
    }
    
    public void setCousins(Agent cousin, int degree, FiliationType fil) {
    	if (cousin.isAlive()) {
    		cousins.get(degree - 1).add(cousin.getId());
    		cousin.cousins.get(degree - 1).add(getId());
    		
    		if (fil == FiliationType.AGNATIC) {
    			agnaticCousins.get(degree - 1).add(cousin.getId());
        		cousin.agnaticCousins.get(degree - 1).add(getId());
    		}
    		else if (fil == FiliationType.UTERINE) {
    			uterineCousins.get(degree - 1).add(cousin.getId());
        		cousin.uterineCousins.get(degree - 1).add(getId());
    		}
    	}
    }
    
    private void exploreDown(Individual alter, int distance, int depth, FiliationType fil) {
    	//System.out.println("distance: " + distance + "; depth: " + depth);
    	
    	if (distance == depth) {
    		//System.out.println("yyy: " + (distance - 1));
    		setCousins(((Agent)alter), distance - 1, fil);
    	}
    	else {
    		for (Individual c : alter.children()) {
    			if ((c == this.getMother()) || (c == this.getFather())) {
    				//System.out.println("xxx");
    				return;
    			}
    			
    			FiliationType newFil;
    			if (c.getGender() == Gender.FEMALE) {
    				switch (fil) {
            		case IDENTITY:
            			newFil = FiliationType.UTERINE;
            			break;
            		case AGNATIC:
            			newFil = FiliationType.COGNATIC;
            			break;
            		case UTERINE:
            			newFil = FiliationType.UTERINE;
            			break;
            		default:
            			newFil = FiliationType.COGNATIC;
            			break;
            		}
    			}
    			else {
    				switch (fil) {
            		case IDENTITY:
            			newFil = FiliationType.AGNATIC;
            			break;
            		case AGNATIC:
            			newFil = FiliationType.AGNATIC;
            			break;
            		case UTERINE:
            			newFil = FiliationType.COGNATIC;
            			break;
            		default:
            			newFil = FiliationType.COGNATIC;
            			break;
            		}
    			}
    			exploreDown((Agent)c, distance, depth + 1, newFil);
    		}
    	}
    }
    
    private void exploreUp(Individual alter, int distance, FiliationType fil) {
    	if (distance < (MAX_COUSIN_DEGREE + 2)) {
    		Individual mother = alter.getMother();
        	if (mother != null) {
        		FiliationType newFil;
        		switch (fil) {
        		case IDENTITY:
        			newFil = FiliationType.UTERINE;
        			break;
        		case AGNATIC:
        			newFil = FiliationType.COGNATIC;
        			break;
        		case UTERINE:
        			newFil = FiliationType.UTERINE;
        			break;
        		default:
        			newFil = FiliationType.COGNATIC;
        			break;
        		}
        		
        		exploreUp(mother, distance + 1, newFil);
        	}
        	
        	Individual father = alter.getFather();
        	if (father != null) {
        		FiliationType newFil;
        		switch (fil) {
        		case IDENTITY:
        			newFil = FiliationType.AGNATIC;
        			break;
        		case AGNATIC:
        			newFil = FiliationType.AGNATIC;
        			break;
        		case UTERINE:
        			newFil = FiliationType.COGNATIC;
        			break;
        		default:
        			newFil = FiliationType.COGNATIC;
        			break;
        		}
        		exploreUp(father, distance + 1, newFil);
        	}
        	
        	if (distance > 1) {
        		exploreDown(alter, distance, 0, fil);
        	}
    	}
    }
    
    public void updateCousins() {
    	exploreUp(this, 0, FiliationType.IDENTITY);
    }
    
    public boolean hasCousin(Agent alter, int degree) {
    	return cousins.get(degree - 1).contains(alter.getId());
    }
    
    public boolean hasAgnaticCousin(Agent alter, int degree) {
    	return agnaticCousins.get(degree - 1).contains(alter.getId());
    }
    
    public boolean hasUterineCousin(Agent alter, int degree) {
    	return uterineCousins.get(degree - 1).contains(alter.getId());
    }
    
    public int getBirth() {
        return birth;
    }
    
    public int getAge() {
        return mas.getCurCycle() - birth;
    }

    public Family getCurrentMarriage() {
        return currentMarriage;
    }

    public void setCurrentMarriage(Family currentMarriage) {
        this.currentMarriage = currentMarriage;
    }
    
    public void addPartner(Agent agent) {
    	partners.add(agent.getId());
    }

    public boolean isMarried() {
        return this.currentMarriage != null;
    }
    
    public int calcNumberOfSiblings() {
    	Set<Integer> siblings = new HashSet<Integer>();
    	
    	if (getMother() != null) {
    		Individuals motherSide = getMother().children();
    		for (Individual s : motherSide) {
    			siblings.add(s.getId());
    		}
    	}
    	
    	if (getFather() != null) {
    		Individuals fatherSide = getFather().children();
    		for (Individual s : fatherSide) {
    			siblings.add(s.getId());
    		}
    	}
    	
    	siblings.remove(getId());
    	
    	return siblings.size();
    }
    
    public int calcNumberOfFullSiblings() {
    	Set<Integer> siblings = new HashSet<Integer>();
    	
    	if (getMother() != null) {
    		Individuals motherSide = getMother().children();
    		for (Individual s : motherSide) {
    			if (s.getFather() == getFather()) {
    				siblings.add(s.getId());
    			}
    		}
    	}
    	
    	if (getFather() != null) {
    		Individuals fatherSide = getFather().children();
    		for (Individual s : fatherSide) {
    			if (s.getMother() == getMother()) {
    				siblings.add(s.getId());
    			}
    		}
    	}
    	
    	siblings.remove(getId());
    	
    	return siblings.size();
    }

    public int getAgeOfMotherAtBirth() {
        return ageOfMotherAtBirth;
    }

    public int getAgeOfFatherAtBirth() {
        return ageOfFatherAtBirth;
    }
    
    public void setAttributes(){
    	setAttribute("BIRT_DATE",birth+"");
    	setAttribute("AGE",getAge()+"");
    }

	public void setBirth(int birth) {
		this.birth = birth;
	}

	public int getTimeOfLastChildBirth() {
		return timeOfLastChildBirth;
	}

	public void setTimeOfLastChildBirth(int timeOfLastChildBirth) {
		this.timeOfLastChildBirth = timeOfLastChildBirth;
	}

	public MAS getMas() {
		return mas;
	}

	public boolean isAlive() {
		return alive;
	}
}