package org.tip.puck.net.workers;

import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.io.permutation.PermutationTable;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Attributes;
import org.tip.puck.net.Family;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.relations.Role;
import org.tip.puck.report.Report;

/**
 * 
 * Methods added in this classes has to be static and autonomous.
 * 
 * @author TIP
 */
public class UpdateWorker {
	public enum UpdateMode {
		APPEND,
		OVERWRITE
	}

	private static final Logger logger = LoggerFactory.getLogger(UpdateWorker.class);

	/**
	 * 
	 * @param target
	 * @param source
	 * @throws PuckException
	 */
	public static Net fuse(final Net target, final Net source, final PermutationTable permutations, final Report errorReport) throws PuckException {
		Net result;

		// Renumber individual ids of source net.
		// List<Integer> sparseIds = target.individuals().getSparseIds();
		// int sparseIdCount = 0;

		int lastId = target.individuals().getLastId();
		int newIdCount = 0;

		for (Individual sourceIndividual : source.individuals().toSortedList()) {
			//
			Integer newId = permutations.getTarget(sourceIndividual.getId());
			if (newId == null) {
				/*				if (sparseIdCount < sparseIds.size()) {
									newId = sparseIds.get(sparseIdCount);
									sparseIdCount += 1;
								} else {*/
				newId = lastId + newIdCount + 1;
				newIdCount += 1;
				// }
			}

			//
			sourceIndividual.setId(newId);
		}

		//
		result = update(target, source, errorReport, UpdateMode.APPEND);

		//
		return result;
	}

	/**
	 * Update link of a individual already transfered.
	 * 
	 * @param sourceIndividual
	 */
	@Deprecated
	private static Family getOrCreateFamily(final Net target, final Individual sourceFather, final Individual sourceMother) {
		Family result;

		//
		Individual targetFather;
		if (sourceFather == null) {
			targetFather = null;
		} else {
			targetFather = target.individuals().getById(sourceFather.getId());
		}

		//
		Individual targetMother;
		if (sourceMother == null) {
			targetMother = null;
		} else {
			targetMother = target.individuals().getById(sourceMother.getId());
		}

		//
		if ((targetMother == null) && (targetFather == null)) {
			result = null;
		} else {
			if ((targetMother == null) || (targetFather == null)) {
				result = new Family(target.families().getLastId() + 1, targetFather, targetMother);
				target.families().add(result);
			} else {
				result = target.families().getBySpouses(targetFather, targetMother);
				if (result == null) {
					result = new Family(target.families().getLastId() + 1, targetFather, targetMother);
					target.families().add(result);
				}
			}

			//
			if (targetFather != null) {
				targetFather.getPersonalFamilies().add(result);
			}
			if (targetMother != null) {
				targetMother.getPersonalFamilies().add(result);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param source
	 */
	public static void update(final String idString, final Attributes target, final Attributes source, final Report errorReport, final UpdateMode mode) {

		String refString = idString;
		if (idString.indexOf("\t")>-1) {
			refString = idString.substring(idString.indexOf("\t")+1);
		}
		refString = " [from "+refString+"]";
		
		for (Attribute sourceAttribute : source) {
			if ((StringUtils.isNotBlank(sourceAttribute.getLabel())) && (StringUtils.isNotBlank(sourceAttribute.getValue()))) {
				Attribute targetAttribute = target.get(sourceAttribute.getLabel());

				if (targetAttribute == null) {
					target.put(sourceAttribute.getLabel(), sourceAttribute.getValue());
				} else if (sourceAttribute.getLabel().equals("DOUBLE") && targetAttribute.getLabel().equals("DOUBLE")){
					targetAttribute.setValue(targetAttribute.getValue()+";"+sourceAttribute.getValue());
				} else if (mode == UpdateMode.OVERWRITE && !targetAttribute.hasEqualValue(sourceAttribute)) {
					target.add(new Attribute(targetAttribute.getLabel()+"_OLD",targetAttribute.getValue()+refString));
					targetAttribute.setValue(sourceAttribute.getValue());
					errorReport.outputs().appendln(idString+"\tChanged attribute\t"+targetAttribute.getLabel()+" "+targetAttribute.getValue()+"\t->\t"+sourceAttribute.getLabel()+" "+sourceAttribute.getValue());
				} else if (mode == UpdateMode.APPEND && !targetAttribute.hasEqualValue(sourceAttribute)) {
					target.add(new Attribute(sourceAttribute.getLabel()+"_ALT",sourceAttribute.getValue()+refString));
					errorReport.outputs().appendln(idString+"\tNot changed attribute\t"+targetAttribute.getLabel()+" "+targetAttribute.getValue()+"\t->|\t"+sourceAttribute.getLabel()+" "+sourceAttribute.getValue());
				}
			}
		}
	}

	/**
	 * 
	 * @param original
	 *            Net to update.
	 * 
	 * @param source
	 *            Net containing data to update in target.
	 * 
	 * @param mode
	 *            Update selector.
	 * 
	 * @return a copy of original net updated with the source net.
	 * 
	 * @throws PuckException
	 */
	public static Net update(final Net original, final Net source, final Report errorReport, final UpdateMode mode) throws PuckException {
		Net result;

		//
		Net target = new Net(original);
		NetUtils.renumberFamilies(target);

		//
		if (StringUtils.isBlank(target.getLabel())) {
			//
			target.setLabel(source.getLabel());

		} else if ((mode == UpdateMode.OVERWRITE) && (StringUtils.isNotBlank(source.getLabel()))) {
			//
			target.setLabel(source.getLabel());
		}

		//
		HashMap<Integer, Object> originalTargetIds = new HashMap<Integer, Object>(target.individuals().size());
		for (Integer id : target.individuals().getIds()) {
			originalTargetIds.put(id, null);
		}

		// Transfer individuals.
		for (Individual sourceIndividual : source.individuals().toSortedList()) {
			//
			Individual targetIndividual = target.individuals().getById(sourceIndividual.getId());
			if (targetIndividual == null) {
				// New individual case.
				targetIndividual = new Individual(sourceIndividual.getId());
				target.individuals().add(targetIndividual);
			}

			if (!originalTargetIds.containsKey(targetIndividual.getId())) {
				
				// Update a new individual.
				targetIndividual.setName(sourceIndividual.getName());
				targetIndividual.setGender(sourceIndividual.getGender());
				targetIndividual.setBirthOrder(sourceIndividual.getBirthOrder());
				targetIndividual.attributes().addAll(sourceIndividual.attributes());

				//
				if (sourceIndividual.getFather() != null) {
					NetUtils.setFatherRelation(target, sourceIndividual.getFather().getId(), targetIndividual.getId());
				}
				if (sourceIndividual.getMother() != null) {
					NetUtils.setMotherRelation(target, sourceIndividual.getMother().getId(), targetIndividual.getId());
				}
				for (Individual partner : sourceIndividual.getPartners()) {
					NetUtils.setSpouseRelationAndFixRoles(target, targetIndividual.getId(), partner.getId());
				}
				
			} else {
								//
				if (mode == UpdateMode.OVERWRITE) {
					// Overwrite name.
					String sourceName = sourceIndividual.getName();
					if ((StringUtils.isNotBlank(sourceName)) && (sourceName.charAt(0) != '#') && (sourceName.indexOf("*") == -1)) {
						if (StringUtils.isNotBlank(targetIndividual.getName()) && !targetIndividual.getName().equals(sourceName)){
							errorReport.outputs().appendln(targetIndividual+"\tchanged Name\t"+targetIndividual.getName()+"\t->\t"+sourceName);
						}
						targetIndividual.setName(sourceName);
					}

					// Overwrite gender.
					if (!sourceIndividual.getGender().isUnknown()) {
						if (!targetIndividual.getGender().isUnknown() && !targetIndividual.getGender().equals(sourceIndividual.getGender())){
							errorReport.outputs().appendln(targetIndividual+"\tchanged Gender\t"+targetIndividual.getGender()+"\t->\t"+sourceIndividual.getGender());
						}
						targetIndividual.setGender(sourceIndividual.getGender());
					}

					// Overwrite birth order.
					if (sourceIndividual.getBirthOrder() != null) {
						//
						if (targetIndividual.getBirthOrder()!=null && targetIndividual.getBirthOrder()!=sourceIndividual.getBirthOrder()){
							errorReport.outputs().appendln(targetIndividual+"\tchanged Birth Order\t"+targetIndividual.getBirthOrder()+"\t->\t"+sourceIndividual.getBirthOrder());
						}
						targetIndividual.setBirthOrder(sourceIndividual.getBirthOrder());
					}

					// Overwrite Attributes.
					update(sourceIndividual.toString(), targetIndividual.attributes(), sourceIndividual.attributes(), errorReport, mode);

					// Overwrite father.
					if (targetIndividual.getFather() != null) {
						//
						if ((sourceIndividual.getFather() == null) || (!sourceIndividual.getFather().equals(targetIndividual.getFather()))) {
							// Remove current father.
							errorReport.outputs().appendln(targetIndividual+"\tchanged Father\t"+targetIndividual.getFather()+"\t->\t"+sourceIndividual.getFather());
							targetIndividual.getOriginFamily().getChildren().removeById(targetIndividual.getId());
							targetIndividual.setOriginFamily(null);
						}
					}

					if ((targetIndividual.getFather() == null) && (sourceIndividual.getFather() != null)) {
						//
						errorReport.outputs().appendln(targetIndividual+"\tadded Father\t->\t"+sourceIndividual.getFather());
						NetUtils.setFatherRelation(target, sourceIndividual.getFather().getId(), targetIndividual.getId());
					}

					// Overwrite mother.
					if (targetIndividual.getMother() != null) {
						//
						if ((sourceIndividual.getMother() == null) || (!sourceIndividual.getMother().equals(targetIndividual.getMother()))) {
							// Remove current mother.
							errorReport.outputs().appendln(targetIndividual+"\tchanged Mother\t"+targetIndividual.getMother()+"\t->\t"+sourceIndividual.getMother());
							targetIndividual.getOriginFamily().getChildren().removeById(targetIndividual.getId());
							targetIndividual.setOriginFamily(null);
						}
					}

					if ((targetIndividual.getMother() == null) && (sourceIndividual.getMother() != null)) {
						//
						errorReport.outputs().appendln(targetIndividual+"\tadded Mother\t->\t"+sourceIndividual.getMother());
						NetUtils.setMotherRelation(target, sourceIndividual.getMother().getId(), targetIndividual.getId());
					}

					// Overwrite partners.
					for (Individual partner : sourceIndividual.getPartners()) {
						//
						if (!targetIndividual.getPartners().contains(partner)){
							errorReport.outputs().appendln(targetIndividual+"\tadded Spouse\t->\t"+partner);
						}
						NetUtils.setSpouseRelationAndFixRoles(target, targetIndividual.getId(), partner.getId());
					}

					// Note: not necessary to append child because it is done by
					// parent update.
				} else {
					// Append attributes.
					update(targetIndividual.toString(), targetIndividual.attributes(), sourceIndividual.attributes(), errorReport, mode);

					// Append name.
					String sourceName = sourceIndividual.getName();
					if ((StringUtils.isNotBlank(sourceName)) && (sourceName.charAt(0) != '#') && (sourceName.indexOf("*") == -1)) {
						//
						if (StringUtils.isBlank(targetIndividual.getName())){
							targetIndividual.setName(sourceName);
							errorReport.outputs().appendln(targetIndividual+"\tchanged Name\t"+targetIndividual.getName()+"\t->\t"+sourceName);
						} else if (!targetIndividual.getName().equals(sourceName)){
							errorReport.outputs().appendln(targetIndividual+"\tnot changed Name\t"+targetIndividual.getName()+"\t->|\t"+sourceName);
						}
					}

					// Append gender.
					if (!sourceIndividual.getGender().isUnknown()) {
						//
						if (targetIndividual.getGender().isUnknown()){
							targetIndividual.setGender(sourceIndividual.getGender());
							errorReport.outputs().appendln(targetIndividual+"\tchanged Gender\t"+targetIndividual.getGender()+"\t->\t"+sourceIndividual.getGender());
						} else if (!targetIndividual.getGender().equals(sourceIndividual.getGender())){
							errorReport.outputs().appendln(targetIndividual+"\tnot changed Gender\t"+targetIndividual.getGender()+"\t->|\t"+sourceIndividual.getGender());
						}
					}

					// Append birth order.
					if (sourceIndividual.getBirthOrder()!=null && targetIndividual.getBirthOrder() == null) {
						//
						targetIndividual.setBirthOrder(sourceIndividual.getBirthOrder());
						errorReport.outputs().appendln(targetIndividual+"\tchanged Birth Order\t"+targetIndividual.getBirthOrder()+"\t->\t"+sourceIndividual.getBirthOrder());
					} else if (sourceIndividual.getBirthOrder() !=null && sourceIndividual.getBirthOrder()!=targetIndividual.getBirthOrder()){
						errorReport.outputs().appendln(targetIndividual+"\tnot changed Birth Order\t"+targetIndividual.getBirthOrder()+"\t->|\t"+sourceIndividual.getBirthOrder());
					}

					// Append parents.
/*					if ((targetIndividual.getFather() == null) && (sourceIndividual.getFather() != null)) {
						//
						NetUtils.setFatherRelation(target, sourceIndividual.getFather().getId(), targetIndividual.getId());
					} else if (sourceIndividual.getFather() != null){
						errorReport.outputs().appendln(targetIndividual+"\tnot changed Father\t"+targetIndividual.getFather()+"\t->|\t"+sourceIndividual.getFather());
					}

					if ((targetIndividual.getMother() == null) && (sourceIndividual.getMother() != null)) {
						//
						NetUtils.setMotherRelation(target, sourceIndividual.getMother().getId(), targetIndividual.getId());
					} else if (sourceIndividual.getMother() != null){
						errorReport.outputs().appendln(targetIndividual+"\tnot changed Mother\t"+targetIndividual.getMother()+"\t->|\t"+sourceIndividual.getMother());
					}*/
					
					if ((targetIndividual.getFather() == null) && (sourceIndividual.getFather() != null)) {
						NetUtils.setFatherRelation(target, sourceIndividual.getFather().getId(), targetIndividual.getId());
						errorReport.outputs().appendln(targetIndividual+"\tchanged Father\t"+targetIndividual.getMother()+"\t->\t"+sourceIndividual.getMother());
					} else if (targetIndividual.getFather()!=null && sourceIndividual.getFather()!=null){
						if (!targetIndividual.getFather().equals(sourceIndividual.getFather()) && (!NumberUtils.isNumber(sourceIndividual.getFather().getFirstName())|| Integer.parseInt(sourceIndividual.getFather().getFirstName())!=targetIndividual.getFather().getId()) && (!NumberUtils.isNumber(targetIndividual.getFather().getFirstName())|| Integer.parseInt(targetIndividual.getFather().getFirstName())!=sourceIndividual.getFather().getId())) {
							errorReport.outputs().appendln("Father conflict for "+targetIndividual+":\t"+sourceIndividual.getFather()+" vs "+targetIndividual.getFather());
							targetIndividual.setAttribute("F_ALT", sourceIndividual.getFather()+"");
						}
					}
					if ((targetIndividual.getMother() == null) && (sourceIndividual.getMother() != null)) {
						NetUtils.setMotherRelation(target, sourceIndividual.getMother().getId(), targetIndividual.getId());
						errorReport.outputs().appendln(targetIndividual+"\tchanged Mother\t"+targetIndividual.getMother()+"\t->\t"+sourceIndividual.getMother());
					} else if (targetIndividual.getMother()!=null && sourceIndividual.getMother()!=null){
						if (!targetIndividual.getMother().equals(sourceIndividual.getMother()) && (!NumberUtils.isNumber(sourceIndividual.getMother().getFirstName())|| Integer.parseInt(sourceIndividual.getMother().getFirstName())!=targetIndividual.getMother().getId())&& (!NumberUtils.isNumber(targetIndividual.getMother().getFirstName())|| Integer.parseInt(targetIndividual.getMother().getFirstName())!=sourceIndividual.getMother().getId())) {
							errorReport.outputs().appendln("Mother conflict for "+targetIndividual+":\t"+sourceIndividual.getMother()+" vs "+targetIndividual.getMother());
							targetIndividual.setAttribute("M_ALT", sourceIndividual.getMother()+"");
						}
					}


					// Append partners.
					for (Individual partner : sourceIndividual.getPartners()) {
						//
						if (!targetIndividual.getPartners().contains(partner)){
							errorReport.outputs().appendln(targetIndividual+"\tadded Spouse\t->\t"+partner);
						}
						NetUtils.setSpouseRelationAndFixRoles(target, targetIndividual.getId(), partner.getId());
					}

					// Note: not necessary to append child because it is done by
					// parent append.
				}
			}
		}
		
		// Transfer relations models.
		logger.debug("===> source " + source.relationModels().size());
		logger.debug("===> target " + target.relationModels().size());
		for (RelationModel sourceRelationModel : source.relationModels()) {
			//
			RelationModel targetRelationModel = target.relationModels().getByName(sourceRelationModel.getName());
			if (targetRelationModel == null) {
				targetRelationModel = target.createRelationModel(sourceRelationModel);
			} else {
				for (Role sourceRole : sourceRelationModel.roles()) {
					if (targetRelationModel.roles().getByName(sourceRole.getName()) == null) {
						targetRelationModel.roles().add(new Role(sourceRole));
					}
				}
			}

			//
			Relations sourceRelations = source.relations().getByModel(sourceRelationModel);
			Relations targetRelations = target.relations().getByModel(targetRelationModel);
			for (Relation sourceRelation : sourceRelations) {
				//
				Relation targetRelation = targetRelations.getById(sourceRelation.getId());
				if (targetRelation == null) {
					//
					targetRelation = target.createRelation(sourceRelation.getTypedId(), sourceRelation.getName(), targetRelationModel);

				} else if (mode == UpdateMode.OVERWRITE) {
					//
					if (!sourceRelation.getName().equals(targetRelation.getName())){
						errorReport.outputs().appendln(sourceRelation.getId()+"\tChanged relation name "+targetRelation.getName()+"\t->\t"+targetRelation.getName());
					}
					targetRelation.setName(sourceRelation.getName());
				} else if (!sourceRelation.getName().equals(targetRelation.getName())){
					errorReport.outputs().appendln(sourceRelation.getId()+"\tNot changed relation name "+targetRelation.getName()+"\t->|\t"+targetRelation.getName());
				}

				//
				for (Actor sourceActor : sourceRelation.actors()) {
					//
					if (!targetRelation.hasActor(sourceActor)) {
						//
						Actor actor = target.createRelationActor(targetRelation, sourceActor.getId(), sourceActor.getRole().getName());
						actor.setRelationOrder(sourceActor.getRelationOrder());
					}
				}

				//
				update(targetRelation.toString(), targetRelation.attributes(), targetRelation.attributes(), errorReport, mode);
			}
		}

		//
		result = NetUtils.buildCleanedNet(target);

		//
		return result;
	}
}
