package org.tip.puckgui.views.kinoath;

import java.awt.Color;
import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.tip.puck.kinoath.IndividualGroup;
import org.tip.puckgui.models.IndividualsCellRenderer;

/**
 * This class provides implementation for displaying decorated individual group
 * list.
 * 
 * @author TIP
 */
public class IndividualGroupsCellRenderer extends JLabel implements ListCellRenderer {

	private static final long serialVersionUID = -3084882194153545640L;
	private static ImageIcon femaleIcon = new ImageIcon(IndividualsCellRenderer.class.getResource("/org/tip/puckgui/images/female-16x16.png"));
	private static ImageIcon maleIcon = new ImageIcon(IndividualsCellRenderer.class.getResource("/org/tip/puckgui/images/male-16x16.png"));
	private static ImageIcon unknowIcon = new ImageIcon(IndividualsCellRenderer.class.getResource("/org/tip/puckgui/images/unknown-16x16.png"));

	/**
	 * 
	 */
	public IndividualGroupsCellRenderer() {
		super();
		setOpaque(true);
	}

	/**
	 * 	
	 */
	@Override
	public Component getListCellRendererComponent(final JList list, final Object lineObject, final int index, final boolean isSelected,
			final boolean cellHasFocus) {

		//
		if (lineObject == null) {
			//
			throw new NullPointerException("Invalid null parameter.");

		} else if (lineObject instanceof String) {
			//
			setText((String) lineObject);

		} else {
			//
			IndividualGroup group = (IndividualGroup) lineObject;

			//
			setText(String.format("%s %d {%d}", group.getLabel(), index + 1, group.size()));
		}

		//
		Color background = null;
		Color foreground = null;
		if (isSelected) {
			//
			background = list.getSelectionBackground();
			foreground = list.getSelectionForeground();

		} else {
			//
			background = list.getBackground();
			foreground = list.getForeground();
		}
		setBackground(background);
		setForeground(foreground);

		//
		return this;
	}
}
