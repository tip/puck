package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.tip.puck.graphs.GraphType;
import org.tip.puck.util.ToolBox;
import org.tip.puckgui.util.GenericFileFilter;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class PajekExportInputDialog extends JDialog {

	private static final long serialVersionUID = -488171894134279755L;
	private final JPanel contentPanel = new JPanel();
	private static PajekExportCriteria defaultCriteria = new PajekExportCriteria();;
	private PajekExportCriteria inputedCriteria;
	private JTextField txtfldTargetFile;
	private final ButtonGroup buttonGroupGraphType = new ButtonGroup();
	private JRadioButton rdbtnPgraph;
	private JComboBox cmbbxLabel1;
	private JComboBox cmbbxLabel2;
	private JComboBox cmbbxLabel3;
	private JComboBox cmbbxLabel4;
	private JComboBox cmbbxLabel5;
	private JRadioButton rdbtnOregraph;
	private JRadioButton rdbtnTipgraph;
	private JRadioButton rdbtnRelgraph;
	private JRadioButton rdbtnBimodgraph;
	private Object[] labels;
	private JDialog thisJDialog;
	private File file;
	private JPanel panelPartitionLabels;
	private JRadioButton rdbtnBimodal;

	/**
	 * Create the dialog.
	 */
	public PajekExportInputDialog(final File file, final List<String> sourceLabels) {
		super();
		this.thisJDialog = this;
		this.file = file;
		if (file != null) {
			defaultCriteria.setTargetFileName(ToolBox.setExtension(file.getAbsoluteFile(), ".paj").getAbsolutePath());
		}
		setMinimumSize(new Dimension(350, 0));
		setResizable(false);
		if (sourceLabels == null) {
			this.labels = new String[1];
			this.labels[0] = "";
		} else {
			sourceLabels.add(0, "");
			this.labels = sourceLabels.toArray();
		}
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Export To Pajek Input");
		setIconImage(Toolkit.getDefaultToolkit().getImage(PajekExportInputDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				PajekExportInputDialog.this.inputedCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 387, 310);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, }));
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "Graph Type", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			this.contentPanel.add(panel, "2, 2, fill, fill");
			panel.setLayout(new FormLayout(new ColumnSpec[] {
					FormFactory.RELATED_GAP_COLSPEC,
					FormFactory.DEFAULT_COLSPEC,
					FormFactory.RELATED_GAP_COLSPEC,
					FormFactory.DEFAULT_COLSPEC,
					FormFactory.RELATED_GAP_COLSPEC,
					FormFactory.DEFAULT_COLSPEC,},
				new RowSpec[] {
					FormFactory.RELATED_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,}));
			{
				this.rdbtnOregraph = new JRadioButton("Ore-Graph");
				this.rdbtnOregraph.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						setEnablePartitionlabels(true);
					}
				});
				this.rdbtnOregraph.setSelected(true);
				panel.add(this.rdbtnOregraph, "2, 2");
				this.buttonGroupGraphType.add(this.rdbtnOregraph);
			}
			{
				this.rdbtnTipgraph = new JRadioButton("Tip-Graph");
				this.rdbtnTipgraph.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						setEnablePartitionlabels(true);
					}
				});
				panel.add(this.rdbtnTipgraph, "2, 4");
				this.buttonGroupGraphType.add(this.rdbtnTipgraph);
			}
			{
				this.rdbtnPgraph = new JRadioButton("P-Graph");
				this.rdbtnPgraph.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						setEnablePartitionlabels(false);
					}
				});
				panel.add(this.rdbtnPgraph, "2, 6");
				this.buttonGroupGraphType.add(this.rdbtnPgraph);
			}
			{
				this.rdbtnRelgraph = new JRadioButton("Relations");
				this.rdbtnRelgraph.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						setEnablePartitionlabels(true);
					}
				});
				panel.add(this.rdbtnRelgraph, "2, 8");
				this.buttonGroupGraphType.add(this.rdbtnRelgraph);
			}
				
				
				{
					this.rdbtnBimodgraph = new JRadioButton("Bimodal");
					this.rdbtnBimodgraph.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(final ActionEvent e) {
							setEnablePartitionlabels(true);
						}
					});
					panel.add(this.rdbtnBimodgraph, "2, 10");
					this.buttonGroupGraphType.add(this.rdbtnBimodgraph);
				}

		}
		{
			this.panelPartitionLabels = new JPanel();
			this.panelPartitionLabels.setBorder(new TitledBorder(null, "Partition Labels", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			this.contentPanel.add(this.panelPartitionLabels, "4, 2, fill, fill");
			this.panelPartitionLabels.setLayout(new BoxLayout(this.panelPartitionLabels, BoxLayout.Y_AXIS));
			{
				this.cmbbxLabel1 = new JComboBox();
				this.panelPartitionLabels.add(this.cmbbxLabel1);
			}
			{
				this.cmbbxLabel2 = new JComboBox();
				this.panelPartitionLabels.add(this.cmbbxLabel2);
			}
			{
				this.cmbbxLabel3 = new JComboBox();
				this.panelPartitionLabels.add(this.cmbbxLabel3);
			}
			{
				this.cmbbxLabel4 = new JComboBox();
				this.panelPartitionLabels.add(this.cmbbxLabel4);
			}
			{
				this.cmbbxLabel5 = new JComboBox();
				this.panelPartitionLabels.add(this.cmbbxLabel5);
			}
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(new TitledBorder(null, "Target File", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			this.contentPanel.add(panel, "2, 4, 3, 1, fill, fill");
			panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
					FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, },
					new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));
			{
				this.txtfldTargetFile = new JTextField();
				panel.add(this.txtfldTargetFile, "2, 2");
				this.txtfldTargetFile.setColumns(10);
			}
			{
				JButton buttonClear = new JButton("");
				buttonClear.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Clear.
						PajekExportInputDialog.this.txtfldTargetFile.setText("");
					}
				});
				panel.add(buttonClear, "4, 2");
				buttonClear.setIcon(new ImageIcon(PajekExportInputDialog.class.getResource("/org/tip/puckgui/images/edit-clear-16x16.png")));
			}
			{
				JButton buttonBrowse = new JButton("...");
				buttonBrowse.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Browse.
						File targetFile;
						if (StringUtils.isBlank(PajekExportInputDialog.this.txtfldTargetFile.getText())) {
							targetFile = file;
						} else {
							targetFile = ToolBox.setExtension(new File(PajekExportInputDialog.this.txtfldTargetFile.getText()), ".paj");
						}

						File selection = selectFile(PajekExportInputDialog.this.thisJDialog, targetFile);
						if (selection != null) {
							selection = ToolBox.setExtension(selection, ".paj");
							PajekExportInputDialog.this.txtfldTargetFile.setText(selection.getAbsolutePath());
						}
					}
				});
				panel.add(buttonBrowse, "6, 2");
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						PajekExportInputDialog.this.inputedCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton btnExport = new JButton("Export");
				btnExport.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						
						//
						if (StringUtils.isBlank(PajekExportInputDialog.this.txtfldTargetFile.getText())) {
							//
							String title = "Error computerum est";
							String message = "Please, enter a valid file.";

							//
							JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
						} else {
							boolean doExport;

							File targetFile = ToolBox.setExtension(new File(PajekExportInputDialog.this.txtfldTargetFile.getText()), ".paj");

							if (targetFile.exists()) {
								//
								String title = "Confirm";
								String message = "A file already exists with name, erase?";
								
								//
								int response = JOptionPane.showConfirmDialog(PajekExportInputDialog.this.contentPanel, message, title, JOptionPane.YES_NO_OPTION);

								if (response == JOptionPane.YES_OPTION) {
									doExport = true;
								} else {
									doExport = false;
								}
							} else {
								doExport = true;
							}

							if (doExport) {
								//
								PajekExportInputDialog.this.inputedCriteria = new PajekExportCriteria();

								//
								if (PajekExportInputDialog.this.rdbtnOregraph.isSelected()) {
									PajekExportInputDialog.this.inputedCriteria.setGraphType(GraphType.OreGraph);
								}
								if (PajekExportInputDialog.this.rdbtnTipgraph.isSelected()) {
									PajekExportInputDialog.this.inputedCriteria.setGraphType(GraphType.TipGraph);
								}
								if (PajekExportInputDialog.this.rdbtnPgraph.isSelected()) {
									PajekExportInputDialog.this.inputedCriteria.setGraphType(GraphType.PGraph);
								}
								if (PajekExportInputDialog.this.rdbtnRelgraph.isSelected()) {
									PajekExportInputDialog.this.inputedCriteria.setGraphType(GraphType.BinaryRelationGraph);
								}
								if (PajekExportInputDialog.this.rdbtnBimodgraph.isSelected()) {
									PajekExportInputDialog.this.inputedCriteria.setGraphType(GraphType.BimodalRelationGraph);
								}
								
								//
								PajekExportInputDialog.this.inputedCriteria.setTargetFileName(targetFile.getAbsolutePath());

								//
								PajekExportInputDialog.this.inputedCriteria.getPartitionLabels().clear();
								PajekExportInputDialog.this.inputedCriteria.getPartitionLabels().add(
										(String) PajekExportInputDialog.this.cmbbxLabel1.getSelectedItem());
								PajekExportInputDialog.this.inputedCriteria.getPartitionLabels().add(
										(String) PajekExportInputDialog.this.cmbbxLabel2.getSelectedItem());
								PajekExportInputDialog.this.inputedCriteria.getPartitionLabels().add(
										(String) PajekExportInputDialog.this.cmbbxLabel3.getSelectedItem());
								PajekExportInputDialog.this.inputedCriteria.getPartitionLabels().add(
										(String) PajekExportInputDialog.this.cmbbxLabel4.getSelectedItem());
								PajekExportInputDialog.this.inputedCriteria.getPartitionLabels().add(
										(String) PajekExportInputDialog.this.cmbbxLabel5.getSelectedItem());

								//
								defaultCriteria = PajekExportInputDialog.this.inputedCriteria;

								//
								setVisible(false);
							}
						}
					}
				});
				btnExport.setActionCommand("OK");
				buttonPane.add(btnExport);
				getRootPane().setDefaultButton(btnExport);
			}
		}
		setCriteria(defaultCriteria);
	}

	/**
	 * 
	 * @return
	 */
	public PajekExportCriteria getCriteria() {
		PajekExportCriteria result;

		result = this.inputedCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final PajekExportCriteria source) {
		//
		if (source != null) {
			//
			this.rdbtnOregraph.setSelected(false);
			this.rdbtnPgraph.setSelected(false);
			this.rdbtnTipgraph.setSelected(false);
			this.rdbtnRelgraph.setSelected(false);
			switch (source.getGraphType()) {
				case OreGraph:
					this.rdbtnOregraph.setSelected(true);
					setEnablePartitionlabels(true);
				break;
				case PGraph:
					this.rdbtnPgraph.setSelected(true);
					setEnablePartitionlabels(false);
				break;
				case TipGraph:
					this.rdbtnTipgraph.setSelected(true);
					setEnablePartitionlabels(true);
				break;
				case BinaryRelationGraph:
					this.rdbtnRelgraph.setSelected(true);
					setEnablePartitionlabels(true);
				break;
				case BimodalRelationGraph:
					this.rdbtnBimodgraph.setSelected(true);
					setEnablePartitionlabels(true);
				break;
			}

			//
			if (StringUtils.isNotBlank(source.getTargetFileName())) {
				this.txtfldTargetFile.setText(source.getTargetFileName());
			}

			//
			this.cmbbxLabel1.setModel(new DefaultComboBoxModel(this.labels));
			this.cmbbxLabel2.setModel(new DefaultComboBoxModel(this.labels));
			this.cmbbxLabel3.setModel(new DefaultComboBoxModel(this.labels));
			this.cmbbxLabel4.setModel(new DefaultComboBoxModel(this.labels));
			this.cmbbxLabel5.setModel(new DefaultComboBoxModel(this.labels));

			//
			int labelIndex = ArrayUtils.indexOf(this.labels, source.getPartitionLabels().get(0));
			if (labelIndex == -1) {
				labelIndex = 0;
			}
			this.cmbbxLabel1.setSelectedIndex(labelIndex);

			//
			labelIndex = ArrayUtils.indexOf(this.labels, source.getPartitionLabels().get(1));
			if (labelIndex == -1) {
				labelIndex = 0;
			}

			//
			labelIndex = ArrayUtils.indexOf(this.labels, source.getPartitionLabels().get(2));
			if (labelIndex == -1) {
				labelIndex = 0;
			}
			this.cmbbxLabel3.setSelectedIndex(labelIndex);

			//
			labelIndex = ArrayUtils.indexOf(this.labels, source.getPartitionLabels().get(3));
			if (labelIndex == -1) {
				labelIndex = 0;
			}
			this.cmbbxLabel4.setSelectedIndex(labelIndex);

			//
			labelIndex = ArrayUtils.indexOf(this.labels, source.getPartitionLabels().get(4));
			if (labelIndex == -1) {
				labelIndex = 0;
			}
			this.cmbbxLabel5.setSelectedIndex(labelIndex);
		}
	}

	/**
	 * 
	 * @param value
	 */
	private void setEnablePartitionlabels(final boolean value) {
		if (value) {
			this.cmbbxLabel1.setEnabled(true);
			this.cmbbxLabel2.setEnabled(true);
			this.cmbbxLabel3.setEnabled(true);
			this.cmbbxLabel4.setEnabled(true);
			this.cmbbxLabel5.setEnabled(true);
		} else {
			this.cmbbxLabel1.setEnabled(false);
			this.cmbbxLabel2.setEnabled(false);
			this.cmbbxLabel3.setEnabled(false);
			this.cmbbxLabel4.setEnabled(false);
			this.cmbbxLabel5.setEnabled(false);
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		List<String> labels = new ArrayList<String>();
		labels.add("ALPHA");
		labels.add("BRAVO");
		labels.add("CHARLIE");
		PajekExportCriteria criteria = showDialog(new File("."), labels);
		if (criteria == null) {
			System.out.println("null");
		} else {
			System.out.println("return=" + criteria.getGraphType());
			System.out.println("return=" + criteria.getPartitionLabels());
			System.out.println("return=" + criteria.getTargetFileName());
		}
	}

	/**
	 * 
	 * 
	 * @param netGUI
	 * @param parentPanel
	 * @param folder
	 */
	public static File selectFile(final Component parentPanel, final File targetFile) {
		File result;

		//
		JFileChooser chooser = new JFileChooser();

		if ((targetFile != null) && (StringUtils.isNotBlank(targetFile.getAbsolutePath()))) {
			chooser.setSelectedFile(ToolBox.setExtension(targetFile, ".paj"));
		}
		chooser.setDialogTitle("Export to Pajek");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setAcceptAllFileFilterUsed(false);
		GenericFileFilter defaultFileFilter = new GenericFileFilter("Pajek Network (*.paj)", "paj");
		chooser.addChoosableFileFilter(defaultFileFilter);
		chooser.setFileFilter(defaultFileFilter);

		//
		if (chooser.showSaveDialog(parentPanel) == JFileChooser.APPROVE_OPTION) {
			System.out.println("getCurrentDirectory(): " + chooser.getCurrentDirectory());
			System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
			result = chooser.getSelectedFile();
		} else {
			result = null;
		}

		//
		return result;
	}

	/**
	 * Launch the application.
	 */
	public static PajekExportCriteria showDialog(final File file, final List<String> sourceLabels) {
		PajekExportCriteria result;

		//
		PajekExportInputDialog dialog = new PajekExportInputDialog(file, sourceLabels);
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getCriteria();

		//
		return result;
	}
}
