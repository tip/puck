package org.tip.puckgui.views.geo;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ListSelectionModel;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.ParserConfigurationException;

import org.geotools.data.DataUtilities;
import org.geotools.data.FeatureSource;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.FeatureCollection;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.renderer.lite.StreamingRenderer;
import org.geotools.styling.SLDParser;
import org.geotools.styling.Style;
import org.geotools.styling.StyleFactory;
import org.geotools.swing.JMapPane;
import org.geotools.swing.MapPane;
import org.geotools.swing.event.MapMouseAdapter;
import org.geotools.swing.event.MapMouseEvent;
import org.geotools.swing.styling.JSimpleStyleDialog;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.FilterFactory;
import org.tip.puck.geo.Place;
import org.tip.puck.geo.Place;
import org.tip.puck.io.gis.SIGFile;
import org.tip.puckgui.util.GenericFileFilter;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;

public class MapPanel_old extends JPanel {

	private static final long serialVersionUID = 7154356808441601309L;

	private JTextField txtFile;

	StyleFactory styleFactory = CommonFactoryFinder.getStyleFactory();
	FilterFactory filterFactory = CommonFactoryFinder.getFilterFactory();

	public MapContent mapContent = new MapContent();
	private JTable table;
	JList listLayers = null;
	DefaultListModel listModel = new DefaultListModel();

	public double lastDraggedX=0;
	public double lastDraggedY=0;

	private enum Tools {
		DRAG, SELECT, ZOOMIN, ZOOMOUT
	}

	Tools selectedTool = Tools.DRAG;

	List<JToggleButton> listToggleButtons = new ArrayList<JToggleButton>();

	/**
	 * Create the panel.
	 */
	public MapPanel_old() {
		setLayout(new BorderLayout(0, 0));

		final JMapPane mapPane = new JMapPane();

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		add(tabbedPane, BorderLayout.CENTER);

		JPanel pMap = new JPanel();
		tabbedPane.addTab("Cartography", null, pMap, null);
		pMap.setLayout(new BorderLayout(0, 0));

		JSplitPane splitPane = new JSplitPane();
		pMap.add(splitPane);

		JPanel panelRightMap = new JPanel();
		splitPane.setRightComponent(panelRightMap);
		panelRightMap.setLayout(new BorderLayout(0, 0));

		JMenuBar menuBarMap = new JMenuBar();
		panelRightMap.add(menuBarMap, BorderLayout.NORTH);

		final JToggleButton tglbtnDrag = new JToggleButton("");
		listToggleButtons.add(tglbtnDrag);
		tglbtnDrag.setIcon(new ImageIcon(MapPanel_old.class.getResource("/org/tip/puckgui/images/gis/mActionPan.png")));
		tglbtnDrag.setSelectedIcon(new ImageIcon(MapPanel_old.class.getResource("/org/tip/puckgui/images/gis/mActionPan_selected.png")));
		menuBarMap.add(tglbtnDrag);
		tglbtnDrag.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				selectedTool = Tools.DRAG;
				MapPanel_old.this.unselectToggleButtonsExcept(tglbtnDrag);
			}
		});

		final JToggleButton tglbtnSelect = new JToggleButton("");
		listToggleButtons.add(tglbtnSelect);
		tglbtnSelect.setIcon(new ImageIcon(MapPanel_old.class.getResource("/org/tip/puckgui/images/gis/mActionSelect.png")));
		tglbtnSelect.setSelectedIcon(new ImageIcon(MapPanel_old.class.getResource("/org/tip/puckgui/images/gis/mActionSelect_selected.png")));
		menuBarMap.add(tglbtnSelect);
		tglbtnSelect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectedTool = Tools.SELECT;
				MapPanel_old.this.unselectToggleButtonsExcept(tglbtnSelect);
			}
		});

		final JToggleButton tglbtnZoomIn = new JToggleButton("");
		listToggleButtons.add(tglbtnZoomIn);
		tglbtnZoomIn.setIcon(new ImageIcon(MapPanel_old.class.getResource("/org/tip/puckgui/images/gis/mActionZoomIn.png")));
		tglbtnZoomIn.setSelectedIcon(new ImageIcon(MapPanel_old.class.getResource("/org/tip/puckgui/images/gis/mActionZoomIn_selected.png")));
		menuBarMap.add(tglbtnZoomIn);
		tglbtnZoomIn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectedTool = Tools.ZOOMIN;
				MapPanel_old.this.unselectToggleButtonsExcept(tglbtnZoomIn);
			}
		});

		final JToggleButton tglbtnZoomOut = new JToggleButton("");
		listToggleButtons.add(tglbtnZoomOut);
		tglbtnZoomOut.setIcon(new ImageIcon(MapPanel_old.class.getResource("/org/tip/puckgui/images/gis/mActionZoomOut.png")));
		tglbtnZoomOut.setSelectedIcon(new ImageIcon(MapPanel_old.class.getResource("/org/tip/puckgui/images/gis/mActionZoomOut_selected.png")));
		menuBarMap.add(tglbtnZoomOut);
		tglbtnZoomOut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectedTool = Tools.ZOOMOUT;
				MapPanel_old.this.unselectToggleButtonsExcept(tglbtnZoomOut);
			}
		});

		JButton tglbtnCenterView = new JButton("");
		tglbtnCenterView.setIcon(new ImageIcon(MapPanel_old.class.getResource("/org/tip/puckgui/images/gis/mActionZoomFullExtent.png")));
		menuBarMap.add(tglbtnCenterView);
		tglbtnCenterView.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				ReferencedEnvelope env = maximiseBoundsForLayers();
				mapPane.setDisplayArea(env);
				mapPane.repaint();
			}
		});

		JButton btExportShapefile = new JButton("");
		btExportShapefile.setIcon(new ImageIcon(MapPanel_old.class.getResource("/org/tip/puckgui/images/gis/mSaveShapefile.png")));
		menuBarMap.add(btExportShapefile);
		btExportShapefile.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				//Export Shapefile
				prepareExportShapeFile();
			}
		});

		JButton btExportSVG = new JButton("");
		btExportSVG.setIcon(new ImageIcon(MapPanel_old.class.getResource("/org/tip/puckgui/images/gis/mSaveSVG.png")));
		menuBarMap.add(btExportSVG);
		btExportSVG.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				//Export SVG
				prepareExportSVG();
			}
		});

		JLabel statusBar = new JLabel("Status bar");
		panelRightMap.add(statusBar, BorderLayout.SOUTH);

		mapPane.addMouseListener(new MapMouseAdapter() {

			@Override
			public void onMouseClicked(MapMouseEvent ev) {

				DirectPosition2D pos = ev.getWorldPos();
				Coordinate coord = new Coordinate(pos.x, pos.y);

				ReferencedEnvelope env = mapPane.getDisplayArea();

				//				ev.getButton() == MouseEvent.

				switch( ev.getButton() ) {

				case MouseEvent.BUTTON1:

					switch (MapPanel_old.this.selectedTool) {
					case DRAG:

						env = MapPanel_old.this.centerOn(env, coord);
						break;

					case ZOOMIN:

						env = MapPanel_old.this.centerOn(env, coord);

						double dh_zi = Math.abs(env.getHeight() - env.getHeight() * 1.03);
						double dw_zi = Math.abs(env.getWidth() - env.getWidth() * 1.03);

						env.expandBy(-Math.max(dh_zi, dw_zi));
						break;

					case ZOOMOUT:

						//					System.out.println("zoomout");

						env = MapPanel_old.this.centerOn(env, coord);

						double dh_zo = Math.abs(env.getHeight() - env.getHeight() * 1.03);
						double dw_zo = Math.abs(env.getWidth() - env.getWidth() * 1.03);

						env.expandBy(Math.max(dh_zo, dw_zo));
						break;

					default:
						break;
					}

					break;

				case MouseEvent.BUTTON2:

					env = MapPanel_old.this.centerOn(env, coord);

					break;

				case MouseEvent.BUTTON3:

					break;

				}

				mapPane.setDisplayArea(env);
				mapPane.repaint();
			}

			@Override
			public void onMouseEntered(MapMouseEvent ev) {
				//				System.out.println("mouse entered map pane");
			}

			@Override
			public void onMouseExited(MapMouseEvent ev) {
				//				System.out.println("mouse left map pane");
			}

		});

		mapPane.addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseDragged(MouseEvent ev) {

				switch( ev.getModifiers() ) {

				case 8:

					System.out.println(ev.getX() + " " + ev.getY());

					Rectangle bounds = ev.getComponent().getBounds();
					ReferencedEnvelope mapArea = mapPane.getDisplayArea();
					double x = (double) (ev.getX());
					double y = (double) (ev.getY());
					double width = mapArea.getWidth();
					double height = mapArea.getHeight();

					double mapX = ((x * width) / (double) bounds.width) + mapArea.getMinX();
					double mapY = (((bounds.getHeight() - y) * height) / (double)bounds.height) + mapArea.getMinY();

					if ((MapPanel_old.this.lastDraggedX > 0) && (MapPanel_old.this.lastDraggedY > 0)) { 
						
						double dx = MapPanel_old.this.lastDraggedX - mapX;
						double dy = MapPanel_old.this.lastDraggedY - mapY;

						System.out.println(mapX + " " + mapY);
						System.out.println(dx + " " + dy);
						System.out.println();

						Coordinate coord = new Coordinate(mapX+dx, mapY+dy);

						ReferencedEnvelope env = MapPanel_old.this.centerOn(mapArea, coord);

						mapPane.setDisplayArea(env);
						mapPane.repaint();

					}
					
					MapPanel_old.this.lastDraggedX = mapX;
					MapPanel_old.this.lastDraggedY = mapY;

				}

			}
		});

		mapPane.addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseWheelMoved(MouseWheelEvent arg0) {

				ReferencedEnvelope env = mapPane.getDisplayArea();

				double clickToZoom = 0.03;
				int clicks = arg0.getWheelRotation();
				// -ve means wheel moved up, +ve means down
				int sign = (clicks < 0 ? -1 : 1);

				double width = env.getWidth();
				double delta = width * clickToZoom * sign;

				env.expandBy(delta, delta);
				mapPane.setDisplayArea(env);
				mapPane.repaint();
			}
		});

		panelRightMap.add(mapPane, BorderLayout.CENTER);

		JPanel panelLeftList = new JPanel();
		splitPane.setLeftComponent(panelLeftList);
		panelLeftList.setLayout(new BorderLayout(0, 0));

		JMenuBar menuBarLayers = new JMenuBar();
		panelLeftList.add(menuBarLayers, BorderLayout.NORTH);

		JButton btnAddShapeLayer = new JButton("");
		btnAddShapeLayer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				JFileChooser inFileDlg = new JFileChooser("user.dir"); //$NON-NLS-1$

				inFileDlg.setFileFilter(new FileNameExtensionFilter("Shapefile (*.shp)", "shp")); //$NON-NLS-1$ //$NON-NLS-2$
				int returnVal = inFileDlg.showOpenDialog(MapPanel_old.this);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					String browseDir = inFileDlg.getCurrentDirectory().toString();

					String inFile = null;
					inFile = inFileDlg.getSelectedFile().toString();
					inFileDlg.setCurrentDirectory(new File(inFile));

					try {

						displaysShapefile(new File(inFile));

					} catch (IOException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}

				}

			}
		});
		btnAddShapeLayer.setIcon(new ImageIcon(MapPanel_old.class.getResource("/org/tip/puckgui/images/add-16x16.png")));
		menuBarLayers.add(btnAddShapeLayer);

		JButton btnRemoveShapeLayer = new JButton("");
		btnRemoveShapeLayer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String selectedLayerTitle = (String) listLayers.getSelectedValue();

				Layer selectedLayer = null;
				//Get selected layer
				for (Layer iterLayer : mapContent.layers()) {
					if( iterLayer.getTitle().compareTo(selectedLayerTitle) == 0) {
						selectedLayer = iterLayer;
						break;
					}
				}

				mapContent.removeLayer(selectedLayer);
				listModel.removeElement(selectedLayerTitle);

				mapPane.repaint();
			}
		});
		btnRemoveShapeLayer.setIcon(new ImageIcon(MapPanel_old.class.getResource("/org/tip/puckgui/images/remove-16x16.png")));
		menuBarLayers.add(btnRemoveShapeLayer);

		JButton btnLayerProperties = new JButton("");
		btnLayerProperties.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String selectedLayerTitle = (String) listLayers.getSelectedValue();

				Layer selectedLayer = null;
				//Get selected layer
				for (Layer iterLayer : mapContent.layers()) {
					if( iterLayer.getTitle().compareTo(selectedLayerTitle) == 0) {
						selectedLayer = iterLayer;
						break;
					}
				}

				if( selectedLayer != null ) {
					//Open style dialog
					Style newStyle = JSimpleStyleDialog.showDialog(null, (SimpleFeatureType)selectedLayer.getFeatureSource().getSchema());
					((FeatureLayer) selectedLayer).setStyle(newStyle);
					mapPane.repaint();

				}
			}
		});
		btnLayerProperties.setIcon(new ImageIcon(MapPanel_old.class.getResource("/org/tip/puckgui/images/gis/mParametersLayer.png")));
		menuBarLayers.add(btnLayerProperties);

		JButton btnLayerUp = new JButton("");
		btnLayerUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String selectedLayerTitle = (String) listLayers.getSelectedValue();
				int selectedIndex = listLayers.getSelectedIndex();

				Layer selectedLayer = null;
				//Get selected layer
				int count = 0;
				for (Layer iterLayer : mapContent.layers()) {
					if( iterLayer.getTitle().compareTo(selectedLayerTitle) == 0) {
						selectedLayer = iterLayer;
						break;
					}
					count++;
				}

				//Calc new position
				int newPosition = count +1;
				System.out.println(mapContent.layers().size());
				System.out.println(listModel.getSize());
				if( newPosition < listModel.getSize()) {

					mapContent.moveLayer(count, newPosition);
					listModel.remove(selectedIndex);
					if( selectedIndex -1 < 0)
						selectedIndex = 0;
					listModel.insertElementAt(selectedLayerTitle, selectedIndex-1);

					listLayers.setSelectedIndex(selectedIndex-1);

					mapPane.repaint();

				}

			}
		});
		btnLayerUp.setIcon(new ImageIcon(MapPanel_old.class.getResource("/org/tip/puckgui/images/up-16x16.png")));
		menuBarLayers.add(btnLayerUp);

		JButton btnLayerDown = new JButton("");
		btnLayerDown.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String selectedLayerTitle = (String) listLayers.getSelectedValue();
				int selectedIndex = listLayers.getSelectedIndex();

				Layer selectedLayer = null;
				//Get selected layer
				int count = 0;
				for (Layer iterLayer : mapContent.layers()) {
					if( iterLayer.getTitle().compareTo(selectedLayerTitle) == 0) {
						selectedLayer = iterLayer;
						break;
					}
					count++;
				}

				//Calc new position
				int newPosition = count -1;
				if( newPosition > -1 ) {
					//					newPosition = listModel.getSize()-1;

					mapContent.moveLayer(count, newPosition);
					listModel.remove(selectedIndex);
					if( selectedIndex +1 > listModel.getSize())
						selectedIndex = listModel.getSize()-1;
					listModel.insertElementAt(selectedLayerTitle, selectedIndex+1);

					mapPane.repaint();

					listLayers.setSelectedIndex(selectedIndex+1);

				}
			}
		});
		btnLayerDown.setIcon(new ImageIcon(MapPanel_old.class.getResource("/org/tip/puckgui/images/down-16x16.png")));
		menuBarLayers.add(btnLayerDown);

		listLayers = new JList(listModel);
		panelLeftList.add(listLayers, BorderLayout.CENTER);

		JPanel pConfig = new JPanel();
		tabbedPane.addTab("Parameters", null, pConfig, null);
		pConfig.setLayout(null);

		JCheckBox chckbxGeonames = new JCheckBox("Geonames");
		chckbxGeonames.setSelected(true);
		chckbxGeonames.setBounds(8, 8, 129, 23);
		pConfig.add(chckbxGeonames);

		JCheckBox chckbxPersonnalDatabase = new JCheckBox("Personnal Database");
		chckbxPersonnalDatabase.setBounds(8, 35, 175, 23);
		pConfig.add(chckbxPersonnalDatabase);

		txtFile = new JTextField();
		txtFile.setEnabled(false);
		txtFile.setText("File");
		txtFile.setBounds(23, 66, 114, 19);
		pConfig.add(txtFile);
		txtFile.setColumns(10);

		JButton btnNewButton = new JButton("Browse");
		btnNewButton.setEnabled(false);
		btnNewButton.setBounds(149, 63, 87, 25);
		pConfig.add(btnNewButton);

		JButton btnGeocode = new JButton("Geocode");
		btnGeocode.setBounds(316, 236, 117, 25);
		pConfig.add(btnGeocode);

		//Map content
		mapPane.setRenderer( new StreamingRenderer() );
		//		GTRenderer renderer = new StreamingRenderer();

		mapPane.setMapContent( this.mapContent );

		JPanel pTableGeoView = new JPanel();
		tabbedPane.addTab("Attributes table", null, pTableGeoView, null);
		pTableGeoView.setLayout(new BorderLayout(0, 0));

		//		JLabel lblStatus = new JLabel("Statusbar");
		//		pTableGeoView.add(lblStatus, BorderLayout.SOUTH);

		JMenuBar menuBar = new JMenuBar();
		pTableGeoView.add(menuBar, BorderLayout.NORTH);

		JButton btnAddPoint = new JButton("");
		btnAddPoint.setIcon(new ImageIcon(MapPanel_old.class.getResource("/org/tip/puckgui/images/add-16x16.png")));
		menuBar.add(btnAddPoint);

		JButton btnDelPoint = new JButton("");
		btnDelPoint.setIcon(new ImageIcon(MapPanel_old.class.getResource("/org/tip/puckgui/images/remove-16x16.png")));
		menuBar.add(btnDelPoint);

		table = new JTable();
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		String[] colName = { "id", "Name", "Country", "Long", "Lat" };

		DefaultTableModel modelAttributesTable = (DefaultTableModel) table.getModel();
		modelAttributesTable.setColumnIdentifiers(colName);

		table.setModel(modelAttributesTable);

		pTableGeoView.add(table, BorderLayout.CENTER);

		try {
			displayWorldBorderShapefile();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void setUpTableData(FeatureSource<?, ?> featureSourcePoints) {
		DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
		ArrayList<Place> list = new ArrayList<Place>();

		SimpleFeatureIterator iterator = null;
		try {
			iterator = (SimpleFeatureIterator) featureSourcePoints.getFeatures().features();

			while( iterator.hasNext() ){
				SimpleFeature feature = iterator.next();
				// process feature

				Geometry g = (Geometry) feature.getAttribute("the_geom");

				list.add(new Place((Integer)feature.getAttribute(0), (String)feature.getAttribute(1), "", "", "", (String)feature.getAttribute(2), g.getCoordinate()));

			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			iterator.close();
		};

		for (int i = 0; i < list.size(); i++) {
			Object[] data = new Object[5];

			data[0] = (Integer) list.get(i).getIdGeonames();
			data[1] = (String) list.get(i).getToponym();

			//			data[?] = list.get(i).getGeonames1();
			//			data[?] = list.get(i).getGeonames2();
			//			data[?] = list.get(i).getGeonames3();
			data[2] = list.get(i).getCountry_ISO2();
			data[3] = new Double(list.get(i).getCoordinate().x);
			data[4] = new Double(list.get(i).getCoordinate().y);

			tableModel.addRow(data);
		}
		table.setModel(tableModel);
		tableModel.fireTableDataChanged();
		table.repaint();
	}

	private void displayWorldBorderShapefile() throws IOException {

// 06/05/2015: temporary fix for Eclipse under Microsoft Windows, because %20 is not available in path. 		
//		URL fileWorldBorders = MapPanel.class.getResource("/org/tip/puckgui/views/geo/data/TM_WORLD_BORDERS-0.3.shp");
//
//		File file = new File(fileWorldBorders.getFile());

		//Extract shapefile from jar in temp directory
		String dirTemp = System.getProperty("java.io.tmpdir");
		
		//NEED TO BE CLEAN, UNTIL IT FULLY WORKS IN A SIGNED JAR FILE AND BE VALIDATED
		
//		URL urlShapeFileZip = getClass().getResource("/org/tip/puckgui/views/geo/data/TM_WORLD_BORDERS-0.3.zip");
//		URL urlShapeFile = getClass().getResource("/org/tip/puckgui/views/geo/data/TM_WORLD_BORDERS-0.3.shp");
//		URL urlDbfFile = getClass().getResource("/org/tip/puckgui/views/geo/data/TM_WORLD_BORDERS-0.3.dbf");
//		URL urlPrjFile = getClass().getResource("/org/tip/puckgui/views/geo/data/TM_WORLD_BORDERS-0.3.prj");
//		URL urlQixFile = getClass().getResource("/org/tip/puckgui/views/geo/data/TM_WORLD_BORDERS-0.3.qix");
//		URL urlShxFile = getClass().getResource("/org/tip/puckgui/views/geo/data/TM_WORLD_BORDERS-0.3.shx");
//		URL urlSldFile = getClass().getResource("/org/tip/puckgui/views/geo/data/TM_WORLD_BORDERS-0.3.sld");
		
//		ShapefileReader sfr = new ShapefileReader(urlShapeFileZip.getFile(), false, , new GeometryFactory());
		
//		extractResourcesToTempFolder(dirTemp);
		
//		File tempShpFile = File.createTempFile("TM_WORLD_BORDERS-0.3", null, new File(urlShapeFile.getFile()));
//		File tempDbfFile = File.createTempFile("TM_WORLD_BORDERS-0.3", ".db, new File(urlDbfFile.getFile()));
//		File tempPrjFile = File.createTempFile("TM_WORLD_BORDERS-0.3", ".prj", new File(urlPrjFile.getFile()));
//		File tempQixFile = File.createTempFile("TM_WORLD_BORDERS-0.3", ".qix", new File(urlQixFile.getFile()));
//		File tempShxFile = File.createTempFile("TM_WORLD_BORDERS-0.3", ".shx", new File(urlShxFile.getFile()));
//		File tempSldFile = File.createTempFile("TM_WORLD_BORDERS-0.3", ".sld", new File(urlStyle.getFile()));
		
		//Convert path to separators / \ to system compliance
//		String fileNameShp = FilenameUtils.separatorsToSystem(MapPanel.class.getResource("/org/tip/puckgui/views/geo/data/TM_WORLD_BORDERS-0.3.shp").getFile());
		
		//With URL
		URL urlShapeFile = getClass().getResource("/org/tip/puckgui/views/geo/TM_WORLD_BORDERS-0.3.shp");
		URL urlSldFile = getClass().getResource("/org/tip/puckgui/views/geo/TM_WORLD_BORDERS-0.3.sld");
		System.out.println(urlShapeFile);
		System.out.println(urlSldFile);
		displaysShapefile(urlShapeFile, urlSldFile);
		
		//With File
//		File file = new File(fileName);
//		displaysShapefile(file);
		
		//With Stream
//		InputStream isSHP = this.getClass().getClassLoader().getResourceAsStream(fileName);

	}

	
	private void displaysShapefile(File file) throws IOException {

		FileDataStore store = FileDataStoreFinder.getDataStore(file);
		FeatureSource<?, ?> featureSource = store.getFeatureSource();

		// Create a basic Style to render the features
		Style style = createStyle(file, featureSource);

		// Add the features and the associated Style object to
		// the MapContext as a new MapLayer
		displayFeatureSourceLayer(file.getName(), featureSource, style);

	}
	
	private void displaysShapefile(URL urlSHP, URL urlStyle) throws IOException {

//		File file = new File(url.getPath());
		
		System.out.println("create style");
//		System.out.println(urlStyle);
		Style style = createFromSLD(urlStyle);
		System.out.println(style.toString());
		
		FileDataStore store = FileDataStoreFinder.getDataStore(urlSHP);
		FeatureSource<?, ?> featureSource = store.getFeatureSource();

		// Create a basic Style to render the features
//		Style style = createStyle(file, featureSource);

		// Add the features and the associated Style object to
		// the MapContext as a new MapLayer
		displayFeatureSourceLayer("World borders", featureSource, style);

	}

	/**
	 * Create a Style to display the features. If an SLD file is in the same
	 * directory as the shapefile then we will create the Style by processing
	 * this. Otherwise we display a JSimpleStyleDialog to prompt the user for
	 * preferences.
	 */
	private Style createStyle(File file, FeatureSource<?, ?> featureSource) {
		File sld = toSLDFile(file);
		if (sld != null) {
			try {
				return createFromSLD(sld.toURI().toURL());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		SimpleFeatureType schema = (SimpleFeatureType)featureSource.getSchema();
		return JSimpleStyleDialog.showDialog(null, schema);
	}

	/**
	 * Figure out if a valid SLD file is available.
	 */
	public File toSLDFile(File file)  {
		String path = file.getAbsolutePath();
		String base = path.substring(0,path.length()-4);
		String newPath = base + ".sld";
		File sld = new File( newPath );
		if( sld.exists() ){
			return sld;
		}
		newPath = base + ".SLD";
		sld = new File( newPath );
		if( sld.exists() ){
			return sld;
		}
		return null;
	}

	/**
	 * Create a Style object from a definition in a SLD document
	 */
	private Style createFromSLD(URL sld) {
		try {
			SLDParser stylereader = new SLDParser(styleFactory, sld);
			Style[] style = stylereader.readXML();
			return style[0];

		} catch (Exception e) {
			//            ExceptionMonitor.show(null, e, "Problem creating style");
		}
		return null;
	}

	public void displayFeatureSourceLayer(String title, FeatureSource<?, ?> featureSource, Style style) {

		FeatureLayer layer = new FeatureLayer(featureSource, style);
		mapContent.addLayer(layer);
		layer.setTitle(title);
		listModel.addElement(title);

	}

	public void displayFeatureCollectionLayer(String title, FeatureCollection<?, ?> featureCollection, Style style) {

		FeatureLayer layer = new FeatureLayer(featureCollection, style);
		mapContent.addLayer(layer);
		layer.setTitle(title);
		listModel.addElement(title);
	}

	public void diplayCollectionsPointsAndLines(ArrayList<DefaultFeatureCollection> collectionfeaturesReadyToBeDisplayed) {

		//Display points
		FeatureSource<?, ?> featureSourcePoints = DataUtilities.source(collectionfeaturesReadyToBeDisplayed.get(0));
//		Style stylePoints = createFromSLD(new File(MapPanel.class.getResource("/org/tip/puckgui/views/geo/data/SCHEMA_POINTS.sld").getFile()));	//mode File
		Style stylePoints = createFromSLD(MapPanel_old.class.getResource("/org/tip/puckgui/views/geo/data/SCHEMA_POINTS.sld"));	//mode URL
		FeatureLayer layerPoints = new FeatureLayer(featureSourcePoints, stylePoints);

		setUpTableData(featureSourcePoints);

		ReferencedEnvelope bounds = layerPoints.getBounds();
		double dh = Math.abs(bounds.getHeight() - bounds.getHeight() * 1.1);
		double dw = Math.abs(bounds.getWidth() - bounds.getWidth() * 1.1);

		bounds.expandBy(Math.max(dh, dw));

		//Display points
		FeatureSource<?, ?> featureSourceLines = DataUtilities.source(collectionfeaturesReadyToBeDisplayed.get(1));
//		Style styleLines = createFromSLD(new File(MapPanel.class.getResource("/org/tip/puckgui/views/geo/data/SCHEMA_LINES.sld").getFile()));	//mode File
		Style styleLines = createFromSLD(MapPanel_old.class.getResource("/org/tip/puckgui/views/geo/data/SCHEMA_LINES.sld"));	//mode URL
		FeatureLayer layerLines = new FeatureLayer(featureSourceLines, styleLines);

		mapContent.addLayer(layerPoints);

		layerPoints.setTitle("Points Layer");
		listModel.add(listModel.getSize()-1, layerPoints.getTitle());
		mapContent.addLayer(layerLines);
		layerLines.setTitle("Links Layer");
		listModel.add(listModel.getSize()-1, layerLines.getTitle());

		mapContent.getViewport().setBounds(bounds);
	}

	public List<Layer> getLayers() {

		return mapContent.layers();

	}

	public ReferencedEnvelope maximiseBoundsForLayers() {

		double minX = Double.MAX_VALUE, minY = Double.MAX_VALUE;
		double maxX = Double.MIN_VALUE, maxY = Double.MIN_VALUE;

		for (Layer iterLayer : getLayers()) {

			if( iterLayer.getTitle().compareTo("TM_WORLD_BORDERS-0.3.shp") != 0 ) {

				ReferencedEnvelope bounds = iterLayer.getBounds();

				double minXTemp = bounds.getMinX();
				minX = Math.min(minX, minXTemp);
				double maxXTemp = bounds.getMaxX();
				maxX = Math.max(maxX, maxXTemp);

				double minYTemp = bounds.getMinY();
				minY = Math.min(minY, minYTemp);
				double maxYTemp = bounds.getMaxY();
				maxY = Math.max(maxY, maxYTemp);
			}
		}

		ReferencedEnvelope env = new ReferencedEnvelope();
		env.init(minX, maxX, minY, maxY);

		double dh = Math.abs(env.getHeight() - env.getHeight() * 1.1);
		double dw = Math.abs(env.getWidth() - env.getWidth() * 1.1);

		env.expandBy(Math.max(dh, dw));

		return env;

	}

	private void unselectToggleButtonsExcept(JToggleButton tglbSelected){
		for (JToggleButton iterTglb: listToggleButtons) {
			iterTglb.setSelected(false);
		}
		tglbSelected.setSelected(true);
	}

	private ReferencedEnvelope centerOn(ReferencedEnvelope env, Coordinate coordClicked){
		Coordinate pEnv = env.centre();
		double dX = -(pEnv.x - coordClicked.x);
		double dY = -(pEnv.y - coordClicked.y);
		env.translate(dX, dY);

		return env;

	}

	private DirectPosition2D calculateWorldPos(MapPane pane, MouseEvent event) {
		AffineTransform tr = pane.getScreenToWorldTransform();
		DirectPosition2D pos = new DirectPosition2D(event.getX(), event.getY());
		tr.transform(pos, pos);
		pos.setCoordinateReferenceSystem(pane.getMapContent().getCoordinateReferenceSystem());
		return pos;
	}

	private void prepareExportSVG() {

		OutputStream output;
		File targetFile = new File("exportPuck.svg");

		boolean ended = false;
		while (!ended) {
			JFileChooser chooser = new JFileChooser();
			chooser.setSelectedFile(targetFile);

			chooser.setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.saveAsFileChooser.text"));
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			chooser.setAcceptAllFileFilterUsed(false);
			GenericFileFilter defaultFileFilter = new GenericFileFilter("Shapefile (*.shp)", "shp");
			chooser.addChoosableFileFilter(defaultFileFilter);
			chooser.setFileFilter(defaultFileFilter);

			if (chooser.showSaveDialog(MapPanel_old.this) == JFileChooser.APPROVE_OPTION) {
				targetFile = chooser.getSelectedFile();

				boolean doSave;
				if (targetFile.exists()) {
					// Manage confirmation dialog.
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.title");
					String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.text");

					int response = JOptionPane.showConfirmDialog(MapPanel_old.this, message, title, JOptionPane.YES_NO_CANCEL_OPTION);

					if (response == JOptionPane.YES_OPTION) {
						doSave = true;
						ended = true;
					} else if (response == JOptionPane.NO_OPTION) {
						doSave = false;
						ended = false;
					} else {
						doSave = false;
						ended = true;
					}
				} else {
					doSave = true;
					ended = true;
				}

				if (doSave) {

					try {

						output = new FileOutputStream(targetFile);

						ReferencedEnvelope env = maximiseBoundsForLayers();

						//						Dimension dim = new Dimension(4*new Double(env.getWidth()).intValue(), 4*new Double(env.getHeight()).intValue());

						SIGFile.exportSVG(mapContent, env, output, null);

					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ParserConfigurationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			} else {
				// Nothing to do.
				System.out.println("No Selection ");
				ended = true;
			}
		}
	}

	private void prepareExportShapeFile() {
		File targetFile = new File("exportPuck.shp");
		boolean ended = false;
		while (!ended) {
			JFileChooser chooser = new JFileChooser();
			chooser.setSelectedFile(targetFile);

			chooser.setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.saveAsFileChooser.text"));
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			chooser.setAcceptAllFileFilterUsed(false);
			GenericFileFilter defaultFileFilter = new GenericFileFilter("Shapefile (*.shp)", "shp");
			chooser.addChoosableFileFilter(defaultFileFilter);
			chooser.setFileFilter(defaultFileFilter);

			//
			if (chooser.showSaveDialog(MapPanel_old.this) == JFileChooser.APPROVE_OPTION) {
				targetFile = chooser.getSelectedFile();

				boolean doSave;
				if (targetFile.exists()) {
					// Manage confirmation dialog.
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.title");
					String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.text");

					int response = JOptionPane.showConfirmDialog(MapPanel_old.this, message, title, JOptionPane.YES_NO_CANCEL_OPTION);

					if (response == JOptionPane.YES_OPTION) {
						doSave = true;
						ended = true;
					} else if (response == JOptionPane.NO_OPTION) {
						doSave = false;
						ended = false;
					} else {
						doSave = false;
						ended = true;
					}
				} else {
					doSave = true;
					ended = true;
				}

				if (doSave) {

					SIGFile.exportToShapefile(MapPanel_old.this.getLayers(), targetFile);
				}

			} else {
				// Nothing to do.
				System.out.println("No Selection ");
				ended = true;
			}
		}
	}

}