package org.tip.puck.graphs.random;

import java.util.Arrays;

/**
 * 
 * @author TIP
 */
public class RandomCriteria {

	public enum ChoiceMode {
		OBSERVER,
		AGENT;
	}

	private int nodeCount;
	private int arcWeightSum;
	private double outPreference;
	private double[] inertia;
	private int runCount;
	private boolean extractRepresentative;
	private boolean showProbabilityEvolution;
	private DistributionType distributionType;


	public DistributionType getDistributionType() {
		return distributionType;
	}

	/**
	 * 
	 * @param choiceMode
	 */
	public RandomCriteria() {
		this.nodeCount = 100;
		this.arcWeightSum = 100;
		this.outPreference = 0.5;
		this.inertia = new double[] { 1., 1., 1. };
		this.runCount = 100;
		this.extractRepresentative = false;
		this.showProbabilityEvolution = false;
		this.distributionType = DistributionType.FREE;
	}

	public void setDistributionType(DistributionType distributionType) {
		this.distributionType = distributionType;
	}

	public int getArcWeightSum() {
		return arcWeightSum;
	}

	public double[] getInertia() {
		double[] result;

		result = this.inertia;

		//
		return result;
	}

	public double getInertia0() {
		return inertia[0];
	}

	public double getInertia1() {
		return inertia[1];
	}

	public double getInertia2() {
		return inertia[2];
	}

	public String getInertiaPattern() {
		return Arrays.toString(getInertia());
	}

	public int getNodeCount() {
		return nodeCount;
	}

	public double getOutPreference() {
		return outPreference;
	}

	public int getRunCount() {
		return runCount;
	}

	public boolean isExtractRepresentative() {
		return extractRepresentative;
	}

	public boolean isShowProbabilityEvolution() {
		return showProbabilityEvolution;
	}

	public void setArcWeightSum(final int arcWeightSum) {
		this.arcWeightSum = arcWeightSum;
	}

	public void setExtractRepresentative(final boolean extractRepresentative) {
		this.extractRepresentative = extractRepresentative;
	}

	public void setInertia0(final double inertia0) {
		this.inertia[0] = inertia0;
	}

	public void setInertia1(final double inertia1) {
		this.inertia[1] = inertia1;
	}

	public void setInertia2(final double inertia2) {
		this.inertia[2] = inertia2;
	}

	public void setNodeCount(final int nodeCount) {
		this.nodeCount = nodeCount;
	}

	public void setOutPreference(final double outPreference) {
		this.outPreference = outPreference;
	}

	public void setRunCount(final int runCount) {
		this.runCount = runCount;
	}

	public void setShowProbabilityEvolution(final boolean showProbabilityEvolution) {
		this.showProbabilityEvolution = showProbabilityEvolution;
	}
}
