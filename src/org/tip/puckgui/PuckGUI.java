package org.tip.puckgui;

import java.awt.EventQueue;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.flatdb4geonames.model.FlatDB4GeoNames;
import org.tip.puck.PuckException;
import org.tip.puck.PuckManager;
import org.tip.puck.graphs.Graph;
import org.tip.puck.io.kinsources.KinsourcesFile;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puckgui.util.GUIToolBox;

/**
 * 
 * @author TIP
 * 
 */
public class PuckGUI {

	/** Lazy-loading with holder */
	private static class SingletonLoader {
		//
		private static final PuckGUI instance = new PuckGUI();
	}

	private static final Logger logger = LoggerFactory.getLogger(PuckGUI.class);

	private List<WindowGUI> windowGUIs;
	private int windowGUICounter;
	private File recentFilesFile;
	private RecentFiles recentFiles;
	private File recentFoldersFile;
	private RecentFiles recentFolders;
	private File puckHome;
	private File preferencesFile;
	private Preferences preferences;

	/**
	 * 
	 */
	private PuckGUI() {

		// Set Puck home directory.
		this.puckHome = new File(System.getProperty("user.home") + File.separator + ".puck");
		if (!this.puckHome.exists()) {
			//
			this.puckHome.mkdir();
			logger.info("Puck home directory created: " + this.puckHome.getAbsolutePath());
		}

		// Initialize the netGUI counter (each main window has an id).
		this.windowGUICounter = 0;
		this.windowGUIs = new ArrayList<WindowGUI>();

		// Set preferences.
		this.preferencesFile = new File(this.puckHome.getAbsolutePath() + File.separator + "preferencesrc");
		if (this.preferencesFile.exists()) {
			//
			try {
				//
				this.preferences = PreferencesFile.load(this.preferencesFile);

			} catch (final Exception exception) {
				//
				exception.printStackTrace();
				logger.error("Error loading preferences file: " + exception.getMessage());
				logger.warn("Ignoring preferences file.");
				this.preferences = new Preferences();
			}
		} else {
			//
			logger.info("No preferences file found.");
			this.preferences = new Preferences();
		}
		updateLanguage();

		// Set recent files.
		this.recentFilesFile = new File(this.puckHome.getAbsolutePath() + File.separator + "recentfilesrc");
		if (this.recentFilesFile.exists()) {
			try {
				this.recentFiles = RecentFilesFile.load(this.recentFilesFile);
			} catch (final PuckException exception) {
				logger.error("Error loading recent files file: " + exception.getMessage());
				logger.warn("Ignoring recent files file.");
				this.recentFiles = new RecentFiles();
			}
		} else {
			this.recentFiles = new RecentFiles();
		}

		// Set recent folders.
		this.recentFoldersFile = new File(this.puckHome.getAbsolutePath() + File.separator + "recentfoldersrc");
		if (this.recentFoldersFile.exists()) {
			try {
				this.recentFolders = RecentFilesFile.load(this.recentFoldersFile);
			} catch (final PuckException exception) {
				logger.error("Error loading recent folders file: " + exception.getMessage());
				logger.warn("Ignoring recent folders file.");
				this.recentFolders = new RecentFiles();
			}
		} else {
			this.recentFolders = new RecentFiles();
		}

		// Remove BOLD on default font.
		UIManager.put("swing.boldMetal", Boolean.FALSE);

		// Set LookAndFeel.
		System.out.println("System lookAndFeel property:" + System.getProperty("swing.defaultlaf"));
		System.out.println("Available lookAndFeel: " + GUIToolBox.availableLookAndFeels().toString());
		System.out.println("System lookAndFeel: " + UIManager.getSystemLookAndFeelClassName());
		System.out.println("Current lookAndFeel: " + UIManager.getLookAndFeel().getName());

		if (!StringUtils.equals(UIManager.getSystemLookAndFeelClassName(), "javax.swing.plaf.metal.MetalLookAndFeel")) {
			try {
				System.out.println("Metal LAF setted and system LAF detected, try to set system LAF.");
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (final Exception exception) {
				System.out.println("Failed to set the system LookAndFeel.");
			}
		} else if (GUIToolBox.availableLookAndFeels().toString().contains("GTK+")) {
			try {
				System.out.println("Metal LAF setted and GTK+ LAF detected, try to set GTK+ LAF.");
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
			} catch (final Exception exception) {
				System.out.println("Failed to set the system LookAndFeel.");
			}
		}

		System.out.println("Activated lookAndFeel: " + UIManager.getLookAndFeel().getName());

		// Set default GUI catch.
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			/**
			 *
			 */
			@Override
			public void uncaughtException(final Thread thread, final Throwable exception) {

				// Show trace.
				exception.printStackTrace();

				//
				String title = "Error computerum est";

				//
				String message;
				if (exception instanceof OutOfMemoryError) {
					message = "Java ran out of memory!";
				} else {
					message = "An unexpected error occured:\n\n" + exception.getMessage();
				}

				//
				JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
			}
		});
	}

	/**
	 * Close a netGUI and ends Puck if there is no more netGUI to manage.
	 * 
	 * @param netGUI
	 *            The netGUI to close.
	 */
	public void close(final GroupNetGUI gui) {
		this.windowGUIs.remove(gui);
		if (this.windowGUIs.size() == 0) {
			exit();
		}
	}

	/**
	 * Close a netGUI and ends Puck if there is no more netGUI to manage.
	 * 
	 * @param netGUI
	 *            The netGUI to close.
	 */
	public void close(final WindowGUI gui) {
		this.windowGUIs.remove(gui);
		if (this.windowGUIs.size() == 0) {
			exit();
		}
	}

	/**
	 * @throws PuckException
	 * @throws Exception
	 * 
	 */
	public GroupNetGUI createGroupNetGUI(final Graph<Cluster<Individual>> graph) throws PuckException {
		GroupNetGUI result;

		this.windowGUICounter += 1;
		result = new GroupNetGUI(this.windowGUICounter, graph);
		this.windowGUIs.add(result);

		//
		return result;
	}

	/**
	 * 
	 */
	public NetGUI createNetGUI() {
		NetGUI result;

		this.windowGUICounter += 1;
		result = new NetGUI(this.windowGUICounter);
		this.windowGUIs.add(result);

		//
		return result;
	}

	/**
	 * @throws PuckException
	 * @throws Exception
	 * 
	 */
	public NetGUI createNetGUI(final File file) throws PuckException {
		NetGUI result;

		result = createNetGUI(file, PuckManager.DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * 
	 */
	public NetGUI createNetGUI(final File file, final Net net) {
		NetGUI result;

		this.windowGUICounter += 1;

		//
		result = new NetGUI(this.windowGUICounter, file, net);
		this.windowGUIs.add(result);

		//
		return result;
	}

	/**
	 * @throws PuckException
	 * 
	 */
	public NetGUI createNetGUI(final File file, final Net net, final Segmentation segmentation) throws PuckException {
		NetGUI result;

		this.windowGUICounter += 1;

		//
		result = new NetGUI(this.windowGUICounter, file, net, segmentation);
		this.windowGUIs.add(result);

		//
		return result;
	}

	/**
	 * @throws PuckException
	 * @throws Exception
	 * 
	 */
	public NetGUI createNetGUI(final File file, final String charsetName) throws PuckException {
		NetGUI result;

		Net net = PuckManager.loadNet(file, charsetName);

		this.windowGUICounter += 1;
		result = new NetGUI(this.windowGUICounter, file, net);
		this.windowGUIs.add(result);

		//
		return result;
	}

	/**
	 * Something, we create a NetGUI from a file and a net, but the filename has
	 * to change in way to avoid accidently remove.
	 */
	public NetGUI createNetGUIWithNewFile(final File file, final Net net) {
		NetGUI result;

		this.windowGUICounter += 1;

		// Insert a difference in the file name.
		File targetFile;
		if (file.getName().contains(".")) {
			String filePathName = file.getAbsolutePath();
			int separatorIndex = filePathName.lastIndexOf('.');
			String leftPart = filePathName.substring(0, separatorIndex);
			String rightPart = filePathName.substring(separatorIndex);
			targetFile = new File(leftPart + "-" + this.windowGUICounter + rightPart);
		} else {
			targetFile = new File(file.getName() + "-" + this.windowGUICounter);
		}

		//
		result = new NetGUI(this.windowGUICounter, targetFile, net);
		this.windowGUIs.add(result);

		//
		return result;
	}

	/**
	 * 
	 * @param gui
	 * @return
	 * @throws PuckException
	 */
	public NetGUI duplicate(final NetGUI source) throws PuckException {
		NetGUI result;

		//
		Net targetNet = new Net(source.getNet());

		//
		Segmentation targetSegmentation = new Segmentation(targetNet, source.getSegmentation());

		//
		result = createNetGUI(source.getFile(), targetNet, targetSegmentation);

		//
		result.setChanged(source.isChanged());

		//
		return result;
	}

	/**
	 * Checks for unsaved netGUI.
	 * 
	 * @return true if unsaved netGUI is existing, false otherwise.
	 */
	public boolean existsUnsavedChanges() {
		boolean result;

		boolean ended = false;
		int count = 0;
		result = false;
		while (!ended) {
			if (count < this.windowGUIs.size()) {
				if (this.windowGUIs.get(count).isChanged()) {
					ended = true;
					result = true;
				} else {
					count += 1;
				}
			} else {
				ended = true;
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public void exit() {
		//
		try {
			RecentFilesFile.save(this.recentFilesFile, this.recentFiles);
			RecentFilesFile.save(this.recentFoldersFile, this.recentFolders);
			savePreferences();
		} catch (final Exception exception) {
			exception.printStackTrace();
			logger.error("Error saving recent files data: " + exception.getMessage());
			logger.warn("Ignoring properties file save.");
		}

		//
		System.exit(0);
	}

	public Preferences getPreferences() {
		return this.preferences;
	}

	/**
	 * 
	 * @return
	 */
	public List<NetGUI> netGUIs() {
		List<NetGUI> result;

		result = new ArrayList<NetGUI>();

		for (WindowGUI gui : this.windowGUIs) {
			if (gui instanceof NetGUI) {
				result.add((NetGUI) gui);
			}
		}

		//
		return result;
	}

	public RecentFiles recentFiles() {
		return this.recentFiles;
	}

	public RecentFiles recentFolders() {
		return this.recentFolders;
	}

	/**
	 *  
	 */
	public void run(final String[] fileNames) {
		// Create the default main window.
		if ((fileNames != null) && (fileNames.length > 0)) {
			//
			for (String fileName : fileNames) {
				//
				try {
					//
					createNetGUI(new File(fileName));

				} catch (final Exception exception) {
					//
					exception.printStackTrace();
					System.err.println("Error loading file named [" + fileName + "]");
				}
			}
		} else if ((this.preferences.isAutoLoadLastFile()) && (!this.recentFiles.isEmpty())) {
			//
			try {
				//
				createNetGUI(this.recentFiles.getMoreRecent());

			} catch (Exception exception) {
				//
				exception.printStackTrace();
			}
		}

		if (this.windowGUIs.isEmpty()) {
			//
			createNetGUI();
		}

		//
		String databaseDirectory = this.preferences.getFlatDB4GeonamesDirectory();
		if (StringUtils.isNotBlank(databaseDirectory)) {
			//
			try {
				//
				FlatDB4GeoNames.open(databaseDirectory);
				logger.debug("FlatDB4GeoNames opened: " + databaseDirectory);

			} catch (Exception exception) {
				//
				logger.error("Cannot open FlatDB4GeoNames database: " + databaseDirectory);
				this.preferences.setFlatDB4GeoNamesDirectory(null);
			}
		}
	}

	/**
	 * 
	 */
	public void savePreferences() throws PuckException {
		PreferencesFile.save(this.preferencesFile, this.preferences);
	}

	/**
	 * 
	 */
	public void updateLanguage() {

		Locale currentLocale = this.preferences.getLanguage().locale();

		// Change JVM default locale.
		java.util.Locale.setDefault(currentLocale);

		// Change LookAndFeel default locale.
		javax.swing.UIManager.getDefaults().setDefaultLocale(currentLocale);

		// Change new component default locale.
		javax.swing.JComponent.setDefaultLocale(currentLocale);

		//
		ResourceBundle.clearCache();

		//
		for (WindowGUI windowGUI : this.windowGUIs) {
			// if (windowGUI instanceof NetGUI) {
			// createNetGUI((windowGUI) windowGUI);
			// }
			windowGUI.updateLocale(currentLocale);
		}
	}

	/**
	 * 
	 * @return
	 */
	public List<WindowGUI> windowGUIs() {
		List<WindowGUI> result;

		result = this.windowGUIs;

		//
		return result;
	}

	/***
	 * 
	 * @return
	 */
	public static PuckGUI instance() {
		PuckGUI result;

		result = SingletonLoader.instance;

		//
		return result;
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {

		// Fix SSL Java 6 prime size limit. Requirement for the
		// "Load from Kinsources" functionality.
		KinsourcesFile.fixPrimeSizeLimitInJava6();
		System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");

		// Configure log.
		File loggerConfig = new File("log4j.properties");
		if (loggerConfig.exists()) {

			PropertyConfigurator.configure(loggerConfig.getAbsolutePath());
			logger.info("Dedicated log configuration done.");
			logger.info("Configuration file was found in [{}].", loggerConfig.getAbsoluteFile());

		} else {

			BasicConfigurator.configure();
			logger.info("Basic log configuration done.");
			logger.info("Configuration file was not found in [{}].", loggerConfig.getAbsoluteFile());
		}

		// Manage parameter.
		if ((args.length > 0) && (StringUtils.startsWithAny(args[0], "-h", "-help", "--help"))) {
			//
			System.out.println("puck [-h|-help|--help|fileNames|fileName1 ... fileNameN]");

		} else {
			//
			EventQueue.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						//
						PuckGUI.instance().run(args);

					} catch (Exception exception) {
						//
						exception.printStackTrace();
					}
				}
			});
		}
	}
}
