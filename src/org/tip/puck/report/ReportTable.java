package org.tip.puck.report;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TreeMap;

import org.tip.puck.util.MathUtils;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class ReportTable {

	private String title;
	private int rowCount;
	private int columnCount;
	private Object[][] data;

	/**
	 * 
	 */
	public ReportTable(final int rowCount, final int columnCount) {
		this.rowCount = rowCount;
		this.columnCount = columnCount;
		this.data = new String[rowCount][columnCount];
	}

	/**
	 * 
	 */
	public ReportTable(final ReportTable source) {
		this.rowCount = source.getRowCount();
		this.columnCount = source.getColumnCount();
		this.data = new String[this.rowCount][this.columnCount];
		for (int rowIndex = 0; rowIndex < this.rowCount; rowIndex++) {
			for (int columnIndex = 0; columnIndex < this.columnCount; columnIndex++) {
				this.data[rowIndex][columnIndex] = source.get(rowIndex, columnIndex);
			}
		}
	}

	/**
	 * 
	 * @param rowIndex
	 * @param columnIndex
	 * @return
	 */
	public Object get(final int rowIndex, final int columnIndex) {
		Object result;

		result = this.data[rowIndex][columnIndex];

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int getColumnCount() {
		int result;

		result = this.columnCount;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int getRowCount() {
		int result;

		result = this.rowCount;

		//
		return result;
	}

	/**
	 * 
	 * @param rowIndex
	 * @param columnIndex
	 * @return
	 */
	public String getString(final int rowIndex, final int columnIndex) {
		String result;

		Object data = this.data[rowIndex][columnIndex];
		if (data == null) {
			result = null;
		} else {
			result = data.toString();
		}

		//
		return result;
	}

	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @param lalel
	 * @param value
	 */
	public void set(final int rowIndex, final int columnIndex, final boolean value) {
		this.set(rowIndex, columnIndex, String.valueOf(value));
	}

	/**
	 * 
	 * @param lalel
	 * @param value
	 */
	public void set(final int rowIndex, final int columnIndex, final Calendar source) {
		if (source != null) {
			this.set(rowIndex, columnIndex, (new SimpleDateFormat("dd/MM/yyyy")).format(source));
		}
	}

	/**
	 * 
	 * @param lalel
	 * @param value
	 */
	public void set(final int rowIndex, final int columnIndex, final double value) {
		this.set(rowIndex, columnIndex, String.valueOf(value));
	}

	/**
	 * 
	 * @param lalel
	 * @param value
	 */
	public void set(final int rowIndex, final int columnIndex, final float value) {
		this.set(rowIndex, columnIndex, String.valueOf(value));
	}

	/**
	 * 
	 * @param lalel
	 * @param value
	 */
	public void set(final int rowIndex, final int columnIndex, final int value) {
		this.set(rowIndex, columnIndex, String.valueOf(value));
	}

	/**
	 * 
	 * @param lalel
	 * @param value
	 */
	public void set(final int rowIndex, final int columnIndex, final Object object) {
		if (object != null) {
			this.set(rowIndex, columnIndex, object.toString());
		}
	}

	/**
	 * 
	 * @param lalel
	 * @param value
	 */
	public void set(final int rowIndex, final int columnIndex, final String value) {
		if (value != null) {
			this.data[rowIndex][columnIndex] = value;
		}
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		String result;

		StringList buffer = new StringList();
		for (int rowIndex = 0; rowIndex < this.rowCount; rowIndex++) {
			for (int columnIndex = 0; columnIndex < this.columnCount; columnIndex++) {
				if (columnIndex != 0) {
					buffer.append("\t");
				}
				buffer.append(getString(rowIndex, columnIndex));
			}
		}

		result = buffer.toString();

		//
		return result;
	}

	public static TreeMap<Integer, Integer> columnSizeDistribution(final ReportTable tableWithSum) {
		TreeMap<Integer, Integer> result;

		result = new TreeMap<Integer, Integer>();

		for (int rowIndex = 1; rowIndex < tableWithSum.getRowCount() - 1; rowIndex++) {
			int size = Integer.parseInt(tableWithSum.getString(rowIndex, tableWithSum.getColumnCount() - 1));
			Integer number = result.get(size);
			if (number == null) {
				result.put(size, 1);
			} else {
				result.put(size, number + 1);
			}
		}
		//
		return result;
	}
	
	public static ReportTable normalize (final ReportTable source){
		ReportTable result;

		//
		result = new ReportTable(source.getRowCount(), source.getColumnCount());
		
		//
		result.setTitle(source.getTitle()+" (%)");

		for (int rowIndex = 1; rowIndex < source.getRowCount(); rowIndex++) {
			result.set(rowIndex, 0, source.get(rowIndex, 0));
		}
		
		//
		for (int columnIndex = 1; columnIndex < source.getColumnCount(); columnIndex++) {
			
			result.set(0, columnIndex, source.get(0, columnIndex));
			Double rowSum = new Double((String)source.get(source.getRowCount()-1, columnIndex));
			
			//
			for (int rowIndex = 1; rowIndex < source.getRowCount(); rowIndex++) {
				
				Double value = new Double((String)source.get(rowIndex, columnIndex));
				Double percentage = MathUtils.percent(value, rowSum);
				
				
				result.set(rowIndex, columnIndex, percentage);
			}
		}

		//
		return result;
		
	}

	/**
	 * 
	 * @return
	 */
	public static ReportTable transpose(final ReportTable source) {
		ReportTable result;

		//
		result = new ReportTable(source.getColumnCount(), source.getRowCount());

		//
		result.setTitle(source.getTitle());

		//
		for (int columnIndex = 0; columnIndex < source.getColumnCount(); columnIndex++) {
			//
			for (int rowIndex = 0; rowIndex < source.getRowCount(); rowIndex++) {
				result.set(columnIndex, rowIndex, source.get(rowIndex, columnIndex));
			}
		}

		//
		return result;
	}

}
