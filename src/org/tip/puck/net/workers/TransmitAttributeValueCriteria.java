package org.tip.puck.net.workers;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.FiliationType;

/**
 * 
 * @author TIP
 */
public class TransmitAttributeValueCriteria {

	private String attributeLabel;
	private FiliationType filiationType;
	private Integer maxGenerations;

	/**
	 * 
	 */
	public TransmitAttributeValueCriteria() {

		this.attributeLabel = null;
		this.filiationType = FiliationType.AGNATIC;
		this.maxGenerations = null;
	}

	public String getAttributeLabel() {
		return this.attributeLabel;
	}

	public FiliationType getFiliationType() {
		return this.filiationType;
	}

	public Integer getMaxGenerations() {
		return this.maxGenerations;
	}

	public void setAttributeLabel(final String attributeLabel) {
		this.attributeLabel = attributeLabel;
	}

	public void setFiliationType(final FiliationType filiationType) {
		this.filiationType = filiationType;
	}

	public void setMaxGenerations(final Integer maxGenerations) {
		this.maxGenerations = maxGenerations;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public static boolean isNotValid(final TransmitAttributeValueCriteria criteria) {
		boolean result;

		result = !isValid(criteria);

		//
		return result;
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public static boolean isValid(final TransmitAttributeValueCriteria criteria) {
		boolean result;

		if ((criteria == null) || (StringUtils.isBlank(criteria.getAttributeLabel())) || (criteria.getFiliationType() == null)) {
			//
			result = false;

		} else {
			//
			result = true;
		}

		//
		return result;
	}
}
