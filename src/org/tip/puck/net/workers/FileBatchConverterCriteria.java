package org.tip.puck.net.workers;

import java.io.File;

import org.apache.commons.io.Charsets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author cpm
 */
public class FileBatchConverterCriteria {

	public enum Mode {
		SKIP,
		OVERWRITE
	}

	public enum TargetFormat {
		BARODS(".bar.ods"),
		BARTXT(".bar.txt"),
		BARXLS(".bar.xls"),
		GEDCOM(".ged"),
		IURODS(".iur.ods"),
		IURTXT(".iur.txt"),
		IURXLS(".iur.xls"),
		IURTXTS(".iurs.txt"),
		PUC(".puc");

		private String extension;

		/**
		 * 
		 * @param extension
		 */
		private TargetFormat(final String extension) {
			this.extension = extension;
		}

		/**
		 * 
		 * @return
		 */
		public String getExtension() {
			return this.extension;
		}
	}

	private static final Logger logger = LoggerFactory.getLogger(FileBatchConverterCriteria.class);

	private File sourceDirectory;
	private File targetDirectory;
	private TargetFormat targetFormat;
	private Mode mode;
	private String charsetName;

	/**
	 * Instantiates a new file batch converter criteria.
	 */
	public FileBatchConverterCriteria() {
		this.sourceDirectory = null;
		this.targetDirectory = null;
		this.targetFormat = TargetFormat.PUC;
		this.mode = Mode.SKIP;
		this.charsetName = Charsets.UTF_8.name();
	}

	public String getCharsetName() {
		return this.charsetName;
	}

	public Mode getMode() {
		return this.mode;
	}

	public File getSourceDirectory() {
		return this.sourceDirectory;
	}

	public File getTargetDirectory() {
		return this.targetDirectory;
	}

	public TargetFormat getTargetFormat() {
		return this.targetFormat;
	}

	public void setCharsetName(final String charsetName) {
		this.charsetName = charsetName;
	}

	public void setMode(final Mode mode) {
		this.mode = mode;
	}

	public void setSourceDirectory(final File sourceDirectory) {
		this.sourceDirectory = sourceDirectory;
	}

	public void setTargetDirectory(final File targetDirectory) {
		this.targetDirectory = targetDirectory;
	}

	public void setTargetFormat(final TargetFormat targetFormat) {
		this.targetFormat = targetFormat;
	}
}
