package org.tip.puck.visualization.layouts.hierarchical.datastructs.partition;

import java.util.*;

/**
 * @date Jun 23, 2012
 *
 * @author Edoardo Savoia
 */
public class Partition<E> {

    final Map<E, PartitionItem<E>> partitionsMap;

    public Partition() {
	partitionsMap = new HashMap<E, PartitionItem<E>>();
    }

    public Partition(Collection<? extends E> elements){
	this();
	if (elements != null) {
	    for (E element : elements) {
		unsafeAdd(element);
	    }
	}
    }

    // no checks
    private void unsafeAdd(E element){
	PartitionItem<E> singletonPartition = PartitionItem.singleton(element);
	partitionsMap.put(element, singletonPartition);
    }

    // like a set
    // add true if set is changed
    public boolean add(E element){
	boolean result = false;
	if (!partitionsMap.containsKey(element)) {
	    PartitionItem<E> singletonPartition = PartitionItem.singleton(element);
	    partitionsMap.put(element, singletonPartition);
	    result = true;
	}
	return result;
    }

    // contains
    public boolean contains(E element){
	return partitionsMap.containsKey(element);
    }

    // getPartition
    public PartitionItem<E> getPartition(E element) {
	return partitionsMap.get(element);
    }

    /**
     * Get PartitionItem for element or add and return a new one (singleton) for it.
     * @param element
     * @param initialize
     * @return 
     * @NotNullSafe
     */
    public PartitionItem<E> getPartition(E element,boolean initialize) {
	if (initialize) {
	    add(element);
	}
	return partitionsMap.get(element);
    }

    public Set<PartitionItem<E>> getRoots(){
	Set<PartitionItem<E>> roots = new HashSet<PartitionItem<E>>();
	
	Collection<PartitionItem<E>> values = partitionsMap.values();
	Set<PartitionItem<E>> temp = new HashSet<PartitionItem<E>>(values);
	while (!temp.isEmpty()) {
	    PartitionItem<E> partitionItem = temp.iterator().next();
	    PartitionItem<E> pRoot = partitionItem.find();
	    roots.add(pRoot);
	    boolean canGo = true;
	    while (canGo && pRoot != null) {
		canGo = temp.remove(pRoot);
		pRoot = pRoot.getChild();
	    }
	    if(temp.contains(partitionItem) || canGo == false){
		System.out.println("Unexpected");
	    }
	}
	return roots;
    }

    public Set<PartitionItem<E>> getNonSingletonRoots(){
	Set<PartitionItem<E>> roots = new HashSet<PartitionItem<E>>();

	Collection<PartitionItem<E>> values = partitionsMap.values();
	Set<PartitionItem<E>> temp = new HashSet<PartitionItem<E>>(values);
	while (!temp.isEmpty()) {
	    PartitionItem<E> partitionItem = temp.iterator().next();
	    PartitionItem<E> pRoot = partitionItem.find();
	    if (!pRoot.isSingleton()) {
		roots.add(pRoot);
		boolean canGo = true;
		while (canGo && pRoot != null) {
		    canGo = temp.remove(pRoot);
		    pRoot = pRoot.getChild();
		}
		if(temp.contains(partitionItem) || canGo == false){
		    System.out.println("Unexpected");
		}
	    } else {
		temp.remove(pRoot);
	    }
	}
	return roots;
    }

    public PartitionItem<E> merge(PartitionItem<E> a, PartitionItem<E> b){
	if (areMerged(a, b)) {
	    return a.find();
	}
	return b.merge(a);
    }

    /**
     * Returns a copy of subset rooted at element.
     */
    public static<E> PartitionItem<E> copySubset(PartitionItem<E> element){
	if (element == null) {
	    return null;
	}


	PartitionItem<E> lastSubsetChild = getLastSubsetChild(element);

	if (lastSubsetChild == null) {
	    return null;
	}

	PartitionItem<E> newRoot = PartitionItem.singleton(element.value());
	PartitionItem<E> temp = element.getChild();

	boolean canGo = !lastSubsetChild.equals(element);
	// true if element is not a 0 rank node
	// if false returns the singleton partition newRoot
	while (canGo && temp != null) {
	    PartitionItem<E> subsetCopy = copySubset(temp);
	    if (subsetCopy != null) {
		newRoot.append(subsetCopy);
		temp = temp.getChild();
		canGo = !lastSubsetChild.equals(temp);
	    } else {
		return null;
	    }

	}

	return newRoot;
    }

    /**
     * Returns the last partition contained in subset rooted at element.
     * do not work correctly!
     * Rank is increased only if a partition with the same rank is added to it.
     * Addition of many smaller partitions (with rank less than root's rank) do not change root's rank
     */
    public static<E> PartitionItem<E> getLastSubsetChild(PartitionItem<E> element){
	if (element == null) {
	    return null;
	}
	int rank = element.getRank();

	if (rank == 0) {
	    return element;
	}
	if (rank == 1) {
	    if (element.getChild() != null) {
		return element.getChild().getRank() == 0 ? element.getChild() : null;
	    }
	    return element.getChild();
	}

	// rank > = 2
	PartitionItem<E> temp = element.getChild();
	while (temp != null && rank > 0) {
	    PartitionItem<E> last = getLastSubsetChild(temp);
	    if (last != null) {
		rank--;
		temp = rank == 0 ? last : last.getChild();
	    }else{
		System.out.println("Unexpected");
		temp = null;
	    }
	}

	return rank == 0 ? temp: null;
    }

    private List<PartitionItem<E>> children(final E element){
	PartitionItem<E> partition = partitionsMap.get(element);
	if (partition == null) {
	    return null;
	}
	List<PartitionItem<E>> result = new ArrayList<PartitionItem<E>>();
	Collection<PartitionItem<E>> allPartitions = partitionsMap.values();
	for (PartitionItem<E> partitionItem : allPartitions) {
	    if (partitionItem.getParent().equals(partition)) {
		result.add(partitionItem);
	    }
	}
	return result;
    }

    public Collection<E> toList() {
	List<E> list = new ArrayList<E>();
	Set<PartitionItem<E>> roots = getRoots();
	for (PartitionItem<E> rootPartition : roots) {
	    List<E> values = rootPartition.values();
	    if (values != null && !values.isEmpty()) {
		System.out.println("values size: "+values.size());
		list.addAll(values);
	    }
	}
	System.out.println("list size: "+list.size());
	return list;
    }

    private static <E> List<E> values(PartitionItem<E> partition) {
	PartitionItem<E> rep = partition.find();
	PartitionItem<E> tmp = rep.getChild();
	List<E> retSet = new LinkedList<E>();
	while (tmp != null) {
	    retSet.add(tmp.value());
	    tmp = tmp.getChild();
	}
	return retSet;
    }

    /**
     * Checks whether the two elements have been merged.
     *
     * <p>Equivalent to {@code partition1.find() == partition2.find()}.
     *
     * @param partition1 the first element
     * @param partition2 the second element
     * @return whether the two partitions have been merged
     */
    public static boolean areMerged(PartitionItem<?> partition1, PartitionItem<?> partition2) {
	return partition1.find() == partition2.find();
    }

    public static void main(String... args) {
	/*
	Group group = GroupOperations.createGroup(null,null);
	Group group2 = GroupOperations.createGroup(null,null);
	GroupOperations.getFirst(group);
	*/



	Partition<Integer> part = new Partition<Integer>();
	
	part.add(1);
	part.add(2);
	part.add(3);
	part.add(4);

	part.add(5);
	part.add(6);

	part.add(7);
	part.add(8);

	PartitionItem<Integer> first = part.getPartition(2).merge(part.getPartition(1));
	System.out.println("{1} U {2}");
	print(first);

	System.out.println("{3} U {4}");
	print(part.getPartition(4).merge(part.getPartition(3)));

	first = part.merge(first,part.getPartition(3));

	System.out.println("{1,2} U {3,4}");
	print(first);



	// PartitionItem<Integer> first = ((part.getPartition(1).merge(part.getPartition(2))).merge(part.getPartition(3))).merge(part.getPartition(4));
	
	 PartitionItem<Integer> second = part.getPartition(6).merge(part.getPartition(5));
//	System.out.println("{5} U {6}");
//	print(second);

	// PartitionItem<Integer> third = part.getPartition(8).merge(part.getPartition(7));
//	System.out.println("{7} U {8}");
//	print(third);


	// System.out.println("{1,2,3,4,5} U {6}");
	// print((part.getPartition(6)).merge(first));

	System.out.println("{1,2,3,4} U {7}");
	print((part.getPartition(7)).merge(first));

	System.out.println("{1,2,3,4,7} U {8}");
	print((part.getPartition(8)).merge(first));

	System.out.println("{1,2,3,4,7,8} U {5,6}");
	print((part.getPartition(5)).merge(first));

	// PartitionItem<Integer> firstAndSec = first.merge(second);
	
	// PartitionItem<Integer> thirdAndSec = third.merge(second);
	// System.out.println("{5,6} U {7,8}");
	// print(thirdAndSec);

	// PartitionItem<Integer> firstAndThird = thirdAndSec.merge(first);
	// PartitionItem<Integer> last = part.getLastSubsetChild(firstAndThird);
	// System.out.println("last firstAndThird: " + last.value());
	// print(firstAndThird);


	// second.find() == third.find() == secAndThird.find();
    }

    public static void print(PartitionItem<Integer> item){
	PartitionItem<Integer> root = item;
	while(root != null){
	    PartitionItem<Integer> last = getLastSubsetChild(root);
	    System.out.println("root: "+ root.value() + " ("+root.getParent().value()+") last: " + last.value()+" rank: " + root.getRank());
	    root = root.getChild();
	}
    }

}

