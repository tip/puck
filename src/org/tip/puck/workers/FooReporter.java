package org.tip.puck.workers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.net.Net;
import org.tip.puck.report.Report;
import org.tip.puck.report.ReportAttributes;
import org.tip.puck.report.ReportChart;
import org.tip.puck.report.ReportChart.GraphType;
import org.tip.puck.report.ReportChart.LogarithmType;
import org.tip.puck.report.ReportTable;
import org.tip.puck.util.Chronometer;

/**
 * The FooWorker is example class for report generation.
 * 
 * @author TIP
 */
public class FooReporter {
	private static final Logger logger = LoggerFactory.getLogger(FooReporter.class);

	/**
	 * Generates a report.
	 * 
	 * @param net
	 *            Source to report.
	 * @throws PuckException
	 */
	public static Report work(final Net source, final String fooParameter1, final String fooParameter2) throws PuckException {
		Report result;

		if (source == null) {
			//
			throw PuckExceptions.INVALID_PARAMETER.create("Foo error.");

		} else {
			//
			result = new Report();

			Chronometer chrono = new Chronometer();
			result.setTitle("FOO REPORT");
			result.setOrigin("Foo worker");
			result.setTarget(source.getLabel());
			result.setInputComment("It is a foo step for user.");
			result.inputs().add("Foo1", fooParameter1);
			result.inputs().add("Foo2", fooParameter2);

			//
			ReportAttributes attributes = new ReportAttributes();
			attributes.add("Foo42", "123456789");
			attributes.add("FooFoo", "Foooooooooo");
			result.outputs().append(attributes);

			//
			result.outputs().appendln("If results seem to be foo, don't panic, nobody's perfect.");

			ReportTable table = new ReportTable(2, 8);
			table.set(0, 0, "Women");
			table.set(0, 1, "36");
			table.set(0, 2, "57");
			table.set(0, 3, "89");

			table.set(1, 0, "Men");
			table.set(1, 1, "44");
			table.set(1, 2, "55");
			table.set(1, 3, "66");

			result.outputs().append(table);
			result.outputs().appendln();

			//
			{
				ReportChart reportChart = new ReportChart("SEX", GraphType.STACKED_BARS);

				reportChart.setHeader("Female", 0);
				reportChart.addValue(1234, 0);

				reportChart.setHeader("Male", 1);
				reportChart.addValue(1444, 0);

				result.outputs().append(reportChart);
				result.outputs().appendln();
			}

			//
			{
				ReportChart reportChart = new ReportChart("TEST1: BAR CHARTS ON SAME LINE", GraphType.STACKED_BARS);

				reportChart.setHeader("AAA", 0);
				reportChart.addValue(50, 0);
				reportChart.addValue(50, 1);

				reportChart.setHeader("BBB", 1);
				reportChart.addValue(40, 0);
				reportChart.addValue(30, 1);

				reportChart.setHeader("FFF", 2);
				reportChart.addValue(30, 0);
				reportChart.addValue(20, 1);

				reportChart.setHeader("RRR", 3);
				reportChart.addValue(20, 0);
				reportChart.addValue(10, 1);

				result.outputs().append(reportChart);
			}

			//
			{
				// Test without set headers.
				ReportChart reportChart = new ReportChart("TEST2: STACKED BARS", GraphType.STACKED_BARS);

				reportChart.setLineTitle("Female", 0);
				reportChart.addValue(50, 0);
				reportChart.addValue(40, 0);
				reportChart.addValue(30, 0);
				reportChart.addValue(20, 0);

				reportChart.setLineTitle("Male", 1);
				reportChart.addValue(50, 1);
				reportChart.addValue(30, 1);
				reportChart.addValue(20, 1);
				reportChart.addValue(10, 1);

				result.outputs().append(reportChart);
				result.outputs().appendln();
			}

			result.outputs().appendln();

			//
			{
				// Test without set headers.
				ReportChart reportChart = new ReportChart("TEST3: LINE", GraphType.LINES);

				reportChart.setHeadersLegend("Generational distance");
				reportChart.setLinesLegend("% individuals");

				reportChart.setLineTitle("Female", 0);
				reportChart.addValue(50, 0);
				reportChart.addValue(40, 0);
				reportChart.addValue(30, 0);
				reportChart.addValue(20, 0);

				reportChart.setLineTitle("Male", 1);
				reportChart.addValue(50, 1);
				reportChart.addValue(30, 1);
				reportChart.addValue(20, 1);
				reportChart.addValue(10, 1);

				result.outputs().append(reportChart);
			}

			//
			{
				// Test without set headers.
				ReportChart reportChart = new ReportChart("TEST4: LINE & XYDATA", GraphType.LINES);

				reportChart.setHeadersLegend("Generational distance");
				reportChart.setLinesLegend("% individuals");

				reportChart.setLineTitle("Female", 0);
				reportChart.addValue(3, 5, 0);
				reportChart.addValue(6, 15, 0);
				reportChart.addValue(9, 10, 0);
				reportChart.addValue(11, 20, 0);

				reportChart.setLineTitle("Male", 1);
				reportChart.addValue(2, 10, 1);
				reportChart.addValue(4, 5, 1);
				reportChart.addValue(6, 5, 1);
				reportChart.addValue(8, 10, 1);
				reportChart.addValue(10, 10, 1);

				result.outputs().append(reportChart);
				result.outputs().appendln();
			}

			//
			{
				// Test without set headers.
				ReportChart reportChart = new ReportChart("TEST5: SCATTER", GraphType.SCATTER);

				reportChart.setHeadersLegend("Mean Level");
				reportChart.setLinesLegend("Size");

				reportChart.setLineTitle("Female", 0);
				reportChart.addValue(1, 5, 0);
				reportChart.addValue(3, 15, 0);
				reportChart.addValue(6, 10, 0);
				reportChart.addValue(9, 20, 0);

				reportChart.setLineTitle("Male", 1);
				reportChart.addValue(2, 10, 1);
				reportChart.addValue(4, 5, 1);
				reportChart.addValue(6, 5, 1);
				reportChart.addValue(8, 10, 1);
				reportChart.addValue(10, 10, 1);

				result.outputs().append(reportChart);
				result.outputs().appendln();
			}

			//
			{
				// Test without set headers.
				ReportChart reportChart = new ReportChart("TEST 6: LOG HORIZONTAL", GraphType.LINES);
				reportChart.setLogarithmType(LogarithmType.HORIZONTAL);

				reportChart.setHeadersLegend("Generational distance");
				reportChart.setLinesLegend("% individuals");

				reportChart.setLineTitle("Female", 0);
				reportChart.addValue(3, 5, 0);
				reportChart.addValue(6, 15, 0);
				reportChart.addValue(9, 10, 0);
				reportChart.addValue(11, 20, 0);

				reportChart.setLineTitle("Male", 1);
				reportChart.addValue(2, 10, 1);
				reportChart.addValue(4, 5, 1);
				reportChart.addValue(6, 5, 1);
				reportChart.addValue(8, 10, 1);
				reportChart.addValue(10, 10, 1);

				result.outputs().append(reportChart);
			}

			//
			{
				// Test without set headers.
				ReportChart reportChart = new ReportChart("TEST 7: LOG VERTICAL", GraphType.LINES);
				reportChart.setLogarithmType(LogarithmType.VERTICAL);

				reportChart.setHeadersLegend("Generational distance");
				reportChart.setLinesLegend("% individuals");

				reportChart.setLineTitle("Female", 0);
				reportChart.addValue(3, 5, 0);
				reportChart.addValue(6, 15, 0);
				reportChart.addValue(9, 10, 0);
				reportChart.addValue(11, 20, 0);

				reportChart.setLineTitle("Male", 1);
				reportChart.addValue(2, 10, 1);
				reportChart.addValue(4, 5, 1);
				reportChart.addValue(6, 5, 1);
				reportChart.addValue(8, 10, 1);
				reportChart.addValue(10, 10, 1);

				result.outputs().append(reportChart);
			}

			//
			result.setTimeSpent(chrono.stop().interval());
		}

		//
		return result;
	}
}
