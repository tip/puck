package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.census.workers.CensusReporter;
import org.tip.puck.net.IndividualComparator.Sorting;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.report.Report;
import org.tip.puckgui.NetGUI;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class ListMissingRelativesByClusterInputWindow extends JFrame {

	private static final long serialVersionUID = -3911848572558551916L;
	private static final Logger logger = LoggerFactory.getLogger(ListMissingRelativesByClusterInputWindow.class);

	private JFrame thisJFrame;
	private NetGUI netGUI;
	private JPanel contentPane;
	private static String lastParameter;
	private static Sorting lastSorting = Sorting.ID;
	private JComboBox comboBoxParameter;

	/**
	 * Create the frame.
	 */
	public ListMissingRelativesByClusterInputWindow(final NetGUI netGUI) {
		//
		List<String> availableLabels = IndividualValuator.getAttributeLabelSample(netGUI.getNet().individuals());

		//
		if (lastParameter == null) {
			availableLabels.add(0, "");
		} else {
			availableLabels.add(0, lastParameter);
		}

		// /////////////////////////////////
		setIconImage(Toolkit.getDefaultToolkit().getImage(ListMissingRelativesByClusterInputWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));

		this.thisJFrame = this;
		this.netGUI = netGUI;
		setTitle("List Relatives by Cluster Inputs");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 365, 200);
		setLocationRelativeTo(null);

		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		this.contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"), FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel label = new JLabel("Label:");
		panel.add(label, "2, 4, right, default");

		this.comboBoxParameter = new JComboBox(availableLabels.toArray());
		this.comboBoxParameter.setSelectedIndex(0);
		this.comboBoxParameter.setMaximumRowCount(12);
		this.comboBoxParameter.setEditable(true);
		panel.add(this.comboBoxParameter, "4, 4, fill, default");

		JPanel buttonPanel = new JPanel();
		this.contentPane.add(buttonPanel, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Close.
				dispose();
			}
		});
		buttonPanel.add(btnCancel);

		JButton btnLaunch = new JButton("Launch");
		getRootPane().setDefaultButton(btnLaunch);
		btnLaunch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Launch.
				try {
					//
					String parameter = (String) ListMissingRelativesByClusterInputWindow.this.comboBoxParameter.getSelectedItem();

					//
					lastParameter = parameter;

					//
					if (parameter == null) {
						//
						String title = "Bad input";
						String message = "Please, enter none empty input.";

						//
						JOptionPane.showMessageDialog(ListMissingRelativesByClusterInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
					} else {
						Report report = CensusReporter.reportMissingRelativesList(netGUI.getSegmentation(), parameter);
						netGUI.addReportTab(report);
						dispose();
					}
				} catch (final Exception exception) {
					// Show trace.
					exception.printStackTrace();

					//
					String title = "Error computerum est";
					String message = "Error occured during working: " + exception.getMessage();

					//
					JOptionPane.showMessageDialog(ListMissingRelativesByClusterInputWindow.this.thisJFrame, message, title, JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonPanel.add(btnLaunch);
	}

}
