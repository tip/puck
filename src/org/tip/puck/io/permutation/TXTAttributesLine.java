package org.tip.puck.io.permutation;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a line of attributes from a TXT file.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class TXTAttributesLine {
	public static final int NOID = 0;

	private int id;
	private List<String> values;

	/**
	 * 
	 */
	public TXTAttributesLine() {
		this.id = NOID;
		this.values = new ArrayList<String>();
	}

	public int id() {
		return this.id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public List<String> values() {
		return this.values;
	}

}
