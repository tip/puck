package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

public class OpenEncodingWindow extends JDialog {

	private static final long serialVersionUID = 7303787713722592612L;
	private static final Logger logger = LoggerFactory.getLogger(OpenEncodingWindow.class);
	private final JScrollPane scrollPane = new JScrollPane();
	private JTable table;
	private static String[][] charsets = charsets();
	private String inputedCriteria;

	/**
	 * Create the dialog.
	 */
	public OpenEncodingWindow() {
		super();
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(OpenEncodingWindow.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		setTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("AboutPopup.this.title"));
		setModal(true);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 475, 500);
		setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout(0, 0));

		JPanel panelContent = new JPanel();
		getContentPane().add(panelContent, BorderLayout.CENTER);
		panelContent.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, },
				new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));
		JTextPane txtpnByDefaultPuck = new JTextPane();
		txtpnByDefaultPuck.setBackground(new Color(238, 238, 238));
		panelContent.add(txtpnByDefaultPuck, "4, 2");
		txtpnByDefaultPuck.setEditable(false);
		txtpnByDefaultPuck.setText(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("OpenEncodingWindow.txtpnByDefaultPuck.text")); //$NON-NLS-1$ //$NON-NLS-2$

		this.table = new JTable();
		this.table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.table.setModel(new DefaultTableModel(charsets, new String[] { "Encoding Code", "Description" }) {
			private static final long serialVersionUID = -2980267568221549783L;
			Class[] columnTypes = new Class[] { String.class, String.class };

			@Override
			public Class getColumnClass(final int columnIndex) {
				return this.columnTypes[columnIndex];
			}
		});
		this.scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		this.scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		panelContent.add(this.scrollPane, "4, 5");
		this.scrollPane.setViewportView(this.table);

		// //////////////////////

		JPanel panel_1 = new JPanel();
		getContentPane().add(panel_1, BorderLayout.SOUTH);

		JButton btnCancel = new JButton(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("OpenEncodingWindow.btnCancel.text")); //$NON-NLS-1$ //$NON-NLS-2$
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				// Cancel.
				dispose();
			}
		});
		btnCancel.setActionCommand("OK");
		panel_1.add(btnCancel);
		JButton button = new JButton("OK");
		panel_1.add(button);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				System.out.println("OK pressed.");

				int selectedIndex = OpenEncodingWindow.this.table.getSelectedRow();
				if (selectedIndex == -1) {
					JOptionPane.showMessageDialog(null, "Select one line in table.", "Bad selection", JOptionPane.ERROR_MESSAGE);
				} else {
					//
					OpenEncodingWindow.this.inputedCriteria = charsets[selectedIndex][0];

					//
					setVisible(false);
				}

			}
		});
		button.setActionCommand("OK");
	}

	/**
	 * 
	 * @return
	 */
	public String getCriteria() {
		String result;

		result = this.inputedCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public static String[][] charsets() {
		String[][] result;

		result = new String[][] {

		{ "8859_1", "ISO Latin-1" },

		{ "8859_2", "ISO Latin-2" },

		{ "8859_3", "ISO Latin-3" },

		{ "8859_4", "ISO Latin-4" },

		{ "8859_5", "ISO Latin/Cyrillic" },

		{ "8859_6", "ISO Latin/Arabic" },

		{ "8859_7", "ISO Latin/Greek" },

		{ "8859_8", "ISO Latin/Hebrew" },

		{ "8859_9", "ISO Latin-5" },

		{ "8859_15", "ISO Latin-9" },

		{ "Big5", "Big 5 Traditional Chinese" },

		{ "CNS11643", "CNS 11643 Traditional Chinese" },

		{ "Cp1250", "Windows Eastern Europe / Latin-2" },

		{ "Cp1251", "Windows Cyrillic" },

		{ "Cp1252", "Windows Western Europe / Latin-1" },

		{ "Cp1253", "Windows Greek" },

		{ "Cp1254", "Windows Turkish" },

		{ "Cp1255", "Windows Hebrew" },

		{ "Cp1256", "Windows Arabic" },

		{ "Cp1257", "Windows Baltic" },

		{ "Cp1258", "Windows Vietnamese" },

		{ "Cp437", "PC Original" },

		{ "Cp737", "PC Greek" },

		{ "Cp775", "PC Baltic" },

		{ "Cp850", "PC Latin-1" },

		{ "Cp852", "PC Latin-2" },

		{ "Cp855", "PC Cyrillic" },

		{ "Cp857", "PC Turkish" },

		{ "Cp860", "PC Portuguese" },

		{ "Cp861", "PC Icelandic" },

		{ "Cp862", "PC Hebrew" },

		{ "Cp863", "PC Canadian French" },

		{ "Cp864", "PC Arabic" },

		{ "Cp865", "PC Nordic" },

		{ "Cp866", "PC Russian" },

		{ "Cp869", "PC Modern Greek" },

		{ "Cp874", "Windows Thai" },

		{ "EUCJIS", "Japanese EUC" },

		{ "GB2312", "GB2312-80 Simplified Chinese" },

		{ "JIS", "JIS" },

		{ "KSC5601", "KSC5601 Korean" },

		{ "MacArabic", "Macintosh Arabic" },

		{ "MacCentralEurope", "Macintosh Latin-2" },

		{ "MacCroatian", "Macintosh Croatian" },

		{ "MacCyrillic", "Macintosh Cyrillic" },

		{ "MacDingbat", "Macintosh Dingbat" },

		{ "MacGreek", "Macintosh Greek" },

		{ "MacHebrew", "Macintosh Hebrew" },

		{ "MacIceland", "Macintosh Iceland" },

		{ "MacRoman", "Macintosh Roman" },

		{ "MacRomania", "Macintosh Romania" },

		{ "MacSymbol", "Macintosh Symbol" },

		{ "MacThai", "Macintosh Thai" },

		{ "MacTurkish", "Macintosh Turkish" },

		{ "MacUkraine", "Macintosh Ukraine" },

		{ "SJIS", "PC and Windows Japanese" },

		{ "UTF8", "Standard UTF-8" }, };

		//
		return result;
	}

	/**
	 * Launch the application.
	 */
	public static String showDialog() {
		String result;

		//
		OpenEncodingWindow dialog = new OpenEncodingWindow();
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getCriteria();

		//
		return result;
	}

}
