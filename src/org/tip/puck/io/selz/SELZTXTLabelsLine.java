package org.tip.puck.io.selz;

import java.util.ArrayList;

/**
 * This class represents a line of labels from a TXT file.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class SELZTXTLabelsLine extends ArrayList<String> {

	private static final long serialVersionUID = 1569025250348474017L;

	/**
	 * 
	 */
	public SELZTXTLabelsLine() {
		super();
	}
}
