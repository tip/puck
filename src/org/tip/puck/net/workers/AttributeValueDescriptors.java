package org.tip.puck.net.workers;

import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.util.StringList;
import fr.devinsy.util.StringSet;

/**
 * 
 * 
 * @author TIP
 */
public class AttributeValueDescriptors extends HashMap<String, AttributeValueDescriptor> implements Iterable<AttributeValueDescriptor> {

	private static final long serialVersionUID = 8880871594768360775L;

	/**
	 * 
	 * @param scope
	 * @param label
	 */
	public AttributeValueDescriptors() {
		super();
	}

	/**
	 * 
	 * @param initialCapacity
	 */
	public AttributeValueDescriptors(final int initialCapacity) {
		super(initialCapacity);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public long getCountOf(final String value) {
		long result;

		if (value == null) {

			result = 0;
		} else {

			AttributeValueDescriptor target = get(value);

			if (target == null) {

				result = 0;
			} else {

				result = target.getCount();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public long getCountOf(final String... values) {
		long result;

		result = getCountOf(new StringSet(values));

		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public long getCountOf(final StringList value) {
		long result;

		if (value == null) {
			result = 0;

		} else {

			result = getCountOf(new StringSet(value));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public long getCountOf(final StringSet values) {
		long result;

		if (values == null) {

			result = 0;
		} else {

			result = 0;
			for (String value : values) {
				AttributeValueDescriptor target = get(value);

				if (target != null) {

					result += target.getCount();
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public StringList getKeyList() {
		StringList result;

		result = new StringList();

		for (String key : getKeys()) {

			result.add(key);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public StringSet getKeys() {
		StringSet result;

		result = new StringSet();

		for (String key : this.keySet()) {

			result.add(key);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public StringList getValueList() {
		StringList result;

		result = new StringList();

		for (String key : this.keySet()) {
			//
			result.append(key);
		}

		//
		return result;
	}

	@Override
	public Iterator<AttributeValueDescriptor> iterator() {
		Iterator<AttributeValueDescriptor> result;

		result = this.values().iterator();

		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void put(final AttributeValueDescriptor source) {

		if (source != null) {

			AttributeValueDescriptor target = get(source.getValue());

			if (target == null) {

				put(source.getValue(), source);

			} else {

				target.inc(source.getCount());
			}
		}
	}

	/**
	 * 
	 * @param value
	 */
	public void put(final String value) {

		if (StringUtils.isNotBlank(value)) {

			AttributeValueDescriptor descriptor = get(value);

			if (descriptor == null) {

				descriptor = new AttributeValueDescriptor(value);
				put(value, descriptor);
			}

			descriptor.inc();
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void putAll(final AttributeValueDescriptors source) {

		for (AttributeValueDescriptor descriptor : source) {

			put(descriptor);
		}
	}

	/**
	 * 
	 * @return
	 */
	public AttributeValueDescriptorList toList() {
		AttributeValueDescriptorList result;

		result = new AttributeValueDescriptorList(this);

		//
		return result;
	}
}
