package org.tip.puck.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author TIP
 */
public class Values extends ArrayList<Value> {

	private static final long serialVersionUID = 562986261984871649L;

	/**
	 *
	 */
	public Values() {
		super();
	}
	
	public <E> Values(List<E> list){
		super();
		for (E item : list){
			if (item == null){
				add(null);
			} else {
				add(new Value(item));
			}
		}
	}

	/**
	 *
	 */
	public Values(final int initialCapacity) {
		super(initialCapacity);
	}

	/**
	 * 
	 * @return
	 */
	public double average() {
		double result;

		//
		double sum = 0;
		int count = 0;
		for (Value value : this) {
			if ((value != null) && (value.isNumber())) {
				sum += value.intValue();
				count += 1;
			}
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum * 1.0 / count;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double averagePositives() {
		double result;

		//
		double sum = 0;
		int count = 0;
		for (Value value : this) {
			if ((value != null) && (value.isNumber())) {
				if (value.doubleValue() > 0) {
					sum += value.doubleValue();
					count += 1;
				}
			}
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum * 1.0 / count;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNumbers() {
		boolean result;

		boolean ended = false;
		result = false;
		Iterator<Value> iterator = this.iterator();
		while (!ended) {
			if (iterator.hasNext()) {
				Value value = iterator.next();
				if ((value == null) || (value.isNotNumber())) {
					result = false;
					ended = true;
				}
			} else {
				result = true;
				ended = true;
			}
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isNumeric() {
		boolean result;

		boolean ended = false;
		result = false;
		Iterator<Value> iterator = this.iterator();
		
		if (iterator.hasNext()){
			while (!ended) {
				if (iterator.hasNext()) {
					Value value = iterator.next();
					if ((value == null) || (value.isNotNumber())) {
						ended = true;
						result = false;
					}
				} else {
					ended = true;
					result = true;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double max() {
		double result;

		Double maxValue = null;
		for (Value value : this) {
			if ((value != null) && (value.isNumber())) {
				if (maxValue == null) {
					maxValue = value.doubleValue();
				} else if (value.doubleValue() > maxValue) {
					maxValue = value.doubleValue();
				}
			}
		}

		if (maxValue == null) {
			result = 0;
		} else {
			result = maxValue;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double min() {
		double result;

		Double minValue = null;
		for (Value value : this) {
			if ((value != null) && (value.isNumber())) {
				if (minValue == null) {
					minValue = value.doubleValue();
				} else if (value.doubleValue() < minValue) {
					minValue = value.doubleValue();
				}
			}
		}

		if (minValue == null) {
			result = 0;
		} else {
			result = minValue;
		}

		//
		return result;
	}
	
	public boolean isNull(){
		boolean result;
		
		result = true;
		for (Value value : this){
			if (value != null){
				result = false;
				break;
			}
		}
		//
		return result;
	}
	
	public void nullToZero (){
		
		for (int i=0;i<size();i++){
			if (get(i) == null){
				set(i,new Value(0));
			}
		}
	}
}
