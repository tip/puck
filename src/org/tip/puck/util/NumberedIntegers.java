package org.tip.puck.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author TIP
 */
public class NumberedIntegers extends HashMap<Integer, Integer> implements Iterable<Integer> {

	private static final long serialVersionUID = -7834882304366892614L;

	/**
	 *
	 */
	public NumberedIntegers() {
		super();
	}

	/**
	 *
	 */
	public NumberedIntegers(final int initialCapacity) {
		super(initialCapacity);
	}

	/**
	 * 
	 * @return
	 */
	public double average() {
		double result;

		//
		int sum = 0;
		int count = 0;
		for (int value : this) {
			sum += value;
			count += 1;
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum * 1.0 / count;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double averagePositives() {
		double result;

		int sum = 0;
		int count = 0;
		for (int value : this) {
			if (value != 0) {
				sum += value;
				count += 1;
			}
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum * 1.0 / count;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Integer> iterator() {
		Iterator<Integer> result;

		result = super.values().iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int max() {
		int result;

		if (this.size() == 0) {
			result = 0;
		} else {
			result = Integer.MIN_VALUE;
			for (int value : this) {
				if (value > result) {
					result = value;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int min() {
		int result;

		if (this.size() == 0) {
			result = 0;
		} else {
			result = Integer.MAX_VALUE;
			for (int value : this) {
				if (value < result) {
					result = value;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Integer> toList() {
		List<Integer> result;

		result = new ArrayList<Integer>(values());

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Integer> toSortedList() {
		List<Integer> result;

		result = toList();
		Collections.sort(result);

		//
		return result;
	}

}
