package org.tip.puckgui.views.geo;

import java.io.File;
import java.util.ArrayList;

import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import org.geotools.feature.DefaultFeatureCollection;
import org.tip.puck.PuckException;
import org.tip.puck.geo.GeoLevel;
import org.tip.puck.geo.Place;
import org.tip.puck.geo.Place;
import org.tip.puck.geo.io.GeoNamesWebService;
import org.tip.puck.geo.tools.GeotoolsUtils;
import org.tip.puck.graphs.Graph;
import org.tip.puck.io.csv.CSVFile;
import org.tip.puckgui.GroupNetGUI;
import org.tip.puckgui.NetGUI;

public class Geocode extends SwingWorker<ArrayList<DefaultFeatureCollection>, String>{

	ArrayList<DefaultFeatureCollection> collectionfeaturesReadyToBeDisplayed;
	ConfigGeocodingDialog mainDialog;
	JProgressBar progressBar;
	String typeOfGeocodingSource;
	GroupNetGUI parentGUI = null;
	NetGUI netGUI = null;
	File csvFile= null;
	Graph<?> graphFromFlowReport = null;
	Graph<?> genericGraphPlaces = null;

	public Geocode(ConfigGeocodingDialog mainDialog, JProgressBar progressBar, String typeOfGeocodingSource) {
		this.mainDialog = mainDialog;
		this.progressBar = progressBar;
		this.typeOfGeocodingSource = typeOfGeocodingSource;
	}

	public Geocode(ConfigGeocodingDialog mainDialog, JProgressBar progressBar, String typeOfGeocodingSource, NetGUI netGUI, Graph<?> graph) {
		this(mainDialog, progressBar, typeOfGeocodingSource);
		this.netGUI = netGUI;
		this.graphFromFlowReport = graph;
	}

	public Geocode(ConfigGeocodingDialog mainDialog, JProgressBar progressBar, String typeOfGeocodingSource, GroupNetGUI parentGUI) {
		this(mainDialog, progressBar, typeOfGeocodingSource);
		this.parentGUI = parentGUI;
	}
	
	public Geocode(ConfigGeocodingDialog mainDialog, JProgressBar progressBar, String typeOfGeocodingSource, NetGUI netGUI, File csvFile, Graph<?> graph) {
		this(mainDialog, progressBar, typeOfGeocodingSource);
		this.netGUI = netGUI;;
		this.csvFile = csvFile;
		this.genericGraphPlaces = graph;
	}

	@Override
	protected ArrayList<DefaultFeatureCollection> doInBackground() throws Exception {
		//Choice geoname or file,etc.
		if(typeOfGeocodingSource.compareTo("geonames") == 0 ) {
			//Creating GeoNames WebService
			GeoNamesWebService ws;
			ws = new GeoNamesWebService(this.progressBar);

			if( this.parentGUI != null )
				collectionfeaturesReadyToBeDisplayed = ws.getCoordForNodesPlaces(this.parentGUI.getGroupNet());
			if( this.graphFromFlowReport != null )
				collectionfeaturesReadyToBeDisplayed = ws.getCoordForNodesPlaces(this.graphFromFlowReport);

		}
		if(typeOfGeocodingSource.compareTo("personnalSources") == 0 ) {
			//First open csv file
	        try {
	        	Graph<Place> geoPlaces = CSVFile.load(csvFile, this.genericGraphPlaces, 1);
	        	GeotoolsUtils gtu = new GeotoolsUtils();
	        	collectionfeaturesReadyToBeDisplayed = gtu.getFeaturesForNodesPlaces(geoPlaces, this.graphFromFlowReport);
			} catch (PuckException e) {
				e.printStackTrace();
			}
		}
		if(typeOfGeocodingSource.compareTo("geonamesFile") == 0 ) {
			
			//First open csv file
	        try {
	        	Graph<Place> geoPlaces = CSVFile.load(csvFile, this.genericGraphPlaces, 0);
				GeotoolsUtils gtu = new GeotoolsUtils();
	        	collectionfeaturesReadyToBeDisplayed = gtu.getFeaturesForNodesPlaces(geoPlaces, this.graphFromFlowReport);
			} catch (PuckException e) {
				e.printStackTrace();
			}
	        
	        //Then geocode toponyms with coordinate/names in csv
		}

		if(typeOfGeocodingSource.compareTo("integrated") == 0 ) {
			
        	Graph<Place> geoPlaces = netGUI.getNet().getGeography().graph(GeoLevel.TOWN);
			GeotoolsUtils gtu = new GeotoolsUtils();
        	collectionfeaturesReadyToBeDisplayed = gtu.getFeaturesForNodesPlaces(geoPlaces, this.graphFromFlowReport);
		}

		return collectionfeaturesReadyToBeDisplayed;
	}

	@Override
	protected void done() {
		try {

			mainDialog.dispose();

			MapPanel_old geographyMap = new MapPanel_old();
			geographyMap.diplayCollectionsPointsAndLines(collectionfeaturesReadyToBeDisplayed);

			if( parentGUI != null )
				parentGUI.addTab("Geography", geographyMap);
			if( netGUI != null )
				netGUI.addTab("Geography", geographyMap);

//			System.out.println("done");
			//can call other gui update code here
		} catch (Throwable t) {
			//do something with the exception
		}
	}
}