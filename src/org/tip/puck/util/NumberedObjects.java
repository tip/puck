package org.tip.puck.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author TIP
 */
public class NumberedObjects extends HashMap<Integer, Object> implements Iterable<Object> {

	private static final long serialVersionUID = -7078856429536208678L;

	/**
	 *
	 */
	public NumberedObjects() {
		super();
	}

	/**
	 *
	 */
	public NumberedObjects(final int initialCapacity) {
		super(initialCapacity);
	}

	/**
	 * 
	 * @param number
	 * @param value
	 */
	public void addValue(final int number, final double value) {
		this.put(number, new Double(value));
	}

	/**
	 * 
	 * @param number
	 * @param value
	 */
	public void addValue(final int number, final int value) {
		this.put(number, new Integer(value));
	}

	/**
	 * 
	 * @param number
	 * @param value
	 */
	public void addValue(final int number, final Object value) {
		if (value == null) {
			this.put(number, null);
		} else {
			this.put(number, value);
		}
	}

	/**
	 * 
	 * @return
	 */
	public double average() {
		double result;

		//
		double sum = 0;
		int count = 0;
		for (Object value : this) {
			if ((value != null) && (isNumber(value))) {
				sum += doubleValue(value);
				count += 1;
			}
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum * 1.0 / count;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double averagePositives() {
		double result;

		//
		double sum = 0;
		int count = 0;
		for (Object value : this) {
			if ((value != null) && (isNumber(value))) {
				double doubleValue = doubleValue(value);
				if (doubleValue > 0) {
					sum += doubleValue;
					count += 1;
				}
			}
		}

		//
		if (count == 0) {
			result = 0;
		} else {
			result = sum * 1.0 / count;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Object> iterator() {
		Iterator<Object> result;

		result = super.values().iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double max() {
		double result;

		Double maxValue = null;
		for (Object value : this) {
			if ((value != null) && (isNumber(value))) {
				if (maxValue == null) {
					maxValue = doubleValue(value);
				} else if (doubleValue(value) > maxValue) {
					maxValue = doubleValue(value);
				}
			}
		}

		if (maxValue == null) {
			result = 0;
		} else {
			result = maxValue;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public double min() {
		double result;

		Double minValue = null;
		for (Object value : this) {
			if ((value != null) && (isNumber(value))) {
				if (minValue == null) {
					minValue = doubleValue(value);
				} else if (doubleValue(value) < minValue) {
					minValue = doubleValue(value);
				}
			}
		}

		if (minValue == null) {
			result = 0;
		} else {
			result = minValue;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Object> toList() {
		List<Object> result;

		result = new ArrayList<Object>(values());

		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private static double doubleValue(final Object value) {
		double result;

		if (value == null) {
			throw new ClassCastException("Null is not a number.");
		} else if (value instanceof Integer) {
			result = ((Integer) value).doubleValue();
		} else if (value instanceof Double) {
			result = ((Double) value).doubleValue();
		} else {
			throw new ClassCastException("Value is not a number.");
		}

		//
		return result;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private static boolean isNumber(final Object value) {
		boolean result;

		if (value == null) {
			result = false;
		} else if ((value instanceof Integer) || (value instanceof Double)) {
			result = true;
		} else {
			result = false;
		}

		//
		return result;
	}
}
