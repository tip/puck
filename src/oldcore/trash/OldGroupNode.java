package oldcore.trash;

import java.util.List;
import java.util.Set;


import maps.CountMap;
import maps.GroupNet;
import chains.Ring;
import elements.Arc;
import elements.Element;
import groups.ChainGroup;

/**
 * This class defines a vertex representing a cluster of circuits
 * <p> It is used for ring intersection networks.
 * @author Klaus Hamberger
 * @since Version 0.6
 *
 */
public class OldGroupNode  {



	/**
	 * the circuit associated to the node 
	 * <p> for circuit intersection networks
	 */
	private OldRing oldRing;
	
	/**
	 * The constructor for vertices representing ringclasses
	 * @param c the ringclass
	 */
	public OldGroupNode (ChainGroup<OldRing> c) {
		setNumber(c.getID());
		setRelationAttributeLabel(c.signature(2));
		setGender(1);
		oldRing = c.getModel();
		setValue(c.nodeCount());
	}
	
	/**
	 * The standard constructor
	 * @param i the current index of the vertex
	 * @param v the value of the vertex
	 * @param lb the label of the vertex
	 */
	public OldGroupNode (int i, int v, String lb){
		setNumber(i);
		setGender(1);
		setValue(v);
		setRelationAttributeLabel(lb);
	}
	
	/**
	 * The constructor for vertices representing ring types
	 * @param r the ring model
	 * @param i the current index of the vertex
	 * @param v the value of the vertex
	 */
	public OldGroupNode (OldRing r, int i, int v){
		setNumber(i);
		setRelationAttributeLabel(r.signature());
		setGender(1);
		setValue(v);
        oldRing = r;
	}

	
	/**
	 * compares the Vertex with another vertex
	 * @param v the vertex to be compared
	 * @return 1 if ego precedes alter, -1 if alter precedes ego, 0 if alter and ego are equal
	 */
	@Override
	public int compareTo (Element v) {
		return oldRing.compareTo(((OldGroupNode)v).getRing());
	}

	
	//Test
	/**
	 * gets the arc representations of links to other vertices and stores them in an arc list
	 * @param arcs the arc list  
//	 * @param t the code for the graph type
	 */
	public void getArcs (List<Arc> arcs, int t){
		if (getLinks()!=null) getLinks().getArcs(getCurrent(),arcs);
	}


	/**
	 * gets the cluster to which the vertex belongs 
	 * <p>Implementation of the abstract method; bin and deg are only functional for standard vertices
	 * @param str the attribute label of the partition 
	 * @return the cluster to which the vertex belongs
	 */
	public Object getCluster (String str, String bin, int deg){
		if (str.equals("SIMPLE")) return getAttributeLabel();
		return oldRing.getHeading(str);
	}
	
	/**
	 * returns a new vertex of equal number, value and label
	 * @return the copy of the vertex
	 * @see OldGroupNet.GroupNet#ClassNet(OldGroupNet)
	 */
	public OldGroupNode getCopy(){
		return new OldGroupNode(getID(), getValue(), getAttributeLabel());
	}

	
	/**
	 * gets the ring represented by the vertex (only for ring networks)
	 * @return the ring represented by the vertex
	 */
	public OldRing getRing() {
		return oldRing;
	}


	
}
