package org.tip.puck.sequences;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Individualizable;
import org.tip.puck.net.relations.Actor;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.net.relations.workers.RelationValuator;
import org.tip.puck.net.workers.IndividualValuator;
import org.tip.puck.sequences.workers.SequenceCriteria;
import org.tip.puck.util.Numberable;

/**
 * 
 * @author Klaus Hamberger
 *
 */
public class EgoSequence extends Sequence<Relation> implements Individualizable, Sequenceable<Relation> {
	
//	int id;
//	Map<Ordinal,Relation> stations;
//	public Map<Relation,String> stationTypes;
//	SequenceType type;

	Individual ego;
	
//	String egoRoleName;
//	public Partition<Individual> alters;
//	public Map<Individual,String> alterRelations;
//	String dateLabel;
	
	

	public EgoSequence (Individual ego){
		super();
		this.ego = ego;
		this.id = ego.getId();
	}
	
	public EgoSequence (Individual ego, int id){
		super();
		this.ego = ego;
		this.id = id;
	}
	
	public Individual getEgo() {
		return ego;
	}
	
	public String getLabel(){
		return ego.signature();
	}
	
	public <S extends Sequenceable<E>,E extends Numberable> EgoSequence(S sequence){
		super();
		this.ego = new Individual(sequence.getId(),sequence.getLabel(),Gender.UNKNOWN);
		this.id = sequence.getId();
		this.idLabel = sequence.idLabel();
		for (Ordinal time : sequence.getTimes()){
			this.stations.put(time, (Relation)sequence.getStation(time));
		}
	}

/*	public void setEgo(Individual ego) {
		this.ego = ego;
	}

	public String getEgoRoleName() {
		return egoRoleName;
	}

	public void setEgoRoleName(String egoRoleName) {
		this.egoRoleName = egoRoleName;
	}

	
	public Ordinal getNext (Ordinal key){
		Ordinal result;
		
		if (key!=null){
			result = ((TreeMap<Ordinal,Relation>)events).higherKey(key);
		} else {
			result = null;
		}
		//
		return result;
	}
	
	public Ordinal getNextFree (Ordinal key, Set<Ordinal> filter){
		Ordinal result;
		
		if (key!=null){
			result = ((TreeMap<Ordinal,Relation>)events).higherKey(key);
			if (result !=null && filter.contains(result)){
				result = getNextFree(result,filter);
			}
		} else {
			result = null;
		}
		//
		return result;
	}
	
	public Ordinal getFirst (){
		Ordinal result;
		
		result = ((TreeMap<Ordinal,Relation>)events).firstKey();
		
		//
		return result;
	}
	

	public Roles getRoles(){
		Roles result;
		
		result = new Roles();
		
		for (Relation relation : stations.values()){
			for (Actor actor : relation.actors()){
				if (!result.contains(actor.getRole())) {
					result.add(actor.getRole());
				}
			}
		}
		//
		return result;
	}
	
	public Individuals individuals(){
		Individuals result;
		
		result = new Individuals();
		for (Relation relation : stations.values()){
			for (Actor actor : relation.actors()){
				if (!result.contains(actor.getIndividual())) {
					result.add(actor.getIndividual());
				}
			}
		}
		//
		return result;
	}*/
	
/*	public Graph getExtendedParcoursGraph (Geography geography) {
		Graph result;
		
		List<String> labels = new ArrayList<String>();
		labels.add("HOST");
		labels.add("MIG");
		
		result = new Graph<Relation>();
		result.setLabel("Extended Parcours "+ego);
		
		Relation lastEvent = null;
		for (Relation event : events.values()){
			if (lastEvent != null){
				Link<Relation> eventLink = result.addArc(lastEvent, event,2);
				eventLink.setTag(ego.getId()+"");
				for (String label : labels){
					for (Individual alter : event.getIndividuals(label)){
						if (label!=egoRoleName || alter!=ego){
							Link alterLink = result.addArc(event, alter, 1);
							alterLink.setTag(label);
						}
					}
				}
			}
			lastEvent = event;
		}
		
		if (events.size()==1){
			result.addNode(lastEvent);
		}
		
		for (String label : labels){
			Cluster<Individual> cluster = alters.getCluster(new Value(label));
			if (cluster!=null){
				for (Individual alter : cluster.getItems()){
					for (Relation event : alter.relations().getByModel(lastEvent.getModel())){
						if (!getEvents().containsValue(event)){
							if(event.getAttributeValue("START_PLACE")!=null && getYear(event)!=null && getYear(lastEvent)!=null && getYear(event)<=getYear(lastEvent)){
								for (Individual tertius : event.getIndividuals()){
									if (tertius.getId()> alter.getId() && result.getNode(tertius)!=null){
										Link link1 = result.addArc(event, tertius, 1);
										link1.setTag(event.getRoleNamesAsString(tertius));
										Link link2 = result.addArc(event, alter, 1);
										link2.setTag(event.getRoleNamesAsString(alter));
									}
								}
							}
						}
					}
				}
			}
		}
		
		int order = 1;
		for (Object obj : result.getNodes()){
			Node node = (Node)obj;
			if (node.getReferent() instanceof Individual){
				node.setAttribute("TYPE", "INDIVIDUAL");
				Value roleNames = alters.getValue((Individual)node.getReferent());
//				if (roleNames!=null){
					node.setAttribute("MOVEMENT", roleNames.toString());
//				}
				node.setAttribute("RELATIONS", alterRelations.get((Individual)node.getReferent()));
				node.setAttribute("ORDER", "0");
			} else if (node.getReferent() instanceof Relation){
				node.setAttribute("TYPE", "EVENT");
				GeoLevel distance = getDistance(geography,(Relation)node.getReferent());
				String type = getEventType((Relation)node.getReferent());
				if (distance!=null){
					node.setAttribute("MOVEMENT", distance.toString());
				}
				if (type!=null){
					node.setAttribute("RELATIONS", type);
				}
				node.setAttribute("ORDER", order+"");
				order++;
			}
		}
		
		//
		return result;

	}*/
	
/*	public Partition<Individual> alterRelations (String alterRoleName){
		Partition<Individual> result;
		
		result = new Partition<Individual>();
		
		List<Individual> individuals;
		if (alterRoleName.equals("ALL")){
			individuals = alters.getItemsAsList();
		} else {
			individuals = alters(alterRoleName);
		}
		
		for (Individual alter : individuals){
			result.put(alter, new Value(alterRelations.get(alter)));
		}
		
		//
		return result;
	}*/
	

	/**
	 * Replace by SequenceCensus.setAlters()
	 */
/*	public void setAlters () {
		
		alters = new Partition<Individual>();
		
		//
		for (Relation event : events.values()){
			if (event.hasRole(ego, egoRoleName) && (event.getAttributeValue("START_PLACE")!=null)){
				for (Actor actor : event.actors()){
					if (!actor.getRole().getName().equals(egoRoleName) || actor.getIndividual()!=ego){
						Value value = alters.getValue(actor.getIndividual());
						if (value==null){
							alters.put(actor.getIndividual(), new Value(actor.getRole().getName()));
						} else {
							alters.put(actor.getIndividual(), new Value(value.toString()+" "+actor.getRole().getName()));
						}
					}
				}
			}
		}
	}*/
	
	public Integer getEgoAge (Integer year){
		Integer result;
		
		if (year!=null){
			result = IndividualValuator.ageAtYear(ego, year);
		} else {
			result = null;
		}

		//
		return result;
	}
	
	/**
	 * replace by SequenceCensus.getAge
	 * @param event
	 * @return
	 * @deprecated
	 */
/*	public Integer getAge (Relation event){
		Integer result;
		
		result = null;

		if (event!=null){
			int year = getYear(event);
			result = IndividualValuator.ageAtYear(ego, year);
		}
		//
		return result;
	}*/
	


	
/*	public boolean isMigration(Relation event){
		boolean result;
		
		result = event.getRoleNames(ego).contains("MIG") && event.getAttributeValue("START_PLACE")!=null && event.getAttributeValue("END_PLACE")!=null;

		//
		return result;
	}
	
	public boolean isEarlyMigration(Relation event, int ageLimit){
		boolean result;
		
		Integer age = getAge(event);
		result = isMigration(event) && age!=null && age<ageLimit;
		
		//
		return result;
	}
	
	public boolean isMigrationToParent (Relation event){
		boolean result;
		
		Individuals hosts = event.getIndividuals("HOST");
		
		result = isMigration(event) && (hosts.contains(ego.getFather()) || hosts.contains(ego.getMother()));

		//
		return result;
	}
	
	public boolean isMigrationToNonParent (Relation event){
		boolean result;
		
		result = isMigration(event) && !isMigrationToParent(event);

		//
		return result;
	}

	public boolean isMigrationWithParent (Relation event){
		boolean result;
		
		Individuals company = event.getIndividuals("MIG");
		
		result = isMigration(event) && (company.contains(ego.getFather()) || company.contains(ego.getMother()));

		//
		return result;
	}
	
	public boolean isMigrationWithoutParent (Relation event){
		boolean result;
		
		result = isMigration(event) && !isMigrationWithParent(event);

		//
		return result;
	}
	
	public boolean isMigrationWithSpouse (Relation event){
		boolean result;
		
		result = false;
		
		if (isMigration(event)){
			Individuals company = event.getIndividuals("MIG");
			for (Individual spouse : ego.getSpousesAt(getYear(event))){
				if (company.contains(spouse)){
					result = true;
					break;
				}
			}
		}

		//
		return result;
	}
	
	public boolean isMigrationWithoutSpouse (Relation event){
		boolean result;
		
		result = isMigration(event) && ego.getSpousesAt(getYear(event)).size()>0 && !isMigrationWithSpouse(event);

		//
		return result;
	}

	public boolean isMigrationToNonSpouse (Relation event){
		boolean result;
		
		result = isMigration(event) && ego.getSpousesAt(getYear(event)).size()>0 && !isMigrationToSpouse(event);

		//
		return result;
	}

	
	public boolean isMigrationToSpouse (Relation event){
		boolean result;
		
		result = false;
		
		if (isMigration(event)){
			Individuals hosts = event.getIndividuals("HOST");
			for (Individual spouse : ego.getSpousesAt(getYear(event))){
				if (hosts.contains(spouse)){
					result = true;
					break;
				}
			}
		}

		//
		return result;
	}*/
	
	/**
	 * replace by census.getRelationTypes(alters)
	 * @param alters
	 * @return
	 * @deprecated
	 */
/*	public List<String> getRelationTypes (Individuals alters){
		List<String> result;
		
		result = new ArrayList<String>();
		for (Individual alter : alters){
			String relation = alterRelations.get(alter);
			if (!result.contains(relation)){
				result.add(relation);
			}
		}
		//
		Collections.sort(result);
		
		//
		return result;
	}*/
	
	/**
	 * replace by Census.getType
	 * @param event
	 * @param roleName
	 * @return
	 */
/*	public String getType (Relation event, String roleName){
		String result;
		
		Individuals alters = event.getIndividuals(roleName);
		if (roleName.equals("MIG")){
			alters.removeById(ego.getId());
		}
		
		result = Trafo.asShortCutString(getRelationTypes(alters), 1);
		
		if (result==null){
			if (roleName.equals("MIG")){
				result = "SINGLE";
			} else {
				result = "NONE";
			}
		}
		
		//
		return result;
	}
	
	public String getEventTypePattern (){
		String result;
		
		result = "";
		boolean first = true;
		
		for (Relation event : getStations().values()){
			if (first){
				result += getEventType(event);
				first = false;
			} else {
				result += ";"+getEventType(event);
			}
		}
		//
		return result;
	}*/
	
/*	public List<String> getStationTypesAsSortedList(){
		List<String> result;
		
		result = new ArrayList<String>();
		for (String type : stationTypes.values()){
			if (!result.contains(type)){
				result.add(type);
			}
		}
		Collections.sort(result);
		
		//
		return result;
	}
	
	public String getStationTypesAsString (){
		String result;
		
		result ="";
		boolean first = true;
		List<String> list = getStationTypesAsSortedList();
		
		if (stationTypes != null){
			for (String type : list){
				if (first){
					result += type;
					first = false;
				} else {
					result += ";"+type;
				}
			}
		}
		
		//
		return result;
	}
	
	public int getNrStations(){
		return stations.size();
	}
	
	public int getNrStationTypes(){
		int result;
		
		if (stationTypes != null){
			result = getStationTypesAsSortedList().size();
		} else {
			result = 0;
		}
		//
		return result;
	}
	
	public String getStationType (Relation event){
		String result;
		
		result = stationTypes.get(event);

		//
		return result;
		
	}
	
	public Partition<Relation> getStationTypes (){
		Partition<Relation> result;
		
		result = new Partition<Relation>();
		
		for (Relation event : stations.values()){
			result.put(event, new Value(getStationType(event)));
		}
		
		//
		return result;
	}*/
	
	
/*	public Relations childMigrationsWithoutParents() {
		Relations result;
		
		result = new Relations();
		
		for (Relation event : stations.values()){
			Integer age = getAge(event);
			if (age!=null && age<18 && event.getRoleNames(ego).contains("MIG") && event.getAttributeValue("START_PLACE")!=null && event.getAttributeValue("END_PLACE")!=null 
					&& !event.getIndividuals("MIG").contains(ego.getFather())
					&& !event.getIndividuals("MIG").contains(ego.getMother())
					&& !event.getIndividuals("HOST").contains(ego.getFather())
					&& !event.getIndividuals("HOST").contains(ego.getMother())) {
				result.add(event);
			} else if (age!=null && age>=18){
				break;
			}
		}
		//
		return result;
	}*/
	
/*	public Relation firstMigrationWithoutParents(){
		Relation result;
		
		result = null;
		
		for (Relation event : events.values()){
			if (event.getRoleNames(ego).contains("MIG") && event.getAttributeValue("START_PLACE")!=null && event.getAttributeValue("END_PLACE")!=null 
					&& !event.getIndividuals("MIG").contains(ego.getFather())
					&& !event.getIndividuals("MIG").contains(ego.getMother())
					&& !event.getIndividuals("HOST").contains(ego.getFather())
					&& !event.getIndividuals("HOST").contains(ego.getMother()))
			
			{
				result = event;
				break;
			}
		}
		
		//
		return result;
	}*/
	
	
	
	
	
/*	public void put(Ordinal ordinal, Relation relation){
		stations.put(ordinal, relation);
	}*/
	
	public EgoSequence clone (){
		EgoSequence result;
		
		result = new EgoSequence(ego,id);
//		result.setEgoRoleName(egoRoleName);
		
		for (Ordinal key : stations.keySet()){
			result.put(key, getStation(key));
		}
		
		//
		return result;
	}
	
	public EgoSequence clone(int id){
		EgoSequence result;
		
		result = clone();
		result.setId(id);
		
		//
		return result;
	}

	
/*	public List<Relation> toList(){
		List<Relation> result;
		
		//
		result = new ArrayList<Relation>(stations.values());
		
		//
		return result;
	}
	

	
	public Places getPlaceSequence(Geography geography, GeoLevel level){
		Places result;
		
		//
		result = new Places();
		
		for (Relation relation : stations.values()){
			Place place = geography.getPlace(relation.getAttributeValue("END_PLACE"),level);
			if (place==null){
				place = new Place(level,result.size()+" ?");
			}
			if (result.size()==0 || !place.equals(result.get((result.size()-1)+""))){
				result.put(result.size()+"",place);
			}
		}
		//
		return result;
	}*/
	
	public void putInOrder (Relations events, SequenceType type) {
		
		// Overrides criteria (so that ego is considered in any role)
		SequenceCriteria criteria = new SequenceCriteria();

		List<Relation> orderedEvents = new ArrayList<Relation>();
		for (Relation event : events){
						
			String startPlace=event.getAttributeValue(criteria.getStartPlaceLabel());
			String endPlace = event.getAttributeValue(criteria.getEndPlaceLabel());
			
			if (type != SequenceType.MOVES || (startPlace!=null && endPlace!=null)){
				
				// Adjust for non-mobility events
				if (startPlace==null && endPlace == null){
					startPlace=event.getAttributeValue("PLACE");
					endPlace = event.getAttributeValue("PLACE");
				}
				
				if (startPlace!=null || endPlace != null){
					orderedEvents.add(event);
				}
			}
		}
		Collections.sort(orderedEvents, new EventComparator(criteria));
		
		for (Relation event : orderedEvents){
			this.stations.put(new Ordinal(event.getId()+"",this.stations.size(),RelationValuator.getYear(event)), event);
		}

		
/*		boolean end = false;
		String previousPlace = null;
		
		while (!end){
			
			boolean fits = false;
			for (Relation event : orderedEvents){
				String newPlace = putInOrder(event, previousPlace, type, true, criteria);
				if (newPlace != null){
					fits = true;
					previousPlace = newPlace;
					break;
				}
			}
			if (!fits){
				for (Relation event : orderedEvents){
					String newPlace = putInOrder(event, previousPlace, type, false, criteria);
					if (newPlace != null){
						fits = true;
						previousPlace = newPlace;
						break;
					}
				}
			}
			if (!fits){
				end = true;
			}
		}*/
	}
	
/*	public String putInOrder(Relation event, String previousPlace, SequenceType type, boolean strict, SequenceCriteria criteria){
		String result;
		
		result = null;
		
		if (!stations.containsValue(event)){
			
			String startPlace=event.getAttributeValue(criteria.getStartPlaceLabel());
			String endPlace = event.getAttributeValue(criteria.getStartPlaceLabel());
			
			if (type != SequenceType.MOVEMENTS_ONLY || (startPlace!=null && endPlace!=null) || (startPlace==null && endPlace==null)){
				
				// Adjust for non-mobility events
				if (startPlace==null && endPlace == null){
					startPlace=event.getAttributeValue("PLACE");
					endPlace = event.getAttributeValue("PLACE");
				}
				
				if (startPlace!=null || endPlace != null){

					if (!strict || previousPlace==null || previousPlace.equals(startPlace)){
						stations.put(new Ordinal(event.getName(),stations.size(),getYear(event)), event);
						result = endPlace;
					}

//					Ordinal ordinal = new Ordinal(event.getAttributeValue(dateLabel),getOrder(event), startPlace, endPlace, previousPlace, event);
				}
			}
			
/*			Relation previousEvent = events.get(ordinal);
			String name = ordinal.getName();
			Integer order = ordinal.getOrder();
			Integer year = ordinal.getYear();

			if (previousEvent != null){

				int add = 2;
				previousEvent = events.get(new Ordinal(name+" "+add,order,year));
				
				while (previousEvent != null){
					add++;
					previousEvent = events.get(new Ordinal(name+" "+add,order,year));
				}
				name = name+" "+add;
			}

			put(new Ordinal(name,order,year),event);*/
/*		}		
		//
		return result;
	}*/
	
/*	public Map<Ordinal, Relation> getStations() {
		return stations;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String hashKey() {
		return id + "";
	}

	@Override
	public int compareTo(EgoRelationSequence sequence) {
		return Integer.valueOf(this.id).compareTo(Integer.valueOf(sequence.id));
	}

	public String dateLabel() {
		return dateLabel;
	}

	public void setDateLabel(String dateLabel) {
		this.dateLabel = dateLabel;
	}*/

	public Integer getOrder (Relation event){
		Integer result;
		
		result = null;
		
		for (Actor actor : event.actors().getById(ego.getId())){
			
			String orderString = actor.getAttributeValue("ORDER");
			
			if (StringUtils.isNumeric(orderString)){
				result = Integer.parseInt(orderString); 
				break;
			}

		}
		
		//
		return result;
	}
	
	
}
