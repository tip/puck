package org.tip.puck.io.kinsources;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.tip.puck.io.kinsources.CatalogItemComparator.Criteria;

/**
 * The <code>Accounts</code> class represents an account collection.
 * 
 * @author christian.momon@devinsy.fr
 */
public class Catalog implements Iterable<CatalogItem> {

	private String source;
	private List<CatalogItem> items;

	/**
	 * 
	 */
	public Catalog() {
		super();

		this.items = new ArrayList<CatalogItem>();
	}

	/**
	 * 
	 */
	public Catalog(final int initialCapacity) {

		this.items = new ArrayList<CatalogItem>(initialCapacity);
	}

	/**
	 * 
	 */
	public void add(final CatalogItem source) {
		//
		if (source == null) {
			//
			throw new IllegalArgumentException("source is null");

		} else {
			//
			this.items.add(source);
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void addAll(final Catalog source) {
		if (source != null) {
			for (CatalogItem item : source) {
				if (!this.items.contains(item)) {
					add(item);
				}
			}
		}
	}

	/**
	 * 
	 */
	public void clear() {
		this.items.clear();
	}

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	public boolean contains(final CatalogItem criteria) {
		boolean result;

		result = this.items.contains(criteria);

		//
		return result;
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public Catalog copy() {
		Catalog result;

		//
		result = new Catalog(this.items.size());

		//
		for (CatalogItem item : this.items) {
			result.add(item);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfItems() {
		long result;

		result = this.items.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Catalog findWithFile() {
		Catalog result;

		//
		result = new Catalog(this.items.size());

		for (CatalogItem dataset : this.items) {
			//
			if ((dataset.getIndividualCount() != null) && (dataset.getIndividualCount() != 0)) {
				result.add(dataset);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public CatalogItem getByIndex(final int index) {
		CatalogItem result;

		result = this.items.get(index);

		//
		return result;
	}

	public String getSource() {
		return this.source;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		boolean result;

		result = this.items.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty() {
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public Iterator<CatalogItem> iterator() {
		Iterator<CatalogItem> result;

		result = this.items.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final CatalogItem item) {
		this.items.remove(item);
	}

	/**
	 * 
	 * @return
	 */
	public Catalog reverse() {
		Catalog result;

		//
		Collections.reverse(this.items);

		//
		result = this;

		//
		return result;
	}

	public void setSource(final String source) {
		this.source = source;
	}

	/**
	 * 
	 * @return
	 */
	public int size() {
		int result;

		result = this.items.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Catalog sortBy(final Criteria sortCriteria) {
		Catalog result;

		//
		Collections.sort(this.items, new CatalogItemComparator(sortCriteria));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Catalog sortById() {
		Catalog result;

		//
		Collections.sort(this.items, new CatalogItemComparator(Criteria.ID));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Catalog sortByName() {
		Catalog result;

		//
		Collections.sort(this.items, new CatalogItemComparator(Criteria.NAME));

		//
		result = this;

		//
		return result;
	}

}
