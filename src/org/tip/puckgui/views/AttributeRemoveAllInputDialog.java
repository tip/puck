package org.tip.puckgui.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

/**
 * 
 * @author TIP
 */
public class AttributeRemoveAllInputDialog extends JDialog {

	private static final long serialVersionUID = -7375912189288125774L;

	public enum Criteria {
		CORPUS_INCLUDED,
		CORPUS_EXCLUDED
	}

	private final JPanel contentPanel = new JPanel();
	private Criteria dialogCriteria;
	private static Criteria lastCriteria = Criteria.CORPUS_EXCLUDED;

	private JCheckBox chckbxIncludeCase;

	/**
	 * Create the dialog.
	 */
	public AttributeRemoveAllInputDialog() {
		super();

		// ////////////////////////////////////////////////
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setAlwaysOnTop(true);
		setTitle("Rename All Attribute");
		setIconImage(Toolkit.getDefaultToolkit().getImage(AttributeRemoveAllInputDialog.class.getResource("/org/tip/puckgui/favicon-16x16.jpg")));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				// Closing window.
				// Cancel button.
				AttributeRemoveAllInputDialog.this.dialogCriteria = null;
				setVisible(false);
			}
		});

		setBounds(100, 100, 372, 168);
		getContentPane().setLayout(new BorderLayout());
		this.contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(this.contentPanel, BorderLayout.CENTER);
		this.contentPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, }));
		{
			JLabel lblNewLabel = new JLabel("");
			lblNewLabel.setIcon(new ImageIcon(AttributeRemoveAllInputDialog.class.getResource("/org/tip/puckgui/images/dialog-warning.png")));
			this.contentPanel.add(lblNewLabel, "2, 2");
		}
		{
			JLabel lblNewLabel_1 = new JLabel("<html>You are about to remove all attributes.<br/>Do you confirm?</html>");
			this.contentPanel.add(lblNewLabel_1, "4, 2");
		}
		{
			this.chckbxIncludeCase = new JCheckBox("include corpus tab attributes");
			this.contentPanel.add(this.chckbxIncludeCase, "4, 4");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						// Cancel button.
						AttributeRemoveAllInputDialog.this.dialogCriteria = null;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("Remove All");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(final ActionEvent e) {
						//
						Criteria criteria = getCriteria();
						//
						lastCriteria = criteria;
						AttributeRemoveAllInputDialog.this.dialogCriteria = criteria;

						//
						setVisible(false);

					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}

		// ////////////////////////
		setCriteria(lastCriteria);
	}

	/**
	 * 
	 * @return
	 */
	public Criteria getCriteria() {
		Criteria result;

		//
		if (this.chckbxIncludeCase.isSelected()) {
			//
			result = Criteria.CORPUS_INCLUDED;

		} else {
			//
			result = Criteria.CORPUS_EXCLUDED;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Criteria getDialogCriteria() {
		Criteria result;

		result = this.dialogCriteria;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	private void setCriteria(final Criteria source) {
		//
		if (source != null) {

			//
			switch (source) {
				case CORPUS_EXCLUDED:
					this.chckbxIncludeCase.setSelected(false);
				break;

				case CORPUS_INCLUDED:
					this.chckbxIncludeCase.setSelected(true);
				break;
			}
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		/* AttributeFilter criteria = */showDialog();
	}

	/**
	 * Launch the application.
	 */
	public static Criteria showDialog() {
		Criteria result;

		//
		AttributeRemoveAllInputDialog dialog = new AttributeRemoveAllInputDialog();
		dialog.setLocationRelativeTo(null);
		dialog.pack();
		dialog.setVisible(true);

		//
		result = dialog.getDialogCriteria();

		//
		return result;
	}
}
