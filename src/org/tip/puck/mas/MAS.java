/**
 * 
 */
package org.tip.puck.mas;


import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;


/**
 * @author Telmo Menezes
 *
 */
public class MAS {

    private Vector<Agent> livingAgents;
    private Vector<Agent> livingMen;
    private Vector<Agent> livingWomen;
    private Vector<Agent> allAgents;
    private Vector<Family> currentMarriages;
    private Vector<Family> allMarriages;
    
    private List<WeightFactor> weightFactors;
    
    private Random randGen;
    
    private int curId;
    private int curFamilyId;
    private int curCycle;
  
    private int years;
    private int initialPopulation;
    private int maxAge;
    private double fertilityRate;
    
    private int cousin1Marriages;
    private int agnaticCousin1Marriages;
    private int uterineCousin1Marriages;
    private double cousins;
    
    private int samples;
    private Agent[] mothers;
    private Agent[] fathers;
    private double[] weights;
    
    public MAS() {
    	this.samples = -1;
    	
        // init agent lists
        livingAgents = new Vector<Agent>();
        livingMen = new Vector<Agent>();
        livingWomen = new Vector<Agent>();
        allAgents = new Vector<Agent>();
        currentMarriages = new Vector<Family>();
        allMarriages = new Vector<Family>();
    
        // init weight factors lists
        weightFactors = new LinkedList<WeightFactor>();
        
        // default values
/*        years = 300;
        initialPopulation = 100;
        maxAge = 70;
        fertilityRate = 2.0;*/
        
        // init random number generator
        randGen = new Random();
        
        // auxiliary simulation vars
        curId = 1;
        curFamilyId = 0;
        curCycle = 0;
        
        cousin1Marriages = 0;
        agnaticCousin1Marriages = 0;
        uterineCousin1Marriages = 0;
        cousins = 0;
    }
    
    public void addFactor(WeightFactor factor) {
        weightFactors.add(factor);
    }
    
    public WeightFactor getFactor (String factorName){
    	WeightFactor result;
    	
    	result = null;
    	for (WeightFactor factor : weightFactors){
    		if (factor.factorName().equals(factorName)){
    			result = factor;
    			break;
    		}
    	}
    	//
    	return result;
    }
    
    private boolean testProb(double p) {
        if (p <= 0) {
            return false;
        }
        else if (p >= 1) {
            return true;
        }
        double r = randGen.nextDouble();
        return (r <= p);
    }
    
    public void initializePopulation(int size) {
        for (int i = 0; i < size; i++) {
            Agent a = birth(null, null);
            
            // Assing varied ages to inital population
            a.setBirth(-randGen.nextInt(maxAge));
        }
    }
    
    public void run() {
        curCycle = 0;
        
        initializePopulation(initialPopulation);
        
        for (int i = 0; i < years; i++) {
            cycle();
            curCycle++;
        }
    }
    
    private Agent birth(Agent mother, Agent father) {
        // check if this changes marriage status on the parents
        if ((mother != null) && (father != null)) {
            if (!mother.isPartnerOf(father)) {
                if (mother.getCurrentMarriage() != null) {
                    divorce(mother);
                }
                if (father.getCurrentMarriage() != null) {
                    divorce((Agent) father.getCurrentMarriage().getWife());
                }
                marriage(father, mother);
            }
        }
        
        Gender gender = Gender.MALE;
        
        // TODO: configure male/female ratio
        if (testProb(0.5)) {
            gender = Gender.FEMALE;
        }
        
        Family origFamily = null;
        if (mother != null) {
        	origFamily = mother.getCurrentMarriage();
        }
        Agent child = new Agent(this, curId++, gender, curCycle, origFamily);
        livingAgents.add(child);
        allAgents.add(child);
        
        if (gender.isMale()) {
            livingMen.add(child);
        }
        else if (gender.isFemale()) {
            livingWomen.add(child);
        }
        
        // add child to marriage
        if (mother != null) {
        	mother.getCurrentMarriage().getChildren().add(child);
        }
        
        // inc child count and update timeOfLastChildBirth on both parents
        if (mother != null) {
        	mother.setTimeOfLastChildBirth(curCycle);
        }
        if (father != null) {
        	father.setTimeOfLastChildBirth(curCycle);
        }
        
        // update cousins
        child.updateCousins();
        
        return child;
    }
    
    private void marriage(Agent husband, Agent wife) {
        Family marriage = new Family(curFamilyId++, husband, wife);
        marriage.setMarried(true);
        currentMarriages.add(marriage);
        allMarriages.add(marriage);
        husband.setCurrentMarriage(marriage);
        wife.setCurrentMarriage(marriage);
        husband.addPersonalFamily(marriage);
        wife.addPersonalFamily(marriage);
        husband.addPartner(wife);
        wife.addPartner(husband);
        
        if (husband.hasCousin(wife, 1)) {
        	cousin1Marriages += 1;
        	husband.setAttribute("COUSINMARRIAGE", wife.getId()+"");
        	wife.setAttribute("COUSINMARRIAGE", husband.getId()+"");
        	marriage.setAttribute("COUSINMARRIAGE", "true");
        }
        if (husband.hasAgnaticCousin(wife, 1)) {
        	agnaticCousin1Marriages += 1;
        }
        if (husband.hasUterineCousin(wife, 1)) {
        	uterineCousin1Marriages += 1;
        }
    }
    
    private void divorce(Agent wife) {
    	//logicEngine.addDivorce((Agent)wife.getCurrentMarriage().getHusband(), wife);
    	
        endMarriage((Agent)wife.getCurrentMarriage().getHusband());
        endMarriage(wife);
    }
    
    private void endMarriage(Agent agent) {
        if (agent.getCurrentMarriage() != null) {
            currentMarriages.remove(agent.getCurrentMarriage());
            
            if (agent.isMale()) {
                Agent wife = (Agent)agent.getCurrentMarriage().getWife();
                wife.setCurrentMarriage(null);
            }
            else if (agent.isFemale()) {
                Agent husband = (Agent)agent.getCurrentMarriage().getHusband();
                husband.setCurrentMarriage(null);
            }
            
            agent.setCurrentMarriage(null);
        }
    }
    
    private void death(Agent agent) {
        endMarriage(agent);
            
        if (agent.isMale()) {
            livingMen.remove(agent);
        }
        else if (agent.isFemale()) {
            livingWomen.remove(agent);
        }
        
        livingAgents.remove(agent);
        
        cousins += agent.firstCousins().size();
        
        agent.die();
    }
    
    private double computeWeight(Agent mother, Agent father) {
        double weight = 1.0;
        for (WeightFactor factor : weightFactors) {
            weight *= factor.factor(mother, father);
        }
        return weight;
    }
    
    private void generateBirth() {
    	double totalWeight = 0;
    	
        for (int i = 0; i < samples; i++) {
            Agent mother = null;
            Agent father = null;
        		
            while ((mother == null) || (isIncest(mother, father))) {
            	int indexMother = randGen.nextInt(livingWomen.size());
            	int indexFather = randGen.nextInt(livingMen.size());
            	mother = livingWomen.get(indexMother);
            	father = livingMen.get(indexFather);
            }
            		
            double weight = computeWeight(mother, father);
            totalWeight += weight;
                
            mothers[i] = mother;
            fathers[i] = father;
            weights[i] = weight;
        }
            
        // random position in [0, totalWeight]
        double pos = randGen.nextDouble() * totalWeight;
            
        totalWeight = 0;
        for (int i = 0; i < samples; i++) {
            Agent mother = mothers[i];
            Agent father = fathers[i];
            totalWeight += weights[i];
            if (totalWeight > pos) {
                birth(mother, father);
                return;
            }
        }
    }
    
    
    private void cycle() {
        double pop = livingAgents.size();
        double birthsPerYear = (pop / 2.0) * (1.0 / maxAge) * fertilityRate;
        double bpyInteger = Math.floor(birthsPerYear);
        double bpyFrac = birthsPerYear - bpyInteger;
        
        int birthsThisYear = (int)bpyInteger;
        if (testProb(bpyFrac)) {
            birthsThisYear += 1;
        }
        
//        System.out.println("birthsPerYear: " + birthsPerYear);
//        System.out.println("birthsThisYear: " + birthsThisYear);
        
        // births
        for (int i = 0; i < birthsThisYear; i++) {
            generateBirth();
        }
        
        // deaths
        List<Agent> deathList = new LinkedList<Agent>();
        for (Agent a : livingAgents) {
            if (a.getAge() >= maxAge) {
                deathList.add(a);
            }
        }
        for (Agent a : deathList) {
            death(a);
        }
        
//        System.out.println("\n>> Year: " + curCycle);
//        System.out.println("population: " + livingAgents.size() + "; men: " + livingMen.size() + "; women: " + livingWomen.size() + "; families: " + currentMarriages.size());
    }
    
    
    private boolean isIncest(Agent mother, Agent father) {
    	// mother - son
    	if (father.getMother() == mother) {
    		//System.out.println(">>>>>>>>>>> mother - son");
    		return true;
    	}
    	// father - daughter
    	if (mother.getFather() == father) {
    		//System.out.println(">>>>>>>>>>> father - daughter");
    		return true;
    	}
    	// siblings
    	if ((mother.getFather() == father.getFather()) && (mother.getFather() != null)) {
    		//System.out.println(">>>>>>>>>>> siblings 1");
    		return true;
    	}
    	if ((mother.getMother() == father.getMother()) && (mother.getMother() != null)) {
    		//System.out.println(">>>>>>>>>>> siblings 2");
    		return true;
    	}
    	// grandmother
    	if (father.getFather() != null) {
    		if (father.getFather().getMother() == mother) {
    			//System.out.println(">>>>>>>>>>> grandmother 1");
    			return true;
    		}
    	}
    	if (father.getMother() != null) {
    		if (father.getMother().getMother() == mother) {
    			//System.out.println(">>>>>>>>>>> grandmother 2");
    			return true;
    		}
    	}
    	// grandfather
    	if (mother.getFather() != null) {
    		if (mother.getFather().getFather() == father) {
    			//System.out.println(">>>>>>>>>>> grandfather 1");
    			return true;
    		}
    	}
    	if (mother.getMother() != null) {
    		if (mother.getMother().getFather() == father) {
    			//System.out.println(">>>>>>>>>>> grandfather 2");
    			return true;
    		}
    	}
    	
    	return false;
    }
    
    
    public void writeDemographicData(String filePath) {
        try {
            FileWriter outFile = new FileWriter(filePath);
            PrintWriter out = new PrintWriter(outFile);

            out.println("id, mother_age, father_age");

            for (Agent agent : allAgents) {
                out.println(String.format("%d,%d,%d",
                        agent.getId(), agent.getAgeOfMotherAtBirth(), agent.getAgeOfFatherAtBirth()));
            }

            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public int getCurCycle() {
        return curCycle;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    public double getFertilityRate() {
        return fertilityRate;
    }

    public void setFertilityRate(double fertilityRate) {
        this.fertilityRate = fertilityRate;
    }
    
    public Net toNet() {

        Net net = new Net();
        
        // add individuals to net
        for (Individual individual : allAgents) {
        	((Agent)individual).setAttributes();
            net.individuals().put(individual);
        }

        // add families to net
        for (Family marriage : allMarriages) {
        	Family family = net.families().getBySpouses(marriage.getHusband(), marriage.getWife());
        	if (family!=null){
        		family.getChildren().add(marriage.getChildren());
        	} else {
                net.families().put(marriage);
        	}
        }

        return net;
    }
    
    @Override
    public String toString() {
    	String str = "years: " + years + "\n";
    	str += "initial population: " + initialPopulation + "\n";
    	str += "fertility rate: " + fertilityRate + "\n";
    	str += "max age: " + maxAge + "\n";
    	str += "samples: " + samples + "\n";
    	
    	return str;
    }
    
    public Vector<Agent> getAllAgents() {
		return allAgents;
	}

	public int getYears() {
		return years;
	}

	public void setYears(int years) {
		this.years = years;
	}

	public int getInitialPopulation() {
		return initialPopulation;
	}

	public void setInitialPopulation(int initalPopulation) {
		this.initialPopulation = initalPopulation;
	}
	
	public double calcMeanSiblings() {
		double sum = 0;
		double count = 0;
		
		for (Agent a : allAgents) {
			sum += a.calcNumberOfSiblings();
			count += 1;
		}
		
		return sum / count;
	}
	
	public double calcMeanFullSiblings() {
		double sum = 0;
		double count = 0;
		
		for (Agent a : allAgents) {
			sum += a.calcNumberOfFullSiblings();
			count += 1;
		}
		
		return sum / count;
	}

	public int getSamples() {
		return samples;
	}

	public void setSamples(int samples) {
		this.samples = samples;
		
		if (samples > 0) {
    		mothers = new Agent[samples];
    		fathers = new Agent[samples];
    		weights = new double[samples];
    	}
	}
	
	public static void main(String[] args) {
		MASConfig config = new MASConfig();
        config.fromFile("experiments/mas/example.txt");
        MAS mas = config.getMas();
        
        System.out.println(mas);
        
        // run simulation
        mas.run();
        
        System.out.println("\n\nall marriages: " + mas.allMarriages.size());
        System.out.println("cousin1Marriages: " + mas.cousin1Marriages);
        System.out.println("agnaticCousin1Marriages: " + mas.agnaticCousin1Marriages);
        System.out.println("uterineCousin1Marriages: " + mas.uterineCousin1Marriages);
        System.out.println("meanSiblings: " + mas.calcMeanSiblings());
        System.out.println("meanFullSiblings: " + mas.calcMeanFullSiblings());
        System.out.println("meanCousins: " + mas.cousins / (double)(mas.allAgents.size()));
	}
}