package org.tip.puckgui.models;

import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractListModel;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.tip.puck.net.Families;
import org.tip.puck.net.Family;

/**
 * 
 * @author TIP
 */
public class FamiliesModel extends AbstractListModel {

	private static final long serialVersionUID = -501467776070549207L;
	protected Families source;
	protected List<Family> delegate;
	private int lastFamilySearchIndex;
	private String lastFamilySearchPattern;

	/**
	 * 
	 */
	public FamiliesModel(final Families source) {
		//
		super();

		//
		this.source = source;
		if (source == null) {
			this.delegate = new ArrayList<Family>(0);
		} else {
			this.delegate = this.source.toSortedList();
		}

		//
		this.lastFamilySearchIndex = -1;
		this.lastFamilySearchPattern = null;
	}

	/**
	 * 
	 */
	@Override
	public Object getElementAt(final int index) {
		Object result;

		result = this.delegate.get(index);

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public int getSize() {
		int result;

		result = this.delegate.size();

		//
		return result;
	}

	public Families getSource() {
		return this.source;
	}

	/**
	 * 
	 * @param individual
	 * @return
	 */
	public int indexOf(final Family family) {
		int result;

		if (this.source == null) {
			result = -1;
		} else {
			boolean ended = false;
			result = -1;
			int index = 0;
			while (!ended) {
				if (index < this.delegate.size()) {
					if (this.delegate.get(index) == family) {
						ended = true;
						result = index;
					} else {
						index += 1;
					}
				} else {
					ended = true;
					result = -1;
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param pattern
	 */
	public Family nextSearchedFamily(final String pattern) {
		Family result;

		if (StringUtils.isBlank(pattern)) {
			//
			result = null;

		} else if (NumberUtils.isDigits(pattern)) {
			// Search individual by id.
			int familyId = Integer.parseInt(pattern);
			result = this.source.getById(familyId);
			resetFamilySearch();

		} else {
			// Search family by name.
			List<Family> foundFamilies = this.source.searchByName(pattern).toSortedList();

			if (foundFamilies.isEmpty()) {
				//
				resetFamilySearch();
				result = null;

			} else if ((this.lastFamilySearchPattern == null) || (!this.lastFamilySearchPattern.equals(pattern))) {
				//
				this.lastFamilySearchIndex = 0;
				this.lastFamilySearchPattern = pattern;
				result = foundFamilies.get(0);

			} else {
				//
				this.lastFamilySearchIndex += 1;
				if (this.lastFamilySearchIndex >= foundFamilies.size()) {
					//
					this.lastFamilySearchIndex = 0;
				}
				result = foundFamilies.get(this.lastFamilySearchIndex);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param pattern
	 */
	public int nextSearchedFamilyIndex(final String pattern) {
		int result;

		result = indexOf(nextSearchedFamily(pattern));

		//
		return result;
	}

	/**
	 * 
	 */
	public void resetFamilySearch() {
		this.lastFamilySearchIndex = -1;
		this.lastFamilySearchPattern = null;
	}

	/**
	 * 
	 * @param source
	 */
	public void setSource(final Families source) {
		resetFamilySearch();
		fireIntervalRemoved(this, 0, this.delegate.size());
		this.source = source;
		this.delegate = this.source.toSortedList();
		fireIntervalAdded(this, 0, this.delegate.size());
	}
}
