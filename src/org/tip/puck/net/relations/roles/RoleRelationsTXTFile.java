package org.tip.puck.net.relations.roles;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.PuckException;
import org.tip.puck.PuckExceptions;
import org.tip.puck.net.relations.RelationModel;
import org.tip.puck.report.Report;
import org.tip.puck.report.ReportTXTFile;

import fr.devinsy.util.StringList;

public class RoleRelationsTXTFile {
	
	public static final String DEFAULT_CHARSET_NAME = "UTF-8";
	public static final int MAX_LINE_SIZE = 1024;;
	private static final Logger logger = LoggerFactory.getLogger(ReportTXTFile.class);

	/**
	 * Saves a report in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static Report save(final File file, final RelationModel source) throws PuckException {
		Report result;
		
		PrintWriter out = null;
		try {
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
			result = write(out,source);

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} finally {
			if (out != null) {
				out.close();
			}
		}
		//
		return result;
	}
	
	/**
	 * Saves a report in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 * @throws PuckException
	 */
	public static Report write(final PrintWriter out, final RelationModel source) throws PuckException {
		Report result;
		
		result = new Report("Export log");
		
		RoleDefinitions definitions = RoleRelationMaker.createRoleDefinitions(source.getRoleRelations(),source.getRules(),result);
		RoleRelationMaker.compareDefinitions(source.getRoleRelations().getOriginalDefinitions(),definitions,source.getRoleRelations(),result);
			
		for (RoleDefinition definition: definitions.toSortedList()){
			out.println(definition.toString());
		}
		//
		return result;
	}
	
	/**
	 * Loads a TXT file into a Net.
	 * 
	 * @param file
	 *            file from where load a net.
	 * 
	 * @return the loaded net.
	 * 
	 * @throws PuckException
	 */
	public static StringList load(final File file, final String charsetName) throws PuckException {
		StringList result;

		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), charsetName));
			result = read(in);

		} catch (UnsupportedEncodingException exception) {
			throw PuckExceptions.UNSUPPORTED_ENCODING.create("Opening file [" + file + "]");
		} catch (FileNotFoundException exception) {
			throw PuckExceptions.FILE_NOT_FOUND.create("Opening file [" + file + "]");
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException exception) {
				// logger.warn("Not managed error.");
				exception.printStackTrace();
			}
		}

		//
		return result;
	}
	
	public static StringList read(final BufferedReader in) throws PuckException {
		StringList result;

		result = new StringList();

		boolean ended = false;
		while (!ended) {
			//

			String source = readNotEmptyLine(in);
			if (source == null) {
				ended = true;
			} else {
				result.append(source);
			}
		}
		//
		return result;
	}
	
	public static String readNotEmptyLine(final BufferedReader in) throws PuckException {
		String result;

		try {
			boolean ended = false;
			result = null;
			while (!ended) {
				//
				String line = in.readLine();

				if (line == null) {
					ended = true;
					result = null;
				} else if (StringUtils.isNotBlank(line)) {
					ended = true;
					result = line;
				}
			}
		} catch (final IOException exception) {
			throw PuckExceptions.IO_ERROR.create(exception, "Reading line.");
		}

		//
		return result;
	}








}
