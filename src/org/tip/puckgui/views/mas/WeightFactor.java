package org.tip.puckgui.views.mas;

/**
 * 
 * @author TIP
 */
public interface WeightFactor {

	public enum Type {
		NONE,
		DIVORCE,
		DIVORCE2,
		NORMAL_AGE,
		NORMAL_AGE_DIFFERENCE,
		COUSINS,
		AGNATICCOUSINS,
		UTERINECOUSINS,
		PREGNANCY
	}

	/**
	 * 
	 * @return
	 */
	public Type getType();
}
