package org.tip.puckgui.views.visualization;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import org.gephi.project.api.Workspace;
import org.tip.puck.visualization.VisualizationController;
import org.tip.puck.visualization.exporter.Exporter;
import org.tip.puck.visualization.exporter.implementations.ExporterJPG;

/**
 *
 * @author TIP
 */
public class VisualizationPanel extends JPanel implements ActionListener {

    private static final long serialVersionUID = -6476390946825042648L;
    private final Workspace workspace;

    /**
     * Create the panel.
     */
    public VisualizationPanel(Workspace workspace, Component component) {
	super();
	this.workspace = workspace;

	setLayout(new BorderLayout());
	add(component, BorderLayout.CENTER);

	JButton btn = new JButton("Export to jpg..");
	btn.setActionCommand("export");

	btn.addActionListener(this);
	add(btn, BorderLayout.SOUTH);
    }



    @Override
    public void actionPerformed(ActionEvent e) {
	Exporter exporter = VisualizationController.getSharedInstance().getExporter(ExporterJPG.class);
		// get file
		JFileChooser chooser = new JFileChooser();
		int returnVal = chooser.showSaveDialog(VisualizationPanel.this);
		if (returnVal != JFileChooser.APPROVE_OPTION) {
		    return;
		}
		File selectedFile = chooser.getSelectedFile();
		exporter.exportToFile(selectedFile, this.workspace);
    }

}
