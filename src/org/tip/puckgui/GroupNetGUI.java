package org.tip.puckgui;

import java.util.Locale;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puck.geo.Geography;
import org.tip.puck.graphs.Graph;
import org.tip.puck.net.Individual;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.report.Report;
import org.tip.puckgui.views.GroupNetWindow;

/**
 * 
 * GroupNetworkGUI manages a window of group network. It never create them
 * itself.
 * 
 * @author TIP
 */
public class GroupNetGUI implements WindowGUI {

	private static final Logger logger = LoggerFactory.getLogger(GroupNetGUI.class);

	private int id;
	private GroupNetWindow window;
	private Graph<Cluster<Individual>> groupNetwork;
	private Geography geography;

	/**
	 * Initializes a newly create GroupNetwork object so that it represents a
	 * group network.
	 * 
	 */
	public GroupNetGUI(final int id, final Graph<Cluster<Individual>> groupNetwork) {
		//
		this.id = id;
		this.groupNetwork = groupNetwork;
		this.window = new GroupNetWindow(this);

		//
		this.window.setVisible(true);
	}

	/**
	 * 
	 * @param report
	 */
	@Override
	public void addRawTab(final String tabTitle, final JPanel panel) {
		this.window.addRawTab(tabTitle, panel);
	}

	/**
	 * 
	 * @param report
	 */
	@Override
	public void addReportTab(final Report report) {
		this.window.addReportTab(report);
	}

	/**
	 * 
	 */
	@Override
	public void addReportTab(final String tabTitle, final JPanel panel) {
		//
		this.window.addTab(tabTitle, panel);
	}

	/**
	 * 
	 * @param report
	 */
	public void addTab(final String tabTitle, final JPanel panel) {
		this.window.addTab(tabTitle, panel);
	}

	/**
	 * 
	 */
	@Override
	public void closeCurrentTab() {
		this.window.closeCurrentTab();
	}

	public Geography getGeography() {
		return this.geography;
	}

	public Graph<Cluster<Individual>> getGroupNet() {
		return this.groupNetwork;
	}

	@Override
	public int getId() {
		return this.id;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public JFrame getJFrame() {
		JFrame result;

		result = this.window;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public String getTitle() {
		String result;

		result = getGroupNet().getLabel() + "-" + getId() + " (Group Net)";

		//
		return result;
	}

	public GroupNetWindow getWindow() {
		return this.window;
	}

	/**
	 * 
	 */
	@Override
	public boolean isChanged() {
		boolean result;

		result = false;

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public void setChanged(final boolean changed) {
		// Do nothing.
	}

	public void setGeography(final Geography geography) {
		this.geography = geography;
	}

	public void setWindow(final GroupNetWindow window) {
		this.window = window;
	}

	/**
	 * Place the window on top.
	 */
	@Override
	public void toFront() {
		this.window.toFront();
	}

	/**
	 * 
	 */
	public void updateAll() {
		//
	}

	/**
	 * 
	 * @param locale
	 */
	@Override
	public void updateLocale(final Locale locale) {
		this.window.updateLocale(locale);
		// TODO Implements way to not close current windows.
		this.window.dispose();

		//
		this.window = new GroupNetWindow(this);
		// this.window.updateIndividualIdentity(null);
	}
}