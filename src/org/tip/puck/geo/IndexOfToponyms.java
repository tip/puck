package org.tip.puck.geo;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.util.StringList;

/**
 * 
 * This index ignores case of toponyms using lower case method.
 * 
 */
public class IndexOfToponyms {

	private Map<String, Place> index;

	/**
	 * 
	 */
	public IndexOfToponyms() {
		//
		this.index = new HashMap<String, Place>();
	}

	/**
	 * 
	 */
	public IndexOfToponyms clear() {

		this.index.clear();

		//
		return this;
	}

	/**
	 * 
	 * @param place
	 * @return
	 */
	public boolean contains(final String toponym) {
		boolean result;

		if (get(toponym) == null) {
			//
			result = false;

		} else {
			//
			result = true;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param toponym
	 * @return
	 */
	public Place get(final String toponym) {
		Place result;

		if (toponym == null) {
			//
			result = null;

		} else {
			//
			result = this.index.get(StringUtils.lowerCase(toponym));
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public StringList getToponyms() {
		StringList result;

		result = new StringList();

		for (String name : this.index.keySet()) {
			//
			result.add(name);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param place
	 */
	public IndexOfToponyms index(final Place place) {

		if (place != null) {
			//
			this.index.put(StringUtils.lowerCase(place.getToponym()), place);

			//
			for (String homonym : place.getHomonyms()) {
				//
				this.index.put(StringUtils.lowerCase(homonym), place);
			}
		}

		//
		return this;
	}

	/**
	 * 
	 * @param place
	 */
	public IndexOfToponyms index(final Places places) {

		if (places != null) {
			//
			for (Place place : places) {
				index(place);
			}
		}

		//
		return this;
	}

	/**
	 * 
	 * @return
	 */
	public int size() {
		int result;

		result = this.index.size();

		//
		return result;
	}
}
