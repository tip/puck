package org.tip.puck.mas;

public class WFDivorce2 implements WeightFactor {
    private double wgtDivorceMother;
    private double wgtDivorceFather;
    private double wgtDivorceBoth;
    
    public WFDivorce2(double wgtDivorceMother, double wgtDivorceFather, double wgtDivorceBoth) {
        this.wgtDivorceMother = wgtDivorceMother;
        this.wgtDivorceFather = wgtDivorceFather;
        this.wgtDivorceBoth = wgtDivorceBoth;
    }
    
    public double getWgtDivorceMother() {
		return wgtDivorceMother;
	}

	public double getWgtDivorceFather() {
		return wgtDivorceFather;
	}

	public void setWgtDivorceMother(double wgtDivorceMother) {
		this.wgtDivorceMother = wgtDivorceMother;
	}

	public void setWgtDivorceFather(double wgtDivorceFather) {
		this.wgtDivorceFather = wgtDivorceFather;
	}

	public void setWgtDivorceBoth(double wgtDivorceBoth) {
		this.wgtDivorceBoth = wgtDivorceBoth;
	}

	public double getWgtDivorceBoth() {
		return wgtDivorceBoth;
	}

	public String factorName(){
    	return "divorce2";
    }
    
    @Override
    public double factor(Agent mother, Agent father) {
        if ((father.getCurrentMarriage() == null) && (mother.getCurrentMarriage() == null)) {
            // first marriage
            return 1.0;
        }
        if (father.isPartnerOf(mother)) {
            // stay in marriage
            return 1.0;
        }
        else if ((father.getCurrentMarriage() != null) && (mother.getCurrentMarriage() != null)) {
            // divorce both
            return wgtDivorceBoth;
        }
        else if (father.getCurrentMarriage() != null) {
            // divorce father only
            return wgtDivorceFather;
        }
        else {
            // divorce mother only
            return wgtDivorceMother;
        }
    }
    
    @Override
    public boolean appliesToMarriage() {return false;}
    
    @Override
    public boolean appliesToDivorce() {return true;}
}