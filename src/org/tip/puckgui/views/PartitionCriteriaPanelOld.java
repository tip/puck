package org.tip.puckgui.views;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import org.apache.commons.lang3.math.NumberUtils;
import org.tip.puck.partitions.PartitionCriteria;
import org.tip.puck.partitions.PartitionCriteria.FamilyScope;
import org.tip.puck.partitions.PartitionCriteria.PartitionType;
import org.tip.puck.util.MathUtils;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

public class PartitionCriteriaPanelOld extends JPanel {

	private static final long serialVersionUID = -5771328198288945829L;
	// private static final Logger logger =
	// LoggerFactory.getLogger(PartitionCriteriaPanel.class);
	private static PartitionCriteria defaultCriteria = null;
	private List<String> targetLabels;
	private JTextField txtfldParameter;
	private JTextField txtfldPattern;
	private JTextField txtfldCountedStart;
	private JTextField txtfldCountedCount;
	private JTextField txtfldCountedEnd;
	private JTextField txtfldSteps;
	private JTextField txtfldSizedStart;
	private JTextField txtfldSizedSize;
	private JTextField txtfldSizedEnd;
	private JPanel panelRaw;
	private JPanel panelBinarization;
	private JPanel panelFreeGrouping;
	private JPanel panelCountedGrouping;
	private JPanel panelSizedGrouping;
	private final ButtonGroup buttonGroupType = new ButtonGroup();
	private JComboBox comboBoxLabel;
	private final ButtonGroup buttonGroupScope = new ButtonGroup();
	private JRadioButton rdbtnSome;
	private JRadioButton rdbtnAll;

	/**
	 * Create the panel.
	 */
	public PartitionCriteriaPanelOld(final List<String> labels) {
		// Set default labels.
		if (labels == null) {
			this.targetLabels = new ArrayList<String>();
			this.targetLabels.add("");
		} else {
			this.targetLabels = new ArrayList<String>(labels.size() + 1);
			this.targetLabels.add("");
			this.targetLabels.addAll(labels);
		}

		setBorder(new TitledBorder(null, "Partition Criteria", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		JPanel panelGenericData2 = new JPanel();
		panelGenericData2.setAlignmentX(Component.LEFT_ALIGNMENT);
		add(panelGenericData2);
		panelGenericData2.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, },
				new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblLabel = new JLabel("Label:");
		panelGenericData2.add(lblLabel, "2, 2");

		this.comboBoxLabel = new JComboBox(this.targetLabels.toArray());
		panelGenericData2.add(this.comboBoxLabel, "4, 2");
		this.comboBoxLabel.setSelectedIndex(0);
		this.comboBoxLabel.setMaximumRowCount(12);
		this.comboBoxLabel.setEditable(true);

		JLabel lblParameter = new JLabel("Parameter:");
		panelGenericData2.add(lblParameter, "6, 2");

		this.txtfldParameter = new JTextField();
		panelGenericData2.add(this.txtfldParameter, "8, 2");
		this.txtfldParameter.setColumns(10);

		Component verticalStrut = Box.createVerticalStrut(10);
		add(verticalStrut);

		JPanel panel_2 = new JPanel();
		panel_2.setAlignmentX(Component.LEFT_ALIGNMENT);
		add(panel_2);
		panel_2.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblType = new JLabel("Type:");
		panel_2.add(lblType, "2, 2, left, top");

		JPanel panelPartitionType = new JPanel();
		panelPartitionType.setAlignmentX(Component.LEFT_ALIGNMENT);
		add(panelPartitionType);
		panelPartitionType.setLayout(new BoxLayout(panelPartitionType, BoxLayout.X_AXIS));

		JPanel panelPartitionTypes = new JPanel();
		panelPartitionTypes.setAlignmentY(Component.TOP_ALIGNMENT);
		panelPartitionType.add(panelPartitionTypes);
		panelPartitionTypes.setLayout(new BoxLayout(panelPartitionTypes, BoxLayout.Y_AXIS));

		JRadioButton rdbtnRaw = new JRadioButton("Raw");
		this.buttonGroupType.add(rdbtnRaw);
		rdbtnRaw.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				display(PartitionType.RAW);
			}
		});
		rdbtnRaw.setSelected(true);
		panelPartitionTypes.add(rdbtnRaw);

		JRadioButton rdbtnBinarization = new JRadioButton("Binarization");
		this.buttonGroupType.add(rdbtnBinarization);
		rdbtnBinarization.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				display(PartitionType.BINARIZATION);
			}
		});
		panelPartitionTypes.add(rdbtnBinarization);

		JRadioButton rdbtnFreeGrouping = new JRadioButton("Free Grouping");
		this.buttonGroupType.add(rdbtnFreeGrouping);
		rdbtnFreeGrouping.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				display(PartitionType.FREE_GROUPING);
			}
		});
		panelPartitionTypes.add(rdbtnFreeGrouping);

		JRadioButton rdbtnCountedGrouping = new JRadioButton("Counted Grouping");
		this.buttonGroupType.add(rdbtnCountedGrouping);
		rdbtnCountedGrouping.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				display(PartitionType.COUNTED_GROUPING);
			}
		});
		panelPartitionTypes.add(rdbtnCountedGrouping);

		JRadioButton rdbtnSizedGrouping = new JRadioButton("Sized Grouping");
		this.buttonGroupType.add(rdbtnSizedGrouping);
		rdbtnSizedGrouping.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				display(PartitionType.SIZED_GROUPING);
			}
		});
		panelPartitionTypes.add(rdbtnSizedGrouping);

		JPanel panel = new JPanel();
		panel.setAlignmentY(Component.TOP_ALIGNMENT);
		panelPartitionType.add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		this.panelRaw = new JPanel();
		panel.add(this.panelRaw);

		this.panelBinarization = new JPanel();
		panel.add(this.panelBinarization);
		this.panelBinarization.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel label_1 = new JLabel(" ");
		this.panelBinarization.add(label_1, "2, 2");

		JLabel lblPattern = new JLabel("Pattern:");
		this.panelBinarization.add(lblPattern, "2, 4, right, default");

		this.txtfldPattern = new JTextField();
		this.panelBinarization.add(this.txtfldPattern, "4, 4, fill, default");
		this.txtfldPattern.setColumns(10);

		this.panelFreeGrouping = new JPanel();
		panel.add(this.panelFreeGrouping);
		this.panelFreeGrouping.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblNewLabel = new JLabel(" ");
		this.panelFreeGrouping.add(lblNewLabel, "2, 2");

		JLabel label = new JLabel(" ");
		this.panelFreeGrouping.add(label, "2, 4");

		JLabel lblSteps = new JLabel("Steps:");
		this.panelFreeGrouping.add(lblSteps, "2, 6, right, default");

		this.txtfldSteps = new JTextField();
		this.panelFreeGrouping.add(this.txtfldSteps, "4, 6, fill, default");
		this.txtfldSteps.setColumns(10);

		JLabel lblExample = new JLabel("e.g. 1990 1993 2000");
		this.panelFreeGrouping.add(lblExample, "4, 8");

		this.panelCountedGrouping = new JPanel();
		panel.add(this.panelCountedGrouping);
		this.panelCountedGrouping.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel label_4 = new JLabel("    ");
		this.panelCountedGrouping.add(label_4, "2, 2");

		JLabel lblStart = new JLabel("Start:");
		this.panelCountedGrouping.add(lblStart, "2, 4, right, default");

		this.txtfldCountedStart = new JTextField();
		this.panelCountedGrouping.add(this.txtfldCountedStart, "4, 4, fill, default");
		this.txtfldCountedStart.setColumns(10);

		JLabel lblCount = new JLabel("Count:");
		this.panelCountedGrouping.add(lblCount, "2, 6, right, default");

		this.txtfldCountedCount = new JTextField();
		this.panelCountedGrouping.add(this.txtfldCountedCount, "4, 6, fill, default");
		this.txtfldCountedCount.setColumns(10);

		JLabel lblEnd = new JLabel("End:");
		this.panelCountedGrouping.add(lblEnd, "2, 8, right, default");

		this.txtfldCountedEnd = new JTextField();
		this.panelCountedGrouping.add(this.txtfldCountedEnd, "4, 8, fill, default");
		this.txtfldCountedEnd.setColumns(10);

		this.panelSizedGrouping = new JPanel();
		panel.add(this.panelSizedGrouping);
		this.panelSizedGrouping.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel label_2 = new JLabel("    ");
		this.panelSizedGrouping.add(label_2, "2, 2");

		JLabel lblStart_1 = new JLabel("Start:");
		this.panelSizedGrouping.add(lblStart_1, "2, 4, right, default");

		this.txtfldSizedStart = new JTextField();
		this.panelSizedGrouping.add(this.txtfldSizedStart, "4, 4, fill, default");
		this.txtfldSizedStart.setColumns(10);

		JLabel lblSize = new JLabel("Size:");
		this.panelSizedGrouping.add(lblSize, "2, 6, right, default");

		this.txtfldSizedSize = new JTextField();
		this.panelSizedGrouping.add(this.txtfldSizedSize, "4, 6, fill, default");
		this.txtfldSizedSize.setColumns(10);

		JLabel lblEnd_1 = new JLabel("End:");
		this.panelSizedGrouping.add(lblEnd_1, "2, 8, right, default");

		this.txtfldSizedEnd = new JTextField();
		this.panelSizedGrouping.add(this.txtfldSizedEnd, "4, 8, fill, default");
		this.txtfldSizedEnd.setColumns(10);

		JPanel panelFamilyScope = new JPanel();
		panelFamilyScope.setAlignmentX(Component.LEFT_ALIGNMENT);
		add(panelFamilyScope);
		panelFamilyScope.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, }));

		JLabel lblFamilyScope = new JLabel("Family Scope:");
		panelFamilyScope.add(lblFamilyScope, "2, 2");

		this.rdbtnSome = new JRadioButton("Some");
		this.buttonGroupScope.add(this.rdbtnSome);
		panelFamilyScope.add(this.rdbtnSome, "4, 2");
		this.rdbtnSome.setSelected(true);

		this.rdbtnAll = new JRadioButton("All");
		this.buttonGroupScope.add(this.rdbtnAll);
		panelFamilyScope.add(this.rdbtnAll, "6, 2");

		JPanel panel_1 = new JPanel();
		panel_1.setAlignmentX(Component.LEFT_ALIGNMENT);
		add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));

		// =========================================
		display(PartitionType.RAW);

		if (defaultCriteria != null) {
			this.comboBoxLabel.setModel(new DefaultComboBoxModel(this.targetLabels.toArray()));
			if (defaultCriteria.getLabel() != null) {
				int index = this.targetLabels.indexOf(defaultCriteria.getLabel());
				if (index != -1) {
					this.comboBoxLabel.setSelectedIndex(index);
				}
			}

			//
			if (defaultCriteria.getLabelParameter() != null) {
				this.txtfldParameter.setText(defaultCriteria.getLabelParameter());
			}

			//
			if (defaultCriteria.getType() != null) {
				//
				display(defaultCriteria.getType());

				//
				switch (defaultCriteria.getType()) {
					case RAW:
						rdbtnRaw.setSelected(true);
					break;

					case BINARIZATION:
						rdbtnBinarization.setSelected(true);

						if (defaultCriteria.getPattern() != null) {
							this.txtfldPattern.setText(defaultCriteria.getPattern());
						}
					break;

					case FREE_GROUPING:
						rdbtnFreeGrouping.setSelected(true);

						if (defaultCriteria.getIntervals() != null) {
							this.txtfldSteps.setText(defaultCriteria.getIntervals().toBasicStepString());
						}
					break;

					case COUNTED_GROUPING:
						rdbtnCountedGrouping.setSelected(true);

						if (defaultCriteria.getStart() != null) {
							this.txtfldCountedStart.setText(MathUtils.toString(defaultCriteria.getStart()));
						}

						if (defaultCriteria.getCount() != null) {
							this.txtfldCountedCount.setText(MathUtils.toString(defaultCriteria.getCount()));
						}

						if (defaultCriteria.getEnd() != null) {
							this.txtfldCountedEnd.setText(MathUtils.toString(defaultCriteria.getEnd()));
						}
					break;

					case SIZED_GROUPING:
						rdbtnSizedGrouping.setSelected(true);

						if (defaultCriteria.getStart() != null) {
							this.txtfldSizedStart.setText(MathUtils.toString(defaultCriteria.getStart()));
						}

						if (defaultCriteria.getSize() != null) {
							this.txtfldSizedSize.setText(MathUtils.toString(defaultCriteria.getSize()));
						}

						if (defaultCriteria.getEnd() != null) {
							this.txtfldSizedEnd.setText(MathUtils.toString(defaultCriteria.getEnd()));
						}
					break;
				}
			}

			//
			if (defaultCriteria.getFamilyScope() == FamilyScope.SOME) {
				this.rdbtnSome.setSelected(true);
				this.rdbtnAll.setSelected(false);
			} else {
				this.rdbtnAll.setSelected(true);
				this.rdbtnSome.setSelected(false);
			}

		}
	}

	/**
	 * 
	 */
	public void clear() {
		this.targetLabels.set(0, "");
		this.comboBoxLabel.setModel(new DefaultComboBoxModel(this.targetLabels.toArray()));
	}

	/**
	 * 
	 * @param type
	 */
	public void display(final PartitionType type) {
		//
		this.panelRaw.setVisible(false);
		this.panelBinarization.setVisible(false);
		this.panelFreeGrouping.setVisible(false);
		this.panelCountedGrouping.setVisible(false);
		this.panelSizedGrouping.setVisible(false);

		//
		switch (type) {
			case RAW:
				this.panelRaw.setVisible(true);
			break;

			case BINARIZATION:
				this.panelBinarization.setVisible(true);
			break;

			case FREE_GROUPING:
				this.panelFreeGrouping.setVisible(true);
			break;

			case COUNTED_GROUPING:
				this.panelCountedGrouping.setVisible(true);
			break;

			case SIZED_GROUPING:
				this.panelSizedGrouping.setVisible(true);
			break;
		}
	}

	/**
	 * 
	 * @return
	 */
	public PartitionCriteria getCriteria() {
		PartitionCriteria result;

		//
		String label = (String) this.comboBoxLabel.getSelectedItem();

		//
		String parameter = this.txtfldParameter.getText();

		//
		if (this.panelRaw.isVisible()) {
			result = PartitionCriteria.createRaw(label, parameter);
		} else if (this.panelBinarization.isVisible()) {
			result = PartitionCriteria.createBinarization(label, this.txtfldParameter.getText(), this.txtfldPattern.getText());
		} else if (this.panelFreeGrouping.isVisible()) {
			result = PartitionCriteria.createGrouping(label, this.txtfldParameter.getText(), this.txtfldSteps.getText());
		} else if (this.panelCountedGrouping.isVisible()) {
			//
			Double start;
			if (NumberUtils.isNumber(this.txtfldCountedStart.getText())) {
				start = Double.parseDouble(this.txtfldCountedStart.getText());
			} else {
				start = null;
			}

			//
			Double count;
			if (NumberUtils.isNumber(this.txtfldCountedCount.getText())) {
				count = Double.parseDouble(this.txtfldCountedCount.getText());
			} else {
				count = null;
			}

			//
			Double end;
			if (NumberUtils.isNumber(this.txtfldCountedEnd.getText())) {
				end = Double.parseDouble(this.txtfldCountedEnd.getText());
			} else {
				end = null;
			}

			//
			result = PartitionCriteria.createCountedGrouping(label, parameter, start, count, end);
		} else if (this.panelSizedGrouping.isVisible()) {
			//
			Double start;
			if (NumberUtils.isNumber(this.txtfldSizedStart.getText())) {
				start = Double.parseDouble(this.txtfldSizedStart.getText());
			} else {
				start = null;
			}

			//
			Double size;
			if (NumberUtils.isNumber(this.txtfldSizedSize.getText())) {
				size = Double.parseDouble(this.txtfldSizedSize.getText());
			} else {
				size = null;
			}

			//
			Double end;
			if (NumberUtils.isNumber(this.txtfldSizedEnd.getText())) {
				end = Double.parseDouble(this.txtfldSizedEnd.getText());
			} else {
				end = null;
			}

			//
			result = PartitionCriteria.createSizedGrouping(label, parameter, start, size, end);
		} else {
			result = null;
		}

		//
		if (this.rdbtnSome.isSelected()) {
			result.setFamilyScope(FamilyScope.SOME);
		} else {
			result.setFamilyScope(FamilyScope.ALL);
		}

		//
		defaultCriteria = result;

		//
		return result;
	}
}
