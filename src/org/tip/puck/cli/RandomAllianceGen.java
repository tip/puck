package org.tip.puck.cli;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.tip.puck.alliancenets.AllianceNet;
import org.tip.puck.alliancenets.RandomGen;
import org.tip.puck.alliancenets.TopologicalIndices;


public class RandomAllianceGen extends Command {

    @Override
    public boolean run(CommandLine cline) {
        if(!cline.hasOption("inet")) {
            setErrorMessage("input network file must be specified");
            return false;
        }
        
        if(!cline.hasOption("ocsv")) {
            setErrorMessage("output csv file must be specified");
            return false;
        }
        
        String netfile = cline.getOptionValue("inet");
        String csvfile = cline.getOptionValue("ocsv");
        
        System.out.println("target net: " + netfile);
        
        AllianceNet targNet = AllianceNet.load(netfile);
        TopologicalIndices targIndices = new TopologicalIndices(targNet);
        RandomGen gen = new RandomGen(targNet.getNodeCount(), targNet.getEdgeCount(), targIndices);
        gen.run();
        AllianceNet simNet = gen.getNet();
        TopologicalIndices simIndices = new TopologicalIndices(simNet);
        
        double fit = simIndices.distance(targIndices);
        
        System.out.println("targ:");
        System.out.println(targIndices);
        System.out.println("sim:");
        System.out.println(simIndices);
        
        System.out.println("fitness: " + fit);
        
        try {
        	FileWriter fwriter = new FileWriter(csvfile, false);
        	BufferedWriter writer = new BufferedWriter(fwriter);
			String str = "" + fit 
					+ "," + simIndices.getEndogamousPercentage()
	                + "," + simIndices.getNetworkConcentration()
	                + "," + simIndices.getEndogamicNetworkConcentration()
	                + "," + simIndices.getNetworkSymmetry()
	                + "," + simIndices.getParallels()
	                + "," + simIndices.getCrosses()
	                + "," + simIndices.getLoopTri()
	                + "," + simIndices.getTranTri();
        	writer.write(str);
			writer.close();
		}
        catch (IOException e) {
			e.printStackTrace();
		}
        
        return true;
    }
}