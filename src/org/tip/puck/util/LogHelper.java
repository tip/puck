package org.tip.puck.util;

import java.io.File;
import java.text.MessageFormat;

import org.tip.puck.census.workers.CensusCriteria;
import org.tip.puck.io.bar.BARAttributesLine;
import org.tip.puck.io.bar.BARTXTIndividualLine;
import org.tip.puck.io.bar.BARTXTLabelsLine;
import org.tip.puck.io.ged.GEDLine;
import org.tip.puck.io.paj.PAJLine;
import org.tip.puck.io.tip.TIPLine;
import org.tip.puck.net.Attribute;
import org.tip.puck.net.Attributes;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puckgui.RecentFiles;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 * 
 */
public class LogHelper extends LogHelperCore {

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final Attribute source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			result = MessageFormat.format("label={0},value={1}", source.getLabel(), source.getValue());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final Attributes source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			StringList buffer = new StringList();
			for (Attribute attribute : source) {
				buffer.append("[");
				buffer.append(toString(attribute));
				buffer.append("]");
				buffer.append(',');
			}
			if (!source.isEmpty()) {
				buffer.removeLast();
			}

			result = buffer.toString();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final BARAttributesLine source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			result = String.format("[id=%d,values=%s]", source.id(), toString(source.values()));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static String toString(final BARTXTIndividualLine source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			result = String.format("[id=%d,name=%s,gender=%c,fatherId=%d,motherId=%d,spouseIds=%s]", source.id(), source.name(), source.gender(),
					source.fatherId(), source.motherId(), toString(source.spouseIds()));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final BARTXTLabelsLine source) {
		String result;

		if (source == null) {

			result = "[null]";

		} else {

			result = source.toStringWithCommas();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final CensusCriteria source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			result = MessageFormat.format("pattern={0},filter={1},partitionLabel={2},closingRelation={3},crossSex={4},"
					+ "marriedOnly={5},circuitType={6},filiationType={7},restrictionType={8},siblingMode={9}," + "symmetryType={10}]", source.getPattern(),
					source.getFilter(), source.getClassificatoryLinking(), source.getClosingRelation(), source.isCrossSexChainsOnly(), source.isCouplesOnly(),
					source.getCircuitType(), source.getFiliationType(), source.getRestrictionType(), source.getSiblingMode(), source.getSymmetryType());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final GEDLine source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			result = String.format("[level=%d, ref=%s, tag=%s, value=%s]", source.getLevel(), source.getRef(), source.tag(), source.value());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final Individual source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			StringList buffer = new StringList();

			buffer.append("[");
			buffer.append("id=");
			buffer.append(source.getId());
			buffer.append(",name=");
			buffer.append(source.getName());
			buffer.append(",gender=");
			buffer.append(source.getGender().toString());
			buffer.append(",originFamily=");
			if (source.getOriginFamily() == null) {
				buffer.append("null");
			} else {
				buffer.append(source.getOriginFamily().getId());
			}
			buffer.append(",attributes=");
			buffer.append(toString(source.attributes()));

			result = buffer.toString();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param net
	 * @return
	 */
	public static String toString(final Net source) {
		String result;

		result = String.format("[label=%s,individuals=%d,families=%d]", source.getLabel(), source.individuals().size(), source.families().size());

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final org.tip.puck.io.ged.GEDBlock source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			StringBuffer buffer = new StringBuffer(192);

			buffer.append("#");
			buffer.append(source.size());
			buffer.append("{");
			int startSize = buffer.length();
			for (GEDLine subSource : source) {
				if (buffer.length() != startSize) {
					buffer.append(",");
				}
				buffer.append(toString(subSource));
			}
			buffer.append("}");
			result = buffer.toString();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final org.tip.puck.io.pl.PLLine source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			result = String.format("[label=%s, value1=%d, value2=%s]", source.label(), source.value1(), source.value2());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final PAJLine source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			result = String.format("[type=%s, relationCode=%d, cardinal=%d, label=%s, name=%s, shape=%s, sourceId=%d, targetId=%d, value=%s]",
					source.getType(), source.getRelationCode(), source.getCardinal(), source.getLabel(), source.getName(), source.getShape(),
					source.getSourceId(), source.getTargetId(), source.getValue());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final RecentFiles source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			StringBuffer buffer = new StringBuffer(512);
			buffer.append("#");
			buffer.append(source.size());
			buffer.append("{");
			int startSize = buffer.length();
			for (File file : source) {
				if (buffer.length() != startSize) {
					buffer.append(",");
				}

				buffer.append(file.getName());
			}
			buffer.append("}");
			result = buffer.toString();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString(final TIPLine source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			result = String
					.format("[lineType=%s, individualId=%d, gender=%s, name=%s, alterId=%d, relation=%s, propertyLabel=%s, propertyValue=%s, propertyPlace=%s, propertyDate=%s]",
							source.lineType(), source.individualId(), source.gender(), source.name(), source.alterId(), source.relation(),
							source.propertyLabel(), source.propertyValue(), source.propertyPlace(), source.propertyDate());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toString2(final org.tip.puck.io.ged.GEDBlock source) {
		String result;

		if (source == null) {
			result = "[null]";
		} else {
			StringBuffer buffer = new StringBuffer(192);

			for (int index = 0; index < source.size(); index++) {
				GEDLine subSource = source.get(index);

				buffer.append("#");
				buffer.append(index);
				buffer.append("/");
				buffer.append(source.size());
				buffer.append("{");
				buffer.append(toString(subSource));
				buffer.append("}\n");
			}
			buffer.append("}");
			result = buffer.toString();
		}

		//
		return result;
	}
}
