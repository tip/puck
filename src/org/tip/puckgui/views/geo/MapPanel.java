package org.tip.puckgui.views.geo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.ListSelectionModel;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FilenameUtils;
import org.geotools.data.DataUtilities;
import org.geotools.data.FeatureSource;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.FeatureCollection;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.renderer.lite.StreamingRenderer;
import org.geotools.styling.FeatureTypeStyle;
import org.geotools.styling.Fill;
import org.geotools.styling.Graphic;
import org.geotools.styling.LineSymbolizer;
import org.geotools.styling.Mark;
import org.geotools.styling.PointSymbolizer;
import org.geotools.styling.Stroke;
import org.geotools.styling.PolygonSymbolizer;
import org.geotools.styling.Rule;
import org.geotools.styling.SLDParser;
import org.geotools.styling.Style;
import org.geotools.styling.StyleFactory;
import org.geotools.styling.StyledLayer;
import org.geotools.styling.StyledLayerDescriptor;
import org.geotools.styling.UserLayer;
import org.geotools.swing.JMapPane;
import org.geotools.swing.MapPane;
import org.geotools.swing.action.InfoAction;
import org.geotools.swing.action.PanAction;
import org.geotools.swing.action.ZoomInAction;
import org.geotools.swing.action.ZoomOutAction;
import org.geotools.swing.event.MapMouseEvent;
import org.geotools.swing.event.MapMouseListener;
import org.geotools.swing.styling.JSimpleStyleDialog;
import org.geotools.swing.tool.ZoomInTool;
import org.geotools.swing.tool.ZoomOutTool;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.FilterFactory;
import org.tip.puck.geo.Place;
import org.tip.puck.geo.tools.GeotoolsUtils;
import org.tip.puck.graphs.Graph;
import org.tip.puck.io.gis.SIGFile;
import org.tip.puckgui.util.GenericFileFilter;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;


public class MapPanel extends JPanel {
	private static final long serialVersionUID = 3496316496852295919L;

	private enum Tools {
		DRAG,
		SELECT,
		ZOOMIN,
		ZOOMOUT
	}

	StyleFactory styleFactory = CommonFactoryFinder.getStyleFactory();

	FilterFactory filterFactory = CommonFactoryFinder.getFilterFactory();
	public MapContent mapContent = new MapContent();
	private JTable tablePoints, tableLinks;
	JList listLayers = null;

	DefaultListModel listModel = new DefaultListModel();

	public double lastDraggedX = 0, lastDraggedY = 0, initialXClicked = 0, initialYClicked = 0;
	public boolean firstClickOnDrag = false;

	Tools selectedTool = Tools.DRAG;

	List<JToggleButton> listToggleButtons = new ArrayList<JToggleButton>();

	ZoomInTool zit = new ZoomInTool();
	ZoomOutTool zot = new ZoomOutTool();

	String labelGraph = "";

	JLabel lblPathToLayers, lblAvailableLayers;

	JTabbedPane tabbedPane;
	JScrollPane pTablLinesGeoView, pTablPointsGeoView ;

	/**
	 * Create the panel.
	 */
	public MapPanel(final Graph<Place> source) {
		setLayout(new BorderLayout(0, 0));

		//		JMapFrame jmf = new JMapFrame();
		//		jmf.enableToolBar(true);

		final JMapPane mapPane = new JMapPane();

		// //////////////////////////////////
		ArrayList<DefaultFeatureCollection> collectionfeaturesReadyToBeDisplayed = GeotoolsUtils.getFeaturesForNodesPlaces(source);
		
		labelGraph = source.getLabel();

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		add(tabbedPane, BorderLayout.CENTER);

		JPanel pMap = new JPanel();
		tabbedPane.addTab("Viewer", null, pMap, null);
		pMap.setLayout(new BorderLayout(0, 0));

		JSplitPane splitPane = new JSplitPane();
		pMap.add(splitPane);

		JPanel panelRightMap = new JPanel();
		splitPane.setRightComponent(panelRightMap);
		panelRightMap.setLayout(new BorderLayout(0, 0));

		JMenuBar menuBarMap = new JMenuBar();
		panelRightMap.add(menuBarMap, BorderLayout.NORTH);

		final JToggleButton tglbtnDrag = new JToggleButton(new PanAction(mapPane));
		this.listToggleButtons.add(tglbtnDrag);
		tglbtnDrag.setIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/gis/mActionPan.png")));
		tglbtnDrag.setSelectedIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/gis/mActionPan_selected.png")));
		menuBarMap.add(tglbtnDrag);
		tglbtnDrag.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				MapPanel.this.selectedTool = Tools.DRAG;
				MapPanel.this.unselectToggleButtonsExcept(tglbtnDrag);
			}
		});

		final JToggleButton tglbtnSelect = new JToggleButton(new InfoAction(mapPane));
		this.listToggleButtons.add(tglbtnSelect);
		tglbtnSelect.setIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/gis/mActionSelect.png")));
		tglbtnSelect.setSelectedIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/gis/mActionSelect_selected.png")));
		menuBarMap.add(tglbtnSelect);
		tglbtnSelect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				MapPanel.this.selectedTool = Tools.SELECT;
				MapPanel.this.unselectToggleButtonsExcept(tglbtnSelect);
			}
		});

		final JToggleButton tglbtnZoomIn = new JToggleButton(new ZoomInAction(mapPane));
		this.listToggleButtons.add(tglbtnZoomIn);
		tglbtnZoomIn.setIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/gis/mActionZoomIn.png")));
		tglbtnZoomIn.setSelectedIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/gis/mActionZoomIn_selected.png")));
		menuBarMap.add(tglbtnZoomIn);
		tglbtnZoomIn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				MapPanel.this.selectedTool = Tools.ZOOMIN;
				MapPanel.this.unselectToggleButtonsExcept(tglbtnZoomIn);
			}
		});

		final JToggleButton tglbtnZoomOut = new JToggleButton(new ZoomOutAction(mapPane));
		this.listToggleButtons.add(tglbtnZoomOut);
		tglbtnZoomOut.setIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/gis/mActionZoomOut.png")));
		tglbtnZoomOut.setSelectedIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/gis/mActionZoomOut_selected.png")));
		menuBarMap.add(tglbtnZoomOut);
		tglbtnZoomOut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				MapPanel.this.selectedTool = Tools.ZOOMOUT;
				MapPanel.this.unselectToggleButtonsExcept(tglbtnZoomOut);
			}
		});

		JButton tglbtnCenterView = new JButton("");
		tglbtnCenterView.setIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/gis/mActionZoomFullExtent.png")));
		menuBarMap.add(tglbtnCenterView);
		tglbtnCenterView.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent arg0) {
				ReferencedEnvelope env = maximiseBoundsForLayers();
				mapPane.setDisplayArea(env);
				mapPane.repaint();
			}
		});

		JButton btExportShapefile = new JButton("");
		btExportShapefile.setIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/gis/mSaveShapefile.png")));
		menuBarMap.add(btExportShapefile);
		btExportShapefile.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent arg0) {
				// Export Shapefile
				prepareExportShapeFile();
			}
		});

		JButton btExportSVG = new JButton("");
		btExportSVG.setIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/gis/mSaveSVG.png")));
		menuBarMap.add(btExportSVG);
		btExportSVG.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(final ActionEvent arg0) {
				// Export SVG
				prepareExportSVG();
			}
		});

		JLabel statusBar = new JLabel("Status bar");
		panelRightMap.add(statusBar, BorderLayout.SOUTH);

		mapPane.addMouseListener(new MapMouseListener() {

			@Override
			public void onMouseWheelMoved(MapMouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onMouseReleased(MapMouseEvent arg0) {
				firstClickOnDrag = false;

			}

			@Override
			public void onMousePressed(MapMouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onMouseMoved(MapMouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onMouseExited(MapMouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onMouseEntered(MapMouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onMouseDragged(MapMouseEvent ev) {

				DirectPosition2D pos = ev.getWorldPos();
				Coordinate coord = new Coordinate(pos.x, pos.y);

				ReferencedEnvelope env = mapPane.getDisplayArea();

				if(!firstClickOnDrag){
					MapPanel.this.initialXClicked = pos.x;
					MapPanel.this.initialYClicked = pos.y;
					firstClickOnDrag = true;
				}

			}

			@Override
			public void onMouseClicked(MapMouseEvent ev) {

				DirectPosition2D pos = ev.getWorldPos();
				Coordinate coord = new Coordinate(pos.x, pos.y);

				ReferencedEnvelope env = mapPane.getDisplayArea();

				// ev.getButton() == MouseEvent.

				switch (ev.getButton()) {

				case MouseEvent.BUTTON1:

					switch (MapPanel.this.selectedTool) {
					case DRAG:

						env = MapPanel.this.centerOn(env, coord);
						break;

					default:
						break;
					}

					break;

				case MouseEvent.BUTTON2:

					env = MapPanel.this.centerOn(env, coord);

					break;

				case MouseEvent.BUTTON3:

					break;

				}

				MapPanel.this.lastDraggedX = pos.x;
				MapPanel.this.lastDraggedY = pos.y;

				MapPanel.this.initialXClicked = pos.x;
				MapPanel.this.initialYClicked = pos.y;

				mapPane.setDisplayArea(env);
				mapPane.repaint();

			}
		});

		mapPane.addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseWheelMoved(final MouseWheelEvent arg0) {

				ReferencedEnvelope env = mapPane.getDisplayArea();

				double clickToZoom = 0.03;
				int clicks = arg0.getWheelRotation();
				// -ve means wheel moved up, +ve means down
				int sign = (clicks < 0 ? -1 : 1);

				double width = env.getWidth();
				double delta = width * clickToZoom * sign;

				env.expandBy(delta, delta);
				mapPane.setDisplayArea(env);
				mapPane.repaint();
			}
		});

		panelRightMap.add(mapPane, BorderLayout.CENTER);
		//		panelRightMap.add(jmf, BorderLayout.CENTER);

		JPanel panelLeftList = new JPanel();
		splitPane.setLeftComponent(panelLeftList);
		panelLeftList.setLayout(new BorderLayout(0, 0));

		JMenuBar menuBarLayers = new JMenuBar();
		panelLeftList.add(menuBarLayers, BorderLayout.NORTH);

		JButton btnAddShapeLayer = new JButton("");
		btnAddShapeLayer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {

				JFileChooser inFileDlg = new JFileChooser("user.dir"); //$NON-NLS-1$

				inFileDlg.setFileFilter(new FileNameExtensionFilter("Shapefile (*.shp)", "shp")); //$NON-NLS-1$ //$NON-NLS-2$
				int returnVal = inFileDlg.showOpenDialog(MapPanel.this);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					String browseDir = inFileDlg.getCurrentDirectory().toString();

					String inFile = null;
					inFile = inFileDlg.getSelectedFile().toString();
					inFileDlg.setCurrentDirectory(new File(inFile));

					try {

						displaysShapefile(new File(inFile));

					} catch (IOException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}

				}

			}
		});
		btnAddShapeLayer.setIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/add-16x16.png")));
		menuBarLayers.add(btnAddShapeLayer);

		JButton btnRemoveShapeLayer = new JButton("");
		btnRemoveShapeLayer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {

				String selectedLayerTitle = (String) MapPanel.this.listLayers.getSelectedValue();

				Layer selectedLayer = null;
				// Get selected layer
				for (Layer iterLayer : MapPanel.this.mapContent.layers()) {
					if (iterLayer.getTitle().compareTo(selectedLayerTitle) == 0) {
						selectedLayer = iterLayer;
						break;
					}
				}

				MapPanel.this.mapContent.removeLayer(selectedLayer);
				MapPanel.this.listModel.removeElement(selectedLayerTitle);

				mapPane.repaint();
			}
		});
		btnRemoveShapeLayer.setIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/remove-16x16.png")));
		menuBarLayers.add(btnRemoveShapeLayer);

		JButton btnLayerProperties = new JButton("");
		btnLayerProperties.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {

				String selectedLayerTitle = (String) MapPanel.this.listLayers.getSelectedValue();

				Layer selectedLayer = null;
				// Get selected layer
				for (Layer iterLayer : MapPanel.this.mapContent.layers()) {
					if (iterLayer.getTitle().compareTo(selectedLayerTitle) == 0) {
						selectedLayer = iterLayer;
						break;
					}
				}

				if (selectedLayer != null) {
					// Open style dialog
					Style newStyle = JSimpleStyleDialog.showDialog(null, (SimpleFeatureType) selectedLayer.getFeatureSource().getSchema());
					((FeatureLayer) selectedLayer).setStyle(newStyle);
					mapPane.repaint();

				}
			}
		});
		btnLayerProperties.setIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/gis/mParametersLayer.png")));
		menuBarLayers.add(btnLayerProperties);

		JButton btnLayerUp = new JButton("");
		btnLayerUp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {

				String selectedLayerTitle = (String) MapPanel.this.listLayers.getSelectedValue();
				int selectedIndex = MapPanel.this.listLayers.getSelectedIndex();

				Layer selectedLayer = null;
				// Get selected layer
				int count = 0;
				for (Layer iterLayer : MapPanel.this.mapContent.layers()) {
					if (iterLayer.getTitle().compareTo(selectedLayerTitle) == 0) {
						selectedLayer = iterLayer;
						break;
					}
					count++;
				}

				// Calc new position
				int newPosition = count + 1;
				System.out.println(MapPanel.this.mapContent.layers().size());
				System.out.println(MapPanel.this.listModel.getSize());
				if (newPosition < MapPanel.this.listModel.getSize()) {

					MapPanel.this.mapContent.moveLayer(count, newPosition);
					MapPanel.this.listModel.remove(selectedIndex);
					if (selectedIndex - 1 < 0) {
						selectedIndex = 0;
					}
					MapPanel.this.listModel.insertElementAt(selectedLayerTitle, selectedIndex - 1);

					MapPanel.this.listLayers.setSelectedIndex(selectedIndex - 1);

					mapPane.repaint();

				}

			}
		});
		btnLayerUp.setIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/up-16x16.png")));
		menuBarLayers.add(btnLayerUp);

		JButton btnLayerDown = new JButton("");
		btnLayerDown.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {

				String selectedLayerTitle = (String) MapPanel.this.listLayers.getSelectedValue();
				int selectedIndex = MapPanel.this.listLayers.getSelectedIndex();

				Layer selectedLayer = null;
				// Get selected layer
				int count = 0;
				for (Layer iterLayer : MapPanel.this.mapContent.layers()) {
					if (iterLayer.getTitle().compareTo(selectedLayerTitle) == 0) {
						selectedLayer = iterLayer;
						break;
					}
					count++;
				}

				// Calc new position
				int newPosition = count - 1;
				if (newPosition > -1) {
					// newPosition = listModel.getSize()-1;

					MapPanel.this.mapContent.moveLayer(count, newPosition);
					MapPanel.this.listModel.remove(selectedIndex);
					if (selectedIndex + 1 > MapPanel.this.listModel.getSize()) {
						selectedIndex = MapPanel.this.listModel.getSize() - 1;
					}
					MapPanel.this.listModel.insertElementAt(selectedLayerTitle, selectedIndex + 1);

					mapPane.repaint();

					MapPanel.this.listLayers.setSelectedIndex(selectedIndex + 1);

				}
			}
		});
		btnLayerDown.setIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/down-16x16.png")));
		menuBarLayers.add(btnLayerDown);

		this.listLayers = new JList(this.listModel);
		panelLeftList.add(this.listLayers, BorderLayout.CENTER);

		JPanel pInformation = new JPanel();
		tabbedPane.addTab("Layers Information", null, pInformation, null);
		pInformation.setLayout(null);

		JLabel lblCartoTypeFixed = new JLabel("Cartography type :");
		lblCartoTypeFixed.setBounds(12, 39, 143, 15);
		pInformation.add(lblCartoTypeFixed);

		JLabel lblAvailableLayersFixed = new JLabel("Available layers :");
		lblAvailableLayersFixed.setBounds(12, 66, 155, 15);
		pInformation.add(lblAvailableLayersFixed);

		JLabel lblCorpusNameFixed = new JLabel("Corpus Name :");
		lblCorpusNameFixed.setBounds(12, 12, 117, 15);
		pInformation.add(lblCorpusNameFixed);

		JLabel lblDateFixed = new JLabel("Date :");
		lblDateFixed.setBounds(12, 93, 66, 15);
		pInformation.add(lblDateFixed);

		JLabel lblSavedLayersFixed = new JLabel("Layers saved at :");
		lblSavedLayersFixed.setBounds(12, 120, 117, 15);
		pInformation.add(lblSavedLayersFixed);

		JLabel lblCorpus = new JLabel(labelGraph.substring(0, labelGraph.lastIndexOf(".")));
		lblCorpus.setBounds(200, 12, 500, 15);
		pInformation.add(lblCorpus);

		JLabel lblCartoType = new JLabel(labelGraph.substring(labelGraph.lastIndexOf(".")+5));
		lblCartoType.setBounds(200, 39, 500, 15);
		pInformation.add(lblCartoType);

		lblAvailableLayers = new JLabel("ListOfLayers");
		lblAvailableLayers.setBounds(200, 66, 500, 15);
		pInformation.add(lblAvailableLayers);

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
		JLabel lblDate = new JLabel(dateFormat.format(date));
		lblDate.setBounds(200, 93, 300, 15);
		pInformation.add(lblDate);

		lblPathToLayers = new JLabel("/path/to/layers");
		lblPathToLayers.setBounds(200, 120, 500, 15);
		pInformation.add(lblPathToLayers);

		JLabel lblSCRFixed = new JLabel("SCR :");
		lblSCRFixed.setBounds(12, 147, 66, 15);
		pInformation.add(lblSCRFixed);

		JLabel lblSCR = new JLabel("WGS84 - (4326)");
		lblSCR.setBounds(200, 147, 117, 15);
		pInformation.add(lblSCR);

		// Map content
		mapPane.setRenderer(new StreamingRenderer());
		// GTRenderer renderer = new StreamingRenderer();

		mapPane.setMapContent(this.mapContent);

		pTablPointsGeoView = new JScrollPane();
		//		scrollpanePointsGeoView = new JScrollPane(pTablPointsGeoView);
		tabbedPane.addTab("Attributes table points", null, pTablPointsGeoView, null);
		//		pTablPointsGeoView.setLayout(new BorderLayout(0, 0));

		pTablLinesGeoView = new JScrollPane();
		tabbedPane.addTab("Attributes table links", null, pTablLinesGeoView, null);
		//		pTablLinesGeoView.setLayout(new BorderLayout(0, 0));

		// JLabel lblStatus = new JLabel("Statusbar");
		// pTableGeoView.add(lblStatus, BorderLayout.SOUTH);

		JMenuBar menuBarPoints = new JMenuBar();
		JMenuBar menuBarLinks = new JMenuBar();
		pTablPointsGeoView.add(menuBarPoints);
		pTablLinesGeoView.add(menuBarLinks);

		JButton btnAddPoint = new JButton("");
		btnAddPoint.setIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/add-16x16.png")));
		menuBarPoints.add(btnAddPoint);

		JButton btnDelPoint = new JButton("");
		btnDelPoint.setIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/remove-16x16.png")));
		menuBarPoints.add(btnDelPoint);

		JButton btnAddLinks = new JButton("");
		btnAddLinks.setIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/add-16x16.png")));
		menuBarLinks.add(btnAddLinks);

		JButton btnDelLinks = new JButton("");
		btnDelLinks.setIcon(new ImageIcon(MapPanel.class.getResource("/org/tip/puckgui/images/remove-16x16.png")));
		menuBarLinks.add(btnDelLinks);

		//Build Model for attribute table points
		this.tablePoints = new JTable();
		this.tablePoints.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		String[] colNameNodes;
		if( labelGraph.contains("Net Places") || labelGraph.contains("Net_Places")){
			colNameNodes = new String[]{ "id_geonames", "Toponym", "Country", "Long", "Lat", "Weight"};
		}else{
			colNameNodes = new String[]{ "id_geonames", "Toponym", "Country", "Long", "Lat" };
		}

		DefaultTableModel modelAttributesTablePoints = (DefaultTableModel) this.tablePoints.getModel();
		modelAttributesTablePoints.setColumnIdentifiers(colNameNodes);

		this.tablePoints.setModel(modelAttributesTablePoints);

		//Build Model for attribute table links
		this.tableLinks = new JTable();
		this.tableLinks.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		String[] colNameLinks = { "Toponym_Origin", "Toponym_Destination", "Weight", "Long_Origin", "Lat_Origin", "Long_Destination", "Lat_Destination" };

		DefaultTableModel modelAttributesTableLinks = (DefaultTableModel) this.tableLinks.getModel();
		modelAttributesTableLinks.setColumnIdentifiers(colNameLinks);

		this.tableLinks.setModel(modelAttributesTableLinks);

		JScrollPane scrollPaneTablePoints = new JScrollPane();
		pTablPointsGeoView.setViewportView(scrollPaneTablePoints);

		scrollPaneTablePoints.setViewportView(tablePoints);

		JScrollPane scrollPaneTableLinks = new JScrollPane();
		pTablLinesGeoView.setViewportView(scrollPaneTableLinks);

		scrollPaneTableLinks.setViewportView(tableLinks);

		try {
			displayWorldBorderShapefile();
		} catch (Exception e) {
			e.printStackTrace();
		}

		diplayCollectionsPointsAndLines(collectionfeaturesReadyToBeDisplayed);

	}

	private DirectPosition2D calculateWorldPos(final MapPane pane, final MouseEvent event) {
		AffineTransform tr = pane.getScreenToWorldTransform();
		DirectPosition2D pos = new DirectPosition2D(event.getX(), event.getY());
		tr.transform(pos, pos);
		pos.setCoordinateReferenceSystem(pane.getMapContent().getCoordinateReferenceSystem());
		return pos;
	}

	private ReferencedEnvelope centerOn(final ReferencedEnvelope env, final Coordinate coordClicked) {
		Coordinate pEnv = env.centre();
		double dX = -(pEnv.x - coordClicked.x);
		double dY = -(pEnv.y - coordClicked.y);
		env.translate(dX, dY);

		return env;

	}

	/**
	 * Create a Style object from a definition in a SLD file document
	 */
	private Style createFromSLD(final File sld) {

		try {
			createFromSLD2(sld.toURI().toURL());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Create a Style object from a definition in a SLD document url
	 */
	private Style createFromSLD2(final URL sld) {
		try {
			SLDParser stylereader = new SLDParser(styleFactory, sld.toURI().toURL());
			Style[] style = stylereader.readXML();
			return style[0];

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Create a Style to display the features. If an SLD file is in the same
	 * directory as the shapefile then we will create the Style by processing
	 * this. Otherwise we display a JSimpleStyleDialog to prompt the user for
	 * preferences.
	 */
	private Style createStyle(final File file, final FeatureSource<?, ?> featureSource) {
		File sld = toSLDFile(file);
		if (sld != null) {
			return createFromSLD(sld);
		}

		SimpleFeatureType schema = (SimpleFeatureType) featureSource.getSchema();
		return JSimpleStyleDialog.showDialog(null, schema);
	}

	/**
	 * Here is a programmatic alternative to using JSimpleStyleDialog to
	 * get a Style. This methods works out what sort of feature geometry
	 * we have in the shapefile and then delegates to an appropriate style
	 * creating method.
	 */
	private Style createStyle2(FeatureSource featureSource) {
		SimpleFeatureType schema = (SimpleFeatureType)featureSource.getSchema();
		Class geomType = schema.getGeometryDescriptor().getType().getBinding();

		if (Polygon.class.isAssignableFrom(geomType)
				|| MultiPolygon.class.isAssignableFrom(geomType)) {
			return createPolygonStyle();

		} else if (LineString.class.isAssignableFrom(geomType)
				|| MultiLineString.class.isAssignableFrom(geomType)) {
			return createLineStyle();

		} else {
			return createPointStyle();
		}
	}

	/**
	 * Create a Style to draw polygon features with a thin blue outline and
	 * a cyan fill
	 */
	private Style createPolygonStyle() {

		// create a partially opaque outline stroke
		Stroke stroke = styleFactory.createStroke(
				filterFactory.literal(Color.LIGHT_GRAY),
				filterFactory.literal(1),
				filterFactory.literal(0.5));

		// create a partial opaque fill
		Fill fill = styleFactory.createFill(
				filterFactory.literal(new Color(227, 198, 141)),
				filterFactory.literal(0.5));

		/*
		 * Setting the geometryPropertyName arg to null signals that we want to
		 * draw the default geomettry of features
		 */
		PolygonSymbolizer sym = styleFactory.createPolygonSymbolizer(stroke, fill, null);

		Rule rule = styleFactory.createRule();
		rule.symbolizers().add(sym);
		FeatureTypeStyle fts = styleFactory.createFeatureTypeStyle(new Rule[]{rule});
		Style style = styleFactory.createStyle();
		style.featureTypeStyles().add(fts);

		return style;
	}

	/**
	 * Create a Style to draw line features as thin blue lines
	 */
	private Style createLineStyle() {
		Stroke stroke = styleFactory.createStroke(
				filterFactory.literal(Color.BLUE),
				filterFactory.literal(1));

		/*
		 * Setting the geometryPropertyName arg to null signals that we want to
		 * draw the default geomettry of features
		 */
		LineSymbolizer sym = styleFactory.createLineSymbolizer(stroke, null);

		Rule rule = styleFactory.createRule();
		rule.symbolizers().add(sym);
		FeatureTypeStyle fts = styleFactory.createFeatureTypeStyle(new Rule[]{rule});
		Style style = styleFactory.createStyle();
		style.featureTypeStyles().add(fts);

		return style;
	}

	/**
	 * Create a Style to draw point features as circles with blue outlines
	 * and cyan fill
	 */
	private Style createPointStyle() {
		Graphic gr = styleFactory.createDefaultGraphic();

		Mark mark = styleFactory.getCircleMark();

		mark.setStroke(styleFactory.createStroke(
				filterFactory.literal(Color.BLUE), filterFactory.literal(1)));

		mark.setFill(styleFactory.createFill(filterFactory.literal(Color.CYAN)));

		gr.graphicalSymbols().clear();
		gr.graphicalSymbols().add(mark);
		gr.setSize(filterFactory.literal(5));

		/*
		 * Setting the geometryPropertyName arg to null signals that we want to
		 * draw the default geomettry of features
		 */
		PointSymbolizer sym = styleFactory.createPointSymbolizer(gr, null);

		Rule rule = styleFactory.createRule();
		rule.symbolizers().add(sym);
		FeatureTypeStyle fts = styleFactory.createFeatureTypeStyle(new Rule[]{rule});
		Style style = styleFactory.createStyle();
		style.featureTypeStyles().add(fts);

		return style;
	}

	public void diplayCollectionsPointsAndLines(final ArrayList<DefaultFeatureCollection> collectionfeaturesReadyToBeDisplayed) {

		try {

			// Display points
			FeatureSource<?, ?> featureSourcePoints = DataUtilities.source(collectionfeaturesReadyToBeDisplayed.get(0));
			Style stylePoints = createFromSLD2(MapPanel.class.getResource("/org/tip/puckgui/views/geo/data/SCHEMA_POINTS.sld"));
			FeatureLayer layerPoints = new FeatureLayer(featureSourcePoints, stylePoints);

			setUpTablePointsData(featureSourcePoints);

			int nbFeaturePoints = featureSourcePoints.getFeatures().size();

			ReferencedEnvelope bounds = layerPoints.getBounds();
			double dh = Math.abs(bounds.getHeight() - bounds.getHeight() * 1.1);
			double dw = Math.abs(bounds.getWidth() - bounds.getWidth() * 1.1);

			bounds.expandBy(Math.max(dh, dw));

			// Display lines
			FeatureSource<?, ?> featureSourceLines = DataUtilities.source(collectionfeaturesReadyToBeDisplayed.get(1));
			Style styleLines = createFromSLD2(MapPanel.class.getResource("/org/tip/puckgui/views/geo/data/SCHEMA_LINES.sld"));
			FeatureLayer layerLines = new FeatureLayer(featureSourceLines, styleLines);

			if( featureSourceLines.getFeatures().size() == 0 ) {
				MapPanel.this.tabbedPane.setEnabledAt(3, false);
			} else {
				MapPanel.this.tabbedPane.setEnabledAt(3, true);
			}

			setUpTableLinksData(featureSourceLines);

			int nbFeatureLines = featureSourceLines.getFeatures().size();

			if( nbFeaturePoints > 0) {
				this.mapContent.addLayer(layerPoints);
				layerPoints.setTitle("Points Layer");
				this.listModel.add(this.listModel.getSize() - 1, layerPoints.getTitle());
				lblAvailableLayers.setText(lblAvailableLayers.getText() + ", " + layerPoints.getTitle()); 
			}

			if( nbFeatureLines > 0) {
				this.mapContent.addLayer(layerLines);
				layerLines.setTitle("Links Layer");
				this.listModel.add(this.listModel.getSize() - 1, layerLines.getTitle());
				lblAvailableLayers.setText(lblAvailableLayers.getText() + ", " + layerLines.getTitle()); 
			}

			this.mapContent.getViewport().setBounds(bounds);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void displayFeatureCollectionLayer(final String title, final FeatureCollection<?, ?> featureCollection, final Style style) {

		FeatureLayer layer = new FeatureLayer(featureCollection, style);
		this.mapContent.addLayer(layer);
		layer.setTitle(title);
		this.listModel.addElement(title);
	}

	public void displayFeatureSourceLayer(final String title, final FeatureSource<?, ?> featureSource, final Style style) {

		FeatureLayer layer = new FeatureLayer(featureSource, style);
		this.mapContent.addLayer(layer);
		layer.setTitle(title);
		this.listModel.addElement(title);

	}

	private void displaysShapefile(final File file) throws IOException {

		FileDataStore store = FileDataStoreFinder.getDataStore(file);
		FeatureSource<?, ?> featureSource = store.getFeatureSource();

		// Create a basic Style to render the features
		Style style = createStyle(file, featureSource);

		// Add the features and the associated Style object to
		// the MapContext as a new MapLayer
		displayFeatureSourceLayer(file.getName(), featureSource, style);

	}

	private void displaysShapefile(final URL urlSHP, final URL urlStyle) throws IOException {

		// File file = new File(url.getPath());

		System.out.println("create style");
		System.out.println(urlStyle.getPath());
		Style style = createFromSLD(new File(urlStyle.getPath()));
		System.out.println(style.toString());

		FileDataStore store = FileDataStoreFinder.getDataStore(urlSHP);
		FeatureSource<?, ?> featureSource = store.getFeatureSource();

		// Create a basic Style to render the features
		// Style style = createStyle(file, featureSource);

		// Add the features and the associated Style object to
		// the MapContext as a new MapLayer

		String fileName = urlSHP.toString().substring(urlSHP.toString().lastIndexOf("/")+1, urlSHP.toString().lastIndexOf("."));

		displayFeatureSourceLayer(fileName, featureSource, style);

	}

	private void displaysShapefile2(final URL urlSHP, final URL urlStyle) throws IOException {

		// File file = new File(url.getPath());

		System.out.println("create style");
		System.out.println("urlStyle.getpath=" + urlStyle.getPath());
		//		Style style = createFromSLD2(urlStyle);
		//		System.out.println("style=" + style);

		FileDataStore store = FileDataStoreFinder.getDataStore(urlSHP);
		FeatureSource<?, ?> featureSource = store.getFeatureSource();

		// Create a basic Style to render the features
		Style style = createStyle2(featureSource);

		// Add the features and the associated Style object to
		// the MapContext as a new MapLayer

		String fileName = urlSHP.toString().substring(urlSHP.toString().lastIndexOf("/") + 1, urlSHP.toString().lastIndexOf("."));

		displayFeatureSourceLayer(fileName, featureSource, style);

	}

	private void displayWorldBorderShapefile() throws IOException {

		// 06/05/2015: temporary fix for Eclipse under Microsoft Windows,
		// because %20 is not available in path.
		// URL fileWorldBorders =
		// MapPanel.class.getResource("/org/tip/puckgui/views/geo/data/TM_WORLD_BORDERS-0.3.shp");
		//
		// File file = new File(fileWorldBorders.getFile());

		// Convert path to separators / \ to system compliance
		//		String fileName = FilenameUtils.separatorsToSystem(MapPanel.class.getResource("/org/tip/puckgui/views/geo/data/TM_WORLD_BORDERS-0.3.shp").getFile());

		// With URL
		URL urlShapeFile = MapPanel.class.getResource("/org/tip/puckgui/views/geo/data/TM_WORLD_BORDERS-0.3.shp");
		URL urlStyle = MapPanel.class.getResource("/org/tip/puckgui/views/geo/data/TM_WORLD_BORDERS-0.3.sld");
		//		System.out.println(urlShapeFile);
		displaysShapefile2(urlShapeFile, urlStyle);

		lblAvailableLayers.setText("WORLD BORDERS"); 

	}

	public List<Layer> getLayers() {

		return this.mapContent.layers();

	}

	public ReferencedEnvelope maximiseBoundsForLayers() {

		double minX = Double.MAX_VALUE, minY = Double.MAX_VALUE;
		double maxX = Double.MIN_VALUE, maxY = Double.MIN_VALUE;

		for (Layer iterLayer : getLayers()) {

			if (iterLayer.getTitle().compareTo("TM_WORLD_BORDERS-0.3.shp") != 0) {

				ReferencedEnvelope bounds = iterLayer.getBounds();

				double minXTemp = bounds.getMinX();
				minX = Math.min(minX, minXTemp);
				double maxXTemp = bounds.getMaxX();
				maxX = Math.max(maxX, maxXTemp);

				double minYTemp = bounds.getMinY();
				minY = Math.min(minY, minYTemp);
				double maxYTemp = bounds.getMaxY();
				maxY = Math.max(maxY, maxYTemp);
			}
		}

		ReferencedEnvelope env = new ReferencedEnvelope();
		env.init(minX, maxX, minY, maxY);

		double dh = Math.abs(env.getHeight() - env.getHeight() * 1.1);
		double dw = Math.abs(env.getWidth() - env.getWidth() * 1.1);

		env.expandBy(Math.max(dh, dw));

		return env;

	}

	private void prepareExportShapeFile() {

		//Prepare ShapefileName
		//format demandé Ebrei_netplaces_20161128_points.shp

		String prefixFileName = MapPanel.this.labelGraph;

		//Prepare date format for export filename
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();
		String fileDate = dateFormat.format(date);

		File targetFile = new File(prefixFileName + "_" + fileDate + ".shp");
		boolean ended = false;
		while (!ended) {
			JFileChooser chooser = new JFileChooser();
			chooser.setSelectedFile(targetFile);

			chooser.setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.saveAsFileChooser.text"));
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			chooser.setAcceptAllFileFilterUsed(false);
			GenericFileFilter defaultFileFilter = new GenericFileFilter("Shapefile (*.shp)", "shp");
			chooser.addChoosableFileFilter(defaultFileFilter);
			chooser.setFileFilter(defaultFileFilter);

			//
			if (chooser.showSaveDialog(MapPanel.this) == JFileChooser.APPROVE_OPTION) {
				targetFile = chooser.getSelectedFile();

				boolean doSave;
				if (targetFile.exists()) {
					// Manage confirmation dialog.
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.title");
					String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.text");

					int response = JOptionPane.showConfirmDialog(MapPanel.this, message, title, JOptionPane.YES_NO_CANCEL_OPTION);

					if (response == JOptionPane.YES_OPTION) {
						doSave = true;
						ended = true;
					} else if (response == JOptionPane.NO_OPTION) {
						doSave = false;
						ended = false;
					} else {
						doSave = false;
						ended = true;
					}
				} else {
					doSave = true;
					ended = true;
				}

				if (doSave) {

					SIGFile.exportToShapefile(MapPanel.this.getLayers(), targetFile);
					lblPathToLayers.setText(targetFile.getPath());
				}

			} else {
				// Nothing to do.
				System.out.println("No Selection");
				ended = true;
			}
		}
	}

	private void prepareExportSVG() {

		//Prepare SVG Export
		//format demandé Ebrei_netplaces_20161128_points.shp

		String prefixFileName = MapPanel.this.labelGraph;

		//Prepare date format for export filename
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmm");
		Date date = new Date();
		String fileDate = dateFormat.format(date);

		File targetFile = new File(prefixFileName + "_" + fileDate + ".svg");

		OutputStream output;

		boolean ended = false;
		while (!ended) {
			JFileChooser chooser = new JFileChooser();
			chooser.setSelectedFile(targetFile);

			chooser.setDialogTitle(ResourceBundle.getBundle("org.tip.puckgui.messages").getString("MainWindow.saveAsFileChooser.text"));
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			chooser.setAcceptAllFileFilterUsed(false);
			GenericFileFilter defaultFileFilter = new GenericFileFilter("Shapefile (*.svg)", "svg");
			chooser.addChoosableFileFilter(defaultFileFilter);
			chooser.setFileFilter(defaultFileFilter);

			if (chooser.showSaveDialog(MapPanel.this) == JFileChooser.APPROVE_OPTION) {
				targetFile = chooser.getSelectedFile();

				boolean doSave;
				if (targetFile.exists()) {
					// Manage confirmation dialog.
					String title = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.title");
					String message = ResourceBundle.getBundle("org.tip.puckgui.messages").getString("dialog.saveFileConfirm.existingFile.text");

					int response = JOptionPane.showConfirmDialog(MapPanel.this, message, title, JOptionPane.YES_NO_CANCEL_OPTION);

					if (response == JOptionPane.YES_OPTION) {
						doSave = true;
						ended = true;
					} else if (response == JOptionPane.NO_OPTION) {
						doSave = false;
						ended = false;
					} else {
						doSave = false;
						ended = true;
					}
				} else {
					doSave = true;
					ended = true;
				}

				if (doSave) {

					try {

						output = new FileOutputStream(targetFile);

						ReferencedEnvelope env = maximiseBoundsForLayers();

						SIGFile.exportSVG(this.mapContent, env, output, null);

					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ParserConfigurationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			} else {
				// Nothing to do.
				System.out.println("No Selection ");
				ended = true;
			}
		}
	}

	public void setUpTablePointsData(final FeatureSource<?, ?> featureSourcePoints) {
		DefaultTableModel tableModelPoints = (DefaultTableModel) this.tablePoints.getModel();
		ArrayList<SimpleFeature> list = new ArrayList<SimpleFeature>();

		SimpleFeatureIterator iterator = null;
		try {
			iterator = (SimpleFeatureIterator) featureSourcePoints.getFeatures().features();

			while (iterator.hasNext()) {
				SimpleFeature feature = iterator.next();
				list.add(feature);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			iterator.close();
		};

		for (int index = 0; index < list.size(); index++) {

			SimpleFeature feature = list.get(index);

			Object[] data;
			if( labelGraph.contains("Net Places") || labelGraph.contains("Net Places")){
				data = new Object[6];
			}else{
				data = new Object[5];
			}
			if( feature.getAttribute(0) != null ){
				int ix_id = ((String)feature.getAttribute(0)).indexOf("id=");
				data[0] = ((String)feature.getAttribute(0)).substring(ix_id+3, ((String)feature.getAttribute(0)).indexOf(";")); // GeonameId
				int ix_admin = ((String)feature.getAttribute(0)).indexOf("admin=");
				data[2] = ((String)feature.getAttribute(0)).substring(ix_admin+6, ((String)feature.getAttribute(0)).indexOf("]"));// ISO
			} else {
				data[0] = "N/A";
				data[2] = "N/A";
			}
			data[1] = feature.getAttribute(1); // Name

			// data[?] = list.get(i).getGeonames1();
			// data[?] = list.get(i).getGeonames2();
			// data[?] = list.get(i).getGeonames3();
			
			Geometry g = (Geometry) feature.getAttribute("the_geom");
			data[3] = new Double(g.getCoordinate().x);
			data[4] = new Double(g.getCoordinate().y);
			
			if( labelGraph.contains("Net Places") || labelGraph.contains("Net_Places")){
				data[5] = (Double) feature.getAttribute("weight");
			}

			tableModelPoints.addRow(data);
		}
		this.tablePoints.setModel(tableModelPoints);
		tableModelPoints.fireTableDataChanged();
		this.tablePoints.repaint();
	}

	public void setUpTableLinksData(final FeatureSource<?, ?> featureSourceLinks) {

		//Test if links structure is empty
		try {
			if( featureSourceLinks.getFeatures().size() == 0)
				return;
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		DefaultTableModel tableModelLinks = (DefaultTableModel) this.tableLinks.getModel();
		ArrayList<SimpleFeature> list = new ArrayList<SimpleFeature>();

		SimpleFeatureIterator iterator = null;
		try {
			iterator = (SimpleFeatureIterator) featureSourceLinks.getFeatures().features();

			while (iterator.hasNext()) {
				SimpleFeature feature = iterator.next();
				list.add(feature);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			iterator.close();
		};

		//Format is
		//		{ "NameOrigin", "NameDestination", Weight, "Long_Origin", "Lat_Origin", "Long_Destination", "Lat_Destination" };

		for (int index = 0; index < list.size(); index++) {

			SimpleFeature feature = list.get(index);

			Object[] data = new Object[7];

			String OD = (String) feature.getAttribute(0);
			//Filter on comma and spaces
			OD = OD.replace(",", "").replace(" ","");

			String O = OD.substring(0, OD.indexOf("--"));
			String D = OD.substring(OD.indexOf("--")+2, OD.length());

			data[0] = O; // NameOrigin
			data[1] = D; // NameDestination

			// data[?] = list.get(i).getGeonames1();
			// data[?] = list.get(i).getGeonames2();
			// data[?] = list.get(i).getGeonames3();
			data[2] = feature.getAttribute(1); // Weight

			Geometry g = (Geometry) feature.getAttribute("the_geom");
			data[3] = new Double(g.getCoordinates()[0].x);
			data[4] = new Double(g.getCoordinates()[0].y);
			data[5] = new Double(g.getCoordinates()[g.getCoordinates().length-1].x);
			data[6] = new Double(g.getCoordinates()[g.getCoordinates().length-1].y);

			tableModelLinks.addRow(data);
		}
		this.tableLinks.setModel(tableModelLinks);
		tableModelLinks.fireTableDataChanged();
		this.tableLinks.repaint();
	}

	/**
	 * Figure out if a valid SLD file is available.
	 */
	public File toSLDFile(final File file) {
		String path = file.getAbsolutePath();
		String base = path.substring(0, path.length() - 4);
		String newPath = base + ".sld";
		File sld = new File(newPath);
		if (sld.exists()) {
			return sld;
		}
		newPath = base + ".SLD";
		sld = new File(newPath);
		if (sld.exists()) {
			return sld;
		}
		return null;
	}

	private void unselectToggleButtonsExcept(final JToggleButton tglbSelected) {
		for (JToggleButton iterTglb : this.listToggleButtons) {
			iterTglb.setSelected(false);
		}
		tglbSelected.setSelected(true);
	}
}