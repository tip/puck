/**
 * Copyright 2011 Christian P. MOMON (christian.momon@devinsy.fr).
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Adaptations by TIP.
 * 
 */
package org.tip.puck.util;

/**
 * @author Christian P. MOMON
 * @author TIP
 * 
 */
public class ExceptionUtil {

	/**
	 * Returns information about the calling class of a calledClass.
	 * 
	 * @param calledClassName
	 *            the class name which is the subject of the search.
	 * 
	 * @return information about the calling class.
	 */
	public static StackTraceElement getCaller(final String calledClassName) {
		StackTraceElement result;

		//
		StackTraceElement[] stack = Thread.currentThread().getStackTrace();
		// System.out.println("////////////////////////////");
		// for (int i = 0; (i < stack.length) && (i < 4); i++) {
		// System.out.println(i + " " + stack[i].getClassName());
		// }

		// Search for first entry of class called.
		boolean ended = false;
		Integer indexOfCalled = null;
		int depth = 1;
		while (!ended) {
			if (depth < stack.length) {
				String currentClassName = stack[depth].getClassName();
				if (currentClassName.equals(calledClassName)) {
					ended = true;
					indexOfCalled = Integer.valueOf(depth);
				} else {
					depth += 1;
				}
			} else {
				ended = true;
				result = null;
			}
		}

		// Search for caller of class called.
		if (indexOfCalled == null) {
			result = null;
		} else {
			result = null;
			ended = false;
			depth = indexOfCalled;
			while (!ended) {
				if (depth < stack.length) {
					String currentClassName = stack[depth].getClassName();
					if (currentClassName.equals(calledClassName)) {
						depth += 1;
					} else {
						ended = true;
						result = stack[depth];
					}
				} else {
					ended = true;
					result = null;
				}
			}
		}

		//
		return result;
	}
}
