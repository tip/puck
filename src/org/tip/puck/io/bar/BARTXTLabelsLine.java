package org.tip.puck.io.bar;

import org.tip.puck.io.bar.BARTXTFile.Format;

import fr.devinsy.util.StringList;

/**
 * This class represents a line of labels from a BAR TXT file.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class BARTXTLabelsLine extends StringList {

	private static final long serialVersionUID = -6301738360903920290L;

	private Format format;

	/**
	 * 
	 */
	public BARTXTLabelsLine() {
		super();
	}

	public Format getFormat() {
		return this.format;
	}

	public void setFormat(final Format format) {
		this.format = format;
	}
}
