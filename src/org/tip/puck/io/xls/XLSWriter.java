package org.tip.puck.io.xls;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.Locale;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents a translation writer from TXT to XLS.
 * 
 * @author Klaus Hamberger
 * @author TIP
 */
public class XLSWriter extends Writer {

	private static final Logger logger = LoggerFactory.getLogger(XLSWriter.class);

	private WritableWorkbook workbook;
	private int currentSheet;
	private int currentRow;
	private int currentColumn;

	/**
	 * @throws IOException
	 * @throws BiffException
	 * 
	 */
	public XLSWriter(final File file, final String... tabNames) throws IOException {

		WorkbookSettings settings = new WorkbookSettings();
		settings.setLocale(new Locale("fr", "FR"));
		this.workbook = Workbook.createWorkbook(file, settings);

		//
		if (tabNames != null) {
			int sheetCounter = 0;
			for (String tabName : tabNames) {
				this.workbook.createSheet(tabName, sheetCounter);
				sheetCounter += 1;
			}
		}

		this.currentSheet = 0;
		this.currentRow = 0;
		this.currentColumn = 0;
	}

	/**
	 * 
	 */
	@Override
	public void close() throws IOException {

		try {
			this.workbook.write();
			this.workbook.close();
		} catch (WriteException exception) {
			throw new IOException(exception.getMessage());
		}
	}

	/**
	 * 
	 */
	@Override
	public void flush() {
	}

	/**
	 * 
	 */
	@Override
	public void write(final char[] cbuf, final int off, final int len) throws IOException {

		write(new String(cbuf, off, len));
	}

	/**
	 * 
	 */
	@Override
	public void write(final String str, final int off, final int len) throws IOException {
		try {
			//
			String[] tokens = str.split("\t");

			//
			for (String token : tokens) {
				if (token.contains("\n")) {
					if (this.currentColumn == 0) {
						this.currentSheet += 1;
						this.currentRow = 0;
						this.currentColumn = 0;
					} else {
						this.currentRow += 1;
						this.currentColumn = 0;
					}
				} else {
					//
					if (NumberUtils.isDigits(token)) {
						this.workbook.getSheet(this.currentSheet).addCell(new Number(this.currentColumn, this.currentRow, new Double(token).doubleValue()));
					} else {
						//
						if ((this.currentSheet >= this.workbook.getNumberOfSheets()) && (this.currentRow == 0) && (this.currentColumn == 0)) {
							// Create relation sheet with relation model name.
							this.workbook.createSheet(token, this.currentSheet);
						}

						//
						this.workbook.getSheet(this.currentSheet).addCell(new Label(this.currentColumn, this.currentRow, token));
					}

					//
					this.currentColumn += 1;
				}
			}
		} catch (RowsExceededException exception) {
			//
			throw new IOException("JXL RowsExceededException: " + exception.getMessage());

		} catch (WriteException exception) {
			//
			throw new IOException("JXL WriteException: " + exception.getMessage());

		} catch (IndexOutOfBoundsException exception) {
			//
			throw new IOException("JXL IndexOutOfBoundsException: " + exception.getMessage());
		}
	}
}
