package org.tip.puck.partitions.graphs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import oldcore.trash.RingGroupMap;

import org.tip.puck.PuckException;
import org.tip.puck.alliancenets.AllianceNet;
import org.tip.puck.alliancenets.Group;
import org.tip.puck.census.chains.Chain;
import org.tip.puck.census.workers.CircuitFinder;
import org.tip.puck.census.workers.SymmetryType;
import org.tip.puck.graphs.ClusterPair;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.Link;
import org.tip.puck.graphs.Node;
import org.tip.puck.graphs.NodeComparatorByLabel;
import org.tip.puck.matrix.Matrix;
import org.tip.puck.net.Family;
import org.tip.puck.net.Gender;
import org.tip.puck.net.Individual;
import org.tip.puck.net.Net;
import org.tip.puck.net.relations.Relation;
import org.tip.puck.net.relations.Relations;
import org.tip.puck.partitions.Cluster;
import org.tip.puck.partitions.Partition;
import org.tip.puck.partitions.PartitionMaker;
import org.tip.puck.report.Report;
import org.tip.puck.segmentation.Segmentation;
import org.tip.puck.util.MathUtils;
import org.tip.puck.util.Value;

import fr.devinsy.util.StringList;

/**
 * 
 * @author TIP
 */
public class ClusterNetworkUtils {

	public enum AllianceType {
		WIFE_HUSBAND,
		SISTER_BROTHER,
		PARENT_CHILD
	}

	public enum LineType {
		SIMPLE_ARC,
		WEIGHTED_ARC;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Graph<Cluster<Individual>> createAllianceNetwork(final Segmentation source, final String partitionLabel, final LineType lineType)
			throws PuckException {
		Graph<Cluster<Individual>> result;

		if ((source == null) || (partitionLabel == null)) {
			throw new IllegalArgumentException("Null parameter detected.");
		} else {
			//
			Partition<Individual> partition = PartitionMaker.createRaw(source.getLabel(), source.getCurrentIndividuals(), partitionLabel, source.getGeography());

			//
			result = new Graph<Cluster<Individual>>("Alliance Network " + source.getLabel() + " " + partitionLabel);
			for (Cluster<Individual> cluster : partition.getClusters()) {
				if (!cluster.isNull()) {
					result.addNode(cluster);
				}
			}

			//
			for (Family family : source.getCurrentFamilies()) {
				if (family.hasMarried()) {
					Individual husband = family.getHusband();
					Individual wife = family.getWife();
					if ((husband != null) && (wife != null)) {
						Cluster<Individual> husbandsCluster = partition.getCluster(husband);
						Cluster<Individual> wifesCluster = partition.getCluster(wife);
						if (husbandsCluster != null && !husbandsCluster.isNull() && wifesCluster != null && !wifesCluster.isNull()) {
							if (lineType == LineType.WEIGHTED_ARC) {
								result.incArcWeight(wifesCluster, husbandsCluster);
							} else if (lineType == LineType.SIMPLE_ARC) {
								if (result.getArc(wifesCluster, husbandsCluster) == null) {
									result.addArc(wifesCluster, husbandsCluster, 1);
								}
							}
						}
					}
				}
			}
		}

		//
		return result;
	}

	/*	public static Graph<Cluster<Chain>> createCircuitIntersectionNetwork1(final CircuitFinder source) throws PuckException {
			Graph<Cluster<Chain>> result;

			if (source == null) {
				throw new NullPointerException("Null parameter detected.");
			} else {
				//
				result = new Graph<Cluster<Chain>>("Circuit Intersection Network: " + source.getLabel());

				int z = 0;
				
				for (Cluster<Chain> coupleCluster : source.byCouplesWithoutTransformation(Gender.MALE).getClusters().toListSortedByValue()) {
					for (int i = 0; i < coupleCluster.count(); i++) {
						Cluster<Chain> typeCluster1 = source.getCircuits().getCluster(coupleCluster.getItems().get(i).standard());
						for (int j = 0; j <= i; j++) {
							Cluster<Chain> typeCluster2 = source.getCircuits().getCluster(coupleCluster.getItems().get(j).standard());
							if (!typeCluster1.isNull() && !typeCluster2.isNull()) {
								result.incEdgeWeight(typeCluster2, typeCluster1);
								if (typeCluster1.getLabel().equals("0") && (i==j)) {
									z++;
									System.out.println(z+" "+result.getEdgeWeight(result.getNode(typeCluster1), result.getNode(typeCluster1))+" "+coupleCluster.getLabel());
								}
							}
						}
					}
				}
			}
			//
			return result;
		}*/

	public static Graph<Cluster<Chain>> createCircuitIntersectionNetwork(final CircuitFinder source) throws PuckException {
		Graph<Cluster<Chain>> result;

		if (source == null) {
			throw new NullPointerException("Null parameter detected.");
		} else {
			//
			result = new Graph<Cluster<Chain>>("Circuit Intersection Network: " + source.getLabel());
			
			Map<Chain,List<Cluster<Chain>>> clustersByCouples = source.clustersByCouples(Gender.MALE);

			for (Chain couple : clustersByCouples.keySet()) {
				List<Cluster<Chain>> clusters = clustersByCouples.get(couple);
				for (int i = 0; i < clusters.size(); i++) {
					Cluster<Chain> typeCluster1 = clusters.get(i);
					for (int j = 0; j <= i; j++) {
						Cluster<Chain> typeCluster2 = clusters.get(j);
						if (!typeCluster1.isNull() && !typeCluster2.isNull()) {
							result.incEdgeWeight(typeCluster2, typeCluster1);
							Link<Cluster<Chain>> edge = result.getEdge(typeCluster2, typeCluster1);
							Cluster<Chain> couples = edge.getReferent();
							if (couples == null) {
								couples = new Cluster<Chain>(new Value(typeCluster1.getLabel() + " " + typeCluster2.getLabel()));
								edge.setReferent(couples);
							}
							couples.put(couple);
						}
					}
				}
			}
		}
		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param birthPartitionLabel
	 * @param deathPartitionLabel
	 * @return
	 * @throws PuckException
	 */
	public static Graph<ClusterPair<Individual>> createFlowNetwork(final Segmentation source, final String sourceLabel, final String targetLabel,
			final LineType lineType) throws PuckException {
		Graph<ClusterPair<Individual>> result;

		if ((source == null) || (sourceLabel == null) || (targetLabel == null)) {

			throw new IllegalArgumentException("Null parameter detected.");

		} else {
			//
			Partition<Individual> sourcePartition = PartitionMaker.createRaw(source.getLabel(), source.getCurrentIndividuals(), sourceLabel, source.getGeography());
			Partition<Individual> targetPartition = PartitionMaker.createRaw(source.getLabel(), source.getCurrentIndividuals(), targetLabel, source.getGeography());

			//
			result = new Graph<ClusterPair<Individual>>("Flow Network: " + source.getLabel() + " " + sourceLabel + " " + targetLabel);

			//
			for (Individual individual : source.getCurrentIndividuals()) {
				//
				Cluster<Individual> sourceCluster = sourcePartition.getCluster(individual);
				Cluster<Individual> targetCluster = targetPartition.getCluster(individual);

				//
				ClusterPair<Individual> sourcePair = new ClusterPair<Individual>(sourceCluster, targetPartition.getCluster(sourceCluster.getValue()));
				ClusterPair<Individual> targetPair = new ClusterPair<Individual>(sourcePartition.getCluster(targetCluster.getValue()), targetCluster);

				//
				if (!sourcePair.getLeft().isNull() && !targetPair.getRight().isNull()) {
					if (lineType == LineType.WEIGHTED_ARC) {
						result.incArcWeight(sourcePair, targetPair);
					} else if (lineType == LineType.SIMPLE_ARC) {
						if (result.getArc(sourcePair, targetPair) == null) {
							result.addArc(sourcePair, targetPair, 1);
						}
					}
				}
			}
		}

		//
		return result;
	}

	/*	public static Graph<Cluster<Chain>> createCircuitIntersectionNetwork(final Map<Chain,Set<Cluster<Chain>>> map, CircuitFinder source) throws PuckException {
			Graph<Cluster<Chain>> result;

			if (map == null) {
				throw new NullPointerException("Null parameter detected.");
			} else {
				//
				result = new Graph<Cluster<Chain>>("Circuit Intersection Network: " + source.getLabel());

				for (Set<Cluster<Chain>> clusters : map.values()){
					List<Cluster<Chain>> coupleCluster = new ArrayList<Cluster<Chain>>(clusters);
					for (int i = 0; i < coupleCluster.size(); i++) {
						Cluster<Chain> typeCluster1 = coupleCluster.get(i);
	//					Cluster<Chain> typeCluster1 = source.getCircuits().getCluster(coupleCluster.get(i).standard());
						for (int j = 0; j < i; j++) {
							Cluster<Chain> typeCluster2 = coupleCluster.get(j);
	//						Cluster<Chain> typeCluster2 = source.getCircuits().getCluster(coupleCluster.get(j).standard());
							if (!typeCluster1.isNull() && !typeCluster2.isNull()) {
								result.incEdgeWeight(typeCluster2, typeCluster1);
							}
						}
					}
				}
				
			}
			//
			return result;
		}*/

	/**
	 * 
	 * @param source
	 * @param birthPartitionLabel
	 * @param deathPartitionLabel
	 * @return
	 * @throws PuckException
	 */
	public static Graph<ClusterPair<Relation>> createFlowNetwork(final Segmentation source, final String relationLabel, final String sourceLabel,
			final String targetLabel, final String labelParameter, final LineType lineType) throws PuckException {
		Graph<ClusterPair<Relation>> result;

		if ((source == null) || (sourceLabel == null) || (targetLabel == null) || (relationLabel == null)) {

			throw new IllegalArgumentException("Null parameter detected.");

		} else {
			Relations relations = source.getCurrentRelations().getByModelName(relationLabel);
			//

			Partition<Relation> sourcePartition = PartitionMaker.createRaw(source.getLabel(), source.getCurrentRelations(), sourceLabel, labelParameter, source.getGeography());
			Partition<Relation> targetPartition = PartitionMaker.createRaw(source.getLabel(), source.getCurrentRelations(), targetLabel, labelParameter, source.getGeography());

			//
			result = new Graph<ClusterPair<Relation>>("Flow Network: " + source.getLabel() + " " + sourceLabel + " " + targetLabel);

			//
			for (Relation relation : relations) {
				
				//
				Cluster<Relation> sourceCluster = sourcePartition.getCluster(relation);
				Cluster<Relation> targetCluster = targetPartition.getCluster(relation);

				//
				ClusterPair<Relation> sourcePair = new ClusterPair<Relation>(sourceCluster, targetPartition.getCluster(sourceCluster.getValue()));
				ClusterPair<Relation> targetPair = new ClusterPair<Relation>(sourcePartition.getCluster(targetCluster.getValue()), targetCluster);

				//
				if (!sourcePair.getLeft().isNull() && !targetPair.getRight().isNull()) {
					if (lineType == LineType.WEIGHTED_ARC) {
						result.incArcWeight(sourcePair, targetPair);
					} else if (lineType == LineType.SIMPLE_ARC) {
						if (result.getArc(sourcePair, targetPair) == null) {
							result.addArc(sourcePair, targetPair, 1);
						}
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Graph<Cluster<Individual>> createParentChildNetwork(final Segmentation source, final String partitionLabel, final LineType lineType)
			throws PuckException {
		Graph<Cluster<Individual>> result;

		if ((source == null) || (partitionLabel == null)) {
			throw new NullPointerException("Null parameter detected.");
		} else {
			//
			Partition<Individual> partition = PartitionMaker.createRaw(source.getLabel(), source.getCurrentIndividuals(), partitionLabel, source.getGeography());

			//
			result = new Graph<Cluster<Individual>>("Parent-Child Network " + source.getLabel() + " " + partitionLabel);
			for (Cluster<Individual> cluster : partition.getClusters()) {
				if (!cluster.isNull()) {
					result.addNode(cluster);
				}
			}

			//
			for (Individual child : source.getCurrentIndividuals()) {
				if (child != null && !child.isOrphan()) {
					for (Individual parent : child.getParents()) {
						Cluster<Individual> childCluster = partition.getCluster(child);
						Cluster<Individual> parentCluster = partition.getCluster(parent);
						if (childCluster != null && !childCluster.isNull() && parentCluster != null && !parentCluster.isNull()) {
							if (lineType == LineType.WEIGHTED_ARC) {
								result.incArcWeight(parentCluster, childCluster);
							} else if (lineType == LineType.SIMPLE_ARC) {
								if (result.getArc(parentCluster, childCluster) == null) {
									result.addArc(parentCluster, childCluster, 1);
								}
							}
						}
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws PuckException
	 */
	public static Graph<Cluster<Individual>> createSiblingNetwork(final Segmentation source, final String partitionLabel, final LineType lineType)
			throws PuckException {
		Graph<Cluster<Individual>> result;

		if ((source == null) || (partitionLabel == null)) {
			throw new NullPointerException("Null parameter detected.");
		} else {
			//
			Partition<Individual> partition = PartitionMaker.createRaw(source.getLabel(), source.getCurrentIndividuals(), partitionLabel, source.getGeography());

			//
			result = new Graph<Cluster<Individual>>("Sibling Network " + source.getLabel() + " " + partitionLabel);
			for (Cluster<Individual> cluster : partition.getClusters()) {
				if (!cluster.isNull()) {
					result.addNode(cluster);
				}
			}

			//
			for (Family family : source.getCurrentFamilies()) {
				for (Individual brother : family.getChildren()) {
					if (brother != null && brother.isMale() && brother.isNotSingle()) {
						for (Individual sister : family.getChildren()) {
							if (sister != null && sister.isFemale() && sister.isNotSingle()) {
								Cluster<Individual> brothersCluster = partition.getCluster(brother);
								Cluster<Individual> sistersCluster = partition.getCluster(sister);
								if (brothersCluster != null && !brothersCluster.isNull() && sistersCluster != null && !sistersCluster.isNull()) {
									if (lineType == LineType.WEIGHTED_ARC) {
										result.incArcWeight(sistersCluster, brothersCluster);
									} else if (lineType == LineType.SIMPLE_ARC) {
										if (result.getArc(sistersCluster, brothersCluster) == null) {
											result.addArc(sistersCluster, brothersCluster, 1);
										}
									}
								}
							}
						}
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param birthPartitionLabel
	 * @param deathPartitionLabel
	 * @return
	 * @throws PuckException
	 */
	public static Graph<Cluster<Individual>> createUnmergedFlowNetwork(final Net source, final String sourceLabel, final String targetLabel)
			throws PuckException {
		Graph<Cluster<Individual>> result;

		if ((source == null) || (sourceLabel == null) || (targetLabel == null)) {
			throw new NullPointerException("Null parameter detected.");
		} else {

			//
			Partition<Individual> sourcePartition = PartitionMaker.createRaw(source, sourceLabel);
			Partition<Individual> targetPartition = PartitionMaker.createRaw(source, targetLabel);

			//
			result = new Graph<Cluster<Individual>>("Unmerged Flow Network: " + source.getLabel() + " " + sourceLabel + " " + targetLabel);

			//
			for (Individual individual : source.individuals()) {
				//
				Cluster<Individual> sourceCluster = sourcePartition.getCluster(individual);
				Cluster<Individual> targetCluster = targetPartition.getCluster(individual);

				//
				if ((sourceCluster.getLabel() != null) && (targetCluster.getLabel() != null)) {
					result.incArcWeight(sourceCluster, targetCluster);
				}
			}
		}

		//
		return result;
	}

	public static Map<Node<Cluster<Chain>>, Integer[]> getCircuitAndNetMarriageNumbers(final Graph<Cluster<Chain>> source) {
		Map<Node<Cluster<Chain>>, Integer[]> result;

		result = new TreeMap<Node<Cluster<Chain>>, Integer[]>(new NodeComparatorByLabel<Cluster<Chain>>());

		for (Node<Cluster<Chain>> node : source.getNodes().toListSortedByLabel()) {
			Integer[] numbers = new Integer[2];
			Set<Chain> intersectingMarriages = new HashSet<Chain>();
			for (Link<Cluster<Chain>> edge : node.getEdges()) {
				if (!edge.isLoop()) {
					for (Chain couple : edge.getReferent().getItems()) {
						intersectingMarriages.add(couple);
					}
				}
			}
			Set<Chain> nonIntersectingMarriages = new HashSet<Chain>();
			for (Chain circuit : node.getReferent().getItems()) {
				for (Chain couple : circuit.getCouples()) {
					if (!intersectingMarriages.contains(couple)) {
						nonIntersectingMarriages.add(couple);
					}
				}
			}
			numbers[0] = node.getReferent().size(); // number of circuits
			numbers[1] = nonIntersectingMarriages.size(); // number of marriages
															// that are in no
															// other circuit
			result.put(node, numbers);
		}
		//
		return result;

	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static <E> AllianceNet graphToAllianceNet(final Graph<E> source) {
		AllianceNet result;

		//
		Matrix matrix = source.getSquareMatrix().getMatrix();

		//
		result = new AllianceNet(true);

		// Add nodes.
		Vector<Group> groups = new Vector<Group>();
		for (int columnIndex = 0; columnIndex < matrix.getColDim(); columnIndex++) {
			Group group = result.addNode();
			groups.add(group);
		}

		//
		for (int rowIndex = 0; rowIndex < matrix.getRowDim(); rowIndex++) {
			//
			for (int columnIndex = 0; columnIndex < matrix.getColDim(); columnIndex++) {
				result.addEdge(groups.get(rowIndex), groups.get(columnIndex), matrix.get(rowIndex, columnIndex));
			}
		}

		//
		return result;
	}

}
