package org.tip.puck.visualization.layouts;

import java.util.*;
import org.gephi.graph.api.*;
import org.tip.puck.visualization.layouts.hierarchical.datastructs.chain.NodeLayoutData;

/**
 * @date Jun 23, 2012
 *
 * @author Edoardo Savoia
 */
public class GraphSource {

    private final MixedGraph graph;

    public GraphSource(MixedGraph graph) {
	this.graph = graph;
    }

    public Node findTopParent(Node node) {
	Node topParent = node;
	Set<Node> parents = getParents(node);
	if (parents.size() == 1) {
	    topParent = findTopParent(parents.iterator().next());
	}
	return topParent;
    }

    public Set<Node> getNeighbours(Node node) {
	Set<Node> neighbours = new HashSet<Node>();
	Set<Node> children = getChildren(node);
	for (Node child : children) {
	    Node otherParent = getOtherParent(child, node);
	    // Set<Node> parents = getParents(child);
	    // parents.remove(node);
	    if (otherParent != null) {
		neighbours.add(otherParent);
	    }
	}
	return neighbours;
    }

    public Set<Node> getChildren(Node node) {
	// MixedGraph graph = graphModel.getMixedGraph();
	EdgeIterable edgesIterable = graph.getEdges(node);
	Set<Node> list = new HashSet<Node>();
	for (EdgeIterator it = edgesIterable.iterator(); it.hasNext();) {
	    Edge edge = it.next();
	    if (edge.isDirected() && edge.getSource().equals(node)) {
		list.add(edge.getTarget());
	    }
	}
	return list;
    }

    public Set<Node> getRedChildren(Node node) {
	// MixedGraph graph = graphModel.getMixedGraph();
	EdgeIterable edgesIterable = graph.getEdges(node);
	Set<Node> list = new HashSet<Node>();
	for (EdgeIterator it = edgesIterable.iterator(); it.hasNext();) {
	    Edge edge = it.next();
	    if (edge.isDirected() && edge.getSource().equals(node)) {
		if (NodeLayoutData.isColored(node, NodeLayoutData.Color.RED)) {
		    list.add(edge.getTarget());
		}
	    }
	}
	return list;
    }

    public Node getOtherParent(Node node, Node parent) {
	Set<Node> parents = getParents(node);
	if (parents.size() > 1) {
	    parents.remove(parent);
	}
	Iterator<Node> iterator = parents.iterator();
	return iterator.hasNext() ? iterator.next() : null;
    }

    public Set<Node> getParents(Node node) {
	// MixedGraph graph = graphModel.getMixedGraph();
	EdgeIterable edgesIterable = graph.getEdges(node);
	Set<Node> list = new HashSet<Node>();
	for (EdgeIterator it = edgesIterable.iterator(); it.hasNext();) {
	    Edge edge = it.next();
	    if (edge.isDirected() && edge.getTarget().equals(node)) {
		list.add(edge.getSource());
	    }
	}
	return list;
    }

    public Collection<Edge> getNeighbourEdges(Node node) {
	LinkedList<Edge> list = new LinkedList<Edge>();
	EdgeIterable edgesIterable = graph.getEdges(node);
	for (EdgeIterator it = edgesIterable.iterator(); it.hasNext();) {
	    Edge edge = it.next();
	    if (edge.isDirected()) {
		list.add(edge);
	    }
	}
	return list;
    }

   
}
