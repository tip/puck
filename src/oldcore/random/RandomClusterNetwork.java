package oldcore.random;

import static oldcore.trash.Mat.event;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oldcore.trash.ClusterNetwork;

import org.tip.puck.net.Individual;


/**
 * The class for simulating alliance networks
 * @author Klaus Hamberger
 * @since 11-05-22
 */
@SuppressWarnings("serial")
public class RandomClusterNetwork extends ClusterNetwork<Individual> {
	
	static int arcs;
//	int m;
//	boolean[][] nonrandom;
	
	static double[] inertia;
	
/*	private static int arcs(int i){
		int[] arcs = {4000,2000};
		return arcs[i];
	}*/
	
	public static void clusterNetworkSimulation (int runs){
		int size = 100;
		
		double concentrationIndex = 0.;
		double symmetryIndex = 0.;
		double endogamicConcentrationIndex = 0.;
		double endogamyIndex = 0.;
		
		for (int i=0;i<runs;i++){
			RandomClusterNetwork network = new RandomClusterNetwork(size);
			network.matrix.analyze();
			concentrationIndex = concentrationIndex + network.matrix.getConcentrationIndex();
			symmetryIndex = symmetryIndex + network.matrix.getSymmetryIndex();
			endogamicConcentrationIndex = endogamicConcentrationIndex + network.matrix.getEndogamicConcentrationIndex();
			endogamyIndex = endogamyIndex + network.matrix.getEndogamyIndex();
		}
		concentrationIndex = concentrationIndex/runs;
		symmetryIndex = symmetryIndex/runs;
		endogamicConcentrationIndex = endogamicConcentrationIndex/runs;
		endogamyIndex = endogamyIndex/runs;
		
		StringBuffer buffer = new StringBuffer(2048);
		
		buffer.append("Random Alliance Network Simulation - runs = "+runs);

		buffer.append("Nr of Groups = "+size);
		String[] s1 = {"Marriages", "Observations"};
		String[] s2 = {"marriage", "informant"};
		buffer.append("Nr of "+s1[0]+" ="+arcs);
		buffer.append("\n");
		buffer.append("Probability of male "+s2[0]+": "+pviri);
		buffer.append("\n");
		for (int j=0;j<3;j++){
//			if (i==1 && j==2) continue;
			buffer.append(endotext(0,j));
			buffer.append("\n");
		}
		buffer.append("\n");
		buffer.append("Weight distribution (marriages by cluster pairs)" + "\n");
		buffer.append("Mean concentration index:" + concentrationIndex + "\n");
		buffer.append("Mean endogamic concentration index: " + endogamicConcentrationIndex + "\n");
		buffer.append("Mean symmetry index: " + symmetryIndex + "\n");
		buffer.append("Mean endogamy index:\t" + endogamyIndex + "\n");
		buffer.append("\n" + "\n");
		
		String report = buffer.toString();
		System.out.println(report);
		
	}
	
	/**
		 * checks whether an integer set of a set map contains an Integer 
		 * @param map the set map
		 * @param e the key integer
		 * @param a the integer to be checked
		 * @return true if the set contains the integer
		 * @since 11-07-02
		 */
/*		private static boolean contains(Map<Integer,Set<Integer>> map, Integer e, Integer a){
			Set<Integer> set = map.get(e);
			if (set==null) return false;
			return set.contains(a);
		}*/
	/**
		 * temporary setting (should be parametrized by user)
		 * @param i the type of simulation (0 agent behaviour, 1 observer behaviour)
		 * @param j the degree of the neighborhood
		 * @return the local propensity
		 * @return
		 */
/*		private static double[] endo (int i){
			double[][] endo = {{0.0,0.0,0.0},{0.5,0.5,-1.}};
			return endo[i];
		}*/
		
	/**
	 * generates labels for the local propensities (for the protocol)
	 * @param i the type of simulation (0 agent behaviour, 1 observer behaviour)
	 * @param j the degree of the neighborhood
	 * @return the local propensity as a text following the corresponding label
	 */
/*	private static String endotext (int i, int j){
		String[] s1 = {"Agent", "Observer"};
		String[] s2 = {"Endogamy", "Relinking Preference", "Indirect Relinking Preference"};
		String s = s1[i]+" "+s2[j]+": ";
		if (endo(i)[j]<0) return s+"Random";
		else return s+endo(i)[j];
	}*/
/**
	 * puts an integer into a set mapped to another integer
	 * @param map the set map
	 * @param e the key integer
	 * @param a the integer to be put
	 * @since 11-07-02
	 */
/*	private static void put(Map<Integer,Set<Integer>> map, Integer e, Integer a){
		Set<Integer> set = map.get(e);
		if (set==null) {
			set = new HashSet<Integer>();
			map.put(e, set);
		}
		if (e!=a) set.add(a);
	}*/
	
	/**
	 * removes an integer from a set mapped to another integer
	 * @param map the set map
	 * @param e the key integer
	 * @param a the integer to be removed
	 * @since 11-07-02
	 */
/*	private static void remove(Map<Integer,Set<Integer>> map, Integer e, Integer a){
		Set<Integer> set = map.get(e);
		if (set==null) return;
		set.remove(a);
	}*/
	
	
/*	private static double viri(int i){
		double[] viri = {0.5,0.5};
		return viri[i];
	}*/
	
//	Matrix distances;
	
//	private Map<Integer,Set<Integer>> affines;

	/**
	 * IHM
	 * gets the content of field i of the OptionsScreen for Random Parameters
	 * @param i the field index
	 */
/*	private static boolean check (int i){
		return Puck.getValue(25,i);
	}*/

	//	double[][] endo;
	static double pviri;
	
	/**
	 * IHM
	 * gets the Integer in field i of the OptionsScreen for Random Parameters
	 * @param i the field index
	 */
/*	private static double d (int i){
		return new Double(s(i));
	}*/

	private List<String> protocol;
	
	
	/**
	 * IHM
	 * gets the content of field i of the OptionsScreen for Random Parameters
	 * @param i the field index
	 */
/*	private static String s (int i){
		final String def = ((String[])Puck.getParam("Field Labels",25))[i];
		return Puck.get("FIELDS "+25+" "+i,def);
	}*/
	
//	private Map<Integer,Set<Integer>> coaffines;
	
	int currentEgo;
	
	
	//	int circle;
	int mar;
	
	
	/***
	 * generates a RandomGroupNet based on another RandomGroupNet (modelization of observer behaviour)
	 * @param basicNet the underlying RandomGroupNet (result of agent behaviour)
	 * @since 11-07-02
	 */
/*	public RandomClusterNetwork (ClusterNetwork<Individual> basicNet){
		super(basicNet.size());
		initialize(1);
		setProtocol(1);
		int marriages = 0;
		distances = new Matrix(size());
		distances.isSymmetric=true;
		currentEgo = draw();
		for (mar=0;mar<arcs;mar++){
			boolean viri = event(pviri);
			Cluster<Individual> ego = get(currentEgo);
			Cluster<Individual> alter = get(drawClusterValue(basicNet.getAllies(currentEgo,viri)));
			if (alter!=null){
				if (viri) addLink(ego,alter,basicNet);
				else addLink(alter,ego,basicNet);
				marriages++;
			} 
			currentEgo = drawNode(1);
		}
		protocol.add(2, "Nr of Marriages = "+marriages+" (of "+arcs+")");
	}*/
		
	/**
	 * generates a RandomGroupNet (modelization of agent behavior)
	 * @since 11-05-22 (modified 11-07-02)
	 */
/*	public RandomClusterNetwork (int size){
		super(size);
		initialize(0);
		setProtocol(0);
		distances = new Matrix(size);
		distances.isSymmetric=true;
		int marriages = 0;
		for (mar=0;mar<arcs;mar++){
			currentEgo = draw();
			Cluster<Individual> ego = get(currentEgo);
			Cluster<Individual> alter = get(drawNode(0));
			if (alter!=null){
				if (event(pviri)) addLink(ego,alter,null);
				else addLink(alter,ego,null);
				marriages++;
			}
		}
		protocol.add(2, "Nr of Marriages = "+marriages+" (of "+arcs+")");
	}*/
	
/*	protected void addLink(Cluster<Individual> giver, Cluster<Individual> taker){
		giver.setLink(taker);
		taker.setInvLink(giver);
		
		matrix.augment((Integer)giver.getVal(), (Integer)taker.getVal());
	}*/

	/**
	 * sets a link between two nodes and modifies the affines and coaffines maps accordingly
	 * @param giver the ego node
	 * @param taker the alter node
	 * @param net the underlying network (or null if there is none)
	 */
/*	private void addLink(Cluster<Individual> giver, Cluster<Individual> taker, ClusterNetwork<Individual> net){
		addLink(giver,taker);
		giver.augmentWeight();
		taker.augmentWeight();
		int e = (Integer)(giver.getVal());
		int a = (Integer)(taker.getVal());
		distances.set(e, a, 1);
		for (int i=0;i<size();i++){
			adjustDistance(e,a,i);
			adjustDistance(a,e,i);
		}
		
		
//		put(affines,e,a);
//		put(affines,a,e);
//		remove(coaffines,e,a);
//		remove(coaffines,a,e);
		if (net!=null) {
			net.get(a).diminishLink(net.get(e));
//			int r = net.get(a).getLinks().get(net.get(e));
//			getProtocol().add(giver.getHeading()+" < "+taker.getHeading()+" ("+r+" links left)");
//			return;
		}
//		if (giver.getHeading().equals(currentEgo)) getProtocol().add(mar+": "+giver.getHeading()+" < "+taker.getHeading()+"\t"+circle);
//		else getProtocol().add(mar+": "+taker.getHeading()+" > "+giver.getHeading()+"\t"+circle);
		for (int c : affines.get(e)){
			if (affines.get(a).contains(c)) continue;
			put(coaffines,c,a);
			put(coaffines,a,c);
		}
		for (int c : affines.get(a)){
			if (affines.get(e).contains(c)) continue;
			put(coaffines,c,e);
			put(coaffines,e,c);
		}
	}*/
	
/*	private void adjustDistance (int ego, int medius, int alter){
		if (ego==alter) 
			return;
		int distance1 = distances.get(medius,alter);
		if (distance1==0) {
			return;
		}
		int distance2 = distances.get(ego,alter);
		if (distance2==0 || distance2>distance1+1) {
			distances.set(ego, alter,distance1+1);
		}
	}*/	

		
/*	private int draw (){
		return (int)(Math.random()*size());
	}*/
	
	/**
	 * draws a random integer from an integer collection
	 * @param pop the collection
	 * @return the random integer
	 * @since 11-05-22
	 */
/*	private Integer draw(Collection<Integer> pop){
		if (pop==null) return draw();
		if (pop.size()==0) return null;
		int a = (int)(Math.random()*pop.size());
		int i = 0;
		for (Integer id : pop){
			if (i==a) return id;
			i++;
		}
		System.out.println("Error "+i+" "+a);
		return null;
	}*/

	
	/**
	 * draws a random Cluster from a collection
	 * @param pop the collection
	 * @return the random Cluster
	 * @since 11-07-02
	 */
/*	private Integer drawClusterValue(Set<Cluster<Individual>> pop){
		if (pop==null || pop.size()==0) return null;
		int a = (int)(Math.random()*pop.size());
		int i = 0;
		for (Cluster<Individual> cluster : pop){
			if (i==a) return (Integer)(cluster.getVal());
			i++;
		}
		System.out.println("Error "+i+" "+a);
		return null;
	}*/
	
	/**
	 * draws a randomNode 
	 * @param i the type of node (0 alter/marriage partner, 1 ego/informant))
	 * @return the random node
	 */
/*	private Integer drawNode (int i){
//		circle = -1;
		for (int j=0;j<3;j++){
//			circle++;
			if (inertia[j]<0) return draw(getRest(j));
			if (!isInert(j)) continue;
			if (j==0) return currentEgo;
			Collection<Integer> partners = getPartners(j);
			if (partners.size()==0) return draw(getRest(j)); // formerly: continue
			return draw(partners);
		}
//		circle++;
		return draw(getRest(3));
	}*/

	/**
	 * a modification of the value getter
	 * @param i the key
	 * @return the value
	 */
/*	public Cluster<Individual> get (Integer i){
		if (i == null) return null;
		return super.get(i);
	}*/
	
	/**
	 * a shortcut for the affines and coaffines of current ego
	 * @param j a key (1 affines, 2 coaffines)
	 * @return the id set of affines/coaffines of current ego
	 */
/*	private Collection<Integer> getPartners(int j){
		Collection<Integer> partners = new ArrayList<Integer>();
		int[] distanceToEgo = distances.getRow(currentEgo);
		for (int i=0;i<size();i++) {
			if (distanceToEgo[i]==j) partners.add(i);
		}
		return partners;
	}*/
	
	/**
	 * gets the protocol
	 * @return the protocol
	 */
/*	public List<String> getProtocol() {
		return protocol;
	}*/
	
	/**
	 * gets the complement of a k-neighborhood
	 * @param k the distance of the neighborhood
	 * @return the complement of the k-neighborhood
	 */
/*	private Collection<Integer> getRest(int k){
		Collection<Integer> rest = new ArrayList<Integer>();
//		if (k==0) return null;
		for (int i=0;i<size();i++){
			if (k==0) {
				rest.add(i);
			} else {
				int distanceToEgo = distances.get(currentEgo, i);
				if (distanceToEgo >= k || (distanceToEgo == 0 && currentEgo != i)) {
					rest.add(i);
				}
			}
			
//			if (k>0 && i==currentEgo) continue;
//			if (k>1 && contains(affines, currentEgo, i)) continue;
//			if (k>2 && contains(coaffines, currentEgo, i)) continue;
//			rest.add(i);
		}
		return rest;
	}*/
	
	
	/**
	 * initializes the field parameters
	 */
/*	private void initialize(int key){
		inertia = endo(key);
		arcs = arcs(key);
		pviri = viri(key);
//		affines = new HashMap<Integer,Set<Integer>>();
//		coaffines = new HashMap<Integer,Set<Integer>>();
	}*/
	
	/**
	 * a shortcut for local propensities 
	 * @param i the type of simulation (0 agent behaviour, 1 observer behaviour)
	 * @param j the degree of the neighborhood
	 * @return the local propensity
	 */
/*	private boolean isInert(int i){
		return event(inertia[i]);
	}*/
	
/*	public void printProtocol(){
		for (String s: protocol){
			System.out.println(s);
		}
	}*/
	

	/**
	 * initializes the protocol
	 * @param i the type of simulation (0 agent behavior, 1 observer behaviour)
	 */
/*	private void setProtocol(int i){
		protocol = new ArrayList<String>();
		getProtocol().add("Nr of Groups = "+size());
		String[] s1 = {"Marriages", "Observations"};
		String[] s2 = {"marriage", "informant"};
		getProtocol().add("Nr of "+s1[i]+" ="+arcs);
		getProtocol().add("Probability of male "+s2[i]+": "+pviri);
		for (int j=0;j<3;j++){
			if (i==1 && j==2) continue;
			getProtocol().add(endotext(i,j));
		}
		getProtocol().add("");
	}*/
	

	
	
	

}
