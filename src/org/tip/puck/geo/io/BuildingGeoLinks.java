package org.tip.puck.geo.io;

import javax.swing.JProgressBar;

import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.tip.puck.geo.Coordinate2;
import org.tip.puck.geo.Place;
import org.tip.puck.geo.tools.GeotoolsUtils;
import org.tip.puck.graphs.Graph;
import org.tip.puck.graphs.Link;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateList;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.LinearRing;

public class BuildingGeoLinks {

	DefaultFeatureCollection featureLinesCollection;
	SimpleFeatureBuilder featureBuilder;

	GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory(null);

	SimpleFeatureType lineFeatureType;
	Object[] attributesSchema = new Object[3];

	public BuildingGeoLinks() {
		SimpleFeatureTypeBuilder b = new SimpleFeatureTypeBuilder();

		// set the name
		b.setName("SCHEMA_LINES");

		// Add attributes
		b.add("name", String.class);
		b.add("value", Double.class);
		// add a geometry property
		b.setCRS(DefaultGeographicCRS.WGS84); // set crs first
		b.add("the_geom", LineString.class); // then add geometry

		// build the type
		this.lineFeatureType = b.buildFeatureType();

		this.featureLinesCollection = new DefaultFeatureCollection("internal", this.lineFeatureType);

		this.featureBuilder = new SimpleFeatureBuilder(this.lineFeatureType);
	}

	public DefaultFeatureCollection buildLinkGeometry(final JProgressBar progressBar, final Graph<Place> graphPlaces) {

		for (Link<Place> link : graphPlaces.getArcs()) {

			if (link.getSourceNode().getReferent().getCoordinate2() != null & link.getTargetNode().getReferent().getCoordinate2() != null) {

				if (link.getSourceId() != link.getTargetId()) {

					Coordinate[] tabCoord = new Coordinate[] { GeotoolsUtils.convert(link.getSourceNode().getReferent().getCoordinate2()),
							GeotoolsUtils.convert(link.getTargetNode().getReferent().getCoordinate2()) };

					LineString line = this.geometryFactory.createLineString(tabCoord);
					this.attributesSchema[2] = line;

				} else {

					// Create a LineString ring from circle coordinates

					int nbPoints = 10;
					double radius = 0.1;
					double x_start = GeotoolsUtils.convert(link.getSourceNode().getReferent().getCoordinate2()).x;
					double y_start = GeotoolsUtils.convert(link.getSourceNode().getReferent().getCoordinate2()).y + radius;

					Coordinate[] coordTabCircle = new Coordinate[2 * nbPoints + 1];

					int i = 0;
					for (double ang_rd = -Math.PI / 2; ang_rd <= 3 * Math.PI / 2; ang_rd += Math.PI / nbPoints) {

						double x = radius * Math.cos(ang_rd) + x_start;
						double y = radius * Math.sin(ang_rd) + y_start;
						coordTabCircle[i++] = new Coordinate(x, y);

					}

					CoordinateList coordList2 = new CoordinateList(coordTabCircle);

					LinearRing ring = this.geometryFactory.createLinearRing(coordList2.toCoordinateArray());
					this.attributesSchema[2] = ring;
				}

				this.attributesSchema[0] = link.getSourceNode().getReferent().getToponym() + " -- " + link.getTargetNode().getReferent().getToponym();
				this.attributesSchema[1] = link.getWeight();

				SimpleFeature f = SimpleFeatureBuilder.build(this.lineFeatureType, this.attributesSchema, null);

				this.featureLinesCollection.add(f);

			}
			if (progressBar != null) {
				int i = progressBar.getValue();
				progressBar.setValue(i++);
			}
		}
		return this.featureLinesCollection;

	}

	/**
	 * 
	 * @param progressBar
	 * @param graphPlaces
	 * @return
	 */
	public DefaultFeatureCollection buildLinkGeometry2(final JProgressBar progressBar, final Graph<Place> graphPlaces) {

		for (Link<Place> link : graphPlaces.getArcs()) {

			if (link.getSourceNode().getReferent().getCoordinate2() != null & link.getTargetNode().getReferent().getCoordinate2() != null) {

				if (link.getSourceId() != link.getTargetId()) {

					Coordinate[] tabCoord = new Coordinate[] { GeotoolsUtils.convert(link.getSourceNode().getReferent().getCoordinate2()),
							GeotoolsUtils.convert(link.getTargetNode().getReferent().getCoordinate2()) };

					LineString line = this.geometryFactory.createLineString(tabCoord);
					this.attributesSchema[2] = line;

				} else {

					// Create a LineString ring from circle coordinates

					Coordinate sourceCoordinate = GeotoolsUtils.convert(link.getSourceNode().getReferent().getCoordinate2());

					int nbPoints = 10;
					double radius = 0.1;
					double x_start = sourceCoordinate.x;
					double y_start = sourceCoordinate.y + radius;

					Coordinate[] coordTabCircle = new Coordinate[2 * nbPoints + 1];

					int i = 0;
					for (double ang_rd = -Math.PI / 2; ang_rd <= 3 * Math.PI / 2; ang_rd += Math.PI / nbPoints) {

						double x = radius * Math.cos(ang_rd) + x_start;
						double y = radius * Math.sin(ang_rd) + y_start;
						coordTabCircle[i++] = new Coordinate(x, y);

					}

					// Patch waiting validation: be sure that first point and
					// last point have same coordinates.
					if (i > 0) {
						coordTabCircle[i - 1].x = coordTabCircle[0].x;
						coordTabCircle[i - 1].y = coordTabCircle[0].y;
					}

					CoordinateList coordList2 = new CoordinateList(coordTabCircle);

					LinearRing ring = this.geometryFactory.createLinearRing(coordList2.toCoordinateArray());
					this.attributesSchema[2] = ring;
				}

				this.attributesSchema[0] = link.getSourceNode().getReferent().getToponym() + " -- " + link.getTargetNode().getReferent().getToponym();
				this.attributesSchema[1] = link.getWeight();

				SimpleFeature f = SimpleFeatureBuilder.build(this.lineFeatureType, this.attributesSchema, null);

				this.featureLinesCollection.add(f);

			}
			if (progressBar != null) {
				int i = progressBar.getValue();
				progressBar.setValue(i++);
			}
		}
		return this.featureLinesCollection;

	}

	public SimpleFeatureBuilder getFeatureBuilder() {
		return this.featureBuilder;
	}

	public DefaultFeatureCollection getFeatureLinesCollection() {
		return this.featureLinesCollection;
	}

	public GeometryFactory getGeometryFactory() {
		return this.geometryFactory;
	}

	public SimpleFeatureType getLineFeatureType() {
		return this.lineFeatureType;
	}

	public void setFeatureBuilder(final SimpleFeatureBuilder featureBuilder) {
		this.featureBuilder = featureBuilder;
	}

	public void setFeatureLinesCollection(final DefaultFeatureCollection featureLinesCollection) {
		this.featureLinesCollection = featureLinesCollection;
	}

	public void setGeometryFactory(final GeometryFactory geometryFactory) {
		this.geometryFactory = geometryFactory;
	}

	public void setLineFeatureType(final SimpleFeatureType lineFeatureType) {
		this.lineFeatureType = lineFeatureType;
	}

}