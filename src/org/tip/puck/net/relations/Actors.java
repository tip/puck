package org.tip.puck.net.relations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.tip.puck.net.Individual;
import org.tip.puck.net.relations.roles.RoleActor;

/**
 * 
 * @author TIP
 */
public class Actors extends ArrayList<Actor> {

	private static final long serialVersionUID = 3026982626076910942L;

	/**
	 * 
	 */
	public Actors() {
		super();
	}

	/**
	 * 
	 */
	public Actors(final Actors source) {
		super(source);
	}
	
	public boolean addNew(final Actor actor){
		boolean result;
		
		if (contains(actor)){
			result = false;
		} else {
			result = add(actor);
		}
		//
		return result;
	}
	

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Actor get(final int individualId, final Role role) {
		Actor result;

		boolean ended = false;
		result = null;
		Iterator<Actor> iterator = this.iterator();
		while (!ended) {
			//
			if (iterator.hasNext()) {
				//
				Actor actor = iterator.next();

				if ((actor.getId() == individualId) && ((actor.getRole() == role))) {
					//
					ended = true;
					result = actor;
				}
			} else {
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public Actor get(final int individualId, final String roleName) {
		Actor result;

		boolean ended = false;
		result = null;
		Iterator<Actor> iterator = this.iterator();
		while (!ended) {
			//
			if (iterator.hasNext()) {
				//
				Actor actor = iterator.next();

				if ((actor.getId() == individualId) && (StringUtils.equals(actor.getRole().getName(), roleName))) {
					//
					ended = true;
					result = actor;
				}
			} else {
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public Actors getByIndividual(final Individual individual) {
		Actors result;

		result = new Actors();
		for (Actor actor : this) {
			if (actor.getIndividual().equals(individual)) {
				result.add(actor);
			}
		}

		//
		return result;
	}



	/**
	 * 
	 * @param id
	 * @return
	 */
	public Actors getById(final int id) {
		Actors result;

		result = new Actors();
		for (Actor actor : this) {
			if (actor.getId() == id) {
				result.add(actor);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public Actors getByRole(final Role role) {
		Actors result;

		result = new Actors();
		for (Actor actor : this) {
			if (actor.getRole().equals(role)) {
				result.add(actor);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public Actors getByRole(final String roleName) {
		Actors result;

		result = new Actors();
		for (Actor actor : this) {
			if (StringUtils.equals(actor.getRole().getName(), roleName)) {
				result.add(actor);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Actors getOthers(final int id) {
		Actors result;

		result = new Actors();
		for (Actor actor : this) {
			if (actor.getId() != id) {
				result.add(actor);
			}
		}

		//
		return result;
	}

	public Roles getRoles(final int id) {
		Roles result;

		result = new Roles();
		for (Actor actor : this) {
			if (actor.getId() == id) {
				result.add(actor.getRole());
			}
		}
		//
		return result;
	}
	
	public Roles getRoles(){
		Roles result;

		result = new Roles();
		for (Actor actor : this) {
			if (!result.contains(actor.getRole())){
				result.add(actor.getRole());
			}
		}
		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public boolean hasActor(final int id) {
		boolean result;

		boolean ended = false;
		result = false;
		Iterator<Actor> iterator = this.iterator();
		while (!ended) {
			if (iterator.hasNext()) {
				Actor actor = iterator.next();
				if (actor.getId() == id) {
					ended = true;
					result = true;
				}
			} else {
				ended = true;
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @param role
	 * @return
	 */
	public boolean hasActor(final int id, final String role) {
		boolean result;

		boolean ended = false;
		result = false;
		Iterator<Actor> iterator = this.iterator();
		while (!ended) {
			if (iterator.hasNext()) {
				Actor actor = iterator.next();
				if ((actor.getId() == id) && (StringUtils.equals(actor.getRole().getName(), role))) {
					ended = true;
					result = true;
				}
			} else {
				ended = true;
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Actor[] toArray() {
		Actor[] result;

		result = new Actor[this.size()];
		int actorCount = 0;
		for (Actor actor : this) {
			//
			result[actorCount] = actor;
			actorCount += 1;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Actor> toList() {
		List<Actor> result;

		result = new ArrayList<Actor>(this.size());
		for (Actor actor : this) {
			result.add(actor);
		}

		//
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<Actor> toSortedList() {
		List<Actor> result;

		result = toList();
		Collections.sort(result);

		//
		return result;
	}

}
